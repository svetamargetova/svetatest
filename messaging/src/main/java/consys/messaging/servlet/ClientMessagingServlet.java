package consys.messaging.servlet;

import consys.messaging.SystemManager;
import consys.messaging.interfaces.DoMessageAction;
import consys.messaging.processing.SAXRequestHandler;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet pro zpracovavani klientskych pozadavku
 * @author pepa
 */
public class ClientMessagingServlet extends HttpServlet {

    private static final long serialVersionUID = -6509192661261067057L;
    // logger
    private static Logger log = Logger.getLogger(ClientMessagingServlet.class);

    public ClientMessagingServlet() {
        super();
    }

    /** Zpracovava pozadavky z obou HTTP metod (<code>GET</code> a <code>POST</code>)
     * @param request servletovy pozadavek
     * @param response servletova odpoved
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String forwarded = request.getHeader("x-forwarded-for");
        log.info("Client request: forwarded for " + (forwarded == null ? "<missing>" : forwarded) + " {" + request.getRemoteAddr() + "}");
        response.setCharacterEncoding("UTF-8");

        // server uzavren?
        if (SystemManager.get().isClosed()) {
            log.debug("Client request: server closed");
            printSimpleJSONResponseState(response, ResponseUtils.STATE_CLOSED);
            return;
        }

        // nacteni json requestu
        String jsonRequest = null;
        try {
            jsonRequest = readInputRequest(request);
        } catch (IOException ex) {
            log.error("reading request input stream", ex);
        }
        if (jsonRequest == null || jsonRequest.trim().length() == 0) {
            printSimpleJSONResponseState(response, ResponseUtils.STATE_FAIL);
            return;
        }
        jsonRequest = jsonRequest.trim();

        if (log.isDebugEnabled()) {
            log.debug("json string: " + jsonRequest);
        }

        try {
            JSONObject json = new JSONObject(jsonRequest);
            String type = json.getString(SAXRequestHandler.TYPE);

            Class clazz = RequestUtils.CLIENT_REQUEST_TYPES.get(type);
            if (clazz == null) {
                log.error("Can't translate type to action " + type);
                printSimpleJSONResponseState(response, ResponseUtils.STATE_FAIL);
                return;
            }
            if (log.isDebugEnabled()) {
                log.debug("Action " + clazz.getSimpleName());
            }
            DoMessageAction action = (DoMessageAction) clazz.newInstance();
            action.setParameters(json);
            action.setResponse(response);

            action.doAction();
        } catch (JSONException ex) {
            log.error("process json", ex);
            response.setStatus(response.SC_BAD_REQUEST);
        } catch (InstantiationException ex) {
            log.error("client servlet run class", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
        } catch (IllegalAccessException ex) {
            log.error("sax run class", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(resp.SC_METHOD_NOT_ALLOWED);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** odesle odpoved s informaci o uzavrenem serveru */
    private void printSimpleJSONResponseState(HttpServletResponse response, String state) {
        try {
            response.setStatus(response.SC_OK);
            response.getWriter().print(ResponseUtils.simpleJSONResponse(state));
        } catch (IOException ex) {
            log.error("print response", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
        }
        return;
    }

    /** nacte data requestu */
    private String readInputRequest(HttpServletRequest request) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        return sb.toString();
    }

    @Override
    public void destroy() {
        super.destroy();
        log.info("Destroying");
    }
}
