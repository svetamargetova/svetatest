package consys.messaging.servlet;

import consys.messaging.SystemManager;
import consys.messaging.processing.SAXRequestHandler;
import consys.messaging.utils.ResponseUtils;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Servlet pro zpracovani pozadavku od serveru
 * @author pepa
 */
public class ServerMessagingServlet extends HttpServlet {

    private static final long serialVersionUID = -5022844373519319597L;
    // logger
    private static Logger log = Logger.getLogger(ServerMessagingServlet.class);

    public ServerMessagingServlet() {
        super();
    }

    /** Zpracovava pozadavky z obou HTTP metod (<code>GET</code> a <code>POST</code>)
     * @param request servletovy pozadavek
     * @param response servletova odpoved
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String forwarded = request.getHeader("x-forwarded-for");
        log.info("Client request: forwarded for " + (forwarded == null ? "<missing>" : forwarded) + " {" + request.getRemoteAddr() + "}");
        response.setCharacterEncoding("UTF-8");

        try {
            request.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            log.error("Unsupported encoding", ex);
        }

        // TODO: overeni zda jde o povoleneho klienta (nas server, spravny rozsah ip, ...)
        if (false) {
            response.setStatus(response.SC_FORBIDDEN);
            return;
        }

        // server uzavren?
        if (SystemManager.get().isClosed()) {
            printSimpleXMLResponseClosed(response);
        }

        // nacteni xml requestu
        String xmlRequest = request.getParameter("xml");
        if (xmlRequest == null) {
            response.setStatus(response.SC_BAD_REQUEST);
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("xml string: " + xmlRequest);
        }

        try {
            // zpracovani requestu
            XMLReader parser = XMLReaderFactory.createXMLReader();
            parser.setContentHandler(new SAXRequestHandler(response));
            parser.parse(new InputSource(new StringReader(xmlRequest)));
        } catch (IOException ex) {
            log.error("process xml", ex);
            response.setStatus(response.SC_BAD_REQUEST);
        } catch (SAXException ex) {
            log.error("process xml", ex);
            response.setStatus(response.SC_BAD_REQUEST);
        }
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(resp.SC_METHOD_NOT_ALLOWED);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** odesle odpoved s informaci o uzavrenem serveru */
    private void printSimpleXMLResponseClosed(HttpServletResponse response) {
        log.debug("Server request: server CLOSED");
        try {
            response.getWriter().print(ResponseUtils.simpleXMLResponse(ResponseUtils.STATE_CLOSED));
            response.setStatus(response.SC_OK);
        } catch (IOException ex) {
            log.error("print response", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
        }
        return;
    }

    @Override
    public void destroy() {
        super.destroy();
        log.info("Destroying");
    }
}
