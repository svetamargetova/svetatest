package consys.messaging.mail;

import consys.messaging.bo.Mail;
import consys.messaging.processing.mail.abst.AbstractMailBuilder;
import consys.messaging.processing.mail.abst.Mailer;
import consys.messaging.processing.mail.send.AWSMailFactory;
import consys.messaging.processing.mail.send.CommonMailFactory;
import consys.messaging.utils.AppUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.commons.mail.EmailException;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class MailWorker implements Runnable {

    private static Logger log = Logger.getLogger(MailWorker.class);
    private static int counter = 0;
    private Queue mainQueue;
    private final int mailsPerTask;
    private final Object sharedLock;
    private Thread thread;
    //destroy shared variable
    private boolean destroy;
    private Mailer mailer;
    private MailFactory mailFactory;

    public MailWorker(Queue mainQueue, Object lock) {
        sharedLock = lock;
        this.mainQueue = mainQueue;
        String host = AppUtils.mailHost();
        String user = AppUtils.mailUser();
        String pass = AppUtils.mailPass();
        boolean ssl = AppUtils.mailSsl();
        boolean debug = AppUtils.mailDebug();
        mailsPerTask = AppUtils.schedulerMailsPerTask();
        setMailFactory(new AWSMailFactory());
        //setMailFactory(new CommonMailFactory());
        destroy = false;
        mailer = new Mailer(host, user, pass, ssl, debug);
        thread = new Thread(this, "MailWorker" + counter++);
        thread.start();
        log.info("instance created");


    }

    public void destroy() throws InterruptedException {
        log.info("destroy mail worker");
        destroy = true;
        thread.interrupt();
        thread.join();
    }

    @Override
    public void run() {
        log.info("Starting mail worker");
        // a valime do nekonecna
        while (true) {
            // cekam na notify
            try {
                synchronized (sharedLock) {
                    // test ci medzitym nekdo neco nevlozil
                    if (mainQueue.isEmpty()) {
                        log.debug("waiting for notify");
                        sharedLock.wait();
                    }
                }
            } catch (InterruptedException ex) {
                log.info("Interrupt - do save inmemory queues");
                return;
            }
            // nekdo notifel alebo vlozil takze vim ze tam su emaile
            List<Mail> mails = null;
            do {
                mails = mainQueue.first(mailsPerTask);
                if (mails != null) {
                    List<Mail> failedMails = new ArrayList<Mail>(mailsPerTask);
                    if (log.isDebugEnabled()) {
                        log.debug("Process " + mails.size() + " mails");
                    }
                    for (int i = 0; i < mails.size() && !destroy; i++) {
                        Mail mail = mails.get(i);
                        AbstractMailBuilder email = getMailFactory().mailFor(mail);
                        try {
                            email.send(mailer);
                        } catch (EmailException exx) {
                            log.error("Send failed", exx);
                            failedMails.add(mail);
                        }
                    }
                    if (destroy) {
                        failedMails.addAll(mails);
                        doBackup(failedMails);
                    }
                }
            } while (mails != null && mails.size() > 0);
        }
    }

    private void doBackup(List<Mail> failedMails) {
        for (Mail m : failedMails) {
            mainQueue.insert(m);
        }
    }

    /**
     * @return the mailFactory
     */
    public MailFactory getMailFactory() {
        return mailFactory;
    }

    /**
     * @param mailFactory the mailFactory to set
     */
    public void setMailFactory(MailFactory mailFactory) {
        this.mailFactory = mailFactory;
    }
}
