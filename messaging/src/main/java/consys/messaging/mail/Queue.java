package consys.messaging.mail;

import consys.messaging.bo.Mail;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Queue implements MailQueue {

    private static Logger log = Logger.getLogger(PersistQueue.class);
    // data
    private InMemoryQueue inMemoryQueue;
    private PersistQueue persistQueue;
    private MailQueue activeInputQueue;

    public Queue() {
        inMemoryQueue = new InMemoryQueue();
        persistQueue = new PersistQueue();
        activeInputQueue = inMemoryQueue;
    }

    public final synchronized void persist() {
        log.info("Persisting all mails in InMemmoryQueue - " + inMemoryQueue.getSize());
        for (Mail m : inMemoryQueue.first(inMemoryQueue.getSize())) {
            persistQueue.insert(m);
        }
    }

    public final synchronized boolean isEmpty() {
        return inMemoryQueue.isEmpty();
    }

    @Override
    public final synchronized List<Mail> first(int size) {
        // vytahni secko z inmemory
        List<Mail> mails = inMemoryQueue.first(size);
        // skopiruj do perzistovanej
        if (mails == null || mails.isEmpty()) {
            mails = persistQueue.first(inMemoryQueue.getMaxLength() - 1);
            log.info("Copy " + mails.size() + " to inmemory queue");
            for (Mail mail : mails) {
                inMemoryQueue.insert(mail);
            }

            // ak je nacitane menej nez max tak sa nastavi active queue inmemory            
            if (mails.size() < inMemoryQueue.getMaxLength()) {
                log.info("Active queue => Inmemory Queue");
                activeInputQueue = inMemoryQueue;
            }
            mails = inMemoryQueue.first(size);
        }
        return mails;
    }

    @Override
    public final synchronized void insert(Mail mail) {
        try {
            activeInputQueue.insert(mail);
        } catch (QueueFullException ex) {
            log.info("Active queue => Persisted");
            activeInputQueue = persistQueue;
            activeInputQueue.insert(mail);
        }
    }
}
