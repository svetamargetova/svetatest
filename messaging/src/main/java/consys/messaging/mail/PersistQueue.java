package consys.messaging.mail;

import consys.messaging.bo.Mail;
import consys.messaging.processing.mail.DeleteMail;
import consys.messaging.processing.mail.ListMail;
import consys.messaging.processing.mail.QueueMail;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PersistQueue implements MailQueue {

    private static Logger log = Logger.getLogger(PersistQueue.class);

    @Override
    public List<Mail> first(int size) {
        List<Mail> mails = new ListMail(size).run();
        new DeleteMail(mails).run();
        log.info("LOAD DB: " + mails.size());
        return mails;
    }

    @Override
    public void insert(Mail mail) throws QueueFullException {
        new QueueMail(mail).run();
    }
}
