package consys.messaging.mail;

import consys.messaging.bo.Mail;
import java.util.List;

/**
 * Rozhranie pre frontu
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface MailQueue {

    public List<Mail> first(int size);

    public void insert(Mail m);

    public class QueueFullException extends RuntimeException {

        private static final long serialVersionUID = -7624816377129415809L;
    }
}
