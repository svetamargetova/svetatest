package consys.messaging.mail;

import consys.messaging.bo.Mail;
import consys.messaging.utils.AppUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class InMemoryQueue implements MailQueue {

    private static Logger log = Logger.getLogger(InMemoryQueue.class);
    private final List<Mail> queue;
    private final int maxLength;

    public InMemoryQueue() {
        maxLength = AppUtils.inmemoryQueueSize();
        queue = Collections.synchronizedList(new ArrayList<Mail>());
    }

    public final boolean isEmpty() {
        return queue.isEmpty();
    }

    @Override
    public final List<Mail> first(int size) {        
        // synchronizacia na ziskanie velkosti a navratenie urciteho poctu
        List<Mail> out = new ArrayList<Mail>();
        try {
            for (int i = 0; i < size; i++) {
                out.add(queue.remove(0));
            }
        } catch (IndexOutOfBoundsException e) {
        }        
        return out;
    }

    @Override
    public final void insert(Mail m) throws QueueFullException {        
        int qsize = queue.size();
        if (qsize >= getMaxLength()) {
            throw new QueueFullException();
        }
        queue.add(m);
    }

    /**
     * @return the maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * @return the maxLength
     */
    public int getSize() {
        return queue.size();
    }
}
