package consys.messaging.mail;

import consys.messaging.bo.Mail;
import consys.messaging.processing.mail.abst.AbstractMailBuilder;

/**
 * Factory ktora vytvara objekt emailu urceneho k poslaniu
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface MailFactory {

    public AbstractMailBuilder mailFor(Mail m);
}
