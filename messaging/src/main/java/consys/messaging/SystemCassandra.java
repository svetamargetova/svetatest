package consys.messaging;

import consys.messaging.exception.CassandraFailException;
import consys.messaging.utils.AppUtils;
import net.dataforte.cassandra.pool.ConnectionPool;
import net.dataforte.cassandra.pool.PoolConfiguration;
import net.dataforte.cassandra.pool.PoolProperties;
import org.apache.cassandra.thrift.Cassandra;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Trida spravuje cassandru
 * @author pepa
 */
public class SystemCassandra {

    // logger
    private static Logger log = LoggerFactory.getLogger(SystemCassandra.class);
    // instance
    private static SystemCassandra instance;
    // data
    private ConnectionPool pool;

    private SystemCassandra(boolean testKeyspace) throws TException {
        log.debug("Creating Cassandra pool");

        if (testKeyspace) {
            SystemManager.get().setTest();
        }

        PoolConfiguration prop = new PoolProperties();
        prop.setKeySpace(SystemManager.get().keyspace()); // keyspace se kterym se bude pracovat
        prop.setHost(AppUtils.cassandraAddress());
        prop.setPort(AppUtils.cassandraPort());
        prop.setInitialSize(AppUtils.cassandraInitialSize()); // default 10 - kolik se na zacatku zinicializuje spojeni
        prop.setMinIdle(AppUtils.cassandraMinIdle()); // minimalni pocet pripravenach (cekajicich) spojeni
        prop.setMaxIdle(AppUtils.cassandraMaxIdle()); // default 100 - maximalni pocet pripravenych (cekajicich) spojeni
        prop.setMaxActive(AppUtils.cassandraMaxActive()); // default 100 - maximalni pocet otevrenych spojeni najednou
        pool = new ConnectionPool(prop);
    }

    /** vraci instanci Cassandra */
    public static SystemCassandra get() throws TException {
        if (instance == null) {
            instance = new SystemCassandra(false);
        }
        return instance;
    }

    /** volat pouze z testu !!! */
    public void switchToTest() throws TException {
        if (pool != null) {
            pool.close(true);
        }
        instance = new SystemCassandra(true);
    }

    /** zapujceni Cassandra.Client pro komunikaci s Cassandrou */
    public synchronized Cassandra.Client loanClient() throws CassandraFailException {
        if (log.isTraceEnabled()) {
            log.trace("Thread " + Thread.currentThread().getName() + " loan client");
        }

        Cassandra.Client client = null;
        try {
            client = pool.getConnection();
        } catch (TException ex) {
            log.error("pool get connection", ex);
        }

        if (client == null) {
            throw new CassandraFailException();
        }
        return client;
    }

    /** vraceni Cassandra.Client po pouziti */
    public synchronized void returnClient(Cassandra.Client client) {
        if (log.isTraceEnabled()) {
            log.trace("Thread " + Thread.currentThread().getName() + " return client");
        }
        pool.release(client);
    }

    /** volat pri ukonceni aplikace */
    public void destroy() {
        pool.close();
    }
}
