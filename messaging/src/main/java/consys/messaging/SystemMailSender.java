package consys.messaging;

import consys.messaging.bo.Mail;
import consys.messaging.mail.MailWorker;
import consys.messaging.mail.Queue;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Systemovy planovac spousteni
 * @author pepa
 */
public class SystemMailSender {

    // logger
    private static Logger log = LoggerFactory.getLogger(SystemMailSender.class);
    // instance
    private static SystemMailSender instance;
    // data
    private Queue mainQueue;
    private MailWorker mailWorker;
    private final Object sharedLock = new Object();

    private SystemMailSender() {
        log.info("Creating mail sender");
        mainQueue = new Queue();
        mailWorker = new MailWorker(mainQueue, sharedLock);
    }

    private void processMail(Mail mail) {
        synchronized (sharedLock) {
            mainQueue.insert(mail);
            sharedLock.notify();
        }
    }

    /** volat pri ukonceni */
    protected void destroy() throws InterruptedException {
        // destroy worker
        getMailWorker().destroy();
        // persist and destroy
        mainQueue.persist();
    }

    /** vraci instanci SystemScheduleru */
    public static SystemMailSender get() {
        if (instance == null) {
            instance = new SystemMailSender();
        }
        return instance;
    }

    /** vlozi mail do fronty k odeslani */
    public static void sendMail(Mail mail) {
        if (StringUtils.isBlank(mail.getFrom()) || StringUtils.isBlank(mail.getTo())) {
            throw new IllegalArgumentException("FROM or TO is empty:"+mail);
        }
        get().processMail(mail);
    }

    /**
     * @return the mailWorker
     */
    public MailWorker getMailWorker() {
        return mailWorker;
    }
}

