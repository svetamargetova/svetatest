package consys.messaging.exception;

/**
 * Vyjimka vyhozena pokud nema uzivatel pravo vykonat nejakou akci nebo nema pravo pristupu ke zpravam
 * @author pepa
 */
public class AccessDeniedException extends RuntimeException {

    private static final long serialVersionUID = 6715323636723046024L;
}
