package consys.messaging.exception;

/**
 * Vyjimka vyhazovana kdyz neni nalezena slozka
 * @author pepa
 */
public class FolderNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -1824662005414911427L;
}
