package consys.messaging.exception;

/**
 * Vyjimka vyhozena pri nenalezeni entity se zadanym uuid
 * @author pepa
 */
public class EntityNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -8675086703102942002L;
}
