package consys.messaging.exception;

/**
 * Vyjimka vyhazovana, kdyz nebyla nalezena pozadovana zprava
 * @author pepa
 */
public class MessageNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -2583203086790076752L;
}
