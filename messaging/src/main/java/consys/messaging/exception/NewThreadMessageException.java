package consys.messaging.exception;

/**
 * Vyjimka vyhozena, kdyz byla v mezicase pridana nova zprava do vlakna
 * @author pepa
 */
public class NewThreadMessageException extends RuntimeException {

    private static final long serialVersionUID = 1044663862429774368L;
}
