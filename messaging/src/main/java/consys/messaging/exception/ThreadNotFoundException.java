package consys.messaging.exception;

/**
 * Vyjimka vyhazovana, kdyz neni nalezeno vlakno
 * @author pepa
 */
public class ThreadNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 8443566534733855097L;
}
