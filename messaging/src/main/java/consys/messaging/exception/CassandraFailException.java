package consys.messaging.exception;

/**
 * Vyjimka vyhozena pri chybe cassandry
 * @author pepa
 */
public class CassandraFailException extends RuntimeException {

    private static final long serialVersionUID = -7244317159342949210L;
}
