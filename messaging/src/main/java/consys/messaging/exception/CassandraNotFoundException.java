package consys.messaging.exception;

/**
 * Vyjimka vyhozena v pripadu, ze nebyla nalezena cassandra
 * @author pepa
 */
public class CassandraNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 3981898535034905078L;
}
