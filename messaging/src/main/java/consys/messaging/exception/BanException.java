package consys.messaging.exception;

/**
 * Vyjimka vyhozena pokud ma entita zakaz zasilat zpravy
 * @author pepa
 */
public class BanException extends RuntimeException {

    private static final long serialVersionUID = 5494957031450080671L;
}
