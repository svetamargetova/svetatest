package consys.messaging.bo;

import consys.messaging.interfaces.ColumnRecords;
import consys.messaging.utils.CassandraUtils;
import org.apache.cassandra.thrift.ColumnPath;

/**
 * Atributy vlakna
 * @author pepa
 */
public class ThreadAttribute implements ColumnRecords {

    // konstanty - polozky
    public static final String LAST_TIMESTAMP = "last_timestamp";
    public static final String TITLE = "title";
    public static final String AUTHOR = "author";
    public static final String RECEIVERS = "receivers";
    // konstanta - column family
    public static final String CF_THREAD_ATTRIBUTES = "Thread_Attributes";
    // konstanty - column path
    public static final ColumnPath lastTimestampCP = CassandraUtils.createColumnPath(CF_THREAD_ATTRIBUTES, LAST_TIMESTAMP);
    public static final ColumnPath titleCP = CassandraUtils.createColumnPath(CF_THREAD_ATTRIBUTES, TITLE);
    public static final ColumnPath receiversCP = CassandraUtils.createColumnPath(CF_THREAD_ATTRIBUTES, RECEIVERS);
    // data
    private String uuid;
    private String title;
    private long lastTimestamp;
    private String author;
    /** uuid prijemcu oddelena carkou */
    private String receivers;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getLastTimestamp() {
        return lastTimestamp;
    }

    public void setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    public String getReceivers() {
        return receivers;
    }

    public void setReceivers(String receivers) {
        this.receivers = receivers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
