package consys.messaging.bo.assist;

/**
 * Abstrakni predek odesilatelnych zprav
 * @author pepa
 */
public abstract class Message {

    private String from;
    private String message;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
