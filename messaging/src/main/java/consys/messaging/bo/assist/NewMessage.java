package consys.messaging.bo.assist;

import java.util.HashMap;
import java.util.Map;

/**
 * Pomocny objekt pro predavani parametru zpravy
 * @author pepa
 */
public class NewMessage extends Message {

    // data
    private String subject;
    private Map<String, Boolean> to;

    public NewMessage() {
        to = new HashMap<String, Boolean>();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Map<String, Boolean> getTo() {
        return to;
    }

    public void setTo(Map<String, Boolean> to) {
        this.to = to;
    }
}
