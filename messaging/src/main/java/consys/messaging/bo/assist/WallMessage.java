package consys.messaging.bo.assist;

/**
 * Zprava na zed
 * @author pepa
 */
public class WallMessage {

    /** uuid od koho je zprava odeslana */
    private String from;
    /** uuid komu je zprava urcena */
    private String to;
    private String text;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
