package consys.messaging.bo.assist;

/**
 *
 * @author pepa
 */
public class ThreadMessage extends Message {

    // data
    private String thread;

    public String getThread() {
        return thread;
    }

    public void setThread(String thread) {
        this.thread = thread;
    }
}
