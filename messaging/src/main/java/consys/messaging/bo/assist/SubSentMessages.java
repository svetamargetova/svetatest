package consys.messaging.bo.assist;

/**
 * Podcast objektu SentMessages
 * @author pepa
 */
public class SubSentMessages {

    // data
    private String title;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
