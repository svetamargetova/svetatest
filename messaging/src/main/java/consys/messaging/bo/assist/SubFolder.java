package consys.messaging.bo.assist;

/**
 * Podcast objektu Folder
 * @author pepa
 */
public class SubFolder {

    // data
    private String title;
    private String author;
    private String thread;
    private boolean update;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThread() {
        return thread;
    }

    public void setThread(String thread) {
        this.thread = thread;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }
}
