package consys.messaging.bo.assist;

import java.util.HashMap;
import java.util.Map;

/**
 * Podcast objektu Thread
 * @author pepa
 */
public class SubThread {

    // data
    private String text;
    private String from;
    private Map<String, Boolean> uuids;

    public SubThread() {
        uuids = new HashMap<String, Boolean>();
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Map<String, Boolean> getUuids() {
        return uuids;
    }

    public void setUuids(Map<String, Boolean> uuids) {
        this.uuids = uuids;
    }
}
