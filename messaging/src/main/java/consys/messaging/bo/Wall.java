package consys.messaging.bo;

import consys.messaging.bo.assist.SubWall;
import consys.messaging.interfaces.SuperColumnRecords;
import java.util.LinkedHashMap;

/**
 * Zpravy na zdi
 * @author pepa
 */
public class Wall implements SuperColumnRecords {

    // konstanty - polozky
    public static final String FROM = "from";
    public static final String TITLE = "title";
    public static final String TEXT = "text";
    // konstanta - column family
    public static final String CF_WALLS = "Walls";
    // data
    private String uuid;
    private LinkedHashMap<Long, SubWall> timestamps;
    private long total;

    public Wall() {
        timestamps = new LinkedHashMap<Long, SubWall>();
    }

    public LinkedHashMap<Long, SubWall> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(LinkedHashMap<Long, SubWall> timestamps) {
        this.timestamps = timestamps;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
