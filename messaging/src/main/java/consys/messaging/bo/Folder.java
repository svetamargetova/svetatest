package consys.messaging.bo;

import consys.messaging.bo.assist.SubFolder;
import consys.messaging.interfaces.SuperColumnRecords;
import java.util.LinkedHashMap;

/**
 * Slozka zprav
 * @author pepa
 */
public class Folder implements SuperColumnRecords {

    // konstanty - polozky
    public static final String TITLE = "title";
    public static final String AUTHOR = "author";
    public static final String THREAD = "thread";
    public static final String UPDATE = "update";
    // konstanta - column family
    public static final String CF_FOLDERS = "Folders";
    // data
    private String uuid;
    private LinkedHashMap<String, SubFolder> timestamps;
    private long nextPageTimestamp;
    /** celkovy pocet polozek ve slozce */
    private long total;

    public Folder() {
        timestamps = new LinkedHashMap<String, SubFolder>();
        total = 0;
    }

    public long getNextPageTimestamp() {
        return nextPageTimestamp;
    }

    public void setNextPageTimestamp(long nextPageTimestamp) {
        this.nextPageTimestamp = nextPageTimestamp;
    }

    public LinkedHashMap<String, SubFolder> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(LinkedHashMap<String, SubFolder> timestamps) {
        this.timestamps = timestamps;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
