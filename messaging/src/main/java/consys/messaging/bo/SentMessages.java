package consys.messaging.bo;

import consys.messaging.bo.assist.SubSentMessages;
import java.util.LinkedHashMap;

/**
 * Odeslane zpravy
 * @author pepa
 */
public class SentMessages {

    // konstanty - polozky
    public static final String TITLE = "title";
    public static final String MESSAGE = "message";
    // data
    private String uuid;
    private LinkedHashMap<String, SubSentMessages> timestamps;
    private long nextPageTimestamp;
    /** celkovy pocet polozek ve slozce */
    private long total;

    public SentMessages() {
        timestamps = new LinkedHashMap<String, SubSentMessages>();
        total = 0;
    }

    public long getNextPageTimestamp() {
        return nextPageTimestamp;
    }

    public void setNextPageTimestamp(long nextPageTimestamp) {
        this.nextPageTimestamp = nextPageTimestamp;
    }

    public LinkedHashMap<String, SubSentMessages> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(LinkedHashMap<String, SubSentMessages> timestamps) {
        this.timestamps = timestamps;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
