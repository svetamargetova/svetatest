package consys.messaging.bo;

/**
 * Mail ulozitelny do cassandry
 * @author pepa
 */
public class Mail {

    // konstanty - polozky
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String SUBJECT = "sub";
    public static final String MESSAGE = "msg";
    public static final String HTML = "html";
    // konstanta - column family
    public static final String CF_MESSAGES_QUEUE_NR = "Messages_Queue_NR";
    // data
    private long timestamp;
    private String key;
    private String from;
    private String to;
    private String subject;
    private String message;
    private boolean html;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public boolean isHtml() {
        return html;
    }

    public void setHtml(boolean html) {
        this.html = html;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return String.format("Mail[from=%s to=%s subject=%s]", from,to,subject);
    }


}
