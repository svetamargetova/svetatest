package consys.messaging.bo;

import consys.messaging.interfaces.ColumnRecords;
import consys.messaging.utils.CassandraUtils;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ColumnPath;

/**
 * Komunikacni entita
 * @author pepa
 */
public class Entity implements ColumnRecords {

    // konstanty - polozky
    public static final String BAN = "ban";
    public static final String EMAIL = "email";
    public static final String UPDATE = "update";
    // konstanta - column family
    public static final String CF_ENTITIES = "Entities";
    public static final ColumnParent parent = new ColumnParent(CF_ENTITIES);
    // konstanty - column path
    public static final ColumnPath banCP = CassandraUtils.createColumnPath(CF_ENTITIES, BAN);
    public static final ColumnPath emailCP = CassandraUtils.createColumnPath(CF_ENTITIES, EMAIL);
    public static final ColumnPath updateCP = CassandraUtils.createColumnPath(CF_ENTITIES, UPDATE);
    // data
    private String uuid;
    private String email;
    private int update;
    private boolean ban;

    public boolean isBan() {
        return ban;
    }

    public void setBan(boolean ban) {
        this.ban = ban;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUpdate() {
        return update;
    }

    public void setUpdate(int update) {
        this.update = update;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
