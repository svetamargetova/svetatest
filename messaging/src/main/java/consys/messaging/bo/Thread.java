package consys.messaging.bo;

import consys.messaging.bo.assist.SubThread;
import consys.messaging.interfaces.SuperColumnRecords;
import java.util.LinkedHashMap;

/**
 * Vlakno zprav
 * @author pepa
 */
public class Thread implements SuperColumnRecords {

    // konstanty - polozky
    public static final String FROM = "from";
    public static final String TEXT = "text";
    // konstanta - column family
    public static final String CF_THREADS = "Threads";
    // data
    private String uuid;
    private LinkedHashMap<String, SubThread> timestamps;

    public Thread() {
        timestamps = new LinkedHashMap<String, SubThread>();
    }

    public LinkedHashMap<String, SubThread> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(LinkedHashMap<String, SubThread> timestamps) {
        this.timestamps = timestamps;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
