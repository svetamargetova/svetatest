package consys.messaging.processing.mail;

import consys.messaging.bo.Mail;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.SuperColumn;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.thrift.TException;

/**
 * Vlozi mail do fronty pro odeslani
 * @author pepa
 */
public class QueueMail extends CassandraExceptionWrapper {

    // data
    private final Mail mail;

    public QueueMail(Mail mail) {
        this.mail = mail;
    }

    @Override
    protected synchronized Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        final long timestamp = System.currentTimeMillis();

        // polozka do SCF Messages_Queue_NR - from, to, subject, message
        SuperColumn queueNRSuperColumn = new SuperColumn();
        queueNRSuperColumn.setName(ConvertUtils.toBytes(CassandraUtils.getTimeUUID().toString()));
        queueNRSuperColumn.addToColumns(CassandraUtils.createColumn(Mail.FROM, mail.getFrom(), timestamp));
        queueNRSuperColumn.addToColumns(CassandraUtils.createColumn(Mail.TO, ConvertUtils.toBytes(mail.getTo()), timestamp));
        queueNRSuperColumn.addToColumns(CassandraUtils.createColumn(Mail.SUBJECT, ConvertUtils.toBytes(mail.getSubject()), timestamp));
        queueNRSuperColumn.addToColumns(CassandraUtils.createColumn(Mail.MESSAGE, ConvertUtils.toBytes(mail.getMessage()), timestamp));
        queueNRSuperColumn.addToColumns(CassandraUtils.createColumn(Mail.HTML, ConvertUtils.toBytes(mail.isHtml()), timestamp));

        List<Mutation> queueNRColumns = CassandraUtils.wrapSCToListMutaion(queueNRSuperColumn);

        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Mail.CF_MESSAGES_QUEUE_NR, queueNRColumns);
        Map<ByteBuffer, Map<String, List<Mutation>>> queueMail = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();
        queueMail.put(ConvertUtils.encodeString(CassandraUtils.QUEUE_NR), subResult);

        client.batch_mutate(queueMail, ConsistencyLevel.ONE);
        // nevraci hodnotu
        return null;
    }
}
