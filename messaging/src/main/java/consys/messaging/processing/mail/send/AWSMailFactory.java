package consys.messaging.processing.mail.send;

import consys.messaging.bo.Mail;
import consys.messaging.mail.MailFactory;
import consys.messaging.processing.mail.abst.AbstractMailBuilder;

/**
 *
 * @author pepa
 */
public class AWSMailFactory implements MailFactory {

    @Override
    public AbstractMailBuilder mailFor(Mail m) {
        return new AWSMail(m);
    }
}
