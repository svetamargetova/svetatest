package consys.messaging.processing.mail;

import consys.messaging.bo.Mail;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.Deletion;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Smaze zadany seznam mailu (mely by byt odeslane)
 * @author pepa
 */
public class DeleteMail extends CassandraExceptionWrapper {

    // logger
    private static Logger log = Logger.getLogger(DeleteMail.class);
    // data
    private final List<Mail> mails;

    public DeleteMail(List<Mail> mails) {
        this.mails = mails;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        Map<ByteBuffer, Map<String, List<Mutation>>> deleteMail = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();

        List<Mutation> deleteColumns = new ArrayList<Mutation>();
        for (Mail mail : mails) {
            Deletion deletion = new Deletion().setTimestamp(mail.getTimestamp());
            deletion.setSuper_column(ConvertUtils.toBytes(mail.getKey()));
            deleteColumns.add(new Mutation().setDeletion(deletion));
        }
        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Mail.CF_MESSAGES_QUEUE_NR, deleteColumns);
        deleteMail.put(ConvertUtils.encodeString(CassandraUtils.QUEUE_NR), subResult);

        if (mails.size() > 0) {
            client.batch_mutate(deleteMail, ConsistencyLevel.ONE);
            log.trace("mails deleted");
        } else {
            log.trace("no mails to delete");
        }

        // nic nevraci
        return null;
    }
}
