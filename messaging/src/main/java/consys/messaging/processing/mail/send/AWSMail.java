package consys.messaging.processing.mail.send;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.amazonaws.services.simpleemail.model.SendRawEmailResult;
import consys.messaging.SystemManager;
import consys.messaging.bo.Mail;
import consys.messaging.processing.mail.abst.AbstractMailBuilder;
import consys.messaging.processing.mail.abst.Mailer;
import consys.messaging.utils.ConvertUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

/**
 *
 * @author pepa
 */
public class AWSMail extends AbstractMailBuilder {

    // logger
    private static Logger log = Logger.getLogger(AWSMail.class);
    // data
    private Mail mail;
    private AmazonSimpleEmailServiceClient client;

    public AWSMail(Mail mail) {
        super();
        this.mail = mail;

        client = new AmazonSimpleEmailServiceClient(new BasicAWSCredentials(SystemManager.get().getAwsAccessKey(), SystemManager.get().getAwsSecretKey()));
    }

    @Override
    public void send(Mailer m) throws EmailException {
        if (mail.getFrom() == null || mail.getTo() == null) {
            log.error("CommonMail not send, mail contains null data");
            throw new EmailException();
        }
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            MimeMessage msg = generateMessage();
            msg.writeTo(out);

            RawMessage rm = new RawMessage();
            rm.setData(ByteBuffer.wrap(out.toString().getBytes()));

            SendRawEmailResult result = client.sendRawEmail(new SendRawEmailRequest().withRawMessage(rm));
            log.info(mail.getTo() + " - success: " + result);
        } catch (MessagingException ex) {
            log.error(mail.getTo() + " - fail: " + "AWS send mail problem", ex);
            throw new EmailException();
        } catch (IOException ex) {
            log.error(mail.getTo() + " - fail: " + "AWS send mail problem", ex);
            throw new EmailException();
        } catch (AmazonServiceException ex) {
            log.error(mail.getTo() + " - fail: " + ex);
            throw new EmailException();
        } catch (AmazonClientException ex) {
            log.error(mail.getTo() + " - fail: " + ex);
            throw new EmailException();
        } catch (Exception ex) {
            log.error(mail.getTo() + " - fail: " + ex);
            throw new EmailException();
        }
    }

    /** vytvori MemeMessage z Mail */
    private MimeMessage generateMessage() throws MessagingException {
        Session s = Session.getInstance(new Properties(), null);
        MimeMessage msg = new MimeMessage(s);
        List<String> tos = ConvertUtils.normalize(mail.getTo());

        msg.setFrom(new InternetAddress(mail.getFrom()));
        if (tos.size() == 1) {
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(tos.get(0)));
        } else {
            Address[] address = new Address[tos.size()];
            for (int i = 0; i < tos.size(); i++) {
                address[i] = new InternetAddress(tos.get(i));
            }
            msg.setRecipients(Message.RecipientType.TO, address);
        }

        msg.setSubject(mail.getSubject());
        MimeMultipart mp = new MimeMultipart();

        if (mail.isHtml()) {
            BodyPart part = new MimeBodyPart();
            part.setContent(mail.getMessage(), "text/html; charset=utf-8");
            mp.addBodyPart(part);
        } else {
            BodyPart part = new MimeBodyPart();
            part.setText(mail.getMessage());
            mp.addBodyPart(part);
        }

        msg.setContent(mp);

        return msg;
    }

    /** pro testovací účely */
    public static void main(String[] args) {
        System.out.println("Acces key id in env: " + (System.getenv("aws_access") != null));
        System.out.println("Scret key in env: " + (System.getenv("aws_secret") != null));

        Mail mail = new Mail();
        mail.setFrom("hubr@acemcee.com");
        mail.setTo("hubr@acemcee.com");
        mail.setHtml(true);
        mail.setMessage("<html><head><title>Testovací mail</title></head><body>Ahoj <b>Pepčo</b> toto je zkušební mail, snad ti dojde</body></html>");
        mail.setSubject("Testovací mail 5");

        AWSMail aws = new AWSMail(mail);
        try {
            aws.send(null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
