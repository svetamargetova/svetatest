package consys.messaging.processing.mail.abst;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;
/**
 * email.setTLS(true); pro Gmail
 * @author Palo
 */
public class Mailer {
    private static Logger log = Logger.getLogger(Mailer.class);

    private String host;
    private String user;
    private String pass;
    private boolean ssl = false;
    private boolean tls = false;
    private boolean debug = false;

    public Mailer(String host, String user, String pass, boolean ssl,boolean tls, boolean debug) {
        this(host, user, pass, ssl,tls);
        this.debug = debug;
    }

    public Mailer(String host, String user, String pass, boolean ssl,boolean tls) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.ssl = ssl;
        this.tls = tls;
    }

    public void send(Email e) throws EmailException {
        
        e.setSSL(ssl);
        e.setTLS(tls);
        e.setHostName(host);
        e.setAuthentication(user, pass);
        e.setDebug(debug);
        e.send();
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
