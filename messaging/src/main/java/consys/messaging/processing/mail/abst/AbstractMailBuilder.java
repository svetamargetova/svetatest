package consys.messaging.processing.mail.abst;

import consys.messaging.bo.Mail;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author Palo
 */
public abstract class AbstractMailBuilder {

    protected HtmlEmail getHtmlEmail(String from, List<String> tos, String htmlMsg) throws EmailException {
        HtmlEmail e = new HtmlEmail();
        e.setCharset("UTF-8");
        e.setFrom(from);
        e.addReplyTo(from);
        e.setHtmlMsg(htmlMsg);
        for (String to : tos) {
            e.addTo(to);
        }
        return e;
    }

    protected SimpleEmail getSimpleEmail(String from, String to) throws EmailException {
        List<String> s = new ArrayList();
        s.add(to);
        return getSimpleEmail(from, s);
    }

    protected SimpleEmail getSimpleEmail(String from) throws EmailException {
        SimpleEmail e = new SimpleEmail();
        e.setCharset("UTF-8");
        e.setFrom(from);
        e.addReplyTo(from);
        return e;
    }

    protected SimpleEmail getSimpleEmail(String from, List<String> tos) throws EmailException {
        SimpleEmail e = getSimpleEmail(from);
        for (String to : tos) {
            e.addTo(to);
        }
        return e;
    }

    /** spusti odeslani mailu */
    public abstract void send(Mailer m) throws EmailException;
}
