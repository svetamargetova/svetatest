package consys.messaging.processing.mail;

import consys.messaging.bo.Mail;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 *
 * @author pepa
 */
public class ListMail extends CassandraExceptionWrapper<List<Mail>> {

    // logger
    private static Logger log = Logger.getLogger(ListMail.class);
    // data
    private final int number;

    public ListMail(int number) {
        this.number = number;
    }

    @Override
    protected List<Mail> doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        List<Mail> mails = new ArrayList<Mail>();

        ColumnParent parent = new ColumnParent(Mail.CF_MESSAGES_QUEUE_NR);

        SlicePredicate predicate = new SlicePredicate();
        predicate.setSlice_range(new SliceRange(ConvertUtils.encodeString(""), ConvertUtils.encodeString(""), false, number));

        List<ColumnOrSuperColumn> cscs = client.get_slice(ConvertUtils.encodeString(CassandraUtils.QUEUE_NR), parent, predicate, ConsistencyLevel.ONE);
        log.trace("mail data fetched");
        if (cscs == null || cscs.isEmpty()) {
            return mails;
        }

        for (ColumnOrSuperColumn csc : cscs) {
            Mail mail = new Mail();
            mail.setKey(ConvertUtils.bytesToString(csc.getSuper_column().getName()));
            for (Column col : csc.getSuper_column().getColumns()) {
                String name = ConvertUtils.bytesToString(col.getName());
                if (name.equals(Mail.FROM)) {
                    mail.setFrom(ConvertUtils.bytesToString(col.getValue()));
                    mail.setTimestamp(col.getTimestamp());
                } else if (name.equals(Mail.TO)) {
                    mail.setTo(ConvertUtils.bytesToString(col.getValue()));
                } else if (name.equals(Mail.SUBJECT)) {
                    mail.setSubject(ConvertUtils.bytesToString(col.getValue()));
                } else if (name.equals(Mail.MESSAGE)) {
                    mail.setMessage(ConvertUtils.bytesToString(col.getValue()));
                } else if (name.equals(Mail.HTML)) {
                    mail.setHtml(ConvertUtils.bytesToBoolean(col.getValue()));
                }
            }
            mails.add(mail);
        }

        return mails;
    }
}
