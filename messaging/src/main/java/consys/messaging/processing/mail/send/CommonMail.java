package consys.messaging.processing.mail.send;

import consys.messaging.bo.Mail;
import consys.messaging.processing.mail.abst.AbstractMailBuilder;
import consys.messaging.processing.mail.abst.Mailer;
import consys.messaging.utils.ConvertUtils;
import java.util.List;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

/**
 * Obecny mail (musi jit pres nej odeslat jakykoliv jiny mail)
 * @author pepa
 */
public class CommonMail extends AbstractMailBuilder {

    // logger
    private static Logger log = Logger.getLogger(CommonMail.class);
    // data
    private Mail mail;

    public CommonMail(Mail mail) {
        super();
        this.mail = mail;
    }

    @Override
    public void send(Mailer m) throws EmailException {
        if (mail.getFrom() == null || mail.getTo() == null) {
            log.error("CommonMail not send, mail contains null data");
            return;
        }
        List<String> tos = ConvertUtils.normalize(mail.getTo());

        Email e = null;
        if (mail.isHtml()) {
            e = getHtmlEmail(mail.getFrom(), tos, mail.getMessage());
        } else {
            e = getSimpleEmail(mail.getFrom(), tos);
            e.setMsg(mail.getMessage());
        }
        e.setSubject(mail.getSubject());
        m.send(e);
    }
}
