package consys.messaging.processing.action;

import consys.messaging.bo.assist.NewMessage;
import consys.messaging.exception.BanException;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.messaging.SendNewMessage;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONArray;

/**
 * Provede pridani zpravy a umisteni odkazu do do schranek uzivatelu
 * @author pepa
 */
public class SendNewMessageAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(SendNewMessageAction.class);

    /** provede spolecne akce pro xml i json a vrati stav */
    private String action(NewMessage message) {
        String state = ResponseUtils.STATE_OK;
        try {
            new SendNewMessage(message).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;
        } catch (BanException ex) {
            state = ResponseUtils.STATE_BAN;
        } catch (ThreadNotFoundException ex) {
            state = ResponseUtils.STATE_THREAD_NOT_FOUND;
        }
        return state;
    }

    @Override
    public void doXMLAction() {
        String state = "";
        try {
            state = action(createFromXMLMessage());
        } catch (IllegalArgumentException ex) {
            log.error("missing message values");
            state = ResponseUtils.STATE_FAIL;
        }
        printSimpleXMLResponse(state);
    }

    /** vytvori objekt message z parametru, pokud chybi potrebne parametry vyhazuje IllegaArgumentException */
    private NewMessage createFromXMLMessage() throws IllegalArgumentException {
        NewMessage message = new NewMessage();
        try {
            String from = (String) params.get(Tags.FROM);
            Map<String, Boolean> to = new HashMap<String, Boolean>();
            List<String> xmlTo = (List<String>) params.get(Tags.TO);
            for (String uuid : xmlTo) {
                if (!RequestUtils.validUuid(uuid)) {
                    throw new IllegalArgumentException();
                }
                to.put(uuid, Boolean.FALSE);
            }
            String subject = (String) params.get(Tags.SUBJECT);
            String msg = (String) params.get(Tags.MESSAGE);

            if (from == null || to == null || subject == null || msg == null || to.isEmpty()) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(from)) {
                throw new IllegalArgumentException();
            }

            message.setFrom(from);
            message.setTo(to);
            message.setSubject(subject);
            message.setMessage(msg);
        } catch (Throwable t) {
            throw new IllegalArgumentException();
        }
        return message;
    }

    @Override
    public void doJSONAction() {
        String state = "";
        try {
            state = action(createFromJSONMessage());
        } catch (IllegalArgumentException ex) {
            log.error("missing message values");
            state = ResponseUtils.STATE_FAIL;
        }
        printSimpleJSONResponse(state);
    }

    /** vytvori objekt message z json parametru, pokud chybi potrebne parametry vyhazuje IllegaArgumentException */
    private NewMessage createFromJSONMessage() throws IllegalArgumentException {
        NewMessage message = new NewMessage();
        try {
            String from = json.getString(Tags.FROM);
            Map<String, Boolean> to = new HashMap<String, Boolean>();
            JSONArray jsonTo = json.getJSONArray(Tags.TO);
            for (int i = 0; i < jsonTo.length(); i++) {
                String uuid = jsonTo.getString(i);
                if (!RequestUtils.validUuid(uuid)) {
                    throw new IllegalArgumentException();
                }
                to.put(uuid, Boolean.FALSE);
            }
            String subject = json.getString(Tags.SUBJECT);
            String msg = json.getString(Tags.MESSAGE);

            if (from == null || to == null || subject == null || msg == null || to.isEmpty()) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(from)) {
                throw new IllegalArgumentException();
            }

            message.setFrom(from);
            message.setTo(to);
            message.setSubject(subject);
            message.setMessage(msg);
        } catch (Throwable t) {
            throw new IllegalArgumentException();
        }
        return message;
    }
}
