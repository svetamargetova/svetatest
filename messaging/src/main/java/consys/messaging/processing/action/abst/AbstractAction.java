package consys.messaging.processing.action.abst;

import consys.messaging.interfaces.DoMessageAction;
import consys.messaging.processing.action.RegisterEntityAction;
import consys.messaging.utils.ResponseUtils;
import java.io.IOException;
import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 * Abstraktni trida pro automaticke akce se zpravami
 * @author pepa
 */
public abstract class AbstractAction implements DoMessageAction {

    // logger
    private static Logger logger = Logger.getLogger(RegisterEntityAction.class);
    // data
    protected HashMap<String, Object> params;
    protected HttpServletResponse response;
    protected JSONObject json;

    @Override
    public void doAction() {
        if (json == null) {
            doXMLAction();
        } else {
            doJSONAction();
        }
    }

    @Override
    public void setParameters(JSONObject json) {
        this.json = json;
    }

    @Override
    public void setParameters(HashMap<String, Object> params) {
        this.params = params;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    /**
     * odesle odpoved se zvolenym stavem, nastavi HTTP status na OK a vraci true<br><br>
     * pokud se nepodari zapsat odpoved, nastavi HTTP status na INTERNAL ERROR a vraci false
     */
    protected boolean printSimpleXMLResponse(String state) {
        try {
            response.getWriter().print(ResponseUtils.simpleXMLResponse(state));
            response.setStatus(response.SC_OK);
            return true;
        } catch (IOException ex) {
            logger.error("print xml response", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            return false;
        }
    }

    /**
     * odesle odpoved se zvolenym stavem, nastavi HTTP status na OK a vraci true<br><br>
     * pokud se nepodari zapsat odpoved, nastavi HTTP status na INTERNAL ERROR a vraci false
     */
    protected boolean printSimpleJSONResponse(String state) {
        try {
            response.getWriter().print(ResponseUtils.simpleJSONResponse(state));
            response.setStatus(response.SC_OK);
            return true;
        } catch (IOException ex) {
            logger.error("print json response", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            return false;
        }
    }

    /** provede akci z XML pozadavku */
    protected abstract void doXMLAction();

    /** provede akci z JSON pozadavku */
    protected abstract void doJSONAction();
}
