package consys.messaging.processing.action;

import consys.messaging.bo.Thread;
import consys.messaging.bo.ThreadAttribute;
import consys.messaging.bo.assist.SubThread;
import consys.messaging.exception.AccessDeniedException;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.messaging.LoadThread;
import consys.messaging.processing.messaging.LoadThreadAttributes;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import java.io.IOException;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Provede nacteni vlakna
 * @author pepa
 */
public class LoadThreadAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(LoadThreadAction.class);

    @Override
    protected void doXMLAction() {
        // TODO: implementovat xml zpracovani pro LoadThreadAction
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void doJSONAction() {
        String state = ResponseUtils.STATE_OK;
        Thread thread = null;
        ThreadAttribute ta = null;
        String entityUuid = null;

        try {
            String threadUuid = json.getString(Tags.THREAD);
            entityUuid = json.getString(Tags.ENTITY);

            if (threadUuid == null || entityUuid == null) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(threadUuid) || !RequestUtils.validUuid(entityUuid)) {
                throw new IllegalArgumentException();
            }

            thread = new LoadThread(threadUuid, entityUuid, false).run();
            ta = new LoadThreadAttributes(threadUuid).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (ThreadNotFoundException ex) {
            state = ResponseUtils.STATE_THREAD_NOT_FOUND;
        } catch (AccessDeniedException ex) {
            state = ResponseUtils.STATE_ACCESS_DENIED;
        } catch (JSONException ex) {
            log.error("load json value", ex);
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing or bad values");
            state = ResponseUtils.STATE_FAIL;
        }

        printLoadThreadJSONResponse(thread, ta, entityUuid, state);
    }

    /**
     * odesle odpoved se zvolenym stavem a daty, nastavi HTTP status na OK a vraci true<br><br>
     * pokud se nepodari zapsat odpoved, nastavi HTTP status na INTERNAL ERROR a vraci false
     */
    private boolean printLoadThreadJSONResponse(Thread thread, ThreadAttribute ta, String eUuid, final String state) {
        if (state.equals(ResponseUtils.STATE_OK)) {
            try {
                JSONObject jr = new JSONObject();
                jr.put(Tags.RESPONSE_STATE, state);

                JSONObject data = new JSONObject();
                data.put(Tags.NAME, ta.getTitle());
                JSONArray msgs = new JSONArray();
                for (Entry<String, SubThread> m : thread.getTimestamps().entrySet()) {
                    JSONObject msg = new JSONObject();
                    msg.put(Tags.TIME, Long.parseLong(m.getKey()));
                    SubThread st = m.getValue();
                    msg.put(Tags.FROM, st.getFrom());
                    Boolean readed = st.getUuids().get(eUuid);
                    msg.put(Tags.READED, readed == null || readed == true);
                    msg.put(Tags.MESSAGE, st.getText());
                    msgs.put(msg);
                }
                data.put(Tags.MESSAGES, msgs);
                jr.put(Tags.RESPONSE_DATA, data);

                response.getWriter().print(jr.toString());
                response.setStatus(response.SC_OK);
                return true;
            } catch (JSONException ex) {
                log.error("create json response", ex);
                response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            } catch (IOException ex) {
                log.error("print json response", ex);
                response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            }
            return false;
        } else {
            return printSimpleJSONResponse(state);
        }
    }
}
