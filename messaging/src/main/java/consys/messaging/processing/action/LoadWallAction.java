package consys.messaging.processing.action;

import consys.messaging.bo.Wall;
import consys.messaging.bo.assist.SubWall;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.wall.LoadWall;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import java.io.IOException;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Akce pro nacteni zprav na zdi
 * @author pepa
 */
public class LoadWallAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(LoadWallAction.class);

    @Override
    protected void doXMLAction() {
        // TODO: implementovat xml zpracovani pro LoadWallAction
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void doJSONAction() {
        String state = ResponseUtils.STATE_OK;
        Wall wall = null;

        try {
            String eUuid = json.getString(Tags.ENTITY);
            int number = json.getInt(Tags.NUMBER);
            long offset = json.getLong(Tags.OFFSET);

            if (eUuid == null || !RequestUtils.validUuid(eUuid)) {
                throw new IllegalArgumentException();
            }
            if (number < 0 || offset < 0) {
                throw new IllegalArgumentException();
            }

            wall = new LoadWall(eUuid, number, offset).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_FOLDER_NOT_FOUND;
        } catch (JSONException ex) {
            log.error("load json value", ex);
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing or bad values");
            state = ResponseUtils.STATE_FAIL;
        }

        printLoadWallJSONResponse(wall, state);
    }

    /**
     * odesle odpoved se zvolenym stavem a daty, nastavi HTTP status na OK a vraci true<br><br>
     * pokud se nepodari zapsat odpoved, nastavi HTTP status na INTERNAL ERROR a vraci false
     */
    private boolean printLoadWallJSONResponse(Wall wall, final String state) {
        if (state.equals(ResponseUtils.STATE_OK)) {
            try {
                JSONObject jr = new JSONObject();
                jr.put(Tags.RESPONSE_STATE, state);

                JSONObject data = new JSONObject();
                data.put(Tags.TOTAL, wall.getTotal());
                JSONArray itms = new JSONArray();
                for (Entry<Long, SubWall> i : wall.getTimestamps().entrySet()) {
                    JSONObject itm = new JSONObject();
                    itm.put(Tags.TIME, i.getKey());
                    SubWall sw = i.getValue();
                    itm.put(Tags.FROM, sw.getFrom());
                    itm.put(Tags.MESSAGE, sw.getText());
                    itms.put(itm);
                }
                data.put(Tags.ITEMS, itms);
                jr.put(Tags.RESPONSE_DATA, data);

                response.getWriter().print(jr.toString());
                response.setStatus(response.SC_OK);
                return true;
            } catch (JSONException ex) {
                log.error("create json response", ex);
                response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            } catch (IOException ex) {
                log.error("print json response", ex);
                response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            }
            return false;
        } else {
            return printSimpleJSONResponse(state);
        }
    }
}
