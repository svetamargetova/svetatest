package consys.messaging.processing.action.mail;

import consys.messaging.SystemMailSender;
import consys.messaging.bo.Mail;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;

/**
 * Akce pro zaslani systemoveho mailu entite
 * @author pepa
 */
public class SystemMailAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(SystemMailAction.class);

    @Override
    protected void doXMLAction() {
        String state = ResponseUtils.STATE_OK;
        try {
            String from = (String) params.get(Tags.FROM);
            String mail = (String) params.get(Tags.MAIL);
            String subject = (String) params.get(Tags.SUBJECT);
            String msg = (String) params.get(Tags.MESSAGE);
            boolean isHTML = Boolean.parseBoolean((String) params.get(Tags.HTML));

            if (from == null || mail == null || subject == null || msg == null) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validEmail(from) || !RequestUtils.validEmail(mail) || subject.isEmpty() || msg.isEmpty()) {
                throw new IllegalArgumentException();
            }

            Mail m = new Mail();
            m.setFrom(from);
            m.setHtml(isHTML);
            m.setMessage(msg);
            m.setSubject(subject);
            m.setTo(mail);
            SystemMailSender.sendMail(m);
            
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;        
        } catch (IllegalArgumentException ex) {
            log.error("missing entity values");
            state = ResponseUtils.STATE_FAIL;
        }

        printSimpleXMLResponse(state);
    }

    @Override
    protected void doJSONAction() {
        throw new UnsupportedOperationException("Not supported from client.");
    }
}
