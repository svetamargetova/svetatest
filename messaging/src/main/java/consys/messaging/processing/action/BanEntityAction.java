package consys.messaging.processing.action;

import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.admin.BanEntity;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;

/**
 * Provede zabanovani/odbanovani entity
 * @author pepa
 */
public class BanEntityAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(BanEntityAction.class);

    /**
     * Request:<pre>
     * &lt;rq&gt;
     *     &lt;t&gt;ban&lt;/t&gt; &lt;!-- typ --&gt;
     *     &lt;e&gt;&lt;/e&gt; &lt;!-- uuid entity --&gt;
     *     &lt;ban&gt;&lt;/ban&gt; &lt;!-- hodnota 0 nebo 1 --&gt;
     * &lt;/rq&gt;</pre>
     *
     * Response:<pre>
     * &lt;rs&gt;
     *     &lt;s&gt;OK/FAIL/ENF/CLOSED&lt;/s&gt; &lt;!-- stav --&gt;
     * &lt;/rs&gt;</pre>
     */
    @Override
    public void doXMLAction() {
        String state = ResponseUtils.STATE_OK;
        try {
            String uuid = (String) params.get(Tags.ENTITY);
            String ban = (String) params.get(Tags.BAN);

            if (uuid == null || ban == null) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(uuid) || !RequestUtils.validBoolean(ban)) {
                throw new IllegalArgumentException();
            }

            new BanEntity(uuid, ban.equals("1")).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;
        } catch (IllegalArgumentException ex) {
            log.error("missing entity values");
            state = ResponseUtils.STATE_FAIL;
        }

        printSimpleXMLResponse(state);
    }

    @Override
    protected void doJSONAction() {
        throw new UnsupportedOperationException("Not supported from client.");
    }
}
