package consys.messaging.processing.action;

import consys.messaging.bo.assist.WallMessage;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.wall.SendWallMessage;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;
import org.json.JSONException;

/**
 * Akce pro odeslani zpravy na zed
 * @author pepa
 */
public class SendWallMessageAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(SendWallMessageAction.class);

    @Override
    protected void doXMLAction() {
        // TODO: implementovat xml zpracovani pro SendWallMessageAction
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void doJSONAction() {
        String state = ResponseUtils.STATE_OK;
        try {
            WallMessage msg = new WallMessage();
            msg.setFrom(json.getString(Tags.ENTITY));
            msg.setTo(json.getString(Tags.TO));
            msg.setText(json.getString(Tags.MESSAGE));

            if (msg.getFrom() == null || msg.getTo() == null || msg.getText() == null) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(msg.getFrom()) || !RequestUtils.validUuid(msg.getTo())) {
                throw new IllegalArgumentException();
            }
            if (msg.getText().isEmpty()) {
                throw new IllegalArgumentException();
            }

            new SendWallMessage(msg).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;
        } catch (JSONException ex) {
            log.error("load json value", ex);
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing message values");
            state = ResponseUtils.STATE_FAIL;
        }
        printSimpleJSONResponse(state);
    }
}
