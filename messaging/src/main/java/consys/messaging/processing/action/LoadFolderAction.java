package consys.messaging.processing.action;

import consys.messaging.bo.Folder;
import consys.messaging.bo.assist.SubFolder;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.FolderNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.messaging.LoadFolder;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import java.io.IOException;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author pepa
 */
public class LoadFolderAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(LoadFolderAction.class);

    @Override
    protected void doXMLAction() {
        // TODO: implementovat xml zpracovani pro LoadFolderAction
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void doJSONAction() {
        String state = ResponseUtils.STATE_OK;
        Folder folder = null;

        try {
            String eUuid = json.getString(Tags.ENTITY);
            int number = json.getInt(Tags.NUMBER);
            long offset = json.getLong(Tags.OFFSET);

            if (eUuid == null || !RequestUtils.validUuid(eUuid)) {
                throw new IllegalArgumentException();
            }
            if (number < 0 || offset < 0) {
                throw new IllegalArgumentException();
            }

            folder = new LoadFolder(eUuid, CassandraUtils.SYSTEM_FOLDER_IN, number, offset).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (FolderNotFoundException ex) {
            state = ResponseUtils.STATE_FOLDER_NOT_FOUND;
        } catch (JSONException ex) {
            log.error("load json value", ex);
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing or bad values");
            state = ResponseUtils.STATE_FAIL;
        }

        printLoadFolderJSONResponse(folder, state);
    }

    /**
     * odesle odpoved se zvolenym stavem a daty, nastavi HTTP status na OK a vraci true<br><br>
     * pokud se nepodari zapsat odpoved, nastavi HTTP status na INTERNAL ERROR a vraci false
     */
    private boolean printLoadFolderJSONResponse(Folder folder, final String state) {
        if (state.equals(ResponseUtils.STATE_OK)) {
            try {
                JSONObject jr = new JSONObject();
                jr.put(Tags.RESPONSE_STATE, state);

                JSONObject data = new JSONObject();
                data.put(Tags.TOTAL, folder.getTotal());
                data.put(Tags.NEXT, folder.getNextPageTimestamp());
                JSONArray itms = new JSONArray();
                for (Entry<String, SubFolder> i : folder.getTimestamps().entrySet()) {
                    JSONObject itm = new JSONObject();
                    itm.put(Tags.TIME, Long.parseLong(i.getKey()));
                    SubFolder sf = i.getValue();
                    itm.put(Tags.NAME, sf.getTitle());
                    itm.put(Tags.THREAD, sf.getThread());
                    itm.put(Tags.UPDATED, sf.isUpdate());
                    itm.put(Tags.AUTHOR, sf.getAuthor());
                    itms.put(itm);
                }
                data.put(Tags.ITEMS, itms);
                jr.put(Tags.RESPONSE_DATA, data);

                response.getWriter().print(jr.toString());
                response.setStatus(response.SC_OK);
                return true;
            } catch (JSONException ex) {
                log.error("create json response", ex);
                response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            } catch (IOException ex) {
                log.error("print json response", ex);
                response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
            }
            return false;
        } else {
            return printSimpleJSONResponse(state);
        }
    }
}
