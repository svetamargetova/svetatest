package consys.messaging.processing.action;

import consys.messaging.bo.Entity;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.admin.RegisterEntity;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;

/**
 * Provede zaregistrovani entity
 * @author pepa
 */
public class RegisterEntityAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(RegisterEntityAction.class);

    @Override
    protected void doXMLAction() {
        String state = ResponseUtils.STATE_OK;
        try {
            Entity ent = createEntity();
            new RegisterEntity(ent).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing entity values", ex);
            state = ResponseUtils.STATE_FAIL;
        }

        printSimpleXMLResponse(state);
    }

    /** vytvori objekt entity z parametru, pokud chybi potrebne parametry vyhazuje IllegaArgumentException */
    private Entity createEntity() throws IllegalArgumentException {
        Entity ent = new Entity();
        try {
            String entity = (String) params.get(Tags.ENTITY);
            String mail = (String) params.get(Tags.MAIL);
            if (log.isDebugEnabled()) {
                log.debug("Register Entity[" + entity + "," + mail + "]");
            }
            if (entity == null || mail == null) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(entity) || !RequestUtils.validEmail(mail)) {
                throw new IllegalArgumentException();
            }

            ent.setUuid(entity);
            ent.setEmail(mail);
            ent.setUpdate(0);
            ent.setBan(false);
        } catch (Throwable t) {
            throw new IllegalArgumentException();
        }
        return ent;
    }

    @Override
    protected void doJSONAction() {
        throw new UnsupportedOperationException("Not supported from client.");
    }
}
