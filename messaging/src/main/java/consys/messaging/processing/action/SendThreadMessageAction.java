package consys.messaging.processing.action;

import consys.messaging.bo.assist.ThreadMessage;
import consys.messaging.exception.BanException;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.messaging.SendThreadMessage;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;

/**
 * Provede pridani zpravy a umisteni odkazu do do schranek uzivatelu
 * @author pepa
 */
public class SendThreadMessageAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(SendThreadMessageAction.class);

    /** provede spolecne akce pro xml i json a vrati stav */
    private String action(ThreadMessage message) {
        String state = ResponseUtils.STATE_OK;
        try {
            new SendThreadMessage(message).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;
        } catch (BanException ex) {
            state = ResponseUtils.STATE_BAN;
        } catch (ThreadNotFoundException ex) {
            state = ResponseUtils.STATE_THREAD_NOT_FOUND;
        } catch (IllegalArgumentException ex) {
            log.error("missing message values");
            state = ResponseUtils.STATE_FAIL;
        }
        return state;
    }

    @Override
    public void doXMLAction() {
        printSimpleXMLResponse(action(createFromXMLMessage()));
    }

    /** vytvori objekt message z parametru, pokud chybi potrebne parametry vyhazuje IllegaArgumentException */
    private ThreadMessage createFromXMLMessage() throws IllegalArgumentException {
        ThreadMessage message = new ThreadMessage();
        try {
            String from = (String) params.get(Tags.FROM);
            String msg = (String) params.get(Tags.MESSAGE);
            String thread = (String) params.get(Tags.THREAD);

            if (from == null || msg == null || thread == null) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(from)) {
                throw new IllegalArgumentException();
            }
            if (!thread.isEmpty() && !RequestUtils.validUuid(thread)) {
                throw new IllegalArgumentException();
            }

            message.setFrom(from);
            message.setMessage(msg);
            message.setThread(thread);
        } catch (Throwable t) {
            throw new IllegalArgumentException();
        }
        return message;
    }

    @Override
    public void doJSONAction() {
        printSimpleJSONResponse(action(createFromJSONMessage()));
    }

    /** vytvori objekt message z json parametru, pokud chybi potrebne parametry vyhazuje IllegaArgumentException */
    private ThreadMessage createFromJSONMessage() throws IllegalArgumentException {
        ThreadMessage message = new ThreadMessage();
        try {
            String from = json.getString(Tags.FROM);
            String msg = json.getString(Tags.MESSAGE);
            String thread = json.getString(Tags.THREAD);

            if (from == null || msg == null || thread == null) {
                throw new IllegalArgumentException();
            }
            if (!RequestUtils.validUuid(from)) {
                throw new IllegalArgumentException();
            }
            if (!thread.isEmpty() && !RequestUtils.validUuid(thread)) {
                throw new IllegalArgumentException();
            }

            message.setFrom(from);
            message.setMessage(msg);
            message.setThread(thread);
        } catch (Throwable t) {
            throw new IllegalArgumentException();
        }
        return message;
    }
}
