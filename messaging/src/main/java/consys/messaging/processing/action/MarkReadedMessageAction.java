package consys.messaging.processing.action;

import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.messaging.MarkReadedMessage;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;
import org.json.JSONException;

/**
 * Provede oznaceni zpravy za prectenou
 * @author pepa
 */
public class MarkReadedMessageAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(MarkReadedMessageAction.class);

    @Override
    protected void doXMLAction() {
        // TODO: implementovat xml zpracovani pro LoadThreadAction
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void doJSONAction() {
        String state = ResponseUtils.STATE_OK;

        try {
            String eUuid = json.getString(Tags.ENTITY);
            String tUuid = json.getString(Tags.THREAD);
            long timestamp = json.getLong(Tags.TIME);

            if (eUuid == null || tUuid == null || !RequestUtils.validUuid(eUuid) || !RequestUtils.validUuid(tUuid)) {
                throw new IllegalArgumentException();
            }

            new MarkReadedMessage(eUuid, tUuid, timestamp).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;
        } catch (ThreadNotFoundException ex) {
            state = ResponseUtils.STATE_THREAD_NOT_FOUND;
        } catch (JSONException ex) {
            log.error("load json value", ex);
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing or bad values");
            state = ResponseUtils.STATE_FAIL;
        }

        printSimpleJSONResponse(state);
    }
}
