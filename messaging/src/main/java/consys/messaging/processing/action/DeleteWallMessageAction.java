package consys.messaging.processing.action;

import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.MessageNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.wall.DeleteWallMessage;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;
import org.json.JSONException;

/**
 * Akce pro smazani zpravy ze zdi
 * @author pepa
 */
public class DeleteWallMessageAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(DeleteWallMessageAction.class);

    @Override
    protected void doXMLAction() {
        // TODO: implementovat xml zpracovani pro DeleteWallMessageAction
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void doJSONAction() {
        String state = ResponseUtils.STATE_OK;

        try {
            String eUuid = json.getString(Tags.ENTITY);
            long timestamp = json.getLong(Tags.TIME);

            if (eUuid == null || !RequestUtils.validUuid(eUuid)) {
                throw new IllegalArgumentException();
            }

            new DeleteWallMessage(eUuid, timestamp).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;
        } catch (MessageNotFoundException ex) {
            state = ResponseUtils.STATE_MESSAGE_NOT_FOUND;
        } catch (JSONException ex) {
            log.error("load json value", ex);
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing or bad values");
            state = ResponseUtils.STATE_FAIL;
        }

        printSimpleJSONResponse(state);
    }
}
