package consys.messaging.processing.action;

import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.NewThreadMessageException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.action.abst.AbstractAction;
import consys.messaging.processing.messaging.MarkReadedThread;
import consys.messaging.utils.RequestUtils;
import consys.messaging.utils.ResponseUtils;
import consys.messaging.utils.Tags;
import org.apache.log4j.Logger;
import org.json.JSONException;

/**
 * Provede oznaceni vlakna za prectene
 * @author pepa
 */
public class MarkReadedThreadAction extends AbstractAction {

    // logger
    private static Logger log = Logger.getLogger(MarkReadedThreadAction.class);

    @Override
    protected void doXMLAction() {
        // TODO: implementovat xml zpracovani pro LoadThreadAction
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void doJSONAction() {
        String state = ResponseUtils.STATE_OK;

        try {
            String eUuid = json.getString(Tags.ENTITY);
            String tUuid = json.getString(Tags.THREAD);
            long timestamp = json.getLong(Tags.TIME);

            if (eUuid == null || tUuid == null || !RequestUtils.validUuid(eUuid) || !RequestUtils.validUuid(tUuid)) {
                throw new IllegalArgumentException();
            }

            new MarkReadedThread(eUuid, tUuid, timestamp).run();
        } catch (CassandraFailException ex) {
            state = ResponseUtils.STATE_FAIL;
        } catch (EntityNotFoundException ex) {
            state = ResponseUtils.STATE_ENTITY_NOT_FOUND;
        } catch (ThreadNotFoundException ex) {
            state = ResponseUtils.STATE_THREAD_NOT_FOUND;
        } catch (NewThreadMessageException ex) {
            state = ResponseUtils.STATE_NEW_MESSAGE;
        } catch (JSONException ex) {
            log.error("load json value", ex);
            state = ResponseUtils.STATE_FAIL;
        } catch (IllegalArgumentException ex) {
            log.error("missing or bad values");
            state = ResponseUtils.STATE_FAIL;
        }

        printSimpleJSONResponse(state);
    }
}
