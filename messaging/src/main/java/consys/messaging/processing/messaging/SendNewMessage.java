package consys.messaging.processing.messaging;

import consys.messaging.bo.Entity;
import consys.messaging.bo.ThreadAttribute;
import consys.messaging.bo.assist.NewMessage;
import consys.messaging.exception.BanException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.processing.messaging.common.CommonSend;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.NotFoundException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Odeslani nove zpravy
 * @throws BanException uzivatel je zabanovany
 * @throws EntityNotFoundException entita nebyla nalezena
 * @throws ThreadNotFoundException vlakno nebylo nalezeno
 * @author pepa
 */
public class SendNewMessage extends CassandraExceptionWrapper {

    // logger
    private static Logger log = Logger.getLogger(SendNewMessage.class);
    // data
    private NewMessage message;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param message je zprava urcena k odeslani
     */
    public SendNewMessage(final NewMessage message) {
        super();
        this.message = message;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        String checkUuid = null;
        try {
            // overeni odesilatele
            checkUuid = message.getFrom();
            CommonSend.checkSender(client, message);
            // overeni prijemcu
            for (String uuid : message.getTo().keySet()) {
                checkUuid = uuid;
                client.get(ConvertUtils.encodeString(uuid), Entity.banCP, ConsistencyLevel.ONE);
            }
            log.debug("to entities checked");
        } catch (NotFoundException ex) {
            log.error("entity not found: " + checkUuid);
            throw new EntityNotFoundException();
        }

        final long timestamp = System.currentTimeMillis();

        // ulozeni zpravy
        List<String> receivers = new ArrayList<String>(message.getTo().keySet());
        if (!receivers.contains(message.getFrom())) {
            receivers.add(message.getFrom());
        }
        Map<ByteBuffer, Map<String, List<Mutation>>> newMessage = createNewMessage(timestamp, message, receivers);
        ByteBuffer threadUuid = (ByteBuffer) newMessage.keySet().toArray()[0];
        client.batch_mutate(newMessage, ConsistencyLevel.ONE);
        log.debug("new message saved");

        // pridani novych udaju do slozek
        Map<ByteBuffer, Map<String, List<Mutation>>> folderMessage = CommonSend.addToFolder(
                timestamp, ConvertUtils.decodeString(threadUuid), message.getFrom(), receivers,
                message.getSubject(), message.getMessage(), message.getFrom());
        client.batch_mutate(folderMessage, ConsistencyLevel.ONE);
        log.debug("messages write to folders");

        // aktualizace poctu zprav
        ActualizeEntityUpdate.get().run(new ArrayList<String>(message.getTo().keySet()), true);

        // nic nevraci
        return null;
    }

    /** vytvori mapu Columnu a Supercolumnu pro davkovy zapis nove zpravy */
    private Map<ByteBuffer, Map<String, List<Mutation>>> createNewMessage(long timestamp, NewMessage message, List<String> receivers) {
        // polozka do SCF Threads
        List<Mutation> threadColumns = CassandraUtils.wrapSCToListMutaion(CommonSend.threadSuperColumn(message, receivers, timestamp));

        // polozka do CF Thread_Attributes - nazev vlakna, posledni zmena a prijemci
        ColumnOrSuperColumn taTextCSC = new ColumnOrSuperColumn();
        taTextCSC.setColumn(CassandraUtils.createColumn(ThreadAttribute.TITLE, message.getSubject(), timestamp));
        ColumnOrSuperColumn taLastCSC = new ColumnOrSuperColumn();
        taLastCSC.setColumn(CassandraUtils.createColumn(ThreadAttribute.LAST_TIMESTAMP, ConvertUtils.toBytes(timestamp), timestamp));
        ColumnOrSuperColumn taAuthorCSC = new ColumnOrSuperColumn();
        taAuthorCSC.setColumn(CassandraUtils.createColumn(ThreadAttribute.AUTHOR, ConvertUtils.toBytes(message.getFrom()), timestamp));
        ColumnOrSuperColumn taReceiversCSC = new ColumnOrSuperColumn();
        taReceiversCSC.setColumn(CassandraUtils.createColumn(ThreadAttribute.RECEIVERS,
                ConvertUtils.toBytes(ConvertUtils.denormalize(receivers)), timestamp));
        List<Mutation> threadAttributesColumns = new ArrayList<Mutation>();
        threadAttributesColumns.add(new Mutation().setColumn_or_supercolumn(taTextCSC));
        threadAttributesColumns.add(new Mutation().setColumn_or_supercolumn(taLastCSC));
        threadAttributesColumns.add(new Mutation().setColumn_or_supercolumn(taAuthorCSC));
        threadAttributesColumns.add(new Mutation().setColumn_or_supercolumn(taReceiversCSC));

        UUID threadUuid = UUID.randomUUID();
        if (log.isDebugEnabled()) {
            log.debug("createNewMessage: uuid = " + threadUuid.toString());
        }

        return CommonSend.createMessageResult(threadUuid.toString(), threadColumns, threadAttributesColumns);
    }
}
