package consys.messaging.processing.messaging;

import consys.messaging.bo.Thread;
import consys.messaging.bo.assist.SubThread;
import consys.messaging.exception.AccessDeniedException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.List;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Nacte vlakno
 * @throws ThreadNotFoundException vlakno nebylo nalezeno
 * @throws AccessDeniedException pristup odmitnut
 * @author pepa
 */
public class LoadThread extends CassandraExceptionWrapper<Thread> {

    // logger
    private static Logger log = Logger.getLogger(LoadThread.class);
    // data
    private final String originalUuid;
    private final ByteBuffer uuid;
    private final String eUuid;
    private final boolean reversed;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param uuid je uuid vlakna
     * @param eUuid je uuid zadajici entity
     * @param reversed true pokud se ma nacitat od nejnovejsiho
     */
    public LoadThread(final String uuid, final String eUuid, final boolean reversed) {
        super();
        originalUuid = uuid;
        this.uuid = ConvertUtils.encodeString(uuid);
        this.eUuid = eUuid;
        this.reversed = reversed;
    }

    @Override
    protected Thread doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        Thread thread = new Thread();
        thread.setUuid(originalUuid);

        // nacteni poctu zprav ve vlakne
        int itemsCount = client.get_count(uuid, new ColumnParent(Thread.CF_THREADS),
                CassandraUtils.getLongSlicePredicate(), ConsistencyLevel.ONE);
        if (log.isDebugEnabled()) {
            log.debug("thread items: " + itemsCount);
        }

        // nacteni vsech zprav ve vlakne
        SlicePredicate predicate = new SlicePredicate();
        predicate.setSlice_range(new SliceRange(ConvertUtils.encodeString(""), ConvertUtils.encodeString(""), reversed, itemsCount));

        List<ColumnOrSuperColumn> cscs = client.get_slice(uuid, new ColumnParent(Thread.CF_THREADS), predicate, ConsistencyLevel.ONE);
        log.debug("thread data fetched");
        if (cscs == null || cscs.isEmpty()) {
            throw new ThreadNotFoundException();
        }

        // nasetovani dat do objektu vlakna a overeni uzivatele
        boolean checked = false;
        for (ColumnOrSuperColumn csc : cscs) {
            long key = ConvertUtils.bytesToLong(csc.getSuper_column().getName());
            SubThread subThread = new SubThread();
            for (Column col : csc.getSuper_column().getColumns()) {
                String name = ConvertUtils.bytesToString(col.getName());
                if (name.equals(Thread.FROM)) {
                    subThread.setFrom(ConvertUtils.bytesToString(col.getValue()));
                } else if (name.equals(Thread.TEXT)) {
                    subThread.setText(ConvertUtils.bytesToString(col.getValue()));
                } else {
                    subThread.getUuids().put(name, ConvertUtils.bytesToBoolean(col.getValue()));
                }
            }
            thread.getTimestamps().put(String.valueOf(key), subThread);

            // overeni ze je uzivatel jako prijemce vlakna a muze ho cist
            if (!checked) {
                if (!subThread.getUuids().keySet().contains(eUuid) && !subThread.getFrom().equals(eUuid)) {
                    // zadatel neni v seznamu prijemcu (ani neni odesilatel) -> nema pravo prohlizet vlakno
                    throw new AccessDeniedException();
                }
                checked = true;
            }
        }
        log.debug("new thread setted to object");

        return thread;
    }
}
