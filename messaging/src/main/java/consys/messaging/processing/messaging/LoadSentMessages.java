package consys.messaging.processing.messaging;

import consys.messaging.bo.Folder;
import consys.messaging.bo.SentMessages;
import consys.messaging.bo.assist.SubSentMessages;
import consys.messaging.exception.FolderNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.List;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Nacte odeslane zpravy entity
 * @throws FolderNotFoundException slozka nebyla nalezena
 * @author pepa
 */
public class LoadSentMessages extends CassandraExceptionWrapper<SentMessages> {

    // logger
    private static Logger log = Logger.getLogger(LoadSentMessages.class);
    // data
    private final String eUuid;
    private final String folderName;
    private final int number;
    private final long offset;

    /**
     * kombinace number 0 a offset 0 se nactou vsechny zaznamy
     * @param eUuid je uuid entity
     * @param folderName systemovy nazev slozky (in, out, ...)
     * @param number kolik se ma nacist zaznamu
     * @param offset od jakeho zaznamu se ma nacitat (timestamp)
     */
    public LoadSentMessages(final String eUuid, final String folderName, final int number, final long offset) {
        super();
        this.eUuid = eUuid;
        this.folderName = folderName;
        this.number = number;
        this.offset = offset;
    }

    @Override
    protected SentMessages doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        final String folderUuid = ConvertUtils.folderUuid(eUuid, folderName);
        final ByteBuffer uuid = ConvertUtils.encodeString(folderUuid);
        SentMessages sm = new SentMessages();
        sm.setUuid(folderUuid);

        // nacteni poctu zprav ve slozce
        int itemsCount = client.get_count(uuid, new ColumnParent(Folder.CF_FOLDERS),
                CassandraUtils.getLongSlicePredicate(), ConsistencyLevel.ONE);
        if (log.isDebugEnabled()) {
            log.debug("sent folder items: " + itemsCount);
        }

        if (itemsCount == 0) {
            return sm;
        }
        sm.setTotal(itemsCount);

        // nacteni zprav ve slozce
        SlicePredicate predicate = new SlicePredicate();
        if (number == 0 && offset == 0) {
            // bude se nacitat vsechno
            predicate.setSlice_range(new SliceRange(ConvertUtils.encodeString(""), ConvertUtils.encodeString(""), true, itemsCount));
        } else {
            // bude se nacitat jen interval
            long from = offset == 0 ? System.currentTimeMillis() : offset;
            predicate.setSlice_range(new SliceRange(ConvertUtils.encodeLong(from),
                    ConvertUtils.encodeLong(0l), true, number + 1));
        }

        List<ColumnOrSuperColumn> cscs = client.get_slice(uuid, new ColumnParent(Folder.CF_FOLDERS), predicate, ConsistencyLevel.ONE);
        log.debug("sent folder data fetched");
        if (cscs == null || cscs.isEmpty()) {
            throw new FolderNotFoundException();
        }

        sm.setNextPageTimestamp(ConvertUtils.bytesToLong(cscs.get(cscs.size() - 1).getSuper_column().getName()));

        int counter = 0;
        // nasetovani dat do objektu slozky
        for (ColumnOrSuperColumn csc : cscs) {
            if (counter == number) {
                break;
            }
            long key = ConvertUtils.bytesToLong(csc.getSuper_column().getName());
            SubSentMessages subSent = new SubSentMessages();
            for (Column col : csc.getSuper_column().getColumns()) {
                String name = ConvertUtils.bytesToString(col.getName());
                if (name.equals(Folder.TITLE)) {
                    subSent.setTitle(ConvertUtils.bytesToString(col.getValue()));
                } else if (name.equals(SentMessages.MESSAGE)) {
                    subSent.setMessage(ConvertUtils.bytesToString(col.getValue()));
                }
            }
            sm.getTimestamps().put(String.valueOf(key), subSent);
            counter++;
        }
        log.debug("new sent folder setted to object");

        return sm;
    }
}
