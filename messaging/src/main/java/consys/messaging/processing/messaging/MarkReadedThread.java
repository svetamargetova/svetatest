package consys.messaging.processing.messaging;

import consys.messaging.bo.Folder;
import consys.messaging.bo.ThreadAttribute;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.NewThreadMessageException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.processing.messaging.common.MarkCommon;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import org.apache.cassandra.thrift.ColumnPath;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Oznaci vlakno za prectene
 * @throws EntityNotFoundException entita nebyla nalezena
 * @throws ThreadNotFoundException vlakno nebylo nalezeno
 * @throws NewThreadMessageException v mezicase byla pridana nova zprava do vlakna
 * @author pepa
 */
public class MarkReadedThread extends CassandraExceptionWrapper<Object> {

    // logger
    private static Logger log = Logger.getLogger(MarkReadedThread.class);
    // data
    private final String eUuid;
    private final String tUuid;
    private final long lastTimestamp;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param eUuid uuid entity
     * @param tUuid uuid vlakna
     * @param lastTimestamp casove razitko posledni (nejnovejsi) zpravy ve vlakne
     */
    public MarkReadedThread(final String eUuid, final String tUuid, final long lastTimestamp) {
        super();
        this.eUuid = eUuid;
        this.tUuid = tUuid;
        this.lastTimestamp = lastTimestamp;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        // TODO: delat hlubsi kontrolu nez timestamp posledni zpravy?
        // TODO: osefovat krizove misto pro paralelni update ThreadAttribute!!!
        ThreadAttribute ta = MarkCommon.checkThreadAndEntity(eUuid, tUuid);
        if (ta.getLastTimestamp() != lastTimestamp) {
            // byla pridana novejsi zprava a nelze vlakno oznacit za prectene
            log.debug("New message in thread, marking interrupted");
            throw new NewThreadMessageException();
        }

        final ByteBuffer fUuid = ConvertUtils.encodeString(ConvertUtils.folderUuid(eUuid, CassandraUtils.SYSTEM_FOLDER_IN));

        ColumnPath columnPath = new ColumnPath(Folder.CF_FOLDERS);
        columnPath.setSuper_column(ConvertUtils.toBytes(lastTimestamp));
        columnPath.setColumn(ConvertUtils.toBytes(Folder.UPDATE));

        client.remove(fUuid, columnPath, lastTimestamp, ConsistencyLevel.ONE);

        if (log.isDebugEnabled()) {
            log.debug("Folder " + fUuid + " marked as not updated");
        }

        // nic se nevraci
        return null;
    }
}
