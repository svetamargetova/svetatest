package consys.messaging.processing.messaging;

import consys.messaging.bo.Folder;
import consys.messaging.bo.ThreadAttribute;
import consys.messaging.bo.assist.ThreadMessage;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.processing.messaging.common.CommonSend;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.Deletion;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.NotFoundException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Odeslani zpravy do vlakna
 * @throws BanException uzivatel je zabanovany
 * @throws EntityNotFoundException entita nebyla nalezena
 * @throws ThreadNotFoundException vlakno nebylo nalezeno
 * @author pepa
 */
public class SendThreadMessage extends CassandraExceptionWrapper {

    // logger
    private static Logger log = Logger.getLogger(SendThreadMessage.class);
    // data
    private ThreadMessage message;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param message je zprava urcena k odeslani
     */
    public SendThreadMessage(final ThreadMessage message) {
        super();
        this.message = message;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        try {
            // overeni odesilatele
            CommonSend.checkSender(client, message);
        } catch (NotFoundException ex) {
            log.error("entity not found: " + message.getFrom());
            throw new EntityNotFoundException();
        }

        // TODO: osefovat krizove misto pro paralelni update ThreadAttribute !!!
        final long timestamp = System.currentTimeMillis();

        // overeni existence vlakna a zaroven nacteni atributu
        ThreadAttribute ta = new LoadThreadAttributes(message.getThread()).run();
        final long oldLastTimestamp = ta.getLastTimestamp();
        if (log.isDebugEnabled()) {
            log.debug("loaded thread attribute: " + ta.getUuid());
        }

        // ulozeni zpravy
        List<String> receivers = ConvertUtils.normalize(ta.getReceivers());
        Map<ByteBuffer, Map<String, List<Mutation>>> threadMessage = createThreadMessage(timestamp, message, receivers);
        client.batch_mutate(threadMessage, ConsistencyLevel.ONE);
        log.debug("new message to thread saved");

        // pridani novych udaju do slozek
        Map<ByteBuffer, Map<String, List<Mutation>>> folderMessage = CommonSend.addToFolder(timestamp, message.getThread(),
                message.getFrom(), receivers, ta.getTitle(), message.getMessage(), ta.getAuthor());
        client.batch_mutate(folderMessage, ConsistencyLevel.ONE);
        log.debug("thread messages write folders");

        // smazani starych udaju ze slozek (column update se nesmaze, protoze ma jiny timestamp)
        Map<ByteBuffer, Map<String, List<Mutation>>> folderDeleteMessage = deleteFromFolder(oldLastTimestamp, receivers);
        client.batch_mutate(folderDeleteMessage, ConsistencyLevel.ONE);
        log.debug("redundant message records in folders removed");

        // aktualizace poctu zprav
        List<String> changeEntity = new ArrayList<String>(receivers);
        changeEntity.remove(message.getFrom());
        ActualizeEntityUpdate.get().run(changeEntity, true);

        // nic nevraci
        return null;
    }

    /** vytvori mapu Columnu a Supercolumnu pro davkovy zapis zpravy do vlakna */
    private Map<ByteBuffer, Map<String, List<Mutation>>> createThreadMessage(long timestamp, ThreadMessage message, List<String> receivers) {
        // polozka do SCF Threads
        List<Mutation> threadColumns = CassandraUtils.wrapSCToListMutaion(CommonSend.threadSuperColumn(message, receivers, timestamp));

        // polozka do CF Thread_Attributes - posledni zmena
        ColumnOrSuperColumn taLastCSC = new ColumnOrSuperColumn();
        taLastCSC.setColumn(CassandraUtils.createColumn(ThreadAttribute.LAST_TIMESTAMP, ConvertUtils.toBytes(timestamp), timestamp));
        List<Mutation> threadAttributesColumns = new ArrayList<Mutation>();
        threadAttributesColumns.add(new Mutation().setColumn_or_supercolumn(taLastCSC));

        return CommonSend.createMessageResult(message.getThread(), threadColumns, threadAttributesColumns);
    }

    /** vytvori mapu Columnu a Supercolumnu pro davkove smazani starych informaci z SCF Folders */
    private Map<ByteBuffer, Map<String, List<Mutation>>> deleteFromFolder(long oldLastTimestamp, List<String> receivers) {
        Map<ByteBuffer, Map<String, List<Mutation>>> result = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();
        for (String uuid : receivers) {
            Deletion deletion = new Deletion();
            deletion.setTimestamp(oldLastTimestamp);
            deletion.setSuper_column(ConvertUtils.toBytes(oldLastTimestamp));
            List<Mutation> deleteColumns = new ArrayList<Mutation>();
            deleteColumns.add(new Mutation().setDeletion(deletion));
            Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
            subResult.put(Folder.CF_FOLDERS, deleteColumns);
            final String folderUuid = ConvertUtils.folderUuid(uuid, CassandraUtils.SYSTEM_FOLDER_IN);
            result.put(ConvertUtils.encodeString(folderUuid), subResult);
        }
        return result;
    }
}
