package consys.messaging.processing.messaging;

import consys.messaging.bo.Entity;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Aktualizace hodnoty update u entity
 * @author pepa
 */
public class ActualizeEntityUpdate extends CassandraExceptionWrapper<Object> {

    // logger
    private static Logger log = Logger.getLogger(ActualizeEntityUpdate.class);
    // instance
    private static final ActualizeEntityUpdate instance = new ActualizeEntityUpdate();
    // data
    private List<ByteBuffer> entities;
    private boolean add;

    private ActualizeEntityUpdate() {
        super();
    }

    /** vraci instanci ActualizeEntityUpdate */
    public static ActualizeEntityUpdate get() {
        return instance;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        final long timestamp = System.currentTimeMillis();

        // nacteni hodnot update pro vsechny entity
        List<ByteBuffer> columnNames = new ArrayList<ByteBuffer>();
        columnNames.add(ConvertUtils.encodeString(Entity.UPDATE));
        SlicePredicate slicePredicate = new SlicePredicate();
        slicePredicate.setColumn_names(columnNames);
        Map<ByteBuffer, List<ColumnOrSuperColumn>> result = client.multiget_slice(entities,
                new ColumnParent(Entity.CF_ENTITIES), slicePredicate, ConsistencyLevel.ONE);
        log.debug("entitys update values fetched");

        // aktualizace hodnot update pro vsechny entity
        Map<ByteBuffer, Map<String, List<Mutation>>> updateEntites = createEntityUpdate(timestamp, result);
        client.batch_mutate(updateEntites, ConsistencyLevel.ONE);
        log.debug("entity update updated");

        // nic nevraci
        return null;
    }

    /** vytvori mapu Columnu a Supercolumnu pro davkovy zapis aktualizace update entity */
    private Map<ByteBuffer, Map<String, List<Mutation>>> createEntityUpdate(final long timestamp,
            Map<ByteBuffer, List<ColumnOrSuperColumn>> entities) {
        Map<ByteBuffer, Map<String, List<Mutation>>> result = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();

        for (Entry<ByteBuffer, List<ColumnOrSuperColumn>> entity : entities.entrySet()) {
            List<ColumnOrSuperColumn> values = entity.getValue();
            if (values.size() != 1) {
                throw new CassandraFailException();
            }
            int updated = ConvertUtils.bytesToInt(values.get(0).getColumn().getValue());
            int diff = add ? 1 : -1;

            ColumnOrSuperColumn taUpdateCSC = new ColumnOrSuperColumn();
            taUpdateCSC.setColumn(CassandraUtils.createColumn(Entity.UPDATE, ConvertUtils.toBytes(updated + diff), timestamp));
            List<Mutation> entitieColumns = new ArrayList<Mutation>();
            entitieColumns.add(new Mutation().setColumn_or_supercolumn(taUpdateCSC));

            Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
            subResult.put(Entity.CF_ENTITIES, entitieColumns);
            result.put(entity.getKey(), subResult);
        }

        return result;
    }

    @Override
    public Object run() {
        throw new UnsupportedOperationException("Not supported, use synchronized run.");
    }

    /**
     * synchronizovane spusteni akce
     * @param keyspace klicovy prostor cassandry
     * @param entities uuid aktualizovanych entit
     */
    public synchronized Object run(List<String> entities, boolean add) {
        this.entities = new ArrayList<ByteBuffer>();

        for (String s : entities) {
            this.entities.add(ConvertUtils.encodeString(s));
        }

        this.add = add;
        return super.run();
    }
}
