package consys.messaging.processing.messaging;

import consys.messaging.bo.ThreadAttribute;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Nacte atributy vlakna
 * @throws ThreadNotFoundException vlakno nebylo nalezeno
 * @author pepa
 */
public class LoadThreadAttributes extends CassandraExceptionWrapper<ThreadAttribute> {

    // logger
    private static Logger log = Logger.getLogger(LoadThreadAttributes.class);
    // data
    private final ByteBuffer uuid;
    private String originalUuid;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param uuid vlakna kteremu atribut patri
     */
    public LoadThreadAttributes(final String uuid) {
        super();
        originalUuid = uuid;
        this.uuid = ConvertUtils.encodeString(uuid);
    }

    @Override
    protected ThreadAttribute doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        ThreadAttribute ta = new ThreadAttribute();
        ta.setUuid(originalUuid);

        // nacteni sloupcu atributu vlakna
        List<ByteBuffer> keys = new ArrayList<ByteBuffer>();
        keys.add(uuid);

        List<ByteBuffer> columnNames = new ArrayList<ByteBuffer>();
        columnNames.add(ConvertUtils.encodeString(ThreadAttribute.AUTHOR));
        columnNames.add(ConvertUtils.encodeString(ThreadAttribute.TITLE));
        columnNames.add(ConvertUtils.encodeString(ThreadAttribute.RECEIVERS));
        columnNames.add(ConvertUtils.encodeString(ThreadAttribute.LAST_TIMESTAMP));
        SlicePredicate slicePredicate = new SlicePredicate();
        slicePredicate.setColumn_names(columnNames);

        Map<ByteBuffer, List<ColumnOrSuperColumn>> result = client.multiget_slice(keys,
                new ColumnParent(ThreadAttribute.CF_THREAD_ATTRIBUTES), slicePredicate, ConsistencyLevel.ONE);
        log.debug("thread attribute data fetched");

        List<ColumnOrSuperColumn> columns = result.get(uuid);
        if (columns == null || columns.isEmpty()) {
            throw new ThreadNotFoundException();
        }

        for (ColumnOrSuperColumn csc : columns) {
            Column col = csc.getColumn();
            String name = new String(col.getName());
            if (name.equals(ThreadAttribute.TITLE)) {
                ta.setTitle(ConvertUtils.bytesToString(col.getValue()));
            } else if (name.equals(ThreadAttribute.LAST_TIMESTAMP)) {
                ta.setLastTimestamp(ConvertUtils.bytesToLong(col.getValue()));
            } else if (name.equals(ThreadAttribute.RECEIVERS)) {
                ta.setReceivers(ConvertUtils.bytesToString(col.getValue()));
            } else if (name.equals(ThreadAttribute.AUTHOR)) {
                ta.setAuthor(ConvertUtils.bytesToString(col.getValue()));
            }
        }
        log.debug("new threadAttributes setted to object");

        return ta;
    }
}
