package consys.messaging.processing.messaging.common;

import consys.messaging.bo.Entity;
import consys.messaging.bo.Folder;
import consys.messaging.bo.SentMessages;
import consys.messaging.bo.Thread;
import consys.messaging.bo.ThreadAttribute;
import consys.messaging.bo.assist.Message;
import consys.messaging.exception.BanException;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.Cassandra;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.NotFoundException;
import org.apache.cassandra.thrift.SuperColumn;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Obecne funkce pro odeslani zpravy
 * @author pepa
 */
public class CommonSend {

    // logger
    private static Logger log = Logger.getLogger(CommonSend.class);

    /**
     * overi zda neni odesilatel zabanovany
     * @throws BanException uzivatel je zabanovany
     */
    public static void checkSender(Cassandra.Client client, final Message message)
            throws InvalidRequestException, NotFoundException, UnavailableException, TimedOutException, TException {
        ColumnOrSuperColumn authorBan = client.get(ConvertUtils.encodeString(message.getFrom()), Entity.banCP, ConsistencyLevel.ONE);
        if (ConvertUtils.bytesToBoolean(authorBan.getColumn().getValue())) {
            if (log.isDebugEnabled()) {
                log.debug("entity " + message.getFrom() + " is banned");
            }
            throw new BanException();
        }
        log.debug("from entity ban checked");
    }

    /**
     * vytvori supercolumn vlakna
     * @param message vlastni zprava (kdo posila a co pise)
     * @param receivers seznam vsech ucastniku komunikace (jak odesilatel tak prijemci)
     * @param timestamp slouzi jako klic superColumnu i jako parametr pro vytvareni columnu
     */
    public static SuperColumn threadSuperColumn(Message message, List<String> receivers, long timestamp) {
        SuperColumn threadSuperColumn = new SuperColumn();
        threadSuperColumn.setName(ConvertUtils.toBytes(timestamp));
        // text zpravy
        threadSuperColumn.addToColumns(CassandraUtils.createColumn(Thread.TEXT, message.getMessage(), timestamp));
        // odesilatel nove zpravy nebo zpravy do vlakna
        threadSuperColumn.addToColumns(CassandraUtils.createColumn(Thread.FROM, message.getFrom(), timestamp));
        for (String receiver : receivers) {
            // vsichni ucastnici komunikace
            Boolean value = Boolean.FALSE;
            if (receiver.equals(message.getFrom())) {
                // odesilatel ma zpravu oznacenou jako prectenou, ostatni jako neprectenou
                value = Boolean.TRUE;
            }
            threadSuperColumn.addToColumns(CassandraUtils.createColumn(receiver, value, timestamp));
        }
        return threadSuperColumn;
    }

    /**
     * vytvori vyslednou mapu pozadavku pro vytvoreni zpravy
     * @param threadUuid uuid vlakna
     * @param threadColumns seznam mutaci urcenych pro vlakno
     * @param threadAttributesColumns seznam mutaci urcenych pro atributy vlakna
     */
    public static Map<ByteBuffer, Map<String, List<Mutation>>> createMessageResult(String threadUuid,
            List<Mutation> threadColumns, List<Mutation> threadAttributesColumns) {
        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Thread.CF_THREADS, threadColumns);
        subResult.put(ThreadAttribute.CF_THREAD_ATTRIBUTES, threadAttributesColumns);
        Map<ByteBuffer, Map<String, List<Mutation>>> result = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();
        result.put(ConvertUtils.encodeString(threadUuid), subResult);
        return result;
    }

    /** vytvori mapu Columnu a Supercolumnu pro davkovy zapis udaju o nove zprave do slozek prijemcu a odesilatele */
    public static Map<ByteBuffer, Map<String, List<Mutation>>> addToFolder(long timestamp, String threadUuid,
            String sender, List<String> receivers, String title, String message, String threadAuthor) {
        Map<ByteBuffer, Map<String, List<Mutation>>> result = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();

        // polozka do SCF Folders - prijemci i odesilatel
        for (String receiver : receivers) {
            boolean updated = !sender.equals(receiver);
            List<Mutation> folderColumns = CassandraUtils.wrapSCToListMutaion(
                    folderSuperColumn(threadUuid, updated, title, threadAuthor, timestamp));

            Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
            subResult.put(Folder.CF_FOLDERS, folderColumns);

            final String folderUuid = ConvertUtils.folderUuid(receiver, CassandraUtils.SYSTEM_FOLDER_IN);
            result.put(ConvertUtils.encodeString(folderUuid), subResult);
        }

        // vlozeni do slozky odeslane zpravy odesilatele
        List<Mutation> sentFolderColumns = CassandraUtils.wrapSCToListMutaion(sentFolderSuperColumn(title, message, timestamp));

        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Folder.CF_FOLDERS, sentFolderColumns);

        final String folderUuid = ConvertUtils.folderUuid(sender, CassandraUtils.SYSTEM_FOLDER_OUT);
        result.put(ConvertUtils.encodeString(folderUuid), subResult);

        return result;
    }

    /** vytvori supercolumn slozky */
    private static SuperColumn folderSuperColumn(String threadUuid, boolean updated, String title, String threadAuthor, long timestamp) {
        SuperColumn folderSuperColumn = new SuperColumn();
        folderSuperColumn.setName(ConvertUtils.toBytes(timestamp));
        folderSuperColumn.addToColumns(CassandraUtils.createColumn(Folder.TITLE, title, timestamp));
        folderSuperColumn.addToColumns(CassandraUtils.createColumn(Folder.AUTHOR, threadAuthor, timestamp));
        folderSuperColumn.addToColumns(CassandraUtils.createColumn(Folder.THREAD, threadUuid, timestamp));
        folderSuperColumn.addToColumns(CassandraUtils.createColumn(Folder.UPDATE, updated, timestamp));
        return folderSuperColumn;
    }

    /** vytvori supercolumn slozky odeslanych zprav */
    private static SuperColumn sentFolderSuperColumn(String title, String message, long timestamp) {
        SuperColumn folderSuperColumn = new SuperColumn();
        folderSuperColumn.setName(ConvertUtils.toBytes(timestamp));
        folderSuperColumn.addToColumns(CassandraUtils.createColumn(SentMessages.TITLE, title, timestamp));
        folderSuperColumn.addToColumns(CassandraUtils.createColumn(SentMessages.MESSAGE, message, timestamp));
        return folderSuperColumn;
    }
}
