package consys.messaging.processing.messaging.common;

import consys.messaging.bo.ThreadAttribute;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.messaging.LoadThreadAttributes;
import org.apache.log4j.Logger;

/**
 * Obecne metody pro oznacovani za prectene
 * @author pepa
 */
public class MarkCommon {

    // logger
    private static Logger log = Logger.getLogger(MarkCommon.class);

    /**
     * overeni existence tUuid a jestli entita patri do vlakna
     * @throws ThreadNotFoundException vlakno nebylo nalezeno
     * @throws EntityNotFoundException entita nebyla nalezena
     */
    public static ThreadAttribute checkThreadAndEntity(final String eUuid, final String tUuid) {
        ThreadAttribute ta = new LoadThreadAttributes(tUuid).run();
        if (!ta.getReceivers().contains(eUuid)) {
            log.error("entity not found " + eUuid + " in thread " + tUuid);
            throw new EntityNotFoundException();
        }
        return ta;
    }
}
