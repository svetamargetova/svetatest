package consys.messaging.processing.messaging;

import consys.messaging.bo.Thread;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.ThreadNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.processing.messaging.common.MarkCommon;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.SuperColumn;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Oznaci zpravu za prectenou
 * @throws EntityNotFoundException entita nebyla nalezena
 * @throws ThreadNotFoundException vlakno nebylo nalezeno
 * @author pepa
 */
public class MarkReadedMessage extends CassandraExceptionWrapper<Object> {

    // logger
    private static Logger log = Logger.getLogger(MarkReadedMessage.class);
    // data
    private final String eUuid;
    private final String tUuid;
    private final long timestamp;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param eUuid uuid entity
     * @param tUuid uuid vlakna
     * @param timestamp casove razitko zpravy
     */
    public MarkReadedMessage(final String eUuid, final String tUuid, long timestamp) {
        super();
        this.eUuid = eUuid;
        this.tUuid = tUuid;
        this.timestamp = timestamp;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        MarkCommon.checkThreadAndEntity(eUuid, tUuid);

        final long changeTimestamp = System.currentTimeMillis();

        // aktualizace hodnoty ve vlakne
        SuperColumn threadSuperColumn = new SuperColumn();
        threadSuperColumn.setName(ConvertUtils.toBytes(timestamp));
        threadSuperColumn.addToColumns(CassandraUtils.createColumn(eUuid, Boolean.TRUE, changeTimestamp));

        List<Mutation> mutations = CassandraUtils.wrapSCToListMutaion(threadSuperColumn);

        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Thread.CF_THREADS, mutations);
        Map<ByteBuffer, Map<String, List<Mutation>>> result = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();
        result.put(ConvertUtils.encodeString(tUuid), subResult);

        client.batch_mutate(result, ConsistencyLevel.ONE);

        if (log.isDebugEnabled()) {
            log.debug("Thread (" + tUuid + ") message " + timestamp + " marked as readed for entity " + eUuid);
        }

        // aktualizace hodnoty v entite
        List<String> uuids = new ArrayList<String>();
        uuids.add(eUuid);
        ActualizeEntityUpdate.get().run(uuids, false);

        // nic nevraci
        return null;
    }
}
