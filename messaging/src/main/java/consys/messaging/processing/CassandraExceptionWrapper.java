package consys.messaging.processing;

import consys.messaging.SystemCassandra;
import consys.messaging.exception.CassandraFailException;
import org.apache.cassandra.thrift.Cassandra;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Abstraktni predek pro akce s cassandrou, automaticky zpracovava cassandra vyjimky
 * @author pepa
 */
public abstract class CassandraExceptionWrapper<T> {

    // logger
    protected static Logger logger = Logger.getLogger(CassandraExceptionWrapper.class);
    // sluzby
    protected Cassandra.Client client = null;

    /** spusti vykonani akce */
    public T run() throws CassandraFailException {
        T result = null;
        try {
            client = SystemCassandra.get().loanClient();
            result = doActions();
        } catch (TException ex) {
            logger.error(getClass().getName(), ex);
            throw new CassandraFailException();
        } catch (InvalidRequestException ex) {
            logger.error(getClass().getName(), ex);
            throw new CassandraFailException();
        } catch (TimedOutException ex) {
            logger.error(getClass().getName(), ex);
            throw new CassandraFailException();
        } catch (UnavailableException ex) {
            logger.error(getClass().getName(), ex);
            throw new CassandraFailException();
        } finally {
            try {
                SystemCassandra.get().returnClient(client);
            } catch (TException ex) {
                logger.error(getClass().getName() + ": return client", ex);
            }
        }
        return result;
    }

    /**
     * implementace vlastni akci zpracovani pozadavku
     * @return T podle generiky, pokud nic nevraci, vraci se hodnota null
     */
    protected abstract T doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException;
}
