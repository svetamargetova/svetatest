package consys.messaging.processing.wall;

import consys.messaging.bo.Wall;
import consys.messaging.bo.assist.WallMessage;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.MessageNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.SuperColumn;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Aktualizuje zpravu na zdi
 * @throws EntityNotFoundException entita nebyla nalezena
 * @throws MessageNotFoundException zprava nebyla nalezena
 * @author pepa
 */
public class UpdateWallMessage extends CassandraExceptionWrapper<Object> {

    // logger
    private static Logger log = Logger.getLogger(UpdateWallMessage.class);
    // data
    private final long timestamp;
    private final WallMessage msg;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param eUuid uuid entity, ktera aktualizuje
     * @param timestamp casove razitko zpravy
     * @param msg aktualizovana zprava
     */
    public UpdateWallMessage(final long timestamp, final WallMessage msg) {
        super();
        this.timestamp = timestamp;
        this.msg = msg;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        // TODO: overit existenci zpravy
        if (false) {
            throw new MessageNotFoundException();
        }
        // TODO: overit existenci entity
        if (false) {
            throw new EntityNotFoundException();
        }

        // aktualizace zpravy
        Map<ByteBuffer, Map<String, List<Mutation>>> updateWallMessage = createMutation(timestamp, msg);
        client.batch_mutate(updateWallMessage, ConsistencyLevel.ONE);
        log.debug("wall message saved");

        // nic se nevraci
        return null;
    }

    /** vytvori mapu Columnu a Supercolumnu pro davkovou aktualizaci zpravy */
    private Map<ByteBuffer, Map<String, List<Mutation>>> createMutation(final long time, WallMessage message) {
        SuperColumn wallSuperColumn = new SuperColumn();
        wallSuperColumn.setName(ConvertUtils.toBytes(timestamp));
        // odesilatel zpravy
        wallSuperColumn.addToColumns(CassandraUtils.createColumn(Wall.FROM, message.getFrom(), time));
        // text zpravy
        wallSuperColumn.addToColumns(CassandraUtils.createColumn(Wall.TEXT, message.getText(), time));

        List<Mutation> wallColumns = CassandraUtils.wrapSCToListMutaion(wallSuperColumn);

        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Wall.CF_WALLS, wallColumns);
        Map<ByteBuffer, Map<String, List<Mutation>>> result = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();
        result.put(ConvertUtils.encodeString(message.getTo()), subResult);
        return result;
    }
}
