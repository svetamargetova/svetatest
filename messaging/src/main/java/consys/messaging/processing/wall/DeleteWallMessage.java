package consys.messaging.processing.wall;

import consys.messaging.bo.Wall;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.MessageNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.Deletion;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Smazani zpravy ze zdi
 * @throws EntityNotFoundException entita nebyla nalezena
 * @throws MessageNotFoundException zprava nebyla nalezena
 * @author pepa
 */
public class DeleteWallMessage extends CassandraExceptionWrapper<Object> {

    // logger
    private static Logger log = Logger.getLogger(DeleteWallMessage.class);
    // data
    private final String eUuid;
    private final long timestamp;

    public DeleteWallMessage(final String eUuid, final long timestamp) {
        super();
        this.eUuid = eUuid;
        this.timestamp = timestamp;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        // TODO: overit existenci zpravy
        if (false) {
            throw new MessageNotFoundException();
        }
        // TODO: overit existenci entity
        if (false) {
            throw new EntityNotFoundException();
        }

        Map<ByteBuffer, Map<String, List<Mutation>>> deleteWallMessage = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();
        Deletion deletion = new Deletion().setTimestamp(timestamp).setSuper_column(ConvertUtils.toBytes(timestamp));
        List<Mutation> deleteColumns = new ArrayList<Mutation>();
        deleteColumns.add(new Mutation().setDeletion(deletion));
        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Wall.CF_WALLS, deleteColumns);
        deleteWallMessage.put(ConvertUtils.encodeString(eUuid), subResult);

        client.batch_mutate(deleteWallMessage, ConsistencyLevel.ONE);
        if (log.isDebugEnabled()) {
            log.debug("wall " + eUuid + " message " + timestamp + " removed");
        }
        // nic se nevraci
        return null;
    }
}
