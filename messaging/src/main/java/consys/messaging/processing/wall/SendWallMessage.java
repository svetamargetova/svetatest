package consys.messaging.processing.wall;

import consys.messaging.bo.Entity;
import consys.messaging.bo.Wall;
import consys.messaging.bo.assist.WallMessage;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.NotFoundException;
import org.apache.cassandra.thrift.SuperColumn;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Odesle zpravu na zed
 * @throws EntityNotFoundException entita nebyla nalezena
 * @author pepa
 */
public class SendWallMessage extends CassandraExceptionWrapper<Object> {

    // logger
    private static Logger log = Logger.getLogger(SendWallMessage.class);
    // data
    private final WallMessage message;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param message vlastni zprava
     */
    public SendWallMessage(final WallMessage message) {
        super();
        this.message = message;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        final long timestamp = System.currentTimeMillis();

        ByteBuffer checkUuid = null;
        try {
            checkUuid = ConvertUtils.encodeString(message.getFrom());
            client.get(checkUuid, Entity.banCP, ConsistencyLevel.ONE);
            checkUuid = ConvertUtils.encodeString(message.getTo());
            client.get(checkUuid, Entity.banCP, ConsistencyLevel.ONE);

            log.debug("to entities checked");
        } catch (NotFoundException ex) {
            log.error("entity not found: " + checkUuid);
            throw new EntityNotFoundException();
        }

        // ulozeni zpravy
        Map<ByteBuffer, Map<String, List<Mutation>>> newWallMessage = createMutation(timestamp, message);
        client.batch_mutate(newWallMessage, ConsistencyLevel.ONE);
        log.debug("wall message saved");

        // nic nevraci
        return null;
    }

    /** vytvori mapu Columnu a Supercolumnu pro davkovy zapis zpravy */
    private Map<ByteBuffer, Map<String, List<Mutation>>> createMutation(final long timestamp, WallMessage message) {
        SuperColumn wallSuperColumn = new SuperColumn();
        wallSuperColumn.setName(ConvertUtils.toBytes(timestamp));
        // odesilatel zpravy
        wallSuperColumn.addToColumns(CassandraUtils.createColumn(Wall.FROM, message.getFrom(), timestamp));
        // text zpravy
        wallSuperColumn.addToColumns(CassandraUtils.createColumn(Wall.TEXT, message.getText(), timestamp));

        List<Mutation> wallColumns = CassandraUtils.wrapSCToListMutaion(wallSuperColumn);

        Map<String, List<Mutation>> subResult = new HashMap<String, List<Mutation>>();
        subResult.put(Wall.CF_WALLS, wallColumns);
        Map<ByteBuffer, Map<String, List<Mutation>>> result = new HashMap<ByteBuffer, Map<String, List<Mutation>>>();
        result.put(ConvertUtils.encodeString(message.getTo()), subResult);
        return result;
    }
}
