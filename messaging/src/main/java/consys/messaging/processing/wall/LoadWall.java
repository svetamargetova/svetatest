package consys.messaging.processing.wall;

import consys.messaging.bo.Wall;
import consys.messaging.bo.assist.SubWall;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.List;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Nacte zpravy na zdi
 * @throws EntityNotFoundException entita nebyla nalezena
 * @author pepa
 */
public class LoadWall extends CassandraExceptionWrapper<Wall> {

    // logger
    private static Logger log = Logger.getLogger(LoadWall.class);
    // data
    private final ByteBuffer eUuid;
    private final String originalEUuid;
    private final int number;
    private final long offset;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param eUuid uuid entity, ktere zed patri
     * @param number maximalni pocet zaznamu k nacteni
     * @param offset casove razitko od ktereho se ma nacitat
     */
    public LoadWall(final String eUuid, final int number, final long offset) {
        super();
        originalEUuid = eUuid;
        this.eUuid = ConvertUtils.encodeString(eUuid);
        this.number = number;
        this.offset = offset;
    }

    @Override
    protected Wall doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        Wall wall = new Wall();
        wall.setUuid(originalEUuid);

        // nacteni poctu zprav na zdi
        int itemsCount = client.get_count(eUuid, new ColumnParent(Wall.CF_WALLS),
                CassandraUtils.getLongSlicePredicate(), ConsistencyLevel.ONE);
        if (log.isDebugEnabled()) {
            log.debug("wall items: " + itemsCount);
        }

        if (itemsCount == 0) {
            return wall;
        }
        wall.setTotal(itemsCount);

        // nacteni zprav na zdi
        SlicePredicate predicate = new SlicePredicate();
        if (number == 0 && offset == 0) {
            // bude se nacitat vsechno
            predicate.setSlice_range(new SliceRange(ConvertUtils.encodeString(""), ConvertUtils.encodeString(""), true, itemsCount));
        } else {
            // bude se nacitat jen interval
            predicate.setSlice_range(new SliceRange(ConvertUtils.encodeLong(System.currentTimeMillis()),
                    ConvertUtils.encodeLong(offset), true, number));
        }

        List<ColumnOrSuperColumn> cscs = client.get_slice(eUuid, new ColumnParent(Wall.CF_WALLS), predicate, ConsistencyLevel.ONE);
        log.debug("wall data fetched");
        if (cscs == null || cscs.isEmpty()) {
            throw new EntityNotFoundException();
        }

        // nasetovani dat do objektu
        for (ColumnOrSuperColumn csc : cscs) {
            long key = ConvertUtils.bytesToLong(csc.getSuper_column().getName());
            SubWall subWall = new SubWall();
            for (Column col : csc.getSuper_column().getColumns()) {
                String name = ConvertUtils.bytesToString(col.getName());
                if (name.equals(Wall.TEXT)) {
                    subWall.setText(ConvertUtils.bytesToString(col.getValue()));
                } else if (name.equals(Wall.FROM)) {
                    subWall.setFrom(ConvertUtils.bytesToString(col.getValue()));
                }
            }
            wall.getTimestamps().put(key, subWall);
        }
        log.debug("new wall setted to object");

        return wall;
    }
}
