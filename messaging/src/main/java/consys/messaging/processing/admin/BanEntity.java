package consys.messaging.processing.admin;

import consys.messaging.bo.Entity;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.NotFoundException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Nastavuje hodnotu banu zadane entite
 * @throws EntityNotFoundException entita nebyla nalezena
 * @author pepa
 */
public class BanEntity extends CassandraExceptionWrapper {

    // logger
    private static Logger log = Logger.getLogger(BanEntity.class);
    // data
    private final ByteBuffer eUuid;
    private final boolean ban;

    public BanEntity(final String eUuid, final boolean ban) {
        super();
        this.eUuid = ConvertUtils.encodeString(eUuid);
        this.ban = ban;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        final long timestamp = System.currentTimeMillis();
        final byte[] value = ConvertUtils.toBytes(ban);

        try {
            // overeni existence entity
            client.get(eUuid, Entity.banCP, ConsistencyLevel.ONE);
            log.debug("entity uuid checked");
            // vlozeni nove hodnoty do polozky ban
            client.insert(eUuid, Entity.parent, CassandraUtils.createColumn(Entity.BAN, value, timestamp), ConsistencyLevel.ONE);
            log.debug("ban inserted");
        } catch (NotFoundException ex) {
            log.error("entity not found: " + eUuid);
            throw new EntityNotFoundException();
        }

        // nevraci zadny vysledek
        return null;
    }
}
