package consys.messaging.processing.admin;

import consys.messaging.bo.Entity;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.CassandraUtils;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Zaregistruje entitu
 * @author pepa
 */
public class RegisterEntity extends CassandraExceptionWrapper {

    // logger
    private static Logger log = Logger.getLogger(RegisterEntity.class);
    // data
    private final Entity entity;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param entity registrovana entita
     */
    public RegisterEntity(final Entity entity) {
        super();
        this.entity = entity;
    }

    @Override
    protected Object doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        // TODO: testovat jestli uz uuid neni ulozeno?
        final long timestamp = System.currentTimeMillis();
        final ByteBuffer uuid = ConvertUtils.encodeString(entity.getUuid());

        // ban
        client.insert(uuid, Entity.parent, CassandraUtils.createColumn(Entity.BAN, entity.isBan(), timestamp), ConsistencyLevel.ONE);
        log.debug("ban value inserted");
        // email
        client.insert(uuid, Entity.parent, CassandraUtils.createColumn(Entity.EMAIL, entity.getEmail(), timestamp), ConsistencyLevel.ONE);
        log.debug("email value inserted");
        // update
        client.insert(uuid, Entity.parent, CassandraUtils.createColumn(Entity.UPDATE, entity.getUpdate(), timestamp), ConsistencyLevel.ONE);
        log.debug("update value inserted");

        // nevraci zadnou hodnotu
        return null;
    }
}
