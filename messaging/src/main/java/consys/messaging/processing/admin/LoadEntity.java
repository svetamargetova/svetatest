package consys.messaging.processing.admin;

import consys.messaging.bo.Entity;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.CassandraExceptionWrapper;
import consys.messaging.utils.ConvertUtils;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnParent;
import org.apache.cassandra.thrift.ConsistencyLevel;
import org.apache.cassandra.thrift.InvalidRequestException;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.TimedOutException;
import org.apache.cassandra.thrift.UnavailableException;
import org.apache.log4j.Logger;
import org.apache.thrift.TException;

/**
 * Nacte entitu
 * @throws EntityNotFoundException entita nebyla nalezena
 * @author pepa
 */
public class LoadEntity extends CassandraExceptionWrapper<Entity> {

    // logger
    private static Logger log = Logger.getLogger(LoadEntity.class);
    // data
    private final ByteBuffer eUuid;
    private final String originalEUuid;

    /**
     * @param keyspace klicovy prostor cassandry
     * @param eUuid uuid nacitane entity
     */
    public LoadEntity(final String eUuid) {
        super();
        originalEUuid = eUuid;
        this.eUuid = ConvertUtils.encodeString(eUuid);
    }

    @Override
    protected Entity doActions() throws InvalidRequestException, UnavailableException, TimedOutException, TException {
        Entity entity = new Entity();
        entity.setUuid(originalEUuid);

        // nacteni sloupcu entity
        List<ByteBuffer> keys = new ArrayList<ByteBuffer>();
        keys.add(eUuid);

        List<ByteBuffer> columnNames = new ArrayList<ByteBuffer>();
        columnNames.add(ConvertUtils.encodeString(Entity.EMAIL));
        columnNames.add(ConvertUtils.encodeString(Entity.BAN));
        columnNames.add(ConvertUtils.encodeString(Entity.UPDATE));
        SlicePredicate slicePredicate = new SlicePredicate();
        slicePredicate.setColumn_names(columnNames);

        Map<ByteBuffer, List<ColumnOrSuperColumn>> result = client.multiget_slice(keys,
                new ColumnParent(Entity.CF_ENTITIES), slicePredicate, ConsistencyLevel.ONE);
        log.debug("entity data fetched");

        List<ColumnOrSuperColumn> columns = result.get(eUuid);
        if (columns == null || columns.isEmpty()) {
            throw new EntityNotFoundException();
        }

        for (ColumnOrSuperColumn csc : columns) {
            Column col = csc.getColumn();
            String name = new String(col.getName());
            if (name.equals(Entity.EMAIL)) {
                entity.setEmail(ConvertUtils.bytesToString(col.getValue()));
            } else if (name.equals(Entity.BAN)) {
                entity.setBan(ConvertUtils.bytesToBoolean(col.getValue()));
            } else if (name.equals(Entity.UPDATE)) {
                entity.setUpdate(ConvertUtils.bytesToInt(col.getValue()));
            }
        }
        log.debug("new entity setted to object");

        return entity;
    }
}
