package consys.messaging.processing;

import consys.messaging.interfaces.DoMessageAction;
import consys.messaging.utils.RequestUtils;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Rozhodovaci cast sax parseru
 * @author pepa
 */
public class SAXRequestHandler extends DefaultHandler {

    // logger
    private static Logger log = Logger.getLogger(SAXRequestHandler.class);
    // konstanty
    public static final String REQUEST = "rq";
    public static final String TYPE = "t";
    // data
    private String temp;
    private int level = 0;
    private int lastLevel = 0;
    private ArrayList<String> list;
    private HashMap<String, Object> result = new HashMap<String, Object>();
    private HttpServletResponse response;

    public SAXRequestHandler(HttpServletResponse response) {
        super();
        this.response = response;
    }

    @Override
    public void startDocument() throws SAXException {
    }

    @Override
    public void endDocument() throws SAXException {
        String type = (String) result.get(TYPE);

        if (!(type != null && RequestUtils.SERVER_REQUEST_TYPES.get(type) != null)) {
            log.error("Unknown request type");
            throw new SAXException();
        }

        try {
            Class clazz = RequestUtils.SERVER_REQUEST_TYPES.get(type);

            DoMessageAction action = (DoMessageAction) clazz.newInstance();
            action.setParameters(result);
            action.setResponse(response);

            action.doAction();
        } catch (InstantiationException ex) {
            log.error("sax run class", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
        } catch (IllegalAccessException ex) {
            log.error("sax run class", ex);
            response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        level++;
        if (level > 3) {
            log.error("Invalid xml depth");
            throw new SAXException();
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        temp = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (level == 2) {
            result.put(qName, lastLevel == 3 ? list : temp);
        } else if (lastLevel != level && level == 3) {
            list = new ArrayList<String>();
            list.add(temp);
        } else if (level == 3) {
            list.add(temp);
        }
        lastLevel = level;
        level--;
        temp = "";
    }
}
