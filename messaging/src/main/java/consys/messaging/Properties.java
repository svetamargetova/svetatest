package consys.messaging;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface Properties {

    public static final String CASSANDRA_HOST = "cassandra.host";
    public static final String CASSANDRA_PORT = "cassandra.port";
    public static final String CASSANDRA_INITIAL_SIZE = "cassandra.initial_size";
    public static final String CASSANDRA_MIN_IDLE = "cassandra.min_idle";
    public static final String CASSANDRA_MAX_IDLE = "cassandra.max_idle";
    public static final String CASSANDRA_MAX_ACTIVE = "cassandra.max_active";
    public static final String MAIL_DEBUG = "mail.debug";
    public static final String MAIL_HOST = "mail.host";
    public static final String MAIL_USER = "mail.user";
    public static final String MAIL_PASS = "mail.pass";
    public static final String MAIL_SSL = "mail.ssl";
    public static final String MAIL_TLS = "mail.tls";
    public static final String MAIL_FROM_DEFAULT = "mail.from_default";
    public static final String MAIL_FROM_BUG = "mail.bug_default";
    public static final String SCHEDULE_MAILS_PER_TASK = "scheduler.mails_per_task";
    public static final String SCHEDULE_DELAY_BETWEEN_MAILS = "scheduler.repeat_delay";
    public static final String INMEMORY_QUEUE_SIZE = "queue.inmemory.length";

}
