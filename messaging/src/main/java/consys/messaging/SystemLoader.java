package consys.messaging;

import consys.messaging.exception.CassandraNotFoundException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Vytvori a zinicializuje system
 * @author pepa
 */
public class SystemLoader implements ServletContextListener {

    // logger
    private static Logger log = LoggerFactory.getLogger(SystemLoader.class);
    // spravce cassandry
    private SystemCassandra cassandra;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            log.info("Initializing ... ");
            // iniciace systemoveho managera + uzamceni jmena keyspace
            SystemManager.get().lockKeyspace();

            // iniciace singletonu pro praci s cassandrou
            try {
                cassandra = SystemCassandra.get();
                SystemMailSender.get();
            } catch (TException ex) {
                log.error("cassandra initialization failed", ex);
                throw new CassandraNotFoundException();
            }

            log.info("Initilized");
        } catch (Exception e) {
            log.error("Initialization failed!", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            SystemMailSender.get().destroy();
        } catch (InterruptedException ex) {
            log.error("Intteruption when waiting for thred");
        }

        if (cassandra != null) {
            // zruseni spojeni do cassandry
            cassandra.destroy();
        }

        log.debug("destroyed");
    }
}
