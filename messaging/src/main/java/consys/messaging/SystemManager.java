package consys.messaging;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author pepa
 */
public class SystemManager {

    // logger
    private static Logger log = Logger.getLogger(SystemManager.class);
    // instance
    private static SystemManager instance = new SystemManager();
    // konstanty
    private static final String KEYSPACE = "Messaging";
    private static final String KEYSPACE_TEST = "MessagingTest";
    // data
    private Properties properties;
    private boolean closed = false;
    private boolean lock = false;
    private boolean test = false;
    private String awsAccessKey;
    private String awsSecretKey;

    private SystemManager() {
        InputStream inputStream = getClass().getResourceAsStream("System.properties");
        properties = new Properties();

        awsAccessKey = System.getenv("aws_access");
        awsSecretKey = System.getenv("aws_secret");

        if (awsAccessKey == null || awsSecretKey == null) {
            throw new IllegalStateException("Missing enviroment variable AWS");
        }

        try {
            // nacteni properties
            properties.load(inputStream);
        } catch (IOException ex) {
            log.error("Reading System.properties", ex);
        }
    }

    /** vraci instanci SystemManager */
    public static SystemManager get() {
        return instance;
    }

    /** vraci keyspace */
    public String keyspace() {
        if (test) {
            return KEYSPACE_TEST;
        }
        return KEYSPACE;
    }

    /** vraci aplikacni properties */
    public Properties properties() {
        return properties;
    }

    /** !!! vola se pouze z testu, meni se keyspace !!! */
    public boolean setTest() {
        if (!lock) {
            test = true;
            return true;
        }
        return false;
    }

    /** uzamkne keyspace a ten uz nejde menit (kvuli testum, abych si to nahodou nekde omylem nezmenil v ostrem) */
    public void lockKeyspace() {
        lock = true;
    }

    /** @return true pokud je server uzavren */
    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /** pristupovy klic aws */
    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    /** tajny klic aws */
    public String getAwsSecretKey() {
        return awsSecretKey;
    }
}
