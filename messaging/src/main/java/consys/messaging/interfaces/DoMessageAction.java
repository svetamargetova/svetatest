package consys.messaging.interfaces;

import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 * Interface pro provadeni akce
 * @author pepa
 */
public interface DoMessageAction {

    /** spusteni akce */
    public void doAction();

    /** vlozeni parametru ziskanych ze serveroveho pozadavku */
    public void setParameters(HashMap<String, Object> params);

    /** vlozeni parametru ziskanych z klientskeho pozadavku */
    public void setParameters(JSONObject json);

    /** nastaveni odpovedi */
    public void setResponse(HttpServletResponse response);
}
