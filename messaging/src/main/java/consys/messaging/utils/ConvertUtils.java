package consys.messaging.utils;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Udelatko pro prevody
 * @author pepa
 */
public class ConvertUtils {

    // logger
    private static Logger log = Logger.getLogger(ConvertUtils.class);
    // konstanty
    public static final String UTF8 = "UTF-8";

    /** prevede zadany string na pole bytu v kodovani utf8 */
    public static final byte[] toBytes(String value) {
        try {
            return value.getBytes(UTF8);
        } catch (UnsupportedEncodingException ex) {
            log.error("toBytes", ex);
        }
        return null;
    }

    /** prevede pole bytu na utf8 string */
    public static final String bytesToString(byte[] b) {
        try {
            return new String(b, UTF8);
        } catch (UnsupportedEncodingException ex) {
            log.error("bytesToString", ex);
        }
        return null;
    }

    /** prevede boolean na pole bytu (1 - true, 0 - false) */
    public static final byte[] toBytes(boolean value) {
        /*byte[] result = {value ? (byte) 1 : (byte) 0};
        return result;*/
        try {
            return new String(value ? "yes" : "no").getBytes(UTF8);
        } catch (UnsupportedEncodingException ex) {
            log.error("toBytes", ex);
        }
        return null;
    }

    /** prevede pole bytu na boolean */
    public static final boolean bytesToBoolean(byte[] b) {
        //return (b[0] == (byte) 1);
        try {
            String value = new String(b, UTF8);
            return value.equals("yes");
        } catch (UnsupportedEncodingException ex) {
            log.error("bytesToString", ex);
        }
        return false;
    }

    /** prevede int na pole bytu */
    public static final byte[] toBytes(int value) {
        /*return new byte[]{
        (byte) (value >>> 24),
        (byte) (value >>> 16),
        (byte) (value >>> 8),
        (byte) value};*/
        try {
            return String.valueOf(value).getBytes(UTF8);
        } catch (UnsupportedEncodingException ex) {
            log.error("toBytes", ex);
        }
        return null;
    }

    /** prevede pole bytu na int */
    public static final int bytesToInt(byte[] b) {
        /*return (b[0] << 24)
        + ((b[1] & 0xFF) << 16)
        + ((b[2] & 0xFF) << 8)
        + (b[3] & 0xFF);*/
        try {
            return Integer.parseInt(new String(b, UTF8));
        } catch (UnsupportedEncodingException ex) {
            log.error("bytesToInt", ex);
        }
        return -1;
    }

    /** prevede long na pole bytu */
    public static final byte[] toBytes(long value) {
        /*return new byte[]{
        (byte) (value >>> 56),
        (byte) (value >>> 48),
        (byte) (value >>> 40),
        (byte) (value >>> 32),
        (byte) (value >>> 24),
        (byte) (value >>> 16),
        (byte) (value >>> 8),
        (byte) value};*/
        try {
            return String.valueOf(value).getBytes(UTF8);
        } catch (UnsupportedEncodingException ex) {
            log.error("toBytes", ex);
        }
        return null;
    }

    /** prevede pole bytu na long, autor: Josef Kopecny */
    public static final long bytesToLong(byte[] b) {
        /*long result = 0;
        for (int i = 0; i < 8; ++i) {
        result |= (((long) b[7 - i]) & 0xff) << (i * 8);
        }
        return result;*/
        try {
            return Long.parseLong(new String(b, UTF8));
        } catch (UnsupportedEncodingException ex) {
            log.error("bytesToLong", ex);
        }
        return -1;
    }

    /** prevede parametry na uuid slozky */
    public static final String folderUuid(String entityUuid, String folderName) {
        return entityUuid + "_" + folderName;
    }

    /** denormalizuje seznam Stringu (oddelovac je carka) -> string1,string2,...,stringN (pozor na carky v Stringach!) */
    public static String denormalize(List<String> values) {
        if (values == null || values.size() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = values.iterator();
        sb.append(it.next());
        while (it.hasNext()) {
            String value = it.next();
            sb.append(",");
            sb.append(value);
        }
        return sb.toString();
    }

    /** normalizuje hodnoty (oddelovac je carka) string1,string2,...,stringN na seznam Stringu*/
    public static List<String> normalize(String value) {
        List<String> result = new ArrayList<String>();
        String[] split = value.split(",");
        for (int i = 0; i < split.length; i++) {
            result.add(split[i]);
        }
        return result;
    }

    /** prekoduje String na ByteBuffer */
    public static ByteBuffer encodeString(String value) {
        try {
            return ByteBuffer.wrap(value.getBytes(UTF8));
        } catch (Exception ex) {
            throw new RuntimeException("encoding key fail: " + value);
        }
    }

    /** prekoduje String na ByteBuffer */
    public static ByteBuffer encodeLong(long value) {
        try {
            return ByteBuffer.wrap(String.valueOf(value).getBytes(UTF8));
        } catch (Exception ex) {
            throw new RuntimeException("encoding key fail: " + value);
        }
    }

    /** prevede ByteBuffer na String */
    public static String decodeString(ByteBuffer value) {
        Charset charset = Charset.forName(UTF8);
        return charset.decode(value).toString();
    }
}
