package consys.messaging.utils;

/**
 * Pomocna udelatka a konstanty pro zpracovani odpovedi
 * @author pepa
 */
public class ResponseUtils {

    // konstanty - mozne stavy odovedi
    public static final String STATE_OK = "OK";
    public static final String STATE_FAIL = "FAIL";
    public static final String STATE_CLOSED = "CLOSED";
    public static final String STATE_BAN = "BAN";
    public static final String STATE_ENTITY_NOT_FOUND = "ENF";
    public static final String STATE_FOLDER_NOT_FOUND = "FNF";
    public static final String STATE_THREAD_NOT_FOUND = "TNF";
    public static final String STATE_MESSAGE_NOT_FOUND = "MNF";
    public static final String STATE_ACCESS_DENIED = "DENIED";
    public static final String STATE_NEW_MESSAGE = "NEW";

    /** vytvori jednoduchou xml odpoved */
    public static String simpleXMLResponse(String state) {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(Tags.RESPONSE);
        sb.append("><");
        sb.append(Tags.RESPONSE_STATE);
        sb.append(">");
        sb.append(state);
        sb.append("</");
        sb.append(Tags.RESPONSE_STATE);
        sb.append("></");
        sb.append(Tags.RESPONSE);
        sb.append(">");
        return sb.toString();
    }

    /** vytvori jednoduchou json odpoved */
    public static String simpleJSONResponse(String state) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"");
        sb.append(Tags.RESPONSE_STATE);
        sb.append("\":\"");
        sb.append(state);
        sb.append("\"}");
        return sb.toString();
    }
}
