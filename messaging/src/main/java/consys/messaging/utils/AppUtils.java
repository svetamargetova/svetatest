package consys.messaging.utils;

import consys.messaging.Properties;
import consys.messaging.SystemManager;
import org.apache.log4j.Logger;

/**
 * Aplikacni nastroje a konstanty
 * @author pepa
 */
public class AppUtils implements Properties {

    // logger
    private static Logger log = Logger.getLogger(AppUtils.class);

    /** mail pro zasilani bugu */
    public static String bugMail() {
        return SystemManager.get().properties().getProperty(MAIL_FROM_BUG).trim();
    }

    /** adresa cassandry */
    public static String cassandraAddress() {
        return SystemManager.get().properties().getProperty(CASSANDRA_HOST).trim();
    }

    /** adresa cassandry */
    public static int cassandraPort() {
        return Integer.parseInt(SystemManager.get().properties().getProperty(CASSANDRA_PORT).trim());
    }

    /** adresa cassandry */
    public static int cassandraInitialSize() {
        return Integer.parseInt(SystemManager.get().properties().getProperty(CASSANDRA_INITIAL_SIZE).trim());
    }

    /** adresa cassandry */
    public static int cassandraMinIdle() {
        return Integer.parseInt(SystemManager.get().properties().getProperty(CASSANDRA_MIN_IDLE).trim());
    }

    /** adresa cassandry */
    public static int cassandraMaxIdle() {
        return Integer.parseInt(SystemManager.get().properties().getProperty(CASSANDRA_MAX_IDLE).trim());
    }

    /** adresa cassandry */
    public static int cassandraMaxActive() {
        return Integer.parseInt(SystemManager.get().properties().getProperty(CASSANDRA_MAX_ACTIVE).trim());
    }

    /** budou zapnute debugovaci vypisy? */
    public static boolean mailDebug() {
        return Boolean.parseBoolean(SystemManager.get().properties().getProperty(MAIL_DEBUG));
    }

    /** adresa mailoveho serveru */
    public static String mailHost() {
        return SystemManager.get().properties().getProperty(MAIL_HOST).trim();
    }

    /** prihlasovaci heslo k mail serveru */
    public static String mailPass() {
        return SystemManager.get().properties().getProperty(MAIL_PASS).trim();
    }

    /** prihlasovaci jmeno k mail serveru */
    public static String mailUser() {
        return SystemManager.get().properties().getProperty(MAIL_USER).trim();
    }

    /** bude se pri prihlasovani pouzivat ssl? */
    public static boolean mailSsl() {
        return Boolean.parseBoolean(SystemManager.get().properties().getProperty(MAIL_SSL));
    }

    /** bude se pri prihlasovani pouzivat ssl? */
    public static boolean mailTLS() {
        return Boolean.parseBoolean(SystemManager.get().properties().getProperty(MAIL_TLS));
    }

    /** vraci systemovou emailovou adresu, na kterou se nema odpovidat */
    public static String noReplyMail() {
        return SystemManager.get().properties().getProperty(MAIL_FROM_DEFAULT).trim();
    }

    /** vraci maximalni pocet mailu, ktere se mohou poslat pri spusteni casovace  */
    public static int schedulerMailsPerTask() {
        int count = 10;
        try {
            count = Integer.parseInt(SystemManager.get().properties().getProperty(SCHEDULE_MAILS_PER_TASK));
        } catch (NumberFormatException ex) {
            log.warn("unable parse property schedulerMailsPerTask to int, used default count 10", ex);
        }
        return count;
    }

    /** vraci interval spousteni planovaciho tasku (vychozi hodnota je 5sec ms) */
    public static long schedulerRepeatDelay() {
        long delay = 5;
        try {
            delay = Long.parseLong(SystemManager.get().properties().getProperty(SCHEDULE_DELAY_BETWEEN_MAILS));
            delay = delay / 1000;
        } catch (NumberFormatException ex) {
            log.warn("unable parse property schedulerRepeatDelay to long, used default delay 5s", ex);
        }
        return delay;
    }

    public static int inmemoryQueueSize() {
        return Integer.parseInt(SystemManager.get().properties().getProperty(INMEMORY_QUEUE_SIZE).trim());
    }
}
