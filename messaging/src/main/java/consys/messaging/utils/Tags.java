package consys.messaging.utils;

/**
 * Tagy zasilane mezi serverem a klientem
 * @author pepa
 */
public interface Tags {

    /** tag ban - hodnota banu (0 - ne, 1 ano) */
    public static final String BAN = "ban";
    /** tag e - uuid entity */
    public static final String ENTITY = "e";
    /** tag f - uuid zdroje zpravy */
    public static final String FROM = "f";
    /** tag itm - jedna polozka */
    public static final String ITEM = "itm";
    /** tag itms - seznam polozek */
    public static final String ITEMS = "itms";
    /** tag mail - email registrovane entity */
    public static final String MAIL = "mail";
    /** tag name - nazev (titulek, ...) */
    public static final String NAME = "name";
    /** tag msg - text zpravy */
    public static final String MESSAGE = "msg";
    /** tag msgs - seznam zprav */
    public static final String MESSAGES = "msgs";
    /** tag num - cislo, pocet polozek, ... */
    public static final String NUMBER = "num";
    /** tag offs - offset */
    public static final String OFFSET = "offs";
    /** tag sub - predmet zpravy */
    public static final String SUBJECT = "sub";
    /** tag rd - precteno */
    public static final String READED = "rd";
    /** tag rs - koren xml odpovedi */
    public static final String RESPONSE = "rs";
    /** tag r - vysledek akce (data) */
    public static final String RESPONSE_DATA = "r";
    /** tag s - vysledek akce (stav) */
    public static final String RESPONSE_STATE = "s";
    /** tag time - cas */
    public static final String TIME = "time";
    /** tag thrd - uuid vlakna */
    public static final String THREAD = "thrd";
    /** tag to - mnozina prijemcu */
    public static final String TO = "to";
    /** tag total - celkovy pocet polozek */
    public static final String TOTAL = "total";
    /** tag updt - aktualizovano */
    public static final String UPDATED = "updt";
    /** tag html - je to html */
    public static final String HTML = "html";
    /** tag autor - uuid autora */
    public static final String AUTHOR = "autor";
    /** tag next - pristi hodnota */
    public static final String NEXT = "next";
}
