package consys.messaging.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.cassandra.thrift.Column;
import org.apache.cassandra.thrift.ColumnOrSuperColumn;
import org.apache.cassandra.thrift.ColumnPath;
import org.apache.cassandra.thrift.Mutation;
import org.apache.cassandra.thrift.SlicePredicate;
import org.apache.cassandra.thrift.SliceRange;
import org.apache.cassandra.thrift.SuperColumn;

/**
 * Pomocna udelatka pro praci s cassandrou
 * @author pepa
 */
public class CassandraUtils {

    // konstanty
    public static final String SYSTEM_FOLDER_IN = "in";
    public static final String SYSTEM_FOLDER_OUT = "out";
    public static final String QUEUE_NR = "QUEUE_NR";

    /**
     * vytvori column path podle zadanych parametru, typ column
     * @param columnFamily nazev column family
     * @param columnName nazev sloupce
     */
    public static ColumnPath createColumnPath(final String columnFamily, final String columnName) {
        ColumnPath cp = new ColumnPath(columnFamily);
        cp.setColumn(ConvertUtils.toBytes(columnName));
        return cp;
    }

    /**
     * vytvori column path podle zadanych parametru, typ super column
     * @param columnFamily nazev column family
     * @param superColumnName nazev sloupce
     */
    public static ColumnPath createSuperColumnPath(final String columnFamily, final String superColumnName) {
        ColumnPath cp = new ColumnPath(columnFamily);
        cp.setSuper_column(ConvertUtils.toBytes(superColumnName));
        return cp;
    }

    /**
     * vytvori sloupec podle zadanych parametru
     * @param name nazev polozky
     * @param value hodnota
     * @param timestamp casove razitko
     */
    public static Column createColumn(String name, String value, long timestamp) {
        Column c = new Column(ConvertUtils.encodeString(name));
        c.setValue(ConvertUtils.toBytes(value));
        c.setTimestamp(timestamp);
        return c;
    }

    /**
     * vytvori sloupec podle zadanych parametru
     * @param name nazev polozky
     * @param value hodnota
     * @param timestamp casove razitko
     */
    public static Column createColumn(String name, boolean value, long timestamp) {
        Column c = new Column(ConvertUtils.encodeString(name));
        c.setValue(ConvertUtils.toBytes(value));
        c.setTimestamp(timestamp);
        return c;
    }

    /**
     * vytvori sloupec podle zadanych parametru
     * @param name nazev polozky
     * @param value hodnota
     * @param timestamp casove razitko
     */
    public static Column createColumn(String name, int value, long timestamp) {
        Column c = new Column(ConvertUtils.encodeString(name));
        c.setValue(ConvertUtils.toBytes(value));
        c.setTimestamp(timestamp);
        return c;
    }

    /**
     * vytvori sloupec podle zadanych parametru
     * @param name nazev polozky
     * @param value hodnota
     * @param timestamp casove razitko
     */
    public static Column createColumn(String name, byte[] value, long timestamp) {
        Column c = new Column(ConvertUtils.encodeString(name));
        c.setValue(value);
        c.setTimestamp(timestamp);
        return c;
    }

    /** vygeneruje casove uuid */
    public static java.util.UUID getTimeUUID() {
        return java.util.UUID.fromString(new com.eaio.uuid.UUID().toString());
    }

    /** zaobali superColumn do seznamu s mutacema */
    public static List<Mutation> wrapSCToListMutaion(SuperColumn sc) {
        ColumnOrSuperColumn csc = new ColumnOrSuperColumn().setSuper_column(sc);
        List<Mutation> columns = new ArrayList<Mutation>();
        columns.add(new Mutation().setColumn_or_supercolumn(csc));
        return columns;
    }

    /** vraci slice predicate pro vsechny klice s casem v longu (0 az ted) s limitem 10 milionu zaznamu */
    public static SlicePredicate getLongSlicePredicate() {
        final long now = new Date().getTime();
        SliceRange range = new SliceRange(ConvertUtils.encodeLong(0), ConvertUtils.encodeLong(now), false, 10000000);
        SlicePredicate slicePredicate = new SlicePredicate();
        slicePredicate.setSlice_range(range);
        return slicePredicate;
    }
}
