package consys.messaging.utils;

import consys.messaging.processing.action.BanEntityAction;
import consys.messaging.processing.action.DeleteWallMessageAction;
import consys.messaging.processing.action.LoadFolderAction;
import consys.messaging.processing.action.LoadSentMessagesAction;
import consys.messaging.processing.action.LoadThreadAction;
import consys.messaging.processing.action.LoadWallAction;
import consys.messaging.processing.action.MarkReadedMessageAction;
import consys.messaging.processing.action.MarkReadedThreadAction;
import consys.messaging.processing.action.RegisterEntityAction;
import consys.messaging.processing.action.SendNewMessageAction;
import consys.messaging.processing.action.SendThreadMessageAction;
import consys.messaging.processing.action.SendWallMessageAction;
import consys.messaging.processing.action.UpdateWallMessageAction;
import consys.messaging.processing.action.mail.SystemMailAction;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Pomocna udelatka a konstanty pro zpracovani pozadavku
 * @author pepa
 */
public class RequestUtils {

    private static final Logger log = Logger.getLogger(RequestUtils.class);

    // konstanty
    public static final Map<String, Class> SERVER_REQUEST_TYPES = getTypes(true);
    public static final Map<String, Class> CLIENT_REQUEST_TYPES = getTypes(false);
    public static final String EMAIL_REG_EXP = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
    // UUID uz mame ako 32 Hexadecimalnych cisel bez pomlciek
    @Deprecated
    public static final String UUID_REG_EXP = "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$";

    /**
     * validace na boolean (0 - false, 1 - true)
     * @return true pokud je validni
     */
    public static boolean validBoolean(String value) {
        return (value.equals("0") || value.equals("1"));
    }

    /**
     * validace na email
     * @return true pokud je validni
     */
    public static boolean validEmail(String email) {
        return email.toLowerCase().matches(EMAIL_REG_EXP);
    }

    /**
     * validace na uuid
     * @return true pokud je validni
     */
    public static boolean validUuid(String uuid) {
        boolean result = false;
        if (uuid.length() == 32 || uuid.length() == 36) {
            result = true;
        }        
        return result;
    }

    /**
     * vygeneruje seznam podporovanych typu pozadavku
     * @param server je true pokud chceme vygenerovat i typy se serverovymi sluzbami
     */
    private static Map<String, Class> getTypes(boolean server) {
        HashMap<String, Class> types = new HashMap<String, Class>();

        if (server) {
            // registrace
            types.put("register", RegisterEntityAction.class);
            // banovani
            types.put("ban", BanEntityAction.class);
            // systemovy mail
            types.put("sysMail", SystemMailAction.class);
        }

        // odesilani nove zpravy
        types.put("sendNM", SendNewMessageAction.class);
        // odesilani vlaknove zpravy
        types.put("sendTM", SendThreadMessageAction.class);
        // nacteni slozky
        types.put("lf", LoadFolderAction.class);
        // nacteni vlakna
        types.put("lt", LoadThreadAction.class);
        // oznaceni zpravy za prectenou
        types.put("mm", MarkReadedMessageAction.class);
        // oznaceni vlakna za prectene
        types.put("mt", MarkReadedThreadAction.class);
        // oznaceni odeslanych zprav
        types.put("lsm", LoadSentMessagesAction.class);

        // odeslani zpravy na wall
        types.put("sendWM", SendWallMessageAction.class);
        // oznaceni vlakna za prectene
        types.put("lw", LoadWallAction.class);
        // smazani zpravy ze zdi
        types.put("delWM", DeleteWallMessageAction.class);
        // aktualizace zpravy na zdi
        types.put("upWM", UpdateWallMessageAction.class);

        return Collections.synchronizedMap(types);
    }
}
