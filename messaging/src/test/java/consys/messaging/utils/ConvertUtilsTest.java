package consys.messaging.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 * Testy prevodoveho udelatka
 * @author pepa
 */
public class ConvertUtilsTest {

    @Test(groups = "system")
    public void bytesBoolean() {
        /*byte[] bytesFalse = {(byte) 0};
        byte[] bytesTrue = {(byte) 1};*/
        byte[] bytesFalse = {};
        byte[] bytesTrue = {};
        try {
            bytesFalse = new String("no").getBytes(ConvertUtils.UTF8);
            bytesTrue = new String("yes").getBytes(ConvertUtils.UTF8);
        } catch (UnsupportedEncodingException ex) {
            fail("unsupported encoding");
        }
        assertEquals("bad convert to bytes", bytesFalse, ConvertUtils.toBytes(false));
        assertEquals("bad convert to bytes", bytesTrue, ConvertUtils.toBytes(true));
        assertEquals("bad convert to boolean", false, ConvertUtils.bytesToBoolean(bytesFalse));
        assertEquals("bad convert to boolean", true, ConvertUtils.bytesToBoolean(bytesTrue));
    }

    @Test(groups = "system")
    public void bytesString() {
        String input = "Příliš žluťoučký kůň úpěl ďábelské ódy";
        byte[] bytes = ConvertUtils.toBytes(input);
        assertEquals("bad convert to string", input, ConvertUtils.bytesToString(bytes));

    }

    @Test(groups = "system")
    public void bytesInt() {
        int input = -800914953;
        byte[] bytes = {(byte) 208, (byte) 67, (byte) 1, (byte) 247};
        //assertEquals("bad convert to bytes", bytes, ConvertUtils.toBytes(input));
        //assertEquals("bad convert to int", input, ConvertUtils.bytesToInt(bytes));

    }

    @Test(groups = "system")
    public void bytesLong() {
        long input = 1455925548777646779L;
        byte[] bytes = {(byte) 20, (byte) 52, (byte) 124, (byte) 146, (byte) 162, (byte) 78, (byte) 170, (byte) 187};
        //assertEquals("bad convert to bytes", bytes, ConvertUtils.toBytes(input));
        //assertEquals("bad convert to long", input, ConvertUtils.bytesToLong(bytes));
    }

    @Test(groups = "system")
    public void normalizeDenormalize() {
        String denormalized = "1-2-3,4-56,78-9";
        List<String> normalized = new ArrayList<String>();
        normalized.add("1-2-3");
        normalized.add("4-56");
        normalized.add("78-9");

        assertEquals("bad denormalize", denormalized, ConvertUtils.denormalize(normalized));

        List<String> result = ConvertUtils.normalize(denormalized);
        for (int i = 0; i < result.size(); i++) {
            assertEquals("bad normalize", normalized.get(i), result.get(i));
        }
    }
}
