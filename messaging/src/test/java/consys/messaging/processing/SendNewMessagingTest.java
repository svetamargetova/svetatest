package consys.messaging.processing;

import consys.messaging.SystemCassandra;
import consys.messaging.bo.Folder;
import consys.messaging.bo.Thread;
import consys.messaging.bo.ThreadAttribute;
import consys.messaging.bo.assist.NewMessage;
import consys.messaging.bo.assist.SubFolder;
import consys.messaging.bo.assist.SubThread;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.FolderNotFoundException;
import consys.messaging.exception.NewThreadMessageException;
import consys.messaging.processing.admin.LoadEntity;
import consys.messaging.processing.messaging.LoadFolder;
import consys.messaging.processing.messaging.LoadThread;
import consys.messaging.processing.messaging.LoadThreadAttributes;
import consys.messaging.processing.messaging.MarkReadedMessage;
import consys.messaging.processing.messaging.MarkReadedThread;
import consys.messaging.processing.messaging.SendNewMessage;
import consys.messaging.utils.CassandraUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map.Entry;
import java.util.UUID;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */
public class SendNewMessagingTest {

    private static final String from = UUID.randomUUID().toString();
    private static final ArrayList<String> tos = getTos();
    private static final String sub = UUID.randomUUID().toString();
    private static final String msg = UUID.randomUUID().toString();
    private static Thread thread;

    @BeforeClass
    public void init() {
        //SystemManager.get().setTest();
        try {
            SystemCassandra.get().switchToTest();
        } catch (Exception ex) {
            fail("Neco neproslo pri prepnuti na testovaci keyspace");
        }
    }

    @Test(groups = "cassandra", dependsOnMethods = {"testBanEntity"}, dependsOnGroups = "system")
    public void testSendNewMessage() {
        try {
            registerEntities(from, tos);

            NewMessage message = getMessage(from, tos, sub, msg);
            new SendNewMessage(message).run();
        } catch (CassandraFailException ex) {
            fail("Fail while read entity");
        } catch (EntityNotFoundException ex) {
            fail("Fail entity not found in db");
        } catch (FolderNotFoundException ex) {
            fail("Folder not found");
        }
    }

    @Test(groups = "cassandra", dependsOnMethods = {"testSendNewMessage"}, dependsOnGroups = "system")
    public void testSendNewMessageCheckUpdates() {
        // kontrola slozek
        Folder ffr = new LoadFolder(from, CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();
        Folder ft1 = new LoadFolder(tos.get(0), CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();
        Folder ft2 = new LoadFolder(tos.get(1), CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();

        assertEquals("Bad total count item in folder", 1, ffr.getTotal());
        assertEquals("Bad total count item in folder", 1, ft1.getTotal());
        assertEquals("Bad total count item in folder", 1, ft2.getTotal());

        assertEquals("Bad item count in folder result", 1, ffr.getTimestamps().size());
        assertEquals("Bad item count in folder result", 1, ft1.getTimestamps().size());
        assertEquals("Bad item count in folder result", 1, ft2.getTimestamps().size());

        Entry<String, SubFolder> efr = ffr.getTimestamps().entrySet().iterator().next();
        Entry<String, SubFolder> et1 = ft1.getTimestamps().entrySet().iterator().next();
        Entry<String, SubFolder> et2 = ft2.getTimestamps().entrySet().iterator().next();

        assertEquals("Thread timestamp different", efr.getKey(), et1.getKey());
        assertEquals("Thread timestamp different", et1.getKey(), et2.getKey());

        assertEquals("Bad thread value", efr.getValue().getThread(), et1.getValue().getThread());
        assertEquals("Bad thread value", et1.getValue().getThread(), et2.getValue().getThread());

        assertEquals("Bad thread update value", false, efr.getValue().isUpdate());
        assertEquals("Bad thread update value", true, et1.getValue().isUpdate());
        assertEquals("Bad thread update value", true, et2.getValue().isUpdate());

        // kontrola zpravy
        thread = new LoadThread(et1.getValue().getThread(), tos.get(0), true).run();
        SubThread subThread = thread.getTimestamps().entrySet().iterator().next().getValue();
        assertEquals("Bad message in thread", msg, subThread.getText());

        // kontrola atributu vlakna
        String threadUuid = efr.getValue().getThread();
        ThreadAttribute ta = new LoadThreadAttributes(threadUuid).run();

        assertEquals("Bad last update", efr.getKey(), String.valueOf(ta.getLastTimestamp()));
        assertEquals("Bad author", from, ta.getAuthor());
        assertEquals("Bad subject", sub, ta.getTitle());

        ArrayList<String> receivers = new ArrayList<String>(tos);
        receivers.add(from);
        for (String s : receivers) {
            if (!ta.getReceivers().contains(s)) {
                fail("missing entity in receivers");
            }
        }

        // kontrola hodnot update v entitach
        assertEquals("Bad updated value entity from", 0, new LoadEntity(from).run().getUpdate());
        assertEquals("Bad updated value entity to1", 1, new LoadEntity(tos.get(0)).run().getUpdate());
        assertEquals("Bad updated value entity to2", 1, new LoadEntity(tos.get(1)).run().getUpdate());
    }

    @Test(groups = "cassandra", dependsOnMethods = {"testSendNewMessageCheckUpdates"}, dependsOnGroups = "system")
    public void testSendNewMessageMark() {
        String m = thread.getTimestamps().entrySet().iterator().next().getKey();

        // kontrola oznaceni zpravy za neprectenou
        Thread t = new LoadThread(thread.getUuid(), tos.get(0), true).run();
        Boolean readed = t.getTimestamps().entrySet().iterator().next().getValue().getUuids().get(tos.get(0));
        assertEquals("Bad message readed value", Boolean.FALSE, readed);

        // pokus oznaceni vlakna za prectene - neuspesne
        try {
            new MarkReadedThread(tos.get(0), thread.getUuid(), new Date().getTime()).run();
            fail("Thread bad mark as readed");
        } catch (NewThreadMessageException ex) {
            // spravne
        }

        // oznaceni zpravy
        new MarkReadedMessage(tos.get(0), thread.getUuid(), Long.parseLong(m)).run();

        // kontrola oznaceni zpravy za prectenou
        t = new LoadThread(thread.getUuid(), tos.get(0), true).run();
        readed = t.getTimestamps().entrySet().iterator().next().getValue().getUuids().get(tos.get(0));
        assertEquals("Bad message readed value", Boolean.TRUE, readed);

        // pokus oznaceni vlakna za prectene - uspesne
        new MarkReadedThread(tos.get(0), thread.getUuid(), Long.parseLong(m)).run();
        Folder folderT1 = new LoadFolder(tos.get(0), CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();
        Entry<String, SubFolder> eft1 = folderT1.getTimestamps().entrySet().iterator().next();
        if (!eft1.getValue().getThread().equals(t.getUuid())) {
            fail("Different thread uuid");
        }
        assertEquals("Thread not marked as readed", false, eft1.getValue().isUpdate());
    }

    /** new message */
    public static NewMessage getMessage(String from, ArrayList<String> to, String sub, String msg) {
        NewMessage m = new NewMessage();
        m.setFrom(from);
        for (String t : to) {
            m.getTo().put(t, Boolean.FALSE);
        }
        m.setSubject(sub);
        m.setMessage(msg);
        return m;
    }

    /** vraci dva prijemce */
    public static ArrayList<String> getTos() {
        ArrayList<String> to = new ArrayList<String>();
        for (int i = 0; i < 2; i++) {
            to.add(UUID.randomUUID().toString());
        }
        return to;
    }

    /** zaregistruje odesilaci a entitu a dve prijimaci */
    public static void registerEntities(String from, ArrayList<String> to) {
        final String email = "myTestMail@my.my";
        AdminServicesTest.registerEntity(from, email);
        AdminServicesTest.registerEntity(to.get(0), email);
        AdminServicesTest.registerEntity(to.get(1), email);
    }
}
