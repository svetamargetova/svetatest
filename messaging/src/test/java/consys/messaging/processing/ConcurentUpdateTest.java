package consys.messaging.processing;

import consys.messaging.bo.Entity;
import consys.messaging.bo.Folder;
import consys.messaging.bo.assist.SubFolder;
import consys.messaging.processing.admin.LoadEntity;
import consys.messaging.processing.messaging.ActualizeEntityUpdate;
import consys.messaging.processing.messaging.LoadFolder;
import consys.messaging.processing.messaging.SendNewMessage;
import consys.messaging.utils.CassandraUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */
public class ConcurentUpdateTest {

    private String UPDATE_UUID = UUID.randomUUID().toString();

    @Test(groups = "cassandra", dependsOnGroups = "system", dependsOnMethods = "testSendThreadMessageCheckUpdates")
    public void preTestConcurentUpdateEntityUpdateColumn() {
        AdminServicesTest.registerEntity(UPDATE_UUID, UPDATE_UUID + "@test.cz");
        Entity entity = new LoadEntity(UPDATE_UUID).run();
        assertEquals("Bad uuid value", UPDATE_UUID, entity.getUuid());
        assertEquals("Bad update value", 0, entity.getUpdate());
    }

    @Test(groups = "cassandra", dependsOnGroups = "system",
    dependsOnMethods = "preTestConcurentUpdateEntityUpdateColumn", threadPoolSize = 4, invocationCount = 10)
    public void testConcurentUpdateEntityUpdateColumn() {
        List<String> list = new ArrayList<String>();
        list.add(UPDATE_UUID);

        for (int i = 0; i < 100; i++) {
            ActualizeEntityUpdate.get().run(list, true);
        }
    }

    @Test(groups = "cassandra", dependsOnGroups = "system", dependsOnMethods = "testConcurentUpdateEntityUpdateColumn")
    public void afterTestConcurentUpdateEntityUpdateColumn() {
        int update = new LoadEntity(UPDATE_UUID).run().getUpdate();
        assertEquals("Bad update value", 10 * 100, update);
    }

    @Test(groups = "cassandra", dependsOnGroups = "system", dependsOnMethods = "afterTestConcurentUpdateEntityUpdateColumn")
    public void loadLimitFolderTest() {
        final int count = 23;

        final String fromuuid = UUID.randomUUID().toString();
        ArrayList<String> tos = SendNewMessagingTest.getTos();
        SendNewMessagingTest.registerEntities(fromuuid, tos);

        List<Long> timestamps = new ArrayList<Long>();

        // odeslani zprav
        for (int i = 0; i < count; i++) {
            final long timestamp = System.currentTimeMillis();
            final String text = "te" + timestamp;
            final String title = "tt" + timestamp;
            new SendNewMessage(SendNewMessagingTest.getMessage(fromuuid, tos, title, text)).run();
            timestamps.add(timestamp);
        }

        Folder folderFull = new LoadFolder(fromuuid, CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();

        assertEquals("Full: Missing some thread", count, folderFull.getTimestamps().size());

        long previousTimestamp = System.currentTimeMillis();
        for (Entry<String, SubFolder> thread : folderFull.getTimestamps().entrySet()) {
            long timestamp = Long.parseLong(thread.getKey());
            if (previousTimestamp < timestamp) {
                fail("Bad folder thread order");
            }
            previousTimestamp = timestamp;
        }

        Folder folderNm1 = new LoadFolder(fromuuid, CassandraUtils.SYSTEM_FOLDER_IN, count,
                timestamps.get(timestamps.size() - 1)).run();

        assertEquals("N-1: Missing some thread", 22, folderNm1.getTimestamps().size());

        previousTimestamp = System.currentTimeMillis();
        for (Entry<String, SubFolder> thread : folderNm1.getTimestamps().entrySet()) {
            long timestamp = Long.parseLong(thread.getKey());
            if (previousTimestamp < timestamp) {
                fail("Bad folder thread order");
            }
            previousTimestamp = timestamp;
        }

        Folder folderNm5 = new LoadFolder(fromuuid, CassandraUtils.SYSTEM_FOLDER_IN, count,
                timestamps.get(timestamps.size() - 5)).run();
        assertEquals("N-5: Missing some thread", 18, folderNm5.getTimestamps().size());
        previousTimestamp = System.currentTimeMillis();
        for (Entry<String, SubFolder> thread : folderNm5.getTimestamps().entrySet()) {
            long timestamp = Long.parseLong(thread.getKey());
            if (previousTimestamp < timestamp) {
                fail("Bad folder thread order");
            }
            previousTimestamp = timestamp;
        }
    }
}
