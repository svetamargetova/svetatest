package consys.messaging.processing;

import consys.messaging.SystemCassandra;
import consys.messaging.SystemManager;
import consys.messaging.bo.Entity;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.processing.admin.BanEntity;
import consys.messaging.processing.admin.LoadEntity;
import consys.messaging.processing.admin.RegisterEntity;
import java.util.UUID;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 * Testy administracnich sluzeb
 * @author pepa
 */
public class AdminServicesTest {

    @BeforeClass
    public void init() {
        //SystemManager.get().setTest();
        try {
            SystemCassandra.get().switchToTest();
        } catch (Exception ex) {
            fail("Neco neproslo pri prepnuti na testovaci keyspace");
        }
    }

    @Test(groups = "cassandra", dependsOnGroups = "system")
    public void testCreateEntity() {
        try {
            String uuid = UUID.randomUUID().toString();
            String email = "myTestMail@my.my";

            Entity entity = createEntity(uuid, email);
            registerEntity(entity);

            Entity entityFromDb = new LoadEntity(uuid).run();

            assertEquals("Uuid not identical", uuid, entityFromDb.getUuid());
            assertEquals("Email not identical", email, entityFromDb.getEmail());
            assertEquals("Ban not identical", false, entityFromDb.isBan());
            assertEquals("Update not identical", 0, entityFromDb.getUpdate());
        } catch (CassandraFailException ex) {
            fail("Fail while read entity " + ex.getMessage());
        } catch (EntityNotFoundException ex) {
            fail("Fail entity not found in db");
        }
    }

    @Test(groups = "cassandra", dependsOnMethods = {"testLoadEntity"}, dependsOnGroups = "system")
    public void testBanEntity() {
        try {
            final String uuid = UUID.randomUUID().toString();
            final String email = "myTestMail@my.my";

            registerEntity(uuid, email);
            Entity entityFromDb = new LoadEntity(uuid).run();
            assertEquals("Ban is set", false, entityFromDb.isBan());

            new BanEntity(uuid, true).run();
            Entity entityFromDbBan = new LoadEntity(uuid).run();
            assertEquals("Ban is not set", true, entityFromDbBan.isBan());

            new BanEntity(uuid, false).run();
            Entity entityFromDbUnban = new LoadEntity(uuid).run();
            assertEquals("Ban is set", false, entityFromDbUnban.isBan());
        } catch (CassandraFailException ex) {
            fail("Fail while read entity");
        } catch (EntityNotFoundException ex) {
            fail("Fail entity not found in db");
        }
    }

    @Test(groups = "cassandra", dependsOnMethods = {"testCreateEntity"}, dependsOnGroups = "system")
    public void testLoadEntity() {
        try {
            final String uuid = UUID.randomUUID().toString();
            final String email = "myTestMail@my.my";

            registerEntity(uuid, email);
            Entity entityFromDb = new LoadEntity(uuid).run();
            assertEquals("Bad uuid", uuid, entityFromDb.getUuid());
            assertEquals("Bad email", email, entityFromDb.getEmail());
            assertEquals("Bad update value", 0, entityFromDb.getUpdate());
            assertEquals("Ban is set", false, entityFromDb.isBan());
        } catch (CassandraFailException ex) {
            fail("Fail while read entity");
        } catch (EntityNotFoundException ex) {
            fail("Fail entity not found in db");
        }
    }

    /** vytvori entitu */
    public static Entity createEntity(final String uuid, final String email) {
        Entity entity = new Entity();
        entity.setUuid(uuid);
        entity.setEmail(email);
        entity.setBan(false);
        entity.setUpdate(0);
        return entity;
    }

    /** zaregistruje entitu */
    public static void registerEntity(final String uuid, final String email) {
        Entity entity = createEntity(uuid, email);
        registerEntity(entity);
    }

    /** zaregistruje entitu */
    public static void registerEntity(final Entity entity) {
        new RegisterEntity(entity).run();
    }
}
