package consys.messaging.processing.mail.send;

import consys.messaging.processing.mail.abst.AbstractMailBuilder;
import consys.messaging.processing.mail.abst.Mailer;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class FakeMail extends AbstractMailBuilder {

    private static Logger log = Logger.getLogger(FakeMail.class);
    public static int MAIL_SENT;

    @Override
    public void send(Mailer m) throws EmailException {
        MAIL_SENT += 1;
        if (MAIL_SENT == 400) {
            log.info("400 --- OK");
        }
    }
}
