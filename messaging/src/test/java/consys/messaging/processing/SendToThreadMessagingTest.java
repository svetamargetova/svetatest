package consys.messaging.processing;

import consys.messaging.SystemManager;
import consys.messaging.bo.Folder;
import consys.messaging.bo.Thread;
import consys.messaging.bo.ThreadAttribute;
import consys.messaging.bo.assist.SubFolder;
import consys.messaging.bo.assist.SubThread;
import consys.messaging.bo.assist.ThreadMessage;
import consys.messaging.exception.CassandraFailException;
import consys.messaging.exception.EntityNotFoundException;
import consys.messaging.exception.FolderNotFoundException;
import consys.messaging.processing.admin.LoadEntity;
import consys.messaging.processing.messaging.LoadFolder;
import consys.messaging.processing.messaging.LoadThread;
import consys.messaging.processing.messaging.LoadThreadAttributes;
import consys.messaging.processing.messaging.SendNewMessage;
import consys.messaging.processing.messaging.SendThreadMessage;
import consys.messaging.utils.CassandraUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */
public class SendToThreadMessagingTest {

    private static final String from = UUID.randomUUID().toString();
    private static final ArrayList<String> tos = SendNewMessagingTest.getTos();
    private static final String sub = UUID.randomUUID().toString();
    private static final String msg1 = UUID.randomUUID().toString();
    private static final String msg2 = UUID.randomUUID().toString();

    @Test(groups = "cassandra", dependsOnMethods = {"testSendNewMessageMark"}, dependsOnGroups = "system")
    public void testSendThreadMessage() {
        try {
            SendNewMessagingTest.registerEntities(from, tos);

            new SendNewMessage(SendNewMessagingTest.getMessage(from, tos, sub, msg1)).run();

            Folder ft1 = new LoadFolder(tos.get(0), CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();
            String tUuid = ft1.getTimestamps().entrySet().iterator().next().getValue().getThread();

            new SendThreadMessage(getThreadMessage(tUuid, tos.get(0), msg2)).run();
        } catch (CassandraFailException ex) {
            fail("Fail while read entity");
        } catch (EntityNotFoundException ex) {
            fail("Fail entity not found in db");
        } catch (FolderNotFoundException ex) {
            fail("Folder not found");
        }
    }

    @Test(groups = "cassandra", dependsOnMethods = {"testSendThreadMessage"}, dependsOnGroups = "system")
    public void testSendThreadMessageCheckUpdates() {
        // kontrola slozek
        Folder ffr = new LoadFolder(from, CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();
        Folder ft1 = new LoadFolder(tos.get(0), CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();
        Folder ft2 = new LoadFolder(tos.get(1), CassandraUtils.SYSTEM_FOLDER_IN, 0, 0).run();

        assertEquals("Bad total count item in folder", 1, ffr.getTotal());
        assertEquals("Bad total count item in folder", 1, ft1.getTotal());
        assertEquals("Bad total count item in folder", 1, ft2.getTotal());

        assertEquals("Bad item count in folder result", 1, ffr.getTimestamps().size());
        assertEquals("Bad item count in folder result", 1, ft1.getTimestamps().size());
        assertEquals("Bad item count in folder result", 1, ft2.getTimestamps().size());

        Entry<String, SubFolder> efr = ffr.getTimestamps().entrySet().iterator().next();
        Entry<String, SubFolder> et1 = ft1.getTimestamps().entrySet().iterator().next();
        Entry<String, SubFolder> et2 = ft2.getTimestamps().entrySet().iterator().next();

        assertEquals("Thread timestamp different", efr.getKey(), et1.getKey());
        assertEquals("Thread timestamp different", et1.getKey(), et2.getKey());

        assertEquals("Bad thread value", efr.getValue().getThread(), et1.getValue().getThread());
        assertEquals("Bad thread value", et1.getValue().getThread(), et2.getValue().getThread());

        assertEquals("Bad thread update value", true, efr.getValue().isUpdate());
        assertEquals("Bad thread update value", false, et1.getValue().isUpdate());
        assertEquals("Bad thread update value", true, et2.getValue().isUpdate());

        // kontrola zpravy
        Thread thread = new LoadThread(et1.getValue().getThread(), tos.get(0), true).run();
        Iterator<Entry<String, SubThread>> ti = thread.getTimestamps().entrySet().iterator();
        SubThread subThread1 = ti.next().getValue();
        assertEquals("Bad message in thread", msg2, subThread1.getText());
        SubThread subThread2 = ti.next().getValue();
        assertEquals("Bad message in thread", msg1, subThread2.getText());

        // kontrola atributu vlakna
        String threadUuid = efr.getValue().getThread();
        ThreadAttribute ta = new LoadThreadAttributes(threadUuid).run();

        assertEquals("Bad last update", efr.getKey(), String.valueOf(ta.getLastTimestamp()));
        assertEquals("Bad author", from, ta.getAuthor());
        assertEquals("Bad subject", sub, ta.getTitle());

        ArrayList<String> receivers = new ArrayList<String>(tos);
        receivers.add(from);
        for (String s : receivers) {
            if (!ta.getReceivers().contains(s)) {
                fail("missing entity in receivers");
            }
        }

        // kontrola hodnot update v entitach
        assertEquals("Bad updated value entity from", 1, new LoadEntity(from).run().getUpdate());
        assertEquals("Bad updated value entity to1", 1, new LoadEntity(tos.get(0)).run().getUpdate());
        assertEquals("Bad updated value entity to2", 2, new LoadEntity(tos.get(1)).run().getUpdate());
    }

    /** thread message */
    public static ThreadMessage getThreadMessage(String thread, String from, String msg) {
        ThreadMessage m = new ThreadMessage();
        m.setThread(thread);
        m.setFrom(from);
        m.setMessage(msg);
        return m;
    }
}
