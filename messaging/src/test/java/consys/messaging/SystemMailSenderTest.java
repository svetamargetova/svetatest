package consys.messaging;

import consys.messaging.bo.Mail;
import consys.messaging.processing.mail.send.FakeMail;
import consys.messaging.processing.mail.send.FakeMailFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SystemMailSenderTest {



    public SystemMailSenderTest() {
        SystemMailSender.get();
        SystemMailSender.get().getMailWorker().setMailFactory(new FakeMailFactory());
    }


    // Group maling frci nad 4 vlaknama takze u kazdeho dame ze ma posle 100 sprav
    @Test(groups = {"mailing"},threadPoolSize=4, invocationCount=4)
    public void test() {

        for (int i = 0; i < 100; i++) {
            Mail m = new Mail();
            m.setFrom("me");
            m.setTo("me");
            m.setMessage("ahoj");
            m.setSubject("Ahoj");
            SystemMailSender.sendMail(m);
        }

    }

}
