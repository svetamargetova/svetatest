package consys.messaging;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */
public class SystemManagerTest {

    private static final String KEYSPACE = "Messaging";
    private static final String KEYSPACE_TEST = "MessagingTest";
    // TODO: predelat testy na EmbeddedCassandraService (cassandra v pameti)

    @BeforeClass
    public void init() {
        SystemManager.get();
        assertEquals("Bad start keyspace", KEYSPACE, SystemManager.get().keyspace());
        //SystemManager.get().setTest();
        try {
            SystemCassandra.get().switchToTest();
        } catch (Exception ex) {
            fail("Neco neproslo pri prepnuti na testovaci keyspace");
        }
        assertEquals("Bad test keyspace", KEYSPACE_TEST, SystemManager.get().keyspace());
    }

    @Test(groups = "system")
    public void testClose() {
        assertEquals("System is close when be open", SystemManager.get().isClosed(), false);
        SystemManager.get().setClosed(true);
        assertEquals("System is open when be close", SystemManager.get().isClosed(), true);
        SystemManager.get().setClosed(false);
        assertEquals("System is close when be open", SystemManager.get().isClosed(), false);
    }
}
