/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class OrderAlreadyPayedException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public OrderAlreadyPayedException(String message) {
	super(message);
    }


    public OrderAlreadyPayedException(String message, Throwable cause) {
        super(message, cause);
    }


    public OrderAlreadyPayedException(Throwable cause) {
        super(cause);
    }



}
