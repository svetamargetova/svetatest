package consys.payment.webservice;

import consys.payment.ws.order.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface OrderWebService {

    public final static String NAMESPACE = "http://takeplace.eu/payment/order";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public CreateOrderResponse createOrder(CreateOrderRequest request);

    public CreateOrderWithProfileResponse createOrderWithProfile(CreateOrderWithProfileRequest request);

    public ConfirmOrderResponse confirmOrder(ConfirmOrderRequest request);

    public CancelOrderResponse cancelOrder(CancelOrderRequest request);

    public CancelOrdersResponse cancelOrders(CancelOrdersRequest request);

    public LoadOrderResponse loadOrder(LoadOrderRequest request);
}
