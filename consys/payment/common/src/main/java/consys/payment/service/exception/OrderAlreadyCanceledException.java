/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class OrderAlreadyCanceledException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public OrderAlreadyCanceledException(String message) {
	super(message);
    }


    public OrderAlreadyCanceledException(String message, Throwable cause) {
        super(message, cause);
    }


    public OrderAlreadyCanceledException(Throwable cause) {
        super(cause);
    }



}
