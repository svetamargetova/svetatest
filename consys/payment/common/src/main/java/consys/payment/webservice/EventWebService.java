package consys.payment.webservice;

import consys.payment.ws.event.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface EventWebService {

    public final static String NAMESPACE = "http://takeplace.eu/payment/event";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public InitializeEventResponse initializeEvent(InitializeEventRequest request);

    public InitializeCloneEventResponse initializeCloneEvent(InitializeCloneEventRequest request);

    public BuyLicenseResponse buyLicense(BuyLicenseRequest request);

    public BuyLicenseWithProfileResponse buyLicenseWithProfile(BuyLicenseWithProfileRequest request);

    public LoadEventPaymentDetailsResponse loadEventPaymentDetails(LoadEventPaymentDetailsRequest request);

    public LoadEventPaymentProfileResponse loadEventPaymentProfile(LoadEventPaymentProfileRequest request);

    public UpdateEventProfileResponse updateEventProfile(UpdateEventProfileRequest request);

    public ListEventProfilesResponse listEventProfiles(ListEventProfilesRequest request);
}
