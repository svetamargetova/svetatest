/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class IbanFormatException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public IbanFormatException(String message) {
	super(message);
    }


    public IbanFormatException(String message, Throwable cause) {
        super(message, cause);
    }


    public IbanFormatException(Throwable cause) {
        super(cause);
    }



}
