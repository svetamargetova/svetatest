/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class NoRecordException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public NoRecordException(String message) {
	super(message);
    }


    public NoRecordException(String message, Throwable cause) {
        super(message, cause);
    }


    public NoRecordException(Throwable cause) {
        super(cause);
    }



}
