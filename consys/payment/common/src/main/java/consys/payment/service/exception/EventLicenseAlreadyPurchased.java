/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class EventLicenseAlreadyPurchased extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public EventLicenseAlreadyPurchased(String message) {
	super(message);
    }


    public EventLicenseAlreadyPurchased(String message, Throwable cause) {
        super(message, cause);
    }


    public EventLicenseAlreadyPurchased(Throwable cause) {
        super(cause);
    }



}
