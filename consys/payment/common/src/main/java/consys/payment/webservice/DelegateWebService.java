package consys.payment.webservice;

import consys.payment.ws.delegate.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface DelegateWebService {

    public final static String NAMESPACE = "http://takeplace.eu/payment/delegate";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public DelegateOrderStateChangedResponse delegateOrderStateChanged(DelegateOrderStateChangedRequest request);
}
