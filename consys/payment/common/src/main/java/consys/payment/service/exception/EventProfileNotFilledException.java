/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class EventProfileNotFilledException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public EventProfileNotFilledException(String message) {
	super(message);
    }


    public EventProfileNotFilledException(String message, Throwable cause) {
        super(message, cause);
    }


    public EventProfileNotFilledException(Throwable cause) {
        super(cause);
    }



}
