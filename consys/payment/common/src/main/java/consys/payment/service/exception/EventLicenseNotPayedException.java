/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class EventLicenseNotPayedException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public EventLicenseNotPayedException(String message) {
	super(message);
    }


    public EventLicenseNotPayedException(String message, Throwable cause) {
        super(message, cause);
    }


    public EventLicenseNotPayedException(Throwable cause) {
        super(cause);
    }



}
