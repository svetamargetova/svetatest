/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class EventAlreadyActiveException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public EventAlreadyActiveException(String message) {
	super(message);
    }


    public EventAlreadyActiveException(String message, Throwable cause) {
        super(message, cause);
    }


    public EventAlreadyActiveException(Throwable cause) {
        super(cause);
    }



}
