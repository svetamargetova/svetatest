/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class ProductAlreadyUsedException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public ProductAlreadyUsedException(String message) {
	super(message);
    }


    public ProductAlreadyUsedException(String message, Throwable cause) {
        super(message, cause);
    }


    public ProductAlreadyUsedException(Throwable cause) {
        super(cause);
    }



}
