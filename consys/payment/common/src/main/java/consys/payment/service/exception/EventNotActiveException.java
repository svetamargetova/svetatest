/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class EventNotActiveException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public EventNotActiveException(String message) {
	super(message);
    }


    public EventNotActiveException(String message, Throwable cause) {
        super(message, cause);
    }


    public EventNotActiveException(Throwable cause) {
        super(cause);
    }



}
