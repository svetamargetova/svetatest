package consys.payment.service.exception;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PaymentException extends Exception{
    private static final long serialVersionUID = -1465770894671709688L;

    public PaymentException(String message) {
	super(message);
    }


    public PaymentException(String message, Throwable cause) {
        super(message, cause);
    }


    public PaymentException(Throwable cause) {
        super(cause);
    }





}
