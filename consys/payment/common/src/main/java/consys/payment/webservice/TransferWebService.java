package consys.payment.webservice;

import consys.payment.ws.transfer.ListTransfersRequest;
import consys.payment.ws.transfer.ListTransfersResponse;
import consys.payment.ws.transfer.LoadTransferRequest;
import consys.payment.ws.transfer.LoadTransferResponse;
import consys.payment.ws.transfer.ObjectFactory;

/**
 * Rozhranie pre pracu s tansferami
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface TransferWebService {

    public final static String NAMESPACE = "http://takeplace.eu/payment/transfer";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    /**
     * Vylistuje vsetky transfere eventu podla datumu
     *
     * @param request
     * @return
     */
    public ListTransfersResponse listEventTransfers(ListTransfersRequest request);

    /**
     * Nacita detail konkretneho transferu. Detail obsahuje naviac vsetky faktury.
     *
     * @param request
     * @return
     */
    public LoadTransferResponse loadEventTransfer(LoadTransferRequest request);


}
