/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class ProductIncompatibilityException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public ProductIncompatibilityException(String message) {
	super(message);
    }


    public ProductIncompatibilityException(String message, Throwable cause) {
        super(message, cause);
    }


    public ProductIncompatibilityException(Throwable cause) {
        super(cause);
    }



}
