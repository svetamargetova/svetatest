package consys.payment.webservice;

import consys.payment.ws.product.CreateEventProductRequest;
import consys.payment.ws.product.CreateEventProductResponse;
import consys.payment.ws.product.CreateEventProductsRequest;
import consys.payment.ws.product.CreateEventProductsResponse;
import consys.payment.ws.product.DeleteEventProductsRequest;
import consys.payment.ws.product.DeleteEventProductsResponse;
import consys.payment.ws.product.LoadLicenseProductRequest;
import consys.payment.ws.product.LoadLicenseProductResponse;
import consys.payment.ws.product.LoadProductRequest;
import consys.payment.ws.product.LoadProductResponse;
import consys.payment.ws.product.ObjectFactory;
import consys.payment.ws.product.UpdateEventProductRequest;
import consys.payment.ws.product.UpdateEventProductResponse;
import consys.payment.ws.product.UpdateEventProductsRequest;
import consys.payment.ws.product.UpdateEventProductsResponse;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ProductWebService {

    public final static String NAMESPACE = "http://takeplace.eu/payment/product";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public CreateEventProductResponse createProduct(CreateEventProductRequest request);

    public CreateEventProductsResponse createProducts(CreateEventProductsRequest request);

    public LoadProductResponse loadProduct(LoadProductRequest request);
    
    public LoadLicenseProductResponse loadLicenseProduct(LoadLicenseProductRequest request);

    public UpdateEventProductResponse updateProduct(UpdateEventProductRequest request);

    public UpdateEventProductsResponse updateProducts(UpdateEventProductsRequest request);

    public DeleteEventProductsResponse deleteEventProducts(DeleteEventProductsRequest request);   
}
