/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class PropertyReadOnlyException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public PropertyReadOnlyException(String message) {
	super(message);
    }


    public PropertyReadOnlyException(String message, Throwable cause) {
        super(message, cause);
    }


    public PropertyReadOnlyException(Throwable cause) {
        super(cause);
    }



}
