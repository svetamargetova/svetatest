package consys.payment.webservice;

import consys.payment.ws.profile.ListUserProfilesRequest;
import consys.payment.ws.profile.ListUserProfilesResponse;
import consys.payment.ws.profile.LoadOrderProfileRequest;
import consys.payment.ws.profile.LoadOrderProfileResponse;
import consys.payment.ws.profile.LoadProfileRequest;
import consys.payment.ws.profile.LoadProfileResponse;
import consys.payment.ws.profile.ObjectFactory;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ProfileWebService {

    public final static String NAMESPACE = "http://takeplace.eu/payment/profile";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    LoadOrderProfileResponse loadOrderProfile(LoadOrderProfileRequest request);

    public LoadProfileResponse loadProfile(LoadProfileRequest request);

    public ListUserProfilesResponse listUserProfiles(ListUserProfilesRequest request);
}
