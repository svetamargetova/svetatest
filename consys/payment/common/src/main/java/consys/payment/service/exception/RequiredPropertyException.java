/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.service.exception;

/**
 *
 * @author palo
 */
public class RequiredPropertyException extends PaymentException{
    private static final long serialVersionUID = -2339875639640029944L;

     public RequiredPropertyException(String message) {
	super(message);
    }


    public RequiredPropertyException(String message, Throwable cause) {
        super(message, cause);
    }


    public RequiredPropertyException(Throwable cause) {
        super(cause);
    }



}
