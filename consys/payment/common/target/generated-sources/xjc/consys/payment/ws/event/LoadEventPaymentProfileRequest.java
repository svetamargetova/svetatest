//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.01 at 04:53:44 PM CET 
//


package consys.payment.ws.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventProfileUuid" type="{http://takeplace.eu/payment/bo}Uuid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "eventProfileUuid"
})
@XmlRootElement(name = "LoadEventPaymentProfileRequest")
public class LoadEventPaymentProfileRequest
    implements Equals, HashCode, ToString
{

    @XmlElement(name = "EventProfileUuid", required = true)
    protected String eventProfileUuid;

    /**
     * Gets the value of the eventProfileUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventProfileUuid() {
        return eventProfileUuid;
    }

    /**
     * Sets the value of the eventProfileUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventProfileUuid(String value) {
        this.eventProfileUuid = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEventProfileUuid;
            theEventProfileUuid = this.getEventProfileUuid();
            strategy.appendField(locator, this, "eventProfileUuid", buffer, theEventProfileUuid);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof LoadEventPaymentProfileRequest)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final LoadEventPaymentProfileRequest that = ((LoadEventPaymentProfileRequest) object);
        {
            String lhsEventProfileUuid;
            lhsEventProfileUuid = this.getEventProfileUuid();
            String rhsEventProfileUuid;
            rhsEventProfileUuid = that.getEventProfileUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventProfileUuid", lhsEventProfileUuid), LocatorUtils.property(thatLocator, "eventProfileUuid", rhsEventProfileUuid), lhsEventProfileUuid, rhsEventProfileUuid)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEventProfileUuid;
            theEventProfileUuid = this.getEventProfileUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventProfileUuid", theEventProfileUuid), currentHashCode, theEventProfileUuid);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
