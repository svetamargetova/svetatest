//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.01 at 04:53:44 PM CET 
//


package consys.payment.ws.delegate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventUuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrderUuid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChangeDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Notify" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ToState" type="{http://takeplace.eu/payment/delegate}State"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "eventUuid",
    "orderUuid",
    "changeDate",
    "notify",
    "toState"
})
@XmlRootElement(name = "DelegateOrderStateChangedRequest")
public class DelegateOrderStateChangedRequest
    implements Equals, HashCode, ToString
{

    @XmlElement(name = "EventUuid", required = true, nillable = true)
    protected String eventUuid;
    @XmlElement(name = "OrderUuid", required = true)
    protected String orderUuid;
    @XmlElement(name = "ChangeDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar changeDate;
    @XmlElement(name = "Notify")
    protected boolean notify;
    @XmlElement(name = "ToState", required = true)
    protected State toState;

    /**
     * Gets the value of the eventUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * Sets the value of the eventUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventUuid(String value) {
        this.eventUuid = value;
    }

    /**
     * Gets the value of the orderUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderUuid() {
        return orderUuid;
    }

    /**
     * Sets the value of the orderUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderUuid(String value) {
        this.orderUuid = value;
    }

    /**
     * Gets the value of the changeDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getChangeDate() {
        return changeDate;
    }

    /**
     * Sets the value of the changeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setChangeDate(XMLGregorianCalendar value) {
        this.changeDate = value;
    }

    /**
     * Gets the value of the notify property.
     * 
     */
    public boolean isNotify() {
        return notify;
    }

    /**
     * Sets the value of the notify property.
     * 
     */
    public void setNotify(boolean value) {
        this.notify = value;
    }

    /**
     * Gets the value of the toState property.
     * 
     * @return
     *     possible object is
     *     {@link State }
     *     
     */
    public State getToState() {
        return toState;
    }

    /**
     * Sets the value of the toState property.
     * 
     * @param value
     *     allowed object is
     *     {@link State }
     *     
     */
    public void setToState(State value) {
        this.toState = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theEventUuid;
            theEventUuid = this.getEventUuid();
            strategy.appendField(locator, this, "eventUuid", buffer, theEventUuid);
        }
        {
            String theOrderUuid;
            theOrderUuid = this.getOrderUuid();
            strategy.appendField(locator, this, "orderUuid", buffer, theOrderUuid);
        }
        {
            XMLGregorianCalendar theChangeDate;
            theChangeDate = this.getChangeDate();
            strategy.appendField(locator, this, "changeDate", buffer, theChangeDate);
        }
        {
            boolean theNotify;
            theNotify = this.isNotify();
            strategy.appendField(locator, this, "notify", buffer, theNotify);
        }
        {
            State theToState;
            theToState = this.getToState();
            strategy.appendField(locator, this, "toState", buffer, theToState);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof DelegateOrderStateChangedRequest)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final DelegateOrderStateChangedRequest that = ((DelegateOrderStateChangedRequest) object);
        {
            String lhsEventUuid;
            lhsEventUuid = this.getEventUuid();
            String rhsEventUuid;
            rhsEventUuid = that.getEventUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventUuid", lhsEventUuid), LocatorUtils.property(thatLocator, "eventUuid", rhsEventUuid), lhsEventUuid, rhsEventUuid)) {
                return false;
            }
        }
        {
            String lhsOrderUuid;
            lhsOrderUuid = this.getOrderUuid();
            String rhsOrderUuid;
            rhsOrderUuid = that.getOrderUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderUuid", lhsOrderUuid), LocatorUtils.property(thatLocator, "orderUuid", rhsOrderUuid), lhsOrderUuid, rhsOrderUuid)) {
                return false;
            }
        }
        {
            XMLGregorianCalendar lhsChangeDate;
            lhsChangeDate = this.getChangeDate();
            XMLGregorianCalendar rhsChangeDate;
            rhsChangeDate = that.getChangeDate();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "changeDate", lhsChangeDate), LocatorUtils.property(thatLocator, "changeDate", rhsChangeDate), lhsChangeDate, rhsChangeDate)) {
                return false;
            }
        }
        {
            boolean lhsNotify;
            lhsNotify = this.isNotify();
            boolean rhsNotify;
            rhsNotify = that.isNotify();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "notify", lhsNotify), LocatorUtils.property(thatLocator, "notify", rhsNotify), lhsNotify, rhsNotify)) {
                return false;
            }
        }
        {
            State lhsToState;
            lhsToState = this.getToState();
            State rhsToState;
            rhsToState = that.getToState();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "toState", lhsToState), LocatorUtils.property(thatLocator, "toState", rhsToState), lhsToState, rhsToState)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theEventUuid;
            theEventUuid = this.getEventUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventUuid", theEventUuid), currentHashCode, theEventUuid);
        }
        {
            String theOrderUuid;
            theOrderUuid = this.getOrderUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderUuid", theOrderUuid), currentHashCode, theOrderUuid);
        }
        {
            XMLGregorianCalendar theChangeDate;
            theChangeDate = this.getChangeDate();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "changeDate", theChangeDate), currentHashCode, theChangeDate);
        }
        {
            boolean theNotify;
            theNotify = this.isNotify();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "notify", theNotify), currentHashCode, theNotify);
        }
        {
            State theToState;
            theToState = this.getToState();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "toState", theToState), currentHashCode, theToState);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
