//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.01 at 04:53:44 PM CET 
//


package consys.payment.ws.order;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CancelOrderReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CancelOrderReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Timeouted"/>
 *     &lt;enumeration value="Owner"/>
 *     &lt;enumeration value="Organizator"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CancelOrderReason")
@XmlEnum
public enum CancelOrderReason {

    @XmlEnumValue("Timeouted")
    TIMEOUTED("Timeouted"),
    @XmlEnumValue("Owner")
    OWNER("Owner"),
    @XmlEnumValue("Organizator")
    ORGANIZATOR("Organizator");
    private final String value;

    CancelOrderReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CancelOrderReason fromValue(String v) {
        for (CancelOrderReason c: CancelOrderReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
