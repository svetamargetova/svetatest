//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.01 at 04:53:44 PM CET 
//


package consys.payment.ws.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderUuid" type="{http://takeplace.eu/payment/bo}Uuid"/>
 *         &lt;element name="EventUuid" type="{http://takeplace.eu/payment/bo}Uuid"/>
 *         &lt;element name="UserUuid" type="{http://takeplace.eu/payment/bo}Uuid"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderUuid",
    "eventUuid",
    "userUuid",
    "note"
})
@XmlRootElement(name = "ConfirmOrderRequest")
public class ConfirmOrderRequest
    implements Equals, HashCode, ToString
{

    @XmlElement(name = "OrderUuid", required = true)
    protected String orderUuid;
    @XmlElement(name = "EventUuid", required = true)
    protected String eventUuid;
    @XmlElement(name = "UserUuid", required = true)
    protected String userUuid;
    @XmlElement(name = "Note", required = true)
    protected String note;

    /**
     * Gets the value of the orderUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderUuid() {
        return orderUuid;
    }

    /**
     * Sets the value of the orderUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderUuid(String value) {
        this.orderUuid = value;
    }

    /**
     * Gets the value of the eventUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * Sets the value of the eventUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventUuid(String value) {
        this.eventUuid = value;
    }

    /**
     * Gets the value of the userUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserUuid() {
        return userUuid;
    }

    /**
     * Sets the value of the userUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserUuid(String value) {
        this.userUuid = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theOrderUuid;
            theOrderUuid = this.getOrderUuid();
            strategy.appendField(locator, this, "orderUuid", buffer, theOrderUuid);
        }
        {
            String theEventUuid;
            theEventUuid = this.getEventUuid();
            strategy.appendField(locator, this, "eventUuid", buffer, theEventUuid);
        }
        {
            String theUserUuid;
            theUserUuid = this.getUserUuid();
            strategy.appendField(locator, this, "userUuid", buffer, theUserUuid);
        }
        {
            String theNote;
            theNote = this.getNote();
            strategy.appendField(locator, this, "note", buffer, theNote);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof ConfirmOrderRequest)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final ConfirmOrderRequest that = ((ConfirmOrderRequest) object);
        {
            String lhsOrderUuid;
            lhsOrderUuid = this.getOrderUuid();
            String rhsOrderUuid;
            rhsOrderUuid = that.getOrderUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "orderUuid", lhsOrderUuid), LocatorUtils.property(thatLocator, "orderUuid", rhsOrderUuid), lhsOrderUuid, rhsOrderUuid)) {
                return false;
            }
        }
        {
            String lhsEventUuid;
            lhsEventUuid = this.getEventUuid();
            String rhsEventUuid;
            rhsEventUuid = that.getEventUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventUuid", lhsEventUuid), LocatorUtils.property(thatLocator, "eventUuid", rhsEventUuid), lhsEventUuid, rhsEventUuid)) {
                return false;
            }
        }
        {
            String lhsUserUuid;
            lhsUserUuid = this.getUserUuid();
            String rhsUserUuid;
            rhsUserUuid = that.getUserUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "userUuid", lhsUserUuid), LocatorUtils.property(thatLocator, "userUuid", rhsUserUuid), lhsUserUuid, rhsUserUuid)) {
                return false;
            }
        }
        {
            String lhsNote;
            lhsNote = this.getNote();
            String rhsNote;
            rhsNote = that.getNote();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "note", lhsNote), LocatorUtils.property(thatLocator, "note", rhsNote), lhsNote, rhsNote)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theOrderUuid;
            theOrderUuid = this.getOrderUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "orderUuid", theOrderUuid), currentHashCode, theOrderUuid);
        }
        {
            String theEventUuid;
            theEventUuid = this.getEventUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventUuid", theEventUuid), currentHashCode, theEventUuid);
        }
        {
            String theUserUuid;
            theUserUuid = this.getUserUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "userUuid", theUserUuid), currentHashCode, theUserUuid);
        }
        {
            String theNote;
            theNote = this.getNote();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "note", theNote), currentHashCode, theNote);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
