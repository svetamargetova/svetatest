//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.01 at 04:53:44 PM CET 
//


package consys.payment.ws.event;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentProfileExceptionEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PaymentProfileExceptionEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NoRecordException"/>
 *     &lt;enumeration value="IbanFormatException"/>
 *     &lt;enumeration value="RequiredPropertyException"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PaymentProfileExceptionEnum")
@XmlEnum
public enum PaymentProfileExceptionEnum {

    @XmlEnumValue("NoRecordException")
    NO_RECORD_EXCEPTION("NoRecordException"),
    @XmlEnumValue("IbanFormatException")
    IBAN_FORMAT_EXCEPTION("IbanFormatException"),
    @XmlEnumValue("RequiredPropertyException")
    REQUIRED_PROPERTY_EXCEPTION("RequiredPropertyException");
    private final String value;

    PaymentProfileExceptionEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PaymentProfileExceptionEnum fromValue(String v) {
        for (PaymentProfileExceptionEnum c: PaymentProfileExceptionEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
