/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 *
 * @author palo
 */
public final class BigDecimalUtils {

    public static final BigDecimal HUNDRED = new BigDecimal(100);
    public static final MathContext DEFAULT = new MathContext(8, RoundingMode.HALF_UP);

    /**
     * Vypocita kolko je percentualna case z base.; <br/> base=1000<br/>
     * percent=25<br/> return=250<br/>
     *
     * @param base
     * @param percent
     * @return
     */
    public static BigDecimal countPercent(BigDecimal base, BigDecimal percent) {
        return base.multiply(percent).divide(HUNDRED).setScale(4, RoundingMode.HALF_UP);
    }

    /**
     * Vypocita kolko je cast base bez percent. <br/> base=1000<br/>
     * percent=25<br/> return=750<br/>
     *
     * @param base
     * @param percent
     * @return
     */
    public static BigDecimal countPercentage(BigDecimal base, BigDecimal percent) {
        return base.subtract(countPercent(base, percent)).setScale(4, RoundingMode.HALF_UP);
    }

    public static BigDecimal add(BigDecimal one, BigDecimal two) {
        return one.add(two, DEFAULT);
    }

    public static BigDecimal computeVatCoefficient(BigDecimal vatRate) {
        return vatRate.divide(BigDecimalUtils.HUNDRED.add(vatRate), 4, RoundingMode.HALF_UP);
    }
}
