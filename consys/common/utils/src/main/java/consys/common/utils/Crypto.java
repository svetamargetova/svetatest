package consys.common.utils;

import java.security.MessageDigest;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;


/**
 *
 * @author Palo
 */
public class Crypto {

    /** vytvori SHA-1 hash
     * @return hash pokud je vse v poradku, jinak vraci null
     */
    public static String sha1(String text) {
        try {
            byte[] digest = new byte[40];
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(text.getBytes("UTF-8"), 0, text.length());
            digest = md.digest();
            return convertToHex(digest);
        } catch (Exception ex) {
            return null;
        }
    }

    /** vytvori MD5 hash
     * @return hash pokud je vse v poradku, jinak vraci null
     */
    public static String md5(String text) {
        try {
            Md5PasswordEncoder enc = new Md5PasswordEncoder();
            return enc.encodePassword(text, null);
        } catch (Exception ex) {
            return null;
        }
    }

    /** pomocna metoda pro prevod pole bytu do hex stringu */
    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }
}
