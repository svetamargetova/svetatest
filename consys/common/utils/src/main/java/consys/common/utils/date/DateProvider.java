package consys.common.utils.date;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.FastDateFormat;

/**
 *
 * @author Palci
 */
public class DateProvider {

    private static final Long DAY_IN_MILISECONDS = 86400000L;
    public static final FastDateFormat EUROPE_FORMAT =
            FastDateFormat.getInstance("HH:mm:ss - dd.MM.yyyy");
    public static final FastDateFormat US_FORMAT =
            FastDateFormat.getInstance("HH:mm:ss - MM/dd/yy");

    /**
     * Odcita z calendara pocet dni
     */
    public static synchronized void substractDays(Calendar c, int days) {
        long milis = c.getTimeInMillis();
        c.setTimeInMillis(milis - (days * DAY_IN_MILISECONDS));
    }

    /**
     * Cas
     * - 1 sekundu kvoli integracii s databazov
     * ! Databaza a tomcat musia mat rovnaky cas !
     */
    public static synchronized Date getCurrentDate() {
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.SECOND, -1);
        return cal.getTime();
    }

    /**
     * Cas naformatovany do naseho formatu
     */
    public static synchronized String getCurrentFormatedDate() {
        return DateFormatUtils.ISO_DATETIME_FORMAT.format(getCurrentDate());
    }

    /** vraci aktualni casove razitko */
    public static synchronized Timestamp getTimeStamp() {
        return new Timestamp(getCurrentDate().getTime());
    }

    public static synchronized String formatEuropeDate(Date d) {
        return d == null ? "×" : EUROPE_FORMAT.format(d);
    }

    public static synchronized String formatUsDate(Date d) {
        return d == null ? "×" : US_FORMAT.format(d);
    }

    /** vraci pocet dni mezi kalendari */
    public static synchronized int daysBetween(Calendar c1, Calendar c2) {
        if (c1.before(c2)) {
            Calendar c = c1;
            c1 = c2;
            c2 = c;
        }
        return (int) ((c1.getTime().getTime() - c2.getTime().getTime()) / DAY_IN_MILISECONDS);
    }

    /** vraci datum posledniho dne predchoziho mesice od data v parametru */
    public static synchronized Date previousMonthLastDayDate(Date now) {
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(now);
        c.add(Calendar.MONTH, -1);
        int lastDay = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        c.set(Calendar.DAY_OF_MONTH, lastDay);
        return c.getTime();
    }
}
