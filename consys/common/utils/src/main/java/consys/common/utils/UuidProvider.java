package consys.common.utils;

import java.util.UUID;

/**
 *
 * @author Palo
 */
public class UuidProvider {

    /** Returns val represented by the specified number of hex digits. */
    private static String digits(long val, int digits) {
        long hi = 1L << (digits * 4);
        return Long.toHexString(hi | (val & (hi - 1))).substring(1);
    }

    /**
     * To string je uprava originalej UUID.toString ale bez pridavania pomlciek.
     */
    public static String getUuid() {
        UUID u = UUID.randomUUID();
        return String.format("%s%s%s%s%s",
                digits(u.getMostSignificantBits() >> 32, 8),
                digits(u.getMostSignificantBits() >> 16, 4),
                digits(u.getMostSignificantBits(), 4),
                digits(u.getLeastSignificantBits() >> 48, 4),
                digits(u.getLeastSignificantBits(), 12));
    }
}
