package consys.common.utils.date;

import java.util.Date;
import org.apache.commons.lang.time.FastDateFormat;

/**
 *
 * Objekt rozsahu datumu.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DateRange {

    private static final FastDateFormat FORMAT = FastDateFormat.getInstance("dd.MM.yyyy hh:mm");
    public Date start;
    public Date end;

    public DateRange(Date start, Date end) {
        this.start = start;
        this.end = end;

        if (start.after(end)) {
            throw new IllegalArgumentException("Start date " + start + " after end " + end);
        }
    }

    /**
     * Zisti prienik intervalov casovych mnozin pouzitim oboch uzatvorenych intervalov
     * <a,b> <c,d> return true
     *
     */
    public boolean intersectsClosedInterval(DateRange dr) {
        return intersects(dr, true, true);
    }
    
    
    
    
    // START < R1 < END
    private static boolean isStartInside(Date R1, Date start, Date end) {
        return R1.after(start) && R1.before(end);
    }
    
    
    
    // START < R1 <= END
    private static boolean isStartInsideEquals(Date R1, Date start, Date end) {        
        return DateUtils.isSameDay(R1, end) ? false : isStartInside(R1, start, end);
    }

    // START < R2 < END
    private static boolean isEndInside(Date R2, Date start, Date end) {
        return R2.after(start) && R2.before(end);
    }
    
    // START <= R2 < END
    private static boolean isEndInsideEquals(Date R2, Date start, Date end) {
        return DateUtils.isSameDay(R2, start) ? false : R2.after(start) && R2.before(end);
    }

    private static boolean isInside(Date R1, Date R2, Date start, Date end) {
        return R1.before(start) && R2.after(end);
    }

    /**
     * Zisti prienik interval casovych mnozin. Vlastnosti prieniku sa nastavuju
     * cez parameter:
     * <ol>
     * <li> <code>lClosed</code> - lavy interval uzatvoreny ak <code>true</code> inak interval otvoreny. 
     * <li> <code>rClosed</code> - pravy interval uzatvoreny ak <code>true</code> inak interval otvoreny
     * </ol>
     * 
     * <BR>
     * PRIKLAD:
     * true, false => < X , Y )
     * 
     * @param dr
     * @return 
     */
    public boolean intersects(DateRange dr, boolean lClosed, boolean rClosed) {
        Date R1 = dr.start;
        Date R2 = dr.end;


        // ak je jeden interval v druhom.
        // R1 < START < END < R2
        if (isInside(R1, R2, start, end) || isStartInside(R1, start, end) || isEndInside(R2, start, end)) {
            return true;
        }

        
        if (lClosed && rClosed) {
            // ak je interval uzatvoreny znamena ze nesmu byt v sebe .. zakladna podmienka
            if (eq(R1,end) || eq(R2,start)) {
                return true;
            }            
        } 



        return false;
    }
    
    private static final boolean eq(Date d1, Date d2){
        return d1.getTime()== d2.getTime();
    }

    @Override
    public String toString() {
        return String.format("DateRange[%s - %s]", FORMAT.format(start), FORMAT.format(end));
    }
}
