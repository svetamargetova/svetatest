package consys.common.utils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class AsciiUtils {

    /**
     * Converts characters above ASCII to their ASCII equivalents.  For example,
     * accents are removed from accented characters.
     * @param input The string to fold
     * @param length The number of characters in the input string
     */
    public static final String foldToASCII(char[] input, int length) {

        char[] output = new char[length];
        int outputPos = 0;
        for (int pos = 0; pos < length; ++pos) {
            final char c = input[pos];
            
            if ((c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A')) {
                output[outputPos++] = c;
            } else {
                output[outputPos++] = 'x';
            }
        }
        return new String(output);
    }
}

