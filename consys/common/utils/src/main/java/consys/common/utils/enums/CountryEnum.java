package consys.common.utils.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public enum CountryEnum {

    AFGHANISTAN(1, "AF"),
    ALAND_ISLANDS(2, "AX"),
    ALBANIA(3, "AL"),
    ALGERIA(4, "DZ"),
    AMERICAN_SAMOA(5, "AS"),
    ANDORRA(6, "AD"),
    ANGOLA(7, "AO"),
    ANGUILLA(8, "AI"),
    ANTARCTICA(9, "AQ"),
    ANTIGUA_AND_BARBUDA(10, "AG"),
    ARGENTINA(11, "AR"),
    ARMENIA(12, "AM"),
    ARUBA(13, "AW"),
    AUSTRALIA(14, "AU"),
    AUSTRIA(15, "AT"),
    AZERBAIJAN(16, "AZ"),
    BAHAMAS(17, "BS"),
    BAHRAIN(18, "BH"),
    BANGLADESH(19, "BD"),
    BARBADOS(20, "BB"),
    BELARUS(21, "BY"),
    BELGIUM(22, "BE"),
    BELIZE(23, "BZ"),
    BENIN(24, "BJ"),
    BERMUDA(25, "BM"),
    BHUTAN(26, "BT"),
    BOLIVIA_PLURINATIONAL_STATE_OF(27, "BO"),
    BONAIRE_SAINT_EUSTATIUS_AND_SABA(28, "BQ"),
    BOSNIA_AND_HERZEGOVINA(29, "BA"),
    BOTSWANA(30, "BW"),
    BOUVET_ISLAND(31, "BV"),
    BRAZIL(32, "BR"),
    BRITISH_INDIAN_OCEAN_TERRITORY(33, "IO"),
    BRUNEI_DARUSSALAM(34, "BN"),
    BULGARIA(35, "BG"),
    BURKINA_FASO(36, "BF"),
    BURUNDI(37, "BI"),
    CAMBODIA(38, "KH"),
    CAMEROON(39, "CM"),
    CANADA(40, "CA"),
    CAPE_VERDE(41, "CV"),
    CAYMAN_ISLANDS(42, "KY"),
    CENTRAL_AFRICAN_REPUBLIC(43, "CF"),
    CHAD(44, "TD"),
    CHILE(45, "CL"),
    CHINA(46, "CN"),
    CHRISTMAS_ISLAND(47, "CX"),
    COCOS_KEELING_ISLANDS(48, "CC"),
    COLOMBIA(49, "CO"),
    COMOROS(50, "KM"),
    CONGO(51, "CG"),
    CONGO_THE_DEMOCRATIC_REPUBLIC_OF_THE(52, "CD"),
    COOK_ISLANDS(53, "CK"),
    COSTA_RICA(54, "CR"),
    CUTE_DIVOIRE(55, "CI"),
    CROATIA(56, "HR"),
    CURACAO(57, "CW"),
    CUBA(58, "CU"),
    CYPRUS(59, "CY"),
    CZECH_REPUBLIC(60, "CZ"),
    DENMARK(61, "DK"),
    DJIBOUTI(62, "DJ"),
    DOMINICA(63, "DM"),
    DOMINICAN_REPUBLIC(64, "DO"),
    ECUADOR(65, "EC"),
    EGYPT(66, "EG"),
    EL_SALVADOR(67, "SV"),
    EQUATORIAL_GUINEA(68, "GQ"),
    ERITREA(69, "ER"),
    ESTONIA(70, "EE"),
    ETHIOPIA(71, "ET"),
    FALKLAND_ISLANDS_MALVINAS(72, "FK"),
    FAROE_ISLANDS(73, "FO"),
    FIJI(74, "FJ"),
    FINLAND(75, "FI"),
    FRANCE(76, "FR"),
    FRENCH_GUIANA(77, "GF"),
    FRENCH_POLYNESIA(78, "PF"),
    FRENCH_SOUTHERN_TERRITORIES(79, "TF"),
    GABON(80, "GA"),
    GAMBIA(81, "GM"),
    GEORGIA(82, "GE"),
    GERMANY(83, "DE"),
    GHANA(84, "GH"),
    GIBRALTAR(85, "GI"),
    GREECE(86, "GR"),
    GREENLAND(87, "GL"),
    GRENADA(88, "GD"),
    GUADELOUPE(89, "GP"),
    GUAM(90, "GU"),
    GUATEMALA(91, "GT"),
    GUERNSEY(92, "GG"),
    GUINEA(93, "GN"),
    GUINEA_BISSAU(94, "GW"),
    GUYANA(95, "GY"),
    HAITI(96, "HT"),
    HEARD_ISLAND_AND_MCDONALD_ISLANDS(97, "HM"),
    HOLY_SEE_VATICAN_CITY_STATE(98, "VA"),
    HONDURAS(99, "HN"),
    HONG_KONG(100, "HK"),
    HUNGARY(101, "HU"),
    ICELAND(102, "IS"),
    INDIA(103, "IN"),
    INDONESIA(104, "ID"),
    IRAN_ISLAMIC_REPUBLIC_OF(105, "IR"),
    IRAQ(106, "IQ"),
    IRELAND(107, "IE"),
    ISLE_OF_MAN(108, "IM"),
    ISRAEL(109, "IL"),
    ITALY(110, "IT"),
    JAMAICA(111, "JM"),
    JAPAN(112, "JP"),
    JERSEY(113, "JE"),
    JORDAN(114, "JO"),
    KAZAKHSTAN(115, "KZ"),
    KENYA(116, "KE"),
    KIRIBATI(117, "KI"),
    KOREA_DEMOCRATIC_PEOPLES_REPUBLIC_OF(118, "KP"),
    KOREA_REPUBLIC_OF(119, "KR"),
    KUWAIT(120, "KW"),
    KYRGYZSTAN(121, "KG"),
    LAO_PEOPLES_DEMOCRATIC_REPUBLIC(122, "LA"),
    LATVIA(123, "LV"),
    LEBANON(124, "LB"),
    LESOTHO(125, "LS"),
    LIBERIA(126, "LR"),
    LIBYAN_ARAB_JAMAHIRIYA(127, "LY"),
    LIECHTENSTEIN(128, "LI"),
    LITHUANIA(129, "LT"),
    LUXEMBOURG(130, "LU"),
    MACAO(131, "MO"),
    MACEDONIA_THE_FORMER_YUGOSLAV_REPUBLIC_OF(132, "MK"),
    MADAGASCAR(133, "MG"),
    MALAWI(134, "MW"),
    MALAYSIA(135, "MY"),
    MALDIVES(136, "MV"),
    MALI(137, "ML"),
    MALTA(138, "MT"),
    MARSHALL_ISLANDS(139, "MH"),
    MARTINIQUE(140, "MQ"),
    MAURITANIA(141, "MR"),
    MAURITIUS(142, "MU"),
    MAYOTTE(143, "YT"),
    MEXICO(144, "MX"),
    MICRONESIA_FEDERATED_STATES_OF(145, "FM"),
    MOLDOVA_REPUBLIC_OF(146, "MD"),
    MONACO(147, "MC"),
    MONGOLIA(148, "MN"),
    MONTENEGRO(149, "ME"),
    MONTSERRAT(150, "MS"),
    MOROCCO(151, "MA"),
    MOZAMBIQUE(152, "MZ"),
    MYANMAR(153, "MM"),
    NAMIBIA(154, "NA"),
    NAURU(155, "NR"),
    NEPAL(156, "NP"),
    NETHERLANDS(157, "NL"),
    NEW_CALEDONIA(158, "NC"),
    NEW_ZEALAND(159, "NZ"),
    NICARAGUA(160, "NI"),
    NIGER(161, "NE"),
    NIGERIA(162, "NG"),
    NIUE(163, "NU"),
    NORFOLK_ISLAND(164, "NF"),
    NORTHERN_MARIANA_ISLANDS(165, "MP"),
    NORWAY(166, "NO"),
    OMAN(167, "OM"),
    PAKISTAN(168, "PK"),
    PALAU(169, "PW"),
    PALESTINIAN_TERRITORY_OCCUPIED(170, "PS"),
    PANAMA(171, "PA"),
    PAPUA_NEW_GUINEA(172, "PG"),
    PARAGUAY(173, "PY"),
    PERU(174, "PE"),
    PHILIPPINES(175, "PH"),
    PITCAIRN(176, "PN"),
    POLAND(177, "PL"),
    PORTUGAL(178, "PT"),
    PUERTO_RICO(179, "PR"),
    QATAR(180, "QA"),
    REUNION(181, "RE"),
    ROMANIA(182, "RO"),
    RUSSIAN_FEDERATION(183, "RU"),
    RWANDA(184, "RW"),
    SAINT_BARTHELEMY(185, "BL"),
    SAINT_HELENA_ASCENSION_AND_TRISTAN_DA_CUNHA(186, "SH"),
    SAINT_KITTS_AND_NEVIS(187, "KN"),
    SAINT_LUCIA(188, "LC"),
    SAINT_MARTIN_FRENCH_PART(189, "MF"),
    SAINT_PIERRE_AND_MIQUELON(190, "PM"),
    SAINT_VINCENT_AND_THE_GRENADINES(191, "VC"),
    SAMOA(192, "WS"),
    SAN_MARINO(193, "SM"),
    SAO_TOME_AND_PRINCIPE(194, "ST"),
    SAUDI_ARABIA(195, "SA"),
    SENEGAL(196, "SN"),
    SERBIA(197, "RS"),
    SEYCHELLES(198, "SC"),
    SIERRA_LEONE(199, "SL"),
    SINGAPORE(200, "SG"),
    SINT_MAARTEN_DUTCH_PART(201, "SX"),
    SLOVAKIA(202, "SK"),
    SLOVENIA(203, "SI"),
    SOLOMON_ISLANDS(204, "SB"),
    SOMALIA(205, "SO"),
    SOUTH_AFRICA(206, "ZA"),
    SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS(207, "GS"),
    SPAIN(208, "ES"),
    SRI_LANKA(209, "LK"),
    SUDAN(210, "SD"),
    SURINAME(211, "SR"),
    SVALBARD_AND_JAN_MAYEN(212, "SJ"),
    SWAZILAND(213, "SZ"),
    SWEDEN(214, "SE"),
    SWITZERLAND(215, "CH"),
    SYRIAN_ARAB_REPUBLIC(216, "SY"),
    TAIWAN_PROVINCE_OF_CHINA(217, "TW"),
    TAJIKISTAN(218, "TJ"),
    TANZANIA_UNITED_REPUBLIC_OF(219, "TZ"),
    THAILAND(220, "TH"),
    TIMOR_LESTE(221, "TL"),
    TOGO(222, "TG"),
    TOKELAU(223, "TK"),
    TONGA(224, "TO"),
    TRINIDAD_AND_TOBAGO(225, "TT"),
    TUNISIA(226, "TN"),
    TURKEY(227, "TR"),
    TURKMENISTAN(228, "TM"),
    TURKS_AND_CAICOS_ISLANDS(229, "TC"),
    TUVALU(230, "TV"),
    UGANDA(231, "UG"),
    UKRAINE(232, "UA"),
    UNITED_ARAB_EMIRATES(233, "AE"),
    UNITED_KINGDOM(234, "GB"),
    UNITED_STATES(235, "US"),
    UNITED_STATES_MINOR_OUTLYING_ISLANDS(236, "UM"),
    URUGUAY(237, "UY"),
    UZBEKISTAN(238, "UZ"),
    VANUATU(239, "VU"),
    VENEZUELA_BOLIVARIAN_REPUBLIC_OF(240, "VE"),
    VIETNAM(241, "VN"),
    VIRGIN_ISLANDS_BRITISH(242, "VG"),
    VIRGIN_ISLANDS_US(243, "VI"),
    WALLIS_AND_FUTUNA(244, "WF"),
    WESTERN_SAHARA(245, "EH"),
    YEMEN(246, "YE"),
    ZAMBIA(247, "ZM"),
    ZIMBABWE(248, "ZW");
    // konstanty
    private static final String DEFAULT_PATH = "Country";
    private static final ResourceBundle NAMES = ResourceBundle.getBundle(DEFAULT_PATH);
    private static final int MAX_COUNTRY_ID = 248;
    private static final Map<String, String> names;
    private static final Map<String, Integer> idByCode;
    // data
    int id;
    String code;

    static {
        // nainicializujeme na zacatku do pamata
        names = new HashMap<String, String>();
        idByCode = new HashMap<String, Integer>();
        CountryEnum[] vals = values();
        for (int i = 0; i < vals.length; i++) {
            CountryEnum country = vals[i];
            String name = NAMES.getString(country.code + ".name");
            names.put(country.code, name);
            idByCode.put(country.code, country.id);
        }

    }

    CountryEnum(int id, String code) {
        this.id = id;
        this.code = code;
    }

    public static CountryEnum fromId(Integer id) {
        if (id == null || id < 1) {
            return null;
        }

        if (id > MAX_COUNTRY_ID) {
            return AFGHANISTAN;
        }

        return CountryEnum.values()[id - 1];
    }

    /** vraci id country podle kodu (napr.: CZ -> 60) */
    public static Integer fromCode(String code) {
        return idByCode.get(code);
    }

    public Integer getId() {
        return id;
    }

    /** nacita lokalizovane meno z defualtneho anglickeho prop. */
    public String getName() {
        return names.get(code);
    }

    public String getCode() {
        return code;
    }
}
