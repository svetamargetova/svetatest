package consys.common.utils.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;


public enum UsStateEnum {

    ALABAMA(1, "AL"),
    ALASKA(2, "AK"),
    AMERICAN_SAMOA(3, "AS"),
    ARIZONA(4, "AZ"),
    ARKANSAS(5, "AR"),
    CALIFORNIA(6, "CA"),
    COLORADO(7, "CO"),
    CONNECTICUT(8, "CT"),
    DELAWARE(9, "DE"),
    DISTRICT_OF_COLUMBIA(10, "DC"),
    FEDERATED_STATES_OF_MICRONESIA(11, "FM"),
    FLORIDA(12, "FL"),
    GEORGIA(13, "GA"),
    GUAM(14, "GU"),
    HAWAII(15, "HI"),
    IDAHO(16, "ID"),
    ILLINOIS(17, "IL"),
    INDIANA(18, "IN"),
    IOWA(19, "IA"),
    KANSAS(20, "KS"),
    KENTUCKY(21, "KY"),
    LOUISIANA(22, "LA"),
    MAINE(23, "ME"),
    MARSHALL_ISLANDS(24, "MH"),
    MARYLAND(25, "MD"),
    MASSACHUSETTS(26, "MA"),
    MICHIGAN(27, "MI"),
    MINNESOTA(28, "MN"),
    MISSISSIPPI(29, "MS"),
    MISSOURI(30, "MO"),
    MONTANA(31, "MT"),
    NEBRASKA(32, "NE"),
    NEVADA(33, "NV"),
    NEW_HAMPSHIRE(34, "NH"),
    NEW_JERSEY(35, "NJ"),
    NEW_MEXICO(36, "NM"),
    NEW_YORK(37, "NY"),
    NORTH_CAROLINA(38, "NC"),
    NORTH_DAKOTA(39, "ND"),
    NORTHERN_MARIANA_ISLANDS(40, "MP"),
    OHIO(41, "OH"),
    OKLAHOMA(42, "OK"),
    OREGON(43, "OR"),
    PALAU(44, "PW"),
    PENNSYLVANIA(45, "PA"),
    PUERTO_RICO(46, "PR"),
    RHODE_ISLAND(47, "RI"),
    SOUTH_CAROLINA(48, "SC"),
    SOUTH_DAKOTA(49, "SD"),
    TENNESSEE(50, "TN"),
    TEXAS(51, "TX"),
    UTAH(52, "UT"),
    VERMONT(53, "VT"),
    VIRGIN_ISLANDS(54, "VI"),
    VIRGINIA(55, "VA"),
    WASHINGTON(56, "WA"),
    WEST_VIRGINIA(57, "WV"),
    WISCONSIN(58, "WI"),
    WYOMING(59,"WY");

    int id;
    String code;

    private static final String DEFAULT_PATH = "UsState";
    private static final ResourceBundle NAMES = ResourceBundle.getBundle(DEFAULT_PATH);

    private static final Map<String,String> names;

    static{
        // nainicializujeme na zacatku do pamata
        names = new HashMap<String, String>();
        UsStateEnum[] vals = values();
        for (int i = 0; i < vals.length; i++) {
            UsStateEnum state = vals[i];
            String name = NAMES.getString(state.code+".name");
            names.put(state.code, name);
        }
    }



    UsStateEnum(int id, String code) {
        this.id = id;
        this.code = code;
    }


    public Integer getId(){
        return id;
    }

    private static final int MAX_STATE_ID = 59;
    public static UsStateEnum fromId(Integer id){
        if(id == null || id < 1){
            return null;
        }

        if(id > MAX_STATE_ID){
            return ALABAMA;
        }

        return UsStateEnum.values()[id-1];
    }


    // nacita lokalizovane meno z defualtneho anglickeho prop.
    public String getName(){        
        return names.get(code);
    }
}
