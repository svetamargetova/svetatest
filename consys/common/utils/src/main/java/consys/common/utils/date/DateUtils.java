package consys.common.utils.date;

import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DateUtils extends org.apache.commons.lang.time.DateUtils {

    public static boolean isDateInRange(Date date, Date start, Date end) {
        if (date.compareTo(start) >= 0 && date.compareTo(end) < 0) {
            return true;
        }
        return false;
    }
}
