package consys.common.utils.enums;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum Currency {
     CZK, EUR,USD;

    public Integer toId() {
        switch (this) {
            case CZK:
                return 1;
            case EUR:
                return 2;
            case USD:
                return 3;
            default:
                throw new IllegalArgumentException("Type " + this.name() + " has not ID mapping");
        }
    }

    public static Currency fromId(Integer i) {        
        switch (i) {
            case 1:
                return CZK;
            case 2:
                return EUR;
            case 3:
                return USD;
            default:
                return null;
        }
    }




}
