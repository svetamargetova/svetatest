/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.utils.date;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author palo
 */
public class DateUtilsTest {

    SimpleDateFormat f = new SimpleDateFormat("dd.MM.yy hh:mm");

    @Test(groups = {"date"})
    public void dateRangeTest() throws ParseException {

        Date rangeStart = f.parse("01.06.11 12:00");
        Date rangeEnd = f.parse("01.06.11 13:00");
        Date testDate1 = f.parse("01.06.11 12:00");
        Date testDate2 = f.parse("01.06.11 12:30");
        Date testDate3 = f.parse("01.06.11 13:00");

        // test na uzatvorene intervale <a,b> <c,d>
        assertTrue(DateUtils.isDateInRange(testDate1, rangeStart, rangeEnd));
        assertTrue(DateUtils.isDateInRange(testDate2, rangeStart, rangeEnd));
        assertFalse(DateUtils.isDateInRange(testDate3, rangeStart, rangeEnd));
        
    }
}
