/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.testng.annotations.Test;
import static  org.testng.Assert.*;

/**
 *
 * @author palo
 */
public class DateRangeTest {
    
    SimpleDateFormat f = new SimpleDateFormat("dd.MM.yy hh:mm");
    
    @Test(groups={"date"})
    public void intersectionTests() throws ParseException{
    
        DateRange r1 = new DateRange(f.parse("01.06.11 12:00"), f.parse("01.07.11 12:00"));
        DateRange r1s = new DateRange(f.parse("25.06.11 12:00"), f.parse("01.07.11 12:00"));
        DateRange r1e = new DateRange(f.parse("01.05.11 12:00"), f.parse("05.06.11 12:00"));
        
        
        DateRange r2 = new DateRange(f.parse("01.07.11 12:00"), f.parse("01.08.11 12:00"));
        
        // test na uzatvorene intervale <a,b> <c,d>
        assertTrue(r1.intersectsClosedInterval(r2));
        assertTrue(r1.intersectsClosedInterval(r1s));
        assertTrue(r1.intersectsClosedInterval(r1e));
        
        // tesnt na pravy otvoreny ->  <a,b) <c,d)
        assertFalse(r1.intersects(r2, true, false));
        assertTrue(r1.intersects(r1s, true, false));
        assertTrue(r1.intersects(r1e, true, false));
        
        // tesnt na lavy otvoreny ->   (a,b> (c,d>
        assertFalse(r1.intersects(r2, false, true));
        assertTrue(r1.intersects(r1s, false, true));
        assertTrue(r1.intersects(r1e, false, true));
    
    }
    
}
