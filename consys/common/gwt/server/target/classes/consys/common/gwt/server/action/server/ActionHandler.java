package consys.common.gwt.server.action.server;


import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;
import org.springframework.stereotype.Component;


@Component
public interface ActionHandler<A,R extends Result> {

     /**
     * @return The type of {@link Action} supported by this handler.
     */
    Class<A> getActionType();

    /**
     * Handles the specified action.
     *
     * @param <T>
     *            The Result type.
     * @param action
     *            The action.
     * @return The {@link Result}.
     * @throws DispatchException
     *             if there is a problem performing the specified action.
     */
    R execute( A action) throws ActionException;
    
}
