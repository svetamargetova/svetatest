package consys.common.gwt.server.action.server;

import consys.common.gwt.shared.action.Action;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public abstract class ActionHandlerRegistry implements ApplicationListener, ApplicationContextAware {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private ApplicationContext applicationContext;
    private final Map<Class<? extends Action<?>>, ActionHandler<?, ?>> handlers;

    public ActionHandlerRegistry() {
        logger.debug("Initialized...");
        handlers = new java.util.HashMap<Class<? extends Action<?>>, ActionHandler<?, ?>>(100);
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextRefreshedEvent && getHandlers().isEmpty()) {
            logger.info("Initializing action handlers ...");
            if (applicationContext != null) {
                Map<String, ActionHandler> actionHandlers = applicationContext.getBeansOfType(ActionHandler.class);
                registerActionHandlers(actionHandlers);
            } else {
                throw new RuntimeException("Application Context is NULL!!!");
            }
            logger.info("Initializing action handlers finished.");
        }
    }

    protected void registerActionHandlers(Map<String, ActionHandler> actionHandlers) {
        for (Map.Entry<String, ActionHandler> e : actionHandlers.entrySet()) {
            registerHandler(e.getValue(), getHandlers());
        }
    }

    protected void registerHandler(ActionHandler handler, Map<Class<? extends Action<?>>, ActionHandler<?, ?>> handlers) {
        try {
            Class key = handler.getActionType();
            logger.info("   {}", key);
            handlers.put(key, handler);
        } catch (UnsupportedOperationException exception) {
            logger.info("   [Unsupported] {}", handler.getClass().getSimpleName());
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public abstract void clearHandlers();

    /**
     * @return the handlers
     */
    public Map<Class<? extends Action<?>>, ActionHandler<?, ?>> getHandlers() {
        return handlers;
    }
}
