package consys.common.gwt.server.utils;

import consys.common.gwt.shared.bo.Monetary;
import java.math.BigDecimal;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TestMonetaryUtils {

    @Test(groups = {"utils"})
    public void test() {

        BigDecimal b = new BigDecimal("12000");
        Monetary m = MonetaryUtils.fromBigDecimal(b);
        assertEquals(m.getDecimal(), 0);
        assertEquals(m.getInteger(), 12000);

        b = new BigDecimal("12000.50");
        m = MonetaryUtils.fromBigDecimal(b);
        assertEquals(m.getDecimal(), 50);
        assertEquals(m.getInteger(), 12000);

        b = new BigDecimal("117.50");
        m = MonetaryUtils.fromBigDecimal(b);
        assertEquals(m.getDecimal(), 50);
        assertEquals(m.getInteger(), 117);

    }
}
