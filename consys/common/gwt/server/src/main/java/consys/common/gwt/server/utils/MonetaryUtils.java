package consys.common.gwt.server.utils;

import consys.common.gwt.shared.bo.Monetary;
import java.math.BigDecimal;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class MonetaryUtils {

    public static Monetary fromBigDecimal(BigDecimal d) {
        if (d == null) {
            return null;
        }
        int right = 0;
        int left = 0;
        try {
            // skuisme ci mozeme prevest
            left = d.intValueExact();
        } catch (ArithmeticException exception) {
            // spracujeme string
            String value = d.toPlainString();
            int idx = value.indexOf('.');

            left = d.toBigInteger().intValue();
            String rightl = value.substring(idx + 1, value.length());
            if (rightl.length() > 2) {
                rightl = rightl.substring(0, 2);
            }
            right = Integer.parseInt(rightl);

        }
        return new Monetary(left, right);
    }

    public static BigDecimal fromMonetary(Monetary m) {
        return new BigDecimal(String.format("%d.%d", m.getInteger(), m.getDecimal()));
    }
}
