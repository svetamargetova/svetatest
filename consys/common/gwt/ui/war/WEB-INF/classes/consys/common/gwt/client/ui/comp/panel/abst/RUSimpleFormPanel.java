package consys.common.gwt.client.ui.comp.panel.abst;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;

/**
 * Predpripraveny simple form s obecne pouzivanymi metodami pro prohlizeni a editaci
 * @author pepa
 */
public abstract class RUSimpleFormPanel extends SimpleFormPanel implements RU {

    public RUSimpleFormPanel() {
        super();
    }

    public RUSimpleFormPanel(String width) {
        super(width);
    }

    public RUSimpleFormPanel(boolean withHiddableLabel) {
        super(withHiddableLabel);
    }

    public RUSimpleFormPanel(String width, boolean withHiddableLabel) {
        super(null, width, withHiddableLabel);
    }

    @Override
    protected void onLoad() {
        setWidget(readForm());
    }

    /** ClickHandler pro prepnuti do editacniho zobrazeni */
    @Override
    public ClickHandler toEditModeClickHandler(final String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setWidget(updateForm(id));
            }
        };
    }

    /** ClickHandler pro prepnuti do prohlizeciho zobrazeni */
    @Override
    public ClickHandler toReadModeClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                clearMessageBox();
                setWidget(readForm());
            }
        };
    }

    @Override
    public void onSuccessUpdate(String message) {
        clearMessageBox();
        getHiddableLabel().show(message);
        setWidget(readForm());
    }

    @Override
    public void onFailedUpdate(String message) {
        getFailMessage().setText(message);
    }
}
