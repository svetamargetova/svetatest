package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import java.util.ArrayList;

/**
 *
 *  Spravit jak obecny decorator v priapde editacie. Bude obecny list item ktery
 *  zaobaly listitem ale prida Remove. rovnako provola moznost do list itemu aby
 *  sa neco schovalo.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserListEditPanel extends FlowPanel implements SearchDelegate<ClientInvitedUser>, UserListEditItem.RemoveDelegate {

    private UserListPanel userList;
    private ArrayList<ClientUserWithAffiliationThumb> newMembers;
    private ArrayList<ClientUserWithAffiliationThumb> removedMembers;
    private ArrayList<ClientUserWithAffiliationThumb> pureItems;
    private String addLabel;

    public void setAddLabelName(String addLabel) {
        this.addLabel = addLabel;
    }

    public UserListEditPanel(ArrayList<ClientUserWithAffiliationThumb> items) {
        newMembers = new ArrayList<ClientUserWithAffiliationThumb>();
        removedMembers = new ArrayList<ClientUserWithAffiliationThumb>();
        pureItems = items;
    }

    @Override
    protected void onLoad() {
        ActionLabel addUser = new ActionLabel(addLabel);
        addUser.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog searchDialog = new UserSearchDialog();
                searchDialog.setDelegate(UserListEditPanel.this);
                searchDialog.showCentered();
            }
        });
        SimplePanel actionWrapper = new SimplePanel();
        actionWrapper.setWidget(addUser);
        actionWrapper.setStyleName(ResourceUtils.system().css().listPanelEditAction());
        add(actionWrapper);

        userList = new UserListPanel();
        for (ClientUserWithAffiliationThumb item : pureItems) {
            addItem(item);
        }
        add(userList);
    }

    private void addItem(ClientUserWithAffiliationThumb item) {
        UserListEditItem editItem = new UserListEditItem(item);
        editItem.setRemoveDelegate(this);
        userList.addItem(editItem);
    }

    @Override
    public void didSelect(ClientInvitedUser item) {
        UserListItem uli = new UserListItem(item.getUserWithAffiliationThumb());
        newMembers.add(uli.getItem());
        addItem(uli.getItem());
    }

    @Override
    public void didRemoveItem(UserListEditItem item) {
        removedMembers.add(item.getItem());
        userList.removeItem(item.getIndex());
    }
}
