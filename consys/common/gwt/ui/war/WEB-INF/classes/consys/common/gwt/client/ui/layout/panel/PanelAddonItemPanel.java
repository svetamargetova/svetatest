package consys.common.gwt.client.ui.layout.panel;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Podpanel pridavneho panelu (zobrazuje se po rozkliknuti titulku)
 * @author pepa
 */
public class PanelAddonItemPanel extends FlowPanel {

    // data
    List<PanelAddonSubItem> childs;

    public PanelAddonItemPanel() {
        this(false);
    }

    public PanelAddonItemPanel(boolean visible) {
        super();
        childs = new ArrayList<PanelAddonSubItem>();
        init(visible);
    }

    /** zakladni nastaveni panelu */
    private void init(boolean visible) {
        setStyleName(ResourceUtils.system().css().subAddon());

        FlowPanel sep = new FlowPanel();
        sep.setStyleName(ResourceUtils.system().css().subAddonTopSep());
        super.add(sep);
        setVisible(visible);
    }

    @Override
    public void add(Widget w) {
        throw new UnsupportedOperationException("Not supported. Use add(PanelAddonSubItem item)");
    }        
    
    public void clearChildrens(){
        super.clear();
        childs.clear();
    }
    
    public PanelAddonSubItem findChild(String urlName){
        for (PanelAddonSubItem item : childs) {
            LoggerFactory.log(PanelAddonItemPanel.class, "test: "+item.getName()+":"+item.getUrlName());
            if(urlName.equalsIgnoreCase(item.getUrlName())){
                return item;
            }
        }
        return null;
    }

    /** prida polozku */
    public void addChild(PanelAddonSubItem subItem) {
        if (!childs.isEmpty()) {
            super.add(separator());
        }
        childs.add(subItem);
        super.add(subItem);
    }

    /** vytvori objekt separatoru */
    private FlowPanel separator() {
        FlowPanel separator = new FlowPanel();
        separator.setStyleName(ResourceUtils.system().css().subAddonSep());
        return separator;
    }
}
