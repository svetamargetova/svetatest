package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Komponenta prepinacich tlacitek, najednou muze byt zmackle jen jedno
 * @author pepa
 */
public class Reverser extends Composite {

    // komponenty
    private FlowPanel panel;
    private FlowPanel leftBorder;
    private FlowPanel content;
    private FlowPanel rightBorder;
    // data
    private int itemCounter = 0;
    private int buttonWidth = 0;
    private ReverserItem lastSelected;

    public Reverser() {
        panel = new FlowPanel();
        initWidget(panel);
        init();
    }

    /** vytvori zakladni casti reverseru */
    private void init() {
        leftBorder = new FlowPanel();
        leftBorder.addStyleName(StyleUtils.FLOAT_LEFT);
        leftBorder.addStyleName(ResourceUtils.system().css().reverserOnLeft());

        content = new FlowPanel();
        content.addStyleName(StyleUtils.FLOAT_LEFT);

        rightBorder = new FlowPanel();
        rightBorder.addStyleName(StyleUtils.FLOAT_LEFT);
        rightBorder.addStyleName(ResourceUtils.system().css().reverserOffRight());

        panel.add(leftBorder);
        panel.add(content);
        panel.add(rightBorder);
        panel.add(StyleUtils.clearDiv());
    }

    /** vybere polozku podle poradi (prvni polozka ma index 0) */
    public void selectItem(int index) {
        if (index < 0 || index > itemCounter - 1) {
            throw new IndexOutOfBoundsException();
        }
    }

    /** prida polozku do prepinace */
    public void add(String text, ClickHandler handler) {
        add(text, handler, 0, false);
    }

    /** prida polozku do prepinace */
    public void add(String text, ClickHandler handler, int widthInt, boolean isHtml) {
        int width;
        if (widthInt == 0) {
            width = text.length() * 8 + 20;
        } else {
            width = widthInt;
        }

        if (width > buttonWidth) {
            String w = width + "px";
            for (int i = 0; i < content.getWidgetCount(); i++) {
                content.getWidget(i).setWidth(w);
            }
            buttonWidth = width;
        }

        ReverserItem item;
        switch (itemCounter) {
            case 0:
                item = new ReverserItem(text, handler, ReverserItemPosition.FIRST, buttonWidth, isHtml);
                lastSelected = item;
                break;
            case 1:
                item = new ReverserItem(text, handler, ReverserItemPosition.LAST, buttonWidth, isHtml);
                break;
            default:
                ReverserItem previous = (ReverserItem) content.getWidget(itemCounter - 1);
                previous.changePosition(ReverserItemPosition.CENTER);
                item = new ReverserItem(text, handler, ReverserItemPosition.LAST, buttonWidth, isHtml);
                break;
        }
        content.add(item);
        itemCounter++;
    }

    /** oznaci reverser, ze ma pouze jedinou polozku -> obarvi pravy rozek jako pro oznacenou polozku */
    public void onlyOneItem() {
        if (content.getWidgetCount() == 1) {
            rightBorder.removeStyleName(ResourceUtils.system().css().reverserOffRight());
            rightBorder.addStyleName(ResourceUtils.system().css().reverserOnRight());
            ((ReverserItem) content.getWidget(0)).getSeparator().removeFromParent();
        }
    }

    /** enum moznych pozic tlacitka */
    private enum ReverserItemPosition {

        FIRST, LAST, CENTER;
        private static final long serialVersionUID = 4889957874806374963L;
    }

    /** jedno tlacitko prepinace */
    private class ReverserItem extends ClickFlowPanel {

        // komponenty
        private Widget widget;
        private FlowPanel sep;
        // data
        private ReverserItemPosition position;
        private boolean isHtml;

        public ReverserItem(String title, ClickHandler handler, ReverserItemPosition position, int width, boolean isHtml) {
            super();
            this.position = position;
            this.isHtml = isHtml;

            addStyleName(position == ReverserItemPosition.FIRST
                    ? ResourceUtils.system().css().reverserOnRep() : ResourceUtils.system().css().reverserOffRep());
            addStyleName(StyleUtils.FLOAT_LEFT);
            addStyleName(StyleUtils.HAND);
            setWidth(width + "px");

            sep = new FlowPanel();
            sep.addStyleName(StyleUtils.FLOAT_RIGHT);
            if (position == ReverserItemPosition.FIRST) {
                sep.addStyleName(ResourceUtils.system().css().reverserOnCenter());
            }
            add(sep);

            if (isHtml) {
                HTML html = new HTML(title);
                html.addStyleName(position == ReverserItemPosition.FIRST
                        ? ResourceUtils.system().css().reverserOnHTML() : ResourceUtils.system().css().reverserOffHTML());
                widget = html;
            } else {
                Label label = new Label(title);
                label.setHorizontalAlignment(Label.ALIGN_CENTER);
                label.addStyleName(position == ReverserItemPosition.FIRST
                        ? ResourceUtils.system().css().reverserOnLabel() : ResourceUtils.system().css().reverserOffLabel());
                widget = label;
            }
            add(widget);

            addClickHandler(handler);
            addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    if (lastSelected != ReverserItem.this) {
                        lastSelected.unselect();
                        select();
                        lastSelected = ReverserItem.this;
                    }
                }
            });
        }

        /** oznaci tlacitko za nezmacknute */
        private void unselect() {
            switch (position) {
                case FIRST:
                    leftBorder.removeStyleName(ResourceUtils.system().css().reverserOnLeft());
                    leftBorder.addStyleName(ResourceUtils.system().css().reverserOffLeft());
                    sep.removeStyleName(ResourceUtils.system().css().reverserOnCenter());
                    sep.addStyleName(ResourceUtils.system().css().reverserOffCenter());
                    break;
                case LAST:
                    rightBorder.removeStyleName(ResourceUtils.system().css().reverserOnRight());
                    rightBorder.addStyleName(ResourceUtils.system().css().reverserOffRight());
                    break;
                case CENTER:
                    sep.removeStyleName(ResourceUtils.system().css().reverserOnCenter());
                    sep.addStyleName(ResourceUtils.system().css().reverserOffCenter());
                    break;
            }
            if (isHtml) {
                widget.removeStyleName(ResourceUtils.system().css().reverserOnHTML());
                widget.addStyleName(ResourceUtils.system().css().reverserOffHTML());
            } else {
                widget.removeStyleName(ResourceUtils.system().css().reverserOnLabel());
                widget.addStyleName(ResourceUtils.system().css().reverserOffLabel());
            }
            removeStyleName(ResourceUtils.system().css().reverserOnRep());
            addStyleName(ResourceUtils.system().css().reverserOffRep());
        }

        /** oznaci tlacitko za zmacknute */
        private void select() {
            switch (position) {
                case FIRST:
                    leftBorder.removeStyleName(ResourceUtils.system().css().reverserOffLeft());
                    leftBorder.addStyleName(ResourceUtils.system().css().reverserOnLeft());
                    sep.removeStyleName(ResourceUtils.system().css().reverserOffCenter());
                    sep.addStyleName(ResourceUtils.system().css().reverserOnCenter());
                    break;
                case LAST:
                    rightBorder.removeStyleName(ResourceUtils.system().css().reverserOffRight());
                    rightBorder.addStyleName(ResourceUtils.system().css().reverserOnRight());
                    break;
                case CENTER:
                    sep.removeStyleName(ResourceUtils.system().css().reverserOffCenter());
                    sep.addStyleName(ResourceUtils.system().css().reverserOnCenter());
                    break;
            }
            if (isHtml) {
                widget.removeStyleName(ResourceUtils.system().css().reverserOffHTML());
                widget.addStyleName(ResourceUtils.system().css().reverserOnHTML());
            } else {
                widget.removeStyleName(ResourceUtils.system().css().reverserOffLabel());
                widget.addStyleName(ResourceUtils.system().css().reverserOnLabel());
            }
            removeStyleName(ResourceUtils.system().css().reverserOffRep());
            addStyleName(ResourceUtils.system().css().reverserOnRep());
        }

        /** zmeni styl tlacitka podle zadane pozice */
        public void changePosition(ReverserItemPosition position) {
            this.position = position;
            if (position == ReverserItemPosition.CENTER) {
                sep.addStyleName(ResourceUtils.system().css().reverserOffCenter());
            }
        }

        /** vraci oddelovac */
        public FlowPanel getSeparator() {
            return sep;
        }
    }
}
