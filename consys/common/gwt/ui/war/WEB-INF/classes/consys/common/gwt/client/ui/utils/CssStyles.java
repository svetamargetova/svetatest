package consys.common.gwt.client.ui.utils;

/**
 *
 * @author palo
 */
public interface CssStyles {

    // align
    public static final String ALIGN_LEFT = "al";
    public static final String ALIGN_RIGHT = "ar";
    public static final String ALIGN_CENTER = "ac";
    // margin 1px
    public static final String MARGIN_LEFT_1 = "marL1";
    public static final String MARGIN_RIGHT_1 = "marR1";
    public static final String MARGIN_TOP_1 = "marT1";
    public static final String MARGIN_BOT_1 = "marB1";
    public static final String MARGIN_VER_1 = "marV1";
    public static final String MARGIN_HOR_1 = "marH1";
    // margin 3px
    public static final String MARGIN_LEFT_3 = "marL3";
    public static final String MARGIN_RIGHT_3 = "marR3";
    public static final String MARGIN_TOP_3 = "marT3";
    public static final String MARGIN_BOT_3 = "marB3";
    public static final String MARGIN_VER_3 = "marV3";
    public static final String MARGIN_HOR_3 = "marH3";
    // margin 5px
    public static final String MARGIN_LEFT_5 = "marL5";
    public static final String MARGIN_RIGHT_5 = "marR5";
    public static final String MARGIN_TOP_5 = "marT5";
    public static final String MARGIN_BOT_5 = "marB5";
    public static final String MARGIN_VER_5 = "marV5";
    public static final String MARGIN_HOR_5 = "marH5";
    // margin 8px
    public static final String MARGIN_LEFT_8 = ResourceUtils.system().css().marginLeft8();
    public static final String MARGIN_RIGHT_8 = ResourceUtils.system().css().marginRight8();
    public static final String MARGIN_TOP_8 = ResourceUtils.system().css().marginTop8();
    public static final String MARGIN_BOT_8 = ResourceUtils.system().css().marginBot8();
    public static final String MARGIN_VER_8 = ResourceUtils.system().css().marginVer8();
    public static final String MARGIN_HOR_8 = ResourceUtils.system().css().marginHor8();
    // margin 10px
    public static final String MARGIN_LEFT_10 = "marL10";
    public static final String MARGIN_RIGHT_10 = "marR10";
    public static final String MARGIN_TOP_10 = "marT10";
    public static final String MARGIN_BOT_10 = "marB10";
    public static final String MARGIN_VER_10 = "marV10";
    public static final String MARGIN_HOR_10 = "marH10";
    // margin 20px
    public static final String MARGIN_LEFT_20 = "marL20";
    public static final String MARGIN_RIGHT_20 = "marR20";
    public static final String MARGIN_TOP_20 = "marT20";
    public static final String MARGIN_BOT_20 = "marB20";
    public static final String MARGIN_VER_20 = "marV20";
    public static final String MARGIN_HOR_20 = "marH20";
    // margin center
    public static final String MARGIN_CENTER = "marC";
    // padding 2px
    public static final String PADDING_LEFT_2 = "padL2";
    public static final String PADDING_RIGHT_2 = "padR2";
    public static final String PADDING_TOP_2 = "padT2";
    public static final String PADDING_BOT_2 = "padB2";
    public static final String PADDING_VER_2 = "padV2";
    public static final String PADDING_HOR_2 = "padH2";
    // padding 5px
    public static final String PADDING_LEFT_5 = "padL5";
    public static final String PADDING_RIGHT_5 = "padR5";
    public static final String PADDING_TOP_5 = "padT5";
    public static final String PADDING_BOT_5 = "padB5";
    public static final String PADDING_VER_5 = "padV5";
    public static final String PADDING_HOR_5 = "padH5";
    // padding 10px
    public static final String PADDING_LEFT_10 = "padL10";
    public static final String PADDING_RIGHT_10 = "padR10";
    public static final String PADDING_TOP_10 = "padT10";
    public static final String PADDING_BOT_10 = "padB10";
    public static final String PADDING_VER_10 = "padV10";
    public static final String PADDING_HOR_10 = "padH10";
    // padding 20px
    public static final String PADDING_LEFT_20 = "padL20";
    public static final String PADDING_RIGHT_20 = "padR20";
    public static final String PADDING_TOP_20 = "padT20";
    public static final String PADDING_BOT_20 = "padB20";
    public static final String PADDING_VER_20 = "padV20";
    public static final String PADDING_HOR_20 = "padH20";
    // color background
    public static final String BACKGROUND_GRAY = "consysGrayBg";
    public static final String BACKGROUND_LIGHT_GRAY = "tabGrayBg";
    public static final String BACKGROUND_GREEN = "consysGreenBg";
    public static final String BACKGROUND_RED = "consysRedBg";
    public static final String BACKGROUND_TRANSPARENT = "transparentBg";
    public static final String BACKGROUND_WHITE = "whiteBg";
    // color text
    public static final String TEXT_WHITE = "textWhite";
    public static final String TEXT_DEFAULT = "textDefault";
    public static final String TEXT_BLUE = "textConsysBlue";
    public static final String TEXT_GRAY = "textGray";
    public static final String TEXT_GREEN = "textGreen";
    public static final String TEXT_PURPLE = "textPurple";
    public static final String TEXT_RED = "textRed";
    public static final String TEXT_NOWRAP = "noWrap";
    public static final String TEXT_BLACK2 = "textBlack2";
    public static final String TEXT_GRAY2 = "textGray2";
    public static final String TEXT_GRAY3 = "textGray3";
    public static final String TEXT_GRAY4 = "textGray4";
    // cursor
    public static final String HAND = "cursorHand";
    public static final String DEFAULT_CURSOR = "cursorDefault";
    // font size
    public static final String FONT_8PX = "f8px";
    public static final String FONT_10PX = "f10px";
    public static final String FONT_11PX = "f11px";
    public static final String FONT_12PX = "f12px";
    public static final String FONT_13PX = "f13px";
    public static final String FONT_14PX = "f14px";
    public static final String FONT_16PX = "f16px";
    public static final String FONT_17PX = "f17px";
    public static final String FONT_19PX = "f19px";
    public static final String FONT_24PX = "f24px";
    // font style
    public static final String FONT_BOLD = "fontBold";
    public static final String FONT_NORMAL = "fontNormal";
    public static final String FONT_UNDERLINE = "fontUnderline";
    // line height
    public static final String LINE_150 = "line150";
    // border
    public static final String BORDER = "inputBorder";
    public static final String BORDER_GRAY = "inputBorderGray";
    public static final String BORDER_LIGHT_GRAY = "borderLGray";
    public static final String BORDER_TRANSPARENT = "inputBorderTrans";
    // width
    public static final String WIDTH_40 = "w40";
    public static final String WIDTH_85 = "w85";
    public static final String WIDTH_270 = "w270";
    // height
    public static final String HEIGHT_21 = "h21";
    // display
    public static final String INLINE = "inline";
    public static final String UNSELECTABLE = "unsel";
    // position
    public static final String POSITION_ABS = "posAbs";
    public static final String POSITION_REL = "posRel";
    // z-index
    public static final String INDEX_1 = "posI1";
    public static final String INDEX_2 = "posI2";
    public static final String INDEX_100 = "posI100";
    // overflow
    public static final String OVERFLOW_HIDDEN = "overHide";
    // float
    public static final String FLOAT_LEFT = "floatL";
    public static final String FLOAT_RIGHT = "floatR";
    // clear
    public static final String CLEAR_BOTH = "clearB";
    public static final String CLEAR_LEFT = "clearL";
    public static final String CLEAR_RIGHT = "clearR";
    // suggest
    public static final String SUGGEST = "sugBoxPopup";
    // DOM - hodnoty pro vkladani pres DOM, nazvy zacinaji VALUE
    public static final String VALUE_COLOR_CONSYS_DEFAULT = "#555555";
    public static final String VALUE_COLOR_BLUE = "#1a809a";
    public static final String VALUE_COLOR_GRAY = "#bababa";
    public static final String VALUE_COLOR_GREEN = "#329900";
    public static final String VALUE_COLOR_PROGRESS = "#ebefdb";
    public static final String VALUE_COLOR_RED = "#c81b1b";
    public static final String VALUE_DEFAULT = "default";
    public static final String VALUE_FONT_SIZE_8PT = "10px";
}
