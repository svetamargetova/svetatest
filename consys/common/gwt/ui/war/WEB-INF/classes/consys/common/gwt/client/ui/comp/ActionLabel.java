package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 *
 * @author pepa
 */
public class ActionLabel extends Composite {

    private Label label;
    private String customStyle;
    private HandlerRegistration overReg;
    private HandlerRegistration outReg;
    private HandlerRegistration clickReg;
    private ClickHandler clickHandler;
    private boolean inverseUnderline;
    private FocusPanel focusPanel;
    private boolean disabled;

    public ActionLabel(String text) {
        this(text, null);
    }

    public ActionLabel(String text, String customStyle) {
        this(false, text, customStyle);
    }

    public ActionLabel(boolean inverseUnderline, String text) {
        this(inverseUnderline, text, "");
    }

    public ActionLabel(boolean inverseUnderline, String text, String customStyle) {
        this.inverseUnderline = inverseUnderline;
        this.customStyle = customStyle;
        disabled = false;
        label = new Label(text);
        label.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!disabled) {
                    if (ActionLabel.this.inverseUnderline) {
                        normalLabel(label);
                    } else {
                        underlineLabel(label);
                    }
                }
            }
        });

        focusPanel = new FocusPanel();
        focusPanel.setWidget(label);

        DOM.setStyleAttribute(focusPanel.getElement(), "display", "inline");
        enable();
        initWidget(label);
    }

    /** zakaze ActionLabel */
    public void disable() {
        label.setStyleName(ResourceUtils.css().actionLabel());
        label.addStyleName(ResourceUtils.css().actionLabelDisabled());
        overReg.removeHandler();
        outReg.removeHandler();
        if (clickReg != null) {
            clickReg.removeHandler();
        }
        disabled = true;
    }

    /** povoli ActionLabel */
    public void enable() {
        label.setStyleName(customStyle);
        label.addStyleName(ResourceUtils.css().actionLabel());
        label.addStyleName(ResourceUtils.css().actionLabelEnabled());
        if (inverseUnderline) {
            overReg = label.addMouseOverHandler(inverseMouseOverHandler(label));
            outReg = label.addMouseOutHandler(inverseMouseOutHandler(label));
        } else {
            underlineLabel(label);
            overReg = label.addMouseOverHandler(mouseOverHandler(label));
            outReg = label.addMouseOutHandler(mouseOutHandler(label));
        }
        initClickHandler();
        disabled = false;
    }

    /** handler po prejeti mysi pres label */
    public static MouseOverHandler mouseOverHandler(final Label label) {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                normalLabel(label);
            }
        };
    }

    /** handler po odjeti mysi z labelu */
    public static MouseOutHandler mouseOutHandler(final Label label) {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                underlineLabel(label);
            }
        };
    }

    /** handler po prejeti mysi pres label */
    public static MouseOverHandler inverseMouseOverHandler(final Label label) {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                underlineLabel(label);
            }
        };
    }

    /** handler po odjeti mysi z labelu */
    public static MouseOutHandler inverseMouseOutHandler(final Label label) {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                normalLabel(label);
            }
        };
    }

    /** nastavi pres DOM podtrzeni labelu */
    public static void underlineLabel(Label label) {
        DOM.setStyleAttribute(label.getElement(), "textDecoration", "underline");
    }

    /** nastavi pres DOM zruseni podtrzeni */
    public static void normalLabel(Label label) {
        DOM.setStyleAttribute(label.getElement(), "textDecoration", "none");
    }

    /** zmeni text v odkazu */
    public void setText(String text) {
        label.setText(text);
    }

    /** nastavi white-space:pre */
    public void enableWhiteSpaceSupport() {
        DOM.setStyleAttribute(label.getElement(), "whiteSpace", "pre");
    }

    @Override
    public void setStyleName(String style) {
        customStyle = style;
        label.setStyleName(customStyle);
    }

    @Override
    public void addStyleName(String style) {
        label.addStyleName(style);
    }

    /** nastavi ClickHandler, funguje jen na jeden !!! predchozi se zrusi */
    public void setClickHandler(ClickHandler handler) {
        if (clickReg != null) {
            clickReg.removeHandler();
        }
        clickHandler = handler;
        initClickHandler();
    }

    private void initClickHandler() {
        if (clickHandler != null) {
            clickReg = label.addClickHandler(clickHandler);
            focusPanel.addKeyPressHandler(new KeyPressHandler() {

                @Override
                public void onKeyPress(KeyPressEvent event) {
                    if ((int) event.getCharCode() == KeyCodes.KEY_ENTER) {
                        clickHandler.onClick(null);
                    }
                }
            });
        }
    }

    /**
     * umozni programove kliknout na label
     * @return true pokud bylo umozneno kliknuti (byl nastaven handler a nebylo disablovano)
     */
    public boolean click() {
        boolean isPossible = clickHandler != null && !disabled;
        if (isPossible) {
            clickHandler.onClick(null);
        }
        return isPossible;
    }

    /** vygeneruje novy action label s nazvem cancel */
    public static ActionLabel cancel() {
        return new ActionLabel(UIMessageUtils.c.const_cancel());
    }

    /** vygeneruje novy action label s nazvem cancel, se zadanym stylem */
    public static ActionLabel cancel(String style) {
        return new ActionLabel(UIMessageUtils.c.const_cancel(), style);
    }

    /** vytvori actionLabel cancel s nasetovanym clickhandlerem a akci FormUtils.fireBreadcrumbBack() */
    public static ActionLabel breadcrumbBackCancel() {
        ActionLabel cancel = cancel();
        cancel.setClickHandler(FormUtils.breadcrumbBackClickHandler());
        return cancel;
    }
}
