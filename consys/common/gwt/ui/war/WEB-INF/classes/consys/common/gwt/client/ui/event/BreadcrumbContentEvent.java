package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

public class BreadcrumbContentEvent extends BreadcrumbEvent<BreadcrumbContentEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    /** Popisuje uroven contentu
     */
    public enum LEVEL {

        /* Dalsia uroven */
        NEXT,
        /* Rovanka uroven */
        SAME,
        /* Nastavi vedla korena */
        NEXT_ROOT,
        /* Nastavi ako koren */
        ROOT;
    };
    // data
    private String name;
    private LEVEL level;
    private GwtEvent event;
    private boolean fireChange;

    /**
     * Event pro pridani polozky
     * @param name nazev zobrazeny v navigaci
     * @param level hloubka zanoreni navigace (prvni hodnota je 1)
     * @param event ktory sa vystrli po kliku na danu breadcrum
     */
    public BreadcrumbContentEvent(String name, LEVEL level, GwtEvent event) {
        this(name, level, event, true);
    }

    /**
     * Event pro pridani polozky
     * @param name nazev zobrazeny v navigaci
     * @param level hloubka zanoreni navigace (prvni hodnota je 1)
     * @param event ktory sa vystrli po kliku na danu breadcrum
     * @param fireChange udava zda se ma provolat zmena obsahu
     */
    public BreadcrumbContentEvent(String name, LEVEL level, GwtEvent event, boolean fireChange) {
        this.level = level;
        this.name = name;
        this.event = event;
        this.fireChange = fireChange;
    }

    /** vraci nazev zobrazeny v navigaci */
    public String getName() {
        return name;
    }

    public GwtEvent getEvent() {
        return event;
    }

    public LEVEL getLevel() {
        return level;
    }

    public boolean isFireChange() {
        return fireChange;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onContentBreadcrumb(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {
 
        public void onContentBreadcrumb(BreadcrumbContentEvent event);
    }
}
