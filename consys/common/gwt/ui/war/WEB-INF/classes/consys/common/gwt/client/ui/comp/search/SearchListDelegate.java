package consys.common.gwt.client.ui.comp.search;

import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public interface SearchListDelegate<T> {

    public void didSelect(ArrayList<T> items);
}
