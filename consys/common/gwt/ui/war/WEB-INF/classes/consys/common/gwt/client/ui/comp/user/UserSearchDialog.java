package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Dialog;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ListPanel.ListPanelDelegate;
import consys.common.gwt.client.ui.comp.search.SearchDataSource;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.list.UserInvitePanel;
import consys.common.gwt.client.ui.comp.user.list.UserListItem;
import consys.common.gwt.client.ui.comp.user.list.UserListPanel;
import consys.common.gwt.client.ui.dom.DOMX;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import java.util.ArrayList;

/**
 * Vyhladavaci dialog uzivatelov.
 * 
 *  +----------------------+
 *  |   SearchTextField    |
 *  +----------------------+ 
 *  |  __________________  |
 *  | |                  | |
 *  | |  Searched Items  | |
 *  | |__________________| |
 *  +----------------------+
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserSearchDialog extends Dialog implements ListPanelDelegate<UserListItem> {

    private static final int WIDTH = 400;
    // Komponenty
    TextBox searchInputBox;
    ActionImage select;
    ActionLabel invite;
    UserListPanel users;
    private SimplePanel wrapper;
    private UserInvitePanel inviteUser;
    // Data
    private boolean showInvite;
    private SearchDelegate<ClientInvitedUser> delegate;
    private SearchDataSource<UserListItem> dataSource;
    private UserListItem selectedItem;

    public UserSearchDialog() {
        this(true);
    }

    /** @param showInvite udava, zda se bude zobrazovat moznost pozvanky */
    public UserSearchDialog(boolean showInvite) {
        super(WIDTH);
        this.showInvite = showInvite;
        this.dataSource = GWT.create(UserSearchDataSource.class);
    }

    public SearchDelegate<ClientInvitedUser> getDelegate() {
        return delegate;
    }

    public void setDelegate(SearchDelegate<ClientInvitedUser> delegate) {
        this.delegate = delegate;
    }

    private void addItem(UserListItem searchItem) {
        users.addItem(searchItem);
    }

    @Override
    protected Widget createContent() {
        wrapper = new SimplePanel();

        FlowPanel body = new FlowPanel();
        body.setWidth("100%");
        body.setStyleName(UserResources.INSTANCE.userCss().userSearchDialogBody());

        // Search Text Box
        searchInputBox = new TextBox();
        searchInputBox.setWidth("200px");
        searchInputBox.setStyleName(UserResources.INSTANCE.userCss().userSearchDialogTextBox());
        searchInputBox.addStyleName(StyleUtils.FLOAT_LEFT);
        searchInputBox.addKeyUpHandler(searchKeyUpHandler());
        DOMX.setAutofocus(searchInputBox.getElement());

        if (showInvite) {
            inviteUser = new UserInvitePanel(this);

            invite = new ActionLabel(UIMessageUtils.c.userSearchDialog_action_invite(),
                    StyleUtils.Assemble(CssStyles.FONT_11PX));
            invite.setWidth("140px");
            invite.enableWhiteSpaceSupport();
            invite.removeStyleName(CssStyles.INLINE);
            invite.addStyleName(StyleUtils.FLOAT_LEFT);
            invite.addStyleName(StyleUtils.MARGIN_TOP_5);
            invite.setClickHandler(inviteClickHandler());
        }

        // Search Help
        Label help = new Label(UIMessageUtils.c.userSearchDialog_helpText());
        help.setStyleName(UserResources.INSTANCE.userCss().userSearchDialogHelp());

        // User List        
        users = new UserListPanel();
        users.setDelegate(this);
        users.setWidth("100%");
        users.setStaticHeight();
        users.addStyleName(UserResources.INSTANCE.userCss().userSearchDialogResultScrollPanel());
        wrapper.setWidget(users);

        // Select panel        
        select = ActionImage.getConfirmButton(UIMessageUtils.c.const_select());
        select.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
                if (delegate != null) {
                    ClientUserWithAffiliationThumb thumb = selectedItem.getItem();
                    ClientInvitedUser s = new ClientInvitedUser(thumb.getUuid());
                    s.setName(thumb.getName());
                    s.setPosition(thumb.getPosition());
                    s.setUuidPortraitImagePrefix(thumb.getUuidPortraitImagePrefix());
                    delegate.didSelect(s);
                }
            }
        });
        select.setVisible(false);

        ActionLabel close = new ActionLabel(UIMessageUtils.c.const_close());
        close.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });

        HTMLPanel actionPanel = new HTMLPanel("<table style='width:100%;' cellspacing='10' cellpadding='0'>"
                + "<tr><td style='height:20px;'>"
                + "<div id='select'></div>"
                + "</td><td><div id='close' style='text-align: right;'></div></td></tr></table>");
        actionPanel.setWidth("100%");
        actionPanel.add(select, "select");
        actionPanel.add(close, "close");

        body.add(searchInputBox);
        if (showInvite) {
            body.add(invite);
        }
        body.add(StyleUtils.clearDiv());
        body.add(help);
        body.add(new Separator("100%"));
        body.add(wrapper);
        body.add(new Separator("100%"));
        body.add(actionPanel);

        return body;
    }

    @Override
    public void didSelect(UserListItem item) {
        select.setVisible(true);
        selectedItem = item;
    }

    /** vrati zobrazeni obsahu do stavu pred zapnutim */
    public void toSearchView() {
        searchInputBox.setEnabled(true);
        wrapper.setWidget(users);
    }

    /** vraci keyUpHandler pro vyhledavaci pole */
    private KeyUpHandler searchKeyUpHandler() {
        return new KeyUpHandler() {

            Timer timer;

            @Override
            public void onKeyUp(final KeyUpEvent event) {
                if (timer == null) {
                    timer = new Timer() {

                        @Override
                        public void run() {
                            if (searchInputBox.getText().length() > 3) {
                                dataSource.searchBy(searchInputBox.getText(),
                                        new SearchDataSource.SearchCallback<UserListItem>() {

                                            @Override
                                            public void didFind(ArrayList<UserListItem> items) {
                                                users.clear();
                                                for (int i = 0; i < items.size(); i++) {
                                                    final UserListItem item = items.get(i);
                                                    addItem(item);
                                                }
                                            }
                                        });
                            }
                        }
                    };
                }
                timer.cancel();
                timer.schedule(800);
            }
        };
    }

    /** vraci clickHandler pro actionLabel invite */
    private ClickHandler inviteClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                searchInputBox.setEnabled(false);
                wrapper.setWidget(inviteUser);
            }
        };
    }
}
