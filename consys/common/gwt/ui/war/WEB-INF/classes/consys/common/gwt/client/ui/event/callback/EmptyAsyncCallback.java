package consys.common.gwt.client.ui.event.callback;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author pepa
 */
public class EmptyAsyncCallback<T> implements AsyncCallback<T> {

    @Override
    public void onFailure(Throwable caught) {
    }

    @Override
    public void onSuccess(T result) {
    }
}
