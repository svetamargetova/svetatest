package consys.common.gwt.client.ui.comp.tag;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Jedna položka tagu
 * @author pepa
 */
public class Tag extends FlowPanel {

    // konstanty
    private static final String HEIGHT = "15px";
    private static final String BORDER_WIDTH = "7px";
    // data
    private boolean readMode;
    private Long id;
    private String name;
    private SimplePanel removeWrapper;

    public Tag(Long id, String name) {
        this(id, name, true);
    }

    public Tag(Long id, String name, boolean readMode) {
        this.id = id;
        this.name = name;
        this.readMode = readMode;

        setHeight(HEIGHT);
        setStyleName(StyleUtils.BACKGROUND_GRAY);
        addStyleName(StyleUtils.FLOAT_LEFT);
        addStyleName(StyleUtils.MARGIN_HOR_5);
        addStyleName(StyleUtils.MARGIN_BOT_3);

        Label label = StyleUtils.getStyledLabel(name, StyleUtils.FLOAT_LEFT);

        SimplePanel leftWrapper = new SimplePanel();
        leftWrapper.setStyleName(StyleUtils.FLOAT_LEFT);
        leftWrapper.setSize(BORDER_WIDTH, HEIGHT);
        SimplePanel rightWrapper = new SimplePanel();
        rightWrapper.setStyleName(StyleUtils.FLOAT_LEFT);
        rightWrapper.setSize(BORDER_WIDTH, HEIGHT);
        removeWrapper = new SimplePanel();
        removeWrapper.setStyleName(StyleUtils.FLOAT_LEFT);
        DOM.setStyleAttribute(removeWrapper.getElement(), "marginTop", "4px");
        DOM.setStyleAttribute(removeWrapper.getElement(), "marginLeft", "15px");

        Image remove = new Image(ResourceUtils.system().conCrossOut());
        remove.setStyleName(StyleUtils.HAND);
        remove.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Tag.this.removeFromParent();
            }
        });
        removeWrapper.setWidget(remove);
        removeWrapper.setVisible(!readMode);

        add(leftWrapper);
        add(label);
        add(removeWrapper);
        add(rightWrapper);
    }

    /** vraci id tagu */
    public Long getId() {
        return id;
    }

    /** vraci nazev tagu */
    public String getName() {
        return name;
    }

    /** je tag ve ctecim rezimu? */
    public boolean isReadMode() {
        return readMode;
    }

    /** nastaveni tagu do cteciho/editacniho rezimu */
    public void setReadMode(boolean readMode) {
        this.readMode = readMode;
        removeWrapper.setVisible(!readMode);
    }

    public SimplePanel getRemoveWrapper() {
        return removeWrapper;
    }

    public void setRemoveWrapper(SimplePanel removeWrapper) {
        this.removeWrapper = removeWrapper;
    }
}
