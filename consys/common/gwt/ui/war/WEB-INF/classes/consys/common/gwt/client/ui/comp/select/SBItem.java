package consys.common.gwt.client.ui.comp.select;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.HTML;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * jedna polozka z popup panelu selectboxu
 * @author pepa
 */
public class SBItem<T> extends HTML {

    // data
    private SBItem next;
    private SBItem previous;
    private SelectBoxItem<T> item;

    public SBItem(final ItemBoard parent, SelectBoxItem<T> item) {
        super();
        this.item = item;

        String htmlPart = item.getHtml() == null ? "" : item.getHtml() + "&nbsp;";
        setHTML("<span class=\"" + StyleUtils.PADDING_LEFT_5 + " " + StyleUtils.PADDING_TOP_2 + "\">"
                + htmlPart + item.getName() + "</span>");

        addStyleName(StyleUtils.HAND);
        addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        setHeight(ItemBoard.ITEM_HEIGHT + "px");

        addMouseOverHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                if (parent.getSelectedLabel() != SBItem.this) {
                    addStyleName(StyleUtils.BACKGROUND_GRAY);
                }
            }
        });
        addMouseOutHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                if (parent.getSelectedLabel() != SBItem.this) {
                    removeStyleName(StyleUtils.BACKGROUND_GRAY);
                }
            }
        });
    }

    /** vraci nasledujici polozku */
    public SBItem getNext() {
        return next;
    }

    /** nastavuje nasledujici polozku */
    public void setNext(SBItem next) {
        this.next = next;
    }

    /** vraci predchozi polozku */
    public SBItem getPrevious() {
        return previous;
    }

    /** nastavuje predchozi polozku */
    public void setPrevious(SBItem previous) {
        this.previous = previous;
    }

    /** vraci polozku, kterou objekt reprezentuje */
    public SelectBoxItem<T> getItem() {
        return item;
    }
}
