package consys.common.gwt.client.ui.comp.input;

import consys.common.gwt.client.ui.event.ChangeValueEvent;

/**
 * Interface pro komponenty typu selectBox
 * @author pepa
 */
public interface ConsysSelectBoxInterface<T> {

    /** zapina/vypina komponentu */
    void setEnabled(boolean enabled);

    /** vybere se prvni polozka (true) nebo se zrusi vybrani polozky (false) */
    void selectFirst(boolean selectFirst);

    /** vybere se polozka podle zadane hodnoty */
    void selectItem(T item);
    
    /** prida handler na zmenu vyberu */
    void addChangeValueHandler(ChangeValueEvent.Handler handler);
}
