package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.ui.layout.menu.MenuItem;

/**
 *
 * @author pepa
 */
public class MenuItemEvent extends GwtEvent<MenuItemEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // vnitrni data
    private MenuItem menuItem;
    private MenuItemAction action;
    private int position = -1;
    private boolean success;

    public MenuItemEvent(MenuItemAction action) {
        this.action = action;
    }

    public MenuItemEvent(MenuItem menuItem, MenuItemAction action) {
        this.menuItem = menuItem;
        this.action = action;
    }

    public MenuItemEvent(MenuItem menuItem, MenuItemAction action, int position) {
        this.menuItem = menuItem;
        this.action = action;
        this.position = position;
    }

    /** vraci akci, ktera se ma provest */
    public MenuItemAction getAction() {
        return action;
    }

    /** vraci MenuItem */
    public MenuItem getMenuItem() {
        return menuItem;
    }

    /** vraci pozici, na kterou ma byt menu umisteno */
    public int getPosition() {
        return position;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onMenuItem(this);
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onMenuItem(MenuItemEvent event);
    }

    /** enum akce provadene s panelem */
    public enum MenuItemAction {

        ADD, CLEAR, DISABLE, SELECT;
        private static final long serialVersionUID = -7442779734243612187L;
    }
}
