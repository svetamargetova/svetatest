package consys.common.gwt.client.ui.comp.list.item;

/**
 * Objekt ktory je jeden zaznam v dataliste.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DataListUuidItem extends DataListItem{
    private static final long serialVersionUID = 103699217058572769L;

    private String uuid;

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
