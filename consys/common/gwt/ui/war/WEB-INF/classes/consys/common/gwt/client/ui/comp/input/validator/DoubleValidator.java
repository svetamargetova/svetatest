/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.ui.comp.input.validator;

import consys.common.gwt.client.ui.comp.form.Validator;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 *
 * @author palo
 */
public class DoubleValidator implements Validator<String> {
        
    private String notValidText;
    private String notInRangeText;
    private String notNumericText;
    // Obmedzeni
    private final double min, max;

    /**
     * Vytvori validator na minimum a maximum
     *
     * @param min
     * @param max
     */
    public DoubleValidator(double min, double max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public String validate(String value) {
        if (!ValidatorUtils.isValidString(value)) {
            return getNotValidText();
        }
        try {
            double number = Double.parseDouble(value.replaceAll(",", "."));
            if (!ValidatorUtils.isRange(number, min, max)) {
                return getNotInRangeText();
            }

        } catch (NumberFormatException ex) {
            return getNotNumericText();
        }
        return null;
    }

    /**
     * Hlaska ktora je nastavena alebo default
     * @return the notValidText
     */
    public String getNotValidText() {
        if(notValidText == null){
            return ValidatorResources.c.fieldMustEntered();
        }
        return notValidText;
    }

    /**
     * @param notValidText the notValidText to set
     */
    public void setNotValidText(String notValidText) {
        this.notValidText = notValidText;
    }

    /**
     * @return the notInRangeText
     */
    public String getNotInRangeText() {
        if(notInRangeText == null){
            return ValidatorResources.m.fieldMustBeInRange(min,max);
        }
        return notInRangeText;
    }

    /**
     * @param notInRangeText the notInRangeText to set
     */
    public void setNotInRangeText(String notInRangeText) {
        this.notInRangeText = notInRangeText;
    }

    /**
     * @return the notNumericText
     */
    public String getNotNumericText() {
         if(notNumericText == null){
            return ValidatorResources.c.fieldMustBeNumberic();
        }
        return notNumericText;
    }

    /**
     * @param notNumericText the notNumericText to set
     */
    public void setNotNumericText(String notNumericText) {
        this.notNumericText = notNumericText;
    }
}
