package consys.common.gwt.client.widget.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface CommonWidgetsConstants extends Constants {

    String const_affiliation();

    String const_confirmPassword();

    String const_firstName();

    String const_lastName();

    String const_middleName();

    String const_okLower();

    String const_password();

    String const_passwordContainsSpace();

    String const_passwordIsWeak();

    String const_passwordsNotMatch();

    String const_passwordStrength();

    String const_tooShortLower();

    String const_rememberMe();

    String const_showPassword();

    String const_weakLower();

    String loginContent_button_logIn();        
    
    String loginContent_error_accountLocked();

    String loginContent_error_authFail();

    String loginContent_error_loginDisabled();
    
    String loginContent_error_ssoUnsupported();
    
    String loginContent_text_lostPassword();
    
    String loginContent_text_notActivated();

    String loginContent_text_notRegistered();
    
    String loginContent_action_activateYourAccount();

    String loginContent_action_createYourAccount();
    
    String loginContent_action_sendMeHint();
    
    
    String loginContent_title();    
    
    String registerUser_text_agreeing();
    
    String registerContent_error_alreadyRegistered();   
}
