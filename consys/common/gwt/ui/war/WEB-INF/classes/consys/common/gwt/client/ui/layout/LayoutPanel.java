package consys.common.gwt.client.ui.layout;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventNameChangedEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.event.PanelItemEvent;
import consys.common.gwt.client.ui.event.UriFragmentTokenEvent;
import consys.common.gwt.client.ui.img.SystemCssResource;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.layout.panel.PanelItemType;
import consys.common.gwt.client.ui.layout.panel.PanelItemUI;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel zobrazujici se v prave casti prezentace
 *
 * @author pepa
 */
public class LayoutPanel extends Composite
        implements
        PanelItemEvent.Handler,
        EventNameChangedEvent.Handler,
        UriFragmentTokenEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(LayoutPanel.class);
    // konstanty
    private static final LayoutPanel instance = new LayoutPanel();
    // komponenty
    private FlowPanel systemPanel;
    private FlowPanel systemPanelContent;
    private FlowPanel addonPanel;
    private ClickPanel addonTitle;
    private FlowPanel addonPanelContent;
    private FlowPanel bannerPanel;
    // data
    private List<PanelItem> containsSystemPanel;
    private List<PanelItem> containsAddonPanel;

    private LayoutPanel() {
        containsSystemPanel = new ArrayList<PanelItem>();
        containsAddonPanel = new ArrayList<PanelItem>();

        FlowPanel layoutPanel = new FlowPanel();
        layoutPanel.add(createAddonPanel());
        layoutPanel.add(createSystemPanel());
        layoutPanel.add(createBannerPanel());

        initWidget(layoutPanel);
    }

    /** vraci instanci panelu */
    public static LayoutPanel get() {
        return instance;
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(PanelItemEvent.TYPE, this);
        EventBus.get().addHandler(EventNameChangedEvent.TYPE, this);
        EventBus.get().addHandler(UriFragmentTokenEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(PanelItemEvent.TYPE, this);
        EventBus.get().removeHandler(EventNameChangedEvent.TYPE, this);
        EventBus.get().removeHandler(UriFragmentTokenEvent.TYPE, this);
    }

    /** vytvori panel */
    private FlowPanel createPanel(FlowPanel content, boolean system) {
        SystemCssResource css = ResourceUtils.system().css();

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(css.panel());
        if (!system) {
            panel.addStyleName(css.addon());
        }

        FlowPanel top = new FlowPanel();
        top.setStyleName(system ? css.top() : css.addTop());
        FlowPanel topLeft = new FlowPanel();
        topLeft.setStyleName(system ? css.left() : css.addLeft());
        FlowPanel topCenter = new FlowPanel();
        topCenter.setStyleName(css.center());
        FlowPanel topRight = new FlowPanel();
        topRight.setStyleName(system ? css.right() : css.addRight());
        top.add(topLeft);
        top.add(topCenter);
        top.add(topRight);

        FlowPanel bottom = new FlowPanel();
        bottom.setStyleName(css.bottom());
        FlowPanel bottomLeft = new FlowPanel();
        bottomLeft.setStyleName(css.left());
        FlowPanel bottomCenter = new FlowPanel();
        bottomCenter.setStyleName(css.center());
        FlowPanel bottomRight = new FlowPanel();
        bottomRight.setStyleName(css.right());
        bottom.add(bottomLeft);
        bottom.add(bottomCenter);
        bottom.add(bottomRight);

        panel.add(top);
        if (!system) {
            addonTitle = new ClickPanel();
            addonTitle.setStyleName(css.addonTitle());

            FlowPanel addSep = new FlowPanel();
            addSep.setStyleName(css.addSep());
            addSep.add(new FlowPanel());

            panel.add(addonTitle);
            panel.add(addSep);
        }
        panel.add(content);
        panel.add(bottom);

        return panel;
    }

    private FlowPanel createBannerPanel() {
        bannerPanel = new FlowPanel();
        return bannerPanel;
    }

    /** vytvori systemovy panel (je zobrazen vzdy) */
    private FlowPanel createSystemPanel() {
        systemPanelContent = new FlowPanel() {

            @Override
            public void add(Widget w) {
                super.add(w);
                showSystemPanel(true);
            }

            @Override
            public boolean remove(Widget w) {
                boolean result = super.remove(w);
                if (getWidgetCount() == 0) {
                    showSystemPanel(false);
                }
                return result;
            }

            @Override
            public void clear() {
                super.clear();
                showSystemPanel(false);
            }
        };
        systemPanel = createPanel(systemPanelContent, true);
        systemPanel.setVisible(false);
        return systemPanel;
    }

    /** vytvori doplnkovy panel (zobrazuje se na vyzadani) */
    private FlowPanel createAddonPanel() {
        addonPanelContent = new FlowPanel();
        addonPanel = createPanel(addonPanelContent, false);
        addonPanel.addStyleName(StyleUtils.MARGIN_BOT_20);
        addonPanel.setVisible(false);
        return addonPanel;
    }

    /** zobrazi/skryje systemovy panel */
    private void showSystemPanel(boolean value) {
        if (systemPanel != null) {
            systemPanel.setVisible(value);
        }
    }

    /** prida polozku na zadanou pozici do panelu podle typu */
    private boolean addItem(PanelItem panelItem, PanelItemType type, int position) {
        switch (type) {
            case SYSTEM:
                addItem(systemPanelContent, panelItem, position);
                containsSystemPanel.add(panelItem);
                break;
            case ADDON:
                addItem(addonPanelContent, panelItem, position);
                containsAddonPanel.add(panelItem);
                break;
            case BANNER:
                bannerPanel.add(panelItem.getContent());
                break;
            default:
                return false;
        }
        return true;
    }

    /** vlozi polozku do zvoleneho panelu */
    private void addItem(FlowPanel panel, PanelItem panelItem, int position) {
        boolean showSeparator = panel.getWidgetCount() > 0;
        if (position == -1) {
            PanelItemUI ui = new PanelItemUI(panelItem, showSeparator);
            ui.setBgStyleName(showSeparator ? ResourceUtils.system().css().nxt() : ResourceUtils.system().css().fst());
            panel.add(ui);
        } else {
            PanelItemUI ui = new PanelItemUI(panelItem, showSeparator);
            ui.setBgStyleName(showSeparator ? ResourceUtils.system().css().nxt() : ResourceUtils.system().css().fst());
            panel.insert(ui, position);
        }
    }

    @Override
    public void onPanelItem(PanelItemEvent event) {
        switch (event.getAction()) {
            case ADD:
                if (addItem(event.getPanelItem(), event.getType(), event.getPosition())) {
                    logger.debug("panel item(s) changed, " + event.getAction().toString() + " " + event.getPanelItem().getName());
                } else {
                    logger.debug("bad ADD event type " + event.getType());
                }
                break;
            case CLEAR:
                switch (event.getType()) {
                    case SYSTEM:
                        systemPanelContent.clear();
                        containsSystemPanel.clear();
                        logger.debug("clear system panel");
                        break;
                    case ADDON:
                        addonPanel.setVisible(false);
                        addonPanelContent.clear();
                        containsAddonPanel.clear();
                        bannerPanel.clear();
                        logger.debug("clear addon + banner panel");
                        break;
                    case BANNER:
                        bannerPanel.clear();
                        logger.debug("clear banner panel");
                        break;
                    case ALL:
                        systemPanelContent.clear();
                        addonPanelContent.clear();
                        containsSystemPanel.clear();
                        containsAddonPanel.clear();
                        bannerPanel.clear();
                        logger.debug("clear all panel");
                        break;
                }
                logger.debug("panel cleared - " + event.getType());
                break;
            case HIDE_ADDON:
                addonPanel.setVisible(false);
                bannerPanel.clear();
                logger.debug("addon panel hide");
                break;
            case SHOW_ADDON:
                addonPanel.setVisible(true);
                logger.debug("addon panel show");
                break;
            case ADD_BANNER:
                addItem(event.getPanelItem(), event.getType(), event.getPosition());
                bannerPanel.setVisible(true);
                break;
            case REMOVE_BANNER:
                bannerPanel.clear();
                bannerPanel.setVisible(false);
                break;
        }
    }

    /** @return true pokud obsahuje panel (identifikace podle nazvu) */
    public boolean contains(PanelItem panelItem) {

        for (PanelItem panelItem1 : containsSystemPanel) {
            if (panelItem1.getName().equalsIgnoreCase(panelItem.getName())) {
                return true;
            }
        }

        for (PanelItem panelItem1 : containsAddonPanel) {
            if (panelItem1.getName().equalsIgnoreCase(panelItem.getName())) {
                return true;
            }
        }

        return false;
    }

    /** nastavi text titulku pridavneho panelu */
    public void setAddonPanelTitle(String title) {
        HTML addonTitleLabel = new HTML(title);
        addonTitleLabel.setStyleName(ResourceUtils.system().css().title());
        addonTitle.clear();
        addonTitle.add(addonTitleLabel);
    }

    public void setAddonPanelTitleAction(final ConsysAction action) {
        addonTitle.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                action.run();
            }
        });
    }

    @Override
    public void onEventNameChanged(EventNameChangedEvent event) {
        setAddonPanelTitle(event.getTitle());
        Window.setTitle(event.getTitle());
    }

    @Override
    public void onCatchedFragment(UriFragmentTokenEvent event) {
        for (PanelItem panelItem : containsAddonPanel) {
            panelItem.onCatchedFragment(event);
        }
    }

    /** pomocny panel s moznosti zaregistrovani jen jednoho click handleru */
    private class ClickPanel extends FlowPanel {

        private HandlerRegistration registered;

        public ClickPanel() {
            sinkEvents(Event.ONCLICK);
        }

        public void setClickHandler(ClickHandler handler) {
            if (registered != null) {
                registered.removeHandler();
            } else {
                addStyleName(StyleUtils.HAND);
            }
            registered = addHandler(handler, ClickEvent.getType());
        }
    }
}
