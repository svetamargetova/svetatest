package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.constants.sso.SSO;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.event.SSOButtonEvent;

/**
 * Jedno SSO tlacitko
 * @author pepa
 */
public class LoginPanelButton extends SimplePanel implements SSOButtonEvent.Handler {

    // komponenty
    private AnchorElement anchor;
    // data
    private SSO type;
    private String className = "";
    private String classNameOff = "";
    private String classNameOver = "";
    private boolean off;
    private HandlerRegistration overRegistration;
    private HandlerRegistration outRegistration;

    public LoginPanelButton(SSO type) {
        this.type = type;

        sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        setStyleName(PanelResources.INSTANCE.css().button());

        switch (type) {
            case FACEBOOK:
                className = PanelResources.INSTANCE.css().facebook();
                classNameOff = PanelResources.INSTANCE.css().facebookOff();
                classNameOver = PanelResources.INSTANCE.css().facebookOver();
                break;
            case GOOGLE_PLUS:
                className = PanelResources.INSTANCE.css().google_plus();
                classNameOff = PanelResources.INSTANCE.css().google_plusOff();
                classNameOver = PanelResources.INSTANCE.css().google_plusOver();
                break;
            case LINKEDIN:
                className = PanelResources.INSTANCE.css().linkedin();
                classNameOff = PanelResources.INSTANCE.css().linkedinOff();
                classNameOver = PanelResources.INSTANCE.css().linkedinOver();
                break;
            case TWITTER:
                className = PanelResources.INSTANCE.css().twitter();
                classNameOff = PanelResources.INSTANCE.css().twitterOff();
                classNameOver = PanelResources.INSTANCE.css().twitterOver();
                break;
        }

        anchor = Document.get().createAnchorElement();
        anchor.setClassName(className);
        anchor.setHref("javascript:ssoLogin" + type.name() + "();");
        getElement().appendChild(anchor);
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(SSOButtonEvent.TYPE, this);
        registerHandlers();
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(SSOButtonEvent.TYPE, this);
        unregisterHandlers();
    }

    private void registerHandlers() {
        overRegistration = addHandler(overHandler(), MouseOverEvent.getType());
        outRegistration = addHandler(outHandler(), MouseOutEvent.getType());
    }

    private void unregisterHandlers() {
        if (overRegistration != null) {
            overRegistration.removeHandler();
            overRegistration = null;
        }
        if (outRegistration != null) {
            outRegistration.removeHandler();
            outRegistration = null;
        }
    }

    private MouseOverHandler overHandler() {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                anchor.setClassName(classNameOver);
            }
        };
    }

    private MouseOutHandler outHandler() {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                if (off) {
                    anchor.setClassName(classNameOff);
                } else {
                    anchor.setClassName(className);
                }
            }
        };
    }

    @Override
    public void onSSOClick(SSOButtonEvent event) {
        if (event.getType().equals(type)) {
            anchor.setClassName(className);
            off = false;
        } else {
            anchor.setClassName(classNameOff);
            off = true;
        }
    }
}
