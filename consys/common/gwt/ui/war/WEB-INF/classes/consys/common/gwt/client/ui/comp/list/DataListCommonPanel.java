package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class DataListCommonPanel extends HeadPanel implements DataListCommonHeadPanel {

    // komponenty
    private FlowPanel filters;
    private FlowPanel orderers;
    private ListPagerWidget listPagerWidget;
    private SelectBox<DataListCommonOrdererItem> orderSelectBox;
    private SelectBox<DataListCommonFiltersItem> filterSelectBox;
    // data
    private ArrayList<DataListCommonOrdererItem> allOrderers;
    private ArrayList<DataListCommonFiltersItem> allFilters;
    private DataListCommonOrdererItem activeOrderer;
    private DataListCommonFiltersItem activeFilter;

    public DataListCommonPanel() {
        allOrderers = new ArrayList<DataListCommonOrdererItem>();
        allFilters = new ArrayList<DataListCommonFiltersItem>();

        orderSelectBox = new SelectBox<DataListCommonOrdererItem>();
        orderSelectBox.addStyleName(StyleUtils.FLOAT_LEFT);
        orderSelectBox.setWidth(180);
        orderSelectBox.selectFirst(true);
        orderSelectBox.addChangeValueHandler(orderSelectBoxHandler());

        filterSelectBox = new SelectBox<DataListCommonFiltersItem>();
        filterSelectBox.addStyleName(StyleUtils.FLOAT_LEFT);
        filterSelectBox.setWidth(180);
        filterSelectBox.selectFirst(true);
        filterSelectBox.addChangeValueHandler(filterSelectBoxHandler());

        FlowPanel fp = new FlowPanel();
        fp.setHeight("25px");

        orderers = new FlowPanel();
        orderers.addStyleName(StyleUtils.FLOAT_LEFT);
        orderers.addStyleName(StyleUtils.MARGIN_RIGHT_20);
        orderers.add(StyleUtils.getStyledLabel(UIMessageUtils.c.dataListCommon_text_sortBy() + ":", ResourceUtils.system().css().dataListCommonOrdererTitle()));

        filters = new FlowPanel();
        filters.addStyleName(StyleUtils.FLOAT_LEFT);
        filters.add(StyleUtils.getStyledLabel(UIMessageUtils.c.dataListCommon_text_show() + ":", ResourceUtils.system().css().dataListCommonOrdererTitle()));

        listPagerWidget = new ListPagerWidget();

        SimplePanel sp = new SimplePanel();
        sp.addStyleName(StyleUtils.FLOAT_RIGHT);
        sp.setWidget(listPagerWidget);

        fp.add(orderers);
        fp.add(filters);
        fp.add(sp);
        fp.add(StyleUtils.clearDiv());
        initWidget(fp);
    }

    @Override
    protected void onLoad() {
        ArrayList<SelectBoxItem<DataListCommonOrdererItem>> orderItems = new ArrayList<SelectBoxItem<DataListCommonOrdererItem>>();

        for (DataListCommonOrdererItem item : allOrderers) {
            SelectBoxItem<DataListCommonOrdererItem> initItem =
                    new SelectBoxItem<DataListCommonOrdererItem>(item, item.getText());
            orderItems.add(initItem);
        }

        orderSelectBox.setItems(orderItems);
        orderers.add(orderSelectBox);

        ArrayList<SelectBoxItem<DataListCommonFiltersItem>> filterItems = new ArrayList<SelectBoxItem<DataListCommonFiltersItem>>();

        for (DataListCommonFiltersItem item : allFilters) {
            SelectBoxItem<DataListCommonFiltersItem> initItem =
                    new SelectBoxItem<DataListCommonFiltersItem>(item, item.getText());
            filterItems.add(initItem);
        }

        filterSelectBox.setItems(filterItems);
        filters.add(filterSelectBox);
    }

    @Override
    public void setParentList(DataListPanel parentList) {
        for (DataListCommonOrdererItem co : allOrderers) {
            co.setParentList(parentList);
        }
        for (DataListCommonFiltersItem cf : allFilters) {
            cf.setParentList(parentList);
        }
    }

    @Override
    public ListPagerWidget getListPagerWidget() {
        return listPagerWidget;
    }

    /** prida razeni do panelu */
    public void addOrderer(String title, int tag) {
        addOrderer(title, tag, false);
    }

    /** prida razeni do panelu */
    public void addOrderer(String title, int tag, boolean selected) {
        final DataListCommonOrdererItem co = new DataListCommonOrdererItem(tag, title, this);
        co.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeOrderer != null) {
                    activeOrderer.deselect();
                }
                activeOrderer = co;
                activeOrderer.select();
                activeOrderer.refresh();
            }
        });
        allOrderers.add(co);

        if (selected) {
            if (activeOrderer != null) {
                activeOrderer.deselect();
            }
            activeOrderer = co;
            co.select();
            clearOrderer();
            registerOrderer(co);
        } else {
            co.deselect();
        }
    }

    /** prida filtr do panelu */
    public void addFilter(String title, int tag) {
        addFilter(title, tag, false);
    }

    /** prida filtr do panelu */
    public void addFilter(String title, int tag, boolean selected) {
        final DataListCommonFiltersItem cf = new DataListCommonFiltersItem(tag, title, this);
        cf.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                activeFilter = cf;
                activeFilter.select();
                activeFilter.refresh();
            }
        });
        allFilters.add(cf);

        // nastavime aktivny filtr
        if (selected) {
            if (activeFilter != null) {
                activeFilter.deselect();
            }
            activeFilter = cf;
            cf.select();
            clearFilter();
            registerFilter(cf);
        } else {
            cf.deselect();
        }
    }

    private ChangeValueEvent.Handler<SelectBoxItem<DataListCommonOrdererItem>> orderSelectBoxHandler() {
        return new ChangeValueEvent.Handler<SelectBoxItem<DataListCommonOrdererItem>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<DataListCommonOrdererItem>> event) {
                if (activeOrderer != null) {
                    activeOrderer.deselect();
                }
                DataListCommonOrdererItem value = event.getValue().getItem();
                activeOrderer = value;
                activeOrderer.select();
                activeOrderer.refresh();
            }
        };
    }

    private ChangeValueEvent.Handler<SelectBoxItem<DataListCommonFiltersItem>> filterSelectBoxHandler() {
        return new ChangeValueEvent.Handler<SelectBoxItem<DataListCommonFiltersItem>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<DataListCommonFiltersItem>> event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                DataListCommonFiltersItem value = event.getValue().getItem();
                activeFilter = value;
                activeFilter.select();
                activeFilter.refresh();
            }
        };
    }
}
