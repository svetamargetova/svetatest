package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class ListPanelItem<T> extends SimplePanel {

    private FlowPanel itemBody;
    private int index;
    private boolean selectable = true;
    private boolean selected = false;

    public ListPanelItem() {
        itemBody = new FlowPanel();
        setStyleName(ResourceUtils.system().css().listPanelItem());
        setWidget(itemBody);

        addDomHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                hover();
            }
        }, MouseOverEvent.getType());

        addDomHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                unhover();
            }
        }, MouseOutEvent.getType());
    }

    @Override
    protected void onLoad() {
        create(itemBody);
        unselected();
    }

    public void addClickHandler(ClickHandler handler) {
        addDomHandler(handler, ClickEvent.getType());
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    protected FlowPanel getBody() {
        return itemBody;
    }

    public abstract void create(FlowPanel panel);

    public void hover() {
    }

    public void unhover() {
    }

    public void selected() {
        selected = true;
        addStyleName(ResourceUtils.system().css().listPanelItemSelected());
        removeStyleName(ResourceUtils.system().css().listPanelItemEven());
        removeStyleName(ResourceUtils.system().css().listPanelItemOdd());
    }

    public void unselected() {
        selected = false;
        removeStyleName(ResourceUtils.system().css().listPanelItemSelected());
        showOddEven();
    }

    /** oznaci polozku za sudou nebo lichou */
    protected void showOddEven() {
        removeStyleName(ResourceUtils.system().css().listPanelItemEven());
        removeStyleName(ResourceUtils.system().css().listPanelItemOdd());
        if (index == 0 || index % 2 == 0) {
            addStyleName(ResourceUtils.system().css().listPanelItemEven());
        } else {
            addStyleName(ResourceUtils.system().css().listPanelItemOdd());
        }
    }

    /** vraci true pokud je polozka vybrana */
    public boolean isSelected() {
        return selected;
    }

    public abstract T getObject();

    /**
     * @return the selectable
     */
    public boolean isSelectable() {
        return selectable;
    }

    /**
     * @param selectable the selectable to set
     */
    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }
}
