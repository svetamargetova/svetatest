package consys.common.gwt.client.ui.layout;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.MenuSearcher;
import consys.common.gwt.client.ui.event.MenuItemEvent;
import consys.common.gwt.client.ui.layout.menu.MenuItem;
import consys.common.gwt.client.ui.layout.menu.MenuItemUI;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * 
 * @author pepa
 */
public class LayoutTopMenu extends Composite implements MenuItemEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(LayoutTopMenu.class);
    // instance
    private static LayoutTopMenu instance;
    // konstanty
    private static final String PANEL_HEIGHT = "33px";
    private static final String PANEL_BORDER_WIDTH = "6px";
    // komponenty
    private HorizontalPanel contentPanel;
    private MenuSearcher menuSearcher;

    private LayoutTopMenu() {
        FlowPanel panel = new FlowPanel();
        panel.setSize(LayoutManager.LAYOUT_WIDTH, PANEL_HEIGHT);
        panel.addStyleName(StyleUtils.POSITION_REL);
        panel.addStyleName(StyleUtils.INDEX_1);

        Image leftBorder = new Image(ResourceUtils.system().topMenuLeft());
        SimplePanel leftBorderWrapper = new SimplePanel();
        leftBorderWrapper.setSize(PANEL_BORDER_WIDTH, PANEL_HEIGHT);
        leftBorderWrapper.addStyleName(StyleUtils.FLOAT_LEFT);
        leftBorderWrapper.setWidget(leftBorder);

        Image rightBorder = new Image(ResourceUtils.system().topMenuRight());
        SimplePanel rightBorderWrapper = new SimplePanel();
        rightBorderWrapper.setSize(PANEL_BORDER_WIDTH, PANEL_HEIGHT);
        rightBorderWrapper.addStyleName(StyleUtils.FLOAT_RIGHT);
        rightBorderWrapper.setWidget(rightBorder);

        contentPanel = new HorizontalPanel();
        contentPanel.setStyleName(ResourceUtils.system().css().panelMenuTopMargin());

        menuSearcher = new MenuSearcher();
        menuSearcher.addStyleName(StyleUtils.FLOAT_RIGHT);

        FlowPanel styleWrapperMenu = new FlowPanel();
        styleWrapperMenu.setStyleName(ResourceUtils.system().css().topMenuCenter());
        styleWrapperMenu.addStyleName(StyleUtils.FLOAT_LEFT);
        styleWrapperMenu.setWidth((LayoutManager.LAYOUT_WIDTH_INT - 12) + "px");
        //styleWrapperMenu.add(menuSearcher);
        styleWrapperMenu.add(contentPanel);
        //styleWrapperMenu.add(StyleUtils.clearDiv());

        panel.add(leftBorderWrapper);
        panel.add(styleWrapperMenu);
        panel.add(rightBorderWrapper);
        panel.add(StyleUtils.clearDiv());

        initWidget(panel);
    }

    /** vraci instanci LayoutMenu */
    public static LayoutTopMenu get() {
        if (instance == null) {
            instance = new LayoutTopMenu();
        }
        return instance;
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(MenuItemEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(MenuItemEvent.TYPE, this);
    }

    /** prida jednu polozku do menu */
    private void addItem(MenuItem menuItem, int position) {
        if (position == -1) {
            contentPanel.add(new MenuItemUI(menuItem, true));
        } else {
            contentPanel.insert(new MenuItemUI(menuItem, true), position);
        }
    }

    /** vraci MenuSearcher pro nastaveni podle potreby */
    public MenuSearcher getMenuSearcher() {
        return menuSearcher;
    }

    @Override
    public void onMenuItem(MenuItemEvent event) {
        switch (event.getAction()) {
            case ADD:
                addItem(event.getMenuItem(), event.getPosition());
                logger.debug("+ item " + event.getMenuItem().getTitle());
                break;
            case CLEAR:
                contentPanel.clear();
                logger.debug(" cleared");
                break;
        }
    }
}
