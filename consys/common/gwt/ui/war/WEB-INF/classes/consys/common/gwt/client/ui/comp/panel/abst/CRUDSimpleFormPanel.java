package consys.common.gwt.client.ui.comp.panel.abst;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;

/**
 * Predpripraveny simple form s obecne pouzivanymi metodami pro pridavani,
 * prohlizeni, editaci a mazani
 * @author pepa
 */
public abstract class CRUDSimpleFormPanel extends RUSimpleFormPanel implements CRUD {

    public CRUDSimpleFormPanel() {
        super();
    }

    public CRUDSimpleFormPanel(String width) {
        super(width);
    }

    public CRUDSimpleFormPanel(boolean withHiddableLabel) {
        super(withHiddableLabel);
    }

    public CRUDSimpleFormPanel(String width, boolean withHiddableLabel) {
        super(width, withHiddableLabel);
    }

    /** ClickHandler pro prepnuti do pridavaciho zobrazeni */
    @Override
    public ClickHandler createClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Widget w = createForm();
                if (w != null) {
                    setWidget(w);
                }
            }
        };
    }

    /** ClickHandler pro prepnuti do mazaciho zobrazeni */
    @Override
    public ClickHandler deleteClickHandler(final String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Widget w = deleteForm(id);
                if (w != null) {
                    setWidget(w);
                }
            }
        };
    }
}
