package consys.common.gwt.client.ui.layout.panel;

/**
 * Enum akce provadene s panelem
 * @author pepa
 */
public enum PanelItemAction {

    ADD, REMOVE, CLEAR, SHOW_ADDON, HIDE_ADDON, ADD_BANNER,REMOVE_BANNER;
    private static final long serialVersionUID = 5033661075823564827L;
}
