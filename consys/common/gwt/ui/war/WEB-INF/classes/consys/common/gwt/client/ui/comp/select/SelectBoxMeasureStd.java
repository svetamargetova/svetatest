package consys.common.gwt.client.ui.comp.select;

/**
 * Udava rozmery SelectBox, pro neIE prohlizece
 * @author pepa
 */
public class SelectBoxMeasureStd extends SelectBoxMeasure {

    // konstanty
    private static final int STD_DEFAULT_WIDTH = 268;
    private static final int STD_DEFAULT_HEIGHT = 19;
    private static final int STD_DEFAULT_INPUT_WIDTH_DIFF = 26;

    @Override
    public String defaultHeight() {
        return STD_DEFAULT_HEIGHT + "px";
    }

    @Override
    public String defaultInputWidth() {
        return (STD_DEFAULT_WIDTH - STD_DEFAULT_INPUT_WIDTH_DIFF) + "px";
    }

    @Override
    public String defaultWidth() {
        return STD_DEFAULT_WIDTH + "px";
    }

    @Override
    public String basePanelWidth(int width) {
        return (width - 2) + "px";
    }

    @Override
    public String selectItemLabelWidth(int width) {
        return (width - STD_DEFAULT_INPUT_WIDTH_DIFF) + "px";
    }

    @Override
    public String inputWidth(int width) {
        return (width - STD_DEFAULT_INPUT_WIDTH_DIFF) + "px";
    }
}
