package consys.common.gwt.client.ui.resource;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import consys.common.gwt.client.ui.css.CommonCss;

/**
 * Zdielany resource. 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface CommonResources extends ClientBundle {
    
    /**
     * Common CSS resouce, s anotaciou @ImportedWithPrefix("css"). Pouzivat na
     * obecne selektory
     * @return 
     */
    @Source("consys/common/gwt/client/ui/css/common.css")
    public CommonCss css();


}
