package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Panel s editacnim tlacitkem a mazacim tlacitkem
 * @author pepa
 */
public class EditWithRemover extends FlowPanel {

    // data
    protected ConsysAction editAction;
    protected ConsysAction removeAction;

    public EditWithRemover(ConsysAction editAction, ConsysAction removeAction) {
        if (editAction == null || removeAction == null) {
            throw new IllegalArgumentException();
        }
        this.editAction = editAction;
        this.removeAction = removeAction;
    }

    @Override
    protected void onLoad() {
        clear();
        generateContent();
    }

    /** vygeneruje vlastni akce */
    protected void generateContent() {
        ActionLabel edit = getEditWithoutAction();
        edit.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                editAction.run();
            }
        });

        Remover remover = Remover.commonRemover(removeAction);
        remover.addStyleName(ResourceUtils.system().css().editWithRemoverItem());

        add(edit);
        add(remover);
        add(StyleUtils.clearDiv());
    }

    /** vraci nastylovany ActionLabel bez nasetovane akce */
    protected ActionLabel getEditWithoutAction() {
        return new ActionLabel(UIMessageUtils.c.const_edit(), ResourceUtils.system().css().editWithRemoverItem());
    }

    /** prida styl s floatovanim doprava a odsazenim zhora */
    public void atRight() {
        addStyleName(ResourceUtils.system().css().editWithRemoverAtRight());
    }
}
