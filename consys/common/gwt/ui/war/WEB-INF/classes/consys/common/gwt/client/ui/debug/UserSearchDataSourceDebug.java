package consys.common.gwt.client.ui.debug;

import consys.common.gwt.client.ui.comp.search.SearchDataSource;
import consys.common.gwt.client.ui.comp.search.SearchDataSource.SearchCallback;
import consys.common.gwt.client.ui.comp.user.list.UserListItem;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserSearchDataSourceDebug implements SearchDataSource<UserListItem> {

    private static final Map<String, String> names = new HashMap<String, String>();

    {
        names.put("Peter Veliky", "Rome");
        names.put("Abraham Lincoln", "Preziden of USA");
        names.put("David Gahan", "Depeche Mode member");
        names.put("Martin Gore", "Depeche Mode member");
        names.put("Andy Fletcher", "Depeche Mode member");
        names.put("Jimmy Hendrix", " just Jimmy");
        names.put("Robert Plant", "Led Zeppelin");
        names.put("Jimmy Page", "Led Zeppelin");
        names.put("Syd Barrett", "Pink Floyd");
        names.put("David Gilmour", "Pink Floyd");
        names.put("Nick Mason", "Pink Floyd");
        names.put("Roger Waters", "Pink Floyd");
        names.put("Richard Wright", "Pink Floyd");
    }

    @Override
    public void searchBy(String input, final SearchCallback<UserListItem> callback) {
        ArrayList<UserListItem> list = new ArrayList<UserListItem>();
        Set<String> keys = names.keySet();
        int i = 0;
        for (Iterator<String> it = keys.iterator(); it.hasNext();i++) {
            String key = it.next();
            ClientUserWithAffiliationThumb csut = new ClientUserWithAffiliationThumb();
            csut.setUuid(i+"-debug");
            csut.setName(key);
            csut.setPosition(names.get(key));
            list.add(new UserListItem(csut));
        }
        callback.didFind(list);
    }
}
