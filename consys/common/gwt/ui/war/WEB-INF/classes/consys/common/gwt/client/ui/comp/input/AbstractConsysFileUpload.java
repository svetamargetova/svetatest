package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;

/**
 * Uploadovaci komponentka
 * @author pepa
 */
public abstract class AbstractConsysFileUpload extends Composite {

    // konstanty
    private static final String STYLE_FILE_UPLOAD = "consysFileUpload";
    // komponenty
    private FlowPanel panel;
    private FileUpload upload;
    private FlowPanel originalWrapper;
    // data
    private int intWidth;
    private int intHeight;

    public AbstractConsysFileUpload(int intWidth, int intHeight) {
        this.intWidth = intWidth;
        this.intHeight = intHeight;

        InputResources.INSTANCE.css().ensureInjected();

        panel = new FlowPanel();
        panel.setStyleName(InputResources.INSTANCE.css().consysFileUpload());
        panel.setSize(intWidth + "px", intHeight + "px");

        initWidget(panel);
        generate();
    }

    /** vygeneruje obsah do panelu */
    private void generate() {
        upload = new InnerFileUpload();
        upload.getElement().setAttribute("size", "1");
        upload.setStyleName(STYLE_FILE_UPLOAD);

        originalWrapper = new FlowPanel();
        originalWrapper.setStyleName(InputResources.INSTANCE.css().uploadWrapper());
        originalWrapper.add(upload);

        FlowPanel face = new FlowPanel();
        face.setStyleName(InputResources.INSTANCE.css().uploadFaceWrapper());
        initFace(face);

        panel.add(originalWrapper);
        panel.add(face);
    }

    /** nastavi zobrazovaci ksichtik */
    protected abstract void initFace(FlowPanel face);

    /** zaridi ze se ksichnik bude tvarit jako disablovany */
    protected abstract void disable(boolean disable);

    /** nastavi nazev komponenty pro odeslani ve formulari */
    public void setName(String name) {
        upload.setName(name);
    }

    /** vraci nazev vybraneho souboru */
    public String getFilename() {
        return upload.getFilename();
    }

    /** enabluje/disabluje komponentu */
    public void setEnabled(boolean enabled) {
        if (enabled) {
            panel.insert(originalWrapper, 0);
        } else {
            panel.remove(originalWrapper);
        }

        disable(!enabled);
    }

    /** vraci sirku komponenty */
    public int getIntWidth() {
        return intWidth;
    }

    /** vraci vysku komponenty */
    public int getIntHeight() {
        return intHeight;
    }

    /** prida handler na zmenu */
    public HandlerRegistration addUploadChangeHandler(ChangeHandler handler) {
        return upload.addChangeHandler(handler);
    }

    /** prida mouse over handler */
    protected HandlerRegistration addUploadOverHandler(MouseOverHandler handler) {
        return upload.addHandler(handler, MouseOverEvent.getType());
    }

    protected HandlerRegistration addUploadOutHandler(MouseOutHandler handler) {
        return upload.addHandler(handler, MouseOutEvent.getType());
    }

    /** vnitrni komponenta fileUpload, ktera prijima mouseOver a mouseOut */
    private class InnerFileUpload extends FileUpload {

        public InnerFileUpload() {
            sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        }
    }
}
