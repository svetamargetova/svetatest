package consys.common.gwt.client.ui.comp.search;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SearchItem<T> {

    public T getItem();
}
