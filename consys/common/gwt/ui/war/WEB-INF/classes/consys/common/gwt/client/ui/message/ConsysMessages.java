package consys.common.gwt.client.ui.message;

import com.google.gwt.i18n.client.Messages;

/**
 * abecedne serazene zpravy
 * @author pepa
 */
public interface ConsysMessages extends Messages {

    String const_fieldMustEntered(String field);

    String const_fieldMustBeInRange(String field, int low, int high);

    String const_fieldMustInteger(String field);

    String const_fieldMustNumeric(String field);

    String const_fieldUpdateSuccess(String field);

    String const_fieldUpdateFailed(String field);

    String const_invalidValueInField(String field);

    String const_hoursLower(int hours);

    String const_minutesLower(int minutes);

    String const_missingParameter(String parameterName);

    String const_mustChoose(String field);

    String const_notAfter(String from, String to);

    String const_notInCache(String name);

    String const_notInRange(String field);

    String const_more(int numOfActions);

    String consysAutoFileUpload_error_invalidExtension(String validExtension);

    String consysAutoFileUpload_error_invalidExtensions(String validExtensions);

    String subbundlePanel_text_freeCapacity(@PluralCount int places);
}
