package consys.common.gwt.client.ui.comp.tag;

import consys.common.gwt.client.ui.comp.ConsysSuggestOracle;
import consys.common.gwt.shared.bo.ClientTag;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Vyhledavani tagu podle query stringu
 * @author pepa
 */
public class TagSuggestOracle extends ConsysSuggestOracle {

    // data
    private ArrayList<SearchWrapper> data;

    public TagSuggestOracle() {
        super(250, 1);
        setIsDisplayAsHTML(true);
    }

    @Override
    public void suggest(String query, int limit, SuggestCallback callback) {
        ArrayList<TagSuggestion> suggestions = new ArrayList<TagSuggestion>();

        searchResult(query, limit, suggestions);
        // pokud se neshoduje aktualne napsany tag s prvnim nalezenym, vlozi se na prvni misto
        if (suggestions.isEmpty()) {
            suggestions.add(0, new TagSuggestion(null, query, query));
        } else if (!suggestions.get(0).getReplacementString().toLowerCase().equals(query.toLowerCase())) {
            suggestions.add(0, new TagSuggestion(null, query, query));
        }

        callback.reponse(suggestions);
    }

    /** data nad kteryma se bude vyhledvat */
    public void setData(ArrayList<ClientTag> data) {
        // seradim, at se lip hleda
        Collections.sort(data, new Comparator<ClientTag>() {

            @Override
            public int compare(ClientTag o1, ClientTag o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        // do budoucna rozsekat na listy podle zacatecniho pismena, at se nemusi prohledavat jeden velky seznam
        this.data = new ArrayList<SearchWrapper>();
        for (ClientTag t : data) {
            this.data.add(new SearchWrapper(t));
        }
    }

    /** vyhleda vysledky a vlozi je do seznamu */
    private void searchResult(String query, int limit, ArrayList<TagSuggestion> suggestions) {
        // TODO: do vyhledavni neumistovat tagy, ktere jsou uz v zobrazovatku
        if (data == null) {
            return;
        }
        query = query.toLowerCase();
        int counter = 0;
        for (SearchWrapper sw : data) {
            if (sw.startsWith(query)) {
                ClientTag t = sw.getTag();
                suggestions.add(new TagSuggestion(t.getId(), t.getName(), query));
                counter++;
                if (counter > limit) {
                    break;
                }
            }
        }
    }

    /** objekt pro predkousane vyhledavani */
    private class SearchWrapper {

        private ClientTag tag;
        private String search;

        public SearchWrapper(ClientTag tag) {
            this.tag = tag;
            search = tag.getName().toLowerCase();
        }

        /** vraci pokud zacina na zadany prefix */
        public boolean startsWith(String prefix) {
            return search.startsWith(prefix);
        }

        public String getSearch() {
            return search;
        }

        public ClientTag getTag() {
            return tag;
        }
    }
}
