package consys.common.gwt.client.ui.comp.panel.abst;

/**
 *
 * @author Palo
 */
public class ValidateException extends Exception{
    private static final long serialVersionUID = 4858848959865265722L;

    public ValidateException() {
        super();
    }
    
    public ValidateException(String message) {
        super(message);
    }

}
