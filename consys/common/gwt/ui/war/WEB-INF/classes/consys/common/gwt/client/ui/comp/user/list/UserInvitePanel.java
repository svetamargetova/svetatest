package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;

/**
 * Panel s pozvankou uzivatele
 * @author pepa
 */
public class UserInvitePanel extends FlowPanel implements ActionExecutionDelegate {

    // komponenty
    private ConsysStringTextBox email;
    private ConsysStringTextBox name;
    private FlowPanel failPanel;
    // data
    private UserSearchDialog parent;

    public UserInvitePanel(UserSearchDialog parent) {
        this.parent = parent;
        setSize("350px", "255px");
        addStyleName(StyleUtils.MARGIN_TOP_20);
        failPanel = new FlowPanel();
    }

    @Override
    protected void onLoad() {
        clear();
        add(failPanel);
        initContent();
    }

    private void initContent() {
        final BaseForm form = new BaseForm();
        email = new ConsysStringTextBox(1, 320, UIMessageUtils.c.const_email());
        name = new ConsysStringTextBox(1, 128, UIMessageUtils.c.userInvitePanel_form_name());

        ActionImage invite = ActionImage.getPersonButton(UIMessageUtils.c.userInvitePanel_button_invite(), false);
        invite.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }
                if (!ValidatorUtils.isValidEmailAddress(email.getText().trim())) {
                    getFailMessage().addOrSetText(UIMessageUtils.c.const_error_invalidEmailAddress());
                    return;
                }
                parent.getDelegate().didSelect(new ClientInvitedUser(email.getText().trim(), name.getText().trim()));
                parent.hide();
            }
        });

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                parent.toSearchView();
            }
        });

        form.addRequired(0, UIMessageUtils.c.const_email(), email);
        form.addRequired(1, UIMessageUtils.c.userInvitePanel_form_name(), name);
        form.addActionMembers(2, invite, cancel);
        add(form);
    }

    /** vraci objekt pro chybove zpravy */
    private ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.addStyleName(StyleUtils.FLOAT_RIGHT);
        failMessage.addStyleName(StyleUtils.MARGIN_BOT_10);
        failMessage.setWidth(185);
        failPanel.add(failMessage);
        failPanel.add(StyleUtils.clearDiv());
        return failMessage;
    }

    @Override
    public void actionStarted() {
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }
}
