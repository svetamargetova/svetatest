package consys.common.gwt.client.widget.validator;

import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 *
 * @author pepa
 */
public class CWValidator {

    public static boolean updateUser(ClientUser user, ConsysMessage fail) {
        boolean isOk = true;

        if (!ValidatorUtils.isValidString(user.getFirstName())) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(CWMessageUtils.c.const_firstName()));
            isOk = false;
        }
        if (!ValidatorUtils.isValidString(user.getLastName())) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(CWMessageUtils.c.const_lastName()));
            isOk = false;
        }

        return isOk;
    }
    
    
}
