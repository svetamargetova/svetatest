package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.base.TitlePanel;
import consys.common.gwt.client.ui.comp.DownMenu;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;

/** 
 *
 * @author Palo
 */
public class RootPanel extends SimpleFormPanel implements TitlePanel {

    // komponenty
    private FlowPanel leftControllPart;
    private FlowPanel rightControllPart;
    private FlowPanel headPanel;
    private Label titleLabel;

    /** @param title nazev nebo null */
    public RootPanel(String title) {
        super();
        titleLabel.setText(title);
    }

    @Override
    public String getPanelTitle() {
        return titleLabel.getText();
    }

    public void setTitleText(String text) {
        titleLabel.setText(text);
    }

    @Override
    protected Widget createHeadPart() {
       
        
        // ovladacie prvky hned za nadpisom
        leftControllPart = new FlowPanel();
        leftControllPart.setStyleName(CssStyles.FLOAT_LEFT);

        // ovladacie prvky od konca
        rightControllPart = new FlowPanel();
        rightControllPart.setStyleName(CssStyles.FLOAT_RIGHT);

        // title
        titleLabel = StyleUtils.getStyledLabel("", FONT_19PX, FONT_BOLD, MARGIN_RIGHT_10, FLOAT_LEFT);
                
        // cely pas
        FlowPanel headContent = new FlowPanel();
        headContent.add(titleLabel);                        
        headContent.add(leftControllPart);
        headContent.add(rightControllPart);        

         // cely head panel
        headPanel = new FlowPanel();
        headPanel.setWidth("100%");
        headPanel.add(headContent);
        headPanel.add(StyleUtils.clearDiv());
        headPanel.add(Separator.addedStyle(MARGIN_VER_10));
        return headPanel;
    }



    /**
     * Prida widget do lavej kontrolnej oblasti ktora zacina od nadpisu a postupne
     * pokracuje z lava do prava.
     * @param widget
     */
    public void addLeftControll(Widget widget) {
        widget.addStyleName(MARGIN_LEFT_20);
        widget.addStyleName(FLOAT_LEFT);
        leftControllPart.add(widget);
    }

    /**
     * Prida widget do prave kontrolnej oblasti ktora zacina od praveho konca a
     * postupne pokracuje z lava do prava.
     * @param widget
     */
    public void addRightControll(Widget widget) {
        widget.addStyleName(MARGIN_LEFT_20);
        widget.addStyleName(FLOAT_LEFT);
        rightControllPart.add(widget);
    }
    
    /** vymaze polozky v pravej aj lavej controll casti */
    public void clearControll() {
        leftControllPart.clear();
        rightControllPart.clear();
    }    

    protected void setHeadPanelVisible(boolean visible){
        headPanel.setVisible(visible);
    }
    
    public void enableDownMenu(DownMenu.DownMenuItem...menus){
        DownMenu menu = new DownMenu();        
        for (DownMenu.DownMenuItem item : menus) {
            menu.addMenu(item);
        }
        addRightControll(menu);        
    }        
        
    
}
