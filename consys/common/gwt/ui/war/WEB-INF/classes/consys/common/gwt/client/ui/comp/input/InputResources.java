package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.CssResource.Import;
import com.google.gwt.resources.client.ImageResource;
import consys.common.gwt.client.ui.comp.action.ActionCss;

/**
 *
 * @author pepa
 */
public interface InputResources extends ClientBundle {

    public static final InputResources INSTANCE = GWT.create(InputResources.class);

    /** css styl pro input komponenty */
    @Import(value = {ActionCss.class})
    @Source("consys/common/gwt/client/ui/comp/input/input.css")
    public InputCss css();
    
    @Source("img/selectBoxArrow.png")
    public ImageResource consysSelectBoxArrow();
}
