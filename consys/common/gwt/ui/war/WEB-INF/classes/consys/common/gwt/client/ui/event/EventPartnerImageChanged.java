package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 *
 * @author Palo
 */
public class EventPartnerImageChanged extends GwtEvent<EventPartnerImageChanged.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    private String type;
    private String uuid;
    
    public EventPartnerImageChanged(String uuid, String type){
        this.uuid = uuid;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getUuid() {
        return uuid;
    }
        
    
    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onChange(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onChange(EventPartnerImageChanged event);
    }
}
