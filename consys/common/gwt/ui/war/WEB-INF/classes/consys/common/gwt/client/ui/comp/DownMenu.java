/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.ui.comp;

import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 *
 * @author palo
 */
public class DownMenu extends Composite {

    private final MenuDialog menu;
    private final ActionImage buttonImage;
    private int menuCount;

    public DownMenu() {
        menu = new MenuDialog();
        DOM.setStyleAttribute(menu.getElement(), "zIndex", "10");


        buttonImage = ActionImage.getDownArrowButton(UIMessageUtils.m.const_more(0), true);
        buttonImage.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (menu.isShowing()) {
                    menu.hide();
                } else {
                    menu.setPopupPositionAndShow(new PositionCallback() {

                        @Override
                        public void setPosition(int offsetWidth, int offsetHeight) {
                            int left = (getAbsoluteLeft() + buttonImage.getOffsetWidth()) - offsetWidth;
                            int top = getAbsoluteTop() + getOffsetHeight();

                            menu.setPopupPosition(left, top);
                            menu.setPixelSize(offsetWidth, offsetHeight);
                        }
                    });
                }

            }
        });
        sinkEvents(Event.ONCLICK);
        initWidget(buttonImage);
    }

    public void addMenu(DownMenuItem call) {
        ++menuCount;
        buttonImage.setText(UIMessageUtils.m.const_more(menuCount));
        menu.addMenu(call);
    }

    private class MenuDialog extends PopupPanel {

        private final FlowPanel menus;

        public MenuDialog() {
            super(true);
            menus = new FlowPanel();
            menus.setStyleName(ResourceUtils.system().css().downMenuContainer());
            
            SimplePanel panel = new SimplePanel();            
            panel.setStyleName(ResourceUtils.system().css().downMenu());
            panel.setWidget(menus);
            
            setWidget(panel);
        }

        public void addMenu(final DownMenuItem menu) {
            Element label = DOM.createSpan();
            Element item = DOM.createDiv();            
            item.appendChild(label);
            item.setClassName(ResourceUtils.system().css().downMenuItem());
            label.setInnerText(menu.getTitle());
            DOM.setEventListener(label.<com.google.gwt.user.client.Element>cast(), new EventListener() {

                @Override
                public void onBrowserEvent(Event event) {
                    MenuDialog.this.hide();
                    menu.doAction();
                }
            });
            DOM.sinkEvents(label.<com.google.gwt.user.client.Element>cast(), Event.ONCLICK);
            menus.getElement().appendChild(item);
        }
    }

    public interface DownMenuItem {

        public String getTitle();

        public void doAction();
    }
}
