package consys.common.gwt.client.ui.comp.panel.abst;

import consys.common.gwt.client.ui.comp.ConsysMessage;

/**
 * Interface, ktery implementuji komponenty pro vstup a je mozne je automaticky validovat
 * @author pepa
 */
public interface ValidableComponent {

    /**
     * spusti validaci dat v komponente
     * @param fail zprava do ktere se budou pridavat texty o chybe
     * @return true pokud probehla validace v poradku
     */
    public boolean doValidate(ConsysMessage fail);

    /** akce provedena pri uspesne validaci (vraceni barvy ramecku do vychozi hodnoty, ...) */
    public void onSuccess();

    /** akce provedena pri zjisteni chyby pri validaci (zmena barvy ramecku, ...) */
    public void onFail();
}
