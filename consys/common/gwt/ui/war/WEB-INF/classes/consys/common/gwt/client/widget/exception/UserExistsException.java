package consys.common.gwt.client.widget.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class UserExistsException extends ActionException {

    private static final long serialVersionUID = -6331817380354178617L;

    public UserExistsException() {
        super();
    }
}
