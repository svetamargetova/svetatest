package consys.common.gwt.client.ui.comp;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;

/**
 * Animace rozbalení smerem dolu / nahoru<br/>
 * Potrebne nastaveni:<br/>
 * - widget musi byt viditelny v rodicovi
 * - animuje se rodic, takze viditelnost/neviditelnost se nastavuje na nej
 * @author pepa
 */
public class RollAnimation extends Animation {

    // konstanty
    protected static final int ANIMATION_DURATION = 350;
    // data
    protected boolean down;
    protected Element element;
    protected Element child;
    protected int height;
    private boolean fixedHeight;

    public boolean isFixedHeight() {
        return fixedHeight;
    }

    /** pokud ma obsah rolovane komponenty konstantni vysku a blbne v IE, je potreba nastavit true */
    public void setFixedHeight(boolean fixedHeight) {
        this.fixedHeight = fixedHeight;
    }

    /** @param down urcuje zda se bude zabalovat (false) nebo rozbalovat (true) */
    public void rollWidget(Widget widget, Widget parent, boolean down) {
        cancel();
        this.down = down;

        child = widget.getElement();
        element = parent.getElement();

        run(ANIMATION_DURATION);
    }

    @Override
    protected void onStart() {
        DOM.setStyleAttribute(element, "overflow", "hidden");
        onUpdate(0d);
        UIObject.setVisible(element, true);
        // velikost je zjistitelna az kdyz je rodic viditelny (i pokud je hidden)
        height = child.getOffsetHeight();
    }

    @Override
    protected void onUpdate(double progress) {
        if (down) {
            progress = 1 - progress;
        }
        long updatedHeight = Math.round(height * progress);
        // IE zobrazi obsah rodice, kdyz je rodic 0 px vysoky
        if (updatedHeight == 0) {
            updatedHeight = 1;
        }
        DOM.setStyleAttribute(element, "height", updatedHeight + "px");
    }

    @Override
    protected void onComplete() {
        if (down) {
            UIObject.setVisible(element, false);
        }
        element.getStyle().clearOverflow();
        if (!fixedHeight) {
            element.getStyle().clearHeight();
        } else {
            DOM.setStyleAttribute(element, "height", height + "px");
        }

        child = null;
        element = null;
    }
}
