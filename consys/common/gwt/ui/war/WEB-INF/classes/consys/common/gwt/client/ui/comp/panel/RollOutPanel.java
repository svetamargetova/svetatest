package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAsyncAction;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Rozbalovací panel s ikonou
 * @author pepa
 */
public abstract class RollOutPanel extends Composite implements ActionExecutionDelegate {

    // konstanty
    public static final String TITLE_TEXT_STYLE = StyleUtils.FONT_BOLD + " " + StyleUtils.FONT_16PX;
    /** 38px */
    public static final int LEFT_PART_WIDTH = 38;
    /** 56px */
    public static final int ORDER_PART_WIDTH = 51;
    // komponenty
    private FlowPanel panel;
    private FocusPanel icon;
    private FlowPanel iconPanel;
    private SimplePanel orderPanel;
    private SimplePanel headPanel;
    private SimplePanel content;
    private FlowPanel mainPanel;
    private Waiting waiting;
    private SimplePanel pUp;
    private SimplePanel pDown;
    // data
    private boolean rollUp;
    private RollAnimation animation;
    private ConsysAsyncAction onOpenAction;

    /** vytvori zabaleny rozbalovaci panel bez nastaveneho obsahu a titulku */
    public RollOutPanel() {
        this(null, true);
    }

    /**
     * vytvori rozbalovaci panel
     * @param rollUp true pokud ma byt panel zabaleny, jinak false
     */
    public RollOutPanel(boolean rollUp) {
        this(null, rollUp);
    }

    /** vytvori zabaleny rozbalovaci panel s nastavenym titulkem */
    public RollOutPanel(String title) {
        this(title, true);
    }

    /**
     * vytvori rozbalovaci panel
     * @param title text titulku
     * @param rollUp true pokud ma byt panel zabaleny, jinak false
     */
    public RollOutPanel(String title, boolean rollUp) {
        this.rollUp = rollUp;

        icon = new FocusPanel();
        icon.setStyleName(StyleUtils.HAND);
        icon.addStyleName(rollUp ? ResourceUtils.system().css().rollPlus() : ResourceUtils.system().css().rollMinus());

        orderPanel = new SimplePanel();
        orderPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        orderPanel.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        orderPanel.setSize("0px", "0px");

        headPanel = new SimplePanel();
        headPanel.addStyleName(StyleUtils.FLOAT_LEFT);

        content = new SimplePanel();
        content.setVisible(!rollUp);

        if (title != null) {
            headPanel.setWidget(StyleUtils.getStyledLabel(title, TITLE_TEXT_STYLE));
        }

        mainPanel = new FlowPanel();
        mainPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        mainPanel.add(orderPanel);
        mainPanel.add(headPanel);
        mainPanel.add(StyleUtils.clearDiv());
        mainPanel.add(content);

        iconPanel = new FlowPanel();
        iconPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        iconPanel.setWidth(LEFT_PART_WIDTH + "px");
        iconPanel.add(icon);

        panel = new FlowPanel();
        panel.add(iconPanel);
        panel.add(mainPanel);
        panel.add(StyleUtils.clearDiv());

        initHandlers();
        initWidget(panel);
    }

    /** vytvori a priradi potrebne handlery */
    private void initHandlers() {
        icon.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                rollPanel();
            }
        });
        icon.addKeyPressHandler(new KeyPressHandler() {

            @Override
            public void onKeyPress(KeyPressEvent event) {
                if ((int) event.getCharCode() == KeyCodes.KEY_ENTER) {
                    rollPanel();
                }
            }
        });
        onOpenAction = new ConsysAsyncAction() {

            @Override
            public void onSuccess() {
                RollOutPanel.this.rollUp = !RollOutPanel.this.rollUp;
                icon.addStyleName(ResourceUtils.system().css().rollMinus());
                icon.removeStyleName(ResourceUtils.system().css().rollPlus());
                animation.rollWidget(content.getWidget(), RollOutPanel.this.rollUp);
            }

            @Override
            public void onFail() {
                // not used
            }

            @Override
            public void run() {
                // not used
            }
        };
    }

    /** nastaveni sirky prave strany */
    public void setMainPanelWidth(String width) {
        mainPanel.setWidth(width);
    }

    /** sirka pouze prave casti (bez rozklikavaciho tlacitka, ktere je siroke 38px) */
    @Override
    public void setWidth(String width) {
        panel.setWidth(width);
    }

    /** zapne zobrazovani waitingu pri pouziti jako delegat */
    public void enableWaiting() {
        if (waiting == null) {
            waiting = new Waiting(this);
            mainPanel.add(waiting);
        }
    }

    @Override
    public void actionStarted() {
        if (waiting != null) {
            waiting.show();
        }
    }

    @Override
    public void actionEnds() {
        if (waiting != null) {
            waiting.hide();
        }
    }

    /** rozbali nebo zabali panel */
    protected void rollPanel() {
        if (animation == null) {
            animation = new RollAnimation();
        }
        if (!rollUp) {
            rollUp = !rollUp;
            icon.addStyleName(ResourceUtils.system().css().rollPlus());
            icon.removeStyleName(ResourceUtils.system().css().rollMinus());
            animation.rollWidget(content.getWidget(), rollUp);
        } else {
            onOpenRoll(onOpenAction);
        }
    }

    /** akce pred rozrolovanim panelu */
    protected abstract void onOpenRoll(ConsysAsyncAction onAction);

    /** nastaveni widgetu viditelneho jak v zabalenem tak v rozbalenem stavu */
    public void setTitle(Widget widget) {
        headPanel.setWidget(widget);
    }

    /** vraci widget nastaveny v obsahu */
    public Widget getContent() {
        return content.getWidget();
    }

    /** nastavani widgetu obsahu co je pri sbalenem stavu neviditelny */
    public void setContent(Widget widget) {
        content.setWidget(widget);
    }

    /** @return true pokud je rozbaleny jinak false */
    public boolean isRolled() {
        return !rollUp;
    }

    /** @param down true pokud se ma rozbalit, false pokud se ma zabalit */
    public void rollOutPanel(boolean down) {
        rollUp = down;
        rollPanel();
    }

    /** nastavi se akce po kliknuti na radici tlacitka */
    public void setOrderersActions(boolean showOrderers, final ConsysAction upAction, final ConsysAction downAction) {
        Image u = new Image(ResourceUtils.system().orderUp());
        u.addStyleName(StyleUtils.HAND);
        if (upAction != null) {
            u.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    upAction.run();
                }
            });
        }

        Image d = new Image(ResourceUtils.system().orderDown());
        d.addStyleName(StyleUtils.HAND);
        if (downAction != null) {
            d.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    downAction.run();
                }
            });
        }

        pUp = new SimplePanel();
        pUp.addStyleName(StyleUtils.FLOAT_LEFT);
        pUp.addStyleName(StyleUtils.MARGIN_RIGHT_3);
        pUp.setWidget(u);

        pDown = new SimplePanel();
        pDown.addStyleName(StyleUtils.FLOAT_LEFT);
        pDown.setWidget(d);

        FlowPanel p = new FlowPanel();
        p.add(pUp);
        p.add(pDown);
        p.add(StyleUtils.clearDiv());

        orderPanel.setWidget(p);
        orderPanel.setSize(ORDER_PART_WIDTH + "px", "14px");
        orderPanel.removeStyleName(StyleUtils.OVERFLOW_HIDDEN);

        showUpAndDownOrderer(showOrderers);
    }

    /** zobrazi/skryje tlacitko se sipkou nahoru */
    public void showUpOrderer(boolean show) {
        if (pUp != null) {
            pUp.setVisible(show);
        }
    }

    /** zobrazi/skryje tlacitko se sipkou dolu */
    public void showDownOrderer(boolean show) {
        if (pDown != null) {
            pDown.setVisible(show);
        }
    }

    /** zobrazi/skryje obe tlacitka ordereru */
    public void showUpAndDownOrderer(boolean show) {
        showUpOrderer(show);
        showDownOrderer(show);
    }

    private class RollAnimation extends Animation {

        // konstanty
        private static final int ANIMATION_DURATION = 350;
        // data
        private boolean down;
        private Element element;
        private Element child;
        private int height;

        /** @param down urcuje zda se bude zabalovat (false) nebo rozbalovat (true) */
        public void rollWidget(Widget widget, boolean down) {
            cancel();
            this.down = down;

            child = widget.getElement();
            element = widget.getParent().getElement();

            run(ANIMATION_DURATION);
        }

        @Override
        protected void onStart() {
            DOM.setStyleAttribute(element, "overflow", "hidden");
            onUpdate(0d);
            UIObject.setVisible(element, true);
            // velikost je zjistitelna az kdyz je rodic viditelny (i pokud je hidden)
            height = child.getOffsetHeight();
        }

        @Override
        protected void onUpdate(double progress) {
            if (down) {
                progress = 1 - progress;
            }
            long updatedHeight = Math.round(height * progress);
            // IE zobrazi obsah rodice, kdyz je rodic 0 px vysoky
            if (updatedHeight == 0) {
                updatedHeight = 1;
            }
            DOM.setStyleAttribute(element, "height", updatedHeight + "px");
        }

        @Override
        protected void onComplete() {
            if (down) {
                UIObject.setVisible(element, false);
            }
            element.getStyle().clearOverflow();
            element.getStyle().clearHeight();

            child = null;
            element = null;
        }
    }
}
