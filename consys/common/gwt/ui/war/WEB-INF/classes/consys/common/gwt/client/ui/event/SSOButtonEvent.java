package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.constants.sso.SSO;

/**
 *
 * @author pepa
 */
public class SSOButtonEvent extends GwtEvent<SSOButtonEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<SSOButtonEvent.Handler> TYPE = new GwtEvent.Type<SSOButtonEvent.Handler>();
    // data
    private SSO type;

    public SSOButtonEvent(SSO type) {
        this.type = type;
    }

    public SSO getType() {
        return type;
    }

    @Override
    public GwtEvent.Type<SSOButtonEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(SSOButtonEvent.Handler handler) {
        handler.onSSOClick(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onSSOClick(SSOButtonEvent event);
    }
}
