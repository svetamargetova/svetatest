package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.ui.comp.context.ContextMenuItem;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Kontextove menu
 * @author pepa
 */
public class ContextMenu extends Composite {

    // konstanty
    private static final String HEIGHT = "15px";
    private static final String BORDER_WIDTH = "7px";
    // komponenty
    private HorizontalPanel mainPanel;

    public ContextMenu() {
        mainPanel = new HorizontalPanel();
        mainPanel.setHeight(HEIGHT);
        mainPanel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        mainPanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        mainPanel.setStyleName(StyleUtils.BACKGROUND_GRAY);

        HorizontalPanel panel = new HorizontalPanel();
        panel.setHeight(HEIGHT);
        panel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);

        Image leftBorder = new Image(ResourceUtils.system().conLeft());
        Image rightBorder = new Image(ResourceUtils.system().conRight());

        panel.add(leftBorder);
        panel.setCellWidth(leftBorder, BORDER_WIDTH);

        panel.add(mainPanel);

        panel.add(rightBorder);
        panel.setCellWidth(rightBorder, BORDER_WIDTH);

        initWidget(panel);
        setVisible(false);
    }

    /** zobrazeni kontextoveho menu */
    public void show() {
        setVisible(true);
    }

    /** skryti kontextoveho menu */
    public void hide() {
        setVisible(false);
    }

    /** prida polozku kontextoveho menu na konec */
    public void addMenuItem(ContextMenuItem item) {
        mainPanel.add(item);
    }
}
