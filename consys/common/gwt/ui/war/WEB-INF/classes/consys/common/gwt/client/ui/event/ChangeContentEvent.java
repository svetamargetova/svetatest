package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;

/**
 * Event oznamujici zmenu obsahu
 * @author pepa
 */
public class ChangeContentEvent extends GwtEvent<ChangeContentEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // vnitrni data
    private Widget content;
    private String contentTitle;

    public ChangeContentEvent(Widget content) {
        this.content = content;
    }

    public ChangeContentEvent(Widget content, String contentTitle) {
        this.content = content;
        this.contentTitle = contentTitle;
    }

    /** vraci widget */
    public Widget getContent() {
        return content;
    }

    /** vraci popisek widgetu */
    public String getContentTitle() {
        return contentTitle;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onChangeContent(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onChangeContent(ChangeContentEvent event);
    }
}
