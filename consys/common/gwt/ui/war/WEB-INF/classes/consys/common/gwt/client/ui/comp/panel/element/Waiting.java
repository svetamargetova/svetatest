package consys.common.gwt.client.ui.comp.panel.element;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Panel znazornujici ze probiha nejaka operace s panelem
 * @author pepa
 */
public class Waiting extends Composite {

    // konstanty
    private static final int IMAGE_SIZE_W = 50;
    private static final int IMAGE_SIZE_H = 52;
    private static final int IMAGE_SIZE_HALF_W = 25;
    private static final int IMAGE_SIZE_HALF_H = 26;
    private static final int IE_PADDING = 2;
    public static final String IMAGE_NAME = "img/loader/loading.gif";
    // styly
    private static final String STYLE_WAITING_PANEL = "waitingPanel";
    // komponenty
    private SimplePanel panel;
    private Widget waitingWidget;
    private Image waitingImage;
    private FlowPanel content;
    private Label label;
    // data
    private int storedWidth = -1;
    private int storedHeight = -1;
    private int storedTop = -1;
    private Timer timer;
    private boolean hide;

    public Waiting(Widget waitingWidget) {
        this.waitingWidget = waitingWidget;
        waitingImage = new Image(IMAGE_NAME);
        waitingImage.setStyleName(StyleUtils.POSITION_REL);

        label = new Label();
        label.setStyleName(ResourceUtils.css().waitingText());
        label.setVisible(false);

        content = new FlowPanel();
        content.setSize(IMAGE_SIZE_W + "px", IMAGE_SIZE_H + "px");
        content.setStyleName(ResourceUtils.css().waitingWrapper());
        content.add(waitingImage);
        content.add(label);

        panel = new SimplePanel();
        panel.setStyleName(STYLE_WAITING_PANEL);
        panel.setVisible(false);
        panel.setWidget(content);

        hide = true;

        initWidget(panel);
    }

    public void show() {
        hide = false;
        startTimer(null);
    }

    public void show(Widget correctionWidget) {
        hide = false;
        startTimer(correctionWidget);
    }

    public void showText(String text) {
        label.setVisible(StringUtils.isNotBlank(text));
        label.setText(text);
    }

    private void startTimer(final Widget correctionWidget) {
        timer = new Timer() {
            @Override
            public void run() {
                if (!hide) {
                    int width = waitingWidget.getOffsetWidth();
                    int height = waitingWidget.getOffsetHeight();
                    int top = waitingWidget.getAbsoluteTop();
                    resize(width, height, top, correctionWidget);
                    if (!panel.isVisible()) {
                        panel.setVisible(true);
                    }
                } else {
                    cancel();
                    panel.setVisible(false);
                }
            }
        };
        timer.scheduleRepeating(150);
    }

    /** zmeni velikost waitingu podle velikosti panelu, ktery ceka */
    private void resize(int width, int height, int top, Widget correctionWidget) {
        if (height != storedHeight || width != storedWidth || top != storedTop) {
            storedHeight = height;
            storedWidth = width;
            storedTop = top;

            if (width < IMAGE_SIZE_W) {
                width = IMAGE_SIZE_W;
            }
            if (height < IMAGE_SIZE_H) {
                height = IMAGE_SIZE_H;
            }
            panel.setSize((width + IE_PADDING) + "px", height + "px");

            Element panelElement = panel.getElement();
            DOM.setStyleAttribute(panelElement, "left", ((correctionWidget == null ? waitingWidget.getAbsoluteLeft()
                    : waitingWidget.getAbsoluteLeft() - correctionWidget.getAbsoluteLeft()) - IE_PADDING) + "px");
            DOM.setStyleAttribute(panelElement, "top",
                    (correctionWidget == null ? top : top - correctionWidget.getAbsoluteTop()) + "px");

            Element imageElement = content.getElement();
            DOM.setStyleAttribute(imageElement, "left", (width / 2 - IMAGE_SIZE_HALF_W) + "px");
            DOM.setStyleAttribute(imageElement, "top", (height / 2 - IMAGE_SIZE_HALF_H) + "px");
        }
    }

    /** spusteni skryti waitingu */
    public void hide() {
        hide = true;
    }
}
