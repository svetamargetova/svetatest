package consys.common.gwt.client.widget.validator;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 *
 * @author pepa
 */
public class PasswordValidator {

    // nechavam kdyby jsme nekdy v budoucnu zmenili nazor, at to nemusime zase znovu vytvaret
    //private static final String PASSWORD_STRENGTH = "(?=^.{6,}$)(?=.*\\d)(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*";
    private static final int PASSWORD_STRENGTH = 8;
    private static final int MAX_CHAR_VALUE = 127;

    /** vraci silu zadaneho hesla */
    public static PasswordStrengthEnum passwordStrength(String password) {
        if (!ValidatorUtils.isValidString(password)) {
            return PasswordStrengthEnum.NONE;
        } else {
            if (password.length() >= PASSWORD_STRENGTH) {
                return PasswordStrengthEnum.STRONG;
            } else {
                return PasswordStrengthEnum.SHORT;
            }
        }
    }

    /** vraci KeyUpHandler, ktery zjistuje silu hesla a podle hodnoty meni text a barvu labelu */
    public static KeyUpHandler passwordStrengthHandler(final TextBox passwordBox, final Label passwordStrengthLabel) {
        return new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                PasswordStrengthEnum value = passwordStrength(passwordBox.getText());
                String text = "";
                switch (value) {
                    case NONE:
                        StyleUtils.changeTextColor(passwordStrengthLabel, StyleUtils.VALUE_COLOR_CONSYS_DEFAULT);
                        break;
                    case SHORT:
                        text = CWMessageUtils.c.const_tooShortLower();
                        StyleUtils.changeTextColor(passwordStrengthLabel, StyleUtils.VALUE_COLOR_RED);
                        break;
                    case WEAK:
                        text = CWMessageUtils.c.const_weakLower();
                        StyleUtils.changeTextColor(passwordStrengthLabel, StyleUtils.VALUE_COLOR_RED);
                        break;
                    case STRONG:
                        text = CWMessageUtils.c.const_okLower();
                        StyleUtils.changeTextColor(passwordStrengthLabel, StyleUtils.VALUE_COLOR_GREEN);
                        break;
                }
                passwordStrengthLabel.setText(text);
            }
        };
    }

    /** enum udavajici silu hesla */
    public enum PasswordStrengthEnum {

        NONE, SHORT, WEAK, STRONG;
        private static final long serialVersionUID = -2124737882586056374L;
    }

    /** zkontroluje jestli jsou hesla shodna */
    public static boolean changePassword(String newPassword, String confirmPassword) {
        return newPassword.equals(confirmPassword);
    }

    /** overi jestli heslo obsahuje pouze povolene znaky */
    public static boolean permitedCharacters(String password) {
        for (int i = 0; i < password.length(); i++) {
            if (password.charAt(i) > MAX_CHAR_VALUE) {
                return false;
            }
        }
        return true;
    }
}
