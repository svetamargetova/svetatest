package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * Tlacitko pro rozkliknuti polozek v obsahu (napr. formular Bid for Review)
 * @author pepa
 */
public class UnpackingButtonBig extends FocusPanel {

    // komponenty
    private Image icon;
    // data
    private boolean unpacked;
    private ConsysAction onPackAction;
    private ConsysAction onUnpackAction;

    public UnpackingButtonBig() {
        this(false);
    }

    public UnpackingButtonBig(boolean unpacked) {
        super();
        setStyleName(ResourceUtils.system().css().unpackingBig());
        this.unpacked = unpacked;

        icon = new Image(unpacked
                ? ResourceUtils.system().unpackingBigButtonUp()
                : ResourceUtils.system().unpackingBigButtonDown());
        icon.setStyleName(unpacked
                ? ResourceUtils.system().css().unpackingBigIconUp()
                : ResourceUtils.system().css().unpackingBigIconDown());
        setWidget(icon);

        initHandlers();
    }

    /** vytvori potrebne handlery */
    private void initHandlers() {
        addMouseOverHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                icon.setResource(unpacked
                        ? ResourceUtils.system().unpackingBigButtonUpOn()
                        : ResourceUtils.system().unpackingBigButtonDownOn());
            }
        });
        addMouseOutHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                icon.setResource(unpacked
                        ? ResourceUtils.system().unpackingBigButtonUp()
                        : ResourceUtils.system().unpackingBigButtonDown());
            }
        });
        addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                unpacked = !unpacked;

                icon.setResource(unpacked
                        ? ResourceUtils.system().unpackingBigButtonUpOn()
                        : ResourceUtils.system().unpackingBigButtonDownOn());
                icon.setStyleName(unpacked
                        ? ResourceUtils.system().css().unpackingBigIconUp()
                        : ResourceUtils.system().css().unpackingBigIconDown());

                if (unpacked) {
                    if (onPackAction != null) {
                        onPackAction.run();
                    }
                } else {
                    if (onUnpackAction != null) {
                        onUnpackAction.run();
                    }
                }
            }
        });
    }

    /** akce, ktera se ma provest na sbaleni */
    public void setOnPackAction(ConsysAction onPackAction) {
        this.onPackAction = onPackAction;
    }

    /** akce, ktera se ma provest na rozbaleni */
    public void setOnUnpackAction(ConsysAction onUnpackAction) {
        this.onUnpackAction = onUnpackAction;
    }
}
