package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.ui.SimplePanel;

/**
 *
 * @author palo
 */
public abstract class Itemizer extends SimplePanel {

    private DataListPanel parentList;

    public void setParentList(DataListPanel parentList) {
        this.parentList = parentList;
    }

    public DataListPanel getParentList() {
        return parentList;
    }

    public abstract void refresh();

    /**
     * Kazdy itemizer obsahuje unikatny tag ktory sa pridava do generickej akcie
     * kde na strane servera sa na zaklade tychto tagov vytvori prislusny dotaz.
     * @return unikatny tag
     */
    public abstract int getTag();
}
