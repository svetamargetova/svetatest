package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.layout.panel.PanelItemAction;
import consys.common.gwt.client.ui.layout.panel.PanelItemType;

/**
 *
 * @author pepa
 */
public class PanelItemEvent extends GwtEvent<PanelItemEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // vnitrni data
    private PanelItem panelItem;
    private PanelItemAction action;
    private int position = -1;
    private PanelItemType type;

    public PanelItemEvent(PanelItemAction action) {
        this.action = action;
    }

    public PanelItemEvent(PanelItemAction action, PanelItemType type) {
        this.action = action;
        this.type = type;
    }

    public PanelItemEvent(PanelItem panelItem, PanelItemAction action, PanelItemType type) {
        this.panelItem = panelItem;
        this.action = action;
        this.type = type;
    }

    public PanelItemEvent(PanelItem panelItem, PanelItemAction action, PanelItemType type, int position) {
        this.panelItem = panelItem;
        this.action = action;
        this.type = type;
        this.position = position;
    }

    /** vraci widget */
    public PanelItem getPanelItem() {
        return panelItem;
    }

    /** vraci akci, ktera se ma provest */
    public PanelItemAction getAction() {
        return action;
    }

    /** vraci pozici, na kterou ma byt polozka umistena */
    public int getPosition() {
        return position;
    }

    /** vraci typ panelu do ktereho je urcen */
    public PanelItemType getType() {
        return type;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onPanelItem(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onPanelItem(PanelItemEvent event);
    }
}
