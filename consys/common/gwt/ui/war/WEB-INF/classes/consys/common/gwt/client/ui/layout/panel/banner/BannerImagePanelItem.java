package consys.common.gwt.client.ui.layout.panel.banner;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.ui.layout.panel.PanelItem;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class BannerImagePanelItem extends PanelItem{

    private String imageId;
    private Image image;
    
    public BannerImagePanelItem(String id) {
        super(false);
        imageId = id;
    }    
    
    @Override
    public String getName() {
        return "banner-item";
    }

    @Override
    public Widget getTitle() {
        return null;
    }

    @Override
    public Widget getContent() {
        image = new Image(URLFactory.loadImageUrl(imageId));
        image.setWidth("220px");
        return image;
    }
    
    public void setUuid(String uuid) {        
        image.setUrl(URLFactory.loadImageUrl(uuid));       
    }
}
