package consys.common.gwt.client.ui.comp;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;
import java.util.Date;

/**
 * Komponenta pro vyber mesice a roku
 * @author pepa
 */
public class MonthYearDatePicker extends Composite implements ValidableComponent {

    // konstanty
    public static final String[] MONTHS = new String[]{
        UIMessageUtils.c.const_date_monthJanuary(),
        UIMessageUtils.c.const_date_monthFebruary(),
        UIMessageUtils.c.const_date_monthMarch(),
        UIMessageUtils.c.const_date_monthApril(),
        UIMessageUtils.c.const_date_monthMay(),
        UIMessageUtils.c.const_date_monthJune(),
        UIMessageUtils.c.const_date_monthJuly(),
        UIMessageUtils.c.const_date_monthAugust(),
        UIMessageUtils.c.const_date_monthSeptember(),
        UIMessageUtils.c.const_date_monthOctober(),
        UIMessageUtils.c.const_date_monthNovember(),
        UIMessageUtils.c.const_date_monthDecember()};
    private static final String DEFAULT_YEAR_WIDTH = "40px";
    // komponenty
    private SelectBox<Integer> monthBox;
    private ConsysIntTextBox yearBox;
    // data
    private boolean required = false;

    public MonthYearDatePicker() {
        yearBox = new ConsysIntTextBox(1950, DateTimeUtils.getYear(new Date()), UIMessageUtils.c.const_date_year());
        yearBox.setWidth(DEFAULT_YEAR_WIDTH);
        yearBox.addStyleName(StyleUtils.FLOAT_LEFT);
        yearBox.setMaxLength(4);

        monthBox = new SelectBox<Integer>();
        monthBox.setWidth(95);
        ArrayList<SelectBoxItem<Integer>> items = new ArrayList<SelectBoxItem<Integer>>();
        for (int i = 0; i < MONTHS.length; i++) {
            items.add(new SelectBoxItem(i, MONTHS[i]));
        }
        monthBox.setItems(items);
        monthBox.selectFirst(true);
        monthBox.addStyleName(StyleUtils.MARGIN_RIGHT_5);
        monthBox.addStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel panel = new FlowPanel();
        panel.add(monthBox);
        panel.add(yearBox);
        panel.add(StyleUtils.clearDiv());

        initWidget(panel);
    }

    /** vrací zvolený měsíc, pokud nebyl zvolen, tak -1, (prvni mesic ma cislo 0) */
    public int getMonth() {
        SelectBoxItem<Integer> item = monthBox.getSelectedItem();
        return item == null ? -1 : item.getItem();
    }

    /** nastavi mesic, rozsah (0-11) */
    public void setMonth(int value) {
        monthBox.selectItem(value);
    }

    /** vrací zvolený rok (1950-letos) */
    public int getYear() {
        return yearBox.getTextInt();
    }

    /** nastavi rok (hodnota musi byt 1950-letos) */
    public void setYear(int value) {
        yearBox.setText(value);
    }

    /** vraci vybrany datum, pokud neni vybrany mesic nahrazuje se prvnim mesicem v roce, pokud neni vybrany rok nahrazuje se 2010 */
    public Date getDate() {
        if (getMonth() == -1 || getYear() < 1950 || getYear() > DateTimeUtils.getYear(new Date())) {
            return null;
        }
        int month = getMonth() + 1;
        int year = getYear();
        String dateText = "1." + month + "." + year;
        DateTimeFormat format = DateTimeFormat.getFormat("d.M.y");
        return format.parse(dateText);
    }

    /** nastavi datum do komponenty */
    public void setDate(Date date) {
        if (date != null) {
            setMonth(date.getMonth());
            setYear(DateTimeUtils.getYear(date));
        }
    }

    /** vynuluje data */
    public void reset() {
        monthBox.selectItemByIndex(0);
        yearBox.setText(DateTimeUtils.getYear(new Date()));
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        boolean out = true;
        if (isVisible() == true && getParent() != null) {
            if (!yearBox.doValidate(fail)) {
                return false;
            }
            out = !isRequired() ? true : getDate() == null ? false : true;
        }
        return out;
    }

    @Override
    public void onSuccess() {
        yearBox.removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        yearBox.addStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }
}
