package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 *
 * @author pepa
 */
@ImportedWithPrefix("wrapper")
public interface WrapperCss extends CssResource {

    String contentPanelWrapper();

    String contentPanelWrapperTop();

    String contentPanelWrapperContent();

    String contentPanelWrapperBottom();

    // common wrapper
    String textAreaWrapper();

    String textAreaWrapperTop();

    String textAreaWrapperTopLeft();

    String textAreaWrapperTopCenter();

    String textAreaWrapperTopRight();

    String textAreaWrapperCenter();

    String textAreaWrapperCenterLeft();

    String textAreaWrapperCenterCenter();

    String textAreaWrapperCenterRight();

    String textAreaWrapperBottom();

    String textAreaWrapperBottomLeft();

    String textAreaWrapperBottomCenter();

    String textAreaWrapperBottomRight();

    String textAreaWrapperOver();

    String textAreaWrapperNormal();

    String textAreaWrapperDisabled();

    String textAreaWrapperContentNormal();

    String textAreaWrapperContentEdit();

    String textAreaWrapperContentOver();

    String textAreaWrapperContentDisabled();
}
