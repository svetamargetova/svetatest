package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.user.client.ui.IsWidget;

/**
 *
 * @author pepa
 */
public interface ConsysTextBoxInterface extends IsWidget {

    /** je komponenta zapnuta? */
    boolean isEnabled();

    /** nastavuje zda je komponenta zapnuta nebo vypnuta */
    void setEnabled(boolean enabled);

    void addStyleName(String styleName);

    void setText(String text);

    String getText();
}
