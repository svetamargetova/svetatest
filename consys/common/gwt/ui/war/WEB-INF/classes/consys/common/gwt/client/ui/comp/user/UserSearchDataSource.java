package consys.common.gwt.client.ui.comp.user;


import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONParser;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.solr.bo.SearchUserSolrResponse;
import consys.common.gwt.client.solr.bo.SearchedUser;
import consys.common.gwt.client.ui.comp.user.list.UserListItem;
import consys.common.gwt.client.ui.comp.search.SearchDataSource;
import consys.common.gwt.client.ui.comp.search.SearchDataSource.SearchCallback;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import java.util.ArrayList;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserSearchDataSource implements SearchDataSource<UserListItem> {

    private static final Logger logger = LoggerFactory.getLogger(UserSearchDataSource.class);

    // konstanty
    private static final String URL_PART = "search/?wt=json&rows=10&start=0&q=user:";

    @Override
    public void searchBy(String input, final SearchCallback<UserListItem> callback) {
        try {
            RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, URLFactory.serverURL() + URL_PART + input);

            rb.sendRequest(null, new RequestCallback() {

                @Override
                public void onResponseReceived(Request request, Response response) {
                    int returnCode = response.getStatusCode();
                    if (returnCode == 200) {
                        // ok 
                        SearchUserSolrResponse rspn = JSONParser.parseStrict(response.getText()).isObject().getJavaScriptObject().cast();
                         
                        ArrayList<UserListItem> list = new ArrayList<UserListItem>();

                        for (int i = 0; i < rspn.getResponse().getNumFound(); i++) {
                            SearchedUser searchedUser = rspn.getResponse().getDocs().get(i);
                            ClientUserWithAffiliationThumb csut = new ClientUserWithAffiliationThumb();
                            csut.setUuid(searchedUser.getUuid());
                            csut.setName(searchedUser.getUserNames().get(0));
                            csut.setPosition(searchedUser.getOrganizations().get(0));
                            // TODO: user portrait uuid
                            list.add(new UserListItem(csut));
                        }

                        callback.didFind(list);
                    } else {
                        // chyba
                        // TODO: zobrazit chybu?
                    }
                }

                @Override
                public void onError(Request request, Throwable exception) {
                    logger.error("request send failed", exception);
                    // TODO: zobrazit chybu?
                }
            });
        } catch (RequestException ex) {
            logger.error("request send failed", ex);
        }
    }
}
