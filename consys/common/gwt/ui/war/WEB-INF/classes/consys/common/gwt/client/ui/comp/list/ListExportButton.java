package consys.common.gwt.client.ui.comp.list;

import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.PopupPanel.PositionCallback;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.ui.comp.ImageLink;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListExportButton extends Composite {

    private ListExportTypeEnum[] types;

    public ListExportButton(final DataListPanel parent) {
        final Image buttonImage = new Image(ResourceUtils.system().dataListExportImage());
        FocusPanel focusPanel = new FocusPanel(buttonImage);
        focusPanel.addStyleName(ResourceUtils.system().css().dataListExportButton());
        initWidget(focusPanel);
        focusPanel.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // vytvorime dialog
                if (types.length > 1) {
                    showExportTypesDialog();
                } else {
                    // rovno stiahneme
                    Window.open(createUrl(parent, types[0]), "_blank", "");
                }
            }

            private void showExportTypesDialog() {
                final ExportFileTypeDialog dialog = new ExportFileTypeDialog(parent);
                DOM.setStyleAttribute(dialog.getElement(), "zIndex", "10");
                dialog.setPopupPositionAndShow(new PositionCallback() {

                    @Override
                    public void setPosition(int offsetWidth, int offsetHeight) {
                        int left = getAbsoluteLeft() + (buttonImage.getWidth() >> 1);
                        int top = getAbsoluteTop() + getOffsetHeight();

                        int half = offsetWidth >> 1;
                        dialog.setPopupPosition(left - half, top);
                        dialog.setPixelSize(offsetWidth, offsetHeight);
                    }
                });
            }
        });
        sinkEvents(Event.ONCLICK);
    }

    public void setExportTypes(ListExportTypeEnum... e) {
        if(e == null || e.length == 0){
            throw new NullPointerException("Missing at least one export type.");
        }
        Arrays.sort(e, new Comparator<ListExportTypeEnum>() {

            @Override
            public int compare(ListExportTypeEnum o1, ListExportTypeEnum o2) {
                return o1.name().compareTo(o2.name());
            }
        });
        types = e;
    }

    private final class ExportFileTypeDialog extends PopupPanel {

        public ExportFileTypeDialog(DataListPanel parent) {
            super(true);
            setWidget(createContent(parent));
        }

        protected Widget createContent(DataListPanel parent) {
            FlowPanel typesPanel = new FlowPanel();
            typesPanel.setStyleName(ResourceUtils.system().css().dataListExportDialog());

            for (int i = 0; i < types.length; i++) {
                Widget option = createOption(types[i], parent);
                option.addStyleName(ResourceUtils.system().css().exportOption());
                typesPanel.add(option);
            }
            return typesPanel;
        }

        private Widget createOption(ListExportTypeEnum e, DataListPanel parent) {

            String link = createUrl(parent, e);
            switch (e) {
                case EXCEL:
                    return new ImageLink(new Image(ResourceUtils.system().commonXls()), link);
                case PDF:
                    return new ImageLink(new Image(ResourceUtils.system().commonPdf()), link);
            }
            throw new IllegalArgumentException("Unsupported export format!");

        }
    }
    static final String PARAM_TAG = "t";
    static final String PARAM_FILTER = "f";
    static final String PARAM_FILTER_INT_VALUE = "fi";
    static final String PARAM_FILTER_STRING_VALUE = "fs";
    static final String PARAM_ORDER_PREFIX = "fo";
    static final String PARAM_OUTPUT_TYPE = "out";

    private static String createUrl(DataListPanel parent, ListExportTypeEnum type) {
        try {
            // pridame & pretoze uz tam je defaultne pridany eid=xxxx
            StringBuilder sb = new StringBuilder("&");
            EventURLFactory.appendQuery(sb, PARAM_TAG, parent.getDataListTag());
            EventURLFactory.appendQuery(sb, PARAM_FILTER, parent.getActiveFilter().getTag());
            EventURLFactory.appendQuery(sb, PARAM_FILTER_INT_VALUE, parent.getActiveFilter().getFilter().getIntValue());
            EventURLFactory.appendQuery(sb, PARAM_FILTER_STRING_VALUE, parent.getActiveFilter().getFilter().getStringValue());
            EventURLFactory.appendQuery(sb, PARAM_OUTPUT_TYPE, type.name());
            List<Integer> orderers = parent.getOrderers();
            for (int i = 0; i < orderers.size(); i++) {
                EventURLFactory.appendQuery(sb, PARAM_ORDER_PREFIX + i, orderers.get(i));
            }

            return EventURLFactory.downloadListExport(sb.toString());
        } catch (NotInCacheException ex) {
            throw new NullPointerException("Can't create list export download link beacuse Event is missing in local Cache");
        }
    }
}
