package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class InputTextWrapper extends Composite {

    // konstanty
    public static final int BORDER_WIDTH = 8;
    // komponenty
    private FlowPanel panel;
    private SimplePanel content;
    // data
    private Widget input;
    private final int width;
    private final int innerWidth;
    private WrapperState state;
    private ConsysAction onEnterToEditAction;
    private HandlerRegistration focusRegistration;
    private HandlerRegistration blurRegistration;

    /** enum stavu InputTextWrapperu */
    private enum WrapperState {

        NORMAL, EDIT, OVER, DISABLED;
        private static final long serialVersionUID = -1362374762863392880L;
    }

    public InputTextWrapper(Widget input, int width) {
        this.input = input;
        this.width = width;
        innerWidth = width - (2 * BORDER_WIDTH);

        WrapperResources.INSTANCE.css().ensureInjected();

        content = new SimplePanel();
        content.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperCenterCenter());
        content.setWidth(innerWidth + "px");
        content.setWidget(input);

        panel = new FlowPanel();
        panel.setWidth(width + "px");

        modeNormal();

        initWidget(panel);
        initHandlers();
    }

    @Override
    protected void onLoad() {
        generate();
    }

    private void generate() {
        panel.clear();
        topOrBottom(true);
        center();
        topOrBottom(false);
    }

    private void topOrBottom(boolean top) {
        SimplePanel left = new SimplePanel();
        left.setStyleName(top
                ? WrapperResources.INSTANCE.css().textAreaWrapperTopLeft()
                : WrapperResources.INSTANCE.css().textAreaWrapperBottomLeft());

        SimplePanel center = new SimplePanel();
        center.setStyleName(top
                ? WrapperResources.INSTANCE.css().textAreaWrapperTopCenter()
                : WrapperResources.INSTANCE.css().textAreaWrapperBottomCenter());
        center.setWidth(innerWidth + "px");

        SimplePanel right = new SimplePanel();
        right.setStyleName(top
                ? WrapperResources.INSTANCE.css().textAreaWrapperTopRight()
                : WrapperResources.INSTANCE.css().textAreaWrapperBottomRight());

        FlowPanel p = new FlowPanel();
        p.setStyleName(top
                ? WrapperResources.INSTANCE.css().textAreaWrapperTop()
                : WrapperResources.INSTANCE.css().textAreaWrapperBottom());
        p.add(left);
        p.add(center);
        p.add(right);
        p.add(StyleUtils.clearDiv());
        panel.add(p);
    }

    private void center() {
        SimplePanel left = new SimplePanel();
        left.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperCenterLeft());

        SimplePanel right = new SimplePanel();
        right.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperCenterRight());
        DOM.setStyleAttribute(right.getElement(), "left", (width - BORDER_WIDTH) + "px");

        DOM.setStyleAttribute(content.getElement(), "left", BORDER_WIDTH + "px");

        FlowPanel p = new FlowPanel();
        p.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperCenter());
        p.add(left);
        p.add(content);
        p.add(right);

        panel.add(p);
    }

    /** nastaveni akce, ktera se spusti pri prechodu do editacniho modu */
    public void setOnEnterToEditAction(ConsysAction onEnterToEditAction) {
        this.onEnterToEditAction = onEnterToEditAction;
    }

    private void initHandlers() {
        sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        addHandler(overHandler(), MouseOverEvent.getType());
        addHandler(outHandler(), MouseOutEvent.getType());
        initInputHandlers();
    }

    private void initInputHandlers() {
        if (input instanceof TextBoxBase) {
            removeInputRegistrations();
            TextBoxBase i = (TextBoxBase) input;
            focusRegistration = i.addFocusHandler(focusHandler());
            blurRegistration = i.addBlurHandler(blurHandler());
        }
    }

    private void removeInputRegistrations() {
        if (focusRegistration != null) {
            focusRegistration.removeHandler();
        }
        if (blurRegistration != null) {
            blurRegistration.removeHandler();
        }
    }

    /** vlozeni jine vstupni komponenty do wrapperu (mela by rozmerove odpovidat puvodni) */
    public void setInput(Widget input) {
        this.input = input;
        content.setWidget(input);
        modeNormal();
        initInputHandlers();
    }

    /** prida stredni casti pozadovany styl */
    public void addCenterWrapperStyle(String style) {
        content.addStyleName(style);
    }

    private MouseOverHandler overHandler() {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                if (input instanceof TextBoxBase) {
                    TextBoxBase i = (TextBoxBase) input;
                    if (!state.equals(WrapperState.EDIT) && i.isEnabled()) {
                        modeOver();
                    }
                }
            }
        };
    }

    private MouseOutHandler outHandler() {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                if (input instanceof TextBoxBase) {
                    TextBoxBase i = (TextBoxBase) input;
                    if (!state.equals(WrapperState.EDIT) && i.isEnabled()) {
                        modeNormal();
                    }
                }
            }
        };
    }

    private FocusHandler focusHandler() {
        return new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                if (input instanceof TextBoxBase) {
                    TextBoxBase i = (TextBoxBase) input;
                    if (i.isEnabled()) {
                        modeEdit();
                    }
                }
            }
        };
    }

    private BlurHandler blurHandler() {
        return new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (input instanceof TextBoxBase) {
                    TextBoxBase i = (TextBoxBase) input;
                    if (i.isEnabled()) {
                        modeNormal();
                    }
                }
            }
        };
    }

    public final void modeNormal() {
        state = WrapperState.NORMAL;
        panel.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperNormal());
        input.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperContentNormal());
    }

    public void modeEdit() {
        state = WrapperState.EDIT;
        panel.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapper());
        input.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperContentEdit());

        if (onEnterToEditAction != null) {
            onEnterToEditAction.run();
        }
    }

    public void modeOver() {
        state = WrapperState.OVER;
        panel.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperOver());
        input.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperContentOver());
    }

    public void modeDisabled() {
        state = WrapperState.DISABLED;
        panel.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperDisabled());
        input.setStyleName(WrapperResources.INSTANCE.css().textAreaWrapperContentDisabled());
    }
}
