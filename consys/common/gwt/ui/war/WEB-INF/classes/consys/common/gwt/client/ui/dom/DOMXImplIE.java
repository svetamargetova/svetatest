package consys.common.gwt.client.ui.dom;

import com.google.gwt.user.client.Element;

/**
 * Implementace DOMX pro IE obecne
 * @author pepa
 */
public class DOMXImplIE extends DOMXImpl {

    // mrdka IE -> (zoom = 1), nekde uvadi jeste width:100%
    @Override
    public native void setOpacity(Element e, double opacity) /*-{
        var roundOpacity = Math.round(opacity * 100);
        if(roundOpacity < 1) {
            roundOpacity = 1;
        }
        e.style.zoom = 1;
        e.style.filter = "alpha(opacity=" + roundOpacity + ")";
    }-*/;
    
    public int getOffsetHeightDiff() {
        return 20;
    }
}
