package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.form.Validator;
import consys.common.gwt.client.ui.comp.input.validator.DoubleValidator;
import consys.common.gwt.client.ui.comp.input.validator.IntegerValidator;
import consys.common.gwt.client.ui.comp.input.validator.StringValidator;
import consys.common.gwt.client.ui.comp.wrapper.InputTextWrapper;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Nase vlastni komponenta TextBoxu
 *
 * @author pepa
 */
public class ConsysTextBox extends Composite implements Validable,  ConsysTextBoxInterface {

    static {
        InputResources.INSTANCE.css().ensureInjected();
    }
    private static final Logger logger = LoggerFactory.getLogger(ConsysTextBox.class);
    // konstanty
    public static final String NO_NUMBER_REG_EXP = "[^0-9]";
    // komponenty
    private final TextBox box;
    // data
    private ConsysTextBoxType type;
    private final int min;
    private final int max;
    private HandlerRegistration keyUpRegistration;
    private HandlerRegistration blurRegistration;
    private final List<Validator<String>> validators;
    

    public ConsysTextBox(int min, int max) {
        this(min, max, ConsysTextArea.WIDTH);
    }

    public ConsysTextBox(int min, int max, int width) {
        this.min = min;
        this.max = max;
        this.validators = new ArrayList<Validator<String>>();



        box = new TextBox();
        box.setStyleName(InputResources.INSTANCE.css().consysTextBoxInput());
        box.setWidth((width - (2 * InputTextWrapper.BORDER_WIDTH)) + "px");

        setInputType(ConsysTextBoxType.STRING);

        InputTextWrapper wrapper = new InputTextWrapper(box, width);
        wrapper.addCenterWrapperStyle(InputResources.INSTANCE.css().consysTextBoxCenterWrapper());

        FlowPanel textBoxPanel = new FlowPanel();
        textBoxPanel.setStyleName(InputResources.INSTANCE.css().consysTextBox());
        textBoxPanel.add(wrapper);
        initWidget(textBoxPanel);
    }

    public final void setInputType(ConsysTextBoxType type) {
        if (this.type != null && this.type == type) {
            // nerobime nic lebo je to to iste
            logger.info("Not changing input type to: " + type + " because its already set.");
            return;
        }
        logger.info("Changing input type to: " + type);
        this.type = type;
        validators.clear();
        removeHandlerRegistrations();
        switch (type) {
            case DOUBLE:
                addDoubleHandlers();
                addCustomValidator(new DoubleValidator(min, max));
                break;
            case INTEGER:
                addIntegerHandlers();
                addCustomValidator(new IntegerValidator(min, max));
                break;
            case STRING:
                addCustomValidator(new StringValidator(min, max));
                break;
        }
    }

    public void clearValidators(){
        validators.clear();
    }
    
    public List<Validator<String>> getValidators(){
        return validators;
    }
    
    private void removeHandlerRegistrations() {
        if (keyUpRegistration != null) {
            keyUpRegistration.removeHandler();
        }
        if (blurRegistration != null) {
            blurRegistration.removeHandler();
        }
    }

    /** vraci true pokud je komponenta povolena */
    @Override
    public boolean isEnabled() {
        return box.isEnabled();
    }

    /** enabluje/disabluje komponentu */
    @Override
    public void setEnabled(boolean value) {
        box.setEnabled(value);
        if (value) {
            box.removeStyleName(InputResources.INSTANCE.css().disabled());
        } else {
            StyleUtils.addStyleNameIfNotSet(box, InputResources.INSTANCE.css().disabled());
        }
    }

    /** nastavi nazev komponenty */
    public void setName(String name) {
        box.setName(name);
    }

    /** prida / zrusi focus na komponentu */
    public void setFocus(boolean focused) {
        box.setFocus(focused);
    }

    /** vraci textovou hodnotu */
    @Override
    public String getText() {
        return box.getText().trim();
    }

    /** nastavuje textovou hodnotu */
    @Override
    public void setText(String text) {
        box.setText(text);
    }

    /**
     * @throws IllegalArgumentException pokud neni nastaven spravny typ chovani
     * ConsysTextBoxu
     * @throws NumberFormatException pokud se nepodari rozparsovat zadany vstup
     * jako double
     */
    public double getDouble() {
        if (!type.equals(ConsysTextBoxType.DOUBLE)) {
            throw new IllegalArgumentException("This is not double text box. If you want use this call behaviorDouble first.");
        }
        return Double.parseDouble(getText().replaceAll(",", "."));
    }

    public void setDouble(double value) {
        setText(FormUtils.doubleFormat.format(value));
    }

    /**
     * @throws IllegalArgumentException pokud neni nastaven spravny typ chovani
     * ConsysTextBoxu
     * @throws NumberFormatException pokud se nepodari rozparsovat zadany vstup
     * jako int
     */
    public int getInteger() {
        if (!type.equals(ConsysTextBoxType.INTEGER)) {
            throw new IllegalArgumentException("This is not integer text box. If you want use this call behaviorInteger first.");
        }
        return Integer.parseInt(getText());
    }

    public void setInteger(int value) {
        setText(String.valueOf(value));
    }

    @Override
    public String validate() {
        if (box.isEnabled()) {
            logger.info("Validating ");
            for (Validator<String> v : validators) {
                String failText = v.validate(getText());
                if (failText != null) {
                    logger.debug(" failed: " + v.toString() + " message: " + failText);
                    return failText;
                }
                logger.debug(" checked: " + v.toString());
            }
        }
        return null;
    }

    private void addDoubleHandlers() {
        keyUpRegistration = box.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                // kontrola na spravnost povolenych znaku
                String text = getText();
                if (!text.matches("[\\-]?[0-9]+\\.?[0-9]*")) {
                    int dot = text.indexOf(".");
                    if (text.startsWith("-")) {
                        text = "-" + replaceNoNumbers(text.substring(1), dot);
                    } else {
                        text = replaceNoNumbers(getText(), dot);
                    }
                    setText(text);
                }
                // kontrola na rozsah
                try {
                    double number = Double.parseDouble(text);
                    if (number > max) {
                        setDouble(max);
                    }
                } catch (NumberFormatException ex) {
                    // bylo zatim zadano jen minusko a nelze prevest na cislo, nic nedelam
                }
            }
        });
        blurRegistration = box.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                String text = getText();
                if (text.equals("-") || text.equals(".") || text.equals("-.")) {
                    setText("");
                }
            }
        });
    }

    private void addIntegerHandlers() {
        keyUpRegistration = box.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                // kontrola na spravnost povolenych znaku
                String text = getText();
                if (!text.matches("[\\-]?[0-9]+")) {
                    if (text.startsWith("-")) {
                        text = "-" + text.substring(1).replaceAll(NO_NUMBER_REG_EXP, "");
                    } else {
                        text = getText().replaceAll(NO_NUMBER_REG_EXP, "");
                    }
                    setText(text);
                }
                // kontrola na rozsah
                try {
                    int number = Integer.parseInt(text);
                    if (number > max) {
                        setInteger(max);
                    }
                } catch (NumberFormatException ex) {
                    // bylo zatim zadano jen minusko a nelze prevest na cislo, nic nedelam
                }
            }
        });
        blurRegistration = box.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (getText().equals("-")) {
                    setText("");
                }
            }
        });
    }

    /**
     * vyhodi neplatne znaky pro cislo
     *
     * @param dot pozice desetinne tecky/carky
     */
    private String replaceNoNumbers(String text, int dot) {
        if (dot != -1) {
            String part1 = text.substring(0, dot);
            String part2 = text.substring(dot, text.length());
            return part1.replaceAll(NO_NUMBER_REG_EXP, "")
                    + "."
                    + part2.replaceAll(NO_NUMBER_REG_EXP, "");
        } else {
            return text.replaceAll(NO_NUMBER_REG_EXP, "");
        }
    }      

    public TextBox getTextBox() {
        return box;
    }

    public void addCustomValidator(Validator<String> validator) {
        validators.add(validator);
    }

    /** typ dat v ConsysTextBox */
    public enum ConsysTextBoxType {

        STRING, INTEGER, DOUBLE;
        private static final long serialVersionUID = -8386821467772818773L;
    }
}
