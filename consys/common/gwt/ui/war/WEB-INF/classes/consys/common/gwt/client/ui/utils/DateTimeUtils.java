package consys.common.gwt.client.ui.utils;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.shared.bo.ClientDate;
import consys.common.gwt.shared.bo.ClientDateTime;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class DateTimeUtils {

    // konstanty
    /** pocatecni rok pro datum v js */
    private static final int JS_START_YEAR = 1900;
    private static final int MINUTE_IN_MILISECONDS = 60000;
    private static final int HOUR_IN_MILISECONDS = 3600000;
    /** formater vypisujici datum v klasickem formatu dd.MM.yyyy */
    public static final DateTimeFormat classicDateFormat = DateTimeFormat.getFormat("dd.MM.yyyy");
    /** formater vypisujici datum ve formatu dd.MM. */
    public static final DateTimeFormat dateWithouYearFormat = DateTimeFormat.getFormat("dd.MM.");
    /** fromater datumu na datum a cas */
    public static final DateTimeFormat dateAndTimeFormat = DateTimeFormat.getFormat("dd.MM.yyyy HH:mm");
    /** formater datumu na hodiny a minuty */
    public static final DateTimeFormat hourAndMinutesFormat = DateTimeFormat.getFormat("HH:mm");
    /** formater datumu mesic den, rok napr Jan 21, 2010 */
    public static final DateTimeFormat monthDayYearFormat = DateTimeFormat.getFormat("LLLL dd, yyyy");
    /** formater datumu mesic den, cas napr Jan 21, 12:50 */
    public static final DateTimeFormat monthDayTimeFormat = DateTimeFormat.getFormat("LLLL dd, HH:mm");
    /** formater datumu mesic den rok, cas napr Jan 21 2011, 12:50 */
    public static final DateTimeFormat monthDayYearTimeFormat = DateTimeFormat.getFormat("LLLL dd yyyy, HH:mm");
    /** formater datumu na dny v tydnu */
    public static final DateTimeFormat weekDayFormat = DateTimeFormat.getFormat("cccc");
    /** formater ktery vraci casovou zonu */
    public static final DateTimeFormat timeZoneFormat = DateTimeFormat.getFormat("ZZZ");
    /** formater ktery vraci hodiny */
    public static final DateTimeFormat hourFormat = DateTimeFormat.getFormat("H");
    /** formater ktery vraci minuty */
    public static final DateTimeFormat minuteFormat = DateTimeFormat.getFormat("m");
    // pro parsovani do ClientDate
    private static final DateTimeFormat dFormat = DateTimeFormat.getFormat("d");
    private static final DateTimeFormat mFormat = DateTimeFormat.getFormat("M");
    private static final DateTimeFormat yFormat = DateTimeFormat.getFormat("yyyy");

    /** vraci datum ve standardnim formatu dd.MM.yyyy */
    public static String getDate(Date date) {
        return date == null ? FormUtils.MDASH : classicDateFormat.format(date);
    }

    /** vraci datum ve standardnim formatu dd.MM.yyyy */
    public static String getDate(Date date, TimeZone timeZone) {
        return date == null ? FormUtils.MDASH : classicDateFormat.format(date, timeZone);
    }

    /** vraci datum ve standardnim formatu dd.MM.yyyy */
    public static String getDate(ClientDate date) {
        if (date == null) {
            return FormUtils.MDASH;
        }
        StringBuilder sb = new StringBuilder();
        appendZeroIfLessThanTen(sb, date.getDay());
        sb.append(date.getDay());
        sb.append(".");
        appendZeroIfLessThanTen(sb, date.getMonth());
        sb.append(date.getMonth());
        sb.append(".");
        sb.append(date.getYear());
        return sb.toString();
    }

    /** vytvori ClientDate z date */
    public static ClientDate toClientDate(Date date) {
        if (date == null) {
            return null;
        }
        try {
            ClientDate t = new ClientDate();
            t.setDay(Integer.parseInt(dFormat.format(date)));
            t.setMonth(Integer.parseInt(mFormat.format(date)));
            t.setYear(Integer.parseInt(yFormat.format(date)));
            return t;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /** vytvori ClientDate z date */
    public static ClientDateTime toClientDateTime(Date date) {
        if (date == null) {
            return null;
        }
        try {
            ClientDate d = new ClientDate();
            d.setDay(Integer.parseInt(dFormat.format(date)));
            d.setMonth(Integer.parseInt(mFormat.format(date)));
            d.setYear(Integer.parseInt(yFormat.format(date)));

            ClientTime t = new ClientTime(Integer.parseInt(hourFormat.format(date)),
                    Integer.parseInt(minuteFormat.format(date)));

            ClientDateTime cdt = new ClientDateTime();
            cdt.setDate(d);
            cdt.setTime(t);
            return cdt;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    /** vytvori Date z objektu ClientDate */
    public static Date toDate(ClientDate date) {
        if (date == null) {
            return null;
        }
        return classicDateFormat.parse(getDate(date));
    }

    /** vytvori Date z objektu ClientDateTime */
    public static Date toDate(ClientDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        return dateAndTimeFormat.parse(getDateAndTime(dateTime));
    }

    /** vraci datum ve formatu dd.MM. */
    public static String getDateWithoutYear(Date date, TimeZone timeZone) {
        return date == null ? FormUtils.MDASH : dateWithouYearFormat.format(date, timeZone);
    }

    /** vraci datum ve formatu dd.MM. HH:mm*/
    public static String getDateWithoutYearWithTime(ClientDateTime dateTime) {
        if (dateTime == null) {
            return FormUtils.MDASH;
        }
        StringBuilder sb = new StringBuilder();
        appendZeroIfLessThanTen(sb, dateTime.getDate().getDay());
        sb.append(dateTime.getDate().getDay());
        sb.append(".");
        appendZeroIfLessThanTen(sb, dateTime.getDate().getMonth());
        sb.append(dateTime.getDate().getMonth());
        sb.append(" ");
        sb.append(getHourAndMinutes(dateTime));
        return sb.toString();
    }

    /** vraci datum ve formatu dd.MM. */
    public static String getDateWithoutYear(ClientDateTime dateTime) {
        if (dateTime == null) {
            return FormUtils.MDASH;
        }
        StringBuilder sb = new StringBuilder();
        appendZeroIfLessThanTen(sb, dateTime.getDate().getDay());
        sb.append(dateTime.getDate().getDay());
        sb.append(".");
        appendZeroIfLessThanTen(sb, dateTime.getDate().getMonth());
        sb.append(dateTime.getDate().getMonth());
        return sb.toString();
    }

    /** vraci datum a cas ve formatu dd.MM.yyyy HH:mm */
    public static String getDateAndTime(Date date) {
        return date == null ? FormUtils.MDASH : dateAndTimeFormat.format(date);
    }

    /** vraci datum a cas ve formatu dd.MM.yyyy HH:mm */
    public static String getDateAndTime(Date date, TimeZone timeZone) {
        return date == null ? FormUtils.MDASH : dateAndTimeFormat.format(date, timeZone);
    }

    /** vraci datum a cas ve formatu dd.MM.yyyy HH:mm */
    public static String getDateAndTime(ClientDateTime dateTime) {
        if (dateTime == null) {
            return FormUtils.MDASH;
        }
        StringBuilder sb = new StringBuilder(getDate(dateTime.getDate()));
        sb.append(" ");
        appendZeroIfLessThanTen(sb, dateTime.getTime().getHours());
        sb.append(dateTime.getTime().getHours());
        sb.append(":");
        appendZeroIfLessThanTen(sb, dateTime.getTime().getMinutes());
        sb.append(dateTime.getTime().getMinutes());
        return sb.toString();
    }

    private static void appendZeroIfLessThanTen(StringBuilder sb, int value) {
        if (value < 10) {
            sb.append("0");
        }
    }

    /** vraci hodiny a minuty podle zadaneho data */
    public static String getHourAndMinutes(Date date) {
        return date == null ? FormUtils.MDASH : hourAndMinutesFormat.format(date);
    }

    /** vraci hodiny a minuty podle zadaneho data */
    public static String getHourAndMinutes(Date date, TimeZone timeZone) {
        return date == null ? FormUtils.MDASH : hourAndMinutesFormat.format(date, timeZone);
    }

    /** vraci hodiny a minuty podle zadaneho data */
    public static String getHourAndMinutes(ClientDateTime dateTime) {
        if (dateTime == null) {
            return FormUtils.MDASH;
        }
        StringBuilder sb = new StringBuilder();
        appendZeroIfLessThanTen(sb, dateTime.getTime().getHours());
        sb.append(dateTime.getTime().getHours());
        sb.append(":");
        appendZeroIfLessThanTen(sb, dateTime.getTime().getMinutes());
        sb.append(dateTime.getTime().getMinutes());
        return sb.toString();
    }

    /** vraci datum ve formatu LLLL dd, yyyy */
    public static String getMonthDayYear(Date date) {
        return date == null ? FormUtils.MDASH : monthDayYearFormat.format(date);
    }

    /** vraci datum ve formatu LLLL dd, yyyy */
    public static String getMonthDayYear(Date date, TimeZone timeZone) {
        return date == null ? FormUtils.MDASH : monthDayYearFormat.format(date, timeZone);
    }

    /** vraci datum ve formatu LLLL dd, HH:mm */
    public static String getMonthDayTime(Date date, TimeZone timeZone) {
        return date == null ? FormUtils.MDASH : monthDayTimeFormat.format(date, timeZone);
    }

    /** vraci datum ve formatu LLLL dd, HH:mm - HH:mm */
    public static String getMonthDayTime(Date date1, Date date2, TimeZone timeZone) {
        if (date1 == null || date2 == null) {
            return FormUtils.MDASH;
        } else {
            return monthDayTimeFormat.format(date1, timeZone)
                    + FormUtils.UNB_NDASH_UNB
                    + hourAndMinutesFormat.format(date2, timeZone);
        }
    }

    /** vraci datum ve formatu LLLL dd yyyy, HH:mm */
    public static String getMonthDayYearTime(Date date, TimeZone timeZone) {
        return date == null ? FormUtils.MDASH : monthDayYearTimeFormat.format(date, timeZone);
    }

    /** vraci den v tydnu podle zadaneho data */
    public static String getWeekDay(Date date) {
        if (date == null) {
            return "";
        }
        return weekDayFormat.format(date);
    }

    /** vraci spravny rok z data */
    public static int getYear(Date date) {
        return date.getYear() + JS_START_YEAR;
    }

    /**
     * prepocita klientsky cas na klientsky cas casove zony eventu
     * @param date klientsky cas
     * @param eventTimeZone casova zona eventu
     */
    public static Date computeEventDate(Date date, TimeZone eventTimeZone) {
        int offsetNow = timeZoneToOffset(timeZoneFormat.format(date));

        // otocit znamenko + pripocitat posun pokud je letni cas
        int offsetEvent = (-1) * eventTimeZone.getStandardOffset();
        offsetEvent += eventTimeZone.getDaylightAdjustment(date);

        // time je cas v miliseknudach, takez pokud chceme zohlednit rozdil mezi nyni a eventem
        // tak musime offset v minutach vynasobit 1000 (a jsme na sekundach) a 60 a jsme na minutach
        long time = date.getTime() - ((-offsetNow + offsetEvent) * MINUTE_IN_MILISECONDS);
        return new Date(time);
    }

    /**
     * prepocita eventovy cas na eventovy cas casove zony klienta
     * @param date klientsky cas
     * @param eventTimeZone casova zona eventu
     */
    public static Date computeClientEventDate(Date date, TimeZone eventTimeZone) {
        Date now = new Date();
        int offsetNow = timeZoneToOffset(timeZoneFormat.format(now));

        // otocit znamenko + pripocitat posun pokud je letni cas
        int offsetEvent = (-1) * eventTimeZone.getStandardOffset();
        offsetEvent += eventTimeZone.getDaylightAdjustment(date);

        long time = date.getTime() + ((-offsetNow + offsetEvent) * MINUTE_IN_MILISECONDS);
        return new Date(time);
    }

    /** prepocita aktualni datum na stejne datum v gmt */
    public static Date toGMTDate(Date date) {
        int offset = timeZoneToOffset(timeZoneFormat.format(date));
        long time = date.getTime() + (offset * MINUTE_IN_MILISECONDS);
        return new Date(time);
    }

    /** prepocita datum v case gmt na datum na klientovi */
    public static Date fromGMTDate(Date date) {
        int offset = timeZoneToOffset(timeZoneFormat.format(date));
        long time = date.getTime() - (offset * MINUTE_IN_MILISECONDS);
        Date d = new Date(time);
        String checkHours = hourFormat.format(date, TimeZone.createTimeZone(0));
        int diffHours = Integer.parseInt(checkHours);
        // TODO: pro nestandardni pasma dodelat jeste kontrolu na minuty
        //String checkMinutes = minuteFormat.format(date, TimeZoneProvider.getTimeZoneByID(TimeZoneProvider.LONDON));
        //int diffMinutes = Integer.parseInt(checkMinutes);
        if (diffHours != 0) {
            // neco tu nesedi, bude potrebovat korigovat (letni cas, ...)
            if (diffHours < 12) {
                // odecteme, protoze jsme pres
                time -= diffHours * HOUR_IN_MILISECONDS;
            } else {
                // pricteme, protoze jsme pod
                time += ((24 - diffHours) * HOUR_IN_MILISECONDS);
            }
            d = new Date(time);
        }
        return d;
    }

    /**
     * prevede casovou zonu na offset v minutach
     * @param timeZone je retezec ve formatatu znamenkoCas (napr.: +04:00)
     */
    private static int timeZoneToOffset(String timeZone) {
        boolean minus = timeZone.startsWith("-");
        String time = timeZone.substring(1);
        String[] parts = time.split(":");

        try {
            int minutes = 60 * Integer.parseInt(parts[0]);
            minutes += Integer.parseInt(parts[1]);
            return minus ? -1 * minutes : minutes;
        } catch (Exception ex) {
            // neco se pokazilo, coz by se nikdy nemelo stat
            return 0;
        }
    }

    /** handler pro automaticke nastaveni datumu do pokud je prazdny a nastavuje se datum od */
    public static ValueChangeHandler<Date> dateValueChangeHandler(final ConsysDateBox from, final ConsysDateBox to) {
        return new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                if (to.getValue() == null) {
                    to.setValue(from.getValue());
                }
            }
        };
    }
}
