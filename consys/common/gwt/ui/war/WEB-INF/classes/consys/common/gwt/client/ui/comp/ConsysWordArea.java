package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 * Vlastni implementace TextArea s moznosti omezeni maximalniho poctu znaku
 * @author pepa
 */
public class ConsysWordArea extends Composite implements ValidableComponent, HasIdentifier {

    // konstanty
    public static final String WIDTH = "270px";
    public static final String HEIGHT = "80px";
    public static final int UNLIMITED = 0;
    // komponenty
    private FlowPanel panel;
    private TextArea area;
    private Label wordsLabel;
    // data
    private String emptyText;
    private int minWord;
    private int maxWord;
    private String fieldName;
    private boolean showInfo;

    public ConsysWordArea(int minWord, int maxWord) {
        this(minWord, maxWord, WIDTH, HEIGHT, true, "");
    }

    public ConsysWordArea(int minWord, int maxWord, String fieldName) {
        this(minWord, maxWord, WIDTH, HEIGHT, true, fieldName);
    }

    public ConsysWordArea(int maxWord, String width, String height, String fieldName) {
        this(0, maxWord, width, height, true, fieldName);
    }

    public ConsysWordArea(int maxWord, String width, String height, boolean showInfo, String fieldName) {
        this(0, maxWord, width, height, showInfo, fieldName);
    }

    public ConsysWordArea(int minWord, final int maxWord, String width, String height, boolean showInfo, String fieldName) {
        this.minWord = minWord;
        this.maxWord = maxWord;
        this.showInfo = showInfo;
        this.fieldName = fieldName;
        panel = new FlowPanel();
        panel.setWidth(width);

        area = new TextArea();
        area.setStyleName(StyleUtils.BORDER);
        area.addStyleName(ResourceUtils.system().css().inputAreaPadd());
        area.setWidth(width);
        area.setHeight(height);
        panel.add(area);

        if (showInfo) {
            FlowPanel infoPanel = new FlowPanel();
            infoPanel.setStyleName(StyleUtils.MARGIN_TOP_5);
            infoPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
            wordsLabel = StyleUtils.getStyledLabel(String.valueOf(maxWord), StyleUtils.FONT_10PX,
                    StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_RIGHT_5);
            wordsLabel.setVisible(maxWord != UNLIMITED);
            infoPanel.add(wordsLabel);
            String maxText = maxWord == UNLIMITED ? UIMessageUtils.c.const_unlimited() : UIMessageUtils.c.consysWordArea_text_left();
            infoPanel.add(StyleUtils.getStyledLabel(maxText, StyleUtils.FONT_10PX, StyleUtils.FLOAT_LEFT));

            FlowPanel infoPanelWrapper = new FlowPanel();
            infoPanelWrapper.add(infoPanel);
            panel.add(infoPanelWrapper);

            area.addKeyUpHandler(new KeyUpHandler() {

                @Override
                public void onKeyUp(KeyUpEvent event) {
                    String text = area.getText();
                    text = text.replaceAll("\\s+", " ");
                    String[] split = text.split(" ");
                    int length = split.length;
                    if (maxWord != UNLIMITED && length > maxWord) {
                        length = maxWord;
                        int index = text.lastIndexOf(" ");
                        int scrolled = area.getElement().getScrollTop();
                        area.setText(text.substring(0, index));
                        area.getElement().setScrollTop(scrolled);
                    }
                    if (text.length() == 0) {
                        length = 0;
                    }
                    wordsLabel.setText(String.valueOf(maxWord - length));
                }
            });

            panel.add(StyleUtils.clearDiv());
        }

        initWidget(panel);
    }

    /** enabluje/disabluje komponentu */
    public void setEnabled(boolean value) {
        area.setEnabled(value);
    }

    /** vraci text */
    public String getText() {
        String result = area.getText().trim();
        if (emptyText != null && emptyText.equals(result)) {
            return "";
        }
        return result;
    }

    /** nastavuje text */
    public void setText(String text) {
        if (emptyText != null) {
            area.setText(text == null || text.isEmpty() ? emptyText : text);
        } else {
            area.setText(text);
        }
        if (showInfo) {
            if (text != null) {
                text = text.replaceAll("\\s+", " ");
                String[] split = text.split(" ");
                int length = split.length;
                wordsLabel.setText(String.valueOf(maxWord - length));
            } else {
                wordsLabel.setText(String.valueOf(0));
            }
        }
    }

    @Override
    public void setWidth(String width) {
        panel.setWidth(width);
        area.setWidth(width);
    }

    @Override
    public void setHeight(String height) {
        area.setHeight(height);
    }

    /** prida / zrusi focus na komponentu */
    public void setFocus(boolean focused) {
        area.setFocus(focused);
    }

    /** prida blur handler */
    public HandlerRegistration addBlurHandler(BlurHandler handler) {
        return area.addBlurHandler(handler);
    }

    /**
     * nastavi text, ktery se ma zobrazit v prazdne komponente,
     * pokud je tento text nastaven, meni se i chovani metody getText
     * (jestli se zadany text shoduje s textem v komponente, vraci se prezdny string!)
     */
    public void setEmptyText(String text) {
        if (emptyText == null) {
            area.addFocusHandler(new FocusHandler() {

                @Override
                public void onFocus(FocusEvent event) {
                    if (area.getText().trim().equals(emptyText)) {
                        area.setText("");
                    }
                }
            });
            area.addBlurHandler(new BlurHandler() {

                @Override
                public void onBlur(BlurEvent event) {
                    if (area.getText().trim().isEmpty()) {
                        area.setText(emptyText);
                    }
                }
            });
        }
        emptyText = text;
        if (area.getText().trim().isEmpty()) {
            area.setText(emptyText);
        }
    }

    /** nastavi nazev komponenty (pro odesilani ve formulari) */
    public void setName(String name) {
        area.setName(name);
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        String text = getText().trim();
        text = text.replaceAll("\\s+", " ");
        String[] split = text.split(" ");
        int length = split.length;
        if (text.length() == 0) {
            length = 0;
        }
        if (maxWord == UNLIMITED) {
            if (length < minWord) {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
                onFail();
                return false;
            }
            onSuccess();
            return true;
        }
        int result = 0;
        if (length < minWord) {
            result = length - minWord;
        }
        if (length > maxWord) {
            result = length - maxWord;
        }
        if (result != 0) {
            if (!ValidatorUtils.isValidString(text)) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(fieldName));
            } else {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
            }
            onFail();
            return false;
        }
        onSuccess();
        return true;
    }

    @Override
    public void onSuccess() {
        area.removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        area.addStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public String getIdentifier() {
        return fieldName;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.fieldName = identifier;
    }
}
