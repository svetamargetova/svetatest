/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.gwt.client.ui.comp.list;

/**
 *
 * @author palo
 */
public interface ListDelegate<I,T extends ListItem> {

    public T createCell(I item);
    
}
