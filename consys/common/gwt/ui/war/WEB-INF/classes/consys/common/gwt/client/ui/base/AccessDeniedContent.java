package consys.common.gwt.client.ui.base;

import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Zobrazuje chybu o zakazu pristupu
 * @author pepa
 */
public class AccessDeniedContent extends SimpleFormPanel {

    public AccessDeniedContent() {
        super();
        getFailMessage().setText(UIMessageUtils.c.accessDeniedContent_error());
    }
}
