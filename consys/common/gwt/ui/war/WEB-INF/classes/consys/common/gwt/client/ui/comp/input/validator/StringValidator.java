/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.ui.comp.input.validator;

import consys.common.gwt.client.ui.comp.form.Validator;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 *
 * @author palo
 */
public class StringValidator implements Validator<String> {

    private String notEnteredText;
    private String notInRangeText = UIMessageUtils.m.const_notInRange("#");
    private final int min, max;

    public StringValidator(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public String validate(String value) {
        if (!ValidatorUtils.isValidString(value)) {
            return getNotEnteredText();
        } else if (ValidatorUtils.isLenght(value, min, max) != 0) {
            return getNotInRangeText();
        }
        return null;
    }

    /**
     * @return the notEnteredText
     */
    public String getNotEnteredText() {
        if (notEnteredText == null) {
            return ValidatorResources.c.fieldMustEntered();
        }
        return notEnteredText;
    }

    /**
     * @param notEnteredText the notEnteredText to set
     */
    public void setNotEnteredText(String notEnteredText) {
        this.notEnteredText = notEnteredText;
    }

    /**
     * @return the notInRangeText
     */
    public String getNotInRangeText() {
        if(notInRangeText == null){
            return ValidatorResources.m.fieldMustBeInRange(min,max);
        }
        return notInRangeText;
    }

    /**
     * @param notInRangeText the notInRangeText to set
     */
    public void setNotInRangeText(String notInRangeText) {
        this.notInRangeText = notInRangeText;
    }
}
