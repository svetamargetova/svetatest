package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.panel.ListPanelItem;
import consys.common.gwt.client.ui.comp.user.UserLabel;
import consys.common.gwt.client.ui.comp.user.UserResources;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.gwt.shared.bo.EventUser;

/**
 * 
 * Jeden zaznam do zoznamu uzivatelov kde je uzivatel len ako UserLabel.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserLabelListItem extends ListPanelItem<ClientUserThumb>{

    public static final int HEIGHT = 25;
    public static final String HEIGHT_PX = "25px";

    private ClientUserThumb object;
    

    public UserLabelListItem(EventUser eu) {
        super();
        UserResources.INSTANCE.userCss().ensureInjected();
        ClientUserThumb thumb = new ClientUserThumb();
        thumb.setUuid(eu.getUuid());
        thumb.setName(eu.getFullName());
        this.object = thumb;
        setHeight(HEIGHT_PX);
    }

    public UserLabelListItem(ClientUserThumb object) {
        this.object = object;
        setHeight(HEIGHT_PX);
    }      

    @Override
    public ClientUserThumb getObject() {
        return object;
    }   

    @Override
    public void create(FlowPanel panel) {
        UserLabel userLabel = new UserLabel(object.getUuid(),object.getName());        
        SimplePanel wrapper = new SimplePanel();
        wrapper.setWidget(userLabel);
        wrapper.addStyleName(UserResources.INSTANCE.userCss().userLabelListItem());
        panel.add(wrapper);
    }

    @Override
    public void hover() {
    }

    @Override
    public void unhover() {
    }


}
