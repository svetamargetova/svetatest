package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Zakladni file upload
 * @author pepa
 */
public class ConsysFileUpload extends AbstractConsysFileUpload {

    // konstanty
    public static final int HEIGHT = 24;
    // komponenty
    private ConsysTextBox tb;
    private ActionImage button;

    public ConsysFileUpload(int width) {
        super(width, HEIGHT);
    }

    @Override
    protected void initFace(FlowPanel face) {
        // sirka tlacitka je zavisla na lokalizaci (delka nazvu v tlacitku)
        int width = getIntWidth() - UIMessageUtils.c.consysFileUpload_button_width();

        tb = new ConsysTextBox(0, 0, width);
        tb.setText(UIMessageUtils.c.consysFileUpload_text_chooseFileToUpload());

        button = ActionImage.confirm(UIMessageUtils.c.consysFileUpload_button_browsFile());

        face.add(tb);
        face.add(button);
        face.add(StyleUtils.clearDiv());

        initHandlers();
    }

    private void initHandlers() {
        addUploadChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                String[] text = getFilename().split("\\\\");
                tb.setText(text[text.length - 1]);
            }
        });
        addUploadOverHandler(mouseOverHandler(button));
        addUploadOutHandler(mouseOutHandler(button));
    }

    @Override
    protected void disable(boolean disable) {
        tb.setEnabled(!disable);
        button.setEnabled(!disable);
    }

    /** mouse over handler po najeti nad tlacitko */
    public static MouseOverHandler mouseOverHandler(final ActionImage button) {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                button.over();
            }
        };
    }

    /** mouse out handler po odjeti z tlacitka */
    public static MouseOutHandler mouseOutHandler(final ActionImage button) {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                button.out();
            }
        };
    }
}
