package consys.common.gwt.client.ui.layout.panel;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * Reprezentuje jeden zaznam v panelu
 *
 * @author pepa
 */
public class PanelItemUI extends Composite {

    // komponenty
    private FlowPanel bg;
    // data    
    private final PanelItem panelItem;

    public PanelItemUI(PanelItem panelItem, boolean separatorFirst) {
        this.panelItem = panelItem;

        FlowPanel item = new FlowPanel();
        if (separatorFirst) {
            item.add(separator());
        }

        addWidgets(panelItem, item);
        initWidget(item);
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        panelItem.onLoad();
    }

    @Override
    protected void onUnload() {
        super.onUnload();
        panelItem.onUnload();
    }

    /**
     * zaridi vlozeni titulku a obsahu do polozky
     */
    private void addWidgets(PanelItem panelItem, FlowPanel item) {
        bg = new FlowPanel();
        item.add(bg);
        if (panelItem.getTitle() != null) {
            item.add(panelItem.getTitle());
            if (panelItem.isMinimalizable()) {
                // TODO: minimalizace
            }
        }
        item.add(panelItem.getContent());
    }

    /**
     * vytvori separator
     */
    private static FlowPanel separator() {
        FlowPanel separator = new FlowPanel();
        separator.setStyleName(ResourceUtils.system().css().sep());
        return separator;
    }

    /**
     * vraci oznaceni polozky
     */
    public String getName() {
        return panelItem.getName();
    }

    /**
     * nastavi styl pozadi (prechod nahore)
     */
    public void setBgStyleName(String style) {
        bg.setStyleName(style);
    }
}
