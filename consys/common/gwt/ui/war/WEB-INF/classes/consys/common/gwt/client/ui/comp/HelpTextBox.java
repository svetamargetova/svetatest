package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Textbox, ktery kdyz je prazdny obsahuje sedou napovedu co do nej patri vlozit
 * @author pepa
 */
public class HelpTextBox<T extends TextBox & ValidableComponent> extends Composite implements ValidableComponent {

    // komonenty
    private T textBox;
    // data
    private final String helpText;

    private HelpTextBox(String helpText, T textBox) {
        this.helpText = helpText;

        this.textBox = textBox;
        textBox.setText(helpText);

        initWidget(textBox);
        initHandlers();
    }

    private void initHandlers() {
        textBox.addFocusHandler(new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                if (textBox.getText().equals(helpText)) {
                    textBox.setText("");
                    showNormalColor();
                }
            }
        });
        textBox.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (textBox.getText().trim().equals("")) {
                    textBox.setText(helpText);
                    showGrayColor();
                }
            }
        });
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        String text = textBox.getText().trim();
        if (text.equals(helpText)) {
            textBox.setText("");
        }
        boolean result = textBox.doValidate(fail);
        textBox.setText(text);
        return result;
    }

    @Override
    public void onSuccess() {
        textBox.onSuccess();
    }

    @Override
    public void onFail() {
        textBox.onFail();
    }

    public String getText() {
        String text = textBox.getText().trim();
        return text.equals(helpText) ? "" : text;
    }

    public void setText(String text) {
        if (StringUtils.isEmpty(text)) {
            textBox.setText(helpText);
            showGrayColor();
        } else {
            textBox.setText(text);
            showNormalColor();
        }
    }

    private void showGrayColor() {
        DOM.setStyleAttribute(textBox.getElement(), "color", CssStyles.VALUE_COLOR_GRAY);
    }

    private void showNormalColor() {
        textBox.getElement().getStyle().clearColor();
    }

    public static HelpTextBox stringHelpTextBox(String helpText, int min, int max) {
        ConsysStringTextBox cstb = new ConsysStringTextBox(min, max);
        return new HelpTextBox(helpText, cstb);
    }
}
