package consys.common.gwt.client.ui.comp;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.Date;

/**
 * Komponenta pro zadavani casu + pomocne metody pro praci s casem
 * @author pepa
 */
public class ConsysDateBox extends DateBox {

    // logger
    private Logger log = LoggerFactory.getLogger(ConsysDateBox.class);
    // konstanty
    public static final String HALF_WIDTH = "110px";
    private static final DateTimeFormat sp = DateTimeFormat.getFormat("dd.MM.yyyy hh:mm");
    // data
    private ShowDateAs showDateAs;

    /** enum jak se ma chovat ConsysDateBox k datumu (zobrazeni a vraceni hodnoty data) */
    public enum ShowDateAs {

        /** cas na klientovi */
        CLIENT,
        /** cas eventu */
        EVENT,
        /** hodnota v gmt, pouziva se pouze pro zobrazeni jako datum */
        ONLY_DATE;
        private static final long serialVersionUID = -3257946923038108486L;
    }

    public ConsysDateBox() {
        super();
        showDateAs = ShowDateAs.CLIENT;
    }

    public ConsysDateBox(DatePicker picker, Date date, DateBox.Format format) {
        super(picker, date, format);
        showDateAs = ShowDateAs.CLIENT;
    }

    public String getAsText() {
        return sp.format(getValue());
    }

    @Override
    public Date getValue() {
        Date date = super.getValue();
        switch (showDateAs) {
            case EVENT:
                if (date != null) {
                    TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
                    log.debug("getValue (event): " + date + " -> " + DateTimeUtils.computeEventDate(date, tz));
                    date = DateTimeUtils.computeEventDate(date, tz);
                }
                return date;
            case ONLY_DATE:
                if (date != null) {
                    log.debug("getValue (only_date): " + date + " -> " + DateTimeUtils.toGMTDate(date));
                    date = DateTimeUtils.toGMTDate(date);
                }
                return date;
            case CLIENT:
            default:
                // vezme se hodnota tak jak je
                return super.getValue();

        }
    }

    @Override
    public void setValue(Date date) {
        setValue(date, false);
    }

    @Override
    public void setValue(Date date, boolean fireEvents) {
        if (showDateAs != null) {
            switch (showDateAs) {
                case EVENT:
                    if (date != null) {
                        TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
                        log.debug("setValue (event): " + date + " -> " + DateTimeUtils.computeClientEventDate(date, tz));
                        super.setValue(DateTimeUtils.computeClientEventDate(date, tz), fireEvents);
                    }
                    break;
                case ONLY_DATE:
                    if (date != null) {
                        log.debug("setValue (only_date): " + date + " -> " + DateTimeUtils.fromGMTDate(date));
                        super.setValue(DateTimeUtils.fromGMTDate(date), fireEvents);
                    }
                    break;
                case CLIENT:
                default:
                    super.setValue(date, fireEvents);
                    break;
            }
        } else {
            super.setValue(date, fireEvents);
        }
    }

    /** vraci chovani komponenty */
    public ShowDateAs getShowDateAs() {
        return showDateAs;
    }

    /** nastavuje chovani komponenty<br>POZOR!!! je potreba nastavit pred setovanim hodnoty */
    public void setShowDateAs(ShowDateAs showDateAs) {
        this.showDateAs = showDateAs;
    }

    /** tovarni metoda, ktera vraci vytvoreny ConsysDateBox ve standardni sirce a vzhledu */
    public static ConsysDateBox getBox() {
        return getBox(null);
    }

    /** tovarni metoda, ktera vraci vytvoreny ConsysDateBox ve standardni sirce a vzhledu */
    public static ConsysDateBox getBox(boolean withTime) {
        return getBox(null, withTime);
    }

    /** tovarni metoda, ktera vraci vytvoreny ConsysDateBox se zadanou sirkou a standardnim vzhledem */
    public static ConsysDateBox getBox(String width) {
        return getBox(width, false);
    }

    /** tovarni metoda, ktera vraci vytvoreny ConsysDateBox se zadanou sirkou a standardnim vzhledem */
    public static ConsysDateBox getBox(String width, boolean withTime) {
        ConsysDateBox box = new ConsysDateBox(new ConsysDatePicker(), null,
                new ConsysDateBox.DefaultFormat(withTime ? DateTimeUtils.dateAndTimeFormat : DateTimeUtils.classicDateFormat));
        TextBox input = box.getTextBox();
        input.setStyleName(StyleUtils.BORDER);
        if (width == null) {
            input.addStyleName(StyleUtils.WIDTH_270);
        } else {
            input.setWidth(width);
        }
        input.addStyleName(StyleUtils.HEIGHT_21);
        input.addStyleName(ResourceUtils.system().css().inputPadd());
        return box;
    }

    /**
     * tovarni metoda, ktera vraci vytvoreny ConsysDateBox
     * v polovicni sirce a standardnim vzhledu (do standardniho formu se vlezou dva vedle sebe)
     */
    public static ConsysDateBox getHalfBox() {
        return getBox(HALF_WIDTH);
    }

    /**
     * tovarni metoda, ktera vraci vytvoreny ConsysDateBox
     * v polovicni sirce a standardnim vzhledu (do standardniho formu se vlezou dva vedle sebe)
     */
    public static ConsysDateBox getHalfBox(boolean withTime) {
        return getBox(HALF_WIDTH, withTime);
    }

    /**
     * tovarni metoda, ktera vraci vytvoreny ConsysDateBox, s chovanim datumu pro event
     * v polovicni sirce a standardnim vzhledu (do standardniho formu se vlezou dva vedle sebe)
     */
    public static ConsysDateBox getEventHalfBox(boolean withTime) {
        return getEventHalfBox(withTime, null);
    }

    /**
     * tovarni metoda, ktera vraci vytvoreny ConsysDateBox, s chovanim datumu pro event
     * v polovicni sirce a standardnim vzhledu (do standardniho formu se vlezou dva vedle sebe)
     */
    public static ConsysDateBox getEventHalfBox(boolean withTime, Date value) {
        ConsysDateBox db = getHalfBox(withTime);
        db.setShowDateAs(ConsysDateBox.ShowDateAs.EVENT);
        db.setValue(value);
        return db;
    }
}
