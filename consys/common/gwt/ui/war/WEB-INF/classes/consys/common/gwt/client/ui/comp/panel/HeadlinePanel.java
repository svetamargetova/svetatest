package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Help;
import consys.common.gwt.client.ui.comp.wrapper.ContentPanelWrapper;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Panel ktory obsahuje text v ohranicenej sedej oblasti a pouziva sa na definovanie
 * novej sekcie.
 * 
 * Dalsie rozsirenia:
 * - z helpom
 * - z nejakou kaciu na klik 
 * 
 * @author palo
 */
public class HeadlinePanel extends Composite {

    // komponenty
    private ContentPanelWrapper wrapper;
    private FlowPanel panel;
    private Help help;
    private Widget titleWidget;
    private Widget helpWidget;
    // data
    private String text;

    public HeadlinePanel(String text) {
        this(text, null, null, null);
    }

    public HeadlinePanel(String text, String helpTitle, String helpText) {
        this(text, helpTitle, helpText, null);
    }

    /** vytvori ActionLabel a umozni na klik spustit akci, pote je nahrazeno obycejnym Label */
    public HeadlinePanel(String text, ConsysAction clickAction) {
        this(text, null, null, clickAction);
    }

    /** vytvori ActionLabel a umozni na klik spustit akci, pote je nahrazeno obycejnym Label */
    public HeadlinePanel(String text, String helpTitle, String helpText, ConsysAction clickAction) {
        this.text = text;

        PanelResources.INSTANCE.css().ensureInjected();

        titleWidget = titleWidget(text, clickAction);
        helpWidget = helpWidget(helpTitle, helpText);

        init();
    }

    private Widget titleWidget(String text, ConsysAction clickAction) {
        Widget widget;
        if (clickAction != null) {
            ActionLabel action = new ActionLabel(text);
            action.setClickHandler(actionClickHandler(clickAction));
            widget = action;
        } else {
            widget = new Label(text);
        }
        return widget;
    }

    private Widget helpWidget(String helpTitle, String helpText) {
        if (StringUtils.isNotEmpty(helpTitle) || StringUtils.isNotEmpty(helpText)) {
            help = Help.left(helpTitle, helpText);

            Label h = new Label();
            h.setStyleName(PanelResources.INSTANCE.css().help());
            h.getElement().setInnerHTML("<sup>?</sup>");
            h.sinkEvents(Event.ONMOUSEOUT | Event.ONMOUSEOVER);
            h.addHandler(overHandler(h), MouseOverEvent.getType());
            h.addHandler(outHandler(), MouseOutEvent.getType());
            return h;
        }
        return null;
    }

    private void init() {
        panel = new FlowPanel();

        wrapper = new ContentPanelWrapper(panel);
        wrapper.addStyleName(PanelResources.INSTANCE.css().headlinePanel());
        initWidget(wrapper);

        initPanel();
    }

    private void initPanel() {
        panel.add(titleWidget);
        if (help != null) {
            panel.add(helpWidget);
        }
        panel.add(StyleUtils.clearDiv());
    }

    private ClickHandler actionClickHandler(final ConsysAction clickAction) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                titleWidget = titleWidget(text, null);
                panel.clear();
                initPanel();
                clickAction.run();
            }
        };
    }

    private MouseOverHandler overHandler(final Label h) {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                help.setPopupPosition(h.getAbsoluteLeft() + 10, h.getAbsoluteTop() + 3);
                help.show();
            }
        };
    }

    private MouseOutHandler outHandler() {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                help.hide();
            }
        };
    }
}
