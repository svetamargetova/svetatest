package consys.common.gwt.client.widget.utils;

import com.google.gwt.core.client.GWT;
import consys.common.gwt.client.widget.message.CommonWidgetsConstants;
import consys.common.gwt.client.widget.message.CommonsWidgetsMessages;


/**
 *
 * @author pepa
 */
public class CWMessageUtils {

    /** systemove konstanty modulu admin user */
    public static final CommonWidgetsConstants c = (CommonWidgetsConstants) GWT.create(CommonWidgetsConstants.class);
    public static final CommonsWidgetsMessages m = (CommonsWidgetsMessages) GWT.create(CommonsWidgetsMessages.class);
    
}
