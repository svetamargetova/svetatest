package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.ui.layout.panel.PanelAddonSubItem;

/**
 * Event pro odznaceni polozek v pridavnem panelu za vybrane
 * @author pepa
 */
public class SelectedAddonSubItemEvent extends GwtEvent<SelectedAddonSubItemEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    private PanelAddonSubItem addonSubItem;

    public SelectedAddonSubItemEvent() {
    }

    public SelectedAddonSubItemEvent(PanelAddonSubItem addonSubItem) {
        this.addonSubItem = addonSubItem;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onSelect(addonSubItem);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onSelect(PanelAddonSubItem newSelected);
    }
}
