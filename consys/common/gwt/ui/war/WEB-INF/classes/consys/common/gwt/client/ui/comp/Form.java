package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Komponenta ktora rozsiruje Zakladny formular tak ze pridavane polozky su
 * automaticky pridavane za sebou.
 * 
 *
 * @author pepa
 */
public class Form extends BaseForm {

   private int rowCounter = -1;

    public Form() {
        super();
    }

    public Form(String width) {
        super(width);

    }

    private void nextRow(){
        rowCounter += 1;
    }

    public void addSeparator(){
        Label separator = new Label("");
        separator.setSize("100px", "10px");
        addContent(separator);
    }

    /** přidá titulek povinné položky */
    public void addRequired(String text, Widget widget) {
        nextRow();
        super.addRequired(rowCounter, text, widget);
    }

    public void addActionWidget(Widget widget){
        nextRow();
        addActionWidget(rowCounter, widget);
    }
    
    public void addWidgets(Widget... widgets) {
            nextRow();
            addWidgets(rowCounter, widgets);
    }


    /** přidá titulek volitelné položky */
    public void addOptional(String text, Widget widget) {
        nextRow();
        addOptional(rowCounter, text, widget);
    }

    /** vloží formulářovou komponentu */
    public void addContent(Widget widget) {
        nextRow();
        addContent(rowCounter, widget);
    }

    @Override
    public void insertRow(int beforeRow){
        nextRow();
        super.insertRow(beforeRow);
    }

    /** nastavi akcni prvky formulare */
    public void addActionMembers(ActionImage actionImage, ActionLabel actionLabel) {
        nextRow();
        super.addActionMembers(rowCounter, actionImage, actionLabel);
    }

    /** nastavi akcni prvky formulare */
    public void addActionMembers(ActionImage actionImage, ActionLabel actionLabel, String contentWidth) {
        nextRow();
        super.addActionMembers(rowCounter, actionImage, actionLabel, contentWidth);
    }
}
