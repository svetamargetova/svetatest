package consys.common.gwt.client.widget;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.widget.security.SpringSecurityAdministrationLoginHandler;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 * Komponenta pro prihlaseni uzivatele
 * @author pepa
 */
public class LoginForm extends Form {

    // komponenty
    private String succRedirect;
    private TextBox userBox;
    private PasswordTextBox passwordBox;
    private ConsysCheckBox checkBox;
    private SpringSecurityAdministrationLoginHandler handler = new SpringSecurityAdministrationLoginHandler() {

        @Override
        public void clearBeforeRedirect() {
            userBox.setText("");
            passwordBox.setText("");
        }

        @Override
        public String getLogin() {
            return userBox.getText().trim();
        }

        @Override
        public String getPassword() {
            return passwordBox.getText().trim();
        }

        @Override
        public boolean getRememberMe() {
            return checkBox.getValue();
        }

        @Override
        public String getSuccessRedirect() {
            return succRedirect;
        }
    };

    public LoginForm(final ConsysAction loginAction) {
        this(loginAction, null);
    }

    public LoginForm(final ConsysAction loginAction, String inputWidth) {
        addStyleName(ResourceUtils.css().loginForm());
        
        userBox = StyleUtils.getFormInput(inputWidth);
        DOM.setElementAttribute(userBox.getElement(), "type", "email");
        DOM.setElementAttribute(userBox.getElement(), "name", "email");
        passwordBox = StyleUtils.getFormPasswordInput(inputWidth);
        passwordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    loginAction.run();
                }
            }
        });

        checkBox = new ConsysCheckBox();
        checkBox.addStyleName(StyleUtils.FLOAT_LEFT);

        Label rememberMe = new Label(CWMessageUtils.c.const_rememberMe());
        rememberMe.setStyleName(ResourceUtils.css().rememberMe());
        
        FlowPanel rememberPanel = new FlowPanel();
        rememberPanel.setStyleName(StyleUtils.MARGIN_TOP_3);
        rememberPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
        rememberPanel.add(checkBox);
        rememberPanel.add(rememberMe);
        rememberPanel.add(StyleUtils.clearDiv());

        ActionImage button = ActionImage.getConfirmButton(CWMessageUtils.c.loginContent_button_logIn());
        button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                loginAction.run();
            }
        });
        button.addStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(button);
        controlPanel.add(rememberPanel);
        controlPanel.add(StyleUtils.clearDiv());

        addRequired(UIMessageUtils.c.const_email(), userBox);
        addRequired(CWMessageUtils.c.const_password(), passwordBox);
        addContent(controlPanel);
        addContent(button);
        //addContent(getSso());
    }

    public void addHint(String labelText, String actionText, ClickHandler handler) {
        Label label = StyleUtils.getStyledLabel(labelText, StyleUtils.MARGIN_TOP_10, StyleUtils.MARGIN_RIGHT_10, StyleUtils.ALIGN_RIGHT);
        ActionLabel action = new ActionLabel(actionText, StyleUtils.MARGIN_TOP_10);
        action.removeStyleName(StyleUtils.INLINE);
        action.setClickHandler(handler);
        addWidgets(label, action);
    }

    public void enableLostPasswordHint() {
        addHint(CWMessageUtils.c.loginContent_text_lostPassword(), CWMessageUtils.c.loginContent_action_sendMeHint(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CommonWidgetsModule.LOST_PASSWORD);
            }
        });
    }

    public void enableNotRegistredHint() {
        addHint(CWMessageUtils.c.loginContent_text_notRegistered(), CWMessageUtils.c.loginContent_action_createYourAccount(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CommonWidgetsModule.REGISTER_ACCOUNT);
            }
        });
    }

    public void enableNotActivatedHint() {
        addHint(CWMessageUtils.c.loginContent_text_notActivated(), CWMessageUtils.c.loginContent_action_activateYourAccount(), new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CommonWidgetsModule.ACTIVATE_ACCOUNT);
            }
        });
    }

    public void setup(String username, String password) {
        getUserBox().setText(username);
        getPasswordBox().setText(password);
    }

    public ConsysCheckBox getCheckBox() {
        return checkBox;
    }

    public PasswordTextBox getPasswordBox() {
        return passwordBox;
    }

    public TextBox getUserBox() {
        return userBox;
    }

    public SpringSecurityAdministrationLoginHandler getHandler() {
        return handler;
    }

    public String getSuccRedirect() {
        return succRedirect;
    }

    public void setSuccRedirect(String succRedirect) {
        this.succRedirect = succRedirect;
    }
}
