package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class ConsysBaseWrapper extends ConsysWrapper {

    public ConsysBaseWrapper() {
        super();
    }

    public ConsysBaseWrapper(Widget widget) {
        super(widget);
    }

    public ConsysBaseWrapper(Widget widget, int settings) {
        super(widget, settings);
    }

    @Override
    public WrapperFace getWrappertFace() {
        Image bottomLeft = new Image(ResourceUtils.system().corGrayBl());
        Image bottomRight = new Image(ResourceUtils.system().corGrayBr());
        Image topRight = new Image(ResourceUtils.system().corGrayTr());
        Image topLeft = new Image(ResourceUtils.system().corGrayTl());
        WrapperFace face = new WrapperFace(new Image[]{bottomLeft, bottomRight, topRight, topLeft}, 3, StyleUtils.BACKGROUND_GRAY);
        return face;
    }
}
