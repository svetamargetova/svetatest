package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 * Focusovatelne obrazkove tlacitko
 * @author pepa
 */
public class ActionImage extends Composite {

    // konstanty
    public static final int TEMPORARILY_DISABLE_DELAY = 1500;
    // konstanty
    public static final String HEIGHT = "20px";
    public static final String HEIGHT_BIG = "25px";
    // komponenty
    private HorizontalPanel content;
    private Label label;
    private FocusPanel focusPanel;
    private Image left;
    private Image right;
    private FlowPanel leftWrapper;
    private FlowPanel rightWrapper;
    // data
    private boolean big;
    private boolean enabled;
    private ArrayList<ClickHandler> clickHandlers;
    private ArrayList<HandlerRegistration> overOutHandlerRegistrations;
    private Timer enableTimer;
    private boolean temporarilyDisabled;

    public ActionImage(Image icon, String text) {
        init(icon, text, false);
    }

    public ActionImage(Image icon, String text, boolean big) {
        init(icon, text, big);
    }

    private void init(Image icon, String text, boolean big) {
        enabled = true;
        this.big = big;
        clickHandlers = new ArrayList<ClickHandler>();
        overOutHandlerRegistrations = new ArrayList<HandlerRegistration>();

        enableTimer = enableTimer();

        content = new HorizontalPanel();
        content.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        content.addStyleName(StyleUtils.HAND);
        content.addStyleName(big ? ResourceUtils.system().css().actionImageBig()
                : ResourceUtils.system().css().actionImage());

        label = new Label(text);
        label.setStyleName(StyleUtils.FONT_BOLD);
        label.addStyleName(StyleUtils.TEXT_WHITE);
        label.addStyleName(StyleUtils.MARGIN_HOR_5);

        if (icon != null) {
            icon.setStyleName(StyleUtils.MARGIN_LEFT_5);
            content.add(icon);
        }
        content.add(label);

        focusPanel = new FocusPanel();
        focusPanel.addStyleName(ResourceUtils.system().css().positionStatic());
        focusPanel.setWidget(content);
        focusPanel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                focusPanel.setFocus(false);
            }
        });

        left = big ? new Image(ResourceUtils.system().actionImageBigLeft())
                : new Image(ResourceUtils.system().actionImageBorder());
        right = big ? new Image(ResourceUtils.system().actionImageBigRight())
                : new Image(ResourceUtils.system().actionImageBorder());

        leftWrapper = new FlowPanel();
        leftWrapper.add(left);

        rightWrapper = new FlowPanel();
        rightWrapper.add(right);

        HorizontalPanel panel = new HorizontalPanel();
        panel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        panel.setHeight(big ? HEIGHT_BIG : HEIGHT);
        panel.add(leftWrapper);
        panel.add(focusPanel);
        panel.add(rightWrapper);

        sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT | Event.ONCLICK);
        initOverOutHandlers();

        initWidget(panel);

        addMyHandler(myClickkHandler());
    }

    /** zinicializuje handlery reagujici na prejeti mysi nad labelem */
    private void initOverOutHandlers() {
        overOutHandlerRegistrations.add(addHandler(ActionLabel.inverseMouseOverHandler(label), MouseOverEvent.getType()));
        overOutHandlerRegistrations.add(addHandler(ActionLabel.inverseMouseOutHandler(label), MouseOutEvent.getType()));
    }

    /** zrusi inicializovane handlery reagujici na prejeti mysi nad labelem */
    private void clearOverOutHandlers() {
        for (HandlerRegistration hr : overOutHandlerRegistrations) {
            hr.removeHandler();
        }
        overOutHandlerRegistrations.clear();
    }

    /** nastaveni pevne sirky popisku v tlacitku */
    public void setLabelWidth(String width) {
        label.setWidth(width);
    }

    /** prida styl popisku tlacitka */
    public void addLabelStyleName(String styleName) {
        label.addStyleName(styleName);
    }

    /** vraci ActionLink s ikonou check */
    public static ActionImage getCheckButton(String text) {
        return new ActionImage(new Image(ResourceUtils.system().actionImageCheck()), text);
    }

    /** vraci ActionLink s ikonou confirm */
    public static ActionImage getConfirmButton(String text) {
        return new ActionImage(new Image(ResourceUtils.system().actionImageConfirm()), text);
    }

    /** vraci ActionLink s ikonou confirm */
    public static ActionImage getConfirmButton(String text, boolean big) {
        return new ActionImage(new Image(ResourceUtils.system().actionImageConfirm()), text, big);
    }

    /** vraci ActionLink s ikonou krizku */
    public static ActionImage getCrossButton(String text, boolean big) {
        return new ActionImage(new Image(ResourceUtils.system().actionImageCross()), text, big);
    }

    /** vraci ActionLink s ikonou back */
    public static ActionImage getBackButton(String text, boolean big) {
        return new ActionImage(new Image(ResourceUtils.system().actionImageBack()), text, big);
    }

    /** vraci ActionLink s ikonou plus */
    public static ActionImage getPlusButton(String text, boolean big) {
        return new ActionImage(new Image(ResourceUtils.system().actionImagePlus()), text, big);
    }

    /** vraci ActionLink s ikonou person */
    public static ActionImage getPersonButton(String text) {
        return new ActionImage(new Image(ResourceUtils.system().actionImagePerson()), text);
    }

    /** vraci ActionLink s ikonou person */
    public static ActionImage getPersonButton(String text, boolean big) {
        return new ActionImage(new Image(ResourceUtils.system().actionImagePerson()), text, big);
    }

    /** vraci ActionImage s ikonou person */
    public static ActionImage getMailButton(String text, boolean big) {
        return new ActionImage(new Image(ResourceUtils.system().actionImageMail()), text, big);
    }

    /** vraci ActionImage s ikonou person */
    public static ActionImage getDownArrowButton(String text, boolean big) {
        return new ActionImage(new Image(ResourceUtils.system().actionImageDownArrow()), text, big);
    }

    /** vracil velke tlacitko zpet (co se dava vedle nadpisu formu) s nastavenym vystrelenim breadcrumbback */
    public static ActionImage breadcrumbBackBigBack() {
        ActionImage back = ActionImage.getBackButton(UIMessageUtils.c.const_back(), true);
        back.addClickHandler(FormUtils.breadcrumbBackClickHandler());
        return back;
    }

    /** zaregistruje zadany ClickHandler */
    public void addClickHandler(ClickHandler handler) {
        clickHandlers.add(handler);
    }

    /** priradi handler pro kliknuti a stisknuti enter */
    private void addMyHandler(final ClickHandler handler) {
        focusPanel.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    handler.onClick(null);
                }
            }
        });
        addHandler(handler, ClickEvent.getType());
    }

    /** vytvori interni ClickHandler, ktery se stara o spousteni ostatnich */
    private ClickHandler myClickkHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (enabled && !temporarilyDisabled) {
                    temporarilyDisabled = true;
                    for (ClickHandler handler : clickHandlers) {
                        handler.onClick(event);
                    }
                    enableTimer.schedule(TEMPORARILY_DISABLE_DELAY);
                }
            }
        };
    }

    /** casovac ktery vypne docasne vypnuti zneaktivovani tlacitka */
    private Timer enableTimer() {
        return new Timer() {
            @Override
            public void run() {
                temporarilyDisabled = false;
            }
        };
    }

    /** vraci true pokud je komponenta povolena */
    public boolean isEnabled() {
        return enabled;
    }

    /** enabluje / disabluje komponentu */
    public void setEnabled(boolean enabled) {
        if (enabled == this.enabled) {
            return;
        }
        this.enabled = enabled;
        if (enabled) {
            initOverOutHandlers();
            content.addStyleName(big ? ResourceUtils.system().css().actionImageBig()
                    : ResourceUtils.system().css().actionImage());
            content.removeStyleName(big ? ResourceUtils.system().css().actionImageBigDisabled()
                    : ResourceUtils.system().css().actionImageDisabled());
            content.addStyleName(StyleUtils.HAND);

            leftWrapper.clear();
            leftWrapper.add(left);
            rightWrapper.clear();
            rightWrapper.add(right);
        } else {
            clearOverOutHandlers();
            content.addStyleName(big ? ResourceUtils.system().css().actionImageBigDisabled()
                    : ResourceUtils.system().css().actionImageDisabled());
            content.removeStyleName(big ? ResourceUtils.system().css().actionImageBig()
                    : ResourceUtils.system().css().actionImage());
            content.removeStyleName(StyleUtils.HAND);

            leftWrapper.clear();
            leftWrapper.add(big ? new Image(ResourceUtils.system().actionImageBigLeftDisabled())
                    : new Image(ResourceUtils.system().actionImageBorderDisabled()));
            rightWrapper.clear();
            rightWrapper.add(big ? new Image(ResourceUtils.system().actionImageBigRightDisabled())
                    : new Image(ResourceUtils.system().actionImageBorderDisabled()));
        }
    }

    /** zmeni text v nazvu tlacitka */
    public void setText(String text) {
        label.setText(text);
    }

    protected void mouseOver() {
        ActionLabel.underlineLabel(label);
    }

    protected void mouseOut() {
        ActionLabel.normalLabel(label);
    }
}
