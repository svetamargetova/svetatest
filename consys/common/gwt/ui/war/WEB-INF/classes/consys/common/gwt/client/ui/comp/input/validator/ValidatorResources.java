/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.ui.comp.input.validator;

import com.google.gwt.core.client.GWT;

/**
 *
 * @author palo
 */
public class ValidatorResources {
    /** systemove konstanty modulu ui */
    public static final ValidatorConstants c = (ValidatorConstants) GWT.create(ValidatorConstants.class);
    /** systemove zpravy modulu ui */
    public static final ValidatorMessages m = (ValidatorMessages) GWT.create(ValidatorMessages.class);

}
