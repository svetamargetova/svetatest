package consys.common.gwt.client.ui.dom;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

/**
 * Upravene metody pro neposlusne prohlizece
 * @author pepa
 */
public class DOMX {

    private static DOMXImpl impl;

    static {
        impl = (DOMXImpl) GWT.create(DOMXImpl.class);
    }

    /** nastavi pruhlednost */
    public static void setOpacity(Element e, double opacity) {
        if (opacity < 0.00001) {
            opacity = 0.0;
        }
        if (opacity > 1.0) {
            opacity = 1.0;
        }
        impl.setOpacity(e, opacity);
    }

    public static void setAutofocus(Element e) {
        DOM.setElementAttribute(e, "autofocus", "autofocus");
    }

    public static void setColor(Element e, String color) {
        DOM.setStyleAttribute(e, "color", color);
    }

    public static int getOffsetHeightDiff() {
        return impl.getOffsetHeightDiff();
    }

    /** vraci rozliseni klienta na vysku */
    public static native int getScreenHeight() /*-{
    return $wnd.screen.height;
    }-*/;
}
