package consys.common.gwt.client.ui.css;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 *
 * @author pepa
 */
@ImportedWithPrefix("common")
public interface CommonCss extends CssResource {

    String leftColumn();

    String rightColumn();

    String active();

    String notActive();
    
    String action();
    
    String title();
    
    String error();
    
    String success();
    
}
