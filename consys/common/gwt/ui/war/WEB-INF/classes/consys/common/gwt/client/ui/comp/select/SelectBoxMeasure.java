package consys.common.gwt.client.ui.comp.select;

/**
 * Udava rozmery SelectBox
 * @author pepa
 */
public class SelectBoxMeasure {

    // konstanty
    private static final int DEFAULT_WIDTH = 270;
    private static final int DEFAULT_HEIGHT = 21;
    private static final int DEFAULT_INPUT_WIDTH_DIFF = 24;

    /** vraci vychozi vysku */
    public String defaultHeight() {
        return DEFAULT_HEIGHT + "px";
    }

    /** vraci vychozi sirku vstupu */
    public String defaultInputWidth() {
        return (DEFAULT_WIDTH - DEFAULT_INPUT_WIDTH_DIFF) + "px";
    }

    /** vraci vychozi sirku */
    public String defaultWidth() {
        return DEFAULT_WIDTH + "px";
    }

    /** pozice itemBoardu od vrhu */
    public int itemBoardTop(int top) {
        return top + DEFAULT_HEIGHT;
    }

    /** vraci sirku popupu */
    public String itemBoardWidth(int customWidth) {
        return (customWidth == 0 ? DEFAULT_WIDTH : customWidth) + "px";
    }

    /** zakladni sirka */
    public String basePanelWidth(int width) {
        return width + "px";
    }

    /** sirka vybrane polozky */
    public String selectItemLabelWidth(int width) {
        return (width - DEFAULT_INPUT_WIDTH_DIFF) + "px";
    }

    /** sirka vstupu */
    public String inputWidth(int width) {
        return (width - DEFAULT_INPUT_WIDTH_DIFF) + "px";
    }
}
