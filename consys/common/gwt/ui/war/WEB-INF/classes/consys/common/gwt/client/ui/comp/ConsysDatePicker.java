package consys.common.gwt.client.ui.comp;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.datepicker.client.CalendarModel;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.google.gwt.user.datepicker.client.DefaultCalendarView;
import com.google.gwt.user.datepicker.client.DefaultMonthSelector;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 *
 * @author pepa
 */
public class ConsysDatePicker extends DatePicker {

    public ConsysDatePicker() {
        super(new DefaultMonthSelector(), new DefaultCalendarView(), new ConsysCalendarModel());
    }

    private static class ConsysCalendarModel extends CalendarModel {

        @Override
        protected DateTimeFormat getMonthAndYearFormatter() {
            if (UIMessageUtils.c.system_locale().equals("cs")) {
                return DateTimeFormat.getFormat("LLLL yyyy");
            } else {
                return DateTimeFormat.getFormat(PredefinedFormat.YEAR_MONTH_ABBR);
            }
        }
    }
}
