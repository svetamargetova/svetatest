package consys.common.gwt.client.ui;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.cache.CacheRegistrator;
import consys.common.gwt.client.event.*;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.Asyncable;
import consys.common.gwt.client.module.event.EventNavigationItem;
import consys.common.gwt.client.ui.base.AccessDeniedContent;
import consys.common.gwt.client.ui.base.UnknownTokenContent;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.comp.upload.UploadingForm;
import consys.common.gwt.client.ui.event.*;
import consys.common.gwt.client.ui.layout.*;
import consys.common.gwt.client.ui.layout.panel.PanelAddonItem;
import consys.common.gwt.client.ui.layout.panel.PanelItemAction;
import consys.common.gwt.client.ui.layout.panel.PanelItemType;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.utils.StringUtils;
import java.util.HashMap;
import java.util.Map;

/**
 * Spravce obsahu, obsahuje rozmerove konstanty
 *
 * @author pepa
 */
public class LayoutManager extends HTMLPanel
        implements
        ChangeContentEvent.Handler,
        DispatchEvent.Handler,
        ApplicationSecurityEvent.Handler,
        AccessDeniedEvent.Handler,
        ValueChangeHandler<String>,
        EventNavigationModelUpdate.Handler,
        RedirectEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(LayoutManager.class);
    private static final String MAIN_BODY =
            "<div class='" + ResourceUtils.system().css().clearDiv() + "' style=\"height:10px\"></div>"
            + "<div id='header'></div>"
            + "<div class='" + ResourceUtils.system().css().clearDiv() + "' style=\"height:5px\"></div>"
            + "<div id='menu'></div>"
            + "<div class='" + ResourceUtils.system().css().clearDiv() + "'></div>"
            + "<div id='breadcrum'></div>"
            + "<div class='" + ResourceUtils.system().css().clearDiv() + "' style=\"height:20px\"></div>"
            + "<div id='content' class='"
            + ResourceUtils.system().css().contentCol()
            + "'></div>"
            + "<div id='panel' class='"
            + ResourceUtils.system().css().panelCol()
            + "'></div>"
            + "<div id='footer' class='"
            + ResourceUtils.system().css().footer()
            + "'></div><div id='uploading'></div>";
    // konstanty verejne
    public static final int LAYOUT_WIDTH_INT = 940;
    public static final String LAYOUT_WIDTH = LAYOUT_WIDTH_INT + "px";
    public static final int LAYOUT_PANEL_WIDTH_INT = 220;
    public static final String LAYOUT_PANEL_WIDTH = LAYOUT_PANEL_WIDTH_INT + "px";
    public static final int LAYOUT_CONTENT_FULL_WIDTH_INT = LAYOUT_WIDTH_INT - LAYOUT_PANEL_WIDTH_INT;
    public static final String LAYOUT_CONTENT_FULL_WIDTH = LAYOUT_CONTENT_FULL_WIDTH_INT + "px";
    public static final int LAYOUT_CONTENT_WIDTH_INT = LAYOUT_CONTENT_FULL_WIDTH_INT - 20;
    public static final String LAYOUT_CONTENT_WIDTH = LAYOUT_CONTENT_WIDTH_INT + "px";
    // instance
    private static LayoutManager layoutManager;
    // komponenty
    private SimplePanel content;
    private Waiting waiting;
    private FlowPanel uploadingFormsPlace;
    // data
    private Map<String, UploadingForm> uploadingForms;

    private LayoutManager() {
        super(MAIN_BODY);
        History.addValueChangeHandler(this);
        getElement().setId("centerContent");
        setWidth(LAYOUT_WIDTH);

        uploadingForms = new HashMap<String, UploadingForm>();

        generateLayout();

        // ziniciovani CommonHistoryHandleru
        CommonHistoryHandler.get();
    }

    /**
     * vygeneruje a vysklada polozky layoutu
     */
    private void generateLayout() {
        // Hlavick - Logo
        add(LayoutHead.get(), "header");

        // Hlavne Menu
        add(LayoutTopMenu.get(), "menu");

        // Breadcrum
        add(LayoutBreadcrumb.get(), "breadcrum");

        // Content
        content = new SimplePanel();
        content.setWidth(LAYOUT_CONTENT_FULL_WIDTH);
        content.addStyleName(ResourceUtils.system().css().minHeight());
        content.addStyleName(ResourceUtils.system().css().clearLeft());
        add(content, "content");

        // Misto pro uploadovaci formulare
        uploadingFormsPlace = new FlowPanel() {
            @Override
            public void add(Widget w) {
                super.add(w);
                if (w instanceof UploadingForm) {
                    UploadingForm f = (UploadingForm) w;
                    f.runTimer();
                }
            }
        };
        uploadingFormsPlace.addStyleName(ResourceUtils.system().css().uploadWrapper());
        add(uploadingFormsPlace, "uploading");

        // Pravy panel
        add(LayoutPanel.get(), "panel");

        // Paticka
        add(LayoutBottomMenu.get(), "footer");
    }

    /**
     * vraci instanci LayoutManageru
     */
    public static LayoutManager get() {
        if (layoutManager == null) {
            layoutManager = new LayoutManager();
        }
        return layoutManager;
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(ChangeContentEvent.TYPE, this);
        EventBus.get().addHandler(DispatchEvent.TYPE, this);
        EventBus.get().addHandler(ApplicationSecurityEvent.TYPE, this);
        EventBus.get().addHandler(AccessDeniedEvent.TYPE, this);
        EventBus.get().addHandler(EventNavigationModelUpdate.TYPE, this);
        EventBus.get().addHandler(RedirectEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(ChangeContentEvent.TYPE, this);
        EventBus.get().removeHandler(DispatchEvent.TYPE, this);
        EventBus.get().removeHandler(ApplicationSecurityEvent.TYPE, this);
        EventBus.get().removeHandler(AccessDeniedEvent.TYPE, this);
        EventBus.get().removeHandler(EventNavigationModelUpdate.TYPE, this);
        EventBus.get().removeHandler(RedirectEvent.TYPE, this);
    }

    @Override
    public void onChangeContent(ChangeContentEvent event) {
        content.clear();
        content.setWidget(event.getContent());
        // pokud se jedna o asynchronni obsah, zavola se jeho nacteni
        if (event.getContent() instanceof Asyncable) {
            ((Asyncable) event.getContent()).runComponent();
        }
        logger.debug("LayoutManager: content changed ");
    }

    /**
     * zobrazi pres formular panel s cekacim obrazkem
     */
    public void showWaiting(boolean value) {
        if (waiting == null) {
            waiting = new Waiting(content);
            add(waiting, "content");
        }
        if (value) {
            waiting.show();
        } else {
            waiting.hide();
        }
    }

    @Override
    public void onDispatch(DispatchEvent event) {
        String name = event.getAction().getClass().getName();
        if (name.lastIndexOf('.') > 0) {
            name = name.substring(name.lastIndexOf('.') + 1);
        }
        logger.debug("Dispatching action - " + name);
        if (CacheRegistrator.hasCacheHandler(event.getAction())) {
            CacheRegistrator.getCacheHandler(event.getAction()).doAction(event.getAction(), event.getCallback(), event.getActionDelegate());
        } else {
            ActionExecutor.execute(event.getAction(), event.getCallback(), event.getActionDelegate());
        }
    }

    @Override
    public void onApplicationSecurity(ApplicationSecurityEvent event) {
        logger.debug("ApplicationSecurityEvent");
        EventBus.get().fireEvent(new UserLogoutEvent());
    }

    @Override
    public void onAccessDenied(AccessDeniedEvent event) {
        logger.debug("AccessDeniedEvent");
        ChangeContentEvent accessDeniedEvent = new ChangeContentEvent(new AccessDeniedContent());
        EventBus.get().fireEvent(accessDeniedEvent);
    }

    /**
     * Vystreli event UriFragmentTokenEvent. Ak sa nenajde obsluha a user je
     * prihlaseny a nenajde sa obsluha tak sa nastavi deafultna straka
     * UnknownFragmentPanel, AK nie je uzivatel prihlaseny tak sa vystreli
     * UknownUriFragmentEvent.
     *
     */
    @Override
    public void onValueChange(final ValueChangeEvent<String> event) {
        Window.setTitle("Takeplace");
        LayoutBreadcrumb.get().hide();
        EventBus.get().fireEvent(new PanelItemEvent(PanelItemAction.CLEAR, PanelItemType.ADDON));

        if (StringUtils.isNotBlank(event.getValue())) {
            Cache.get().doCacheAction(new CacheAction<Boolean>() {
                @Override
                public String getName() {
                    return Cache.LOGGED;
                }

                @Override
                public void doAction(Boolean value) {
                    // Prihlaseny
                    if (value) {
                        logger.debug("URI Fragment " + event.getValue() + " when LOGGED IN");
                        processFragmentWhenLoggedIn(event.getValue());
                    } else {
                        logger.debug("URI Fragment " + event.getValue() + " when LOGGED OUT");
                        processFragmentWhenLoggedOut(event.getValue());
                    }
                }

                @Override
                public void doTimeoutAction() {
                    processFragmentWhenLoggedOut(event.getValue());
                }

                @Override
                public boolean isCancel() {
                    return true;
                }
            });
        }
    }

    private void processFragmentWhenLoggedIn(String fragment) {
        UriFragmentTokenEvent event = new UriFragmentTokenEvent(fragment);
        EventBus.get().fireEvent(event);
        // Ak nie je fragment obsluzeny tak sa nastavi defaultny content
        if (!event.isHandled()) {
            logger.debug("Setting UnknownTokenConten ");
            EventBus.get().fireEvent(new ChangeContentEvent(new UnknownTokenContent()));
        }
    }

    private void processFragmentWhenLoggedOut(String fragment) {
        UriFragmentTokenEvent event = new UriFragmentTokenEvent(fragment);
        EventBus.get().fireEvent(event);
        // Ak nie je fragment obsluzeny tak sa vystreli event ze neexistuje
        // obsluha a mal by to LogIn obsluzit
        if (!event.isHandled()) {
            logger.debug("firing UnknownUriFragmentTokenEvent");
            UnknownUriFragmentTokenEvent uufte = new UnknownUriFragmentTokenEvent(fragment);
            EventBus.get().fireEvent(uufte);
        }
    }

    @Override
    public void onModelUpdate(EventNavigationModelUpdate enmu) {

        // Vycistime                 
        EventBus.get().fireEvent(new PanelItemEvent(PanelItemAction.CLEAR, PanelItemType.ADDON));
        EventBus.get().fireEvent(new PanelItemEvent(PanelItemAction.SHOW_ADDON));

        // Nastavime navigacny ROOT pro panel aj pro Breadcrum
        LayoutPanel.get().setAddonPanelTitle(enmu.getTitle());

        for (EventNavigationItem item : enmu.getEventNavigationItems()) {
            // ak je zlozka viditelna ideme dalej inac ignorujeme
            if (item.isShowDirectory()) {
                PanelAddonItem addon = new PanelAddonItem(item.getTitle(), item.isVisible() ? item.getAction() : null);
                boolean visibleSubItem = false;
                for (EventNavigationItem subItem : item.getChilds()) {
                    if (subItem.isVisible()) {
                        addon.addSubItem(subItem.getTitle(), subItem.getUrlName(), subItem.getAction());
                        visibleSubItem = true;
                    }
                }
                if (visibleSubItem || item.isVisible()) {
                    PanelItemEvent ap1 = new PanelItemEvent(addon, PanelItemAction.ADD, PanelItemType.ADDON);
                    EventBus.get().fireEvent(ap1);
                }
            }
        }
    }

    @Override
    public void onRedirect(RedirectEvent event) {
        redirect(event.getUrl());
    }

    native void redirect(String url) /*-{
     $wnd.location.replace(url);
     }-*/;

    /** mapa aktualne uploadovanych formularu */
    public Map<String, UploadingForm> getUploadingForms() {
        return uploadingForms;
    }

    /** vraci panel pro aktualne probihajici uploady */
    public FlowPanel getUploadingFormsPlace() {
        return uploadingFormsPlace;
    }
}
