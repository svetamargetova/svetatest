package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * Rozhranie pre validavovanie komponenty. Sama komponenta sa musi validovat.
 * @author palo
 */
public interface Validable extends IsWidget {

    /**
     * Provede validaci komponenty 
     * @return chybova hlaska alebo null
     */
    public String validate();

}
