package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Komponenta, ktera automaticky po zadani souboru spusti odeslani
 * @author pepa
 */
public abstract class ConsysAutoFileUpload<U extends AbstractConsysFileUpload> extends Composite {

    // konstanty
    public static final int DEFAULT_WIDTH = 270;
    // komponenty
    private FormPanel formPanel;
    private U upload;
    private Label infoLabel;
    // data
    private String servletUrl;
    private int intWidth;
    private boolean enabled;

    public ConsysAutoFileUpload(String servletUrl) {
        this(servletUrl, DEFAULT_WIDTH);
    }

    public ConsysAutoFileUpload(String servletUrl, int intWidth) {
        this.servletUrl = servletUrl;
        this.intWidth = intWidth;
        this.enabled = true;

        InputResources.INSTANCE.css().ensureInjected();

        formPanel = createFormPanel();
        formPanel.setWidget(formDataPanel());

        SimplePanel panel = new SimplePanel(formPanel);
        panel.setStyleName(InputResources.INSTANCE.css().consysAutoFileUpload());

        initWidget(panel);
    }

    private FormPanel createFormPanel() {
        FormPanel panel = new FormPanel();
        panel.setEncoding(FormPanel.ENCODING_MULTIPART);
        panel.setMethod(FormPanel.METHOD_POST);
        panel.setAction(servletUrl);
        panel.addSubmitHandler(submitHandler());
        panel.addSubmitCompleteHandler(submitCompleteHandler());
        return panel;
    }

    private FlowPanel formDataPanel() {
        upload = uploadComponent();
        upload.addUploadChangeHandler(uploadChangeHandler());

        infoLabel = new Label();
        infoLabel.setStyleName(InputResources.INSTANCE.css().infoLabel());

        FlowPanel wrapper = new FlowPanel();
        wrapper.add(upload);
        wrapper.add(infoLabel);

        additionalFormItems(wrapper);

        return wrapper;
    }

    /** vraci info label pokud nechceme pouzivat vestaveny */
    public Label getInfoLabel() {
        return infoLabel;
    }

    /** vraci pozadovanou sirku komponenty */
    public int getIntWidth() {
        return intWidth;
    }

    /** nazev upload komponentky */
    public void setName(String name) {
        upload.setName(name);
    }

    /** povoleni/zakazani komponentky a odeslani */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        upload.setEnabled(enabled);
    }

    /** handler reagujici na odeslani formulare */
    private SubmitHandler submitHandler() {
        return new SubmitHandler() {

            @Override
            public void onSubmit(SubmitEvent event) {
                ConsysAutoFileUpload.this.onSubmit();
                stateUploading();
            }
        };
    }

    /** handler po dokonceni odesilani */
    private SubmitCompleteHandler submitCompleteHandler() {
        return new SubmitCompleteHandler() {

            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {
                // zpracovat odpoved
                boolean standardProcess = processResult(event.getResults());
                stateUploaded(standardProcess);
            }
        };
    }

    /** stav komponenty behem uploadu */
    private void stateUploading() {
        // supneme upload bokem at nezaclani a nejde na nej znovu klikat dokud upload neskonci
        DOM.setStyleAttribute(upload.getElement(), "position", "absolute");
        DOM.setStyleAttribute(upload.getElement(), "left", "-4000px");
        // vypiseme informaci
        infoLabel.removeStyleName(InputResources.INSTANCE.css().uploaded());
        infoLabel.removeStyleName(InputResources.INSTANCE.css().error());
        StyleUtils.addStyleNameIfNotSet(infoLabel, InputResources.INSTANCE.css().uploading());
        infoLabel.setText(UIMessageUtils.c.consysAutoFileUpload_text_uploading());
    }

    /** stav komponenty po dokonceni uploadu */
    private void stateUploaded(boolean standardProcess) {
        // vratime upload na puvodni pozici
        upload.getElement().getStyle().clearPosition();
        upload.getElement().getStyle().clearLeft();
        // vypiseme informaci
        if (standardProcess) {
            infoLabel.removeStyleName(InputResources.INSTANCE.css().error());
            infoLabel.removeStyleName(InputResources.INSTANCE.css().uploading());
            StyleUtils.addStyleNameIfNotSet(infoLabel, InputResources.INSTANCE.css().uploaded());
            infoLabel.setText(UIMessageUtils.c.consysAutoFileUpload_text_uploaded());
        }
    }

    /** po zadani souboru se automaticky odesle */
    private ChangeHandler uploadChangeHandler() {
        return new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (enabled) {                    
                    if (validateSuffix()) {
                        formPanel.submit();
                    } else {
                        invalidSuffix();
                    }
                }
            }
        };
    }

    /** provode kontrolu na koncovku souboru */
    private boolean validateSuffix() {
        String[] suffixs = validSuffixs();
        if (suffixs == null || suffixs.length == 0) {
            // je povoleno cokoliv
            return true;
        }

        String fileName = upload.getFilename().toLowerCase();
        for (int i = 0; i < suffixs.length; i++) {
            if (fileName.endsWith(suffixs[i].toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    /** nastavi hlasku a styl, ze je neplatna koncovka */
    private void invalidSuffix() {
        String[] suffixs = validSuffixs();
        String required = suffixs[0];

        for (int i = 1; i < suffixs.length; i++) {
            required += ", " + suffixs[i];
        }

        StyleUtils.addStyleNameIfNotSet(infoLabel, InputResources.INSTANCE.css().error());
        infoLabel.setText(suffixs.length == 1
                ? UIMessageUtils.m.consysAutoFileUpload_error_invalidExtension(required)
                : UIMessageUtils.m.consysAutoFileUpload_error_invalidExtensions(required));
    }

    /** vraci vytvorenou uploadovaci komponentu */
    protected abstract U uploadComponent();

    /** pridani dodatecnych polozek formulare (urceno k pretizeni) */
    protected abstract void additionalFormItems(FlowPanel panel);

    /** vola se po spusteni odesilani */
    protected abstract void onSubmit();

    /** zpracovani odpovedi ze serveru (urceno k pretizeni), vraci true pokud se ma standardne zpracovat infoLabel */
    protected abstract boolean processResult(String result);

    /** vraci povolene koncovky, prazdne pole nebo null znaci, ze je povoleno vsechno */
    protected abstract String[] validSuffixs();
}
