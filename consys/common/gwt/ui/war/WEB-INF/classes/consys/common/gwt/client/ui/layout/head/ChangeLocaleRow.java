package consys.common.gwt.client.ui.layout.head;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.utils.LocaleUtils;
import consys.common.gwt.client.ui.utils.ParamUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import java.util.ArrayList;

/**
 * Vyber lokalizace
 * @author pepa
 */
public class ChangeLocaleRow extends FlowPanel {

    public ChangeLocaleRow() {
        addStyleName(ResourceUtils.system().css().changeLocale());

        String cookie = LocaleUtils.getLocale();
        if (ParamUtils.isSet("locale")) {
            // je nastaven parametr lokalizace v url, tak ho pouzijeme, protoze ma prednost
            cookie = ParamUtils.getValue("locale");
        }

        if (!cookie.equals(LocaleUtils.LOCALE_ENGLISH) && !cookie.equals(LocaleUtils.LOCALE_CZECH)) {
            // nepodporovana lokalizace, nastavime vychozi
            cookie = LocaleUtils.LOCALE_ENGLISH;
        }

        Image flagGreatBritain = new Image(ResourceUtils.system().flagUSGreatBritain());
        Image flagCzech = new Image(ResourceUtils.system().flagCzech());

        // popisky se nelokalizují, protože zůstavají pořád stejné v jazyku konkrétní lokalizace, do které přepínají
        ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
        items.add(new SelectBoxItem<String>(LocaleUtils.LOCALE_ENGLISH, "English", flagGreatBritain.toString()));
        items.add(new SelectBoxItem<String>(LocaleUtils.LOCALE_CZECH, "Čeština", flagCzech.toString()));

        SelectBox<String> localeSelect = new SelectBox<String>();
        localeSelect.setWidth(150);
        localeSelect.addChangeValueHandler(LocaleUtils.changeLocaleHandler());
        localeSelect.setItems(items);
        localeSelect.selectItem(cookie);

        // ulozeni cookie -> prodlouzeni o dalsi casovou periodu
        LocaleUtils.setLocale(cookie);

        add(localeSelect);
    }
}
