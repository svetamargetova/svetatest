/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.ui.comp.form;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author palo
 */
public interface FormElement{
                    
    public boolean isValidable();
    
    public boolean doValidation();
    
    public Widget getTitleWidget();

    public Widget getValueWidget();
    
    /**
     * Nastavi ze tento form elementobsahuje titulku a preto sa nastavi vacsia
     * medzera medzi tymto a nasledujucim elementom.
     * 
     * @param title 
     */
    public void setTitle(boolean title);
}
