package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.ui.comp.Help;

/**
 *
 * @author pepa
 */
public class HelpEvent extends GwtEvent<HelpEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private Help help;

    public HelpEvent(Help help) {
        this.help = help;
    }

    public Help getHelp() {
        return help;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onShowHelp(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onShowHelp(HelpEvent helpEvent);
    }
}
