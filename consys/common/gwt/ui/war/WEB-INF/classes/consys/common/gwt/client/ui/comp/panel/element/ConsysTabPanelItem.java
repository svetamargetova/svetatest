package consys.common.gwt.client.ui.comp.panel.element;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;

/**
 * interface pro polozky ConsysTabPanelu
 * @author pepa
 */
public interface ConsysTabPanelItem<T> {

    /** vraci widget titulku */
    public Widget getTitleNote();

    /** vraci widget obsahu */
    public Widget getContent();

    /** pouziva ConsysTabPanel pro nastaveni sveho odkazu do potomka */
    public void setConsysTabPanel(ConsysTabPanel<T> tabPanel);
}
