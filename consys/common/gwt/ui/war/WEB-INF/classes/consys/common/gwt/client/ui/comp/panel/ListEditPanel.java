package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.ListEditPanelItem.RemoveDelegate;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import java.util.ArrayList;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListEditPanel<T extends ListEditPanelItem> extends FlowPanel implements RemoveDelegate<T> {

    // Komponenty
    private ListPanel<T> itemList;
    private ActionLabel action;
    // Data
    /** Nove zaznamy */
    private ArrayList<T> newMembers;
    /** Odstranene zaznamy */
    private ArrayList<T> removedMembers;
    /** Povodne zaznamy */
    private ArrayList<T> pureItems;

    public void setActionName(String actionName) {
        action.setText(actionName);
    }

    public void setActionClickHandler(ClickHandler clickHandler) {
        action.setClickHandler(clickHandler);
    }

    public void setStaticHeight() {
        itemList.setStaticHeight();
    }

    public ListEditPanel(ArrayList<T> items, int cellHeight, int visibleCells) {
        newMembers = new ArrayList<T>();
        removedMembers = new ArrayList<T>();
        pureItems = items;
        itemList = new ListPanel<T>(cellHeight, visibleCells);
        action = new ActionLabel("");
    }

    @Override
    protected void onLoad() {
        SimplePanel actionWrapper = new SimplePanel();
        actionWrapper.setWidget(action);
        actionWrapper.setStyleName(ResourceUtils.system().css().listPanelEditAction());
        add(actionWrapper);

        for (T item : getPureItems()) {
            item.setRemoveDelegate(this);
            itemList.addItem(item);
        }
        add(itemList);
    }

    /** prida styl do list panelu */
    public void addListStyleName(String styleName) {
        itemList.addStyleName(styleName);
    }

    public void addItem(T item) {
        // overeni jestli uz neni vlozen
        boolean inList = false;
        for (T t : newMembers) {
            if (t.equals(item)) {
                inList = true;
                break;
            }
        }
        if (!inList) {
            for (T t : pureItems) {
                if (t.equals(item)) {
                    inList = true;
                    break;
                }
            }
        }
        boolean inRList = false;
        for (T t : removedMembers) {
            if (t.equals(item)) {
                inRList = true;
                break;
            }
        }
        // pokud neni vlozime
        if (!inList) {
            item.setRemoveDelegate(this);
            itemList.addItem(item);
            newMembers.add(item);
        } else if (inRList) {
            itemList.addItem(item);
        }
        // pokud byl odstranen a znovu se vlozi
        if (inRList) {
            removedMembers.remove(item);
        }
    }

    @Override
    public void didRemoveItem(T item) {
        removedMembers.add(item);
        itemList.removeItem(item.getIndex());
        boolean inList = false;
        for (T t : newMembers) {
            if (t.equals(item)) {
                inList = true;
                break;
            }
        }
        if (inList) {
            newMembers.remove(item);
        }
    }

    /**
     * @return the newMembers
     */
    public ArrayList<T> getNewMembers() {
        return newMembers;
    }

    /**
     * @return the removedMembers
     */
    public ArrayList<T> getRemovedMembers() {
        return removedMembers;
    }

    /**
     * @return the pureItems
     */
    public ArrayList<T> getPureItems() {
        return pureItems;
    }

    /** vrati vsechny polozky v seznamu */
    public ArrayList<T> getAllItems() {
        return itemList.getItems();
    }
}
