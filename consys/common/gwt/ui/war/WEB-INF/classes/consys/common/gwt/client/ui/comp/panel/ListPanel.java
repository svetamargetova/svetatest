package consys.common.gwt.client.ui.comp.panel;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import java.util.ArrayList;

/**
 *  
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListPanel<T extends ListPanelItem> extends ScrollPanel implements ActionExecutionDelegate {
    
    private ArrayList<T> items;
    private boolean selectable;
    private T selectedItem;
    private ListPanelDelegate<T> delegate;
    // Podpora pre nastveni vysky
    private int visibleItems;
    private int cellHeight;
    private boolean scrollbarVisible = false;

    public ListPanel(int cellHeight, int visibleItems) {
        super(new FlowPanel());
        setAlwaysShowScrollBars(false);
        setStyleName(ResourceUtils.system().css().listPanel());
        items = new ArrayList<T>();
        selectable = false;
        this.visibleItems = visibleItems;
        this.cellHeight = cellHeight;
        setWidth("350px");
    }

    public void removeItem(int index) {
        T item = items.get(index);
        item.removeFromParent();
        items.remove(index);
        refreshUnselected();
    }

    private FlowPanel getPanel() {
        return (FlowPanel) getWidget();
    }

    public void refreshUnselected() {
        for (int i = 0; i < items.size(); i++) {
            ListPanelItem listPanelItem = items.get(i);
            listPanelItem.setIndex(i);
            listPanelItem.unselected();
        }
    }

    private void setSelected(T newItem) {
        if (selectable) {
            if (selectedItem != null) {
                selectedItem.unselected();
            }
            newItem.selected();
            selectedItem = newItem;
            if (delegate != null) {
                delegate.didSelect(newItem);
            }
        }
    }

    @Override
    public void clear() {
        items.clear();
        getPanel().clear();
        scrollbarVisible = false;
    }

    public void setItems(ArrayList<T> items) {
        for (T listPanelItem : items) {
            addItem(listPanelItem);
        }
    }

    public void addItem(final T item) {
        if (!scrollbarVisible && items.size() > visibleItems) {
            contentIsScrollable();
        }
        item.setIndex(items.size());
        items.add(item);
        if (selectable && item.isSelectable()) {
            item.addStyleName(CssStyles.HAND);
        }
        item.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setSelected(item);
            }
        });
        getPanel().add(item);
    }

    @Override
    public void actionStarted() {
        GWT.log("TODO: nasetovat ajax kolecko");
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
    }

    public ArrayList<T> getItems() {
        return items;
    }

    /**
     * @return the delegate
     */
    public ListPanelDelegate<T> getDelegate() {
        return delegate;
    }

    /**
     * @param delegate the delegate to set
     */
    public void setDelegate(ListPanelDelegate<T> delegate) {
        setSelectable(true);
        this.delegate = delegate;
    }

    /**
     *
     * Priznak ci sa mozu oznacovat jednotlive itemy
     */
    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public void setStaticHeight() {
        contentIsScrollable();
        DOM.setStyleAttribute(getElement(), "overflowY", "scroll");
    }

    private void contentIsScrollable() {
        setHeight(visibleItems * cellHeight + "px");
        scrollbarVisible = true;
    }

    public interface ListPanelDelegate<T> {

        public void didSelect(T item);
    }
}
