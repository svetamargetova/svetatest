package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pepa
 */
public class ChangeValueEvent<T> extends GwtEvent<ChangeValueEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private Widget component;
    private T oldValue;
    private T value;

    public ChangeValueEvent(Widget component, T oldValue, T value) {
        this.component = component;
        this.oldValue = oldValue;
        this.value = value;
    }

    /** vraci komponentu, ktera udalost vyvolala */
    public Widget getComponent() {
        return component;
    }

    /** vraci starou hodnotu */
    public T getOldValue() {
        return oldValue;
    }

    /** vraci novou hodnotu */
    public T getValue() {
        return value;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onChangeValue(this);
    }

    /** interface eventu */
    public interface Handler<I> extends EventHandler {

        public void onChangeValue(ChangeValueEvent<I> event);
    }
}
