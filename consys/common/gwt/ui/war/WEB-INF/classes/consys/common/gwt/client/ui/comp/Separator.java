package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * Separator (bily pixel, sedy pixel)
 * @author pepa
 */
public class Separator extends Composite {

    public Separator() {
        this("100%", null);
    }

    public Separator(String width) {
        this(width, null);
    }

    /**
     * @param width sirka separatoru
     * @param marginStyle styl pridany k separatoru (mel by byt jen vertikalni margin)
     */
    public Separator(String width, String marginStyle) {
        SimplePanel panel = new SimplePanel();
        panel.setSize(width, "1px");
        panel.setStyleName(ResourceUtils.system().css().separator());
        if (marginStyle != null) {
            panel.addStyleName(marginStyle);
        }
        initWidget(panel);
    }

    /** vraci separator s jinou sirkou nez vychozi */
    public static Separator changedWidth(String width) {
        return new Separator(width);
    }

    /** vraci separator s pridanym stylem */
    public static Separator addedStyle(String marginStyle) {
        return new Separator("100%", marginStyle);
    }

    /** vraci separator s width 100% */
    public static Separator separator() {
        return new Separator("100%");
    }
}
