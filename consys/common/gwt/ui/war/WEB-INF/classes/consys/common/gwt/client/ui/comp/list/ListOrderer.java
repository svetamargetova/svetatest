package consys.common.gwt.client.ui.comp.list;

/**
 *
 * @author palo
 */
public abstract class ListOrderer extends Itemizer {

    private int tag;

    public ListOrderer(int tag) {
        this.tag = tag;
    }

    /**
     * Metoda zaisti ze orderer nebude nijak oznaceny
     */
    public abstract void deselect();

    /**
     * Metoda zaisti ze orderer oznaci svoju prvu poziciu
     */
    public abstract void select();

    @Override
    public void refresh() {
        getParentList().refresWithOrderer(this);
    }

    @Override
    public int getTag() {
        return tag;
    }
}
