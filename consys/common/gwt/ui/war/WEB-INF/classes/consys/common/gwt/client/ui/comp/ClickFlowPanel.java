package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlowPanel;

/**
 * Klikatelny FlowPanel
 * @author pepa
 */
public class ClickFlowPanel extends FlowPanel {

    public ClickFlowPanel() {
        super();
        sinkEvents(Event.ONCLICK);
    }

    /** zaregistruje ClickHandler na wrapper titulku */
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return addHandler(handler, ClickEvent.getType());
    }
}
