package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.StringResult;


/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */ 
public class ListDataSourceImpl implements ListDataSource<StringResult> {

    @Override
    public void sendRequest(ListDataSourceRequest request, AsyncCallback<ListDataSourceResult> callback, ActionExecutionDelegate executionDelegate) {
        EventBus.get().fireEvent(new EventDispatchEvent(request,callback,executionDelegate));
    }
}
