package consys.common.gwt.client.ui.layout.menu;

import com.google.gwt.user.client.ui.Widget;

/**
 * trida jedne polozky v hlavnich menu (top, bottom)
 * @author pepa
 */
public class MenuItem {

    /** text zobrazeny v polozce menu */
    private final String title;
    /** history token */
    private final String token;
    /** widget ktery se ma po zobrazeni polozky zobrazit */
    private Widget content;

    public MenuItem(String title, String token) {
        this.title = title;
        this.token = token;
    }

    /** nazev menu */
    public String getTitle() {
        return title;
    }

    /** history token */
    public String getToken() {
        return token;
    }

    /** vraci obsah, ktery se ma zobrazit v obsahu po vybrani polozky */
    public Widget getContent() {
        return content;
    }

    /** nastavuje widget, ktery se ma zobrazit po vybrani polozky */
    public void setContent(Widget content) {
        this.content = content;
    }
}
