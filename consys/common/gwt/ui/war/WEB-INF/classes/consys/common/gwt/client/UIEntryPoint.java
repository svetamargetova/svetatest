package consys.common.gwt.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysAsyncAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.action.LoadUserInfoAction;
import consys.common.gwt.client.rpc.action.LoadUserThumbAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.base.UnknownTokenContent;
import consys.common.gwt.client.ui.comp.*;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.common.gwt.client.ui.comp.input.ConsysPasswordBox;
import consys.common.gwt.client.ui.comp.input.ConsysRichTextArea;
import consys.common.gwt.client.ui.comp.input.ConsysRichTextToolbar;
import consys.common.gwt.client.ui.comp.input.ConsysSelectBox;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.*;
import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import consys.common.gwt.client.ui.comp.panel.OrderPanel.OrderPanelListener;
import consys.common.gwt.client.ui.comp.panel.*;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.comp.panel.element.OrderPanelItem;
import consys.common.gwt.client.ui.comp.progress.Progress;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.tag.TagDisplay;
import consys.common.gwt.client.ui.comp.tag.TagSuggest;
import consys.common.gwt.client.ui.comp.user.UserCard;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.comp.user.list.*;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.common.gwt.client.ui.debug.ListDataSourceDebugImpl;
import consys.common.gwt.client.ui.event.BreadcrumbContentEvent;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.PanelItemEvent;
import consys.common.gwt.client.ui.layout.LayoutPanel;
import consys.common.gwt.client.ui.layout.menu.MenuDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelAddonItem;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.layout.panel.PanelItemAction;
import consys.common.gwt.client.ui.layout.panel.PanelItemType;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.client.utils.DetectTimeZone;
import consys.common.gwt.client.utils.TimeZoneProvider;
import consys.common.gwt.client.widget.LoginForm;
import consys.common.gwt.shared.action.CheckCorporationAction;
import consys.common.gwt.shared.bo.*;
import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pepa
 */
public class UIEntryPoint extends DebugModuleEntryPoint implements OrderPanelListener<OrderPanelItem> {

    private static int rootCount = 0;
    Logger logger = Logger.getLogger("LOGGER");
    // komponenty
    private OrderDialog od;

    @Override
    public void onOrderChange(OrderPanelItem object, int oldPosition, int newPosition) {
        /*if (object != null) {
         Window.alert(object.getName() + "; " + oldPosition + " -> " + "; -> " + newPosition);
         } else {
         Window.alert("object is null");
         }*/
        Window.alert("onOrderChange");
    }

    @Override
    public void initMocks() {
        logger.log(Level.INFO, "Loguju v GWT");
        /*---- MOCKS ------*/
        ClientUserThumb cut = new ClientUserThumb();
        cut.setName("Jindrisek Veliky");
        cut.setUuid("12");
        //cut.setUuidPortraitImagePrefix("123");
        MockFactory.addActionMock(LoadUserThumbAction.class, new ActionMock<ClientUserThumb>(cut));

        // ---- ListUserAffiliationThumbsAction
        ArrayListResult<ClientUserWithAffiliationThumb> listThumbs = new ArrayListResult<ClientUserWithAffiliationThumb>();
        for (int i = 0; i < 20; i++) {
            ClientUserWithAffiliationThumb cult = new ClientUserWithAffiliationThumb();
            cult.setName("Ivan Krasko");
            cult.setPosition("Dictator of Democratic republic of Kenya, Kongo, J.A.R and other free republics");
            listThumbs.getArrayListResult().add(cult);
        }
        MockFactory.addActionMock(ListUserAffiliationThumbsAction.class, new ActionMock<ArrayListResult<ClientUserWithAffiliationThumb>>(listThumbs));

        // ---- ListUserThumbsAction
        ArrayListResult<ClientUserThumb> userThumbs = new ArrayListResult<ClientUserThumb>();
        for (int i = 0; i < 20; i++) {
            ClientUserThumb cult = new ClientUserThumb();
            cult.setName("prof. Doc. Martin Kukucin, CSC");
            userThumbs.getArrayListResult().add(cult);
        }
        MockFactory.addActionMock(ListUserThumbsAction.class, new ActionMock<ArrayListResult<ClientUserThumb>>(userThumbs));

        // ---- ListDataSourceRequest
        ArrayList<DataListUuidItem> items = new ArrayList<DataListUuidItem>();
        for (int i = 1; i <= 56; i++) {
            DataListUuidItem item = new DataListUuidItem();
            item.setUuid("Objekt " + i);
        }
        ListDataSourceResult dataSourceResult = new ListDataSourceResult(items, 300);
        MockFactory.addActionMock(ListDataSourceRequest.class, new ActionMock<ListDataSourceResult>(dataSourceResult));

        ClientUserCommonInfo cui = new ClientUserCommonInfo();
        cui.setFullName("Lojzin Statečný");
        cui.setPosition("Popelář");
        cui.setOrganization(".A.S.A.");
        //cui.setImageUuid("32424");
        cui.setUuid("user123123");
        cui.setEmail("mail@email.djdj.co");
        MockFactory.addActionMock(LoadUserInfoAction.class, new ActionMock<ClientUserCommonInfo>(cui));
        
        //----------------------------------------------
        MockFactory.addActionMock(CheckCorporationAction.class, new ActionMock<BooleanResult>(new BooleanResult(false)));
    }

    @Override
    public void registerModules(List<Module> modules) {
    }

    @Override
    public void registerForms(Map<String, Widget> formMap) {
        Label ropTitle = new Label("my title");
        Label popisekLabel = new Label("zabalovaný popisek");
        final RollOutPanel rop = new RollOutPanel() {

            @Override
            protected void onOpenRoll(ConsysAsyncAction onAction) {
                onAction.onSuccess();
            }

            @Override
            public void setFailMessage(CommonExceptionEnum value) {
            }
        };
        rop.setTitle(ropTitle);
        rop.setContent(popisekLabel);
        /*rop.setOrderersActions(true, new ConsysAction() {
        
         @Override
         public void run() {
         rop.showUpOrderer(false);
         }
         }, new ConsysAction() {
        
         @Override
         public void run() {
         rop.showDownOrderer(false);
         }
         });*/

        RollOutPanel rop1 = new RollOutPanel() {

            @Override
            protected void onOpenRoll(ConsysAsyncAction onAction) {
                onAction.onSuccess();
            }

            @Override
            public void setFailMessage(CommonExceptionEnum value) {
            }
        };
        rop1.setTitle(new Label("Název"));
        rop1.setContent(new Label("popisk"));

        final FlowPanel frop = new FlowPanel();
        frop.add(rop);
        frop.add(rop1);

        final ConsysTabPanel<Integer> tabsV2 = new ConsysTabPanel<Integer>("pokus");
        tabsV2.showOrderIcon(new ConsysAction() {

            @Override
            public void run() {
                ArrayList<OrderPanelItem> lis = new ArrayList<OrderPanelItem>();
                for (int i = 0; i < 1; i++) {
                    lis.add(new OrderPanelItem("Název položky " + (i + 1)));
                }
                od = new OrderDialog(lis, UIEntryPoint.this);
                od.showCentered();
            }
        });
        for (int i = 1; i <= 5; i++) {
            final int row = i;
            ConsysTabPanelLabel<Integer> lab = new ConsysTabPanelLabel<Integer>(i, "panel " + row);
            lab.setTab(tabsV2);
            tabsV2.addTabItem(lab, new ConsysTabPanelItem() {

                private Label label = new Label("Panel " + row);

                @Override
                public Widget getTitleNote() {
                    return null;
                }

                @Override
                public Widget getContent() {
                    switch (row) {
                        case 1:
                            return frop;
                        case 2:
                            ArrayList<OrderPanelItem> items = new ArrayList<OrderPanelItem>();
                            for (int i = 1; i <= 5; i++) {
                                items.add(new OrderPanelItem("Popisek " + i));
                            }
                            final OrderPanel orderPanel = new OrderPanel<OrderPanelItem>(items);
                            orderPanel.addListener(UIEntryPoint.this);

                            ActionImage orderButton = ActionImage.getBackButton("back move", false);
                            orderButton.addClickHandler(new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    orderPanel.back();
                                }
                            });

                            FlowPanel ppp = new FlowPanel();
                            ppp.add(orderPanel);
                            ppp.add(orderButton);
                            return ppp;
                        default:
                            ActionImage im = ActionImage.getCheckButton("Select tab2");
                            im.addClickHandler(new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    tabsV2.selectTabById(2);
                                }
                            });
                            FlowPanel pnl = new FlowPanel();
                            pnl.add(label);
                            pnl.add(im);
                            return pnl;
                    }
                }

                @Override
                public void setConsysTabPanel(ConsysTabPanel tabPanel) {
                }
            });
        }

        ArrayList<SelectBoxItem<String>> sitems = new ArrayList<SelectBoxItem<String>>();
        for (int i = 1; i <= 50; i++) {
            sitems.add(new SelectBoxItem<String>("jedna" + i, i + " zaznam " + i + " neco delsiho a budu videt co tu drbe, snad"));
        }

        final SelectBox<String> selectBox = new SelectBox<String>();
        selectBox.setItems(sitems);
        selectBox.selectFirst(true);
        selectBox.selectItem(sitems.get(43).getItem());

        TagDisplay tagDisplay = new TagDisplay();
        tagDisplay.setStringTags("koníčci, Nissan GTR", ",");
        tagDisplay.setReadMode(false);

        TagSuggest tagSuggest = new TagSuggest(tagDisplay);
        ArrayList<ClientTag> tags = new ArrayList<ClientTag>();
        tags.add(new ClientTag(1l, "Auto"));
        tags.add(new ClientTag(1l, "Auto jedna"));
        tags.add(new ClientTag(1l, "Auto jelen"));
        tags.add(new ClientTag(1l, "Auto dva"));
        tags.add(new ClientTag(1l, "Kočka"));
        tags.add(new ClientTag(1l, "Kočička"));
        tags.add(new ClientTag(1l, "Kočkodan"));
        tagSuggest.setOracleData(tags);

        final ConsysTextArea textArea = new ConsysTextArea(1, 0);
        textArea.addStyleName(StyleUtils.MARGIN_TOP_20);
        final ConsysWordArea wordArea = new ConsysWordArea(1, 3);

        ActionLabel worda = new ActionLabel("check word area");
        worda.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                wordArea.doValidate(ConsysMessage.getFail());
            }
        });

        Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, TimeZoneProvider.getTimeZoneByID("Europe/London"));

        ConsysRichTextArea area = new ConsysRichTextArea();
        ConsysRichTextToolbar toolbar = new ConsysRichTextToolbar(area);

        DetectTimeZone dtz = new DetectTimeZone();
        dtz.init();
        String offset = dtz.offset() + "(" + dtz.minutesOffset() + ")>>" + dtz.name() + ">>" + dtz.dst();

        int eventOffset = -60;
        Date nowDate = new Date();
        long timeNow = nowDate.getTime();
        Date eventDate = DateTimeUtils.computeEventDate(nowDate, TimeZone.createTimeZone(eventOffset));
        long timeEvent = eventDate.getTime();

        Date date = nowDate;

        Date testDate1 = new Date(1316736000000l); // u nas v lete cas 23.9.2011 02:00 (gmt 23.9.2011 00:00) event +1
        Date testDate2 = new Date(1316728800000l); // u nas v lete cas 23.9.2011 00:00 (gmt 22.9.2011 22:00) event +1
        Date testDate3 = new Date(1316725200000l); // u nas v lete cas 22.9.2011 23:00 (gmt 22.9.2011 21:00) event +1

        final Label resultLabel = new Label();

        final ConsysDateBox cdb = ConsysDateBox.getBox(true);
        cdb.setShowDateAs(ConsysDateBox.ShowDateAs.EVENT);
        cdb.setValue(testDate3);
        ActionImage dateButton = ActionImage.getCheckButton("get date");
        dateButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resultLabel.setText(cdb.getValue().toString());
            }
        });

        ConsysFileUpload fileUpload = new ConsysFileUpload();
        fileUpload.setEnabled(false);
        ConsysFileUpload fileUpload1 = new ConsysFileUpload();

        FlowPanel selectBoxPanel = new FlowPanel();
        selectBoxPanel.add(new Label("Setting date: " + testDate3));
        selectBoxPanel.add(resultLabel);
        selectBoxPanel.add(cdb);
        selectBoxPanel.add(dateButton);
        /*selectBoxPanel.add(new Label("Is DST for: " + date + ": " + dtz.getDateOffset((int) (date.getTime() / 1000))));
         selectBoxPanel.add(new Label("Event offset: " + eventOffset + " minutes"));
         selectBoxPanel.add(new Label("Now date  : " + nowDate + ", " + timeNow));
         selectBoxPanel.add(new Label("Event date: " + eventDate + ", " + timeEvent));
         selectBoxPanel.add(new Label("Offset datumu je: " + offset));*/
        selectBoxPanel.add(toolbar);
        selectBoxPanel.add(area);
        selectBoxPanel.add(fileUpload);
        selectBoxPanel.add(fileUpload1);
        selectBoxPanel.add(tabsV2);
        selectBoxPanel.add(tagSuggest);
        selectBoxPanel.add(tagDisplay);
        selectBoxPanel.add(StyleUtils.clearDiv());
        selectBoxPanel.add(textArea);
        selectBoxPanel.add(wordArea);
        selectBoxPanel.add(worda);
        selectBoxPanel.add(selectBox);

        ActionLabel al = new ActionLabel("search dialog");
        al.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog searchDialog = new UserSearchDialog();
                searchDialog.setDelegate(new SearchDelegate<ClientInvitedUser>() {

                    @Override
                    public void didSelect(ClientInvitedUser items) {
                    }
                });
                searchDialog.showCentered();
            }
        });
        selectBoxPanel.add(al);

        ActionLabel onFail = new ActionLabel("onFail");
        onFail.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                selectBox.onFail();
            }
        });
        selectBoxPanel.add(onFail);
        ActionLabel onSuccess = new ActionLabel("onSuccess");
        onSuccess.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                selectBox.onSuccess();
            }
        });
        selectBoxPanel.add(onSuccess);
        ActionLabel disableSB = new ActionLabel("Disable");
        disableSB.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                selectBox.setEnabled(false);
            }
        });
        selectBoxPanel.add(disableSB);
        ActionLabel enableSB = new ActionLabel("Enable");
        enableSB.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                selectBox.setEnabled(true);
            }
        });
        selectBoxPanel.add(enableSB);
        ActionLabel getSB = new ActionLabel("Get");
        getSB.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                SelectBoxItem i = selectBox.getSelectedItem();
                Window.alert(i == null ? "null" : i.getItem().toString());
            }
        });
        selectBoxPanel.add(getSB);


        Progress p = Progress.register(1);
        p.addStyleName(StyleUtils.MARGIN_TOP_10);
        selectBoxPanel.add(p);

        SelectBox<String> selectBox1 = new SelectBox<String>();
        selectBox1.setItems(sitems);
        selectBox1.selectFirst(true);
        selectBoxPanel.add(selectBox1);

        String s = "";
        String[] ar = LocaleInfo.getCurrentLocale().getDateTimeFormatInfo().monthsShort();
        //String[] ar = LocaleInfo.getAvailableLocaleNames();
        for (int i = 0; i < ar.length; i++) {
            s += ar[i] + ",";
        }
        Label infoLabel = new Label(s);
        selectBoxPanel.add(infoLabel);
        selectBoxPanel.add(ConsysDateBox.getBox());
        selectBoxPanel.add(ConsysDateBox.getHalfBox());


        /**********************************************************************/
        // DATA LIST
        DataListPanel list = new DataListPanel<String, TestListItem>("Test data list panel", "1", new TestListDelegate());
        list.setListDataSource(new ListDataSourceDebugImpl());
        DataListCommonOrderer orderer = new DataListCommonOrderer();
        orderer.addOrderer("Name", 1);
        orderer.addOrderer("Date", 2);
        list.addHeadPanel(orderer);

        list.setDefaultFilter(new ListFilter(1) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
        list.addLeftControll(ActionImage.getConfirmButton("You little shit!", true));
        list.addLeftControll(ActionImage.getConfirmButton("Fuck you, moron!", true));
        list.addLeftControll(ActionImage.getConfirmButton("Fuck you too", true));
        list.enableExport(ListExportTypeEnum.EXCEL, ListExportTypeEnum.PDF);

        DownMenu dm = new DownMenu();
        DownMenu.DownMenuItem item1 = new DownMenu.DownMenuItem() {

            @Override
            public String getTitle() {
                return "Subbundles";
            }

            @Override
            public void doAction() {
                FormUtils.fireNextBreadcrumb("Discount coupons", new UnknownTokenContent());
            }
        };
        DownMenu.DownMenuItem item2 = new DownMenu.DownMenuItem() {

            @Override
            public String getTitle() {
                return "System properties";
            }

            @Override
            public void doAction() {
                FormUtils.fireNextBreadcrumb("System properties", new UnknownTokenContent());
            }
        };


        list.enableDownMenu(item1, item2);

        /**********************************************************************/
        // U S E R   C O M P O N E N T S
        final Form userComponents = new Form();

        // Search User Dialog
        ActionLabel actionLabel = new ActionLabel("Add user");
        actionLabel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {

                UserSearchDialog sd = new UserSearchDialog();
                sd.showCentered();
            }
        });
        userComponents.addSeparator();
        userComponents.addOptional("Search User Dialog", actionLabel);

        // User List Panel

        final UserListPanel userPanel = new UserListPanel();
        ArrayList<String> userlist = new ArrayList<String>();
        for (int i = 0; i < 15; i++) {
            userlist.add("1");
        }

        userComponents.addSeparator();
        userPanel.setUsers(userlist);

        userComponents.addOptional("User List Panel", userPanel);

        // User Edit List Panel

        // Potrebuje data od zaciatku
        ArrayList<ClientUserWithAffiliationThumb> listThumbs = new ArrayList<ClientUserWithAffiliationThumb>();
        for (int i = 0; i < 20; i++) {
            ClientUserWithAffiliationThumb cult = new ClientUserWithAffiliationThumb();
            cult.setName("Ivan Krasko");
            cult.setPosition("Dictator of Democratic republic of Kenya, Kongo, J.A.R and other free republics");
            listThumbs.add(cult);
        }

        UserListEditPanel editUserList = new UserListEditPanel(listThumbs);
        editUserList.setAddLabelName("Add New User");

        userComponents.addSeparator();
        userComponents.addOptional("Edit User List Panel", editUserList);

        /// User Label List Panel
        UserLabelListPanel labelListPanel = new UserLabelListPanel();
        labelListPanel.setUsers(userlist);
        labelListPanel.setStaticHeight();
        userComponents.addSeparator();
        userComponents.addOptional("User Label List Panel", labelListPanel);

        /// User Label Edit List Panel
        ArrayList<ClientUserThumb> userThumbs = new ArrayList<ClientUserThumb>();
        for (int i = 0; i < 20; i++) {
            ClientUserThumb cult = new ClientUserThumb();
            cult.setName("prof. Doc. Martin Kukucin, CSC");
            userThumbs.add(cult);
        }
        UserLabelListEditPanel labelListEditPanel = new UserLabelListEditPanel(userThumbs);
        labelListEditPanel.setStaticHeight();
        labelListEditPanel.setAddLabelName("Add New User");
        userComponents.addSeparator();
        userComponents.addOptional("User Label List Edit Panel", labelListEditPanel);



        /**********************************************************************/
        // HELP PANEL
        final SimpleFormWithHelpPanel helpPanel = new SimpleFormWithHelpPanel();
        helpPanel.addHelpTitle("Help Title");
        helpPanel.addHelpText("Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text ");


        FlowPanel fp = new FlowPanel();
        fp.add(new LoginForm(new ConsysAction() {

            @Override
            public void run() {
                Window.alert("login action run");
            }
        }));

        ConsysMessage cm = ConsysMessage.getInfo();
        cm.setText("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nunc dapibus tortor vel mi dapibus sollicitudin. Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus.");
        fp.add(cm);

        //fp.add(new SelectDatePicker());
        SelectDatePicker req = new SelectDatePicker();
        req.setRequired(true);
        //fp.add(req);

        req = new SelectDatePicker(new Date(120, 9, 20));
        req.setRequired(true);
        //fp.add(req);

        //fp.add(new UserUuidLabel("12"));
        //fp.add(new UserUuidLabel("123"));


        ListPanel listPanel = new ListPanel(30, 10);
        listPanel.addStyleName(CssStyles.MARGIN_VER_10);

        ClientUserWithAffiliationThumb thumb = new ClientUserWithAffiliationThumb();
        thumb.setName("Gordon Freeman");
        thumb.setPosition("CIO of the World");

        for (int i = 0; i < 20; i++) {
            listPanel.addItem(new UserListItem(thumb));
        }


        fp.add(listPanel);
        helpPanel.setWidget(fp);

        Reverser reverser = new Reverser();
        reverser.add("first", new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
            }
        });
        reverser.add("second long", new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
            }
        });
        reverser.add("<div>Den 1</div><div>20.6.2011</div>", new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
            }
        }, 50, true);
        fp.add(reverser);

        int minutes = 1;
        fp.add(new Label(UIMessageUtils.m.const_minutesLower(minutes) + " ago"));
        minutes = 5;
        fp.add(new Label(UIMessageUtils.m.const_minutesLower(minutes) + " ago"));

        ConsysMessage fail = ConsysMessage.getFail();
        fail.hideClose(true);
        fail.addOrSetText("Text error aaaaaa a ajf a fasjdflk adlf lqdsz b azs ad  fzas eapf  apezf  azaepf");
        fp.add(fail);

        ActionLabel showUserCard = new ActionLabel("show user card");
        showUserCard.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserCard uc = new UserCard("123455");
                uc.setPopupPosition(50, 50);
                uc.show();
            }
        });
        fp.add(showUserCard);

        final ActionImage ai = ActionImage.getPlusButton("plus", true);
        ai.setStyleName(StyleUtils.CLEAR_BOTH);
        ai.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Window.alert("plus");
            }
        });
        ai.setEnabled(false);

        final ActionImageBig acc = ActionImageBig.acceptButton();
        acc.setStyleName(StyleUtils.FLOAT_LEFT);
        acc.addStyleName(StyleUtils.MARGIN_RIGHT_20);
        acc.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Window.alert("Accept");
            }
        });
        ActionImageBig dec = ActionImageBig.declineButton();
        dec.setStyleName(StyleUtils.FLOAT_LEFT);
        dec.addStyleName(StyleUtils.MARGIN_RIGHT_20);
        dec.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                acc.setEnabled(!acc.isEnabled());
                ai.setEnabled(!ai.isEnabled());
            }
        });
        ActionImageBig sav = ActionImageBig.saveButton();
        sav.setStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel buttPanel = new FlowPanel();
        buttPanel.add(acc);
        buttPanel.add(dec);
        buttPanel.add(sav);
        fp.add(buttPanel);

        fp.add(ai);

        fp.add(new LoadIndicator(73, 100));
        fp.add(new ConsysTextArea(73, 100));

        final ConsysPasswordBox passwordBox = new ConsysPasswordBox(250, true);
        fp.add(passwordBox);

        ConsysCheckBox checkBox = new ConsysCheckBox();
        checkBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                passwordBox.showPassword(event.getValue());
            }
        });
        fp.add(checkBox);
        fp.add(consys.common.gwt.client.ui.comp.action.ActionImage.delete("smaž to"));
        
        ActionLabel waiting = new ActionLabel("show waiting");
        waiting.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                helpPanel.showWaiting(true);
                helpPanel.showWaitingText("55 %");
            }
        });
        fp.add(waiting);

        ConsysSelectBox<String> selectbox1 = new ConsysSelectBox<String>(270);
        selectbox1.setItems(sitems);
        selectbox1.setSelectedItem(sitems.get(43));
        fp.add(selectbox1);

        // Create Dialog
        ActionLabel createDialogLabel = new ActionLabel("Create Dialog");
        createDialogLabel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TestCreateDialog createDialog = new TestCreateDialog();
                createDialog.showCentered();
            }
        });

        fp.add(createDialogLabel);


        formMap.put("Help", helpPanel);
        formMap.put("User Components", userComponents);
        formMap.put("DataList", list);
        formMap.put("New select box", selectBoxPanel);


        //// Navigacia
        PanelItemEvent pie = new PanelItemEvent(PanelItemAction.SHOW_ADDON);
        EventBus.get().fireEvent(pie);

        LayoutPanel.get().setAddonPanelTitle("Takeplace 2010");
        PanelAddonItem item = new PanelAddonItem("Registration");
        item.addSubItem("Polozka 1", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        item.addSubItem("Polozka 2", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        item.addSubItem("Polozka 3", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        item.addSubItem("Polozka 4", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        PanelItemEvent ap1 = new PanelItemEvent(item, PanelItemAction.ADD, PanelItemType.ADDON);
        EventBus.get().fireEvent(ap1);
        item = new PanelAddonItem("Submission", new ConsysAction() {

            @Override
            public void run() {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });
        item.addSubItem("Polozka 1", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        item.addSubItem("Polozka 2", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        item.addSubItem("Polozka 3", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        item.addSubItem("Polozka 4", "Polozka 1", new ConsysAction() {

            @Override
            public void run() {
            }
        });
        PanelItemEvent ap2 = new PanelItemEvent(item, PanelItemAction.ADD, PanelItemType.ADDON);
        EventBus.get().fireEvent(ap2);

        PanelItemEvent ap3 = new PanelItemEvent(new PanelItem(true) {

            @Override
            public String getName() {
                return "SYSTEM_NOTIFICATION";
            }

            @Override
            public Widget getTitle() {
                return null;
            }

            @Override
            public Widget getContent() {


                FlowPanel panel = new FlowPanel();
                Button button = new Button("ROOT");
                button.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        BreadcrumbContentEvent bce = new BreadcrumbContentEvent("Root Breadcrumb " + rootCount, BreadcrumbContentEvent.LEVEL.ROOT, new ChangeContentEvent(new UnknownTokenContent()));
                        EventBus.get().fireEvent(bce);
                    }
                });
                panel.add(new HTML("Pri <b>LEVEL.ROOT</b> sa nastavi koren nabigacie (identifikuje Aktualne vybrany event)"));
                panel.add(button);

                button = new Button("Next To Root");
                button.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        FormUtils.fireNextRootBreadcrumb("Next To Root", new UnknownTokenContent());
                    }
                });
                panel.add(new HTML("</br></br>Pri <b>LEVEL.NEXT_TO_ROOT</b> sa nastavi breadcrumb vedla korena(identifikuje korenovy formular)"));
                panel.add(button);

                button = new Button("Next Level");
                button.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        FormUtils.fireNextBreadcrumb("Next", new UnknownTokenContent());
                    }
                });
                panel.add(new HTML("</br></br>Pri <b>LEVEL.NEXT</b> sa breadcrumb prida jak dalsi prvek v ceste."));
                panel.add(button);

                button = new Button("Same Level");
                button.addClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        FormUtils.fireSameBreadcrumb("Same", new UnknownTokenContent());
                    }
                });
                panel.add(new HTML("</br></br>Pri <b>LEVEL.SAME</b> sa nahradi posledny breadcrumb prave tym ktery je posledny"));
                panel.add(button);

                //panel.add(new SelectBox());
                panel.add(ActionImage.getCheckButton("check"));

                return panel;
            }
        }, PanelItemAction.ADD, PanelItemType.SYSTEM);
        EventBus.get().fireEvent(ap3);

        MenuDispatcher.get().addMenuItem("První odkaz", "#prvni");
        MenuDispatcher.get().addMenuItem("Druhý odkaz", "#druhy");
    }

    public class TestListItem extends ListItem<String> {

        public TestListItem(String string) {
            super(string);
        }

        @Override
        protected void createCell(String item, FlowPanel panel) {
            panel.add(new Label(item));
        }
    }

    public class TestListDelegate implements ListDelegate<String, TestListItem> {

        @Override
        public TestListItem createCell(String item) {
            return new TestListItem(item);
        }
    }

    public class TestCreateDialog extends NewItemDialog<NewItemUI> {

        public TestCreateDialog() {
            super("Create Dialog", ConsysMessage.getSuccess());
        }

        @Override
        public NewItemUI newItem() {
            NewItemUI newItemUI = new NewItemUI();
            newItemUI.form().addOptional(0, "Text", new Label("Label"));
            return newItemUI;
        }

        @Override
        public void doCreateAction(ArrayList<NewItemUI> uis, ConsysMessage fail) {
        }
    }
}
