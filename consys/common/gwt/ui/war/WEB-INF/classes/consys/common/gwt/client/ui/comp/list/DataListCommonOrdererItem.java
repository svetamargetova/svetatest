package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class DataListCommonOrdererItem extends ListOrderer {

    // data
    private HeadPanel parent;
    private String text;

    public DataListCommonOrdererItem(int tag, String text, HeadPanel parent) {
        super(tag);
        this.text = text;
        this.parent = parent;
        setStyleName(StyleUtils.INLINE);
    }

    void addClickHandler(ClickHandler h) {
        addDomHandler(h, ClickEvent.getType());
    }

    @Override
    protected void onLoad() {
        Label l = new Label(text);
        setWidget(l);
    }

    @Override
    public void deselect() {
        removeStyleName(ResourceUtils.system().css().dataListCommonOrdererOrderSelected());
        addStyleName(ResourceUtils.system().css().dataListCommonOrdererOrder());
    }

    @Override
    public void select() {
        removeStyleName(ResourceUtils.system().css().dataListCommonOrdererOrder());
        addStyleName(ResourceUtils.system().css().dataListCommonOrdererOrderSelected());

        parent.clearOrderer();
        parent.registerOrderer(this);
    }

    public String getText() {
        return text;
    }
}
