package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.UpdateEventPropertyAction;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.BadInputException;
import java.util.ArrayList;

/**
 * Nastaveni property
 * @author pepa
 */
public class PropertyPanelItem extends FlowPanel {

    // komponenty
    private ActionLabel update;
    private SelectBox<String> select;
    private TextBox text;
    private Widget widget;
    private FlowPanel compPanel;
    // data
    private PropertyPanel parent;
    private String key;
    private String keyName;
    private String originalValue;
    private PropertyPanelItemValidator validator;

    public PropertyPanelItem(PropertyPanel parent, String key, String keyName, ArrayList<SelectBoxItem<String>> items,
            String actualValue, String helpText, String labelWidth) {
        super();
        this.parent = parent;
        this.key = key;
        originalValue = actualValue;
        this.keyName = keyName == null ? key : keyName;

        if (items != null && items.size() > 1) {
            // vyberova komponenta
            select = new SelectBox<String>();
            select.setWidth(200);
            select.addStyleName(StyleUtils.FLOAT_LEFT);
            select.addStyleName(StyleUtils.MARGIN_RIGHT_5);
            select.setItems(items);
            select.selectFirst(true);
            select.selectItem(originalValue);
            select.addChangeValueHandler(selectHandler());
            widget = select;
        } else {
            // textova komponenta
            text = StyleUtils.getFormInput("200px");
            text.addStyleName(StyleUtils.FLOAT_LEFT);
            text.addStyleName(StyleUtils.MARGIN_RIGHT_5);
            text.setValue(originalValue);
            text.addKeyUpHandler(textHandler());
            widget = text;
        }

        initUI(helpText, labelWidth == null ? "180px" : labelWidth);
    }

    public PropertyPanelItem(PropertyPanel parent, String key, String keyName, TextBox text,
            String actualValue, String helpText, String labelWidth, PropertyPanelItemValidator validator) {
        super();
        this.parent = parent;
        this.key = key;
        originalValue = actualValue;
        this.keyName = keyName == null ? key : keyName;
        this.validator = validator;

        this.text = text;
        text.setWidth("200px");
        text.addStyleName(StyleUtils.FLOAT_LEFT);
        text.addStyleName(StyleUtils.MARGIN_RIGHT_5);
        text.setValue(originalValue);
        text.addKeyUpHandler(textHandler());
        widget = text;

        initUI(helpText, labelWidth == null ? "180px" : labelWidth);
    }

    private void initUI(String helpText, String labelWidth) {
        SimplePanel labelPanel = new SimplePanel();
        labelPanel.setWidth(labelWidth);
        labelPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        labelPanel.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        labelPanel.setWidget(StyleUtils.getStyledLabel(keyName + ":", StyleUtils.FLOAT_RIGHT, StyleUtils.MARGIN_RIGHT_10));
        add(labelPanel);

        update = new ActionLabel(UIMessageUtils.c.const_update(), StyleUtils.FLOAT_LEFT + " " + StyleUtils.MARGIN_TOP_3);
        update.setVisible(false);
        update.setClickHandler(updateHandler());

        compPanel = new FlowPanel();
        compPanel.setWidth("300px");
        compPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        compPanel.add(widget);
        compPanel.add(update);
        add(compPanel);

        Label helpLabel = StyleUtils.getStyledLabel(helpText, StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_LEFT_20);
        helpLabel.setWidth("200px");
        add(helpLabel);

        add(StyleUtils.clearDiv());
    }

    /** handler pro sledovani zmen v selectu */
    private ChangeValueEvent.Handler<SelectBoxItem<String>> selectHandler() {
        return new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                update.setVisible(!event.getValue().getItem().equals(originalValue));
            }
        };
    }

    /** handler pro sledovani zmen v textu */
    private KeyUpHandler textHandler() {
        return new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                update.setVisible(!text.getText().trim().equals(originalValue));
            }
        };
    }

    /** handler pro odeslani aktualizace hodnoty */
    private ClickHandler updateHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final String value;
                if (text == null) {
                    // select box
                    value = select.getSelectedItem().getItem();
                } else {
                    // text box
                    if (validator != null) {
                        try {
                            validator.doValidate(text);
                        } catch (BadInputException ex) {
                            parent.getFailMessage().setText(ex.getMessage());
                            return;
                        }
                    }
                    value = text.getText().trim();
                }

                EventDispatchEvent updateEvent = new EventDispatchEvent(
                        new UpdateEventPropertyAction(key, value),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                originalValue = value;
                                update.setVisible(false);
                                // aktualizace hodnoty v cache
                                Cache.get().register(key, value);
                            }
                        }, parent);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }

    /** validator textboxu property */
    public interface PropertyPanelItemValidator {

        /** provede validaci polozky, pokud je vse v poradku nic nevyhodi */
        public void doValidate(TextBox box) throws BadInputException;
    }
}
