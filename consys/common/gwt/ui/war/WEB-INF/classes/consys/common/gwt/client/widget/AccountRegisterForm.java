package consys.common.gwt.client.widget;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysPasswordTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.gwt.client.widget.utils.CWMessageUtils;
import consys.common.gwt.client.widget.validator.PasswordValidator;
import consys.common.gwt.client.widget.validator.PasswordValidator.PasswordStrengthEnum;

/**
 * Komponenta pro registraci noveho uzivatele
 * @author pepa
 */
public class AccountRegisterForm extends BaseForm {

    // konstanty
    public static final int ACTION_MEMBER_ROW = 7;
    // komponenty
    private ConsysStringTextBox firstNameBox;
    private ConsysStringTextBox lastNameBox;
    private ConsysStringTextBox affiliationBox;
    private ConsysStringTextBox contactEmailBox;
    private ConsysStringTextBox fakePasswordBox;
    private ConsysPasswordTextBox passwordBox;
    private ConsysPasswordTextBox passwordAgainBox;
    private ConsysCheckBox showPassword;
    // data
    private ClientRegistration reg;

    public AccountRegisterForm(String width, boolean showHtmlText) {
        this(width, showHtmlText, false);
    }

    public AccountRegisterForm(String width, boolean showHtmlText, boolean showAffiliation) {
        super(width);
        firstNameBox = new ConsysStringTextBox(1, 255, CWMessageUtils.c.const_firstName());
        lastNameBox = new ConsysStringTextBox(1, 255, CWMessageUtils.c.const_lastName());
        affiliationBox = new ConsysStringTextBox(0, 255, CWMessageUtils.c.const_affiliation());
        contactEmailBox = new ConsysStringTextBox(1, 255, UIMessageUtils.c.const_email()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                boolean result = super.doValidate(fail);
                if (result) {
                    result = ValidatorUtils.isValidEmailAddress(getText().trim());
                    if (!result) {
                        fail.addOrSetText(UIMessageUtils.c.const_error_invalidEmailAddress());
                    }
                }
                return result;
            }
        };
        fakePasswordBox = new ConsysStringTextBox("100px", 6, 255, CWMessageUtils.c.const_password());
        fakePasswordBox.setVisible(false);
        passwordBox = new ConsysPasswordTextBox("100px", 6, 255, CWMessageUtils.c.const_password());
        passwordAgainBox = new ConsysPasswordTextBox("100px", 6, 255, CWMessageUtils.c.const_confirmPassword()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                // validuje se pouze vzhledem ke shode passwordBox
                boolean result = true;
                if (!PasswordValidator.changePassword(reg.getPassword(), reg.getConfirmPassword())) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordsNotMatch());
                    result = false;
                }
                return result;
            }
        };

        showPassword = new ConsysCheckBox();
        showPassword.addStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel checkPanel = new FlowPanel();
        checkPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
        checkPanel.addStyleName(StyleUtils.MARGIN_TOP_3);
        checkPanel.add(showPassword);
        checkPanel.add(StyleUtils.getStyledLabel(CWMessageUtils.c.const_showPassword(), StyleUtils.MARGIN_LEFT_5, StyleUtils.FLOAT_LEFT));
        checkPanel.add(StyleUtils.clearDiv());

        ConsysFlowPanel passwordPanel = new ConsysFlowPanel() {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                if (!PasswordValidator.passwordStrength(reg.getPassword()).equals(PasswordStrengthEnum.STRONG)) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordIsWeak());
                    return false;
                }
                if (reg.getPassword().contains(" ")) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordContainsSpace());
                    return false;
                }
                return true;
            }

            @Override
            public void onSuccess() {
                passwordBox.onSuccess();
                fakePasswordBox.onSuccess();
            }

            @Override
            public void onFail() {
                passwordBox.onFail();
                fakePasswordBox.onFail();
            }
        };
        passwordPanel.add(checkPanel);
        passwordPanel.add(passwordBox);
        passwordPanel.add(fakePasswordBox);
        passwordPanel.add(StyleUtils.clearDiv());

        Label passwordStrengthLabel = new Label();
        passwordStrengthLabel.setStyleName(StyleUtils.FONT_BOLD);
        passwordStrengthLabel.addStyleName(StyleUtils.FLOAT_RIGHT);

        ConsysFlowPanel passwordAgainPanel = new ConsysFlowPanel();
        passwordAgainPanel.add(passwordStrengthLabel);
        passwordAgainPanel.add(passwordAgainBox);
        passwordAgainPanel.add(StyleUtils.clearDiv());

        addRequired(0, CWMessageUtils.c.const_firstName(), firstNameBox);
        addRequired(1, CWMessageUtils.c.const_lastName(), lastNameBox);
        if (showAffiliation) {
            addOptional(2, CWMessageUtils.c.const_affiliation(), affiliationBox);
        }
        addRequired(3, UIMessageUtils.c.const_email(), contactEmailBox);
        addRequired(4, CWMessageUtils.c.const_password(), passwordPanel);
        addRequired(5, CWMessageUtils.c.const_confirmPassword(), passwordAgainPanel);
        if (showHtmlText) {
            addContent(6, termsOfServiceHTML());
        }

        initHandlers(passwordStrengthLabel);
    }

    /** inicializuje handlery */
    private void initHandlers(final Label passwordStrengthLabel) {
        passwordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(passwordBox, passwordStrengthLabel));
        fakePasswordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(fakePasswordBox, passwordStrengthLabel));
        fakePasswordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                passwordBox.setText(fakePasswordBox.getText());
            }
        });
        passwordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                fakePasswordBox.setText(passwordBox.getText());
            }
        });
        showPassword.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                if (event.getValue()) {
                    passwordAgainBox.onSuccess();
                }
                fakePasswordBox.setVisible(event.getValue());
                passwordBox.setVisible(!event.getValue());
                passwordAgainBox.setEnabled(!event.getValue());
                passwordAgainBox.setText("");
            }
        });
    }

    /** vygeneruje objekt ClientRegistration z dat ve formulari */
    public ClientRegistration generateRegistration() {
        ClientRegistration registration = new ClientRegistration();
        registration.setFirstName(getFirstNameBox().getText());
        registration.setLastName(getLastNameBox().getText());
        registration.setAffiliation(affiliationBox.getText());
        registration.setContactEmail(contactEmailBox.getText());
        registration.setPassword(passwordBox.getText().trim());

        if (passwordAgainBox.isEnabled()) {
            registration.setConfirmPassword(passwordAgainBox.getText().trim());
        } else {
            registration.setConfirmPassword(fakePasswordBox.getText().trim());
        }
        this.reg = registration;
        return registration;
    }

    /** vynuluje data zadana do formu */
    public void clearData() {
        getFirstNameBox().setText("");
        getLastNameBox().setText("");
        contactEmailBox.setText("");
        fakePasswordBox.setText("");
        passwordBox.setText("");
        passwordAgainBox.setText("");
        showPassword.setValue(Boolean.FALSE);
    }

    /** nastavi registracni udaje z pozvanky, pokud neni nic vyplneno, pole zustavaji prazdna */
    public void setupForm(String firstName, String lastName, String affiliation, String email) {
        firstNameBox.setText(StringUtils.isEmpty(firstName) ? "" : firstName);
        lastNameBox.setText(StringUtils.isEmpty(lastName) ? "" : lastName);
        affiliationBox.setText(StringUtils.isEmpty(affiliation) ? "" : affiliation);
        contactEmailBox.setText(StringUtils.isEmpty(email) ? "" : email);
    }

    /** vraci html objekt textem a odkazem na podminky */
    public HTML termsOfServiceHTML() {
        HTML html = new HTML(CWMessageUtils.c.registerUser_text_agreeing());
        html.addStyleName(ResourceUtils.system().css().htmlAgreeing());
        html.addStyleName(StyleUtils.MARGIN_TOP_10);
        html.setWidth("270px");
        return html;
    }

    /**
     * @return the firstNameBox
     */
    public ConsysStringTextBox getFirstNameBox() {
        return firstNameBox;
    }

    /**
     * @return the lastNameBox
     */
    public ConsysStringTextBox getLastNameBox() {
        return lastNameBox;
    }
}
