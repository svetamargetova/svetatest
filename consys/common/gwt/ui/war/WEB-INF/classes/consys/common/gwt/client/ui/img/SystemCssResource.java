package consys.common.gwt.client.ui.img;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 *
 * @author pepa
 */
@ImportedWithPrefix("ui")
public interface SystemCssResource extends CssResource {

    String actionLabel();

    String actionLabelEnabled();

    String actionLabelDisabled();

    // zakladni styly
    String marginLeft8();

    String marginRight8();

    String marginTop8();

    String marginBot8();

    String marginVer8();

    String marginHor8();

    // ostatni
    String contentCol();

    String panelCol();

    String footer();

    String clearDiv();

    String clearLeft();

    String clearRight();

    String clearBoth();

    String breadcrumbSep();

    String breadcrumbSepIn();

    String msgCloseOver();

    String msgCloseOut();

    String helpPanel();

    String helpItem();

    String helpTitle();

    String admin();

    /** position:static */
    String positionStatic();

    /** stred horniho menu */
    String topMenuCenter();

    /** stred horniho vybraneho menu */
    String topMenuSelectedCenter();

    /** stred horniho nevybraneho menu */
    String topMenuUnselectedCenter();

    /** mezera od horni casti topMenu po panel s polozkami menu */
    String panelMenuTopMargin();

    /** dolni cast separatoru nad spodnim menu */
    String bottomMenuSeparator2();

    /** styl separatoru mezi odkazy v paticce */
    String bottomLinkSeparator();

    /** odkaz na acemcee z paticky */
    String bottomAcemceeLink();

    /** pro padding 3px od vrchu a zleva v inputech */
    String inputPadd();

    /** pro consys text area */
    String inputAreaPadd();

    /** minimalni velikost contentu */
    String minHeight();

    /** stredni cast ActionImage */
    String actionImage();

    /** stredni cast ActionImageBig */
    String actionImageBig();

    /** stredni cast ActionImage - disablovany */
    String actionImageDisabled();

    /** stredni cast ActionImageBig - disablovany */
    String actionImageBigDisabled();

    /** obecny separator (tecky na bilem podkladu), opakuje se horizontalne */
    String separator();

    /** margin od horniho okraje formularoveho titulku */
    String formTitleTop();

    /** styl spodni hrany base wrapperu */
    String corGrayBottom();

    /** styl aktivni polozky progressu */
    String progressActive();

    /** styl neaktivni polozky progressu */
    String progressInactive();

    /** styl aktivni polozky v MenuFormPanelu */
    String mfpActive();

    /** styl neaktivni polozky v MenuFormPanelu */
    String mfpInactive();

    /** styl pro separator mezi posuvnymi sipkami ve wrapperu */
    String wrapperArrowSeparator();

    /** kontextova ikona person over */
    String contextPersonOver();

    /** kontextova ikona person out */
    String contextPersonOut();

    /** kontextova ikona gear over */
    String contextGearOver();

    /** kontextova ikona gear out */
    String contextGearOut();

    /** kontextova ikona cross over */
    String contextCrossOver();

    /** kontextova ikona cross out */
    String contextCrossOut();

    /** breadcrumb pozadi stredni casti */
    String breadcrumbCenter();

    /** ikona plus pro RollUpPanel */
    String rollPlus();

    /** ikona minus pro RollUpPanel */
    String rollMinus();

    /** zalozkovy panel pozadi vybrane zalozky */
    String tabSelectedCenter();

    /** zalozkovy panel pozadi nevybrane zalozky */
    String tabUnselectedCenter();

    /** standardni vyska radku ve formulari (pro IE 21px pro FF a spol 19px) */
    String rowHeight();

    /** vyhledavaci vstup v top menu */
    String menuSearcherInput();

    /** tlacitko vyhledavaciho vstupu v top menu */
    String menuSearcherSubmit();

    /** vzdalenost vyhledavain v top menu od horniho okraje */
    String menuSearcherTop();

    /** novy panel */
    String panel();

    /** novy panel - zahlavi */
    String top();

    /** novy panel - zahlavi pridavny panel */
    String addTop();

    /** novy panel - zapati */
    String bottom();

    /** novy panel - separator */
    String sep();

    /** novy panel - titulek pridavneho panelu */
    String addonTitle();

    /** novy panel - separator pod hlavicku pridavneho panelu */
    String addSep();

    /** novy panel - left */
    String left();

    /** novy panel - center */
    String center();

    /** novy panel - right */
    String right();

    /** novy panel - left pridavny panel */
    String addLeft();

    /** novy panel - right pridavny panel */
    String addRight();

    /** addon cas panelu */
    String addon();

    /** novy panel - prvni polozka */
    String fst();

    /** novy panel - dalsi polozky */
    String nxt();

    /** hlavicka aplikace */
    String head();

    /** hlavicka aplikace - radek s komponentama */
    String row();

    String rowPositioner();

    /** hlavicka aplikace - radek s profilem */
    String headProfile();

    /** obecne titulek */
    String title();

    /** obecne text */
    String text();

    /** obalovy objekt titulku */
    String titleWrapper();

    /** podcast pridavneho panelu (po rozkliknuti) */
    String subAddon();

    /** polozka */
    String subAddonItem();

    /** vybrana polozka */
    String subAddonItemSelected();

    /** podcast pridavneho panelu - horni oddelovac */
    String subAddonTopSep();

    /** podcast pridavneho panelu - ostatni oddelovace */
    String subAddonSep();

    String downMenu();

    String downMenuItem();

    String downMenuContainer();

    /** DATA LIST - obalovy objekt pre cely datalist */
    String dataList();

    /** DATA LIST - obalovy objekt pre cely datalist */
    String dataListHeader();

    /** DATA LIST - obalovy objekt pre cely datalist */
    String dataListFooter();

    /** DATA LIST - obalovy objekt pre cely datalist */
    String dataListBody();

    String dataListCommonOrdererTitle();

    String dataListCommonOrdererBreak();

    String dataListCommonOrdererOrder();

    String dataListCommonOrdererOrderSelected();

    /** DATA LIST - zadne zaznamy sa nenacitaly */
    String emptyList();

    /** DATA LIST - zadne zaznamy sa nenacitaly titulka*/
    String emptyListText();

    /** DATA LIST ITEM - obalujuci objekt pro jeden zaznam */
    String dataListItem();

    String dataListItemNoBorder();

    /** DATA LIST ITEM - body zaznamu */
    String dataListItemBody();

    /** DATA LIST ITEM - lichy prvek*/
    String dataListItemOdd();

    /** DATA LIST ITEM - sudy prvek*/
    String dataListItemEven();

    /** DATA LIST ITEM - style ked je zaznam oznaceny */
    String dataListItemSelected();

    /** DATA LIST EXPORT - button */
    String dataListExportButton();

    /** DATA LIST EXPORT - dialog panel */
    String dataListExportDialog();

    /** DATA LIST EXPORT - export option */
    String exportOption();

    /** Styl pageru */
    String pager();

    /** Textovy popisek */
    String pagerInfoLabel();

    /** Sipecka prava-lava */
    String pagerArrowRight();

    String pagerArrowLeft();

    /** SelectBox style */
    String perPageSelect();

    /** Dialog - Body */
    String dialogBodyPanel();

    String dialogInnerPanel();

    String dialogInnerPanelWrapper();

    /** Create Dialog - Header Wrapper*/
    String createDialogHeaderWrapper();

    /** Create Dialog - Header Wrapper Titulky */
    String createDialogHeaderTitleWrapper();

    /** Create Dialog - Header Titulka */
    String createDialogHeaderTitle();

    /** Create Dialog - Header Controll Panel */
    String createDialogHeaderControllPanel();

    /** Create Dialog - Header Controll Panel Item */
    String createDialogHeaderControllPanelItem();

    /** Create Dialog - Controll Panel */
    String createDialogControllPanel();

    /** Reverser */
    String reverserOffLeft();

    /** List Panel - Skorolovacia lista  */
    String listPanel();

    String listPanelItem();

    String listPanelItemSelected();

    String listPanelItemOdd();

    String listPanelItemEven();

    String listPanelEditAction();

    String listPanelEditItemContent();

    String listPanelEditItemAction();

    /** Multi button - Reverser(Switch) */
    String reverserOffRight();

    String reverserOffCenter();

    String reverserOnLeft();

    String reverserOnRight();

    String reverserOnCenter();

    String reverserOffRep();

    String reverserOnRep();

    String reverserOnLabel();

    String reverserOffLabel();

    String reverserOnHTML();

    String reverserOffHTML();

    /* Velke buttony ZELENY A CERVENY */
    String bigBlueB();

    String bigGreenB();

    String bigRedB();

    String bigDisabledB();

    /** panel s teckamy v ShowLoad */
    String loadDots();

    /** tlacitko pro rozbaleni/zabaleni casti ve strance */
    String unpackingBig();

    String unpackingBigIconDown();

    String unpackingBigIconUp();

    String borderGray1();

    /** souhlas s licenci */
    String htmlAgreeing();

    /** komponenta zmeny lokalizace */
    String changeLocale();

    /** seznam produktu pod platebnim profilu */
    String paymentPriceListHeadTitle();

    String paymentPriceListHeadTitleNext();

    /** barva textu */
    String colorOrange();

    /** barva pozadi info ConsysMessage */
    String infoBg();

    /** styl komponenty EditWithRemover zarovnane doprava a odsazene zhora */
    String editWithRemoverAtRight();

    /** styl jedne polozky komponenty EditWithRemover */
    String editWithRemoverItem();

    String helpPopupWrapper();

    String helpPopup();

    String helpContent();

    String helpBottom();

    String helpTriangleLeft();

    String helpTriangleRight();

    String helpTriangleBottom();

    String more();

    String smartDialogClose();

    String smartDialogCloseImage();

    String loginForm();

    String rememberMe();

    String selectBox();

    String bottomMenu();

    String waitingWrapper();

    String waitingText();
    
    String uploadWrapper();
}
