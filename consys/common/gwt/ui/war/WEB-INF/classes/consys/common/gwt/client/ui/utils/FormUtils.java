package consys.common.gwt.client.ui.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ServletConstants;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysFileUpload;
import consys.common.gwt.client.ui.event.BreadcrumBackEvent;
import consys.common.gwt.client.ui.event.BreadcrumbContentEvent;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.constants.img.EventLogoImageEnum;
import consys.common.constants.img.EventProfileImageEnum;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import java.util.Date;

/**
 * Pomocne metody pro praci s formulari
 * @author pepa
 */
public class FormUtils {

    /** formater double pro dve desetinna mista */
    public final static NumberFormat doubleFormat = NumberFormat.getFormat("###,###,##0.00");
    /** pomlcka (mdash) */
    public static final String MDASH = "—";
    /** pomlcka (ndash) */
    public static final String NDASH = "\u2013";
    /** nezlomitelna mezera */
    public static final String UNB_SPACE = "\u00a0";
    /** [nezlomitelna mezera, pomlcka, nezlomitelna mezera]   */
    public static final String UNB_NDASH_UNB = FormUtils.UNB_SPACE + FormUtils.NDASH + FormUtils.UNB_SPACE;
    /***/
    public static final double D_MAX = 1.7976931348623157e+308;
    public static final double D_MIN = -100000;
    /** rozmer velkeho profiloveho obrazku uzivatele */
    public static final String LIST_PORTRAIT_SIZE = "55px";

    /** pokud je hodnota prazdna vraci se pomlcka, pokud ne, vraci se hodnota */
    public static String valueOrDash(String text) {
        return (text == null || text.isEmpty() ? MDASH : text);
    }

    /** pokud je hodnota prazdna vraci se pomlcka, pokud ne, vraci se toString() */
    public static String valueOrDash(Object o) {
        return (o == null ? MDASH : o.toString());
    }

    /** pokud je hodnota prazdna vraci se pomlcka, pokud ne, vraci se hodnota */
    public static String valueOrDash(int value) {
        return (value == -1 ? MDASH : String.valueOf(value));
    }

    /**
     * vraci text zkraceny podle zadane delky na cele slovo,
     * pokud je text delsi nez pozadovany, na konci jsou mezera a tri tecky
     */
    public static String shortTextOnWord(int maxLength, String original) {
        if (original == null || original.isEmpty()) {
            return original;
        }
        if (original.length() < maxLength) {
            return original;
        }
        String shrt = original.substring(0, maxLength - 4);
        int lastSpaceIndex = shrt.lastIndexOf(" ");
        return shrt.substring(0, lastSpaceIndex) + " ...";
    }

    /** vraci pred jakou dobou se vyskytovalo zadane casove razitko */
    public static String ago(long timestamp) {
        // TODO: prepocet podle casoveho pasma
        long now = System.currentTimeMillis();
        long diff = (now - timestamp) / 1000; // rozdil v sekundach
        if (diff < 3600) {
            return UIMessageUtils.m.const_minutesLower((int) (diff / 60)) + " " + UIMessageUtils.c.const_date_agoLower();
        } else if (diff < 86400) {
            return UIMessageUtils.m.const_hoursLower((int) (diff / 3600)) + " " + UIMessageUtils.c.const_date_agoLower();
        } else if (diff < 172800) {
            return UIMessageUtils.c.const_date_yesterday();
        } else if (diff < 604800) {
            return DateTimeUtils.getWeekDay(new Date(timestamp));
        } else {
            return DateTimeUtils.getMonthDayYear(new Date(timestamp));
        }
    }

    /** vraci pozadovany obrazek uzivatele, v pridade, ze je imageUuid null, vraci vychozi obrazek */
    public static Image userPortrait(String imgUuid, UserProfileImageEnum imageEnum) {
        Image portrait = null;
        if (imgUuid == null) {
            switch (imageEnum) {
                case LIST:
                    portrait = new Image(ResourceUtils.system().portraitBig());
                    break;
                case PROFILE:
                    portrait = new Image(ResourceUtils.system().portraitBig());
                    break;
            }
        } else {
            String url = GWT.getModuleBaseURL() + ServletConstants.USER_PROFILE_IMG_LOAD + imageEnum.getFullUuid(imgUuid);
            portrait = new Image(url);
        }
        return portrait;
    }

    public static Image eventLogo(String imgUuid, EventLogoImageEnum imageEnum) {
        Image logo = null;
        if (imgUuid == null) {
            switch (imageEnum) {
                case WALL:
                    logo = new Image(ResourceUtils.system().confLogoBig());
                    break;
                case LIST:
                    logo = new Image(ResourceUtils.system().confLogoSmall());
                    break;
            }
        } else {
            String url = GWT.getModuleBaseURL() + ServletConstants.EVENT_LOGO_IMG_LOAD + imageEnum.getFullUuid(imgUuid);
            logo = new Image(url);
        }
        return logo;
    }

    public static Image eventProfileLogo(String imgUuid, EventProfileImageEnum imageEnum) {
        Image logo = null;
        if (imgUuid == null) {
            switch (imageEnum) {
                case LIST:
                    logo = new Image(ResourceUtils.system().confProfileLogoSmall());
                    break;
                case WEB:
                    logo = new Image(ResourceUtils.system().confProfileLogoBig());
                    break;
            }
        } else {
            String url = URLFactory.defaultModuleUrl() + "/" + ServletConstants.EVENT_PROFILE_IMG_LOAD + imageEnum.getFullUuid(imgUuid);
            logo = new Image(url);
        }
        return logo;
    }

    /** vytvori wrapper pro portret uzivatele */
    public static FlowPanel portraitWrapper(UserProfileImageEnum imageEnum) {
        FlowPanel portraitWrapper = new FlowPanel();
        switch (imageEnum) {
            case LIST:
                portraitWrapper.setSize(LIST_PORTRAIT_SIZE, LIST_PORTRAIT_SIZE);
                portraitWrapper.addStyleName(StyleUtils.FLOAT_LEFT);
                portraitWrapper.addStyleName(StyleUtils.MARGIN_LEFT_10);
                portraitWrapper.addStyleName(StyleUtils.MARGIN_RIGHT_20);
                portraitWrapper.addStyleName(StyleUtils.MARGIN_BOT_5);
                break;
        }
        return portraitWrapper;
    }

    /** vytvori panel se vstupem pro odeslani souboru a ovladacimi tlacitky (odeslat, zrusit) */
    public static FlowPanel sendFileWrapper(String name, int width, String note, ClickHandler sendHandler, ClickHandler cancelHandler) {
        ConsysFileUpload imageUpload = new ConsysFileUpload(width);
        imageUpload.setName(name);

        final ActionImage submit = ActionImage.getConfirmButton(UIMessageUtils.c.const_sendUpper());
        submit.addClickHandler(sendHandler);
        submit.setEnabled(false);

        imageUpload.setEmptyHandler(new ConsysActionWithValue<Boolean>() {

            @Override
            public void run(Boolean data) {
                submit.setEnabled(!data);
            }
        });

        SimplePanel submitWrapper = new SimplePanel(submit);
        submitWrapper.addStyleName(StyleUtils.FLOAT_LEFT);
        submitWrapper.setWidth((imageUpload.getInputWidth() + 10) + "px");

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), StyleUtils.FLOAT_RIGHT + " " + StyleUtils.MARGIN_TOP_3);
        cancel.setClickHandler(cancelHandler);

        FlowPanel fp = new FlowPanel();
        fp.setWidth(width + "px");
        fp.setStyleName(StyleUtils.MARGIN_TOP_10);
        fp.add(submitWrapper);
        fp.add(cancel);
        fp.add(StyleUtils.clearDiv());

        FlowPanel panel = new FlowPanel();
        panel.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        panel.add(imageUpload);
        if (note != null) {
            panel.add(StyleUtils.getStyledLabel(note, StyleUtils.FONT_11PX, StyleUtils.TEXT_GRAY));
        }
        panel.add(fp);

        return panel;
    }

    /** vraci Flowpanel ktary obsahuje title: value, title tucne */
    public static FlowPanel titleValue(String title, String value) {
        InlineLabel titleLabel = new InlineLabel(title + ": ");
        titleLabel.setStyleName(StyleUtils.FONT_BOLD);

        InlineLabel valueLabel = new InlineLabel(value);

        FlowPanel panel = new FlowPanel();
        panel.add(titleLabel);
        panel.add(valueLabel);
        return panel;
    }

    public static void fireBreadcrumbBack() {
        EventBus.get().fireEvent(new BreadcrumBackEvent());
    }

    public static void fireNextBreadcrumb(String title, Widget widget) {
        fireBreadcrumb(title, widget, BreadcrumbContentEvent.LEVEL.NEXT);
    }

    public static void fireNextRootBreadcrumb(String title, Widget widget) {
        fireBreadcrumb(title, widget, BreadcrumbContentEvent.LEVEL.NEXT_ROOT);
    }

    public static void fireSameBreadcrumb(String title, Widget widget) {
        fireBreadcrumb(title, widget, BreadcrumbContentEvent.LEVEL.SAME);
    }

    private static void fireBreadcrumb(String title, Widget widget, BreadcrumbContentEvent.LEVEL level) {
        BreadcrumbContentEvent changeEvent = new BreadcrumbContentEvent(title, level, new ChangeContentEvent(widget));
        EventBus.get().fireEvent(changeEvent);
    }

    public static ClickHandler breadcrumbBackClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                FormUtils.fireBreadcrumbBack();
            }
        };
    }

    /** vytvori ChangeContentEvent se zadanym widgetem a necha ho vystrelit pres EventBus */
    public static void fireChangeContent(Widget w) {
        fireChangeContent(w, null);
    }

    /** vytvori ChangeContentEvent se zadanym widgetem a necha ho vystrelit pres EventBus */
    public static void fireChangeContent(Widget w, String title) {
        EventBus.get().fireEvent(new ChangeContentEvent(w, title));
    }

    /** vytvori simple panel se zadanym stylem */
    public static SimplePanel createPanel(String styleName) {
        SimplePanel panel = new SimplePanel();
        panel.setStyleName(styleName);
        return panel;
    }

    /** vygeneruje skrytou chybovou zpravu do zadaneho panelu (podporovane instance: FlowPanel, SimplePanel) */
    public static ConsysMessage failMessage(Widget toPanel, String... styles) {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setVisible(false);

        for (String style : styles) {
            failMessage.addStyleName(style);
        }

        if (toPanel instanceof FlowPanel) {
            ((FlowPanel) toPanel).add(failMessage);
        } else if (toPanel instanceof SimplePanel) {
            ((SimplePanel) toPanel).setWidget(failMessage);
        }

        return failMessage;
    }

    /** otevre popup okno o zadanych rozmerech a se zadanou adresou */
    public static native void popupWindow(String url, String windowName, int width, int height)/*-{
     var left = (screen.width/2)-(width/2);
     var top = (screen.height/2)-(height/2);
     var targetWin = $wnd.open(url, windowName, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+width+', height='+height+', top='+top+', left='+left);
     }-*/;
}
