package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Zeleny wrapper
 * @author pepa
 */
public class ConsysGreenWrapper extends ConsysWrapper {

    public ConsysGreenWrapper() {
        super();
    }

    public ConsysGreenWrapper(Widget widget) {
        super(widget);
    }

    public ConsysGreenWrapper(Widget widget, int settings) {
        super(widget, settings);
    }

    @Override
    public WrapperFace getWrappertFace() {
        Image bottomLeft = new Image(ResourceUtils.system().corGreenBl());
        Image bottomRight = new Image(ResourceUtils.system().corGreenBr());
        Image topRight = new Image(ResourceUtils.system().corGreenTr());
        Image topLeft = new Image(ResourceUtils.system().corGreenTl());
        WrapperFace face = new WrapperFace(new Image[]{bottomLeft, bottomRight, topRight, topLeft}, 2, StyleUtils.BACKGROUND_GREEN);
        return face;
    }
}
