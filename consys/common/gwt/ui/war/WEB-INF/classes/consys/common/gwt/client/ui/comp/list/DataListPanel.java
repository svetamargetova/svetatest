package consys.common.gwt.client.ui.comp.list;

import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Zoznam prvkov. Zoznam moze obshaovat niekolko HeadPanel-ov. Jeden takyto panel
 * moze obshaovat lubovolny pocet<ul>
 * <li>ListOrderer-ov - zoradenie zoznamu</li>
 * <li>ListFilterov-ov - filter zoznamu</li>
 * </ul>
 *
 * <h2>Itemizer</h2>
 * Itemizer je abstraktny predok predstavujuci objekt ktory ovplyvnuje obsah
 * zoznamu. Kazdty itemizer by mal obsahovat unikatny <i>tag</i> ktory jasne
 * identifikuje dany itemizer.
 *
 * <h2>ListOrderer</h2>
 * V zozname su aktivne vzdy vsetky orderovace takze vsetky naraz ovlyvnuju vys-
 * ledny zoznam. Na pociatku je vzdy kazdy orderer nastavny na defaultnu hodnotu.
 * Zoradovace su vo vyslednom dotaze ulozene tak ako boli vlozene do head pane-
 * lov.
 *
 * <h2>ListFilter</h2>
 * Zoznam moze obsahovat lubovolne mnozstvo filtrov ale v case je aktivny prave
 * jeden. 
 *
 *
 * <h2>Action</h2>
 * Existuje genericka ListAction ktora obsahuje:
 * <ul>
 *  <li>Tagy vsetkych ListOrderovacov</li>
 *  <li>Jeden tag prave aktivneho listFiltera</li>
 *  <li>Stranku ktora je pozadovana</li>
 *  <li>Pocet zaznamov na stranku</li>
 * </ul> vsetky ListOrderovace a prave
 *
 * @author Palo
 */
public class DataListPanel<ItemType, ItemCell extends ListItem<ItemType>>     extends RootPanel
        implements ListPagerWidget.ListPagerDelegate, ActionExecutionDelegate {

    private static final Logger logger = LoggerFactory.getLogger(DataListPanel.class);
    // Konstanty    
    public static final int PAGE25 = 25;
    public static final int PAGE50 = 50;
    public static final int PAGE100 = 100;
    // Komponenty
    private FlowPanel header;
    private SimplePanel body;
    private FlowPanel footer;
    private ScrollPanel itemsScrollPanel;
    private FlowPanel itemsPanel;
    private SelectBox<Integer> perPageSelectBox;
    // waiting
    private Waiting waiting;
    // Data
    private List<HeadPanel> headPanels;
    private ListFilter activeFilter;
    private String dataListTag;
    private ListDelegate<ItemType, ItemCell> delegate;
    private List<ListPagerWidget> pagers;
    private boolean useEvenOddRowsStyle;
    /** Datasource pro list. */
    private ListDataSource dataSource;
    // Strankovani
    private int actualPage = 1;
    private int perPage;

    public DataListPanel(String title, String dataListTag) {
        this(title, dataListTag, null, true);
    }

    public DataListPanel(String title, String dataListTag, boolean useEvenOddRowsStyle) {
        this(title, dataListTag, null, useEvenOddRowsStyle);
    }

    public DataListPanel(String title, String dataListTag, ListDelegate<ItemType, ItemCell> delegate) {
        this(title, dataListTag, delegate, true);
    }

    public DataListPanel(String title, String dataListTag, ListDelegate<ItemType, ItemCell> delegate, boolean useEvenOddRowsStyle) {
        super(title);
        this.useEvenOddRowsStyle = useEvenOddRowsStyle;
        if (StringUtils.isBlank(dataListTag)) {
            throw new IllegalArgumentException("Missing dataListTag for " + title + " list panel");
        }
        this.dataListTag = dataListTag;
        this.delegate = delegate;
        this.dataSource = new ListDataSourceImpl();
        addContentStyleName(ResourceUtils.system().css().dataList());

        waiting = new Waiting(this);
        addWidget(waiting);

        generateHeader();
        generateBody();
        generateFooter();
    }

    /**
     * Nastavi delegata listu.
     * @param delegate the delegate to set
     */
    public void setListDelegate(ListDelegate<ItemType, ItemCell> delegate) {
        this.delegate = delegate;
    }

    /**
     * Nastavi zdroj z akeho sa budu nacitavat spravy
     * @param dataSource
     */
    public void setListDataSource(ListDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void onLoad() {
        if (delegate == null) {
            throw new NullPointerException("List delegate for list '" + this.getPanelTitle() + "' is not set!");
        }
        loadItems();
    }

    /**************************************************************************/
    /* Controls                                                               */
    /**************************************************************************/
    public void enableExport(ListExportTypeEnum... e) {
        if (e.length > 0) {
            ListExportButton button = new ListExportButton(this);
            button.setExportTypes(e);
            addRightControll(button);
        }
    }

    /**
     * Filter prevolava tuto metodu ak chce prekreslit zoznam
     */
    void refresWithFilter(ListFilter filter) {
        logger.debug("Refresh list: Filter changed");
        setActiveFilter(filter);
        reserItems();
    }

    /**
     * Orderer prevolava tuto metodu ak chce prekreslit zoznam
     */
    void refresWithOrderer(ListOrderer orderer) {
        logger.debug("Refresh list: Orderer changed");
        reserItems();
    }

    /**
     * Pri zmene nastavenia zoznamu sa znovu nastavi prva stranka
     */
    private void reserItems() {
        actualPage = 1;
        loadItems();
    }

    protected void preLoadProcessing() {
    }

    protected void postLoadProcessing() {
    }

    /**
     * Metoda obnovi list.
     */
    private void loadItems() {
        // Kontrola aktivneho filtru
        if (getActiveFilter() == null) {
            throw new NullPointerException("ActiveFilter must be set!");
        }

        List<Integer> orderTagsList = getOrderers();


        int[] orderTagsArray = new int[orderTagsList.size()];
        for (int i = 0; i < orderTagsList.size(); i++) {
            orderTagsArray[i] = orderTagsList.get(i);
        }

        // Vytvorenie requestu
        ListDataSourceRequest request = createRequest();
        request.setFilter(getActiveFilter().getFilter());
        request.setOrderers(orderTagsArray);

        request.setItemsPerPage(perPage);
        // Vypocitani prveho resultu. prvy vyseldek je od 0.
        request.setFirstResult((actualPage - 1) * perPage);

        request.setDataListTag(getDataListTag());
        preLoadProcessing();
        dataSource.sendRequest(request, new AsyncCallback<ListDataSourceResult>() {

            @Override
            public void onFailure(Throwable caught) {
                showError(caught);
            }

            @Override
            public void onSuccess(ListDataSourceResult result) {
                refreshItems(result);
            }
        }, this);
    }

    /**
     * Metoda ktoru je mozne pretazit aby sa zmenilo chovanie nacitavania datalistu
     * 
     * @return
     */
    protected ListDataSourceRequest createRequest() {
        return new ListDataSourceRequest();
    }

    /**
     * Volane z ListDataSource ked pridu data
     *
     */
    void refreshItems(final ListDataSourceResult result) {
        // Remove NoItems
        body.clear();
        // odstranime vsetky zaznamy stare
        itemsPanel.clear();
        // Vytvorime novy obsah
        if (result.getAllItemsCount() == 0) {
            showEmpty();
        } else {
            if (asyncPreprocesing()) {
                doAsyncPreprocesing(result, new ConsysAction() {

                    @Override
                    public void run() {
                        showItems(result);
                        actualizePagers(result.getAllItemsCount());
                        postLoadProcessing();
                    }
                });
                return;
            } else {
                showItems(result);
            }
        }
        actualizePagers(result.getAllItemsCount());
        postLoadProcessing();
    }

    private void showItems(ListDataSourceResult result) {
        //setPanelHeight();
        int index = 0;
        body.setWidget(itemsScrollPanel);
        actualizePagers(result.getAllItemsCount());
        for (Object item : result.getItems()) {
            ItemCell cell = delegate.createCell((ItemType) item);
            addCellItem(cell, index++);
        }
    }

    /** bude se neco predzpracovavat? (urceno k pretizeni v kombinaci s doAsyncPreprocesing) */
    protected boolean asyncPreprocesing() {
        return false;
    }

    /** provedeni asynchronni akce pred vykreslenim seznamu, na konci zavolat action, ktery spusti pokracovani */
    protected void doAsyncPreprocesing(ListDataSourceResult result, ConsysAction action) {
    }

    /**
     * Na zaklade aktualnej stranky ,poctu zaznamov na stranku a vsetkych zaznamov
     * nastavi pagere.
     */
    private void actualizePagers(long allItemsCount) {
        int from = ((actualPage * perPage) - perPage) + 1;
        int to = actualPage * perPage;
        if (to > allItemsCount) {
            to = (int) allItemsCount;
        }
        for (ListPagerWidget lp : pagers) {
            lp.setPage(from, to, allItemsCount);
        }
    }

    /**
     * Nastavenie defaultneho filteru ktori je aktivny hned od zaciatku
     */
    public void setDefaultFilter(ListFilter filter) {
        setActiveFilter(filter);
    }

    /**
     * Metoda je volana na nastavenie prave aktivneho filtra
     */
    private void setActiveFilter(ListFilter filter) {
        if (getActiveFilter() != null) {
            getActiveFilter().deselect();
        }
        activeFilter = filter;
    }

    /**************************************************************************/
    /* Header                                                                 */
    /**************************************************************************/
    private void generateHeader() {
        header = new FlowPanel();
        header.setStyleName(ResourceUtils.system().css().dataListHeader());
        headPanels = new ArrayList<HeadPanel>();
        pagers = new ArrayList<ListPagerWidget>();
        addWidget(header);
    }

    /**
     * Prida headpanel do headeru a zaregistruje potrebne akcie. Tak ako sa tam
     * panele davaju tak sa aj vykresluju.
     */
    public void addHeadPanel(HeadPanel headPanel) {
        // prasacinka ale je to jedine misto kde sa pridava order a je tam pager
        if (headPanel instanceof DataListCommonHeadPanel) {
            DataListCommonHeadPanel chp = (DataListCommonHeadPanel) headPanel;
            ListPagerWidget pager = chp.getListPagerWidget();
            pager.setDelegate(this);
            pagers.add(pager);

            // nasetovani parenta vsem ordererum
            chp.setParentList(this);
        }
        headPanels.add(headPanel);
        for (ListFilter f : headPanel.filterList) {
            f.setParentList(this);
        }
        for (ListOrderer f : headPanel.ordererList) {
            f.setParentList(this);
        }
        header.add(headPanel);
    }

    public void addHeadPanel(Widget panel) {
        header.add(panel);
    }

    /**************************************************************************/
    /* Body                                                           */
    /**************************************************************************/
    private void generateBody() {
        itemsPanel = new FlowPanel();
        itemsScrollPanel = new ScrollPanel();
        itemsScrollPanel.setWidget(itemsPanel);
        body = new SimplePanel();
        body.setStyleName(ResourceUtils.system().css().dataListBody());
        addWidget(body);
    }

    /**************************************************************************/
    /* Body - ITEMS                                                           */
    /**************************************************************************/
    private void addCellItem(ItemCell cellItem, int index) {
        if (useEvenOddRowsStyle) {
            if (index % 2 == 0) { // sude
                cellItem.addStyleName(ResourceUtils.system().css().dataListItemEven());
            } else {
                cellItem.addStyleName(ResourceUtils.system().css().dataListItemOdd());
            }
        }
        cellItem.getElement().setId("item-" + index);
        itemsPanel.add(cellItem);
    }

    /**************************************************************************/
    /* Body - OTHER                                                           */
    /**************************************************************************/
    /**
     * Ukaze do body empty panel
     */
    private void showEmpty() {
        SimplePanel emptyPanel = new SimplePanel();
        emptyPanel.setStyleName(ResourceUtils.system().css().emptyList());
        Label l = new Label(UIMessageUtils.c.dataList_text_empty());
        l.setStyleName(ResourceUtils.system().css().emptyListText());
        emptyPanel.setWidget(l);
        body.setWidget(emptyPanel);
    }

    /**
     * Ukaze do body error
     */
    private void showError(Throwable caught) {
        ConsysMessage failed = ConsysMessage.getFail();
        failed.setText(UIMessageUtils.c.dataList_error());
        body.setWidget(failed);
    }

    /**************************************************************************/
    /* Footer                                                                 */
    /**************************************************************************/
    /**
     * Footer obsahuje dafaltne nastavenie vyberu zaznamov na stranku a aktualen
     * vybranu stranku
     */
    private void generateFooter() {
        FlowPanel flowPanel = new FlowPanel();
        flowPanel.setWidth("100%");

        // PerPage Select Box
        perPageSelectBox = new SelectBox<Integer>();
        perPageSelectBox.setWidth(50);
        perPageSelectBox.addStyleName(CssStyles.MARGIN_LEFT_10);
        perPageSelectBox.setItems(getPerPageValues());
        perPageSelectBox.selectFirst(true);
        perPageSelectBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<Integer>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                perPage = event.getValue().getItem();
                reserItems();
            }
        });
        perPage = perPageSelectBox.getSelectedItem().getItem();

        // PerPage Select Box wrapper
        SimplePanel selectBoxWrapper = new SimplePanel();
        selectBoxWrapper.setWidget(perPageSelectBox);
        selectBoxWrapper.setStyleName(ResourceUtils.system().css().perPageSelect());


        flowPanel.add(selectBoxWrapper);
        // Pager
        ListPagerWidget listPagerWidget = new ListPagerWidget(this);
        SimplePanel sp = new SimplePanel();
        sp.setWidget(listPagerWidget);
        sp.addStyleName(CssStyles.FLOAT_RIGHT);
        flowPanel.add(sp);

        pagers.add(listPagerWidget);
        footer = new FlowPanel();
        footer.add(flowPanel);
        footer.setStyleName(ResourceUtils.system().css().dataListFooter());
        addWidget(footer);
    }

    /**
     * nastavi vychozi hodnotu strankovani
     * @return true pokud je spravna hodnota strankovani (nastavi hodnotu)
     * @return false pokud neodpovida hodnota, hodnote strankovani (nic nemeni)
     */
    public boolean setDefaultPaging(int value) {
        int[] paging = {PAGE25, PAGE50, PAGE100};
        boolean found = false;
        for (int p : paging) {
            if (p == value) {
                found = true;
                break;
            }
        }
        if (found) {
            perPageSelectBox.selectItem(value);
        }
        return found;
    }

    protected ArrayList<SelectBoxItem<Integer>> getPerPageValues() {
        return generatePerPageValues(PAGE25, PAGE50, PAGE100);
    }

    protected ArrayList<SelectBoxItem<Integer>> generatePerPageValues(int... values) {
        ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
        for (int i = 0; i < values.length; i++) {
            int j = values[i];
            out.add(new SelectBoxItem<Integer>(values[i]));
        }
        return out;
    }

    /**************************************************************************/
    /* Pager Delegate                                                         */
    /**************************************************************************/
    @Override
    public void nextPage() {
        actualPage += 1;
        loadItems();
    }

    @Override
    public void previousPage() {
        actualPage -= 1;
        loadItems();
    }

    /**************************************************************************/
    /* Action Executor                                                        */
    /**************************************************************************/
    @Override
    public void actionStarted() {
        waiting.show();
    }

    @Override
    public void actionEnds() {
        waiting.hide();
    }

    /**
     * Aktualne nic nespracovava
     */
    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        showError(null);
    }

    /**
     * @return the activeFilter
     */
    public ListFilter getActiveFilter() {
        return activeFilter;
    }

    public List<Integer> getOrderers() {
        List<Integer> orderTagsList = new ArrayList<Integer>();
        for (HeadPanel head : headPanels) {
            for (ListOrderer o : head.ordererList) {
                orderTagsList.add(o.getTag());
            }
        }
        return orderTagsList;
    }

    /**
     * @return the dataListTag
     */
    public String getDataListTag() {
        return dataListTag;
    }
}
