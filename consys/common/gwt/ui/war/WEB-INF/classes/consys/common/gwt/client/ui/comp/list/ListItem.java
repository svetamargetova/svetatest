package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * Predok grafickej reprezentacie jedneho zaznamu v zozname.
 * @author palo
 */
public abstract class ListItem<T> extends SimplePanel {

    // komponenty
    private FlowPanel body;
    // data
    private boolean selected = false;
    private T dataItem;

    public ListItem(T dataItem) {
        this(dataItem, false);
    }

    public ListItem(T dataItem, boolean useDataListItemNoBorder) {
        this.dataItem = dataItem;
        setStyleName(useDataListItemNoBorder
                ? ResourceUtils.system().css().dataListItemNoBorder()
                : ResourceUtils.system().css().dataListItem());
    }

    @Override
    protected void onLoad() {
        if (body == null) {
            body = new FlowPanel();
            setWidget(body);
            body.setStyleName(ResourceUtils.system().css().dataListItemBody());
        }
        createCell(dataItem, body);
    }

    protected abstract void createCell(T item, FlowPanel panel);

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public T getDataItem() {
        return dataItem;
    }
}
