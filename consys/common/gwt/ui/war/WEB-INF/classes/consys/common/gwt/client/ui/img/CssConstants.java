package consys.common.gwt.client.ui.img;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 * Konstanty css stylu
 * @author pepa
 */
@ImportedWithPrefix("const")
public interface CssConstants extends CssResource {

    String COLOR_CONSYS_BLUE();

    String COLOR_GREEN();

    String COLOR_RED();

    String COLOR_GRAY();
}
