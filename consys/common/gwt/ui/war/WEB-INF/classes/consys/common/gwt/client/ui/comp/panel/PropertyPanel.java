package consys.common.gwt.client.ui.comp.panel;

import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;

/**
 * Panel pro editaci property
 * @author pepa
 */
public class PropertyPanel extends SimpleFormPanel {

    // data
    private boolean first;

    public PropertyPanel() {
        super();
        first = true;
    }

    @Override
    public void clear() {
        super.clear();
        first = true;
    }

    /** prida nastavitelne property, pro textovou hodnotu */
    public void addProperty(String key, String name, String actualValue, String helpText) {
        addProperty(key, name, new String[]{}, actualValue, helpText);
    }

    /** prida nastavitelne property, pro vyberovou hodnotu */
    public void addProperty(String key, String name, String[] values, String actualValue, String helpText) {
        ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
        for (int i = 0; i < values.length; i++) {
            items.add(new SelectBoxItem<String>(values[i]));
        }
        addProperty(key, name, items, actualValue, helpText);
    }

    /** prida nastavitelne property */
    public void addProperty(String key, String name, ArrayList<SelectBoxItem<String>> items, String actualValue, String helpText) {
        addProperty(key, name, items, actualValue, helpText, null);
    }

    /** prida nastavitelne property */
    public void addProperty(String key, String name, ArrayList<SelectBoxItem<String>> items, String actualValue,
            String helpText, String labelWidth) {
        if (first) {
            first = false;
        } else {
            addWidget(new Separator("100%", MARGIN_VER_10));
        }
        addWidget(new PropertyPanelItem(this, key, name, items, actualValue, helpText, labelWidth));
    }

    /** prida property */
    public void addProperty(PropertyPanelItem item, boolean addSeparator) {
        if (addSeparator) {
            addWidget(new Separator("100%", MARGIN_VER_10));
        }
        if (!first && !addSeparator) {
            addWidget(StyleUtils.clearDiv("20px"));
        }
        addWidget(item);
    }
}
