package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class ConsysRedWrapper extends ConsysWrapper {

    public ConsysRedWrapper() {
        super();
    }

    public ConsysRedWrapper(Widget widget) {
        super(widget);
    }

    public ConsysRedWrapper(Widget widget, int settings) {
        super(widget, settings);
    }

    @Override
    public WrapperFace getWrappertFace() {
        Image bottomLeft = new Image(ResourceUtils.system().corRedBl());
        Image bottomRight = new Image(ResourceUtils.system().corRedBr());
        Image topRight = new Image(ResourceUtils.system().corRedTr());
        Image topLeft = new Image(ResourceUtils.system().corRedTl());
        WrapperFace face = new WrapperFace(new Image[]{bottomLeft, bottomRight, topRight, topLeft}, 3, StyleUtils.BACKGROUND_RED);
        return face;
    }
}
