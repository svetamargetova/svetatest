package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBlueWrapper;
import consys.common.gwt.client.ui.comp.wrapper.ConsysGreenWrapper;
import consys.common.gwt.client.ui.comp.wrapper.ConsysRedWrapper;
import consys.common.gwt.client.ui.comp.wrapper.ConsysWrapper;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class ConsysMessage extends Composite {

    // konstanty
    private static final String INNER_MESSAGE_WIDTH = "484px";
    public static final String MINIMINAL_HEIGHT = "50px";
    private static final String STYLE_CLASS = "consys-message";
    private static final int DEFAULT_CLOSE_TIMEOUT = 7;
    // komponenty
    private ClickAndHoverFlowPanel close;
    private FlowPanel content;
    private ConsysWrapper wrapper;
    private FlowPanel iconWrapper;
    private FlowPanel panelWrapper;
    // data
    private Timer autoCloseTimer;
    private boolean dontStartCloseTimer = false;

    private ConsysMessage(Image icon, ConsysWrapper wrapper) {
        this.wrapper = wrapper;

        content = new FlowPanel();
        content.setWidth("404px");
        content.setStyleName(StyleUtils.MARGIN_TOP_10);
        content.addStyleName(StyleUtils.MARGIN_BOT_10);
        content.addStyleName(StyleUtils.MARGIN_VER_10);
        content.addStyleName(StyleUtils.FLOAT_LEFT);
        content.addStyleName(STYLE_CLASS);

        close = new ClickAndHoverFlowPanel();
        close.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ConsysMessage.this.removeFromParent();
                if (autoCloseTimer != null) {
                    autoCloseTimer.cancel();
                    autoCloseTimer = null;
                }
            }
        });

        icon.setStyleName(StyleUtils.MARGIN_LEFT_20);
        icon.addStyleName(StyleUtils.MARGIN_VER_10);
        iconWrapper = new FlowPanel();
        iconWrapper.setWidth("55px");
        iconWrapper.setStyleName(StyleUtils.FLOAT_LEFT);
        iconWrapper.add(icon);

        FlowPanel padding = new FlowPanel();
        padding.setHeight("1px");
        padding.setStyleName(StyleUtils.CLEAR_BOTH);
        padding.addStyleName(StyleUtils.OVERFLOW_HIDDEN);

        FlowPanel fp = new FlowPanel();
        fp.setStyleName(StyleUtils.FLOAT_LEFT);
        fp.add(iconWrapper);
        fp.add(content);
        fp.add(StyleUtils.clearDiv());

        FlowPanel panel = new FlowPanel();
        panel.add(fp);
        panel.add(close);
        panel.add(padding);

        wrapper.setWidth(INNER_MESSAGE_WIDTH);
        wrapper.setWidget(panel);

        panelWrapper = new FlowPanel();
        panelWrapper.add(wrapper);

        initWidget(panelWrapper);
        setVisible(false);
    }

    public void setWide(boolean wide) {
        if (wide) {
            wrapper.setWidth("100%");
        } else {
            wrapper.setWidth(INNER_MESSAGE_WIDTH);
        }
    }

    /**
     * nastavi sirku. Automaticky sa pripocitava 80px;
     * @parame width je maximalni sirka textu zpravy
     */
    public void setWidth(int width) {
        content.setWidth(width + "px");
        wrapper.setWidth((width + 80) + "px");
    }

    /** necha zobrazit nebo skrit zaviraci krizek */
    public void hideClose(boolean value) {
        close.setVisible(!value);
    }

    /**
     * Odstrani ikonu z komponenty
     */
    public void hideIcon() {
        iconWrapper.removeFromParent();
    }

    /** vraci objekt pro zpravu o chybe ([!] cervene) */
    public static ConsysMessage getFail() {
        return new ConsysMessage(new Image(ResourceUtils.system().messageFailIcon()), new ConsysRedWrapper());
    }

    /** vraci objekt pro zpravu o infu ([i] modre) */
    public static ConsysMessage getInfo() {
        ConsysMessage m = new ConsysMessage(new Image(ResourceUtils.system().messageInfoIcon()), new ConsysBlueWrapper());
        m.dontStartCloseTimer();
        m.setWide(false);
        return m;
    }

    /** vraci objekt pro zpravu o uspechu ([i] zelene) */
    public static ConsysMessage getSuccess() {
        return getSuccess(false);
    }

    /** vraci objekt pro zpravu o uspechu ([i] zelene) */
    public static ConsysMessage getSuccess(boolean wide) {
        ConsysMessage m = new ConsysMessage(new Image(ResourceUtils.system().messageSuccessIcon()), new ConsysGreenWrapper());
        m.setWide(wide);
        m.dontStartCloseTimer();
        return m;
    }

    /** vlozi text zpravy a automaticky zobrazi komponentu */
    public void setText(String text) {
        setText(text, false);
    }

    public void setText(String text, boolean asHtml) {
        content.clear();
        addOrSetText(text, asHtml);
    }

    /** vlozi text zpravy a automaticky zobrazi komponentu */
    /*public void addOrSetTextWithAutohide(String text) {
     addOrSetText(text, false);
     Timer t = new Timer() {

     int visibility = 95;
     int decrement = 5;
     boolean first = true;

     @Override
     public void run() {
     if (first) {
     scheduleRepeating(70);
     first = false;
     }
     if (visibility == 0) {
     cancel();
     content.clear();
     setVisible(false);
     return;
     }
     DOM.setStyleAttribute(ConsysMessage.this.getElement(), "opacity", "." + visibility);
     visibility -= decrement;
     }
     };
     t.schedule(1000);
     }*/
    public void addOrSetText(String text) {
        addOrSetText(text, false);
    }

    /** prida text zpravy a automaticky zobrazi komponentu */
    public void addOrSetText(String text, boolean asHtml) {
        Label label;
        if (asHtml) {
            label = new HTML(text);
        } else {
            label = new Label(text);
        }

        label.setStyleName(StyleUtils.FONT_BOLD);
        label.addStyleName(StyleUtils.FONT_13PX);
        content.add(label);

        DOM.setStyleAttribute(ConsysMessage.this.getElement(), "opacity", "1.0");
        setVisible(true);

        if (!dontStartCloseTimer) {
            startCloseTimeout(DEFAULT_CLOSE_TIMEOUT);
        }
    }

    /** 
     * po uplynuti zadaneho poctu sekund se automaticky zrusi chybova hlaska, vsechny zpravy maji ve vychozim
     * stavu nastaven standardni zaviraci cas 7 sekund
     */
    public void startCloseTimeout(int second) {
        if (autoCloseTimer != null) {
            autoCloseTimer.cancel();
            autoCloseTimer = null;
        }
        autoCloseTimer = new Timer() {
            @Override
            public void run() {
                ConsysMessage.this.removeFromParent();
            }
        };
        autoCloseTimer.schedule(second * 1000);
    }

    /** zastavi odpocet do zavreni */
    public void dontStartCloseTimer() {
        dontStartCloseTimer = true;
    }

    /** nastavi vertikalni mezery (pomoci vlozeni divu nad a pod - kvuli spravnemu zobrazeni ve dialogu) */
    public void setVerticalSpace(String heightInPixels) {
        panelWrapper.insert(StyleUtils.getPadder(heightInPixels), 0);
        panelWrapper.add(StyleUtils.getPadder(heightInPixels));
    }

    /** nastaveni sirky wrapperu komponenty */
    public void setWrapperWidth(String width) {
        panelWrapper.setWidth(width);
    }

    /** vnitri trida zaviratka */
    public class ClickAndHoverFlowPanel extends FlowPanel {

        public ClickAndHoverFlowPanel() {
            super();

            setStyleName(StyleUtils.FLOAT_LEFT);
            addStyleName(StyleUtils.HAND);
            addStyleName(StyleUtils.MARGIN_TOP_5);
            addStyleName(StyleUtils.MARGIN_LEFT_10);
            addStyleName(ResourceUtils.system().css().msgCloseOut());

            sinkEvents(Event.ONCLICK | Event.ONMOUSEOVER | Event.ONMOUSEOUT);

            addHandler(new MouseOverHandler() {
                @Override
                public void onMouseOver(MouseOverEvent event) {
                    addStyleName(ResourceUtils.system().css().msgCloseOver());
                    removeStyleName(ResourceUtils.system().css().msgCloseOut());
                }
            }, MouseOverEvent.getType());
            addHandler(new MouseOutHandler() {
                @Override
                public void onMouseOut(MouseOutEvent event) {
                    addStyleName(ResourceUtils.system().css().msgCloseOut());
                    removeStyleName(ResourceUtils.system().css().msgCloseOver());
                }
            }, MouseOutEvent.getType());
        }

        /** zaregistruje ClickHandler */
        public HandlerRegistration addClickHandler(ClickHandler handler) {
            return addHandler(handler, ClickEvent.getType());
        }
    }
}
