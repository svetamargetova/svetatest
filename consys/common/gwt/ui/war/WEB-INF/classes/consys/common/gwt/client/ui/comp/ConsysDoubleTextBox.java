package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 * Automaticky validovatelny TextBox pro double
 * @author pepa
 */
public class ConsysDoubleTextBox extends TextBox implements ValidableComponent, HasIdentifier {

    private double min;
    private double max;
    private String fieldName;

    public ConsysDoubleTextBox(double min, double max) {
        this(null, min, max, "");
    }

    public ConsysDoubleTextBox(double min, double max, String fieldName) {
        this(null, min, max, fieldName);
    }

    public ConsysDoubleTextBox(String width, final double min, final double max, String fieldName) {
        this.min = min;
        this.max = max;
        this.fieldName = fieldName;

        setStyleName(StyleUtils.BORDER);

        if (width == null) {
            addStyleName(StyleUtils.WIDTH_270);
        } else {
            setWidth(width);
        }

        addStyleName(StyleUtils.HEIGHT_21);
        addStyleName(ResourceUtils.system().css().inputPadd());

        addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                // kontrola na spravnost povolenych znaku
                String text = getText();
                if (!text.matches("[\\-]?[0-9]+\\.?[0-9]*")) {
                    int dot = text.indexOf(".");
                    if (text.startsWith("-")) {
                        text = "-" + replace(text.substring(1), dot);
                    } else {
                        text = replace(getText(), dot);
                    }
                    setText(text);
                }
                // kontrola na rozsah
                try {
                    double number = Double.parseDouble(text);
                    if (number > max) {
                        setText(max);
                    }
                } catch (NumberFormatException ex) {
                    // bylo zatim zadano jen minusko a nelze prevest na cislo, nic nedelam
                }
            }
        });
        addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                String text = getText();
                if (text.equals("-") || text.equals(".") || text.equals("-.")) {
                    setText("");
                }
            }
        });
    }

    /** vyhodi neplatne znaky */
    private String replace(String text, int dot) {
        if (dot != -1) {
            String part1 = text.substring(0, dot);
            String part2 = text.substring(dot, text.length());
            return part1.replaceAll(ConsysIntTextBox.NO_NUMBER_REG_EXP, "")
                    + "."
                    + part2.replaceAll(ConsysIntTextBox.NO_NUMBER_REG_EXP, "");
        } else {
            return text.replaceAll(ConsysIntTextBox.NO_NUMBER_REG_EXP, "");
        }
    }

    /** nastavi double jako text */
    public void setText(double value) {
        setText(FormUtils.doubleFormat.format(value));
    }

    /** vraci hodnotu textu jako double */
    public double getTextDouble() {
        // at funguje i na tecku i na carku
        return Double.parseDouble(getText().trim().replaceAll(",", "."));
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        if (!ValidatorUtils.isValidString(getText().trim())) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(fieldName));
            return false;
        }
        try {
            double value = getTextDouble();
            if (!ValidatorUtils.isRange(value, min, max)) {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
                return false;
            }
        } catch (NumberFormatException ex) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustNumeric(fieldName));
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        addStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public String getIdentifier() {
        return fieldName;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.fieldName = identifier;
    }
}
