package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.user.UserLabel;
import consys.common.gwt.client.ui.comp.user.UserResources;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserThumb;

/**
 * 
 * Jeden zaznam do zoznamu uzivatelov kde je uzivatel len ako UserLabel.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserLabelListEditItem extends UserLabelListItem {

    // data
    private RemoveDelegate removeDelegate;
    private ClientInvitedUser csoiu;

    public UserLabelListEditItem(ClientUserThumb object) {
        super(object);
        csoiu = new ClientInvitedUser(object.getUuid());
        csoiu.setName(object.getName());
        csoiu.setUuidPortraitImagePrefix(object.getUuidPortraitImagePrefix());
    }

    public UserLabelListEditItem(ClientInvitedUser csoiu) {
        super(csoiu.getClientUserThumb());
        this.csoiu = csoiu;
    }

    @Override
    public void create(FlowPanel panel) {
        UserLabel userLabel = new UserLabel(getObject().getUuid(),getObject().getName());        
        SimplePanel userLabelWrapper = new SimplePanel();
        userLabelWrapper.setWidget(userLabel);
        userLabelWrapper.setStyleName(UserResources.INSTANCE.userCss().userLabelListEditItemName());

        ActionLabel removeUser = new ActionLabel(UIMessageUtils.c.const_remove());
        removeUser.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (removeDelegate != null) {
                    removeDelegate.didRemoveItem(UserLabelListEditItem.this);
                }
            }
        });

        SimplePanel removeLabelWrapper = new SimplePanel();
        removeLabelWrapper.setStyleName(UserResources.INSTANCE.userCss().userLabelListEditItemAction());
        removeLabelWrapper.setWidget(removeUser);


        panel.add(userLabelWrapper);
        panel.add(removeLabelWrapper);
    }

    public void setRemoveDelegate(RemoveDelegate removeDelegate) {
        this.removeDelegate = removeDelegate;
    }

    public ClientInvitedUser getClientSearchedOrInvitedUser() {
        return csoiu;
    }

    public interface RemoveDelegate {

        public void didRemoveItem(UserLabelListEditItem item);
    }
}
