package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;
import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SelectDatePicker extends FlowPanel {

    private static final int DEFAULT_TO_YEAR_OFFSET = 20;
    private static final int DEFAULT_FROM_YEAR_OFFSET = 70;
    private static final String[] MONTHS = new String[]{
        UIMessageUtils.c.const_date_monthJanuary(), // 31
        UIMessageUtils.c.const_date_monthFebruary(), // 2012 29 .... 28
        UIMessageUtils.c.const_date_monthMarch(), // 31
        UIMessageUtils.c.const_date_monthApril(), // 30
        UIMessageUtils.c.const_date_monthMay(), // 31
        UIMessageUtils.c.const_date_monthJune(), // 30
        UIMessageUtils.c.const_date_monthJuly(), // 31
        UIMessageUtils.c.const_date_monthAugust(), // 31
        UIMessageUtils.c.const_date_monthSeptember(), // 30
        UIMessageUtils.c.const_date_monthOctober(), // 31
        UIMessageUtils.c.const_date_monthNovember(), // 30
        UIMessageUtils.c.const_date_monthDecember()}; //31
    private Date date;
    private boolean required;
    private int fromYear;
    private int toYear;
    // Edit Mode
    private SelectBox<Integer> day;
    private SelectBox<Integer> month;
    private SelectBox<Integer> year;

    public SelectDatePicker() {
        this(false);
    }

    public SelectDatePicker(boolean past) {
        super();
        Date d = new Date();
        if (past) {
            setYearInterval(d.getYear() - DEFAULT_FROM_YEAR_OFFSET, d.getYear());
        } else {
            setYearInterval(d.getYear(), d.getYear() + DEFAULT_TO_YEAR_OFFSET);
        }
        createWidget();
        initBoxesFromDate();
    }

    /**
     * Automatickyty read mode
     */
    public SelectDatePicker(Date date) {
        super();
        createWidget();
        setYearInterval(date.getYear(), date.getYear() + DEFAULT_TO_YEAR_OFFSET);
        setDate(date);
    }

    public void setDate(Date date) {
        this.date = date;
        initBoxesFromDate();
        refreshBoxesWithDate();
    }

    public Date getSelectedDate() {
        return date;
    }

    /**
     * Tento getter je pre vnutorne pouzitie kedy na prve zavolanie (vyber) datumu
     * sa autoaticky vytvory dnesok ak este neexistuje
     * @return the date
     */
    private Date getDate() {
        if (date == null) {
            date = new Date();
        }
        return date;
    }

    public void setYearInterval(int from, int to) {
        if (from > to) {
            throw new IllegalArgumentException("From is bigger then to!");
        }
        fromYear = from;
        toYear = to;
    }

    private void createWidget() {
        day = new SelectBox<Integer>();
        day.setWidth(50);
        day.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<Integer>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                getDate().setDate(event.getValue().getItem());
            }
        });

        month = new SelectBox<Integer>();
        month.setWidth(95);
        month.addStyleName(CssStyles.MARGIN_HOR_5);
        month.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<Integer>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                int actualDay = getDate().getDate();
                int newDays = daysInMonth(event.getValue().getItem());

                day.setItems(createDays(newDays));
                if (actualDay > newDays) {                    
                    getDate().setDate(newDays);
                    day.selectItem(newDays);
                }
                getDate().setMonth(event.getValue().getItem());
            }
        });


        year = new SelectBox<Integer>();
        year.setWidth(60);
        year.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<Integer>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                // Zistime ci nie je vybrany februar
                if (getDate().getMonth() == 1) {
                    // Zistime ci sa aktualny rok a vybrany rok zhoduju v prechodovosti. Ak nie tak musime osetrit nastavenie dna
                    if (isLeapYear(getDate().getYear()) ^ isLeapYear(event.getValue().getItem())) {
                        int actualDay = getDate().getDate();
                        int newDays = daysInMonth(getDate().getMonth(), event.getValue().getItem());

                        day.setItems(createDays(newDays));
                        if (actualDay > newDays) {                            
                            getDate().setDate(newDays);
                            day.selectItem(newDays);
                        }
                    }
                }
                getDate().setYear(event.getValue().getItem());
            }
        });

        add(day);
        add(month);
        add(year);

        day.addStyleName(CssStyles.FLOAT_LEFT);
        month.addStyleName(CssStyles.FLOAT_LEFT);
    }

    private void initBoxesFromDate() {
        day.setItems(createDays(daysInMonth(getDate().getMonth())));
        month.setItems(createMonths());
        year.setItems(createYears());
    }

    public int daysInMonth(int month) { // 0 - 11
        return daysInMonth(month, getDate().getYear());
    }

    public int daysInMonth(int month, int year) { // 0 - 11
        switch (month) {
            case 0:
            case 2:
            case 4:
            case 6:
            case 7:
            case 9:
            case 11:
                return 31;
            case 1:
                return isLeapYear(year) ? 29 : 28;
            default:
                return 30;
        }
    }

    public static boolean isLeapYear(int theYear) {
        // Is theYear Divisible by 4?
        if (theYear % 4 == 0) {

            // Is theYear Divisible by 4 but not 100?
            if (theYear % 100 != 0) {
                return true;
            } // Is theYear Divisible by 4 and 100 and 400?
            else if (theYear % 400 == 0) {
                return true;
            } // It is Divisible by 4 and 100 but not 400!
            else {
                return false;
            }
        }
        return false;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
        // Ak je required tak defaultne nastavime dnesok
        if (required) {
            refreshBoxesWithDate();
        }
    }

    private void refreshBoxesWithDate() {
        day.selectItem(getDate().getDate());
        month.selectItem(getDate().getMonth());
        year.selectItem(getDate().getYear());
    }

    //************** GENERATORY OBSAHU
    private static final ArrayList<SelectBoxItem<Integer>> createDays(int days) {
        ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
        for (int i = 1; i <= days; i++) {
            out.add(new SelectBoxItem<Integer>(i));
        }
        return out;
    }

    private static final ArrayList<SelectBoxItem<Integer>> createMonths() {
        ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
        for (int i = 0; i < 12; i++) {
            out.add(new SelectBoxItem<Integer>(i, MONTHS[i]));
        }
        return out;
    }

    private final ArrayList<SelectBoxItem<Integer>> createYears() {

        ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
        for (int i = fromYear; i <= toYear; i++) {
            out.add(new SelectBoxItem<Integer>(i, (i + 1900) + ""));
        }
        return out;
    }
}
