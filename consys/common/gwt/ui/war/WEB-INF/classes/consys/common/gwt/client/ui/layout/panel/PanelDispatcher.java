package consys.common.gwt.client.ui.layout.panel;

import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.event.UserLogoutEvent;
import consys.common.gwt.client.ui.event.PanelItemEvent;
import consys.common.gwt.client.ui.layout.LayoutPanel;
import java.util.ArrayList;

/**
 * Třída má na starost layout panel
 * @author pepa
 */
public class PanelDispatcher implements UserLoginEvent.Handler, UserLogoutEvent.Handler {

    // instance
    private static PanelDispatcher instance;
    // seznam vychozi nabidky prihlaseneho uzivatele
    private ArrayList<PanelItem> defaultItems;
    // seznam polozek panelu neprihlaseneho uzivatele
    private ArrayList<PanelItem> logoutItems;

    public PanelDispatcher() {
        defaultItems = new ArrayList<PanelItem>();
        logoutItems = new ArrayList<PanelItem>();
        EventBus.get().addHandler(UserLoginEvent.TYPE, this);
        EventBus.get().addHandler(UserLogoutEvent.TYPE, this);
    }

    /** vraci instanci PanelDispatcher */
    public static PanelDispatcher get() {
        if (instance == null) {
            instance = new PanelDispatcher();
        }
        return instance;
    }

    /** prida polozku do seznamu vychozich polozek, bude se vkladat do kazdeho panelu prihlaseneho uzivatele */
    public void addDefaultPanelItem(PanelItem item) {
        defaultItems.add(item);
    }

    /**
     * prida polozku do panelu pokud ji jiz neobsahuje
     * @return true pokud se polozka v panelu nevyskytuje a je pridana
     */
    public boolean addPanelItem(PanelItem item, PanelItemType type) {
        boolean result = LayoutPanel.get().contains(item);
        if (!result) {
            PanelItemEvent event = new PanelItemEvent(item, PanelItemAction.ADD, type);
            EventBus.get().fireEvent(event);
        }
        return result;
    }

    /** prida polozku do panelu neprihlaseneho uzivatele */
    public void addLogoutPanelItem(PanelItem item) {
        logoutItems.add(item);
    }

    /** vygeneruje panel v zavislosti na tom zda je uzivatel prihlaseny nebo ne */
    public void generatePanel() {
        Cache.get().doCacheAction(new CacheAction<Boolean>() {

            @Override
            public void doAction(Boolean value) {
                if (value) {
                    generateLoginPanel();
                } else {
                    generateLogoutPanel();
                }
            }

            @Override
            public void doTimeoutAction() {
                // TODO: vypsani chyby a popis dalsiho postupu
            }

            @Override
            public String getName() {
                return Cache.LOGGED;
            }

            @Override
            public boolean isCancel() {
                return false;
            }
        });
    }

    /** vygeneruje panel prihlaseneho uzivatele */
    private void generateLoginPanel() {
        PanelItemEvent event = new PanelItemEvent(PanelItemAction.CLEAR, PanelItemType.ALL);
        EventBus.get().fireEvent(event);
        event = new PanelItemEvent(PanelItemAction.HIDE_ADDON, PanelItemType.ALL);
        EventBus.get().fireEvent(event);

        for (PanelItem item : defaultItems) {
            event = new PanelItemEvent(item, PanelItemAction.ADD, PanelItemType.SYSTEM);
            EventBus.get().fireEvent(event);
        }
    }

    /** vygeneruje panel neprihlaseneho uzivatele */
    private void generateLogoutPanel() {
        PanelItemEvent event = new PanelItemEvent(PanelItemAction.CLEAR, PanelItemType.ALL);
        EventBus.get().fireEvent(event);
        event = new PanelItemEvent(PanelItemAction.HIDE_ADDON, PanelItemType.ALL);
        EventBus.get().fireEvent(event);

        for (PanelItem item : logoutItems) {
            event = new PanelItemEvent(item, PanelItemAction.ADD, PanelItemType.SYSTEM);
            EventBus.get().fireEvent(event);
        }
    }

    @Override
    public void onUserLogin(UserLoginEvent event) {
        generateLoginPanel();
    }

    @Override
    public void onUserLogout(UserLogoutEvent event) {
        generateLogoutPanel();
    }
}
