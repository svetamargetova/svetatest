package consys.common.gwt.client.ui.comp.tag;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import consys.common.gwt.client.ui.comp.SuggestionBox;
import consys.common.gwt.shared.bo.ClientTag;
import java.util.ArrayList;

/**
 * Slouzi pro vkladani tagu / topiku
 * @author pepa
 */
public class TagSuggest extends Composite implements SelectionHandler<SuggestOracle.Suggestion> {

    // komponenty
    private SuggestionBox suggestBox;
    private TagSuggestOracle oracle;
    // data
    private TagDisplay tagDisplay;

    public TagSuggest(TagDisplay tagDisplay) {
        this.tagDisplay = tagDisplay;

        oracle = new TagSuggestOracle();
        suggestBox = new SuggestionBox(oracle);
        suggestBox.addSelectionHandler(this);

        FlowPanel panel = new FlowPanel();
        panel.add(suggestBox);

        initWidget(panel);
    }

    /** vlozi data nad kteryma se bude vyhledavat */
    public void setOracleData(ArrayList<ClientTag> data) {
        oracle.setData(data);
    }

    @Override
    public void onSelection(SelectionEvent<Suggestion> event) {
        Suggestion suggestion = event.getSelectedItem();
        if (suggestion instanceof TagSuggestion) {
            TagSuggestion ts = (TagSuggestion) suggestion;
            if (!tagDisplay.containsTag(ts.getReplacementString())) {
                tagDisplay.addTag(ts.getTagId(), ts.getReplacementString());
            }
        }
        suggestBox.setText("");
    }
}
