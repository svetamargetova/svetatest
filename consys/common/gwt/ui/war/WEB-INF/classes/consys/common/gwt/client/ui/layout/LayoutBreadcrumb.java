package consys.common.gwt.client.ui.layout;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventNameChangedEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.event.BreadcrumBackEvent;
import consys.common.gwt.client.ui.event.BreadcrumbContentEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Drobeckova navigace
 * @author pepa
 */
public class LayoutBreadcrumb extends FlowPanel
        implements BreadcrumbContentEvent.Handler, BreadcrumBackEvent.Handler, EventNameChangedEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(LayoutBreadcrumb.class);

    // instance
    private static LayoutBreadcrumb instance;
    // konstanty
    private static final String BORDER_WIDTH = "5px";
    private static final String SEP_WIDTH = "16px";
    private static final String HEIGHT = "32px";
    // komponenty
    private FlowPanel content;

    private LayoutBreadcrumb() {
        super();
        setVisible(false);
        setSize(LayoutManager.LAYOUT_WIDTH, HEIGHT);

        Image leftBorder = new Image(ResourceUtils.system().breadLeft());
        FlowPanel leftBorderDiv = new FlowPanel();
        leftBorderDiv.setWidth(BORDER_WIDTH);
        leftBorderDiv.setStyleName(StyleUtils.FLOAT_LEFT);
        leftBorderDiv.add(leftBorder);

        Image rightBorder = new Image(ResourceUtils.system().breadRight());
        FlowPanel rightBorderDiv = new FlowPanel();
        rightBorderDiv.setWidth(BORDER_WIDTH);
        rightBorderDiv.setStyleName(StyleUtils.FLOAT_LEFT);
        rightBorderDiv.add(rightBorder);

        SimplePanel contentWrapper = new SimplePanel();
        content = new FlowPanel();
        content.setStyleName(StyleUtils.MARGIN_LEFT_10);
        content.setHeight(HEIGHT);
        contentWrapper.setWidget(content);
        contentWrapper.setStyleName(ResourceUtils.system().css().breadcrumbCenter());
        contentWrapper.addStyleName(StyleUtils.FLOAT_LEFT);

        add(leftBorderDiv);
        add(contentWrapper);
        add(rightBorderDiv);
    }

    /** vraci instanci LayoutBreadcrumb */
    public static LayoutBreadcrumb get() {
        if (instance == null) {
            instance = new LayoutBreadcrumb();
        }
        return instance;
    }

    /** zobrazi breadcrumb, je potreba ho nejdriv naplnit !!! */
    public void show() {
        setVisible(true);
    }

    /** skryje breadcrumb a vyprazdni ho */
    public void hide() {
        setVisible(false);
        content.clear();
    }

    /** vlozi separator */
    private void addSeparator() {
        FlowPanel sep = new FlowPanel();
        sep.setWidth(SEP_WIDTH);
        sep.setStyleName(StyleUtils.FLOAT_LEFT);
        sep.addStyleName(ResourceUtils.system().css().breadcrumbSep());
        Image separator = new Image(ResourceUtils.system().breadSeparator());
        separator.addStyleName(ResourceUtils.system().css().breadcrumbSepIn());
        sep.add(separator);
        content.add(sep);
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(BreadcrumbContentEvent.TYPE, this);
        EventBus.get().addHandler(BreadcrumBackEvent.TYPE, this);
        EventBus.get().addHandler(EventNameChangedEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(BreadcrumbContentEvent.TYPE, this);
        EventBus.get().removeHandler(BreadcrumBackEvent.TYPE, this);
        EventBus.get().removeHandler(EventNameChangedEvent.TYPE, this);
    }

    /** Vycisti cely breadcrumb */
    public void clearBreadcrumbs() {
        logger.debug("clear all");
        content.clear();
    }

    /** Odstrani vsetky breadcrumby az po ROOT breadcrumb */
    public void resetBreadcrumbs() {
        logger.debug("reset to root");
        Widget w = content.getWidget(0);
        content.clear();
        content.add(w);
    }

    /** vymazani odkazu od odkazu niz */
    private void clearFromIndex(int index) {
        logger.debug("clear from " + index + " to " + content.getWidgetCount());
        for (int i = content.getWidgetCount() - 1; i >= index; i--) {
            content.remove(i);
        }
    }

    /** odstrani posledni polozku z Breadcrumbu */
    private void removeLastItem() {
        logger.debug("removing last link");
        int count = content.getWidgetCount();
        if (count == 0) {
            logger.debug("nothing to remove");
            return;
        }
        if (count > 1) {
            content.remove(count - 1);
            content.remove(count - 2);
        } else {
            content.remove(0);
        }
    }

    /** prida polozku do breadcrumbu */
    private void addBreadcrumb(final BreadcrumbContentEvent event) {
        // Zistime aktualnu uroven
        final int actualLevel = content.getWidgetCount();

        // vytvoreni odkazu
        ActionLabel action = new ActionLabel(true, event.getName(), StyleUtils.MARGIN_TOP_3);
        action.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent et) {
                clearFromIndex(actualLevel + (actualLevel == 0 ? 1 : 2));
                EventBus.get().fireEvent(event.getEvent());
            }
        });

        // Ak je uroven 0 tak sa len nastavi novy breadcrumb - Nazov Eventu a
        // ak je vacsi tak sa najskor vlozi separator a potom odkaz
        if (content.getWidgetCount() > 0) {
            addSeparator();
        } else {
            show();
        }

        FlowPanel item = new FlowPanel();
        item.add(action);
        item.setStyleName(StyleUtils.FLOAT_LEFT);
        item.addStyleName(StyleUtils.MARGIN_TOP_10);
        content.add(item);
    }

    /** zpracuje polozku Breadcrumbu */
    private void processContentBreadcrumb(final BreadcrumbContentEvent event) {
        logger.debug(event.getName() + ", level " + event.getLevel());
        switch (event.getLevel()) {
            case NEXT:
                addBreadcrumb(event);
                break;
            case SAME:
                removeLastItem();
                addBreadcrumb(event);
                break;
            case NEXT_ROOT:
                resetBreadcrumbs();
                addBreadcrumb(event);
                break;
            case ROOT:
                clearBreadcrumbs();
                addBreadcrumb(event);
                break;
        }
        if (event.isFireChange()) {
            EventBus.get().fireEvent(event.getEvent());
        }
    }

    @Override
    public void onContentBreadcrumb(BreadcrumbContentEvent event) {
        processContentBreadcrumb(event);
    }

    @Override
    public void onBack() {
        removeLastItem();
        FlowPanel wrapper = (FlowPanel) content.getWidget(content.getWidgetCount() - 1);
        ActionLabel action = (ActionLabel) wrapper.getWidget(0);
        action.click();
    }

    @Override
    public void onEventNameChanged(EventNameChangedEvent event) {
        if (isVisible() && content.getWidgetCount() > 0) {
            FlowPanel wrapper = (FlowPanel) content.getWidget(0);
            ActionLabel action = (ActionLabel) wrapper.getWidget(0);
            action.setText(event.getTitle());
        }
    }
}
