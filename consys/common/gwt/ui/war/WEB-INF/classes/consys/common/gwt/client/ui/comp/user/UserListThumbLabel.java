package consys.common.gwt.client.ui.comp.user;

import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserListThumbLabel extends UserLabel {

    public UserListThumbLabel(ClientUserWithAffiliationThumb searchUserThumb) {
        super(searchUserThumb.getUuid(),searchUserThumb.getName());
    }
}
