package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 * Vlastni implementace TextArea s moznosti omezeni maximalniho poctu znaku
 *
 * @author pepa
 */
public class ConsysWordArea extends ConsysTextArea {

    public ConsysWordArea(int min, int max) {
        this(min, max, WIDTH, HEIGHT, true, "");
    }

    public ConsysWordArea(int min, int max, String fieldName) {
        this(min, max, WIDTH, HEIGHT, true, fieldName);
    }

    public ConsysWordArea(int min, int max, boolean showInfo, String fieldName) {
        this(min, max, WIDTH, HEIGHT, showInfo, fieldName);
    }

    public ConsysWordArea(int max, int width, int height, String fieldName) {
        this(0, max, width, height, true, fieldName);
    }

    public ConsysWordArea(int max, int width, int height, boolean showInfo, String fieldName) {
        this(0, max, width, height, showInfo, fieldName);
    }

    public ConsysWordArea(int min, final int max, int width, int height, boolean showInfo, String fieldName) {
        super(min, max, width, height, showInfo, fieldName);
    }

    @Override
    protected String startLengthValue() {
        return max == UNLIMITED ? "0" : String.valueOf(max);
    }

    @Override
    protected KeyUpHandler areaKeyUpHandler() {
        return new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                String text = area.getText();
                text = text.replaceAll("\\s+", " ");
                String[] split = text.split(" ");
                int length = split.length;
                if (!isUnlimited() && length > max) {
                    length = max;
                    int index = text.lastIndexOf(" ");
                    int scrolled = area.getElement().getScrollTop();
                    area.setText(text.substring(0, index));
                    area.getElement().setScrollTop(scrolled);
                }
                if (text.length() == 0) {
                    length = 0;
                }
                lengthLabel().setText(isUnlimited() ? String.valueOf(length) : String.valueOf(max - length));
            }
        };
    }

    @Override
    public void setText(String text) {
        if (getEmptyText() != null) {
            area.setText(text == null || text.isEmpty() ? getEmptyText() : text);
        } else {
            area.setText(text);
        }
        if (showInfo) {
            if (text != null) {
                text = text.replaceAll("\\s+", " ");
                String[] split = text.split(" ");
                int length = split.length;
                if (isUnlimited()) {
                    lengthLabel().setText(String.valueOf(length));                    
                } else {
                    lengthLabel().setText(String.valueOf(max - length));
                }
            } else {
                lengthLabel().setText(String.valueOf(0));
            }
        }
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        String text = getText();
        text = text.replaceAll("\\s+", " ");
        String[] split = text.split(" ");
        int length = split.length;
        if (text.length() == 0) {
            length = 0;
        }
        if (max == UNLIMITED) {
            if (length < min) {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
                onFail();
                return false;
            }
            onSuccess();
            return true;
        }
        int result = 0;
        if (length < min) {
            result = length - min;
        }
        if (length > max) {
            result = length - max;
        }
        if (result != 0) {
            if (!ValidatorUtils.isValidString(text)) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(fieldName));
            } else {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
            }
            onFail();
            return false;
        }
        onSuccess();
        return true;
    }
}
