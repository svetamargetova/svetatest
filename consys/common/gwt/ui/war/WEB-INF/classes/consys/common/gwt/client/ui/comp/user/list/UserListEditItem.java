package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.user.UserListThumbLabel;
import consys.common.gwt.client.ui.comp.user.UserResources;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserListEditItem extends UserListItem {

    private RemoveDelegate removeDelegate;

    public UserListEditItem(ClientUserWithAffiliationThumb object) {
        super(object);
    }

    @Override
    public void create(FlowPanel panel) {
        
        UserListThumbLabel label = new UserListThumbLabel(getObject());
        Label position = new Label(getObject().getPosition());        
        FlowPanel infoPanel = new FlowPanel();
        infoPanel.add(label);        
        infoPanel.add(position);
        infoPanel.setStyleName(UserResources.INSTANCE.userCss().userListEditItemName());
              

        ActionLabel removeUser = new ActionLabel(UIMessageUtils.c.const_remove());
        removeUser.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (removeDelegate != null) {
                    removeDelegate.didRemoveItem(UserListEditItem.this);
                }
            }
        });

        
        SimplePanel removeWrapper = new SimplePanel();        
        removeWrapper.setStyleName(UserResources.INSTANCE.userCss().userListEditItemAction());
        removeWrapper.setWidget(removeUser);

        panel.add(infoPanel);
        panel.add(removeWrapper);

    }

    public void setRemoveDelegate(RemoveDelegate removeDelegate) {
        this.removeDelegate = removeDelegate;
    }

    public interface RemoveDelegate {

        public void didRemoveItem(UserListEditItem item);
    }
}
