package consys.common.gwt.client.ui.comp.panel.element;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.panel.OrderPanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class OrderPanelItemUI<T extends OrderPanelItem> extends FocusPanel {

    // konstanty
    public static final String DEFAULT_WIDTH = OrderPanel.DEFAULT_BOUNDARY_WIDTH;
    public static final int DEFAULT_HEIGHT_INT = 25;
    public static final String DEFAULT_HEIGHT = DEFAULT_HEIGHT_INT + "px";
    private static final int DEFAULT_ORDERPANEL_WIDTH = 44;
    // komponenty
    private SimplePanel orderPanel;
    private Label label;
    private SimplePanel pUp;
    private SimplePanel pDown;
    // data
    private T object;
    private ConsysAction upAction;
    private ConsysAction downAction;
    private OrderPanel parent;

    public OrderPanelItemUI(T object, OrderPanel parent) {
        this.object = object;
        this.parent = parent;

        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        addStyleName(StyleUtils.OVERFLOW_HIDDEN);

        orderPanel = new SimplePanel();
        orderPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        orderPanel.setWidth(DEFAULT_ORDERPANEL_WIDTH + "px");

        initButtonsActions();
        initButtons();

        FlowPanel panel = new FlowPanel();
        panel.add(orderPanel);
        panel.add(generateContent());
        panel.add(StyleUtils.clearDiv());
        setWidget(panel);
    }

    /** 
     * vygeneruje obsah ktery se vklada vedle posunovacich tlacitek<br>
     * POZOR: musi mit nastaveny float:left a musi se jeste pretizit metoda getHandler()
     */
    protected Widget generateContent() {
        label = StyleUtils.getStyledLabel(object.getName(), StyleUtils.FLOAT_LEFT);
        label.setWidth((OrderPanel.DEFAULT_BOUNDARY_WIDTH_INT - DEFAULT_ORDERPANEL_WIDTH) + "px");
        return label;
    }

    /** vytvori akce, ktera volaji tlacitka pri kliku */
    private void initButtonsActions() {
        upAction = new ConsysAction() {

            @Override
            public void run() {
                parent.moveUp(OrderPanelItemUI.this);
            }
        };
        downAction = new ConsysAction() {

            @Override
            public void run() {
                parent.moveDown(OrderPanelItemUI.this);
            }
        };
    }

    /** vytvori tlacitka pro zmenu poradi */
    private void initButtons() {
        Image u = new Image(ResourceUtils.system().orderUp());
        u.addStyleName(StyleUtils.HAND);
        u.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                upAction.run();
            }
        });

        Image d = new Image(ResourceUtils.system().orderDown());
        d.addStyleName(StyleUtils.HAND);
        d.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                downAction.run();
            }
        });

        pUp = new SimplePanel();
        pUp.addStyleName(StyleUtils.FLOAT_LEFT);
        pUp.addStyleName(StyleUtils.MARGIN_RIGHT_3);
        pUp.setWidget(u);

        pDown = new SimplePanel();
        pDown.addStyleName(StyleUtils.FLOAT_LEFT);
        pDown.setWidget(d);

        FlowPanel p = new FlowPanel();
        p.add(pUp);
        p.add(pDown);
        p.add(StyleUtils.clearDiv());
        orderPanel.setWidget(p);
    }

    /** vraci vlozeny objekt */
    public T getObject() {
        return object;
    }

    /** vraci widget za ktery se ma chytat k pretahovani */
    public Widget getHandler() {
        return label;
    }
}
