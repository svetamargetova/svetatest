package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class DataListCommonFilters extends HeadPanel implements DataListCommonHeadPanel {

    // komponenty
    private FlowPanel filters;
    private ListPagerWidget listPagerWidget;
    // data
    private ArrayList<DataListCommonFiltersItem> allFilters;
    private DataListCommonFiltersItem activeFilter;
    private DataListPanel parentList;

    public DataListCommonFilters() {
        this(true);
    }

    public DataListCommonFilters(boolean showListPager) {
        allFilters = new ArrayList<DataListCommonFiltersItem>();

        FlowPanel flowPanel = new FlowPanel();
        flowPanel.setHeight("25px");

        Label title = new Label(UIMessageUtils.c.dataListCommon_text_show() + ":");
        title.setStyleName(ResourceUtils.system().css().dataListCommonOrdererTitle());
        title.addStyleName(StyleUtils.FLOAT_LEFT);

        filters = new FlowPanel();
        filters.add(title);
        filters.addStyleName(StyleUtils.FLOAT_LEFT);

        listPagerWidget = new ListPagerWidget();

        SimplePanel sp = new SimplePanel();
        sp.setWidget(listPagerWidget);
        sp.addStyleName(StyleUtils.FLOAT_RIGHT);
        sp.setVisible(showListPager);

        flowPanel.add(filters);
        flowPanel.add(sp);
        initWidget(flowPanel);
    }

    @Override
    public ListPagerWidget getListPagerWidget() {
        return listPagerWidget;
    }

    @Override
    public void setParentList(DataListPanel parentList) {
        this.parentList = parentList;
        for (DataListCommonFiltersItem cf : allFilters) {
            cf.setParentList(parentList);
        }
    }

    /** prida filtr do panelu */
    public void addFilter(String title, int tag) {
        addFilter(title, tag, false);
    }

    /** prida filtr do panelu */
    public void addFilter(String title, int tag, boolean selected) {
        final DataListCommonFiltersItem cf = new DataListCommonFiltersItem(tag, title, this);
        cf.addStyleName(StyleUtils.FLOAT_LEFT);
        cf.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                activeFilter = cf;
                activeFilter.select();
                activeFilter.refresh();
            }
        });
        allFilters.add(cf);

        // nastavime aktivny filtr
        if (selected) {
            if (activeFilter != null) {
                activeFilter.deselect();
            }
            activeFilter = cf;
            cf.select();
            clearFilter();
            registerFilter(cf);
        } else {
            cf.deselect();
        }

        if (filters.getWidgetCount() > 1) {
            Label separator = new Label("|");
            separator.setStyleName(ResourceUtils.system().css().dataListCommonOrdererBreak());
            separator.addStyleName(StyleUtils.FLOAT_LEFT);
            filters.add(separator);
        }
        filters.add(cf);
    }

    /** prida selectboxovy filtr */
    public void addSelectFilter(String title, int tag, SelectBox<String> selectBox, boolean selected) {
        final DataListCommonFiltersSelectItem csf = new DataListCommonFiltersSelectItem(tag, selectBox, title, this);
        csf.addStyleName(StyleUtils.FLOAT_LEFT);
        csf.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                activeFilter = csf;
                activeFilter.select();
                activeFilter.refresh();
            }
        });
        allFilters.add(csf);
        if (parentList != null) {
            csf.setParentList(parentList);
        }

        // nastavime aktivny filtr
        if (selected) {
            if (activeFilter != null) {
                activeFilter.deselect();
            }
            activeFilter = csf;
            csf.select();
            clearFilter();
            registerFilter(csf);
        } else {
            csf.deselect();
        }

        if (filters.getWidgetCount() > 1) {
            Label separator = new Label("|");
            separator.setStyleName(ResourceUtils.system().css().dataListCommonOrdererBreak());
            separator.addStyleName(StyleUtils.FLOAT_LEFT);
            filters.add(separator);
        }
        filters.add(csf);
    }
}
