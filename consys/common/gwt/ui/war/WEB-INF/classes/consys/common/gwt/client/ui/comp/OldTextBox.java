package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.ui.comp.input.ConsysTextBoxInterface;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class OldTextBox extends Composite implements ConsysTextBoxInterface {

    // komponenty
    private TextBox textBox;

    public OldTextBox() {
        textBox = StyleUtils.getFormInput();
        initWidget(textBox);
    }

    @Override
    public boolean isEnabled() {
        return textBox.isEnabled();
    }

    @Override
    public void setEnabled(boolean enabled) {
        textBox.setEnabled(enabled);
    }

    @Override
    public void setText(String text) {
        textBox.setText(text);
    }

    @Override
    public String getText() {
        return textBox.getText().trim();
    }
}
