package consys.common.gwt.client.ui.comp.select;


import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import java.util.ArrayList;

/**
 * Casovac pro spusteni vyhledani polozky a nastaveny nalezene hodnoty
 * @author pepa
 */
public abstract class SearchTimer<T> extends Timer {

    private static final Logger logger = LoggerFactory.getLogger(SearchTimer.class);

    // data
    private TextBox input;
    private boolean selectFirst;
    private ArrayList<SelectBoxItem<T>> itemList;
    private ItemBoard board;

    public SearchTimer(TextBox input) {
        this.input = input;
    }

    /** injectovaci metoda nastavi seznam polozek */
    public void setItemList(ArrayList<SelectBoxItem<T>> itemList) {
        this.itemList = itemList;
    }

    /** injectovaci metoda nastavi item board */
    public void setItemBoard(ItemBoard board) {
        this.board = board;
    }

    @Override
    public void run() {
        SelectBoxItem<T> found = searching(input.getText().trim().toLowerCase());
        input.setText("");
        if (board != null) {
            if (found != null) {
                board.setBoardSelectedItem(found);
            } else {
                if (selectFirst && itemList != null && !itemList.isEmpty()) {
                    SelectBoxItem<T> i = itemList.get(0);
                    board.setBoardSelectedItem(i);
                }
            }
        } else {
            logger.error("ItemBoard not set");
        }
    }

    /** projde seznam a pokusi se najit polozku podle zacatku jmena, pokud nenajde vraci null */
    private SelectBoxItem<T> searching(String search) {
        for (SelectBoxItem<T> item : itemList) {
            if (item.getName().toLowerCase().startsWith(search)) {
                return item;
            }
        }
        return null;
    }

    public abstract void setSelectedItem(SelectBoxItem<T> selectedItem);
}
