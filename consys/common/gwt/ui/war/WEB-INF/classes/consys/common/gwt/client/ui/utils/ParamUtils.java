package consys.common.gwt.client.ui.utils;

import com.google.gwt.user.client.Window.Location;
import consys.common.gwt.client.utils.StringUtils;
import java.util.List;
import java.util.Map;

/**
 * Pomocne metody pro praci s GET parametry
 * @author pepa
 */
public class ParamUtils {

    private ParamUtils() {
    }

    /** vraci hodnotu GET parametru se zadanym nazvem */
    public static String getValue(String name) {
        return Location.getParameter(name);
    }

    /** vraci mapu parametru */
    public static Map<String, List<String>> getParameters() {
        return Location.getParameterMap();
    }

    /** pokud je parametr nastaven (je obsazen v GET a ma prirazenou hodnotu), vraci true */
    public static boolean isSet(String name) {
        String value = getValue(name);
        return StringUtils.isNotEmpty(value);
    }

    /** zkontroluje jestli jsou nastaveny vsechny zadane parametry */
    public static boolean isSetAll(String... names) {
        Map<String, List<String>> params = getParameters();

        for (String name : names) {
            // ve values by mela byt vzdy jen jedna polozka
            List<String> values = params.get(name);
            if (values == null || values.isEmpty()) {
                return false;
            }
            // kontrola jestli neni prazdny parametr
            for (String v : values) {
                if (v == null || v.isEmpty()) {
                    return false;
                }
            }
        }

        return true;
    }
}
