package consys.common.gwt.client.ui.comp.action;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 *
 * @author pepa
 */
@ImportedWithPrefix("action")
public interface ActionCss extends CssResource {

    String actionImageBig();

    String actionImageBigContent();

    String actionImageBigLeft();

    String actionImageBigCenter();

    String actionImageBigRight();

    String bigGreen();

    String bigRed();

    String bigBlue();

    String bigClear();

    String actionImage();

    String actionImageContent();

    String actionImageLeft();

    String actionImageCenter();

    String actionImageRight();

    String enabled();

    String disabled();

    String icon();

    String withIcon();

    String iconWrapper();

    String iconBigWrapper();

    String iconConfirm();

    String over();

    String red();
}
