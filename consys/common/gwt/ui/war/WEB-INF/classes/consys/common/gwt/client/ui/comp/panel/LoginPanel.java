package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.user.client.ui.*;
import consys.common.constants.sso.SSO;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstraktni komponentka pro prihlasovani pres sso
 * @author pepa
 */
public abstract class LoginPanel extends Composite implements ActionExecutionDelegate {

    // komponenty
    private FlowPanel mainPanel;
    private SimplePanel failPanel;
    // data
    private static Map<SSO, ConsysAction> actionsMap = new HashMap<SSO, ConsysAction>();

    public LoginPanel() {
        PanelResources.INSTANCE.css().ensureInjected();

        mainPanel = new FlowPanel();
        mainPanel.setStyleName(PanelResources.INSTANCE.css().loginPanel());

        failPanel = new SimplePanel();

        initWidget(mainPanel);
    }

    @Override
    protected void onLoad() {
        mainPanel.clear();
        mainPanel.add(failPanel);
        generate();
    }

    private void generate() {
        mainPanel.add(new Label(UIMessageUtils.c.loginPanel_text_signUpWith() + ":"));
        mainPanel.add(button(SSO.FACEBOOK));
        mainPanel.add(button(SSO.TWITTER));
        mainPanel.add(button(SSO.LINKEDIN));
        mainPanel.add(button(SSO.GOOGLE_PLUS));
        mainPanel.add(StyleUtils.clearDiv("10px"));
        generateAdditional(mainPanel);

        exportSSOLogin();
    }

    /** vygeneruje cast pod tlacitky */
    protected abstract void generateAdditional(FlowPanel panel);

    /** vygeneruje akci pro zadany typ tlacitka */
    protected abstract ConsysAction buttonAction(SSO type);

    private Widget button(SSO type) {
        actionsMap.put(type, buttonAction(type));
        return new LoginPanelButton(type);
    }

    /** vyexportuje funkce aby byly volatelne z javascriptu */
    private static native void exportSSOLogin() /*-{
     $wnd.ssoLoginFACEBOOK = $entry(@consys.common.gwt.client.ui.comp.panel.LoginPanel::runSSOLoginFacebook());
     $wnd.ssoLoginGOOGLE_PLUS = $entry(@consys.common.gwt.client.ui.comp.panel.LoginPanel::runSSOLoginGooglePlus());
     $wnd.ssoLoginLINKEDIN = $entry(@consys.common.gwt.client.ui.comp.panel.LoginPanel::runSSOLoginLinkedIn());
     $wnd.ssoLoginTWITTER = $entry(@consys.common.gwt.client.ui.comp.panel.LoginPanel::runSSOLoginTwitter());
     }-*/;

    public static void runSSOLoginFacebook() {
        actionsMap.get(SSO.FACEBOOK).run();
    }

    public static void runSSOLoginGooglePlus() {
        actionsMap.get(SSO.GOOGLE_PLUS).run();
    }

    public static void runSSOLoginLinkedIn() {
        actionsMap.get(SSO.LINKEDIN).run();
    }

    public static void runSSOLoginTwitter() {
        actionsMap.get(SSO.TWITTER).run();
    }

    @Override
    public void actionStarted() {
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    protected ConsysMessage getFailMessage() {
        return FormUtils.failMessage(failPanel, StyleUtils.MARGIN_TOP_10);
    }
}
