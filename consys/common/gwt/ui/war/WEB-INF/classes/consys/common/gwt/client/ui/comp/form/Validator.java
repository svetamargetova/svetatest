package consys.common.gwt.client.ui.comp.form;

/**
 * Interface pro validovani konkretniho pole formulare
 * @author pepa
 */
public interface Validator<T> {

    /** vraci chybovou zpravu, pokud je vse v poradku, vraci null */
    String validate(T value);
          
}
