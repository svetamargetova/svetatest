package consys.common.gwt.client.ui;


import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.event.UserLogoutEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.base.LoginSuccessContent;
import consys.common.gwt.client.ui.event.UriFragmentTokenEvent;
import java.util.HashMap;

/**
 * Zpracovava specialni pripady hodnot v history tokenu
 * @author pepa
 */
public class CommonHistoryHandler implements UserLogoutEvent.Handler, UriFragmentTokenEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(CommonHistoryHandler.class);

    // instance
    private static CommonHistoryHandler instance;
    // konstanty
    public static final String LOGIN_SUCCESS = "LoginSuccess";
    // data
    private HashMap<String, CommonHistoryItem> loggedOutTokenMap;
    private HashMap<String, CommonHistoryItem> loggedInTokenMap;

    private CommonHistoryHandler() {        
        loggedOutTokenMap = new HashMap<String, CommonHistoryItem>();
        loggedInTokenMap = new HashMap<String, CommonHistoryItem>();
        registerCommonToken();
        EventBus.get().addHandler(UserLogoutEvent.TYPE, this);
        EventBus.get().addHandler(UriFragmentTokenEvent.TYPE, this);
    }

    /** vraci instanci CommonHistoryHandler */
    public static CommonHistoryHandler get() {
        if (instance == null) {
            instance = new CommonHistoryHandler();
        }
        return instance;
    }

    /** zaregistruje vychozi polozky z UI */
    private void registerCommonToken() {
        registerOutToken(LOGIN_SUCCESS, new CommonHistoryItem() {

            @Override
            public Widget createInstace() {
                LoginSuccessContent.get().runEvents();
                return null;
            }
        });
    }

    /** zaregistruje polozku do handleru */
    public void registerOutToken(String token, CommonHistoryItem item) {
        loggedOutTokenMap.put(token.toLowerCase(), item);
    }

    public void registerLoggedInToken(String token, CommonHistoryItem item) {
        loggedInTokenMap.put(token.toLowerCase(), item);
    }
   
    @Override
    public void onUserLogout(UserLogoutEvent event) {
        // pri odhlaseni se zrusi handlery urcene jen pro prihlasene uzivatele        
        loggedInTokenMap = new HashMap<String, CommonHistoryItem>();
    }

    @Override
    public void onCatchedFragment(UriFragmentTokenEvent event) {
        String token = event.getFragment();
        int index = token.indexOf("?");
        if (index > -1) {
            token = token.substring(0, index);
        }
        token = token.toLowerCase();
        CommonHistoryItem item = loggedOutTokenMap.get(token);
        CommonHistoryItem loggedInItem = loggedInTokenMap.get(token);

        if (item == null) {
            item = loggedInItem;
        }
        if (item != null) {
            logger.debug("has content for token "+event.getFragment());
            event.setHandled(true);            
            Widget widget = item.createInstace();
            ChangeContentEvent contentEvent = new ChangeContentEvent(widget, token);
            EventBus.get().fireEvent(contentEvent);
        }else{
            logger.debug("no registred content for token "+event.getFragment());
        }
    }
}
