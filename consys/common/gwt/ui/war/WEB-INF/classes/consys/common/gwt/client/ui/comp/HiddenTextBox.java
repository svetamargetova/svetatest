package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.TextBoxBase;

/**
 * Input pro skryte odesilani dat ve formulari
 * @author pepa
 */
public class HiddenTextBox extends TextBoxBase {

    public HiddenTextBox(Object o, String name) {
        super(DOM.createElement("input"));
        getElement().setAttribute("type", "hidden");
        getElement().setAttribute("name", name);
        setValue(String.valueOf(o));
    }
}
