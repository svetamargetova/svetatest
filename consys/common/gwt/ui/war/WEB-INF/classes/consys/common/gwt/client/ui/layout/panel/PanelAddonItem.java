package consys.common.gwt.client.ui.layout.panel;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.ClickFlowPanel;
import consys.common.gwt.client.ui.comp.RollAnimation;
import consys.common.gwt.client.ui.event.UriFragmentTokenEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * Abstraktni trida polozky v pridavnem panelu
 *
 * @author pepa
 */
public class PanelAddonItem extends PanelItem {

    private static final Logger logger = LoggerFactory.getLogger(PanelAddonItem.class);
    // komponenty
    private PanelAddonItemPanel container;
    private FlowPanel wrapper;
    // data
    private String titleText;
    private ConsysAction action;
    private AddonRollAnimation animation;

    public PanelAddonItem(String titleText) {
        this(titleText, null);
    }

    public PanelAddonItem(String titleText, ConsysAction action) {
        super(false);
        this.titleText = titleText;
        this.action = action;
        container = new PanelAddonItemPanel(true);

        wrapper = new FlowPanel();
        wrapper.add(container);
        wrapper.setVisible(false);
    }

    @Override
    public String getName() {
        return titleText;
    }

    @Override
    public Widget getContent() {
        return wrapper;
    }

    public void addSubItem(String name, String urlName, ConsysAction action) {
        PanelAddonSubItem subitem = new PanelAddonSubItem(name, urlName, action);
        container.addChild(subitem);
    }

    @Override
    public Widget getTitle() {
        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ResourceUtils.system().css().titleWrapper());

        if (action != null) {
            ClickFlowPanel adminButton = new ClickFlowPanel();
            adminButton.setStyleName(ResourceUtils.system().css().admin());
            adminButton.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    action.run();
                }
            });
            Image gear = new Image(ResourceUtils.system().blackGear());
            adminButton.add(gear);
            panel.add(adminButton);
        }

        ClickFlowPanel titlePanel = new ClickFlowPanel();
        if (action != null) {
            titlePanel.setWidth("195px");
        }
        titlePanel.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (animation == null) {
                    animation = new AddonRollAnimation();
                }
                if (wrapper.isVisible()) {
                    animation.rollWidget(container, wrapper, true);
                } else {
                    animation.rollWidget(container, wrapper, false);
                }
            }
        });

        Label text = new Label(titleText);
        text.setStyleName(ResourceUtils.system().css().title());
        titlePanel.add(text);
        panel.add(titlePanel);

        return panel;
    }

    /**
     * Rozbali panel
     */
    public void showContent() {
        if (!wrapper.isVisible()) {
            if (animation == null) {
                animation = new AddonRollAnimation();
            }
            animation.rollWidget(container, wrapper, true);
        }
    }

    /**
     * Prevolanie tohto interface sa vykonava z {@link LayoutPanel}
     *
     * @param event
     */
    @Override
    public void onCatchedFragment(UriFragmentTokenEvent event) {
        if (event.isHandled()) {
            return;
        }
        logger.info("[" + titleText + "] Check for uri fragment " + event.getFragment());
        PanelAddonSubItem item = container.findChild(event.getFragment());
        if (item != null) {
            LoggerFactory.log(PanelAddonItem.class, "Found item: " + event.getFragment());
            showContent();
            item.showItem();
            event.setHandled(true);
        }
    }

    private class AddonRollAnimation extends RollAnimation {

        @Override
        protected void onComplete() {
            if (down) {
                UIObject.setVisible(element, false);
            }
            element.getStyle().clearOverflow();
            element.getStyle().setHeight(height, Unit.PX);

            child = null;
            element = null;
        }
    }
}
