package consys.common.gwt.client.ui.comp.panel.element;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.PopupHelp;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;

/**
 * Label pro ConsysTabPanel, umoznuje automaticky refreshovat zobrazeni posuvniku
 * @author pepa
 */
public class ConsysTabPanelLabel<T> extends Label {

    // konstanty
    private static final int MAX_CHAR_SHOW = 20;
    // data
    private ConsysTabPanel tab;
    private String fullText;
    private HandlerRegistration mouseOver;
    private HandlerRegistration mouseOut;
    private T id;

    public ConsysTabPanelLabel(T id, String text) {
        this.id = id;
        process(text);
    }

    /** vraci panel ke kteremu byl label prirazen */
    public ConsysTabPanel getTab() {
        return tab;
    }

    /** nastaveni tab panelu, ke kteremu label patri (nastavuje si ConsysTabPanel sam) */
    public void setTab(ConsysTabPanel tab) {
        this.tab = tab;
    }

    @Override
    public void setText(String text) {
        process(text);
        if (tab != null) {
            tab.refreshScroll();
        }
    }

    /** zpracuje dlouhy text */
    private void process(String text) {
        fullText = text;
        boolean shorted = text.length() > (MAX_CHAR_SHOW + 1);
        if (shorted) {
            text = text.substring(0, MAX_CHAR_SHOW) + " ...";
        }
        refreshHandlers(shorted);
        super.setText(text);
    }

    /** vraci plny nazev zalozky */
    public String getFullText() {
        return fullText;
    }

    /** zaktualizuje handlery v zavislosti na tom, jestli byl text zkrácen */
    private void refreshHandlers(boolean shorted) {
        removePreviousHandlers();
        if (shorted) {
            final PopupHelp pup = new PopupHelp(fullText, ConsysTabPanelLabel.this);
            mouseOver = addMouseOverHandler(new MouseOverHandler() {

                @Override
                public void onMouseOver(MouseOverEvent event) {
                    pup.show();
                }
            });
            mouseOut = addMouseOutHandler(new MouseOutHandler() {

                @Override
                public void onMouseOut(MouseOutEvent event) {
                    pup.hide();
                }
            });
        }
    }

    /** odstratni predchozi zaregistrovane handlery */
    private void removePreviousHandlers() {
        if (mouseOver != null) {
            mouseOver.removeHandler();
            mouseOver = null;
        }
        if (mouseOut != null) {
            mouseOut.removeHandler();
            mouseOut = null;
        }
    }

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
