package consys.common.gwt.client.ui.utils;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Obsahuje zakladni metody pro validaci obsahu
 * @author pepa
 */
public class ValidatorUtils {

    // konstanty
    public static final String EMAIL_REG_EXP = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

    /** 
     * kontroluje zda text splnuje zadanou delku
     * @return 0 pokud je text ve stanovenem rozmezi
     * @return zaporny int pokud je hodnota kraci (hodnota udava o kolik)
     * @return kladny int pokud je hodnota delsi (hodnota udava o kolik)
     */
    public static int isLenght(String text, int minLength, int maxLength) {
        int length = text.length();
        if (length < minLength) {
            return length - minLength;
        }
        if (length > maxLength) {
            return length - maxLength;
        }
        return 0;
    }

    /** @return true pokud je cislo v rozsahu (v rozsahu je i kdyz se rovna meznim hodnotam) */
    public static boolean isRange(int number, int min, int max) {
        return (number >= min && number <= max);
    }

    /** @return true pokud je cislo v rozsahu (v rozsahu je i kdyz se rovna meznim hodnotam) */
    public static boolean isRange(double number, double min, double max) {
        return (number >= min && number <= max);
    }

    /** kontroluje zda uvedeny text je platna emailova adresa */
    public static boolean isValidEmailAddress(String email) {
        return (isValidString(email) && email.toLowerCase().matches(EMAIL_REG_EXP));
    }

    /** kontroluje zda hodnota textu neni null a že je text naplněn */
    public static boolean isValidString(String text) {
        return (text != null && text.trim().length() > 0);
    }

    /** povoluje do TextBox napsat jen cislice */
    public static KeyUpHandler onlyNumberKeyUpHandler(final TextBox box) {
        return new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                String text = box.getText();
                if (!text.matches("[0-9]*")) {
                    StringBuffer result = new StringBuffer();
                    char c;
                    for (int i = 0; i < text.length(); i++) {
                        c = text.charAt(i);
                        if (!Character.isDigit(c)) {
                            break;
                        }
                        result.append(c);
                    }
                    box.setText(result.toString());
                }
            }
        };
    }
}
