package consys.common.gwt.client.ui.comp.form;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Zakldna kostra formulara. Obsahuje vsetky zdedene metody ktore neimplementuje.
 * @author palo
 */
public class BaseForm extends Composite implements Form {

    public BaseForm() {        
        FormResources.INSTANCE.formCss().ensureInjected();
        initWidget(new FlowPanel());
        getWidget().setStyleName(FormResources.INSTANCE.formCss().form());
    }
    
    protected void add(Widget w){
        ((FlowPanel)getWidget()).add(w);
    }

    @Override
    public FormElement add(String title, String value, String defaultValue, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FormElement add(String title, Widget value, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FormElement add(String title, String value, String defaultValue, String help, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FormElement add(String title, Widget value, String help, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FormElement add(Widget titleWidget, Widget value, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FormElement add(Widget titleWidget, Widget value, String help, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FormElement addValueWidget(Widget value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /*@Override
    public FormElement add(String title, Validable value, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FormElement add(String title, Validable value, String help, boolean required) {
        throw new UnsupportedOperationException("Not supported yet.");
    }*/

    @Override
    public boolean validate() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
