package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import java.util.ArrayList;

/**
 * vlastni komponenta CheckBoxu, rozmery 13x14
 * @author pepa
 */
public class ConsysCheckBox extends Composite implements ChangeValueEvent.Handler<Boolean> {

    // konstanty
    private static final String WIDTH = "13px";
    private static final String HEIGHT = "14px";
    // data
    private FocusPanel content;
    private Image checkOn;
    private Image checkOnDis;
    private Image checkOff;
    private Image checkOffDis;
    private boolean isChecked = false;
    private boolean enabled;
    // seznam zaregistrovanych handleru
    private ArrayList<ChangeValueEvent.Handler<Boolean>> changeValueHandlers;

    public ConsysCheckBox() {
        this(true);
    }

    public ConsysCheckBox(boolean enabled) {
        this.enabled = enabled;
        changeValueHandlers = new ArrayList<ChangeValueEvent.Handler<Boolean>>();

        content = new FocusPanel();
        content.setSize(WIDTH, HEIGHT);

        if (enabled) {
            checkOff = new Image(ResourceUtils.system().checkOff());
            content.setWidget(checkOff);
        } else {
            checkOffDis = new Image(ResourceUtils.system().checkOffDis());
            content.setWidget(checkOffDis);
        }

        sinkEvents(Event.ONCLICK);
        initHandlers();

        initWidget(content);
    }

    /** zinicializuje zakladni handlery */
    private void initHandlers() {
        addHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (enabled) {
                    setValue(!getValue());
                }
            }
        }, ClickEvent.getType());
        content.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (enabled) {
                    if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                        setValue(!getValue());
                    }
                }
            }
        });
    }

    /** nastavi focus */
    public void setFocus(boolean value) {
        content.setFocus(value);
    }

    /** @return true pokud je polozka aktivovana */
    public boolean isEnabled() {
        return enabled;
    }

    /** enabluje / disabluje polozku */
    public void setEnabled(boolean value) {
        enabled = value;
        changeImage(getValue());
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(ChangeValueEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(ChangeValueEvent.TYPE, this);
    }

    /** vraci aktualni hodnotu */
    public Boolean getValue() {
        return isChecked;
    }

    /** nastavuje novou hodnotu */
    public void setValue(Boolean value) {
        isChecked = value;
        changeImage(value);

        ChangeValueEvent<Boolean> event = new ChangeValueEvent<Boolean>(this, !value, value);
        EventBus.get().fireEvent(event);
    }

    /** nastavi obrazek checkboxu podle jeho hodnoty */
    private void changeImage(boolean value) {
        if (value) {
            if (enabled) {
                if (checkOn == null) {
                    checkOn = new Image(ResourceUtils.system().checkOn());
                }
                content.setWidget(checkOn);
            } else {
                if (checkOnDis == null) {
                    checkOnDis = new Image(ResourceUtils.system().checkOnDis());
                }
                content.setWidget(checkOnDis);
            }
        } else {
            if (enabled) {
                if (checkOff == null) {
                    checkOff = new Image(ResourceUtils.system().checkOff());
                }
                content.setWidget(checkOff);
            } else {
                if (checkOffDis == null) {
                    checkOffDis = new Image(ResourceUtils.system().checkOffDis());
                }
                content.setWidget(checkOffDis);
            }
        }
    }

    /** zaregistruje handler */
    public void addChangeValueHandler(final ChangeValueEvent.Handler<Boolean> handler) {
        changeValueHandlers.add(handler);
    }

    @Override
    public void onChangeValue(ChangeValueEvent<Boolean> event) {
        if (enabled) {
            if (event.getComponent().equals(this)) {
                for (ChangeValueEvent.Handler handler : changeValueHandlers) {
                    handler.onChangeValue(event);
                }
            }
        }
    }
}
