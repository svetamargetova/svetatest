package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ConsysFormPanel extends FormPanel implements ValidableComponent {

    ArrayList<ValidableComponent> validatingComponents = new ArrayList<ValidableComponent>();

    @Override
    public void add(Widget w) {
        super.add(w);
        if (w instanceof ValidableComponent) {
            validatingComponents.add((ValidableComponent) w);
        }
    }

    @Override
    public void clear() {
        super.clear();
        validatingComponents.clear();
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        boolean result = true;
        for (ValidableComponent comp : validatingComponents) {
            if (comp.doValidate(fail)) {
                comp.onSuccess();
            } else {
                comp.onFail();
                result = false;
            }
        }
        return result;
    }

    @Override
    public void onSuccess() {
        for (ValidableComponent vc : validatingComponents) {
            vc.onSuccess();
        }
    }

    @Override
    public void onFail() {
        for (ValidableComponent vc : validatingComponents) {
            vc.onFail();
        }
    }
}
