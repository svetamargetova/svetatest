package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 * Focusovatelne obrazkove tlacitko velke, bez ikony
 * @author pepa
 */
public class ActionImageBig extends Composite {

    // konstanty
    public static final int DEFAULT_WIDTH = 100;
    public static final String DEFAULT_WIDTH_PX = DEFAULT_WIDTH + "px";
    // komponenty
    private FocusPanel focusPanel;
    private Image left;
    private Image right;
    private FlowPanel leftWrapper;
    private FlowPanel rightWrapper;
    // data
    private ArrayList<ClickHandler> clickHandlers;
    private Timer enableTimer;
    private boolean temporarilyDisabled;
    private boolean enabled;
    private String baseStyle;

    public ActionImageBig(String text, ActionImageBigEnum type) {
        clickHandlers = new ArrayList<ClickHandler>();
        enabled = true;

        baseStyle = "";
        left = new Image();
        right = new Image();
        switch (type) {
            case GREEN:
                baseStyle = ResourceUtils.system().css().bigGreenB();
                left.setResource(ResourceUtils.system().bigGreenBL());
                right.setResource(ResourceUtils.system().bigGreenBR());
                break;
            case RED:
                baseStyle = ResourceUtils.system().css().bigRedB();
                left.setResource(ResourceUtils.system().bigRedBL());
                right.setResource(ResourceUtils.system().bigRedBR());
                break;
            case BLUE:
                baseStyle = ResourceUtils.system().css().bigBlueB();
                left.setResource(ResourceUtils.system().bigBlueBL());
                right.setResource(ResourceUtils.system().bigBlueBR());
                break;
        }

        leftWrapper = new FlowPanel();
        leftWrapper.add(left);
        leftWrapper.setStyleName(StyleUtils.FLOAT_LEFT);
        focusPanel = new FocusPanel();
        focusPanel.setWidth((DEFAULT_WIDTH - 6) + "px");
        focusPanel.setStyleName(baseStyle);
        focusPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        focusPanel.addStyleName(StyleUtils.HAND);
        rightWrapper = new FlowPanel();
        rightWrapper.add(right);
        rightWrapper.setStyleName(StyleUtils.FLOAT_LEFT);

        Label label = StyleUtils.getStyledLabel(text,
                StyleUtils.FONT_16PX, StyleUtils.TEXT_WHITE, StyleUtils.FONT_BOLD, StyleUtils.MARGIN_TOP_5);
        label.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        focusPanel.add(label);

        FlowPanel panel = new FlowPanel();
        panel.add(leftWrapper);
        panel.add(focusPanel);
        panel.add(rightWrapper);

        sinkEvents(Event.ONCLICK);
        initWidget(panel);

        addMyHandler(myClickkHandler());
    }

    /** vraci vytvorene zelene tlacitko s textem Accept */
    public static ActionImageBig acceptButton() {
        return new ActionImageBig(UIMessageUtils.c.const_accept(), ActionImageBigEnum.GREEN);
    }

    /** vraci vytvorene cervene tlacitko s textem Decline */
    public static ActionImageBig declineButton() {
        return new ActionImageBig(UIMessageUtils.c.const_decline(), ActionImageBigEnum.RED);
    }

    /** vraci vytvorene modre tlacitko s textem Save */
    public static ActionImageBig saveButton() {
        return new ActionImageBig(UIMessageUtils.c.const_save(), ActionImageBigEnum.BLUE);
    }

    /** zaregistruje zadany ClickHandler */
    public void addClickHandler(ClickHandler handler) {
        clickHandlers.add(handler);
    }

    /** prida interne handler */
    private void addMyHandler(final ClickHandler handler) {
        focusPanel.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    handler.onClick(null);
                }
            }
        });
        addHandler(handler, ClickEvent.getType());
    }

    /** vytvori interni ClickHandler, ktery se stara o spousteni ostatnich */
    private ClickHandler myClickkHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (enabled && !temporarilyDisabled) {
                    temporarilyDisabled = true;
                    for (ClickHandler handler : clickHandlers) {
                        handler.onClick(event);
                    }
                    enableTimer.schedule(ActionImage.TEMPORARILY_DISABLE_DELAY);
                }
            }
        };
    }

    /** casovac ktery vypne docasne vypnuti zneaktivovani tlacitka */
    private Timer enableTimer() {
        return new Timer() {
            @Override
            public void run() {
                temporarilyDisabled = false;
            }
        };
    }

    /** vraci true pokud je tlacitko zapnuto */
    public boolean isEnabled() {
        return enabled;
    }

    /** enabluje nebo disabluje tlacitko */
    public void setEnabled(boolean enabled) {
        if (enabled == this.enabled) {
            return;
        }
        if (enabled) {
            focusPanel.addStyleName(baseStyle);
            focusPanel.addStyleName(StyleUtils.HAND);
            focusPanel.removeStyleName(ResourceUtils.system().css().bigDisabledB());

            leftWrapper.clear();
            leftWrapper.add(left);
            rightWrapper.clear();
            rightWrapper.add(right);
        } else {
            focusPanel.addStyleName(ResourceUtils.system().css().bigDisabledB());
            focusPanel.removeStyleName(baseStyle);
            focusPanel.removeStyleName(StyleUtils.HAND);

            leftWrapper.clear();
            leftWrapper.add(new Image(ResourceUtils.system().bigDisabledBL()));
            rightWrapper.clear();
            rightWrapper.add(new Image(ResourceUtils.system().bigDisabledBR()));
        }
        this.enabled = enabled;
    }

    /** enum  moznych velkych tlacitek */
    public enum ActionImageBigEnum {

        GREEN, RED, BLUE;
        private static final long serialVersionUID = 3596704493125678557L;
    }
}
