package consys.common.gwt.client.ui.utils;

import consys.common.gwt.client.ui.comp.ConsysMessage;

/**
 * Pomocna udelatka pro praci se servlety z klienta
 * @author pepa
 */
public class ServletUtils {

    /** regularni vyraz pro hledani tagu */
    public final static String TAG_MATCH = "\\<.*?\\>";
    public final static String TAG_MATCH_CODE = "&lt;.*?&gt;";
    // cisla http chyb
    private static final String HTTP_TOMCAT_ERROR = "HTTP Status";
    private static final String HTTP_ERROR = "HTTP ERROR: ";
    public static final String HTTP_BAD_REQUEST = HTTP_ERROR + "400";
    public static final String HTTP_UNAUTHORIZED = HTTP_ERROR + "401";
    public static final String HTTP_FORBIDDEN = HTTP_ERROR + "403";
    public static final String HTTP_NOT_FOUND = HTTP_ERROR + "404";
    public static final String HTTP_NOT_ACCEPTABLE = HTTP_ERROR + "406";
    public static final String HTTP_REQUEST_ENTITY_TOO_LARGE = HTTP_ERROR + "413";
    public static final String HTTP_UNSUPPORTED_MEDIA_TYPE = HTTP_ERROR + "415";
    public static final String HTTP_INTERNAL_SERVER_ERROR = HTTP_ERROR + "500";

    /** vracti true pokud se nachazi chybovy retezec ve stringovem parametru */
    public static boolean containHttpFail(String msg, ConsysMessage fail) {
        boolean result = false;
        if (msg != null && msg.length() > 0) {
            if (msg.contains(HTTP_BAD_REQUEST)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http400());
                result = true;
            } else if (msg.contains(HTTP_UNAUTHORIZED)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http401());
                result = true;
            } else if (msg.contains(HTTP_FORBIDDEN)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http403());
                result = true;
            } else if (msg.contains(HTTP_NOT_FOUND)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http404());
                result = true;
            } else if (msg.contains(HTTP_NOT_ACCEPTABLE)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http406());
                result = true;
            } else if (msg.contains(HTTP_REQUEST_ENTITY_TOO_LARGE)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http413());
                result = true;
            } else if (msg.contains(HTTP_UNSUPPORTED_MEDIA_TYPE)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http415());
                result = true;
            } else if (msg.contains(HTTP_INTERNAL_SERVER_ERROR)) {
                fail.setText(UIMessageUtils.c.servletUtils_error_http500());
                result = true;
            }

            if (msg.contains(HTTP_TOMCAT_ERROR)) {
                result = true;
            }
        }
        return result;
    }
}
