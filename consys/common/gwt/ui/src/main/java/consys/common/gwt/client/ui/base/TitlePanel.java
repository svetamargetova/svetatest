package consys.common.gwt.client.ui.base;

/**
 * Rozhranie ktore definuje ze panel alebo iny obsah ma titulku
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface TitlePanel {

    public String getPanelTitle();

}
