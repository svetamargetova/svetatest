package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.panel.OrderPanel;
import consys.common.gwt.client.ui.comp.panel.OrderPanel.OrderPanelListener;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.OrderPanelItem;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class OrderDialog<T extends OrderPanelItem> extends SmartDialog {

    // komponenty
    private OrderPanel orderPanel;

    public OrderDialog(ArrayList<T> itemList, OrderPanelListener<T> listener) {
        orderPanel = new OrderPanel(itemList);
        orderPanel.addListener(listener);
    }

    @Override
    protected void generateContent(SimpleFormPanel panel) {
        Image close = new Image(ResourceUtils.system().removeCross());
        close.addStyleName(StyleUtils.HAND);
        close.addClickHandler(hideClickHandler());

        SimplePanel closePanel = new SimplePanel();
        closePanel.addStyleName(StyleUtils.FLOAT_RIGHT);
        closePanel.setWidth("30px");
        closePanel.add(close);

        panel.addWidget(getPaddingDiv("15px"));
        panel.addWidget(closePanel);
        panel.addWidget(StyleUtils.clearDiv());
        panel.addWidget(orderPanel);
        panel.addWidget(getPaddingDiv("15px"));
    }

    /** vycpávkový div */
    private FlowPanel getPaddingDiv(String height) {
        FlowPanel div = new FlowPanel();
        div.setSize("10px", height);
        return div;
    }

    /** vraci orderpanel */
    public OrderPanel getOrderPanel() {
        return orderPanel;
    }
}
