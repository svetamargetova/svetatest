package consys.common.gwt.client.ui.comp.form;

import consys.common.gwt.client.ui.comp.input.Validable;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;

/**
 * Validovatelny panel, ve kterem lze urcit, ktere podcasti se maji validovat
 * @author pepa
 */
public class ValidablePanel extends Composite implements Validable {

    // komponenty
    private FlowPanel panel;
    // data
    protected ArrayList<Validable> validateItems;
    

    public ValidablePanel() {
        validateItems = new ArrayList<Validable>();

        panel = new FlowPanel();
        initWidget(panel);
    }

    /** prida polozku do panelu */
    public void add(Widget widget) {
        add(widget, false);
    }

    /** prida polozku do panelu a v pripadu, ze je polozka validovatelna a validate je nastaveno na true, bude se validovat */
    public void add(Widget widget, boolean validate) {
        panel.add(widget);
        if (validate && (widget instanceof Validable)) {
            validateItems.add((Validable) widget);
        }
    }

    /** vymaze obsah panelu */
    public void clear() {
        panel.clear();
        validateItems.clear();
    }

    @Override
    public String validate() {
        StringBuilder sb = null;        
        for (Validable v : validateItems) {
            String msg = v.validate();            
            if(msg != null){
                if(sb == null){
                    sb = new StringBuilder(msg);
                }else{
                    sb.append(" ").append(msg);
                }                
            }
        }
        return sb == null ? null : sb.toString();
    }  
}
