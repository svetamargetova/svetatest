package consys.common.gwt.client.ui.utils;

import com.google.gwt.core.client.GWT;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.message.ConsysConstants;
import consys.common.gwt.client.ui.message.ConsysMessages;
import consys.common.constants.currency.CurrencyEnum;
import consys.common.gwt.shared.bo.Monetary;
import java.util.ArrayList;

/**
 * Zpravy a textove konstanty pouzivane v UI
 * @author pepa
 */
public class UIMessageUtils {

    /** systemove konstanty modulu ui */
    public static final ConsysConstants c = (ConsysConstants) GWT.create(ConsysConstants.class);
    /** systemove zpravy modulu ui */
    public static final ConsysMessages m = (ConsysMessages) GWT.create(ConsysMessages.class);

    /** vraci zpravu v zavislosti na hodnote CommonExceptionEnum */
    public static String getFailMessage(CommonExceptionEnum value) {
        switch (value) {
            case BAD_INPUT:
                return c.const_exceptionBadInput();
            case SERVICE_FAILED:
                return c.const_exceptionServiceFailed();
            case NOT_PERMITTED:
                return c.const_exceptionNotPermittedAction();
            default:
                return "";
        }
    }

    /** vraci nove vytvoreny seznam s polozkama pro selectbox */
    public static ArrayList<SelectBoxItem<String>> currencyItems() {
        ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
        items.add(new SelectBoxItem<String>(CurrencyEnum.EUR.toString()));
        items.add(new SelectBoxItem<String>(CurrencyEnum.USD.toString()));
        items.add(new SelectBoxItem<String>(CurrencyEnum.CZK.toString()));
        return items;
    }

    /** priradi menu k hodnote */
    public static String assignCurrency(CurrencyEnum currency, Monetary value) {
        String price = FormUtils.doubleFormat.format(Double.parseDouble(value.toString()));
        return assignCurrency(currency, price);
    }

    public static String assignCurrency(CurrencyEnum currency, String price) {        
        switch (currency) {
            case CZK:
                return price + " Kč";
            case EUR:
                return "€ " + price;
            case USD:
                return "$ " + price;
            default:
                return currency + " " + price;
        }
    }

    public static String assignCurrency(String currency, String price) {
        CurrencyEnum curr;
        try {
            curr = CurrencyEnum.toEnum(currency);
        } catch (IllegalArgumentException ex) {
            return currency + " " + price;
        }
        return assignCurrency(curr, price);
    }
    
    /** priradi menu k hodnote */
    public static String assignCurrency(String currency, Monetary value) {
        if(value.isZero()) {
            return c.const_freeUpper();
        }
        String price = FormUtils.doubleFormat.format(Double.parseDouble(value.toString()));
        return assignCurrency(currency,price);
    }

    /** podle hodnoty booleanu v parametru vraci lokalizovany text Yes nebo No */
    public static String yesNo(boolean yes) {
        return yes ? c.const_yes() : c.const_no();
    }
}
