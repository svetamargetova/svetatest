package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Chytrejsi dialog s vyresenym waitingem a getFailMessage
 * @author pepa
 */
public abstract class SmartDialog extends Dialog implements ActionExecutionDelegate {

    // komponenty
    private FlowPanel mainPanel;
    protected SimpleFormPanel panel;
    // waiting
    private Waiting waiting;
    // data
    private int width;
    private boolean withCloseCross;

    public SmartDialog() {
        this.width = Dialog.DEFAULT_WIDTH;
        init();
    }

    public SmartDialog(int width) {
        this(width, false);
    }

    public SmartDialog(int width, boolean withCloseCross) {
        super(width);
        this.width = width;
        this.withCloseCross = withCloseCross;
        init();
    }

    private void init() {
        panel = new SimpleFormPanel(width + "px");

        mainPanel = new FlowPanel();
        mainPanel.setWidth(width + "px");
        waiting = new Waiting(mainPanel);

        mainPanel.add(waiting);
        if (withCloseCross) {
            mainPanel.add(close());
            mainPanel.add(StyleUtils.clearDiv());
        }
        mainPanel.add(panel);
    }

    /** vytvori widget s krizkem floatovanym doprava */
    private Widget close() {
        Image closeCross = new Image(ResourceUtils.system().dialogClose());
        closeCross.setStyleName(ResourceUtils.css().smartDialogCloseImage());

        FocusPanel close = new FocusPanel();
        close.setStyleName(ResourceUtils.css().smartDialogClose());
        close.setWidget(closeCross);
        close.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });
        return close;
    }

    @Override
    protected Widget createContent() {
        panel.clear();
        generateContent(panel);
        return mainPanel;
    }

    /** vytvori obsah pro simpleFormPanel predany v parametru */
    protected abstract void generateContent(SimpleFormPanel panel);

    @Override
    public void actionStarted() {
        waiting.show(mainPanel);
    }

    @Override
    public void actionEnds() {
        waiting.hide();
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    public ConsysMessage getFailMessage() {
        ConsysMessage fail = panel.getFailMessage();
        fail.setWidth(width - 96);
        fail.addStyleName(StyleUtils.ALIGN_CENTER);
        fail.addStyleName(StyleUtils.MARGIN_CENTER);
        DOM.setStyleAttribute(fail.getParent().getElement(), "textAlign", "center");
        return fail;
    }

    protected ClickHandler hideClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        };
    }
}
