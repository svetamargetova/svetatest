package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
@CssResource.ImportedWithPrefix("user")
public interface UserCss extends CssResource {

           
    /** User List */
    String userListItemPortrait();

    String userListItemName();

    String userListEditItemName();

    String userListEditItemAction();

    /** User Label */
    String userLabel();
    
    /** User Label List */
    String userLabelListItem();

    String userLabelListEditItemName();

    String userLabelListEditItemAction();

    /** User Search Dialog */        
    String userSearchDialogBody();
    
    String userSearchDialogTextBox();

    String userSearchDialogResultScrollPanel();

    String userSearchDialogHelp();
    
    /** User Inline List Panel */
    String userInlineListPanel();
    
    /** User Card */
    String userCard();
    
    String userCardContent();
    
    String userCardContentControlls();
    
    String userCardContentPortrait();
    
    String userCardContentUserInfo();
    
    
    
}
