package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.panel.ListPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import consys.common.gwt.shared.bo.EventUser;
import java.util.ArrayList;

/**
 *
 * Uzivatelsky seznam s fotkama a afiliacema.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserLabelListPanel extends ListPanel<UserLabelListItem> {

    private static final int VISIBLE_ITEMS = 5;

    public UserLabelListPanel() {
        super(UserLabelListItem.HEIGHT, VISIBLE_ITEMS);
    }

    public ArrayList<ClientUserThumb> getUsers() {
        ArrayList<ClientUserThumb> users = new ArrayList<ClientUserThumb>();
        for (UserLabelListItem item : getItems()) {
            ClientUserThumb t = new ClientUserThumb();
            t.setName(item.getObject().getName());
            t.setUuid(item.getObject().getUuid());
            t.setUuidPortraitImagePrefix(item.getObject().getUuidPortraitImagePrefix());
            users.add(t);
        }
        return users;
    }

    public void setUsers(ArrayList<String> usersUuids) {
        clear();
        // TODO load current event
        if (!(usersUuids == null || usersUuids.size() == 0)) {
            ArrayList<String> list = new ArrayList<String>(usersUuids);
            EventBus.get().fireEvent(new DispatchEvent(new ListUserAffiliationThumbsAction(list, ""), new AsyncCallback<ArrayListResult<ClientUserWithAffiliationThumb>>() {

                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(ArrayListResult<ClientUserWithAffiliationThumb> result) {
                    for (ClientUserWithAffiliationThumb thumb : result.getArrayListResult()) {
                        addItem(new UserLabelListItem(thumb));
                    }
                }
            }, this));
        }
    }

    /** vygeneruje user labely ze seznamu event useru */
    public void setEventUsers(ArrayList<EventUser> eventUsers) {
        clear();
        if (eventUsers != null && !eventUsers.isEmpty()) {
            for (EventUser eu : eventUsers) {
                addItem(new UserLabelListItem(eu));
            }
        }
    }
}
