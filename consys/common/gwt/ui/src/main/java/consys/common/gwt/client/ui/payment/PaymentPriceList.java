package consys.common.gwt.client.ui.payment;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientPaymentOrderData;
import consys.common.gwt.shared.bo.ClientPaymentProduct;

/**
 * Prehled zakupovanych produktu se zahlavim a konecnou sumou
 * //
 * @author pepa
 */
public class PaymentPriceList extends FlowPanel {

    // konstanty
    private static final int DIFF = 200;
    // komponenty
    private SimplePanel dataPanel;
    private SimplePanel totalPanel;
    // data
    private int width;

    public PaymentPriceList(ClientPaymentOrderData data, String currency) {
        width = 600;
        initHeader();
        showData(data, currency);
    }

    /** sirka sloupce nazvu se pocita jako zadana sirka - 200 px */
    public PaymentPriceList(ClientPaymentOrderData data, String currency, int width) {
        this.width = width;
        setWidth(width + "px");
        initHeader();
        showData(data, currency);
    }

    /** vygeneruje hlavicku */
    private void initHeader() {
        clear();

        dataPanel = new SimplePanel();
        totalPanel = new SimplePanel();

        // hlavicka
        Label nameLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.paymentPriceList_text_name(),
                ResourceUtils.system().css().paymentPriceListHeadTitle());
        nameLabel.setWidth((width - DIFF) + "px");
        Label quantityLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.paymentPriceList_text_quantity(),
                ResourceUtils.system().css().paymentPriceListHeadTitleNext());
        quantityLabel.setWidth("70px");
        Label unitPriceLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.paymentPriceList_text_unitPrice(),
                ResourceUtils.system().css().paymentPriceListHeadTitleNext());
        unitPriceLabel.setWidth("100px");

        // suma
        Label totalLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.paymentPriceList_text_total() + ":",
                StyleUtils.PADDING_LEFT_10, StyleUtils.MARGIN_VER_5, StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT);
        totalLabel.setWidth((width - DIFF) + "px");

        Separator separator = new Separator("250px");
        separator.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        separator.addStyleName(StyleUtils.FLOAT_RIGHT);

        add(nameLabel);
        add(quantityLabel);
        add(unitPriceLabel);
        add(StyleUtils.clearDiv());
        add(dataPanel);
        add(separator);
        add(StyleUtils.clearDiv());
        add(totalLabel);
        add(totalPanel);
        add(StyleUtils.clearDiv());
    }

    /** vykresli data */
    private void showData(ClientPaymentOrderData data, String currency) {
        FlowPanel p = new FlowPanel();
        for (ClientPaymentProduct cpp : data.getProducts()) {
            // hodnoty
            Label nameValueLabel = StyleUtils.getStyledLabel(cpp.getName(),
                    StyleUtils.PADDING_LEFT_10, StyleUtils.MARGIN_VER_5, StyleUtils.FLOAT_LEFT);
            nameValueLabel.setWidth((width - DIFF) + "px");
            Label quantityValueLabel = StyleUtils.getStyledLabel(String.valueOf(cpp.getQuantity()),
                    StyleUtils.PADDING_RIGHT_10, StyleUtils.MARGIN_VER_5, StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT);
            quantityValueLabel.setWidth("70px");
            String price = cpp.getPrice().getDecimal() == 0 && cpp.getPrice().getInteger() == 0
                    ? UIMessageUtils.c.const_freeUpper()
                    : cpp.getPrice().toString() + " " + currency;
            Label unitPriceValueLabel = StyleUtils.getStyledLabel(price,
                    StyleUtils.PADDING_RIGHT_10, StyleUtils.MARGIN_VER_5, StyleUtils.FLOAT_LEFT, StyleUtils.ALIGN_RIGHT);
            unitPriceValueLabel.setWidth("100px");

            p.add(nameValueLabel);
            p.add(quantityValueLabel);
            p.add(unitPriceValueLabel);
            p.add(StyleUtils.clearDiv());
        }
        dataPanel.setWidget(p);

        Label sumLabel = StyleUtils.getStyledLabel(data.getTotalPrice().toString() + " " + currency,
                StyleUtils.PADDING_RIGHT_10, StyleUtils.MARGIN_VER_5, StyleUtils.FLOAT_RIGHT, StyleUtils.ALIGN_RIGHT);
        sumLabel.setWidth("100px");
        totalPanel.setWidget(sumLabel);
    }
}
