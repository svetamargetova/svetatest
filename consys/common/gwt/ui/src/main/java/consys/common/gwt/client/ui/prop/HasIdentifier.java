package consys.common.gwt.client.ui.prop;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface HasIdentifier {

    public String getIdentifier();

    public void setIdentifier(String identifier);

}
