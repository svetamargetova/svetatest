/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.ui.comp.form;

import com.google.gwt.user.client.ui.Widget;

/**
 * Rozhranie k implementacii formulara verzie 3.0
 * @author palo
 */
public interface Form {
    /**
     * Prida na formular jeden textovy zaznam.
     * 
     * @param title
     * @param value
     * @return DOM element ktory obaluje tuto dvojicu
     */
    
    FormElement add(String title, String value, String defaultValue, boolean required);
    
    FormElement add(String title, String value, String defaultValue, String help, boolean required);
    
    
    FormElement add(String title, Widget value, boolean required);                
        
    FormElement add(String title, Widget value, String help, boolean required);
    
    /**
     * Prida na formular vstup ktory je validovatelny
     * 
     * @param title
     * @param value
     * @param required
     * @return 
     */
    //FormElement add(String title, Validable value, boolean required);
        
    /**
     * Prida na formular vstup ktory je vadlitovatelny
     * @param title
     * @param value
     * @param help
     * @param required
     * @return 
     */
    //FormElement add(String title, Validable value, String help, boolean required);
    
                
    FormElement add(Widget titleWidget, Widget value, boolean required);
    
    FormElement add(Widget titleWidget, Widget value, String help, boolean required);
    
    FormElement addValueWidget(Widget value);
    
    /**
     * Zvaliduje vsetky validovatelne elementy. 
     * @return 
     */
    boolean validate();
        
    
        
     
    
}
