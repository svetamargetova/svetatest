package consys.common.gwt.client.ui.comp.action;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Abstraktni predek pro ActionImage
 *
 * @author pepa
 */
public abstract class AbstractActionImage<E extends Enum> extends Composite {

    // konstanty
    private static final int TEMPORARILY_DISABLE_DELAY = 1500;
    // komponenty
    private FlowPanel panel;
    private FlowPanel content;
    private FocusPanel center;
    private Timer temporarilyDisabledTimer;
    // data
    private String text;
    private E type;
    private boolean disabled;
    private boolean temporarilyDisabled;
    private ClickHandler click;

    /** enum typu */
    protected enum AbstractActionImageType {

        DISABLED;
        private static final long serialVersionUID = 3578655922726202276L;
    }

    protected AbstractActionImage(String text, E type, String componentStyleName) {
        this.text = text;
        this.type = type;

        ActionResources.INSTANCE.css().ensureInjected();

        panel = new FlowPanel();
        panel.setStyleName(componentStyleName);
        initWidget(panel);

        generate();
    }

    /** vraci text zobrazovany v tlacitku */
    public String getText() {
        return text;
    }

    /** vraci typ tlacitka */
    public E getType() {
        return type;
    }

    /** vraci styl pro levy okraj */
    protected abstract String leftPartStyleName();

    /** vraci styl pro stredni cast */
    protected abstract String centerPartStyleName();

    /** vraci styl pro pravy okraj */
    protected abstract String rightPartStyleName();

    /** vytvari obsah stredni casti a vkladaji do panelu predaneho parametrem */
    protected abstract void createCenter(FocusPanel center);

    /** pokud je potreba nastavi se styl celeho obsahu (obalovy div pro okraje a stredni cast) */
    protected abstract void setContentStyleName(FlowPanel content);

    private void generate() {
        temporarilyDisabledTimer = temporarilyDisabledTimer();

        SimplePanel left = new SimplePanel();
        left.setStyleName(leftPartStyleName());

        center = new FocusPanel();
        center.setStyleName(centerPartStyleName());
        createCenter(center);

        SimplePanel right = new SimplePanel();
        right.setStyleName(rightPartStyleName());

        content = new FlowPanel();
        content.sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        content.addHandler(overHandler(), MouseOverEvent.getType());
        content.addHandler(outHandler(), MouseOutEvent.getType());
        setContentStyleName(content);

        content.add(left);
        content.add(center);
        content.add(right);
        content.add(StyleUtils.clearDiv());

        panel.add(content);
        panel.add(StyleUtils.clearDiv());

        ClickHandler handler = mainClickHandler();
        center.addClickHandler(handler);
        center.addKeyUpHandler(keyUpHandler(handler));
    }

    /** nastavi style obsahu pro zakazanou komponentu */
    protected abstract void setContentDisabledStyleName(FlowPanel content);

    /** nastavi style obsahu pro povolenou komponentu */
    protected abstract void setContentEnabledStyleName(FlowPanel content);

    public void setEnabled(boolean enabled) {
        if (this.disabled == !enabled) {
            // uz je ve stavu v jakem chceme
            return;
        }

        this.disabled = !enabled;

        if (!enabled) {
            setContentDisabledStyleName(content);
        } else {
            setContentEnabledStyleName(content);
        }
    }

    /** pokud je komponenta disablovana vraci true jinak false */
    public boolean isEnabled() {
        return !disabled;
    }

    /** vraci nastaveny clickhandler, jinak null */
    public ClickHandler getClickHandler() {
        return click;
    }

    /** nastavi click handler ktery se spusti po kliknuti */
    public void setClickHandler(ClickHandler handler) {
        click = handler;
    }

    /** hlavni clickHandler, slouzi k rizeni docasneho zneaktivneni tlacitka, kvuli zabraneni dvojkliku */
    private ClickHandler mainClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                click();
            }
        };
    }

    /** pokud neni tlacitko disablovane a click handler neni null, spustime handler */
    public void click() {
        if (!disabled && click != null && !temporarilyDisabled) {
            temporarilyDisabled = true;
            click.onClick(null);
            temporarilyDisabledTimer.schedule(TEMPORARILY_DISABLE_DELAY);
        }
    }

    /** handler, ktery reaguje na enter a spousti click handler s parametrem null */
    private KeyUpHandler keyUpHandler(final ClickHandler handler) {
        return new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    handler.onClick(null);
                }
            }
        };
    }

    /** handler, ktery necha podtrhnout text v tlacitku */
    private MouseOverHandler overHandler() {
        return new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                over();
            }
        };
    }

    /** programove vyvolani stavu, kdyz se najede nad komponentu */
    public void over() {
        if (!disabled) {
            addUnderline();
        }
    }

    /** handler, ktery rusi podtrzeni textu v tlacitku */
    private MouseOutHandler outHandler() {
        return new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                out();
            }
        };
    }

    /** programove vyvolani stavu, kdyz se odjede z komponenty */
    public void out() {
        if (!disabled) {
            removeUnderline();
        }
    }

    /** prida styl znacici podtrzeni */
    protected void addUnderline() {
        StyleUtils.addStyleNameIfNotSet(content, ActionResources.INSTANCE.css().over());
    }

    /** odstrani styl znacici podtrzeni */
    protected void removeUnderline() {
        content.removeStyleName(ActionResources.INSTANCE.css().over());
    }

    /** casovac pro zaktivneni tlacitka po predchozim kliknuti */
    private Timer temporarilyDisabledTimer() {
        return new Timer() {
            @Override
            public void run() {
                temporarilyDisabled = false;
            }
        };
    }
}
