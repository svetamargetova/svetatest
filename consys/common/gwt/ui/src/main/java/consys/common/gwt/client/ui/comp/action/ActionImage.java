package consys.common.gwt.client.ui.comp.action;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.action.ActionImage.ActionImageType;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Akcni tlacitko podle noveho navrhu
 * @author pepa
 */
public class ActionImage extends AbstractActionImage<ActionImageType> {

    /** enum typu */
    public enum ActionImageType {

        NO_ICON, CONFIRM, DELETE;
        private static final long serialVersionUID = -6484853079631000039L;
    }

    private ActionImage(String text, ActionImageType type) {
        super(text, type, ActionResources.INSTANCE.css().actionImage());
    }

    @Override
    protected String leftPartStyleName() {
        return ActionResources.INSTANCE.css().actionImageLeft();
    }

    @Override
    protected String centerPartStyleName() {
        return ActionResources.INSTANCE.css().actionImageCenter();
    }

    @Override
    protected String rightPartStyleName() {
        return ActionResources.INSTANCE.css().actionImageRight();
    }

    @Override
    protected void createCenter(FocusPanel center) {
        switch (getType()) {
            case CONFIRM:
                center.setWidget(iconContent());
                break;
            case NO_ICON:
            case DELETE:
                center.setWidget(new Label(getText()));
                break;
        }
    }

    private FlowPanel iconContent() {
        SimplePanel icon = new SimplePanel();
        icon.setStyleName(ActionResources.INSTANCE.css().icon());

        switch (getType()) {
            case CONFIRM:
                icon.addStyleName(ActionResources.INSTANCE.css().iconConfirm());
                break;
        }

        FlowPanel p = new FlowPanel();
        p.setStyleName(ActionResources.INSTANCE.css().iconWrapper());
        p.add(icon);
        p.add(new Label(getText()));
        p.add(StyleUtils.clearDiv());
        return p;
    }

    @Override
    protected void setContentStyleName(FlowPanel content) {
        contentStyle(content, true);
    }

    @Override
    protected void setContentDisabledStyleName(FlowPanel content) {
        contentStyle(content, false);
    }

    /** nastavi spravne styl podle toho jestli je komponentka enablovana nebo disablovana */
    private void contentStyle(FlowPanel content, boolean enabled) {
        content.setStyleName(ActionResources.INSTANCE.css().actionImageContent());
        if (enabled) {
            switch (getType()) {
                case CONFIRM:
                case NO_ICON:
                    content.addStyleName(ActionResources.INSTANCE.css().enabled());
                    break;
                case DELETE:
                    content.addStyleName(ActionResources.INSTANCE.css().red());
            }
        } else {
            content.addStyleName(ActionResources.INSTANCE.css().disabled());
        }
        switch (getType()) {
            case CONFIRM:
                content.addStyleName(ActionResources.INSTANCE.css().withIcon());
                break;
        }
    }

    @Override
    protected void setContentEnabledStyleName(FlowPanel content) {
        // styl je stejny jako vychozi
        setContentStyleName(content);
    }

    /** vytvori tlacitko bez ikonky */
    public static ActionImage withoutIcon(String text) {
        return new ActionImage(text, ActionImageType.NO_ICON);
    }

    /** vytvori tlacitko s ikonkou "sipka doprava" */
    public static ActionImage confirm(String text) {
        return new ActionImage(text, ActionImageType.CONFIRM);
    }

    /** vytvori cervene tlacitko */
    public static ActionImage delete(String text) {
        return new ActionImage(text, ActionImageType.DELETE);
    }
}
