package consys.common.gwt.client.ui.comp.list.item;

/**
 * Objekt ktory je jeden zaznam v dataliste.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DataListIdItem extends DataListItem{
    private static final long serialVersionUID = -8479798876023824553L;
    
    /** ID zaznamu v databaze */
    private Long id;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }


}
