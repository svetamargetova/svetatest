package consys.common.gwt.client.widget;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.constants.sso.SSO;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.panel.LoginPanel;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Prihlasovaci komponentka
 * @author pepa
 */
public class SSOLogin extends LoginPanel {

    // komponenty
    private LoginForm loginComp;

    public SSOLogin() {
        loginComp = new LoginForm(new ConsysAction() {

            @Override
            public void run() {
                LayoutManager.get().showWaiting(true);
                History.newItem("");
                loginComp.getHandler().doLogin();
            }
        }, "205px");
    }

    @Override
    protected void generateAdditional(FlowPanel panel) {
        panel.add(new Label(UIMessageUtils.c.ssoLogin_text_orUseTakeplaceAccount() + ":"));
        panel.add(loginComp);
    }

    @Override
    protected ConsysAction buttonAction(final SSO type) {
        return ssoLogin(type);
    }

    public LoginForm getLoginComp() {
        return loginComp;
    }

    /** vytvori akci pro sso prihlaseni (url podle SSO a otevre nove okno s touto url) */
    public static ConsysAction ssoLogin(final SSO type) {
        return new ConsysAction() {

            @Override
            public void run() {
                // vyskladani url
                final String url = URLFactory.serverURL()+"oauth/connect/login?service=" + type.name();
                Window.open(url, "_self", ""); 
            }
        };
    }
}
