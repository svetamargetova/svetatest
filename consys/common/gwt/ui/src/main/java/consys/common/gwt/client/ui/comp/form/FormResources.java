package consys.common.gwt.client.ui.comp.form;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

/**
 *
 * @author palo
 */
public interface FormResources extends ClientBundle {

    public static final FormResources INSTANCE = GWT.create(FormResources.class);

    /** css styl pro wrapper komponenty */
    @Source("consys/common/gwt/client/ui/comp/form/form.css")
    public FormCss formCss();
}
