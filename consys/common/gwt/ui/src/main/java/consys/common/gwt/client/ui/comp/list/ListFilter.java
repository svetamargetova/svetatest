package consys.common.gwt.client.ui.comp.list;

import consys.common.gwt.shared.bo.list.Filter;

/**
 *
 * @author palo
 */
public abstract class ListFilter extends Itemizer {

    private Filter filter;

    public ListFilter(int tag) {
        filter = new Filter(tag);
    }

    public ListFilter(int tag, String value) {
        filter = new Filter(tag, value);
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * Metoda zaisti ze filter nebude nijak oznaceny
     */
    public abstract void deselect();

    /**
     * Metoda zaisti ze filter oznaci svoju prvu poziciu
     */
    public abstract void select();

    @Override
    public void refresh() {
        getParentList().refresWithFilter(this);
    }

    @Override
    public int getTag() {
        return filter.tag();
    }
}
