package consys.common.gwt.client.ui.comp;
/*
 * Copyright 2009 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

import com.google.gwt.event.dom.client.HandlesAllKeyEvents;
import com.google.gwt.event.dom.client.HasAllKeyHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusListener;
import com.google.gwt.user.client.ui.HasAnimation;
import com.google.gwt.user.client.ui.HasFocus;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.ListenerWrapper;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SourcesChangeEvents;
import com.google.gwt.user.client.ui.SourcesClickEvents;
import com.google.gwt.user.client.ui.SourcesFocusEvents;
import com.google.gwt.user.client.ui.SourcesKeyboardEvents;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.SuggestOracle.Callback;
import com.google.gwt.user.client.ui.SuggestOracle.Request;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.SuggestionHandler;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.UIObject;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import java.util.Collection;
import java.util.List;

public class SuggestionBox extends Composite implements HasText, HasFocus,
        HasAnimation, SourcesClickEvents, SourcesFocusEvents, SourcesChangeEvents,
        SourcesKeyboardEvents, HasAllKeyHandlers,
        HasValue<String>, HasSelectionHandlers<Suggestion>, ValidableComponent {

   

    /**
     * The SuggestionMenu class is used for the display and selection of
     * suggestions in the SuggestBox widget. SuggestionMenu differs from MenuBar
     * in that it always has a vertical orientation, and it has no submenus. It
     * also allows for programmatic selection of items in the menu, and
     * programmatically performing the action associated with the selected item.
     * In the MenuBar class, items cannot be selected programatically - they can
     * only be selected when the user places the mouse over a particlar item.
     * Additional methods in SuggestionMenu provide information about the number
     * of items in the menu, and the index of the currently selected item.
     */
    private static class SuggestionMenu extends MenuBar {

        public SuggestionMenu() {
            super();
            // Make sure that CSS styles specified for the default Menu classes
            // do not affect this menu
            setStyleName("");
        }

        public void doSelectedItemAction() {
            // In order to perform the action of the item that is currently
            // selected, the menu must be showing.
            MenuItem selectedItem = getSelectedItem();
            if (selectedItem != null) {
                doItemAction(selectedItem, true, false);
            }
        }

        public int getNumItems() {
            return getItems().size();
        }

        /**
         * Returns the index of the menu item that is currently selected.
         *
         * @return returns the selected item
         */
        public int getSelectedItemIndex() {
            // The index of the currently selected item can only be
            // obtained if the menu is showing.
            MenuItem selectedItem = getSelectedItem();
            if (selectedItem != null) {
                return getItems().indexOf(selectedItem);
            }
            return -1;
        }

        /**
         * Selects the item at the specified index in the menu. Selecting the item
         * does not perform the item's associated action; it only changes the style
         * of the item and updates the value of SuggestionMenu.selectedItem.
         *
         * @param index index
         */
        public void selectItem(int index) {
            List<MenuItem> items = getItems();
            if (index > -1 && index < items.size()) {
                itemOver(items.get(index), false);
            }
        }
    }

    /**
     * Class for menu items in a SuggestionMenu. A SuggestionMenuItem differs from
     * a MenuItem in that each item is backed by a Suggestion object. The text of
     * each menu item is derived from the display string of a Suggestion object,
     * and each item stores a reference to its Suggestion object.
     */
    private class SuggestionMenuItem extends MenuItem {

        private static final String STYLENAME_DEFAULT = "item";
        private Suggestion suggestion;

        public SuggestionMenuItem(Suggestion suggestion, boolean asHTML) {
            super(suggestion.getDisplayString(), asHTML);

            // Each suggestion should be placed in a single row in the suggestion
            // menu. If the window is resized and the suggestion cannot fit on a
            // single row, it should be clipped (instead of wrapping around and
            // taking up a second row).
            //DOM.setStyleAttribute(getElement(), "whiteSpace", "nowrap");
            setStyleName(STYLENAME_DEFAULT);
            setSuggestion(suggestion);
        }

        public Suggestion getSuggestion() {
            return suggestion;
        }

        public void setSuggestion(Suggestion suggestion) {
            this.suggestion = suggestion;
        }
    }
    private static final String STYLENAME_DEFAULT = "gwt-SuggestBox";
    private int limit = 20;
    private boolean selectsFirstItem = true;
    private SuggestOracle oracle;
    private String currentText;
    private final SuggestionMenu suggestionMenu;
    private final PopupPanel suggestionPopup;
    private final TextBoxBase box;
    private boolean required;
    private final Callback callback = new Callback() {

        @Override
        public void onSuggestionsReady(SuggestOracle.Request request, SuggestOracle.Response response) {
            showSuggestions(response.getSuggestions());
        }
    };

   

    /**
     * Constructor for {@link SuggestBox}. Creates a {@link TextBox} to use with
     * this {@link SuggestBox}.
     *
     * @param oracle the oracle for this <code>SuggestBox</code>
     */
    public SuggestionBox(ConsysSuggestOracle oracle) {
        this(oracle, new TextBox());
    }

    /**
     * Constructor for {@link SuggestBox}. The text box will be removed from it's
     * current location and wrapped by the {@link SuggestBox}.
     *
     * @param oracle supplies suggestions based upon the current contents of the
     *          text widget
     * @param box the text widget
     */
    public SuggestionBox(ConsysSuggestOracle oracle, TextBoxBase box) {
        this.box = box;
        initWidget(box);

        // suggestionMenu must be created before suggestionPopup, because
        // suggestionMenu is suggestionPopup's widget
        suggestionMenu = new SuggestionMenu();
        suggestionPopup = createPopup();


        addEventsToTextBox();

        setOracle(oracle);

        addStyleName(StyleUtils.HEIGHT_21);
        addStyleName(ResourceUtils.system().css().inputPadd());
        setPopupStyleName(StyleUtils.SUGGEST);
        setStyleName(STYLENAME_DEFAULT);
        addStyleName(StyleUtils.BORDER);
        addStyleName(StyleUtils.WIDTH_270);
        suggestionPopup.addStyleName(StyleUtils.WIDTH_270);
        suggestionMenu.addStyleName(StyleUtils.WIDTH_270);
        setAutoSelectEnabled(true);

        addStyleName(StyleUtils.HEIGHT_21);
        addStyleName(ResourceUtils.system().css().inputPadd());
        setPopupStyleName(StyleUtils.SUGGEST);
        setAutoSelectEnabled(true);

    }

    /**
     * Gets the limit for the number of suggestions that should be displayed for
     * this box. It is up to the current {@link SuggestOracle} to enforce this
     * limit.
     *
     * @return the limit for the number of suggestions
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Gets the suggest box's {@link com.google.gwt.user.client.ui.SuggestOracle}.
     *
     * @return the {@link SuggestOracle}
     */
    public SuggestOracle getSuggestOracle() {
        return oracle;
    }

    @Override
    public int getTabIndex() {
        return box.getTabIndex();
    }

    @Override
    public String getText() {
        return box.getText();
    }

    /**
     * Get the text box associated with this suggest box.
     *
     * @return this suggest box's text box
     */
    public TextBoxBase getTextBox() {
        return box;
    }

    @Override
    public String getValue() {
        return box.getValue();
    }

    /**
     * Hide current suggestions.
     */
    public void hideSuggestionList() {
        this.getSuggestionPopup().hide();
    }

    @Override
    public boolean isAnimationEnabled() {
        return getSuggestionPopup().isAnimationEnabled();
    }

    /**
     * Returns whether or not the first suggestion will be automatically selected.
     * This behavior is on by default.
     *
     * @return true if the first suggestion will be automatically selected
     */
    public boolean isAutoSelectEnabled() {
        return selectsFirstItem;
    }

    /**
     * @return true if the list of suggestions is currently showing, false if not
     */
    public boolean isSuggestionListShowing() {
        return getSuggestionPopup().isShowing();
    }

    /**
     * @deprecated Use the {@link HandlerRegistration#removeHandler}
     * method on the object returned by {@link #getTextBox}().addChangeHandler instead
     */
    @Deprecated
    @Override
    public void removeChangeListener(ChangeListener listener) {
        ListenerWrapper.WrappedChangeListener.remove(box, listener);
    }

    /**
     * @deprecated Use the {@link HandlerRegistration#removeHandler}
     * method on the object returned by {@link #getTextBox}().addClickHandler instead
     */
    @Deprecated
    @Override
    public void removeClickListener(ClickListener listener) {
        ListenerWrapper.WrappedClickListener.remove(box, listener);
    }

    /**
     * @deprecated Use the {@link HandlerRegistration#removeHandler}
     * method no the object returned by {@link #addSelectionHandler} instead
     */
    @Deprecated
    public void removeEventHandler(SuggestionHandler handler) {
    }

    /**
     * @deprecated Use the {@link HandlerRegistration#removeHandler}
     * method on the object returned by {@link #getTextBox}().addFocusListener instead
     */
    @Deprecated
    @Override
    public void removeFocusListener(FocusListener listener) {
        ListenerWrapper.WrappedFocusListener.remove(this, listener);
    }

    /**
     * @deprecated Use the {@link HandlerRegistration#removeHandler}
     * method on the object returned by {@link #getTextBox}().add*Handler instead
     */
    @Deprecated
    @Override
    public void removeKeyboardListener(KeyboardListener listener) {
        ListenerWrapper.WrappedKeyboardListener.remove(this, listener);
    }

    @Override
    public void setAccessKey(char key) {
        box.setAccessKey(key);
    }

    @Override
    public void setAnimationEnabled(boolean enable) {
        getSuggestionPopup().setAnimationEnabled(enable);
    }

    /**
     * Turns on or off the behavior that automatically selects the first suggested
     * item. This behavior is on by default.
     *
     * @param selectsFirstItem Whether or not to automatically select the first
     *          suggestion
     */
    public void setAutoSelectEnabled(boolean selectsFirstItem) {
        this.selectsFirstItem = selectsFirstItem;
    }

    @Override
    public void setFocus(boolean focused) {
        box.setFocus(focused);
    }

    /**
     * Sets the limit to the number of suggestions the oracle should provide. It
     * is up to the oracle to enforce this limit.
     *
     * @param limit the limit to the number of suggestions provided
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * Sets the style name of the suggestion popup.
     *
     * @param style the new primary style name
     * @see UIObject#setStyleName(String)
     */
    public void setPopupStyleName(String style) {
        getSuggestionPopup().setStyleName(style);
    }

    @Override
    public void setTabIndex(int index) {
        box.setTabIndex(index);
    }

    @Override
    public void setText(String text) {
        box.setText(text);
    }

    @Override
    public void setValue(String newValue) {
        box.setValue(newValue);
    }

    @Override
    public void setValue(String value, boolean fireEvents) {
        box.setValue(value, fireEvents);
    }

    /**
     * Show the current list of suggestions.
     */
    public void showSuggestionList() {
        if (isAttached()) {
            currentText = null;
            refreshSuggestions();
        }
    }

    @Override
    protected void onEnsureDebugId(String baseID) {
        super.onEnsureDebugId(baseID);
        suggestionPopup.ensureDebugId(baseID + "-popup");
        suggestionMenu.setMenuItemDebugIds(baseID);
    }

    /**
     * Gets the specified suggestion from the suggestions currently showing.
     *
     * @param index the index at which the suggestion lives
     *
     * @throws IndexOutOfBoundsException if the index is greater then the number
     *           of suggestions currently showing
     *
     * @return the given suggestion
     */
    Suggestion getSuggestion(int index) {
        if (!isSuggestionListShowing()) {
            throw new IndexOutOfBoundsException(
                    "No suggestions showing, so cannot show " + index);
        }
        return ((SuggestionMenuItem) suggestionMenu.getItems().get(index)).suggestion;
    }

    /**
     * Get the number of suggestions that are currently showing.
     *
     * @return the number of suggestions currently showing, 0 if there are none
     */
    int getSuggestionCount() {
        return isSuggestionListShowing() ? suggestionMenu.getNumItems() : 0;
    }

    void showSuggestions(String query) {
        if (query.length() == 0) {
            oracle.requestDefaultSuggestions(new Request(null, limit), callback);
        } else {
            oracle.requestSuggestions(new Request(query, limit), callback);
        }
    }

    private void addEventsToTextBox() {
        class TextBoxEvents extends HandlesAllKeyEvents implements
                ValueChangeHandler<String> {

            @Override
            public void onKeyDown(KeyDownEvent event) {
                // Make sure that the menu is actually showing. These keystrokes
                // are only relevant when choosing a suggestion.
                if (getSuggestionPopup().isAttached()) {
                    switch (event.getNativeKeyCode()) {
                        case KeyCodes.KEY_DOWN:
                            suggestionMenu.selectItem(suggestionMenu.getSelectedItemIndex() + 1);
                            break;
                        case KeyCodes.KEY_UP:
                            suggestionMenu.selectItem(suggestionMenu.getSelectedItemIndex() - 1);
                            break;
                        case KeyCodes.KEY_ENTER:
                        case KeyCodes.KEY_TAB:
                            if (suggestionMenu.getSelectedItemIndex() < 0) {
                                getSuggestionPopup().hide();
                            } else {
                                suggestionMenu.doSelectedItemAction();
                            }
                            break;
                    }
                }
                delegateEvent(SuggestionBox.this, event);
            }

            @Override
            public void onKeyPress(KeyPressEvent event) {
                delegateEvent(SuggestionBox.this, event);
            }

            @Override
            public void onKeyUp(KeyUpEvent event) {
                // After every user key input, refresh the popup's suggestions.
                refreshSuggestions();
                delegateEvent(SuggestionBox.this, event);
            }

            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                delegateEvent(SuggestionBox.this, event);
            }
        }

        TextBoxEvents events = new TextBoxEvents();
        events.addKeyHandlersTo(box);
        box.addValueChangeHandler(events);
    }

    private PopupPanel createPopup() {
        PopupPanel p = new PopupPanel(true, false);
        p.setWidget(suggestionMenu);
        p.setStyleName("gwt-SuggestBoxPopup");
        p.setPreviewingAllNativeEvents(true);
        p.addAutoHidePartner(getTextBox().getElement());
        return p;
    }

    private void fireSuggestionEvent(Suggestion selectedSuggestion) {
        SelectionEvent.fire(this, selectedSuggestion);
    }

    private void refreshSuggestions() {
        // Get the raw text.
        String text = box.getText();
        if (text.equals(currentText)) {
            return;
        } else {
            currentText = text;
        }
        showSuggestions(text);
    }

    private void setNewSelection(SuggestionMenuItem menuItem) {
        Suggestion curSuggestion = menuItem.getSuggestion();
        currentText = curSuggestion.getReplacementString();
        setText(currentText);
        getSuggestionPopup().hide();
        fireSuggestionEvent(curSuggestion);
    }

    /**
     * Sets the suggestion oracle used to create suggestions.
     *
     * @param oracle the oracle
     */
    private void setOracle(SuggestOracle oracle) {
        this.oracle = oracle;
    }

    /**
     * Show the given collection of suggestions.
     *
     * @param suggestions suggestions to show
     */
    private void showSuggestions(Collection<? extends Suggestion> suggestions) {
        if (suggestions.size() > 0) {

            // Hide the popup before we manipulate the menu within it. If we do not
            // do this, some browsers will redraw the popup as items are removed
            // and added to the menu.
            boolean isAnimationEnabled = getSuggestionPopup().isAnimationEnabled();
            if (getSuggestionPopup().isAttached()) {
                getSuggestionPopup().hide();
            }

            suggestionMenu.clearItems();

            for (Suggestion curSuggestion : suggestions) {
                final SuggestionMenuItem menuItem = new SuggestionMenuItem(
                        curSuggestion, oracle.isDisplayStringHTML());
                menuItem.setCommand(new Command() {

                    @Override
                    public void execute() {
                        SuggestionBox.this.setNewSelection(menuItem);
                    }
                });

                suggestionMenu.addItem(menuItem);
            }

            if (selectsFirstItem) {
                // Select the first item in the suggestion menu.
                suggestionMenu.selectItem(0);
            }

            getSuggestionPopup().showRelativeTo(getTextBox());
            getSuggestionPopup().setAnimationEnabled(isAnimationEnabled);
        } else {
            getSuggestionPopup().hide();
        }
    }

    @Override
    public void addFocusListener(FocusListener listener) {
        ListenerWrapper.WrappedFocusListener focus = ListenerWrapper.WrappedFocusListener.add(box,
                listener);
        focus.setSource(this);
    }

    @Override
    public void addKeyboardListener(KeyboardListener listener) {
        ListenerWrapper.WrappedKeyboardListener.add(this, listener);
    }

    @Override
    public void addClickListener(ClickListener listener) {
        ListenerWrapper.WrappedClickListener legacy = ListenerWrapper.WrappedClickListener.add(box,
                listener);
        legacy.setSource(this);
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        ListenerWrapper.WrappedLogicalChangeListener.add(box, listener).setSource(this);
    }

    @Override
    public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
        return addDomHandler(handler, KeyDownEvent.getType());
    }

    @Override
    public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
        return addDomHandler(handler, KeyPressEvent.getType());
    }

    @Override
    public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
        return addDomHandler(handler, KeyUpEvent.getType());
    }

    @Override
    public HandlerRegistration addSelectionHandler(SelectionHandler<Suggestion> handler) {
        return addHandler(handler, SelectionEvent.getType());
    }

    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return addHandler(handler, ValueChangeEvent.getType());
    }

     /**
     * @return the suggestionPopup
     */
    public PopupPanel getSuggestionPopup() {
        return suggestionPopup;
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        if (isRequired() && getTextBox().isEnabled()) {
            return ValidatorUtils.isValidString(getText()) ? true : false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        getTextBox().removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        getTextBox().addStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

}
