package consys.common.gwt.client.ui.utils;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pepa
 */
public class StyleUtils implements CssStyles {

    /** vraci velky titulek */
    public static Label getPanelTitle(String text) {
        Label title = new Label(text);
        title.setStyleName(StyleUtils.FONT_19PX);
        title.addStyleName(StyleUtils.FONT_BOLD);
        title.addStyleName(StyleUtils.MARGIN_BOT_20);
        return title;
    }

    /** vraci titulek pro SimpleFormPanelWithHelp */
    public static Label getPanelTitleWithHelp(String text) {
        return StyleUtils.getStyledLabel(text, FONT_19PX, FONT_BOLD, MARGIN_RIGHT_10);
    }

    /** vraci nastylovany label */
    public static String Assemble(String... styles) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < styles.length; i++) {
            String style = styles[i];
            buffer.append(style).append(" ");
        }
        return buffer.toString();
    }

    /** vraci nastylovany label */
    public static Label getStyledLabel(String text, String... styles) {
        Label label = new Label(text);
        for (int i = 0; i < styles.length; i++) {
            String style = styles[i];
            label.addStyleName(style);
        }
        return label;
    }

    public static Label getErrorLabel(String text) {
        Label label = new Label(text);
        label.addStyleName(ResourceUtils.common().css().error());
        return label;
    }

    public static Label getSuccessLabel(String text) {
        Label label = new Label(text);
        label.addStyleName(ResourceUtils.common().css().success());
        return label;
    }

    /** vraci vychozi nastylovany TextBox */
    public static TextBox getFormInput() {
        return getFormInput(null);
    }

    /** vraci vychozi nastylovany TextBox s volitelnou sirkou */
    public static TextBox getFormInput(String width) {
        TextBox input = new TextBox();
        input.setStyleName(BORDER);
        if (width == null) {
            input.addStyleName(WIDTH_270);
        } else {
            input.setWidth(width);
        }
        input.addStyleName(HEIGHT_21);
        input.addStyleName(ResourceUtils.system().css().inputPadd());
        return input;
    }

    /** vraci polovicni vychozi nastylovany TextBox */
    public static TextBox getFormInputHalf() {
        TextBox input = new TextBox();
        input.setStyleName(BORDER);
        input.addStyleName(WIDTH_85);
        input.addStyleName(HEIGHT_21);
        input.addStyleName(ResourceUtils.system().css().inputPadd());
        return input;
    }

    /** vraci vychozi nastylovany PasswordTextBox */
    public static PasswordTextBox getFormPasswordInput(String width) {
        PasswordTextBox input = new PasswordTextBox();
        input.setStyleName(BORDER);
        input.addStyleName(HEIGHT_21);
        input.addStyleName(ResourceUtils.system().css().inputPadd());
        if (width == null) {
            input.addStyleName(WIDTH_270);
        } else {
            input.setWidth(width);
        }
        return input;
    }

    /** nastavi zadanou barvu textu widgetu */
    public static void changeTextColor(Widget widget, String color) {
        DOM.setStyleAttribute(widget.getElement(), "color", color);
    }

    /** vraci prazdny oddelovaci div pro spravnou vysku wrapperu, kdyz obsahuje floatovaci podcasti */
    public static Widget clearDiv() {
        HTML div = new HTML(" ");
        div.setStyleName(ResourceUtils.system().css().clearDiv());
        return div;
    }

    public static Element clearDivElement() {
        Element div = DOM.createDiv();
        div.setClassName(ResourceUtils.system().css().clearDiv());
        return div;
    }

    /** vraci prazdny oddelovaci div pro spravnou vysku wrapperu, kdyz obsahuje floatovaci podcasti */
    public static Widget clearDiv(String height) {
        Widget w = clearDiv();
        w.setHeight(height);
        return w;
    }

    /** vycpavka do dialogu, kde dela problem vertikalni margin */
    public static SimplePanel getPadder() {
        return getPadder("10px");
    }

    public static SimplePanel getPadder(String height) {
        SimplePanel padder = new SimplePanel();
        padder.setSize("10px", height);
        return padder;
    }

    /** pokud widget neobsahuje zadany styl, tak ho priradi jinak nedela nic */
    public static void addStyleNameIfNotSet(Widget w, String styleName) {
        String styles = w.getStyleName();
        if (!styles.contains(styleName)) {
            w.addStyleName(styleName);
        }
    }
}
