package consys.common.gwt.client.ui.comp;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.dom.DOMX;
import consys.common.gwt.client.ui.utils.LabelColorEnum;

/**
 * Label pro dočasné zobrazení zprávy
 * @author pepa
 */
public class HiddableLabel extends Label {


    // data
    private HideAnimation animation;

    public HiddableLabel() {
        this(LabelColorEnum.SUCCESS_COLOR, "#");
    }

    public HiddableLabel(String text) {
        this(LabelColorEnum.SUCCESS_COLOR, text);
    }

    public HiddableLabel(LabelColorEnum color, String text) {
        super(text);
        DOM.setStyleAttribute(getElement(), "color", LabelColorEnum.getHexColor(color));
        DOMX.setOpacity(getElement(), 0);
    }

    /**
     * zmena barvy labelu
     * @param color je hex cislo barvicky (bez #)
     */
    public void setColor(LabelColorEnum color) {
        DOM.setStyleAttribute(getElement(), "color", LabelColorEnum.getHexColor(color));
    }

    /** zobrazi nastaveny text a po chvili ho opet skryje */
    public void show() {
        DOMX.setOpacity(getElement(), 1);
        Timer t = new Timer() {

            @Override
            public void run() {
                if (animation == null) {
                    animation = new HideAnimation();
                }
                animation.hide(HiddableLabel.this);
            }
        };
        t.schedule(2000);
    }

    /** zobrazi zadany text a po chvili ho opet skryje */
    public void show(String text) {
        setText(text);
        show();
    }

    /** zobrazi zadany text, v zadane barve a po chvili ho opet skryje */
    public void show(String text, LabelColorEnum color) {
        setColor(color);
        setText(text);
        show();
    }

    /** vnitrni trida pro animovani skryvani labelu */
    private class HideAnimation extends Animation {

        // konstanty
        private static final int ANIMATION_DURATION = 1350;
        // data
        private HiddableLabel label;

        public void hide(HiddableLabel label) {
            cancel();
            this.label = label;
            run(ANIMATION_DURATION);
        }

        @Override
        protected void onStart() {
            DOMX.setOpacity(label.getElement(), 1);
        }

        @Override
        protected void onUpdate(double progress) {
            progress = 1 - progress;
            DOMX.setOpacity(label.getElement(), progress);
        }

        @Override
        protected void onComplete() {
            DOMX.setOpacity(label.getElement(), 0);
        }
    }
}
