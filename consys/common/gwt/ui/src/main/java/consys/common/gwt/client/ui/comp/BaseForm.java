package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.HashMap;

/**
 * Trida usnadnuje generovani formularu ve formatu |Popisek:|Widget|, jako
 * zaklad se predpoklada vyuziti inputu ze StyleUtils
 *
 *
 * @author pepa
 */
public class BaseForm extends Composite {

    // konstanty
    public static final int TITLE_COLUMN_WIDTH_INT = 140;
    public static final String TITLE_COLUMN_WIDTH = "140px";
    public static final String ROW_MIN_HEIGHT = "26px";
    public static final String CONTENT_COLUMN_WIDTH = "270px";
    // data
    private FlexTable ft;
    private FlexCellFormatter f;
    private String width;
    private HashMap<Integer, ValidableComponent> components;

    public BaseForm() {
        this(TITLE_COLUMN_WIDTH);
    }

    public BaseForm(String width) {
        this.width = width;

        components = new HashMap<Integer, ValidableComponent>();

        ft = new FlexTable();
        ft.setCellPadding(0);
        ft.setCellSpacing(0);
        ft.setBorderWidth(0);
        f = ft.getFlexCellFormatter();

        initWidget(ft);
    }

    /**
     * Niektore komponenty implementuju has identifier tak sa automaticky snazi
     * nastavit identifier podla popisku labelu
     */
    private void tryToSetIdentifier(String label, Widget widget) {
        if (widget instanceof HasIdentifier) {
            ((HasIdentifier) widget).setIdentifier(label);
        }
    }

    /**
     * přidá titulek povinné položky
     */
    public void addRequired(int row, String text, Widget widget) {
        addTitle(row, text, false);
        addContent(row, widget);
        tryToSetIdentifier(text, widget);
    }

    public void addActionWidget(int row, Widget widget) {
        ft.setWidget(row, 2, widget);
    }

    /**
     * Prida do tabulky na definovany riadok widgety, postupe na jednotlive
     * stlpce
     *
     * @param row
     * @param widgets
     */
    public void addWidgets(int row, Widget... widgets) {
        for (int i = 0; i < widgets.length; i++) {            
            ft.setWidget(row, i, widgets[i]);
        }
    }

    /**
     * přidá titulek volitelné položky
     */
    public void addOptional(int row, String text, Widget widget) {
        addTitle(row, text, true);
        addContent(row, widget);
        tryToSetIdentifier(text, widget);
    }

    /**
     * vloží formulářovou komponentu
     */
    public void addContent(int row, Widget widget) {
        // obalovy objekt, aby se mu dal priradit styl a nebyly tim naruseny predchozi styly
        SimplePanel panel = new SimplePanel();
        panel.setWidget(widget);
        panel.setStyleName(StyleUtils.MARGIN_BOT_5);
        ft.setWidget(row, 1, panel);
        if (widget instanceof ValidableComponent) {
            components.put(row, (ValidableComponent) widget);
        }
    }

    /**
     * vlozi novy radek
     */
    public void insertRow(int beforeRow) {
        ft.insertRow(beforeRow);
    }

    /**
     * Vlozi povinny radek pred definovany radek. To znamena ze vytori novy
     * radek a do neho vlozi definovane polozky
     *
     * @param beforeRow
     * @param label
     * @param widget
     */
    public void insertRequiredRowBeforeRow(int beforeRow, String label, Widget widget) {
        ft.insertRow(beforeRow);
        addRequired(beforeRow, label, widget);
    }

    /**
     * Vlozi nepovinnny radek pred definovany radek. To znamena ze vytori novy
     * radek a do neho vlozi definovane polozky
     *
     * @param beforeRow
     * @param label
     * @param widget
     */
    public void insertOptionalRowBeforeRow(int beforeRow, String label, Widget widget) {
        ft.insertRow(beforeRow);
        addOptional(beforeRow, label, widget);
    }

    public void removeRow(int row) {
        ft.removeRow(row);
    }

    public void clearRow(int row) {
        if (ft.getCellCount(row) > 0) {
            ft.clearCell(row, 0);
            ft.clearCell(row, 1);
        }
    }

    /**
     * nastavi akcni prvky formulare
     */
    public void addActionMembers(int row, ActionImage actionImage, ActionLabel actionLabel) {
        addActionMembers(row, actionImage, actionLabel, CONTENT_COLUMN_WIDTH);
    }

    /**
     * nastavi akcni prvky formulare
     */
    public void addActionMembers(int row, ActionImage actionImage, ActionLabel actionLabel, String contentWidth) {
        HorizontalPanel panel = new HorizontalPanel();
        panel.setWidth(contentWidth);
        panel.setStyleName(StyleUtils.MARGIN_TOP_10);
        panel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        if (actionImage != null) {
            panel.add(actionImage);
        }
        if (actionLabel != null) {
            panel.add(actionLabel);
            panel.setCellHorizontalAlignment(actionLabel, HasAlignment.ALIGN_RIGHT);
        }
        addContent(row, panel);
    }

    /**
     * obsluhuje pridavani titulku
     */
    private void addTitle(int row, String text, boolean optional) {
        if (text != null) {
            String style = StyleUtils.MARGIN_RIGHT_10
                    + " " + StyleUtils.MARGIN_BOT_5
                    + " " + ResourceUtils.system().css().formTitleTop();
            String star = "";
            if (!optional) {
                style += " " + StyleUtils.FONT_BOLD;
                star = "<span class=\"textRed\">* </span>";
            }
            HTML title = text.equals("&nbsp;")
                    ? new HTML("<div class=\"" + style + "\">" + text + "</div>")
                    : new HTML("<div class=\"" + style + "\">" + star + text + ":</div>");
            ft.setWidget(row, 0, title);
            f.setWidth(row, 0, width);
            f.setHeight(row, 0, ROW_MIN_HEIGHT);
            f.setHorizontalAlignment(row, 0, HasAlignment.ALIGN_RIGHT);
            f.setVerticalAlignment(row, 0, HasAlignment.ALIGN_TOP);
        }
    }

    /**
     * provede validaci zadanych vstupu
     *
     * @param fail vypis chyb nalezenych pri validaci
     * @return true validace prosly v poradku
     */
    public boolean validate(ConsysMessage fail) {
        boolean result = true;
        for (ValidableComponent comp : components.values()) {
            if (comp.doValidate(fail)) {
                comp.onSuccess();
            } else {
                comp.onFail();
                result = false;
            }
        }
        return result;
    }

    /**
     * zobrazi / skryje radek ve formu
     */
    public void showRow(int row, boolean show) {
        f.setVisible(row, 0, show);
        f.setVisible(row, 1, show);
    }

    /**
     * @return true pokud je radek zobrazeny
     */
    public boolean isShowRow(int row) {
        return f.isVisible(row, 0);
    }

    /**
     * zobrazi / skryje popisek radku ve formu
     */
    public void showTitle(int row, boolean show) {
        Widget w = ft.getWidget(row, 0);
        if (w != null) {
            w.setVisible(show);
        }
    }

    /**
     * @return true pokud je titulek zobrazeny
     */
    public boolean isShowTitle(int row) {
        Widget w = ft.getWidget(row, 0);
        if (w != null) {
            return w.isVisible();
        }
        return false;
    }

    /**
     * vynuluje chybovy stav u validovatelnych komponent
     */
    public void resetFailState() {
        for (ValidableComponent comp : components.values()) {
            comp.onSuccess();
        }
    }

    /**
     * vyresetuje tabulku do prazdneho stavu (odstrani vsechny radky)
     */
    public void reset() {
        components.clear();
        ft.removeAllRows();
    }

    /**
     * vraci pocet radku v tabulce
     */
    public int getRowCount() {
        return ft.getRowCount();
    }
}
