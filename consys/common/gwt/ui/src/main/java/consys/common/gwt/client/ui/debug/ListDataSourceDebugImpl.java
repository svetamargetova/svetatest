package consys.common.gwt.client.ui.debug;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.ui.comp.list.ListDataSource;
import consys.common.gwt.client.ui.comp.list.ListDataSourceRequest;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import java.util.ArrayList;

/**
 *  
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListDataSourceDebugImpl implements ListDataSource {   

    @Override
    public void sendRequest(ListDataSourceRequest request, AsyncCallback callback, ActionExecutionDelegate executionDelegate) {
        ArrayList<DataListUuidItem> items = new ArrayList<DataListUuidItem>();
        for (int i = 1; i <= request.getItemsPerPage(); i++) {
            DataListUuidItem item = new DataListUuidItem();
            item.setUuid("Objekt " + i * request.getFirstResult());
        }
        ListDataSourceResult dataSourceResult = new ListDataSourceResult(items, 36);
        callback.onSuccess(dataSourceResult);
    }


}
