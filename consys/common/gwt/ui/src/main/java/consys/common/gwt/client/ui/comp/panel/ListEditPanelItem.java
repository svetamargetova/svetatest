package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class ListEditPanelItem<T> extends ListPanelItem<T>{

    private RemoveDelegate removeDelegate;

    @Override
    public void create(FlowPanel panel) {
        SimplePanel content = createContent();
        content.addStyleName(ResourceUtils.system().css().listPanelEditItemContent());

        ActionLabel removeUser = new ActionLabel(UIMessageUtils.c.const_remove());
        removeUser.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (removeDelegate != null) {
                    removeDelegate.didRemoveItem(ListEditPanelItem.this);
                }
            }
        });

        SimplePanel removeLabelWrapper = new SimplePanel();
        removeLabelWrapper.setStyleName(ResourceUtils.system().css().listPanelEditItemAction());
        removeLabelWrapper.setWidget(removeUser);


        panel.add(content);
        panel.add(removeLabelWrapper);
        
    }

    public abstract SimplePanel createContent();


    public void setRemoveDelegate(RemoveDelegate removeDelegate) {
        this.removeDelegate = removeDelegate;
    }

    public interface RemoveDelegate<T extends ListEditPanelItem> {

        public void didRemoveItem(T item);
    }




}
