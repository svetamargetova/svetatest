package consys.common.gwt.client.ui.comp.panel.abst;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;

/**
 * Predpripraveny simple form s obecne pouzivanymi metodami pro pridavani,
 * prohlizeni, editaci a mazani
 * @author pepa
 */
public abstract class CRUDSimpleHelpFormPanel extends RUSimpleHelpFormPanel implements CRUD {

    public CRUDSimpleHelpFormPanel() {
        super();
    }

    public CRUDSimpleHelpFormPanel(boolean withHiddableLabel) {
        super(withHiddableLabel);
    }

    /** ClickHandler pro prepnuti do pridavaciho zobrazeni */
    @Override
    public ClickHandler createClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Widget w = createForm();
                if (w != null) {
                    setWidget(w);
                }
            }
        };
    }

    /** ClickHandler pro prepnuti do mazaciho zobrazeni */
    @Override
    public ClickHandler deleteClickHandler(final String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                Widget w = deleteForm(id);
                if (w != null) {
                    setWidget(w);
                }
            }
        };
    }
}
