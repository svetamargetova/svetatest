package consys.common.gwt.client.ui.comp.add;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.Dialog;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Trida nove polozky pridavane ve vytvarecim dialogu
 * @author pepa
 */
public class NewItemUI extends FlowPanel {

    // konstanty
    public static final String WIDTH_INPUT = "400px";
    // komponenty
    private BaseForm form = new BaseForm();
    private ConsysStringTextBox nameBox;
    private ConsysTextArea descBox;
    // data
    private int nameLimit;
    private int desctiptionLimit;

    public NewItemUI() {
        this(255, 255);
    }

    public NewItemUI(int nameLimit, int desctiptionLimit) {
        this.nameLimit = nameLimit;
        this.desctiptionLimit = desctiptionLimit;
    }

    @Override
    protected void onLoad() {
        prepareContent();
    }

    protected void prepareContent() {
        Separator separator = new Separator(Dialog.DEFAULT_WIDTH - 20 + "px", StyleUtils.MARGIN_BOT_10);
        separator.addStyleName(StyleUtils.MARGIN_LEFT_10);
        add(separator);

        Image remove = new Image(ResourceUtils.system().removeCross());
        remove.setStyleName(StyleUtils.HAND);
        remove.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        remove.addStyleName(StyleUtils.MARGIN_BOT_5);
        remove.addStyleName(StyleUtils.FLOAT_RIGHT);
        remove.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                removeFromParent();
            }
        });
        add(remove);

        form.setStyleName(StyleUtils.MARGIN_BOT_20);
        form.addStyleName(StyleUtils.CLEAR_BOTH);
        createForm(form);
        add(form);
    }

    public void createForm(BaseForm form) {
        nameBox = new ConsysStringTextBox(1, nameLimit, UIMessageUtils.c.const_name());
        nameBox.setWidth(WIDTH_INPUT);
        descBox = new ConsysTextArea(0, desctiptionLimit, UIMessageUtils.c.const_description());
        descBox.setWidth(WIDTH_INPUT);
        form.addRequired(0, UIMessageUtils.c.const_name(), nameBox);
        form.addOptional(1, UIMessageUtils.c.const_description(), descBox);
    }

    /** vraci formular (validace/pridani novych polozek) */
    public BaseForm form() {
        return form;
    }

    /** zameri vstup pro nazev */
    public void focus() {
        nameBox.setFocus(true);
    }

    /** vraci zadany nazev */
    public String getName() {
        return nameBox.getText().trim();
    }

    /** vraci zadany description */
    public String getDescription() {
        return descBox.getText().trim();
    }
}
