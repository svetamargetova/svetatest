package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.list.Filter;

/**
 * Výběrová položka ze select boxu
 * @author pepa
 */
public class DataListCommonFiltersSelectItem extends DataListCommonFiltersItem {

    // komponenty
    private SelectBox<String> selectBox;
    private Label label;
    // data
    private Filter filter;
    private HeadPanel parent;

    public DataListCommonFiltersSelectItem(final int tag, SelectBox<String> selectBox, String text, HeadPanel parent) {
        super(tag);
        this.selectBox = selectBox;
        this.parent = parent;

        label = StyleUtils.getStyledLabel(text + ":", StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_RIGHT_10);

        filter = new Filter(tag, null);

        selectBox.setWidth(160);
        selectBox.addStyleName(StyleUtils.FLOAT_LEFT);
        selectBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                filter = new Filter(tag, event.getValue().getItem());
            }
        });
    }

    @Override
    protected void onLoad() {
        FlowPanel panel = new FlowPanel();
        panel.addStyleName(StyleUtils.FLOAT_LEFT);
        panel.add(label);
        panel.add(selectBox);
        panel.add(StyleUtils.clearDiv());
        setWidget(panel);
    }

    /** prida change value handler */
    public void addChangeValueHandler(ChangeValueEvent.Handler<SelectBoxItem<String>> handler) {
        selectBox.addChangeValueHandler(handler);
    }

    @Override
    public void deselect() {
        selectBox.selectNone();
    }

    @Override
    public void select() {
        parent.clearFilter();
        parent.registerFilter(this);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @Override
    public int getTag() {
        return filter.tag();
    }
}
