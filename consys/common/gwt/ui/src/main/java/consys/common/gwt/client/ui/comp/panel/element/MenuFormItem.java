package consys.common.gwt.client.ui.comp.panel.element;

import com.google.gwt.user.client.ui.SimplePanel;

/**
 * Interface pro widgety, ktere maji byt jako polozky MenuFormPanelu
 * @author pepa
 */
public interface MenuFormItem {

    /** metoda je volana pro ziskani popisku do tlacitka v menu */
    public String getName();

    /** metoda je volana pro ziskani poznamky popisku do tlacitka v menu */
    public String getNote();

    /**
     * metoda je volana, kdyz ma byt komponenta zobrazena
     * @param panel je obsah, do ktereho bude komponenta vlozena
     */
    public void onShow(SimplePanel panel);
}
