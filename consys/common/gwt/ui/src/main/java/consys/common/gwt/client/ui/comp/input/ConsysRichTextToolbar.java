package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RichTextArea.Formatter;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Toolbar pro rich text area
 * @author pepa
 */
public class ConsysRichTextToolbar extends Composite {

    // komponenty
    private ToggleButton bold;
    private ToggleButton italic;
    private ToggleButton underline;
    private ToggleButton strikethrough;
    private ToggleButton createLink;
    private ToggleButton removeLink;
    private ToggleButton removeFormat;
    private ToggleButton list;
    // data
    private RichTextArea area;
    private Formatter formatter;
    private EventHandler handler;

    public ConsysRichTextToolbar(RichTextArea area) {
        this.area = area;
        formatter = area.getFormatter();
        handler = new EventHandler();

        area.addKeyUpHandler(handler);
        area.addClickHandler(handler);

        init();
    }

    /** vytvori toolbar */
    private void init() {
        bold = createToggleButton(ResourceUtils.system().richTextBold(),
                UIMessageUtils.c.consysRichTextToolbar_text_bold());
        bold.addStyleName(StyleUtils.FLOAT_LEFT);
        italic = createToggleButton(ResourceUtils.system().richTextItalic(),
                UIMessageUtils.c.consysRichTextToolbar_text_italic());
        italic.addStyleName(StyleUtils.FLOAT_LEFT);
        underline = createToggleButton(ResourceUtils.system().richTextUnderline(),
                UIMessageUtils.c.consysRichTextToolbar_text_underline());
        underline.addStyleName(StyleUtils.FLOAT_LEFT);
        strikethrough = createToggleButton(ResourceUtils.system().richTextStrikeThrough(),
                UIMessageUtils.c.consysRichTextToolbar_text_strikethrough());
        strikethrough.addStyleName(StyleUtils.FLOAT_LEFT);
        createLink = createToggleButton(ResourceUtils.system().richTextCreateLink(),
                UIMessageUtils.c.consysRichTextToolbar_text_createLink());
        createLink.addStyleName(StyleUtils.FLOAT_LEFT);
        removeLink = createToggleButton(ResourceUtils.system().richTextRemoveLink(),
                UIMessageUtils.c.consysRichTextToolbar_text_removeLink());
        removeLink.addStyleName(StyleUtils.FLOAT_LEFT);
        removeFormat = createToggleButton(ResourceUtils.system().richTextRemoveFormat(),
                UIMessageUtils.c.consysRichTextToolbar_text_removeFormat());
        removeFormat.addStyleName(StyleUtils.FLOAT_LEFT);
        list = createToggleButton(ResourceUtils.system().richTextList(),
                UIMessageUtils.c.consysRichTextToolbar_text_list());
        list.addStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel panel = new FlowPanel();
        panel.add(bold);
        panel.add(italic);
        panel.add(underline);
        panel.add(strikethrough);
        panel.add(createLink);
        panel.add(removeLink);
        panel.add(removeFormat);
        panel.add(list);
        panel.add(StyleUtils.clearDiv());
        initWidget(panel);
    }

    /** vytvori tlacitko toolbaru */
    private ToggleButton createToggleButton(ImageResource resource, String tip) {
        ToggleButton tb = new ToggleButton(new Image(resource));
        tb.addClickHandler(handler);
        tb.setTitle(tip);
        return tb;
    }

    /** handler pro zachytavani udalosti z komponent */
    private class EventHandler implements ClickHandler, KeyUpHandler {

        @Override
        public void onClick(ClickEvent event) {
            Widget sender = (Widget) event.getSource();

            if (sender == bold) {
                formatter.toggleBold();
            } else if (sender == italic) {
                formatter.toggleItalic();
            } else if (sender == underline) {
                formatter.toggleUnderline();
            } else if (sender == strikethrough) {
                formatter.toggleStrikethrough();
            } else if (sender == createLink) {
                String url = Window.prompt(UIMessageUtils.c.consysRichTextToolbar_text_enterURL() + ":", "http://");
                if (url != null) {
                    formatter.createLink(url);
                }
            } else if (sender == removeLink) {
                formatter.removeLink();
            } else if (sender == removeFormat) {
                formatter.removeFormat();
            } else if (sender == list) {
                formatter.insertUnorderedList();
            } else if (sender == area) {
                updateStatus();
            }
        }

        @Override
        public void onKeyUp(KeyUpEvent event) {
            Widget sender = (Widget) event.getSource();
            if (sender == area) {
                updateStatus();
            }
        }
    }

    /** aktualizuje stav tlacitek */
    private void updateStatus() {
        bold.setDown(formatter.isBold());
        italic.setDown(formatter.isItalic());
        underline.setDown(formatter.isUnderlined());
        strikethrough.setDown(formatter.isStrikethrough());
    }
}
