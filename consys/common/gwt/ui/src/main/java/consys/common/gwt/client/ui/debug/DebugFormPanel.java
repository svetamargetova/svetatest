package consys.common.gwt.client.ui.debug;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.utils.CssStyles;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DebugFormPanel extends PanelItem {

    FlowPanel forms;

    public DebugFormPanel() {
        super(false);
        forms = new FlowPanel();
    }


    @Override
    public String getName() {
        return "Debug Form Panel";
    }

    @Override
    public Widget getTitle() {
        return new Label(getName());
    }

    @Override
    public Widget getContent() {
        return forms;
    }

    public void addDebugPanel(String name, final Widget w){
        ActionLabel actionLabel = new ActionLabel(name,CssStyles.MARGIN_HOR_5);
        actionLabel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new ChangeContentEvent(w));
            }
        });
        SimplePanel panel = new SimplePanel();
        panel.setHeight("15px");
        panel.setWidget(actionLabel);
        forms.add(panel);
    }




}
