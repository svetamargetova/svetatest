package consys.common.gwt.client.ui.comp.list;

import consys.common.gwt.client.ui.comp.list.item.DataListItem;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;


/**
 * ListDataSource Action result. Obsahuje vsetky informacie tak aby datalist
 * mohol vykrelist nove zaznamy.
 */
public class ListDataSourceResult implements Result{
    private static final long serialVersionUID = -3156668714133458651L;

    private ArrayList<? extends DataListItem> items;
    private long allItemsCount;

    public ListDataSourceResult() {
    }

    public ListDataSourceResult(ArrayList<? extends DataListItem> items, long allItemsCount) {
        this.items = items;
        this.allItemsCount = allItemsCount;
    }

    public ArrayList getItems() {
        return items;
    }

    public long getAllItemsCount() {
        return allItemsCount;
    }
}
