package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Vlastni implementace TextArea s moznosti omezeni maximalniho poctu znaku
 * @author pepa
 */
public class ConsysTextArea extends Composite implements ValidableComponent, HasIdentifier {

    // konstanty
    public static final String WIDTH = "270px";
    public static final String HEIGHT = "80px";
    public static final int UNLIMITED = 0;
    // komponenty
    private FlowPanel panel;
    private TextArea area;
    private Label textLengthLabel;
    // data
    private String emptyText;
    private int minChar;
    private int maxChar;
    private String fieldName;
    private boolean showInfo;

    public ConsysTextArea(int minChar, int maxChar) {
        this(minChar, maxChar, WIDTH, HEIGHT, true, "");
    }

    public ConsysTextArea(int minChar, int maxChar, String fieldName) {
        this(minChar, maxChar, WIDTH, HEIGHT, true, fieldName);
    }

    public ConsysTextArea(int maxChar, String width, String height, String fieldName) {
        this(0, maxChar, width, height, true, fieldName);
    }

    public ConsysTextArea(int maxChar, String width, String height, boolean showInfo, String fieldName) {
        this(0, maxChar, width, height, showInfo, fieldName);
    }

    public ConsysTextArea(int minChar, final int maxChar, String width, String height, boolean showInfo, String fieldName) {
        this.minChar = minChar;
        this.maxChar = maxChar;
        this.showInfo = showInfo;
        this.fieldName = fieldName;
        panel = new FlowPanel();
        panel.setWidth(width);

        area = new TextArea();
        area.setStyleName(StyleUtils.BORDER);
        area.addStyleName(ResourceUtils.system().css().inputAreaPadd());
        area.setWidth(width);
        area.setHeight(height);
        panel.add(area);

        if (showInfo) {
            FlowPanel infoPanel = new FlowPanel();
            infoPanel.setStyleName(StyleUtils.MARGIN_TOP_5);
            infoPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
            textLengthLabel = StyleUtils.getStyledLabel("0", StyleUtils.FONT_10PX, StyleUtils.FLOAT_LEFT);
            infoPanel.add(textLengthLabel);
            String maxText = "/" + (maxChar == UNLIMITED ? UIMessageUtils.c.const_unlimited() : maxChar);
            infoPanel.add(StyleUtils.getStyledLabel(maxText, StyleUtils.FONT_10PX, StyleUtils.FLOAT_LEFT));

            FlowPanel infoPanelWrapper = new FlowPanel();
            infoPanelWrapper.add(infoPanel);
            panel.add(infoPanelWrapper);

            area.addKeyUpHandler(new KeyUpHandler() {
                @Override
                public void onKeyUp(KeyUpEvent event) {
                    String text = area.getText();
                    int length = text.length();
                    if (maxChar != UNLIMITED && length > maxChar) {
                        length = maxChar;
                        int scrolled = area.getElement().getScrollTop();
                        area.setText(text.substring(0, maxChar));
                        area.getElement().setScrollTop(scrolled);
                    }
                    textLengthLabel.setText(String.valueOf(length));
                }
            });

            panel.add(StyleUtils.clearDiv());
        }

        initWidget(panel);
    }

    /** enabluje/disabluje komponentu */
    public void setEnabled(boolean value) {
        area.setEnabled(value);
    }

    /** vraci text */
    public String getText() {
        String result = area.getText().trim();
        if (emptyText != null && emptyText.equals(result)) {
            return "";
        }
        return result;
    }

    /** nastavuje text */
    public void setText(String text) {
        if (emptyText != null) {
            if (StringUtils.isBlank(text)) {
                area.setText(emptyText);
                showGrayColor();
            } else {
                area.setText(text);
                showNormalColor();
            }
        } else {
            area.setText(text);
            showNormalColor();
        }
        if (showInfo) {
            textLengthLabel.setText(String.valueOf(text == null ? 0 : text.length()));
        }
    }

    @Override
    public void setWidth(String width) {
        panel.setWidth(width);
        area.setWidth(width);
    }

    @Override
    public void setHeight(String height) {
        area.setHeight(height);
    }

    /** prida / zrusi focus na komponentu */
    public void setFocus(boolean focused) {
        area.setFocus(focused);
    }

    /** prida blur handler */
    public HandlerRegistration addBlurHandler(BlurHandler handler) {
        return area.addBlurHandler(handler);
    }

    /** prida value change handler - zavola az po opusteni */
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return area.addValueChangeHandler(handler);
    }

    /** prida */
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return area.addChangeHandler(handler);
    }

    /** prida key up hadnler*/
    public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
        return area.addKeyUpHandler(handler);
    }

    /**
     * nastavi text, ktery se ma zobrazit v prazdne komponente,
     * pokud je tento text nastaven, meni se i chovani metody getText
     * (jestli se zadany text shoduje s textem v komponente, vraci se prezdny string!)
     */
    public void setEmptyText(String text) {
        if (emptyText == null) {
            area.addFocusHandler(new FocusHandler() {
                @Override
                public void onFocus(FocusEvent event) {
                    if (area.getText().trim().equals(emptyText)) {
                        area.setText("");
                        showNormalColor();
                    }
                }
            });
            area.addBlurHandler(new BlurHandler() {
                @Override
                public void onBlur(BlurEvent event) {
                    if (area.getText().trim().isEmpty()) {
                        area.setText(emptyText);
                        showGrayColor();
                    }
                }
            });
        }
        emptyText = text;
        if (area.getText().trim().isEmpty()) {
            area.setText(emptyText);
            showGrayColor();
        }
    }

    /** nastavi nazev komponenty (pro odesilani ve formulari) */
    public void setName(String name) {
        area.setName(name);
    }

    private void showGrayColor() {
        DOM.setStyleAttribute(area.getElement(), "color", CssStyles.VALUE_COLOR_GRAY);
    }

    private void showNormalColor() {
        area.getElement().getStyle().clearColor();
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        String text = getText().trim();
        if (maxChar == UNLIMITED) {
            if (text.length() < minChar) {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
                return false;
            }
            return true;
        }
        int result = ValidatorUtils.isLenght(text, minChar, maxChar);
        if (result != 0) {
            if (!ValidatorUtils.isValidString(text)) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(fieldName));
            } else {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
            }
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        area.removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        area.addStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public String getIdentifier() {
        return fieldName;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.fieldName = identifier;
    }
}
