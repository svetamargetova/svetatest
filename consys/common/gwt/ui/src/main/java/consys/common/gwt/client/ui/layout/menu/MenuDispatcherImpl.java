package consys.common.gwt.client.ui.layout.menu;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.event.UserLogoutEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleRegistrator;
import consys.common.gwt.client.ui.event.FireAfterLoginEvent;
import consys.common.gwt.client.ui.event.MenuItemEvent;
import consys.common.gwt.client.ui.event.MenuItemEvent.MenuItemAction;
import consys.common.gwt.client.ui.event.UriFragmentTokenEvent;
import consys.common.gwt.client.ui.layout.LayoutTopMenu;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import java.util.Collections;

/**
 * Produkcni implementace MenuDispatcheru
 * Třída má na starost správu menu (generovani, přidávání)
 * @author pepa
 */
public class MenuDispatcherImpl implements
        UserLoginEvent.Handler,
        UserLogoutEvent.Handler,
        UriFragmentTokenEvent.Handler,
        FireAfterLoginEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(MenuDispatcherImpl.class);
    // konstanty
    protected static final String WEB_URL = "http://www.takeplace.eu/";
    // konstanty - history token
    public static final String SIGN_UP = "SignUp";
    public static final String FAQ = "FAQ";
    // komponenty
    private Widget signUpWidget;
    private Widget faqWidget;
    // data
    protected ModuleRegistrator registrator;
    private String redirectFragments;

    public MenuDispatcherImpl() {
        registrator = ModuleRegistrator.get();
        EventBus.get().addHandler(UserLoginEvent.TYPE, this);
        EventBus.get().addHandler(UserLogoutEvent.TYPE, this);
        EventBus.get().addHandler(UriFragmentTokenEvent.TYPE, this);
        EventBus.get().addHandler(FireAfterLoginEvent.TYPE, this);
    }

    /** vlozi polozku menu na konec menu */
    public void addMenuItem(String title, String token) {
        addMenuItem(title, token, null, -1);
    }

    /** vlozi polozku menu na konec menu */
    public void addMenuItem(String title, String token, Widget widget) {
        addMenuItem(title, token, widget, -1);
    }

    /** vlozi polozku menu na pozici menu */
    public void addMenuItem(String title, String token, Widget widget, int position) {
        MenuItem menuItem = new MenuItem(title, token);
        menuItem.setContent(widget);
        MenuItemEvent menuItemEvent = new MenuItemEvent(menuItem, MenuItemAction.ADD, position);
        EventBus.get().fireEvent(menuItemEvent);
    }

    /** vlozi polozku menu na konec menu */
    public void addMenuItem(MenuItem menuItem) {
        addMenuItem(menuItem, -1);
    }

    /** vlozi polozku menu na pozici menu */
    public void addMenuItem(MenuItem menuItem, int position) {
        MenuItemEvent menuItemEvent = new MenuItemEvent(menuItem, MenuItemAction.ADD, position);
        EventBus.get().fireEvent(menuItemEvent);
    }

    /** vygeneruje zakladni menu v zavislosti na tom jestli je uzivatel prihlaseny nebo ne */
    public void generateMenu() {
        LayoutTopMenu.get().getMenuSearcher().setVisible(false);

        Cache.get().doCacheAction(new CacheAction<Boolean>() {

            @Override
            public void doAction(Boolean value) {
                if (value) {
                    generateLoginMenu(true);
                } else {
                    generateLogoutMenu();
                }
            }

            @Override
            public void doTimeoutAction() {
                // TODO: vypsani chyby a popis dalsiho postupu
            }

            @Override
            public String getName() {
                return Cache.LOGGED;
            }

            @Override
            public boolean isCancel() {
                return false;
            }
        });
    }

    /** vygeneruje zakladni menu prihlaseneho uzivatele */
    protected void generateLoginMenu(boolean selectFirst) {
        //EventBus.get().fireEvent(new ChangeContentEvent(new Label("")));
        EventBus.get().fireEvent(new MenuItemEvent(MenuItemAction.CLEAR));

        MenuItem firstMenuItem = null;
        Collections.sort(registrator.getLoggedInMenuModuleRegistrator().getMenu());
        for (Module.ModuleMenuItem menuItem : registrator.getLoggedInMenuModuleRegistrator().getMenu()) {
            if (firstMenuItem == null) {
                firstMenuItem = new MenuItem(menuItem.getTitle(), menuItem.getToken());
                firstMenuItem.setContent(menuItem.getWidget());
                addMenuItem(firstMenuItem);
            } else {
                addMenuItem(menuItem.getTitle(), menuItem.getToken(), menuItem.getWidget());
            }
        }

        // FAQ
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_faq(), FAQ, faqWidget);

        if (selectFirst) {
            // vybrani vychozi polozky            
            final MenuItemEvent menuItemEvent = new MenuItemEvent(firstMenuItem, MenuItemAction.SELECT);
            Timer t = new Timer() { // TODO: proc to tu bez zpozdeni nefunguje????

                @Override
                public void run() {
                    History.newItem(menuItemEvent.getMenuItem().getToken());
                }
            };
            t.schedule(100);
        }
    }

    /**
     * vygeneruje zakladni menu neprihlaseneho uzivatele
     * TODO(palo): Unifikovat pomocu module registratoru kde sa zaregistruje module 
     * ktery je do loggedOud sekcie. Nutne vytvorit Widget ktery bude obsahovat 
     * redirect
     */
    protected void generateLogoutMenu() {
        EventBus.get().fireEvent(new MenuItemEvent(MenuItemAction.CLEAR));

        // Home
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_home(), WEB_URL + "en");

        // Features
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_features(), WEB_URL + "en/features");

        // Overview
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_overview(), WEB_URL + "en/overview");

        // Customers
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_customers(), WEB_URL + "en/customers");

        // Press & Media
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_pressAndMedia(), WEB_URL + "en/press-media");

        // About us
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_aboutUs(), WEB_URL + "en/about-us");

        // SIGN UP
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_signUp(), SIGN_UP, signUpWidget);

        // FAQ
        addMenuItem(UIMessageUtils.c.menuDispatcherImpl_mainMenu_faq(), FAQ, faqWidget);


        Timer t = new Timer() { // TODO: proc to tu bez zpozdeni nefunguje????

            @Override
            public void run() {
                String token = History.getToken();
                // Ak je token uz nastaveny tak je nutne ho znova spracovat
                if (!token.isEmpty()) {
                    logger.debug("Application was reloaded and token is set to: " + token);
                    // Musime prevolat tento stav pretoze ak sa vstupuje do entry pointu
                    // uz s nastavenym obsahom tak sa neprevolava history handler
                    History.fireCurrentHistoryState();
                } else {
                    History.newItem(SIGN_UP);
                }
            }
        };
        t.schedule(50);
    }

    /** obsah umisteny pod "Sign up" v neprihlasenem menu */
    public void setSignUpWidget(Widget widget) {
        signUpWidget = widget;
    }

    /** obsah umisteny pod "FAQ" v neprihlasenem menu */
    public void setFAQWidget(Widget widget) {
        faqWidget = widget;
    }

    @Override
    public void onUserLogin(UserLoginEvent event) {
        for (Module module : registrator.getLoggedInModules()) {
            logger.debug("Register module - " + module.getClass());
            module.registerModule();
        }

        // Redirected content
        if (StringUtils.isNotBlank(redirectFragments)) {
            logger.debug(" onUserLogin with fragment " + redirectFragments);
            generateLoginMenu(false);
            // vybrani vychozi polozky
            Timer t = new Timer() { // TODO: proc to tu bez zpozdeni nefunguje????

                @Override
                public void run() {
                    logger.debug(" redirecting " + redirectFragments);
                    final String tmp = redirectFragments;
                    redirectFragments = null;
                    if (History.getToken().equals(tmp)) {
                        History.newItem("refresh-" + tmp);
                        Timer t = new Timer() {

                            @Override
                            public void run() {
                                History.newItem(tmp);
                            }
                        };
                        t.schedule(300);
                    } else {
                        History.newItem(tmp);
                    }
                }
            };
            t.schedule(100);

        } else {
            logger.debug("onUserLogin " + redirectFragments);
            generateLoginMenu(true);
        }
    }

    @Override
    public void onUserLogout(UserLogoutEvent event) {
        try {
            for (Module module : registrator.getLoggedInModules()) {
                module.unregisterModule();
            }
        } catch (AssertionError e) {
        }
        generateLogoutMenu();
    }

    @Override
    public void onCatchedFragment(UriFragmentTokenEvent event) {
        if (event.isHandled()) {
            // Ked uz ho ohandloval nekdo iny neni duvod to handlovat tu
            return;
        }
        String token = event.getFragment();
        if (token.contains("http://www.takeplace.eu")) {
            Window.Location.replace(token);
        } else {
            MenuItem menuItem = new MenuItem(null, token);
            MenuItemEvent menuItemEvent = new MenuItemEvent(menuItem, MenuItemAction.SELECT);
            logger.debug("event firing for token " + token);
            EventBus.get().fireEvent(menuItemEvent);
            if (!menuItemEvent.isSuccess()) {
                logger.debug("no MenuItem for token " + token + "  ");
            } else {
                event.setHandled(true);
            }
        }
    }

    @Override
    public void afterLoginFire(FireAfterLoginEvent event) {
        redirectFragments = event.getFragmane();
    }
}
