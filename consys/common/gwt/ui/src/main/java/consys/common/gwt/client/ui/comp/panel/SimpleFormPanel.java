package consys.common.gwt.client.ui.comp.panel;

import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.HiddableLabel;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Zakladni panel vkladany do contentu
 *   ________________
 *  |______HEAD______| \
 *  |_HIDDABLE LABLE_| |
 *  |______BODY______|  \ Panel
 *  |____CONTENT_____|  /
 *  |_____/BODY______| |
 *  |______FOOT______| /
 *
 * @author pepa
 */
public class SimpleFormPanel extends Composite implements ActionExecutionDelegate, CssStyles {

    // konstatnty
    private static final String STYLE_CLASS_HEAD = "simpleForm-Head";
    private static final String STYLE_CLASS_BODY = "simpleForm-Body";
    private static final String STYLE_CLASS_CONTENT = "simpleForm-Content";
    private static final String STYLE_CLASS_FOOT = "simpleForm-Foot";
    // komponenty
    private FlowPanel panel;
    private FlowPanel messagePart;
    /**
     * 4  zakladne casti:
     * HEAD
     * HIDDABLE LABEL
     * BODY + MESSAGE PART
     * FOOT
     */
    private Widget headPart;
    private SimplePanel bodyPart;
    private Widget footPart;
    private HiddableLabel hiddableLabel;
    // Komponenty
    private FlowPanel content;
    // waiting
    private Waiting waiting;

    public SimpleFormPanel() {
        this(null, null);
    }

    public SimpleFormPanel(boolean withHiddableLabel) {
        this(null, null, withHiddableLabel);
    }

    public SimpleFormPanel(Widget widget) {
        this(widget, null);
    }

    public SimpleFormPanel(String width) {
        this(null, width);
    }

    public SimpleFormPanel(Widget widget, String width) {
        this(widget, width, false);
    }

    public SimpleFormPanel(Widget widget, String width, boolean withHiddableLabel) {
        panel = new FlowPanel();
        panel.setWidth(width == null ? LayoutManager.LAYOUT_CONTENT_WIDTH : width);
        messagePart = new FlowPanel();
        headPart = createHeadPart();
        headPart.addStyleName(STYLE_CLASS_HEAD);
        bodyPart = new SimplePanel();
        Widget w = createContentPart();
        w.setWidth("100%");
        w.addStyleName(STYLE_CLASS_CONTENT);
        bodyPart.setWidget(w);
        bodyPart.addStyleName(STYLE_CLASS_BODY);
        footPart = createFootPart();
        footPart.addStyleName(STYLE_CLASS_FOOT);
        footPart.addStyleName(CLEAR_BOTH);

        if (widget != null) {
            setWidget(widget);
        }

        panel.add(headPart);
        headPart.setWidth("100%");
        if (withHiddableLabel) {
            hiddableLabel = new HiddableLabel();
            hiddableLabel.setStyleName(StyleUtils.MARGIN_BOT_5);
            panel.add(hiddableLabel);
        }
        panel.add(bodyPart);
        panel.add(footPart);

        initWidget(panel);
    }

    //--------------------------------
    // NASTAVENI ZAKLADNICH CASTI
    //--------------------------------
    /** Vycisti obsah */
    public void clear() {
        content.clear();
        content.add(getMessagePart());
        clearMessageBox();
    }

    /**
     * Vytvoreni zahlavi.
     * @default SimplePanel
     */
    protected Widget createHeadPart() {
        return new SimplePanel();
    }

    /**
     * Vytvoreni paticky.
     * @default SimplePanel
     */
    protected Widget createFootPart() {
        return new SimplePanel();
    }

    /**
     * Vytvoreni obsahovej casti. Message box je dobrovolan zalezitost a pro to
     * si musi sam formular pohlidat kam a jak ho da.
     * @default VerticalPanel + messagePart
     */
    protected Widget createContentPart() {
        content = new FlowPanel();
        content.add(getMessagePart());
        return content;
    }

    //--------------------------------
    // NASTAVENI VLASTNOSTI
    //--------------------------------
    /** Nastavi dynamicku sirku celeho formulara. */
    public void setDynamicWidth(boolean set) {
        panel.setWidth(set ? "100%" : LayoutManager.LAYOUT_CONTENT_WIDTH);
    }

    /** Nastaveni vysky wrapperu ktery obsahuje content */
    public void setBodyHeight(String height) {
        bodyPart.setHeight(height);
    }

    /** nastaveni sirky vlastniho obsahu */
    public void setBodyWidth(String width) {
        bodyPart.setWidth(width);
    }

    /** vraci sebe sama */
    protected SimpleFormPanel getSelf() {
        return this;
    }

    //--------------------------------
    // MESSAGE BOXY
    //--------------------------------
    /** Vycisti message box */
    public void clearMessageBox() {
        getMessagePart().clear();
    }

    /** vraci objekt pro zpravu chyby */
    public ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setStyleName(StyleUtils.MARGIN_BOT_10);
        getMessagePart().add(failMessage);
        return failMessage;
    }

    /** vraci objekt pro zpravu uspechu */
    public ConsysMessage getSuccessMessage() {
        return getSuccessMessage(false);
    }

    /** vraci objekt pro zpravu uspechu */
    public ConsysMessage getSuccessMessage(boolean wide) {
        ConsysMessage successMessage = ConsysMessage.getSuccess();
        successMessage.setStyleName(StyleUtils.MARGIN_BOT_10);
        successMessage.setWide(wide);
        getMessagePart().add(successMessage);
        return successMessage;
    }

    /** vraci label urceny k zobrazeni docasnych zprav */
    public HiddableLabel getHiddableLabel() {
        return hiddableLabel;
    }

    /** vraci pocet widgetu v content cati */
    public int getContentWidgetCount() {
        return content.getWidgetCount();
    }

    /** vraci widget na zadanem indexu v hlavnim panelu */
    public Widget getContentWidget(int index) {
        return content.getWidget(index);
    }

    /** 
     * Nastavi widget do contentu. Vsetky ostatne widgety zmaze. Chovanie ako
     * SimpleFormPanel. Automaticky pridava consys message
     */
    @Override
    public void setWidget(Widget widget) {
        content.clear();
        content.add(getMessagePart());
        content.add(widget);
    }

    /** prida widget do tela formu */
    public void addWidget(Widget widget) {
        content.add(widget);
    }

    /** zobrazi pres formular panel s cekacim obrazkem */
    public void showWaiting(boolean value) {
        if (waiting == null) {
            waiting = new Waiting(panel);
            SimplePanel waitingPart = new SimplePanel();
            waitingPart.setWidget(waiting);
            panel.add(waitingPart);
        }
        if (value) {
            waiting.show();
        } else {
            waiting.hide();
        }
    }

    public void showWaitingText(String text) {
        if (waiting != null) {
            waiting.showText(text);
        }
    }

    @Override
    public void actionStarted() {
        showWaiting(true);
    }

    @Override
    public void actionEnds() {
        showWaiting(false);
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    /** nastavuje skrytelny informacni text */
    public void setHiddableLabel(HiddableLabel hiddableLabel) {
        this.hiddableLabel = hiddableLabel;
    }

    /** vraci cast ktera obsahuje zpravy */
    public FlowPanel getMessagePart() {
        return messagePart;
    }

    public void addContentStyleName(String name) {
        content.addStyleName(name);
    }
}
