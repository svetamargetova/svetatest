package consys.common.gwt.client.ui.comp.search;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SearchDelegate<T> {

    public void didSelect(T item);
}
