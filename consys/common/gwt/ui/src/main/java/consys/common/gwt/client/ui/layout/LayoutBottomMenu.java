package consys.common.gwt.client.ui.layout;

import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.event.EventPartnerImageChanged;
import consys.common.gwt.client.ui.event.MenuItemEvent;
import consys.common.gwt.client.ui.event.PanelItemEvent;
import consys.common.gwt.client.ui.layout.menu.MenuItem;
import consys.common.gwt.client.ui.layout.menu.MenuItemUI;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Cast layoutu starajici se o menu pod obsahem
 * @author pepa
 */
public class LayoutBottomMenu extends Composite implements MenuItemEvent.Handler, PanelItemEvent.Handler, EventPartnerImageChanged.Handler {

    private static final Logger logger = LoggerFactory.getLogger(LayoutBottomMenu.class);
    // instance
    private static LayoutBottomMenu instance;
    // promenne
    private FlowPanel contentPanel;
    private SimplePanel footerEventImagePanel;

    private LayoutBottomMenu() {
        footerEventImagePanel = new SimplePanel();
        footerEventImagePanel.setVisible(false);

        SimplePanel separatorBottom = new SimplePanel();
        separatorBottom.setStyleName(ResourceUtils.system().css().bottomMenuSeparator2());

        Image powered = new Image(ResourceUtils.system().poweredByTakeplace());
        powered.setAltText("Powered by Takeplace");

        FlowPanel takeplacePanel = new FlowPanel();
        takeplacePanel.setWidth("110px");
        takeplacePanel.addStyleName(StyleUtils.FLOAT_LEFT);
        takeplacePanel.add(powered);

        contentPanel = new FlowPanel();
        contentPanel.addStyleName(ResourceUtils.css().bottomMenu());

        HTML copyrightTime = new HTML("2009&nbsp;&mdash;&nbsp;2013");
        copyrightTime.addStyleName(StyleUtils.FLOAT_RIGHT);
        copyrightTime.addStyleName(StyleUtils.FONT_BOLD);
        copyrightTime.addStyleName(StyleUtils.FONT_11PX);
        HTML copyright = new HTML("&#169;&nbsp;<a title=\"ACEMCEE\" href=\"http://acemcee.com\" class=\""
                + ResourceUtils.system().css().bottomAcemceeLink() + "\">ACEMCEE</a>");
        copyright.addStyleName(StyleUtils.FLOAT_RIGHT);
        copyright.addStyleName(StyleUtils.MARGIN_LEFT_10);
        copyright.addStyleName(StyleUtils.FONT_11PX);
        HTML copyrightReserved = new HTML("All rights reserved");
        copyrightReserved.addStyleName(StyleUtils.FLOAT_RIGHT);
        copyrightReserved.addStyleName(StyleUtils.MARGIN_LEFT_10);
        copyrightReserved.addStyleName(StyleUtils.FONT_11PX);

        FlowPanel copyrightPanel = new FlowPanel();
        copyrightPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
        copyrightPanel.add(copyrightReserved);
        copyrightPanel.add(copyright);
        copyrightPanel.add(copyrightTime);
        copyrightPanel.add(StyleUtils.clearDiv());

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(StyleUtils.MARGIN_VER_3);
        panel.add(takeplacePanel);
        panel.add(contentPanel);
        panel.add(copyrightPanel);
        panel.add(StyleUtils.clearDiv());

        FlowPanel wrapper = new FlowPanel();
        wrapper.add(footerEventImagePanel);
        wrapper.add(separatorBottom);
        wrapper.add(panel);

        initWidget(wrapper);
    }

    public void showEventFooterImage(String imgUuid) {
        if (StringUtils.isNotBlank(imgUuid)) {
            footerEventImagePanel.setVisible(true);
            Image image = new Image(URLFactory.loadImageUrl(imgUuid));
            footerEventImagePanel.setWidget(image);
        }
    }

    public void hideEventFooterImage() {
        footerEventImagePanel.clear();
        footerEventImagePanel.setVisible(false);
    }

    /** vraci instanci LayoutBottomMenu */
    public static LayoutBottomMenu get() {
        if (instance == null) {
            instance = new LayoutBottomMenu();
        }
        return instance;
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(MenuItemEvent.TYPE, this);
        EventBus.get().addHandler(PanelItemEvent.TYPE, this);
        EventBus.get().addHandler(EventPartnerImageChanged.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(MenuItemEvent.TYPE, this);
        EventBus.get().removeHandler(PanelItemEvent.TYPE, this);
        EventBus.get().removeHandler(EventPartnerImageChanged.TYPE, this);
    }

    /**
     * prida jednu polozku do menu
     * @param position pozice na kterou se ma polozka menu umistit (vychozi hodnota je -1, polozka se v klada na konec)
     */
    private void addItem(MenuItem menuItem, int position) {
        if (contentPanel.getWidgetCount() > 0) {
            Label separator = StyleUtils.getStyledLabel("|", ResourceUtils.system().css().bottomLinkSeparator());
            if (position == -1) {
                contentPanel.add(separator);
                contentPanel.add(new MenuItemUI(menuItem, false));
            } else {
                contentPanel.insert(separator, position);
                contentPanel.insert(new MenuItemUI(menuItem, false), position + 1);
            }
        } else {
            if (position == -1) {
                contentPanel.add(new MenuItemUI(menuItem, false));
            } else {
                contentPanel.insert(new MenuItemUI(menuItem, false), position);
            }
        }
    }

    @Override
    public void onMenuItem(MenuItemEvent event) {
        switch (event.getAction()) {
            case ADD:
                addItem(event.getMenuItem(), event.getPosition());
                logger.debug("+ item " + event.getMenuItem().getTitle());
                break;
            case CLEAR:
                contentPanel.clear();
                logger.debug("cleared");
                break;
        }
    }

    @Override
    public void onPanelItem(PanelItemEvent event) {
        switch (event.getAction()) {
            case CLEAR:
            case REMOVE_BANNER:
                hideEventFooterImage();
        }
    }

    @Override
    public void onChange(EventPartnerImageChanged event) {
        if (event.getType().equalsIgnoreCase("FOOTER")) {
            showEventFooterImage(event.getUuid());
        }
    }
}
