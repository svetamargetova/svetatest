package consys.common.gwt.client.ui.comp.panel;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.allen_sauer.gwt.dnd.client.drop.AbstractPositioningDropController;
import com.allen_sauer.gwt.dnd.client.util.CoordinateLocation;
import com.allen_sauer.gwt.dnd.client.util.DOMUtil;
import com.allen_sauer.gwt.dnd.client.util.DragClientBundle;
import com.allen_sauer.gwt.dnd.client.util.LocationWidgetComparator;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.InsertPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.panel.element.OrderPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.OrderPanelItemUI;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;

/**
 * Panel pro vyber poradi polozek
 * @author pepa
 */
public class OrderPanel<T extends OrderPanelItem> extends FlowPanel {

    // konstanty
    public static final String DEFAULT_WIDTH = "330px";
    public static final int DEFAULT_BOUNDARY_WIDTH_INT = 300;
    public static final String DEFAULT_BOUNDARY_WIDTH = DEFAULT_BOUNDARY_WIDTH_INT + "px";
    private static final String DEFAULT_ORDER_WIDTH = "25px";
    /** Label for IE quirks mode workaround */
    private static final Label DUMMY_LABEL_IE_QUIRKS_MODE_OFFSET_HEIGHT = new Label("x");
    // komponenty
    private AbsolutePanel boundaryPanel;
    // data
    private ArrayList<OrderPanelListener<T>> listeners;
    private OrderPanelItemUI<T> lastItem = null;
    private int lastOldPosition = -1;
    private int lastNewPosition = -1;
    private OrderPanelDragController dragController;
    /* Hodnota o ktoru sa upravi vysledna pozicia. Pozicia moze byt na serveru pocitana od 0 ako aj od 1 */
    private int finalPositionOffset = 1;

    public OrderPanel() {
        this(new ArrayList<T>());
    }

    public OrderPanel(ArrayList<T> items) {
        setWidth(DEFAULT_WIDTH);
        listeners = new ArrayList<OrderPanelListener<T>>();

        FlowPanel orderPanel = new FlowPanel();
        orderPanel.setWidth(DEFAULT_ORDER_WIDTH);
        orderPanel.addStyleName(StyleUtils.FLOAT_LEFT);
        orderPanel.addStyleName(StyleUtils.MARGIN_RIGHT_5);

        for (int i = 0; i < items.size(); i++) {
            HTML html = new HTML((i + 1) + ".");
            html.setSize(DEFAULT_ORDER_WIDTH, OrderPanelItemUI.DEFAULT_HEIGHT);
            html.addStyleName(StyleUtils.ALIGN_RIGHT);
            orderPanel.add(html);
        }

        boundaryPanel = new AbsolutePanel();
        boundaryPanel.setSize(DEFAULT_BOUNDARY_WIDTH, (items.size() * OrderPanelItemUI.DEFAULT_HEIGHT_INT) + "px");
        boundaryPanel.addStyleName(StyleUtils.FLOAT_LEFT);

        add(orderPanel);
        add(boundaryPanel);
        add(StyleUtils.clearDiv());

        OrderPanelDropController dropController = new OrderPanelDropController(boundaryPanel);

        dragController = new OrderPanelDragController(boundaryPanel, false);
        dragController.setDropController(dropController);

        for (T i : items) {
            OrderPanelItemUI<T> item = new OrderPanelItemUI<T>(i, this);
            boundaryPanel.add(item);
            dragController.makeDraggable(item, item.getHandler());
        }
    }

    /** prida listener */
    public void addListener(OrderPanelListener<T> listener) {
        listeners.add(listener);
    }

    /** odebere listener */
    public void removeListener(OrderPanelListener<T> listener) {
        listeners.remove(listener);
    }

    /** pokud lze, posune polozku o jednu pozici nahoru */
    public void moveUp(OrderPanelItemUI<T> ui) {
        if (boundaryPanel.getWidgetCount() == 1) {
            // pokud je polozka jen jedna nic nedelat
            return;
        }
        int oldPosition = getOrderPosition(ui, true, true);
        int newPosition = getOrderPosition(ui, false, true);
        if (oldPosition != newPosition) {
            move(ui, oldPosition, newPosition, false);
            notifyOrderChange(ui.getObject(), oldPosition, newPosition);
        }
    }

    /** pokud lze, posune polozku o jednu pozici dolu */
    public void moveDown(OrderPanelItemUI<T> ui) {
        if (boundaryPanel.getWidgetCount() == 1) {
            // pokud je polozka jen jedna nic nedelat
            return;
        }
        int oldPosition = getOrderPosition(ui, true, false);
        int newPosition = getOrderPosition(ui, false, false);
        if (oldPosition != newPosition) {
            move(ui, oldPosition, newPosition, false);
            notifyOrderChange(ui.getObject(), oldPosition, newPosition);
        }
    }

    /** posledni prohozeni vrati zpet */
    public void back() {
        back(false);
    }

    /**
     * posledni prohozeni vrati zpet
     * @param fireListeners pokud maji byt uvedomeny listenery tak hodnota true jinak false
     */
    public void back(boolean fireListeners) {
        if (lastItem != null && lastOldPosition != -1 && lastNewPosition != -1) {
            move(lastItem, lastNewPosition, lastOldPosition, true);
            if (fireListeners) {
                notifyOrderChange(lastItem.getObject(), lastNewPosition, lastOldPosition);
            }
        }
    }

    /** 
     * vraci pozici poradi 
     * @param old vraci puvodni pozici pokud je true, jinak novou
     * @param up uplatnuje se pouze pokud se jedna o pozadavek na novou pozici, pokud je true jedna se o smer nahoru
     * v opacnem pripadu se jedna o pozici dolu
     */
    private int getOrderPosition(OrderPanelItemUI item, boolean old, boolean up) {
        int count = boundaryPanel.getWidgetCount();
        int oldPos = boundaryPanel.getWidgetIndex(item);
        if (old) {
            return oldPos;
        } else {
            if (oldPos > 0 && oldPos < count - 1) {
                // neni ani prvni, ani posledni
                return oldPos + (up ? -1 : 1);
            } else {
                if (oldPos == 0 && up) {
                    // je prvni a nahoru nejde
                    return oldPos;
                } else if (oldPos == 0 && !up) {
                    // je prvni a dolu jde
                    return oldPos + 1;
                } else {
                    if (up) {
                        // je posledni a nahoru jde
                        return oldPos - 1;
                    } else {
                        // je posledni a dolu nejde
                        return oldPos;
                    }
                }
            }
        }
    }

    /** 
     * pokud neni v zadanem smeru na poslednim miste, prohodi se se sousedem<br>
     * POZOR: testováno pouze mezi sousednimi polozkami!!!
     * @param up pokud je true jedna se o smer nahoru, pokud je false jedna se o smer dolu
     */
    private void move(OrderPanelItemUI item, int oldPosition, int newPosition, boolean back) {
        if (oldPosition > newPosition) {
            boundaryPanel.insert(item, newPosition);
        } else {
            boundaryPanel.insert(item, newPosition + 1);
        }

        if (back) {
            lastItem = null;
            lastOldPosition = -1;
            lastNewPosition = -1;
        } else {
            lastItem = item;
            lastOldPosition = oldPosition;
            lastNewPosition = newPosition;
        }
    }

    /** @return the finalPositionOffset */
    public int getFinalPositionOffset() {
        return finalPositionOffset;
    }

    /** @param finalPositionOffset the finalPositionOffset to set */
    public void setFinalPositionOffset(int finalPositionOffset) {
        this.finalPositionOffset = finalPositionOffset;
    }

    /** oznami zmenu zaregistrovanym listenerum s pozici posunutou o offset */
    public void notifyOrderChange(T object, int oldPosition, int newPosition) {
        for (OrderPanelListener<T> l : listeners) {
            l.onOrderChange(object, oldPosition + getFinalPositionOffset(), newPosition + getFinalPositionOffset());
        }
    }

    /** interface pro listener zmen order panelu */
    public interface OrderPanelListener<T> {

        /** vola se pri zmene poradi nektere polozky, v parametrech se predava objekt polozky, puvodni a nova pozice */
        public void onOrderChange(T object, int oldPosition, int newPosition);
    }

    private class OrderPanelDragController extends PickupDragController {

        // data
        private OrderPanelItemUI<T> draggable;
        private int draggableOriginalOrder;
        private OrderPanelDropController dropController;
        private boolean veto;

        public OrderPanelDragController(AbsolutePanel boundaryPanel, boolean allowDroppingOnBoundaryPanel) {
            super(boundaryPanel, allowDroppingOnBoundaryPanel);
            setBehaviorMultipleSelection(false);
            veto = false;
        }

        public void setDropController(OrderPanelDropController dropController) {
            if (this.dropController != null) {
                unregisterDropController(this.dropController);
                this.dropController = null;
            }
            registerDropController(dropController);
            this.dropController = dropController;
        }

        private int getOrderPosition() {
            for (int i = 0; i < boundaryPanel.getWidgetCount(); i++) {
                if (draggable == boundaryPanel.getWidget(i)) {
                    return i;
                }
            }
            return -1;
        }

        @Override
        public void dragStart() {
            draggable = (OrderPanelItemUI<T>) context.draggable;
            draggableOriginalOrder = getOrderPosition();
            super.dragStart();
        }

        @Override
        public void dragEnd() {
            super.dragEnd();

            // odstranime positioner pomoci dropcontrolleru
            if (dropController != null) {
                dropController.removePositioner();
            }

            // skoncilo dragovani, zjistime stav a dame vedet listenerum
            if (draggable != null && !veto) {
                int newPosition = getOrderPosition();

                if (draggableOriginalOrder != newPosition) {
                    lastItem = draggable;
                    lastOldPosition = draggableOriginalOrder;
                    lastNewPosition = newPosition;
                    notifyOrderChange(draggable.getObject(), draggableOriginalOrder, newPosition);
                }
                draggable = null;
            }
        }

        @Override
        public void previewDragEnd() throws VetoDragException {
            try {
                super.previewDragEnd();
            } catch (VetoDragException ex) {
                veto = true;
                throw ex;
            }
            veto = false;
        }

        @Override
        protected void restoreSelectedWidgetsLocation() {
            if (dropController != null) {
                dropController.removePositioner();
            }
            boundaryPanel.insert(draggable, draggableOriginalOrder);
        }
    }

    /** dropController pro vkladani polozek */
    private class OrderPanelDropController extends AbstractPositioningDropController {

        private final InsertPanel dropTarget;
        private int dropIndex;
        private Widget positioner = null;

        public OrderPanelDropController(InsertPanel dropTarget) {
            super((Panel) dropTarget);
            this.dropTarget = dropTarget;
        }

        @Override
        public void onDrop(DragContext context) {
            assert dropIndex != -1 : "Should not happen after onPreviewDrop did not veto";
            for (Widget widget : context.selectedWidgets) {
                dropTarget.insert(widget, dropIndex);
                // Works with and without drag proxy
                dropIndex = dropTarget.getWidgetIndex(widget) + 1;
            }
            super.onDrop(context);
        }

        @Override
        public void onEnter(DragContext context) {
            super.onEnter(context);
            if (positioner == null) {
                positioner = newPositioner(context);
            }
            int targetIndex = DOMUtil.findIntersect(dropTarget, new CoordinateLocation(context.mouseX,
                    context.mouseY), getLocationWidgetComparator());
            dropTarget.insert(positioner, targetIndex);
        }

        @Override
        public void onLeave(DragContext context) {
            super.onLeave(context);
        }

        @Override
        public void onMove(DragContext context) {
            super.onMove(context);
            int targetIndex = DOMUtil.findIntersect(dropTarget, new CoordinateLocation(context.mouseX,
                    context.mouseY), getLocationWidgetComparator());

            // check that positioner not already in the correct location
            int positionerIndex = dropTarget.getWidgetIndex(positioner);

            if (positionerIndex != targetIndex && (positionerIndex != targetIndex - 1 || targetIndex == 0)) {
                if (positionerIndex == 0 && dropTarget.getWidgetCount() == 1) {
                    // do nothing, the positioner is the only widget
                } else if (targetIndex == -1) {
                    // outside drop target, so remove positioner to indicate a drop will not happen
                    positioner.removeFromParent();
                } else {
                    dropTarget.insert(positioner, targetIndex);
                }
            }
        }

        @Override
        public void onPreviewDrop(DragContext context) throws VetoDragException {
            dropIndex = dropTarget.getWidgetIndex(positioner);
            if (dropIndex == -1) {
                throw new VetoDragException();
            }
            super.onPreviewDrop(context);
        }

        protected LocationWidgetComparator getLocationWidgetComparator() {
            return LocationWidgetComparator.BOTTOM_HALF_COMPARATOR;
        }

        protected Widget newPositioner(DragContext context) {
            // Use two widgets so that setPixelSize() consistently affects dimensions
            // excluding positioner border in quirks and strict modes
            SimplePanel outer = new SimplePanel();
            outer.addStyleName(DragClientBundle.INSTANCE.css().positioner());

            // place off screen for border calculation
            RootPanel.get().add(outer, -500, -500);

            // Ensure IE quirks mode returns valid outer.offsetHeight, and thus valid
            // DOMUtil.getVerticalBorders(outer)
            outer.setWidget(DUMMY_LABEL_IE_QUIRKS_MODE_OFFSET_HEIGHT);

            int width = 0;
            int height = 0;
            for (Widget widget : context.selectedWidgets) {
                width = Math.max(width, widget.getOffsetWidth());
                height += widget.getOffsetHeight();
            }

            SimplePanel inner = new SimplePanel();
            inner.setPixelSize(width - DOMUtil.getHorizontalBorders(outer), height - DOMUtil.getVerticalBorders(outer));

            outer.setWidget(inner);

            return outer;
        }

        /** odstrani positioner */
        public void removePositioner() {
            if (positioner != null) {
                positioner.removeFromParent();
                positioner = null;
            }
        }
    }

    // puvodne neplanovana moznost vkladat vlasti itemy k pretahovani
    /** vraci pocet ui polozek */
    public int getItemUICount() {
        return boundaryPanel.getWidgetCount();
    }

    public <I extends OrderPanelItemUI> void addItemUI(I item) {
        boundaryPanel.add(item);
        dragController.makeDraggable(item, item.getHandler());
        refreshSize();
    }

    /** vraci polozku na zadane pozici */
    public Widget getItemUI(int index) {
        return boundaryPanel.getWidget(index);
    }

    /** odstrani polozku */
    public <I extends OrderPanelItemUI> void removeItemUI(I item) {
        dragController.makeNotDraggable(item);
        boundaryPanel.remove(item);
        refreshSize();
    }

    /** provede prepocitani velikosti panelu v zavislosti na poctu polozek */
    private void refreshSize() {
        boundaryPanel.setSize(DEFAULT_BOUNDARY_WIDTH, (getItemUICount() * OrderPanelItemUI.DEFAULT_HEIGHT_INT) + "px");
    }
}
