package consys.common.gwt.client.ui.comp.action;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.action.ActionImageBig.ActionImageBigType;

/**
 * Velke akcni tlacitko podle noveho navrhu
 * @author pepa
 */
public class ActionImageBig extends AbstractActionImage<ActionImageBigType> {

    /** enum typu */
    public enum ActionImageBigType {

        GREEN, RED, BLUE, CLEAR;
        private static final long serialVersionUID = 5725464050200333798L;
    }

    private ActionImageBig(String text, ActionImageBigType type) {
        super(text, type, ActionResources.INSTANCE.css().actionImageBig());
    }

    @Override
    protected String leftPartStyleName() {
        return ActionResources.INSTANCE.css().actionImageBigLeft();
    }

    @Override
    protected String centerPartStyleName() {
        return ActionResources.INSTANCE.css().actionImageBigCenter();
    }

    @Override
    protected String rightPartStyleName() {
        return ActionResources.INSTANCE.css().actionImageBigRight();
    }

    @Override
    protected void createCenter(FocusPanel center) {
        center.setWidget(new Label(getText()));
    }

    @Override
    protected void setContentStyleName(FlowPanel content) {
        content.setStyleName(ActionResources.INSTANCE.css().actionImageBigContent());
        switch (getType()) {
            case GREEN:
                content.addStyleName(ActionResources.INSTANCE.css().bigGreen());
                break;
            case RED:
                content.addStyleName(ActionResources.INSTANCE.css().bigRed());
                break;
            case BLUE:
                content.addStyleName(ActionResources.INSTANCE.css().bigBlue());
                break;
            case CLEAR:
                content.addStyleName(ActionResources.INSTANCE.css().bigClear());
                break;
        }
    }

    @Override
    protected void setContentDisabledStyleName(FlowPanel content) {
        content.setStyleName(ActionResources.INSTANCE.css().actionImageBigContent());
        content.addStyleName(ActionResources.INSTANCE.css().disabled());
    }

    @Override
    protected void setContentEnabledStyleName(FlowPanel content) {
        // styl je stejny jako vychozi
        setContentStyleName(content);
    }

    /** vytvori velke zelene tlacitko se zadanym textem */
    public static ActionImageBig green(String text) {
        return new ActionImageBig(text, ActionImageBigType.GREEN);
    }

    /** vytvori velke cervene tlacitko se zadanym textem */
    public static ActionImageBig red(String text) {
        return new ActionImageBig(text, ActionImageBigType.RED);
    }

    /** vytvori velke modre tlacitko se zadanym textem */
    public static ActionImageBig blue(String text) {
        return new ActionImageBig(text, ActionImageBigType.BLUE);
    }

    /** vytvori velke tlacitko (modry okraj bily podklad) se zadanym textem */
    public static ActionImageBig clear(String text) {
        return new ActionImageBig(text, ActionImageBigType.CLEAR);
    }
}
