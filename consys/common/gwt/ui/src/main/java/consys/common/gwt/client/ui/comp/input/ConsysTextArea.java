package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.comp.wrapper.InputTextWrapper;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 * Vlastni implementace TextArea s moznosti omezeni maximalniho poctu znaku
 * @author pepa
 */
public class ConsysTextArea extends Composite implements ValidableComponent, HasIdentifier {

    // konstanty
    public static final int WIDTH = 270;
    public static final int HEIGHT = 80;
    public static final int UNLIMITED = 0;
    // komponenty
    private FlowPanel panel;
    private Label lengthLabel;
    private Label maxLabel;
    protected final TextArea area;
    private InputTextWrapper wrapper;
    // data
    private String emptyText;
    protected String fieldName;
    protected final int min;
    protected final int max;
    protected final boolean showInfo;

    public ConsysTextArea(int min, int max) {
        this(min, max, WIDTH, HEIGHT, true, "");
    }

    public ConsysTextArea(int min, int max, String fieldName) {
        this(min, max, WIDTH, HEIGHT, true, fieldName);
    }

    public ConsysTextArea(int min, int max, boolean showInfo, String fieldName) {
        this(min, max, WIDTH, HEIGHT, showInfo, fieldName);
    }

    public ConsysTextArea(int max, int width, int height, String fieldName) {
        this(0, max, width, height, true, fieldName);
    }

    public ConsysTextArea(int max, int width, int height, boolean showInfo, String fieldName) {
        this(0, max, width, height, showInfo, fieldName);
    }

    public ConsysTextArea(int min, final int max, int width, int height, boolean showInfo, String fieldName) {
        this.min = min;
        this.max = max;
        this.showInfo = showInfo;
        this.fieldName = fieldName;

        InputResources.INSTANCE.css().ensureInjected();

        area = new TextArea();
        area.setWidth((width - (2 * InputTextWrapper.BORDER_WIDTH)) + "px");
        area.setHeight(height + "px");

        wrapper = new InputTextWrapper(area, width);

        panel = new FlowPanel();
        panel.setStyleName(InputResources.INSTANCE.css().consysTextArea());
        panel.setWidth(width + "px");
        panel.add(wrapper);

        showInfo();

        initWidget(panel);
    }

    public boolean isUnlimited(){
        return max==UNLIMITED;
    }
    
    private void showInfo() {
        if (showInfo) {
            lengthLabel = new Label(startLengthValue());

            String maxText = "/" + (isUnlimited() ? UIMessageUtils.c.const_unlimited() : max);
            maxLabel = new Label(maxText);

            FlowPanel infoPanel = new FlowPanel();
            infoPanel.setStyleName(InputResources.INSTANCE.css().consysTextAreaInfoPanel());
            infoPanel.add(lengthLabel);
            infoPanel.add(maxLabel);
            infoPanel.add(StyleUtils.clearDiv());

            panel.add(infoPanel);
            panel.add(StyleUtils.clearDiv());

            area.addKeyUpHandler(areaKeyUpHandler());
        }
    }

    protected String startLengthValue() {
        return "0";
    }

    protected KeyUpHandler areaKeyUpHandler() {
        return new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                String text = area.getText();
                int length = text.length();
                if (!isUnlimited() && length > max) {
                    length = max;
                    int scrolled = area.getElement().getScrollTop();
                    area.setText(text.substring(0, max));
                    area.getElement().setScrollTop(scrolled);
                }
                lengthLabel.setText(String.valueOf(length));
            }
        };
    }

    protected Label lengthLabel() {
        return lengthLabel;
    }

    /** enabluje/disabluje komponentu */
    public void setEnabled(boolean value) {
        area.setEnabled(value);
        if (lengthLabel != null) {
            if (value) {
                lengthLabel.removeStyleName(InputResources.INSTANCE.css().disabled());
                maxLabel.removeStyleName(InputResources.INSTANCE.css().disabled());
            } else {
                StyleUtils.addStyleNameIfNotSet(lengthLabel, InputResources.INSTANCE.css().disabled());
                StyleUtils.addStyleNameIfNotSet(maxLabel, InputResources.INSTANCE.css().disabled());
            }
        }
    }

    /** vraci text */
    public String getText() {
        String result = area.getText();
        result = result == null ? "" : result.trim();
        if (emptyText != null && emptyText.equals(result)) {
            return "";
        }
        return result;
    }

    /** nastavuje text */
    public void setText(String text) {
        if (emptyText != null) {
            area.setText(text == null || text.isEmpty() ? emptyText : text);
        } else {
            area.setText(text);
        }
        if (showInfo) {
            lengthLabel.setText(String.valueOf(text == null ? 0 : text.length()));
        }
    }

    @Override
    @Deprecated
    /** pouzivat nastavovani sirky jen v konstruktoru ! */
    public void setWidth(String width) {
        throw new UnsupportedOperationException("Nastavit sirku lze jen v konstruktoru!");
    }

    @Override
    public void setHeight(String height) {
        area.setHeight(height);
    }

    /** prida / zrusi focus na komponentu */
    public void setFocus(boolean focused) {
        area.setFocus(focused);
    }

    /** prida blur handler */
    public HandlerRegistration addBlurHandler(BlurHandler handler) {
        return area.addBlurHandler(handler);
    }

    /** prida value change handler - zavola az po opusteni */
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return area.addValueChangeHandler(handler);
    }

    /** prida */
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return area.addChangeHandler(handler);
    }

    /** prida key up hadnler*/
    public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
        return area.addKeyUpHandler(handler);
    }

    /** vraci text, ktery se zobrazuje pokud neni v komponenty zadany zadny text */
    protected String getEmptyText() {
        return emptyText;
    }

    /**
     * nastavi text, ktery se ma zobrazit v prazdne komponente,
     * pokud je tento text nastaven, meni se i chovani metody getText
     * (jestli se zadany text shoduje s textem v komponente, vraci se prezdny string!)
     */
    public void setEmptyText(String text) {
        if (emptyText == null) {
            area.addFocusHandler(new FocusHandler() {

                @Override
                public void onFocus(FocusEvent event) {
                    if (area.getText().trim().equals(emptyText)) {
                        area.setText("");
                    }
                }
            });
            area.addBlurHandler(new BlurHandler() {

                @Override
                public void onBlur(BlurEvent event) {
                    if (area.getText().trim().isEmpty()) {
                        area.setText(emptyText);
                    }
                }
            });
        }
        emptyText = text;
        if (area.getText().trim().isEmpty()) {
            area.setText(emptyText);
        }
    }

    /** nastavi nazev komponenty (pro odesilani ve formulari) */
    public void setName(String name) {
        area.setName(name);
    }

    /** nastavi akci, ktera se ma spustit pri prechodu do editacniho zobrazeni */
    public void setOnEnterToEdit(ConsysAction onEnterToEditAction) {
        wrapper.setOnEnterToEditAction(onEnterToEditAction);
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        String text = getText();
        if (max == UNLIMITED) {
            if (text.length() < min) {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
                return false;
            }
            return true;
        }
        int result = ValidatorUtils.isLenght(text, min, max);
        if (result != 0) {
            if (!ValidatorUtils.isValidString(text)) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(fieldName));
            } else {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
            }
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        area.removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        String style = area.getStyleName();
        if (!style.contains(StyleUtils.BACKGROUND_GRAY)) {
            area.addStyleName(StyleUtils.BACKGROUND_GRAY);
        }
    }

    @Override
    public String getIdentifier() {
        return fieldName;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.fieldName = identifier;
    }

    /** prida focus handler */
    public HandlerRegistration addFocusHandler(FocusHandler handler) {
        return area.addFocusHandler(handler);
    }
}
