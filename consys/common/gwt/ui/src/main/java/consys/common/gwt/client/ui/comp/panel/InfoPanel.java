package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Panel ktory jednodtne zobrazuje informacie velkym pismom do stredu formulara.
 *
 * @author palo
 */
public class InfoPanel extends Composite {

    static {
        PanelResources.INSTANCE.css().ensureInjected();
    }

    public InfoPanel(String text) {
        this(text, null, null);
    }

    public InfoPanel(String text, String actionText, final ConsysAction action) {

        final Label label = new Label(text);
        label.setStyleName(PanelResources.INSTANCE.css().infoPanelLabel());

        final FlowPanel fp = new FlowPanel();
        fp.setStyleName(PanelResources.INSTANCE.css().infoPanelLabel());
        fp.add(StyleUtils.clearDiv("10px"));
        fp.add(label);
        if (action != null && actionText != null && !actionText.isEmpty()) {
            fp.add(StyleUtils.clearDiv("10px"));
            ActionLabel al = new ActionLabel(actionText, PanelResources.INSTANCE.css().action());
            al.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    action.run();
                }
            });
            fp.add(al);
        }
        fp.add(StyleUtils.clearDiv("20px"));

        initWidget(fp);
    }
}
