package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;

/**
 * Panel pro prohazovani dvou obsahu
 * @author pepa
 */
public class SwapPanel extends SimplePanel implements ValidableComponent {

    // data
    private Widget defaultWidget;
    private Widget swapWidget;
    private boolean isDefault;

    public SwapPanel() {
        this(null, null);
    }

    public SwapPanel(Widget defaultWidget, Widget swapWidget) {
        super();
        this.defaultWidget = defaultWidget;
        this.swapWidget = swapWidget;

        setWidget(defaultWidget);
        isDefault = true;
    }

    /** vraci vychozi widget */
    public Widget getDefaultWidget() {
        return defaultWidget;
    }

    /** nastavuje vychozi widget */
    public void setDefaultWidget(Widget defaultWidget) {
        this.defaultWidget = defaultWidget;
        if (isDefault) {
            setWidget(defaultWidget);
        }
    }

    /** nastavuj swapovaci widget */
    public Widget getSwapWidget() {
        return swapWidget;
    }

    /** vraci swapovaci widget */
    public void setSwapWidget(Widget swapWidget) {
        this.swapWidget = swapWidget;
        if (!isDefault) {
            setWidget(swapWidget);
        }
    }

    /** prohodi obsahy */
    public void swap() {
        setWidget(isDefault ? swapWidget : defaultWidget);
        isDefault = !isDefault;
    }

    /** @return true pokud je zobrazen vychozi widget */
    public boolean isShowDefaultWidget() {
        return isDefault;
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        return true;
    }

    @Override
    public void onSuccess() {
    }

    @Override
    public void onFail() {
    }
}
