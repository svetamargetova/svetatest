package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

/**
 *
 * @author palo
 */
public interface WrapperResources extends ClientBundle {

    public static final WrapperResources INSTANCE = GWT.create(WrapperResources.class);

    /** css styl pro wrapper komponenty */
    @Source("consys/common/gwt/client/ui/comp/wrapper/wrapper.css")
    public WrapperCss css();

    /*------------------------------------------------------------------------*/
    @Source("consys/common/gwt/client/ui/img/wrapper/panelWrapperContent.png")
    @ImageResource.ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource panelWrapperContent();

    @Source("consys/common/gwt/client/ui/img/wrapper/panelWrapperBottom.png")
    public ImageResource panelWrapperBottom();

    @Source("consys/common/gwt/client/ui/img/wrapper/panelWrapperTop.png")
    public ImageResource panelWrapperTop();

    // textarea wrapper
    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperTopLeft.png")
    public ImageResource textAreaWrapperTopLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperTopCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperTopCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperTopRight.png")
    public ImageResource textAreaWrapperTopRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperCenterLeft.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperCenterLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperCenterRight.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperCenterRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperBottomLeft.png")
    public ImageResource textAreaWrapperBottomLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperBottomCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperBottomCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperBottomRight.png")
    public ImageResource textAreaWrapperBottomRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverTopLeft.png")
    public ImageResource textAreaWrapperOverTopLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverTopCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperOverTopCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverTopRight.png")
    public ImageResource textAreaWrapperOverTopRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverCenterLeft.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperOverCenterLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverCenterRight.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperOverCenterRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverBottomLeft.png")
    public ImageResource textAreaWrapperOverBottomLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverBottomCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperOverBottomCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperOverBottomRight.png")
    public ImageResource textAreaWrapperOverBottomRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalTopLeft.png")
    public ImageResource textAreaWrapperNormalTopLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalTopCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperNormalTopCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalTopRight.png")
    public ImageResource textAreaWrapperNormalTopRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalCenterLeft.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperNormalCenterLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalCenterRight.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperNormalCenterRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalBottomLeft.png")
    public ImageResource textAreaWrapperNormalBottomLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalBottomCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperNormalBottomCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperNormalBottomRight.png")
    public ImageResource textAreaWrapperNormalBottomRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledTopLeft.png")
    public ImageResource textAreaWrapperDisabledTopLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledTopCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperDisabledTopCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledTopRight.png")
    public ImageResource textAreaWrapperDisabledTopRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledCenterLeft.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperDisabledCenterLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledCenterRight.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource textAreaWrapperDisabledCenterRight();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledBottomLeft.png")
    public ImageResource textAreaWrapperDisabledBottomLeft();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledBottomCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource textAreaWrapperDisabledBottomCenter();

    @Source("consys/common/gwt/client/ui/img/wrapper/textAreaWrapperDisabledBottomRight.png")
    public ImageResource textAreaWrapperDisabledBottomRight();
}
