package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;

/**
 * Event oznamujici, ze byla vybrana nasledujici komponenta
 * @author pepa
 */
public class NextComponentEvent extends GwtEvent<NextComponentEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private Widget component;

    public NextComponentEvent(Widget component) {
        this.component = component;
    }

    public Widget getComponent() {
        return component;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onNextComponent(this);
    }

    /** interface eventu */
    public interface Handler<I> extends EventHandler {

        public void onNextComponent(NextComponentEvent event);
    }
}
