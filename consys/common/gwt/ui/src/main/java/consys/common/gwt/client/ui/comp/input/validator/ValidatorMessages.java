package consys.common.gwt.client.ui.comp.input.validator;

import com.google.gwt.i18n.client.Messages;

/**
 * abecedne serazene zpravy
 * @author pepa
 */
public interface ValidatorMessages extends Messages {
    
    String fieldMustBeInRange(Number low, Number high);
    
}
