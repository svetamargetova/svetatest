package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class ConsysBaseWrapperWithBorderFace extends ConsysWrapper {

    public ConsysBaseWrapperWithBorderFace() {
        super();
    }

    public ConsysBaseWrapperWithBorderFace(Widget widget) {
        super(widget);
    }

    public ConsysBaseWrapperWithBorderFace(Widget widget, int settings) {
        super(widget, settings);
    }

    @Override
    public WrapperFace getWrappertFace() {
        Image bottomLeft = new Image(ResourceUtils.system().corGraySBl());
        Image bottomRight = new Image(ResourceUtils.system().corGraySBr());
        Image topRight = new Image(ResourceUtils.system().corGraySTr());
        Image topLeft = new Image(ResourceUtils.system().corGraySTl());
        WrapperFace face = new WrapperFace(new Image[]{bottomLeft, bottomRight, topRight, topLeft}, 6, StyleUtils.BACKGROUND_GRAY);
        face.setBorderStyles(new String[]{ResourceUtils.system().css().corGrayBottom(), null, null, null});
        return face;
    }
}
