package consys.common.gwt.client.ui.utils;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.abst.RU;

/**
 *
 * @author Palo
 */
public class FormPanelUtils implements CssStyles {

    public static ActionLabel getEditModeButton(ClickHandler handler) {
        ActionLabel edit = new ActionLabel(UIMessageUtils.c.const_edit(), StyleUtils.Assemble(MARGIN_LEFT_20, FONT_10PX));
        edit.setClickHandler(handler);
        return edit;
    }

    /**
     * Vrati ActionLabel pouzivany k prechodu do editcneho modu
     *
     * @param title label buttonu
     * @param form RU formular obshujuci callbacky
     * @param id identifikator ktory sa vlozi do callbacku
     *
     */
    public static ActionLabel getEditModeButton(String title, RU form, String id) {
        ActionLabel edit = new ActionLabel(title, StyleUtils.Assemble(MARGIN_LEFT_20, FONT_10PX));
        edit.setClickHandler(form.toEditModeClickHandler(id));
        return edit;
    }

    /**
     * Vrati ActionLabel pouzivany k prechodu do editcneho modu s generickym nazovm
     * EDIT
     *
     * @param form RU formular obshujuci callbacky
     * @param id identifikator ktory sa vlozi do callbacku
     *
     */
    public static ActionLabel getEditModeButton(RU form, String id) {
        return getEditModeButton(UIMessageUtils.c.const_edit(), form, id);
    }

    /**
     * Vrati ActionImage pre vykonanie akcie vo formulary. Obsahuje genericky nazov EDIT
     *
     * @param form RU formular obshujuci callbacky
     * @param id identifikator ktory sa vlozi do callbacku
     *
     */
    public static ActionImage getUpdateButton(RU form, String id) {
        return getUpdateButton(UIMessageUtils.c.const_saveUpper(), form, id);
    }

    /**
     * Vrati ActionImage pre vykonanie akcie vo formulary.
     *
     * @param title label buttonu
     * @param form RU formular obshujuci callbacky
     * @param id identifikator ktory sa vlozi do callbacku
     *
     */
    public static ActionImage getUpdateButton(String title, RU form, String id) {
        ActionImage button = ActionImage.getCheckButton(title);
        button.addClickHandler(form.confirmUpdateClickHandler(id));
        return button;
    }

    /**
     * Vrati ActionLabel pre zrusenie editacneho modu a navratu do read modu
     *
     * @param form RU formular obshujuci callbacky
     *
     */
    public static ActionLabel getCancelButton(RU form) {
        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(form.toReadModeClickHandler(null));
        return cancel;
    }

    /**
     * Vrati Widget pre jednoduchy READ mode, obsahujuci len titulku a editacny
     * button.
     *
     * @param title titulka formulara
     * @param edit pomenovanie editacneho odkazu
     * @param form formular nad ktory bude umestneny
     *
     */
    public static Widget getOneLinedReadMode(RU form, String title, String edit) {
        ActionLabel editLabel = new ActionLabel(edit,
                StyleUtils.Assemble(MARGIN_TOP_3, FONT_10PX, FLOAT_LEFT));
        editLabel.setClickHandler(form.toEditModeClickHandler(null));

        FlowPanel viewPanel = new FlowPanel();
        viewPanel.setStyleName(MARGIN_BOT_20);
        viewPanel.add(StyleUtils.getStyledLabel(title, FONT_16PX, FONT_BOLD, FLOAT_LEFT, MARGIN_RIGHT_20));
        viewPanel.add(editLabel);
        viewPanel.add(StyleUtils.clearDiv());

        return viewPanel;
    }

    /** Vraci titutlek pro zobrazeni v editacnim modu formu */
    public static Widget getOneLineEditMode(String title) {
        return StyleUtils.getStyledLabel(title, FONT_16PX, FONT_BOLD, MARGIN_RIGHT_20, StyleUtils.MARGIN_BOT_20);
    }
}
