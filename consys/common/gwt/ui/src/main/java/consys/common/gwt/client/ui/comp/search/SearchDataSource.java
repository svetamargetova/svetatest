package consys.common.gwt.client.ui.comp.search;

import java.util.ArrayList;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SearchDataSource<T extends SearchItem> {

    public void searchBy(String input, SearchCallback<T> callback);

    public interface SearchCallback<T> {

        public void didFind(ArrayList<T> items);
    }
}
