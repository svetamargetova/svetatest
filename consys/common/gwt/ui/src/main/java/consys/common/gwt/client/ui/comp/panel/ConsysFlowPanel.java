package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import java.util.ArrayList;
import java.util.List;

/**
 * Rozsireny zakladny FlowPanel o podporu validacie  {@link ValidableComponent} a
 * zobrazenie cakacieho kolecka {@link ActionExecutionDelegate}
 *
 * @author pepa
 */
public class ConsysFlowPanel extends FlowPanel implements ValidableComponent, ActionExecutionDelegate {

    List<ValidableComponent> validatingComponents = new ArrayList<ValidableComponent>();
    Waiting waiting;

    @Override
    public void add(Widget w) {
        super.add(w);
        if (w instanceof ValidableComponent) {
            validatingComponents.add((ValidableComponent) w);
        }
    }

    @Override
    public void clear() {
        super.clear();
        validatingComponents.clear();
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        boolean result = true;
        for (ValidableComponent comp : validatingComponents) {
            if (comp.doValidate(fail)) {
                comp.onSuccess();
            } else {
                comp.onFail();
                result = false;
            }
        }
        return result;
    }

    @Override
    public void onSuccess() {
        for (ValidableComponent vc : validatingComponents) {
            vc.onSuccess();
        }
    }

    @Override
    public void onFail() {
        for (ValidableComponent vc : validatingComponents) {
            vc.onFail();
        }
    }

    private Waiting getWaiting() {
        if (waiting == null) {
            waiting = new Waiting(this);
            SimplePanel waitingPart = new SimplePanel();
            waitingPart.setWidget(waiting);
            add(waitingPart);
        }
        return waiting;
    }

    @Override
    public void actionStarted() {
        getWaiting().show();
    }

    @Override
    public void actionEnds() {
        getWaiting().hide();
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        
    }
}
