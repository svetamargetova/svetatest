package consys.common.gwt.client.ui.layout.panel;

/**
 * Enum typu panelu, do ktereho je item patri
 * @author pepa
 */
public enum PanelItemType {

    SYSTEM, ADDON, BANNER, ALL;
    private static final long serialVersionUID = 1077136840266127519L;
}
