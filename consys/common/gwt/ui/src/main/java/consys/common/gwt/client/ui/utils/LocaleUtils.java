package consys.common.gwt.client.ui.utils;

import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import java.util.Date;

/**
 * Pomocne metody pro práci s cookie
 * @author pepa
 */
public class LocaleUtils {

    // konstanty
    public static final String LOCALE_COOKIE_NAME = "takeplacelocale";
    public static final String LOCALE_ENGLISH = "en";
    public static final String LOCALE_CZECH = "cs";
    public static final String LOCALE_DEFAULT = LOCALE_ENGLISH;
    public static final String REG_EXP_REMOVE_LOCALE = "&locale=[^&^#]*|locale=[^&^#]*";

    /** nacte lokalizaci z cookie, pokud cookie nenalezne ulozi do cookie vychozi lokalizaci */
    public static String getLocale() {
        String cookie = Cookies.getCookie(LOCALE_COOKIE_NAME);
        if (cookie == null) {
            setLocale(LOCALE_DEFAULT);
            cookie = LOCALE_DEFAULT;
        }
        return cookie;
    }

    /** nastavi lokalizaci do cookie */
    public static void setLocale(String locale) {
        Date date = new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 7 * 60)); // 60 tydnu
        Cookies.setCookie(LOCALE_COOKIE_NAME, locale, date);
    }

    /** handler pro zmenu vyberu lokalizace ze selectboxu */
    public static ChangeValueEvent.Handler<SelectBoxItem<String>> changeLocaleHandler() {
        return new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                LocaleUtils.setLocale(event.getValue().getItem());
                reloadWithoutLocaleParam();
            }
        };
    }

    /** pokud existuje parametr */
    private static void reloadWithoutLocaleParam() {
        String href = Window.Location.getHref();
        if (href.indexOf("locale=") != -1) {
            // obsahuje parametr locale a je potreba ho vyhodit
            String url = href.replaceAll(REG_EXP_REMOVE_LOCALE, "");
            Window.Location.replace(url);
        } else {
            Window.Location.reload();
        }
    }
}
