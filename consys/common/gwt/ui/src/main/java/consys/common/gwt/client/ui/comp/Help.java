package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.event.HelpEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Popup obsahujici napovedu
 * @author pepa
 */
public class Help extends PopupPanel implements HelpEvent.Handler {

    // komponenty
    private FlowPanel panel;
    // data
    private String title;
    private String text;
    private HelpType type;

    /** enum typu helpu */
    private enum HelpType {

        LEFT, RIGHT, BOTTOM;
        private static final long serialVersionUID = 672571601948641687L;
    }

    public Help(String title, String text) {
        this(title, text, null);
    }

    private Help(String title, String text, HelpType type) {
        this.title = StringUtils.isEmpty(title) ? UIMessageUtils.c.help_text_noTitle() : title;
        this.text = StringUtils.isEmpty(text) ? UIMessageUtils.c.help_text_noDescription() : text;
        this.type = type;

        // kvuli ie, aby byl z-index, ktery prekryje ostatni relative a absolute komponenty
        getContainerElement().getParentElement().addClassName(ResourceUtils.css().helpPopupWrapper());

        panel = new FlowPanel();
        panel.setStyleName(ResourceUtils.css().helpPopup());
        setWidget(panel);
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(HelpEvent.TYPE, this);
        panel.clear();
        generateContent();
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(HelpEvent.TYPE, this);
    }

    @Override
    public void onShowHelp(HelpEvent helpEvent) {
        if (helpEvent.getHelp() != this) {
            hide();
        }
    }

    private void generateContent() {
        SimplePanel titlePanel = new SimplePanel();
        titlePanel.setStyleName(ResourceUtils.css().helpTitle());
        titlePanel.setWidget(new Label(title));

        SimplePanel contentPanel = new SimplePanel();
        contentPanel.setStyleName(ResourceUtils.css().helpContent());
        contentPanel.setWidget(new Label(text));

        SimplePanel bottomPanel = new SimplePanel();
        bottomPanel.setStyleName(ResourceUtils.css().helpBottom());

        panel.add(titlePanel);
        panel.add(contentPanel);
        panel.add(bottomPanel);

        addTriangle();
    }

    private void addTriangle() {
        if (type != null) {
            SimplePanel triangle = new SimplePanel();

            switch (type) {
                case LEFT:
                    triangle.setStyleName(ResourceUtils.css().helpTriangleLeft());
                    break;
                case RIGHT:
                    triangle.setStyleName(ResourceUtils.css().helpTriangleRight());
                    break;
                case BOTTOM:
                    triangle.setStyleName(ResourceUtils.css().helpTriangleBottom());
                    DOM.setStyleAttribute(triangle.getElement(), "top", (panel.getOffsetHeight() - 9) + "px");
                    DOM.setStyleAttribute(panel.getElement(), "top", (-panel.getOffsetHeight()) + "px");
                    break;
            }

            panel.add(triangle);
        }
    }

    @Override
    public boolean onKeyDownPreview(char key, int modifiers) {
        switch (key) {
            case KeyCodes.KEY_ESCAPE:
                hide();
                break;
        }
        return true;
    }

    @Override
    public void setPopupPosition(int left, int top) {
        int pLeft = left;
        int pTop = top;
        if (type != null) {
            switch (type) {
                case LEFT:
                    pLeft += 5;
                    pTop -= 23;
                    break;
                case RIGHT:
                    pLeft -= 110;
                    pTop -= 23;
                    break;
                case BOTTOM:
                    pLeft -= 12;
                    // vertikalni pozice se urcuje a nastavuje v addTriangle
                    break;
            }
        }
        super.setPopupPosition(pLeft, pTop);
    }

    @Override
    public void show() {
        EventBus.get().fireEvent(new HelpEvent(this));
        super.show();
    }

    /** help s trojuhelnicem doleva */
    public static Help left(String title, String text) {
        return new Help(title, text, HelpType.LEFT);
    }

    /** help s trojuhelnicem doprava */
    public static Help right(String title, String text) {
        return new Help(title, text, HelpType.RIGHT);
    }

    /** help s trojuhelnicem doprava */
    public static Help bottom(String title, String text) {
        return new Help(title, text, HelpType.BOTTOM);
    }
}
