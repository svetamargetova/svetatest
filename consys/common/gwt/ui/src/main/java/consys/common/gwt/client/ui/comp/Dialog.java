package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * Nadefinovanie vlastneho dialogu
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class Dialog extends PopupPanel {

    private static final String DIALOG_GLASS_PANEL_STYLE = "dialogGlassPanel";
    private static final String DIALOG_BODY_OUTER_WRAPPER_STYLE_TOP = "dialogBodyOuterWrapperTop";
    private static final String DIALOG_BODY_OUTER_WRAPPER_STYLE_BOTTOM = "dialogBodyOuterWrapperBottom";
    private static final String DIALOG_BODY_OUTER_BORDER = "dialogBodyOuterBorder";
    private static final String DIALOG_BODY_INNER_WRAPPER_STYLE_TOP = "dialogBodyInnerWrapperTop";
    private static final String DIALOG_BODY_INNER_WRAPPER_STYLE_BOTTOM = "dialogBodyInnerWrapperBottom";
    // konstanty
    public static final int DEFAULT_WIDTH = 600;
    public static final String DEFAULT_WIDTH_PX = "600px";
    private static final int BORDER = 20;
    private static final String BORDER_TOP_BOTTOM = "23px";
    private static final String INNER_BORDER_HEIGHT = "3px";
    // komponenty
    private SimplePanel content;
    // data
    private boolean allowEscapeClose = true;

    public Dialog() {
        this(DEFAULT_WIDTH);
    }

    public Dialog(int width) {
        super();
        setGlassEnabled(true);
        setGlassStyleName(DIALOG_GLASS_PANEL_STYLE);
        setStyleName(ResourceUtils.system().css().dialogBodyPanel());

        final String totalWidth = (width + BORDER * 2) + "px";

        // vrchni cast
        SimplePanel transpTop = new SimplePanel();
        transpTop.setSize(totalWidth, BORDER_TOP_BOTTOM);
        transpTop.setStyleName(DIALOG_BODY_OUTER_WRAPPER_STYLE_TOP);
        SimplePanel centerTop = new SimplePanel();
        centerTop.setSize(width + "px", INNER_BORDER_HEIGHT);
        centerTop.setStyleName(DIALOG_BODY_INNER_WRAPPER_STYLE_TOP);

        FlowPanel top = new FlowPanel();
        top.addStyleName(StyleUtils.POSITION_REL);
        top.setSize(totalWidth, BORDER_TOP_BOTTOM);
        top.add(transpTop);
        top.add(centerTop);

        // stredni cast
        SimplePanel left = new SimplePanel();
        left.setStyleName(DIALOG_BODY_OUTER_BORDER);

        content = new SimplePanel();
        content.setWidth(width + "px");
        content.setStyleName(ResourceUtils.system().css().dialogInnerPanel());

        SimplePanel right = new SimplePanel();
        right.setStyleName(DIALOG_BODY_OUTER_BORDER);
        DOM.setStyleAttribute(right.getElement(), "left", (width + BORDER) + "px");

        // spodni cast
        SimplePanel transpBottom = new SimplePanel();
        transpBottom.setSize(totalWidth, BORDER_TOP_BOTTOM);
        transpBottom.setStyleName(DIALOG_BODY_OUTER_WRAPPER_STYLE_BOTTOM);
        SimplePanel centerBottom = new SimplePanel();
        centerBottom.setSize(width + "px", INNER_BORDER_HEIGHT);
        centerBottom.setStyleName(DIALOG_BODY_INNER_WRAPPER_STYLE_BOTTOM);

        FlowPanel bottom = new FlowPanel();
        bottom.addStyleName(StyleUtils.POSITION_ABS);
        bottom.setSize(totalWidth, BORDER_TOP_BOTTOM);
        bottom.add(transpBottom);
        bottom.add(centerBottom);

        FlowPanel center = new FlowPanel();
        center.addStyleName(ResourceUtils.system().css().dialogInnerPanelWrapper());
        center.setWidth(totalWidth);
        center.add(left);
        center.add(content);
        center.add(right);

        // celek
        FlowPanel flowPanel = new FlowPanel();
        flowPanel.add(top);
        flowPanel.add(center);
        flowPanel.add(bottom);

        super.setWidget(flowPanel);
    }

    @Override
    protected void onLoad() {
        // vytvoreni spolocneho contentu
        content.setWidget(createContent());
    }

    /** vraci vytvoreny formular, ktery se zobrazi v dialogu */
    protected abstract Widget createContent();

    @Override
    public boolean onKeyDownPreview(char key, int modifiers) {
        if (allowEscapeClose) {
            switch (key) {
                case KeyCodes.KEY_ESCAPE:
                    hide();
                    break;
            }
        }
        return true;
    }

    public void setAllowEscapeClose(boolean allowEscapeClose) {
        this.allowEscapeClose = allowEscapeClose;
    }

    public void showCentered() {
        setPopupPositionAndShow(new PositionCallback() {

            @Override
            public void setPosition(int offsetWidth, int offsetHeight) {
                int left = (Window.getClientWidth() - offsetWidth) >> 1;
                int top = (Window.getClientHeight() - offsetHeight) / 3;
                setPopupPosition(Math.max(Window.getScrollLeft() + left, 0), Math.max(Window.getScrollTop() + top, 0));
            }
        });
    }
}
