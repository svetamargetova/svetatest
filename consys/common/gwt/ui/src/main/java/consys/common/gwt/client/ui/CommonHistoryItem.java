package consys.common.gwt.client.ui;

import com.google.gwt.user.client.ui.Widget;

/**
 * Interface pro polozky, ktere se registruji do CommonHistoryHandleru
 * @author pepa
 */
public interface CommonHistoryItem {

    /** vytvori a vrati instanci zadaneho widgetu */
    public Widget createInstace();
}
