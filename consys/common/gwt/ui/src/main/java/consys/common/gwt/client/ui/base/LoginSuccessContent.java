package consys.common.gwt.client.ui.base;

import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 * Obsah zobrazeny po uspesnem prihlaseni uzivatele
 * @author pepa
 */
public class LoginSuccessContent extends SimpleFormPanel {

    // instance
    private static LoginSuccessContent instance;
    // data
    private ArrayList<GwtEvent> events;

    private LoginSuccessContent() {
        super();
        addWidget(StyleUtils.getStyledLabel("Login success", StyleUtils.FONT_BOLD));
        addWidget(StyleUtils.getStyledLabel(UIMessageUtils.c.const_loadingDots(), StyleUtils.MARGIN_TOP_20));
        events = new ArrayList<GwtEvent>();
    }

    /** vraci instanci LoginSuccessContent */
    public static LoginSuccessContent get() {
        if (instance == null) {
            instance = new LoginSuccessContent();
        }
        return instance;
    }

    /** zavola vychozi a pak zaregistovane eventy potrebne pro chod aplikace */
    public void runEvents() {
        // uzivatel byl prihlasen        
        EventBus.get().fireEvent(new UserLoginEvent());
        LayoutManager.get().showWaiting(false);
        runModuleEvents();
    }

    /** zavola registrovane eventy z ostatnich modulu */
    private void runModuleEvents() {
        for (GwtEvent event : events) {
            EventBus.get().fireEvent(event);
        }
    }

    /** zaregistruje eventy pro jednotlive moduly, ktere se maji spustit po prihlaseni */
    public void registerModuleEvent(GwtEvent event) {
        events.add(event);
    }
}
