package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Zobrazovatko zatizeni
 * @author pepa
 */
public class LoadIndicator extends Composite {

    // konstanty
    private static final int MAX_DOTS = 10;
    // komponenty
    private FlowPanel dotsPanel;
    private Label loadLabel;
    // data
    private int actualLoad;
    private int maxLoad;

    public LoadIndicator(int actualLoad, int maxLoad) {
        this.actualLoad = actualLoad;
        this.maxLoad = maxLoad;

        FlowPanel panel = new FlowPanel();

        loadLabel = StyleUtils.getStyledLabel(getLabelText(), StyleUtils.FLOAT_LEFT);

        dotsPanel = new FlowPanel();
        dotsPanel.addStyleName(ResourceUtils.system().css().loadDots());

        drawContent();

        panel.add(loadLabel);
        panel.add(dotsPanel);
        initWidget(panel);
    }

    /** podle hodnoty atributu necha vykreslit tecky a nastavi hodnotu textu */
    private void drawContent() {
        loadLabel.setText(getLabelText());
        if (maxLoad > MAX_DOTS) {
            loadOverMaxDots();
        } else {
            loadToMaxDots();
        }
    }

    /** vraci text znazornujici zatez */
    private String getLabelText() {
        return actualLoad + "/" + maxLoad;
    }

    /** maximalni zatez vic nez pocet tecek */
    private void loadOverMaxDots() {
        dotsPanel.clear();
        int tenPer = maxLoad;
        int count = (actualLoad * 10) / tenPer;
        boolean half = (actualLoad * 10) % tenPer != 0;
        boolean wasHalf = false;
        for (int i = 0; i < MAX_DOTS; i++) {
            SimplePanel dot = new SimplePanel();
            dot.addStyleName(StyleUtils.FLOAT_LEFT);
            dot.addStyleName(StyleUtils.MARGIN_LEFT_1);
            if (i < count) {
                dot.add(new Image(ResourceUtils.system().showLoadLoaded()));
            } else if (half && !wasHalf) {
                dot.add(new Image(ResourceUtils.system().showLoadHalfLoaded()));
                wasHalf = true;
            } else {
                dot.add(new Image(ResourceUtils.system().showLoadUnloaded()));
            }
            dotsPanel.add(dot);
        }
    }

    /** maximalni zatez do maximalniho poctu tecek */
    private void loadToMaxDots() {
        dotsPanel.clear();
        for (int i = 0; i < maxLoad; i++) {
            SimplePanel dot = new SimplePanel();
            dot.addStyleName(StyleUtils.FLOAT_LEFT);
            dot.addStyleName(StyleUtils.MARGIN_LEFT_1);
            if (i < actualLoad) {
                dot.add(new Image(ResourceUtils.system().showLoadLoaded()));
            } else {
                dot.add(new Image(ResourceUtils.system().showLoadUnloaded()));
            }
            dotsPanel.add(dot);
        }
    }

    /** zvysi zatez o zadany pocet */
    public void increase(int diff) {
        actualLoad += diff;
        drawContent();
    }

    /** snizi zatez o zadany pocet */
    public void decrease(int diff) {
        actualLoad -= diff;
        if (actualLoad < 0) {
            actualLoad = 0;
        }
        drawContent();
    }
}
