package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Uzivatel s afilaci (pouziti hlavne seznamy)
 * @author pepa
 */
public class NameWithAffiliation extends Composite {

    public NameWithAffiliation(String name, String affiliation) {
        this(name, affiliation, null);
    }

    public NameWithAffiliation(String name, String affiliation, final ConsysAction action) {
        Widget nameWidget;

        if (action == null) {
            nameWidget = StyleUtils.getStyledLabel(name, StyleUtils.FONT_17PX);
        } else {
            ActionLabel label = new ActionLabel(name, StyleUtils.FONT_17PX);
            label.removeStyleName(StyleUtils.INLINE);
            label.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    action.run();
                }
            });
            nameWidget = label;
        }

        Label affiliationLabel = StyleUtils.getStyledLabel(affiliation, StyleUtils.MARGIN_TOP_5);

        FlowPanel panel = new FlowPanel();
        panel.addStyleName(StyleUtils.MARGIN_BOT_10);
        panel.add(nameWidget);
        panel.add(affiliationLabel);
        initWidget(panel);
    }
}
