package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Jednotny dialog pro jakekoliv pridavani/vytvareni/mazani polozek
 * @author pepa
 */
public abstract class CreateDialog extends Dialog implements ActionExecutionDelegate {

    // komponenty
    private SimpleFormPanel bodyFormPanel;
    private Label headTitleLabel;
    private FlowPanel headControllPanel;
    // data
    private String titleText;
    private boolean hideTopSeparator;
    // waiting
    private Waiting waiting;

    public CreateDialog(String titleText) {
        this(titleText, false);
    }

    public CreateDialog(String titleText, boolean hideTopSeparator) {
        super();
        this.titleText = titleText;
        this.hideTopSeparator = hideTopSeparator;
    }

    @Override
    protected Widget createContent() {
        // vytvoreni spolocneho contentu
        bodyFormPanel = new SimpleFormPanel(Dialog.DEFAULT_WIDTH_PX);

        // TITULKA
        headTitleLabel = StyleUtils.getStyledLabel(titleText, ResourceUtils.system().css().createDialogHeaderTitle());
        SimplePanel headerTitleWrapper = new SimplePanel();
        headerTitleWrapper.setStyleName(ResourceUtils.system().css().createDialogHeaderTitleWrapper());
        headerTitleWrapper.setWidget(headTitleLabel);

        // CONTROLL PANEL
        headControllPanel = new FlowPanel();
        headControllPanel.setStyleName(ResourceUtils.system().css().createDialogHeaderControllPanel());

        // HEAD PANEL
        FlowPanel headPanel = new FlowPanel();
        headPanel.setStyleName(ResourceUtils.system().css().createDialogHeaderWrapper());
        headPanel.add(headerTitleWrapper);
        headPanel.add(headControllPanel);
        bodyFormPanel.addWidget(headPanel);

        if (!hideTopSeparator) {
            Separator separator = new Separator(Dialog.DEFAULT_WIDTH - 20 + "px", StyleUtils.MARGIN_BOT_10);
            separator.addStyleName(StyleUtils.MARGIN_LEFT_10);
            bodyFormPanel.addWidget(separator);
        }

        bodyFormPanel.addWidget(createForm());

        // SEPARATOR
        Separator separator = new Separator(Dialog.DEFAULT_WIDTH - 20 + "px", StyleUtils.MARGIN_BOT_10);
        separator.addStyleName(StyleUtils.MARGIN_LEFT_10);
        bodyFormPanel.addWidget(separator);

        // CONRTOLL PANEL
        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_save());
        button.addStyleName(StyleUtils.MARGIN_RIGHT_20);
        button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onSave();
            }
        });

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), StyleUtils.MARGIN_LEFT_20);
        cancel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
                onCancel();
            }
        });


        HTMLPanel actionPanel = new HTMLPanel("<table cellspacing='0' cellpadding='0'><tr><td style='height:40px;'><div id='action'></div></td><td><div id='close'></div></td></tr></table>");
        actionPanel.addStyleName(ResourceUtils.system().css().createDialogControllPanel());
        actionPanel.add(button, "action");
        actionPanel.add(cancel, "close");

        SimplePanel actionWrapper = new SimplePanel();
        actionWrapper.setWidget(actionPanel);
        bodyFormPanel.addWidget(actionWrapper);

        return bodyFormPanel;
    }

    /** vraci vytvoreny formular, ktery se zobrazi v dialogu */
    protected abstract Widget createForm();

    /** akce na pozadavek ulozeni formulare */
    protected abstract void onSave();

    /** akce na pozadavek zruseni formulare */
    protected abstract void onCancel();

    @Override
    public void setWidget(Widget widget) {
        throw new UnsupportedOperationException("use createForm");
    }

    /** vraci panel s titulkem dialogu */
    public Label getTitleLabel() {
        return headTitleLabel;
    }

    public void addControll(Widget w) {
        SimplePanel panel = new SimplePanel();
        panel.setWidget(w);
        panel.addStyleName(ResourceUtils.system().css().createDialogHeaderControllPanelItem());
        headControllPanel.add(panel);
    }

    /** vraci panel dialogu */
    public SimpleFormPanel getFormPanel() {
        return bodyFormPanel;
    }

    /** 
     * slouzi pro nastaveni chybove zpravy pres delegata
     * @return ConsysMessage pokud je dialog pouzity jako delegat, jinak vraci null
     */
    public abstract ConsysMessage getFailMessage();

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        if (getFailMessage() != null) {
            getFailMessage().setText(UIMessageUtils.getFailMessage(value));
        }
    }

    /** zobrazi pres formular panel s cekacim obrazkem */
    public void showWaiting(boolean value) {
        if (waiting == null) {
            waiting = new Waiting(bodyFormPanel);
            bodyFormPanel.addWidget(waiting);
        }
        if (value) {
            waiting.show(bodyFormPanel);
        } else {
            waiting.hide();
        }
    }

    @Override
    public void actionStarted() {
        showWaiting(true);
    }

    @Override
    public void actionEnds() {
        showWaiting(false);
    }
}
