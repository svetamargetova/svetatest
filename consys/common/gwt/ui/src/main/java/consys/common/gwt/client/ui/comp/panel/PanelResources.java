package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource.Import;
import com.google.gwt.resources.client.ImageResource;
import consys.common.gwt.client.ui.img.SystemCssResource;

/**
 *
 * @author palo
 */
public interface PanelResources extends ClientBundle {

    public static final PanelResources INSTANCE = GWT.create(PanelResources.class);

    @Source("img/facebook.png")
    public ImageResource buttonFacebook();

    @Source("img/facebookOff.png")
    public ImageResource buttonFacebookOff();

    @Source("img/facebookOver.png")
    public ImageResource buttonFacebookOver();

    @Source("img/facebookButton.png")
    public ImageResource buttonFacebookButton();

    @Source("img/googleplus.png")
    public ImageResource buttonGooglePlus();

    @Source("img/googleplusOff.png")
    public ImageResource buttonGooglePlusOff();

    @Source("img/googleplusOver.png")
    public ImageResource buttonGooglePlusOver();

    @Source("img/googleplusButton.png")
    public ImageResource buttonGooglePlusButton();

    @Source("img/linkedin.png")
    public ImageResource buttonLinkedIn();

    @Source("img/linkedinOff.png")
    public ImageResource buttonLinkedInOff();

    @Source("img/linkedinOver.png")
    public ImageResource buttonLinkedInOver();

    @Source("img/linkedinButton.png")
    public ImageResource buttonLinkedInButton();

    @Source("img/twitter.png")
    public ImageResource buttonTwitter();

    @Source("img/twitterOff.png")
    public ImageResource buttonTwitterOff();

    @Source("img/twitterOver.png")
    public ImageResource buttonTwitterOver();

    @Source("img/twitterButton.png")
    public ImageResource buttonTwitterButton();

    /** css styl pro wrapper komponenty */
    @Import(value = {SystemCssResource.class})
    @ClientBundle.Source("consys/common/gwt/client/ui/comp/panel/panel.css")
    public PanelCss css();
}
