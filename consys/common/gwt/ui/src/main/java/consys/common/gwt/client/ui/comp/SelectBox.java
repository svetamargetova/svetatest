package consys.common.gwt.client.ui.comp;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.input.ConsysSelectBoxInterface;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.comp.select.ItemBoard;
import consys.common.gwt.client.ui.comp.select.SearchTimer;
import consys.common.gwt.client.ui.comp.select.SelectBoxMeasure;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.NextComponentEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;

/**
 * Nase vlastni komponenta selectu
 * @author pepa
 */
public class SelectBox<T> extends Composite implements ConsysSelectBoxInterface<T>,
        ChangeValueEvent.Handler<SelectBoxItem<T>>, NextComponentEvent.Handler, ValidableComponent {

    // konstanty
    private static final SelectBoxMeasure sbm = GWT.create(SelectBoxMeasure.class);
    // komponenty
    private FocusPanel basePanel;
    private TextBox input;
    private HTML selectedItemLabel;
    private ItemBoard<T> pup;
    // seznam zaregistrovanych handleru
    private ArrayList<ChangeValueEvent.Handler> changeValueHandlers;
    // data
    private int customWidth = 0;
    private boolean enabled;
    private boolean selectFirst;
    private SelectBoxItem<T> selectedItem;
    private ArrayList<SelectBoxItem<T>> itemList;
    private boolean required = false;
    private SearchTimer<T> searchTimer;
    private T preSelectedItem;

    public SelectBox() {
        enabled = true;
        changeValueHandlers = new ArrayList<ChangeValueEvent.Handler>();

        String width = sbm.defaultInputWidth();

        input = new TextBox();
        input.setSize(width, sbm.defaultHeight());
        input.addStyleName(StyleUtils.FLOAT_LEFT);
        input.addStyleName(ResourceUtils.system().css().inputPadd());
        input.addStyleName(StyleUtils.BORDER_TRANSPARENT);
        input.setVisible(false);
        input.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                searchItem(event.getNativeKeyCode());
            }
        });

        selectedItemLabel = new HTML();
        selectedItemLabel.setSize(width, sbm.defaultHeight());
        selectedItemLabel.addStyleName(StyleUtils.FLOAT_LEFT);
        selectedItemLabel.addStyleName(StyleUtils.PADDING_LEFT_5);
        selectedItemLabel.addStyleName(StyleUtils.PADDING_TOP_2);
        selectedItemLabel.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        selectedItemLabel.addStyleName(StyleUtils.ALIGN_LEFT);
        selectedItemLabel.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (enabled) {
                    selectMode(true);
                }
            }
        });

        Image arrowImage = new Image(ResourceUtils.system().sizeArrowDown());
        arrowImage.addStyleName(StyleUtils.FLOAT_RIGHT);
        arrowImage.addStyleName(StyleUtils.MARGIN_HOR_5);
        DOM.setStyleAttribute(arrowImage.getElement(), "marginTop", "7px");

        basePanel = new FocusPanel();
        basePanel.setSize(sbm.defaultWidth(), sbm.defaultHeight());
        basePanel.addStyleName(ResourceUtils.css().selectBox());
        basePanel.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        basePanel.addStyleName(StyleUtils.BORDER);
        basePanel.addStyleName(StyleUtils.BACKGROUND_WHITE);
        basePanel.addStyleName(StyleUtils.HAND);
        basePanel.addFocusHandler(new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                if (enabled) {
                    selectMode(true);
                }
            }
        });

        searchTimer = new SearchTimer<T>(input) {

            @Override
            public void setSelectedItem(SelectBoxItem<T> selectedItem) {
                SelectBox.this.setSelectedItem(selectedItem);
            }
        };

        FlowPanel main = new FlowPanel();
        main.add(selectedItemLabel);
        main.add(input);
        main.add(arrowImage);
        main.add(StyleUtils.clearDiv());
        basePanel.setWidget(main);

        initWidget(basePanel);
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(ChangeValueEvent.TYPE, this);
        EventBus.get().addHandler(NextComponentEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(ChangeValueEvent.TYPE, this);
        EventBus.get().removeHandler(NextComponentEvent.TYPE, this);
        if (pup != null) {
            pup.hide();
        }
    }

    /** zobrazi vstup pro vyhledavani v polozkach a skryje label */
    private void selectMode(boolean value) {
        selectedItemLabel.setVisible(!value);
        input.setVisible(value);
        if (value) {
            EventBus.get().fireEvent(new NextComponentEvent(this));
            input.setFocus(true);
            if (!enabled || itemList == null) {
                return;
            }
            if (pup == null) {
                pup = new ItemBoard<T>(sbm.itemBoardWidth(customWidth), this) {

                    @Override
                    public SelectBoxItem<T> getSelectedItem() {
                        return SelectBox.this.getSelectedItem();
                    }

                    @Override
                    public void setSelectedItem(SelectBoxItem<T> i) {
                        SelectBox.this.setSelectedItem(i);
                    }
                };
                pup.addCloseHandler(new CloseHandler<PopupPanel>() {

                    @Override
                    public void onClose(CloseEvent<PopupPanel> event) {
                        input.setFocus(false);
                        selectMode(false);
                    }
                });
                searchTimer.setItemBoard(pup);
                pup.setItems(itemList);
                pup.setBoardSelectedItem(selectedItem);
            }
            if (!pup.isShowing()) {
                pup.setPopupPosition(getAbsoluteLeft(), sbm.itemBoardTop(getAbsoluteTop()));
                pup.show();
                pup.setBoardSelectedItem(selectedItem);
            }
        }
    }

    /** nastavuje zda se ma automaticky vybrat prvni polozka */
    @Override
    public void selectFirst(boolean value) {
        selectFirst = value;
        if (selectFirst && itemList != null && !itemList.isEmpty()) {
            setSelectedItem(itemList.get(0));
        } else {
            selectedItem = null;
            selectedItemLabel.setText("");
        }
    }

    /** vraci true pokud se automaticky vybira prvni polozka */
    public boolean isSelectFirst() {
        return selectFirst;
    }

    /** nastavi data do selectboxu  */
    public void setItems(ArrayList<SelectBoxItem<T>> itemList) {
        searchTimer.setItemList(itemList);
        this.itemList = itemList;
        pup = null;
        if (selectFirst && itemList != null && !itemList.isEmpty()) {
            setSelectedItem(itemList.get(0));
        }
        if (preSelectedItem != null) {
            doSelectItem(preSelectedItem);
        }
    }

    /**
     * vraci vybranou polozku
     * @return null pokud neni nic vybrano, jinak vybrany objekt
     */
    public SelectBoxItem<T> getSelectedItem() {
        return selectedItem;
    }

    /** nastavi vybranou polozku */
    public void setSelectedItem(SelectBoxItem<T> item) {
        selectedItem = item;
        input.setText("");
        if (item != null) {
            String htmlPart = item.getHtml() == null ? "" : item.getHtml() + "&nbsp;";
            selectedItemLabel.setHTML(htmlPart + item.getName());
        } else {
            selectedItemLabel.setText("");
        }
    }

    /** vybere polozku podle poradi v seznamu */
    public void selectItemByIndex(int index) {
        selectedItem = itemList.get(index);
        String htmlPart = selectedItem.getHtml() == null ? "" : selectedItem.getHtml() + "&nbsp;";
        selectedItemLabel.setHTML(htmlPart + selectedItem.getName());
        if (pup != null) {
            pup.setBoardSelectedItem(selectedItem);
        }
    }

    /** vybere polozku podle nazvu popisku */
    public void selectItemByName(String name) {
        for (int i = 0; i < itemList.size(); i++) {
            SelectBoxItem<T> item = itemList.get(i);
            if (item.getName().equalsIgnoreCase(name)) {
                selectItemByIndex(i);
                break;
            }
        }
    }

    /** vybere polozku podle hodnoty v seznamu */
    @Override
    public void selectItem(T value) {
        if (itemList == null) {
            preSelectedItem = value;
        } else {
            doSelectItem(value);
        }
    }

    /** vybere zadanou polozku */
    private void doSelectItem(T value) {
        for (int i = 0; i < itemList.size(); i++) {
            SelectBoxItem<T> item = itemList.get(i);
            if (item.getItem().equals(value)) {
                selectItemByIndex(i);
                break;
            }
        }
    }

    /** oznaci ze neni vybrano nic */
    public void selectNone() {
        selectedItemLabel.setText("");
        selectedItem = null;
    }

    /** vraci true pokud je komponenta povolena */
    public boolean isEnabled() {
        return enabled;
    }

    /** enabluje nebo disabluje komponentu podle parametru */
    @Override
    public void setEnabled(boolean value) {
        if (value == enabled) {
            return;
        }
        if (value) {
            // enable
            basePanel.addStyleName(StyleUtils.BACKGROUND_WHITE);
            basePanel.removeStyleName(StyleUtils.BACKGROUND_GRAY);
        } else {
            // disable
            basePanel.addStyleName(StyleUtils.BACKGROUND_GRAY);
            basePanel.removeStyleName(StyleUtils.BACKGROUND_WHITE);
        }
        input.setEnabled(value);
        enabled = value;
    }

    /** je polozka povinna? */
    public boolean isRequired() {
        return required;
    }

    /** nastaveni ze se jedna o povinnou polozku */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /** nastavi sirku komponenty (pouziva se misto setWidth(String width)) */
    public void setWidth(int width) {
        customWidth = width;
        basePanel.setWidth(sbm.basePanelWidth(width));
        selectedItemLabel.setWidth(sbm.selectItemLabelWidth(width));
        input.setWidth(sbm.inputWidth(width));
    }

    @Override
    public void setWidth(String width) {
        throw new UnsupportedOperationException("Use setWidth(int width)");
    }

    /** zaregistruje handler */
    @Override
    public void addChangeValueHandler(ChangeValueEvent.Handler handler) {
        changeValueHandlers.add(handler);
    }

    @Override
    public void onChangeValue(ChangeValueEvent<SelectBoxItem<T>> event) {
        if (enabled && event.getComponent().equals(this)) {
            for (ChangeValueEvent.Handler handler : changeValueHandlers) {
                handler.onChangeValue(event);
            }
        }
    }

    @Override
    public void onNextComponent(NextComponentEvent event) {
        if (event.getComponent() != this) {
            if (pup != null) {
                pup.hide();
            }
        }
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        return !isRequired() ? true : getSelectedItem() != null ? true : false;
    }

    @Override
    public void onSuccess() {
        basePanel.removeStyleName(StyleUtils.BACKGROUND_WHITE);
        basePanel.addStyleName(StyleUtils.BACKGROUND_WHITE);
        basePanel.removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        basePanel.removeStyleName(StyleUtils.BACKGROUND_GRAY);
        basePanel.addStyleName(StyleUtils.BACKGROUND_GRAY);
        basePanel.removeStyleName(StyleUtils.BACKGROUND_WHITE);
    }

    /** vyhledavani polozky podle psani nebo posunu sipek */
    private void searchItem(int keyCode) {
        if (keyCode == KeyCodes.KEY_ENTER) {
            SelectBoxItem<T> item = pup.getBoardSelectedItem();
            // vyvolani eventu
            EventBus.get().fireEvent(new ChangeValueEvent<SelectBoxItem>(this, getSelectedItem(), item));
            // vybrani nove hodnoty
            setSelectedItem(item);
            input.setFocus(false);
            selectMode(false);
            pup.hide();
        } else if (keyCode == KeyCodes.KEY_UP) {
            pup.up();
        } else if (keyCode == KeyCodes.KEY_DOWN) {
            pup.down();
        } else if (keyCode < 32) {
            return;
        } else {
            searchTimer.cancel();
            searchTimer.schedule(500);
        }
    }
}
