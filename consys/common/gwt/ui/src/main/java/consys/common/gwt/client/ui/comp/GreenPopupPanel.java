package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.comp.wrapper.ConsysGreenWrapper;

/**
 * Abstraktni predek zelenych PopupPanelu
 * @author pepa
 */
public abstract class GreenPopupPanel extends PopupPanel implements ActionExecutionDelegate {

    // komponenty
    private Waiting waiting;

    public GreenPopupPanel() {
        super(true, true);
        setGlassEnabled(true);

        waiting = new Waiting(this);      
    }

    /** vraci obsah popup panelu */
    protected abstract Widget getContent();

    @Override
    protected void onLoad() {
        super.onLoad();
        FlowPanel content = new FlowPanel();
        content.add(getContent());
        content.add(waiting);

        ConsysGreenWrapper wrapper = new ConsysGreenWrapper(content);
        setWidget(wrapper);        
    }

    
    
    @Override
    public boolean onKeyDownPreview(char key, int modifiers) {
        switch (key) {
            case KeyCodes.KEY_ESCAPE:
                hide();
                break;
        }
        return true;
    }

    @Override
    public void actionStarted() {
        waiting.show(this);
    }

    @Override
    public void actionEnds() {
        waiting.hide();
    }
}
