package consys.common.gwt.client.ui.comp.panel.element;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Třída uchovává jednu položku menu v MenuFormPanelu a reprezentuje její vzhled
 * @author pepa
 */
public class MenuFormPart extends Composite {

    // konstanty
    private static final int BORDER_WIDTH_INT = 5;
    private static final String BORDER_WIDTH = BORDER_WIDTH_INT + "px";
    private static final int BORDER_HEIGHT_INT = 32;
    private static final String BORDER_HEIGHT = BORDER_HEIGHT_INT + "px";
    // komponenty
    private HorizontalPanel panel;
    private Label nameLabel;
    private Label noteLabel;
    private Image borderLeft;
    private Image borderRight;
    private Image borderSLeft;
    private Image borderSRight;
    private SimplePanel leftPanel;
    private SimplePanel rightPanel;
    // data
    private MenuFormItem item;
    private ClickHandler clickHandler;
    private boolean enabled;
    // handler registration
    private HandlerRegistration overRegistration;
    private HandlerRegistration outRegistration;
    private HandlerRegistration clickRegistration;

    public MenuFormPart(MenuFormItem item) {
        this(item, true);
    }

    public MenuFormPart(MenuFormItem item, boolean enabled) {
        this.item = item;
        this.enabled = enabled;

        leftPanel = new SimplePanel();
        leftPanel.setSize(BORDER_WIDTH, BORDER_HEIGHT);
        rightPanel = new SimplePanel();
        rightPanel.setSize(BORDER_WIDTH, BORDER_HEIGHT);

        borderLeft = new Image(ResourceUtils.system().subMenuBorderLeft());
        leftPanel.setWidget(borderLeft);
        borderRight = new Image(ResourceUtils.system().subMenuBorderRight());
        rightPanel.setWidget(borderRight);

        panel = new HorizontalPanel();
        panel.setStyleName(ResourceUtils.system().css().mfpInactive());
        panel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);

        // nazev
        nameLabel = StyleUtils.getStyledLabel(item.getName(), StyleUtils.FONT_BOLD);
        nameLabel.addStyleName(StyleUtils.TEXT_BLUE);
        nameLabel.addStyleName(StyleUtils.UNSELECTABLE);
        nameLabel.addStyleName(StyleUtils.MARGIN_LEFT_5);
        panel.add(nameLabel);

        // poznamka
        String note = item.getNote();
        if (note != null && !note.isEmpty()) {
            noteLabel = new Label(note);
        } else {
            noteLabel = new Label();
        }
        noteLabel.setStyleName(StyleUtils.MARGIN_RIGHT_10);
        noteLabel.addStyleName(StyleUtils.MARGIN_BOT_3);
        noteLabel.addStyleName(StyleUtils.TEXT_BLUE);
        noteLabel.addStyleName(StyleUtils.UNSELECTABLE);
        panel.add(noteLabel);
        panel.setCellHorizontalAlignment(noteLabel, HasAlignment.ALIGN_RIGHT);
        panel.setCellVerticalAlignment(noteLabel, HasAlignment.ALIGN_MIDDLE);

        HorizontalPanel wrapper = new HorizontalPanel();
        wrapper.add(leftPanel);
        wrapper.add(panel);
        wrapper.add(rightPanel);

        sinkEvents(Event.ONCLICK | Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        initWidget(wrapper);

        if (enabled) {
            panel.addStyleName(StyleUtils.HAND);
            overRegistration = addHandler(ActionLabel.inverseMouseOverHandler(nameLabel), MouseOverEvent.getType());
            outRegistration = addHandler(ActionLabel.inverseMouseOutHandler(nameLabel), MouseOutEvent.getType());
        }
    }

    /** vrací data položky */
    public MenuFormItem getItem() {
        return item;
    }

    /** oznaceni polozky za vybranou */
    public void select(boolean value) {
        if (value) {
            panel.removeStyleName(ResourceUtils.system().css().mfpInactive());
            panel.addStyleName(ResourceUtils.system().css().mfpActive());
            nameLabel.removeStyleName(StyleUtils.TEXT_BLUE);
            noteLabel.removeStyleName(StyleUtils.TEXT_BLUE);
            nameLabel.addStyleName(StyleUtils.TEXT_WHITE);
            noteLabel.addStyleName(StyleUtils.TEXT_WHITE);

            if (borderSLeft == null) {
                borderSLeft = new Image(ResourceUtils.system().subMenuBorderSLeft());
                borderSRight = new Image(ResourceUtils.system().subMenuBorderSRight());
            }
            leftPanel.setWidget(borderSLeft);
            rightPanel.setWidget(borderSRight);
        } else {
            panel.removeStyleName(ResourceUtils.system().css().mfpActive());
            panel.addStyleName(ResourceUtils.system().css().mfpInactive());
            nameLabel.removeStyleName(StyleUtils.TEXT_WHITE);
            noteLabel.removeStyleName(StyleUtils.TEXT_WHITE);
            nameLabel.addStyleName(StyleUtils.TEXT_BLUE);
            noteLabel.addStyleName(StyleUtils.TEXT_BLUE);
            leftPanel.setWidget(borderLeft);
            rightPanel.setWidget(borderRight);
        }
    }

    /** @return true pokud je polozka aktivovana */
    public boolean isEnabled() {
        return enabled;
    }

    /** aktivuje / deaktivuje položku */
    public void setEnabled(boolean value) {
        enabled = value;
        if (value) {
            overRegistration = addHandler(ActionLabel.inverseMouseOverHandler(nameLabel), MouseOverEvent.getType());
            outRegistration = addHandler(ActionLabel.inverseMouseOutHandler(nameLabel), MouseOutEvent.getType());
            clickRegistration = addHandler(clickHandler, ClickEvent.getType());
            panel.removeStyleName(StyleUtils.DEFAULT_CURSOR);
            panel.addStyleName(StyleUtils.HAND);
            nameLabel.removeStyleName(StyleUtils.TEXT_GRAY);
            noteLabel.removeStyleName(StyleUtils.TEXT_GRAY);
        } else {
            if (overRegistration != null) {
                overRegistration.removeHandler();
            }
            if (outRegistration != null) {
                outRegistration.removeHandler();
            }
            if (clickRegistration != null) {
                clickRegistration.removeHandler();
            }
            panel.removeStyleName(StyleUtils.HAND);
            panel.addStyleName(StyleUtils.DEFAULT_CURSOR);
            nameLabel.addStyleName(StyleUtils.TEXT_GRAY);
            noteLabel.addStyleName(StyleUtils.TEXT_GRAY);
        }
    }

    /** zaktualizuje popis tlacitka */
    public void refresh() {
        nameLabel.setText(item.getName());
        noteLabel.setText(item.getNote());
    }

    /** nastavi ClickHandler */
    public void setClickHandler(ClickHandler handler) {
        if (clickRegistration != null) {
            clickRegistration.removeHandler();
        }
        clickHandler = handler;
        if (enabled) {
            clickRegistration = addHandler(clickHandler, ClickEvent.getType());
        }
    }

    /** nastavi spravou sirku i s ohledem na boky polozky */
    public void setWidth(int width) {
        panel.setWidth((width - 2 * BORDER_WIDTH_INT) + "px");
    }
}
