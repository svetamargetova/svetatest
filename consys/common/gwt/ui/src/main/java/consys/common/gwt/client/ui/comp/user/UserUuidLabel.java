package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.action.LoadUserThumbAction;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.shared.bo.ClientUserThumb;

/**
 * Label ktory prezentuje usera kdekolvek. Vstup je len UUID usera, ostatne si
 * komponenta sama dotiahne.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserUuidLabel extends UserLabel {

    private ClientUserThumb clientUserThumb;
    private String userUuid;

    public UserUuidLabel() {
    }
    
    
    public UserUuidLabel(String userUuid) {
        this.userUuid = userUuid;
    }   

    @Override
    protected void onLoad() {
        super.onLoad();
        if (getUserUuid() != null) {
            load();
        }
    }

    private void load() {
        EventBus.get().fireEvent(new DispatchEvent(new LoadUserThumbAction(getUserUuid()), new AsyncCallback<ClientUserThumb>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ClientUserThumb result) {
                clientUserThumb = result;
                init(userUuid, clientUserThumb.getName());
            }
        }, this));
    }
}
