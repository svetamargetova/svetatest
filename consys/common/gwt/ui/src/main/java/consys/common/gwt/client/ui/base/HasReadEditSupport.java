package consys.common.gwt.client.ui.base;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface HasReadEditSupport {

    public void toReadMode();

    public void toEditMode();

}
