package consys.common.gwt.client.ui.comp.list;

/**
 *
 * @author pepa
 */
public interface DataListCommonHeadPanel {

    /** nastaveni rodicovskeho datalistu head panelu */
    public void setParentList(DataListPanel parentList);

    /** vraci strankovac */
    public ListPagerWidget getListPagerWidget();
}
