package consys.common.gwt.client.ui.comp;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.shared.bo.ClientAddress;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.action.ListUsStateAction;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.input.ConsysSelectBoxInterface;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 * Formular pro zadani adresy
 * @author pepa
 */
public class AddressForm extends BaseForm {

    private static final Logger logger = LoggerFactory.getLogger(AddressForm.class);
    // konstanty
    private static final Integer USA_ID = 231;
    // komponenty
    private SelectBox<Integer> locationBox;
    private SelectBox<Integer> stateBox;
    private TextBox cityBox;
    private TextBox streetBox;
    private TextBox zipBox;

    public AddressForm() {
        this(null, null);
    }

    public AddressForm(ActionImage button, ActionLabel cancel) {
        super();

        locationBox = new SelectBox<Integer>();
        stateBox = new SelectBox<Integer>();
        stateBox.setWidth(100);
        stateBox.setEnabled(false);
        cityBox = StyleUtils.getFormInput();
        streetBox = StyleUtils.getFormInput();
        zipBox = StyleUtils.getFormInputHalf();
        zipBox.setMaxLength(8);

        locationBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<Integer>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                stateBox.setEnabled(event.getValue().getItem().equals(USA_ID));
            }
        });
        locationBox.selectFirst(false);

        HorizontalPanel statePanel = new HorizontalPanel();
        statePanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        statePanel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_address_state() + ":", StyleUtils.MARGIN_RIGHT_10));
        statePanel.add(stateBox);

        HorizontalPanel zipStatePanel = new HorizontalPanel();
        zipStatePanel.setWidth("100%");
        zipStatePanel.add(zipBox);
        zipStatePanel.add(statePanel);
        zipStatePanel.setCellHorizontalAlignment(statePanel, HasAlignment.ALIGN_RIGHT);

        this.addOptional(1, UIMessageUtils.c.const_address_location(), locationBox);
        this.addOptional(2, UIMessageUtils.c.const_address_city(), cityBox);
        this.addOptional(3, UIMessageUtils.c.const_address_street(), streetBox);
        this.addOptional(4, UIMessageUtils.c.const_address_zip(), zipStatePanel);
        if (button != null && cancel != null) {
            this.addActionMembers(5, button, cancel);
        }

        // meli by byt nacitane uz na zacatku takze to bude z Cache
        EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {

            @Override
            public void onFailure(Throwable caught) {
                logger.error("Failed to load locations");
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
                for (BoxItem e : result.getList()) {
                    out.add(new SelectBoxItem<Integer>(e.getId(), e.getName()));
                }
                locationBox.setItems(out);

            }
        }, null));

        EventBus.get().fireEvent(new DispatchEvent(new ListUsStateAction(), new AsyncCallback<SelectBoxData>() {

            @Override
            public void onFailure(Throwable caught) {
                logger.error("Failed to load US states");
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
                for (BoxItem e : result.getList()) {
                    out.add(new SelectBoxItem<Integer>(e.getId(), e.getName()));
                }
                stateBox.setItems(out);
            }
        }, null));

    }

    /** vrati adresu vytvorenou za zadanych dat */
    public ClientAddress getAddress() {
        ClientAddress address = new ClientAddress();
        address.setLocation(locationBox.getSelectedItem() != null ? locationBox.getSelectedItem().getItem() : 0);
        address.setCity(cityBox.getText().trim());
        address.setStreet(streetBox.getText().trim());
        address.setZip(zipBox.getText().trim());
        if (address.getLocation() == USA_ID) {
            address.setStateUs(stateBox.getSelectedItem() != null ? stateBox.getSelectedItem().getItem() : 0);
        }
        return address;
    }

    /** nastavi ve formulari data podle hodnot parametru */
    public void setAddress(ClientAddress address) {
        if (address != null) {
            if (address.getLocation() > 0) {
                locationBox.selectItem(address.getLocation());
            } else {
                initCountryBox(locationBox);
            }
            cityBox.setText(address.getCity());
            streetBox.setText(address.getStreet());
            zipBox.setText(address.getZip());
            stateBox.setEnabled(address.getLocation() == USA_ID);
            if (address.getStateUs() > 0) {
                stateBox.selectItem(address.getStateUs());
            }
        } else {
            locationBox.selectFirst(false);
            cityBox.setText("");
            streetBox.setText("");
            zipBox.setText("");
            stateBox.setEnabled(false);
            stateBox.selectFirst(false);
            initCountryBox(locationBox);
        }
    }

    public static void initCountryBox(final ConsysSelectBoxInterface<Integer> locationBox) {
        initCountryBox(locationBox, null);
    }

    public static void initCountryBox(final ConsysSelectBoxInterface<Integer> locationBox, final ConsysAction action) {
        locationBox.setEnabled(false);
        try {
            RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, URLFactory.countryServletUrl());

            rb.sendRequest(null, new RequestCallback() {

                @Override
                public void onResponseReceived(Request request, Response response) {
                    int returnCode = response.getStatusCode();
                    if (returnCode == 200) {
                        // ok
                        try {
                            Integer id = Integer.parseInt(response.getText());
                            locationBox.selectItem(id);
                        } catch (NumberFormatException ex) {
                            locationBox.selectFirst(true);
                        }
                    } else {
                        // chyba
                        locationBox.selectFirst(true);
                    }
                    locationBox.setEnabled(true);
                    if (action != null) {
                        action.run();
                    }
                }

                @Override
                public void onError(Request request, Throwable exception) {
                    logger.error("request send failed", exception);
                    locationBox.setEnabled(true);
                }
            });
        } catch (RequestException ex) {
            logger.error("request send failed", ex);
            locationBox.setEnabled(true);
        }
    }
}
