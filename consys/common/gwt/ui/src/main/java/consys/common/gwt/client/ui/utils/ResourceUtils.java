package consys.common.gwt.client.ui.utils;

import com.google.gwt.core.client.GWT;
import consys.common.gwt.client.ui.img.SystemBundle;
import consys.common.gwt.client.ui.img.SystemCssResource;
import consys.common.gwt.client.ui.resource.CommonResources;

/**
 *
 * @author pepa
 */
public class ResourceUtils {

    private static SystemBundle systemBundle;
    private static CommonResources common;

    public static CommonResources common() {
        if (common == null) {
            common = (CommonResources) GWT.create(CommonResources.class);
            common.css().ensureInjected();
        }
        return common;
    }

    public static SystemBundle system() {
        if (systemBundle == null) {
            systemBundle = (SystemBundle) GWT.create(SystemBundle.class);
            systemBundle.css().ensureInjected();
        }
        return systemBundle;
    }

    public static SystemCssResource css() {
        return system().css();
    }
}
