
package consys.common.gwt.client.ui.comp.list;

import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.list.Filter;



/**
 * Abstraktny predok pre Action request obsahujuci informacie pre
 */
public class ListDataSourceRequest extends EventAction<ListDataSourceResult>{
    private static final long serialVersionUID = -6114110886321657912L;

    private String dataListTag;
    /** prvy result od nuly */
    private int firstResult;
    private int itemsPerPage;
    private int[] orderers;
    private Filter filter;

    public ListDataSourceRequest() {
    }        

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    /**
     * @return the dataListTag
     */
    public String getDataListTag() {
        return dataListTag;
    }

    /**
     * @param dataListTag the dataListTag to set
     */
    public void setDataListTag(String dataListTag) {
        this.dataListTag = dataListTag;
    }

    /**
     * @return the orderers
     */
    public int[] getOrderers() {
        return orderers;
    }

    /**
     * @param orderers the orderers to set
     */
    public void setOrderers(int[] orderers) {
        this.orderers = orderers;
    }

    /**
     * @return the filter
     */
    public Filter getFilter() {
        return filter;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * prvy result od nuly
     * @return the firstResult
     */
    public int getFirstResult() {
        return firstResult;
    }

    /**
     * prvy result od nuly
     * @param firstResult the firstResult to set
     */
    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }

    
}
