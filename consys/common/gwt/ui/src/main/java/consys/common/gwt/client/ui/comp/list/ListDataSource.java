package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ListDataSource<T> {

    public void sendRequest(ListDataSourceRequest request, AsyncCallback<ListDataSourceResult> callback, ActionExecutionDelegate executionDelegate);
    
}
