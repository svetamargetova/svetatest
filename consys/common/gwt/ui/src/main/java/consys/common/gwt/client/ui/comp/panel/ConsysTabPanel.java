package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItemUI;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ConsysTabPanel<T> extends Composite {

    // konstanty
    private static final int FULL_DIFF = 31;
    private static final int WIDTH = LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 2;
    private static final int TAB_ITEM_WIDTH = 223;
    private static final String SUFFIX = "-tab";
    // komponenty
    private FlowPanel tabPanel;
    private SimplePanel tabPanelWrapper;
    private FlowPanel panelWithScroll;
    private SimplePanel content;
    private FocusPanel scrollLeft;
    private FocusPanel scrollRight;
    // data
    private final String panelName;
    private int count = 0;
    private ConsysTabPanelItemUI<T> lastSelected;
    private boolean scrolling = false;
    private int scrollPosition = 0;
    private boolean showOrderIcon = false;
    private ConsysAction orderIconAction;

    public ConsysTabPanel(String panelName) {
        this.panelName = panelName;

        tabPanel = new FlowPanel();
        tabPanel.setStyleName(StyleUtils.POSITION_REL);

        tabPanelWrapper = new SimplePanel();
        tabPanelWrapper.setStyleName(StyleUtils.OVERFLOW_HIDDEN);
        tabPanelWrapper.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - FULL_DIFF) + "px");
        tabPanelWrapper.setWidget(tabPanel);

        panelWithScroll = new FlowPanel();
        panelWithScroll.setWidth(WIDTH + "px");
        panelWithScroll.setStyleName(StyleUtils.POSITION_REL);
        panelWithScroll.addStyleName(StyleUtils.INDEX_1);
        DOM.setStyleAttribute(panelWithScroll.getElement(), "top", "1px");
        panelWithScroll.add(tabPanelWrapper);

        content = new SimplePanel();
        content.setWidth(WIDTH + "px");
        content.setStyleName(StyleUtils.BORDER_LIGHT_GRAY);
        content.addStyleName(StyleUtils.BACKGROUND_LIGHT_GRAY);
        content.addStyleName(StyleUtils.OVERFLOW_HIDDEN);

        FlowPanel panel = new FlowPanel();
        panel.add(panelWithScroll);
        panel.add(StyleUtils.clearDiv());
        panel.add(content);
        initWidget(panel);
    }

    @Override
    protected void onLoad() {
        refreshScroll();
    }

    /** vraci vybranou zalozku */
    public ConsysTabPanelItemUI<T> getSelectedTab() {
        return lastSelected;
    }

    /** nastavi vybranou zalozku */
    public void setSelectedTab(ConsysTabPanelItemUI<T> item) {
        lastSelected = item;
        if (showOrderIcon) {
            lastSelected.setShowChangeOrderIcon(true);
            lastSelected.setChangeOrderAction(orderIconAction);
        }
    }

    /** vraci panel pro obsah zalozky */
    public SimplePanel getContent() {
        return content;
    }

    /** vlozi zalozku */
    public void addTabItem(ConsysTabPanelLabel<T> label, ConsysTabPanelItem item) {
        addTabItem(label, item, null);
    }

    /** vlozi zalozku */
    public void addTabItem(ConsysTabPanelLabel<T> label, ConsysTabPanelItem item, ConsysAction onSelectAction) {
        boolean selected = tabPanel.getWidgetCount() == 0;

        label.setTab(this);
        item.setConsysTabPanel(this);
        ConsysTabPanelItemUI<T> tabItem = new ConsysTabPanelItemUI<T>(panelName, label, item, selected, onSelectAction);
        if (showOrderIcon) {
            tabItem.setShowChangeOrderIcon(true);
            tabItem.setChangeOrderAction(orderIconAction);
        }
        tabItem.setParent(this);
        tabPanel.add(tabItem);
        count++;
        tabPanel.setWidth((TAB_ITEM_WIDTH * count) + "px");

        T cacheValue = (T) Cache.get().getSelectedTab(ConsysTabPanel.cachePanelName(panelName));
        if (cacheValue == null) {
            // nebyla zatim nactena zadna zalozka
            if (content.getWidget() == null) {
                showItem(tabItem);
            }
        } else if (cacheValue.equals(label.getId())) {
            showItem(tabItem);
        } else if (selected) {
            tabItem.unselect();
        }

        // TODO: asi casovac, pokud nebude nasetovano (nemelo by nastat) do obsahu po nejakou dobu, at se vybere prvni polozka

        refreshScroll();
    }

    /** necha zobrazi zadanou zalozku do obsahu */
    private void showItem(ConsysTabPanelItemUI<T> tabItem) {
        if (lastSelected != null) {
            lastSelected.unselect();
        }
        content.setWidget(tabItem.getContent());
        lastSelected = tabItem;
        tabItem.select();
        if (showOrderIcon) {
            lastSelected.showChangeOrderIcon(true);
        }
    }

    /** zaridi vybrani zadane polozky */
    private void selectItem(ConsysTabPanelItemUI item) {
        if (item != lastSelected) {
            showItem(item);
        }
    }

    /** vraci obsah zalozky na i-te pozici (cisluje se od 0) */
    public Widget getTabContent(int index) {
        if (index >= 0 && index < count) {
            ConsysTabPanelItemUI item = (ConsysTabPanelItemUI) tabPanel.getWidget(index);
            return item.getContent();
        }
        return null;
    }

    /** vraci pocet zalozek */
    public int getTabCount() {
        return count;
    }

    /** vybere i-tou zalozku za aktivni (cisluje se od 0) */
    public boolean selectTab(int index) {
        if (index >= 0 && index < count) {
            ConsysTabPanelItemUI item = (ConsysTabPanelItemUI) tabPanel.getWidget(index);
            selectItem(item);
            return true;
        }
        return false;
    }

    /** vybere zalozku podl identifikacniho objektu */
    public boolean selectTabById(T id) {
        if (id == null) {
            return false;
        }
        for (int i = 0; i < tabPanel.getWidgetCount(); i++) {
            ConsysTabPanelItemUI<T> item = (ConsysTabPanelItemUI<T>) tabPanel.getWidget(i);
            if (id.equals(item.getId())) {
                selectItem(item);
                return true;
            }
        }
        return false;
    }

    /** odstrani pozadovanou zalozku */
    public void removeTab(ConsysTabPanelItem name) {
        ConsysTabPanelItemUI previousItem = null;
        for (int i = 0; i < tabPanel.getWidgetCount(); i++) {
            ConsysTabPanelItemUI item = (ConsysTabPanelItemUI) tabPanel.getWidget(i);
            if (item.getContent().equals(name.getContent())) {
                if (tabPanel.getWidgetCount() > 1) {
                    if (previousItem == null) {
                        selectTab(i + 1);
                    } else {
                        selectTab(i - 1);
                    }
                } else {
                    lastSelected = null;
                    Cache.get().register(cachePanelName(panelName), null);
                    content.setWidget(null);
                }
                item.removeFromParent();
                count--;
                tabPanel.setWidth((TAB_ITEM_WIDTH * count) + "px");
                refreshScroll();
                break;
            }
            previousItem = item;
        }
    }

    /** vraci nazev klice v cache */
    public static String cachePanelName(String panelName) {
        return panelName + SUFFIX;
    }

    /** vyprazdni panel */
    public void clear() {
        tabPanel.clear();
        content.setWidget(null);
    }

    /** vraci nazev zalozky se zadanym indexem */
    public String getTabName(int index) {
        ConsysTabPanelItemUI ui = (ConsysTabPanelItemUI) tabPanel.getWidget(index);
        return ui.getTabName();
    }

    /** provede kontrolu jestli zobrazovat soupaci tlacitka */
    public void refreshScroll() {
        if (count <= 3 && scrollLeft != null && scrollRight != null) {
            scrollPosition = 0;
            scrollLeft.setVisible(false);
            scrollRight.setVisible(false);
            DOM.setStyleAttribute(tabPanel.getElement(), "left", "0px");
            tabPanelWrapper.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - FULL_DIFF) + "px");
            scrolling = false;
            return;
        }
        if (count > 3) {
            if (!scrolling) {
                if (scrollLeft == null) {
                    scrollLeft = new FocusPanel();
                    scrollLeft.setSize("14px", "38px");
                    Image left = new Image(ResourceUtils.system().tabLeft());
                    left.setStyleName(StyleUtils.MARGIN_TOP_10);
                    scrollLeft.setWidget(left);
                    scrollLeft.addStyleName(StyleUtils.HAND);
                    scrollLeft.addStyleName(StyleUtils.FLOAT_LEFT);
                    scrollLeft.addClickHandler(scrollLeftClickHandler());
                    panelWithScroll.insert(scrollLeft, 0);
                }
                if (scrollRight == null) {
                    scrollRight = new FocusPanel();
                    scrollRight.setSize("15px", "38px");
                    Image right = new Image(ResourceUtils.system().tabRight());
                    right.setStyleName(StyleUtils.MARGIN_TOP_10);
                    scrollRight.setWidget(right);
                    scrollRight.addStyleName(StyleUtils.HAND);
                    scrollRight.addStyleName(StyleUtils.FLOAT_RIGHT);
                    scrollRight.addStyleName(StyleUtils.ALIGN_RIGHT);
                    scrollRight.addClickHandler(scrollRightClickHandler());
                    panelWithScroll.add(scrollRight);
                    tabPanelWrapper.addStyleName(StyleUtils.FLOAT_LEFT);
                }
                showScrollers();
            }
            scrollOnSelectedTab();
        } else {
            if (scrolling) {
                scrollPosition = 0;
                scrollLeft.setVisible(false);
                scrollRight.setVisible(false);
                DOM.setStyleAttribute(tabPanel.getElement(), "left", "0px");
                tabPanelWrapper.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - FULL_DIFF) + "px");
                scrolling = false;
            }
        }
    }

    /** zobrazi skrolovatka podle pozice */
    private void showScrollers() {
        if (scrollLeft == null) {
            return;
        }
        tabPanelWrapper.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - FULL_DIFF) + "px");
        if (scrollPosition == 0) {
            scrollLeft.setVisible(false);
            scrollRight.setVisible(true);
        } else if (scrollPosition >= count - 2) {
            scrollRight.setVisible(false);
            scrollLeft.setVisible(true);
        } else {
            scrollLeft.setVisible(true);
            scrollRight.setVisible(true);
        }
    }

    /** click handler pro posunovani doleva */
    private ClickHandler scrollLeftClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                scrollPosition--;
                if (scrollPosition < 0) {
                    scrollPosition = 0;
                }
                scroll();
            }
        };
    }

    /** click handler pro posunovani doprava */
    private ClickHandler scrollRightClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                scrollPosition++;
                if (scrollPosition > count - 1) {
                    scrollPosition = count - 1;
                }
                scroll();
            }
        };
    }

    private void scroll() {
        int scroll = 0;
        for (int i = 0; i < scrollPosition; i++) {
            scroll += ConsysTabPanelItemUI.ITEM_WIDTH;
            // mezera
            scroll += 10;
        }
        DOM.setStyleAttribute(tabPanel.getElement(), "left", -scroll + "px");
        showScrollers();
    }

    /** prescroluje na vybranou polozku */
    public void scrollOnSelectedTab() {
        if (lastSelected != null && lastSelected.getId() != null) {
            T id = lastSelected.getId();
            for (int i = 0; i < tabPanel.getWidgetCount(); i++) {
                ConsysTabPanelItemUI<T> item = (ConsysTabPanelItemUI<T>) tabPanel.getWidget(i);
                if (id.equals(item.getId())) {
                    if (!(i >= scrollPosition && i <= scrollPosition + 2)) {
                        scrollPosition = i;
                        if (scrollPosition > count - 2) {
                            scrollPosition = count - 2;
                        }

                        if (scrollPosition < 2) {
                            scrollPosition = 0;
                        }
                        scroll();
                    }
                    return;
                }
            }
        }
    }

    /** u vybrane zalozky se bude zobrazovat ikona pro zmenu poradi */
    public void showOrderIcon(ConsysAction action) {
        this.showOrderIcon = true;
        this.orderIconAction = action;
        for (int i = 0; i < tabPanel.getWidgetCount(); i++) {
            Widget w = tabPanel.getWidget(i);
            if (w instanceof ConsysTabPanelItemUI) {
                ConsysTabPanelItemUI tabItem = (ConsysTabPanelItemUI) w;
                tabItem.setShowChangeOrderIcon(true);
                tabItem.setChangeOrderAction(orderIconAction);
                if (lastSelected == tabItem) {
                    tabItem.showChangeOrderIcon(true);
                }
            }
        }
    }

    /** vrati seznam zalozek */
    public ArrayList<ConsysTabPanelItemUI<T>> getTabs() {
        ArrayList<ConsysTabPanelItemUI<T>> tabs = new ArrayList<ConsysTabPanelItemUI<T>>();
        for (int i = 0; i < tabPanel.getWidgetCount(); i++) {
            Widget w = tabPanel.getWidget(i);
            if (w instanceof ConsysTabPanelItemUI) {
                tabs.add((ConsysTabPanelItemUI<T>) w);
            }
        }
        return tabs;
    }

    /** zmeni pozici zalozky */
    public void changeTabPosition(ConsysTabPanelItemUI<T> ui, int oldPosition, int newPosition) {
        if (oldPosition > newPosition) {
            tabPanel.insert(ui, newPosition);
        } else {
            tabPanel.insert(ui, newPosition + 1);
        }
    }
}
