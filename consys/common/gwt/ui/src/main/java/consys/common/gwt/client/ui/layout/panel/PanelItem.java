package consys.common.gwt.client.ui.layout.panel;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.event.UriFragmentTokenEvent;

/**
 * Abstraktni trida polozky v panelu
 * @author pepa
 */
public abstract class PanelItem implements UriFragmentTokenEvent.Handler{

    private boolean minimalizable;

    public PanelItem(boolean minimalizable) {
        this.minimalizable = minimalizable;
    }

    /** vraci unikatni nazev panelu */
    public abstract String getName();

    /** titulek polozky, muze vracet null */
    public abstract Widget getTitle();

    /** obsah polozky */
    public abstract Widget getContent();

    public void onLoad(){
        // urcene k pretazeniu
    }
    
    public void onUnload(){
        // urcene k pretazeniu
    }
    
    /** @return true pokud ma byt polozka minimalizovatelna */
    public boolean isMinimalizable() {
        return minimalizable;
    }

    /**
     * Implementuju potomkovia
     * @param event 
     */
    @Override
    public void onCatchedFragment(UriFragmentTokenEvent event) {
        // no-op -
    }
    
    
}
