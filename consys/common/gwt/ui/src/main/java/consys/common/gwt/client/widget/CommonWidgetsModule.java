package consys.common.gwt.client.widget;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface CommonWidgetsModule {
    /** pro zobrazeni aktivacniho formu */
    public static final String ACTIVATE_ACCOUNT = "ActivateAccount";
    /** pro zobrazeni formu pro zapomenute heslo */
    public static final String LOST_PASSWORD = "LostPassword";
    /** pro zobrazeni formu s registraci do systemu */
    public static final String REGISTER_ACCOUNT = "RegisterAccount";
}
