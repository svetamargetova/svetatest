package consys.common.gwt.client.ui.comp.tag;

import com.google.gwt.user.client.ui.HTML;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientSuggestion;

/**
 *
 * @author pepa
 */
public class TagSuggestion implements ClientSuggestion<String> {

    // data
    private Long id;
    private String name;
    private int boldLength;

    public TagSuggestion(Long id, String name, String query) {
        this.id = id;
        this.name = name;
        boldLength = query.length();
    }

    /** vraci id tagu nebo null */
    public Long getTagId() {
        return id;
    }

    @Override
    public String getSuggestionId() {
        return id == null ? null : id.toString();
    }

    @Override
    public String getDisplayString() {
        HTML html = new HTML();
        html.setStyleName(StyleUtils.ALIGN_LEFT);
        html.addStyleName(StyleUtils.PADDING_VER_5);
        html.addStyleName(StyleUtils.PADDING_HOR_5);
        if (name.length() == boldLength) {
            html.setHTML("<b>" + name + "</b>");
        } else {
            String s1 = name.substring(0, boldLength);
            String s2 = name.substring(boldLength);
            html.setHTML("<b>" + s1 + "</b>" + s2);
        }
        return html.toString();
    }

    @Override
    public String getReplacementString() {
        return name;
    }
}
