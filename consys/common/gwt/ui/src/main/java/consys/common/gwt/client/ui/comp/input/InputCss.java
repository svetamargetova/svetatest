package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 *
 * @author pepa
 */
@ImportedWithPrefix("input")
public interface InputCss extends CssResource {

    String consysTextArea();

    String consysTextAreaInfoPanel();

    String consysAutoFileUpload();

    String error();

    String uploading();

    String uploaded();

    String consysTextBox();

    String consysTextBoxInput();

    String consysFileUpload();

    String uploadWrapper();

    String uploadFaceWrapper();

    String infoLabel();

    String disabled();

    String consysPasswordBox();

    String wrapper();

    String consysSelectBox();

    String consysSelectBoxInput();

    String consysSelectBoxLabel();

    String arrowPanel();

    String consysTextBoxCenterWrapper();
}
