package consys.common.gwt.client.ui.comp.panel.element;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class ConsysTabPanelItemUI<T> extends FlowPanel {

    // konstanty
    public static final int ITEM_WIDTH = 213;
    private static final String HEIGHT = "39px";
    private static final String INNER_WIDTH = "203px";
    private static final String BORDER_WIDTH = "5px";
    private static final String LABEL_WIDTH = "167px";
    private static final String LABEL_HEIGHT = "28px";
    private static final String LABEL_WIDTH_WITHOUT_ICON = "183px";
    private static final String ORDER_ICON_WIDTH = "16px";
    // komponenty
    private ConsysTabPanelLabel<T> titleLabel;
    private Image leftBorder;
    private Image rightBorder;
    private FlowPanel titlePanel;
    private SimplePanel orderPanel;
    private Image changeOrder;
    // data
    private String panelName;
    private ConsysTabPanelItem item;
    private Widget widget;
    private ConsysAction onSelectAction;
    private HandlerRegistration changeOrderHandlerRegistration;
    private boolean showOrderIcon;

    public ConsysTabPanelItemUI(String panelName, ConsysTabPanelLabel<T> label, final ConsysTabPanelItem item,
            boolean selected, final ConsysAction onSelectAction) {
        this.panelName = panelName;
        this.onSelectAction = onSelectAction;
        showOrderIcon = false;

        addStyleName(StyleUtils.MARGIN_RIGHT_10);
        addStyleName(StyleUtils.FLOAT_LEFT);

        this.item = item;
        widget = item.getContent();

        titleLabel = label;
        titleLabel.setSize(LABEL_WIDTH_WITHOUT_ICON, LABEL_HEIGHT);
        titleLabel.setStyleName(StyleUtils.FONT_16PX);
        titleLabel.addStyleName(StyleUtils.FONT_BOLD);
        titleLabel.addStyleName(StyleUtils.TEXT_NOWRAP);
        titleLabel.addStyleName(StyleUtils.UNSELECTABLE);
        titleLabel.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        titleLabel.addStyleName(StyleUtils.HAND);
        titleLabel.addStyleName(StyleUtils.MARGIN_HOR_10);
        DOM.setStyleAttribute(titleLabel.getElement(), "marginTop", "8px");

        if (!selected) {
            titleLabel.addStyleName(StyleUtils.TEXT_BLUE);
        }

        changeOrder = new Image(ResourceUtils.system().changeOrder());
        changeOrder.addStyleName(StyleUtils.HAND);

        orderPanel = new SimplePanel();
        orderPanel.setWidth(ORDER_ICON_WIDTH);
        orderPanel.addStyleName(StyleUtils.FLOAT_RIGHT);
        orderPanel.addStyleName(StyleUtils.MARGIN_TOP_8);
        orderPanel.add(changeOrder);
        orderPanel.setVisible(false);

        titlePanel = new FlowPanel();
        titlePanel.setSize(INNER_WIDTH, HEIGHT);
        titlePanel.setStyleName(selected ? ResourceUtils.system().css().tabSelectedCenter() : ResourceUtils.system().css().tabUnselectedCenter());

        titlePanel.add(orderPanel);
        titlePanel.add(titleLabel);
        if (item.getTitleNote() != null) {
            // TODO: pocitat se sirkou, float left, ... predtim se vyuzivalo vlastnosti tabulky
            titlePanel.add(item.getTitleNote());
        }
        titlePanel.add(StyleUtils.clearDiv());

        leftBorder = new Image(selected ? ResourceUtils.system().tabSLeft() : ResourceUtils.system().tabULeft());
        SimplePanel leftBorderWrapper = new SimplePanel();
        leftBorderWrapper.setWidth(BORDER_WIDTH);
        leftBorderWrapper.addStyleName(StyleUtils.FLOAT_LEFT);
        leftBorderWrapper.setWidget(leftBorder);

        rightBorder = new Image(selected ? ResourceUtils.system().tabSRight() : ResourceUtils.system().tabURight());
        SimplePanel rightBorderWrapper = new SimplePanel();
        rightBorderWrapper.setWidth(BORDER_WIDTH);
        rightBorderWrapper.addStyleName(StyleUtils.FLOAT_LEFT);
        rightBorderWrapper.setWidget(rightBorder);

        add(leftBorderWrapper);
        add(titlePanel);
        add(rightBorderWrapper);
        add(StyleUtils.clearDiv());
    }

    public void setParent(final ConsysTabPanel tabPanel) {
        titleLabel.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (ConsysTabPanelItemUI.this != tabPanel.getSelectedTab()) {
                    tabPanel.getContent().setWidget(item.getContent());
                    tabPanel.getSelectedTab().unselect();
                    tabPanel.setSelectedTab(ConsysTabPanelItemUI.this);
                    select();
                }
            }
        });
    }

    /** necha zobrazit nebo skryt ikonu pro zmenu poradi */
    public void showChangeOrderIcon(boolean show) {
        orderPanel.setVisible(show);
        titleLabel.setWidth(show ? LABEL_WIDTH : LABEL_WIDTH_WITHOUT_ICON);
    }

    /** nastavi, ze se ma/nema zobrazovat ikona pro zmenu poradi */
    public void setShowChangeOrderIcon(boolean showOrderIcon) {
        this.showOrderIcon = showOrderIcon;
    }

    /** nactavi akci po kliknuti na tlacitko zmena poradi */
    public void setChangeOrderAction(final ConsysAction action) {
        if (changeOrderHandlerRegistration != null) {
            changeOrderHandlerRegistration.removeHandler();
        }
        changeOrderHandlerRegistration = changeOrder.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                action.run();
            }
        });
    }

    /** ulozi co cache naposled vybranou zalozku pro zalozkovy panel urceny jeho nazvem */
    private void registerSelectedTab() {
        Cache.get().registerSelectedTab(ConsysTabPanel.cachePanelName(panelName), titleLabel.getId());
    }

    /** oznaci zalozku za vybranou */
    public void select() {
        registerSelectedTab();

        leftBorder.setResource(ResourceUtils.system().tabSLeft());
        rightBorder.setResource(ResourceUtils.system().tabSRight());
        titlePanel.setStyleName(ResourceUtils.system().css().tabSelectedCenter());
        titleLabel.removeStyleName(StyleUtils.TEXT_BLUE);
        runOnSelectAction();
        if (showOrderIcon) {
            showChangeOrderIcon(true);
        }

        titleLabel.getTab().scrollOnSelectedTab();
    }

    /** pokud je nastavena onsSelectAction, provola ji */
    public void runOnSelectAction() {
        if (onSelectAction != null) {
            onSelectAction.run();
        }
    }

    /** oznaci zalozku za nevybranou */
    public void unselect() {
        leftBorder.setResource(ResourceUtils.system().tabULeft());
        rightBorder.setResource(ResourceUtils.system().tabURight());
        titlePanel.setStyleName(ResourceUtils.system().css().tabUnselectedCenter());
        titleLabel.addStyleName(StyleUtils.TEXT_BLUE);
        if (showOrderIcon) {
            showChangeOrderIcon(false);
        }
    }

    /** vraci obsah zalozky */
    public Widget getContent() {
        return widget;
    }

    /** vraci nazev zalozky */
    public String getTabName() {
        return titleLabel.getFullText();
    }

    /** vraci identifikacni objekt zalozky */
    public T getId() {
        return titleLabel.getId();
    }

    /** nastavi identifikacni objekt zalozky */
    public void setId(T id) {
        titleLabel.setId(id);
    }
}
