package consys.common.gwt.client.ui.comp.tag;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.shared.bo.ClientTag;
import java.util.ArrayList;

/**
 * Slouzi pro zobrazovani tagu / topiku
 * @author pepa
 */
public class TagDisplay extends Composite {

    // komponenty
    private FlowPanel panel;
    // data
    private boolean readMode;
    private ArrayList<InsertTagHandler> insertHandlers;
    private ArrayList<RemoveTagHandler> removeHandlers;

    public TagDisplay() {
        insertHandlers = new ArrayList<InsertTagHandler>();
        removeHandlers = new ArrayList<RemoveTagHandler>();

        panel = new FlowPanel() {

            @Override
            public boolean remove(Widget w) {
                if (w instanceof Tag) {
                    for (RemoveTagHandler h : removeHandlers) {
                        h.removeTagAction(((Tag) w).getId());
                    }
                }
                return super.remove(w);
            }
        };
        initWidget(panel);
    }

    /** vymazani vsech tagu v komponente */
    public void clear() {
        panel.clear();
    }

    /** je zobrazovatko ve ctecim rezimu? */
    public boolean isReadMode() {
        return readMode;
    }

    /** nastaveni zobrazovatka do cteciho nebo editacniho rezimu */
    public void setReadMode(boolean readMode) {
        this.readMode = readMode;
        for (int i = 0; i < panel.getWidgetCount(); i++) {
            Widget w = panel.getWidget(i);
            if (w instanceof Tag) {
                ((Tag) w).setReadMode(readMode);
            }
        }
    }

    /** retezec oddeleny carkama, POZOR!!! tagy jsou vedeny bez id */
    public void setStringTags(String tags, String separator) {
        String[] s = tags.split(separator);

        panel.clear();
        for (int i = 0; i < s.length; i++) {
            panel.add(new Tag(null, s[i].trim(), readMode));
        }
    }

    /** seznam objektu tagu */
    public void setArrayTags(ArrayList<ClientTag> tags) {
        panel.clear();
        for (ClientTag t : tags) {
            panel.add(new Tag(t.getId(), t.getName(), readMode));
        }
    }

    /** pridani dagu do zobrazovatka */
    public void addTag(Long id, String name) {
        panel.add(new Tag(id, name, readMode));
        for (InsertTagHandler h : insertHandlers) {
            h.insertTagAction(id, name);
        }
    }

    /** vraci true pokud uz zobrazovatko obsahuje tag se zadanym nazvem */
    public boolean containsTag(String name) {
        name = name.toLowerCase();
        for (int i = 0; i < panel.getWidgetCount(); i++) {
            Tag t = (Tag) panel.getWidget(i);
            if (t.getName().toLowerCase().equals(name)) {
                return true;
            }
        }
        return false;
    }

    /** handler pro zpracovani odebiraneho tagu */
    public void addRemoveTagHandler(RemoveTagHandler handler) {
        removeHandlers.add(handler);
    }

    /** handler pro zpracovani pridavaneho tagu */
    public void addInsertTagHandler(InsertTagHandler handler) {
        insertHandlers.add(handler);
    }

    /** handler pro zachytavani vlozeni tagu do TagDisplay */
    public interface InsertTagHandler {

        public void insertTagAction(Long id, String name);
    }

    /** handler pro zachytavani odebrani tagu do TagDisplay */
    public interface RemoveTagHandler {

        public void removeTagAction(Long id);
    }
}
