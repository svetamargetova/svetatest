package consys.common.gwt.client.ui.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface ConsysConstants extends Constants {

    String locale();

    String accessDeniedContent_error();

    String const_accept();

    String const_acronym();

    String const_add();

    String const_addMore();

    String const_address_city();

    String const_address_location();

    String const_address_place();

    String const_address_state();

    String const_address_street();

    String const_address_zip();

    String const_apply();

    String const_at();

    String const_back();

    String const_cancel();

    String const_change();

    String const_check();

    String const_close();

    String const_clearForm();

    String const_confirmPassword();

    String const_copy();

    String const_currency();

    String const_date_agoLower();

    String const_date_monthApril();

    String const_date_monthAugust();

    String const_date_monthDecember();

    String const_date_monthFebruary();

    String const_date_monthJanuary();

    String const_date_monthJuly();

    String const_date_monthJune();

    String const_date_monthMarch();

    String const_date_monthMay();

    String const_date_monthNovember();

    String const_date_monthOctober();

    String const_date_monthSeptember();

    String const_date_year();

    String const_date_yesterday();

    String const_delete();

    String const_decline();

    String const_description();

    String const_discountCode();

    String const_download();

    String const_duplicate();

    String const_edit();

    String const_email();

    String const_error_invalidEmailAddress();

    String const_error_noPermissionForAction();

    String const_exceptionBadIban();

    String const_exceptionBadInput();

    String const_exceptionNotPermittedAction();

    String const_exceptionServiceFailed();

    String const_free();

    String const_freeUpper();

    String const_gender();

    String const_genderMan();

    String const_genderNotSpec();

    String const_genderWoman();

    String const_isLower();

    String const_item_optionalLower();

    String const_item_requiredLower();

    String const_loadingDots();

    String const_more();

    String const_name();

    String const_no();

    String const_noneLower();

    String const_noRecordFound();

    String const_organization();

    String const_password();

    String const_personalInfo_celiac();

    String const_personalInfo_vegetarian();

    String const_phoneNumber();

    String const_position();

    String const_property();

    String const_remove();

    String const_save();

    String const_saveUpper();

    String const_select();

    String const_sendUpper();

    String const_settings();

    String const_showPassword();

    String const_ssoNotWork();

    String const_submit();

    String const_toLower();

    String const_timeZone();

    String const_unlimited();

    String const_update();

    String const_user_firstName();

    String const_user_lastName();

    String const_warning();

    String const_website();

    String const_yes();

    String consysAutoFileUpload_text_uploaded();

    String consysAutoFileUpload_text_uploading();

    String consysFileUpload_button_browsFile();

    int consysFileUpload_button_width();

    String consysFileUpload_text_chooseFileToUpload();

    String consysPasswordBox_error_passwordNotEntered();

    String consysPasswordBox_error_passwordsNotSame();

    String consysRichTextToolbar_text_bold();

    String consysRichTextToolbar_text_createLink();

    String consysRichTextToolbar_text_enterURL();

    String consysRichTextToolbar_text_italic();

    String consysRichTextToolbar_text_list();

    String consysRichTextToolbar_text_removeFormat();

    String consysRichTextToolbar_text_removeLink();

    String consysRichTextToolbar_text_strikethrough();

    String consysRichTextToolbar_text_underline();

    String consysWordArea_text_left();

    String dataList_error();

    String dataList_text_empty();

    String dataListCommon_text_show();

    String dataListCommon_text_sortBy();

    String help_text_noDescription();

    String help_text_noTitle();

    String loginPanel_text_signUpWith();

    String menuDispatcherImpl_mainMenu_aboutUs();

    String menuDispatcherImpl_mainMenu_customers();

    String menuDispatcherImpl_mainMenu_faq();

    String menuDispatcherImpl_mainMenu_features();

    String menuDispatcherImpl_mainMenu_home();

    String menuDispatcherImpl_mainMenu_overview();

    String menuDispatcherImpl_mainMenu_pressAndMedia();

    String menuDispatcherImpl_mainMenu_signUp();

    String paymentPriceList_text_name();

    String paymentPriceList_text_quantity();

    String paymentPriceList_text_total();

    String paymentPriceList_text_unitPrice();

    String paymentProfilePanel_error_badIban();

    String paymentProfilePanel_form_accountNumber();

    String paymentProfilePanel_form_bankNumber();

    String paymentProfilePanel_form_registrationNumber();

    String paymentProfilePanel_form_vatNumber();

    String paymentProfilePanel_text_invoicedTo();

    String paymentProfilePanel_text_supportedIban();

    String paymentProfilePanel_text_supportedSwift();

    String paymentProfilePanel_text_supportedText();

    String paymentProfilePanel_title();

    String progress_text1();

    String progress_text2();

    String progress_text3();

    String remover_text_areYouSure();

    String servletUtils_error_http400();

    String servletUtils_error_http401();

    String servletUtils_error_http403();

    String servletUtils_error_http404();

    String servletUtils_error_http406();

    String servletUtils_error_http413();

    String servletUtils_error_http415();

    String servletUtils_error_http500();

    String ssoLogin_text_orUseTakeplaceAccount();

    String subbundlePanel_action_showDetails();

    String system_locale();

    String unknownTokenContent_text();

    String userCard_error_userDataNotYetInEvent();

    String userInvitePanel_button_invite();

    String userInvitePanel_form_name();

    String userSearchDialog_action_invite();

    String userSearchDialog_helpText();
}
