package consys.common.gwt.client.ui.comp.panel.abst;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;

/**
 * Interface pro create, read, update a delete ve formulari
 * @author pepa
 */
public interface CRUD extends RU{

    /** zobrazi pohled pro vytvoreni */
    public Widget createForm();

    /** zobrazi pohled pro smazani */
    public Widget deleteForm(String id);
    
    /** ClickHandler pro potvrzeni smazani */
    public ClickHandler confirmDeleteClickHandler(String id);

     /** ClickHandler pro potvrzeni vytvoreni */
    public ClickHandler confirmCreateClickHandler();

    public ClickHandler createClickHandler();

    public ClickHandler deleteClickHandler(final String id);

}
