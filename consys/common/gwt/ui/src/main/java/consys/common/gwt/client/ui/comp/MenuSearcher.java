package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * Pole pro vyhledavani umisteny v top menu
 * @author pepa
 */
public class MenuSearcher extends Composite {

    public MenuSearcher() {
        HorizontalPanel wrapper = new HorizontalPanel();
        wrapper.addStyleName(ResourceUtils.system().css().menuSearcherTop());

        TextBox box = new TextBox();
        box.setStyleName(ResourceUtils.system().css().menuSearcherInput());

        FocusPanel submit = new FocusPanel();
        submit.setStyleName(ResourceUtils.system().css().menuSearcherSubmit());

        wrapper.add(box);
        wrapper.add(submit);
        initWidget(wrapper);
    }
}
