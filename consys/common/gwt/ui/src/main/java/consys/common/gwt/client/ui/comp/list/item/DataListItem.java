package consys.common.gwt.client.ui.comp.list.item;

import java.io.Serializable;

/**
 * Objekt ktory je jeden zaznam v dataliste.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DataListItem implements Serializable{
    private static final long serialVersionUID = -9019908371729861161L;

}
