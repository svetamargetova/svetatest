package consys.common.gwt.client.ui.layout.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ClickFlowPanel;
import consys.common.gwt.client.ui.event.SelectedAddonSubItemEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 * Model jedneho zaznamu subitemu
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PanelAddonSubItem extends ClickFlowPanel implements SelectedAddonSubItemEvent.Handler {

    private boolean selected = false;
    private final String name;
    private final String urlName;
    private final ConsysAction action;

    public PanelAddonSubItem(String name, String urlName, ConsysAction action) {
        super();
        this.name = name;
        this.urlName = urlName;
        this.action = action;
        setSelected(false);
        Label label = new Label(name);
        label.setStyleName(ResourceUtils.system().css().text());
        addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showItem();
            }
        });
        add(label);

    }

    public void showItem() {
        SelectedAddonSubItemEvent selectEvent = new SelectedAddonSubItemEvent(PanelAddonSubItem.this);
        EventBus.get().fireEvent(selectEvent);
        setSelected(true);
        action.run();
    }

    public String getName() {
        return name;
    }

    public String getUrlName() {
        return urlName;
    }

    public final void setSelected(boolean selected) {
        this.selected = selected;
        if (selected) {
            setStyleName(ResourceUtils.system().css().subAddonItemSelected());
        } else {
            setStyleName(ResourceUtils.system().css().subAddonItem());
        }
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(SelectedAddonSubItemEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(SelectedAddonSubItemEvent.TYPE, this);
    }

    @Override
    protected void onAttach() {
        super.onAttach();
    }

    @Override
    public void onSelect(PanelAddonSubItem newSelected) {
        if (selected && !this.equals(newSelected)) {
            setSelected(false);
        }
    }
}
