package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 *
 * @author pepa
 */
public abstract class BreadcrumbEvent<H extends EventHandler> extends GwtEvent<H> {
}
