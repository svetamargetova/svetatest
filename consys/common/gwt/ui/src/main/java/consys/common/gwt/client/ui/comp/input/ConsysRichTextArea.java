package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.dom.client.BodyElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.IFrameElement;
import com.google.gwt.dom.client.LinkElement;
import com.google.gwt.event.logical.shared.InitializeEvent;
import com.google.gwt.event.logical.shared.InitializeHandler;
import com.google.gwt.user.client.ui.RichTextArea;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class ConsysRichTextArea extends RichTextArea {

    public static final String WIDTH = "270";
    public static final String HEIGHT = "80";

    public ConsysRichTextArea() {
        this(WIDTH, HEIGHT);
    }

    public ConsysRichTextArea(String width) {
        this(width, HEIGHT);
    }

    public ConsysRichTextArea(String width, String height) {
        setStyleName(StyleUtils.BORDER);

        getElement().setAttribute("width", width);
        getElement().setAttribute("height", height);
        getElement().setAttribute("frameBorder", "0");

        addInitializeHandler(initializeHandler());
    }

    public InitializeHandler initializeHandler() {
        return new InitializeHandler() {

            @Override
            public void onInitialize(InitializeEvent ie) {
                IFrameElement iFrame = IFrameElement.as(getElement());
                Document document = iFrame.getContentDocument();
                BodyElement body = document.getBody();
                HeadElement head = HeadElement.as(Element.as(body.getPreviousSibling()));
                LinkElement styleLink = document.createLinkElement();
                styleLink.setType("text/css");
                styleLink.setRel("stylesheet");
                styleLink.setHref(ResourceUtils.system().richTextAreaCSS().getUrl());
                head.appendChild(styleLink);
            }
        };
    }
}
