package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

/**
 *ˆ
 * @author palo
 */
public interface UserResources extends ClientBundle {

    public static final UserResources INSTANCE = GWT.create(UserResources.class);

    /** css styl pro wrapper komponenty */
    @Source("consys/common/gwt/client/ui/comp/user/user.css")
    public UserCss userCss();
}
