/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.gwt.client.ui.utils;

/**
 *
 * @author palo
 */
public enum LabelColorEnum {

    SUCCESS_COLOR,FAIL_COLOR,WARNING_COLOR,QUESTION_COLOR;

    public static final String DEFAULT_SUCCESS_COLOR = "#7a9e24";
    public static final String DEFAULT_FAIL_COLOR = "#c20d0d";
    public static final String DEFAULT_WARNING_COLOR = "#a0a106";
    public static final String DEFAULT_QUESTION_COLOR = "#2987b3";


    public static String getHexColor(LabelColorEnum label){
        switch(label){
            case FAIL_COLOR: return DEFAULT_FAIL_COLOR;
            case QUESTION_COLOR: return DEFAULT_QUESTION_COLOR;
            case WARNING_COLOR: return DEFAULT_WARNING_COLOR;
            case SUCCESS_COLOR: return DEFAULT_SUCCESS_COLOR;
            default: return "purple";
        }
    }



}
