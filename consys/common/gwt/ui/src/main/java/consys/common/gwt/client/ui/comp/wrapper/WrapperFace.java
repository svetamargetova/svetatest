package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.ui.Image;

/**
 * Definuje vzhled wrapperu
 * @author pepa
 */
public class WrapperFace {

    private Image[] corners;
    private int cornerSize;
    private String backgroundStyle;
    private String[] borderStyles;

    /**
     * @param corners pole rozku wrapperu v poradi: levy dolni, pravy dolni, pravy horni, levy horni
     * @param cornerSize velikost rozku v pixelech (predpoklada se ze jsou rozky ctvercove, napr.: rozek 3x3 bude mit parametr 3)
     * @param backgroundStyle nazev stylu pozadi wrapperu
     */
    public WrapperFace(Image[] corners, int cornerSize, String backgroundStyle) {
        this.corners = corners;
        this.cornerSize = cornerSize;
        this.backgroundStyle = backgroundStyle;
    }

    /** vraci jednotny styl pozadi */
    public String getBackgroundStyle() {
        return backgroundStyle;
    }

    /** vraci pole stylu hran v poradi: dolni, pravy, horni, levy */
    public String[] getBorderStyles() {
        return borderStyles;
    }

    /** @return true pokud jsou hrany stylovane zvlast */
    public boolean isBorderStyled() {
        return borderStyles != null;
    }

    /** nastavi pole stylu hran v poradi: dolni, pravy, horni, levy */
    public void setBorderStyles(String[] borderStyles) {
        this.borderStyles = borderStyles;
    }

    /** vraci velikost hrany rozku v pixelech */
    public int getCornerSize() {
        return cornerSize;
    }

    /** vraci pole rozku wrapperu v poradi: levy dolni, pravy dolni, pravy horni, levy horni */
    public Image[] getCorners() {
        return corners;
    }
}
