package consys.common.gwt.client.ui.comp.select;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;

/**
 * Objekt vyskakovaciho panelu selectboxu
 * @author pepa
 */
public abstract class ItemBoard<T> extends PopupPanel {

    // konstanty
    private static final String CONTAINER_STYLE = "consysSelectBox";
    private static final int ITERATE_STEP = 10;
    private static final int MAX_SHOW_ITEMS = 7;
    public static final int ITEM_HEIGHT = 15;
    // komponenty
    private FlowPanel panel;
    private Element container;
    // data
    private Widget parent;
    private int iterance;
    private ArrayList<SBItem> labelList;
    private SBItem selectedLabel;
    private SelectBoxItem<T> preselectedItem;
    // data - inicializace
    private SBItem previous = null;
    private boolean notInitialized = true;

    public ItemBoard(String width, Widget parent) {
        super(true);
        this.parent = parent;
        labelList = new ArrayList<SBItem>();
        getElement().setClassName("posI95");

        container = getContainerElement();
        container.setClassName(CONTAINER_STYLE);
        DOM.setStyleAttribute(container, "width", width);
        DOM.setStyleAttribute(container, "textAlign", "left");

        panel = new FlowPanel();
        panel.setWidth(width);
        panel.addStyleName(StyleUtils.BACKGROUND_WHITE);
        setWidget(panel);
    }

    /** nastavi polozky, ktere se maji zobrazit na vyber */
    public void setItems(final ArrayList<SelectBoxItem<T>> list) {
        iterance = 0;
        panel.clear();
        labelList.clear();
        notInitialized = true;
        if (list == null) {
            return;
        }

        int height = (list.size() < MAX_SHOW_ITEMS ? list.size() * ITEM_HEIGHT : ITEM_HEIGHT * MAX_SHOW_ITEMS);
        DOM.setStyleAttribute(getContainerElement(), "height", height + "px");

        Timer t = new Timer() {

            @Override
            public void run() {
                if (iterance * ITERATE_STEP >= list.size()) {
                    cancel();
                    notInitialized = false;

                    if (preselectedItem != null) {
                        setBoardSelectedItem(preselectedItem);
                    }
                }

                int iterateStart = iterance * ITERATE_STEP;
                for (int index = iterateStart; index < iterateStart + 10 && index < list.size(); index++) {
                    final SelectBoxItem<T> i = list.get(index);
                    final SBItem<T> label = new SBItem<T>(ItemBoard.this, i);
                    label.addClickHandler(new ClickHandler() {

                        @Override
                        public void onClick(ClickEvent event) {
                            // vyvolani eventu
                            EventBus.get().fireEvent(new ChangeValueEvent<SelectBoxItem>(parent, getSelectedItem(), i));
                            // vybrani nove hodnoty a skryti
                            setSelectedItem(i);
                            selectLabel(label);
                            hide();
                        }
                    });

                    if (previous != null) {
                        previous.setNext(label);
                    }
                    label.setPrevious(previous);
                    previous = label;

                    panel.add(label);
                    labelList.add(label);
                }
                iterance++;
            }
        };
        // naplneni at je videt neco hned po kliknuti
        t.run();
        // spousteni zbytku iteraci naplneni
        t.scheduleRepeating(100);
    }

    /** vraci aktualne vybrany label v boardu */
    SBItem getSelectedLabel() {
        return selectedLabel;
    }

    /** vybere a oznaci label */
    private void selectLabel(SBItem label) {
        label.removeStyleName(StyleUtils.BACKGROUND_GRAY);
        if (selectedLabel != null) {
            selectedLabel.removeStyleName(StyleUtils.BACKGROUND_GREEN);
        }
        label.addStyleName(StyleUtils.BACKGROUND_GREEN);
        selectedLabel = label;
        scrollOnSelectedLabel();
    }

    /** prescrolluje na vybrany label pokud neni null */
    public void scrollOnSelectedLabel() {
        if (selectedLabel != null) {
            container.setScrollTop(labelList.indexOf(selectedLabel) * ITEM_HEIGHT);
        }
    }

    /** posune vyber o jednu polozku nahoru */
    public void up() {
        if (selectedLabel != null && selectedLabel.getPrevious() != null) {
            selectLabel(selectedLabel.getPrevious());
        }
    }

    /** posune vyber o jednu polozku dolu */
    public void down() {
        if (selectedLabel != null) {
            if (selectedLabel.getNext() != null) {
                selectLabel(selectedLabel.getNext());
            }
        } else {
            if (!labelList.isEmpty()) {
                selectLabel(labelList.get(0));
            }
        }
    }

    /** vraci vybranou polozku v popupu */
    public SelectBoxItem<T> getBoardSelectedItem() {
        if (selectedLabel == null) {
            return null;
        }
        return selectedLabel.getItem();
    }

    /** vybere polozku v popupu */
    public void setBoardSelectedItem(SelectBoxItem<T> i) {
        if (i == null) {
            return;
        }
        if (notInitialized || labelList.isEmpty()) {
            preselectedItem = i;
            return;
        }
        for (SBItem<T> item : labelList) {
            if (item.getItem().getItem().equals(i.getItem())) {
                selectLabel(item);
                return;
            }
        }
    }

    /** vraci vybranou polozku selectboxu */
    public abstract SelectBoxItem<T> getSelectedItem();

    /** nastavuje polozku selectboxu */
    public abstract void setSelectedItem(SelectBoxItem<T> i);
}
