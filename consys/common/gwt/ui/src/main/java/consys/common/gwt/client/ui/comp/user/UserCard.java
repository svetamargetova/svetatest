package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.LoadUserInfoAction;
import consys.common.gwt.client.ui.comp.GreenPopupPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUserCommonInfo;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.shared.exception.NoRecordsForAction;

/**
 * Vizitka ktora by sa mala automaticky ukazat pri zavolani konstruktora.
 *
 * Vizitka by mala mat v hornom pravom horu kontextove menu obsahujuce:
 * profile - preklik na profil do noveho okna
 * close - zatvori vizitku
 *
 * Vizitka sa rovnako zatvori ak sa klikne niekde mimo viztiku.
 *
 * POZOR: Nacita uzivatele z eventu!
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserCard extends GreenPopupPanel {
    
    // komponenty
    private FlowPanel content;
    // data
    private String userUuid;
    private ClientUserCommonInfo commonInfo;

    public UserCard(String userUuid) {
        this.userUuid = userUuid;
    }

    public UserCard(ClientUserCommonInfo commonInfo) {
        super();        
        this.commonInfo = commonInfo;
    }

    @Override
    protected Widget getContent() {
        content = new FlowPanel();
        content.setStyleName(UserResources.INSTANCE.userCss().userCardContent());        
        return content;
    }

    public void setPosition(Widget w) {
        setPopupPosition(w.getAbsoluteLeft(), w.getAbsoluteTop());        
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        if (commonInfo == null) {
            EventBus.get().fireEvent(new EventDispatchEvent(new LoadUserInfoAction(userUuid),
                    new AsyncCallback<ClientUserCommonInfo>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava ActionExecutor
                            if (caught instanceof NoRecordsForAction) {
                                content.clear();
                                content.add(getFailLabel(UIMessageUtils.c.userCard_error_userDataNotYetInEvent()));
                            }
                        }

                        @Override
                        public void onSuccess(ClientUserCommonInfo result) {
                            content.clear();
                            generateContent(result);
                        }
                    }, this));
        } else {
            content.clear();
            generateContent(commonInfo);
        }
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        content.add(getFailLabel(UIMessageUtils.getFailMessage(value)));
    }

    private FlowPanel createControllPanel() {
        Image close = new Image(ResourceUtils.system().removeCross());
        close.setStyleName(StyleUtils.HAND);
        close.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserCard.this.hide();
            }
        });
        FlowPanel controlPanel = new FlowPanel();
        controlPanel.setStyleName(UserResources.INSTANCE.userCss().userCardContentControlls());
        controlPanel.add(close);
        return controlPanel;
    }

    private FlowPanel createPortraitImagePanel(final ClientUserCommonInfo data) {
        Image portrait = FormUtils.userPortrait(data.getImageUuid(), UserProfileImageEnum.LIST);
        FlowPanel portraitWrapper = new FlowPanel();
        portraitWrapper.setStyleName(UserResources.INSTANCE.userCss().userCardContentPortrait());
        portraitWrapper.add(portrait);
        return portraitWrapper;
    }

    private FlowPanel createUserInfoPanel(final ClientUserCommonInfo data) {
        FlowPanel dataPanel = new FlowPanel();
        dataPanel.setStyleName(UserResources.INSTANCE.userCss().userCardContentUserInfo());        
        dataPanel.add(new NameWithAffiliation(data.getFullName(), data.getAffilation(UIMessageUtils.c.const_at())));
        if (data.getEmail().contains("@")) {
            dataPanel.add(StyleUtils.getStyledLabel(data.getEmail(), StyleUtils.MARGIN_TOP_10, StyleUtils.MARGIN_BOT_5));
        }
        return dataPanel;
    }

    private Label getFailLabel(String text) {
        return StyleUtils.getStyledLabel(text, StyleUtils.TEXT_RED, StyleUtils.MARGIN_HOR_5, StyleUtils.MARGIN_VER_5);
    }

    private void generateContent(final ClientUserCommonInfo data) {        
        content.add(createControllPanel());
        content.add(createPortraitImagePanel(data));
        content.add(createUserInfoPanel(data));
        content.add(StyleUtils.clearDiv());
    }
}
