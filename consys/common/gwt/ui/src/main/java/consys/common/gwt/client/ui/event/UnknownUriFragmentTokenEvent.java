package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 * Event vystreleny history handlerem, kdyz nespracuje history token
 * @author pepa
 */
public class UnknownUriFragmentTokenEvent extends GwtEvent<UnknownUriFragmentTokenEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private String fragmane;

    public UnknownUriFragmentTokenEvent(String fragmane) {
        this.fragmane = fragmane;
    }

    public String getFragmane() {
        return fragmane;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onUnknownUriFragment(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onUnknownUriFragment(UnknownUriFragmentTokenEvent event);
    }
}
