package consys.common.gwt.client.ui.comp.panel.element;

/**
 *
 * @author pepa
 */
public class OrderPanelItem {

    // data
    private String name;

    public OrderPanelItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
