package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.ResourceUtils;

/**
 *
 * @author pepa
 */
public class ConsysBlueWrapper extends ConsysWrapper {

    public ConsysBlueWrapper() {
        super();
    }

    public ConsysBlueWrapper(Widget widget) {
        super(widget);
    }

    public ConsysBlueWrapper(Widget widget, int settings) {
        super(widget, settings);
    }

    @Override
    public WrapperFace getWrappertFace() {
        Image bottomLeft = new Image(ResourceUtils.system().corBlueBl());
        Image bottomRight = new Image(ResourceUtils.system().corBlueBr());
        Image topRight = new Image(ResourceUtils.system().corBlueTr());
        Image topLeft = new Image(ResourceUtils.system().corBlueTl());
        WrapperFace face = new WrapperFace(new Image[]{bottomLeft, bottomRight, topRight, topLeft}, 2,
                ResourceUtils.system().css().infoBg());
        return face;
    }
}
