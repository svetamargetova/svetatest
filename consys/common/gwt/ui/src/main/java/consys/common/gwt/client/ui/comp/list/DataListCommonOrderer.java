package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DataListCommonOrderer extends HeadPanel implements DataListCommonHeadPanel {

    // komponenty
    private FlowPanel orderers;
    private ListPagerWidget listPagerWidget;
    private SelectBox<DataListCommonOrdererItem> selectBox;
    // data
    private DataListCommonOrdererItem activeOrderer;
    private ArrayList<DataListCommonOrdererItem> allOrderers;

    public DataListCommonOrderer() {
        allOrderers = new ArrayList<DataListCommonOrdererItem>();

        selectBox = new SelectBox<DataListCommonOrdererItem>();
        selectBox.addStyleName(StyleUtils.FLOAT_LEFT);
        selectBox.setWidth(240);
        selectBox.selectFirst(true);
        selectBox.addChangeValueHandler(selectBoxHandler());

        FlowPanel flowPanel = new FlowPanel();
        flowPanel.setHeight("25px");
        initWidget(flowPanel);

        // Generate orderer
        Label title = new Label(UIMessageUtils.c.dataListCommon_text_sortBy() + ":");
        title.setStyleName(ResourceUtils.system().css().dataListCommonOrdererTitle());
        orderers = new FlowPanel();
        orderers.add(title);
        orderers.addStyleName(CssStyles.FLOAT_LEFT);
        flowPanel.add(orderers);

        // Generate Pager        
        listPagerWidget = new ListPagerWidget();
        SimplePanel sp = new SimplePanel();
        sp.setWidget(listPagerWidget);
        sp.addStyleName(CssStyles.FLOAT_RIGHT);

        flowPanel.add(sp);
    }

    @Override
    protected void onLoad() {
        ArrayList<SelectBoxItem<DataListCommonOrdererItem>> initItems = new ArrayList<SelectBoxItem<DataListCommonOrdererItem>>();

        for (DataListCommonOrdererItem item : allOrderers) {
            SelectBoxItem<DataListCommonOrdererItem> initItem =
                    new SelectBoxItem<DataListCommonOrdererItem>(item, item.getText());
            initItems.add(initItem);
        }

        selectBox.setItems(initItems);
        orderers.add(selectBox);
    }

    protected FlowPanel getPanel() {
        return (FlowPanel) super.getWidget();
    }

    @Override
    public ListPagerWidget getListPagerWidget() {
        return listPagerWidget;
    }

    @Override
    public void setParentList(DataListPanel parentList) {
        for (DataListCommonOrdererItem co : allOrderers) {
            co.setParentList(parentList);
        }
    }

    public void addOrderer(String title, int tag, boolean selected) {
        final DataListCommonOrdererItem co = new DataListCommonOrdererItem(tag, title, this);
        allOrderers.add(co);
        co.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeOrderer != null) {
                    activeOrderer.deselect();
                }
                activeOrderer = co;
                activeOrderer.select();
                activeOrderer.refresh();
            }
        });
        // Nastavime aktivny orderer
        if (selected) {
            if (activeOrderer != null) {
                activeOrderer.deselect();
            }
            activeOrderer = co;
            co.select();
            clearOrderer();
            registerOrderer(co);
        } else {
            co.deselect();
        }
    }

    public void addOrderer(String title, int tag) {
        addOrderer(title, tag, false);
    }

    private ChangeValueEvent.Handler<SelectBoxItem<DataListCommonOrdererItem>> selectBoxHandler() {
        return new ChangeValueEvent.Handler<SelectBoxItem<DataListCommonOrdererItem>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<DataListCommonOrdererItem>> event) {
                if (activeOrderer != null) {
                    activeOrderer.deselect();
                }
                DataListCommonOrdererItem value = event.getValue().getItem();
                activeOrderer = value;
                activeOrderer.select();
                activeOrderer.refresh();
                activeOrderer.select();
            }
        };
    }
}
