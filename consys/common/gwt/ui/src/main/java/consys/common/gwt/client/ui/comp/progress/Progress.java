package consys.common.gwt.client.ui.comp.progress;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Zobrazuje položky progressu
 * @author pepa
 */
public class Progress extends Composite {

    private HorizontalPanel panel;

    public Progress() {
        panel = new HorizontalPanel();
        panel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        initWidget(panel);
    }

    /** zobrazuje položky progressu při registraci / aktivaci a přihlášení */
    public static Progress register(int activeIndex) {
        Progress p = new Progress();

        p.addProgressItem(new ProgressItem(activeIndex == 1, 1, UIMessageUtils.c.progress_text1()));
        p.addSeparator();
        p.addProgressItem(new ProgressItem(activeIndex == 2, 2, UIMessageUtils.c.progress_text2()));
        p.addSeparator();
        p.addProgressItem(new ProgressItem(activeIndex == 3, 3, UIMessageUtils.c.progress_text3()));

        return p;
    }

    /** prida jednu polozku progressu */
    public void addProgressItem(ProgressItem pi) {
        panel.add(pi);
    }

    /** prida separator za posledne vlozenou polozku */
    public void addSeparator() {
        Label label = new Label();
        label.setSize("15px", "5px");
        label.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        panel.add(label);
    }
}
