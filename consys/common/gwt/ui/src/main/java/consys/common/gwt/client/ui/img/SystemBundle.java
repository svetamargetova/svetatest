package consys.common.gwt.client.ui.img;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.CssResource.Import;
import com.google.gwt.resources.client.DataResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

/**
 *
 * @author pepa
 */
public interface SystemBundle extends ClientBundle {

    public static final SystemBundle INSTANCE = GWT.create(SystemBundle.class);

    /** Ajax Loader Small */
    @Source("ajaxLoaderSmall.gif")
    public ImageResource ajaxLoaderSmall();

    /** PROFILOVE FOTO */
    /** defaultni portret maly */
    @Source("portrait/default_user_small.png")
    public ImageResource portraitSmall();

    /** defaultni portret normalni */
    @Source("portrait/default_user_middle.png")
    public ImageResource portraitMiddle();

    /** defaultni portret normalni */
    @Source("portrait/default_user_big.png")
    public ImageResource portraitBig();

    /** EVENT LOGA */
    /** vychozi logo konference, male velikost */
    @Source("logo/default_event_logo_small.png")
    public ImageResource confLogoSmall();

    /** vychozi logo konference, male velikost */
    @Source("logo/default_event_logo_middle.png")
    public ImageResource confLogoMedium();

    /** vychozi logo konference, velke velikost */
    @Source("logo/default_event_logo_big.png")
    public ImageResource confLogoBig();

    /** EVENT PROFILE LOGA */
    /** vychozi profilove logo konference, male velikost */
    @Source("logo/default_event_profile_small.png")
    public ImageResource confProfileLogoSmall();

    /** vychozi profilove logo konference, stredni velikost */
    @Source("logo/default_event_profile_middle.png")
    public ImageResource confProfileLogoMedium();

    /** vychozi profilove logo konference, velke velikost */
    @Source("logo/default_event_profile_big.png")
    public ImageResource confProfileLogoBig();

    /** spodni levy rozek zakladniho wrapperu */
    @Source("wrapper/corGrayBl.gif")
    public ImageResource corGrayBl();

    /** spodni pravy rozek zakladniho wrapperu */
    @Source("wrapper/corGrayBr.gif")
    public ImageResource corGrayBr();

    /** horni pravy rozek zakladniho wrapperu */
    @Source("wrapper/corGrayTr.gif")
    public ImageResource corGrayTr();

    /** horni levy rozek zakladniho wrapperu */
    @Source("wrapper/corGrayTl.gif")
    public ImageResource corGrayTl();

    /** spodni levy rozek zakladniho wrapperu se spodnim stinem */
    @Source("wrapper/corGraySBl.gif")
    public ImageResource corGraySBl();

    /** spodni pravy rozek zakladniho wrapperu se spodnim stinem */
    @Source("wrapper/corGraySBr.gif")
    public ImageResource corGraySBr();

    /** horni pravy rozek zakladniho wrapperu se spodnim stinem */
    @Source("wrapper/corGraySTr.gif")
    public ImageResource corGraySTr();

    /** horni levy rozek zakladniho wrapperu se spodnim stinem */
    @Source("wrapper/corGraySTl.gif")
    public ImageResource corGraySTl();

    /** levy okraj horniho menu */
    @Source("topmenu/topMl.gif")
    public ImageResource topMenuLeft();

    /** pozadi stredu horniho menu */
    @Source("topmenu/topMc.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource topMenuCenter();

    /** pravy okraj horniho menu */
    @Source("topmenu/topMr.gif")
    public ImageResource topMenuRight();

    /** levy okraj vybraneho horniho menu */
    @Source("topmenu/topMbl.gif")
    public ImageResource topMenuSelectLeft();

    /** stred vybraneho horniho menu */
    @Source("topmenu/topMbc.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource topMenuSelectCenter();

    /** pravy okraj vybraneho horniho menu */
    @Source("topmenu/topMbr.gif")
    public ImageResource topMenuSelectRight();

    /** dolni cast separatoru nad spodnim menu */
    @Source("bottomMs2.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource bottomMenuSeparator2();

    /** teckovany separator */
    @Source("panelS2.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource panelSeparator2();

    /** pozadi ActionImage */
    @Source("button/aiBack.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBg();

    /** levy i pravy okraj ActionImage */
    @Source("button/aiBorder.gif")
    public ImageResource actionImageBorder();

    /** pozadi ActionImage */
    @Source("button/daiBack.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBgDisabled();

    /** levy i pravy okraj ActionImage */
    @Source("button/daiBorder.gif")
    public ImageResource actionImageBorderDisabled();

    /** pozadi ActionImageBig */
    @Source("button/aiBackB.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBigBg();

    /** levy okraj ActionImage */
    @Source("button/aiBorderL.gif")
    public ImageResource actionImageBigLeft();

    /** pravy okraj ActionImageBig */
    @Source("button/aiBorderR.gif")
    public ImageResource actionImageBigRight();

    /** pozadi ActionImageBig */
    @Source("button/daiBackB.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBigBgDisabled();

    /** levy okraj ActionImage */
    @Source("button/daiBorderL.gif")
    public ImageResource actionImageBigLeftDisabled();

    /** pravy okraj ActionImageBig */
    @Source("button/daiBorderR.gif")
    public ImageResource actionImageBigRightDisabled();

    /** back ikona pro ActionImage */
    @Source("button/aiIBack.gif")
    public ImageResource actionImageBack();

    /** confirm ikona pro ActionImage */
    @Source("button/aiIConfirm.gif")
    public ImageResource actionImageConfirm();

    /** cross ikona pro ActionImage */
    @Source("button/aiICross.gif")
    public ImageResource actionImageCross();

    /** check ikona pro ActionImage */
    @Source("button/aiICheck.gif")
    public ImageResource actionImageCheck();

    /** person ikona pro ActionImage */
    @Source("button/aiIPerson.gif")
    public ImageResource actionImagePerson();

    /** plus ikona pro ActionImage */
    @Source("button/aiIPlus.gif")
    public ImageResource actionImagePlus();

    /** obalka ikona pro ActionImage */
    @Source("button/aiIMail.gif")
    public ImageResource actionImageMail();

    /** obalka ikona pro ActionImage */
    @Source("button/aiDownArrow.gif")
    public ImageResource actionImageDownArrow();

    /** check box on */
    @Source("checkbox/checkOn.gif")
    public ImageResource checkOn();

    /** check box off */
    @Source("checkbox/checkOff.gif")
    public ImageResource checkOff();

    /** check box on disablovany */
    @Source("checkbox/checkOnDis.gif")
    public ImageResource checkOnDis();

    /** check box off disablovany */
    @Source("checkbox/checkOffDis.gif")
    public ImageResource checkOffDis();

    /** ikona chyby v ConsysMessage */
    @Source("mIfail.gif")
    public ImageResource messageFailIcon();

    /** ikona uspechu v ConsysMessage */
    @Source("mISucc.gif")
    public ImageResource messageSuccessIcon();

    /** ikona info v ConsysMessage */
    @Source("mIinfo.gif")
    public ImageResource messageInfoIcon();

    /** horni pravy rozek cerveneho wrapperu */
    @Source("wrapper/corRedTr.gif")
    public ImageResource corRedTr();

    /** horni levy rozek cerveneho wrapperu */
    @Source("wrapper/corRedTl.gif")
    public ImageResource corRedTl();

    /** dolni pravy rozek cerveneho wrapperu */
    @Source("wrapper/corRedBr.gif")
    public ImageResource corRedBr();

    /** dolni levy rozek cerveneho wrapperu */
    @Source("wrapper/corRedBl.gif")
    public ImageResource corRedBl();

    /** horni pravy rozek modreho wrapperu */
    @Source("wrapper/corBlueTr.gif")
    public ImageResource corBlueTr();

    /** horni levy rozek modreho wrapperu */
    @Source("wrapper/corBlueTl.gif")
    public ImageResource corBlueTl();

    /** dolni pravy rozek modreho wrapperu */
    @Source("wrapper/corBlueBr.gif")
    public ImageResource corBlueBr();

    /** dolni levy rozek modreho wrapperu */
    @Source("wrapper/corBlueBl.gif")
    public ImageResource corBlueBl();

    /** horni pravy rozek zeleneho wrapperu */
    @Source("wrapper/corGreenTr.gif")
    public ImageResource corGreenTr();

    /** horni levy rozek zeleneho wrapperu */
    @Source("wrapper/corGreenTl.gif")
    public ImageResource corGreenTl();

    /** dolni pravy rozek zeleneho wrapperu */
    @Source("wrapper/corGreenBr.gif")
    public ImageResource corGreenBr();

    /** dolni levy rozek zeleneho wrapperu */
    @Source("wrapper/corGreenBl.gif")
    public ImageResource corGreenBl();

    /** pozadi stinovaneho svetle zeleneho wrapperu spodni hrana */
    @Source("wrapper/corGraySBott.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource corGrayBottomBg();

    /** cislo 1 v zobrazovani progress On */
    @Source("progress/progNo1n.gif")
    public ImageResource progNo1n();

    /** cislo 1 v zobrazovani progress Off */
    @Source("progress/progNo1f.gif")
    public ImageResource progNo1f();

    /** cislo 2 v zobrazovani progress On */
    @Source("progress/progNo2n.gif")
    public ImageResource progNo2n();

    /** cislo 2 v zobrazovani progress Off */
    @Source("progress/progNo2f.gif")
    public ImageResource progNo2f();

    /** cislo 3 v zobrazovani progress On */
    @Source("progress/progNo3n.gif")
    public ImageResource progNo3n();

    /** cislo 3 v zobrazovani progress Off */
    @Source("progress/progNo3f.gif")
    public ImageResource progNo3f();

    /** progress aktivni pozadi */
    @Source("progress/progActBg.gif")
    public ImageResource progActBg();

    /** progress neaktivni pozadi */
    @Source("progress/progInactBg.gif")
    public ImageResource progInactBg();

    /** nevybrana polozka submenu - pozadi stredu */
    @Source("usic.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource subMenuUSBg();

    /** nevybrana polozka submenu - levy okraj */
    @Source("usil.gif")
    public ImageResource subMenuBorderLeft();

    /** nevybrana polozka submenu - pravy okraj */
    @Source("usir.gif")
    public ImageResource subMenuBorderRight();

    /** vybrana polozka submenu - pozadi stredu */
    @Source("sic.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource subMenuSBg();

    /** vybrana polozka submenu - levy okraj */
    @Source("sil.gif")
    public ImageResource subMenuBorderSLeft();

    /** vybrana polozka submenu - pravy okraj */
    @Source("sir.gif")
    public ImageResource subMenuBorderSRight();

    /** sipka pro dolu, pouzivat napr. pro minimalizaci */
    @Source("sizeDown.gif")
    public ImageResource sizeArrowDown();

    /** sipka nahoru posuvna ve wrapperu */
    @Source("arrowUp.gif")
    public ImageResource arrowUp();

    /** sipka dolu posuvna ve wrapperu */
    @Source("arrowDown.gif")
    public ImageResource arrowDown();

    /** separator mezi posuvnymi sipkami ve wrapperu */
    @Source("arrowSep.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource arrowSep();

    /** kontextova ikona person nevybrana */
    @Source("context/ctxLeft.gif")
    public ImageResource conLeft();

    /** kontextova ikona person nevybrana */
    @Source("context/ctxRight.gif")
    public ImageResource conRight();

    /** kontextova ikona person nevybrana */
    @Source("context/personOut.gif")
    public ImageResource conPersonOut();

    /** kontextova ikona person vybrana */
    @Source("context/personOver.gif")
    public ImageResource conPersonOver();

    /** kontextova ikona krizek nevybrana */
    @Source("context/crossOut.gif")
    public ImageResource conCrossOut();

    /** kontextova ikona krizek vybrana */
    @Source("context/crossOver.gif")
    public ImageResource conCrossOver();

    /** kontextova ikona ozubene kolo nevybrana */
    @Source("context/gearOut.gif")
    public ImageResource conGearOut();

    /** kontextova ikona ozubene kolo vybrana */
    @Source("context/gearOver.gif")
    public ImageResource conGearOver();

    /** trojuhelnik smerujici ze zdroje napovedy */
    @Source("hpTri.gif")
    public ImageResource hpTriagle();

    /** breadcrumb levy okraj */
    @Source("topmenu/breadL.gif")
    public ImageResource breadLeft();

    /** breadcrumb pozadi stredu */
    @Source("topmenu/breadC.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource breadCenter();

    /** breadcrumb pravy okraj */
    @Source("topmenu/breadR.gif")
    public ImageResource breadRight();

    /** breadcrumb separator */
    @Source("topmenu/breadS.gif")
    public ImageResource breadSeparator();

    /** tlacitko plus pro rozbaleni */
    @Source("rollPlus.png")
    public ImageResource rollPlus();

    /** tlacitko minus pro zabaleni */
    @Source("rollMinus.png")
    public ImageResource rollMinus();

    /** zalozkovy panel vybrana polozka levy okraj */
    @Source("wrapper/tabSL.gif")
    public ImageResource tabSLeft();

    /** zalozkovy panel vybrana polozka stred podklad */
    @Source("wrapper/tabSC.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource tabSCenter();

    /** zalozkovy panel vybrana polozka pravy okraj */
    @Source("wrapper/tabSR.gif")
    public ImageResource tabSRight();

    /** zalozkovy panel nevybrana polozka levy okraj */
    @Source("wrapper/tabUL.gif")
    public ImageResource tabULeft();

    /** zalozkovy panel nevybrana polozka stred podklad */
    @Source("wrapper/tabUC.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource tabUCenter();

    /** zalozkovy panel nevybrana polozka pravy okraj */
    @Source("wrapper/tabUR.gif")
    public ImageResource tabURight();

    @Source("tabL.png")
    public ImageResource tabLeft();

    @Source("tabR.png")
    public ImageResource tabRight();

    /** obrazek rusiciho tlacitka */
    @Source("removeCross.gif")
    public ImageResource removeCross();

    /** pozadi vyhledavaciho vstupu v top menu */
    @Source("searchMl.gif")
    public ImageResource menuSearchLeft();

    /** odesilaci tlacitko vyhledavani v top menu */
    @Source("searchMr.gif")
    public ImageResource menuSearchRight();

    /** novy pravy panel - levy horni rozek */
    @Source("panel/lt.gif")
    public ImageResource panelLT();

    /** novy pravy panel - stred horni */
    @Source("panel/ct.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource panelCT();

    /** novy pravy panel - pravy horni rozek */
    @Source("panel/rt.gif")
    public ImageResource panelRT();

    /** novy pravy panel - levy horni rozek */
    @Source("panel/alt.gif")
    public ImageResource panelALT();

    /** novy pravy panel - pravy horni rozek */
    @Source("panel/art.gif")
    public ImageResource panelART();

    /** novy pravy panel - levy dolni rozek */
    @Source("panel/lb.gif")
    public ImageResource panelLB();

    /** novy pravy panel - stred dolni */
    @Source("panel/cb.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource panelCB();

    /** novy pravy panel - pravy dolni rozek */
    @Source("panel/rb.gif")
    public ImageResource panelRB();

    /** novy pravy panel - separator */
    @Source("panel/sep.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource panelSep();

    /** novy pravy panel - prvni polozka panelu - prechod */
    @Source("panel/fst.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource panelFst();

    /** novy pravy panel - dalsi polozka panelu - prechod */
    @Source("panel/lst.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource panelNxt();

    /** novy pravy panel - rozklikavaci sipecka na panelu */
    @Source("panel/down.gif")
    public ImageResource panelDown();

    /** Pager pro list Previous */
    @Source("pagePrev.png")
    public ImageResource listPagerPrevious();

    /** Pager pro list Previous Disabled */
    @Source("pagePrevDisabled.png")
    public ImageResource listPagerPreviousDisabled();

    /** Pager pro list Next */
    @Source("pageNext.png")
    public ImageResource listPagerNext();

    /** Pager pro list Next */
    @Source("pageNextDisabled.png")
    public ImageResource listPagerNextDisabled();

    /** krizek pro zavreni zpravy (najety) */
    @Source("msgCloseOn.gif")
    public ImageResource msgCloseOn();

    /** krizek pro zavreni zpravy (nenajety) */
    @Source("msgCloseOff.gif")
    public ImageResource msgCloseOff();

    /** reverser leve pozadi vypnute */
    @Source("reverser/leftOff.png")
    public ImageResource reverserLeftOff();

    /** reverser leve pozadi zapnute */
    @Source("reverser/leftOn.png")
    public ImageResource reverserLeftOn();

    /** reverser stredni pozadi vypnute */
    @Source("reverser/centOff.png")
    public ImageResource reverserCenterOff();

    /** reverser stredni pozadi zapnute */
    @Source("reverser/centOn.png")
    public ImageResource reverserCenterOn();

    /** reverser prave pozadi vypnute */
    @Source("reverser/rightOff.png")
    public ImageResource reverserRightOff();

    /** reverser prave pozadi vypnute */
    @Source("reverser/rightOn.png")
    public ImageResource reverserRightOn();

    /** reverser pozadi vypnute */
    @Source("reverser/off.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource reverserOff();

    /** reverser pozadi zapnute */
    @Source("reverser/on.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource reverserOn();

    /** velke tlacitko */
    @Source("button/accC.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource bigGreenB();

    /** velke tlacitko */
    @Source("button/accL.gif")
    public ImageResource bigGreenBL();

    /** velke tlacitko */
    @Source("button/accR.gif")
    public ImageResource bigGreenBR();

    /** velke tlacitko */
    @Source("button/decC.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource bigRedB();

    /** velke tlacitko */
    @Source("button/decL.gif")
    public ImageResource bigRedBL();

    /** velke tlacitko */
    @Source("button/decR.gif")
    public ImageResource bigRedBR();

    /** velke tlacitko */
    @Source("button/bluC.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource bigBlueB();

    /** velke tlacitko */
    @Source("button/bluL.gif")
    public ImageResource bigBlueBL();

    /** velke tlacitko */
    @Source("button/bluR.gif")
    public ImageResource bigBlueBR();

    /** velke tlacitko */
    @Source("button/disC.gif")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource bigDisabledB();

    /** velke tlacitko */
    @Source("button/disL.gif")
    public ImageResource bigDisabledBL();

    /** velke tlacitko */
    @Source("button/disR.gif")
    public ImageResource bigDisabledBR();

    /** obrazek komponenty ShowLoad */
    @Source("loaded.gif")
    public ImageResource showLoadLoaded();

    /** obrazek komponenty ShowLoad */
    @Source("unloaded.gif")
    public ImageResource showLoadUnloaded();

    /** obrazek komponenty ShowLoad */
    @Source("halfLoaded.gif")
    public ImageResource showLoadHalfLoaded();

    /** rozbalovaci / zabalovaci tlacitko velke */
    @Source("unpacking/unpackingBig.gif")
    public ImageResource unpackingBigButton();

    @Source("unpacking/unpackingDown.gif")
    public ImageResource unpackingBigButtonDown();

    @Source("unpacking/unpackingDownOn.gif")
    public ImageResource unpackingBigButtonDownOn();

    @Source("unpacking/unpackingUp.gif")
    public ImageResource unpackingBigButtonUp();

    @Source("unpacking/unpackingUpOn.gif")
    public ImageResource unpackingBigButtonUpOn();

    @Source("blackGear.png")
    public ImageResource blackGear();

    // vlajky statu
    @Source("flag/cz.gif")
    public ImageResource flagCzech();

    @Source("flag/usgb.gif")
    public ImageResource flagUSGreatBritain();

    // obecne ikony
    @Source("u/listDetail.gif")
    public ImageResource commonListDetail();

    @Source("u/pdf.gif")
    public ImageResource commonPdf();

    @Source("u/xls.gif")
    public ImageResource commonXls();

    @Source("u/zip.gif")
    public ImageResource commonZip();

    // stav transferu
    @Source("transfer/blue.gif")
    public ImageResource transferBlue();

    @Source("transfer/green.gif")
    public ImageResource transferGreen();

    @Source("transfer/orange.gif")
    public ImageResource transferOrange();

    @Source("transfer/red.gif")
    public ImageResource transferRed();

    @Source("bullet.png")
    public ImageResource textBullet();

    @Source("orderUp.png")
    public ImageResource orderUp();

    @Source("orderDown.png")
    public ImageResource orderDown();

    @Source("changeOrder.png")
    public ImageResource changeOrder();

    // ConsysRichTextToolbar
    @Source("toolbar/bold.gif")
    public ImageResource richTextBold();

    @Source("toolbar/italic.gif")
    public ImageResource richTextItalic();

    @Source("toolbar/underline.gif")
    public ImageResource richTextUnderline();

    @Source("toolbar/strikeThrough.gif")
    public ImageResource richTextStrikeThrough();

    @Source("toolbar/createLink.gif")
    public ImageResource richTextCreateLink();

    @Source("toolbar/removeLink.gif")
    public ImageResource richTextRemoveLink();

    @Source("toolbar/removeFormat.gif")
    public ImageResource richTextRemoveFormat();

    @Source("toolbar/list.gif")
    public ImageResource richTextList();

    // Data list export image
    @Source("list/exportButton.png")
    public ImageResource dataListExportImage();

    // Help popup
    @Source("help/titleFull.png")
    public ImageResource helpTitleFull();

    @Source("help/contentFullWidth.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource helpContentFull();

    @Source("help/bottomFull.png")
    public ImageResource helpBottomFull();

    @Source("help/triangleLeft.png")
    public ImageResource helpTriangleLeft();

    @Source("help/triangleRight.png")
    public ImageResource helpTriangleRight();

    @Source("help/triangleBottom.png")
    public ImageResource helpTriangleBottom();

    @Source("dialogClose.png")
    public ImageResource dialogClose();

    @Source("poweredByTakeplace.png")
    public ImageResource poweredByTakeplace();

    /** css styly */
    @Source("CssConstants.css")
    public CssConstants constants();

    @Import(value = {CssConstants.class})
    @Source("ui.css")
    public SystemCssResource css();

    @Source("richTextArea.css")
    @DataResource.MimeType(value = "text/css")
    public DataResource richTextAreaCSS();
}
