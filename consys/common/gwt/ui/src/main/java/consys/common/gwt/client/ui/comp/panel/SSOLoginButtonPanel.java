package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.*;
import consys.common.constants.sso.SSO;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.widget.SSOLogin;
import java.util.HashMap;
import java.util.Map;

/**
 * Komponentka s tlacitky (jen znaky) sso
 * @author pepa
 */
public class SSOLoginButtonPanel extends Composite {

    // komponenty
    private FlowPanel mainPanel;
    // data
    private static Map<SSO, ConsysAction> actionsMap = new HashMap<SSO, ConsysAction>();

    public SSOLoginButtonPanel() {
        PanelResources.INSTANCE.css().ensureInjected();

        mainPanel = new FlowPanel();
        mainPanel.setStyleName(PanelResources.INSTANCE.css().ssoButtons());

        initWidget(mainPanel);
    }

    @Override
    protected void onLoad() {
        mainPanel.clear();
        generate();
    }

    private void generate() {
        mainPanel.add(new Label(UIMessageUtils.c.loginPanel_text_signUpWith() + ":"));

        FlowPanel buttons = new FlowPanel();
        buttons.setStyleName(PanelResources.INSTANCE.css().ssoButtonsWrapper());
        buttons.add(button(SSO.FACEBOOK));
        buttons.add(button(SSO.TWITTER));
        buttons.add(StyleUtils.clearDiv("5px"));
        buttons.add(button(SSO.LINKEDIN));
        buttons.add(button(SSO.GOOGLE_PLUS));
        buttons.add(StyleUtils.clearDiv("5px"));
        mainPanel.add(buttons);

        exportSSOLogin();
    }

    private Widget button(SSO type) {
        actionsMap.put(type, buttonAction(type));

        String className = "";
        switch (type) {
            case FACEBOOK:
                className = PanelResources.INSTANCE.css().facebook();
                break;
            case GOOGLE_PLUS:
                className = PanelResources.INSTANCE.css().google_plus();
                break;
            case LINKEDIN:
                className = PanelResources.INSTANCE.css().linkedin();
                break;
            case TWITTER:
                className = PanelResources.INSTANCE.css().twitter();
                break;
        }

        AnchorElement anchor = Document.get().createAnchorElement();
        anchor.setClassName(className);
        anchor.setHref("javascript:ssoLogin" + type.name() + "();");

        SimplePanel panel = new SimplePanel();
        panel.setStyleName(PanelResources.INSTANCE.css().button());
        panel.getElement().appendChild(anchor);
        return panel;
    }

    private ConsysAction buttonAction(final SSO type) {
        return SSOLogin.ssoLogin(type);
    }

    /** vyexportuje funkce aby byly volatelne z javascriptu */
    private static native void exportSSOLogin() /*-{
     $wnd.ssoLoginFACEBOOK = $entry(@consys.common.gwt.client.ui.comp.panel.SSOLoginButtonPanel::runSSOLoginFacebook());
     $wnd.ssoLoginGOOGLE_PLUS = $entry(@consys.common.gwt.client.ui.comp.panel.SSOLoginButtonPanel::runSSOLoginGooglePlus());
     $wnd.ssoLoginLINKEDIN = $entry(@consys.common.gwt.client.ui.comp.panel.SSOLoginButtonPanel::runSSOLoginLinkedIn());
     $wnd.ssoLoginTWITTER = $entry(@consys.common.gwt.client.ui.comp.panel.SSOLoginButtonPanel::runSSOLoginTwitter());
     }-*/;

    public static void runSSOLoginFacebook() {
        actionsMap.get(SSO.FACEBOOK).run();
    }

    public static void runSSOLoginGooglePlus() {
        actionsMap.get(SSO.GOOGLE_PLUS).run();
    }

    public static void runSSOLoginLinkedIn() {
        actionsMap.get(SSO.LINKEDIN).run();
    }

    public static void runSSOLoginTwitter() {
        actionsMap.get(SSO.TWITTER).run();
    }
}
