package consys.common.gwt.client.ui.comp.context;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.PopupHelp;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Polozka kontextoveho menu
 * @author pepa
 */
public class ContextMenuItem extends Composite {

    // komponenty
    private SimplePanel panel;
    private PopupHelp help;
    // data
    private String overStyle;
    private String outStyle;

    public ContextMenuItem(String iconStyle) {
        this(iconStyle, iconStyle);
    }

    public ContextMenuItem(String overStyle, String outStyle) {
        panel = new SimplePanel();
        panel.setStyleName(StyleUtils.HAND);
        panel.addStyleName(StyleUtils.MARGIN_HOR_3);
        panel.addStyleName(outStyle);

        if (!overStyle.equals(outStyle)) {
            this.overStyle = overStyle;
            this.outStyle = outStyle;
        }

        sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT | Event.ONCLICK);
        initHandlers();
        initWidget(panel);
    }

    /** pokud se maji prepinat ikony, zinicializuje handlery */
    private void initHandlers() {
        addHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                if (overStyle != null) {
                    panel.addStyleName(overStyle);
                    panel.removeStyleName(outStyle);
                }
                if (help != null) {
                    help.show();
                }
            }
        }, MouseOverEvent.getType());
        addHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                if (overStyle != null) {
                    panel.addStyleName(outStyle);
                    panel.removeStyleName(overStyle);
                }
                if (help != null) {
                    help.hide();
                }
            }
        }, MouseOutEvent.getType());
    }

    /** pridani ClickHandleru */
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return addHandler(handler, ClickEvent.getType());
    }

    public void setHelpText(String text) {
        help = new PopupHelp(text, this);
    }

    /** vraci ContextMenuItem Person */
    public static ContextMenuItem itemPerson() {
        return new ContextMenuItem(
                ResourceUtils.system().css().contextPersonOver(),
                ResourceUtils.system().css().contextPersonOut());
    }

    /** vraci ContextMenuItem Cross */
    public static ContextMenuItem itemCross() {
        return new ContextMenuItem(
                ResourceUtils.system().css().contextCrossOver(),
                ResourceUtils.system().css().contextCrossOut());
    }

    /** vraci ContextMenuItem Gear */
    public static ContextMenuItem itemGear() {
        return new ContextMenuItem(
                ResourceUtils.system().css().contextGearOver(),
                ResourceUtils.system().css().contextGearOut());
    }
}
