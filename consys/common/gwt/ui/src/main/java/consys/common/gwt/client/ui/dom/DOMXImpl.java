package consys.common.gwt.client.ui.dom;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

/**
 * Obecna implementace DOMX
 * @author pepa
 */
public abstract class DOMXImpl {

    public void setOpacity(Element e, double opacity) {
        DOM.setStyleAttribute(e, "opacity", Double.toString(opacity));
    }

    public int getOffsetHeightDiff() {
        return 0;
    }
}
