package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.panel.SwapPanel;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBaseWrapper;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Komponenta pro odstranovani
 * @author pepa
 */
public class Remover extends Composite {

    // komponenty
    private SwapPanel panel;
    private ConsysAction action;
    private Widget question;

    /** @param w podporuje ActionLabel a Image */
    public Remover(Widget w, ConsysAction action) {
        this(w, null, action);
    }

    /**
     * @param w obalovy objekt zobrazeny pred prepnutim do dotazu jestli smazat ano/ne
     * @param i podporuje ActionLabel a Image
     */
    public Remover(Widget w, Widget i, ConsysAction action) {
        this.action = action;
        question = questionPanel();
        panel = new SwapPanel(w, question);

        if (i == null) {
            i = w;
        }

        if (i instanceof ActionLabel) {
            // action label
            ((ActionLabel) i).setClickHandler(swapClickHandler());
        } else if (i instanceof Image) {
            // image
            ((Image) i).addClickHandler(swapClickHandler());
        }
        initWidget(panel);
    }

    /** vytvori panel s otazkou pro smazani */
    private HorizontalPanel questionPanel() {
        final HorizontalPanel hp = new HorizontalPanel();
        hp.addStyleName(StyleUtils.MARGIN_HOR_5);

        ActionLabel yes = new ActionLabel(UIMessageUtils.c.const_yes(),
                StyleUtils.Assemble(StyleUtils.FONT_10PX, StyleUtils.MARGIN_LEFT_10));
        ActionLabel no = new ActionLabel(UIMessageUtils.c.const_no(),
                StyleUtils.Assemble(StyleUtils.FONT_10PX, StyleUtils.MARGIN_LEFT_10));

        hp.add(StyleUtils.getStyledLabel(UIMessageUtils.c.remover_text_areYouSure(), StyleUtils.FONT_10PX, StyleUtils.FONT_BOLD));
        hp.add(yes);
        hp.add(no);

        ConsysBaseWrapper wp = new ConsysBaseWrapper(hp);
        wp.addStyleName(StyleUtils.MARGIN_VER_3);

        yes.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                action.run();
            }
        });
        no.setClickHandler(swapClickHandler());

        return hp;
    }

    /** ClickHandler pro swapnuti */
    private ClickHandler swapClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                panel.swap();
                if (panel.isShowDefaultWidget()) {
                    onDefaultSwap();
                } else {
                    onQuestionSwap();
                }
            }
        };
    }

    /** prida styl widgetu s dotazem */
    public void addQuestionStyleName(String style) {
        question.addStyleName(style);
    }

    /** vola se pri prepnuti na otazku (urceno pro pretizeni) */
    protected void onQuestionSwap() {
    }

    /** vola se pri prepnuti na normalni zobrazeni (urceno pro pretizeni) */
    protected void onDefaultSwap() {
    }

    /** tovarni metoda pro ziskani klasickeho removeru */
    public static Remover commonRemover(ConsysAction removeAction) {
        Image remove = new Image(ResourceUtils.system().removeCross());
        remove.setStyleName(StyleUtils.HAND);
        return new Remover(remove, removeAction);
    }
}
