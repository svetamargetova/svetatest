package consys.common.gwt.client.ui.payment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.AddressForm;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.Dialog;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import java.util.ArrayList;

/**
 * Panel pro zobrazeni a editaci platebniho profilu, urceno hlavne pro platebni dialog (sirka formu)
 * @author pepa
 */
public class PaymentProfilePanel extends SimpleFormPanel {

    // komponenty
    private FlowPanel panel;
    private BaseForm form;
    private ConsysStringTextBox nameBox;
    private ConsysStringTextBox cityBox;
    private ConsysStringTextBox streetBox;
    private ConsysStringTextBox zipBox;
    private SelectBox<Integer> countryBox;
    private ConsysStringTextBox regNoBox;
    private ConsysStringTextBox vatNoBox;
    private ConsysStringTextBox accountNoBox;
    private ConsysStringTextBox bankNoBox;
    // data
    private ActionExecutionDelegate parent;
    private ClientUserPaymentProfile profile;
    private boolean edited;
    private boolean showBankInfo;
    private boolean requiredBankInfo;

    public PaymentProfilePanel(ActionExecutionDelegate parent, boolean showBankInfo) {
        super((Dialog.DEFAULT_WIDTH - 10) + "px");
        this.showBankInfo = showBankInfo;
        this.parent = parent;

        requiredBankInfo = false;

        panel = new FlowPanel();
        DOM.setStyleAttribute(panel.getElement(), "minHeight", showBankInfo ? "350px" : "180px");
    }

    /** nastavi profil */
    public void setProfile(ClientUserPaymentProfile profile) {
        this.profile = profile;
        edited = false;

        // validace jestli nechybi nektere povinne udaje
        if (profile == null || StringUtils.isEmpty(profile.getName()) || StringUtils.isEmpty(profile.getCity())
                || StringUtils.isEmpty(profile.getStreet()) || StringUtils.isEmpty(profile.getZip())
                || profile.getCountry() == null || profile.getCountry() == 0) {
            setWidget(editMode());
        } else {
            setWidget(readMode());
        }
    }

    /** vraci true pokud byl formular prepnuty do editacniho rezimu */
    public boolean isEdited() {
        return edited;
    }

    public boolean isRequiredBankInfo() {
        return requiredBankInfo;
    }

    public void setRequiredBankInfo(boolean requiredBankInfo) {
        this.requiredBankInfo = requiredBankInfo;
    }

    /** editacni mod zobrazeni */
    public Widget readMode() {
        final Label countryLabel = new Label();

        EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                boolean found = false;
                for (BoxItem e : result.getList()) {
                    if (profile != null && profile.getCountry() != null) {
                        if (profile.getCountry().equals(e.getId())) {
                            countryLabel.setText(e.getName());
                            found = true;
                            break;
                        }
                    }
                }
                if (!found) {
                    countryLabel.setText(FormUtils.NDASH);
                }
            }
        }, parent));


        form = new BaseForm("170px");
        form.addOptional(0, UIMessageUtils.c.paymentProfilePanel_text_invoicedTo(), new Label(FormUtils.valueOrDash(profile != null ? profile.getName() : "")));
        form.addOptional(1, UIMessageUtils.c.paymentProfilePanel_form_registrationNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getRegNo() : "")));
        form.addOptional(2, UIMessageUtils.c.paymentProfilePanel_form_vatNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getVatNo() : "")));
        form.addOptional(3, UIMessageUtils.c.const_address_street(), new Label(FormUtils.valueOrDash(profile != null ? profile.getStreet() : "")));
        form.addOptional(4, UIMessageUtils.c.const_address_city(), new Label(FormUtils.valueOrDash(profile != null ? profile.getCity() : "")));
        form.addOptional(5, UIMessageUtils.c.const_address_zip(), new Label(FormUtils.valueOrDash(profile != null ? profile.getZip() : "")));
        form.addOptional(6, UIMessageUtils.c.const_address_location(), countryLabel);
        if (showBankInfo) {
            form.addContent(7, createSeparator());
            form.addOptional(8, UIMessageUtils.c.paymentProfilePanel_form_accountNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getAccountNo() : "")));
            form.addOptional(9, UIMessageUtils.c.paymentProfilePanel_form_bankNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getBankNo() : "")));
        }

        ActionLabel edit = new ActionLabel(UIMessageUtils.c.const_edit(), StyleUtils.Assemble(MARGIN_TOP_3, FONT_10PX, FLOAT_LEFT));
        edit.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                setWidget(editMode());
            }
        });

        panel.clear();
        panel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.paymentProfilePanel_title(),
                FONT_16PX, FONT_BOLD, MARGIN_BOT_20, FLOAT_LEFT, MARGIN_RIGHT_20));
        panel.add(edit);
        panel.add(StyleUtils.clearDiv());
        panel.add(form);
        return panel;
    }

    /** editacni mod zobrazeni */
    public Widget editMode() {
        edited = true;
        panel.clear();
        panel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.paymentProfilePanel_title(), FONT_16PX, FONT_BOLD, MARGIN_BOT_20));


        nameBox = new ConsysStringTextBox(1, 150, UIMessageUtils.c.paymentProfilePanel_text_invoicedTo());
        cityBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.const_address_city());
        streetBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.const_address_street());
        zipBox = new ConsysStringTextBox(1, 64, UIMessageUtils.c.const_address_zip());
        countryBox = new SelectBox<Integer>();
        regNoBox = new ConsysStringTextBox(0, 128, UIMessageUtils.c.paymentProfilePanel_form_registrationNumber());
        vatNoBox = new ConsysStringTextBox(0, 128, UIMessageUtils.c.paymentProfilePanel_form_vatNumber());
        accountNoBox = new ConsysStringTextBox(requiredBankInfo ? 1 : 0, 128, UIMessageUtils.c.paymentProfilePanel_form_accountNumber()) {
            @Override
            public boolean doValidate(ConsysMessage fail) {
                boolean result = super.doValidate(fail);
                if (result) {
                    String value = getText().trim();
                    if (!value.isEmpty()) {
                        value = value.replaceAll("\\s+", "");
                        value = value.toLowerCase();
                        if (!value.matches("[a-z]{2}[0-9]{2}[a-z0-9]{1,30}")) {
                            fail.addOrSetText(UIMessageUtils.c.paymentProfilePanel_error_badIban());
                            return false;
                        }
                    }
                }
                return result;
            }
        };
        bankNoBox = new ConsysStringTextBox(requiredBankInfo ? 1 : 0, 128, UIMessageUtils.c.paymentProfilePanel_form_bankNumber());



        form = new BaseForm("170px");
        form.addRequired(0, UIMessageUtils.c.paymentProfilePanel_text_invoicedTo(), getNameBox());
        form.addOptional(1, UIMessageUtils.c.paymentProfilePanel_form_registrationNumber(), getRegNoBox());
        form.addOptional(2, UIMessageUtils.c.paymentProfilePanel_form_vatNumber(), getVatNoBox());
        form.addRequired(3, UIMessageUtils.c.const_address_street(), getStreetBox());
        form.addRequired(4, UIMessageUtils.c.const_address_city(), getCityBox());
        form.addRequired(5, UIMessageUtils.c.const_address_zip(), getZipBox());
        form.addRequired(6, UIMessageUtils.c.const_address_location(), getCountryBox());
        if (showBankInfo) {
            form.addContent(7, createSeparator());
            if (requiredBankInfo) {
                form.addRequired(8, UIMessageUtils.c.paymentProfilePanel_form_accountNumber(), createAccountNumberPanel());
                form.addRequired(9, UIMessageUtils.c.paymentProfilePanel_form_bankNumber(), createBankNoPanel());
            } else {
                form.addOptional(8, UIMessageUtils.c.paymentProfilePanel_form_accountNumber(), createAccountNumberPanel());
                form.addOptional(9, UIMessageUtils.c.paymentProfilePanel_form_bankNumber(), createBankNoPanel());
            }

            HTML html = new HTML(UIMessageUtils.c.paymentProfilePanel_text_supportedText());
            html.addStyleName(TEXT_GRAY);
            html.addStyleName(ResourceUtils.system().css().htmlAgreeing());
            html.addStyleName(MARGIN_TOP_10);
            html.addStyleName(FONT_11PX);
            html.setWidth("270px");
            form.addContent(10, html);
        }
        panel.add(form);


        if (profile != null) {

            if (profile != null && profile.getName() != null && !profile.getName().isEmpty()) {
                getNameBox().setText(profile.getName());
            }/* else {
             ClientUser cu = (ClientUser) Cache.get().get(UserCacheAction.USER);
             if (cu != null) {
             getNameBox().setText(cu.name());
             }
             }*/

            getCityBox().setText(profile.getCity());
            getStreetBox().setText(profile.getStreet());
            getZipBox().setText(profile.getZip());
            getRegNoBox().setText(profile.getRegNo());
            getVatNoBox().setText(profile.getVatNo());
            getAccountNoBox().setText(profile.getAccountNo());
            getBankNoBox().setText(profile.getBankNo());

            if (profile != null && profile.getCountry() != null) {
                getCountryBox().selectItem(profile.getCountry());
            } else {
                AddressForm.initCountryBox(countryBox);
            }

        }



        EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
                for (BoxItem e : result.getList()) {
                    out.add(new SelectBoxItem<Integer>(e.getId(), e.getName()));
                }
                getCountryBox().setItems(out);

            }
        }, parent));

        return panel;
    }

    private ConsysFlowPanel createBankNoPanel() {
        ConsysFlowPanel bankNoPanel = new ConsysFlowPanel();
        bankNoPanel.add(getBankNoBox());
        bankNoPanel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.paymentProfilePanel_text_supportedSwift(),
                FONT_11PX, TEXT_GRAY, WIDTH_270));
        return bankNoPanel;
    }

    private ConsysFlowPanel createAccountNumberPanel() {
        ConsysFlowPanel accountNoPanel = new ConsysFlowPanel();
        accountNoPanel.add(getAccountNoBox());
        accountNoPanel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.paymentProfilePanel_text_supportedIban(),
                FONT_11PX, TEXT_GRAY, WIDTH_270));
        return accountNoPanel;
    }

    private SimplePanel createSeparator() {
        SimplePanel sep = new SimplePanel();
        sep.setSize("30px", "20px");
        return sep;
    }

    /** vraci data z editacniho formu (pokud se do nej neprepne tak vraci puvodni) */
    public ClientUserPaymentProfile getClientPaymentProfile() {
        if (edited) {
            final ClientUserPaymentProfile cpp = new ClientUserPaymentProfile();
            cpp.setUuid(profile.getUuid());
            cpp.setAccountNo(getAccountNoBox().getText());
            cpp.setBankNo(getBankNoBox().getText());
            cpp.setCity(getCityBox().getText());
            cpp.setCountry(getCountryBox().getSelectedItem() != null ? getCountryBox().getSelectedItem().getItem() : 1);
            cpp.setName(getNameBox().getText());
            cpp.setRegNo(getRegNoBox().getText());
            cpp.setStreet(getStreetBox().getText());
            cpp.setVatNo(getVatNoBox().getText());
            cpp.setZip(getZipBox().getText());
            return cpp;
        } else {
            return profile;
        }
    }

    /** provede validaci editacniho formulare */
    public boolean doValidate(ConsysMessage fail) {
        return form.validate(fail);
    }

    /**
     * @return the nameBox
     */
    public ConsysStringTextBox getNameBox() {
        return nameBox;
    }

    /**
     * @return the cityBox
     */
    public ConsysStringTextBox getCityBox() {
        return cityBox;
    }

    /**
     * @return the streetBox
     */
    public ConsysStringTextBox getStreetBox() {
        return streetBox;
    }

    /**
     * @return the zipBox
     */
    public ConsysStringTextBox getZipBox() {
        return zipBox;
    }

    /**
     * @return the countryBox
     */
    public SelectBox<Integer> getCountryBox() {
        return countryBox;
    }

    /**
     * @return the regNoBox
     */
    public ConsysStringTextBox getRegNoBox() {
        return regNoBox;
    }

    /**
     * @return the vatNoBox
     */
    public ConsysStringTextBox getVatNoBox() {
        return vatNoBox;
    }

    /**
     * @return the accountNoBox
     */
    public ConsysStringTextBox getAccountNoBox() {
        return accountNoBox;
    }

    /**
     * @return the bankNoBox
     */
    public ConsysStringTextBox getBankNoBox() {
        return bankNoBox;
    }
}
