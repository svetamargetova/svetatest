package consys.common.gwt.client.ui.layout;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.layout.head.ChangeLocaleRow;
import consys.common.gwt.client.ui.utils.CorporateUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.action.CheckCorporationAction;

/**
 * Zahlavi s logem a zmenou jazyka
 * @author pepa
 */
public class LayoutHead extends FlowPanel {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(LayoutHead.class);
    // instance
    private static LayoutHead instance;
    // komponenty
    private SimplePanel rowPanelPositioner;
    private FlowPanel rowPanel;
    private FocusPanel wrapper;
    // data
    private LogoTimer timer;

    private LayoutHead() {
        super();
        setStyleName(ResourceUtils.system().css().head());

        timer = new LogoTimer();

        rowPanelPositioner = StyleUtils.getPadder("1px");
        rowPanelPositioner.addStyleName(ResourceUtils.css().rowPositioner());

        wrapper = logoWrapper();

        rowPanel = new FlowPanel();
        rowPanel.setStyleName(ResourceUtils.system().css().row());

        add(rowPanelPositioner);
        add(rowPanel);
        add(wrapper);
        add(StyleUtils.clearDiv());

        addToRow(new ChangeLocaleRow());
    }

    @Override
    protected void onLoad() {
        timer.schedule(3000);
    }

    @Override
    protected void onUnload() {
        timer.cancel();
    }

    private Image logoImage(String url) {
        Image image = new Image(url);
        image.addStyleName(StyleUtils.HAND);
        return image;
    }

    /** vraci instanci LayoutHead */
    public static LayoutHead get() {
        if (instance == null) {
            instance = new LayoutHead();
        }
        return instance;
    }

    /** pridani komponenty do radku v hlavicce aplikace (nejlip div) */
    public final void addToRow(Widget panel) {
        rowPanel.add(panel);
    }

    private FocusPanel logoWrapper() {
        FocusPanel imageWrapper = new FocusPanel(new Image(CorporateUtils.DEFAULT_START_TITLE_IMAGE_URL));
        imageWrapper.setWidth("181px");
        imageWrapper.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        imageWrapper.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                History.newItem("Events");
            }
        });
        return imageWrapper;
    }

    private ErrorHandler imageErrorHandler(final Image image) {
        return new ErrorHandler() {
            @Override
            public void onError(ErrorEvent event) {
                image.setUrl(CorporateUtils.DEFAULT_TITLE_IMAGE_URL);
            }
        };
    }

    private LoadHandler imageLoadHandler(final Image image, final SimplePanel positioner) {
        return new LoadHandler() {
            @Override
            public void onLoad(LoadEvent event) {
                positioner.setHeight(Math.max(1, image.getHeight() - 39) + "px");
            }
        };
    }

    /** zpomali vykresleni a overeni loga, protoze se komponenta vklada jeste pred inicializaci volani na server */
    private class LogoTimer extends Timer {

        @Override
        public void run() {
            final String corporationPrefix = CorporateUtils.corporationPrefix();

            if (!corporationPrefix.isEmpty()) {
                EventBus.fire(new DispatchEvent(new CheckCorporationAction(corporationPrefix), new AsyncCallback<BooleanResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // neco se pokazilo nastavime vychozi
                        logger.debug("check corporation problem");
                        wrapper.setWidget(logoImage(CorporateUtils.DEFAULT_TITLE_IMAGE_URL));
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        logger.debug("check corporation result: " + result.isBool());
                        if (result.isBool()) {
                            Image image = logoImage(CorporateUtils.systemTitleImageUrl(corporationPrefix));
                            image.addErrorHandler(imageErrorHandler(image));
                            image.addLoadHandler(imageLoadHandler(image, rowPanelPositioner));
                            wrapper.setWidget(image);
                        } else {
                            Window.Location.replace("https://app.takeplace.eu");
                        }
                    }
                }, null));
            } else {
                logger.debug("no corporation prefix");
                wrapper.setWidget(logoImage(CorporateUtils.DEFAULT_TITLE_IMAGE_URL));
            }
        }
    }
}
