package consys.common.gwt.client.ui.comp.progress;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Polozka progresu
 * @author pepa
 */
public class ProgressItem extends Composite {

    public ProgressItem(boolean active, int number, String title) {
        FlexTable ft = new FlexTable();
        ft.setBorderWidth(0);
        ft.setCellPadding(0);
        ft.setCellSpacing(0);
        String baseStyle = active ? ResourceUtils.system().css().progressActive() : ResourceUtils.system().css().progressInactive();
        ft.setStyleName(baseStyle);

        Image numberImage = null;
        switch (number) {
            case 1:
                if (active) {
                    numberImage = new Image(ResourceUtils.system().progNo1n());
                } else {
                    numberImage = new Image(ResourceUtils.system().progNo1f());
                }
                break;
            case 2:
                if (active) {
                    numberImage = new Image(ResourceUtils.system().progNo2n());
                } else {
                    numberImage = new Image(ResourceUtils.system().progNo2f());
                }
                break;
            case 3:
                if (active) {
                    numberImage = new Image(ResourceUtils.system().progNo3n());
                } else {
                    numberImage = new Image(ResourceUtils.system().progNo3f());
                }
                break;
        }
        numberImage.setStyleName(StyleUtils.MARGIN_LEFT_10);

        Label titleLabel = StyleUtils.getStyledLabel(title, StyleUtils.FONT_14PX);
        titleLabel.addStyleName(StyleUtils.FONT_BOLD);
        titleLabel.addStyleName(StyleUtils.MARGIN_RIGHT_20);
        titleLabel.addStyleName(StyleUtils.MARGIN_VER_10);
        titleLabel.addStyleName(active ? StyleUtils.TEXT_DEFAULT : StyleUtils.TEXT_GRAY4);

        FlexCellFormatter f = ft.getFlexCellFormatter();
        f.setRowSpan(0, 0, 2);
        f.setHorizontalAlignment(0, 0, HasAlignment.ALIGN_CENTER);
        f.setVerticalAlignment(0, 1, HasAlignment.ALIGN_MIDDLE);
        f.setHorizontalAlignment(0, 1, HasAlignment.ALIGN_CENTER);

        ft.setWidget(0, 0, numberImage);
        ft.setWidget(0, 1, titleLabel);

        initWidget(ft);
    }
}
