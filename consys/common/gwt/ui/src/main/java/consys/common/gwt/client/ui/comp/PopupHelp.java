package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.wrapper.ConsysGreenWrapper;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Popup pro zobrazeni napovedy ke komponente
 * @author pepa
 */
public class PopupHelp extends PopupPanel {

    // konstanty
    private static final int SPACE = 4;
    // komponenty
    private Element source;
    private int width;
    private int height;

    /**
     * TODO: Pepo toto prerob tak aby to bylo v CSS a nrejak to cele prejdi
     */
    public PopupHelp(String text, Element source) {
        super(true, false);
        Widget label = StyleUtils.getStyledLabel(text, StyleUtils.MARGIN_VER_3 + " " + StyleUtils.MARGIN_HOR_5 + " " + StyleUtils.FONT_BOLD);
        addStyleName(StyleUtils.INDEX_100);
        this.source = source;
        width = -1;
        Image bubbleFrom = new Image(ResourceUtils.system().hpTriagle());

        VerticalPanel panel = new VerticalPanel();
        panel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        panel.add(new ConsysGreenWrapper(label));
        panel.add(bubbleFrom);
        setWidget(panel);
    }
    
    
    public PopupHelp(String text, Widget source) {
        this(StyleUtils.getStyledLabel(text, StyleUtils.MARGIN_VER_3 + " " + StyleUtils.MARGIN_HOR_5 + " " + StyleUtils.FONT_BOLD), source);
    }

    public PopupHelp(Widget widget, Widget source) {
        super(true, false);
        addStyleName(StyleUtils.INDEX_100);
        this.source = source.getElement();
        width = -1;
        Image bubbleFrom = new Image(ResourceUtils.system().hpTriagle());

        VerticalPanel panel = new VerticalPanel();
        panel.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        panel.add(new ConsysGreenWrapper(widget));
        panel.add(bubbleFrom);
        setWidget(panel);
    }

    /** zobrazi napovedu */
    @Override
    public void show() {
        if (width == -1) {
            setPopupPosition(source.getAbsoluteLeft(), source.getAbsoluteTop());
            super.show();
            width = getOffsetWidth() / 2;
            height = getOffsetHeight() + SPACE;
            super.hide();
        }
        int left = source.getAbsoluteLeft() + (source.getOffsetWidth() / 2) - width;
        int top = source.getAbsoluteTop() - height;
        setPopupPosition(left, top);
        super.show();
    }

    /** pri zobrazeni se vypocita nova vyska popupu */
    public void recalculate() {
        width = -1;
    }
}
