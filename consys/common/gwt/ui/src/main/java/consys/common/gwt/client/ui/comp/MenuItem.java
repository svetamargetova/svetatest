package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Accessibility;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.UIObject;

public class MenuItem extends UIObject implements HasHTML {

    private static final String DEPENDENT_STYLENAME_SELECTED_ITEM = "selected";
    private Command command;
    private MenuBar parentMenu;

    /**
     * Constructs a new menu item that fires a command when it is selected.
     *
     * @param text the item's text
     * @param cmd the command to be fired when it is selected
     */
    public MenuItem(String text, Command cmd) {
        this(text, false);
        setCommand(cmd);
    }

    /**
     * Constructs a new menu item that fires a command when it is selected.
     *
     * @param text the item's text
     * @param asHTML <code>true</code> to treat the specified text as html
     * @param cmd the command to be fired when it is selected
     */
    public MenuItem(String text, boolean asHTML, Command cmd) {
        this(text, asHTML);
        setCommand(cmd);
    }

    public MenuItem(String text, boolean asHTML) {
        setElement(DOM.createDiv());
        setSelectionStyle(false);

        if (asHTML) {
            setHTML(text);
        } else {
            setText(text);
        }
        setStyleName("gwt-MenuItem");

        DOM.setElementAttribute(getElement(), "id", DOM.createUniqueId());
        // Add a11y role "menuitem"
        Accessibility.setRole(getElement(), Accessibility.ROLE_MENUITEM);
    }

    /**
     * Gets the command associated with this item.
     *
     * @return this item's command, or <code>null</code> if none exists
     */
    public Command getCommand() {
        return command;
    }

    @Override
    public String getHTML() {
        return DOM.getInnerHTML(getElement());
    }

    /**
     * Gets the menu that contains this item.
     *
     * @return the parent menu, or <code>null</code> if none exists.
     */
    public MenuBar getParentMenu() {
        return parentMenu;
    }

    @Override
    public String getText() {
        return DOM.getInnerText(getElement());
    }

    /**
     * Sets the command associated with this item.
     *
     * @param cmd the command to be associated with this item
     */
    public void setCommand(Command cmd) {
        command = cmd;
    }

    @Override
    public void setHTML(String html) {
        DOM.setInnerHTML(getElement(), html);
    }

    @Override
    public void setText(String text) {
        DOM.setInnerText(getElement(), text);
    }

    /**
     * Also sets the Debug IDs of MenuItems in the submenu of this
     * {@link MenuItem} if a submenu exists.
     *
     * @see UIObject#onEnsureDebugId(String)
     */
    @Override
    protected void onEnsureDebugId(String baseID) {
        super.onEnsureDebugId(baseID);
    }

    protected void setSelectionStyle(boolean selected) {
        if (selected) {
            addStyleDependentName(DEPENDENT_STYLENAME_SELECTED_ITEM);
        } else {
            removeStyleDependentName(DEPENDENT_STYLENAME_SELECTED_ITEM);
        }
    }

    void setParentMenu(MenuBar parentMenu) {
        this.parentMenu = parentMenu;
    }
}
