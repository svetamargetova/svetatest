package consys.common.gwt.client.ui.comp.panel.abst;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;

/**
 * Interface pro read a update ve formulari
 *
 * Obsahuje clickhandler pro EDIT, Save, Cancel
 * @author pepa
 */
public interface RU {

    /** zobrazi pohled pro prohlizeni */
    public Widget readForm();

    /** zobrazi pohled pro editaci */
    public Widget updateForm(String id);

    /** ClickHandler pro potvrzeni editace */
    public ClickHandler confirmUpdateClickHandler(String id);

    /** ClickHandler pro prechod do editacniho modu */
    public ClickHandler toEditModeClickHandler(String id);

    /** ClickHandler pro prechod do read modu */
    public ClickHandler toReadModeClickHandler(String id);

    /** 
     * V pripade uspechu ulozenia sa prevolava tento callback. S tym ze sa zobra-
     * zi automaticky skovavatelna success message, po prenuti do read modu.
     * 
     * @param message sprava ktora sa zobrazi a zmizne. V pripade ze je null
     * nezobrazi sa nic.
     */
    public void onSuccessUpdate(String message);

     /**
     * V pripade neuspechu ulozenia sa prevolava tento callback. S tym ze sa zobra-
     * zi automaticky skovavatelna success message.
     *
     * @param message sprava ktora sa zobrazi a zmizne. V pripade ze je null
     * nezobrazi sa nic.
     */
    public void onFailedUpdate(String message);
    
}
