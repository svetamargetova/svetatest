package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.select.ItemBoard;
import consys.common.gwt.client.ui.comp.select.SearchTimer;
import consys.common.gwt.client.ui.comp.wrapper.InputTextWrapper;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.NextComponentEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ConsysSelectBox<T> extends Composite implements ConsysSelectBoxInterface<T>,
        ChangeValueEvent.Handler<SelectBoxItem<T>>, NextComponentEvent.Handler, Validable {

    // konstanty
    private static final int TRIANGLE_WIDTH = 20;
    private static final int BORDERS_WIDTH = 2 * InputTextWrapper.BORDER_WIDTH;
    // komponenty
    private FlowPanel mainPanel;
    private TextBox box;
    private Label label;
    private ItemBoard<T> board;
    private SearchTimer<T> searchTimer;
    // data
    private boolean enabled;
    private ArrayList<ChangeValueEvent.Handler> changeValueHandlers;
    private final int innerWidth;
    private ArrayList<SelectBoxItem<T>> items;
    private SelectBoxItem<T> selectedItem;
    private T preselectedItem;
    private String fieldName;    
    private int onInitSelectIndex = -1;

    public ConsysSelectBox() {
        this(ConsysTextArea.WIDTH);
    }

    public ConsysSelectBox(String fieldName) {
        this(ConsysTextArea.WIDTH, fieldName);
    }

    public ConsysSelectBox(int width) {
        this(width, null);
    }

    public ConsysSelectBox(int width, String fieldName) {
        this.fieldName = fieldName;

        enabled = true;
        changeValueHandlers = new ArrayList<ChangeValueEvent.Handler>();

        InputResources.INSTANCE.css().ensureInjected();

        final int boxWidth = width - BORDERS_WIDTH - TRIANGLE_WIDTH;
        innerWidth = width - BORDERS_WIDTH;

        mainPanel = new FlowPanel();
        mainPanel.setWidth(innerWidth + "px");
        mainPanel.setHeight("15px");
        DOM.setStyleAttribute(mainPanel.getElement(), "overflow", "hidden");

        initBox(boxWidth);
        initLabel(boxWidth);

        mainPanel.add(arrowPanel());
        mainPanel.add(label);

        searchTimer = new SearchTimer<T>(box) {

            @Override
            public void setSelectedItem(SelectBoxItem<T> selectedItem) {
                ConsysSelectBox.this.setSelectedItem(selectedItem);
            }
        };

        FocusPanel panel = new FocusPanel();
        panel.setStyleName(InputResources.INSTANCE.css().consysSelectBox());
        panel.setWidget(new InputTextWrapper(mainPanel, width));
        panel.addFocusHandler(panelFocusHandler());
        panel.addClickHandler(panelClickHandler());

        initWidget(panel);
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(ChangeValueEvent.TYPE, this);
        EventBus.get().addHandler(NextComponentEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(ChangeValueEvent.TYPE, this);
        EventBus.get().removeHandler(NextComponentEvent.TYPE, this);
        hideBoard();
    }

    private void initBox(int width) {
        box = new TextBox();
        box.setStyleName(InputResources.INSTANCE.css().consysSelectBoxInput());
        box.setWidth(width + "px");
        box.addKeyUpHandler(boxKeyUpHandler());
    }

    private void initLabel(int width) {
        label = new Label();
        label.setStyleName(InputResources.INSTANCE.css().consysSelectBoxLabel());
        label.setWidth(width + "px");
    }

    private void showBox() {
        mainPanel.getWidget(1).removeFromParent();
        mainPanel.add(box);
        box.setFocus(true);
        showBoard();
    }

    private void showLabel() {
        mainPanel.getWidget(1).removeFromParent();
        mainPanel.add(label);
    }

    private SimplePanel arrowPanel() {
        SimplePanel arrowPanel = FormUtils.createPanel(InputResources.INSTANCE.css().arrowPanel());
        arrowPanel.setWidget(new Image(InputResources.INSTANCE.consysSelectBoxArrow()));
        return arrowPanel;
    }

    private FocusHandler panelFocusHandler() {
        return new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                showBox();
            }
        };
    }

    private ClickHandler panelClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showBox();
            }
        };
    }

    private KeyUpHandler boxKeyUpHandler() {
        return new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                switch (event.getNativeKeyCode()) {
                    case KeyCodes.KEY_ENTER:
                        SelectBoxItem<T> item = board.getBoardSelectedItem();
                        // vyvolani eventu
                        EventBus.fire(new ChangeValueEvent<SelectBoxItem>(ConsysSelectBox.this, getSelectedItem(), item));
                        // vybrani nove hodnoty
                        setSelectedItem(item);
                        box.setFocus(false);
                        showLabel();
                        board.hide();
                        break;
                    case KeyCodes.KEY_UP:
                        board.up();
                        break;
                    case KeyCodes.KEY_DOWN:
                        board.down();
                        break;
                    default:
                        if (event.getNativeKeyCode() < 32) {
                            return;
                        } else {
                            searchTimer.cancel();
                            searchTimer.schedule(500);
                        }
                        break;
                }
            }
        };
    }

    /** zobrazi vyberovy popup panel */
    private void showBoard() {
        if (board == null) {
            board = new ItemBoard<T>(innerWidth + "px", this) {

                @Override
                public SelectBoxItem<T> getSelectedItem() {
                    return ConsysSelectBox.this.getSelectedItem();
                }

                @Override
                public void setSelectedItem(SelectBoxItem<T> i) {
                    ConsysSelectBox.this.setSelectedItem(i);
                }
            };
            board.addCloseHandler(new CloseHandler<PopupPanel>() {

                @Override
                public void onClose(CloseEvent<PopupPanel> event) {
                    showLabel();
                }
            });
            searchTimer.setItemBoard(board);
            board.setItems(items);
            board.setBoardSelectedItem(selectedItem);
        }
        if (!board.isShowing()) {
            board.setPopupPosition(getAbsoluteLeft() + 8, getAbsoluteTop() + 25);
            board.show();
            board.setBoardSelectedItem(selectedItem);
        }
    }

    /** skryje vyberovou cast */
    private void hideBoard() {
        if (board != null) {
            board.hide();
        }
    }

    @Override
    public String validate() {                
        if (selectedItem == null) {             
            return UIMessageUtils.m.const_mustChoose(fieldName);
        }
        return null;
    }

    

    /** vraci true pokud je komponenta povolena */
    public boolean isEnabled() {
        return enabled;
    }

    /** enabluje nebo disabluje komponentu podle parametru */
    @Override
    public void setEnabled(boolean enabled) {
        if (this.enabled == enabled) {
            return;
        }
        this.enabled = enabled;
        box.setEnabled(enabled);
    }

    /** zaregistruje handler */
    @Override
    public void addChangeValueHandler(ChangeValueEvent.Handler handler) {
        changeValueHandlers.add(handler);
    }

    @Override
    public void onChangeValue(ChangeValueEvent<SelectBoxItem<T>> event) {
        if (enabled && event.getComponent().equals(this)) {
            for (ChangeValueEvent.Handler handler : changeValueHandlers) {
                handler.onChangeValue(event);
            }
        }
    }

    @Override
    public void onNextComponent(NextComponentEvent event) {
        if (event.getComponent() != this) {
            hideBoard();
        }
    }

    /** nastavuje polozky k vyberu */
    public void setItems(ArrayList<SelectBoxItem<T>> items) {
        this.items = items;
        searchTimer.setItemList(items);
        board = null;
        if (preselectedItem != null) {
            doSelectItem(preselectedItem);
        }
        if (onInitSelectIndex >= 0) {
            selectItemByIndex(onInitSelectIndex);
        }
    }

    /** vraci vybranou polozku pripadne null, kdyz nebyla vybrana zadna */
    public SelectBoxItem<T> getSelectedItem() {
        return selectedItem;
    }

    /** nastavi vybranou polozku */
    public void setSelectedItem(SelectBoxItem<T> selectedItem) {
        this.selectedItem = selectedItem;
        label.setText(selectedItem.getName());
        showLabel();
        if (board != null) {
            board.setBoardSelectedItem(selectedItem);
        }
    }

    /** nastavi vybranou polozku podle poradi v seznamu polozek */
    public void selectItemByIndex(int index) {
        if (items != null && !items.isEmpty()) {
            setSelectedItem(items.get(index));
        } else {
            onInitSelectIndex = index;
        }
    }

    @Override
    public void selectItem(T item) {
        if (items == null) {
            preselectedItem = item;
        } else {
            doSelectItem(item);
        }
    }

    /** vybere zadanou polozku */
    private void doSelectItem(T value) {
        for (int i = 0; i < items.size(); i++) {
            SelectBoxItem<T> item = items.get(i);
            if (item.getItem().equals(value)) {
                selectItemByIndex(i);
                break;
            }
        }
    }

    @Override
    public void selectFirst(boolean selectFirst) {
        onInitSelectIndex = 0;
        if (items != null && !items.isEmpty()) {
            selectItemByIndex(onInitSelectIndex);
        }
    }
}
