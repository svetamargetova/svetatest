package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import java.util.Date;

/**
 * Zobrazovatko casu klienta
 * @author pepa
 */
public class ClientDate extends Composite {

    // komponenty
    private Label label;

    public ClientDate(Date date, String... styles) {
        label = new Label(DateTimeUtils.getDate(date));
        for (int i = 0; i < styles.length; i++) {
            String style = styles[i];
            label.addStyleName(style);
        }
        initWidget(label);
    }

    /** zobrazi zadane datum */
    public void setDate(Date date) {
        label.setText(DateTimeUtils.getDate(date));
    }
}
