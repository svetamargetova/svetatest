/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.user.UserLabel;
import consys.common.gwt.client.ui.comp.user.UserResources;
import consys.common.gwt.shared.bo.ClientUserCommonInfo;

/**
 * Zoznam uzivatelov ktory su za sebov v inline komponente.
 * @author palo
 */
public class UserInlineListPanel extends FlowPanel{
        
    public UserInlineListPanel() {
        UserResources.INSTANCE.userCss().ensureInjected();
        setStyleName(UserResources.INSTANCE.userCss().userInlineListPanel());
    }
    
    
    public void addUser(String userUuid, String userName){
        UserLabel userLabel = new UserLabel(userUuid, userName);
        this.add(userLabel);
        
    }
    
    public void addUser(ClientUserCommonInfo user){
        UserLabel userLabel = new UserLabel(user);
        this.add(userLabel);
    }                
}
