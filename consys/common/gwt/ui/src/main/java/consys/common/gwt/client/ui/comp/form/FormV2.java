package consys.common.gwt.client.ui.comp.form;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.Help;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author palo
 */
public class FormV2 extends BaseForm {

    // konstanty
    private static final String DEFAULT_END_ELEMENT_TEXT = ":";
    // data
    private List<FormElement> formElements;

    public FormV2() {
        formElements = new ArrayList<FormElement>();
    }

    /*@Override
    public FormElement add(String title, Validable value, boolean required) {
        return newWidgetFormElement(title, value.asWidget(), null, required);
    }

    @Override
    public FormElement add(String title, Validable value, String help, boolean required) {
        return newWidgetFormElement(title, value.asWidget(), help, required);
    }*/

    @Override
    public FormElement add(String title, String value, String defaultValue, boolean required) {
        return add(title, value, defaultValue, null, required);
    }

    @Override
    public FormElement add(String title, String value, String defaultValue, String help, boolean required) {
        return newTextFormElement(title, value, defaultValue, help, required);
    }

    @Override
    public FormElement add(String title, Widget value, boolean required) {
        return add(title, value, null, required);
    }

    @Override
    public FormElement add(String title, Widget value, String help, boolean required) {
        return newWidgetFormElement(title, value, help, required);
    }

    @Override
    public FormElement add(Widget titleWidget, Widget value, boolean required) {
        if (titleWidget == null) {
            return addValueWidget(value);
        } else {
            FormElementImpl fe = createFormElement(titleWidget, value, required);
            super.add(fe.getWrapper());
            return fe;
        }
    }

    @Override
    public FormElement addValueWidget(Widget value) {
        FormElementImpl fe = createFormElement(pad(), value, false);
        super.add(fe.getWrapper());
        return fe;
    }

    @Override
    public boolean validate() {
        boolean out = true;
        for (FormElement formElement : formElements) {
            out &= formElement.doValidation();
        }
        return out;
    }

    private FormElement newWidgetFormElement(String title, Widget value, String help, boolean required) {
        if (title == null) {
            return addValueWidget(value);
        } else {
            FormElementImpl fe = createFormElement(getTitleWidget(title, help, true), value, required);
            super.add(fe.getWrapper());
            return fe;
        }
    }

    private FormElement newTextFormElement(String title, String value, String defaultValue, String help, boolean required) {
        FormElementImpl fe = createFormElement(getTitleWidget(title, help, true), getValueWidget(value, defaultValue), required);
        super.add(fe.getWrapper());
        return fe;
    }

    /** @param rightOrder urcuje poradi vlozeni komponent, standardni poradi (true) je prvni dvojtecka a pak titulek */
    public static Widget getTitleWidget(String title, String help, boolean rightOrder) {
        return getTitleWidget(title, help, rightOrder, DEFAULT_END_ELEMENT_TEXT);
    }

    /** @param rightOrder urcuje poradi vlozeni komponent, standardni poradi (true) je prvni koncovy text a pak titulek */
    public static Widget getTitleWidget(String title, String help, boolean rightOrder, String endElementText) {
        SimplePanel titleWrapper = new SimplePanel();

        Element titleDivWrapper = DOM.createDiv();

        Element titleDiv = DOM.createDiv();
        titleDiv.setInnerText(title);
        titleDiv.setClassName(FormResources.INSTANCE.formCss().formElementTitleWidgetTitle());

        Element endDiv = DOM.createDiv();
        endDiv.setClassName(FormResources.INSTANCE.formCss().formElementTitleWidgetEnd());

        if (help != null) {
            endDiv.appendChild(getHelpElement(title, help));
        }
        endDiv.appendChild(getEndElement(endElementText));

        if (rightOrder) {
            titleDivWrapper.appendChild(endDiv);
            titleDivWrapper.appendChild(titleDiv);
        } else {
            titleDivWrapper.appendChild(titleDiv);
            titleDivWrapper.appendChild(endDiv);
        }
        titleDivWrapper.appendChild(StyleUtils.clearDivElement());
        titleWrapper.getElement().appendChild(titleDivWrapper);
        return titleWrapper;
    }

    public static Widget getValueWidget(String value, String defaultValue) {
        SimplePanel panel = new SimplePanel();
        if (StringUtils.isBlank(value)) {
            Label l = new Label(defaultValue);
            l.setStyleName(FormResources.INSTANCE.formCss().formElementValueWidgetDefault());
            panel.setWidget(l);
        } else {
            SafeHtmlBuilder builder = new SafeHtmlBuilder();
            builder.appendEscapedLines(value);
            SafeHtml shtml = builder.toSafeHtml();
            panel.setWidget(new HTML(shtml));
        }
        return panel;
    }

    private FormElementImpl createFormElement(Widget titleWidget, Widget valueWidget, boolean required) {
        FormElementImpl elem = new FormElementImpl(titleWidget, valueWidget, required);
        formElements.add(elem);
        return elem;

    }

    private static Element getEndElement(String endElementText) {
        Element e = DOM.createSpan();
        e.setInnerText(endElementText);
        return e;
    }

    private static Element getHelpElement(String helpTitle, String helpText) {

        final Element e = DOM.createSpan();
        e.setClassName(FormResources.INSTANCE.formCss().formElementTitleHelp());

        final Help help = Help.left(helpTitle, helpText);
        e.setInnerHTML("<sup>?</sup>");
        DOM.setEventListener(e, new EventListener() {

            @Override
            public void onBrowserEvent(Event event) {
                if (event.getTypeInt() == Event.ONMOUSEOUT) {
                    help.hide();
                } else if (event.getTypeInt() == Event.ONMOUSEOVER) {
                    help.setPopupPosition(e.getAbsoluteLeft() + 10, e.getAbsoluteTop());
                    help.show();
                }
            }
        });
        DOM.sinkEvents(e, Event.ONMOUSEOUT | Event.ONMOUSEOVER);
        return e;
    }

    private SimplePanel pad() {
        SimplePanel panel = new SimplePanel();
        panel.setStyleName(FormResources.INSTANCE.formCss().formPad());
        return panel;
    }
}
