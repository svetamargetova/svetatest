package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.shared.bo.ClientUserCommonInfo;

/**
 * Label ktory reprezentuje uzivatela Takeplace kdekolvek je uzivatel spomenuty.
 * Vstupem je uzivatelske meno a jeho uuid. Po hoveru na meno sa zobrazi user card.
 * <p/>
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserLabel extends Label implements ActionExecutionDelegate {

    private String userUuid;
    private String userName;
    /**
     * Common info ak je nastavene tak sa na hover na meno nedotahuje
     */
    private ClientUserCommonInfo commonInfo;

    public UserLabel() {
    }

    public UserLabel(String userUuid, String userName) {
        this.userUuid = userUuid;
        this.userName = userName;
        init(userName, userUuid);
    }

    public UserLabel(ClientUserCommonInfo commonInfo) {
        this(commonInfo.getUuid(),commonInfo.getFullName());
        this.commonInfo = commonInfo;        
    }
    
    public String getUserUuid() {
        return userUuid;
    }

    public final void init(String userName, String userUuid) {
        this.userUuid = userUuid;
        this.userName = userName;
        //
        setName(userName);
        setStyleName(UserResources.INSTANCE.userCss().userLabel());
    }

    private void setName(String userName) {
        setText(userName);
        addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserCard userCard = null;
                if (commonInfo != null) {
                    userCard = new UserCard(commonInfo);

                } else {
                    userCard = new UserCard(userUuid);
                }
                userCard.setPosition(UserLabel.this);
                userCard.show();
            }
        });
    }

    @Override
    public void actionStarted() {
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
    }
}
