package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.ui.comp.panel.ListPanelItem;
import consys.common.gwt.client.ui.comp.search.SearchItem;
import consys.common.gwt.client.ui.comp.user.UserListThumbLabel;
import consys.common.gwt.client.ui.comp.user.UserResources;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserListItem extends ListPanelItem<ClientUserWithAffiliationThumb> implements SearchItem<ClientUserWithAffiliationThumb> {

    public static final int HEIGHT = 55;
    public static final String HEIGHT_PX = "55px";
    private ClientUserWithAffiliationThumb object;

    public UserListItem(ClientUserWithAffiliationThumb object) {
        this.object = object;
        setHeight(HEIGHT_PX);
    }

    @Override
    public ClientUserWithAffiliationThumb getObject() {
        return object;
    }

    @Override
    public void create(FlowPanel panel) {
        Image portrait = FormUtils.userPortrait(object.getUuidPortraitImagePrefix(), UserProfileImageEnum.LIST);        
        SimplePanel imgWrapper = new SimplePanel();
        imgWrapper.setStyleName(UserResources.INSTANCE.userCss().userListItemPortrait());
        imgWrapper.setWidget(portrait);

        UserListThumbLabel label = new UserListThumbLabel(object);
        Label position = new Label(object.getPosition());

        FlowPanel infoPanel = new FlowPanel();
        infoPanel.add(label);
        infoPanel.add(position);
        infoPanel.addStyleName(UserResources.INSTANCE.userCss().userListItemName());

        panel.add(imgWrapper);
        panel.add(infoPanel);
        panel.add(StyleUtils.clearDiv());
    }

    @Override
    public void hover() {
    }

    @Override
    public void unhover() {
    }

    @Override
    public ClientUserWithAffiliationThumb getItem() {
        return object;
    }
}
