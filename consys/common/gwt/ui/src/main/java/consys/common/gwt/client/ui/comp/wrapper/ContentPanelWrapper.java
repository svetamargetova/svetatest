package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.*;

/**
 * Panel ktory vytvori okolo widgetu sedu oblast.
 * 
 * Wrapper sa sklada z troch DIVov, ktory tvori hornu hranicu, 
 * dolnu hranicu a obsah.
 * 
 * @author palo
 */
public class ContentPanelWrapper extends Composite {

    // komponenty
    private SimplePanel content;

    private final FlowPanel wrapper;

    public ContentPanelWrapper() {
        this(null);
    }

    public ContentPanelWrapper(Widget wrappedObject) {
        WrapperResources.INSTANCE.css().ensureInjected();
        // zakladny wrapper        
        wrapper = new FlowPanel();
        wrapper.setStyleName(WrapperResources.INSTANCE.css().contentPanelWrapper());
        initWidget(wrapper);

        // horna hranica
        Element top = DOM.createDiv();
        top.setClassName(WrapperResources.INSTANCE.css().contentPanelWrapperTop());

        // obsah
        content = new SimplePanel(wrappedObject);
        content.addStyleName(WrapperResources.INSTANCE.css().contentPanelWrapperContent());

        // dolna hranica
        Element bottom = DOM.createDiv();
        bottom.setClassName(WrapperResources.INSTANCE.css().contentPanelWrapperBottom());

        // vyskladanie
        wrapper.getElement().appendChild(top);
        wrapper.add(content);
        wrapper.getElement().appendChild(bottom);
    }

    public void setContent(Widget wrappedObject) {
        content.setWidget(wrappedObject);
    }
}
