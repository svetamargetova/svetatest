package consys.common.gwt.client.ui.comp.form;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
@CssResource.ImportedWithPrefix("form")
public interface FormCss extends CssResource {

    String form();

    String formElement();

    String formElementNormal();

    String formElementTitle();

    String formElementRequired();

    String formElementTitleHelp();

    String formElementTitleWidget();

    String formElementTitleWidgetTitle();

    String formElementTitleWidgetEnd();

    String formElementValueWidget();

    String formElementValueWidgetDefault();

    String formPad();

    String inputWrapper();

    String errorLabel();
}
