package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import java.util.ArrayList;

/**
 *
 *  Spravit jak obecny decorator v priapde editacie. Bude obecny list item ktery
 *  zaobaly listitem ale prida Remove. rovnako provola moznost do list itemu aby
 *  sa neco schovalo.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserLabelListEditPanel extends FlowPanel implements SearchDelegate<ClientInvitedUser>, UserLabelListEditItem.RemoveDelegate {

    private UserLabelListPanel userList;
    private ArrayList<ClientInvitedUser> newMembers;
    private ArrayList<ClientUserThumb> removedMembers;
    private ArrayList<ClientUserThumb> pureItems;
    private String addLabel;

    public void setAddLabelName(String addLabel) {
        this.addLabel = addLabel;
    }

    public void setStaticHeight() {
        userList.setStaticHeight();
    }

    public UserLabelListEditPanel(ArrayList<ClientUserThumb> items) {
        newMembers = new ArrayList<ClientInvitedUser>();
        removedMembers = new ArrayList<ClientUserThumb>();
        pureItems = items;
        userList = new UserLabelListPanel();
    }

    @Override
    protected void onLoad() {
        ActionLabel addUser = new ActionLabel(addLabel);
        addUser.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog searchDialog = new UserSearchDialog();
                searchDialog.setDelegate(UserLabelListEditPanel.this);
                searchDialog.showCentered();
            }
        });

        SimplePanel actionWrapper = new SimplePanel();
        actionWrapper.setWidget(addUser);
        actionWrapper.setStyleName(ResourceUtils.system().css().listPanelEditAction());
        add(actionWrapper);

        for (ClientUserThumb item : pureItems) {
            addItem(item);
        }
        add(userList);
    }

    /** prida styl do list panelu */
    public void addListStyleName(String styleName) {
        userList.addStyleName(styleName);
    }

    private void addItem(ClientUserThumb item) {
        UserLabelListEditItem editItem = new UserLabelListEditItem(item);
        editItem.setRemoveDelegate(this);
        userList.addItem(editItem);
    }

    private void addItem(ClientInvitedUser item) {
        UserLabelListEditItem editItem = new UserLabelListEditItem(item);
        editItem.setRemoveDelegate(this);
        userList.addItem(editItem);
    }

    @Override
    public void didSelect(ClientInvitedUser item) {
        // overeni jestli uz neni vlozen
        boolean inList = false;
        for (ClientInvitedUser u : newMembers) {
            if (u.getUuid().equals(item.getUuid())) {
                inList = true;
                break;
            }
        }
        if (!inList) {
            for (ClientUserThumb t : pureItems) {
                if (t.getUuid().equals(item.getUuid())) {
                    inList = true;
                    break;
                }
            }
        }
        boolean inRList = false;
        for (ClientUserThumb t : removedMembers) {
            if (t.getUuid().equals(item.getUuid())) {
                inRList = true;
                break;
            }
        }
        // pokud neni vlozime
        if (!inList) {
            newMembers.add(item);
            addItem(item);
        } else if (inRList) {
            addItem(item);
        }
        // pokud byl odstranen a znovu se vlozi
        if (inRList) {
            ClientUserThumb thumb = new ClientUserThumb();
            thumb.setName(item.getName());
            thumb.setUuid(item.getUuid());
            thumb.setUuidPortraitImagePrefix(item.getUuidPortraitImagePrefix());
            removedMembers.remove(thumb);
        }
    }

    @Override
    public void didRemoveItem(UserLabelListEditItem item) {
        final String uuid = item.getClientSearchedOrInvitedUser().getUuid();
        final String email = item.getClientSearchedOrInvitedUser().getEmail();
        if (uuid != null) {
            removedMembers.add(item.getClientSearchedOrInvitedUser().getClientUserThumb());
        }
        userList.removeItem(item.getIndex());
        boolean inList = false;
        for (ClientInvitedUser t : newMembers) {
            if (t.isUserNew()) {
                if (t.getEmail().equals(email)) {
                    inList = true;
                    break;
                }
            } else {
                if (t.getUuid().equals(uuid)) {
                    inList = true;
                    break;
                }
            }
        }
        if (inList) {
            newMembers.remove(item.getClientSearchedOrInvitedUser());
        }
    }

    public ArrayList<ClientInvitedUser> getNewMembers() {
        return newMembers;
    }

    public ArrayList<ClientUserThumb> getRemovedMembers() {
        return removedMembers;
    }
}
