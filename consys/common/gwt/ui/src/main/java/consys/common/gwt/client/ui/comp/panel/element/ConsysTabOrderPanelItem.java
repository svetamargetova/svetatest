package consys.common.gwt.client.ui.comp.panel.element;

/**
 *
 * @author pepa
 */
public class ConsysTabOrderPanelItem<T> extends OrderPanelItem {

    // data
    private ConsysTabPanelItemUI<T> ui;

    public ConsysTabOrderPanelItem(ConsysTabPanelItemUI<T> ui, String name) {
        super(name);
        this.ui = ui;
    }

    public ConsysTabPanelItemUI<T> getUi() {
        return ui;
    }
}
