package consys.common.gwt.client.ui.layout.menu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

/**
 * Třída má na starost správu menu (generovani, přidávání)
 * @author pepa
 */
public class MenuDispatcher {

    // instance
    private static MenuDispatcherImpl instance;
    // konstanty
    public static final String SIGN_UP = MenuDispatcherImpl.SIGN_UP;

    private MenuDispatcher() {
    }

    public static MenuDispatcherImpl get() {
        if (instance == null) {
            instance = GWT.create(MenuDispatcherImpl.class);
        }
        return instance;
    }
   

    /** vlozi polozku menu na konec menu */
    public void addMenuItem(String title, String token) {
        instance.addMenuItem(title, token);
    }

    /** vlozi polozku menu na konec menu */
    public void addMenuItem(String title, String token, Widget widget) {
        instance.addMenuItem(title, token, widget);
    }

    /** vlozi polozku menu na pozici menu */
    public void addMenuItem(String title, String token, Widget widget, int position) {
        instance.addMenuItem(title, token, widget, position);
    }

    /** vlozi polozku menu na konec menu */
    public void addMenuItem(MenuItem menuItem) {
        instance.addMenuItem(menuItem);
    }

    /** vlozi polozku menu na pozici menu */
    public void addMenuItem(MenuItem menuItem, int position) {
        instance.addMenuItem(menuItem, position);
    }

    /** vygeneruje zakladni menu v zavislosti na tom jestli je uzivatel prihlaseny nebo ne */
    public void generateMenu() {
        instance.generateMenu();
    }

    /** obsah umisteny pod "Sign up" v neprihlasenem menu */
    public void setSignUpWidget(Widget widget) {
        instance.setSignUpWidget(widget);
    }
}
