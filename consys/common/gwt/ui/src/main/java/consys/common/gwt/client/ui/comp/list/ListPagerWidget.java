package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * ListPagerWidget je komponenta pozostavajuca z 3 casti
 *
 *  | 11 - 29 of 99 | < | > |
 *
 * @author Palo
 */
public class ListPagerWidget extends FlowPanel {

    // data
    private ListPagerDelegate delegate;
    private boolean hasNext;
    private boolean hasPrev;
    // komponenty
    private Label textInfo;
    private Image prev;
    private Image next;

    public ListPagerWidget() {
        this(null);
    }

    public ListPagerWidget(ListPagerDelegate delegate) {
        super();
        this.delegate = delegate;
        this.setStyleName(ResourceUtils.system().css().pager());
        textInfo = new HTML();
        textInfo.setStyleName(ResourceUtils.system().css().pagerInfoLabel());

    }

    public void setDelegate(ListPagerDelegate delegate) {
        this.delegate = delegate;
    }

    public void setPage(int from, int to, long all) {

        if (all == 0) {
            textInfo.setText(" — ");
            disabledBoth();
        } else {
            textInfo.setText(from + " — " + to + " of " + all);
            previous(!(from == 1));
            next(!(to >= all));
        }
    }

    private void previous(boolean has) {
        hasPrev = has;
        if (has) {
            prev.addStyleName(ResourceUtils.system().css().pagerArrowRight());
            prev.setResource(ResourceUtils.system().listPagerPrevious());
        } else {
            prev.removeStyleName(ResourceUtils.system().css().pagerArrowRight());
            prev.setResource(ResourceUtils.system().listPagerPreviousDisabled());
        }
    }

    private void next(boolean has) {
        hasNext = has;
        if (has) {
            next.setStyleName(ResourceUtils.system().css().pagerArrowLeft());
            next.setResource(ResourceUtils.system().listPagerNext());
        } else {
            next.removeStyleName(ResourceUtils.system().css().pagerArrowLeft());
            next.setResource(ResourceUtils.system().listPagerNextDisabled());
        }
    }

    private void disabledBoth() {
        next(false);
        previous(false);
    }

    @Override
    protected void onLoad() {
        clear();

        // prev image
        prev = new Image();
        prev.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // PREV
                if (hasPrev && delegate != null) {
                    delegate.previousPage();
                }
            }
        });

        SimplePanel prevWrapper = new SimplePanel();
        prevWrapper.addStyleName(StyleUtils.FLOAT_RIGHT);
        prevWrapper.setWidget(prev);

        // next image
        next = new Image();
        next.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // NEXT
                if (hasNext && delegate != null) {
                    delegate.nextPage();
                }
            }
        });

        SimplePanel nextWrapper = new SimplePanel();
        nextWrapper.addStyleName(StyleUtils.FLOAT_RIGHT);
        nextWrapper.setWidget(next);

        // vlozeni
        disabledBoth();

        add(nextWrapper);
        add(prevWrapper);
        add(textInfo);
        add(StyleUtils.clearDiv());
    }

    public interface ListPagerDelegate {

        public void nextPage();

        public void previousPage();
    }
}
