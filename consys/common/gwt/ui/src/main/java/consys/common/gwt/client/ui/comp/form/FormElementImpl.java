package consys.common.gwt.client.ui.comp.form;

import consys.common.gwt.client.ui.comp.input.Validable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author palo
 */
public class FormElementImpl implements FormElement {

    protected final FlowPanel wrapper;
    private final Widget titleWidget;
    private final Widget valueWidget;
    private final boolean required;

    public FormElementImpl(Widget titleWidget, Widget valueWidget, boolean required) {
        wrapper = new FlowPanel();
        wrapper.setStyleName(FormResources.INSTANCE.formCss().formElement());
        wrapper.addStyleName(FormResources.INSTANCE.formCss().formElementNormal());
        this.required = required;
        this.titleWidget = titleWidget;
        titleWidget.addStyleName(FormResources.INSTANCE.formCss().formElementTitleWidget());
        if (required) {
            titleWidget.addStyleName(FormResources.INSTANCE.formCss().formElementRequired());
        }
        wrapper.add(titleWidget);

        this.valueWidget = valueWidget;
        valueWidget.addStyleName(FormResources.INSTANCE.formCss().formElementValueWidget());
        wrapper.add(valueWidget);
        wrapper.add(StyleUtils.clearDiv());

    }

    protected FlowPanel getWrapper() {
        return wrapper;
    }

    @Override
    public Widget getTitleWidget() {
        return titleWidget;
    }

    @Override
    public Widget getValueWidget() {
        return valueWidget;
    }

    @Override
    public void setTitle(boolean title) {
        if (title) {
            wrapper.removeStyleName(FormResources.INSTANCE.formCss().formElementNormal());
            wrapper.addStyleName(FormResources.INSTANCE.formCss().formElementTitle());
        } else {
            wrapper.removeStyleName(FormResources.INSTANCE.formCss().formElementTitle());
            wrapper.addStyleName(FormResources.INSTANCE.formCss().formElementNormal());
        }
    }

    @Override
    public boolean isValidable() {
        return (valueWidget instanceof Validable);
    }

    @Override
    public boolean doValidation() {
        if (isValidable() && required) {
            return ((Validable) valueWidget).validate() == null;
        } else {
            return true;
        }
    }
}
