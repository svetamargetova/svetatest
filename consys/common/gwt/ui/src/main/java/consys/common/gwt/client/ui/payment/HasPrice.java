package consys.common.gwt.client.ui.payment;

import consys.common.gwt.shared.bo.Monetary;

/**
 * Implementuji objekty, ktere uchovavaji udaj o cene
 * @author pepa
 */
public interface HasPrice {

    /** vraci obsazenou cenu */
    public Monetary getPrice();
}
