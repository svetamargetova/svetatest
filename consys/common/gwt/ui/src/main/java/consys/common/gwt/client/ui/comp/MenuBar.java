package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Accessibility;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.MenuItemSeparator;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Palo
 */
public class MenuBar extends Widget implements HasCloseHandlers<PopupPanel>, CloseHandler<PopupPanel> {

    /**
     * List of {@link MenuItem}s, not including {@link MenuItemSeparator}s.
     */
    private ArrayList<MenuItem> items = new ArrayList<MenuItem>();
    /**
     * Body
     */
    private Element body;
    private PopupPanel popup;
    private MenuItem selectedItem;

    public MenuBar() {
        init();
    }

    private void init() {
        body = DOM.createDiv();        
        
        FocusPanel fp = new FocusPanel();
        fp.getElement().appendChild(body);
        setElement(fp.getElement());

        Accessibility.setRole(getElement(), Accessibility.ROLE_MENUBAR);

        sinkEvents(Event.ONCLICK | Event.ONMOUSEOVER | Event.ONMOUSEOUT
                | Event.ONFOCUS | Event.ONKEYDOWN);

        // Hide focus outline in Mozilla/Webkit/Opera
        DOM.setStyleAttribute(getElement(), "outline", "0px");

        // Hide focus outline in IE 6/7
        DOM.setElementAttribute(getElement(), "hideFocus", "true");
    }

    // Work with items

    public void clearItems() {
        // Deselect the current item
        selectItem(null);

        Element container = getItemContainerElement();
        while (DOM.getChildCount(container) > 0) {
            DOM.removeChild(container, DOM.getChild(container, 0));
        }

        // Set the parent of all items to null
        for (MenuItem item : items) {
            item.setParentMenu(null);
        }
        // Clear out all of the items and separators
        items.clear();
    }


    // Items api
    /*
     * This method is called when a menu bar is shown.
     */
    private void onShow() {
        // clear the selection; a keyboard user can cursor down to the first item
        selectItem(null);
    }

    private boolean selectFirstItemIfNoneSelected() {
        if (selectedItem == null) {
            if (items.size() > 0) {
                MenuItem nextItem = items.get(0);
                selectItem(nextItem);
            }
            return true;
        }
        return false;
    }

    public void selectItem(MenuItem item) {
        if (item == selectedItem) {
            return;
        }

        if (selectedItem != null) {
            selectedItem.setSelectionStyle(false);
            // Set the style of the submenu indicator

            Element tr = DOM.getParent(selectedItem.getElement());
            if (DOM.getChildCount(tr) == 2) {
                Element td = DOM.getChild(tr, 1);
                setStyleName(td, "subMenuIcon-selected", false);
            }

        }

        if (item != null) {
            item.setSelectionStyle(true);

            Accessibility.setState(getElement(),
                    Accessibility.STATE_ACTIVEDESCENDANT, DOM.getElementAttribute(
                    item.getElement(), "id"));
        }
        selectedItem = item;
    }

    private void selectNextItem() {
        if (selectedItem == null) {
            return;
        }

        int index = items.indexOf(selectedItem);
        // We know that selectedItem is set to an item that is contained in the
        // items collection.
        // Therefore, we know that index can never be -1.
        assert (index != -1);

        MenuItem itemToBeSelected;

        if (index < items.size() - 1) {
            itemToBeSelected = items.get(index + 1);
        } else { // we're at the end, loop around to the start
            itemToBeSelected = items.get(0);
        }

        selectItem(itemToBeSelected);
    }

    private void selectPrevItem() {
        if (selectedItem == null) {
            return;
        }

        int index = items.indexOf(selectedItem);
        // We know that selectedItem is set to an item that is contained in the
        // items collection.
        // Therefore, we know that index can never be -1.
        assert (index != -1);

        MenuItem itemToBeSelected;
        if (index > 0) {
            itemToBeSelected = items.get(index - 1);

        } else { // we're at the start, loop around to the end
            itemToBeSelected = items.get(items.size() - 1);
        }

        selectItem(itemToBeSelected);
    }

    protected MenuItem getSelectedItem() {
        return this.selectedItem;
    }

    private void openPopup(final MenuItem item) {
        // Create a new popup for this item, and position it next to
        // the item (below if this is a horizontal menu bar, to the
        // right if it's a vertical bar).
        popup = new PopupPanel(true, false);
        popup.setAnimationEnabled(true);
        popup.setStyleName("popup");
        String primaryStyleName = getStylePrimaryName();

        popup.addCloseHandler(this);


        // Show the popup, ensuring that the menubar's event preview remains on top
        // of the popup's.
        popup.setPopupPositionAndShow(new PopupPanel.PositionCallback() {

            @Override
            public void setPosition(int offsetWidth, int offsetHeight) {
                // depending on the bidi direction position a menu on the left or right
                // of its base item
                if (LocaleInfo.getCurrentLocale().isRTL()) {
                    popup.setPopupPosition(MenuBar.this.getAbsoluteLeft() - offsetWidth
                            + 1, item.getAbsoluteTop());

                } else {

                    popup.setPopupPosition(MenuBar.this.getAbsoluteLeft()
                            + MenuBar.this.getOffsetWidth() - 1, item.getAbsoluteTop());

                }
            }
        });
    }

    @Override
    protected void onDetach() {
        // When the menu is detached, make sure to close all of its children.
        if (popup != null) {
            popup.hide();
        }

        super.onDetach();
    }

    /**
     * <b>Affected Elements:</b>
     * <ul>
     * <li>-item# = the {@link MenuItem} at the specified index.</li>
     * </ul>
     *
     * @see UIObject#onEnsureDebugId(String)
     */
    @Override
    protected void onEnsureDebugId(String baseID) {
        super.onEnsureDebugId(baseID);
        setMenuItemDebugIds(baseID);
    }

    /*
     * Closes all parent menu popups.
     */
    void closeAllParents() {
        close();
    }

    /*
     * Performs the action associated with the given menu item. If the item has a
     * popup associated with it, the popup will be shown. If it has a command
     * associated with it, and 'fireCommand' is true, then the command will be
     * fired. Popups associated with other items will be hidden.
     *
     * @param item the item whose popup is to be shown. @param fireCommand
     * <code>true</code> if the item's command should be fired, <code>false</code>
     * otherwise.
     */
    public void doItemAction(final MenuItem item, boolean fireCommand, boolean focus) {
        // Ensure that the item is selected.
        selectItem(item);

        if (item != null) {
            // if the command should be fired and the item has one, fire it
            if (fireCommand && item.getCommand() != null) {
                // Close this menu and all of its parents.
                closeAllParents();

                // Fire the item's command.
                Command cmd = item.getCommand();
                DeferredCommand.addCommand(cmd);
            }
        }
    }

    /**
     * Give this MenuBar focus.
     */
    public void focus() {
        getElement().focus();
    }

    void itemOver(MenuItem item, boolean focus) {
        if (item == null) {
            // Don't clear selection if the currently selected item's menu is showing.
            if (selectedItem != null) {
                return;
            }
        }

        // Style the item selected when the mouse enters.
        selectItem(item);
        if (focus) {
            focus();
        }
    }

    /**
     * Set the IDs of the menu items.
     *
     * @param baseID the base ID
     */
    void setMenuItemDebugIds(String baseID) {
        int itemCount = 0;
        for (MenuItem item : items) {
            item.ensureDebugId(baseID + "-item" + itemCount);
            itemCount++;
        }
    }

    /**
     * Physically add the td element of a {@link MenuItem} or
     * {@link MenuItemSeparator} to this {@link MenuBar}.
     *
     * @param beforeIndex the index where the seperator should be inserted
     * @param elem the td element to be added
     */
    private void addItemElement(int beforeIndex, Element elem) {
        Element div = DOM.createDiv();
        DOM.insertChild(body, div, beforeIndex);
        DOM.appendChild(div, elem);
    }

    /**
     * Closes this menu (if it is a popup).
     */
    private void close() {
    }

    public MenuItem addItem(MenuItem item) {
        return insertItem(item, items.size());
    }

    private void eatEvent(Event event) {
        DOM.eventCancelBubble(event, true);
        DOM.eventPreventDefault(event);
    }

    private MenuItem findItem(Element hItem) {
        for (MenuItem item : items) {
            if (DOM.isOrHasChild(item.getElement(), hItem)) {
                return item;
            }
        }
        return null;
    }

    private Element getItemContainerElement() {
        return body;
    }

    private void moveToNextItem() {
        if (selectFirstItemIfNoneSelected()) {
            return;
        }


        selectNextItem();

    }

    private void moveToPrevItem() {
        if (selectFirstItemIfNoneSelected()) {
            return;
        }
    }

    /*
     * This method is called when a menu bar is hidden, so that it can hide any
     * child popups that are currently being shown.
     */
    private void onHide(boolean focus) {

        popup.hide();
        if (focus) {
            focus();
        }

    }

    /**
     * Adds a menu item to the bar at a specific index.
     *
     * @param item the item to be inserted
     * @param beforeIndex the index where the item should be inserted
     * @return the {@link MenuItem} object
     * @throws IndexOutOfBoundsException if <code>beforeIndex</code> is out of
     *           range
     */
    public MenuItem insertItem(MenuItem item, int beforeIndex)
            throws IndexOutOfBoundsException {
        // Check the bounds
        if (beforeIndex < 0 || beforeIndex > items.size()) {
            throw new IndexOutOfBoundsException();
        }

        // Add to the list of items
        items.add(beforeIndex, item);

        // Setup the menu item
        addItemElement(beforeIndex, item.getElement());
        item.setParentMenu(this);
        item.setSelectionStyle(false);
        return item;
    }

    /**
     * Moves the menu selection down to the next item. If there is no selection,
     * selects the first item. If there are no items at all, does nothing.
     */
    public void moveSelectionDown() {
        if (selectFirstItemIfNoneSelected()) {
            return;
        }
        selectNextItem();
    }

    /**
     * Moves the menu selection up to the previous item. If there is no selection,
     * selects the first item. If there are no items at all, does nothing.
     */
    public void moveSelectionUp() {
        if (selectFirstItemIfNoneSelected()) {
            return;
        }
        selectPrevItem();
    }

    protected List<MenuItem> getItems() {
        return this.items;
    }

    @Override
    public void onBrowserEvent(Event event) {
        MenuItem item = findItem(DOM.eventGetTarget(event));
        switch (DOM.eventGetType(event)) {
            case Event.ONCLICK: {
                getElement().focus();
                // Fire an item's command when the user clicks on it.
                if (item != null) {
                    doItemAction(item, true, true);
                }
                break;
            }

            case Event.ONMOUSEOVER: {
                if (item != null) {
                    itemOver(item, true);
                }
                break;
            }

            case Event.ONMOUSEOUT: {
                if (item != null) {
                    itemOver(null, true);
                }
                break;
            }

            case Event.ONFOCUS: {
                selectFirstItemIfNoneSelected();
                break;
            }

            case Event.ONKEYDOWN: {
                int keyCode = DOM.eventGetKeyCode(event);
                switch (keyCode) {
                    case KeyCodes.KEY_LEFT:
                        if (LocaleInfo.getCurrentLocale().isRTL()) {
                            moveToNextItem();
                        } else {
                            moveToPrevItem();
                        }
                        eatEvent(event);
                        break;
                    case KeyCodes.KEY_RIGHT:
                        if (LocaleInfo.getCurrentLocale().isRTL()) {
                            moveToPrevItem();
                        } else {
                            moveToNextItem();
                        }
                        eatEvent(event);
                        break;
                    case KeyCodes.KEY_UP:
                        moveSelectionUp();
                        eatEvent(event);
                        break;
                    case KeyCodes.KEY_DOWN:
                        moveSelectionDown();
                        eatEvent(event);
                        break;
                    case KeyCodes.KEY_ESCAPE:
                        closeAllParents();
                        // Ensure the popup is closed even if it has not been enetered
                        // with the mouse or key navigation
                        if (popup != null) {
                            popup.hide();
                        }
                        eatEvent(event);
                        break;
                    case KeyCodes.KEY_ENTER:
                        if (!selectFirstItemIfNoneSelected()) {
                            doItemAction(selectedItem, true, true);
                            eatEvent(event);
                        }
                        break;
                } // end switch(keyCode)

                break;
            } // end case Event.ONKEYDOWN
        } // end switch (DOM.eventGetType(event))
        super.onBrowserEvent(event);
    }

    /**
     * Closes the menu bar.
     *
     * @deprecated Use {@link #addCloseHandler(CloseHandler)} instead
     */
    @Deprecated
    public void onPopupClosed(PopupPanel sender, boolean autoClosed) {
    }

    /**
     * Removes the specified menu item from the bar.
     *
     * @param item the item to be removed
     */
    public void removeItem(MenuItem item) {
        // Unselect if the item is currently selected
        if (selectedItem == item) {
            selectItem(null);
        }

        if (removeItemElement(item)) {
            items.remove(item);
            item.setParentMenu(null);
        }
    }

    private boolean removeItemElement(UIObject item) {
        int idx = items.indexOf(item);
        if (idx == -1) {
            return false;
        }

        Element container = getItemContainerElement();
        DOM.removeChild(container, DOM.getChild(container, idx));
        items.remove(idx);
        return true;
    }

    @Override
    public HandlerRegistration addCloseHandler(CloseHandler<PopupPanel> handler) {
        return addHandler(handler, CloseEvent.getType());
    }

    @Override
    public void onClose(CloseEvent<PopupPanel> event) {
        // When the menu popup closes, remember that no item is
        // currently showing a popup menu.
        onHide(!event.isAutoClosed());
        CloseEvent.fire(MenuBar.this, event.getTarget());
        popup = null;
    }
}
