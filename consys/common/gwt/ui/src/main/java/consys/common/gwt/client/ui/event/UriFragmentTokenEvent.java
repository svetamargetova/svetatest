package consys.common.gwt.client.ui.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 * Event vystreleny jedinou entitou(LayouManager), ktera zachytava fragmenty uri
 * (#XXXX). Ostatni entity je zachytavaji a v priapde ze maji obsaluhu na dany
 * fragment tak nastavi promenou v eventu, kterou LayoutManager testuje a v pri-
 * pade ze sa nenasla jina entita tak vystrli event UknownUriFragmentTokenEvent.
 * @author pepa
 */
public class UriFragmentTokenEvent extends GwtEvent<UriFragmentTokenEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private String fragment;
    private boolean handled;

    public UriFragmentTokenEvent(String fragment) {
        this.fragment = fragment;
        handled = false;
    }

    public boolean isHandled() {
        return handled;
    }

    public void setHandled(boolean handled) {
        this.handled = handled;
    }

    public String getFragment() {
        return fragment;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onCatchedFragment(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onCatchedFragment(UriFragmentTokenEvent event);
    }
}
