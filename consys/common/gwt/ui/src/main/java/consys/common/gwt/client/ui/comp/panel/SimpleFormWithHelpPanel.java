package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Zakladni panel s napovedou
 *
 *   ________________
 *  |______HEAD______| \
 *  |_HIDDABLE LABLE_| |
 *  |______BODY______|  \ Panel
 *  |_CONTENT_|_HELP_|  /
 *  |_____/BODY______| |
 *  |______FOOT______| /
 *
 *
 *
 *
 * @author pepa
 */
public class SimpleFormWithHelpPanel extends SimpleFormPanel {

    // konstanty
    public static final int HELP_WIDTH_INT = 200;
    public static final String HELP_WIDTH = HELP_WIDTH_INT + "px";
    public static final int CONTENT_WIDTH_INT = LayoutManager.LAYOUT_CONTENT_WIDTH_INT - HELP_WIDTH_INT - 10;
    public static final String CONTENT_WIDTH = CONTENT_WIDTH_INT + "px";
    // data
    private FlowPanel contentPanel;
    private FlowPanel helpPanel;

    public SimpleFormWithHelpPanel() {
        this(false);
    }

    public SimpleFormWithHelpPanel(boolean withHiddableLabel) {
        super(withHiddableLabel);
    }

    @Override
    public void clear() {
        contentPanel.clear();
    }

    @Override
    protected Widget createContentPart() {
        contentPanel = new FlowPanel();
        contentPanel.setWidth(CONTENT_WIDTH);
        contentPanel.addStyleName(MARGIN_RIGHT_10);
        contentPanel.addStyleName(FLOAT_LEFT);
        contentPanel.add(getMessagePart());

        helpPanel = new FlowPanel();
        helpPanel.setStyleName(ResourceUtils.system().css().helpPanel());
        helpPanel.setWidth(HELP_WIDTH);

        FlowPanel panel = new FlowPanel();
        panel.add(contentPanel);
        panel.add(helpPanel);
        return panel;
    }

    public void hideHelp() {
        if (helpPanel.isVisible()) {
            contentPanel.setWidth("100%");
            helpPanel.setVisible(false);
            helpPanel.setWidth("0px");
        }
    }

    public void showHelp() {
        if (!helpPanel.isVisible()) {
            helpPanel.setVisible(true);
            helpPanel.setWidth(HELP_WIDTH);
            contentPanel.setWidth(CONTENT_WIDTH);
        }
    }

    @Override
    public void setWidget(Widget widget) {
        contentPanel.clear();
        contentPanel.add(getMessagePart());
        contentPanel.add(widget);
    }

    @Override
    public void addWidget(Widget widget) {
        contentPanel.add(widget);
    }

    public void setHelpWidget(Widget widget) {
        helpPanel.add(widget);
    }

    public void addHelpTitle(String text) {
        Label label = new Label(text);
        label.setStyleName(ResourceUtils.system().css().helpTitle());
        helpPanel.add(label);
    }

    public void addHelpText(String text) {
        HTML html = new HTML(text);
        html.setStyleName(ResourceUtils.system().css().helpItem());
        helpPanel.add(html);
    }

    public void addHelpSeparator() {
        helpPanel.add(new Separator());
    }
}
