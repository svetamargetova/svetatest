package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 * Automaticky validovatelny TextBox pro int
 * @author pepa
 */
public class ConsysIntTextBox extends TextBox implements ValidableComponent, HasIdentifier {

    // konstanty
    public static final String NO_NUMBER_REG_EXP = "[^0-9]";
    // data
    private int min;
    private int max;
    private String fieldName;

    public ConsysIntTextBox(int min, int max) {
        this(null, min, max, "");
    }

    public ConsysIntTextBox(int min, int max, String fieldName) {
        this(null, min, max, fieldName);
    }

    public ConsysIntTextBox(String width, final int min, final int max, String fieldName) {
        this.min = min;
        this.max = max;
        this.fieldName = fieldName;

        setStyleName(StyleUtils.BORDER);

        if (width == null) {
            addStyleName(StyleUtils.WIDTH_270);
        } else {
            setWidth(width);
        }

        addStyleName(StyleUtils.HEIGHT_21);
        addStyleName(ResourceUtils.system().css().inputPadd());

        addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                // kontrola na spravnost povolenych znaku
                String text = getText();
                if (!text.matches("[\\-]?[0-9]+")) {
                    if (text.startsWith("-")) {
                        text = "-" + text.substring(1).replaceAll(NO_NUMBER_REG_EXP, "");
                    } else {
                        text = getText().replaceAll(NO_NUMBER_REG_EXP, "");
                    }
                    setText(text);
                }
                // kontrola na rozsah
                try {
                    int number = Integer.parseInt(text);
                    if (number > max) {
                        setText(max);
                    }
                } catch (NumberFormatException ex) {
                    // bylo zatim zadano jen minusko a nelze prevest na cislo, nic nedelam
                }
            }
        });
        addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (getText().equals("-")) {
                    setText("");
                }
            }
        });
    }

    /** nastavi int jako text */
    public void setText(int value) {
        setText(Integer.toString(value));
    }

    /** vraci hodnotu textu jako int */
    public int getTextInt() {
        return Integer.parseInt(getText().trim());
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        if (!isEnabled()) {
            return true;
        }
        if (!ValidatorUtils.isValidString(getText().trim())) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(fieldName));
            return false;
        }
        try {
            int value = getTextInt();
            if (!ValidatorUtils.isRange(value, min, max)) {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
                return false;
            }
        } catch (NumberFormatException ex) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustInteger(fieldName));
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        addStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public String getIdentifier() {
        return fieldName;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.fieldName = identifier;
    }
}
