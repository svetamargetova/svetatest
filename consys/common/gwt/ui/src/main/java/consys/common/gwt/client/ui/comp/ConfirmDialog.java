package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Potvrzovaci dialog pred nejakou akci
 * @author pepa
 */
public class ConfirmDialog extends Dialog {

    // konstanty
    private static final int WIDTH = 400;
    // komponenty
    private ActionImage actionButton;
    private ActionLabel cancelButton;
    // data
    private ConsysAction action;
    private String title;

    /**
     * Vytvori YES/NO dialog s vlozenou titulkou
     * @param title
     */
    public ConfirmDialog(String title) {
        super(WIDTH);
        this.title = title;

        actionButton = ActionImage.getConfirmButton(UIMessageUtils.c.const_yes());
        initCancelButton(true);
        initConfirmButton();
    }

    public ConfirmDialog(String actionButtonName, String title) {
        super(WIDTH);
        this.title = title;

        actionButton = ActionImage.getConfirmButton(actionButtonName);
        initCancelButton(false);
        initConfirmButton();
    }

    public ConfirmDialog(ActionImage actionButton, String title) {
        super(WIDTH);
        this.title = title;

        this.actionButton = actionButton;
        initCancelButton(false);
        initConfirmButton();
    }

    private void initCancelButton(boolean noButton) {
        cancelButton = new ActionLabel(noButton ? UIMessageUtils.c.const_no() : UIMessageUtils.c.const_cancel(),
                StyleUtils.MARGIN_TOP_3 + " " + StyleUtils.FLOAT_LEFT);
    }

    private void initConfirmButton() {
        this.getActionButton().addStyleName(StyleUtils.FLOAT_LEFT);
        this.getActionButton().addStyleName(StyleUtils.MARGIN_HOR_20);
    }

    @Override
    protected Widget createContent() {
        SimplePanel sp1 = new SimplePanel();
        sp1.setSize("10px", "10px");
        SimplePanel sp2 = new SimplePanel();
        sp2.setSize("10px", "10px");
        SimplePanel sp3 = new SimplePanel();
        sp3.setSize("10px", "10px");
        FlowPanel titlePanel = new FlowPanel();
        titlePanel.add(sp1);
        titlePanel.add(StyleUtils.getStyledLabel(title, StyleUtils.FONT_16PX, StyleUtils.FONT_BOLD, StyleUtils.MARGIN_LEFT_20));
        titlePanel.add(sp2);

        getActionButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                action.run();
            }
        });

        getCancelButton().setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(getActionButton());
        controlPanel.add(getCancelButton());
        controlPanel.add(StyleUtils.clearDiv());

        FlowPanel panel = new FlowPanel();
        panel.add(titlePanel);
        panel.add(controlPanel);
        panel.add(sp3);
        return panel;
    }

    /** nastavi akci potvrzovaciho tlacitka */
    public void setAction(ConsysAction action) {
        this.action = action;
    }

    /**
     * @return the actionButton
     */
    public ActionImage getActionButton() {
        return actionButton;
    }

    /**
     * @return the cancelButton
     */
    public ActionLabel getCancelButton() {
        return cancelButton;
    }
}
