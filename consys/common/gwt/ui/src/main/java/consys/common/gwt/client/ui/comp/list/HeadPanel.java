package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.ui.Composite;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstraktni predek vsech HeadPanelu pro DataListy
 * @author Palo
 */
public abstract class HeadPanel extends Composite {

    List<ListFilter> filterList;
    List<ListOrderer> ordererList;

    public HeadPanel() {
        filterList = new ArrayList<ListFilter>();
        ordererList = new ArrayList<ListOrderer>();
    }

    protected void registerFilter(ListFilter filter) {
        filterList.add(filter);
    }

    protected void registerOrderer(ListOrderer orderer) {
        ordererList.add(orderer);
    }

    protected void clearFilter() {
        filterList.clear();
    }

    protected void clearOrderer() {
        ordererList.clear();
    }
}
