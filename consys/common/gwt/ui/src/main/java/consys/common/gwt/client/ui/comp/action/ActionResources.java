package consys.common.gwt.client.ui.comp.action;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

/**
 *
 * @author pepa
 */
public interface ActionResources extends ClientBundle {

    public static final ActionResources INSTANCE = GWT.create(ActionResources.class);

    @Source("action.css")
    public ActionCss css();

    // ActionImageBig
    @Source("img/acceptButtonLeft.png")
    public ImageResource actionImageBigGreenLeft();

    @Source("img/acceptButtonCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBigGreenCenter();

    @Source("img/acceptButtonRight.png")
    public ImageResource actionImageBigGreenRight();

    @Source("img/declineButtonLeft.png")
    public ImageResource actionImageBigRedLeft();

    @Source("img/declineButtonCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBigRedCenter();

    @Source("img/declineButtonRight.png")
    public ImageResource actionImageBigRedRight();

    @Source("img/actionImageBigLeft.png")
    public ImageResource actionImageBigLeft();

    @Source("img/actionImageBigCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBigCenter();

    @Source("img/actionImageBigRight.png")
    public ImageResource actionImageBigRight();

    @Source("img/clearButtonLeft.png")
    public ImageResource actionImageBigClearLeft();

    @Source("img/clearButtonCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBigClearCenter();

    @Source("img/clearButtonRight.png")
    public ImageResource actionImageBigClearRight();

    @Source("img/disabledButtonLeft.png")
    public ImageResource actionImageBigDisabledLeft();

    @Source("img/disabledButtonCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageBigDisabledCenter();

    @Source("img/disabledButtonRight.png")
    public ImageResource actionImageBigDisabledRight();

    // ActionImage
    @Source("img/actionImageLeft.png")
    public ImageResource actionImageLeft();

    @Source("img/actionImageCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageCenter();

    @Source("img/actionImageRight.png")
    public ImageResource actionImageRight();

    @Source("img/actionImageDisabledLeft.png")
    public ImageResource actionImageDisabledLeft();

    @Source("img/actionImageDisabledCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageDisabledCenter();

    @Source("img/actionImageDisabledRight.png")
    public ImageResource actionImageDisabledRight();

    @Source("img/actionImageRedLeft.png")
    public ImageResource actionImageRedLeft();

    @Source("img/actionImageRedCenter.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource actionImageRedCenter();

    @Source("img/actionImageRedRight.png")
    public ImageResource actionImageRedRight();

    // ikony
    @Source("img/icoArrowRight.png")
    public ImageResource iconConfirm();

    // ikony velke
    @Source("img/icoBigArrowRight.png")
    public ImageResource iconBigConfirm();
}
