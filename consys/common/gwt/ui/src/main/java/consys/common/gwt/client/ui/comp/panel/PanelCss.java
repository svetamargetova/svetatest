package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 *
 * @author palo
 */
@ImportedWithPrefix("panel")
public interface PanelCss extends CssResource {

    String headlinePanel();

    String help();

    String loginPanel();

    String button();

    String facebook();

    String facebookOff();

    String facebookOver();

    String google_plus();

    String google_plusOff();

    String google_plusOver();

    String linkedin();

    String linkedinOff();

    String linkedinOver();

    String twitter();

    String twitterOff();

    String twitterOver();

    String ssoButtons();

    String ssoButtonsWrapper();

    String infoPanel();

    String infoPanelLabel();

    String action();
}
