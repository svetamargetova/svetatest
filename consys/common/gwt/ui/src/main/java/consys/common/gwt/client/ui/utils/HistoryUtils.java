package consys.common.gwt.client.ui.utils;

import com.google.gwt.user.client.History;
import java.util.HashMap;

/**
 * Usnadneni prace s History
 * @author pepa
 */
public class HistoryUtils {

    /** @return true pokud obsahuje history token "?" a je za nim aspon jeden znak */
    public static boolean hasParameters() {
        String token = History.getToken();
        int index = token.indexOf("?");
        return (index != -1 && index < token.length() - 1);
    }

    /**
     * Najde parametr a vrati jeho hodnotu, pouzivat pouze pro ziskani jedne hodnoty!
     * Jinak pouzivat getParameters a vytahnout si polozky z hash mapy.
     * @param param nazev parametru
     * @return String pokud je parametr nalezen<br><br>
     * @return prazdny String pokud je parametr nalezen, ale nema prirazenou hodnotu<br><br>
     * @return null pokud parametr neexistuje
     */
    public static String getParameter(String param) {
        return getParameters().get(param);
    }

    /**
     * Rozparsuje parametry do hash mapy a tu vrati.
     * Pokud parametr existuje, je v mape (pokud ma hodnotu je ulozena hodnota, pokud ne tak prazdny String)
     * @return HashMap s nazvy parametru jako klici
     */
    public static HashMap<String, String> getParameters() {
        HashMap<String, String> map = new HashMap<String, String>();
        if (hasParameters()) {
            String token = History.getToken();
            token = token.substring(token.indexOf("?") + 1);
            String[] params = token.split("&");
            for (int i = 0; i < params.length; i++) {
                String[] values = params[i].split("=");
                map.put(values[0], values.length > 1 ? values[1] : "");
            }
        }
        return map;
    }
}
