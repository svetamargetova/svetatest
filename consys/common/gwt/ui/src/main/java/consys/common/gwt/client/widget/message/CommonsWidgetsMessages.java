package consys.common.gwt.client.widget.message;

import com.google.gwt.i18n.client.Messages;

/**
 *
 * @author pepa
 */
public interface CommonsWidgetsMessages extends Messages {

    String loginContent_error_ssoFailed(String service);
    
    String loginContent_error_ssoDenied(String service);
}
