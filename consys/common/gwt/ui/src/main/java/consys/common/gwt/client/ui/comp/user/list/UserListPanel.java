package consys.common.gwt.client.ui.comp.user.list;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.panel.ListPanel;
import consys.common.gwt.client.ui.comp.user.UserResources;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Uzivatelsky seznam s fotkama a afiliacema.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserListPanel extends ListPanel<UserListItem> {

    private static final int VISIBLE_ITEMS = 5;
    

    public UserListPanel() {
        super(UserListItem.HEIGHT, VISIBLE_ITEMS);
        setWidth("350px");
        UserResources.INSTANCE.userCss().ensureInjected();
    }


    public void setUsers(List<String> usersUuids) {
        // TODO load current event
        if (!(usersUuids == null || usersUuids.isEmpty())) {
            ArrayList<String> list = new ArrayList<String>(usersUuids);
            EventBus.get().fireEvent(new DispatchEvent(new ListUserAffiliationThumbsAction(list, ""), new AsyncCallback<ArrayListResult<ClientUserWithAffiliationThumb>>() {

                @Override
                public void onFailure(Throwable caught) {
                }

                @Override
                public void onSuccess(ArrayListResult<ClientUserWithAffiliationThumb> result) {
                    for (ClientUserWithAffiliationThumb cult : result.getArrayListResult()) {
                        addItem(new UserListItem(cult));
                    }
                }
            }, this));
        }
    }

    
}
