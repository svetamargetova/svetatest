package consys.common.gwt.client.ui.comp.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormPart;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBaseWrapperWithBorderFace;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Panel s hornim menu
 * @author pepa
 */
public class MenuFormPanel extends RootPanel {

    // konstanty
    /** musi odpovidat stylu nastavovanemu partu */
    private static final int SPACE = 10;
    private static final int DEFAULT_COLS = 4;
    // komponenty
    private FlexTable topPanel;
    private SimplePanel content;
    private MenuFormPart selectPart;
    // data
    private int itemCount = 0;
    private final int cols;
    private final int widthInt;
    private final String width;

    public MenuFormPanel() {
        this(null, DEFAULT_COLS);
    }

    public MenuFormPanel(int cols) {
        this(null, cols);
    }

    public MenuFormPanel(String title) {
        this(title, DEFAULT_COLS);
    }

    public MenuFormPanel(String title, int cols) {
        this(title, cols, LayoutManager.LAYOUT_CONTENT_WIDTH_INT);
    }

    public MenuFormPanel(String title, int cols, int width) {
        super(title);
        this.cols = cols;
        this.widthInt = width - 12;
        this.width = widthInt + "px";

        topPanel = new FlexTable();
        ConsysBaseWrapperWithBorderFace wrapper = new ConsysBaseWrapperWithBorderFace(topPanel);
        wrapper.setWidth(this.width);
        wrapper.setStyleName(StyleUtils.MARGIN_BOT_20);
        addWidget(wrapper);

        content = new SimplePanel();
        content.setWidth("100%");
        addWidget(content);
    }

    /** spusti praci panelu a nacte vybrany panel */
    public void initWidget() {
        if (selectPart != null) {
            selectPart.getItem().onShow(content);
        }
    }

    @Override
    public void setWidget(Widget widget) {
        content.setWidget(widget);
        selectPart.select(false);
        selectPart = null;
    }

    /** prida polozku do menu */
    public void addItem(MenuFormItem item) {
        addItem(item, true);
    }

    /**
     * prida polozku do menu
     * @param enabled urcuje zda bude polozka aktivovana nebo deaktivovana
     */
    public void addItem(MenuFormItem item, boolean enabled) {
        int widthWithNoSpace = widthInt - cols * SPACE;
        int itemWidth = widthWithNoSpace / cols;

        MenuFormPart part = new MenuFormPart(item);
        part.setClickHandler(selectClickHandler(part));
        part.setEnabled(enabled);

        int row = itemCount / cols;
        int col = itemCount % cols;

        if (itemCount == 0) {
            part.select(true);
            selectPart = part;
        }
        if (itemCount < cols - 1) {
            part.setWidth(itemWidth);
            part.setStyleName(MARGIN_RIGHT_10);
        } else {
            // "korekce chyby pri deleni"
            part.setWidth(itemWidth + (widthInt % cols));
        }
        if (itemCount >= cols) {
            part.addStyleName(MARGIN_TOP_5);
        }

        topPanel.setWidget(row, col, part);
        itemCount++;
    }

    /** aktivuje / dekativuje polozku v menu podle jejiho nazvu */
    public void setEnabled(String name, boolean value) {
        for (int i = 0; i < topPanel.getRowCount(); i++) {
            for (int j = 0; j < cols; j++) {
                Widget widget = topPanel.getWidget(i, j);
                if (widget != null) {
                    MenuFormPart part = (MenuFormPart) widget;
                    if (part.getItem().getName().equals(name)) {
                        part.setEnabled(value);
                        return;
                    }
                }
            }
        }
    }

    /** prepnuti polozky v menu podle jmena */
    public void showItem(String name) {
        for (int i = 0; i < topPanel.getRowCount(); i++) {
            for (int j = 0; j < cols; j++) {
                Widget widget = topPanel.getWidget(i, j);
                if (widget != null) {
                    MenuFormPart part = (MenuFormPart) widget;
                    if (part.getItem().getName().equals(name)) {
                        showItem(part);
                        return;
                    }
                }
            }
        }
    }

    /** interni prepnuti polozek */
    private void showItem(MenuFormPart part) {
        if (part != selectPart) {
            part.select(true);
            if (selectPart != null) {
                selectPart.select(false);
            }
            selectPart = part;
            part.getItem().onShow(content);
        }
    }

    /** aktualizuje popisky v tlacitkach menu */
    public void refreshMenu() {
        for (int i = 0; i < topPanel.getRowCount(); i++) {
            for (int j = 0; j < cols; j++) {
                Widget widget = topPanel.getWidget(i, j);
                if (widget != null) {
                    MenuFormPart part = (MenuFormPart) widget;
                    part.refresh();
                }
            }
        }
    }

    /** vraci ClickHandler pro vybirani polozek v menu */
    private ClickHandler selectClickHandler(final MenuFormPart part) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showItem(part);
            }
        };
    }
}
