package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.ui.PasswordTextBox;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.prop.HasIdentifier;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;

/**
 * Automaticky validovatelny PasswordTextBox
 * @author pepa
 */
public class ConsysPasswordTextBox extends PasswordTextBox implements ValidableComponent, HasIdentifier {

    protected int min;
    protected int max;
    protected String fieldName;

    public ConsysPasswordTextBox(int min, int max) {
        this(null, min, max, "");
    }

    public ConsysPasswordTextBox(int min, int max, String fieldName) {
        this(null, min, max, fieldName);
    }

    public ConsysPasswordTextBox(String width, int min, int max, String fieldName) {
        this.min = min;
        this.max = max;
        this.fieldName = fieldName;

        setStyleName(StyleUtils.BORDER);

        if (width == null) {
            addStyleName(StyleUtils.WIDTH_270);
        } else {
            setWidth(width);
        }

        addStyleName(StyleUtils.HEIGHT_21);
        addStyleName(ResourceUtils.system().css().inputPadd());
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        int result = ValidatorUtils.isLenght(getText().trim(), min, max);
        if (result != 0) {
            if (!ValidatorUtils.isValidString(getText().trim())) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(fieldName));
            } else {
                fail.addOrSetText(UIMessageUtils.m.const_notInRange(fieldName));
            }
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        removeStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public void onFail() {
        addStyleName(StyleUtils.BACKGROUND_GRAY);
    }

    @Override
    public String getIdentifier() {
        return fieldName;
    }

    @Override
    public void setIdentifier(String identifier) {
        this.fieldName = identifier;
    }
}

