package consys.common.gwt.client.ui.comp.add;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.CreateDialog;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 * CreateDialog na pridavani polozek
 * @author pepa
 */
public abstract class NewItemDialog<T extends NewItemUI> extends CreateDialog {

    private SimpleFormPanel panel;
    private FlowPanel conent;
    private ConsysMessage helpMessage;
    private T firstItem;

    public NewItemDialog(String titleName, ConsysMessage helpMessage) {
        super(titleName, true);
        this.helpMessage = helpMessage;
    }

    @Override
    protected void onAttach() {
        super.onAttach();
        firstItem.focus();
    }

    /** formular zobrazeny v dialogu */
    @Override
    protected Widget createForm() {
        panel = new SimpleFormPanel("550px");
        conent = new FlowPanel();
        firstItem = newItem();
        conent.add(firstItem);
        panel.addWidget(conent);

        ActionLabel addMore = new ActionLabel(UIMessageUtils.c.const_addMore());
        addMore.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                T nextItem = newItem();
                conent.add(nextItem);
                nextItem.focus();
            }
        });

        addControll(addMore);

        return panel;
    }

    /** vytvori novou polozku */
    public abstract T newItem();

    /** provede akci s daty zadanymi v dialogu */
    public abstract void doCreateAction(ArrayList<T> uis, ConsysMessage fail);

    /** vola se z callbacku po uspesnem vytvoreni dat na serveru a v ui */
    public void successCreateAction() {
        helpMessage.setVisible(false);
        hide();
    }

    @Override
    protected void onSave() {
        ArrayList<T> uis = new ArrayList<T>();
        for (int i = 0; i < conent.getWidgetCount(); i++) {
            uis.add((T) conent.getWidget(i));
        }

        doCreateAction(uis, getFailMessage());
    }

    @Override
    protected void onCancel() {
    }

    @Override
    public ConsysMessage getFailMessage() {
        ConsysMessage fail = getFormPanel().getFailMessage();
        fail.setVerticalSpace("5px");
        fail.setWrapperWidth("500px");
        fail.addStyleName(StyleUtils.MARGIN_CENTER);
        DOM.setStyleAttribute(fail.getParent().getElement(), "textAlign", "center");
        return fail;
    }
}
