package consys.common.gwt.client.ui.utils;

import com.google.gwt.user.client.Window.Location;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;

/**
 *
 * @author pepa
 */
public final class CorporateUtils {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(CorporateUtils.class);
    // konstanty
    public static final String DEFAULT_TITLE_IMAGE_URL = "img/logoTakeplace.gif";
    public static final String DEFAULT_START_TITLE_IMAGE_URL = "img/logoTakeplaceGray.gif";

    private CorporateUtils() {
    }

    /** vraci korporacni prefix precteny z adresy */
    public static String corporationPrefix() {
        String host = Location.getHost();
        String[] split = host.split("\\.");

        if (split.length < 3 || host.startsWith("hekuba.") || host.startsWith("app.") || split[0].equals("127")) {
            logger.debug("corporationPrefix: <none>");
            return "";
        }

        logger.debug("corporationPrefix: " + split[0]);
        return split[0];
    }

    /** vraci adresu korporatniho loga */
    public static String systemTitleImageUrl(String prefix) {
        String host = Location.getHost();

        if (prefix.isEmpty() || host.startsWith("hekuba.") || host.startsWith("app.")) {
            logger.debug("systemTitleImageUrl: " + DEFAULT_TITLE_IMAGE_URL);
            return DEFAULT_TITLE_IMAGE_URL;
        }

        final String url = Location.getProtocol() + "//" + host + "/corporate?title=" + prefix;
        logger.debug("systemTitleImageUrl: " + url);
        return url;
    }
}
