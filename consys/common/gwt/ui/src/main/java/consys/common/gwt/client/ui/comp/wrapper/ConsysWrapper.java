package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant;
import com.google.gwt.user.client.ui.HasVerticalAlignment.VerticalAlignmentConstant;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

/**
 * Obalovy objekt, zajistujici zobrazovani zaoblenych rozku
 * @author pepa
 */
public abstract class ConsysWrapper extends Composite {

    // verejne konstanty
    public static final int ALL = 15;
    public static final int BOTTOM = 12;
    public static final int TOP = 3;
    // interni promenne
    private FlexTable wrapper;
    private Image bl;
    private Image br;
    private Image tr;
    private Image tl;
    /** zvolene nastaveni wrapperu */
    private int settings;

    public ConsysWrapper() {
        this(null);
    }

    public ConsysWrapper(Widget widget) {
        this(widget, ALL);
    }

    /**
     * @param settings je cislo nastaveni, zapsane binarne, jednicka znamena zobrazeni obleho rozku
     * (levySpodni, pravySpodni, pravyHorni, levyHorni), napr cislo 15 (binarne 1111) znamena, ze se zobrazi vsechny rozky,
     * cislo 3 (binarne 0011) jen horni rozky
     */
    public ConsysWrapper(Widget widget, int settings) {
        this.settings = settings;
        wrapper = new FlexTable();
        initWrapper(widget);
        initWidget(wrapper);
    }

    /** zinicializuje vzhled wrapperu a umisti do nej obalovany objekt */
    private void initWrapper(Widget widget) {
        wrapper.setCellPadding(0);
        wrapper.setCellSpacing(0);
        wrapper.setBorderWidth(0);

        wrapper.setWidget(1, 1, widget);

        WrapperFace face = getWrappertFace();
        bl = face.getCorners()[0];
        br = face.getCorners()[1];
        tr = face.getCorners()[2];
        tl = face.getCorners()[3];

        FlexCellFormatter f = wrapper.getFlexCellFormatter();
        String size = face.getCornerSize() + "px";
        String style = face.getBackgroundStyle();
        // velikost rozku
        f.setWidth(0, 0, size);
        f.setWidth(2, 0, size);
        f.setWidth(0, 2, size);
        f.setWidth(2, 2, size);
        f.setHeight(0, 0, size);
        f.setHeight(2, 0, size);
        f.setHeight(0, 2, size);
        f.setHeight(2, 2, size);
        // styl pozadi
        if (face.isBorderStyled()) {
            String[] styles = face.getBorderStyles();
            f.setStyleName(0, 1, styles[2] == null ? style : styles[2]);
            f.setStyleName(1, 0, styles[3] == null ? style : styles[3]);
            f.setStyleName(1, 2, styles[1] == null ? style : styles[1]);
            f.setStyleName(2, 1, styles[0] == null ? style : styles[0]);
        } else {
            f.setStyleName(0, 1, style);
            f.setStyleName(1, 0, style);
            f.setStyleName(1, 2, style);
            f.setStyleName(2, 1, style);
        }
        f.setStyleName(1, 1, style);

        fillCorners(style);
    }

    /** vraci vzhled wrapperu */
    public abstract WrapperFace getWrappertFace();

    /** nastavi widget do wraperu */
    @Override
    public void setWidget(Widget toWrap) {
        wrapper.setWidget(1, 1, toWrap);
    }

    /** zarovnani obaleneho objektu */
    public void setAlignment(HorizontalAlignmentConstant horizontal, VerticalAlignmentConstant vertical) {
        wrapper.getFlexCellFormatter().setAlignment(1, 1, horizontal, vertical);
    }

    /** nastaveni velikosti uvnitr wrapperu */
    @Override
    public void setSize(String width, String height) {
        FlexCellFormatter f = wrapper.getFlexCellFormatter();
        f.setWidth(1, 1, width);
        f.setHeight(1, 1, height);
    }

    /** nastaveni sirky uvnitr wrapperu */
    @Override
    public void setWidth(String width) {
        wrapper.getFlexCellFormatter().setWidth(1, 1, width);
    }

    /** vyplni nastavene rozky odpovidajicim obrazkem */
    private void fillCorners(String style) {
        FlexCellFormatter f = wrapper.getFlexCellFormatter();
        if ((settings & 1) == 1) {
            wrapper.setWidget(0, 0, tl);
        } else {
            f.setStyleName(0, 0, style);
        }

        if ((settings & 8) == 8) {
            wrapper.setWidget(2, 0, bl);
        } else {
            f.setStyleName(2, 0, style);
        }

        if ((settings & 2) == 2) {
            wrapper.setWidget(0, 2, tr);
        } else {
            f.setStyleName(0, 2, style);
        }

        if ((settings & 4) == 4) {
            wrapper.setWidget(2, 2, br);
        } else {
            f.setStyleName(2, 2, style);
        }
    }
}
