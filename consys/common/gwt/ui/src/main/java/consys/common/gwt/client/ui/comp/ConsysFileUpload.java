package consys.common.gwt.client.ui.comp;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Uprava a nastylovani FileUpload
 * @author pepa
 */
public class ConsysFileUpload extends Composite {

    // konstanty
    private static final String STYLE_FILE_UPLOAD = "consysFileUpload";
    private static final String STYLE_FILE_UPLOAD_W = "consysFileUploadW";
    // komponenty
    private FlowPanel panel;
    private FileUpload upload;
    private TextBox tb;
    private FlowPanel originalWrapper;
    private ActionImage button;
    // data
    private int width;
    private ConsysActionWithValue<Boolean> emptyHandler;

    public ConsysFileUpload() {
        this(270);
    }

    public ConsysFileUpload(int intWidth) {
        width = intWidth - UIMessageUtils.c.consysFileUpload_button_width();

        upload = new InnerFileUpload();
        upload.getElement().setAttribute("size", "1");
        upload.addStyleName(STYLE_FILE_UPLOAD);
        upload.addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                String[] text = upload.getFilename().split("\\\\");
                String result = text[text.length - 1];
                tb.setText(result);

                if (emptyHandler != null) {
                    emptyHandler.run(result.isEmpty());
                }
            }
        });

        originalWrapper = new FlowPanel();
        originalWrapper.addStyleName(StyleUtils.POSITION_ABS);
        originalWrapper.addStyleName(STYLE_FILE_UPLOAD_W);
        originalWrapper.add(upload);

        tb = StyleUtils.getFormInput();
        tb.removeStyleName(StyleUtils.WIDTH_270);
        tb.addStyleName(StyleUtils.FLOAT_LEFT);
        tb.setWidth(width + "px");
        tb.setText(UIMessageUtils.c.consysFileUpload_text_chooseFileToUpload());

        button = ActionImage.getConfirmButton(UIMessageUtils.c.consysFileUpload_button_browsFile());
        button.addStyleName(StyleUtils.FLOAT_RIGHT);

        FlowPanel fp = new FlowPanel();
        fp.setWidth("100%");
        fp.addStyleName(StyleUtils.POSITION_ABS);
        DOM.setStyleAttribute(fp.getElement(), "left", "0px");
        DOM.setStyleAttribute(fp.getElement(), "top", "0px");
        fp.add(tb);
        fp.add(button);
        fp.add(StyleUtils.clearDiv());

        panel = new FlowPanel();
        panel.setSize(intWidth + "px", "22px");
        panel.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        panel.addStyleName(StyleUtils.POSITION_REL);
        panel.add(originalWrapper);
        panel.add(fp);

        initWidget(panel);
    }

    /** nastavi nazev komponenty pro odeslani ve formulari */
    public void setName(String name) {
        upload.setName(name);
    }

    /** nastavi id uploadovaci komponente */
    public void setId(String id) {
        upload.getElement().setId(id);
    }
    
    /** vraci nazev vybraneho souboru */
    public String getFilename() {
        return upload.getFilename();
    }

    /** prida handler na zmenu */
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return upload.addChangeHandler(handler);
    }

    /** enabluje/disabluje komponentu */
    public void setEnabled(boolean value) {
        if (value) {
            panel.insert(originalWrapper, 0);
        } else {
            panel.remove(originalWrapper);
        }
        tb.setEnabled(value);
        button.setEnabled(value);
    }

    /** vraci sirku textboxu */
    public int getInputWidth() {
        return width;
    }

    /** nastavi handler ktery sleduje zda je nevybrany soubor (vola true) nebo vybrany soubor (vola false) */
    public void setEmptyHandler(ConsysActionWithValue<Boolean> emptyHandler) {
        this.emptyHandler = emptyHandler;
    }

    /** vnitrni komponenta, ktera prijima mouseOver a mouseOut */
    private class InnerFileUpload extends FileUpload {

        public InnerFileUpload() {
            sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);

            addHandler(new MouseOverHandler() {

                @Override
                public void onMouseOver(MouseOverEvent event) {
                    if (button.isEnabled()) {
                        button.mouseOver();
                    }
                }
            }, MouseOverEvent.getType());
            addHandler(new MouseOutHandler() {

                @Override
                public void onMouseOut(MouseOutEvent event) {
                    if (button.isEnabled()) {
                        button.mouseOut();
                    }
                }
            }, MouseOutEvent.getType());
        }
    }
}
