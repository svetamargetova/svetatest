package consys.common.gwt.client.ui.comp;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.SuggestOracle;
import consys.common.gwt.shared.bo.ClientSuggestion;
import java.util.Collections;
import java.util.List;

/**
 * Oracle pro zpozdene odesilani pozadavku na suggest
 * @author pepa
 */
public abstract class ConsysSuggestOracle extends SuggestOracle {

    // konstanty
    private static final int DEFAULT_TIMEOUT = 500;
    private static final int DEFAULT_MINIMAL_LENGTH = 2;
    private static final int DEFAULT_RESULTS_LIMIT = 5;
    private final int timeout;
    private final int minLength;
    // casovac pro spravu odesilani pozadavku
    private Timer timer;
    // ostatni
    private boolean isDisplayAsHTML = false;

    /**
     * Konstruktor vytvori SuggestOracle s vychozim nastavenim, timenout 500 a minimalni delkou vstupu 1
     */
    public ConsysSuggestOracle() {
        this(DEFAULT_TIMEOUT, DEFAULT_MINIMAL_LENGTH);
    }

    /**
     * Konstruktor vytvori SuggestOracle s vychozim nastavenim timenoutu 500
     * @param minLength je minimalni delka vstupu, od ktere bude aktivni suggest
     */
    public ConsysSuggestOracle(int minLength) {
        this(DEFAULT_TIMEOUT, minLength);
    }

    /**
     * Konstruktor vytvori SuggestOracle
     * @param timeout je hodnota, po ktere zacne reakce na vyhledani
     * @param minLength je minimalni delka vstupu, od ktere bude aktivni suggest
     */
    public ConsysSuggestOracle(int timeout, int minLength) {
        super();
        this.timeout = timeout;
        this.minLength = minLength;
    }

    /** nastavi zda se ma zobrazovat suggestion jako html */
    public void setIsDisplayAsHTML(boolean isDisplayAsHTML) {
        this.isDisplayAsHTML = isDisplayAsHTML;
    }

    @Override
    public boolean isDisplayStringHTML() {
        return isDisplayAsHTML;
    }

    /** 
     * Zrusi pozadavek na requestSuggestions
     */
    public void cancel() {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void requestSuggestions(final Request request, final Callback callback) {
        final String query = request.getQuery();

        cancel();

        if (query.length() > minLength) {
            timer = new Timer() {

                @Override
                public void run() {
                    suggest(query, DEFAULT_RESULTS_LIMIT, new SuggestCallback(callback, request));
                }
            };
            timer.schedule(timeout);
        } else {
            Response response = new Response(Collections.<SuggestOracle.Suggestion>emptyList());
            callback.onSuggestionsReady(request, response);
        }
    }

    public static final class SuggestCallback{

        Callback callback;
        Request request;

        public SuggestCallback(Callback callback, Request request) {
            this.callback = callback;
            this.request = request;
        }

        public void reponse(List<? extends ClientSuggestion> response){
            callback.onSuggestionsReady(request, new Response(response));
        }
    }

    /**
     * Metoda prvyuzit zadane parametryovola potrebnou sluzbu, ktera muze  a vrati vysledek
     * @param query zadany retezec
     * @param limit maximalni pocet vracenych vysledku
     */
    public abstract void suggest(String query, int limit, SuggestCallback callback);
}
