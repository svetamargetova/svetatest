package consys.common.gwt.client.ui.base;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.dom.DOMX;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Obsah, ktery se zobrazuje, kdyz neni k pozadovanemu tokenu nalezen obsah
 * @author pepa
 */
public class UnknownTokenContent extends Label {

    private Timer t;

    public UnknownTokenContent() {
        super(UIMessageUtils.c.unknownTokenContent_text());
        DOMX.setOpacity(this.getElement(), 0);
        t = new Timer() {

            @Override
            public void run() {
                (new ShowAnimation()).show();
            }
        };
    }

    @Override
    protected void onLoad() {
        t.schedule(1000);
    }

    @Override
    protected void onUnload() {
        t.cancel();
    }

    /** vnitrni trida pro animovani skryvani labelu */
    private class ShowAnimation extends Animation {

        // konstanty
        private static final int ANIMATION_DURATION = 1350;
        // data

        public void show() {
            cancel();
            run(ANIMATION_DURATION);
        }

        @Override
        protected void onStart() {
            DOMX.setOpacity(UnknownTokenContent.this.getElement(), 0);
        }

        @Override
        protected void onUpdate(double progress) {
            DOMX.setOpacity(UnknownTokenContent.this.getElement(), progress);
        }

        @Override
        protected void onComplete() {
            DOMX.setOpacity(UnknownTokenContent.this.getElement(), 1);
        }
    }
}
