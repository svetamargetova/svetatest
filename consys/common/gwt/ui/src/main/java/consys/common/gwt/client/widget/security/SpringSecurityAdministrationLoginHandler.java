package consys.common.gwt.client.widget.security;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.UrlBuilder;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.client.event.RedirectEvent;

/**
 * LoginHandler that post information to Spring Security to log the user.
 * <br>
 * Use this class in conjunction with <code>GWTAuthenticationProcessingFilter</code> in order to benefit from the 4xx status code returned in case of a failure (wrong credential).
 *
 * @author David MARTIN
 */
public abstract class SpringSecurityAdministrationLoginHandler extends AbstractSpringSecurityLoginHandler {

    private static final String DEFAULT_SPRING_LOGIN_URL_CLIENT = "secure_login";
    private static final String DEFAULT_SPRING_LOGIN_URL_JS = "/takeplace/secure_login";
    private static final String DEFAULT_SPRING_GWT_LOGIN = "&gwt=true";

    @Override
    protected String getContextLoginUrl() {
        if (GWT.isClient()) {
            return GWT.getHostPageBaseURL() + DEFAULT_SPRING_LOGIN_URL_CLIENT;
        } else {
            return GWT.getHostPageBaseURL() + DEFAULT_SPRING_LOGIN_URL_JS;
        }
    }

    @Override
    public String getFullLoginUrl() {
        return super.getFullLoginUrl() + DEFAULT_SPRING_GWT_LOGIN;
    }

    @Override
    public void doOnResponseReceived(final Request request, final Response response) {
        if (response.getStatusCode() == Response.SC_SERVICE_UNAVAILABLE) {
            Window.Location.reload();
            return;
        }
        String redirectedPage = response.getText();
        String redirected = redirectedPage.substring(redirectedPage.indexOf("#") + 1); // +1 #
        String user = getLogin();
        clearBeforeRedirect();
        // pokial je niekde v tom redirecte '?' potom to znamena ze je chyba
        // a pripojime username        
        if (redirected.contains("?")) {
            History.newItem(redirected + "&username=" + user);
        } else {
            if (StringUtils.isNotBlank(getSuccessRedirect())) {
                String redir = getSuccessRedirect();
                UrlBuilder b = Window.Location.createUrlBuilder();

                // jedna sa o celu uri
                if (redir.startsWith("/")) {
                    b.setPath(redir);
                    // jedna sa len o kontext
                } else {
                    String path = Window.Location.getPath();
                    int l = path.lastIndexOf('/');
                    b.setPath(path.substring(0, l+1)+redir);
                }
                RedirectEvent r = new RedirectEvent(b.buildString());
                EventBus.get().fireEvent(r);
            } else {
                History.newItem(redirected);
            }
        }
    }

    

    @Override
    public void doOnErrorReceived(Request request, Throwable exception) {
        clearBeforeRedirect();
        /* TODO: event na error pri prihalsovani */
        //History.newItem(NavigationItemsLoggedOut.get().getLogIn().getHistoryToken() + "?error=true");
    }

    public abstract void clearBeforeRedirect();

    /** Ak chceme pouzit iny redirect ako bol vrateny z spring security */
    public abstract String getSuccessRedirect();
}

