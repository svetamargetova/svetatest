package consys.common.gwt.client.ui.layout.menu;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.event.MenuItemEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class MenuItemUI extends Composite implements MenuItemEvent.Handler {

    // konstanty
    private static final String PANEL_BORDER_WIDTH = "7px";
    private static final String PANEL_BORDER_HEIGHT = "27px";
    private static final String SHADOW_STYLE = "menuShadow";
    // promenne
    private MenuItem menuItem;
    private boolean topMenu;
    private Widget ui;
    private HandlerRegistration topClickReg;
    // data
    private boolean selected;
    private boolean disabled;

    public MenuItemUI(MenuItem menuItem, boolean topMenu) {
        this.menuItem = menuItem;
        this.topMenu = topMenu;

        selected = false;
        disabled = false;

        if (topMenu) {
            FlowPanel panel = new FlowPanel() {

                private boolean addHandlers = true;

                @Override
                protected void onLoad() {
                    sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
                    if (addHandlers) {
                        addHandler(new MouseOverHandler() {

                            @Override
                            public void onMouseOver(MouseOverEvent event) {
                                onOver();
                            }
                        }, MouseOverEvent.getType());
                        addHandler(new MouseOutHandler() {

                            @Override
                            public void onMouseOut(MouseOutEvent event) {
                                onOut();
                            }
                        }, MouseOutEvent.getType());
                        addHandlers = false;
                    }
                }

                @Override
                protected void onUnload() {
                    unsinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
                }
            };
            panel.setStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
            panel.setHeight(PANEL_BORDER_HEIGHT);

            Image leftBorder = new Image(ResourceUtils.system().topMenuSelectLeft());
            leftBorder.setVisible(false);
            SimplePanel leftWrapper = new SimplePanel();
            leftWrapper.setWidget(leftBorder);
            leftWrapper.setSize(PANEL_BORDER_WIDTH, PANEL_BORDER_HEIGHT);
            leftWrapper.addStyleName(StyleUtils.FLOAT_LEFT);

            Image rightBorder = new Image(ResourceUtils.system().topMenuSelectRight());
            rightBorder.setVisible(false);
            SimplePanel rightWrapper = new SimplePanel();
            rightWrapper.setWidget(rightBorder);
            rightWrapper.setSize(PANEL_BORDER_WIDTH, PANEL_BORDER_HEIGHT);
            rightWrapper.addStyleName(StyleUtils.FLOAT_LEFT);

            Label label = StyleUtils.getStyledLabel(menuItem.getTitle(), StyleUtils.FONT_13PX, StyleUtils.FONT_BOLD,
                    StyleUtils.TEXT_WHITE, StyleUtils.MARGIN_HOR_10, StyleUtils.MARGIN_TOP_3, StyleUtils.HAND,
                    StyleUtils.UNSELECTABLE, SHADOW_STYLE, StyleUtils.TEXT_NOWRAP);
            topClickReg = label.addClickHandler(labelClickHandler());
            SimplePanel labelWrapper = new SimplePanel();
            labelWrapper.setWidget(label);
            labelWrapper.setStyleName(ResourceUtils.system().css().topMenuUnselectedCenter());
            labelWrapper.addStyleName(StyleUtils.FLOAT_LEFT);

            panel.add(leftWrapper);
            panel.add(labelWrapper);
            panel.add(rightWrapper);
            panel.add(StyleUtils.clearDiv());

            ui = panel;
        } else {
            ActionLabel label = new ActionLabel(true, menuItem.getTitle());
            label.setStyleName(StyleUtils.TEXT_BLUE);
            label.addStyleName(StyleUtils.FONT_11PX);
            label.addStyleName(StyleUtils.UNSELECTABLE);
            label.setClickHandler(labelClickHandler());

            ui = label;
            ui.addStyleName(StyleUtils.FLOAT_LEFT);
        }

        initWidget(ui);
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(MenuItemEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(MenuItemEvent.TYPE, this);
    }

    /** provede upravy itemu za vybrany */
    private void select() {
        selected = true;
        if (topMenu) {
            showSelected();
            // nastaveni formulare do obsahu podle polozky menu
            // vola se jen u horniho menu, aby se neprekreslovalo 2x
            ChangeContentEvent changeContentEvent = new ChangeContentEvent(menuItem.getContent(), menuItem.getTitle());
            EventBus.get().fireEvent(changeContentEvent);
        }
    }

    /** upravi vzhled polozky jako vybranou */
    private void showSelected() {
        FlowPanel panel = (FlowPanel) ui;
        panel.setStyleName(StyleUtils.BACKGROUND_WHITE);
        ((SimplePanel) panel.getWidget(0)).getWidget().setVisible(true);
        ((SimplePanel) panel.getWidget(2)).getWidget().setVisible(true);

        SimplePanel sp = (SimplePanel) panel.getWidget(1);
        sp.setStyleName(ResourceUtils.system().css().topMenuSelectedCenter());

        Label label = (Label) sp.getWidget();
        label.setStyleName(StyleUtils.FONT_13PX);
        label.addStyleName(StyleUtils.FONT_BOLD);
        label.addStyleName(StyleUtils.TEXT_DEFAULT);
        label.addStyleName(StyleUtils.MARGIN_HOR_10);
        label.addStyleName(StyleUtils.MARGIN_TOP_5);
        label.addStyleName(StyleUtils.HAND);
        label.addStyleName(StyleUtils.UNSELECTABLE);
        label.addStyleName(StyleUtils.TEXT_NOWRAP);
    }

    /** provede upravy itemu za NEvybrany */
    private void deselect() {
        selected = false;
        if (topMenu) {
            showDeselected();
        }
    }

    /** upravi vzhled polozky jako NEvybranou */
    private void showDeselected() {
        FlowPanel panel = (FlowPanel) ui;
        panel.setStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
        ((SimplePanel) panel.getWidget(0)).getWidget().setVisible(false);
        ((SimplePanel) panel.getWidget(2)).getWidget().setVisible(false);

        SimplePanel sp = (SimplePanel) panel.getWidget(1);
        sp.setStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
        sp.addStyleName(StyleUtils.FLOAT_LEFT);

        Label label = (Label) sp.getWidget();
        label.setStyleName(StyleUtils.FONT_13PX);
        label.addStyleName(StyleUtils.FONT_BOLD);
        label.addStyleName(StyleUtils.TEXT_WHITE);
        label.addStyleName(StyleUtils.MARGIN_HOR_10);
        label.addStyleName(StyleUtils.MARGIN_TOP_3);
        label.addStyleName(StyleUtils.HAND);
        label.addStyleName(SHADOW_STYLE);
        label.addStyleName(StyleUtils.TEXT_NOWRAP);
    }

    /** provede upravy itemu za disablovany */
    private void disable() {
        disabled = true;
        if (topMenu) {
            // musi se stylovat pres DOM, protoze pres set/add style to nechce fungovat
            FlowPanel panel = (FlowPanel) ui;
            SimplePanel sp = (SimplePanel) panel.getWidget(1);
            sp.setStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
            sp.addStyleName(StyleUtils.FLOAT_LEFT);

            Label label = (Label) sp.getWidget();
            DOM.setStyleAttribute(label.getElement(), "color", StyleUtils.VALUE_COLOR_GRAY);
            DOM.setStyleAttribute(label.getElement(), "cursor", StyleUtils.VALUE_DEFAULT);
            topClickReg.removeHandler();
        } else {
            ActionLabel label = (ActionLabel) ui;
            label.disable();
            DOM.setStyleAttribute(label.getElement(), "fontSize", StyleUtils.VALUE_FONT_SIZE_8PT);
        }
    }

    /** zmena po najeti nad polozku */
    private void onOver() {
        if (!disabled && !selected) {
            showSelected();
        }
    }

    /** zmena po odjeti z polozky */
    private void onOut() {
        if (!disabled && !selected) {
            showDeselected();
        }
    }

    /** ClickHandler pro spusteni akci se zmenou menu */
    private ClickHandler labelClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String token = History.getToken();
                if (token.equals(menuItem.getToken())) {
                    History.newItem("refresh-" + menuItem.getToken());
                    Timer t = new Timer() {

                        @Override
                        public void run() {
                            History.newItem(menuItem.getToken());
                        }
                    };
                    t.schedule(300);
                } else {
                    History.newItem(menuItem.getToken());

                }
            }
        };
    }

    @Override
    public void onMenuItem(MenuItemEvent event) {
        switch (event.getAction()) {
            case SELECT:
                // urceni zda je polozka vybrana
                boolean select;
                if (event.getMenuItem().getTitle() != null) {
                    select = event.getMenuItem().getTitle().equals(menuItem.getTitle());
                } else {
                    select = event.getMenuItem().getToken().equals(menuItem.getToken());
                }

                if (select) {
                    select();
                    event.setSuccess(true);
                } else {
                    deselect();
                }
                break;
            case DISABLE:
                if (event.getMenuItem().getTitle().equals(menuItem.getTitle())) {
                    disable();
                }
                break;
        }
    }
}
