package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.wrapper.InputTextWrapper;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 *
 * @author pepa
 */
public class ConsysPasswordBox extends Composite implements Validable {

    // komponenty
    private ConsysFlowPanel mainPanel;
    private TextBox textBox;
    private PasswordTextBox passwordBox;
    private InputTextWrapper wrapper;
    // data
    private int width;
    private boolean enabled = true;
    private boolean required;    
    private ConsysPasswordBox confirm;
    private boolean showPassword;

    public ConsysPasswordBox(int width, boolean required) {
        this.width = width;
        this.required = required;

        mainPanel = new ConsysFlowPanel();
        mainPanel.setStyleName(InputResources.INSTANCE.css().consysPasswordBox());
        initWidget(mainPanel);

        generate();
    }

    private void generate() {
        textBox = prepareComponent(new TextBox());
        passwordBox = prepareComponent(new PasswordTextBox());

        wrapper = new InputTextWrapper(passwordBox, width);
        wrapper.addCenterWrapperStyle(InputResources.INSTANCE.css().wrapper());

        mainPanel.add(wrapper);
        initHandlers();
    }

    private <T extends Widget> T prepareComponent(T widget) {
        widget.setStyleName(InputResources.INSTANCE.css().consysTextBoxInput());
        widget.setWidth((width - (2 * InputTextWrapper.BORDER_WIDTH)) + "px");
        return widget;
    }

    private void initHandlers() {
        passwordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                textBox.setText(passwordBox.getText());
            }
        });
        textBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                passwordBox.setText(textBox.getText());
            }
        });
    }

    public String getPassword() {
        return passwordBox.getText().trim();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        if (this.enabled == enabled) {
            return;
        }
        this.enabled = enabled;
        passwordBox.setEnabled(enabled);
        textBox.setEnabled(enabled);
        refreshWrapper();
    }

    /** zaktualizuje stav wrapperu podle toho jestli je komponenta enablovana nebo disablovana */
    private void refreshWrapper() {
        if (enabled) {
            wrapper.modeNormal();
        } else {
            wrapper.modeDisabled();
        }
    }

    public void clearPassword() {
        passwordBox.setText("");
        textBox.setText("");
    }

    /** @param showPassword pokud je true zobrazuje se heslo, pokud false, zobrazuji se jen tecky/hvezdicky/... */
    public void showPassword(boolean showPassword) {
        this.showPassword = showPassword;
        if (showPassword) {
            wrapper.setInput(textBox);
        } else {
            wrapper.setInput(passwordBox);
        }
        refreshWrapper();
    }

    /** nastavi komponentu opakovaci komponenty */
    public void setConfirmPassword(ConsysPasswordBox confirm) {
        this.confirm = confirm;
    }

    @Override
    public String validate() {
        String result = checkRequired();
        if (result == null) {
            result = checkConfirm();
        }
        return result;
    }

    /** kontrola na to, zda je heslo vyzadovano a jestli je zadano */
    private String checkRequired() {        
        if (required && passwordBox.getText().trim().isEmpty()) {            
            return UIMessageUtils.c.consysPasswordBox_error_passwordNotEntered();
        } 
        return null;
    }

    /** kontrola na na shodnost hesla */
    private String checkConfirm() {
        boolean result = true;
        if (confirm != null && !showPassword) {
            boolean notEmpty = !getPassword().isEmpty();
            if (confirm.isEnabled()) {
                result = notEmpty && getPassword().equals(confirm.getPassword());
            } else {
                result = notEmpty;
            }
        }
        if (!result) {
            return UIMessageUtils.c.consysPasswordBox_error_passwordsNotSame();
        }
        return null;
    }

  
}
