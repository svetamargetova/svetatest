package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.form.FormResources;

/**
 * Wrapper pro validovatelne komponenty, ktery pri chybe pod komponentku zobrazuje cervene chybovou zpravu
 * @author pepa
 */
public class ValidableItem extends Composite implements Validable {

    static{
        FormResources.INSTANCE.formCss().ensureInjected();
    }
    
    // komponenty
    private FlowPanel panel;
    private Validable item;
    private Label errorLabel;

    public ValidableItem(Validable item) {
        this.item = item;
        
        

        errorLabel = new Label();
        errorLabel.setStyleName(FormResources.INSTANCE.formCss().errorLabel());
        errorLabel.setVisible(false);

        panel = new FlowPanel();
        panel.add(item.asWidget());
        panel.add(errorLabel);

        initWidget(panel);
    }

    @Override
    public String validate() {
        String result = item.validate();
        if(result == null){
            errorLabel.setText("");
            errorLabel.setVisible(false);            
        }else{
            errorLabel.setText(result);
            errorLabel.setVisible(true);
        }        
        return result;
    }   
}
