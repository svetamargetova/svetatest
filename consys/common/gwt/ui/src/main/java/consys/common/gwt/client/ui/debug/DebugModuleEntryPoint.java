package consys.common.gwt.client.ui.debug;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.constants.TimeZoneConstants;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleRegistrator;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * 2. verzia debugovacieho modulu
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class DebugModuleEntryPoint implements EntryPoint {

    private static final Logger logger = LoggerFactory.getLogger(DebugModuleEntryPoint.class);

    List<Module> modulesToDebug;

    @Override
    public void onModuleLoad() {
        initMocks();
        modulesToDebug = new ArrayList<Module>();
        TimeZoneConstants tz = GWT.create(TimeZoneConstants.class);
        Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, tz);
        registerModules(modulesToDebug);
        initLayout();
    }

    public abstract void initMocks();

    public abstract void registerModules(List<Module> modules);

    public abstract void registerForms(Map<String,Widget> formMap);

    private void initLayout() {
        
        logger.debug("DebugModuleEntryPoint: start");

        LayoutManager layoutManager = LayoutManager.get();
        RootPanel.get().add(layoutManager);


        for(Module m : modulesToDebug){
            if (m instanceof EventModule) {
                ModuleRegistrator.get().registerInEventModule((EventModule)m);
            }else{
                ModuleRegistrator.get().registerInModule(m);
            }
            m.registerModule();
        }


        
        // nastavi cache, ze je neprihlaseny uzivatel
        Cache.get().register(Cache.LOGGED, Boolean.FALSE);

        
        DebugFormPanel dfp = new DebugFormPanel();
        PanelDispatcher.get().addLogoutPanelItem(dfp);
        PanelDispatcher.get().generatePanel();

        Map<String,Widget> map = new LinkedHashMap<String, Widget>();
        registerForms(map);
        Widget first = null;
        for(Map.Entry<String,Widget> e : map.entrySet()){
            if(first == null) first = e.getValue();
            dfp.addDebugPanel(e.getKey(), e.getValue());
        }

        EventBus.get().fireEvent(new ChangeContentEvent(first));        
    }


    
}
