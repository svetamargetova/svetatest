package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Uzivatel nalezeny pres vyhledavaci dialog uzivatelu, pro komunikaci se serverem
 * @author pepa
 */
public class InvitedUser implements IsSerializable {

    private String uuid;
    private String email;
    private String name;
    private String message;

    public InvitedUser() {
    }

    public InvitedUser(String uuid, String email, String name) {
        this.uuid = uuid;
        this.email = email;
        this.name = name;
    }
    
    public InvitedUser(String uuid) {
        this(uuid, null, null);
    }

    public InvitedUser(String email, String name) {
        this(null, email, name);
    }

    public String getEmail() {
        return email;
    }

    /** vraci true pokud se jedna o pozvaneho uzivatele, jinak false */
    public boolean isUserNew() {
        return uuid == null;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }
}
