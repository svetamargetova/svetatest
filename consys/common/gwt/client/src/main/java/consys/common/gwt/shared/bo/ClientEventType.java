package consys.common.gwt.shared.bo;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum ClientEventType {

    CONFERENCE, BROKERAGE;
}
