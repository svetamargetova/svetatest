/**
 * 
 */
package consys.common.gwt.shared.exception;

/**
 * A more specific exception for access denied error type.
 * @author David MARTIN
 *
 */
public class AccessDeniedException extends ApplicationSecurityException {
    private static final long serialVersionUID = -1362823769009162209L;

  
}
