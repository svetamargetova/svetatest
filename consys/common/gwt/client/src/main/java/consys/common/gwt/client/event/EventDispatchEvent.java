package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.action.EventAction;

/**
 * Na prani Pala.
 *
 * Funguje tak ze sa moze sem pridat cache handler na rozne akcie.
 *
 * @author pepa
 */
public class EventDispatchEvent extends GwtEvent<EventDispatchEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private EventAction action;
    private AsyncCallback callback;
    private ActionExecutionDelegate panel;

    public <A extends EventAction<R>, R extends Result> EventDispatchEvent(final A action, final AsyncCallback<R> callback, final ActionExecutionDelegate panel) {
        this.action = action;
        this.callback = callback;
        this.panel = panel;
    }

    public EventAction getAction() {
        return action;
    }

    public AsyncCallback getCallback() {
        return callback;
    }

    public ActionExecutionDelegate getActionDelegate() {
        return panel;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onDispatch(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onDispatch(EventDispatchEvent event);
    }
}
