package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Nahledovy objekt platebniho profilu
 * @author pepa
 */
public class ClientPaymentProfileThumb implements IsSerializable {

    // data
    private String uuid;
    private String name;

    public ClientPaymentProfileThumb() {
    }

    public ClientPaymentProfileThumb(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
