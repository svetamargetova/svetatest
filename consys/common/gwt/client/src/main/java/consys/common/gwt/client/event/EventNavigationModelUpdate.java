package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.module.event.EventNavigationItem;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventNavigationModelUpdate extends GwtEvent<EventNavigationModelUpdate.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    private List<EventNavigationItem> eventNavigationItems;
    private String title;
    /** Priznak ze sa jedna o kompletny reset navigacneho modelu. V pripade ze false tak sa jedna o update v ramci jedneho eventu*/
    private boolean resetNavigationModel;
    
    public EventNavigationModelUpdate() {
    }

    public EventNavigationModelUpdate(String title,List<EventNavigationItem> addonNavigationItem) {
        this.eventNavigationItems = addonNavigationItem;
        this.title = title;
    }

    public List<EventNavigationItem> getEventNavigationItems() {
        return eventNavigationItems;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onModelUpdate(this);
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Priznak ze sa jedna o kompletny reset navigacneho modelu. V pripade ze false tak sa jedna o update v ramci jedneho eventu
     * @return the resetNavigationModel
     */
    public boolean isResetNavigationModel() {
        return resetNavigationModel;
    }

    /**
     * Priznak ze sa jedna o kompletny reset navigacneho modelu. V pripade ze false tak sa jedna o update v ramci jedneho eventu
     * @param resetNavigationModel the resetNavigationModel to set
     */
    public void setResetNavigationModel(boolean resetNavigationModel) {
        this.resetNavigationModel = resetNavigationModel;
    }

    

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onModelUpdate(EventNavigationModelUpdate event);
    }
}
