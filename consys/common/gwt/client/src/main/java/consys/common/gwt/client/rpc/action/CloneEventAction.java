package consys.common.gwt.client.rpc.action;

import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.shared.action.Action;

/**
 * Akce pro naklonovani vybrane akce
 * @author pepa
 */
public class CloneEventAction implements Action<StringResult> {

    private static final long serialVersionUID = 1086016177055376044L;
    // data
    private String eventUuid;

    public CloneEventAction() {
    }

    public CloneEventAction(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getEventUuid() {
        return eventUuid;
    }
}