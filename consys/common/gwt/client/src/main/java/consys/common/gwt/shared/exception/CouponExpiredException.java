package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 * Vyprselaplatnost kuponu
 * @author pepa
 */
public class CouponExpiredException extends ActionException {

    private static final long serialVersionUID = 8354229271866002984L;

    public CouponExpiredException() {
        super("");
    }

    public CouponExpiredException(String message) {
        super(message);
    }

    public CouponExpiredException(Throwable cause) {
        super(cause);
    }

    public CouponExpiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
