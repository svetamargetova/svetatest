package consys.common.gwt.client.module.event;

import java.util.List;
import java.util.Map;

/**
 * Ak je podmienka zobrazenia NavigacnehoItemu zlozitejsia je mozne pouzit toto
 * rozhranie ktore moze obsahovat lubovolne spracovanie vstupnych dat a zobrazenia.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface EventNavigationItemVisiblityResolver {

    public boolean isItemVisibile(List<String> rights, Map<String,String> systemProperties);            
}
