package consys.common.gwt.client.logger;

import java.util.logging.Level;

/** ALL, FINEST, FINER, FINE, CONFIG, INFO, WARNING, SEVERE
 * L4J   === java.util.Logging
 * TRACE <=> ALL
 * DEBUG <=> FINEST,FINER,FINE,CONFIG
 * INFO  <=> INFO
 * WARN  <=> WARNING
 * ERROR <=> SEVERE
 * FATAL <= no support
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Logger {

    java.util.logging.Logger logger;

    Logger(String name) {
        logger = java.util.logging.Logger.getLogger(name);
    }

    /**
     * java.util.Logger.Levels ALL
     */
    public boolean isTraceEnabled() {
        return logger.isLoggable(Level.ALL);
    }

    /**
     * java.util.Logger.Levels FINEST,FINER,FINE,CONFIG
     */
    public boolean isDebugEnabled() {
        return logger.isLoggable(Level.FINEST)
                || logger.isLoggable(Level.FINER)
                || logger.isLoggable(Level.FINE)
                || logger.isLoggable(Level.CONFIG);
    }

     /**
     * java.util.Logger.Levels WARNING
     */
    public boolean isWarnEnabled() {
        return logger.isLoggable(Level.WARNING);
    }

     /**
     * java.util.Logger.Levels INFO
     */
    public boolean isInfoEnabled() {
        return logger.isLoggable(Level.INFO);
    }

     /**
     * java.util.Logger.Levels SEVERE
     */
    public boolean isErrorEnabled() {
        return logger.isLoggable(Level.SEVERE);
    }


    public void trace(String message){
        logger.log(Level.ALL, message);
    }

    public void trace(String message, Throwable t){
        logger.log(Level.ALL, message, t);
    }

    public void debug(String message){
        // pouzijeme najvyssi mozny tj. Config
        logger.log(Level.CONFIG, message);
    }

    public void debug(String message, Throwable t){
        logger.log(Level.CONFIG, message, t);
    }

    public void info(String message){
        logger.log(Level.INFO, message);
    }

    public void info(String message, Throwable t){
        logger.log(Level.INFO, message, t);
    }

    public void warn(String message){
        logger.log(Level.WARNING, message);
    }

    public void warn(String message, Throwable t){
        logger.log(Level.WARNING, message, t);
    }

    public void error(String message){
        logger.log(Level.SEVERE, message);
    }

    public void error(String message, Throwable t){
        logger.log(Level.SEVERE, message, t);
    }

}
