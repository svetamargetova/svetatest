package consys.common.gwt.client.action;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.AbstractDispatchAsync;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.gwt.shared.exception.UncaughtException;

/**
 * Spousteni akci na serveru
 * @author pepa 
 */
public class ActionExecutor {

    private static final Logger logger = LoggerFactory.getLogger(ActionExecutor.class);
    private static final AbstractDispatchAsync dispatchAsync;

    static {
        dispatchAsync = GWT.create(AbstractDispatchAsync.class);
        dispatchAsync.addExceptionHandler(new SecurityExceptionHandler());
    }

    public static AbstractDispatchAsync getDispatchAsync() {
        return dispatchAsync;
    }

    /**
     * necha provest akci na serveru a vrátí výsledek v callbacku
     * @param action akce, která se má provést na serveru
     * @param callback co se provede s vysledkem nebo pri chybe
     * @param delegate slouzi ke spusteni akci pri spusteni a ukonceni prikazu (napr. zobrazeni/skryti waiting)
     */
    public static void execute(Action action, final AsyncCallback callback, final ActionExecutionDelegate delegate) {
        if (delegate != null) {
            delegate.actionStarted();
        }
        dispatchAsync.execute(action, new AsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                if (caught instanceof StatusCodeException) {
                    StatusCodeException e = (StatusCodeException) caught;
                    if (e.getStatusCode() == Response.SC_SERVICE_UNAVAILABLE) {
                        Window.Location.reload();
                        return;
                    }
                }
                commonException(delegate, caught);
                callback.onFailure(caught);
                if (delegate != null) {
                    delegate.actionEnds();
                }
            }

            @Override
            public void onSuccess(Object result) {
                if (delegate != null) {
                    delegate.actionEnds();
                }
                callback.onSuccess(result);
            }
        });
    }

    /** zkontroluje jestli se nejedna o obecnou vyjimku service failed nebo bad input */
    private static void commonException(ActionExecutionDelegate source, Throwable caught) {
        if (caught instanceof ServiceFailedException) {
            source.setFailMessage(CommonExceptionEnum.SERVICE_FAILED);
        } else if (caught instanceof BadInputException) {
            source.setFailMessage(CommonExceptionEnum.BAD_INPUT);
        } else if (caught instanceof StatusCodeException) {
            logger.error("StatusCodeException", caught);
        } else if (caught instanceof UncaughtException) {
            logger.error("Uncaught exception!!", caught);
            source.setFailMessage(CommonExceptionEnum.SERVICE_FAILED);
        }
    }
}
