package consys.common.gwt.client.solr.bo;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayString;

/**
 * Data jednoho nalezeneho uzivatele
 * @author pepa
 */
public class SearchedUser extends JavaScriptObject {

    protected SearchedUser() {
    }

    /** email */
    public final native String getEmail() /*-{
        return this.email;
    }-*/;

    /** uuid */
    public final native String getUuid() /*-{
        return this.uuid;
    }-*/;

    /** @return seznam polozek */
    public final native JsArrayString getUserNames() /*-{
        return this.user_name;
    }-*/;

    /** @return seznam polozek */
    public final native JsArrayString getOrganizations() /*-{
        return this.organization;
    }-*/;
}
