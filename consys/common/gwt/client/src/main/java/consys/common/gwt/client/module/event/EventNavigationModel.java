package consys.common.gwt.client.module.event;

import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventEntered;
import consys.common.gwt.client.event.EventNavigationModelUpdate;
import consys.common.gwt.client.event.EventSettingsReceivedEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Model ktory uchovava achitekturu celeho panelu ktora je vytvorena jednotlivymi
 * modulami. Po prihalseni sa do eventu a nacitani LoadEventSettings model vytvori
 * kopiu na zaklade uzivatelskych prav a tu nastavuje do panelu.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventNavigationModel implements EventEntered.Handler, EventSettingsReceivedEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(EventNavigationModel.class);
    List<EventNavigationItem> masterAddons;
    List<String> rights;
    String currentActionTitle;

    public EventNavigationModel() {
        masterAddons = new ArrayList<EventNavigationItem>();
        EventBus.get().addHandler(EventEntered.TYPE, this);
        EventBus.get().addHandler(EventSettingsReceivedEvent.TYPE, this);
    }
    private static final EventNavigationModel singletonModel = new EventNavigationModel();

    public static final EventNavigationModel model() {
        return singletonModel;
    }

    /**
     * Metoda vyhodnoti viditelnost polozky na zaklade roli alebo resolvera. Pouzite
     * nastroja je zavisle na typu itemu. Ak sa jedna o zlozku {@link EventNavigationItem#isDirectory()}
     * tak sa vyhodnocuje najskor viditelnost pouzitym {@link EventNavigationItemVisiblityResolver}
     * ktory urci ci je vidiet cely modul. Ak ANO potom spracovava role ktore
     * urcuju ci je viditelna nastavovacie kolecko resp. synovske uzly a polozky.
     */
    private void setItemUserOwnerFlag(EventNavigationItem item, List<String> rights, Map<String, String> properties) {

        // ak je directory tak nastavime defaultne ze je viditelna
        if(item.isDirectory()) item.setShowDirectory(true);
        
        // Ak ma resolver tak sa vyhodnoti.
        if (item.hasResolver()) {
            boolean resolvedVisibility = item.getVisiblityResolver().isItemVisibile(rights, properties);
            if(item.isDirectory()){
                item.setShowDirectory(resolvedVisibility);                
            }else{
                item.setVisible(resolvedVisibility);
            }            
        }

        // ak je directory tak sa vyhodnocuju role, ak je normalny tak sa vyhodnocuju role len ak nema resolver
        if((item.isDirectory() && item.isShowDirectory()) || !item.hasResolver()){
            if (item.getRoles() != null && item.getRoles().length > 0) {
                for (int i = 0; i < item.getRoles().length; i++) {
                    if (rights.contains(item.getRoles()[i])) {
                        item.setVisible(true);
                        return;
                    }
                }
                item.setVisible(false);
            } else {
                // nema nastavene pravo takze je videt
                item.setVisible(true);
            }
        }
    }

    private void setItemsUserOwnerFlag(List<EventNavigationItem> items, List<String> rights, Map<String, String> properties) {
        // nastavime visible priznak a seradime
        Collections.sort(items);
        for (EventNavigationItem master : items) {
            setItemUserOwnerFlag(master, rights, properties);
            if (master.isDirectory()) {
                // ak je directory vidiet pokracujeme v potomkoch
                if(master.isShowDirectory()){
                    setItemsUserOwnerFlag(master.getChilds(), rights, properties);
                }else{
                    // ak nie nastavime vsetkym potomkom visibility false
                    setItemsVisibility(master.getChilds(), false);
                }
            }
        }
    }

    /**
     * Nastavi hromadne viditelnost vsekych itemov do hlbky
     * @param items
     * @param visibility
     */
    private void setItemsVisibility(List<EventNavigationItem> items, boolean visibility){
        for (EventNavigationItem eventNavigationItem : items) {
            eventNavigationItem.setVisible(visibility);
            if(eventNavigationItem.isDirectory()){
                setItemsVisibility(eventNavigationItem.getChilds(), visibility);
            }
        }
    }

    private void newEventSettingsReceived(List<String> rights, Map<String, String> properties,boolean resetNavigationModel) {
        // ulozime prava
        this.rights = rights;
        for (String right : rights) {
            logger.debug(right);
        }
        // nastavime flagy
        setItemsUserOwnerFlag(masterAddons, rights, properties);
        // Vystrelime event ze sa zmenil model
        EventNavigationModelUpdate modelUpdate = new EventNavigationModelUpdate(currentActionTitle, masterAddons);
        modelUpdate.setResetNavigationModel(resetNavigationModel);
        EventBus.get().fireEvent(modelUpdate);
    }

    /**
     * Zistuje ci je parent uz zaregistrovany.
     */
    private boolean isParentModelItemRegistred(EventNavigationItem parent) {
        for (EventNavigationItem item : masterAddons) {
            if (item.equals(parent)) {
                return true;
            }
        }
        return false;
    }

    private EventNavigationItem findParentByName(String name) {
        for (EventNavigationItem item : masterAddons) {
            if (item.getTitle().equalsIgnoreCase(name)) {
                return item;
            }
        }
        throw new NullPointerException("AddonModelItem parent with name " + name + " not registred!");
    }

    private void addMasterAddon(EventNavigationItem item) {
        masterAddons.add(item);
    }

    
    public final void insertFirstLevelAddon(EventNavigationItem parent, String title, String urlName, int index, ConsysAction action, EventNavigationItemVisiblityResolver resolver) {        
        EventNavigationItem child = new EventNavigationItem(title, urlName, action,index, false, resolver);
        if (!isParentModelItemRegistred(parent)) {
            addMasterAddon(parent);
        }
        parent.addChild(child);
        
    }
    public final void insertFirstLevelAddon(EventNavigationItem parent, String title, String urlName, int index, ConsysAction action, String... roles) {
        EventNavigationItem child = new EventNavigationItem(title, urlName, index, action, roles);
        if (!isParentModelItemRegistred(parent)) {
            addMasterAddon(parent);
        }
        parent.addChild(child);
    }
    
    /**
     * Vlozi do prveho levelu modelu parent(ak tam este neexisutje) a do neho
     * vlozi childa.
     */
    public final void insertFirstLevelAddon(EventNavigationItem parent, String title, int index, ConsysAction action, String... roles) {
        EventNavigationItem child = new EventNavigationItem(title, index, action, roles);
        if (!isParentModelItemRegistred(parent)) {
            addMasterAddon(parent);
        }
        parent.addChild(child);
    }

    /**
     * Vyhlada v prvom levely parenta a vlozi do neho childa.
     *
     * @throws NullPointerException ak parent neexstuje este v prvom levely
     */
    public final void insertFirstLevelAddon(String parentAddonName, String title, int index, ConsysAction action, String... roles) {
        EventNavigationItem child = new EventNavigationItem(title, index, action, roles);
        EventNavigationItem parent = findParentByName(parentAddonName);
        parent.addChild(child);
    }

    /**
     * Vlozi do prveho levelu modelu parent(ak tam este neexisutje) a do neho
     * vlozi childa.
     */
    public final void insertFirstLevelAddon(EventNavigationItem parent, String title, int index, ConsysAction action, EventNavigationItemVisiblityResolver visiblityResolver) {
        EventNavigationItem child = new EventNavigationItem(title, index, action, visiblityResolver);
        if (!isParentModelItemRegistred(parent)) {
            addMasterAddon(parent);
        }
        parent.addChild(child);
    }

    /**
     * Vyhlada v prvom levely parenta a vlozi do neho childa.
     *
     * @throws NullPointerException ak parent neexstuje este v prvom levely
     */
    public final void insertFirstLevelAddon(String parentAddonName, String title, int index, ConsysAction action, EventNavigationItemVisiblityResolver visiblityResolver) {
        EventNavigationItem child = new EventNavigationItem(title, index, action, visiblityResolver);
        EventNavigationItem parent = findParentByName(parentAddonName);
        parent.addChild(child);
    }

    @Override
    public void onEventEnter(EventEntered event) {
        currentActionTitle = event.getEnteredEvent().getCurrentEventSystemName();
    }

    @Override
    public void onReceivedSettingsLoaded(List<String> rights, Map<String, String> properties, boolean resetNavigationModel) {
        newEventSettingsReceived(rights, properties,resetNavigationModel);
    }
}
