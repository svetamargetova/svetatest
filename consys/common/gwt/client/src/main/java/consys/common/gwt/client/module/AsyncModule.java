package consys.common.gwt.client.module;

/**
 *
 * @author pepa
 */
public interface AsyncModule<T> {

    /** metoda volana po uspesnem asynchronnim dotazeni kodu s naplnenou instanci daneho objektu */
    public void onSuccess(T instance);

    /** metoda volana pri chybe v asynchronnim dotazeni kodu */
    public void onFail();
}
