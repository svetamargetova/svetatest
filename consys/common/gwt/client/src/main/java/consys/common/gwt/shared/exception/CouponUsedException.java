package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class CouponUsedException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public CouponUsedException() {
        super("");
    }

    public CouponUsedException(String message) {
        super(message);
    }

    public CouponUsedException(Throwable cause) {
        super(cause);
    }

    public CouponUsedException(String message, Throwable cause) {
        super(message, cause);
    }
}
