package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ArrayListResult<T> implements Result {

    private static final long serialVersionUID = -8182567113949115783L;
    private ArrayList<T> list;

    public ArrayListResult() {
        list = new ArrayList<T>();
    }

    public ArrayListResult(ArrayList<T> list) {
        this.list = list;
    }

    public ArrayList<T> getArrayListResult() {
        return list;
    }
}
