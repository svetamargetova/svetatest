package consys.common.gwt.shared.bo;

/**
 * Uzivatel nalezeny pres vyhledavaci dialog uzivatelu, pro pouziti v ui
 * @author pepa
 */
public class ClientInvitedUser extends InvitedUser {

    // data
    private String position;
    private String uuidPortraitImagePrefix;

    public ClientInvitedUser() {
        super();
    }

    public ClientInvitedUser(String uuid, String email,String name) {
        super(uuid, email, name);
    }
    
    public ClientInvitedUser(String uuid) {
        super(uuid);
    }

    public ClientInvitedUser(String email, String name) {
        super(email, name);
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUuidPortraitImagePrefix() {
        return uuidPortraitImagePrefix;
    }

    public void setUuidPortraitImagePrefix(String uuidPortraitImagePrefix) {
        this.uuidPortraitImagePrefix = uuidPortraitImagePrefix;
    }

    /** vytvori a vrati objekt s daty predka */
    public InvitedUser parentObject() {
        InvitedUser parent;
        if (isUserNew()) {
            parent = new InvitedUser(getEmail(), getName());
        } else {
            parent = new InvitedUser(getUuid());
        }
        parent.setMessage(getMessage());
        return parent;
    }

    /** vygeneruje z dat ClientUserWithAffiliationThumb, pozor! pokud jde o noveho uzivatele uuid je null */
    public ClientUserWithAffiliationThumb getUserWithAffiliationThumb() {
        ClientUserWithAffiliationThumb thumb = new ClientUserWithAffiliationThumb();
        thumb.setUuid(getUuid());
        thumb.setUuidPortraitImagePrefix(uuidPortraitImagePrefix);
        thumb.setName(getName());
        thumb.setPosition(position);
        return thumb;
    }

    /** vygeneruje z dat ClientUserThumb, pozor! pokud jde o noveho uuid je null */
    public ClientUserThumb getClientUserThumb() {
        ClientUserThumb thumb = new ClientUserThumb();
        thumb.setUuid(getUuid());
        thumb.setUuidPortraitImagePrefix(getUuidPortraitImagePrefix());
        thumb.setName(getName());
        return thumb;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientInvitedUser other = (ClientInvitedUser) obj;
        if (this.getUuid() != null && other.getUuid() == null) {
            return false;
        }
        if (this.getUuid() == null && other.getUuid() != null) {
            return false;

        }
        if (this.getUuid() != null && other.getUuid() != null) {
            return this.getUuid().equals(other.getUuid());
        } else {
            return this.getEmail().equals(other.getEmail());
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.getUuid() != null ? this.getUuid().hashCode() : this.getEmail().hashCode());
        return hash;
    }
}
