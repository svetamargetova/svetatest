package consys.common.gwt.client.solr.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Parametry hlavicky solr odpovedi
 * @author pepa
 */
public class SolrHeaderParams extends JavaScriptObject {

    protected SolrHeaderParams() {
    }

    /** parametr start */
    public final native String getStart() /*-{
        return this.start;
    }-*/;

    /** query */
    public final native String getQuery() /*-{
        return this.q;
    }-*/;

    /** wt */
    public final native String getWT() /*-{
        return this.wt;
    }-*/;

    /** wt */
    public final native String getRows() /*-{
        return this.rows;
    }-*/;
}
