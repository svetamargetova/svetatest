package consys.common.gwt.client.rpc.action.cache;

import consys.common.gwt.client.cache.CacheAction;
import java.util.List;

/**
 * abstraktni cache akce pro provadeni akci s enum industry
 * @author pepa
 */
public abstract class CurrentEventUserRightsCacheAction implements CacheAction<List<String>> {

    // konstanty
    public static final String CURRENT_USER_EVENT_RIGHTS = "current_user_event_rights";

    @Override
    public String getName() {
        return CURRENT_USER_EVENT_RIGHTS;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
