package consys.common.gwt.client.utils;

/**
 *
 * @author pepa
 */
public final class EventUtils {

    public static final String REGEXP_NEGATION_ALL_ALPHANUMERIC_DASH_AND_UNDERSOCRE = "[^a-zA-Z0-9\\-_]";

    private EventUtils() {
    }

    /** generuje nazev ve formatu: akce[serie]-rok-lokalizace.html */
    public static String profileFileName(String acronym, String year, String series, String locale) {
        return profileName(acronym, year, series, locale) + ".html";
    }

    /** generuje nazev ve formatu: akce[serie]-rok-lokalizace */
    public static String profileName(String acronym, String year, String series, String locale) {
        StringBuilder sb = new StringBuilder(profileName(acronym, year, series));
        sb.append("-");
        sb.append(locale);
        return sb.toString();
    }

    /** generuje nazev ve formatu: akce[serie]-rok */
    public static String profileName(String acronym, String year, String series) {
        String name = acronym.replaceAll(REGEXP_NEGATION_ALL_ALPHANUMERIC_DASH_AND_UNDERSOCRE, "-");
        name = name.toLowerCase();
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append("-");
        sb.append(StringUtils.isNotBlank(series) ? series : "1");
        sb.append("-");
        sb.append(year);
        return sb.toString();
    }

    /** vraci string s iframe na google maps podle zadane adresy a lokalizace */
    public static String googleMap(String encodedAddress, String locale) {
        return googleMap(encodedAddress, locale, "425", "350");
    }

    public static String googleMap(String encodedAddress, String locale, String width, String height) {
        return googleMap(encodedAddress, locale, width, height, null);
    }

    public static String googleMap(String encodedAddress, String locale, String width, String height, String linkText) {
        StringBuilder src = new StringBuilder("http://maps.google.com/maps?hl=");
        src.append(locale);
        src.append("&amp;q=");
        src.append(encodedAddress);
        src.append("&amp;aq=&amp;sll=&amp;sspn=&amp;ie=UTF8&amp;hq=&amp;hnear=");
        src.append(encodedAddress);
        src.append("&amp;ll=&amp;spn=&amp;t=m&amp;z=14");

        String srcString = src.toString();

        StringBuilder sb = new StringBuilder();
        sb.append("<iframe width=\"");
        sb.append(width);
        sb.append("\" height=\"");
        sb.append(height);
        sb.append("\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" src=\"");
        sb.append(srcString);
        sb.append("&amp;iwloc=");
        if (linkText == null) {
            sb.append("A");
        }
        sb.append("&amp;output=embed");
        sb.append("\" style=\"margin-top:10px\"></iframe>");

        if (linkText != null) {
            sb.append("<br/><div style=\"text-align:center\"><small><a href=\"");
            sb.append(srcString);
            sb.append("&amp;iwloc=A&amp;source=embed");
            sb.append("\">");
            sb.append(linkText);
            sb.append("</a></small></div>");
        }

        return sb.toString();
    }

    /** vraci string s iframe na google maps podle zadane adresy a lokalizace */
    public static String googleMapProfilePage(String encodedAddress, String locale, String linkText) {
        return googleMap(encodedAddress, locale, "220", "220", linkText);
    }
}
