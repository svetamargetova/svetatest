package consys.common.gwt.client.rpc;

import consys.common.gwt.shared.action.Action;


/**
 * Implementations of this interface can be added to a {@link DispatchAsync} implementation
 * to intercept exceptions which return from further up the chain.
 *
 * @author David Peterson
 */
public interface ExceptionHandler {

 void onFailure( Action a, Throwable e );
}
