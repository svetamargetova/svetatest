/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class ServiceFailedException extends ActionException{
    private static final long serialVersionUID = -7524741904350747273L;
    

    public ServiceFailedException() {}

    public ServiceFailedException( String message ) {
        super( message );
    }

    public ServiceFailedException( Throwable cause ) {
        super( cause );
    }

    public ServiceFailedException( String message, Throwable cause ) {
        super( message, cause );
    }

}
