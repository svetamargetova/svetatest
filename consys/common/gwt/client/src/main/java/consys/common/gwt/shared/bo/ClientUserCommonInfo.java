package consys.common.gwt.shared.bo;

import consys.common.gwt.client.utils.UserUtils;
import consys.common.gwt.shared.action.Result;

/**
 * Obecne info o uzivateli, ktere se taha do UserCard
 * @author pepa
 */
public class ClientUserCommonInfo implements Result {

    private static final long serialVersionUID = -7065184036091017851L;
    // data
    private String uuid;
    private String fullName;
    private String position;
    private String organization;
    private String imageUuid;
    private String email;

    public String getAffilation() {
        return UserUtils.affilation(position, organization);
    }

    public String getAffilation(String at) {
        return UserUtils.affilation(position, organization, at);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getImageUuid() {
        return imageUuid;
    }

    public void setImageUuid(String imageUuid) {
        this.imageUuid = imageUuid;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
