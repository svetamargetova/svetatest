package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class CouponInvalidException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public CouponInvalidException() {
        super("");
    }

    public CouponInvalidException(String message) {
        super(message);
    }

    public CouponInvalidException(Throwable cause) {
        super(cause);
    }

    public CouponInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
}
