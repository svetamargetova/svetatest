package consys.common.gwt.client.rpc.action;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientUserThumb;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadUserThumbAction implements Action<ClientUserThumb>{
    private static final long serialVersionUID = -7816719862943770739L;    

    private String uuid;

    public LoadUserThumbAction() {
    }

    public LoadUserThumbAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

}
