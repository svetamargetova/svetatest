package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Nazev produktu a jeho cena
 * @author pepa
 */
public class ClientPaymentProduct implements IsSerializable {

    private String name;
    private Monetary price;
    private int quantity = 1;

    public ClientPaymentProduct() {
    }

    public ClientPaymentProduct(String name, Monetary price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Monetary getPrice() {
        return price;
    }

    public void setPrice(Monetary price) {
        this.price = price;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }

    @Override
    public String toString() {
        String p = price == null ? "null" : price.toString();
        return "ClientPaymentProduct[name=" + name + " price=" + p + " quantity=" + quantity + "]";
    }
}
