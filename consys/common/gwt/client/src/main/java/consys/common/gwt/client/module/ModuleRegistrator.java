package consys.common.gwt.client.module;

import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventSettingsLoadedEvent;
import consys.common.gwt.client.event.EventSettingsReceivedEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.event.EventNavigationModel;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.shared.bo.EventSettings;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 *
 * @author pepa
 */
public class ModuleRegistrator {

    Logger logger = LoggerFactory.getLogger(ModuleRegistrator.class);
    // instance
    private static ModuleRegistrator instance;
    // data
    private ArrayList<Module> loggedInModules;
    private ArrayList<Module> loggedOutModules;
    private ModuleMenuRegistrator loggedInMenuModuleRegistrator;
    private ModuleMenuRegistrator loggedOutMenuModuleRegistrator;

    private ModuleRegistrator() {
        loggedInModules = new ArrayList<Module>();
        loggedOutModules = new ArrayList<Module>();
        loggedInMenuModuleRegistrator = new ModuleMenuRegistrator();
        loggedOutMenuModuleRegistrator = new ModuleMenuRegistrator();        
    }

    /** vraci instanci ModuleRegistrator */
    public static ModuleRegistrator get() {
        if (instance == null) {
            instance = new ModuleRegistrator();
        }
        return instance;
    }

    /** zaregistruje modul do systemu */
    public void registerInModule(Module... module) {
        for (int i = 0; i < module.length; i++) {            
            loggedInModules.add(module[i]);
            logger.info("Registred IN modul: "+module[i].toString());
            module[i].initializeModule(getLoggedInMenuModuleRegistrator());
        }
    }

    /** zaregistruje modul do systemu */
    public void registerInEventModule(EventModule... module) {
        registerInModule(module);
        for (int i = 0; i < module.length; i++) {
            module[i].registerEventNavigation(EventNavigationModel.model());
            RoleManager.registerModule(module[i]);
        }
    }

    /** zaregistruje modul do systemu */
    public void registerOutModule(Module... module) {        
        for (int i = 0; i < module.length; i++) {
            loggedOutModules.add(module[i]);
            logger.info("Registred OUT modul: "+module[i].toString());
            module[i].initializeModule(getLoggedOutMenuModuleRegistrator());
            module[i].registerModule();
        }
        
    }

    public ArrayList<Module> getLoggedInModules() {
        return loggedInModules;
    }

    public ArrayList<Module> getLoggedOutModules() {
        return loggedOutModules;
    }

    /**
     * @return the loggedInMenuModuleRegistrator
     */
    public ModuleMenuRegistrator getLoggedInMenuModuleRegistrator() {
        return loggedInMenuModuleRegistrator;
    }

    /**
     * @return the loggedOutMenuModuleRegistrator
     */
    public ModuleMenuRegistrator getLoggedOutMenuModuleRegistrator() {
        return loggedOutMenuModuleRegistrator;
    }

    
}
