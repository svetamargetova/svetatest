package consys.common.gwt.client.logger;

import com.google.gwt.logging.impl.FormatterImpl;
import java.util.Date;
import java.util.logging.LogRecord;

/**
 * Implementacia klasickeho formatu konzoloveho loggeru
 * time LEVEL (Name) - [message]
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ClassicTextFormatter extends FormatterImpl {

    private boolean showStackTraces;

    public ClassicTextFormatter(boolean showStackTraces) {
        this.showStackTraces = showStackTraces;
    }

    @Override
    public String format(LogRecord event) {
        StringBuilder message = new StringBuilder();
        // time
        Date date = new Date(event.getMillis());
        message.append(date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()+" ");
        //LEVEL
        message.append(event.getLevel().getName()+" ");
        // logger name
        message.append("("+event.getLoggerName()+") - ");
        // message        
        message.append(event.getMessage());
        if (showStackTraces) {
            message.append(getStackTraceAsString(event.getThrown(), "\n", "\t"));
        }
        return message.toString();
    }
}
