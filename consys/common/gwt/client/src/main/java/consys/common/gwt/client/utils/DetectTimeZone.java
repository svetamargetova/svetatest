package consys.common.gwt.client.utils;

/**
 *
 * @author pepa
 */
public class DetectTimeZone {

    public native void init()/*-{

$wnd.jstz = (function () {
    'use strict';
    var HEMISPHERE_SOUTH = 's',

        get_date_offset = function (date) {
            var offset = -date.getTimezoneOffset();
            return (offset !== null ? offset : 0);
        },

        get_january_offset = function () {
            return get_date_offset(new Date(2010, 0, 1, 0, 0, 0, 0));
        },

        get_june_offset = function () {
            return get_date_offset(new Date(2010, 5, 1, 0, 0, 0, 0));
        },

        date_is_dst = function (date) {
            var base_offset = ((date.getMonth() > 5 ? get_june_offset()
                                                : get_january_offset())),
                date_offset = get_date_offset(date);

            return (base_offset - date_offset) !== 0;
        },

        lookup_key = function () {
            var january_offset = get_january_offset(),
                june_offset = get_june_offset(),
                diff = get_january_offset() - get_june_offset();

            if (diff < 0) {
                return january_offset + ",1";
            } else if (diff > 0) {
                return june_offset + ",1," + HEMISPHERE_SOUTH;
            }

            return january_offset + ",0";
        },

        determine_timezone = function () {
            var key = lookup_key();
            return new $wnd.jstz.TimeZone($wnd.jstz.olson.timezones[key]);
        };

    return {
        determine_timezone : determine_timezone,
        date_is_dst : date_is_dst,
        get_date_offset : get_date_offset
    };
}());

$wnd.jstz.TimeZone = (function () {
    'use strict';
    var timezone_name = null,
        uses_dst = null,
        utc_offset = null,

        name = function () {
            return timezone_name;
        },

        dst = function () {
            return uses_dst;
        },

        offset = function () {
            return utc_offset;
        },

        ambiguity_check = function () {
            var ambiguity_list = $wnd.jstz.olson.ambiguity_list[timezone_name],
                length = ambiguity_list.length,
                i = 0,
                tz = ambiguity_list[0];

            for (; i < length; i += 1) {
                tz = ambiguity_list[i];

                if ($wnd.jstz.date_is_dst($wnd.jstz.olson.dst_start_dates[tz])) {
                    timezone_name = tz;
                    return;
                }
            }
        },

        is_ambiguous = function () {
            return typeof ($wnd.jstz.olson.ambiguity_list[timezone_name]) !== 'undefined';
        },

        Constr = function (tz_info) {
            utc_offset = tz_info[0];
            timezone_name = tz_info[1];
            uses_dst = tz_info[2];
            if (is_ambiguous()) {
                ambiguity_check();
            }
        };

    Constr.prototype = {
        constructor : $wnd.jstz.TimeZone,
        name : name,
        dst : dst,
        offset : offset
    };

    return Constr;
}());

$wnd.jstz.olson = {};

$wnd.jstz.olson.timezones = (function () {
    "use strict";
    return {
        '-720,0'   : ['-12:00', 'Etc/GMT+12', false],
        '-660,0'   : ['-11:00', 'Pacific/Pago_Pago', false],
        '-600,1'   : ['-11:00', 'America/Adak', true],
        '-660,1,s' : ['-11:00', 'Pacific/Apia', true],
        '-600,0'   : ['-10:00', 'Pacific/Honolulu', false],
        '-570,0'   : ['-10:30', 'Pacific/Marquesas', false],
        '-540,0'   : ['-09:00', 'Pacific/Gambier', false],
        '-540,1'   : ['-09:00', 'America/Anchorage', true],
        '-480,1'   : ['-08:00', 'America/Los_Angeles', true],
        '-480,0'   : ['-08:00', 'Pacific/Pitcairn', false],
        '-420,0'   : ['-07:00', 'America/Phoenix', false],
        '-420,1'   : ['-07:00', 'America/Denver', true],
        '-360,0'   : ['-06:00', 'America/Guatemala', false],
        '-360,1'   : ['-06:00', 'America/Chicago', true],
        '-360,1,s' : ['-06:00', 'Pacific/Easter', true],
        '-300,0'   : ['-05:00', 'America/Bogota', false],
        '-300,1'   : ['-05:00', 'America/New_York', true],
        '-270,0'   : ['-04:30', 'America/Caracas', false],
        '-240,1'   : ['-04:00', 'America/Halifax', true],
        '-240,0'   : ['-04:00', 'America/Santo_Domingo', false],
        '-240,1,s' : ['-04:00', 'America/Asuncion', true],
        '-210,1'   : ['-03:30', 'America/St_Johns', true],
        '-180,1'   : ['-03:00', 'America/Godthab', true],
        '-180,0'   : ['-03:00', 'America/Argentina/Buenos_Aires', false],
        '-180,1,s' : ['-03:00', 'America/Montevideo', true],
        '-120,0'   : ['-02:00', 'America/Noronha', false],
        '-120,1'   : ['-02:00', 'Etc/GMT+2', true],
        '-60,1'    : ['-01:00', 'Atlantic/Azores', true],
        '-60,0'    : ['-01:00', 'Atlantic/Cape_Verde', false],
        '0,0'      : ['00:00', 'Etc/UTC', false],
        '0,1'      : ['00:00', 'Europe/London', true],
        '60,1'     : ['+01:00', 'Europe/Berlin', true],
        '60,0'     : ['+01:00', 'Africa/Lagos', false],
        '60,1,s'   : ['+01:00', 'Africa/Windhoek', true],
        '120,1'    : ['+02:00', 'Asia/Beirut', true],
        '120,0'    : ['+02:00', 'Africa/Johannesburg', false],
        '180,1'    : ['+03:00', 'Europe/Moscow', true],
        '180,0'    : ['+03:00', 'Asia/Baghdad', false],
        '210,1'    : ['+03:30', 'Asia/Tehran', true],
        '240,0'    : ['+04:00', 'Asia/Dubai', false],
        '240,1'    : ['+04:00', 'Asia/Yerevan', true],
        '270,0'    : ['+04:30', 'Asia/Kabul', false],
        '300,1'    : ['+05:00', 'Asia/Yekaterinburg', true],
        '300,0'    : ['+05:00', 'Asia/Karachi', false],
        '330,0'    : ['+05:30', 'Asia/Kolkata', false],
        '345,0'    : ['+05:45', 'Asia/Kathmandu', false],
        '360,0'    : ['+06:00', 'Asia/Dhaka', false],
        '360,1'    : ['+06:00', 'Asia/Omsk', true],
        '390,0'    : ['+06:30', 'Asia/Rangoon', false],
        '420,1'    : ['+07:00', 'Asia/Krasnoyarsk', true],
        '420,0'    : ['+07:00', 'Asia/Jakarta', false],
        '480,0'    : ['+08:00', 'Asia/Shanghai', false],
        '480,1'    : ['+08:00', 'Asia/Irkutsk', true],
        '525,0'    : ['+08:45', 'Australia/Eucla', true],
        '525,1,s'  : ['+08:45', 'Australia/Eucla', true],
        '540,1'    : ['+09:00', 'Asia/Yakutsk', true],
        '540,0'    : ['+09:00', 'Asia/Tokyo', false],
        '570,0'    : ['+09:30', 'Australia/Darwin', false],
        '570,1,s'  : ['+09:30', 'Australia/Adelaide', true],
        '600,0'    : ['+10:00', 'Australia/Brisbane', false],
        '600,1'    : ['+10:00', 'Asia/Vladivostok', true],
        '600,1,s'  : ['+10:00', 'Australia/Sydney', true],
        '630,1,s'  : ['+10:30', 'Australia/Lord_Howe', true],
        '660,1'    : ['+11:00', 'Asia/Kamchatka', true],
        '660,0'    : ['+11:00', 'Pacific/Noumea', false],
        '690,0'    : ['+11:30', 'Pacific/Norfolk', false],
        '720,1,s'  : ['+12:00', 'Pacific/Auckland', true],
        '720,0'    : ['+12:00', 'Pacific/Tarawa', false],
        '765,1,s'  : ['+12:45', 'Pacific/Chatham', true],
        '780,0'    : ['+13:00', 'Pacific/Tongatapu', false],
        '840,0'    : ['+14:00', 'Pacific/Kiritimati', false]
    };
}());

$wnd.jstz.olson.dst_start_dates = (function () {
    "use strict";
    return {
        'America/Denver' : new Date(2011, 2, 13, 3, 0, 0, 0),
        'America/Mazatlan' : new Date(2011, 3, 3, 3, 0, 0, 0),
        'America/Chicago' : new Date(2011, 2, 13, 3, 0, 0, 0),
        'America/Mexico_City' : new Date(2011, 3, 3, 3, 0, 0, 0),
        'Atlantic/Stanley' : new Date(2011, 8, 4, 7, 0, 0, 0),
        'America/Asuncion' : new Date(2011, 9, 2, 3, 0, 0, 0),
        'America/Santiago' : new Date(2011, 9, 9, 3, 0, 0, 0),
        'America/Campo_Grande' : new Date(2011, 9, 16, 5, 0, 0, 0),
        'America/Montevideo' : new Date(2011, 9, 2, 3, 0, 0, 0),
        'America/Sao_Paulo' : new Date(2011, 9, 16, 5, 0, 0, 0),
        'America/Los_Angeles' : new Date(2011, 2, 13, 8, 0, 0, 0),
        'America/Santa_Isabel' : new Date(2011, 3, 5, 8, 0, 0, 0),
        'America/Havana' : new Date(2011, 2, 13, 2, 0, 0, 0),
        'America/New_York' : new Date(2011, 2, 13, 7, 0, 0, 0),
        'Asia/Gaza' : new Date(2011, 2, 26, 23, 0, 0, 0),
        'Asia/Beirut' : new Date(2011, 2, 27, 1, 0, 0, 0),
        'Europe/Minsk' : new Date(2011, 2, 27, 2, 0, 0, 0),
        'Europe/Helsinki' : new Date(2011, 2, 27, 4, 0, 0, 0),
        'Europe/Istanbul' : new Date(2011, 2, 28, 5, 0, 0, 0),
        'Asia/Damascus' : new Date(2011, 3, 1, 2, 0, 0, 0),
        'Asia/Jerusalem' : new Date(2011, 3, 1, 6, 0, 0, 0),
        'Africa/Cairo' : new Date(2010, 3, 30, 4, 0, 0, 0),
        'Asia/Yerevan' : new Date(2011, 2, 27, 4, 0, 0, 0),
        'Asia/Baku'    : new Date(2011, 2, 27, 8, 0, 0, 0),
        'Pacific/Auckland' : new Date(2011, 8, 26, 7, 0, 0, 0),
        'Pacific/Fiji' : new Date(2010, 11, 29, 23, 0, 0, 0),
        'America/Halifax' : new Date(2011, 2, 13, 6, 0, 0, 0),
        'America/Goose_Bay' : new Date(2011, 2, 13, 2, 1, 0, 0),
        'America/Miquelon' : new Date(2011, 2, 13, 5, 0, 0, 0),
        'America/Godthab' : new Date(2011, 2, 27, 1, 0, 0, 0)
    };
}());

$wnd.jstz.olson.ambiguity_list = {
    'America/Denver' : ['America/Denver', 'America/Mazatlan'],
    'America/Chicago' : ['America/Chicago', 'America/Mexico_City'],
    'America/Asuncion' : ['Atlantic/Stanley', 'America/Asuncion', 'America/Santiago', 'America/Campo_Grande'],
    'America/Montevideo' : ['America/Montevideo', 'America/Sao_Paulo'],
    'Asia/Beirut' : ['Asia/Gaza', 'Asia/Beirut', 'Europe/Minsk', 'Europe/Helsinki', 'Europe/Istanbul', 'Asia/Damascus', 'Asia/Jerusalem', 'Africa/Cairo'],
    'Asia/Yerevan' : ['Asia/Yerevan', 'Asia/Baku'],
    'Pacific/Auckland' : ['Pacific/Auckland', 'Pacific/Fiji'],
    'America/Los_Angeles' : ['America/Los_Angeles', 'America/Santa_Isabel'],
    'America/New_York' : ['America/Havana', 'America/New_York'],
    'America/Halifax' : ['America/Goose_Bay', 'America/Halifax'],
    'America/Godthab' : ['America/Miquelon', 'America/Godthab']
};

$wnd.mytimezone = $wnd.jstz.determine_timezone();
    }-*/;

    public native String offset()/*-{
        return $wnd.mytimezone.offset();
    }-*/;

    /** vraci nazev casove zony napr. Europe/London */
    public native String name()/*-{
        return $wnd.mytimezone.name();
    }-*/;

    /** vraci true pokud je daylight save time */
    public native boolean dst()/*-{
        return $wnd.mytimezone.dst();
    }-*/;

    /** vraci offset v minutach */
    public int minutesOffset() {
        String of = offset();
        boolean minus = of.startsWith("-");
        String time = of.substring(1);
        String[] parts = time.split(":");

        try {
            int minutes = 60 * Integer.parseInt(parts[0]);
            minutes += Integer.parseInt(parts[1]);
            return minus? -1 * minutes : minutes;
        } catch(Exception ex) {
            // neco se pokazilo, coz by se nikdy nemelo stat
            return 0;
        }
    }

    /**
     * zjisti offset pro zadane datum a vrati jeho velikost v minutach
     * zohlednuje i
     */
    public native int getDateOffset(int dateTime)/*-{
        var date = new Date(dateTime*1000);
        return $wnd.jstz.get_date_offset(date);
    }-*/;
}
