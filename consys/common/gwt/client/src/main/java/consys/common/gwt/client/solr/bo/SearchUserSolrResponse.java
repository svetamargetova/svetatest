package consys.common.gwt.client.solr.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Vraci zakladni objekty solr odpovedi
 * @author pepa
 */
public class SearchUserSolrResponse extends JavaScriptObject {

    protected SearchUserSolrResponse() {
    }

    /** vraci hlavicku */
    public final native SolrHeader getHeader() /*-{
        return this.responseHeader;
    }-*/;

    /** vraci data odpovedi */
    public final native SolrResponseDataSearchUser getResponse() /*-{
        return this.response;
    }-*/;
}
