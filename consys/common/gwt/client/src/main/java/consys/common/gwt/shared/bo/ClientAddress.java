package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Objekt adresy
 * @author pepa
 */
public class ClientAddress implements IsSerializable {

    // 0 - nothing selected
    private int location;
    private int stateUs;
    private String locationName;
    private String stateUsName;
    private String city;
    private String street;
    private String zip;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public int getStateUs() {
        return stateUs;
    }

    public void setStateUs(int stateUs) {
        this.stateUs = stateUs;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(getStreet());
        if (!city.isEmpty() || !zip.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(city+" ");
            if (!zip.isEmpty()) {
                sb.append(zip);
            }
        }
        if (location > 0) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(locationName);
            if(stateUs > 0){
                sb.append(", "+stateUsName);
            }
        }
        return sb.toString();
    }

    /**
     * @return the locationName
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * @param locationName the locationName to set
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    /**
     * @return the stateUsName
     */
    public String getStateUsName() {
        return stateUsName;
    }

    /**
     * @param stateUsName the stateUsName to set
     */
    public void setStateUsName(String stateUsName) {
        this.stateUsName = stateUsName;
    }



}
