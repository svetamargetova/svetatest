package consys.common.gwt.client;

import com.google.gwt.core.client.GWT;
import consys.common.constants.img.ImageConstants;

/**
 * systemove konstanty
 * @author pepa
 */
public class ServletConstants {

    /** konstanta oznacujici, ze nebyla vybrana zadna hodnota */
    public static final int NOT_SELECT = -1;
    /** url ke korenu aplikace, http://blabla/ */
    public static final String BASE_URL = GWT.getModuleBaseURL();        
    /** profilovy obrazek */
    public static final String USER_PROFILE_IMG_LOAD = ImageConstants.LOAD_IMAGE_SERVLET_PATH;
    public static final String USER_PROFILE_IMG_SAVE = "user/profile/save";
    /** eventovy obrazek - LOGO */
    public static final String EVENT_LOGO_IMG_LOAD = ImageConstants.LOAD_IMAGE_SERVLET_PATH;
    public static final String EVENT_LOGO_IMG_SAVE = "event/logo/save?eid=";
    /** eventovy obrazek - WALL */
    public static final String EVENT_PROFILE_IMG_LOAD = ImageConstants.LOAD_IMAGE_SERVLET_PATH;
    public static final String EVENT_PROFILE_IMG_SAVE = "event/profile/save?eid=";
}
