package consys.common.gwt.client.rpc.mock;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Palo
 */
public class MockFactory implements Serializable{

    private static final long serialVersionUID = -7224309805257219314L;
    private static Map<Class, ActionMock<? extends Result>> mockActionMap = new HashMap<Class, ActionMock<? extends Result>>();

    public static <R extends Result> void addActionMock(Class c, ActionMock<R> mock) {
        mockActionMap.put(c, mock);
    }

    public static <A extends Action<R>, R extends Result> ActionMock<? extends Result> get(A a){
        return mockActionMap.get(a.getClass());
    }
}
