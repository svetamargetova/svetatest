package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event oznamujici zmenu obsahu
 * @author pepa
 */
public class RedirectEvent extends GwtEvent<RedirectEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // vnitrni data    
    private String url;

    
    public RedirectEvent(String url) {
        this.url = url;
    }

    

    /** vraci popisek widgetu */
    public String getUrl() {
        return url;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onRedirect(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onRedirect(RedirectEvent event);
    }
}
