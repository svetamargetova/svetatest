package consys.common.gwt.client.logger.handler;

import com.google.gwt.logging.client.DevelopmentModeLogHandler;
import consys.common.gwt.client.logger.ClassicTextFormatter;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConsysDevelopmentModeLogHandler extends DevelopmentModeLogHandler{

    public ConsysDevelopmentModeLogHandler() {
        super();
        setFormatter(new ClassicTextFormatter(true));
    }
}
