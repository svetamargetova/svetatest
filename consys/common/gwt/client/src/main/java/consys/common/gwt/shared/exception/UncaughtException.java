/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class UncaughtException extends ActionException{
    private static final long serialVersionUID = 2090394507858749138L;

    protected UncaughtException() {}

    public UncaughtException( String message ) {
        super( message );
    }

    public UncaughtException( Throwable cause ) {
        super( cause );
    }

    public UncaughtException( String message, Throwable cause ) {
        super( message, cause );
    }

}
