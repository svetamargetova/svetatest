package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Obecny objekt nahledu, id je string
 * @author pepa
 */
public class CommonThumb implements IsSerializable {

    private String uuid;
    private String name;

    public CommonThumb() {
    }

    public CommonThumb(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
