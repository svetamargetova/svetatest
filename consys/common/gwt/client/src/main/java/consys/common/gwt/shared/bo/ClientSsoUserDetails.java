package consys.common.gwt.shared.bo;

import consys.common.constants.sso.SSO;
import consys.common.gwt.shared.action.Result;

/**
 *
 * @author palo
 */
public class ClientSsoUserDetails implements Result {

    private static final long serialVersionUID = -7909245899176641487L;
    // data
    private Long ssoId;
    private String ssoStringId;
    private SSO fromSso;
    /** Informacie o registracii */
    private String firstName;
    private String middleName;
    private String lastName;
    private String loginEmail;
    private String bio;
    private String profilePictureUrl;
    private String affiliation;
    private String password;
    private String userUuid;
    /*
     * 0 - not spec.
     * 1 - man
     * 2 - woman
     */
    private int gender = 0;

    public Long getSsoId() {
        return ssoId;
    }

    public void setSsoId(Long ssoId) {
        this.ssoId = ssoId;
    }

    public String getSsoStringId() {
        return ssoStringId;
    }

    public void setSsoStringId(String ssoStringId) {
        this.ssoStringId = ssoStringId;
    }

    public SSO getFromSso() {
        return fromSso;
    }

    public void setFromSso(SSO fromSso) {
        this.fromSso = fromSso;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return firstName + " " + middleName + " " + lastName + " " + loginEmail;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /** uuid uzivatele pokud jiz v takeplace existuje */
    public String getUserUuid() {
        return userUuid;
    }

    /** uuid uzivatele pokud jiz v takeplace existuje */
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }
}
