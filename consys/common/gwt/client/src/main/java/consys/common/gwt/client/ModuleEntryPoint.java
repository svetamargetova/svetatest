/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.gwt.client;

import com.google.gwt.core.client.EntryPoint;

/**
 *
 * @author palo
 */
public abstract class ModuleEntryPoint implements EntryPoint{

    @Override
    public void onModuleLoad(){
        initMocks();
        initLayout();
    }

    public abstract void initMocks() ;

    public abstract void initLayout() ;
        
}
