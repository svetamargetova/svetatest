package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;

/**
 * Objekt reprezentuje obecny event. Tento objekt obsahuje vsecky dulezite
 * vlastnosti eventu v jeho zakladu.
 *
 *  Organizatory sa musia dotiahnut extra
 *
 * @author pepa
 */
public class ClientEvent implements IsSerializable, Result {

    private static final long serialVersionUID = -2805537463195143775L;
    private String uuid;
    // Identifikacia konferencie
    private String acronym;
    private String fullName;
    private String shortName;
    private String universalName;
    private String web;
    private String logoUuidPrefix;
    private String profileLogoUuidPrefix;
    private int year;
    private String series;
    private ClientEventType type;
    private ClientDateTime from;
    private ClientDateTime to;
    private String city;
    private String street;
    private String place;
    private Integer location;
    private Integer usState;
    private boolean visible;
    private String tags;
    private String description;

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the acronym
     */
    public String getAcronym() {
        return acronym;
    }

    /**
     * @param acronym the acronym to set
     */
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the universalName
     */
    public String getUniversalName() {
        return universalName;
    }

    /**
     * @param universalName the universalName to set
     */
    public void setUniversalName(String universalName) {
        this.universalName = universalName;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return the logoUuidPrefix
     */
    public String getLogoUuidPrefix() {
        return logoUuidPrefix;
    }

    /**
     * @param logoUuidPrefix the logoUuidPrefix to set
     */
    public void setLogoUuidPrefix(String logoUuidPrefix) {
        this.logoUuidPrefix = logoUuidPrefix;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the series
     */
    public String getSeries() {
        return series;
    }

    /**
     * @param series the series to set
     */
    public void setSeries(String series) {
        this.series = series;
    }

    /**
     * @return the type
     */
    public ClientEventType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(ClientEventType type) {
        this.type = type;
    }

    /**
     * @return the from
     */
    public ClientDateTime getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(ClientDateTime from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public ClientDateTime getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(ClientDateTime to) {
        this.to = to;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the location
     */
    public Integer getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Integer location) {
        this.location = location;
    }

    /**
     * @return the visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * @return the tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(String tags) {
        this.tags = tags;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the usState
     */
    public Integer getUsState() {
        return usState;
    }

    /**
     * @param usState the usState to set
     */
    public void setUsState(Integer usState) {
        this.usState = usState;
    }

    /**
     * @return the profileLogoUuidPrefix
     */
    public String getProfileLogoUuidPrefix() {
        return profileLogoUuidPrefix;
    }

    /**
     * @param profileLogoUuidPrefix the profileLogoUuidPrefix to set
     */
    public void setProfileLogoUuidPrefix(String profileLogoUuidPrefix) {
        this.profileLogoUuidPrefix = profileLogoUuidPrefix;
    }
}
