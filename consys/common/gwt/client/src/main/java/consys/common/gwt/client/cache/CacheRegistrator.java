package consys.common.gwt.client.cache;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.shared.action.Action;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Palo
 */
public class CacheRegistrator {

    private static final Map<Class,CacheHandler> cacheHandlers;
    static{
        cacheHandlers = new HashMap<Class, CacheHandler>(100);
    }

    public static void register(Class<? extends Action> clazz,CacheHandler handler){
        cacheHandlers.put(clazz, handler);
    }

    public static void unregister(Class<? extends Action> clazz){
        cacheHandlers.remove(clazz);
    }

    public static boolean hasCacheHandler(Action a){
        return cacheHandlers.containsKey(a.getClass());
    }

    public static CacheHandler getCacheHandler(Action a){
        return cacheHandlers.get(a.getClass());
    }
 
     /** interface handleru pro cachovani objektu. */
    public interface CacheHandler<T extends Action> {
        
        public void doAction(final T action, final AsyncCallback callback, final ActionExecutionDelegate delegate);
    }
}
