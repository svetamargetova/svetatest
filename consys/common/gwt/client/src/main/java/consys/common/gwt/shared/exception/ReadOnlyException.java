package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class ReadOnlyException extends ActionException{
    private static final long serialVersionUID = 5597105428080324560L;

    

    public ReadOnlyException() {
        super("");
    }

    public ReadOnlyException(String message) {
        super(message);
    }

    public ReadOnlyException(Throwable cause) {
        super(cause);
    }

    public ReadOnlyException(String message, Throwable cause) {
        super(message, cause);
    }

}
