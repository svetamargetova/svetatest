package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;

/**
 * Objekt pro odeslani registrace uzivatele
 * @author pepa
 */
public class ClientRegistration implements IsSerializable, Result {

    private static final long serialVersionUID = 88785141881272552L;
    // data
    private String firstName;
    private String lastName;
    private String affiliation;
    private String contactEmail;
    private String password;
    private String confirmPassword;

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
