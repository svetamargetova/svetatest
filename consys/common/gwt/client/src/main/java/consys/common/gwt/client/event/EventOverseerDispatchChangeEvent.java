package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 * Event ktory sa vystreli ked je potreba inicializovat pripojenie na overseera ale 
 * bez informacii o evente. Pouzitie je pre OuterRegistration
 * @author Palo
 */
public class EventOverseerDispatchChangeEvent extends GwtEvent<EventOverseerDispatchChangeEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    private String eventUuid;
    private String overseerUuid;

    public EventOverseerDispatchChangeEvent() {
    }

    public EventOverseerDispatchChangeEvent(String eventUuid, String overseerUuid) {
        this.eventUuid = eventUuid;
        this.overseerUuid = overseerUuid;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public String getOverseerUuid() {
        return overseerUuid;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onOverseerDispatchChange(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onOverseerDispatchChange(EventOverseerDispatchChangeEvent event);
    }
}
