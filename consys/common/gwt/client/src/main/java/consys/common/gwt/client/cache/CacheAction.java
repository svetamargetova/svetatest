package consys.common.gwt.client.cache;

/**
 * Interface, ktery musí implementova objekty, které chteji pouzivat data v cache
 * @author pepa
 */
public interface CacheAction<T> {

    /** vraci nazev promenne v cache */
    public String getName();

    /** provede akci po vraceni promenne z cache */
    public void doAction(T value);

    /** provede akci, kdyz vyprsi cas pro zinicializovani promenne */
    public void doTimeoutAction();

    /** @return true pokud byla pozadovana akce v cache zrusena */
    public boolean isCancel();
}
