/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.gwt.client.rpc.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;

public interface DispatchServiceAsync {

    /**
     * Executes the specified action.
     *
     * @param action The action to execute.
     * @param callback The callback to execute once the action completes.
     *
     * @see net.customware.gwt.dispatch.server.Dispatch
     */
    void execute( Action<?> action, AsyncCallback<Result> callback );
}
