package consys.common.gwt.shared.action;

import java.io.Serializable;

/**
 * A marker interface for {@link Action} results.
 * 
 * @author David Peterson
 */
public interface Result extends Serializable {
}
