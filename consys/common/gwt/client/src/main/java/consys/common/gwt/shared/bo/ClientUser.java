package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;

/**
 * Objekt uzivatele
 * @author pepa
 */
public class ClientUser implements IsSerializable, Result {

    private static final long serialVersionUID = 2893943405233552765L;
    private String uuid;
    private String uuidImg;
    private String firstName;
    private String middleName;
    private String lastName;
    private String frontDegree;
    private String rearDegree;
    private String loginEmail;
    private boolean activatedUser;
    /* Obsahuje len short info */
    private String userOrgDefault;
    /*  Bio */
    private String bio;
    /*
     * 0 - not spec.
     * 1 - man
     * 2 - woman
     */
    private int gender = 0;

    public ClientUser() {
    }

    public boolean isActivatedUser() {
        return activatedUser;
    }

    public void setActivatedUser(boolean activatedUser) {
        this.activatedUser = activatedUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFrontDegree() {
        return frontDegree;
    }

    public void setFrontDegree(String frontDegree) {
        this.frontDegree = frontDegree;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getRearDegree() {
        return rearDegree;
    }

    public void setRearDegree(String rearDegree) {
        this.rearDegree = rearDegree;
    }

    public String getUserOrgDefault() {
        return userOrgDefault;
    }

    public void setUserOrgDefault(String userOrgDefault) {
        this.userOrgDefault = userOrgDefault;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuidImg() {
        return uuidImg;
    }

    public void setUuidImg(String uuidImg) {
        this.uuidImg = uuidImg;
    }

    /**
     * @return the bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * @param bio the bio to set
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * @return the gender
     */
    public int getGender() {
        return gender;
    }

    /**
     * 
     * 0 - not spec.
     * 1 - man
     * 2 - woman
     *
     * @param gender the gender to set
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    /** vraci jmeno a prostredni jmeno */
    public String firstPartName() {
        String middle = " ";
        if (middleName != null && !middleName.equals("")) {
            middle += middleName + " ";
        }
        return firstName + middle;
    }

    /** vraci jmeno, prostredni jmeno a prijmeni */
    public String name() {
        return firstPartName() + lastName;
    }
}
