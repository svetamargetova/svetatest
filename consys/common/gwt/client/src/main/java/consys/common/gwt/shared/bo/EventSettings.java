package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Palo
 */
public class EventSettings implements Result{
    private static final long serialVersionUID = 6913830429619321359L;

    private HashMap<String,String> systemProperties;
    private ArrayList<String> roles;
    private String userUuid;

    public EventSettings() {
        roles = new ArrayList<String>();
        systemProperties = new HashMap<String, String>();
    }


    

    /**
     * @return the systemProperties
     */
    public HashMap<String, String> getSystemProperties() {
        return systemProperties;
    }

    /**
     * @param systemProperties the systemProperties to set
     */
    public void setSystemProperties(HashMap<String, String> systemProperties) {
        this.systemProperties = systemProperties;
    }

    /**
     * @return the roles
     */
    public ArrayList<String> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }

    /**
     * @return the userUuid
     */
    public String getUserUuid() {
        return userUuid;
    }

    /**
     * @param userUuid the userUuid to set
     */
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }



}
