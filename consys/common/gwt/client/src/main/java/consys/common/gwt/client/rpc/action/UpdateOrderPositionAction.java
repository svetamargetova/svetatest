package consys.common.gwt.client.rpc.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Abstraktni trida pro akci, ktera meni poradi polozek
 * @author pepa
 */
public abstract class UpdateOrderPositionAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -8765630239607190854L;
    // data
    private int oldPosition;
    private int newPosition;

    public UpdateOrderPositionAction() {
    }

    public UpdateOrderPositionAction(int oldPosition, int newPosition) {
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
    }

    public int getNewPosition() {
        return newPosition;
    }

    public int getOldPosition() {
        return oldPosition;
    }
}
