package consys.common.gwt.client.module;

/**
 * interface pro spusteni asynchronniho dotazeni kodu
 * @author pepa
 */
public interface Asyncable {

    /** spusteni dotazeni asynchronni casti kodu */
    public void runComponent();
}
