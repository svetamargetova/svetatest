/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.client.rpc.action;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;

/**
 *
 * @author palo
 */
public class LoadSsoUserDetailsAction implements Action<ClientSsoUserDetails>{
    
    private String token;

    public LoadSsoUserDetailsAction() {
    }

    public LoadSsoUserDetailsAction(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
    
    
    
    
    
}
