package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;

/**
 * Obejekt uchovavajuci informacie ktore su potrebne k inicializacii eventu z
 * overseera. Tento objekt je pristupny z cache.
 * @author Palo
 */
public class ClientCurrentEvent implements Result {

    private static final long serialVersionUID = 163501942701678788L;
    // data
    private String overseerUrl;
    private boolean justVisitor;
    private ClientEvent event;

    public String getCurrentEventSystemName() {
        return event.getAcronym() + " " + event.getYear();
    }

    /**
     * @return the overseerUrl
     */
    public String getOverseerUrl() {
        return overseerUrl;
    }

    /**
     * @param overseerUrl the overseerUrl to set
     */
    public void setOverseerUrl(String overseerUrl) {
        this.overseerUrl = overseerUrl;
    }

    /**
     * @return the event
     */
    public ClientEvent getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(ClientEvent event) {
        this.event = event;
    }

    /**
     * @return the justVisitor
     */
    public boolean isJustVisitor() {
        return justVisitor;
    }

    /**
     * @param justVisitor the justVisitor to set
     */
    public void setJustVisitor(boolean justVisitor) {
        this.justVisitor = justVisitor;
    }
}
