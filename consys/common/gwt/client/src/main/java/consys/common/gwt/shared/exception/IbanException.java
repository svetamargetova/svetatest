package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class IbanException extends ActionException {

    private static final long serialVersionUID = -6331817380354178617L;

    public IbanException() {
        super();
    }
}
