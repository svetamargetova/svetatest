package consys.common.gwt.client.logger.handler;

import com.google.gwt.logging.client.ConsoleLogHandler;
import consys.common.gwt.client.logger.ClassicTextFormatter;

/**
 * Pretazeny ConsoleLogHandler handler. Nastavenie vlastneho message formaterru
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConsysConsoleLogHandler extends ConsoleLogHandler{

    public ConsysConsoleLogHandler() {
        super();
        setFormatter(new ClassicTextFormatter(true));
    }
    
}
