package consys.common.gwt.client.rpc.action.cache;

import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.rpc.result.SelectBoxData;

/**
 * abstraktni cache akce pro provadeni akci se seznamem statu usa
 * @author pepa
 */
public abstract class UsStateCacheAction implements CacheAction<SelectBoxData> {

    // konstanty
    public static final String US_STATES = "us_states";

    @Override
    public String getName() {
        return US_STATES;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
