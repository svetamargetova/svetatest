package consys.common.gwt.client.rpc.mock;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.shared.action.Result;

/**
 *
 * @author Palo
 */
public class ActionMock<R extends Result> {

    private R result;
    private Throwable throwable;

    public ActionMock(R result) { 
        this.result = result;
    }

    public ActionMock(Throwable throwable) {
        this.throwable = throwable;
    }

    public void doAction(AsyncCallback<R> callback){
        if(result != null){
            callback.onSuccess(result);
        }else{
            callback.onFailure(throwable);
        }
    }



}
