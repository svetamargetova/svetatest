package consys.common.gwt.client.action;

/**
 * Enum obecnych chyb
 * @author pepa
 */
public enum CommonExceptionEnum {

    BAD_INPUT, SERVICE_FAILED, NOT_PERMITTED;
    private static final long serialVersionUID = 3002122663505588148L;
}
