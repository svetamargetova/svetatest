package consys.common.gwt.client;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class EventBus extends SimpleEventBus {

    private static final Logger logger = LoggerFactory.getLogger(EventBus.class);
    private static final EventBus eventBus = new EventBus();
    /** lokalna cache handlerov z dovodu opakovaneho odstranenia co sposobi assertion error */
    private Map<EventHandler, HandlerRegistration> handlers;

    private EventBus() {
        super();
        handlers = new HashMap<EventHandler, HandlerRegistration>();
    }

    @Override
    public <H extends EventHandler> HandlerRegistration addHandler(Type<H> type, H handler) {
        HandlerRegistration registration = super.addHandler(type, handler);
        handlers.put(handler, registration);
        return registration;
    }

    public <H extends EventHandler> void removeHandler(Type<H> type, H handler) {
        HandlerRegistration reg = handlers.get(handler);
        if (reg != null) {
            reg.removeHandler();
            handlers.remove(handler);
        }
    }

    @Override
    public void fireEvent(GwtEvent<?> event) {
        String name = event.getClass().getName();
        if (name.lastIndexOf('.') > 0) {
            name = name.substring(name.lastIndexOf('.') + 1);
        }
        logger.debug("fire -> " + name);
        super.fireEvent(event);
    }

    /** zkraceni volani get().fireEvent(event) */
    public static void fire(GwtEvent<?> event) {
        get().fireEvent(event);
    }

    public static EventBus get() {
        return eventBus;
    }
}
