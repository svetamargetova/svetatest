package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Obecny objekt nahledu, id je long
 * @author pepa
 */
public class CommonLongThumb implements IsSerializable {

    // data
    private Long id;
    private String name;

    public CommonLongThumb() {
    }

    public CommonLongThumb(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
