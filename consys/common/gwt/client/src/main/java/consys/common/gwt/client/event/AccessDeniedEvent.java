package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event oznamujici ze byla pozadovana akce, ke ktere nema uzivatel opravneni
 * @author pepa
 */
public class AccessDeniedEvent extends GwtEvent<AccessDeniedEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onAccessDenied(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onAccessDenied(AccessDeniedEvent event);
    }
}
