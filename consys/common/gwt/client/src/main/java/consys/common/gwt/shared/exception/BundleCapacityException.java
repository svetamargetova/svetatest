package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class BundleCapacityException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public BundleCapacityException() {
        super("");
    }

    public BundleCapacityException(String message) {
        super(message);
    }

    public BundleCapacityException(Throwable cause) {
        super(cause);
    }

    public BundleCapacityException(String message, Throwable cause) {
        super(message, cause);
    }
}
