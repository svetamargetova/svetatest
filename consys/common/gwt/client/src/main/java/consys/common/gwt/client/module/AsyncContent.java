package consys.common.gwt.client.module;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * abstraktni trida obaloveho objektu pro asynchronni cast kodu
 * @author pepa
 */
public abstract class AsyncContent extends Composite implements Asyncable {

    public AsyncContent() {
        initWidget(getLoadingWidget());
    }

    /** vraci widget s informaci o nacitani modulu */
    public Widget getLoadingWidget() {
        return new Label("Loading module ...");
    }
}
