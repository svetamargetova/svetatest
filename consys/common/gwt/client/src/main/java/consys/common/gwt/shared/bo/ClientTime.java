package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Objekt casu (0:0 = 0, 0:30 = 30, 7:45 = 465)
 * @author pepa
 */
public class ClientTime implements IsSerializable {

    private int time;
    private int minutes;
    private int hours;

    public ClientTime() {
    }

    public ClientTime(int time) {
        this.time = time;
        this.minutes = time % 60;
        this.hours = time / 60;
    }

    public ClientTime(int hours, int minutes) {
        this.time = (hours * 60) + minutes;
        this.minutes = minutes;
        this.hours = hours;
    }

    public int getTime() {
        return time;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    @Override
    public String toString() {
        return getHours() + ":" + (getMinutes() < 10 ? "0" + getMinutes() : getMinutes());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientTime other = (ClientTime) obj;
        if (this.time != other.time) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.time;
        return hash;
    }
}
