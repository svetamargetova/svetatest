package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;

/**
 *
 * @author pepa
 */
public class ClientPersonalInfo implements Result {

    private static final long serialVersionUID = 1189742913743075365L;
    // data
    private boolean celiac;
    private boolean vegetarian;

    public boolean isCeliac() {
        return celiac;
    }

    public void setCeliac(boolean celiac) {
        this.celiac = celiac;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }
}
