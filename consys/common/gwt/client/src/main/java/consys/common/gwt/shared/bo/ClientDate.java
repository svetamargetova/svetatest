package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author pepa
 */
public class ClientDate implements IsSerializable {

    // konstanty
    public static final int DEFAULT_YEAR = 1970;
    // data
    private int day;
    private int year;
    private int month;

    public ClientDate() {
        day = 1;
        month = 1;
        year = DEFAULT_YEAR;
    }

    public ClientDate(int day, int month, int year) {
        if (!checkValues(day, month, year)) {
            throw new IllegalArgumentException();
        }
        this.day = day;
        this.month = month;
        this.year = year;
    }

    private boolean checkValues(int day, int month, int year) {
        return (dayCheck(day) && monthCheck(month) && yearCheck(year));
    }

    private boolean dayCheck(int day) {
        return (day >= 1 && day <= 31);
    }

    private boolean monthCheck(int month) {
        return (month >= 1 && month <= 12);
    }

    private boolean yearCheck(int year) {
        return (year >= 1970);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(day);
        sb.append(". ");
        sb.append(month);
        sb.append(". ");
        sb.append(year);
        return sb.toString();
    }
}
