package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;

/**
 * Platebni profil
 * @author pepa
 */
public class ClientUserPaymentProfile extends ClientPaymentProfileThumb implements Result {

    private static final long serialVersionUID = 2189135147023282329L;
    // data
    private String city;
    private String street;
    private String zip;
    private Integer country;
    private String regNo;
    private String vatNo;
    private String accountNo;
    private String bankNo;
    private boolean noVatPayer;
    private String country2ch;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public String getCountry2ch() {
        return country2ch;
    }

    public void setCountry2ch(String country2ch) {
        this.country2ch = country2ch;
    }

    public boolean isNoVatPayer() {
        return noVatPayer;
    }

    public void setNoVatPayer(boolean noVatPayer) {
        this.noVatPayer = noVatPayer;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getVatNo() {
        return vatNo;
    }

    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
