package consys.common.gwt.client.rpc.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.gwt.shared.bo.ProductThumb;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Akce pro registraci uzivatele a jeho automatickou registraci do zvoleneho bundlu v eventu
 * @author pepa
 */
public class CreateOuterRegisterAction implements Action<VoidResult> {

    private static final long serialVersionUID = 2091967965810596604L;
    // data
    private String apiKey;
    private String eventUuid;
    private String packageUuid;
    private ClientSsoUserDetails registration;
    private ClientUserPaymentProfile paymentProfile;
    private String discountTicket;
    private int quantity;
    private ArrayList<ProductThumb> subbundles;
    private HashMap<String, String> answers;

    public CreateOuterRegisterAction() {
        subbundles = new ArrayList<ProductThumb>();
        answers = new HashMap<String, String>();
    }

    public HashMap<String, String> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<String, String> answers) {
        this.answers = answers;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getDiscountTicket() {
        return discountTicket;
    }

    public void setDiscountTicket(String discountTicket) {
        this.discountTicket = discountTicket;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getPackageUuid() {
        return packageUuid;
    }

    public void setPackageUuid(String packageUuid) {
        this.packageUuid = packageUuid;
    }

    public ClientUserPaymentProfile getPaymentProfile() {
        return paymentProfile;
    }

    public void setPaymentProfile(ClientUserPaymentProfile paymentProfile) {
        this.paymentProfile = paymentProfile;
    }

    public ClientSsoUserDetails getRegistration() {
        return registration;
    }

    public void setRegistration(ClientSsoUserDetails registration) {
        this.registration = registration;
    }

    public ArrayList<ProductThumb> getSubbundles() {
        return subbundles;
    }

    public void setSubbundles(ArrayList<ProductThumb> subbundles) {
        this.subbundles = subbundles;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }
}
