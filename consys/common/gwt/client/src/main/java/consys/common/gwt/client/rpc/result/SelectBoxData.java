package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Palo
 */
public class SelectBoxData implements Serializable,Result {

    private static final long serialVersionUID = 12149222164574280L;

    private ArrayList<BoxItem> list;

    public SelectBoxData() {
        list = new ArrayList<BoxItem>();
    }

    public ArrayList<BoxItem> getList() {
        return list;
    }
}
