package consys.common.gwt.client.utils;

import java.util.List;


/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUtils {
    
    /**
     * <p>Checks if an list of Objects is empty or <code>null</code>.</p>
     *
     * @param array  the array to test
     * @return <code>true</code> if the list is empty or <code>null</code>
     * @since 2.1
     */
    public static boolean isEmpty(List list) {
        if (list == null || list.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean isNotEmpty(List list){
        return !isEmpty(list);
    }


}
