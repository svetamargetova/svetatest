package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Objekt datumu a casu
 * @author pepa
 */
public class ClientDateTime implements IsSerializable {

    // data
    private ClientTime time;
    private ClientDate date;

    public ClientDate getDate() {
        if (date == null) {
            date = new ClientDate();
        }
        return date;
    }

    public void setDate(ClientDate date) {
        this.date = date;
    }

    public ClientTime getTime() {
        if (time == null) {
            time = new ClientTime();
        }
        return time;
    }

    public void setTime(ClientTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return getDate().toString() + " " + getTime().toString();
    }
}
