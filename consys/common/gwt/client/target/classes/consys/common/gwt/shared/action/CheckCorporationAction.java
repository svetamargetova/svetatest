package consys.common.gwt.shared.action;

import consys.common.gwt.client.rpc.result.BooleanResult;

/**
 *
 * @author pepa
 */
public class CheckCorporationAction implements Action<BooleanResult> {

    private static final long serialVersionUID = -1136721032646466996L;
    // data
    private String prefix;

    public CheckCorporationAction() {
    }

    public CheckCorporationAction(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}
