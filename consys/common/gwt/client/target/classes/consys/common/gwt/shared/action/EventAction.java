package consys.common.gwt.shared.action;

/**
 *
 * @author Palo
 */
public abstract class EventAction<T extends Result> implements Action<T>{
    private static final long serialVersionUID = -5385821496218146304L;

    private String userEventUuid;
    private String eventUuid;

    public EventAction() {
    }

    /**
     * @return the userEventUuid
     */
    public String getUserEventUuid() {
        return userEventUuid;
    }

    /**
     * @param userEventUuid the userEventUuid to set
     */
    public void setUserEventUuid(String userEventUuid) {
        this.userEventUuid = userEventUuid;
    }

    /**
     * @return the eventUuid
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * @param eventUuid the eventUuid to set
     */
    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }



}
