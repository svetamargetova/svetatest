package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 *
 * @author pepa
 */
public class UserLoginEvent extends GwtEvent<UserLoginEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onUserLogin(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onUserLogin(UserLoginEvent event);
    }
}
