package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;

/**
 * Jedna polozka zalozkoveho panelu pro dotahovani az na vybrani zalozky
 * @author pepa
 */
public class ClientTabItem implements IsSerializable, Result {

    private static final long serialVersionUID = 5505939824648190673L;
    // data
    private String uuid;
    private String title;

    public ClientTabItem() {
    }

    public ClientTabItem(String uuid, String title) {
        this.uuid = uuid;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
