package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Jeden tag / topic
 * @author pepa
 */
public class ClientTag implements IsSerializable {

    private Long id;
    private String name;

    public ClientTag() {
    }

    public ClientTag(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
