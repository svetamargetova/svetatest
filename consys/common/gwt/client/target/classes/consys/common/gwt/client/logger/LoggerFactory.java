package consys.common.gwt.client.logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;
import consys.common.gwt.client.logger.handler.ConsysConsoleLogHandler;
import consys.common.gwt.client.logger.handler.ConsysDevelopmentModeLogHandler;
import consys.common.gwt.client.logger.handler.ConsysFirebugLogHandler;
import consys.common.gwt.client.logger.handler.ConsysSystemLogHandler;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoggerFactory implements UncaughtExceptionHandler{

    private static final ConsysConsoleLogHandler CONSOLE_LOG_HANDLER = GWT.create(ConsysConsoleLogHandler.class);
    private static final ConsysDevelopmentModeLogHandler DEVELOPMENT_MODE_LOG_HANDLER = GWT.create(ConsysDevelopmentModeLogHandler.class);
    private static final ConsysFirebugLogHandler FIREBUG_LOG_HANDLER = GWT.create(ConsysFirebugLogHandler.class);
    private static final ConsysSystemLogHandler SYSTEM_LOG_HANDLER = GWT.create(ConsysSystemLogHandler.class);

    private static final Logger sharedLogger = getLogger("Shared");

    private static final LoggerFactory factory = new LoggerFactory();

    private LoggerFactory() {
        GWT.setUncaughtExceptionHandler(this);
    }

    /**
     * Zdielany logger z vykonostnych dovodov
     */
    public static void log(Class clazz,String message){
        sharedLogger.debug(getClassName(clazz)+" : "+message);
    }

    public static Logger getLogger(String name){
        return setUpHandlers(new Logger(name));
    }

    public static Logger getLogger(Class clazz){
        String name = getClassName(clazz);
        return getLogger(name+".java");
    }

    private static String getClassName(Class clazz) {
        String name = clazz.getName();
        if (name.lastIndexOf('.') > 0) {
            name = name.substring(name.lastIndexOf('.') + 1);
        }
        return name;
    }

    private static Logger setUpHandlers(Logger logger){
        logger.logger.addHandler(CONSOLE_LOG_HANDLER);
        logger.logger.addHandler(DEVELOPMENT_MODE_LOG_HANDLER);
        logger.logger.addHandler(FIREBUG_LOG_HANDLER);
        logger.logger.addHandler(SYSTEM_LOG_HANDLER);
        return logger;
    }

    @Override
    public void onUncaughtException(Throwable e) {
        sharedLogger.error("UncaughtExceptionHandler", e);
    }

}
