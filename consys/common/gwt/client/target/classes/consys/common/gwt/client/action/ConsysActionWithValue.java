package consys.common.gwt.client.action;

/**
 * Obecny interface objektu se spustenim akce, ktera muze predavat parametr
 * @author pepa
 */
public interface ConsysActionWithValue<T> {

    /** spusteni akce */
    public void run(T data);
}
