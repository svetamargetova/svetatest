package consys.common.gwt.client.module;

import com.google.gwt.user.client.ui.Widget;

/**
 * Interface, ktery definuje potrebne metody pro moduly
 * @author pepa
 */
public interface Module {
       
    /**
     * Inicializacia modulu. Malo by obsahovat v pripade potreby zaregistrovanie
     * potrebnych konstant a handlerov ktore su dostupne z obidvoch modov.
     */
    public void initializeModule(ModuleMenuRegistrator menuRegistrator);

    /**
     * Registracia modulu volana hned po prihlaseni uzivatela. Modul by mal za-
     * registrovat handlere a history tokeny
     */
    public void registerModule();

    /**
     * Modul by si tu mal <b>ukludit</b>, tj. odregitrovat handlere a history
     * tokeny.
     */
    public void unregisterModule();

    public class ModuleMenuItem implements Comparable<ModuleMenuItem> {

        private Widget widget;
        private String title;
        private String token;
        private int index;

        public ModuleMenuItem(Widget widget, String title, String token, int index) {
            this.widget = widget;
            this.title = title;
            this.token = token;
            this.index = index;
        }

        /**
         * @return the widget
         */
        public Widget getWidget() {
            return widget;
        }

        /**
         * @return the title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @return the token
         */
        public String getToken() {
            return token;
        }

        /**
         * @return the index
         */
        public int getIndex() {
            return index;
        }

        @Override
        public int compareTo(ModuleMenuItem o) {
            if (index < o.getIndex()) {
                return -1;
            } else if (index == o.getIndex()) {
                return 0;
            } else {
                return 1;
            }
        }
    }
}
