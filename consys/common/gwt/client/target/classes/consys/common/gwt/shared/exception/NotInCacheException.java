package consys.common.gwt.shared.exception;

/**
 * Vyjimka vyhozena, kdyz cache neobsahuje pozadovany klic
 * @author pepa
 */
public class NotInCacheException extends Exception {

    private static final long serialVersionUID = -301595883048424936L;

    public NotInCacheException() {
        super();
    }

    public NotInCacheException(String message) {
        super(message);
    }
}
