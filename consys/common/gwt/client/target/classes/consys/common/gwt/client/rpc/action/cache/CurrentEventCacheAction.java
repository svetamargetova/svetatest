package consys.common.gwt.client.rpc.action.cache;

import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.shared.bo.ClientCurrentEvent;

/**
 * Cache action urcena k nacitani aktualneho eventu
 * @author Palo
 */
public abstract class CurrentEventCacheAction implements CacheAction<ClientCurrentEvent> {
    // konstanty
    public static final String CURRENT_EVENT = "current_event";

    @Override
    public String getName() {
        return CURRENT_EVENT;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
