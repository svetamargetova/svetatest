package consys.common.gwt.client.action;

/**
 * Obecny interface objektu se spustenim akce
 * @author pepa
 */
public interface ConsysAction {

    /** spusteni akce */
    public void run();
}
