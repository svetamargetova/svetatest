package consys.common.gwt.client.module.event;

import consys.common.gwt.client.action.ConsysAction;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * EventNavigationItem predstavuje zaregistrovanu entitu do PanelAddon casti
 * pravej navigacie. Viditelnost je vyhodnotena na zaklade roli ktore musi
 * uzivatel vlastnit alebo implementaciou {@link EventNavigationItemVisiblityResolver}a.
 * <p/>
 * Viditelnost navigacneho itemu je zavisla na tom ci je item hlavny (modulovy)
 * alebo je pod-item. Pre pod-item je primarne pouzity visibility resolver
 * nasledne ak ho nema tak role.
 * <p/>
 * Pre hlavny item sa role pouzivaju na viditelnost nastavovacieho kolecka.
 * Visivility resolver je urceny pre viditelnost celeho modulu.
 * <p/>
 * Kazdy navigacny item obsahuje urlName, teda nazov podla ktoreho je automaticky
 * mozene zobrazit si obsah daneho itemu. V pripade ze viac itemov obsahuje roznaky
 * urlName tak sa vybere prvy na ktory sa narazi. 
 *
 *
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventNavigationItem implements Comparable<EventNavigationItem> {

    private static final NoneVisibilityResolver NONE_RESOLVER = new NoneVisibilityResolver();
    private final String title;
    private String urlName;
    private String[] roles;
    private ConsysAction action;
    private int index;
    private boolean directory;
    /**
     * Priznak ci je cela zlozka viditelna. Ak nie je potom nie je viditelny
     * ziaden prvok zlozky bez ohladu na vyhodnotenie roli
     */
    private boolean showDirectory;
    private List<EventNavigationItem> childs;
    private boolean visible;
    private EventNavigationItemVisiblityResolver visiblityResolver;

    /**
     * Konstruktor ktory vytvori regularny Item predstavujuci spustielnu polozku
     * vyuzivajuc na vyhodnotenie podmienky {@link EventNavigationItemVisiblityResolver}
     */
    public EventNavigationItem(String title, int index, ConsysAction action, EventNavigationItemVisiblityResolver navigationItemVisiblityResolver) {
        this(title,null, action, index, false, navigationItemVisiblityResolver);
    }

   
    
    public EventNavigationItem(String title,String urlFragment, int index, ConsysAction action, String... roles) {
        this(title, urlFragment, action, index, false, NONE_RESOLVER, roles);
    }
    
    
    public EventNavigationItem(String title, int index, ConsysAction action, String... roles) {
        this(title, null, action, index, false, NONE_RESOLVER, roles);
    }

    /**
     * Konstruktor ktory vytvori Item predstavujuci Directory
     */
    public EventNavigationItem(String title, int index, ConsysAction action, boolean directory, String... roles) {
        this(title, null, action, index, directory, NONE_RESOLVER, roles);
    }

    public EventNavigationItem(String title, int index, ConsysAction action, boolean directory, EventNavigationItemVisiblityResolver resolver) {
        this(title, null, action, index, directory, resolver);
    }

    public EventNavigationItem(String title, String urlName, ConsysAction action, int index, boolean directory, EventNavigationItemVisiblityResolver resolver, String... roles) {
        this.title = title;
        this.roles = roles;
        this.action = action;
        this.index = index;
        this.directory = directory;
        this.childs = new ArrayList<EventNavigationItem>();
        this.visiblityResolver = resolver;
        this.urlName = urlName;
    }

    public boolean hasResolver() {
        return getVisiblityResolver() != NONE_RESOLVER;
    }

    void addChild(EventNavigationItem child) {
        childs.add(child);
    }

    public String getTitle() {
        return title;
    }

    public boolean isDirectory() {
        return directory;
    }

    public boolean isItem() {
        return !directory;
    }

    public List<EventNavigationItem> getChilds() {
        return childs;
    }

    public String[] getRoles() {
        return roles;
    }

    public ConsysAction getAction() {
        return action;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @return the visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public int compareTo(EventNavigationItem o) {
        if (getIndex() < o.getIndex()) {
            return -1;
        } else if (getIndex() == o.getIndex()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!obj.getClass().equals(EventNavigationItem.class)) {
            return false;
        }
        EventNavigationItem item = (EventNavigationItem) obj;

        return title.equalsIgnoreCase(item.title);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.title != null ? this.title.hashCode() : 0);
        return hash;
    }

    /**
     * @return the visiblityResolver
     */
    public EventNavigationItemVisiblityResolver getVisiblityResolver() {
        return visiblityResolver;
    }

    /**
     * Priznak ci je cela zlozka viditelna. Ak nie je potom nie je viditelny
     * ziaden prvok zlozky bez ohladu na vyhodnotenie roli
     *
     * @return the showDirectory
     */
    public boolean isShowDirectory() {
        return showDirectory;
    }

    /**
     * Priznak ci je cela zlozka viditelna. Ak nie je potom nie je viditelny
     * ziaden prvok zlozky bez ohladu na vyhodnotenie roli
     *
     * @param showDirectory the showDirectory to set
     */
    public void setShowDirectory(boolean showDirectory) {
        this.showDirectory = showDirectory;
    }

    /**
     * @return the urlName
     */
    public String getUrlName() {
        return urlName;
    }

    /**
     * @param urlName the urlName to set
     */
    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public static class NoneVisibilityResolver implements EventNavigationItemVisiblityResolver {

        @Override
        public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
            return false;
        }
    }
}
