package consys.common.gwt.client.rpc.action.cache;

import com.google.gwt.i18n.client.TimeZone;
import consys.common.gwt.client.cache.CacheAction;


/**
 * Cache action urcena k nacitani aktualneho eventu
 * @author Palo
 */
public abstract class CurrentEventTimeZoneCacheAction implements CacheAction<TimeZone> {
    // konstanty
    public static final String CURRENT_EVENT_TIME_ZONE = "current_event_time_zone";

    @Override
    public String getName() {
        return CURRENT_EVENT_TIME_ZONE;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
