package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event oznamujici ze uzivatel neni autentizovany nebo jiny pripad neopravnene akce
 * @author pepa
 */
public class ApplicationSecurityEvent extends GwtEvent<ApplicationSecurityEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onApplicationSecurity(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onApplicationSecurity(ApplicationSecurityEvent event);
    }
}
