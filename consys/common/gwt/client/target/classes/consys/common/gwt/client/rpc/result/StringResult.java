package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;

/**
 *
 * @author pepa
 */
public class StringResult implements Result {

    private static final long serialVersionUID = -7430340923380152065L;
    private String stringResult;

    public StringResult() {
    }

    public StringResult(String stringResult) {
        this.stringResult = stringResult;
    }

    public String getStringResult() {
        return stringResult;
    }
}
