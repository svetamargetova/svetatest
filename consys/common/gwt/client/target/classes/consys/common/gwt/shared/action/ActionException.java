package consys.common.gwt.shared.action;

import java.io.Serializable;

/**
 * An abstract superclass for exceptions that can be thrown by the Dispatch
 * system.
 * 
 * @author David Peterson
 */
public abstract class ActionException extends Exception implements Serializable {
    private static final long serialVersionUID = 3069973287033957189L;

    private String causeClassname;

    protected ActionException() {
    }

    public ActionException( String message ) {
        super( message );
    }

    public ActionException( Throwable cause ) {
        super( cause.getMessage() );
        this.causeClassname = cause.getClass().getName();
    }

    public ActionException( String message, Throwable cause ) {
        super( message + " (" + cause.getMessage() + ")" );
        this.causeClassname = cause.getClass().getName();
    }

    public String getCauseClassname() {
        return causeClassname;
    }

    @Override
    public String toString() {
        return super.toString() + ( causeClassname != null ? " [cause: " + causeClassname + "]" : "" );
    }
}
