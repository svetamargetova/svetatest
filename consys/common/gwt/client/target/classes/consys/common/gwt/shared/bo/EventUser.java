package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.client.utils.UserUtils;

/**
 * Eventovy uzivatel
 * @author pepa
 */
public class EventUser implements IsSerializable, Comparable<EventUser> {

    private String uuid;
    private String fullName;
    private String lastName;
    private String imageUuidPrefix;
    private String position;
    private String organization;

    public String getAffiliation() {
        return UserUtils.affilation(position, organization);
    }

    public String getAffiliation(String at) {
        return UserUtils.affilation(position, organization, at);
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the imageUuidPrefix
     */
    public String getImageUuidPrefix() {
        return imageUuidPrefix;
    }

    /**
     * @param imageUuidPrefix the imageUuidPrefix to set
     */
    public void setImageUuidPrefix(String imageUuidPrefix) {
        this.imageUuidPrefix = imageUuidPrefix;
    }

    @Override
    public int compareTo(EventUser t) {
        return getLastName().compareTo(t.getLastName());
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
