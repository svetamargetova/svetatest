package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;

/**
 *
 * @author pepa
 */
public class LongResult implements Result {

    private static final long serialVersionUID = -7126586977873075074L;
    // data
    private long value;

    public LongResult() {
    }

    public LongResult(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }
}
