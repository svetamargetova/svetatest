package consys.common.gwt.client.rpc.action.cache;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.cache.CacheTimeoutException;
import consys.common.gwt.client.rpc.action.ListUsStateAction;
import consys.common.gwt.client.rpc.result.SelectBoxData;

/**
 *
 * @author pepa
 */
public class ListUsStateCacheHandler implements CacheHandler<ListUsStateAction> {

    @Override
    public void doAction(ListUsStateAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
        if (!Cache.get().isRegistered(UsStateCacheAction.US_STATES)) {

            ActionExecutor.execute(action, new AsyncCallback<SelectBoxData>() {

                @Override
                public void onFailure(Throwable caught) {
                    callback.onFailure(caught);
                }

                @Override
                public void onSuccess(SelectBoxData result) {
                    Cache.get().register(UsStateCacheAction.US_STATES, result);
                    callback.onSuccess(result);
                }
            }, delegate);
        } else {
            Cache.get().doCacheAction(new UsStateCacheAction() {

                @Override
                public void doAction(SelectBoxData value) {
                    callback.onSuccess(value);
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(US_STATES));
                }
            });
        }
    }
}
