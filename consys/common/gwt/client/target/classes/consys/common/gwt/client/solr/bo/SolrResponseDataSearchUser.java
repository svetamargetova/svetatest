package consys.common.gwt.client.solr.bo;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * Data odpovedi ze solru
 * @author pepa
 */
public class SolrResponseDataSearchUser extends JavaScriptObject {
    
    protected SolrResponseDataSearchUser() {
    }

    /** pocet nalezenych polozek */
    public final long getNumFound() {
        return (long) getDoubleNumFound();
    }

    private final native double getDoubleNumFound() /*-{
        return this.numFound;
    }-*/;

    /** start */
    public final long getStart() {
        return (long) getDoubleStart();
    }

    private final native double getDoubleStart() /*-{
        return this.start;
    }-*/;

    /** @return seznam polozek */
    public final native JsArray<SearchedUser> getDocs() /*-{
        return this.docs;
    }-*/;
}
