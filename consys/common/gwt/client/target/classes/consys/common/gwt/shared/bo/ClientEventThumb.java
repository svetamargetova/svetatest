package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;

/**
 * Thumb pro event
 * @author Palo
 */
public class ClientEventThumb implements Result {

    private static final long serialVersionUID = -7948679862270045062L;
    private String uuid;
    // Identifikacia konferencie    
    private String fullName;
    private String web;
    private ClientDateTime from; // formar August XX - XX , 2009
    private ClientDateTime to; // formar August XX - XX , 2009
    private String logoUuidPrefix;
    private String profileLogoUuidPrefix;
    private int year;
    private String overseer;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return the logoUuidPrefix
     */
    public String getLogoUuidPrefix() {
        return logoUuidPrefix;
    }

    /**
     * @param logoUuidPrefix the logoUuidPrefix to set
     */
    public void setLogoUuidPrefix(String logoUuidPrefix) {
        this.logoUuidPrefix = logoUuidPrefix;
    }

    public String getProfileLogoUuidPrefix() {
        return profileLogoUuidPrefix;
    }

    public void setProfileLogoUuidPrefix(String profileLogoUuidPrefix) {
        this.profileLogoUuidPrefix = profileLogoUuidPrefix;
    }

    /**
     * @return the from
     */
    public ClientDateTime getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(ClientDateTime from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public ClientDateTime getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(ClientDateTime to) {
        this.to = to;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * Moze byt nastavene ale aj nemusi. Zalezi od volania.
     * @return the overseer
     */
    public String getOverseer() {
        return overseer;
    }

    /**
     * @param overseer the overseer to set
     */
    public void setOverseer(String overseer) {
        this.overseer = overseer;
    }

    /** pokud se jedna o stejny den, vraci true */
    public boolean isInSameDay() {
        if (getFrom() == null || getTo() == null) {
            return false;
        }
        ClientDateTime f = getFrom();
        ClientDateTime t = getTo();
        return (f.getDate().getDay() == t.getDate().getDay() && f.getDate().getMonth() == t.getDate().getMonth()
                && f.getDate().getYear() == t.getDate().getYear());
    }
}
