package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;

/**
 *
 * @author Palo
 */
public class BooleanResult implements Result{
    private static final long serialVersionUID = 8520969147851906669L;

    private boolean bool;

    public BooleanResult() {
    }

    public BooleanResult(boolean bool) {
        this.bool = bool;
    }

    /**
     * @return the bool
     */
    public boolean isBool() {
        return bool;
    }

}
