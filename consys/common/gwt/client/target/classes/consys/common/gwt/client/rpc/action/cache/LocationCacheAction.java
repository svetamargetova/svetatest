package consys.common.gwt.client.rpc.action.cache;

import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.rpc.result.SelectBoxData;

/**
 * abstraktni cache akce pro provadeni akci s lokaci
 * @author pepa
 */
public abstract class LocationCacheAction implements CacheAction<SelectBoxData> {

    // konstanty
    public static final String LOCATIONS = "locations";

    @Override
    public String getName() {
        return LOCATIONS;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
