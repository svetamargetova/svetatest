package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Data platebniho dialogu
 * @author pepa
 */
public class ClientPaymentOrderData implements Result {

    private static final long serialVersionUID = 3928726664132924607L;
    // data
    private ArrayList<ClientPaymentProduct> products;
    private Monetary totalPrice;

    public ClientPaymentOrderData() {
        products = new ArrayList<ClientPaymentProduct>();
    }

    public ArrayList<ClientPaymentProduct> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ClientPaymentProduct> products) {
        this.products = products;
    }

    public Monetary getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Monetary totalPrice) {
        this.totalPrice = totalPrice;
    }
}
