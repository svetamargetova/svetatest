package consys.common.gwt.client.solr.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Hlavicka solr odpovedi
 * @author pepa
 */
public class SolrHeader extends JavaScriptObject {

    protected SolrHeader() {
    }

    /** stav odpovedi */
    public final long getStatus() {
        return (long) getDoubleStatus();
    }

    private final native double getDoubleStatus() /*-{
        return this.status;
    }-*/;

    /** delka zpracovani */
    public final long getQTime() {
        return (long) getDoubleQTime();
    }

    private final native double getDoubleQTime() /*-{
        return this.QTime;
    }-*/;

    /** vraci parametry hlavicky */
    public final native SolrHeaderParams getParams() /*-{
        return this.params;
    }-*/;
}
