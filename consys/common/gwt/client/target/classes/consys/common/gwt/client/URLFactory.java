package consys.common.gwt.client;

import com.google.gwt.core.client.GWT;
import consys.common.constants.img.ImageConstants;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Vraci zakladni url
 * <p/>
 * @author pepa
 */
public class URLFactory {

    // konstanty
    protected static final String HOST = GWT.getHostPageBaseURL();
    protected static final String SERVER_URL;
    public static final String DEFAULT_MODULE_NAME = "takeplace";

    static {

        String base;
        String protocol = "http://";

        // odstranime http alebo https        
        if (HOST.contains("http://")) {
            base = HOST.substring("http://".length());
        } else if (HOST.contains("https://")) {
            protocol = "https://";
            base = HOST.substring("https://".length());
        } else {
            base = HOST;
        }

        int index = base.indexOf("/");
        if (index != -1) {
            SERVER_URL = protocol + base.substring(0, index + 1);
        } else {
            SERVER_URL = HOST;
        }



    }

    /** adresa korene zakladni stranky (napr.: http://tp.eu/takeplace/) */
    public static String hostPageBaserURL() {
        return HOST;
    }

    /** adresa odvozena ze zakladni stranky (napr.: http://tp.eu/takeplace/ -&gt; http://tp.eu/) */
    public static String serverURL() {
        return SERVER_URL;
    }

    /** adresa messagingu (natvrdo mail/client) */
    public static String messagingURL() {
        return SERVER_URL + "mail/client";
    }

    public static String loadImageUrl(String id) {
        return joinUrls(HOST, ImageConstants.LOAD_IMAGE_SERVLET_PATH) + id;
    }

    /** spoji prefix url1 se sufixem url2 */
    public static String joinUrls(String base, String... urls) {
        StringBuilder sb = new StringBuilder(base);
        if (!base.endsWith("/") && urls.length > 0) {
            sb.append("/");
        }
        for (int i = 0; i < urls.length; i++) {
            // spracujeme prve lomitko
            if (urls[i].startsWith("/")) {
                sb.append(urls[i].substring(2));
            } else {
                sb.append(urls[i]);
            }

            // pridame este posledne lomitko, ale len ak ma este viac 
            if (!urls[i].endsWith("/") && urls.length > i + 1) {
                sb.append(urls[i]).append("/");
            }
        }
        return sb.toString();
    }

    public static String joinUrls(String url1, String url2) {
        if (url1.endsWith("/") && url2.startsWith("/")) {
            String tmp = url2.substring(1);
            return url1 + tmp;
        } else {
            return url1 + url2;
        }
    }

    /**
     * Vysklada query url. Automaticky pridava k
     * <code>base</code> query oddelovac
     * <code>?</code>.
     * Preto ak sa vyskladava viacej queries tak sa musia vlozit naraz.
     * <p/>
     * @param base
     * @param queries
     * @return
     */
    public static String joinQueryUrl(String base, String... queries) {
        StringBuilder sb = new StringBuilder(base);
        if (!base.contains("?")) {
            sb.append("?");
        }
        if (queries.length % 2 != 0) {
            throw new IllegalArgumentException("URL query array is not dividable by 2");
        }
        for (int i = 0; i < queries.length; i += 2) {
            appendQuery(sb, queries[i], queries[i + 1]);
        }
        return sb.toString();
    }

    public static void appendQuery(StringBuilder sb, String name, int value) {
        appendQuery(sb, name, Integer.toString(value));
    }

    public static void appendQuery(StringBuilder sb, String name, String value) {
        if (sb.length() != 0 && sb.charAt(sb.length() - 1) != '?') {
            sb.append("&");
        }
        sb.append(name);
        sb.append("=");
        if (StringUtils.isNotBlank(value)) {
            sb.append(value);
        }
    }

    public static String defaultModuleUrl() {
        return joinUrls(SERVER_URL, DEFAULT_MODULE_NAME);
    }

    /** vygeneruje adresu eventu */
    public static String eventUrl(String eventUuid) {
        String baseUrl = joinUrls(SERVER_URL, DEFAULT_MODULE_NAME);
        return joinUrls(baseUrl, "/takeplace.html#Event?event=" + eventUuid);
    }

    /** vygeneruje adresu eventu, s nastavenym parametrem locale */
    public static String eventUrl(String eventUuid, String locale) {
        String baseUrl = joinUrls(SERVER_URL, DEFAULT_MODULE_NAME);
        return joinUrls(baseUrl, "/takeplace.html?locale=" + locale + "#Event?event=" + eventUuid);
    }

    /** vygeneruje adresu eventu */
    public static String eventRegistrationUrl(String eventUuid) {
        return eventUrl(eventUuid) + "&newUser=yes";
    }

    /** vytvori url k servletu, ktery vraci kod zeme, ze ktere se klient dotazuje (podle ip) */
    public static String countryServletUrl() {
        String baseUrl = joinUrls(SERVER_URL, DEFAULT_MODULE_NAME);
        return joinUrls(baseUrl, "/country");
    }

    /** pripoji dalsi parametr k url -> &key=value */
    public static String appendNextParam(String originalUrl, String key, String value) {
        StringBuilder sb = new StringBuilder(originalUrl);
        sb.append("&");
        sb.append(key);
        sb.append("=");
        sb.append(value);
        return sb.toString();
    }
}
