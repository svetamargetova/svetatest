package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event oznamujici ze byl zmenen nazev eventu
 * @author pepa
 */
public class EventNameChangedEvent extends GwtEvent<EventNameChangedEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private String acronym;
    private int year;

    public EventNameChangedEvent(String acronym, int year) {
        this.acronym = acronym;
        this.year = year;
    }

    /** aktualni titulek eventu */
    public String getTitle() {
        return acronym + " " + year;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onEventNameChanged(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onEventNameChanged(EventNameChangedEvent event);
    }
}
