package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 * Objekt je jiz pouzit
 * @author pepa
 */
public class AlreadyUsedObjectException extends ActionException {

    private static final long serialVersionUID = 1184787086758748017L;

    public AlreadyUsedObjectException() {
        super("");
    }

    public AlreadyUsedObjectException(String message) {
        super(message);
    }

    public AlreadyUsedObjectException(Throwable cause) {
        super(cause);
    }

    public AlreadyUsedObjectException(String message, Throwable cause) {
        super(message, cause);
    }
}
