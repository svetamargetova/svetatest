package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;

/**
 *
 * @author pepa
 */
public class VoidResult implements Result {

    private static final long serialVersionUID = 4529647413218484917L;

    public static VoidResult RESULT(){ return new VoidResult();}
}
