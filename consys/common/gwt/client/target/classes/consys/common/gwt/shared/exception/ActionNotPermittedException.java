package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 * Vyjimka vyhozena, kdyz neni uzivatel opravnen provest danou akci
 * @author pepa
 */
public class ActionNotPermittedException extends ActionException {

    private static final long serialVersionUID = 2785068627871720119L;

    public ActionNotPermittedException() {
        super("");
    }

    public ActionNotPermittedException(String message) {
        super(message);
    }

    public ActionNotPermittedException(Throwable cause) {
        super(cause);
    }

    public ActionNotPermittedException(String message, Throwable cause) {
        super(message, cause);
    }
}
