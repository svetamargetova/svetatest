package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventSettingsReceivedEvent extends GwtEvent<EventSettingsReceivedEvent.Handler>{

     /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    private List<String> rights;
    private Map<String,String> properties;
    private boolean resetNavigationModel;

    public EventSettingsReceivedEvent() {
    }

    public EventSettingsReceivedEvent(List<String> rights, Map<String,String> properties, boolean resetNavigationModel) {
        this.rights = rights;
        this.properties = properties;
        this.resetNavigationModel = resetNavigationModel;
    }



    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onReceivedSettingsLoaded(rights,properties,resetNavigationModel);
    }
  


/** interface eventu */
    public interface Handler extends EventHandler {

        public void onReceivedSettingsLoaded(List<String> rights, Map<String,String> properties, boolean resetNavigationModel);
    }

}
