package consys.common.gwt.client.module.event;

import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleRole;
import java.util.List;

/**
 * Interface pro eventove moduly. 
 * @author pepa
 */
public interface EventModule extends Module {


    public String getLocalizedModuleName();

    public void registerModuleRoles(List<ModuleRole> moduleRoles);

    public void registerEventNavigation(EventNavigationModel model);
    
}
