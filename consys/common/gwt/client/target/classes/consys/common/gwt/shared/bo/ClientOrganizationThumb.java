package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;

/**
 *
 * @author Palo
 */
public class ClientOrganizationThumb implements IsSerializable,Result {
    private static final long serialVersionUID = -2296582626341094971L;

    private String uuid;
    private String universalName;
    private String type;
    private String web;
    private String address;

    public ClientOrganizationThumb() {
    }

    

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the universalName
     */
    public String getUniversalName() {
        return universalName;
    }

    /**
     * @param universalName the universalName to set
     */
    public void setUniversalName(String universalName) {
        this.universalName = universalName;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }


}
