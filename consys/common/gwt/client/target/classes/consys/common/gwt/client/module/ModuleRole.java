package consys.common.gwt.client.module;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ModuleRole implements Comparable<ModuleRole> {
    private String id;
    private String shortcut;
    private String localizedModuleName;
    private String localizedName;
    private String lozalizedDescription;
    private boolean system;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the shortcut
     */
    public String getShortcut() {
        return shortcut;
    }

    /**
     * @param shortcut the shortcut to set
     */
    public void setShortcut(String shortcut) {
        this.shortcut = shortcut;
    }

    /**
     * @return the localizedName
     */
    public String getLocalizedName() {
        return localizedName;
    }

    /**
     * @param localizedName the localizedName to set
     */
    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    /**
     * @return the lozalizedDescription
     */
    public String getLocalizedDescription() {
        return lozalizedDescription;
    }

    /**
     * @param lozalizedDescription the lozalizedDescription to set
     */
    public void setLocalizedDescription(String lozalizedDescription) {
        this.lozalizedDescription = lozalizedDescription;
    }

    @Override
    public String toString() {
        return localizedModuleName+"-"+localizedName;
    }

    /**
     * @return the system
     */
    public boolean isSystem() {
        return system;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(boolean system) {
        this.system = system;
    }

    /**
     * @return the localizedModuleName
     */
    public String getLocalizedModuleName() {
        return localizedModuleName;
    }

    /**
     * @param localizedModuleName the localizedModuleName to set
     */
    public void setLocalizedModuleName(String localizedModuleName) {
        this.localizedModuleName = localizedModuleName;
    }

    @Override
    public int compareTo(ModuleRole t) {
        return toString().compareTo(t.toString());
    }
}
