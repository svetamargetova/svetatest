package consys.common.gwt.client.utils;

/**
 *
 * @author pepa
 */
public class UserUtils {

    public static String affilation(String position, String organization) {
        return affilation(position, organization, "-");
    }

    /**
     * vraci afilaci uzivatele ve formatu [pozice at organizace],
     * pokud je pozice null, vraci jen organizaci,
     * pokud je organizace null vraci jen pozici,
     * pokud je organizace i pozice null vraci prazdny string
     */
    public static String affilation(String position, String organization, String at) {
        boolean emptyPosition = StringUtils.isEmpty(position);
        boolean emptyOrganization = StringUtils.isEmpty(organization);
        if (!emptyPosition && !emptyOrganization) {
            return position + " " + at + " " + organization;
        } else if (emptyPosition && !emptyOrganization) {
            return organization;
        } else if (!emptyPosition && emptyOrganization) {
            return position;
        } else {
            return "";
        }
    }
}
