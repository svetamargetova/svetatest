package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;

/**
 * Objekt urcujici produkt podle uuid + jeho pripadny slevovy kod + kvantitu
 * @author pepa
 */
public class ProductThumb implements Result {

    private static final long serialVersionUID = 7516440042183963507L;
    // data
    private String productUuid;
    private String discountCode;
    private int quantity = 1;

    public ProductThumb() {
        discountCode = "";
    }

    public ProductThumb(String productUuid, String discountCode, int quantity) {
        this.productUuid = productUuid;
        this.discountCode = discountCode == null ? "" : discountCode;
        this.quantity = quantity;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode == null ? "" : discountCode;
    }

    public String getProductUuid() {
        return productUuid;
    }

    public void setProductUuid(String productUuid) {
        this.productUuid = productUuid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }

    @Override
    public String toString() {
        return "ProductThumb [uuid=" + productUuid + " code=" + discountCode + " quantity=" + quantity + "]";
    }
}
