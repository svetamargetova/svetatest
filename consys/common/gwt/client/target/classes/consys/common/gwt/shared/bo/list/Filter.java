package consys.common.gwt.shared.bo.list;

import java.io.Serializable;

/**
 * Prave aktivny filter a jeho aktivna hodnota na zaklde ktorej sa filtruje.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Filter implements Serializable {

    private static final long serialVersionUID = -5200006782499469381L;
    // data
    private int tag;
    private String stringValue;
    private int intValue;

    public Filter() {
    }

    public Filter(int tag) {
        this.tag = tag;
    }

    public Filter(int tag, String value) {
        this.tag = tag;
        this.stringValue = value;
    }

    public int tag() {
        return tag;
    }

    /**
     * @return the stringValue
     */
    public String getStringValue() {
        return stringValue;
    }

    /**
     * @param stringValue the stringValue to set
     */
    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     * @return the intValue
     */
    public int getIntValue() {
        return intValue;
    }

    /**
     * @param intValue the intValue to set
     */
    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }
}
