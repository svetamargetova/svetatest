/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.gwt.shared.bo.list;

/**
 *
 * @author Pavol Gressa <gressa@acemcee.com>
 */
public enum ListExportTypeEnum{
    PDF, EXCEL;
        
}
