package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class NoRecordsForAction extends ActionException{
    private static final long serialVersionUID = 5597105428080324560L;

    

    public NoRecordsForAction() {
        super("");
    }

    public NoRecordsForAction(String message) {
        super(message);
    }

    public NoRecordsForAction(Throwable cause) {
        super(cause);
    }

    public NoRecordsForAction(String message, Throwable cause) {
        super(message, cause);
    }

}
