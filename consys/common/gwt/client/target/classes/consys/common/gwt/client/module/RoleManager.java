package consys.common.gwt.client.module;


import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.event.EventModule;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RoleManager {

    private static final Logger logger = LoggerFactory.getLogger(RoleManager.class);

    private static final RoleManager roleManager = new RoleManager();
    private ArrayList<Module> registredModules;
    /* Mapa na preklad ID zo serveru na shortcut */
    private Map<String, ModuleRole> serverTranslationMap;

    private RoleManager() {
        registredModules = new ArrayList<Module>();
        serverTranslationMap = new HashMap<String, ModuleRole>();
    }

    public static final void registerModule(EventModule module) {
        // Zaregistrujeme si modul
        Module m = new Module();
        m.setName(module.getLocalizedModuleName());
        module.registerModuleRoles(m.getRoles());
        roleManager.registredModules.add(m);

        // Zaregistrujeme si translation mapu
        for (ModuleRole role : m.getRoles()) {
            logger.debug(role.getId() + " " + role.getShortcut());
            role.setLocalizedModuleName(m.getName());
            roleManager.serverTranslationMap.put(role.getId(), role);
        }
    }

    public static final String translateToShortcut(String identifier) {
        ModuleRole role = roleManager.serverTranslationMap.get(identifier);
        return role == null ? null : role.getShortcut();
    }

    public static final ArrayList<ModuleRole> translateToModuleRoles(ArrayList<String> identifiers) {
        ArrayList<ModuleRole> roles = new ArrayList<ModuleRole>();
        if (identifiers != null) {
            for (String identifier : identifiers) {
                ModuleRole role = roleManager.serverTranslationMap.get(identifier);
                if (role != null) {
                    roles.add(role);
                } else {
                    logger.error("Missing ModuleRole for identifier " + identifier);
                }
            }
        }
        return roles;

    }

    public static final ArrayList<Module> registredModules() {
        return roleManager.registredModules;
    }

    public static class Module {

        private String name;
        private ArrayList<ModuleRole> roles = new ArrayList<ModuleRole>();

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the roles
         */
        public ArrayList<ModuleRole> getRoles() {
            return roles;
        }

        /**
         * @param roles the roles to set
         */
        public void setRoles(ArrayList<ModuleRole> roles) {
            this.roles = roles;
        }
    }
}
