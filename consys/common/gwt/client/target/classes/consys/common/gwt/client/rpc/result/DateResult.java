package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class DateResult implements Result {

    private static final long serialVersionUID = 6527588350794113768L;
    // data
    private Date date;

    public DateResult() {
    }

    public DateResult(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }
}
