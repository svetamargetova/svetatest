package consys.common.gwt.client.logger.handler;

import com.google.gwt.logging.client.SystemLogHandler;
import consys.common.gwt.client.logger.ClassicTextFormatter;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConsysSystemLogHandler extends SystemLogHandler {

    public ConsysSystemLogHandler() {
        super();
        setFormatter(new ClassicTextFormatter(true));
    }
}
