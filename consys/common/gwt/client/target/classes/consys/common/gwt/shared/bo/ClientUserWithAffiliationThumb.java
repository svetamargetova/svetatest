package consys.common.gwt.shared.bo;

/**
 * Nahledovy objekt uzivatele. Pouziva sa vo vypisoch zoznamov. Obsahuje data
 * k vytvoreniu zakladnej struktury
 *
 * LOGO : MENO : POZICIA
 *
 * @author pepa
 */
public class ClientUserWithAffiliationThumb extends ClientUserThumb {

    // konstanty
    private static final long serialVersionUID = -3594681004898988291L;
    // data

    private String position;

   
    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }
}
