package consys.common.gwt.client.rpc.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientUserThumb;
import java.util.ArrayList;

/**
 * Action ktora vracia prelozene useruuids na thumby do zoznamu. Ak je nastavene
 * eventUuid tak je to profil vzhaldom k eventu ak nieje nastavene tak defaultne.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUserThumbsAction implements Action<ArrayListResult<ClientUserThumb>> {

    private static final long serialVersionUID = -5483481173906228329L;
    private ArrayList<String> uuids;
    private String eventUuid;

    public ListUserThumbsAction() {
    }

    public ListUserThumbsAction(ArrayList<String> uuids) {
        this.uuids = uuids;
    }

    public ListUserThumbsAction(ArrayList<String> uuids, String eventUuid) {
        this.uuids = uuids;
        this.eventUuid = eventUuid;
    }

    public ArrayList<String> getThumbs() {
        return uuids;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public boolean hasEvent(){
        return eventUuid == null || eventUuid.length() > 0;
    }
}
