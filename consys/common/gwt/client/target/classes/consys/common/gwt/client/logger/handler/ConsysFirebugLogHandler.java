package consys.common.gwt.client.logger.handler;

import com.google.gwt.logging.client.FirebugLogHandler;
import consys.common.gwt.client.logger.ClassicTextFormatter;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConsysFirebugLogHandler extends FirebugLogHandler {

    public ConsysFirebugLogHandler() {
        super();
        setFormatter(new ClassicTextFormatter(true));
    }
}
