package consys.common.gwt.client.utils;

import com.google.gwt.i18n.client.TimeZone;
import java.util.List;


/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TimeZoneUtils {
    
    public static String[] toIdStringArray(TimeZone[] zones){
        String[] array = new String[zones.length];
        for (int i = 0; i < zones.length; i++) {
            array[i] = zones[i].getID();
        }
        return array;
    }

    public static String[] toIdStringList(List<String> zones){
        String[] array = new String[zones.size()];
        for (int i = 0; i < zones.size(); i++) {
            array[i] = zones.get(i);
        }
        return array;
    }



}
