package consys.common.gwt.shared.bo.list;

import java.io.Serializable;

/**
 * Polozka vyberoveho filtru
 * @author pepa
 */
public class SelectFilterItem implements Serializable {
    private static final long serialVersionUID = -6458392624452836693L;

    private String uuid;
    private String name;

    public SelectFilterItem() {
    }

    public SelectFilterItem(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
