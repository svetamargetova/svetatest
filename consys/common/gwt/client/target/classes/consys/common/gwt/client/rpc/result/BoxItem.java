package consys.common.gwt.client.rpc.result;

import java.io.Serializable;

public class BoxItem implements Serializable {

    private static final long serialVersionUID = -3262147227051335854L;
    private String name;
    private Integer id;

    public BoxItem() {
    }

    public BoxItem(String name, Integer id) {
        this.name = name;
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }
}
