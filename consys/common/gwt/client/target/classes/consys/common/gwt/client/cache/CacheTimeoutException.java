package consys.common.gwt.client.cache;

import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;

/**
 *
 * @author Palo
 */
public class CacheTimeoutException extends Exception{
    private static final Logger logger = LoggerFactory.getLogger(CacheTimeoutException.class);
    private static final long serialVersionUID = 1325187150110445484L;

    public CacheTimeoutException(String key) {
        super();
        logger.error(key+" is not in cache. Possible error.");

    }


    

}
