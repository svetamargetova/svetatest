package consys.common.gwt.client.rpc.action.cache;

import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.shared.bo.ClientPersonalInfo;

/**
 *
 * @author pepa
 */
public abstract class PersonalInfoCacheAction implements CacheAction<ClientPersonalInfo> {

    // konstanty
    public static final String PERSONAL_INFO = "personal_info";

    @Override
    public String getName() {
        return PERSONAL_INFO;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
