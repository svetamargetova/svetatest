package consys.common.gwt.client.rpc.action.cache;

import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.client.cache.CacheAction;

/**
 * abstraktni cache akce pro provadeni akci s uzivatelem
 * @author pepa
 */
public abstract class UserCacheAction implements CacheAction<ClientUser> {

    // konstanty
    public static final String USER = "user";

    @Override
    public String getName() {
        return USER;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
