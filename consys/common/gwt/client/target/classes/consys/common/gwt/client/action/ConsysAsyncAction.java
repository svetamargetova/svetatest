package consys.common.gwt.client.action;

/**
 * Obecny interface objektu pro spusteni asynchronni akce a reakcemi na vysledek
 * @author pepa
 */
public interface ConsysAsyncAction extends ConsysAction {

    /** reakce na uspech */
    public void onSuccess();

    /** reakce na neuspech */
    public void onFail();
}
