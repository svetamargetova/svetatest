package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event ktory sa vystreli ked sa odneklial ziada o vstup do eventu
 *
 * @author pepa
 */
public class EventEnterRequest extends GwtEvent<EventEnterRequest.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // konstanty
    public static final String EVENT = "Event";
    public static final String PARAM_EVENT_UUID = "event";
    // data
    private String eventUuid;

    public EventEnterRequest(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    /** uuid eventu */
    public String getEventUuid() {
        return eventUuid;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onEventEntrance(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onEventEntrance(EventEnterRequest event);
    }
}
