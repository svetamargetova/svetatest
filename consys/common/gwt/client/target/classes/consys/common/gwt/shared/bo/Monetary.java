package consys.common.gwt.shared.bo;

import consys.common.gwt.shared.action.Result;

/**
 * Penize<br> omezeni na dve desetinna mista
 *
 * @author pepa
 */
public class Monetary implements Result {

    public static final Monetary ZERO = new Monetary(0);
    private static final long serialVersionUID = 1191453798904951025L;
    /**
     * maximalni hodnota pred desetinnou carkou
     */
    public static final int MAX_INTEGER_VALUE = 999999;
    /**
     * maximalni hodnota za desetinnou carkou
     */
    public static final int MAX_DECIMAL_VALUE = 99;
    /**
     * minimalni hodnota
     */
    public static final int MIN_VALUE = 0;
    /**
     * cela cast cisla
     */
    private int integer;
    /**
     * desetinna cast cisla
     */
    private int decimal;

    public Monetary() {
        this(MIN_VALUE, MIN_VALUE);
    }

    public Monetary(int integer) {
        this(integer, MIN_VALUE);
    }

    public Monetary(int integer, int decimal) {
        if (integer > MAX_INTEGER_VALUE || integer < MIN_VALUE) {
            throw new IllegalArgumentException("integer part of money is restricted to range <" + MIN_VALUE + "," + MAX_INTEGER_VALUE + ">");
        }
        if (decimal > MAX_DECIMAL_VALUE || decimal < MIN_VALUE) {
            throw new IllegalArgumentException("decimal part of money is restricted to range <" + MIN_VALUE + "," + MAX_DECIMAL_VALUE + ">");
        }
        this.integer = integer;
        this.decimal = decimal;
    }

    /**
     * cela cast cisla
     */
    public int getInteger() {
        return integer;
    }

    /**
     * cela cast cisla
     *
     * @throws IllegalArgumentException pokud je číslo mimo rozsah <0,999999>
     */
    public void setInteger(int integer) {
        if (integer > MAX_INTEGER_VALUE || integer < MIN_VALUE) {
            throw new IllegalArgumentException("integer part of money is restricted to range <" + MIN_VALUE + "," + MAX_INTEGER_VALUE + ">");
        }
        this.integer = integer;
    }

    /**
     * desetinna cast cisla
     */
    public int getDecimal() {
        return decimal;
    }

    /**
     * desetinna cast cisla<br> napr.: cislo 5 znaci pet haleru, tj .05<br>
     * napr.: cislo 50 znaci padesat haleru, tj .5<br>
     *
     * @throws IllegalArgumentException pokud je desetinna cast mimo rozsah
     * <0,99>
     */
    public void setDecimal(int decimal) {
        if (decimal > MAX_DECIMAL_VALUE || decimal < MIN_VALUE) {
            throw new IllegalArgumentException("decimal part of money is restricted to range <" + MIN_VALUE + "," + MAX_DECIMAL_VALUE + ">");
        }
        this.decimal = decimal;
    }

    /**
     * vraci jako text ve formatu (celaCast)(tecka)(desetinnaCast),<br> napr.:
     * cela 10 a desetinna 5 -> 10.05 napr.: cela 13 a desetinna 62 -> 13.62
     */
    @Override
    public String toString() {
        return integer + (decimal == 0 ? "" : ("." + (decimal < 10 ? "0" + decimal : decimal)));
    }

    /**
     * prevadi text na typ Monetary
     *
     * @throws IllegalArgumentException pokud obsahuje spatny pocet desetinnych
     * tecek nebo mist
     * @throws NumberFormatException pokud se vyskytne chyba v prevodu cele nebo
     * desetinne casti
     */
    public static Monetary toMonetary(String text) {
        String[] s = text.split("\\.");
        int i = 0;
        int d = 0;

        switch (s.length) {
            case 1:
                i = Integer.parseInt(s[0]);
                break;
            case 2:
                i = Integer.parseInt(s[0]);
                String dec = s[1].trim();
                switch (dec.length()) {
                    case 1:
                        d = Integer.parseInt(dec) * 10;
                        break;
                    case 2:
                        d = Integer.parseInt(dec);
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
                break;
            default:
                throw new IllegalArgumentException();
        }

        return new Monetary(i, d);
    }

    /** pricte a vrati v novem objektu Monetary */
    public Monetary add(Monetary m) {
        int i = integer + m.getInteger();
        int d = decimal + m.getDecimal();
        if (d > MAX_DECIMAL_VALUE) {
            i++;
            d = d - MAX_DECIMAL_VALUE - 1;
        }
        return new Monetary(i, d);
    }

    /** odecte a vrati v novem objektu Monetary */
    public Monetary sub(Monetary m) {
        int i = integer - m.getInteger();
        int d = decimal - m.getDecimal();
        if (decimal < m.getDecimal()) {
            i--;
            d = decimal + 1 + MAX_DECIMAL_VALUE - m.getDecimal();
        }
        return new Monetary(i, d);
    }

    /** vynasobi puvodni hodnotu zadanym cislem a vrati nove monetary */
    public Monetary multiple(int n) {
        int i = integer * n;
        int d = decimal * n;

        i += d / 100;
        d = d % 100;
        return new Monetary(i, d);
    }

    /**
     * Vrati true ak je hodnota nula, tj cela cast a fraktalova cast cisla su 0
     *
     * @return
     * <code>true</code> ak je hodnota 0, inak
     * <code>false</code>
     */
    public boolean isZero() {
        return this.equals(ZERO);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Monetary)) {
            return false;
        }
        Monetary arg = (Monetary) obj;
        return arg.decimal == decimal && arg.integer == integer;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.integer;
        hash = 83 * hash + this.decimal;
        return hash;
    }
}
