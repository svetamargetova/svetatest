package consys.common.gwt.client.rpc.action;

import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientUserCommonInfo;

/**
 * Akce pro natazeni obecnych informacich o uzivateli
 * @author pepa
 */
public class LoadUserInfoAction extends EventAction<ClientUserCommonInfo> {

    private static final long serialVersionUID = -5456595830911239594L;
    // data
    private String userUuid;

    public LoadUserInfoAction() {
    }

    public LoadUserInfoAction(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getUserUuid() {
        return userUuid;
    }
}
