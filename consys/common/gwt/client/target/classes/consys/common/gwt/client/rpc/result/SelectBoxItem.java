package consys.common.gwt.client.rpc.result;

import java.io.Serializable;

/**
 * Jedna polozka SelectBoxu
 * @author pepa
 */
public class SelectBoxItem<T> implements Serializable {

    private static final long serialVersionUID = 6334110341275523537L;
    private T item;
    private String name;
    private String html;

    public SelectBoxItem() {
    }

    public SelectBoxItem(T item) {
        this.item = item;
    }

    public SelectBoxItem(T item, String name) {
        this.item = item;
        this.name = name;
    }

    public SelectBoxItem(T item, String name, String html) {
        this.item = item;
        this.name = name;
        this.html = html;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public String getName() {
        if (name == null) {
            return item.toString();
        } else {
            return name;
        }
    }

    public void setName(String name) {
        this.name = name;
    }
}
