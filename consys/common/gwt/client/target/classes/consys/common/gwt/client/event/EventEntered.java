package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import consys.common.gwt.shared.bo.ClientCurrentEvent;

/**
 * Event ktory sa vystreli ked sa uspesne nacitali data potrebne k vstupu do eventu.
 * Tento EVENT moze handlovat len jedna entita!!! a tou je EventDispatchAsymcImpl
 * @author Palo
 */
public class EventEntered extends GwtEvent<EventEntered.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    private ClientCurrentEvent enteredEvent;

    public EventEntered() {
    }

    public EventEntered(ClientCurrentEvent enteredEvent) {
        this.enteredEvent = enteredEvent;
    }

    public ClientCurrentEvent getEnteredEvent() {
        return enteredEvent;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onEventEnter(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onEventEnter(EventEntered event);
    }
}
