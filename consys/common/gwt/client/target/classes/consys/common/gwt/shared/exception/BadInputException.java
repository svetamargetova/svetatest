package consys.common.gwt.shared.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class BadInputException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public BadInputException() {
        super("");
    }

    public BadInputException(String message) {
        super(message);
    }

    public BadInputException(Throwable cause) {
        super(cause);
    }

    public BadInputException(String message, Throwable cause) {
        super(message, cause);
    }
}
