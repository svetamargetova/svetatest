package consys.common.gwt.client;

import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;

/**
 * Vraci url pro eventy
 * @author pepa
 */
public class EventURLFactory extends URLFactory {

    private static final Logger logger = LoggerFactory.getLogger(EventURLFactory.class);

    /** vraci aktualni url na messaging */
    public static String messagingURL() {
        return SERVER_URL + "mail/client";
    }

    /**
     * vraci url aktualniho eventu, pokud neni v cache CurrentEventCacheAction.CURRENT_EVENT
     * @palo - deprecation, samotna url
     */
    private static String overseerURL() throws NotInCacheException {
        ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
        return joinUrls(SERVER_URL, cce.getOverseerUrl());
    }

    public static String overseerURL(String append) {
        try {
            return joinUrls(overseerURL(), append);
        } catch (NotInCacheException ex) {
            logger.error("NotInCacheException: EventURLFactory - overseerURL");
            return null;
        }
    }

    public static String overseerServlet(String servletPath) throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }

        ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
        return joinUrls(overseerURL, servletPath + "?eid=" + cce.getEvent().getUuid());
    }

    /** vraci url pro zadany event */
    public static String constructOverseerDispatchURL(String overseerPath) {
        return SERVER_URL + overseerPath;
    }

    /** vraci zakladni cast url (bez parametru) ke stazeni souboru */
    public static String overseerDownloadFileURL() throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }
        return overseerURL + "/download";
    }

    /** url pro stazeni faktury */
    public static String downloadInvoice(String invoiceUuid) throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }
        return joinUrls(overseerURL, "/payment/download/invoice?invoice=" + invoiceUuid);
    }

    /** url pro stazeni baliku faktur transferu */
    public static String downloadInvoiceTransferPack(String transferUuid) throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }
        ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);

        return joinUrls(overseerURL, "/payment/download/transfer?invoice=" + transferUuid + "&eid=" + cce.getEvent().getUuid());
    }

    public static String downloadListExport(String query) throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }
        ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
        return overseerServlet("/list/export") + query;

    }

    /** url pro stazeni exportu s prehledem transferu */
    public static String downloadExportTransferOverview(String transferUuid) throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }
        ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);

        return joinUrls(overseerURL, "/payment/download/transferOverviewExport?invoice=" + transferUuid + "&eid=" + cce.getEvent().getUuid());
    }

    /** url pro stazeni faktury */
    public static String downloadTicket(final String registrationUuid) throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }
        final ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
        final ClientUser user = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
        final String eid = cce.getEvent().getUuid();
        final String uid = user.getUuid();
        String params = "?registration=" + registrationUuid + "&eid=" + eid + "&uid=" + uid;
        return joinUrls(overseerURL, "/ticket/download" + params);
    }

    /** url pro stazeni faktury */
    public static String downloadRegistrationBadges(String size) throws NotInCacheException {
        String overseerURL = overseerURL();
        if (overseerURL == null) {
            throw new NotInCacheException();
        }
        final ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
        final ClientUser user = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
        final String eid = cce.getEvent().getUuid();
        final String uid = user.getUuid();
        StringBuilder params = new StringBuilder("?eid=");
        params.append(eid);
        params.append("&uid=");
        params.append(uid);
        params.append("&size=");
        params.append(size);
        return joinUrls(overseerURL, "/badges/download" + params.toString());
    }

    public static String downloadUploadedVideo(String systemName) throws NotInCacheException {
        String overseerURL = overseerURL();
        return joinUrls(overseerURL, "/video/download?id=" + systemName);
    }
}
