package consys.common.gwt.client.action;

import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.AccessDeniedEvent;
import consys.common.gwt.client.event.ApplicationSecurityEvent;
import consys.common.gwt.client.rpc.ExceptionHandler;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.exception.AccessDeniedException;
import consys.common.gwt.shared.exception.ApplicationSecurityException;

/**
 *
 * @author palo
 */
public class SecurityExceptionHandler implements ExceptionHandler {

    @Override
    public void onFailure(Action a, Throwable e) {
        GwtEvent event = null;
        if (e instanceof AccessDeniedException) {
            // pristup odepren (neplatna role)
            event = new AccessDeniedEvent();
        } else if (e instanceof ApplicationSecurityException) {
            // uzivatel neni autentizovany
            event = new ApplicationSecurityEvent();
        }

        if (event != null) {
            EventBus.get().fireEvent(event);
        }
    }
}
