package consys.common.gwt.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.rpc.ExceptionHandler;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;
import java.util.List;

/**
 * An abstract base class that provides methods that can be called to handle success or failure
 * results from the remote service. These should be called by the implementation of
 * {@link #execute(net.customware.gwt.dispatch.shared.Action, com.google.gwt.user.client.rpc.AsyncCallback)}.
 * 
 * @author David Peterson
 */
public abstract class AbstractDispatchAsync implements DispatchAsync {

    List<ExceptionHandler> exceptionHandlers;

    public AbstractDispatchAsync() {
        exceptionHandlers = new ArrayList<ExceptionHandler>();        
    }


    public void addExceptionHandler(ExceptionHandler h) {
        exceptionHandlers.add(h);
    }

    protected <A extends Action<R>, R extends Result> void onFailure(A action, Throwable caught, final AsyncCallback<R> callback) {
        for(ExceptionHandler e: exceptionHandlers){
            e.onFailure(action,caught);
        }
        callback.onFailure(caught);
    }

    protected <A extends Action<R>, R extends Result> void onSuccess(A action, R result, final AsyncCallback<R> callback) {
        callback.onSuccess(result);
    }
}
