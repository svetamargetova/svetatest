package consys.common.gwt.client.module;

import consys.common.gwt.client.module.Module.ModuleMenuItem;
import java.util.ArrayList;

/**
 *
 * @author Palo
 */
public class ModuleMenuRegistrator {

    ArrayList<ModuleMenuItem> menu;

    public ModuleMenuRegistrator() {
        menu = new ArrayList<ModuleMenuItem>();
    }

    public void registerMenuItem(ModuleMenuItem item) {
        if (item == null) {
            throw new NullPointerException("ModuleMenuItem is NULL ");
        }
        menu.add(item);
    }

    public ArrayList<ModuleMenuItem> getMenu() {
        return menu;
    }

    
}
