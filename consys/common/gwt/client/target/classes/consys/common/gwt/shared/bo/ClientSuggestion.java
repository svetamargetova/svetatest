package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.ui.SuggestOracle;

/**
 * Interface pro suggestion pouzivaten consys suggestion komponentou
 * @author pepa
 */
public interface ClientSuggestion<T> extends SuggestOracle.Suggestion {

    public T getSuggestionId();
}
