/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.gwt.shared.bo;

import consys.common.constants.currency.CurrencyEnum;

/**
 *
 * @author palo
 */
public class ClientEventPaymentProfile extends ClientUserPaymentProfile{
    private static final long serialVersionUID = -8925719395430503238L;
    
    private CurrencyEnum currency;
    private String invoiceNote;
    /* Priznak ze tento platobny profil sa bude nachadzat na prikaze k uhrade banke */
    private boolean bankOrderAsProfile;
    
    public CurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyEnum currency) {
        this.currency = currency;
    }

    public String getInvoiceNote() {
        return invoiceNote;
    }

    public void setInvoiceNote(String invoiceNote) {
        this.invoiceNote = invoiceNote;
    }

    /**
     * @return the bankOrderAsProfile
     */
    public boolean isBankOrderAsProfile() {
        return bankOrderAsProfile;
    }

    /**
     * @param bankOrderAsProfile the bankOrderAsProfile to set
     */
    public void setBankOrderAsProfile(boolean bankOrderAsProfile) {
        this.bankOrderAsProfile = bankOrderAsProfile;
    }
    
}
