package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;

/**
 * Nahledovy objekt uzivatele
 * @author pepa
 */
public class ClientUserThumb implements IsSerializable, Result {

    // konstanty
    private static final long serialVersionUID = -3594681004898988291L;
    // data
    private String uuid;
    private String uuidImg;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuidPortraitImagePrefix() {
        return uuidImg;
    }

    public void setUuidPortraitImagePrefix(String uuidImg) {
        this.uuidImg = uuidImg;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ClientUserThumb) && uuid != null && ((ClientUserThumb) o).getUuid() != null && ((ClientUserThumb) o).getUuid().equals(uuid);
    }
}
