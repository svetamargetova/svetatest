package consys.common.gwt.client.rpc.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.HashMap;

/**
 * Akce pro aktualizaci eventove property
 * @author pepa
 */
public class UpdateEventPropertyAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -6286564110087531486L;
    // data
    private String propertyKey;
    private String propertyValue;
    private HashMap<String, String> properties;

    public UpdateEventPropertyAction() {
        properties = new HashMap<String, String>();
    }

    public UpdateEventPropertyAction(String propertyKey, String propertyValue) {
        this.propertyKey = propertyKey;
        this.propertyValue = propertyValue;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public HashMap<String, String> getProperties() {
        return properties;
    }
}
