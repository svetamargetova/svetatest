package consys.common.gwt.client.rpc.action.cache;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.cache.CacheTimeoutException;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.result.SelectBoxData;

/**
 *
 * @author pepa
 */
public class ListLocationCacheHandler implements CacheHandler<ListLocationAction>{

    @Override
    public void doAction(ListLocationAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
         if (!Cache.get().isRegistered(LocationCacheAction.LOCATIONS)) {

            ActionExecutor.execute(action, new AsyncCallback<SelectBoxData>() {

                @Override
                public void onFailure(Throwable caught) {
                    callback.onFailure(caught);
                }

                @Override
                public void onSuccess(SelectBoxData result) {
                    Cache.get().register(LocationCacheAction.LOCATIONS, result);
                    callback.onSuccess(result);
                }
            }, delegate);
        } else {
           Cache.get().doCacheAction(new LocationCacheAction() {

                @Override
                public void doAction(SelectBoxData  value) {
                    callback.onSuccess(value);
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(LOCATIONS));
                }
            });
        }
    }

}