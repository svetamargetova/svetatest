package consys.common.gwt.shared.action.exceptions;

import consys.common.gwt.shared.action.ActionException;

/**
 * Neco neni v poradku s opravnenim provest pozadovanou akci
 * @author pepa
 */
public class PermissionException extends ActionException {

    private static final long serialVersionUID = 6252511086394279420L;
}
