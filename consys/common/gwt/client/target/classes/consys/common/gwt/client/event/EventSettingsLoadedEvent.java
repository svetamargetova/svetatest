package consys.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.shared.bo.EventSettings;


/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventSettingsLoadedEvent extends GwtEvent<EventSettingsLoadedEvent.Handler>{

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    private EventSettings eventSettings;

    public EventSettingsLoadedEvent() {
    }

    public EventSettingsLoadedEvent(EventSettings eventSettings) {
        this.eventSettings = eventSettings;
    }

    

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onLoadedSettings(eventSettings);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onLoadedSettings(EventSettings settings);
    }
}