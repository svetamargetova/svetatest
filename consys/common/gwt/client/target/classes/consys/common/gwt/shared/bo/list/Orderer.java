package consys.common.gwt.shared.bo.list;

import java.io.Serializable;

/**
 *
 * Zoradovac zoznamu.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Orderer implements Serializable {
    private static final long serialVersionUID = 1368261091629563341L;

    private int tag;


    public Orderer() {
    }

    public Orderer(int tag) {
        this.tag = tag;
    }

    public int tag() {
        return tag;
    }
}
