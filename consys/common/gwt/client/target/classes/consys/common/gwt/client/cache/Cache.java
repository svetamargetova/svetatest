package consys.common.gwt.client.cache;

import com.google.gwt.user.client.Timer;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.event.UserLogoutEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.shared.exception.NotInCacheException;
import java.util.HashMap;
import java.util.Map;

/**
 * Cachovaní dat na klientovi
 *
 * @author pepa
 */
public class Cache implements UserLoginEvent.Handler, UserLogoutEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(Cache.class);
    // konstanty
    private static final int TIMEOUT = 100;
    private static final int MAX_TIMEOUT_REPEAT = 300;
    /** konstanta pro promennou uchovavajici zda je uzivatel prihlasen nebo ne */
    public static final String LOGGED = "logged";
    /** konstanta pro promennou uchovavajici uuid prave vytvoreneho eventu */
    public static final String NEW_EVENT_CREATED = "new_event_created";
    /** konstanta pro promennou uchovavajici otevrene zalozky tab panelu */
    private static final String SELECTED_TABS = "tab_panel_selected_tabs";
    // instance
    private static Cache instance;
    // data
    private HashMap<String, CacheItem> cache;

    private Cache() {
        cache = new HashMap<String, CacheItem>();
        EventBus.get().addHandler(UserLoginEvent.TYPE, this);
        EventBus.get().addHandler(UserLogoutEvent.TYPE, this);
    }

    /** vraci instanci Cache */
    public static Cache get() {
        if (instance == null) {
            instance = new Cache();
        }
        return instance;
    }

    /** vstupni misto pro zadani akci s daty ulozenymi v cache */
    public void doCacheAction(CacheAction action) {
        doCacheAction(action, MAX_TIMEOUT_REPEAT);
    }

    /** vstupni misto pro zadani akci s daty ulozenymi v cache */
    public void doCacheAction(final CacheAction action, final int customTimeoutRepeat) {
        CacheItem o = cache.get(action.getName());
        if (o != null) {
            action.doAction(o.object);
            logger.debug("done action:" + o);
        } else {
            Timer t = new Timer() {

                int counter = 0;

                @Override
                public void run() {
                    if (action.isCancel()) {
                        logger.debug("action " + action.getName() + " canceled");
                        cancel();
                        return;
                    }

                    CacheItem o = cache.get(action.getName());
                    if (o != null) {
                        action.doAction(o.object);
                        logger.debug("done action for " + action.getName() + " from timer");
                        cancel();
                        return;
                    }
                    counter++;
                    if (counter > customTimeoutRepeat) {
                        logger.error("timeout for " + action.getName());
                        action.doTimeoutAction();
                        cancel();
                    }
                }
            };
            t.scheduleRepeating(TIMEOUT);
        }
    }

    /**
     * vraci objekt primo z cache, pokud neni zaregistrovany, vyhodi vyjimku,
     * jinak vraci nalezenou hodnotu
     *
     * @param key klic v cache
     */
    public <T> T getSafe(String key) throws NotInCacheException {
        if (!isRegistered(key)) {
            throw new NotInCacheException();
        }
        CacheItem o = cache.get(key);
        return (T) o.object;
    }

    /**
     * vraci objekt primo z cache, pokud neni zaregistrovany vraci nulu jinak
     * vraci nalezenou hodnotu
     *
     * @param key klic v cache
     */
    public <T> T get(String key) {
        CacheItem o = cache.get(key);
        return o == null ? null : (T) o.object;
    }

    /** @return true pokud je nazev zaregistrovany */
    public boolean isRegistered(String name) {
        return cache.containsKey(name);
    }

    public void register(String name, Object data, boolean systemProperty) {
        CacheItem item = new CacheItem(name, data, systemProperty);
        cache.put(name, item);
        logger.debug("cached: " + item);
    }

    /** zaregistruje data do cache */
    public void register(String name, Object data) {
        register(name, data, false);
    }

    /** odregistruje data z cache */
    public void unregister(String name) {
        cache.remove(name);
        logger.debug("variable " + name + " unregistered");
    }

    /** Vrati mapu nacachovanych objektov ktore su len systemove properties */
    public Map<String, String> getSystemPropertiesMap() {
        Map<String, String> systemProperties = new HashMap<String, String>();
        for (Map.Entry<String, CacheItem> e : cache.entrySet()) {
            if (e.getValue().isSystemProperty()) {
                systemProperties.put(e.getKey(), (String) e.getValue().object);
            }
        }
        return systemProperties;
    }

    /** zaregistruje identifikator vybrane zalozky pro konkretni tab panel */
    public void registerSelectedTab(String key, Object value) {
        getSelectedTabsMap().put(key, value);
    }

    /** vraci identifikator zalozky zadaneho tab panelu */
    public Object getSelectedTab(String key) {
        return getSelectedTabsMap().get(key);
    }

    /** mazne v cache zapamatovane vybrane zalozky v tab panelu */
    public void clearSelectedTabs() {
        getSelectedTabsMap().clear();
    }

    /** vraci mapu identifikatoru vybranych zalozek */
    private Map<String, Object> getSelectedTabsMap() {
        CacheItem i = cache.get(SELECTED_TABS);
        Map<String, Object> selectedTab;
        if (i == null) {
            selectedTab = new HashMap<String, Object>();
            CacheItem item = new CacheItem(SELECTED_TABS, selectedTab, false);
            cache.put(SELECTED_TABS, item);
        } else {
            selectedTab = (Map<String, Object>) i.getObject();
        }
        return selectedTab;
    }

    @Override
    public void onUserLogin(UserLoginEvent event) {
        register(LOGGED, Boolean.TRUE);
    }

    @Override
    public void onUserLogout(UserLogoutEvent event) {
        cache = new HashMap<String, CacheItem>();
        logger.debug(" -----  cleared -----");
        register(LOGGED, Boolean.FALSE);
    }

    private class CacheItem {

        private final String key;
        private final Object object;
        private final boolean systemProperty;

        public CacheItem(String key, Object object, boolean systemProperty) {
            this.key = key;
            this.object = object;
            this.systemProperty = systemProperty;
        }

        public String getKey() {
            return key;
        }

        public Object getObject() {
            return object;
        }

        public boolean isSystemProperty() {
            return systemProperty;
        }

        @Override
        public String toString() {
            return "Cache<" + key + ":" + object + ":" + systemProperty + ">";
        }

        @Override
        public boolean equals(Object obj) {
            if (obj != null && obj instanceof CacheItem) {
                return ((CacheItem) obj).key.equalsIgnoreCase(key);
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 19 * hash + (this.key != null ? this.key.hashCode() : 0);
            return hash;
        }
    }
}
