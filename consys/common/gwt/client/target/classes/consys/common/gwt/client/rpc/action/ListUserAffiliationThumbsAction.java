package consys.common.gwt.client.rpc.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import java.util.ArrayList;

/**
 * Action ktora vracia prelozene useruuids na thumby do zoznamu. Ak je nastavene
 * eventUuid tak je to profil vzhaldom k eventu ak nieje nastavene tak defaultne.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUserAffiliationThumbsAction implements Action<ArrayListResult<ClientUserWithAffiliationThumb>> {
    private static final long serialVersionUID = -3960223037530120582L;
    
    private ArrayList<String> uuids;
    private String eventUuid;

    public ListUserAffiliationThumbsAction() {
    }

    public ListUserAffiliationThumbsAction(ArrayList<String> uuids) {
        this.uuids = uuids;
    }

    public ListUserAffiliationThumbsAction(ArrayList<String> uuids, String eventUuid) {
        this.uuids = uuids;
        this.eventUuid = eventUuid;
    }

    public ArrayList<String> getThumbs() {
        return uuids;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public boolean hasEvent(){
        return eventUuid == null || eventUuid.length() > 0;
    }
}
