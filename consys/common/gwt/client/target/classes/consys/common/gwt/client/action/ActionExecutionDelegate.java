package consys.common.gwt.client.action;

/**
 * Delegat ktory je volany z ActionExecutora ktory informuje o zacati/ukonceni
 * akcie. Dale umoznuje nastavovat chybovou zpravu.
 * @author palo
 */
public interface ActionExecutionDelegate {

    void actionStarted();

    void actionEnds();

    void setFailMessage(CommonExceptionEnum value);
}
