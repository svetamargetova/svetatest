package consys.common.gwt.client.rpc.result;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class SelectBoxResult<T> implements Result {

    private static final long serialVersionUID = 259675303706709903L;
    private ArrayList<SelectBoxItem<T>> list;

    public SelectBoxResult() {
    }

    public SelectBoxResult(ArrayList<SelectBoxItem<T>> list) {
        this.list = list;
    }

    public ArrayList<SelectBoxItem<T>> getSelectBoxResult() {
        return list;
    }
}
