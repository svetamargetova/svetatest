package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Jedna zprava ve vlaknu
 * @author pepa
 */
public class ClientThreadMessage extends JavaScriptObject {

    protected ClientThreadMessage() {
    }

    /** @return cas odeslani zpravy */
    public final long getTime() {
        return (long) getDoubleTime();
    }

    /** @return cas odeslani zpravy, js ho umi nacist jen jako double */
    private final native double getDoubleTime() /*-{
        return this.time;
    }-*/;

    /** @return uuid odesilatele zpravy */
    public final native String getFromUuid() /*-{
        return this.f;
    }-*/;

    /** @return true pokud byla zprava jiz prectena */
    public final native boolean isReaded() /*-{
        return this.rd;
    }-*/;

    /** @return vlastni text zpravy */
    public final native String getMessage() /*-{
        return this.msg;
    }-*/;
}
