package consys.common.gwt.client.messaging.module;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.messaging.exception.BadResponseStatusCodeException;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.module.AsyncModule;

/**
 * Rozhrani pro praci s Messagingem
 * @author pepa
 */
public class MessagingApi {

    private static final Logger logger = LoggerFactory.getLogger(MessagingApi.class);
    // instance
    private static MessagingApi instance;
    // konstanty
    /** server zpracoval uspesne request */
    public static final int STATUS_OK = 200;
    // data
    private ApiMessage apiMessage;
    private ApiWall apiWall;

    private MessagingApi() {
        apiMessage = new ApiMessage(this);
        apiWall = new ApiWall(this);
    }

    /** vytvori asynchronne instanci */
    public static void getAsync(final AsyncModule<MessagingApi> asyncModule) {
        GWT.runAsync(new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable reason) {
                asyncModule.onFail();
            }

            @Override
            public void onSuccess() {
                if (instance == null) {
                    instance = new MessagingApi();
                }
                asyncModule.onSuccess(instance);
            }
        });
    }

    /** nastavi url messaging serveru pro klienty (neco ve stylu http://123.123.123.123/messaging/client) */
    public void setMessagingUrl(String messagingUrl) {
        apiMessage.setUrl(messagingUrl);
        apiWall.setUrl(messagingUrl);
    }

    /** vraci rozhrani pro praci se zpravami entity */
    public ApiMessage getApiMessage() {
        return apiMessage;
    }

    /** vraci rozhrani pro praci se zpravami pro zed */
    public ApiWall getApiWall() {
        return apiWall;
    }

    /**
     * odesle objekt na messagingovy server
     * @return true pokud se odeslalo v poradku
     */
    public <T> boolean send(JSONObject jsonRequest, final String url, final AsyncCallback<T> callback) {
        if (url == null) {
            logger.error("Messaging URL not set");
            return false;
        }
        try {
            RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, url);

            rb.sendRequest(jsonRequest.toString(), new RequestCallback() {

                @Override
                public void onResponseReceived(Request request, Response response) {
                    int returnCode = response.getStatusCode();
                    if (returnCode == STATUS_OK) {
                        // ok
                        T object = (T) JSONParser.parse(response.getText()).isObject().getJavaScriptObject();
                        callback.onSuccess(object);
                    } else {
                        // chyba
                        callback.onFailure(new BadResponseStatusCodeException(returnCode + ":" + response.getStatusText()));
                    }
                }

                @Override
                public void onError(Request request, Throwable exception) {
                    logger.error("request send failed", exception);
                    callback.onFailure(exception);
                }
            });
        } catch (RequestException ex) {
            logger.error("request send failed", ex);
            return false;
        }
        return true;
    }

    /** vraci textovou hlasku pro prislusny stav */
    public static String statusText(String status) {
        String result;
        if (status.equals(ResponseUtils.STATE_ACCESS_DENIED)) {
            result = CMMessageUtils.c.messagingApi_text_noPermission();
        } else if (status.equals(ResponseUtils.STATE_BAN)) {
            result = CMMessageUtils.c.messagingApi_text_banned();
        } else if (status.equals(ResponseUtils.STATE_CLOSED)) {
            result = CMMessageUtils.c.messagingApi_text_serverClosed();
        } else if (status.equals(ResponseUtils.STATE_ENTITY_NOT_FOUND)) {
            result = CMMessageUtils.c.messagingApi_text_entityNotFound();
        } else if (status.equals(ResponseUtils.STATE_FAIL)) {
            result = CMMessageUtils.c.messagingApi_text_requestFail();
        } else if (status.equals(ResponseUtils.STATE_MESSAGE_NOT_FOUND)) {
            result = CMMessageUtils.c.messagingApi_text_messageNotFound();
        } else if (status.equals(ResponseUtils.STATE_NEW_MESSAGE)) {
            result = CMMessageUtils.c.messagingApi_text_newMessage();
        } else if (status.equals(ResponseUtils.STATE_THREAD_NOT_FOUND)) {
            result = CMMessageUtils.c.messagingApi_text_threadNotFound();
        } else {
            result = CMMessageUtils.c.messagingApi_text_unknownStatus();
        }
        return result;
    }
}
