package consys.common.gwt.client;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.messaging.EventWallUI;
import consys.common.gwt.client.messaging.entity.MessageBox;
import consys.common.gwt.client.messaging.module.EventWallModule;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.messaging.panel.BugPanel;
import consys.common.gwt.client.module.AsyncContent;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleRegistrator;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.action.LoadUserThumbAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.layout.menu.MenuDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelItemType;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class MessagingEntryPoint extends DebugModuleEntryPoint {

    @Override
    public void initMocks() {

        ClientUser u = new ClientUser();
        //u.setUuid("f0ccbbd3-22d2-40e2-892a-211c448026e4");
        u.setUuid("f0edcc00dbbf44269c6c21eb008019c7");
        Cache.get().register(UserCacheAction.USER, u);

        ClientEvent e = new ClientEvent();
        e.setUniversalName("Universal event long long name");
        e.setUuid("f0ccbbd3-22d2-40e2-892a-211c448026e4");
        ClientCurrentEvent ce = new ClientCurrentEvent();
        ce.setEvent(e);
        Cache.get().register(CurrentEventCacheAction.CURRENT_EVENT, ce);

        ClientUserThumb userThumb = new ClientUserThumb();
        userThumb.setName("prof. Doc. Martin Kukucin, CSC");
        userThumb.setUuidPortraitImagePrefix("234");
        userThumb.setUuid("123");
        MockFactory.addActionMock(LoadUserThumbAction.class, new ActionMock<ClientUserThumb>(userThumb));

        // ---- ListUserThumbsAction
        ArrayListResult<ClientUserThumb> userThumbs = new ArrayListResult<ClientUserThumb>();
        for (int i = 0; i < 30; i++) {
            ClientUserThumb cult = new ClientUserThumb();
            cult.setName("prof. Doc. Martin Kukucin, CSC");
            userThumbs.getArrayListResult().add(cult);
        }
        MockFactory.addActionMock(ListUserThumbsAction.class, new ActionMock<ArrayListResult<ClientUserThumb>>(userThumbs));

        // nastavi cache, ze je neprihlaseny uzivatel
        Cache.get().register(Cache.LOGGED, Boolean.TRUE);
        // zinicializovani MenuDispatcheru a vylozeni menu
        //MenuDispatcher.get().setSignUpWidget(new WallUI());
        MenuDispatcher.get().generateMenu();
        // zinicializovani PanelDispatcheru a vlozeni panelu
        PanelDispatcher.get().addPanelItem(new BugPanel(), PanelItemType.SYSTEM);

        // registrace modulu po prihlaseni
        for (Module module : ModuleRegistrator.get().getLoggedInModules()) {
            module.registerModule();
        }
    }

    @Override
    public void registerModules(List<Module> modules) {
        modules.add(new MessagingModule());
        modules.add(new EventWallModule());
    }

    @Override
    public void registerForms(Map<String, Widget> formMap) {
        formMap.put("messages", new AsyncContent() {

            @Override
            public void runComponent() {
                EventBus.get().fireEvent(new ChangeContentEvent(new MessageBox()));
            }
        });
        formMap.put("wall ui", new AsyncContent() {

            @Override
            public void runComponent() {
                EventBus.get().fireEvent(new ChangeContentEvent(new EventWallUI(true, true, true, null)));
            }
        });
    }
}
