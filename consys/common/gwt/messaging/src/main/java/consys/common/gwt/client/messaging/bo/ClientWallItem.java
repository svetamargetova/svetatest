package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Jedna polozka na zdi
 * @author pepa
 */
public class ClientWallItem extends JavaScriptObject {

    protected ClientWallItem() {
    }

    /** @return cas nejnovejsi zpravy vlakna */
    public final long getTime() {
        return (long) getDoubleTime();
    }

    /** @return cas odeslani zpravy, js ho umi nacist jen jako double */
    private final native double getDoubleTime() /*-{
        return this.time;
    }-*/;

    /** @return uuid autora */
    public final native String getAuthor() /*-{
        return this.f;
    }-*/;

    /** @return text zpravy */
    public final native String getText() /*-{
        return this.msg;
    }-*/;
}
