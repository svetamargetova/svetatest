package consys.common.gwt.client.messaging.utils;

/**
 *
 * @author pepa
 */
public class RequestUtils {

    /** tag t */
    public static final String REQUEST_TYPE = "t";
    /** tag e */
    public static final String REQUEST_ENTITY = "e";
    /** tag thrd */
    public static final String REQUEST_THREAD = "thrd";
    /** tag f */
    public static final String REQUEST_FROM = "f";
    /** tag to */
    public static final String REQUEST_TO = "to";
    /** tag sub */
    public static final String REQUEST_SUBJECT = "sub";
    /** tag msg */
    public static final String REQUEST_MESSAGE = "msg";
    /** tag num */
    public static final String REQUEST_NUMBER = "num";
    /** tag offs */
    public static final String REQUEST_OFFSET = "offs";
    /** tag time */
    public static final String REQUEST_TIME = "time";
    // typy pozadavku
    public static final String TYPE_DELETE_WALL_MESSAGE = "delWM";
    public static final String TYPE_SEND_WALL_MESSAGE = "sendWM";
    public static final String TYPE_SEND_NEW_MESSAGE = "sendNM";
    public static final String TYPE_SEND_THREAD_MESSAGE = "sendTM";
    public static final String TYPE_LOAD_FOLDER = "lf";
    public static final String TYPE_LOAD_SENT_MESSAGES = "lsm";
    public static final String TYPE_LOAD_THREAD = "lt";
    public static final String TYPE_LOAD_WALL = "lw";
    public static final String TYPE_MARK_MESSAGE = "mm";
    public static final String TYPE_MARK_THREAD = "mt";
    public static final String TYPE_UPDATE_WALL_MESSAGE = "upWM";
    public static final String TYPE_BUG = "bug";
}
