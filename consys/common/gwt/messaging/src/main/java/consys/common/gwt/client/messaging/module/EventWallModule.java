package consys.common.gwt.client.messaging.module;

import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleMenuRegistrator;

/**
 *
 * @author pepa
 */
public class EventWallModule implements Module {

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
    }

    @Override
    public void registerModule() {
    }

    @Override
    public void unregisterModule() {
    }
}
