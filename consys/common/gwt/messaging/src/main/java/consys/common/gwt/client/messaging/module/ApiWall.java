package consys.common.gwt.client.messaging.module;

import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.messaging.bo.ClientResponse;
import consys.common.gwt.client.messaging.bo.ClientWall;
import consys.common.gwt.client.messaging.bo.CommonClientResponse;
import consys.common.gwt.client.messaging.bo.RequestWallMessage;
import consys.common.gwt.client.messaging.utils.RequestUtils;

/**
 * api pro praci se zpravami na wall
 * @author pepa
 */
public class ApiWall {

    // data
    private MessagingApi api;
    private String url;

    ApiWall(MessagingApi api) {
        this.api = api;
    }

    /** nastavi komunikacni url */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * smaze zpravu ze zdi
     * @param uuid uuid zdi
     * @param timestamp casove razitko zpravy
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean deleteWallMessage(final String uuid, final long timestamp, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_DELETE_WALL_MESSAGE));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(uuid));
        request.put(RequestUtils.REQUEST_TIME, new JSONNumber(timestamp));
        return api.send(request, url, callback);
    }
    /**
     * nacte zed
     * @param uuid uuid zdi
     * @param num kolik se ma nacist polozek (pro strankovani)
     * @param offset od kolikate polozky se ma nacitat (pro strankovani)
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean loadWall(final String uuid, final int number, final long offset, AsyncCallback<ClientResponse<ClientWall>> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_LOAD_WALL));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(uuid));
        request.put(RequestUtils.REQUEST_NUMBER, new JSONNumber(number));
        request.put(RequestUtils.REQUEST_OFFSET, new JSONNumber(offset));
        return api.send(request, url, callback);
    }

    /**
     * odesle novou zpravu na zed
     * @param msg zprava k odeslani
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean sendWallMessage(final RequestWallMessage msg, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_SEND_WALL_MESSAGE));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(msg.getFrom()));
        request.put(RequestUtils.REQUEST_TO, new JSONString(msg.getTo()));
        request.put(RequestUtils.REQUEST_MESSAGE, new JSONString(msg.getMessage()));
        return api.send(request, url, callback);
    }

    /**
     * aktualizuje zpravu na zdi
     * @param msg aktualizovana zprava
     * @param timestamp casove razitko aktualizovane zpravy
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean updateWallMessage(final RequestWallMessage msg, final long timestamp, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_UPDATE_WALL_MESSAGE));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(msg.getFrom()));
        request.put(RequestUtils.REQUEST_TO, new JSONString(msg.getTo()));
        request.put(RequestUtils.REQUEST_MESSAGE, new JSONString(msg.getMessage()));
        request.put(RequestUtils.REQUEST_TIME, new JSONNumber(timestamp));
        return api.send(request, url, callback);
    }
}
