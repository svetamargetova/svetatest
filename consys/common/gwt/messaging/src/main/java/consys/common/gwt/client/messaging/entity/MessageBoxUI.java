package consys.common.gwt.client.messaging.entity;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.messaging.bo.ClientFolder;
import consys.common.gwt.client.messaging.bo.ClientFolderItem;
import consys.common.gwt.client.messaging.bo.ClientResponse;
import consys.common.gwt.client.messaging.module.ApiMessage;
import consys.common.gwt.client.messaging.module.MessagingApi;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.ListDataSource;
import consys.common.gwt.client.ui.comp.list.ListDataSourceRequest;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.comp.list.item.DataListItem;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.event.BreadcrumbContentEvent;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.NotInCacheException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Schranka zprav
 * @author pepa
 */
public class MessageBoxUI extends DataListPanel implements MenuFormItem {

    // data
    private ApiMessage apiMessage;
    private MessageBox parent;
    private HashMap<String, ClientUserThumb> users;
    private long actualTimestamp = 0;
    private long previousTimestamp = 0;
    private long nextTimestamp = 0;
    private long firstResult = 0;
    private HashMap<Integer, ClientFolderItem> previousFirstListItem;

    public MessageBoxUI(MessageBox parent) {
        super("", "NO_TAG");
        setHeadPanelVisible(false);
        this.parent = parent;
        previousFirstListItem = new HashMap<Integer, ClientFolderItem>();
        users = new HashMap<String, ClientUserThumb>();

        setListDelegate(new ThreadListDelegate());
        setListDataSource(new ListDataSource() {

            @Override
            public void sendRequest(final ListDataSourceRequest request, final AsyncCallback callback, final ActionExecutionDelegate executionDelegate) {
                try {
                    executionDelegate.actionStarted();

                    String uuid = ((ClientUser) Cache.get().getSafe(UserCacheAction.USER)).getUuid();
                    MessagingApi api = (MessagingApi) Cache.get().getSafe(MessagingModule.MESSAGING_API);

                    final long timestamp;
                    if (request.getFirstResult() == 0) {
                        timestamp = 0;
                    } else if (firstResult < request.getFirstResult()) {
                        timestamp = nextTimestamp;
                    } else if (firstResult > request.getFirstResult()) {
                        timestamp = previousTimestamp;
                    } else {
                        timestamp = actualTimestamp;
                    }

                    apiMessage = api.getApiMessage();
                    apiMessage.loadFolder(uuid, request.getItemsPerPage(), timestamp,
                            new AsyncCallback<ClientResponse<ClientFolder>>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    executionDelegate.actionEnds();
                                    getFailMessage().setText(CMMessageUtils.c.const_communicationError());
                                }

                                @Override
                                public void onSuccess(ClientResponse<ClientFolder> result) {
                                    executionDelegate.actionEnds();
                                    if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                        JsArray<ClientFolderItem> list = result.getResponse().getFolderItems();
                                        ClientFolderItem item = list.length() > 0 ? list.get(0) : null;
                                        previousFirstListItem.put(request.getFirstResult(), item);

                                        if (request.getFirstResult() != 0) {
                                            ClientFolderItem previous = previousFirstListItem.get(request.getFirstResult() - request.getItemsPerPage());
                                            previousTimestamp = previous == null ? 0 : previous.getTime();
                                        } else {
                                            previousTimestamp = 0;
                                        }
                                        nextTimestamp = result.getResponse().getNextPageTimestamp();
                                        firstResult = request.getFirstResult();
                                        actualTimestamp = timestamp;

                                        loadAndShowUsers(result.getResponse(), callback);
                                    } else {
                                        getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                                    }
                                }
                            });
                } catch (NotInCacheException ex) {
                    executionDelegate.actionEnds();
                    // TODO: vypsat chybu
                }
            }
        });
        setDefaultFilter(new ListFilter(0) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
    }

    /** nacte data uzivatelu a necha vykreslit seznam */
    private void loadAndShowUsers(final ClientFolder cf, final AsyncCallback callback) {
        JsArray<ClientFolderItem> folders = cf.getFolderItems();

        final ArrayList<MessageBoxListItem> itms = new ArrayList<MessageBoxListItem>();
        for (int i = 0; i < folders.length(); i++) {
            MessageBoxListItem listItem = new MessageBoxListItem();
            listItem.setItem(folders.get(i));
            itms.add(listItem);
        }

        final ArrayList<String> uuids = new ArrayList<String>();
        for (int i = 0; i < folders.length(); i++) {
            String uuid = folders.get(i).getAuthor();
            if (!uuids.contains(uuid)) {
                uuids.add(uuid);
            }
        }

        if (!uuids.isEmpty()) {
            clearMessageBox();
            DispatchEvent de = new DispatchEvent(new ListUserThumbsAction(uuids),
                    new AsyncCallback<ArrayListResult<ClientUserThumb>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                            if (caught instanceof NoRecordsForAction) {
                                getFailMessage().setText("Loading user data failed");
                                ListDataSourceResult dataSourceResult = new ListDataSourceResult(itms, cf.getTotalItems());
                                callback.onSuccess(dataSourceResult);
                            }
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientUserThumb> result) {
                            users.clear();
                            for (ClientUserThumb cut : result.getArrayListResult()) {
                                users.put(cut.getUuid(), cut);
                            }
                            ListDataSourceResult dataSourceResult = new ListDataSourceResult(itms, cf.getTotalItems());
                            callback.onSuccess(dataSourceResult);
                        }
                    },
                    this);
            EventBus.get().fireEvent(de);
        } else {
            ListDataSourceResult dataSourceResult = new ListDataSourceResult(itms, cf.getTotalItems());
            callback.onSuccess(dataSourceResult);
        }
    }

    @Override
    public String getName() {
        return CMMessageUtils.c.messageBoxUI_title();
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.setWidget(this);
    }

    /** polozka data listu */
    private class MessageBoxListItem extends DataListItem {

        private static final long serialVersionUID = 4693309053387327225L;
        // data
        private ClientFolderItem item;

        public MessageBoxListItem() {
        }

        public ClientFolderItem getItem() {
            return item;
        }

        public void setItem(ClientFolderItem item) {
            this.item = item;
        }
    }

    /** delegat seznamu */
    private class ThreadListDelegate implements ListDelegate<MessageBoxListItem, MessageBoxItem> {

        @Override
        public MessageBoxItem createCell(MessageBoxListItem item) {
            return new MessageBoxItem(item);
        }
    }

    /** jedna polozka seznamu */
    private class MessageBoxItem extends ListItem<MessageBoxListItem> {

        public MessageBoxItem(MessageBoxListItem item) {
            super(item);
        }

        @Override
        protected void createCell(final MessageBoxListItem item, FlowPanel panel) {
            panel.clear();

            ActionLabel show = new ActionLabel(CMMessageUtils.c.messageBoxUI_action_show(),
                    FLOAT_RIGHT + " " + MARGIN_RIGHT_5 + " " + MARGIN_TOP_5);
            show.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BreadcrumbContentEvent be = new BreadcrumbContentEvent(CMMessageUtils.c.messageBox_title(),
                            BreadcrumbContentEvent.LEVEL.ROOT, new ChangeContentEvent(parent), false);
                    EventBus.get().fireEvent(be);

                    MessageThreadUI mui = new MessageThreadUI(item.getItem().getThread(), item.getItem().isUpdated());
                    FormUtils.fireNextBreadcrumb(item.getItem().getName(), mui);
                }
            });
            panel.add(show);

            ClientUserThumb user = users.get(item.getItem().getAuthor());
            Image portrait = FormUtils.userPortrait(user != null ? user.getUuidPortraitImagePrefix() : null, UserProfileImageEnum.LIST);
            if (user == null) {
                user = new ClientUserThumb();
                user.setName(CMMessageUtils.c.const_notLoaded());
            }

            FlowPanel portraitWrapper = FormUtils.portraitWrapper(UserProfileImageEnum.LIST);
            portraitWrapper.add(portrait);
            panel.add(portraitWrapper);

            final Label timeLabel = StyleUtils.getStyledLabel(DateTimeUtils.getMonthDayYear(new Date(item.getItem().getTime())),
                    FONT_10PX, FLOAT_LEFT, MARGIN_RIGHT_10);
            final Label updatedLabel = StyleUtils.getStyledLabel(item.getItem().isUpdated()
                    ? CMMessageUtils.c.const_newUpper() : "", FONT_10PX, FLOAT_LEFT, TEXT_RED);
            final Label nameLabel = StyleUtils.getStyledLabel(user.getName(), MARGIN_TOP_5, FONT_BOLD, FONT_11PX);
            final Label subjectLabel = StyleUtils.getStyledLabel(item.getItem().getName(), MARGIN_TOP_5, FONT_14PX);

            FlowPanel timePanel = new FlowPanel();
            timePanel.add(timeLabel);
            timePanel.add(updatedLabel);
            timePanel.add(StyleUtils.clearDiv());

            FlowPanel content = new FlowPanel();
            content.addStyleName(FLOAT_LEFT);
            content.add(timePanel);
            content.add(nameLabel);
            content.add(subjectLabel);
            panel.add(content);

            panel.add(StyleUtils.clearDiv());
        }
    }
}
