package consys.common.gwt.client.messaging.bo;

/**
 * Abstraktni predekj pozadavku zpravy
 * @author pepa
 */
public abstract class RequestMessage {

    private String from;
    private String message;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
