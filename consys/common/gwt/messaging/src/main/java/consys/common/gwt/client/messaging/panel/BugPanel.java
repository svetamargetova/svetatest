package consys.common.gwt.client.messaging.panel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.GWT.UncaughtExceptionHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.messaging.bo.CommonClientResponse;
import consys.common.gwt.client.messaging.module.ApiMessage;
import consys.common.gwt.client.messaging.module.MessagingApi;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Panel pro zasilani bugu
 * @author pepa
 */
public class BugPanel extends PanelItem {

    // konstanty
    public static final String PANEL_NAME = "BugPanel";
    // data
    private ApiMessage message;
    private String uncaught;

    public BugPanel() {
        super(false);
        GWT.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {

            @Override
            public void onUncaughtException(Throwable e) {
                uncaught = new Date() + ": " + e.getMessage();                
            }
        });
    }

    @Override
    public String getName() {
        return PANEL_NAME;
    }

    @Override
    public Widget getTitle() {
        return null;
    }

    @Override
    public Widget getContent() {
        FlowPanel panel = new FlowPanel() {

            @Override
            protected void onLoad() {
                Cache.get().doCacheAction(new CacheAction<MessagingApi>() {

                    @Override
                    public String getName() {
                        return MessagingModule.MESSAGING_API;
                    }

                    @Override
                    public void doAction(MessagingApi value) {
                        message = value.getApiMessage();
                    }

                    @Override
                    public void doTimeoutAction() {
                        // nemelo by nastat
                    }

                    @Override
                    public boolean isCancel() {
                        return false;
                    }
                });
            }
        };
        DOM.setStyleAttribute(panel.getElement(), "paddingLeft", "10px");

        panel.add(StyleUtils.getStyledLabel(CMMessageUtils.c.bugPanel_text_reportBug(), CssStyles.MARGIN_BOT_5, CssStyles.FONT_BOLD));

        SelectBoxItem<String> functionItem = new SelectBoxItem<String>("function", CMMessageUtils.c.bugPanel_text_bugFunction());
        SelectBoxItem<String> graphicItem = new SelectBoxItem<String>("graphic", CMMessageUtils.c.bugPanel_text_bugGraphic());
        SelectBoxItem<String> otherItem = new SelectBoxItem<String>("other", CMMessageUtils.c.bugPanel_text_bugOther());

        ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
        items.add(functionItem);
        items.add(graphicItem);
        items.add(otherItem);

        final SelectBox<String> bugType = new SelectBox<String>();
        bugType.setWidth(200);
        bugType.setItems(items);
        bugType.selectFirst(true);
        panel.add(bugType);

        final ConsysTextArea area = new ConsysTextArea(0, "200px", "50px", false, "");
        area.addStyleName(CssStyles.MARGIN_VER_5);
        panel.add(area);

        final Label sendFailed = StyleUtils.getStyledLabel(CMMessageUtils.c.bugPanel_text_sendFailed(), CssStyles.MARGIN_BOT_5, CssStyles.TEXT_RED);
        sendFailed.setVisible(false);
        panel.add(sendFailed);

        ActionImage sendBug = ActionImage.getConfirmButton(CMMessageUtils.c.bugPanel_button_sendBug());
        sendBug.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (message != null) {
                    sendFailed.setVisible(false);
                    String type = bugType.getSelectedItem().getItem();
                    String msg = area.getText().trim();
                    if (uncaught != null) {
                        msg += "\nnow: " + new Date() + "\n";
                        msg += "\n" + uncaught;
                    }

                    String userName = "";
                    try {
                        ClientUser cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
                        userName = cu.name() + " " + cu.getUuid().substring(0, 5);
                    } catch (NotInCacheException ex) {
                        // aktualni uzivatel nenalezen, coz by mel
                    }

                    message.sendBug(userName, type, msg, new AsyncCallback<CommonClientResponse>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            sendFailed.setVisible(true);
                        }

                        @Override
                        public void onSuccess(CommonClientResponse result) {
                            result.getStatus();
                            if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                bugType.selectItemByIndex(0);
                                area.setText("");
                            } else {
                                sendFailed.setVisible(true);
                            }
                        }
                    });
                } else {
                    sendFailed.setVisible(true);
                }
            }
        });
        panel.add(sendBug);

        SimplePanel sp = new SimplePanel();
        sp.setWidget(panel);

        return sp;
    }
}
