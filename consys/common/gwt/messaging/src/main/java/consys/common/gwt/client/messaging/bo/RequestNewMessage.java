package consys.common.gwt.client.messaging.bo;

import java.util.ArrayList;

/**
 * Objekt zpravy pro odeslani na messagingovy server
 * @author pepa
 */
public class RequestNewMessage extends RequestMessage {

    private String subject;
    private ArrayList<String> to;

    public RequestNewMessage() {
        to = new ArrayList<String>();
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public ArrayList<String> getTo() {
        return to;
    }

    public void setTo(ArrayList<String> to) {
        this.to = to;
    }
}
