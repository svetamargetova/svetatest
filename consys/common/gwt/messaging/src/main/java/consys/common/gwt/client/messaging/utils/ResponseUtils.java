package consys.common.gwt.client.messaging.utils;

/**
 *
 * @author pepa
 */
public class ResponseUtils {

    // konstanty - vysledkove stavy
    public static final String STATE_OK = "OK";
    public static final String STATE_FAIL = "FAIL";
    public static final String STATE_CLOSED = "CLOSED";
    public static final String STATE_BAN = "BAN";
    public static final String STATE_ENTITY_NOT_FOUND = "ENF";
    public static final String STATE_THREAD_NOT_FOUND = "TNF";
    public static final String STATE_MESSAGE_NOT_FOUND = "MNF";
    public static final String STATE_ACCESS_DENIED = "DENIED";
    public static final String STATE_NEW_MESSAGE = "NEW";
}
