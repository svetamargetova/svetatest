package consys.common.gwt.client.messaging.entity;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.messaging.bo.ClientResponse;
import consys.common.gwt.client.messaging.bo.ClientSentMessages;
import consys.common.gwt.client.messaging.bo.ClientSentMessagesItem;
import consys.common.gwt.client.messaging.module.ApiMessage;
import consys.common.gwt.client.messaging.module.MessagingApi;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.ListDataSource;
import consys.common.gwt.client.ui.comp.list.ListDataSourceRequest;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.comp.list.item.DataListItem;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.shared.exception.NotInCacheException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Seznam odeslaných zpráv
 * @author pepa
 */
public class MessageSentBoxUI extends DataListPanel implements MenuFormItem {

    // data
    private ClientUser user;
    private long actualTimestamp = 0;
    private long previousTimestamp = 0;
    private long nextTimestamp = 0;
    private long firstResult = 0;
    private HashMap<Integer, ClientSentMessagesItem> previousFirstListItem;

    public MessageSentBoxUI() {
        super("", "NO_TAG");
        setHeadPanelVisible(false);
        previousFirstListItem = new HashMap<Integer, ClientSentMessagesItem>();

        setListDelegate(new SentListDelegate());
        setListDataSource(new ListDataSource() {

            @Override
            public void sendRequest(final ListDataSourceRequest request, final AsyncCallback callback, final ActionExecutionDelegate executionDelegate) {
                try {
                    executionDelegate.actionStarted();

                    user = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
                    String uuid = user.getUuid();
                    MessagingApi api = (MessagingApi) Cache.get().getSafe(MessagingModule.MESSAGING_API);

                    final long timestamp;
                    if (request.getFirstResult() == 0) {
                        timestamp = 0;
                    } else if (firstResult < request.getFirstResult()) {
                        timestamp = nextTimestamp;
                    } else if (firstResult > request.getFirstResult()) {
                        timestamp = previousTimestamp;
                    } else {
                        timestamp = actualTimestamp;
                    }

                    ApiMessage apiMessage = api.getApiMessage();
                    apiMessage.loadSentMessages(uuid, request.getItemsPerPage(), timestamp,
                            new AsyncCallback<ClientResponse<ClientSentMessages>>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    executionDelegate.actionEnds();
                                    getFailMessage().setText(CMMessageUtils.c.const_communicationError());
                                }

                                @Override
                                public void onSuccess(ClientResponse<ClientSentMessages> result) {
                                    executionDelegate.actionEnds();
                                    if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                        JsArray<ClientSentMessagesItem> list = result.getResponse().getSentItems();
                                        ClientSentMessagesItem item = list.length() > 0 ? list.get(0) : null;
                                        previousFirstListItem.put(request.getFirstResult(), item);

                                        if (request.getFirstResult() != 0) {
                                            ClientSentMessagesItem previous = previousFirstListItem.get(request.getFirstResult() - request.getItemsPerPage());
                                            previousTimestamp = previous == null ? 0 : previous.getTime();
                                        } else {
                                            previousTimestamp = 0;
                                        }
                                        nextTimestamp = result.getResponse().getNextPageTimestamp();
                                        firstResult = request.getFirstResult();
                                        actualTimestamp = timestamp;

                                        loadAndShowMessages(result.getResponse(), callback);
                                    } else {
                                        getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                                    }
                                }
                            });
                } catch (NotInCacheException ex) {
                    executionDelegate.actionEnds();
                    // TODO: vypsat chybu
                }
            }
        });
        setDefaultFilter(new ListFilter(0) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
    }

    /** nacte data uzivatelu a necha vykreslit seznam */
    private void loadAndShowMessages(final ClientSentMessages csm, final AsyncCallback callback) {
        JsArray<ClientSentMessagesItem> folders = csm.getSentItems();

        final ArrayList<MessageSentBoxListItem> itms = new ArrayList<MessageSentBoxListItem>();
        for (int i = 0; i < folders.length(); i++) {
            MessageSentBoxListItem listItem = new MessageSentBoxListItem();
            listItem.setItem(folders.get(i));
            itms.add(listItem);
        }

        ListDataSourceResult dataSourceResult = new ListDataSourceResult(itms, csm.getTotalItems());
        callback.onSuccess(dataSourceResult);
    }

    @Override
    public String getName() {
        return CMMessageUtils.c.messageSentBoxUI_title();
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.setWidget(this);
    }

    /** polozka data listu */
    private class MessageSentBoxListItem extends DataListItem {

        private static final long serialVersionUID = 4693309053387327225L;
        // data
        private ClientSentMessagesItem item;

        public MessageSentBoxListItem() {
        }

        public ClientSentMessagesItem getItem() {
            return item;
        }

        public void setItem(ClientSentMessagesItem item) {
            this.item = item;
        }
    }

    /** delegat seznamu */
    private class SentListDelegate implements ListDelegate<MessageSentBoxListItem, MessageSentBoxItem> {

        @Override
        public MessageSentBoxItem createCell(MessageSentBoxListItem item) {
            return new MessageSentBoxItem(item);
        }
    }

    /** jedna polozka seznamu */
    private class MessageSentBoxItem extends ListItem<MessageSentBoxListItem> {

        public MessageSentBoxItem(MessageSentBoxListItem item) {
            super(item);
        }

        @Override
        protected void createCell(MessageSentBoxListItem item, FlowPanel panel) {
            panel.clear();

            Image portrait = FormUtils.userPortrait(user != null ? user.getUuidImg() : null, UserProfileImageEnum.LIST);
            if (user == null) {
                user = new ClientUser();
                user.setFirstName(CMMessageUtils.c.const_notLoaded());
            }

            FlowPanel portraitWrapper = FormUtils.portraitWrapper(UserProfileImageEnum.LIST);
            portraitWrapper.add(portrait);
            panel.add(portraitWrapper);

            final Label timeLabel = StyleUtils.getStyledLabel(DateTimeUtils.getMonthDayYear(new Date(item.getItem().getTime())), FONT_10PX);
            //final Label nameLabel = StyleUtils.getStyledLabel(user.name(), MARGIN_TOP_5, FONT_BOLD, FONT_11PX);
            final Label subjectLabel = StyleUtils.getStyledLabel(item.getItem().getName(), MARGIN_TOP_5, FONT_14PX, FONT_BOLD);
            final Label messageLabel = StyleUtils.getStyledLabel(item.getItem().getMessage(), MARGIN_TOP_5, FONT_14PX);

            FlowPanel contentWrapper = new FlowPanel();
            contentWrapper.addStyleName(FLOAT_LEFT);
            contentWrapper.add(timeLabel);
            //contentWrapper.add(nameLabel);
            contentWrapper.add(subjectLabel);
            contentWrapper.add(messageLabel);
            panel.add(contentWrapper);

            panel.add(StyleUtils.clearDiv());
        }
    }
}
