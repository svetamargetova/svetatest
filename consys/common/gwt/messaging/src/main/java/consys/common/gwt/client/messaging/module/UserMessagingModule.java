package consys.common.gwt.client.messaging.module;

import consys.common.gwt.client.messaging.entity.MessageBox;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleMenuRegistrator;

/**
 *
 * @author pepa
 */
public class UserMessagingModule implements Module {

    // konstanty
    private static final String MESSAGING = "Messaging";

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
        MessageBox mb = new MessageBox();
        menuRegistrator.registerMenuItem(new ModuleMenuItem(mb, CMMessageUtils.c.userMessagingModule_text_messaging(),
                MESSAGING, 2));
    }

    @Override
    public void registerModule() {
    }

    @Override
    public void unregisterModule() {
    }
}
