package consys.common.gwt.client.messaging;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.messaging.bo.ClientWallItem;
import consys.common.gwt.client.messaging.bo.CommonClientResponse;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.LoadUserThumbAction;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientUserThumb;

/**
 * Jeden zaznam na zdi
 * @author pepa
 */
public class BaseWallItem extends FlowPanel {

    // komponenty
    private Label ago;
    // data
    private long origTime;

    public BaseWallItem(final Wall wall, final ClientWallItem item, ClientCurrentEvent ce, boolean removable) {
        super();
        addStyleName(StyleUtils.CLEAR_BOTH);

        final Label author = StyleUtils.getStyledLabel(UIMessageUtils.c.const_loadingDots(),
                StyleUtils.FONT_14PX, StyleUtils.TEXT_BLUE, StyleUtils.FONT_UNDERLINE, StyleUtils.FLOAT_LEFT);
        author.setHeight("23px");

        final String eventUuid = ce.getEvent().getUuid();
        if (item.getAuthor().equals(eventUuid)) {
            author.setText(ce.getEvent().getUniversalName());
        } else {
            DispatchEvent loadEvent = new DispatchEvent(
                    new LoadUserThumbAction(item.getAuthor()),
                    new AsyncCallback<ClientUserThumb>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava ActionExecutor
                        }

                        @Override
                        public void onSuccess(final ClientUserThumb result) {
                            author.setText(result.getName());
                        }
                    }, null);
            EventBus.get().fireEvent(loadEvent);
        }

        Remover remover = null;
        if (removable) {
            Image remove = new Image(ResourceUtils.system().removeCross());
            remove.setStyleName(StyleUtils.HAND);
            ConsysAction removeAction = new ConsysAction() {

                @Override
                public void run() {
                    wall.getApi().getApiWall().deleteWallMessage(eventUuid, item.getTime(), new AsyncCallback<CommonClientResponse>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            wall.setFailMessage(CMMessageUtils.c.const_communicationError());
                        }

                        @Override
                        public void onSuccess(CommonClientResponse result) {
                            if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                wall.refresh();
                            } else {
                                wall.setFailMessage(wall.getApi().statusText(result.getStatus()));
                            }
                        }
                    });
                }
            };
            remover = new Remover(remove, removeAction);
            remover.addStyleName(StyleUtils.FLOAT_LEFT);
            remover.addStyleName(StyleUtils.MARGIN_LEFT_20);

            if (wall.getItemCount() > 0) {
                remover.addStyleName(StyleUtils.MARGIN_TOP_10);
            }

        }
        if (wall.getItemCount() > 0) {
            add(new Separator());
            author.addStyleName(StyleUtils.MARGIN_TOP_10);
            addStyleName(StyleUtils.MARGIN_TOP_10);
        } else {
            addStyleName(StyleUtils.MARGIN_TOP_20);
        }

        origTime = item.getTime();
        ago = StyleUtils.getStyledLabel(FormUtils.ago(origTime), StyleUtils.FONT_11PX, StyleUtils.CLEAR_BOTH);
        ago.setHeight("16px");

        Label text = StyleUtils.getStyledLabel(item.getText(), StyleUtils.MARGIN_VER_5);
        add(author);
        if (remover != null) {
            add(remover);
        }
        add(ago);
        add(text);
    }

    /** zvysi cas o jednu minutu */
    public void addMinute() {
        long now = System.currentTimeMillis();
        long diff = (now - origTime) / 1000; // rozdil v sekundach
        if (diff < 1209600) {
            ago.setText(FormUtils.ago(origTime));
        }
    }
}
