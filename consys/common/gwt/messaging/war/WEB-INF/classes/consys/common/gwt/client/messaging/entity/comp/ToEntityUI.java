package consys.common.gwt.client.messaging.entity.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;

/**
 * UI jedne polozky ToPanelu
 * @author pepa
 */
public class ToEntityUI extends FlowPanel {

    // data
    private String uuid;

    public ToEntityUI(ClientInvitedUser data) {
        this.uuid = data.getUuid();
        addStyleName(StyleUtils.MARGIN_LEFT_3);
        addStyleName(StyleUtils.MARGIN_VER_3);

        Image closeImage = new Image(ResourceUtils.system().removeCross());
        closeImage.addStyleName(StyleUtils.HAND);
        closeImage.addStyleName(StyleUtils.MARGIN_RIGHT_3);
        closeImage.addStyleName(StyleUtils.FLOAT_RIGHT);
        closeImage.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ToEntityUI.this.removeFromParent();
            }
        });

        add(closeImage);
        add(StyleUtils.getStyledLabel(data.getName(), StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_RIGHT_5));
        add(StyleUtils.clearDiv());
    }

    public String getUuid() {
        return uuid;
    }
}
