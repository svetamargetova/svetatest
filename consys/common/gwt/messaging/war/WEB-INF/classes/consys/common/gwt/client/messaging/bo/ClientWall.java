package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * Zpravy na zdi
 * @author pepa
 */
public class ClientWall extends JavaScriptObject {

    protected ClientWall() {
    }

    /** @return celkovy pocet polozek ve slozce (i nenactenchy) */
    public final long getTotalItems() {
        return (long) getDoubleTotalItems();
    }

    private final native double getDoubleTotalItems() /*-{
        return this.total;
    }-*/;

    /** @return seznam polozek ve slozce */
    public final native JsArray<ClientWallItem> getWallItems() /*-{
        return this.itms;
    }-*/;
}
