package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 *
 * @author pepa
 */
public class ClientSentMessagesItem extends JavaScriptObject {

    protected ClientSentMessagesItem() {
    }

    /** @return cas odeslani zpravy */
    public final long getTime() {
        return (long) getDoubleTime();
    }

    /** @return cas odeslani zpravy, js ho umi nacist jen jako double */
    private final native double getDoubleTime() /*-{
        return this.time;
    }-*/;

    /** @return nazev vlakna */
    public final native String getName() /*-{
        return this.name;
    }-*/;

    /** @return vlastni text zpravy */
    public final native String getMessage() /*-{
        return this.msg;
    }-*/;
}
