package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * Slozka zprav
 * @author pepa
 */
public class ClientFolder extends JavaScriptObject {

    protected ClientFolder() {
    }

    /** @return timestamp prvniho prvku pristi stranky */
    public final long getNextPageTimestamp() {
        return (long) getDoubleNextPageTimestamp();
    }

    private final native double getDoubleNextPageTimestamp() /*-{
        return this.next;
    }-*/;

    /** @return celkovy pocet polozek ve slozce (i nenactenych) */
    public final long getTotalItems() {
        return (long) getDoubleTotalItems();
    }

    private final native double getDoubleTotalItems() /*-{
        return this.total;
    }-*/;

    /** @return seznam polozek ve slozce */
    public final native JsArray<ClientFolderItem> getFolderItems() /*-{
        return this.itms;
    }-*/;
}
