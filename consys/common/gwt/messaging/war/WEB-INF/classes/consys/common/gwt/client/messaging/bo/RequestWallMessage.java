package consys.common.gwt.client.messaging.bo;

/**
 *
 * @author pepa
 */
public class RequestWallMessage extends RequestMessage {

    private String title;
    private String to;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
