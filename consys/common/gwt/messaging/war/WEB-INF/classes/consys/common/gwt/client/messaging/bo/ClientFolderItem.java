package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Jedna polozka ve slozce zprav
 * @author pepa
 */
public class ClientFolderItem extends JavaScriptObject {

    protected ClientFolderItem() {
    }

    /** @return uuid autora vlakna */
    public final native String getAuthor() /*-{
        return this.autor;
    }-*/;

    /** @return cas nejnovejsi zpravy vlakna */
    public final long getTime() {
        return (long) getDoubleTime();
    }

    /** @return cas odeslani zpravy, js ho umi nacist jen jako double */
    private final native double getDoubleTime() /*-{
        return this.time;
    }-*/;

    /** @return nazev vlakna */
    public final native String getName() /*-{
        return this.name;
    }-*/;

    /** @return uuid vlakna */
    public final native String getThread() /*-{
        return this.thrd;
    }-*/;

    /** @return uuid vlakna */
    public final native boolean isUpdated() /*-{
        return this.updt;
    }-*/;
}
