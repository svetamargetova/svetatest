package consys.common.gwt.client.messaging.entity;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.panel.MenuFormPanel;
import consys.common.gwt.client.ui.event.BreadcrumbContentEvent;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.layout.LayoutBreadcrumb;
import consys.common.gwt.client.ui.utils.FormUtils;

/**
 * Zalozkovy panel pro praci se zpravami
 * @author pepa
 */
public class MessageBox extends MenuFormPanel {

    public MessageBox() {
        super(CMMessageUtils.c.messageBox_title());

        ActionImage message = ActionImage.getMailButton(CMMessageUtils.c.messageBox_button_newMessage(), true);
        message.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BreadcrumbContentEvent be = new BreadcrumbContentEvent(CMMessageUtils.c.messageBox_title(),
                        BreadcrumbContentEvent.LEVEL.ROOT, new ChangeContentEvent(MessageBox.this), false);
                EventBus.get().fireEvent(be);

                MessageSendUI mui = new MessageSendUI();
                mui.setAfterSuccessAction(new ConsysAction() {

                    @Override
                    public void run() {
                        FormUtils.fireBreadcrumbBack();
                    }
                });
                mui.setCancelClickHandler(FormUtils.breadcrumbBackClickHandler());

                FormUtils.fireNextBreadcrumb(CMMessageUtils.c.messageBox_button_newMessage(), mui);
            }
        });
        addLeftControll(message);

        addItem(new MessageBoxUI(this));
        addItem(new MessageSentBoxUI());
        initWidget();
    }

    @Override
    protected void onLoad() {
        LayoutBreadcrumb.get().hide();
    }
}
