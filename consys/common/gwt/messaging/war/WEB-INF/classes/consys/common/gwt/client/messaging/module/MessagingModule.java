package consys.common.gwt.client.messaging.module;

import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleMenuRegistrator;

/**
 * Modul pro praci se zpravami
 * @author pepa
 */
public class MessagingModule implements Module {

    public static final String MESSAGING_API = "messaging_api";

    /** asynchronne dotahne modul api */
    public void getApi(AsyncModule<MessagingApi> async) {
        MessagingApi.getAsync(async);
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
    }

    @Override
    public void registerModule() {
        getApi(new AsyncModule<MessagingApi>() {

            @Override
            public void onSuccess(final MessagingApi instance) {
                instance.setMessagingUrl(URLFactory.messagingURL());
                Cache.get().register(MESSAGING_API, instance);
            }

            @Override
            public void onFail() {
                // chyba
                // TODO: co s tim budeme delat?
            }
        });
    }

    @Override
    public void unregisterModule() {
    }
}
