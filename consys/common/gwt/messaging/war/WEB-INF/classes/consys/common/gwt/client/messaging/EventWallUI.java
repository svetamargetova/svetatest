package consys.common.gwt.client.messaging;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.messaging.bo.ClientResponse;
import consys.common.gwt.client.messaging.bo.ClientWall;
import consys.common.gwt.client.messaging.bo.ClientWallItem;
import consys.common.gwt.client.messaging.module.MessagingApi;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientCurrentEvent;

/**
 * UI pro wall
 * @author pepa
 */
public class EventWallUI extends SimpleFormPanel implements Wall {

    // komponenty
    private FlowPanel content;
    private MessageInput messageInput;
    // data
    private MessagingApi api;
    private boolean canInsert;
    private boolean canRemove;
    private Timer actualizeTimer;
    private Timer refreshTimer;

    public EventWallUI(boolean canInsert, boolean canInsertAsEvent, boolean canRemove, String fail) {
        super(true);
        this.canInsert = canInsert;
        this.canRemove = canRemove;

        if (fail != null) {
            getFailMessage().setText("Error in wall permission loading: " + fail);
        }

        if (canInsert) {
            messageInput = new MessageInput(this, getHiddableLabel(), canInsertAsEvent);
            addWidget(messageInput);
        }

        content = new FlowPanel();
        content.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_loadingDots(), CLEAR_BOTH));
        addWidget(content);

        actualizeTimer = new Timer() {

            @Override
            public void run() {
                actualize();
            }
        };
        refreshTimer = new Timer() {

            @Override
            public void run() {
                actualizeTimer.cancel();
                refresh();
            }
        };
    }

    @Override
    protected void onLoad() {
        content.clear();
        content.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_loadingDots(), CLEAR_BOTH));
        Cache.get().doCacheAction(new CacheAction<MessagingApi>() {

            @Override
            public String getName() {
                return MessagingModule.MESSAGING_API;
            }

            @Override
            public void doAction(MessagingApi value) {
                api = value;
                refresh();
            }

            @Override
            public void doTimeoutAction() {
                // nemelo by nastat
            }

            @Override
            public boolean isCancel() {
                return false;
            }
        });
        actualizeTimer.scheduleRepeating(60000);
        refreshTimer.scheduleRepeating(300000);
    }

    @Override
    protected void onUnload() {
        actualizeTimer.cancel();
        refreshTimer.cancel();
    }

    @Override
    public MessagingApi getApi() {
        return api;
    }

    @Override
    public void setFailMessage(String fail) {
        getFailMessage().setText(fail);
    }

    @Override
    public int getItemCount() {
        return content.getWidgetCount();
    }

    /** provede aktualizaci casu jednotlivych zprav */
    private void actualize() {
        for (int i = 0; i < content.getWidgetCount(); i++) {
            Widget w = content.getWidget(i);
            if (w instanceof BaseWallItem) {
                BaseWallItem bwi = (BaseWallItem) w;
                bwi.addMinute();
            }
        }
    }

    @Override
    public void refresh() {
        Cache.get().doCacheAction(new CurrentEventCacheAction() {

            @Override
            public void doAction(final ClientCurrentEvent value) {
                String uuid = value.getEvent().getUuid();
                if (canInsert) {
                    messageInput.setEventUuid(uuid);
                }
                api.getApiWall().loadWall(uuid, 0, 0, new AsyncCallback<ClientResponse<ClientWall>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        getFailMessage().setText(CMMessageUtils.c.const_communicationError());
                    }

                    @Override
                    public void onSuccess(ClientResponse<ClientWall> result) {
                        content.clear();
                        if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                            ClientWall cw = result.getResponse();
                            JsArray<ClientWallItem> items = cw.getWallItems();
                            if (items.length() != 0) {
                                for (int i = 0; i < items.length(); i++) {
                                    ClientWallItem item = items.get(i);
                                    content.add(new BaseWallItem(EventWallUI.this, item, value, canRemove));
                                }
                            } else {
                                content.add(new Label(CMMessageUtils.c.eventWallUI_text_emptyWall()));
                            }
                        } else {
                            getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                        }
                        actualizeTimer.scheduleRepeating(60000);
                    }
                });
            }

            @Override
            public void doTimeoutAction() {
                // nemelo by nastat
            }
        });
    }
}
