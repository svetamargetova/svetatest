package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Obecna odpoved messaging serveru, neobsahuje data, jen stav
 * @author pepa
 */
public class CommonClientResponse extends JavaScriptObject {

    protected CommonClientResponse() {
    }

    /** vraci stav pozadavku (v poradku je status OK, vsechny ostatni jsou chybove) */
    public final native String getStatus() /*-{
        return this.s;
    }-*/;
}
