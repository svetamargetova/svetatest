package consys.common.gwt.client.messaging.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface CommonMessagingConstants extends Constants {

    String bugPanel_button_sendBug();

    String bugPanel_text_bugFunction();

    String bugPanel_text_bugGraphic();

    String bugPanel_text_bugOther();

    String bugPanel_text_reportBug();

    String bugPanel_text_sendFailed();

    String const_communicationError();

    String const_newUpper();

    String const_notLoaded();

    String const_to();

    String eventWallUI_text_emptyWall();

    String messageBox_button_newMessage();

    String messageBox_title();

    String messageBoxUI_action_show();

    String messageBoxUI_title();

    String messageInput_button_publishUpper();

    String messageInput_text_asLower();

    String messageInput_text_messageAdded();

    String messageSendUI_button_sendMessage();

    String messageSendUI_form_message();

    String messageSendUI_form_subject();

    String messageSentBoxUI_title();

    String messageThreadUI_button_reply();

    String messagingApi_text_banned();

    String messagingApi_text_entityNotFound();

    String messagingApi_text_messageNotFound();

    String messagingApi_text_newMessage();

    String messagingApi_text_noPermission();

    String messagingApi_text_requestFail();

    String messagingApi_text_serverClosed();

    String messagingApi_text_threadNotFound();

    String messagingApi_text_unknownStatus();

    String toPanel_action_selectContact();

    String userMessagingModule_text_messaging();
}
