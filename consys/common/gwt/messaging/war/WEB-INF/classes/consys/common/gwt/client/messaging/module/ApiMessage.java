package consys.common.gwt.client.messaging.module;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.messaging.bo.ClientFolder;
import consys.common.gwt.client.messaging.bo.ClientResponse;
import consys.common.gwt.client.messaging.bo.ClientSentMessages;
import consys.common.gwt.client.messaging.bo.ClientThread;
import consys.common.gwt.client.messaging.bo.CommonClientResponse;
import consys.common.gwt.client.messaging.bo.RequestNewMessage;
import consys.common.gwt.client.messaging.bo.RequestThreadMessage;
import consys.common.gwt.client.messaging.utils.RequestUtils;

/**
 * api pro zpravami entity
 * @author pepa
 */
public class ApiMessage {

    // data
    private MessagingApi api;
    private String url;

    ApiMessage(MessagingApi api) {
        this.api = api;
    }

    /** nastavi komunikacni url */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * nacte vlakno
     * @param uuid uuid vlakna
     * @param eUuid uuid uzivatele
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean loadThread(final String uuid, String eUuid, AsyncCallback<ClientResponse<ClientThread>> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_LOAD_THREAD));
        request.put(RequestUtils.REQUEST_THREAD, new JSONString(uuid));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(eUuid));
        return api.send(request, url, callback);
    }

    /**
     * nacte slozku prijmutych zprav
     * @param uuid uuid uzivatele
     * @param num kolik se ma nacist polozek (pro strankovani)
     * @param offset od jake polozky se ma nacitat (jeji timestamp)
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean loadFolder(final String uuid, final int num, final long offset, AsyncCallback<ClientResponse<ClientFolder>> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_LOAD_FOLDER));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(uuid));
        request.put(RequestUtils.REQUEST_NUMBER, new JSONNumber(num));
        request.put(RequestUtils.REQUEST_OFFSET, new JSONNumber(offset));
        return api.send(request, url, callback);
    }

    /**
     * nacte seznam odeslanych zprav
     * @param uuid uuid uzivatele
     * @param num kolik se ma nacist polozek (pro strankovani)
     * @param offset od jake polozky se ma nacitat (jeji timestamp)
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean loadSentMessages(final String uuid, final int num, final long offset, AsyncCallback<ClientResponse<ClientSentMessages>> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_LOAD_SENT_MESSAGES));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(uuid));
        request.put(RequestUtils.REQUEST_NUMBER, new JSONNumber(num));
        request.put(RequestUtils.REQUEST_OFFSET, new JSONNumber(offset));
        return api.send(request, url, callback);
    }

    /**
     * oznaci zpravu za prectenou
     * @param uuid uuid uzivatele
     * @param tUuid uuid vlakna
     * @param messageTimestamp casove razitko zpravy
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean markReadedMessage(final String uuid, final String tUuid, long messageTimestamp, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_MARK_MESSAGE));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(uuid));
        request.put(RequestUtils.REQUEST_THREAD, new JSONString(tUuid));
        request.put(RequestUtils.REQUEST_TIME, new JSONNumber(messageTimestamp));
        return api.send(request, url, callback);
    }

    /**
     * oznaci vlakno za prectene
     * @param uuid uuid uzivatele
     * @param tUuid uuid vlakna
     * @param lastMessageTimestamp casove razitko posledni (nejnovejsi) zpravy ve vlakne
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean markReadedThread(final String uuid, final String tUuid, long lastMessageTimestamp, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_MARK_THREAD));
        request.put(RequestUtils.REQUEST_ENTITY, new JSONString(uuid));
        request.put(RequestUtils.REQUEST_THREAD, new JSONString(tUuid));
        request.put(RequestUtils.REQUEST_TIME, new JSONNumber(lastMessageTimestamp));
        return api.send(request, url, callback);
    }

    /**
     * odesle nam bug
     * @param type typ bugu
     * @param msg zprava reportu
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean sendBug(final String userName, final String type, final String msg, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_BUG));
        request.put(RequestUtils.REQUEST_FROM, new JSONString(userName));
        request.put(RequestUtils.REQUEST_SUBJECT, new JSONString(type));
        request.put(RequestUtils.REQUEST_MESSAGE, new JSONString(msg));
        return api.send(request, url, callback);
    }

    /**
     * odesle novou zpravu
     * @param msg zprava k odeslani
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean sendNewMessage(final RequestNewMessage msg, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_SEND_NEW_MESSAGE));
        request.put(RequestUtils.REQUEST_FROM, new JSONString(msg.getFrom()));
        JSONArray toArray = new JSONArray();
        for (int i = 0; i < msg.getTo().size(); i++) {
            toArray.set(i, new JSONString(msg.getTo().get(i)));
        }
        request.put(RequestUtils.REQUEST_TO, toArray);
        request.put(RequestUtils.REQUEST_SUBJECT, new JSONString(msg.getSubject()));
        request.put(RequestUtils.REQUEST_MESSAGE, new JSONString(msg.getMessage()));
        return api.send(request, url, callback);
    }

    /**
     * odesle vlaknovou zpravu
     * @param msg zprava k odeslani
     * @param callback akce co se ma provest s odpovedi
     * @return true pokud se podarilo zpravu odeslat, ale jeste to nic nerika o vysledku zpracovani
     */
    public boolean sendThreadMessage(final RequestThreadMessage msg, AsyncCallback<CommonClientResponse> callback) {
        JSONObject request = new JSONObject();
        request.put(RequestUtils.REQUEST_TYPE, new JSONString(RequestUtils.TYPE_SEND_THREAD_MESSAGE));
        request.put(RequestUtils.REQUEST_FROM, new JSONString(msg.getFrom()));
        request.put(RequestUtils.REQUEST_MESSAGE, new JSONString(msg.getMessage()));
        request.put(RequestUtils.REQUEST_THREAD, new JSONString(msg.getThread()));
        return api.send(request, url, callback);
    }
}
