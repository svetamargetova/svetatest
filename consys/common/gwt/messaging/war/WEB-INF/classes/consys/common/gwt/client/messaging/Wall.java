package consys.common.gwt.client.messaging;

import consys.common.gwt.client.messaging.module.MessagingApi;

/**
 * Interface pro zed
 * @author pepa
 */
public interface Wall {

    /** vraci api pro praci s messagingem */
    public MessagingApi getApi();

    /** vypise chybu */
    public void setFailMessage(String fail);

    /** pocet polozek na zdi */
    public int getItemCount();

    /** aktualizace zdi */
    public void refresh();
}
