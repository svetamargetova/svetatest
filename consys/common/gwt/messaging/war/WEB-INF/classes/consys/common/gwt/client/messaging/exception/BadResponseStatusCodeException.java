package consys.common.gwt.client.messaging.exception;

/**
 * Vyjimka znacici, ze server vratil neocekavany status code
 * @author pepa
 */
public class BadResponseStatusCodeException extends Exception {

    private static final long serialVersionUID = 2981210254625218223L;

    public BadResponseStatusCodeException() {
        super();
    }

    public BadResponseStatusCodeException(String message) {
        super(message);
    }
}
