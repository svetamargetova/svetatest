package consys.common.gwt.client.messaging;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.messaging.bo.CommonClientResponse;
import consys.common.gwt.client.messaging.bo.RequestWallMessage;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.HiddableLabel;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBaseWrapper;
import consys.common.gwt.client.ui.utils.LabelColorEnum;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;

/**
 * Formular pro zaslani zpravy
 * @author pepa
 */
public class MessageInput extends FlowPanel {

    // konstanty
    private static final String INPUT_WIDTH = "500px";
    // komponenty
    private HiddableLabel hiddableLabel;
    private FlowPanel inputPanel;
    private TextBox fakeInput;
    private ConsysTextArea textBox;
    private FlowPanel control;
    private ActionImage button;
    private RadioButton userRb;
    private RadioButton eventRb;
    // data
    private Wall wall;
    private String eventUuid;
    private ClientUser cu;
    private ClientCurrentEvent cce;
    private boolean initialized;
    private boolean canInsertAsEvent;

    public MessageInput(Wall wall, HiddableLabel hiddableLabel, boolean canInsertAsEvent) {
        this.wall = wall;
        this.hiddableLabel = hiddableLabel;
        this.canInsertAsEvent = canInsertAsEvent;
        initialized = false;

        fakeInput = StyleUtils.getFormInput(INPUT_WIDTH);

        textBox = new ConsysTextArea(0, 2000, "");
        textBox.setWidth(INPUT_WIDTH);
        textBox.setVisible(false);

        inputPanel = new FlowPanel();
        inputPanel.addStyleName(StyleUtils.MARGIN_VER_3);
        inputPanel.addStyleName(StyleUtils.MARGIN_HOR_3);
        inputPanel.add(fakeInput);
        inputPanel.add(textBox);

        ConsysBaseWrapper wrapper = new ConsysBaseWrapper(inputPanel);
        wrapper.addStyleName(StyleUtils.FLOAT_LEFT);
        wrapper.addStyleName(StyleUtils.MARGIN_BOT_20);

        button = ActionImage.getPlusButton(CMMessageUtils.c.messageInput_button_publishUpper(), false);
        button.addStyleName(StyleUtils.MARGIN_LEFT_20);
        button.addStyleName(StyleUtils.MARGIN_TOP_10);

        control = new FlowPanel();
        control.setWidth("170px");
        control.addStyleName(StyleUtils.FLOAT_LEFT);
        control.setVisible(false);

        add(wrapper);
        add(control);
        add(StyleUtils.clearDiv());

        initHandlers();
    }

    private void initHandlers() {
        sinkEvents(Event.ONMOUSEOUT);
        fakeInput.addFocusHandler(new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                fakeInput.setVisible(false);
                textBox.setVisible(true);
                textBox.setFocus(true);
                control.setVisible(true);
            }
        });
        /*addHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                if (textBox.getText().trim().equals("")) {
                    textBox.setVisible(false);
                    control.setVisible(false);
                    fakeInput.setVisible(true);
                }
            }
        }, MouseOutEvent.getType());*/
    }

    @Override
    protected void onLoad() {
        if (!initialized) {
            try {
                cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
                if (canInsertAsEvent) {
                    cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
                }
                button.addClickHandler(sendClickHandler());
                control.add(button);

                control.add(StyleUtils.clearDiv("10px"));

                userRb = new RadioButton("sender");
                userRb.setValue(true);
                userRb.addStyleName(StyleUtils.MARGIN_LEFT_20);
                userRb.addStyleName(StyleUtils.FLOAT_LEFT);
                userRb.addStyleName(StyleUtils.MARGIN_RIGHT_5);
                control.add(userRb);
                Label userLabel = StyleUtils.getStyledLabel(CMMessageUtils.c.messageInput_text_asLower() + " " + cu.name(), StyleUtils.FLOAT_LEFT);
                userLabel.setWidth("120px");
                control.add(userLabel);

                if (cce != null) {
                    control.add(StyleUtils.clearDiv("10px"));

                    eventRb = new RadioButton("sender");
                    eventRb.addStyleName(StyleUtils.MARGIN_LEFT_20);
                    eventRb.addStyleName(StyleUtils.FLOAT_LEFT);
                    eventRb.addStyleName(StyleUtils.MARGIN_RIGHT_5);
                    control.add(eventRb);
                    Label label = StyleUtils.getStyledLabel(CMMessageUtils.c.messageInput_text_asLower() + " " + cce.getEvent().getAcronym(), StyleUtils.FLOAT_LEFT);
                    label.setWidth("120px");
                    control.add(label);
                }
            } catch (NotInCacheException ex) {
            }
            initialized = true;
        }
    }

    /** nastavi uuid eventu, na ktery se zpravy zasilaji */
    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    /** ClickHandler pro odeslani zpravy na zed */
    private ClickHandler sendClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (textBox.getText().trim().equals("")) {
                    return;
                }
                RequestWallMessage msg = new RequestWallMessage();
                msg.setFrom(userRb.getValue() ? cu.getUuid() : cce.getEvent().getUuid());
                msg.setTo(eventUuid);
                msg.setMessage(textBox.getText().trim());

                wall.getApi().getApiWall().sendWallMessage(msg, new AsyncCallback<CommonClientResponse>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        wall.setFailMessage(CMMessageUtils.c.const_communicationError());
                    }

                    @Override
                    public void onSuccess(CommonClientResponse result) {
                        if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                            textBox.setText("");
                            textBox.setVisible(false);
                            control.setVisible(false);
                            fakeInput.setVisible(true);

                            if (hiddableLabel != null) {
                                hiddableLabel.show(CMMessageUtils.c.messageInput_text_messageAdded(), LabelColorEnum.SUCCESS_COLOR);
                            }
                            wall.refresh();
                        } else {
                            wall.setFailMessage(wall.getApi().statusText(result.getStatus()));
                        }
                    }
                });
            }
        };
    }
}
