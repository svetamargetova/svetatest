package consys.common.gwt.client.messaging.entity.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ClickFlowPanel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import java.util.ArrayList;

/**
 * Komponenta pro vyber prijemcu zprav
 * @author pepa
 */
public class ToPanel extends FlowPanel implements ValidableComponent {

    // komponenty
    private ContentPanel content;

    public ToPanel() {
        setWidth("400px");

        ActionLabel selectContact = new ActionLabel(CMMessageUtils.c.toPanel_action_selectContact(), StyleUtils.FLOAT_RIGHT);
        selectContact.setClickHandler(selectContactHandler());
        add(selectContact);

        content = new ContentPanel();
        content.setSize("268px", "19px");
        content.setStyleName(StyleUtils.BORDER);

        add(content);
        add(StyleUtils.clearDiv());
    }

    /** vraci seznam prijemcu zpravy, pokud neni vybran zadny uzivatel, vraci se prazdny seznam */
    public ArrayList<String> getTo() {
        ArrayList<String> tos = new ArrayList<String>();
        for (int i = 0; i < content.getWidgetCount(); i++) {
            ToEntityUI ui = (ToEntityUI) content.getWidget(i);
            tos.add(ui.getUuid());
        }
        return tos;
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        boolean result = true;
        if (content.getWidgetCount() == 0) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(CMMessageUtils.c.const_to()));
            result = false;
        }
        return result;
    }

    @Override
    public void onSuccess() {
        content.setStyleName(StyleUtils.BORDER);
    }

    @Override
    public void onFail() {
        content.setStyleName(StyleUtils.BORDER);
        content.addStyleName(StyleUtils.BACKGROUND_RED);
    }

    /** vymaze zadane uzivatele */
    public void reset() {
        content.clear();
    }

    /** clickHandler pro zobrazeni pridavaciho handleru */
    private ClickHandler selectContactHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog sd = new UserSearchDialog(false);
                sd.setDelegate(new SearchDelegate<ClientInvitedUser>() {

                    @Override
                    public void didSelect(ClientInvitedUser item) {
                        boolean contains = false;
                        String uuid = item.getUuid();
                        for (int i = 0; i < content.getWidgetCount(); i++) {
                            if (uuid.equals(((ToEntityUI) content.getWidget(i)).getUuid())) {
                                contains = true;
                                break;
                            }
                        }
                        if (!contains) {
                            // pridame jen kdyz tam jeste neni
                            content.add(new ToEntityUI(item));
                        }
                    }
                });
                sd.showCentered();
            }
        };
    }

    /** pomocna trida pro chovani ToPanel */
    private class ContentPanel extends ClickFlowPanel {

        HandlerRegistration registration;

        @Override
        public boolean remove(Widget w) {
            boolean result = super.remove(w);
            int count = getWidgetCount();
            if (count <= 1) {
                setHeight("19px");
            }
            if (count == 0 && registration == null) {
                registration = addClickHandler(selectContactHandler());
                addStyleName(StyleUtils.HAND);
            }
            return result;
        }

        @Override
        public void add(Widget w) {
            super.add(w);
            int count = getWidgetCount();
            if (count > 1) {
                setHeight("auto");
            }
            if (count > 0 && registration != null) {
                registration.removeHandler();
                registration = null;
                removeStyleName(StyleUtils.HAND);
            }
        }
    }
}
