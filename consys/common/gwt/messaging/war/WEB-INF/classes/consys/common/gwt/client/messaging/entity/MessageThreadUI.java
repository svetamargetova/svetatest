package consys.common.gwt.client.messaging.entity;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.messaging.bo.ClientResponse;
import consys.common.gwt.client.messaging.bo.ClientThread;
import consys.common.gwt.client.messaging.bo.ClientThreadMessage;
import consys.common.gwt.client.messaging.bo.CommonClientResponse;
import consys.common.gwt.client.messaging.module.MessagingApi;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.NotInCacheException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Zobrazeni vlakna
 * @author pepa
 */
public class MessageThreadUI extends RootPanel {

    // data
    private String threadUuid;
    private MessagingApi mapi;
    private ClientUser cu;
    private long lastMessageTimestamp;
    private ReadedThread readedThread;
    private boolean newMessages;
    private ArrayList<Timer> timerList;

    public MessageThreadUI(String threadUuid, boolean newMessages) {
        super(UIMessageUtils.c.const_loadingDots());
        this.threadUuid = threadUuid;
        this.newMessages = newMessages;
        timerList = new ArrayList<Timer>();
        readedThread = new ReadedThread();
    }

    @Override
    protected void onLoad() {
        reload();
    }

    @Override
    protected void onUnload() {
        readedThread.cancel();
    }

    /** znovu nacte obsah */
    private void reload() {
        clear();
        try {
            cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
            mapi = (MessagingApi) Cache.get().getSafe(MessagingModule.MESSAGING_API);
            mapi.getApiMessage().loadThread(threadUuid, cu.getUuid(),
                    new AsyncCallback<ClientResponse<ClientThread>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            getFailMessage().setText(caught.getMessage());
                        }

                        @Override
                        public void onSuccess(ClientResponse<ClientThread> result) {
                            if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                generateThread(result.getResponse());
                            } else {
                                getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                            }
                        }
                    });
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(MessagingModule.MESSAGING_API));
        }
    }

    /** vygeneruje obsah z objektu odpovedi */
    private void generateThread(ClientThread ct) {
        setTitleText(ct.getTitle());
        final HashMap<String, ClientUserThumb> users = new HashMap<String, ClientUserThumb>();
        final JsArray<ClientThreadMessage> messages = ct.getMessages();

        final ArrayList<String> uuids = new ArrayList<String>();
        for (int i = 0; i < messages.length(); i++) {
            String uuid = messages.get(i).getFromUuid();
            if (!uuids.contains(uuid)) {
                uuids.add(uuid);
            }
        }

        DispatchEvent de = new DispatchEvent(new ListUserThumbsAction(uuids),
                new AsyncCallback<ArrayListResult<ClientUserThumb>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                        if (caught instanceof NoRecordsForAction) {
                            showMessages(messages, users);
                        }
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientUserThumb> result) {
                        for (ClientUserThumb cut : result.getArrayListResult()) {
                            users.put(cut.getUuid(), cut);
                        }
                        showMessages(messages, users);
                    }
                },
                this);
        EventBus.get().fireEvent(de);
    }

    /** vykresli zpravy a moznost odpovedi */
    private void showMessages(JsArray<ClientThreadMessage> messages, final HashMap<String, ClientUserThumb> users) {
        boolean unreaded = false;
        lastMessageTimestamp = messages.get(messages.length() - 1).getTime();

        for (int i = 0; i < messages.length(); i++) {
            final ClientThreadMessage message = messages.get(i);
            ClientUserThumb user = users.get(message.getFromUuid());
            final Label updatedLabel = StyleUtils.getStyledLabel(!message.isReaded()
                    ? CMMessageUtils.c.const_newUpper() : "", FONT_10PX, FLOAT_LEFT, TEXT_RED);

            if (!message.isReaded()) {
                unreaded = true;
                readedThread.addUnreadedMessage(message.getTime());
                Timer t = new Timer() {

                    @Override
                    public void run() {
                        mapi.getApiMessage().markReadedMessage(cu.getUuid(), threadUuid, message.getTime(),
                                new AsyncCallback<CommonClientResponse>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        getFailMessage().setText(caught.getMessage());
                                    }

                                    @Override
                                    public void onSuccess(CommonClientResponse result) {
                                        if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                            // ok
                                            readedThread.readed(message.getTime());
                                            updatedLabel.setVisible(false);
                                        } else {
                                            getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                                        }
                                    }
                                });
                    }
                };
                timerList.add(t);
            }

            if (i != 0) {
                addWidget(Separator.addedStyle(MARGIN_VER_10));
            }

            Image portrait = FormUtils.userPortrait(user != null ? user.getUuidPortraitImagePrefix() : null, UserProfileImageEnum.LIST);
            if (user == null) {
                user = new ClientUserThumb();
                user.setName(CMMessageUtils.c.const_notLoaded());
            }
            
            FlowPanel portraitWrapper = FormUtils.portraitWrapper(UserProfileImageEnum.LIST);
            portraitWrapper.add(portrait);

            FlowPanel timePanel = new FlowPanel();
            timePanel.add(StyleUtils.getStyledLabel(DateTimeUtils.getMonthDayYear(new Date(message.getTime())),
                    FONT_10PX, FLOAT_LEFT, MARGIN_RIGHT_10));
            timePanel.add(updatedLabel);
            timePanel.add(StyleUtils.clearDiv());

            FlowPanel dataPanel = new FlowPanel();
            dataPanel.setWidth("610px");
            dataPanel.addStyleName(FLOAT_LEFT);
            dataPanel.add(timePanel);
            dataPanel.add(StyleUtils.getStyledLabel(user.getName(), MARGIN_TOP_5, FONT_BOLD, FONT_11PX));
            dataPanel.add(StyleUtils.getStyledLabel(message.getMessage(), MARGIN_TOP_5, FONT_14PX));

            FlowPanel content = new FlowPanel();
            content.add(portraitWrapper);
            content.add(dataPanel);
            content.add(StyleUtils.clearDiv());
            addWidget(content);
        }

        if (unreaded) {
            readedThread.start();
        } else if (newMessages) {
            readedThread.markThreadIsReaded();
        }

        addWidget(Separator.addedStyle(MARGIN_VER_10));

        final ActionImage replyToThread = ActionImage.getMailButton(CMMessageUtils.c.messageThreadUI_button_reply(), false);
        replyToThread.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                replyToThread.setVisible(false);
                final MessageSendUI mui = new MessageSendUI(threadUuid);
                mui.setAfterSuccessAction(new ConsysAction() {

                    @Override
                    public void run() {
                        reload();
                    }
                });
                mui.setCancelClickHandler(new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        mui.removeFromParent();
                        replyToThread.setVisible(true);
                    }
                });
                addWidget(mui);
            }
        });
        addWidget(replyToThread);
    }

    /** spusteni akci odznaceni vlakna a zprav za neprectene */
    private class ReadedThread {

        // data
        private Timer timer;
        private ArrayList<Long> list;

        public ReadedThread() {
            list = new ArrayList<Long>();
            timer = new Timer() {

                @Override
                public void run() {
                    if (!timerList.isEmpty()) {
                        int index = timerList.size() - 1;
                        Timer t = timerList.get(index);
                        timerList.remove(index);
                        t.schedule(1000);
                    }
                    if (!list.isEmpty()) {
                        return;
                    }
                    cancel();
                    markThreadIsReaded();
                }
            };
        }

        /** pridani nove neprectene zpravy */
        public void addUnreadedMessage(Long messageTime) {
            list.add(messageTime);
        }

        /** oznaceni zpravy za practenou */
        public void readed(Long messageTime) {
            list.remove(messageTime);
        }

        /** spusti pravidelne kontrolovani, ze jsou vsechny zpravy oznacene za prectene */
        public void start() {
            timer.scheduleRepeating(1000);
        }

        /** zrusi timer */
        public void cancel() {
            timer.cancel();
        }

        /** zkusi oznacit vlakno za prectene */
        public void markThreadIsReaded() {
            if (mapi != null) {
                mapi.getApiMessage().markReadedThread(cu.getUuid(), threadUuid, lastMessageTimestamp,
                        new AsyncCallback<CommonClientResponse>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                getFailMessage().setText(caught.getMessage());
                            }

                            @Override
                            public void onSuccess(CommonClientResponse result) {
                                if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                    // ok
                                } else if (result.getStatus().equals(ResponseUtils.STATE_NEW_MESSAGE)) {
                                    // nova zprava ve vlaknu
                                    getSuccessMessage().setText(MessagingApi.statusText(result.getStatus()));
                                } else {
                                    getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                                }
                            }
                        });
            }
        }
    }
}
