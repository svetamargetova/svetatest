package consys.common.gwt.client.messaging.bo;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;

/**
 * Jedno vlakno
 * @author pepa
 */
public class ClientThread extends JavaScriptObject {

    protected ClientThread() {
    }

    /** vraci nazev vlakna */
    public final native String getTitle() /*-{
        return this.name;
    }-*/;

    /** vraci seznam zprav ve vlaknu */
    public final native JsArray<ClientThreadMessage> getMessages() /*-{
        return this.msgs;
    }-*/;
}
