package consys.common.gwt.client.messaging.bo;

/**
 *
 * @author pepa
 */
public class RequestThreadMessage extends RequestMessage {

    private String thread;

    public String getThread() {
        return thread;
    }

    public void setThread(String thread) {
        this.thread = thread;
    }
}
