package consys.common.gwt.client.messaging.entity;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.messaging.bo.CommonClientResponse;
import consys.common.gwt.client.messaging.bo.RequestNewMessage;
import consys.common.gwt.client.messaging.bo.RequestThreadMessage;
import consys.common.gwt.client.messaging.entity.comp.ToPanel;
import consys.common.gwt.client.messaging.module.MessagingApi;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.messaging.utils.CMMessageUtils;
import consys.common.gwt.client.messaging.utils.ResponseUtils;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;

/**
 * Odeslani zpravy jine entite
 * @author pepa
 */
public class MessageSendUI extends SimpleFormPanel {

    // komponenty
    private BaseForm form;
    private ToPanel toBox;
    private ConsysStringTextBox subjectBox;
    private ConsysTextArea messageBox;
    private ActionLabel cancel;
    // data
    private String threadUuid;
    private ConsysAction afterSuccesssAction;

    public MessageSendUI() {
        this(null);
    }

    public MessageSendUI(String threadUuid) {
        this.threadUuid = threadUuid;
        form = new BaseForm();
        cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        addWidget(form);
    }

    @Override
    protected void onLoad() {
        form.reset();

        try {
            final String uuid = ((ClientUser) Cache.get().getSafe(UserCacheAction.USER)).getUuid();
            final MessagingApi api = (MessagingApi) Cache.get().getSafe(MessagingModule.MESSAGING_API);

            toBox = new ToPanel();
            subjectBox = new ConsysStringTextBox(1, 255, CMMessageUtils.c.messageSendUI_form_subject());
            messageBox = new ConsysTextArea(1, 0, "400px", "140px", false, CMMessageUtils.c.messageSendUI_form_message());

            ActionImage button = ActionImage.getMailButton(CMMessageUtils.c.messageSendUI_button_sendMessage(), false);
            button.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    if (!form.validate(getFailMessage())) {
                        return;
                    }
                    if (threadUuid == null || threadUuid.isEmpty()) {
                        RequestNewMessage msg = new RequestNewMessage();
                        msg.setFrom(uuid);
                        msg.setTo(toBox.getTo());
                        msg.setSubject(subjectBox.getText().trim());
                        msg.setMessage(messageBox.getText().trim());
                        showWaiting(true);
                        api.getApiMessage().sendNewMessage(msg, new AsyncCallback<CommonClientResponse>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                showWaiting(false);
                                getFailMessage().setText(CMMessageUtils.c.const_communicationError());
                            }

                            @Override
                            public void onSuccess(CommonClientResponse result) {
                                showWaiting(false);
                                if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                    resetForm();
                                    if (afterSuccesssAction != null) {
                                        afterSuccesssAction.run();
                                    }
                                } else {
                                    getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                                }
                            }
                        });
                    } else {
                        RequestThreadMessage msg = new RequestThreadMessage();
                        msg.setFrom(uuid);
                        msg.setThread(threadUuid);
                        msg.setMessage(messageBox.getText().trim());
                        showWaiting(true);
                        api.getApiMessage().sendThreadMessage(msg, new AsyncCallback<CommonClientResponse>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                showWaiting(false);
                                getFailMessage().setText(CMMessageUtils.c.const_communicationError());
                            }

                            @Override
                            public void onSuccess(CommonClientResponse result) {
                                showWaiting(false);
                                if (result.getStatus().equals(ResponseUtils.STATE_OK)) {
                                    resetForm();
                                    if (afterSuccesssAction != null) {
                                        afterSuccesssAction.run();
                                    }
                                } else {
                                    getFailMessage().setText(MessagingApi.statusText(result.getStatus()));
                                }
                            }
                        });
                    }
                }
            });

            if (threadUuid == null) {
                form.addRequired(0, CMMessageUtils.c.const_to(), toBox);
                form.addRequired(1, CMMessageUtils.c.messageSendUI_form_subject(), subjectBox);
            }
            form.addRequired(2, CMMessageUtils.c.messageSendUI_form_message(), messageBox);
            form.addActionMembers(3, button, cancel, "400px");
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(UserCacheAction.USER + ", " + MessagingModule.MESSAGING_API));
        }
    }

    /** vymaze zadana data z formulare */
    private void resetForm() {
        toBox.reset();
        subjectBox.setText("");
        messageBox.setText("");
    }

    /** akce spustena po uspesnem odeslani zpravy */
    public void setAfterSuccessAction(ConsysAction action) {
        this.afterSuccesssAction = action;
    }

    /** nastavi click handler na cancel */
    public void setCancelClickHandler(ClickHandler handler) {
        cancel.setClickHandler(handler);
    }
}
