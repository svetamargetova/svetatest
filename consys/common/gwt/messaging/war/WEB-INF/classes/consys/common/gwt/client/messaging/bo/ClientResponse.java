package consys.common.gwt.client.messaging.bo;

/**
 * Odpoved messaging serveru, krome stavu obsahuje i data
 * @author pepa
 */
public class ClientResponse<T> extends CommonClientResponse {

    protected ClientResponse() {
    }

    /** vraci vlastni data odpovedi, typ zalezi na generice */
    public final native T getResponse() /*-{
        return this.r;
    }-*/;
}
