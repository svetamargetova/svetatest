package consys.common.json;

import java.io.IOException;
import java.util.Calendar;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TestActionMapper {

    private static final Logger logger = LoggerFactory.getLogger(TestActionMapper.class);

    @Test(groups = {"mapping"})
    public void testActionMapper() throws JsonParseException, JsonMappingException, IOException {
        String requestEmptyParams = "{ "
                + "\"login\" : \"Kuba\", "
                + "\"uuid\" : \"uuiduiduid\", "
                + "\"password\" : \"skaljkasl\","
                + "\"aid\" : \"198\" ,"
                + "\"action\": \"events\", "
                + "\"param\":{ " +
                "\"param1\":\"value1\"," +
                "\"param2\":22," +
                "\"date1\":\"20100421\" }"
                + "}";


        ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
        Action action = mapper.readValue(requestEmptyParams, Action.class);
        assertEquals(action.getActionId(), 198);
        assertEquals(action.getAction(), "events");
        assertEquals(action.getUserUuid(), "uuiduiduid");
        assertEquals(action.getParams().size(), 3);
        assertEquals(action.getParams().get("param1"), "value1");
        assertEquals(action.getIntParam("param2"), new Integer(22));
        assertEquals(action.getCalendarParam("date1").get(Calendar.MONTH), Calendar.APRIL);
        assertEquals(action.getCalendarParam("date1").get(Calendar.YEAR), 2010);
        assertEquals(action.getCalendarParam("date1").get(Calendar.DAY_OF_MONTH), 21);
    }
}
