package consys.common.json;

import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author pepa
 */
public class TestJsonApiConnector {

    private static final String ADMIN_URL = "http://tp.eu/takeplace/api/json";
    private static final String OVERSEER_URL = "http://tp.eu/overseer/api/json";

    private static String requestUser() {
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        //sb.append(param("event", "", true));
        sb.append(param("action", "user", true));
        sb.append(param("aid", "33", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", "{" + param("uuid", "39b33dbbe75947b9871306f7112d6d4f", false) + "}", false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestLogin() {
        StringBuilder sb1 = new StringBuilder("{");
        sb1.append(param("username", "hubr@acemcee.com", true));
        sb1.append(param("password", "12ABCabc", false));
        sb1.append("}");

        StringBuilder sb = new StringBuilder("{");
        sb.append(param("action", "login", true));
        sb.append(param("aid", "23", true));
        sb.append(paramQuoted("param", sb1.toString(), false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestEvents() {
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        //sb.append(param("event", "", true));
        sb.append(param("action", "events", true));
        sb.append(param("aid", "33", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", "{}", false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestUserDetail() {
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        sb.append(param("event", "98a68d6038334ddc84185ada19af7cd8", true));
        sb.append(param("action", "user-detail", true));
        sb.append(param("aid", "1", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", "{}", false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestRegistrationOptions() {
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        sb.append(param("event", "98a68d6038334ddc84185ada19af7cd8", true));
        sb.append(param("action", "registration-options", true));
        sb.append(param("aid", "1", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", "{}", false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestParticipants() {
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        sb.append(param("event", "98a68d6038334ddc84185ada19af7cd8", true));
        sb.append(param("action", "participants", true));
        sb.append(param("aid", "1", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", "{}", false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestParticipantsManage() {
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        sb.append(param("event", "e60dda59a6b147acbe13be3f08fc6392", true));
        sb.append(param("action", "participants-manage", true));
        sb.append(param("aid", "1", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", "{}", false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestCheckin() {
        StringBuilder sb1 = new StringBuilder("{");
        sb1.append(paramQuoted("action", "0", true, false));
        sb1.append(param("registration-uuid", "a2ad95d4d9294631b9e9c58e3b0b766c", true));
        sb1.append(paramQuoted("count", "-2", false, false));
        sb1.append("}");

        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        sb.append(param("event", "e60dda59a6b147acbe13be3f08fc6392", true));
        sb.append(param("action", "checkin", true));
        sb.append(param("aid", "12", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", sb1.toString(), false, false));
        sb.append("}");
        return sb.toString();
    }

    private static String requestSubmissionOptions() {
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("uuid", "060b6fd8374148d380d474e71425ad6b", true));
        sb.append(param("event", "98a68d6038334ddc84185ada19af7cd8", true));
        sb.append(param("action", "submission-options", true));
        sb.append(param("aid", "1", true));
        sb.append(param("session", "ee5cb6eb1a7c4339b68b7b724b05d703", true));
        sb.append(paramQuoted("param", "{}", false, false));
        sb.append("}");
        return sb.toString();
    }
    
    private static String requestUserState() {
        StringBuilder sb1 = new StringBuilder("{");
        sb1.append(param("email", "hubr@acemcee.com", true));
        sb1.append(param("apikey", "f138b03dfeb8d9d4a0b9994a4250db6a120101e2", false));
        sb1.append("}");
        
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("action", "user-register-state", true));
        sb.append(param("aid", "23", true));
        sb.append(paramQuoted("param", sb1.toString(), false, false));
        sb.append("}");
        return sb.toString();
    }
    
    private static String requestUserPasswordReset() {
        StringBuilder sb1 = new StringBuilder("{");
        sb1.append(param("email", "hubr@acemcee.com", true));
        sb1.append(param("apikey", "f138b03dfeb8d9d4a0b9994a4250db6a120101e2", false));
        sb1.append("}");
        
        StringBuilder sb = new StringBuilder("{");
        sb.append(param("action", "user-password-reset", true));
        sb.append(param("aid", "1", true));
        sb.append(paramQuoted("param", sb1.toString(), false, false));
        sb.append("}");
        return sb.toString();
    }

    public static void main(String[] args) throws Exception {
        //sendRequest(requestLogin(), ADMIN_URL);
        //sendRequest(requestUser(), ADMIN_URL);
        //sendRequest(requestEvents(), ADMIN_URL);
        //sendRequest(requestUserDetail(), OVERSEER_URL);
        //sendRequest(requestRegistrationOptions(), OVERSEER_URL);
        //sendRequest(requestParticipants(), OVERSEER_URL);
        //sendRequest(requestParticipantsManage(), OVERSEER_URL);
        sendRequest(requestCheckin(), OVERSEER_URL);
        //sendRequest(requestSubmissionOptions(), OVERSEER_URL);
        //sendRequest(requestUserState(), ADMIN_URL);
        //sendRequest(requestUserPasswordReset(), ADMIN_URL);
    }

    private static void sendRequest(String requestData, String urlValue) throws Exception {
        URL url = new URL(urlValue);

        URLConnection c = url.openConnection();
        c.setDoInput(true);
        c.setDoOutput(true);
        c.setUseCaches(false);

        System.out.println("Request  -> " + requestData);

        // posleme data
        OutputStreamWriter osw = new OutputStreamWriter(c.getOutputStream());
        //osw.write(URLEncoder.encode(requestData, "UTF-8"));
        osw.write(requestData);
        osw.flush();

        // precteme data
        StringWriter writer = new StringWriter();
        IOUtils.copy(c.getInputStream(), writer, "UTF-8");
        System.out.println("Response -> " + writer.toString());

        osw.close();
        c.getInputStream().close();
    }

    private static String param(String name, String value, boolean appendComma) {
        return paramQuoted(name, value, appendComma, true);
    }

    private static String paramQuoted(String name, String value, boolean appendComma, boolean quoted) {
        StringBuilder sb = new StringBuilder("\"");
        sb.append(name);
        sb.append("\":");
        if (quoted) {
            sb.append("\"");
        }
        sb.append(value);
        if (quoted) {
            sb.append("\"");
        }
        if (appendComma) {
            sb.append(",");
        }
        return sb.toString();
    }
}
