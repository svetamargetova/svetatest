package consys.common.json;



import java.io.IOException;
import org.springframework.stereotype.Component;


@Component
public interface ActionHandler {
    
     /**
     * @return The type of {@link Action} supported by this handler.
     */
    String getActionName();
        
    void execute( Action action, Result result) throws IOException;
    
}
