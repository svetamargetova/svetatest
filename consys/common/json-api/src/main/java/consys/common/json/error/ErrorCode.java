package consys.common.json.error;

import com.google.common.base.Joiner;
import consys.common.core.exception.ConsysException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.json.Result;
import java.io.IOException;
import org.apache.commons.lang.ClassUtils;



/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ErrorCode {
    private static final String EMPTY = "-";
    public static void error(Result result, ConsysException e, String ...args) throws IOException{
        String joinedArgs = null;
        if(args != null && args.length > 0){
            joinedArgs =  Joiner.on(",").join(args);
        }else{
            joinedArgs = EMPTY;
        }
        if(e instanceof RequiredPropertyNullException){
            result.writeError(1, String.format("Missing parameters: %s",joinedArgs));
        } else if(e instanceof NoRecordException){
            result.writeError(2, String.format("No record(s) for given action parameters: %s",joinedArgs));
        } else if(e instanceof ServiceExecutionFailed){
            result.writeError(3, String.format("Service execution failed due to internal error: %s",joinedArgs));
        } else {
            result.writeError(99, String.format("Unresloved exception: "+ClassUtils.getShortClassName(e.getClass())));
        }

    }




}
