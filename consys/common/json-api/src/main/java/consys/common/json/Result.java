package consys.common.json;

import consys.common.utils.date.DateProvider;
import java.io.IOException;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.codehaus.jackson.JsonGenerator;

/**
 * Result vyuziva builder pattern a ktoreho ucelom je vytvorit vysledny objekt
 * JSON akcie. 
 * <p>
 * Obsah odpovede pozozstava z hlavicky ktora sa automaticky vytvori pouzitim 
 * niektorej <code>write*</code> metody. 
 *
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Result {
    private static final String BODY = "body";
    private static final String NOTHING = "";

    private JsonGenerator generator;
    private int actionId;
    // priznak ci je telo otvrorene, resp. napisana hlavicka
    private boolean bodyOpened;

    public Result(JsonGenerator generator, int actionId) {
        this.generator = generator;
        this.actionId = actionId;
        bodyOpened = false;
    }
    
    void writeHeader() throws IOException {
        generator.writeNumberField("aid", actionId);
        generator.writeNumberField("time", DateProvider.getCurrentDate().getTime());
    }

    public void writeError(int errcode) throws IOException {
        writeHeader();
        generator.writeNumberField("error", errcode);
    }

    public void writeError(int errcode, String msg) throws IOException {
        writeError(errcode);
        generator.writeStringField("errmsg", msg);
    }

    void writeSuccessHeader() throws IOException {
        writeError(0);
    }

    void writeBodyOpen() throws IOException{
        getGenerator().writeObjectFieldStart(BODY);
    }

    void writeBodyEnd() throws IOException{
        if(bodyOpened)
            getGenerator().writeEndObject();
    }

    private void bodyCheck() throws IOException{
        if(!bodyOpened){
            // write header
            writeSuccessHeader();
            writeBodyOpen();
            bodyOpened = true;
        }
    }

    public void writeString(String value) throws IOException{
        bodyCheck();
        if(StringUtils.equals(null, value)){
            getGenerator().writeString(NOTHING);
        }else{
            getGenerator().writeString(value);
        }
    }

    public void writeStringField(String field, String value) throws IOException{
        bodyCheck();
        if(StringUtils.equals(null, value)){
            getGenerator().writeStringField(field, NOTHING);
        }else{
            getGenerator().writeStringField(field, value);
        }
    }

    public void writeBooleanField(String field, boolean value) throws IOException{
        bodyCheck();
        getGenerator().writeStringField(field, value ? "1" : "0");
    }

    public void writeDateField(String field, Date value) throws IOException{
        bodyCheck();
        getGenerator().writeStringField(field, value == null ? "" : formatDate(value));
    }

    public void writeDateTimeField(String field, Date value) throws IOException{
        bodyCheck();
        getGenerator().writeStringField(field, value == null ? "" : formatDateTime(value));
    }

    public void writeNumberField(String field, int value) throws IOException{
        bodyCheck();
        getGenerator().writeNumberField(field, value);
    }

    public void writeNumberField(String field, Long value) throws IOException{
        bodyCheck();
        getGenerator().writeNumberField(field, value);
    }

    public void writeArrayFieldStart(String field) throws IOException{
        bodyCheck();
        getGenerator().writeArrayFieldStart(field);
    }
    public void writeEndArray() throws IOException{
        bodyCheck();
        getGenerator().writeEndArray();
    }

    public void writeObjectFieldStart(String field) throws IOException{
        bodyCheck();
        getGenerator().writeObjectFieldStart(field);
    }

    public void writeStartObject() throws IOException{
        bodyCheck();
        getGenerator().writeStartObject();
    }
    
    public void writeEndObject() throws IOException{
        bodyCheck();
        getGenerator().writeEndObject();
    }

    /**
     * @return the generator
     */
    JsonGenerator getGenerator() {
        return generator;
    }

    /**
     * Thread-Safe fast date format
     */
    private static final FastDateFormat dateFormat = FastDateFormat.getInstance("yyyyMMdd");

    /**
     * Formatuje datum s casom do formatu: yyyyMMdd
     * @param date datum ktory sa ma sformatovat
     * @return null ak je date=null alebo zformatovany datum
     */
    public static String formatDate(Date date) {
        if (date == null) {
            return NOTHING;
        }
        return dateFormat.format(date);
    }

    /**
     * Thread-Safe fast date format
     */
    private static final FastDateFormat dateTimeFormat = FastDateFormat.getInstance("yyyyMMddHHmmss");

    /**
     * Formatuje datum s casom do formatu: yyyyMMddHHmmss
     * @param date datum ktory sa ma sformatovat
     * @return null ak je date=null alebo zformatovany datum
     */
    public static String formatDateTime(Date date) {
        if (date == null) {
            return NOTHING;
        }
        return dateTimeFormat.format(date);
    }
}
