package consys.common.json;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractActionHandlerRegistry implements ApplicationListener, ApplicationContextAware {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final JsonFactory FACTORY = new JsonFactory();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper(FACTORY);
    private ApplicationContext applicationContext;
    private Map<String, ActionHandler> handlers;

    public AbstractActionHandlerRegistry() {
        logger.debug("Initialized...");
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextRefreshedEvent && handlers == null) {
            logger.info("Initializing JSON action handlers ...");
            if (applicationContext != null) {
                Map<String, ActionHandler> actionHandlers = applicationContext.getBeansOfType(ActionHandler.class);
                Builder<String, ActionHandler> builder = ImmutableMap.builder();
                for (Map.Entry<String, ActionHandler> e : actionHandlers.entrySet()) {
                    try {
                        String action = e.getValue().getActionName();
                        logger.info(" ->  " + action);
                        builder.put(action, e.getValue());
                    } catch (UnsupportedOperationException exception) {
                        logger.info("   [Unsupported]" + e.getValue().getClass().getSimpleName());
                        continue;
                    }
                }
                handlers = Collections.synchronizedMap(builder.build());
            } else {
                throw new RuntimeException("Application Context is NULL!!!");
            }
            logger.info("Initializing JSON action handlers finished.");
        }
    }

    /**
     * Akcia ktora moze byt aplikovana pred zavolanim handleru.
     * @param action
     * @param result
     */
    public void preAction(Action action, Result result) {
        // no-op
    }

    private boolean validateAction(Action action, Result result) throws IOException {

        if (StringUtils.isBlank(action.getAction())) {
            result.writeError(99, "Missing action name: \"action\":\"ACTION_NAME\"");
            return false;
        }
        return true;

    }

    public void handleAction(InputStream inputStream, OutputStream outputStream) {

        try {
            // vyparsujeme akciu
            Action action = OBJECT_MAPPER.readValue(inputStream, Action.class);
            if (StringUtils.isBlank(action.getAction())) {
                throw new NullPointerException("Missing action name");
            }
            
            logger.debug("Json Dispatch: {}",action.getAction());
            
            // nacitame handler
            ActionHandler handler = handlers.get(action.getAction());
            if (handler == null) {
                throw new NullPointerException("Action is not supported!");
            }
            // vytorime z neho result
            Result result = new Result(FACTORY.createJsonGenerator(outputStream), action.getActionId());
            result.getGenerator().writeStartObject();
            // volame
            preAction(action, result);
            // validujeme
            if (validateAction(action, result)) {
                handler.execute(action, result);
            }
            result.writeBodyEnd();
            // vytvorime ukoncovaciu znacku JSON odpovede
            result.getGenerator().writeEndObject();

            // zatvorime stream - rucne           
            result.getGenerator().close();
        } catch (IOException e) {
            logger.error("Can't send response to client.", e);
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
