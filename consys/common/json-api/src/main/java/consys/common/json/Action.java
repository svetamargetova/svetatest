package consys.common.json;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Action {

    @JsonProperty("uuid")
    private String userUuid;
    @JsonProperty("event")
    private String eventUuid;
    @JsonProperty("action")
    private String action;
    @JsonProperty("aid")
    private int actionId;
    @JsonProperty("param")
    private Map<String, Object> params;

    /**
     * @return the userUuid
     */
    public String getUserUuid() {
        return userUuid;
    }

    /**
     * @param userUuid the userUuid to set
     */
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the actionId
     */
    public int getActionId() {
        return actionId;
    }

    /**
     * @param actionId the actionId to set
     */
    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    /**
     * @return the params
     */
    public Map<String, Object> getParams() {
        return params;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
    }

    /*------------------------------------------------------------------------*/
    /*--  Params converters  --*/
    /*------------------------------------------------------------------------*/
    public String getStringParam(String name) {
        return (String) params.get(name);
    }

    public Boolean getBooleanParam(String name) {
        Integer value = getIntParam(name);
        if (value == null || value == 0) {
            return false;
        }else{
            return true;
        }
    }

    public Integer getIntParam(String name) {
        return (Integer) params.get(name);
    }

    public Date getDateParam(String name) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            return format.parse(getStringParam(name));
        } catch (ParseException ex) {
            throw new IllegalArgumentException();
        }
    }

    public Calendar getCalendarParam(String name) {
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(getDateParam(name));
        return c;
    }

    /**
     * @return the eventUuid
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * @param eventUuid the eventUuid to set
     */
    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }
}
