package consys.common.core.dao;

import consys.common.core.bo.Image;
import consys.common.core.exception.NoRecordException;



/**
 *
 * @author Palci
 */
public interface ImageDao extends GenericDao<Image> {

    /** Smaze obrazek podle uid 
     * @return pocet smazanych obrazku
     */
    public int deleteByUuid(String uuid);

    public int deleteByUuidPrefix(String uuid);

    /** Nacte obrazek podle uid 
     * @return nacteny obrazek
     */
    public Image loadByUid(String uuid) throws NoRecordException;
}
