package consys.common.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.service.CountryService;
import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.UsStateEnum;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Palo
 */
public class CountryServiceImpl extends AbstractService implements CountryService {

    @Override
    public List<CountryEnum> listCountries() {
        CountryEnum[] countries = CountryEnum.values();
        return Arrays.asList(countries);
    }

    @Override
    public List<UsStateEnum> listUsStates() {
        UsStateEnum[] states = UsStateEnum.values();
        return Arrays.asList(states);
    }

    @Override
    public UsStateEnum loadUsStateById(Integer id) throws NoRecordException {
        return UsStateEnum.fromId(id);
    }

    @Override
    public CountryEnum loadCountryById(Integer id) throws NoRecordException {
        return CountryEnum.fromId(id);
    }
}
