package consys.common.core.mail;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;

/**
 * email.setTLS(true); pro Gmail
 * @deprecated  Mame uz MesagingService. tot olen v pripade ze by to nefungovalo
 *
 * @author Palo
 */
@Deprecated
public class Mailer {

    private String host;
    private String user;
    private String pass;
    private boolean ssl = false;
    private boolean debug = false;


    public Mailer(String host, String user, String pass,boolean ssl, boolean debug) {
        this(host, user, pass,ssl);
        this.debug = debug;
    }

    public Mailer(String host, String user, String pass,boolean ssl) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.ssl = ssl;
    }



    public void send(Email e) throws EmailException{
        e.setSSL(ssl);
        e.setHostName(host);
        e.setAuthentication(user, pass);
        e.setDebug(debug);
        e.send();
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }


}
