/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Trieda ktora staticky poskytuje NotX sluzbu
 * @author palo
 */
public final class NotX{

    private static final Logger logger = LoggerFactory.getLogger(NotX.class);
    
    private static NotxService notificationService;

    public static NotxService getService(){
        if(notificationService == null){
            throw new NullPointerException("Missing NotificationService! Initialize first!");
        }
        return notificationService;
    }

    public static void setNotificationService(NotxService notificationService) {        
        logger.info("Initializing NotX service!");
        NotX.notificationService = notificationService;
    }

    public NotxService getNotificationService() {
        return notificationService;
    }
    
    
}
