package consys.common.core.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public abstract class AbstractService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * @return the logger
     */
    public Logger log() {
        return logger;
    }
}
