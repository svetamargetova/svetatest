package consys.common.core.exception;

import java.util.Date;

/**
 *
 * @author Palo
 */
public class CouponOutOfDateException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    private Date endDate;
    
    public CouponOutOfDateException(Date endDate) {
        super();
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }            
}
