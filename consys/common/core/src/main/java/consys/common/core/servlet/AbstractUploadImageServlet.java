package consys.common.core.servlet;

import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.aws.AwsFileStorageService.Cache;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.util.ImageHelper;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.MissingResourceException;
import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author pepa
 */
public abstract class AbstractUploadImageServlet extends HttpServlet implements InitializingBean {

    private static final long serialVersionUID = -5841469688102660718L;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static final String[] imageTypes = new String[]{"gif", "jpeg", "png"};
    @Autowired
    private AwsFileStorageService awsFileStorageService;
    private String bucketName;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
        if (loadBucketFromInitParam()) {
            bucketName = config.getInitParameter("Image.S3.Bucket");
            if (StringUtils.isBlank(bucketName)) {
                throw new IllegalArgumentException("Missing bucket AWS S3 image bucket name");
            }            
        }else{
            bucketName = "Load bucket name from params is not requested";
        }
        logger.info("{} initialized to: {}", this.getClass().getName(), bucketName);
    }

    protected boolean loadBucketFromInitParam() {
        return true;
    }

    /** Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response) {
        ServletFileUpload upload = new ServletFileUpload();
        try {
            FileItemIterator iter = upload.getItemIterator(request);
            while (iter.hasNext()) {
                FileItemStream item = iter.next();

                saveImage(item, request, response);

                break; // pouze jeden soubor
            }
        } catch (Exception e) {
            logger.error("Upload File error", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void saveImage(FileItemStream item, HttpServletRequest request, HttpServletResponse response) throws IOException, ServiceExecutionFailed {
        // IE speciality - contentType
        String contentType = item.getContentType().replace("pjpeg", "jpeg");
        contentType = contentType.replace("x-png", "png");

        String subContentType = contentType.substring(6);
        boolean validImageType = false;
        for (String s : imageTypes) {
            if (s.equals(subContentType)) {
                validImageType = true;
            }
        }

        if (!validImageType) {
            logger.info("Upload Image failed: Unsupported type:" + subContentType);
            response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            return;
        }

        String name = item.getName();
        InputStream stream = item.openStream();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BufferedImage buffImage = ImageIO.read(stream);

        if (!ImageIO.write(buffImage, subContentType, out)) {
            logger.info("Upload Image failed: ImageIO write failed");
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
            return;
        }

        // maximalni velikost obrazku v bytech(1 MB)
        int maxFileSize = 5000 * 1024;
        if (out.size() > maxFileSize) { // TODO: out neodpovida skutecnosti jako by to bylo pouze pri nacteni z inputstreamu
            logger.info("Upload Image failed: TooBig " + maxFileSize + " < " + out.size());
            response.sendError(HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE);
            return;
        }
        try {
            String resp = saveImage(request, name, buffImage, contentType, subContentType);
            if (resp != null) {
                response.getOutputStream().print(resp);
            }
        } catch (MissingResourceException exception) {
            logger.info("Upload Image failed: Missing " + exception.getKey());
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch (NoRecordException ex) {
            logger.info("Upload Image failed: UNAUTHORIZED ");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    protected void createImage(BufferedImage buffImage, String name, String contentType, String subContentType, String uuid) throws IOException, ServiceExecutionFailed {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (!ImageIO.write(buffImage, subContentType, bout)) {
            throw new IOException();
        }
        ByteArrayInputStream stream = new ByteArrayInputStream(bout.toByteArray());
        awsFileStorageService.createObject(bucketName, uuid, name, contentType, Cache.CACHE.getCache(), false, stream, bout.size());
    }

    protected void createImage(int width, int height, BufferedImage buffImage, String name, String contentType, String subContentType, String uuid) throws IOException, ServiceExecutionFailed {
        // zmena na velikost
        if (buffImage.getWidth() > width || buffImage.getHeight() > height) {
            Dimension dim = ImageHelper.getScaledSize(buffImage.getWidth(), buffImage.getHeight(), width, height);
            buffImage = ImageHelper.getScaledInstance(buffImage, dim.width, dim.height, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
        }
        createImage(buffImage, name, contentType, subContentType, uuid);
    }

    protected void deleteImage(String uuid) {
        awsFileStorageService.deleteObject(bucketName, uuid);
    }

    protected abstract String saveImage(HttpServletRequest request, String name, BufferedImage bi, String contentType, String subContentType) throws NoRecordException, ServiceExecutionFailed;

    @Override
    public void afterPropertiesSet() throws Exception {
    }
}
