package consys.common.core.bo.enums;

/**
 * Enum pro uchovani informace o lokalizovane polozce
 * @author pepa
 */
public enum SystemLocale {

    /** vychozi jazyk */
    DEFAULT(1),
    /** anglictina */
    ENGLISH(2),
    /** cestina */
    CZECH(3);
    // data
    private Integer id;

    private SystemLocale(Integer id) {
        this.id = id;
    }

    public static SystemLocale fromId(Integer i) {
        switch (i) {
            case 1:
                return DEFAULT;
            case 2:
                return ENGLISH;
            case 3:
                return CZECH;
        }
        throw new RuntimeException();
    }

    public Integer getId() {
        return id;
    }
}
