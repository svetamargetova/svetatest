package consys.common.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class CouponCodeException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public CouponCodeException() {
        super();
    }

     public CouponCodeException(String message) {
	super(message);
    }

}
