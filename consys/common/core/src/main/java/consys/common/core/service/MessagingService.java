package consys.common.core.service;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Deprecated 
public interface MessagingService {

    /** enum odpovedi messaging serveru pro servery */
    public enum MessagingStateEnum {

        OK, FAIL, CLOSED, BAN, ENF, DENIED, COMMUNICATION_FAIL, UNKNOWN_STATE;
        private static final long serialVersionUID = -1480370517801586472L;
    }

    /** zaregistruje entitu na messaging serveru */
    public MessagingStateEnum registerEntity(String uuid, String email);

    /** odesle systemovy mail na zadanou adresu a se zadanou zpravou */
    public MessagingStateEnum sendMail(String from, String to, String subject, String msg, boolean isHTML);
}
