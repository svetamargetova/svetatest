package consys.common.core.util;




import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.common.core.bo.Image;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Blob;
import javax.imageio.ImageIO;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palci
 */
public class ImageHelper {

    private static final Logger logger = LoggerFactory.getLogger(ImageHelper.class);

    public static synchronized Image createImage(File f) throws IllegalArgumentException {
        try {
            Image image = loadImage(ImageIO.read(f));
            image.setFileName(f.getName());
            image.setUuid(UuidProvider.getUuid());
            return image;
        } catch (Exception ex) {
            logger.error("Cant create Image from file " + f.getName());
            throw new IllegalArgumentException("Can't create Image from file", ex);
        }
    }

    public static synchronized Image createImage(String url) throws IllegalArgumentException {
        try {
            URL uRL = new URL(url);
            Image image = loadImage(ImageIO.read(uRL));
            image.setFileName(uRL.getFile());
            image.setUuid(UuidProvider.getUuid());
            return image;
        } catch (Exception ex) {
            logger.error("Cant create Image from url " + url);
            throw new IllegalArgumentException("Can't create Image from URL ", ex);
        }
    }

    private static synchronized Image loadImage(BufferedImage bufferedImage) {
        try {
            Image image = new Image();
            image.setWidth(bufferedImage.getWidth());
            image.setHeight(bufferedImage.getHeight());
            image.setDateInserted(DateProvider.getCurrentDate());

            ByteArrayOutputStream bas = new ByteArrayOutputStream();
            if (!ImageIO.write(bufferedImage, "png", bas)) {
                throw new IllegalArgumentException("Can't create Image! Writing to output stream failed! ");
            }
            Blob blob = Hibernate.createBlob(bas.toByteArray());
            image.setData(blob);
            image.setUuid(UuidProvider.getUuid());
            return image;
        } catch (IOException ex) {
            logger.error("Cant create Image");
            throw new IllegalArgumentException("Can't create Image", ex);
        }
    }

    /**
     * Vraci sirku a vysku obrazku, na kterou je nutne obrazek resizovat, aby se vesel do pozadovanych rozmeru
     * pri zachovani pomeru stran.
     * @param width sirka obrazku
     * @param height vyska obrazku
     * @param maxWidth maximalni pozadovana sirka
     * @param maxHeight maximalni pozadovana vyska
     * @return pole int, prvni prvek je sirka, druhy vyska
     */
    public static Dimension getScaledSize(int width, int height, int maxWidth, int maxHeight) {

        if (width < 1 || height < 1) {
            throw new IllegalArgumentException("Width and height must be greater than zero.");
        }
        if (maxWidth < 1 || maxHeight < 1) {
            throw new IllegalArgumentException("maxWidth and maxHeight must be greater than zero.");
        }

        double scaleFactor;

        if ((width <= maxWidth) && (height <= maxHeight)) {
            scaleFactor = 1;
        } else if ((width > maxWidth) && (height <= maxHeight)) {
            scaleFactor = (double) maxWidth / width;
        } else if ((width <= maxWidth) && (height > maxHeight)) {
            scaleFactor = (double) maxHeight / height;
        } else if ((width > maxWidth) && (height > maxHeight)) {
            double heightFactor = (double) maxHeight / height;
            double widthFactor = (double) maxWidth / width;
            if (heightFactor < widthFactor) {
                scaleFactor = heightFactor;
            } else {
                scaleFactor = widthFactor;
            }
        } else {
            throw new RuntimeException("Unreachable code reached.");
        }

        int scaledWidth = (int) Math.round(width * scaleFactor);
        int scaledHeight = (int) Math.round(height * scaleFactor);

        // výsledná šířka nebo výška vždy alespoň 1px
        scaledWidth = scaledWidth == 0 ? 1 : scaledWidth;
        scaledHeight = scaledHeight == 0 ? 1 : scaledHeight;

        return new Dimension(scaledWidth, scaledHeight);
    }

    /**
     * Convenience method that returns a scaled instance of the
     * provided {@code BufferedImage}.
     * <br />
     * http://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
     *
     * @param img the original image to be scaled
     * @param targetWidth the desired width of the scaled instance,
     *    in pixels
     * @param targetHeight the desired height of the scaled instance,
     *    in pixels
     * @param hint one of the rendering hints that corresponds to
     *    {@code RenderingHints.KEY_INTERPOLATION} (e.g.
     *    {@code RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR},
     *    {@code RenderingHints.VALUE_INTERPOLATION_BICUBIC})
     * @param higherQuality if true, this method will use a multi-step
     *    scaling technique that provides higher quality than the usual
     *    one-step technique - only useful in downscaling cases, where
     *    {@code targetWidth} or {@code targetHeight} is
     *    smaller than the original dimensions, and generally only when
     *    the {@code BILINEAR} hint is specified)
     * @return a scaled version of the original {@code BufferedImage}
     */
    public static BufferedImage getScaledInstance(BufferedImage img, int targetWidth, int targetHeight, Object hint, boolean higherQuality) {
        if (img == null) {
            throw new IllegalArgumentException("Source img can't be null");
        }
        if (targetWidth <= 0) {
            throw new IllegalArgumentException("Target width is <= 0: [" + targetWidth + "]");
        }
        if (targetHeight <= 0) {
            throw new IllegalArgumentException("Target height is <= 0: [" + targetHeight + "]");
        }

        int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = img;
        int w, h;
        if (higherQuality) {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = img.getWidth();
            if (w < targetWidth) {
                w = targetWidth;
            }
            h = img.getHeight();
            if (h < targetHeight) {
                h = targetHeight;
            }
        } else {
            // Use one-step technique: scale directly from original
            // size to target size with a single drawImage() call
            w = targetWidth;
            h = targetHeight;
        }

        do {
            if (higherQuality && w > targetWidth) {
                w /= 2;
                if (w < targetWidth) {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight) {
                h /= 2;
                if (h < targetHeight) {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.drawImage(ret, 0, 0, w, h, null);
            g2.dispose();

            ret = tmp;
        } while (w != targetWidth || h != targetHeight);

        return ret;
    }
}
