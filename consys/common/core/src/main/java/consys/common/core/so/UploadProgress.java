package consys.common.core.so;

import java.io.Serializable;

/**
 *
 * @author pepa
 */
public class UploadProgress implements Serializable {

    private static final long serialVersionUID = -5299328641183599865L;
    // data
    private long transferedBytes;
    private long totalBytes;
    private boolean cancel;

    public long getTransferedBytes() {
        return transferedBytes;
    }

    public void setTransferedBytes(long transferedBytes) {
        this.transferedBytes = transferedBytes;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }

    public double getUploadPercent() {
        return (transferedBytes / (double) totalBytes) * 100;
    }

    public boolean isCancel() {
        return cancel;
    }

    public void cancel() {
        cancel = true;
    }

    @Override
    public String toString() {
        return transferedBytes + "/" + totalBytes;
    }
}
