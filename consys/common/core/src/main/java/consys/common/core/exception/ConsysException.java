package consys.common.core.exception;

/**
 *
 * @author Palo
 */
public abstract class ConsysException extends Exception{
    private static final long serialVersionUID = 3711711333139830696L;
    
    public ConsysException() {
	super();
    }


    public ConsysException(String message) {
	super(message);
    }


    public ConsysException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConsysException(Throwable cause) {
        super(cause);
    }

}
