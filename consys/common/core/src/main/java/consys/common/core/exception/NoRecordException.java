package consys.common.core.exception;

/**
 *
 * @author Palo
 */
public class NoRecordException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public NoRecordException() {
        super();
    }

     public NoRecordException(String message) {
	super(message);
    }

}
