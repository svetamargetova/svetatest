/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.common.core.service;

import com.google.common.collect.ImmutableList;
import consys.common.core.notx.MessageType;
import consys.common.core.notx.SystemMessage;
import java.util.List;
import net.notx.client.PlaceHolders;

/**
 *
 * @author palo
 */
public interface NotxService {

    public void sytemNotification(SystemMessage msg, String target, PlaceHolders placeholder);

    public void notify(MessageType type, String templateName, String target, PlaceHolders placeholder);
    
    public void tag(TaggingBuilder builder);        
    
    public void unTag(TaggingBuilder builder); 
    
    public TaggingBuilder createTaggingBuilder();
        
    
    public interface TaggingBuilder{
                
        public TaggingBuilder addUsers(List<String> users);
        
        public TaggingBuilder addTags(List<String> tags);
        
        public TaggingBuilder addUser(String userUuid);
        
        public TaggingBuilder addTag(String ...tag);
    }   
    
        
    
}
