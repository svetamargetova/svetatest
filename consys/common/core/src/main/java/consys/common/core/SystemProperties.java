package consys.common.core;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SystemProperties {

    public static final String MESSAGING_SERVER_URL = "messaging_server_url";
}
