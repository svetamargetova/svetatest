package consys.common.core.exception;

/**
 *
 * @author Palo
 */
public class RequiredPropertyNullException extends ConsysException{
    private static final long serialVersionUID = -4011634190869306592L;

    public RequiredPropertyNullException() {
    }
    

    public RequiredPropertyNullException(String message) {
	super(message);
    }


    public RequiredPropertyNullException(String message, Throwable cause) {
        super(message, cause);
    }

}
