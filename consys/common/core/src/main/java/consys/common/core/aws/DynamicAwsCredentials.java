/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.core.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import java.io.File;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

/**
 * Amazon credentials ktore su dynamicky ziskavane na zaklade niekolkych vlasnosti
 * @author palo
 */
public class DynamicAwsCredentials implements AWSCredentials, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(DynamicAwsCredentials.class);
    
    private static final String ENV_ACCESS_KEY = "aws_access";
    private static final String ENV_SECRET_KEY = "aws_secret";
    private String propertiesPath;
    private String awsAccessKey;
    private String awsSecretKey;

    @Override
    public String getAWSAccessKeyId() {        
        return awsAccessKey;
    }

    @Override
    public String getAWSSecretKey() {        
        return awsSecretKey;
    }

    public void setAWSAccessKeyId(String awsAccessKey) {
        this.awsAccessKey = awsAccessKey;
    }
   
    public void setAWSSecretKey(String awsSecretKey) {
        this.awsSecretKey = awsSecretKey;
    }

    
    public void setPropertiesPath(String propertiesPath) {
        this.propertiesPath = propertiesPath;
    }

    public String getPropertiesPath() {
        return propertiesPath;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (StringUtils.isBlank(awsSecretKey)
                || awsSecretKey.charAt(0) == '$'            
                || StringUtils.isBlank(awsAccessKey)
                || awsAccessKey.charAt(0) == '$' ) {

            awsAccessKey = System.getenv(ENV_ACCESS_KEY);
            awsSecretKey = System.getenv(ENV_SECRET_KEY);
            if (awsAccessKey == null) {
                awsAccessKey = System.getenv(ENV_ACCESS_KEY.toUpperCase());
            }

            if (awsSecretKey == null) {
                awsSecretKey = System.getenv(ENV_SECRET_KEY.toUpperCase());
            }

            if (StringUtils.isBlank(awsSecretKey) || StringUtils.isBlank(awsAccessKey)) {

                // inicializujeme - priorita 2: properties file
                if (!StringUtils.isBlank(propertiesPath) && propertiesPath.indexOf('{') == -1) {
                    try {
                        PropertiesCredentials p = new PropertiesCredentials(new File(propertiesPath));
                        awsAccessKey = p.getAWSAccessKeyId();
                        awsSecretKey = p.getAWSSecretKey();
                    } catch (Exception ex) {
                        throw new IllegalArgumentException("Path to AWS properties is invalid!", ex);
                    }

                }
                if (StringUtils.isBlank(awsSecretKey) || StringUtils.isBlank(awsAccessKey)) {
                    throw new IllegalArgumentException("Missing AWS Credentials");
                }
            }  
                                    
        }
        logger.info("AWS credentials configured Access: {}  Secret:{}",awsAccessKey,awsSecretKey);
    }
}
