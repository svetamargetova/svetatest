package consys.common.core.so;

/**
 * Stav jak dopadlo nahravani souboru
 * @author pepa
 */
public enum UploadStatus {

    UPLOADED, CANCELED, ERROR;
}
