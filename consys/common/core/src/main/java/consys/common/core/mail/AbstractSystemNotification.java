package consys.common.core.mail;

import consys.common.core.notx.SystemMessage;
import consys.common.core.service.NotX;
import net.notx.client.PlaceHolders;
import org.apache.commons.lang.StringUtils;

/**
 * AbstractMailBuilder je abstraktna trieda ktora sluzi ako zaklad pre vsetky
 * emaile ktore su posielane zo systemu.
 *
 * @since 2.7.10 Zaklad pre notifikaciu. Zaobaluje vytvorenie placeholderov
 *
 * @author Palo
 */
public abstract class AbstractSystemNotification {

    private final SystemMessage systemMessage;
    private String target;

    /**
     * Notifikacia konkretnemu uzivatelovi,
     *
     * @param systemMessage
     * @param target
     */
    public AbstractSystemNotification(SystemMessage systemMessage, String target) {
        this(systemMessage, target, true);
    }

    /**
     * Na zaklade priznaku <param>userTarget</param> vytvori bud otagovanu
     * notifikaciu alebo konkretnemu uzivatelovi.
     *
     * @param systemMessage
     * @param target
     * @param userTarget
     */
    public AbstractSystemNotification(SystemMessage systemMessage, String target, boolean userTarget) {
        this.systemMessage = systemMessage;
        setTarget(target, userTarget);
    }

    protected abstract void fillPlaceholders(PlaceHolders placeHolders);

    public void sendNotification() {
        if (StringUtils.isBlank(target)) {
            throw new NullPointerException("Missing target");
        }
        PlaceHolders pl = new PlaceHolders();
        fillPlaceholders(pl);
        NotX.getService().sytemNotification(systemMessage, getTarget(), pl);
    }

    public final void setTarget(String target, boolean userTarget) {
        if (target != null) {
            this.target = userTarget ? String.format(":%s", target) : target;
        }
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }
}
