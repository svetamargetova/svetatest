package consys.common.core.service;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Servica si drzi v pamati vsetky systemove properties a na vyziadanie ich
 * znova nacita. Treba respektovat defaultne hodnoty danych properties
 * @author Palo
 */
public interface SystemPropertyService {

    public String getString(String key);

    public Integer getInteger(String key);

    public Boolean getBoolean(String key);

    public Date getDate(String key);

    public void createStringProperty(String key, String value);

    public void createIntegerProperty(String key, Integer value);

    public void createBooleanProperty(String key, Boolean value);

    public void createDateProperty(String key, Date date);

    /**
     * Aktualizacia property s testom na to ci je dana hodnota povolena.
     * 
     * @param key
     * @param newValue
     * @throws IllegalPropertyValue
     * @throws NoRecordException
     */
    public void updateProperty(String key, String newValue) throws IllegalPropertyValue, NoRecordException;

    public void updateStringProperty(String key, String value) throws NoRecordException;

    public void updateIntegerProperty(String key, Integer value) throws NoRecordException;

    public void updateBooleanProperty(String key, Boolean value) throws NoRecordException;

    public void updateDateProperty(String key, Date date) throws NoRecordException;
    
    public void updateProperties(Map<String,Object> properties) throws IllegalPropertyValue,NoRecordException;

    public SystemProperty loadSystemProperty(String key) throws NoRecordException;

    public List<SystemProperty> listProperiesToClient();

    public List<SystemProperty> listAllProperties();
    
    public List<SystemProperty> listProperties(List<String> keys);
}
