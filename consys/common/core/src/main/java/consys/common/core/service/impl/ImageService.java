package consys.common.core.service.impl;

import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.util.ImageHelper;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palci
 */
public class ImageService extends AbstractService{

    @Autowired
    private AwsFileStorageService awsFileStorageService;

    protected void createImage(BufferedImage buffImage, String fileName, String contentType, String subContentType, String uuid, String bucketName) throws IOException, ServiceExecutionFailed {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (!ImageIO.write(buffImage, subContentType, bout)) {
            throw new IOException();
        }
        log().debug("Create image: {} -> {}",fileName,bucketName);
        ByteArrayInputStream stream = new ByteArrayInputStream(bout.toByteArray());
        awsFileStorageService.createObject(bucketName, uuid, fileName, contentType, AwsFileStorageService.Cache.CACHE.getCache(), false, stream, bout.size());
    }

    protected void createImage(int width, int height, BufferedImage buffImage, String name, String contentType, String subContentType, String uuid, String bucketName) throws IOException, ServiceExecutionFailed {
        // zmena na velikost
        if (buffImage.getWidth() > width || buffImage.getHeight() > height) {
            Dimension dim = ImageHelper.getScaledSize(buffImage.getWidth(), buffImage.getHeight(), width, height);
            buffImage = ImageHelper.getScaledInstance(buffImage, dim.width, dim.height, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
        }
        createImage(buffImage, name, contentType, subContentType, uuid, bucketName);
    }

    protected void deleteImage(String uuid, String bucketName) {
        log().debug("Delete image: {} -> {}",uuid,bucketName);
        awsFileStorageService.deleteObject(bucketName, uuid);
    }
}
