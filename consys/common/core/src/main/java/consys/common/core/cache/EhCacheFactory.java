/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.core.cache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.config.CacheConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 *
 * @author palo
 */
public class EhCacheFactory implements DisposableBean, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(EhCacheFactory.class);
    private CacheManager singletonManager;

    public boolean exists(String name) {
        return singletonManager.cacheExists(name);
    }

    public Ehcache create(String name,CacheConfiguration configuration) {
        Cache memoryOnlyCache = new Cache(configuration);        
        logger.info("Creating cache: {}",name);
        
        return memoryOnlyCache;
    }
    
    public Ehcache getCache(String name){
        return singletonManager.getEhcache(name);
    }

    public void initializeCache(Ehcache cache) {
        logger.info("Initalizing cache: {}",cache.getName());
        singletonManager.addCache(cache);
    }

    @Override
    public void destroy() throws Exception {
        singletonManager.shutdown();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        singletonManager = CacheManager.create();
    }
}
