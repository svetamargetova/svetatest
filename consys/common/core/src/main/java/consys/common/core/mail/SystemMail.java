package consys.common.core.mail;

/**
 * Jednoduchy systemovy email ktory bude poslani na konkretnu adresu
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SystemMail {

    private static final String DEFAULT_FROM = "noreply@takeplace.eu";

    private String from;
    private String to;
    private String title;
    private String body;
    private boolean html = true;

    public SystemMail() {
        from = DEFAULT_FROM;
    }



    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return the html
     */
    public boolean isHtml() {
        return html;
    }

    /**
     * @param html the html to set
     */
    public void setHtml(boolean html) {
        this.html = html;
    }
    




}
