/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.core.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.google.common.collect.ImmutableList;
import consys.common.core.notx.MessageType;
import consys.common.core.notx.SystemMessage;
import consys.common.core.service.NotxService;
import java.util.List;
import net.notx.aws.sqs.SqsNotxFacade;
import net.notx.client.PlaceHolders;
import net.notx.ws.tagging.TagRequest;
import net.notx.ws.tagging.TaggingWebService;
import net.notx.ws.tagging.UnTagRequest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class NotxServiceImpl implements NotxService {

    protected SqsNotxFacade notxFacade;
    @Autowired
    protected TaggingWebService taggingWebService;

    public NotxServiceImpl(AWSCredentials credentials, String endpoint, String queueName) {
        notxFacade = new SqsNotxFacade(credentials, endpoint, queueName);
    }

    @Override
    public void sytemNotification(SystemMessage msg, String target, PlaceHolders placeholder) {
        notxFacade.sendNotification(target, MessageType.SYSTEM.name(), msg.name(), placeholder);
    }

    @Override
    public void notify(MessageType type, String templateName, String target, PlaceHolders placeholder) {
        notxFacade.sendNotification(target, type.name(), templateName, placeholder);
    }

    @Override
    public void tag(TaggingBuilder builder) {
        TagRequest r = TaggingWebService.OBJECT_FACTORY.createTagRequest();
        r.getUserIds().addAll(((TaggingBuilderImpl)builder).getUsers());
        r.getTags().addAll(((TaggingBuilderImpl)builder).getTags());
        taggingWebService.createTags(r);
    }

    @Override
    public void unTag(TaggingBuilder builder) {
        UnTagRequest r = TaggingWebService.OBJECT_FACTORY.createUnTagRequest();
        r.getUserIds().addAll(((TaggingBuilderImpl)builder).getUsers());
        r.getTags().addAll(((TaggingBuilderImpl)builder).getTags());
        taggingWebService.removeTags(r);
    }

    @Override
    public TaggingBuilder createTaggingBuilder() {
        return new TaggingBuilderImpl(null);
    }       
      
    
    public class TaggingBuilderImpl implements TaggingBuilder {

        
        private final String domain;
        private final ImmutableList.Builder<String> usersBuilder;
        private final ImmutableList.Builder<String> tagsBuilder;

        public TaggingBuilderImpl(String domain) {
            usersBuilder = ImmutableList.builder();
            tagsBuilder = ImmutableList.builder();
            this.domain = domain;
        }

        @Override
        public TaggingBuilder addUsers(List<String> users) {
            usersBuilder.addAll(users);
            return this;
        }

        @Override
        public TaggingBuilder addTags(List<String> tags) {
            for (String tag : tags) {
                addTag(tag);
            }
            return this;
        }

        @Override
        public TaggingBuilder addUser(String userUuid) {
            usersBuilder.add(userUuid);
            return this;
        }

        @Override
        public TaggingBuilder addTag(String... tag) {
            tagsBuilder.add(composeTags(domain, tag));
            return this;
        }

        List<String> getTags() {
            return tagsBuilder.build();
        }

        List<String> getUsers() {
            return usersBuilder.build();
        }
    }
    
    
    protected static String composeTags(String domain, String[] tags) {
        StringBuilder sb = new StringBuilder();
        if (domain != null) {
            sb.append(domain).append('.');
        }

        for (int i = 0; i < tags.length; i++) {
            if (i != 0) {
                sb.append('.');
            }
            sb.append(tags[i]);
        }
        return sb.toString();
    }

    
    
}
