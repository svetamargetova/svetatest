package consys.common.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class CouponCapacityException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public CouponCapacityException() {
        super();
    }

     public CouponCapacityException(String message) {
	super(message);
    }

}
