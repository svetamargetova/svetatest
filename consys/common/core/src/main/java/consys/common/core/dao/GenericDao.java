package consys.common.core.dao;


import consys.common.core.bo.ConsysObject;
import consys.common.core.exception.NoRecordException;
import java.util.List;



public interface GenericDao<T extends ConsysObject> {

    public void create(T o);
    public void create(T o,boolean flush);
    public void update(T o);
    public void update(T o, boolean flush);
    public void delete(T o);    
    public T load(Long id, Class<T> clazz) throws NoRecordException;
    public T load(Long id) throws NoRecordException ;
    public List<T> listAll(Class<T> c);
    
}
