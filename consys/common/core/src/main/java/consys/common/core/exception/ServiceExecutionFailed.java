package consys.common.core.exception;

/**
 *
 * @author Palo
 */
public class ServiceExecutionFailed extends ConsysException {

    private static final long serialVersionUID = 8934944940313787852L;

    public ServiceExecutionFailed() {super();}

    public ServiceExecutionFailed(String message) {
        super(message);
    }
}
