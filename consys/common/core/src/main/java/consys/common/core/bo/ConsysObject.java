package consys.common.core.bo;

import java.io.Serializable;

public interface  ConsysObject extends Serializable{
    @Override
   public  String toString();

    @Override
   public boolean equals(Object arg);

    @Override
   public int hashCode();
}
