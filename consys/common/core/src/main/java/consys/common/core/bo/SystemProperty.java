package consys.common.core.bo;

/**
 *
 * @author Palo
 */
public class SystemProperty implements ConsysObject{
    private static final long serialVersionUID = -969605736860975759L;

    private static final String BOOL_TRUE = "true";
    private static final String BOOL_FALSE = "false";

    private Long id;
    private String key;
    private String value;
    private boolean toClient;

    public SystemProperty() {
        key = "";
        value = "";
    }



    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    public void setValueFromBoolean(boolean b){
        value = b ? BOOL_TRUE : BOOL_FALSE;
    }

    @Override
    public boolean equals(Object obj) {
         if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        SystemProperty uo = (SystemProperty) obj;
        return uo.key.equalsIgnoreCase(key);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.key != null ? this.key.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
       return "SystemProperty["+key+":"+value+"]";
    }

    /**
     * @return the toClient
     */
    public boolean isToClient() {
        return toClient;
    }

    /**
     * @param toClient the toClient to set
     */
    public void setToClient(boolean toClient) {
        this.toClient = toClient;
    }






}
