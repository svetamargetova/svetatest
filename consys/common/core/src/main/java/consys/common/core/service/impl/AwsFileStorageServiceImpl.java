package consys.common.core.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.internal.Constants;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.Transfer.TransferState;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerConfiguration;
import com.amazonaws.services.s3.transfer.Upload;
import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadStatus;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

/**
 *
 * @author palo
 */
public class AwsFileStorageServiceImpl implements AwsFileStorageService, DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(AwsFileStorageServiceImpl.class);
    // data
    private AWSCredentials credentials;
    private String endpoint;
    private final AmazonS3Client s3Client;

    public AwsFileStorageServiceImpl(AWSCredentials credentials, String endpoint) {
        this.credentials = credentials;
        this.endpoint = endpoint;

        s3Client = createNewClient();
    }

    /** vytvori noveho aws s3 */
    private AmazonS3Client createNewClient() {
        AmazonS3Client client = new AmazonS3Client(credentials);
        client.setEndpoint(endpoint);
        return client;
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, String mimeType, String cache, boolean attachment, InputStream is, long size) {
        if (StringUtils.isBlank(bucket) || StringUtils.isBlank(uniqueKey) || StringUtils.isBlank(fileName) || StringUtils.isBlank(mimeType) || StringUtils.isBlank(cache)) {
            throw new NullPointerException();
        }
        ObjectMetadata om = new ObjectMetadata();
        om.setCacheControl(cache);
        if (size > 0) {
            om.setContentLength(size);
        }
        logger.debug("S3({}) put object {}", bucket, fileName);
        if (attachment) {
            om.setContentDisposition(String.format("attachment; filename=\"%s\"", fileName));
        }
        om.setContentType(mimeType);
        s3Client.putObject(bucket, uniqueKey, is, om);
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, Cache cache, boolean attachment, InputStream is, long size) {
        createObject(bucket, uniqueKey, fileName, fileType.getContentType(), cache.getCache(), attachment, is, size);
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, Cache cache, boolean attachment, InputStream is) {
        createObject(bucket, uniqueKey, fileName, fileType.getContentType(), cache.getCache(), attachment, is, 0);
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, String mimeType, String cache, boolean attachment, InputStream is) {
        createObject(bucket, uniqueKey, fileName, mimeType, cache, attachment, is, 0);
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType file, boolean attachment, InputStream is, long size) {
        createObject(bucket, uniqueKey, fileName, file.getContentType(), Cache.NO_CACHE.getCache(), attachment, is, size);
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType file, boolean attachment, InputStream is) {
        createObject(bucket, uniqueKey, fileName, file, attachment, is, 0);
    }

    @Override
    public void deleteObject(String bucket, String uniqueKey) {
        s3Client.deleteObject(bucket, uniqueKey);
    }

    @Override
    public void deleteObject(String bucket, List<String> uniqueKeys) {
        DeleteObjectsRequest dor = new DeleteObjectsRequest(bucket);
        dor.setQuiet(true);
        for (String key : uniqueKeys) {
            dor.getKeys().add(new DeleteObjectsRequest.KeyVersion(key));
        }
        DeleteObjectsResult res = s3Client.deleteObjects(dor);
    }

    @Override
    public S3Object getObject(String bucket, String uniqueKey) {
        return s3Client.getObject(bucket, uniqueKey);
    }

    @Override
    public UploadStatus uploadFile(String bucket, String fileName, File file, UploadProgress uploadStatus) {
        TransferManager tm = new TransferManager(createNewClient());
        Upload upload = tm.upload(bucket, fileName, file);
        return runUpload(upload, fileName, uploadStatus, tm, bucket);
    }

    @Override
    public UploadStatus uploadFile(String bucketName, String fileName, InputStream is, String contentType, UploadProgress uploadStatus) {
        TransferManagerConfiguration config = new TransferManagerConfiguration();
        config.setMinimumUploadPartSize(15 * Constants.MB);

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(contentType);
        metadata.setContentDisposition(String.format("attachment; filename=\"%s\"", fileName));
        metadata.setContentLength(uploadStatus.getTotalBytes());

        TransferManager tm = new TransferManager(createNewClient());
        tm.setConfiguration(config);

        Upload upload = tm.upload(bucketName, fileName, is, metadata);
        return runUpload(upload, fileName, uploadStatus, tm, bucketName);
    }

    /** spusti upload souboru na amazony */
    private UploadStatus runUpload(Upload upload, String fileName, UploadProgress uploadStatus, TransferManager tm, String bucketName) {
        long counter = 0;
        while (!upload.isDone() && !uploadStatus.isCancel()) {
            if (counter % 30 == 0) {
                TransferState state = upload.getState();
                logger.debug("Transfer: " + upload.getDescription());
                logger.debug("  - State: " + state + "  - Progress: " + upload.getProgress().getBytesTransfered());
            }

            uploadStatus.setTransferedBytes(upload.getProgress().getBytesTransfered());

            // TODO: pokud se delsi dobu nemeni stav prenesenych bytu, tak odstrihnout a ukoncit

            // Do work while we wait for our upload to complete...
            try {
                Thread.sleep(1000);
                counter++;
            } catch (InterruptedException ex) {
                logger.error("Unable thread sleep", ex);
                return UploadStatus.ERROR;
            }
        }
        // uzavreni spojeni s3 klienta
        tm.shutdownNow();
        // TODO: doresit aby to koncilo bez vyjimky

        if (uploadStatus.isCancel()) {
            logger.info("Upload " + fileName + " was canceled");
            return UploadStatus.CANCELED;
        } else {
            logger.info("Upload " + fileName + " finish in state " + upload.getState());
            if (upload.getState() == TransferState.Completed) {
                return UploadStatus.UPLOADED;
            } else {
                return UploadStatus.ERROR;
            }
        }
    }

    @Override
    public void destroy() throws Exception {
        s3Client.shutdown();
    }
}
