package consys.common.core.dao;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import java.util.List;


/**
 *
 * @author Palo
 */
public interface SystemPropertyDao extends GenericDao<SystemProperty> {
    public SystemProperty loadByKey(String key) throws NoRecordException;
    public List<SystemProperty> listByToClient(boolean toClient);
}
