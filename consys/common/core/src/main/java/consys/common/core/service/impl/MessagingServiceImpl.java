package consys.common.core.service.impl;

import consys.common.core.SystemProperties;
import consys.common.core.service.MessagingService;
import consys.common.core.service.SystemPropertyService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class MessagingServiceImpl extends AbstractService implements MessagingService {

    private static final Logger log = LoggerFactory.getLogger(MessagingServiceImpl.class);
    private SystemPropertyService propertyService;
    private String messagingServerUrl;

    /** zaregistruje entitu na messaging serveru */
    @Override
    public MessagingStateEnum registerEntity(String uuid, String email) {
        String request = "<rq><t>register</t><e>" + uuid + "</e><mail>" + email + "</mail></rq>";
        return sendWrappedPost(getMessagingServerUrl(), request);
    }

    /** odesle systemovy mail na zadanou adresu a se zadanou zpravou */
    @Override
    public MessagingStateEnum sendMail(String from, String to, String subject, String msg, boolean isHTML) {
        String request = "<rq><t>sysMail</t><f>" + from + "</f><mail>" + to + "</mail>"
                + "<sub>" + subject + "</sub><msg><![CDATA[" + msg + "]]></msg><html>" + isHTML + "</html></rq>";
        return sendWrappedPost(getMessagingServerUrl(), request);
    }

    /** odesle zpravu jako formular s metodou post */
    private static String sendPost(String servletAddress, String content) throws MalformedURLException, IOException {
        if (log.isDebugEnabled()) {
            log.debug("Server address " + servletAddress);
        }
        URL url = new URL(servletAddress);

        // otevreni spojeni a nastaveni parametru
        URLConnection conn = url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        if (log.isDebugEnabled()) {
            log.debug("Connection open ");
        }

        // jedna se jakoby o data z formulare
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Accept-Charset", "UTF-8;q=0.7,*;q=0.7");

        // parametr xml=data
        content = "xml" + "=" + URLEncoder.encode(content, "UTF-8");
        if (log.isDebugEnabled()) {
            log.debug("Sending " + content);
        }
        // odeslani pozadavku
        OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
        out.write(content);
        out.flush();
        out.close();

        // prijem odpovedi
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            response.append(line + "\n");
        }
        line = null;
        in.close();
        if (log.isDebugEnabled()) {
            log.debug("Response " + response.toString());
        }
        return response.toString();
    }

    /** zaobaluje metodu sendPost - zpracuje vyjimky a zjisti hodnotu odpovedi */
    private static MessagingStateEnum sendWrappedPost(String servletAddress, String content) {
        String result = null;
        try {
            result = sendPost(servletAddress, content);
        } catch (MalformedURLException ex) {
            log.error("bad messaging url", ex);
            return MessagingStateEnum.COMMUNICATION_FAIL;
        } catch (IOException ex) {
            log.error("write or read fail", ex);
            return MessagingStateEnum.COMMUNICATION_FAIL;
        }

        if (result.startsWith("<rs><s>")) {
            // ocekavana odpoved
            String value = result.substring(7, result.indexOf("</s>"));
            try {
                return MessagingStateEnum.valueOf(value);
            } catch (IllegalArgumentException ex) {
                return MessagingStateEnum.UNKNOWN_STATE;
            }
        } else {
            // neocekavana odpoved
            log.error("unexpected messaging response");
            return MessagingStateEnum.COMMUNICATION_FAIL;
        }
    }

    public synchronized String getMessagingServerUrl() {
        if (messagingServerUrl == null) {
            messagingServerUrl = propertyService.getString(SystemProperties.MESSAGING_SERVER_URL);
            if (log().isDebugEnabled()) {
                log().debug("Messaging Server Url: " + messagingServerUrl);
            }
        }
        return messagingServerUrl;
    }

    /**
     * @return the propertyService
     */
    public SystemPropertyService getPropertyService() {
        return propertyService;
    }

    /**
     * @param propertyService the propertyService to set
     */
    public void setPropertyService(SystemPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    /**
     * @param messagingServerUrl the messagingServerUrl to set
     */
    public void setMessagingServerUrl(String messagingServerUrl) {
        log().info("Messaging URL intialized for: "+messagingServerUrl);
        this.messagingServerUrl = messagingServerUrl;
    }
}
