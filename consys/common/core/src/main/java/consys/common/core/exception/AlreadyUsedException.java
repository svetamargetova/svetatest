package consys.common.core.exception;

/**
 * Vyjimka pro pripady, kdy je pozadovano smazani objektu, ale protoze je jiz pouzit, nelze ho odstranit
 * @author pepa
 */
public class AlreadyUsedException extends ConsysException {

    private static final long serialVersionUID = 306801149678766L;

    public AlreadyUsedException() {
        super();
    }

    public AlreadyUsedException(String message) {
        super(message);
    }
}
