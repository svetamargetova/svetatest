package consys.common.core.bo;

import java.sql.Blob;
import java.util.Date;

/**
 * UUID obrazku sa resolvuje tak ze pre
 * - norm    UUID-000
 * - maly    UUID-001
 * - stredny UUID-002
 * - velky   UUID-003
 * @author Palci
 */
public class Image implements ConsysObject {
    private static final long serialVersionUID = -8052496921221263255L;

    enum Size{
        None("-000"),
        Small("-001"),
        Middle("-002"),
        Big("-003");

        private String size;

        private Size(String size) {
            this.size = size;
        }

        public String getSize() {
            return size;
        }
    };

    private Long id;
    private Blob data;
    private String fileName;
    private String contentType;
    private Integer width;
    private Integer height;
    private Date dateInserted;
    private byte[] byteData;
    private String uuid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    /** vraci uid obrazku */
    public String getUuid() {
        return uuid;
    }

    /** nastavuje uid obrazku */
    public void setUuid(String uid) {
        this.uuid = uid;
    }

    @Override
    public String toString() {
        return String.format("Image[id=%s, fileName=%s contentType=%s, width=%s, height=%s]", id, fileName, contentType, width, height);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        Image uo = (Image) obj;
        return uo.uuid.equalsIgnoreCase(uuid);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    

    /**
     * @return the data
     */
    public Blob getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Blob data) {
        this.data = data;
    }

    /**
     * @return the byteData
     */
    public byte[] getByteData() {
        return byteData;
    }

    /**
     * @param byteData the byteData to set
     */
    public void setByteData(byte[] byteData) {
        this.byteData = byteData;
    }
}
