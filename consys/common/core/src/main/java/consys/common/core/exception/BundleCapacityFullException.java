package consys.common.core.exception;

/**
 *
 * @author Palo
 */
public class BundleCapacityFullException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    private String bundleUuid;

    public BundleCapacityFullException(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    /**
     * @return the bundleUuid
     */
    public String getBundleUuid() {
        return bundleUuid;
    }

}
