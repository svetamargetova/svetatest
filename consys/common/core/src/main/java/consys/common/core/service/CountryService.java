/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.core.service;

import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.UsStateEnum;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface CountryService {

    public List<CountryEnum> listCountries();

    public List<UsStateEnum> listUsStates();

    public UsStateEnum loadUsStateById(Integer id) throws NoRecordException;

    public CountryEnum loadCountryById(Integer id) throws NoRecordException;
}
