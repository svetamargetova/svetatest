package consys.common.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class IllegalPropertyValue extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public IllegalPropertyValue() {
        super();
    }

     public IllegalPropertyValue(String message) {
	super(message);
    }

}
