package consys.common.core.aws;

import com.amazonaws.services.s3.model.S3Object;
import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadStatus;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 *
 * @author palo
 */
public interface AwsFileStorageService {

    void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, Cache cache, boolean attachment, InputStream is, long size);

    void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, Cache cache, boolean attachment, InputStream is);

    void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, boolean attachment, InputStream is, long size);

    void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, boolean attachment, InputStream is);

    void createObject(String bucket, String uniqueKey, String fileName, String mimeType, String cache, boolean attachment, InputStream is, long size);

    void createObject(String bucket, String uniqueKey, String fileName, String mimeType, String cache, boolean attachment, InputStream is);

    void deleteObject(String bucket, String uniqueKey);

    void deleteObject(String bucket, List<String> uniqueKeys);

    /**
     * <b>Uzavrit input stream</b>
     * @param bucket
     * @param uniqueKey
     * @return 
     */
    S3Object getObject(String bucket, String uniqueKey);

    UploadStatus uploadFile(String bucket, String fileName, File file, UploadProgress uploadStatus);

    UploadStatus uploadFile(String bucketName, String fileName, InputStream is, String contentType, UploadProgress uploadStatus);

    public enum Cache {

        NO_CACHE("no-cache|no-cache"),
        CACHE("max-age=31556926");
        String cache;

        private Cache(String cache) {
            this.cache = cache;
        }

        public String getCache() {
            return cache;
        }
    }

    public enum FileType {

        PDF("application/pdf"),
        HTML("text/html");
        String contentType;

        FileType(String type) {
            this.contentType = type;
        }

        public String getContentType() {
            return contentType;
        }
    }
}
