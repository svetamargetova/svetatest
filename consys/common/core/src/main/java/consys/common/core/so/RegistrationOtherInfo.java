package consys.common.core.so;

import java.util.HashMap;
import java.util.Map;

/**
 * Objekt pro volitelna pole registrace
 * @author pepa
 */
public class RegistrationOtherInfo {

    // data
    private Map<String, String> answers;

    public RegistrationOtherInfo() {
        answers = new HashMap<String, String>();
    }

    public Map<String, String> getAnswers() {
        return answers;
    }
}
