package consys.common.core.so;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pepa
 */
public class UploadProgressMonitor {

    private static Logger log = LoggerFactory.getLogger(UploadProgressMonitor.class);
    // data
    private static Map<String, UploadProgress> map;
    private static CopyOnWriteArrayList<String> uploadingUsers;

    public UploadProgressMonitor() {
        map = Collections.synchronizedMap(new HashMap<String, UploadProgress>());
        uploadingUsers = new CopyOnWriteArrayList<String>();
    }

    public void addUploadProgress(String key, UploadProgress progress) {
        map.put(key, progress);
    }

    public UploadProgress getProgress(String key) {
        return map.get(key);
    }

    public void removeUploadProgress(String key) {
        String user = key.substring(0, 32);
        log.debug("Uploading finished for user: " + user);
        uploadingUsers.remove(user);
        map.remove(key);
    }

    public boolean isUploadingUser(String user) {
        log.debug("Uploading start for user: " + user);
        return !uploadingUsers.addIfAbsent(user);
    }
}
