/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.common.core.aws;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.util.StringInputStream;
import consys.common.core.service.impl.AwsFileStorageServiceImpl;
import consys.common.utils.UuidProvider;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author palo
 */
@ContextConfiguration(locations = {"event-aws-related-spring.xml"})
public class AwsFileStorageServiceTest extends AbstractTestNGSpringContextTests  {
 
    
    @Autowired
    private AwsFileStorageServiceImpl service;
    
    @Test(groups={"aws-services"})
    public void testOperations(){
        assertNotNull(service);
        
        // test na ulozeni takeplace.pdf
        
        InputStream is = this.getClass().getResourceAsStream("/takeplace.pdf");
        assertNotNull(is);
        
        String uuid = UuidProvider.getUuid();
        try{
        service.createObject("takeplace.test.bucket", uuid, "AwsFileStorageServiceTest_"+uuid, AwsFileStorageService.FileType.PDF,true, is);
        
        
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
        
    }
    
    
}
