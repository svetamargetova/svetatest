package consys.common.constants.img;

/**
 *
 * @author Palo
 */
public enum EventLogoImageEnum {
                
    LIST("-100",225,30),            
    TICKET("-200",400,53),             
    WALL("-300",300,40),        
    ORIGINAL("-000",0,0);
    
    private String suffix;
    private int width;
    private int height;

    private EventLogoImageEnum(String suffix,int w, int h) {
        this.suffix = suffix;
        this.width = w;
        this.height =h;
    }

    public String getFullUuid(String prefix) {
        return prefix + suffix;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }
}
