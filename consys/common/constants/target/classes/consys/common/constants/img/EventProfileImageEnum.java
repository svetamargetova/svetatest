package consys.common.constants.img;

/**
 *
 * @author Palo
 */
public enum EventProfileImageEnum {

    ORIGINAL("-000",0,0),
    WEB("-001",120,120),
    LIST("-002",56,56);
        
    private String suffix;
    private int width;
    private int height;

    private EventProfileImageEnum(String suffix,int w, int h) {
        this.suffix = suffix;
        this.width = w;
        this.height =h;
    }

    public String getFullUuid(String prefix) {
        return prefix + suffix;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }
}
