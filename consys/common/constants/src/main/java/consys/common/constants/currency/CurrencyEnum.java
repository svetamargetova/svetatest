package consys.common.constants.currency;

/**
 *
 * @author pepa
 */
public enum CurrencyEnum {

    EUR, USD, CZK;
    private static final long serialVersionUID = -1533838302507380736L;

    public static CurrencyEnum toEnum(String value) {
        if (value.equalsIgnoreCase("EUR")) {
            return EUR;
        } else if (value.equalsIgnoreCase("USD")) {
            return USD;
        } else if (value.equalsIgnoreCase("CZK")) {
            return CZK;
        } else {
            throw new IllegalArgumentException("unsupported currency");
        }
    }
}
