package consys.common.constants.img;

/**
 *
 * @author Palo
 */
public enum UserProfileImageEnum {
    
    LIST("-200",56,56),    
    PROFILE("-100",120,120),
    ORIGINAL("-000",0,0);
    
    private String suffix;
    private int width;
    private int height;

    private UserProfileImageEnum(String suffix,int w, int h) {
        this.suffix = suffix;
        this.width = w;
        this.height =h;
    }

    public String getFullUuid(String prefix) {
        return prefix + suffix;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }
    
    
    
}
