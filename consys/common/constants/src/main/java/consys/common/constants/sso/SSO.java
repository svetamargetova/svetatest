package consys.common.constants.sso;

/**
 *
 * @author palo
 */
public enum SSO {

    TWITTER, FACEBOOK, LINKEDIN, GOOGLE_PLUS;
    private static final long serialVersionUID = 3782056713193090930L;
}
