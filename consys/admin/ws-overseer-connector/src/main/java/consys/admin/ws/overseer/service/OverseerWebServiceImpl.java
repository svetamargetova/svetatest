package consys.admin.ws.overseer.service;

import consys.admin.ws.overseer.EventWebServiceEnum;
import consys.admin.ws.overseer.EventWebServiceGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OverseerWebServiceImpl implements OverseerWebService {

    private static final Logger logger = LoggerFactory.getLogger(OverseerWebServiceImpl.class);
    private final EventWebServiceGateway commonWSGateway;
    private final EventWebServiceGateway maintenanceWSGateway;
    private final EventWebServiceGateway userWSGateway;
    private final EventWebServiceGateway registrationWSGateway;

    public OverseerWebServiceImpl() throws Exception {
        logger.info("Initializing OverseerWebService");
        commonWSGateway = new EventWebServiceGateway(EventWebServiceEnum.Common.getContextPath());
        maintenanceWSGateway = new EventWebServiceGateway(EventWebServiceEnum.Maintenance.getContextPath());
        registrationWSGateway = new EventWebServiceGateway(EventWebServiceEnum.Registration.getContextPath());
        userWSGateway = new EventWebServiceGateway(EventWebServiceEnum.User.getContextPath());

    }

    @Override
    public <T> T sendAndReceive(EventWebServiceEnum ws, String overseerUri, Object payload) {
        
        String url = overseerUri + ws.getUriPath();
        
        switch (ws) {
            case Common:
                return commonWSGateway.<T>sendAndReceive(url, payload);
            case Maintenance:
                return maintenanceWSGateway.<T>sendAndReceive(url, payload);
            case Registration:
                return registrationWSGateway.<T>sendAndReceive(url, payload);
            case User:
                return userWSGateway.<T>sendAndReceive(url, payload);
            default:
                throw new NullPointerException("No Support for sendAndReceive web service for " + ws);
        }

    }    
}
