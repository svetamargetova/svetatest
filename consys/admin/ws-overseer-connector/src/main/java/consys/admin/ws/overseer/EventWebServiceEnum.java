/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.ws.overseer;

/**
 *
 * @author palo
 */
public enum EventWebServiceEnum {
    
    Common("consys.event.common.ws","/ws/event/common"),
    User("consys.event.user.ws","/ws/event/user"),
    Registration("consys.event.registration.ws","/ws/event/registration"),
    Maintenance("consys.event.maintenance.ws","/ws/event/maintenance");
    
    
    
    private String contextPath;
    private String uriPath;

    private EventWebServiceEnum(String contextPath, String uriPath) {        
        this.contextPath = contextPath;
        this.uriPath = uriPath;
    }

    /**
     * @return the contextPath
     */
    public String getContextPath() {
        return contextPath;
    }

    /**
     * @return the uriPath
     */
    public String getUriPath() {
        return uriPath;
    }
    
    
}
