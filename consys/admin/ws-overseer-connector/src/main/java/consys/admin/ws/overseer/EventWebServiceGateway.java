package consys.admin.ws.overseer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author Palo
 */
public class EventWebServiceGateway extends WebServiceGatewaySupport {

    private static final Logger logger = LoggerFactory.getLogger(EventWebServiceGateway.class);

    public EventWebServiceGateway(String contextPath) throws Exception {
        super();
        logger.info("Initializing WS Gateway for: {}", contextPath);
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath(contextPath);
        marshaller.afterPropertiesSet();
        setMarshaller(marshaller);
        setUnmarshaller(marshaller);
        afterPropertiesSet();
    }

    public <T> T sendAndReceive(String overseerWsUri, Object o) {
        logger.debug(overseerWsUri);
        return (T) getWebServiceTemplate().marshalSendAndReceive(overseerWsUri, o);
    }
}
