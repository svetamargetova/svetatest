package consys.admin.ws.overseer.service;

import consys.admin.ws.overseer.EventWebServiceEnum;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface OverseerWebService {

    public <T> T sendAndReceive(EventWebServiceEnum ws, String overseerUri, Object payload);
    
}
