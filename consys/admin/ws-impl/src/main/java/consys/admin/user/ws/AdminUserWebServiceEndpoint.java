package consys.admin.user.ws;

import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.core.service.UserService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class AdminUserWebServiceEndpoint implements AdminUserWebService, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(AdminUserWebServiceEndpoint.class);
    private ApplicationContext applicationContext;
    private EventUserOrganizationService eventUserOrganizationService;
    private UserOrganizationService userOrganizationService;   
    private UserService userService;

   
    @Override
    @PayloadRoot(localPart = "LoadUserEmailRequest", namespace = NAMESPACE)
    public LoadUserEmailResponse loadUserEmail(LoadUserEmailRequest request) {
        try {
            String email = getUserService().loadUserEmailByUuid(request.getUserUuid());
            LoadUserEmailResponse response = OBJECT_FACTORY.createLoadUserEmailResponse();
            response.setEmail(email);
            return response;
        } catch (RequiredPropertyNullException ex) {
            throw new NullPointerException();
        } catch (NoRecordException ex) {
            throw new NullPointerException();
        }
    }

    

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
       

    public UserService getUserService() {
        if (userService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(UserService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserService not found!");
                }
                userService = (UserService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return userService;
    }
}
