package consys.admin.event.ws;

import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.bo.thumb.UserInvitationRequest;
import consys.admin.event.core.service.EventInvitationService;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.service.EventService;
import consys.admin.user.core.service.UserService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class AdminEventWebServiceEndpoint implements AdminEventWebService, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(AdminEventWebServiceEndpoint.class);
    private ApplicationContext applicationContext;
    private EventUserOrganizationService eventUserOrganizationService;
    private UserOrganizationService userOrganizationService;
    private EventInvitationService eventInvitationService;
    private EventService eventService;
    private UserService userService;

    @Override
    @PayloadRoot(localPart = "LoadUsersToEventRequest", namespace = NAMESPACE)
    public LoadUsersToEventResponse loadUserToEvent(LoadUsersToEventRequest mainRequest) {
        List<UserInvitationRequest> requests = Lists.newArrayList();
        EventUserOrganizationRelation relation = null;
        if (mainRequest.getRelation() != null) {
            relation = parseFromWS(mainRequest.getRelation());
        }

        for (EventUserRequest userRequest : mainRequest.getEventUserRequest()) {
            UserInvitationRequest r = new UserInvitationRequest();
            r.setEmail(userRequest.getUserEmail());
            r.setEvent(mainRequest.getEventUuid());
            r.setFrom(mainRequest.getFromUserUuid());
            r.setMessage(userRequest.getMessage());
            r.setName(userRequest.getUserName());
            r.setRelation(relation);
            r.setUuid(userRequest.getUserUuid());
            r.setWithInvitationConfirm(mainRequest.isInvite());

            logger.debug("Load user '{}' to event into relation '{}'", r, relation);
            requests.add(r);
        }
        try {
            List<User> users = getEventInvitationService().createUsersInvivations(requests);
            LoadUsersToEventResponse out = OBJECT_FACTORY.createLoadUsersToEventResponse();
            for (User u : users) {
                boolean emptyOrganization = u.getDefaultUserOrganization().getOrganization() == null;
                EventUserResponse resp = OBJECT_FACTORY.createEventUserResponse();
                resp.setPosition(u.getDefaultUserOrganization().getPosition());
                resp.setOrganization(emptyOrganization ? "" : u.getDefaultUserOrganization().getOrganization().getName());
                resp.setFullName(u.getFullName());
                resp.setLastName(u.getLastName());
                resp.setUserUuid(u.getUuid());
                resp.setEmail(u.getEmailOrMergeEmail());
                resp.setProfileImagePrefix(u.getPortraitImageUuidPrefix());
                out.getEventUserResponse().add(resp);
            }
            return out;
        } catch (NoRecordException ex) {
            throw new NullPointerException("Some required attribute inside request is missing or bad");
        } catch (RequiredPropertyNullException ex) {
            logger.error("Error", ex);
            throw new NullPointerException("Some required attribute inside request is bad");
        }
    }

    @Override
    @PayloadRoot(localPart = "UpdateUserRelationToEventRequest", namespace = NAMESPACE)
    public UpdateUserRelationToEventResponse updateUserRelationToEvent(UpdateUserRelationToEventRequest request) {
        UpdateUserRelationToEventResponse out = OBJECT_FACTORY.createUpdateUserRelationToEventResponse();
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Update User Relation To Event");
            }
            EventUserOrganizationRelation newRelation = parseFromWS(request.getNewRelation());

            getEventUserOrganizationService().updateEventUserOrganizationRelation(request.getUserUuids(), request.getEventUuid(), request.getMessage(), newRelation);
            out.setResult(true);
        } catch (RequiredPropertyNullException ex) {
            throw new NullPointerException();
        } catch (NoRecordException ex) {
            out.setResult(false);
        }
        return out;
    }

    @Override
    @PayloadRoot(localPart = "LoadEventOverseerRequest", namespace = NAMESPACE)
    public LoadEventOverseerResponse loadEventOverseer(LoadEventOverseerRequest request) {
        try {
            EventOverseer eo = getEventService().loadEventOverseer(request.getEventUuid());
            LoadEventOverseerResponse response = OBJECT_FACTORY.createLoadEventOverseerResponse();
            response.setOverseerWebServiceUrl(eo.getWebServicesUrl());
            return response;
        } catch (NoRecordException ex) {
            throw new NullPointerException();
        } catch (RequiredPropertyNullException ex) {
            throw new NullPointerException();
        }
    }

    private EventUserOrganizationRelation parseFromWS(Relation r) {
        if (r == null) {
            return null;
        }
        switch (r) {
            case ADMINISTRATOR:
                return EventUserOrganizationRelation.ADMINISTRATOR;
            case REGISTRED:
                return EventUserOrganizationRelation.REGISTRED;
            case WAITING:
                return EventUserOrganizationRelation.WAITING;
            case VISITOR:
                return EventUserOrganizationRelation.VISITOR;
            case OWNER:
            // banned
            default:
                return EventUserOrganizationRelation.VISITOR;
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * @return the eventUserOrganizationService
     */
    public EventUserOrganizationService getEventUserOrganizationService() {
        if (eventUserOrganizationService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventUserOrganizationService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean EventUserOrganizationService not found!");
                }
                eventUserOrganizationService = (EventUserOrganizationService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return eventUserOrganizationService;
    }

    /**
     * @return the userOrganizationService
     */
    public UserOrganizationService getUserOrganizationService() {
        if (userOrganizationService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(UserOrganizationService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserOrganizationService not found!");
                }
                userOrganizationService = (UserOrganizationService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return userOrganizationService;
    }

    public EventInvitationService getEventInvitationService() {
        if (eventInvitationService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventInvitationService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserOrganizationService not found!");
                }
                eventInvitationService = (EventInvitationService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return eventInvitationService;
    }

    public EventService getEventService() {
        if (eventService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserOrganizationService not found!");
                }
                eventService = (EventService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return eventService;
    }

    public UserService getUserService() {
        if (userService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(UserService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserService not found!");
                }
                userService = (UserService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return userService;
    }
}
