package consys.admin.payment.ws;

import consys.admin.event.core.service.EventService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.payment.webservice.DelegateWebService;
import consys.payment.ws.delegate.DelegateOrderStateChangedRequest;
import consys.payment.ws.delegate.DelegateOrderStateChangedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class PaymentDelegateEndPoint implements DelegateWebService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentDelegateEndPoint.class);
    private EventService eventService;

    @Override
    @PayloadRoot(localPart = "DelegateOrderStateChangedRequest", namespace = NAMESPACE)
    public DelegateOrderStateChangedResponse delegateOrderStateChanged(DelegateOrderStateChangedRequest request) {
        DelegateOrderStateChangedResponse response = OBJECT_FACTORY.createDelegateOrderStateChangedResponse();
        try {
            switch (request.getToState()) {
                case CONFIRMED:
                    eventService.updateEventActivate(request.getOrderUuid());
                    break;
                default:
                    logger.info("Delegation of {} with invalid state.", request);
            }

            response.setProcessed(true);
        } catch (RequiredPropertyNullException ex) {
            response.setProcessed(false);
        } catch (NoRecordException ex) {
            response.setProcessed(false);
        }
        return response;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
}
