package consys.admin.user.core.dao;

import consys.admin.user.core.AbstractUserAdminTest;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.util.ActivationUtils;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */

public class UserActivationDaoTest extends AbstractUserAdminTest {

    // userDao
    private UserDao userDao;
    // activationDao
    private UserActivationDao activationDao;
    // user login
    private UserLoginInfoDao loginInfoDao;
    // pro vytvořenou aktivaci
    
    
    private UserLoginInfo loginInfo;

    @Autowired(required = true)
    public void setActivationDao(UserActivationDao activationDao) {
        this.activationDao = activationDao;
    }

    @Autowired(required = true)
    public void setLoginInfoDao(UserLoginInfoDao loginInfoDao) {
        this.loginInfoDao = loginInfoDao;
    }

    @Autowired(required = true)
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
    

    @Test(groups = {"dao"})
    public void testDaoNotNull() {
        assertNotNull(activationDao);        
        assertNotNull(loginInfoDao);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testCreateLoginInfoWithUser() throws NoRecordException {
        String email = "chroustal@ema-il.cz";
        UserLoginInfo userlogininfo = UserUtils.prepareNewUser(email, "12345");
        userlogininfo.getUser().setFirstName("Jirka");
        userlogininfo.getUser().setLastName("Jirka");
        UserOrganization uo = UserUtils.prepareUserOrganization(userlogininfo.getUser());
        //userlogininfo.getUser().setPrivateUserOrganization(uo);
        userDao.create(userlogininfo.getUser(),false);
        loginInfoDao.create(userlogininfo);

        UserLoginInfo info = loginInfoDao.loadByUsername(email);
        assertEquals(info, userlogininfo);
        loginInfo = info;
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateLoginInfoWithUser"})
    public void testCRDActivation() throws NoRecordException {

        testCreateLoginInfoWithUser();
        // Create
        UserActivation activation = ActivationUtils.createActivation(loginInfo);
        activationDao.create(activation);
        assertFalse("Activation was not created", activation.getId() == 0);

        // Read
        UserActivation activation1 = activationDao.loadWithUserByActivationKey(activation.getKey());
        assertEquals("Activacie sa nerovnaju!", activation, activation1);

        // Delete
        activationDao.deleteByActivationKey(activation.getKey());

        // Test load
        try {
            activationDao.loadWithUserByActivationKey(activation.getKey());
            fail("Aktivacia sa nacitala!");
        } catch (NoRecordException e) {
        }
    }

}
