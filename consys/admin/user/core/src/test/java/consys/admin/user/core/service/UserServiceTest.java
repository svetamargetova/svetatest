package consys.admin.user.core.service;

import consys.common.core.service.NotxService;
import java.util.List;
import consys.admin.user.core.AbstractUserAdminTest;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.dao.UserActivationDao;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.core.service.exception.InvitationException;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.NotX;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Palo
 */
public class UserServiceTest extends AbstractUserAdminTest {

    @Autowired
    private UserService service;
    @Autowired
    private UserActivationDao activationDao;
    @Autowired
    private UserLoginInfoService loginInfoService;
        

    
    
    @Test(groups = {"service"})
    public void createUserTest() throws ServiceExecutionFailed, UsernameExistsException, NoRecordException, EmailException, RequiredPropertyNullException {
                        
        NotX.setNotificationService(mock(NotxService.class));
        
        
        UserLoginInfo info = UserUtils.prepareNewUser("palo", "palo");
        info.getUser().setFirstName("Palo");
        info.getUser().setLastName("Palo");

        service.createRegistredUser(info);

        UserLoginInfo li = loginInfoService.loadUserLoginInfo("palo");
        assertNotNull(li);
        assertEquals(info, li);

        li = loginInfoService.loadUserLoginInfoWithUserByUuid(info.getUser().getUuid());
        assertNotNull(li);
        assertEquals(info, li);
        
        info = UserUtils.prepareNewUser("PALO@email.com", "palo");
        info.getUser().setFirstName("Palo");
        info.getUser().setLastName("Palo");
        service.createRegistredUser(info);
        
        li = loginInfoService.loadUserLoginInfo("palo@email.com");        
        assertEquals(info, li);
        li = loginInfoService.loadUserLoginInfo("PALO@email.com");        
        assertEquals(info, li);
        String userUuid = service.loadUserUuidByUsername("palo@email.com");        
        assertEquals(info.getUser().getUuid(), userUuid);
        
    }

    @Test(groups = {"service"}, dependsOnMethods = {"createUserTest"}, expectedExceptions = {NoRecordException.class})
    public void createUserAndActivateTest() throws ServiceExecutionFailed, UsernameExistsException, NoRecordException, ActivationKeyException, EmailException, RequiredPropertyNullException, InvitationException {
        NotX.setNotificationService(mock(NotxService.class));
        UserLoginInfo info = UserUtils.prepareNewUser("palo", "palo");
        info.getUser().setFirstName("Palo");
        info.getUser().setLastName("Palo");

        service.createRegistredUser(info);

        UserLoginInfo li = loginInfoService.loadUserLoginInfo("palo");
        assertNotNull(li);
        assertEquals(info, li);

        UserActivation ua = activationDao.loadByUserLoginInfo(info);

        // pokus o aktivaciu pomocou sluzby na pozvanky
        try {
            service.activateInvitedUser("a", "a", "A", ua.getKey());
            fail("Validacia na aktivaciu pozvanky pre kluc nepozvaneho");
        } catch (ServiceExecutionFailed ee) {
        }

        service.activateUser(ua.getKey());

        activationDao.loadWithUserByActivationKey(ua.getKey());

        String email = service.loadUserEmailByUuid(info.getUser().getUuid());
        assertEquals(info.getUser().getEmailOrMergeEmail(), email);
        try {
            service.loadUserProfileImagePrefix(info.getUser().getUuid());
            fail();
        } catch (NoRecordException re) {
        }
    }

    @Test(groups = {"service"}, dependsOnMethods = {"createUserTest"})
    public void createInvitedUserAndActivateTest() throws ServiceExecutionFailed, UsernameExistsException, NoRecordException, ActivationKeyException, EmailException, RequiredPropertyNullException, InvitationException {
        NotX.setNotificationService(mock(NotxService.class));
        UserLoginInfo info = UserUtils.prepareNewUser("palo", "palo");
        info.getUser().setFirstName("Palo");
        info.getUser().setLastName("Palo");

        service.createRegistredUser(info);

        UserLoginInfo li = loginInfoService.loadUserLoginInfo("palo");
        assertNotNull(li);
        assertEquals(info, li);

        UserActivation ua = activationDao.loadByUserLoginInfo(info);
        ua.setInvitation(true);
        activationDao.update(ua);
        
        List l = loginInfoService.listDetailedUserLoginInfo(0, 10);
        assertEquals(l.size(), 1);
        assertEquals(loginInfoService.loadAllUserLoginInfoCount(), 1);
        
        

        try {
            service.activateUser(ua.getKey());
            fail("Validacia na aktiaciu pozvanky failed");
        } catch (InvitationException e) {
        }

        service.activateInvitedUser("Juraj", "Juraj", "Juraj", ua.getKey());

        // opakovana aktiacia
        try {
            service.activateInvitedUser("Juraj", "Juraj", "Juraj", ua.getKey());
        } catch (ActivationKeyException ee) {
        }


    }
}
