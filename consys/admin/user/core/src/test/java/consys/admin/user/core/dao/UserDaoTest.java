package consys.admin.user.core.dao;

import consys.admin.user.core.AbstractUserAdminTest;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */

public class UserDaoTest extends AbstractUserAdminTest {

    // testovací uživatel
    private final String fFirstNameOk = "Josef";
    private final String fLastNameOk = "Hubr";
    private final String fLoginEmailOk = "josef.hubr@gmail.com";
    private final String fPasswordOk = "hash321KKnonGenerated";
    // userDao
    private UserDao userDao;
    private UserOrganizationDao userOrganizationDao;
    private UserActivationDao activationDao;
    // pro vytvořeného uživatele
    private User testUser = null;

    @Autowired(required = true)
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired(required = true)
    public void setActivationDao(UserActivationDao activationDao) {
        this.activationDao = activationDao;
    }
    
    @Autowired(required = true)
    public void setUserOrganizationDao(UserOrganizationDao userOrganizationDao) {
        this.userOrganizationDao = userOrganizationDao;
    }

    @Test(groups = {"dao"})
    public void testDaoNotNull() {
        assertNotNull(userDao);
        assertNotNull(activationDao);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testCreateUser() throws NoRecordException {
        UserLoginInfo u = UserUtils.prepareNewUser(fLoginEmailOk, fPasswordOk);
        User user = u.getUser();
        user.setFirstName(fFirstNameOk);
        user.setLastName(fLastNameOk);
        userDao.create(user);
        UserOrganization uo = UserUtils.prepareUserOrganization(user);
        userOrganizationDao.create(uo);
        user.setPrivateUserOrganization(uo);
        user.setDefaultUserOrganization(uo);
        userDao.update(user);
        assertFalse("User was not created", user.getId() == 0);
        this.testUser = user;
        String uuid = userDao.loadUserUuidByUsername(fLoginEmailOk);
        assertEquals(user.getUuid(), uuid);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateUser"})
    public void testLoadUser() throws NoRecordException {
        testCreateUser();
        User loadedUser = userDao.loadByUuid(testUser.getUuid());
        assertNotNull(loadedUser);
        assertTrue(loadedUser.equals(testUser));
        loadedUser = userDao.loadDetailedByUuid(testUser.getUuid());
        assertNotNull(loadedUser);
        assertTrue(loadedUser.equals(testUser));
    }

}
