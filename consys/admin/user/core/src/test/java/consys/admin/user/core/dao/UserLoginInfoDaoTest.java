package consys.admin.user.core.dao;

import consys.admin.user.core.AbstractUserAdminTest;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */

public class UserLoginInfoDaoTest extends AbstractUserAdminTest {

    // testovací uživatel
    private final String fFirstNameOk = "Josef";
    private final String fLastNameOk = "Hubr";
    private final String fLoginEmailOk = "josef.hubr@gmail.com";
    private final String fPasswordOk = "hash321KKnonGenerated";
    private UserLoginInfoDao dao;
    private UserDao userDao;
    
    // pro vytvořeného uživatele
    private User testUser = null;

    
    

    @Test(groups = {"dao"})
    public void testDaoNotNull() {
        assertNotNull(dao);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testCreateUser() {
        UserLoginInfo u = UserUtils.prepareNewUser(fLoginEmailOk, fPasswordOk);
        User user = u.getUser();
        user.setFirstName(fFirstNameOk);
        user.setLastName(fLastNameOk);
        UserOrganization uo = UserUtils.prepareUserOrganization(user);
        userDao.create(user, false);
        dao.create(u);
        assertFalse("User was not created", user.getId() == 0);
        this.testUser = user;
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateUser"})
    public void testLoadUserByUsername() throws NoRecordException {
        testCreateUser();
        UserLoginInfo loadedUser = dao.loadByUsername(fLoginEmailOk);
        assertNotNull(loadedUser);     
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateUser"})
    public void testLoadUserByUuid() throws NoRecordException {
        testCreateUser();
        UserLoginInfo loadedUser = dao.loadByUserUuid(testUser.getUuid());
        assertNotNull(loadedUser);
    }
    
    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateUser"})
    public void testLoadUserByChangeToken() throws NoRecordException {
        testCreateUser();
        UserLoginInfo loadedUser = dao.loadByUserUuid(testUser.getUuid());
        loadedUser.setLostPasswordKey("123");
        dao.update(loadedUser);
        UserLoginInfo loadedUser1 = dao.loadByLostToken("123");
        assertNotNull(loadedUser1);
        assertEquals(loadedUser, loadedUser1);
    }


    /**
     * @return the dao
     */
    public UserLoginInfoDao getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    @Autowired(required = true)
    public void setDao(UserLoginInfoDao dao) {
        this.dao = dao;
    }

    /**
     * @param userDao the userDao to set
     */
    @Autowired(required = true)
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
