package consys.admin.user.core.dao;

import consys.admin.user.core.AbstractUserAdminTest;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.OrganizationTypeEnum;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.util.OrganizationUtils;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/** 
 *
 * @author pepa
 */

public class UserOrganizationDaoTest extends AbstractUserAdminTest {

    @Autowired
    private UserLoginInfoDao loginInfoDao;
    // UserOrganizationDao
    @Autowired
    private UserOrganizationDao userOrganizationDao;
    // UserDao
    @Autowired
    private UserDao userDao;
    // OrganizationDao
    @Autowired
    private OrganizationDao organizationDao;        
    // pro vytvořenou uživatelovu organizaci s uživatelem
    private UserOrganization testUserOrganization = null;
    // pro vytvořený typ organizace
    private Organization testOrganization = null;
   

    @Test(groups = {"dao"})
    public void testDaoNotNull() {
        assertNotNull(userDao);
        assertNotNull(organizationDao);        
        assertNotNull(userOrganizationDao);
        assertNotNull(loginInfoDao);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testCreateOrganization() {        
        
        User u = UserUtils.createUser();
        u.setFirstName("d");
        u.setLastName("d");
        u.setEmail("d");
        userDao.create(u);

        Organization o = OrganizationUtils.createOrganization("name", null, null);
        o.setOrganizationType(OrganizationTypeEnum.ALTERNATIVE_DISPUTE_RESOLUTION);
        o.setUserCreated(u);
        organizationDao.create(o);
        this.testOrganization = o;
    }

    @Test(groups = {"dao"})
    public void testCreateUserOrganization() {

        UserLoginInfo u = UserUtils.prepareNewUser("Marcel.Chroustal@consys.com", "123456");
        u.getUser().setFirstName("P");
        u.getUser().setLastName("P");

        UserOrganization userOrganization = UserUtils.prepareUserOrganization(u.getUser());
        userOrganization.setPosition("tester");

        userDao.create(u.getUser(), false);
        loginInfoDao.create(u);
        assertFalse("LoginInfo error", u.getId() == 0);
        userOrganization.setUser(u.getUser());

        userOrganizationDao.create(userOrganization);
        assertFalse("UserOrganization was not created", userOrganization.getId() == 0);

        this.testUserOrganization = userOrganization;
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateUserOrganization"})
    public void testLoadUserOrganization() throws NoRecordException {
        testCreateUserOrganization();
        UserOrganization uo = userOrganizationDao.loadUserOrganizationByUuid(testUserOrganization.getUuid());
        assertNotNull(uo);
        assertTrue(uo.equals(testUserOrganization));
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testLoadUserOrganizations() {

        User u = UserUtils.createUser();
        u.setFirstName("f");
        u.setLastName("d");
        u.setEmail("eeee");
        userDao.create(u);
        assertTrue(u.getId() != 0);

        testCreateOrganization();

        UserOrganization userOrganization1 = UserUtils.prepareUserOrganization(u);
        userOrganization1.setPosition("tester");
        userOrganization1.setOrganization(testOrganization);


        UserOrganization userOrganization2 = UserUtils.prepareUserOrganization(u);
        userOrganization2.setPosition("tester");
        userOrganization2.setOrganization(testOrganization);



        userOrganizationDao.create(userOrganization1);
        assertFalse("UserOrganization1 was not created", userOrganization1.getId() == 0);
        u.setPrivateUserOrganization(userOrganization1);


        userOrganizationDao.create(userOrganization2);
        assertFalse("UserOrganization1 was not created", userOrganization2.getId() == 0);

        List<UserOrganization> uos = userOrganizationDao.listByUserUuid(u.getUuid());
        assertTrue("Size " + uos.size(), uos.size() == 1);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testIsPrefferedUserOrganization() {

        User u = UserUtils.createUser();
        u.setFirstName("f");
        u.setLastName("d");
        u.setEmail("eeee");
        userDao.create(u);
        assertTrue(u.getId() != 0);

        testCreateOrganization();

        UserOrganization userOrganization1 = UserUtils.prepareUserOrganization(u);
        userOrganization1.setPosition("tester");
        userOrganization1.setOrganization(testOrganization);


        UserOrganization userOrganization2 = UserUtils.prepareUserOrganization(u);
        userOrganization2.setPosition("tester");
        userOrganization2.setOrganization(testOrganization);



        userOrganizationDao.create(userOrganization1);
        assertFalse("UserOrganization1 was not created", userOrganization1.getId() == 0);

        userOrganizationDao.create(userOrganization2);
        assertFalse("UserOrganization1 was not created", userOrganization2.getId() == 0);

        u.setDefaultUserOrganization(userOrganization1);
        userDao.update(u);

        boolean out = userOrganizationDao.isUserOrganizationPreffered(userOrganization2.getUuid());
        assertFalse("1. neni preferovane ale vratilo true", out);

        out = userOrganizationDao.isUserOrganizationPreffered(userOrganization1.getUuid());
        assertTrue("2. je preferovane ale vratilo false", out);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testLoadPrivateUserOrganization() throws NoRecordException {

        User u = UserUtils.createUser();
        u.setFirstName("f");
        u.setLastName("d");
        u.setEmail("eeee");
        userDao.create(u);

        UserOrganization userOrganization1 = UserUtils.prepareUserOrganization(u);
        userOrganization1.setPosition("tester");

        u.setPrivateUserOrganization(userOrganization1);
        userDao.update(u);

        assertTrue(u.getId() != 0);

        UserOrganization privateUO = userOrganizationDao.loadPrivateUserOrganizationForUser(u.getUuid());

        assertEquals(privateUO, userOrganization1);

    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testLoadDefaultUserOrganization() throws NoRecordException {

        User u = UserUtils.createUser();
        u.setFirstName("f");
        u.setLastName("d");
        u.setEmail("eeee");
        userDao.create(u);

        UserOrganization userOrganization1 = UserUtils.prepareUserOrganization(u);
        userOrganization1.setPosition("tester");
        userOrganizationDao.create(userOrganization1);

        u.setDefaultUserOrganization(userOrganization1);
        userDao.update(u);
        assertTrue(u.getId() != 0);

        UserOrganization privateUO = userOrganizationDao.loadDefaultUserOrganizationForUser(u.getUuid());

        assertEquals("User uuid " + u.getUuid(), privateUO, userOrganization1);

        UserOrganization privateUO2 = userOrganizationDao.loadDefaultUserOrganizationForUser(u.getId());

        assertEquals("User uuid " + u.getUuid(), privateUO2, userOrganization1);

        String uoUuid = userOrganizationDao.loadUuidById(privateUO.getId());

        assertEquals(privateUO.getUuid(), uoUuid);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testIsOwnerUserOrganization() {

        User u1 = UserUtils.createUser();
        u1.setFirstName("f");
        u1.setLastName("d");
        u1.setEmail("eeee");
        userDao.create(u1);
        assertTrue(u1.getId() != 0);

        User u2 = UserUtils.createUser();
        u2.setFirstName("f");
        u2.setLastName("d");
        u2.setEmail("eeee");
        userDao.create(u2);
        assertTrue(u2.getId() != 0);

        testCreateOrganization();

        UserOrganization userOrganization1 = UserUtils.prepareUserOrganization(u1);
        userOrganization1.setPosition("tester");
        userOrganization1.setOrganization(testOrganization);


        UserOrganization userOrganization2 = UserUtils.prepareUserOrganization(u2);
        userOrganization2.setPosition("tester");
        userOrganization2.setOrganization(testOrganization);



        userOrganizationDao.create(userOrganization1);
        assertFalse("UserOrganization1 was not created", userOrganization1.getId() == 0);

        userOrganizationDao.create(userOrganization2);
        assertFalse("UserOrganization1 was not created", userOrganization2.getId() == 0);


        boolean out = userOrganizationDao.isUserOwnerOfUserOrganization(u1.getUuid(), userOrganization1.getUuid());
        assertTrue("1. je vlastnik ale vratilo false", out);

        out = userOrganizationDao.isUserOwnerOfUserOrganization(u1.getUuid(), userOrganization2.getUuid());
        assertFalse("2. neni vlastnik ale vratilo true", out);
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testDaoNotNull"})
    public void testListUserOrganizationsByUuids() {

        User u1 = UserUtils.createUser();
        u1.setFirstName("f");
        u1.setLastName("d");
        u1.setEmail("eeee");
        userDao.create(u1);
        assertTrue(u1.getId() != 0);

        User u2 = UserUtils.createUser();
        u2.setFirstName("f");
        u2.setLastName("d");
        u2.setEmail("eeee");
        userDao.create(u2);
        assertTrue(u2.getId() != 0);

        testCreateOrganization();

        UserOrganization userOrganization1 = UserUtils.prepareUserOrganization(u1);
        userOrganization1.setPosition("tester");
        userOrganization1.setOrganization(testOrganization);

        UserOrganization userOrganization11 = UserUtils.prepareUserOrganization(u1);
        userOrganization11.setPosition("developer");
        userOrganization11.setOrganization(testOrganization);


        UserOrganization userOrganization2 = UserUtils.prepareUserOrganization(u2);
        userOrganization2.setPosition("tester");
        userOrganization2.setOrganization(testOrganization);
        UserOrganization userOrganization22 = UserUtils.prepareUserOrganization(u2);
        userOrganization22.setPosition("developer");
        userOrganization22.setOrganization(testOrganization);
        userOrganizationDao.create(userOrganization1);
        userOrganizationDao.create(userOrganization11);
        userOrganizationDao.create(userOrganization2);
        userOrganizationDao.create(userOrganization22);

        u1.setDefaultUserOrganization(userOrganization1);
        userDao.update(u1);
        u2.setDefaultUserOrganization(userOrganization2);
        userDao.update(u2);

        List<UserOrganization> userOrganizations = userOrganizationDao.listUserOrganizationForUserUuids(Lists.newArrayList(u1.getUuid(),u2.getUuid()));
        assertTrue(userOrganizations.size() == 2);
        assertTrue(userOrganizations.get(0).getPosition().equalsIgnoreCase("tester"));

    }
}
