package consys.admin.user.core.dao;

import consys.admin.user.core.AbstractUserAdminTest;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.OrganizationTypeEnum;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.util.OrganizationUtils;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.*;

/**
 *
 * @author pepa
 */

public class OrganizationDaoTest extends AbstractUserAdminTest {

    // testovací organizace
    private final String fNameOk = "acemcee a.s.";
    private final String fWebOk = "www.acemcee.com";
    // organizationDao
    private OrganizationDao organizationDao;    
    private UserDao userDao;
    // pro vytvořenou organizaci
    private Organization testOrganization = null;
    private OrganizationTypeEnum testOrganizationType = null;

    @Autowired(required = true)
    public void setOrganizationDao(OrganizationDao organizationDao) {
        this.organizationDao = organizationDao;
    }    

    @Autowired(required = true)
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }



    @Test(groups = {"dao"})
    public void testDaoNotNull() {
        assertNotNull(organizationDao);
        assertNotNull(userDao);        
    }

    

    @Test(groups = {"dao"})
    public void testCreateOrganization() {
        User u = UserUtils.createUser();
        u.setFirstName("Joza");
        u.setLastName("Misa");
        u.setEmail("Misa@joza.sk");
        
        userDao.create(u);

        
        Organization organization = OrganizationUtils.createOrganization(fNameOk, fNameOk, fWebOk);
        organization.setOrganizationType(OrganizationTypeEnum.ALTERNATIVE_DISPUTE_RESOLUTION);
        organization.setUserCreated(u);

        organizationDao.create(organization);
        assertFalse("Organization was not created", organization.getId() == 0);
        this.testOrganization = organization;
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateOrganization"})
    public void testLoadOrganizationByName() throws NoRecordException {
        testCreateOrganization();
        Organization o = organizationDao.loadByName(fNameOk);

        assertNotNull(o);
        assertTrue(o.equals(testOrganization));
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateOrganization"})
    public void testLoadOrganizationByUuid() throws NoRecordException {
        testCreateOrganization();
        Organization o = organizationDao.loadByUuid(testOrganization.getUuid());
        assertNotNull(o);
        assertTrue(o.equals(testOrganization));
    }

    @Test(groups = {"dao"}, dependsOnMethods = {"testCreateOrganization"})
    public void testLoadOrganizationByPrefix() throws NoRecordException {
        testCreateOrganization();
        List<Organization> o = organizationDao.listByPrefix("ace", 10);
        assertTrue("Nenasla sa", o.size() > 0);
       
    }
}
