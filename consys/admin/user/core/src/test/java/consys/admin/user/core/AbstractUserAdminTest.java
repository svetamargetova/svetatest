/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.user.core;

import consys.admin.common.test.AbstractAdminDaoTestCase;
import org.springframework.test.context.ContextConfiguration;

/**
 *
 * @author palo
 */
@ContextConfiguration(locations = {"user-spring.xml","classpath:consys/admin/common/core/admin-common-spring.xml"})
public abstract class AbstractUserAdminTest extends AbstractAdminDaoTestCase {

}
