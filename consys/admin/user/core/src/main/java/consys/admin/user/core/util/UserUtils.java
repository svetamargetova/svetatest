package consys.admin.user.core.util;



import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.bo.UserOrganization;
import consys.common.utils.Crypto;
import consys.common.utils.date.DateProvider;
import consys.common.utils.UuidProvider;
import java.util.Random;


/**
 *
 * @author pepa
 */
public final class UserUtils {

    
    private static final String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";   

    /** vygeneruje náhodné 20-místné heslo */
    public static String generatePassword() {
        StringBuilder result = new StringBuilder();
        Random random = new Random();
        int length = chars.length();
        for (int i = 0; i < 20; i++) {
            result.append(chars.charAt(random.nextInt(length)));
        }
        return result.toString();
    }

    /** vytvoří objekt uživatele podle zadaných parametrů */
    public static synchronized User createUser() {
        User user = new User();
        user.setUuid(UuidProvider.getUuid());
        return user;
    }

    public static synchronized String hashPassword(String plain){
        return Crypto.md5(plain);
    }

    /** nastaví proměnné při vytváření nového uživatele, je potřeba doplit pouze jméno, email a heslo */
    public static synchronized UserLoginInfo prepareNewUser(String email, String password) {        
        User user = new User();
        user.setUuid(UuidProvider.getUuid());
        user.setEmail(email);
        
        UserLoginInfo loginInfo = new UserLoginInfo();
        loginInfo.setUser(user);        
        loginInfo.setDeleted(false);
        loginInfo.setActive(false);
        loginInfo.setPassword(hashPassword(password));
        loginInfo.setRegisterDate(DateProvider.getCurrentDate());
        return loginInfo;
    }

    
    public static synchronized UserOrganization prepareUserOrganization(User u) {
        UserOrganization uo = new UserOrganization();
        uo.setDateFrom(DateProvider.getCurrentDate());
        uo.setUuid(UuidProvider.getUuid());
        uo.setUser(u);
        uo.setActual(false);
        return uo;
    }
}
