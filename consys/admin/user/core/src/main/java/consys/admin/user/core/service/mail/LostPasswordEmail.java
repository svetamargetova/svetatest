package consys.admin.user.core.service.mail;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 *
 * @author Palo
 */
public class LostPasswordEmail extends AbstractSystemNotification{

    private UserLoginInfo info;

    public LostPasswordEmail(UserLoginInfo info) {
        super(SystemMessage.ACCOUNT_LOST_PASSWORD, info.getUser().getUuid());
        this.info = info;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        placeHolders.addPlaceHolder("token", info.getLostPasswordKey());
    }
}
