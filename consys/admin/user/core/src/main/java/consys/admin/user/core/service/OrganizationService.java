/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.user.core.service;

import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.OrganizationTypeEnum;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface OrganizationService {

    List<OrganizationTypeEnum> listOrganizationsTypes();

    List<Organization> listByPrefix(String prefix,int limit);

    Organization loadByUuid(String uuid) throws NoRecordException ;

    OrganizationTypeEnum loadOrganizationTypeById(Integer id)  throws NoRecordException;
    
    


}
