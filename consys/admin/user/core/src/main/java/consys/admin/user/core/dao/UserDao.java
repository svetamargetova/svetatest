package consys.admin.user.core.dao;

import consys.common.core.dao.GenericDao;
import consys.admin.user.core.bo.User;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface UserDao extends GenericDao<User> {
    
    public User loadByUuid(String uuid) throws NoRecordException;

    public User loadDetailedByUuid(String uuid) throws NoRecordException;

    public List<User> listUsersByUuids(List<String> uuids) throws NoRecordException;

    public String loadUserUuidByUsername(String email) throws NoRecordException;
    
    public String loadEmailByUuid(String uuid) throws NoRecordException;

    public String loadProfileImagePrefix(String uuid);
}
