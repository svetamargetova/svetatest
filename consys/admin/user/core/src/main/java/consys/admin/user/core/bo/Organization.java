package consys.admin.user.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.UsStateEnum;

/**
 *
 * @author pepa
 */
public class Organization implements ConsysObject {

    private static final long serialVersionUID = -7168765960809278414L;
    /** id organizace */
    private Long id;
    /** uuid */
    private String uuid;
    /** web */
    private String web;
    /** název organizace */
    private String name;
    /** mezinárodní název organizace */
    private String internacionalName;
    /** zkratka */
    private String acronym;
    /* adresa */
    private String street;
    private String city;
    private String zip;
    private CountryEnum country;
    private UsStateEnum usState;
    private User userCreated;
    /** typ organizace */
    private OrganizationTypeEnum organizationType;

    /** zkratka */
    public String getAcronym() {
        return acronym;
    }

    /** zkratka */
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    /** id organizace */
    public Long getId() {
        return id;
    }

    /** id organizace */
    public void setId(Long id) {
        this.id = id;
    }

    /** název organizace */
    public String getName() {
        return name;
    }

    /** název organizace */
    public void setName(String name) {
        this.name = name;
    }

    /** typ organizace */
    public OrganizationTypeEnum getOrganizationType() {
        return organizationType;
    }

    /** typ organizace */
    public void setOrganizationType(OrganizationTypeEnum organizationType) {
        this.organizationType = organizationType;
    }

    @Override
    public String toString() {
        return "Organization[name=" + name + " web=" + web + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        Organization e = (Organization) obj;
        return e.getUuid().equalsIgnoreCase(uuid);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the internacionalName
     */
    public String getInternacionalName() {
        return internacionalName;
    }

    /**
     * @param internacionalName the internacionalName to set
     */
    public void setInternacionalName(String internacionalName) {
        this.internacionalName = internacionalName;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the state
     */
    public CountryEnum getCountry() {
        return country;
    }

    /**
     * @param state the state to set
     */
    public void setCountry(CountryEnum state) {
        this.country = state;
    }

    /**
     * @return the uSState
     */
    public UsStateEnum getUsState() {
        return usState;
    }

    /**
     * @param uSState the uSState to set
     */
    public void setUsState(UsStateEnum uSState) {
        this.usState = uSState;
    }

    /**
     * @return the userCreated
     */
    public User getUserCreated() {
        return userCreated;
    }

    /**
     * @param userCreated the userCreated to set
     */
    public void setUserCreated(User userCreated) {
        this.userCreated = userCreated;
    }
}
