package consys.admin.user.core.service.mail;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 *
 * @author pepa
 */
public class UserApiResetPasswordEmail extends AbstractSystemNotification {

    private UserLoginInfo info;
    
    public UserApiResetPasswordEmail(UserLoginInfo info) {
        super(SystemMessage.API_USER_PASSWORD_RESET, info.getUser().getUuid());
        this.info = info;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        placeHolders.addPlaceHolder("token", info.getLostPasswordKey());
    }
}
