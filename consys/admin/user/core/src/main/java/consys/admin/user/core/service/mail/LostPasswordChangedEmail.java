package consys.admin.user.core.service.mail;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 *
 * @author Palo
 */
public class LostPasswordChangedEmail extends AbstractSystemNotification {
    

    public LostPasswordChangedEmail(UserLoginInfo info) {
        super(SystemMessage.ACCOUNT_LOST_PASSWORD_CHANGE, info.getUser().getUuid());        
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
    }
}
