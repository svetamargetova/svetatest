package consys.admin.user.core.service.impl;

import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.dao.OrganizationDao;
import consys.admin.user.core.dao.UserOrganizationDao;
import consys.admin.user.core.service.UserOrganizationService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.impl.AbstractService;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Palo
 */
public class UserOrganizationServiceImpl extends AbstractService implements UserOrganizationService {

    private UserOrganizationDao userOrganizationDao;
    private OrganizationDao organizationDao;


    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    @Override
    public void createUserOrganization(UserOrganization uo, boolean preferred) 
            throws RequiredPropertyNullException {
        if (uo == null || uo.getUser() == null) {
            throw new RequiredPropertyNullException();
        }
        // test ci je organizacia vytvorena
        User user = uo.getUser();

        if (uo.getOrganization().getUserCreated().equals(user)) {
            processOrganization(uo.getOrganization());
        }

        log().info("Creating " + uo);
        userOrganizationDao.create(uo);

        // test ci sa nahodu nestala organizacia preferovanu
        if (preferred) {
            uo.getUser().setDefaultUserOrganization(uo);
            log().info("Preferred " + uo);
            userOrganizationDao.update(uo);
        }
    }

    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateUserOrganization(UserOrganization uo, boolean preferred) throws RequiredPropertyNullException {
        if (uo == null || uo.getUser() == null) {
            throw new RequiredPropertyNullException();
        }
        // test ci je organizacia vytvorena
        User user = uo.getUser();
        if (uo.getOrganization() != null && uo.getOrganization().getUserCreated().equals(user)) {
            processOrganization(uo.getOrganization());
        }

        log().info("Updating " + uo);
        userOrganizationDao.update(uo);

        // test ci sa nahodu nestala organizacia preferovanu
        if (preferred) {
            uo.getUser().setDefaultUserOrganization(uo);
            log().info("Preferred " + uo);
            userOrganizationDao.update(uo);
        }
    }

    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    @Override
    public UserOrganization loadDefaultUserOrganizationByUserUuid(String uuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return userOrganizationDao.loadDefaultUserOrganizationWithUserByUserUuid(uuid);
    }

    @Override
    public UserOrganization loadDefaultUserOrganizationByUserId(Long userId)
            throws NoRecordException, RequiredPropertyNullException {
        if (userId == null || userId <= 0) {
            throw new RequiredPropertyNullException();
        }
        return userOrganizationDao.loadDefaultUserOrganizationWithUserByUserId(userId);
    }

    @Override
    public UserOrganization loadByUuid(String uuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return getUserOrganizationDao().loadUserOrganizationByUuid(uuid);
    }

    @Override
    public UserOrganization loadPrivateUserOrg(String userUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return getUserOrganizationDao().loadPrivateUserOrganizationForUser(userUuid);
    }

    @Override
    public boolean isUserOrganizationPrefered(String uuid) {
        return getUserOrganizationDao().isUserOrganizationPreffered(uuid);
    }

    @Override
    public boolean isUserOwnerOfUserOrganization(String userUuid, String userOrganizationUuid) {
        return userOrganizationDao.isUserOwnerOfUserOrganization(userUuid, userOrganizationUuid);
    }

    /*------------------------------------------------------------------------*/
    /* ----  L I S T  ---- */
    /*------------------------------------------------------------------------*/
    @Override
    public List<UserOrganization> listUserOrganizations(String userUuid) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return getUserOrganizationDao().listByUserUuid(userUuid);
    }

    @Override
    public List<UserOrganization> listUserOrganizationForUserUuids(List<String> userUuids) throws RequiredPropertyNullException {
        if (userUuids == null || userUuids.isEmpty()) {
            throw new RequiredPropertyNullException();
        }
        return userOrganizationDao.listUserOrganizationForUserUuids(userUuids);
    }

    /*------------------------------------------------------------------------*/
    /* ----  D E L E T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * TODO: treba overit ze:
     * - user maze len svoje UO
     * - mazane UO nie je v ziadnom evente , ale jak ked je to v inem modulu? hm
     */
    @Override
    public void deleteUserOrganization(String loggedUserUuid, String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(loggedUserUuid) || StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        log().debug("Delete user organization");
        getUserOrganizationDao().deleteUserOrganization(uuid);
    }

    /**
     * Ak je user vlastnik organizacie tak sa spracovava aj organizacia
     */
    private void processOrganization(Organization o) {
        if (o.getId() == null) {
            log().info("Creating " + o);
            getOrganizationDao().create(o);
        } else {
            log().info("Updating " + o);
            getOrganizationDao().update(o);
        }
    }

    /*========================================================================*/
    /**
     * @return the userOrganizationDao
     */
    public UserOrganizationDao getUserOrganizationDao() {
        return userOrganizationDao;
    }

    /**
     * @param userOrganizationDao the userOrganizationDao to set
     */
    public void setUserOrganizationDao(UserOrganizationDao userOrganizationDao) {
        this.userOrganizationDao = userOrganizationDao;
    }

    /**
     * @return the organizationDao
     */
    public OrganizationDao getOrganizationDao() {
        return organizationDao;
    }

    /**
     * @param organizationDao the organizationDao to set
     */
    public void setOrganizationDao(OrganizationDao organizationDao) {
        this.organizationDao = organizationDao;
    }
}
