/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.user.core.util;

import consys.admin.user.core.bo.User;
import net.notx.ws.user.Contact;
import net.notx.ws.user.CreateOrUpdateUserRequest;
import net.notx.ws.user.UserWebService;

/**
 *
 * @author palo
 */
public class UserNotxUtils {

    public static CreateOrUpdateUserRequest createRequest(User user) {
        
        net.notx.ws.user.User notxUser = UserWebService.OBJECT_FACTORY.createUser();
        notxUser.setLang("en");
        notxUser.setUserId(user.getUuid());
        notxUser.setName(user.getFullName());
        Contact email = UserWebService.OBJECT_FACTORY.createContact();
        email.setKey("email");
        email.setValue(user.getEmailOrMergeEmail());
        notxUser.getContacts().add(email);
        
        
        CreateOrUpdateUserRequest request = UserWebService.OBJECT_FACTORY.createCreateOrUpdateUserRequest();
        request.setUser(notxUser);                        
        return request;
    }
}
