package consys.admin.user.core.util;

import consys.admin.user.core.bo.Organization;
import consys.common.utils.UuidProvider;

/**
 *
 * @author pepa
 */
public final class OrganizationUtils {

    public static Organization createOrganization() {
        Organization organization = new Organization();
        organization.setUuid(UuidProvider.getUuid());
        return organization;
    }

    /** vytvoří objekt organizace podle zadaných parametrů */
    public static Organization createOrganization(String name, String universal, String web) {
        Organization organization = new Organization();

        organization.setUuid(UuidProvider.getUuid());
        organization.setName(name);
        organization.setInternacionalName(universal);
        organization.setWeb(web);

        return organization;
    }
}
