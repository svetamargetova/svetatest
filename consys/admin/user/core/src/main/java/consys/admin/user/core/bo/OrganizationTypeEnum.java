package consys.admin.user.core.bo;



import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import java.util.ResourceBundle;



/**
 *
 * @author pepa
 */
public enum OrganizationTypeEnum  {
    
    ACCOUNTING(1,"accounting"),
    AIRLINES_AVIATION(2,"airlines_aviation"),
    ALTERNATIVE_DISPUTE_RESOLUTION(3,"alternative_dispute_resolution"),
    ALTERNATIVE_MEDICINE(4,"alternative_medicine"),
    ANIMATION(5,"animation"),
    APPAREL_FASHION(6,"apparel_fashion"),
    ARCHITECTURE_PLANNING(7,"architecture_planning"),
    ARTS_AND_CRAFTS(8,"arts_and_crafts"),
    AUTOMOTIVE(9,"automotive"),
    AVIATION_AEROSPACE(10,"aviation_aerospace"),
    BANKING(11,"banking"),
    BIOTECHNOLOGY(12,"biotechnology"),
    BROADCAST_MEDIA(13,"broadcast_media"),
    BUILDING_MATERIALS(14,"building_materials"),
    BUSINESS_SUPPLIES_AND_EQUIPMENT(15,"business_supplies_and_equipment"),
    CAPITAL_MARKETS(16,"capital_markets"),
    CHEMICALS(17,"chemicals"),
    CIVIC_SOCIAL_ORGANIZATION(18,"civic_social_organization"),
    CIVIL_ENGINEERING(19,"civil_engineering"),
    COMMERCIAL_REAL_ESTATE(20,"commercial_real_estate"),
    COMPUTER_NETWORK_SECURITY(21,"computer_network_security"),
    COMPUTER_GAMES(22,"computer_games"),
    COMPUTER_HARDWARE(23,"computer_hardware"),
    COMPUTER_NETWORKING(24,"computer_networking"),
    COMPUTER_SOFTWARE(25,"computer_software"),
    CONSTRUCTION(26,"construction"),
    CONSUMER_ELECTRONICS(27,"consumer_electronics"),
    CONSUMER_GOODS(28,"consumer_goods"),
    CONSUMER_SERVICES(29,"consumer_services"),
    COSMETICS(30,"cosmetics"),
    DAIRY(31,"dairy"),
    DEFENSE_SPACE(32,"defense_space"),
    DESIGN(33,"design"),
    EDUCATION_MANAGEMENT(34,"education_management"),
    E_LEARNING(35,"e_learning"),
    ELECTRICAL_ELECTRONIC_MANUFACTURING(36,"electrical_electronic_manufacturing"),
    ENTERTAINMENT(37,"entertainment"),
    ENVIRONMENTAL_SERVICES(38,"environmental_services"),
    EVENTS_SERVICES(39,"events_services"),
    EXECUTIVE_OFFICE(40,"executive_office"),
    FACILITIES_SERVICES(41,"facilities_services"),
    FARMING(42,"farming"),
    FINANCIAL_SERVICES(43,"financial_services"),
    FINE_ART(44,"fine_art"),
    FISHERY(45,"fishery"),
    FOOD_BEVERAGES(46,"food_beverages"),
    FOOD_PRODUCTION(47,"food_production"),
    FUND_RAISING(48,"fund_raising"),
    FURNITURE(49,"furniture"),
    GAMBLING_CASINOS(50,"gambling_casinos"),
    GLASS_CERAMICS_CONCRETE(51,"glass_ceramics_concrete"),
    GOVERNMENT_ADMINISTRATION(52,"government_administration"),
    GOVERNMENT_RELATIONS(53,"government_relations"),
    GRAPHIC_DESIGN(54,"graphic_design"),
    HEALTH_WELLNESS_AND_FITNESS(55,"health_wellness_and_fitness"),
    HIGHER_EDUCATION(56,"higher_education"),
    HOSPITAL_HEALTH_CARE(57,"hospital_health_care"),
    HOSPITALITY(58,"hospitality"),
    HUMAN_RESOURCES(59,"human_resources"),
    IMPORT_AND_EXPORT(60,"import_and_export"),
    INDIVIDUAL_FAMILY_SERVICES(61,"individual_family_services"),
    INDUSTRIAL_AUTOMATION(62,"industrial_automation"),
    INFORMATION_SERVICES(63,"information_services"),
    INFORMATION_TECHNOLOGY_AND_SERVICES(64,"information_technology_and_services"),
    INSURANCE(65,"insurance"),
    INTERNATIONAL_AFFAIRS(66,"international_affairs"),
    INTERNATIONAL_TRADE_AND_DEVELOPMENT(67,"international_trade_and_development"),
    INTERNET(68,"internet"),
    INVESTMENT_BANKING(69,"investment_banking"),
    INVESTMENT_MANAGEMENT(70,"investment_management"),
    JUDICIARY(71,"judiciary"),
    LAW_ENFORCEMENT(72,"law_enforcement"),
    LAW_PRACTICE(73,"law_practice"),
    LEGAL_SERVICES(74,"legal_services"),
    LEGISLATIVE_OFFICE(75,"legislative_office"),
    LEISURE_TRAVEL_TOURISM(76,"leisure_travel_tourism"),
    LIBRARIES(77,"libraries"),
    LOGISTICS_AND_SUPPLY_CHAIN(78,"logistics_and_supply_chain"),
    LUXURY_GOODS_JEWELRY(79,"luxury_goods_jewelry"),
    MACHINERY(80,"machinery"),
    MANAGEMENT_CONSULTING(81,"management_consulting"),
    MARITIME(82,"maritime"),
    MARKETING_AND_ADVERTISING(83,"marketing_and_advertising"),
    MARKET_RESEARCH(84,"market_research"),
    MECHANICAL_OR_INDUSTRIAL_ENGINEERING(85,"mechanical_or_industrial_engineering"),
    MEDIA_PRODUCTION(86,"media_production"),
    MEDICAL_DEVICES(87,"medical_devices"),
    MEDICAL_PRACTICE(88,"medical_practice"),
    MENTAL_HEALTH_CARE(89,"mental_health_care"),
    MILITARY(90,"military"),
    MINING_METALS(91,"mining_metals"),
    MOTION_PICTURES_AND_FILM(92,"motion_pictures_and_film"),
    MUSEUMS_AND_INSTITUTIONS(93,"museums_and_institutions"),
    MUSIC(94,"music"),
    NANOTECHNOLOGY(95,"nanotechnology"),
    NEWSPAPERS(96,"newspapers"),
    NON_PROFIT_ORGANIZATION_MANAGEMENT(97,"non_profit_organization_management"),
    OIL_ENERGY(98,"oil_energy"),
    ONLINE_MEDIA(99,"online_media"),
    OTHER(100,"other"),
    OUTSOURCING_OFFSHORING(101,"outsourcing_offshoring"),
    PACKAGE_FREIGHT_DELIVERY(102,"package_freight_delivery"),
    PACKAGING_AND_CONTAINERS(103,"packaging_and_containers"),
    PAPER_FOREST_PRODUCTS(104,"paper_forest_products"),
    PERFORMING_ARTS(105,"performing_arts"),
    PHARMACEUTICALS(106,"pharmaceuticals"),
    PHILANTHROPY(107,"philanthropy"),
    PHOTOGRAPHY(108,"photography"),
    PLASTICS(109,"plastics"),
    POLITICAL_ORGANIZATION(110,"political_organization"),
    PRIMARY_SECONDARY_EDUCATION(111,"primary_secondary_education"),
    PRINTING(112,"printing"),
    PROFESSIONAL_TRAINING_COACHING(113,"professional_training_coaching"),
    PROGRAM_DEVELOPMENT(114,"program_development"),
    PUBLIC_POLICY(115,"public_policy"),
    PUBLIC_RELATIONS_AND_COMMUNICATIONS(116,"public_relations_and_communications"),
    PUBLIC_SAFETY(117,"public_safety"),
    PUBLISHING(118,"publishing"),
    RAILROAD_MANUFACTURE(119,"railroad_manufacture"),
    RANCHING(120,"ranching"),
    REAL_ESTATE(121,"real_estate"),
    RECREATIONAL_FACILITIES_AND_SERVICES(122,"recreational_facilities_and_services"),
    RELIGIOUS_INSTITUTIONS(123,"religious_institutions"),
    RENEWABLES_ENVIRONMENT(124,"renewables_environment"),
    RESEARCH(125,"research"),
    RESTAURANTS(126,"restaurants"),
    RETAIL(127,"retail"),
    SECURITY_AND_INVESTIGATIONS(128,"security_and_investigations"),
    SEMICONDUCTORS(129,"semiconductors"),
    SHIPBUILDING(130,"shipbuilding"),
    SPORTING_GOODS(131,"sporting_goods"),
    SPORTS(132,"sports"),
    STAFFING_AND_RECRUITING(133,"staffing_and_recruiting"),
    SUPERMARKETS(134,"supermarkets"),
    TELECOMMUNICATIONS(135,"telecommunications"),
    TEXTILES(136,"textiles"),
    THINK_TANKS(137,"think_tanks"),
    TOBACCO(138,"tobacco"),
    TRANSLATION_AND_LOCALIZATION(139,"translation_and_localization"),
    TRANSPORTATION_TRUCKING_RAILROAD(140,"transportation_trucking_railroad"),
    UTILITIES(141,"utilities"),
    VENTURE_CAPITAL_PRIVATE_EQUITY(142,"venture_capital_private_equity"),
    VETERINARY(143,"veterinary"),
    WAREHOUSING(144,"warehousing"),
    WHOLESALE(145,"wholesale"),
    WINE_AND_SPIRITS(146,"wine_and_spirits"),
    WIRELESS(147,"wireless"),
    WRITING_AND_EDITING(148,"writing_and_editing");
    
        
    int id;
    String code;

    private static final String DEFAULT_PATH = "OrganizationTypes";
    private static final ResourceBundle NAMES = ResourceBundle.getBundle(DEFAULT_PATH);

    private static final ImmutableMap<String,String> names;

    static{
        // nainicializujeme na zacatku do pamata
        Builder<String,String> builder = ImmutableMap.builder();                
        OrganizationTypeEnum[] vals = values();
        for (int i = 0; i < vals.length; i++) {                                    
            builder.put(vals[i].code, NAMES.getString(vals[i].code+".name"));
        }
        names = builder.build();
    }

    OrganizationTypeEnum(int id, String code) {
        this.id = id;
        this.code = code;
    }


    public Integer getId(){
        return id;
    }

    private static final int MAX_ID = 148;
    public static OrganizationTypeEnum fromId(Integer id){
        if(id == null || id < 1){
            return null;
        }

        if(id > MAX_ID){
            return ACCOUNTING;
        }

        return OrganizationTypeEnum.values()[id-1];
    }

    
    // nacita lokalizovane meno z defualtneho anglickeho prop.
    public String getName(){        
        return names.get(code);
    }

    public String getCode(){
         return code;
    }

    
}
