package consys.admin.user.core.bo;


import consys.common.core.bo.ConsysObject;
import java.util.Date;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Palo
 */
public class UserLoginInfo implements ConsysObject {

    private static final long serialVersionUID = 6603193898395170296L;
    
    private Long id;
    /** hash hesla do systému */
    private String password;
    /** je uživatel aktivní? */
    private boolean active = false;
    /** byl uživatel smazán? */
    private boolean deleted = false;
    /** hash nově vygenerovaného hesla */
    private String lostPasswordKey;
    /* Uzivatel priradeni k loginu */
    private User user;    
    /* Datum registracie */
    private Date registerDate;
    /** id TWITTER */
    private Long twitterId;
    /** id LinkedIn */
    private String linkedInId;
    /** id Facebook */
    private Long facebookId;
    /** id Google */
    private String googleId;

     /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

  
    @Override
    public String toString() {
        return String.format("UserLoginInfo[id=%s email=%s registred=%s]",getId(),getUser().getEmailOrMergeEmail(),getRegisterDate());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        UserLoginInfo e = (UserLoginInfo) obj;
        return e.getUser().getEmailOrMergeEmail().equalsIgnoreCase(user.getEmailOrMergeEmail());

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.user != null ? this.user.hashCode() : 0);
        return hash;
    }

   
    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the registerDate
     */
    public Date getRegisterDate() {
        return registerDate;
    }

    /**
     * @param registerDate the registerDate to set
     */
    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    /**
     * @return the lostPasswordKey
     */
    public String getLostPasswordKey() {
        return lostPasswordKey;
    }

    /**
     * @param lostPasswordKey the lostPasswordKey to set
     */
    public void setLostPasswordKey(String lostPasswordKey) {
        this.lostPasswordKey = lostPasswordKey;
    }

    /**
     * @return the twitterId
     */
    public Long getTwitterId() {
        return twitterId;
    }

    /**
     * @param twitterId the twitterId to set
     */
    public void setTwitterId(Long twitterId) {
        this.twitterId = twitterId;
    }

    /**
     * @return the linkedInId
     */
    public String getLinkedInId() {
        return linkedInId;
    }

    /**
     * @param linkedInId the linkedInId to set
     */
    public void setLinkedInId(String linkedInId) {
        this.linkedInId = linkedInId;
    }

    /**
     * @return the facebookId
     */
    public Long getFacebookId() {
        return facebookId;
    }

    /**
     * @param facebookId the facebookId to set
     */
    public void setFacebookId(Long facebookId) {
        this.facebookId = facebookId;
    }

    /**
     * id Google
     * @return the googleId
     */
    public String getGoogleId() {
        return googleId;
    }

    /**
     * id Google
     * @param googleId the googleId to set
     */
    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public boolean isSsoUser(){
        return StringUtils.isNotBlank(googleId) || StringUtils.isNotBlank(linkedInId) || twitterId != null || facebookId != null;
    }
   
}
