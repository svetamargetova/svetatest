/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.user.core.service;

import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.core.service.exception.InvitationException;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import java.util.List;

/**
 *
 * @author palo
 */
public interface UserService {

    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Aktivuje uzivatela podla kluca a zaregistruje sa do Messaging sluzby.
     *
     * @throws ActivationKeyException ak kluc neexistuje
     *
     * @throws InvitationException ak je aktivacia invitation=true, teda je nutne
     * aby uzivatel prevyplnil registraciu
     *
     * @throws RequiredPropertyNullException ak je key == null
     *
     * @throws ServiceExecutionFailed ak sa nepodarilo zaregistrovat uzivatela do
     * messaging sluzby
     *
     */
    public void activateUser(String key)
            throws
            ServiceExecutionFailed,
            RequiredPropertyNullException,
            ActivationKeyException,
            InvitationException;

    /**
     * Aktivuje pozvaneho uzivatela podla kluca. Zaroven sa mu nastavi heslo a
     * dalsie udaje ktore predvyplnil u poregistracie
     *
     * @throws ActivationKeyException ak kluc neexistuje
     *
     * @throws RequiredPropertyNullException ak je nejaka property blan
     *
     * @throws ServiceExecutionFailed ak sa nepodarilo zaregistrovat uzivatela do
     * messaging sluzby
     *
     */
    public User activateInvitedUser(String firstName, String lastName, String password, String key)
            throws
            RequiredPropertyNullException,
            ServiceExecutionFailed,
            ActivationKeyException;

    /**
     * Vytvori uzivaela na zaklade standardnej registracie do eventu. Aktivuje
     * uzivatela a posle email o registracii.
     *
     * @param uli login info s prepripravenym uzivatelom
     */
    public void createRegistredUser(UserLoginInfo uli)
            throws RequiredPropertyNullException, UsernameExistsException;
    /**
     * Vytvori zaregistrovaneho uzivatela a na zaklade priznakov vytvori aktivaciu
     * a posle email.
     * @param uli
     * @param withActivativation
     * @param sendRegistrationEmail
     * @throws RequiredPropertyNullException
     * @throws UsernameExistsException 
     */
    public void createRegistredUser(UserLoginInfo uli, boolean withActivativation, boolean registerAtNotx)
            throws RequiredPropertyNullException, UsernameExistsException;

    /**
     * Vytvori zaregistrovaneho uzivatela a na zaklade priznakov vytvori aktivaciu
     * a posle email. Ak nie je profilePictureUrl prazdne vytvori aj obrazok
     * 
     * @param uli
     * @param withActivativation
     * @param sendRegistrationEmail
     * @param profilePictureUrl URL na obrazok 
     * @throws RequiredPropertyNullException
     * @throws UsernameExistsException 
     */
    public void createRegistredUser(UserLoginInfo uli, boolean withActivativation, boolean registerAtNotx, String profilePictureUrl)
            throws RequiredPropertyNullException, UsernameExistsException;

    /**
     * Vytvori zakladnu strukturu pre uzivatela s defualtnym privantym profilom.
     * @param user predvyplneni uzivatel.
     */
    public void createUser(User user)
            throws RequiredPropertyNullException, UsernameExistsException;


    /**
     * Vytvori a posle novy aktivacny email uzivatelovi podla vlozeneho <code>userUuid</code>
     *
     * @param userUuid identifikacia uzivatela
     * @throws NoRecordException ak uzivatel neexistuje alebo je uz aktivaovany
     * @throws RequiredPropertyNullException ak chyba paramater
     */
    public void generateActivationEmail(String userUuid)
            throws NoRecordException,RequiredPropertyNullException;


    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    public User loadUserByUuid(String uuid)
            throws RequiredPropertyNullException, NoRecordException;

    public User loadDetailedUserByUuid(String uuid)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException;

    public User loadInvitedActivation(String token)
            throws
            RequiredPropertyNullException,
            NoRecordException,
            ActivationKeyException;

    /**
     * Nacita uzivatelove UUID podla jeho mena
     * @param username uzivatelkse meno
     * @return uuid uzivatela
     * @throws RequiredPropertyNullException ak chyba meno
     * @throws NoRecordException ak uzivatel neexistuje
     */
    public String loadUserUuidByUsername(String username) 
            throws RequiredPropertyNullException,NoRecordException;

    /**
     * Nacita uzivatelksy kontaktny email
     * NOTICE: nahradi NotX
     * @param uuid
     * @return
     * @throws RequiredPropertyNullException
     * @throws NoRecordException
     */
    public String loadUserEmailByUuid(String uuid)
            throws RequiredPropertyNullException,NoRecordException;

    /**
     * Nacita prefix profiloveho obrazku uzivatela
     * @param userUuid
     * @return prefix 
     */
    public String loadUserProfileImagePrefix(String userUuid) 
            throws RequiredPropertyNullException,NoRecordException;

    /*------------------------------------------------------------------------*/
    /* ----  L I S T ---- */
    /*------------------------------------------------------------------------*/
    public List<User> listUsersForUuids(List<String> uuids)
            throws RequiredPropertyNullException, NoRecordException;

    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    public void updateUser(User u)
            throws RequiredPropertyNullException, ServiceExecutionFailed;

    /*------------------------------------------------------------------------*/
    /* ---- D E L E T E  ---- */
    /*------------------------------------------------------------------------*/
    public void deleteUser(User user);
    
    /** zmerguje nastavi ktery ucet patri k jiz existujicimu */
    public void updateMergeLoginInfo(String toMergeUser, User user);
}
