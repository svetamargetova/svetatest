package consys.admin.user.core.dao;


import consys.common.core.dao.GenericDao;
import consys.admin.user.core.bo.Organization;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author pepa 
 */
public interface OrganizationDao extends GenericDao<Organization> {
    
    /**
     * nacte organizaci podle prefixu
     * @param prefix oznacuje string kterym bude nazev organizace zacinat
     */
    public List<Organization> listByPrefix(String prefix, int limit);

    /**
     * nacte organizaci podle nazvu
     * @param name je nazev organizace
     */
    public Organization loadByName(String name) throws NoRecordException;

    /**
     * nacte organizaci podle nazvu
     * @param name je nazev organizace
     */
    public Organization loadByUuid(String uuid) throws NoRecordException;
}
