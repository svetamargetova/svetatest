package consys.admin.user.core.service.impl;

import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.dao.UserActivationDao;
import consys.admin.user.core.dao.UserLoginInfoDao;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.mail.LostPasswordChangedEmail;
import consys.admin.user.core.service.mail.LostPasswordEmail;
import consys.admin.user.core.service.mail.UserApiResetPasswordEmail;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.UuidProvider;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserLoginInfoServiceImpl extends AbstractService implements UserLoginInfoService {

    private UserActivationDao userActivationDao;
    private UserService userService;
    private UserLoginInfoDao userLoginInfoDao;


    /*
     * ------------------------------------------------------------------------     
     * ---- C R E A T E ----
     * ------------------------------------------------------------------------
     */
    @Override
    public void createLostPassword(String username)
            throws RequiredPropertyNullException, NoRecordException, EmailException {
        if (StringUtils.isBlank(username)) {
            throw new RequiredPropertyNullException();
        }
        // vytvorime key hash
        log().info("Send Lost Password request from " + username);
        UserLoginInfo i = null;
        try {
            i = userLoginInfoDao.loadByUsername(username);
        } catch (NoRecordException e) {
            log().error("No user with username " + username);
            throw e;
        }

        i.setLostPasswordKey(UuidProvider.getUuid());
        userLoginInfoDao.update(i);

        LostPasswordEmail lpe = new LostPasswordEmail(i);
        lpe.sendNotification();
    }

    @Override
    public void createApiResetPassword(String username) throws RequiredPropertyNullException, NoRecordException, EmailException {
        if (StringUtils.isBlank(username)) {
            throw new RequiredPropertyNullException();
        }
        // vytvorime key hash
        log().info("API Reset Password request from " + username);
        UserLoginInfo i = null;
        try {
            i = userLoginInfoDao.loadByUsername(username);
        } catch (NoRecordException e) {
            log().error("No user with username " + username);
            throw e;
        }

        i.setLostPasswordKey(UuidProvider.getUuid());
        userLoginInfoDao.update(i);

        UserApiResetPasswordEmail email = new UserApiResetPasswordEmail(i);
        email.sendNotification();
    }

    @Override
    public void createUserLoginInfo(UserLoginInfo userLoginInfo)
            throws RequiredPropertyNullException {
        if (userLoginInfo == null || userLoginInfo.getUser() == null || userLoginInfo.getUser().getId() == null) {
            throw new RequiredPropertyNullException();
        }
        if (log().isDebugEnabled()) {
            log().debug("Creating " + userLoginInfo);
        }
        userLoginInfoDao.create(userLoginInfo);
    }

    @Override
    public void createUserActivation(UserActivation userActivation)
            throws RequiredPropertyNullException {
        if (userActivation == null || userActivation.getLoginInfo() == null || StringUtils.isBlank(userActivation.getKey())) {
            throw new RequiredPropertyNullException();
        }
        userActivationDao.create(userActivation);

    }

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ---- L O A D ----
     */
    /*
     * ------------------------------------------------------------------------
     */
    @Override
    public UserLoginInfo loadUserLoginInfo(String username) throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(username)) {
            throw new RequiredPropertyNullException();
        }
        return userLoginInfoDao.loadByUsername(username);
    }

    @Override
    public UserLoginInfo loadUserLoginInfoByLostToken(String token) throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(token)) {
            throw new RequiredPropertyNullException();
        }
        return userLoginInfoDao.loadByLostToken(token);
    }

    @Override
    public UserLoginInfo loadUserLoginInfoWithUserByUuid(String userUuid)
            throws RequiredPropertyNullException, NoRecordException {

        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return userLoginInfoDao.loadByUserUuid(userUuid);
    }

    @Override
    public UserLoginInfo loadUserLoginInfoByTwitterId(Long twitterId)
            throws RequiredPropertyNullException, NoRecordException {
        if (twitterId == null || twitterId <= 0L) {
            throw new RequiredPropertyNullException();
        }
        return userLoginInfoDao.loadByTwitterId(twitterId);
    }

    @Override
    public UserLoginInfo loadUserLoginInfoByFacebookIdOrEmail(Long facebookId, String email)
            throws RequiredPropertyNullException, NoRecordException {
        if (facebookId == null || facebookId <= 0L) {
            throw new RequiredPropertyNullException();
        }
        return userLoginInfoDao.loadByFacebookIdOrEmail(facebookId, email);
    }

    @Override
    public UserLoginInfo loadUserLoginInfoByLinkedInId(String linkedInId)
            throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(linkedInId)) {
            throw new RequiredPropertyNullException();
        }
        return userLoginInfoDao.loadByLinkedInId(linkedInId);
    }

    @Override
    public UserLoginInfo loadUserLoginInfoByGoogleId(String googleId, String email)
            throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(googleId)) {
            throw new RequiredPropertyNullException();
        }
        return userLoginInfoDao.loadByGoogleId(googleId, email);
    }

    @Override
    public int loadAllUserLoginInfoCount() {
        return userLoginInfoDao.loadAllUserLoginInfoCount();
    }

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ---- U P D A T E ----
     */
    /*
     * ------------------------------------------------------------------------
     */
    @Override
    public void updatePassword(String password, String userUuid) throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(password) || StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }

        UserLoginInfo uli = userLoginInfoDao.loadByUserUuid(userUuid);
        log().debug("Updating password: {}", uli);
        uli.setPassword(UserUtils.hashPassword(password));
        userLoginInfoDao.update(uli);

    }

    @Override
    public void updateLostPassword(String password, UserLoginInfo uli) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(password) || uli == null || uli.getUser() == null) {
            throw new RequiredPropertyNullException();
        }

        log().debug("Updating lost password: {}", uli);

        uli.setLostPasswordKey(null);
        uli.setPassword(UserUtils.hashPassword(password));
        userLoginInfoDao.update(uli);
        // send mail
        LostPasswordChangedEmail email = new LostPasswordChangedEmail(uli);
        email.sendNotification();

    }
    
    @Override
    public void updateLostPasswordWithoutSendMail(String password, UserLoginInfo uli) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(password) || uli == null || uli.getUser() == null) {
            throw new RequiredPropertyNullException();
        }

        log().debug("Updating lost password: {}", uli);

        uli.setLostPasswordKey(null);
        uli.setPassword(UserUtils.hashPassword(password));
        userLoginInfoDao.update(uli);
    }

    @Override
    public void updateUserLoginInfo(UserLoginInfo info) throws RequiredPropertyNullException {
        if (info == null || StringUtils.isBlank(info.getPassword())) {
            throw new RequiredPropertyNullException();
        }
        userLoginInfoDao.update(info);
    }

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ---- L I S T ----
     */
    /*
     * ------------------------------------------------------------------------
     */
    @Override
    public List<UserActivation> listOlderActivationsThan(Date date) {
        return userActivationDao.listOlderActivationsThan(date);
    }

    @Override
    public List<UserLoginInfo> listDetailedUserLoginInfo(int firstItem, int itemsPerPage)
            throws RequiredPropertyNullException, NoRecordException {
        if (firstItem < 0 || itemsPerPage < 0) {
            throw new RequiredPropertyNullException("First item or items per page is < 0");
        }
        return userLoginInfoDao.listDetailedUserLoginInfo(firstItem, itemsPerPage);
    }

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ---- D E L E T E ----
     */
    /*
     * ------------------------------------------------------------------------
     */
    /**
     * Odstrani zaregistrovaneho pouzivatela. Posle mu email o tejto
     * skutocnosti.
     *
     * @param loginInfo
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    @Override
    public void deleteNotActivetedUser(UserActivation userActivation)
            throws RequiredPropertyNullException {
        if (userActivation == null || userActivation.getLoginInfo() == null) {
            throw new RequiredPropertyNullException();
        }
        if (log().isInfoEnabled()) {
            log().info(" ... DELETE USER ACTIVATION" + userActivation.getLoginInfo());
        }
        // delete activation
        UserLoginInfo loginInfo = userActivation.getLoginInfo();
        User user = loginInfo.getUser();
        userActivationDao.delete(userActivation);
        // delete login info
        userLoginInfoDao.delete(loginInfo);
        // delete user
        userService.deleteUser(user);
    }

    /*
     * ========================================================================
     */
    public void setUserLoginInfoDao(UserLoginInfoDao userLoginInfoDao) {
        this.userLoginInfoDao = userLoginInfoDao;
    }

    public UserLoginInfoDao getUserLoginInfoDao() {
        return userLoginInfoDao;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserActivationDao(UserActivationDao userActivationDao) {
        this.userActivationDao = userActivationDao;
    }

    public UserActivationDao getUserActivationDao() {
        return userActivationDao;
    }
}
