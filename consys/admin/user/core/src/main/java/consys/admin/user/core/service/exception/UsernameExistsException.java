package consys.admin.user.core.service.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class UsernameExistsException extends ConsysException{
    private static final long serialVersionUID = 2351863164961261012L;

}
