package consys.admin.user.core.dao;

import consys.common.core.dao.GenericDao;
import consys.admin.user.core.bo.UserOrganization;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface UserOrganizationDao extends GenericDao<UserOrganization> {

    /** 
     * Nacte seznam organizaci podle uzivatelova UUID
     */
    public List<UserOrganization> listByUserUuid(String uuid);

    public UserOrganization loadUserOrganizationByUuid(String uuid) throws NoRecordException;

    public UserOrganization loadDefaultUserOrganizationWithUserByUserUuid(String uuid) throws NoRecordException;

    public UserOrganization loadDefaultUserOrganizationWithUserByUserId(Long id) throws NoRecordException;

    /** nacte UserOrganization do osobnich informaci */
    public UserOrganization loadPrivateUserOrganizationForUser(String uuid) throws NoRecordException;

    public UserOrganization loadDefaultUserOrganizationForUser(String uuid) throws NoRecordException;

    public UserOrganization loadDefaultUserOrganizationForUser(Long id) throws NoRecordException;

    public String loadUuidById(Long id);

    public void deleteUserOrganization(String uuid) throws NoRecordException;

    public boolean isUserOrganizationPreffered(String uuid);

    public boolean isUserOwnerOfUserOrganization(String userUuid,String userOrganizationUuid);

    /**
     * Zoznam UserORganizations ktore maju uzivatelia podla uuid nastavene ako default.
     */
    public List<UserOrganization> listUserOrganizationForUserUuids(List<String> userUuids);
}
