package consys.admin.user.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.dao.OrganizationDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class OrganizationDaoImpl extends GenericDaoImpl<Organization> implements OrganizationDao {

    @Override
    public Organization load(Long id) throws NoRecordException {
        return load(id, Organization.class);
    }

    @Override
    public List<Organization> listByPrefix(String prefix, int limit) {

        Query q = session().createQuery("from Organization o where lower(o.name) like :prefix order by o.name");
        q.setString("prefix", prefix.toLowerCase() + "%");
        q.setMaxResults(limit);
        List<Organization> list = q.list();
        return list;
    }

    @Override
    public Organization loadByName(String name) throws NoRecordException {
        Query q = session().createQuery("from Organization o where o.name=:name");
        q.setString("name", name);
        Organization organization = (Organization) q.uniqueResult();
        if (organization == null) {
            throw new NoRecordException("Organization with name " + name + "not exists!");
        }
        return organization;
    }

    @Override
    public Organization loadByUuid(String uuid) throws NoRecordException {
        Query q = session().createQuery("select o from Organization o JOIN FETCH o.userCreated where o.uuid=:UUID");
        q.setString("UUID", uuid);
        Organization organization = (Organization) q.uniqueResult();
        if (organization == null) {
            throw new NoRecordException("Organization with uuid " + uuid + "not exists!");
        }
        return organization;

    }
}
