/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.user.core;

/**
 *
 * @author palo
 */
public interface UserSystemProperties {

    /* LOST PASSWORD nastavuje sa do emailu kluc $key$ a oslovenie $salutation$ */
    public static final String LOST_PASS_EMAIL_TEXT     = "user_lost_pass_email_text";
    public static final String LOST_PASS_EMAIL_TITLE    = "user_lost_pass_email_title";

    /* LOST PASSWORD CHANGED nastavuje sa do emalu oslovenie $salutation$ */
    public static final String LOST_PASS_CHANGED_EMAIL_TEXT     = "user_lost_pass_email_changed_text";
    public static final String LOST_PASS_CHANGED_EMAIL_TITLE    = "user_lost_pass_email_changed_title";

    /* ACTIVATION nastavuje sa do emailu oslovenie $salutation$ */
    public static final String ACTIVATION_EMAIL_TEXT    = "user_activation_email_text";
    public static final String ACTIVATION_EMAIL_TITLE   = "user_activation_email_title";

    /* REGISTRATION nastavuje sa do emailu oslovenie $salutation$ a aktivacny kluc $activation_key$*/
    public static final String REG_EMAIL_TEXT = "user_registration_email_text";
    public static final String REG_EMAIL_TITLE ="user_registration_email_title";

    
    /* ACTIVATION REQUEST je sa do emailu oslovenie $salutation$ a aktivacny kluc $activation_key$ a $registration_date$*/
    public static final String REP_ACT_EMAIL_TEXT = "user_activation_request_email_text";
    public static final String REP_ACT_EMAIL_TITLE ="user_activation_request_email_title";
    public static final String REP_ACT_EMAIL_DATE_INTERVAL ="user_activation_request_email_date_interval";
    public static final String REP_ACT_EMAIL_MAX_DAYS ="user_activation_request_email_max_days";

   




}
