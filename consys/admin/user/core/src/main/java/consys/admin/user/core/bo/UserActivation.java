package consys.admin.user.core.bo;


import consys.common.core.bo.ConsysObject;

/**
 *
 * @author pepa
 */
public class UserActivation implements ConsysObject {
    private static final long serialVersionUID = 5553244699440921977L;
    public static final int ACTIVATE_KEY_LENGTH = 36;

    /** db id */
    private Long id;        
    /** aktivační klíč */
    private String key;
    /** uživatel k aktivování */
    private UserLoginInfo loginInfo;
    /** jedna sa o invitation */
    private boolean invitation = false;

    /** db id */
    public Long getId() {
        return id;
    }

    /** db id */
    public void setId(Long id) {
        this.id = id;
    }

    /** aktivační klíč */
    public String getKey() {
        return key;
    }

    /** aktivační klíč */
    public void setKey(String key) {
        this.key = key;
    }
    

    @Override
    public String toString() {
        return "Activation["+key+"]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        UserActivation e = (UserActivation) obj;
        return e.getKey().equalsIgnoreCase(key);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.key != null ? this.key.hashCode() : 0);
        return hash;
    }

    /**
     * @return the loginInfo
     */
    public UserLoginInfo getLoginInfo() {
        return loginInfo;
    }

    /**
     * @param loginInfo the loginInfo to set
     */
    public void setLoginInfo(UserLoginInfo loginInfo) {
        this.loginInfo = loginInfo;
    }

    /**
     * @return the invitation
     */
    public boolean isInvitation() {
        return invitation;
    }

    /**
     * @param invitation the invitation to set
     */
    public void setInvitation(boolean invitation) {
        this.invitation = invitation;
    }
}
