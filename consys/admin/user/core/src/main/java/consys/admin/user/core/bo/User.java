package consys.admin.user.core.bo;

import consys.admin.common.core.solr.Indexed;
import consys.admin.user.core.search.UserIndexHandler;
import consys.common.core.bo.ConsysObject;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author pepa
 */
@Indexed(handler = UserIndexHandler.class)
public class User implements ConsysObject {

    private static final long serialVersionUID = -5577779231026967483L;
    /** id uživatele */
    private Long id;
    /** uuid uživatele */
    private String uuid;
    /* Prihalsovaci email*/
    private String email;
    /** jméno uživatele */
    private int gender;
    /** jméno uživatele */
    private String firstName;
    /** prostredni jmeno */
    private String middleName;
    /** příjmení uživatele */
    private String lastName;
    /** uid portretu uzivatele v prevem panelu */
    private String portraitImageUuidPrefix;
    /** tituly pred jmenem */
    private String frontDegree;
    /** tituly za jmenem */
    private String rearDegree;
    /** tituly za jmenem */
    private String bio;
    /** Defaultni Organizace */
    private UserOrganization defaultUserOrganization;
    /** Privatni organizace */
    private UserOrganization privateUserOrganization;
    /** Celiak */
    private boolean celiac;
    /** Vegetarian */
    private boolean vegetarian;
    private User mergeWith;

    public Gender getGenderType() {
        return Gender.fromInt(gender);
    }

    public void setGenderType(Gender g) {
        setGender(Gender.toInt(g));
    }

    /**
     * @return the gender
     */
    public int getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    /** uuid uživatele */
    public String getUuid() {
        return uuid;
    }

    /** uuid uživatele */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /** jméno uživatele */
    public String getFirstName() {
        return firstName;
    }

    /** jméno uživatele */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /** tituly pred jmenem */
    public String getFrontDegree() {
        return frontDegree;
    }

    /** tituly pred jmenem */
    public void setFrontDegree(String frontDegree) {
        this.frontDegree = frontDegree;
    }

    /** id uživatele */
    public Long getId() {
        return id;
    }

    /** id uživatele */
    public void setId(Long id) {
        this.id = id;
    }

    /** příjmení uživatele */
    public String getLastName() {
        return lastName;
    }

    /** příjmení uživatele */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /** prostredni jmeno */
    public String getMiddleName() {
        return middleName;
    }

    /** prostredni jmeno */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /** tituly za jmenem */
    public String getRearDegree() {
        return rearDegree;
    }

    /** tituly za jmenem */
    public void setRearDegree(String rearDegree) {
        this.rearDegree = rearDegree;
    }

    @Override
    public String toString() {
        return "User[id=" + id + " email=" + email + " name=" + getName() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        User e = (User) obj;
        return e.getUuid().equalsIgnoreCase(uuid);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    /** 
     * vraci plne jmeno uzivatele i s tituly 
     */
    public String getFullName() {
        StringBuilder builder = new StringBuilder();

        if (!StringUtils.isEmpty(frontDegree)) {
            builder.append(frontDegree).append(" ");
        }
        builder.append(firstName).append(" ");

        if (!StringUtils.isEmpty(middleName)) {
            builder.append(middleName);
            builder.append(" ");
        }
        builder.append(lastName);
        if (!StringUtils.isEmpty(rearDegree)) {
            builder.append(", ").append(rearDegree);
        }
        return builder.toString();
    }

    /** vraci plne jmeno uzivatele BEZ titulu */
    public String getName() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(firstName).append(" ");
        if (!StringUtils.isEmpty(middleName)) {
            buffer.append(middleName);
            buffer.append(" ");
        }
        buffer.append(lastName);
        return buffer.toString();
    }

    /**
     * @return the defaultUserOrganization
     */
    public UserOrganization getDefaultUserOrganization() {
        return defaultUserOrganization;
    }

    /**
     * @param defaultUserOrganization the defaultUserOrganization to set
     */
    public void setDefaultUserOrganization(UserOrganization defaultUserOrganization) {
        this.defaultUserOrganization = defaultUserOrganization;
    }

    /**
     * @return the privateUserOrganization
     */
    public UserOrganization getPrivateUserOrganization() {
        return privateUserOrganization;
    }

    /**
     * @param privateUserOrganization the privateUserOrganization to set
     */
    public void setPrivateUserOrganization(UserOrganization privateUserOrganization) {
        this.privateUserOrganization = privateUserOrganization;
    }

    /** vraci email uzivatele, v pripade, ze jde o zmergovany ucet, vraci email mergovaneho uctu */
    public String getEmailOrMergeEmail() {
        if (StringUtils.isNotBlank(email) && email.contains("@")) {
            // v poradku ucet ma vyplneny email
            return email;
        } else {
            // mail neni prirazeny, zkusime nacist z nadrazeneho uctu, ktery ho ma, pokud existuje
            if (mergeWith != null) {
                return mergeWith.getEmailOrMergeEmail();
            } else {
                return email;
            }
        }
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the portraitImageUuidPrefix
     */
    public String getPortraitImageUuidPrefix() {
        return portraitImageUuidPrefix;
    }

    /**
     * @param portraitImageUuidPrefix the portraitImageUuidPrefix to set
     */
    public void setPortraitImageUuidPrefix(String portraitImageUuidPrefix) {
        this.portraitImageUuidPrefix = portraitImageUuidPrefix;
    }

    /**
     * @return the bio
     */
    public String getBio() {
        return bio;
    }

    /**
     * @param bio the bio to set
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     * Celiak
     * @return the celiac
     */
    public boolean isCeliac() {
        return celiac;
    }

    /**
     * Celiak
     * @param celiac the celiac to set
     */
    public void setCeliac(boolean celiac) {
        this.celiac = celiac;
    }

    /**
     * Vegetarian
     * @return the vegetarian
     */
    public boolean isVegetarian() {
        return vegetarian;
    }

    /**
     * Vegetarian
     * @param vegetarian the vegetarian to set
     */
    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public User getMergeWith() {
        return mergeWith;
    }

    public void setMergeWith(User mergeWith) {
        this.mergeWith = mergeWith;
    }

    public enum Gender {

        NOT_SPECIFIED(0),
        MAN(1),
        WOMAN(2);
        private int g;

        private Gender(int g) {
            this.g = g;
        }

        public static Gender fromInt(int t) {
            switch (t) {
                case 1:
                    return MAN;
                case 2:
                    return WOMAN;
            }
            return NOT_SPECIFIED;
        }

        public static int toInt(Gender g) {
            switch (g) {
                case MAN:
                    return 1;
                case WOMAN:
                    return 2;
            }
            return 0;
        }
    };
}
