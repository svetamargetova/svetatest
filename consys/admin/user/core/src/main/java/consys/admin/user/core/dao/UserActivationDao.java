package consys.admin.user.core.dao;

import consys.common.core.dao.GenericDao;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.exception.NoRecordException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface UserActivationDao extends GenericDao<UserActivation> {

    public UserActivation loadByUserLoginInfo(UserLoginInfo info);

    public UserActivation loadWithUserByUserUuid(String userUuid) throws NoRecordException;
    
    public UserActivation loadWithUserByActivationKey(String key) throws NoRecordException;

    public void deleteByActivationKey(String key);

    public List<UserActivation> listOlderActivationsThan(Date date);

}
