package consys.admin.user.core.service.mail;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 * Notifikacia o uspesne aktivovanom emailovom ucte
 * @author Palo
 */
public class ActivationEmail extends AbstractSystemNotification {


    public ActivationEmail(UserLoginInfo info) {
        super(SystemMessage.NEW_ACCOUNT_ACTIVATION, info.getUser().getUuid());        
    }
    

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
    }
}
