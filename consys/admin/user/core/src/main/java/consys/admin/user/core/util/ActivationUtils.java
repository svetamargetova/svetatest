package consys.admin.user.core.util;


import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.utils.UuidProvider;




/**
 *
 * @author pepa
 */
public final class ActivationUtils {       

    /** vytvori aktivaci bez prirazeneho uzivatele */
    public static synchronized UserActivation createActivation(UserLoginInfo loginInfo) {
        UserActivation activation = new UserActivation();
        activation.setKey(UuidProvider.getUuid());        
        activation.setLoginInfo(loginInfo);
        return activation;
    }
}
