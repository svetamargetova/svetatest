package consys.admin.user.core.service;

import consys.admin.user.core.bo.UserOrganization;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface UserOrganizationService {

    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    public void createUserOrganization(UserOrganization uo, boolean preferred) 
            throws RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    public void updateUserOrganization(UserOrganization uo, boolean preferred) 
            throws RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    public UserOrganization loadByUuid(String uuid) 
            throws NoRecordException,RequiredPropertyNullException;

    public UserOrganization loadDefaultUserOrganizationByUserUuid(String uuid) 
            throws NoRecordException,RequiredPropertyNullException;
    
    public UserOrganization loadDefaultUserOrganizationByUserId(Long userId)
            throws NoRecordException,RequiredPropertyNullException;

    /** nacte uzivatelovu privatni organizaci */
    public UserOrganization loadPrivateUserOrg(String userUuid) 
            throws NoRecordException,RequiredPropertyNullException;

    public boolean isUserOrganizationPrefered(String uuid);

    public boolean isUserOwnerOfUserOrganization(String userUuid, String userOrganizationUuid);

    /*------------------------------------------------------------------------*/
    /* ----  L I S T  ---- */
    /*------------------------------------------------------------------------*/
    public List<UserOrganization> listUserOrganizations(String userUuid)
            throws RequiredPropertyNullException;

    public List<UserOrganization> listUserOrganizationForUserUuids(List<String> userUuids)
            throws RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  D E L E T E  ---- */
    /*------------------------------------------------------------------------*/
    public void deleteUserOrganization(String uoUuid, String userUuid) 
            throws NoRecordException,RequiredPropertyNullException;
}
