package consys.admin.user.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.dao.UserLoginInfoDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class UserLoginInfoDaoImpl extends GenericDaoImpl<UserLoginInfo> implements UserLoginInfoDao {

    @Override
    public UserLoginInfo load(Long id) throws NoRecordException {
        return load(id, UserLoginInfo.class);
    }

    @Override
    public UserLoginInfo loadByUsername(String username) throws NoRecordException {
        Query q = session().createQuery("from UserLoginInfo l join fetch l.user where lower(l.user.email)=:uusername");
        q.setString("uusername", username.toLowerCase());
        UserLoginInfo l = (UserLoginInfo) q.uniqueResult();
        if (l == null) {
            throw new NoRecordException("UserLoginInfo for username " + username + " not found!");
        }
        return l;
    }

    @Override
    public UserLoginInfo loadByUserUuid(String uuid) throws NoRecordException {
        Query q = session().createQuery("from UserLoginInfo l join fetch l.user where l.user.uuid=:UUID");
        q.setString("UUID", uuid);
        UserLoginInfo l = (UserLoginInfo) q.uniqueResult();
        if (l == null) {
            throw new NoRecordException("UserLoginInfo for user uuid " + uuid + " not found!");
        }
        return l;
    }

    @Override
    public UserLoginInfo loadByLostToken(String token) throws NoRecordException {
        Query q = session().createQuery("from UserLoginInfo l where l.lostPasswordKey=:TOKEN");
        q.setString("TOKEN", token);
        UserLoginInfo l = (UserLoginInfo) q.uniqueResult();
        if (l == null) {
            throw new NoRecordException("UserLoginInfo for lost password token " + token + " not found!");
        }
        return l;
    }

    @Override
    public List<UserLoginInfo> listDetailedUserLoginInfo(int firstItem, int itemsPerPage) throws NoRecordException {
        Query q = session().createQuery("select l from UserLoginInfo l join fetch l.user as u join fetch u.defaultUserOrganization as def left join fetch def.organization");
        q.setFirstResult(firstItem);
        q.setMaxResults(itemsPerPage);
        List<UserLoginInfo> l = q.list();
        if (l.isEmpty()) {
            throw new NoRecordException("No  items");
        }
        return l;
    }

    @Override
    public int loadAllUserLoginInfoCount() {
        return ((Long) session().createQuery("select count(*) from UserLoginInfo l ").uniqueResult()).intValue();
    }

    @Override
    public UserLoginInfo loadByTwitterId(Long twitterId) throws NoRecordException {
        Query q = session().createQuery("from UserLoginInfo l join fetch l.user where l.twitterId=:ID");
        q.setLong("ID", twitterId);
        UserLoginInfo l = (UserLoginInfo) q.uniqueResult();
        if (l == null) {
            throw new NoRecordException();
        }
        return l;
    }

    @Override
    public UserLoginInfo loadByFacebookIdOrEmail(Long facebookId, String email) throws NoRecordException {
        Query q = session().createQuery("from UserLoginInfo l join fetch l.user where l.facebookId=:ID or lower(l.user.email)=:EMAIL");
        q.setLong("ID", facebookId);
        q.setString("EMAIL", email.toLowerCase());
        UserLoginInfo l = (UserLoginInfo) q.uniqueResult();
        if (l == null) {
            throw new NoRecordException();
        }
        return l;
    }

    @Override
    public UserLoginInfo loadByLinkedInId(String linkedInId) throws NoRecordException {
        Query q = session().createQuery("from UserLoginInfo l join fetch l.user where l.linkedInId=:ID");
        q.setString("ID", linkedInId);
        UserLoginInfo l = (UserLoginInfo) q.uniqueResult();
        if (l == null) {
            throw new NoRecordException();
        }
        return l;
    }

    @Override
    public UserLoginInfo loadByGoogleId(String googleId, String email) throws NoRecordException {
        Query q = session().createQuery("from UserLoginInfo l join fetch l.user where l.googleId=:ID or lower(l.user.email)=:EMAIL");
        q.setString("ID", googleId);
        q.setString("EMAIL", email.toLowerCase());
        UserLoginInfo l = (UserLoginInfo) q.uniqueResult();
        if (l == null) {
            throw new NoRecordException();
        }
        return l;
    }
}
