package consys.admin.user.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.dao.UserActivationDao;
import consys.common.core.exception.NoRecordException;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class UserActivationDaoImpl extends GenericDaoImpl<UserActivation> implements UserActivationDao {

    @Override
    public UserActivation load(Long id) throws NoRecordException {
        return load(id, UserActivation.class);
    }

    @Override
    public UserActivation loadWithUserByActivationKey(String key) throws NoRecordException {
        Query q = session().createQuery("select ua from UserActivation ua JOIN FETCH ua.loginInfo as lf JOIN FETCH lf.user where ua.key=:key");
        q.setString("key", key);
        UserActivation ua = (UserActivation) q.uniqueResult();
        if (ua == null) {
            throw new NoRecordException("UserActivation with key " + key + " not exists!");
        }
        return ua;
    }

    @Override
    public UserActivation loadWithUserByUserUuid(String userUuid) throws NoRecordException{

        UserActivation ua = (UserActivation) session().
                createQuery("select ua from UserActivation ua JOIN FETCH ua.loginInfo as lf JOIN FETCH lf.user as u where u.uuid=:UUID").
                setString("UUID", userUuid).
                uniqueResult();
        if (ua == null) {
            throw new NoRecordException("UserActivation for user with uuid " + userUuid + " not exists!");
        }
        return ua;
    }

    @Override
    public void deleteByActivationKey(String key) {
        int i = session().
                createQuery("delete from UserActivation ua where ua.key=:key").
                setString("key", key).executeUpdate();
        if (i != 1) {
            throw new RuntimeException("Key not exists!");
        }
    }

    @Override
    public UserActivation loadByUserLoginInfo(UserLoginInfo info) {
        return (UserActivation) session().createQuery("from UserActivation u where u.loginInfo=:loginInfo").setEntity("loginInfo", info).uniqueResult();

    }

    @Override
    public List<UserActivation> listOlderActivationsThan(Date date) {
        Query q = session().createQuery("from UserActivation ua join fetch ua.loginInfo as lf join fetch lf.user where lf.registerDate <= :OLDER");
        q.setDate("OLDER", date);
        return q.list();
    }


}
