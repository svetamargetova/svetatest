package consys.admin.user.core.service.mail;

import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RepeatingActivationEmail extends AbstractSystemNotification {
    
    private UserActivation activation;

    public RepeatingActivationEmail(UserLoginInfo info, UserActivation activation) {
        super(SystemMessage.NEW_ACCOUNT_REPEATING_ACTIVATION, info.getUser().getUuid());        
        this.activation = activation;
    }
    
    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        placeHolders.addPlaceHolder("activation_key", activation.getKey());
    }
}
