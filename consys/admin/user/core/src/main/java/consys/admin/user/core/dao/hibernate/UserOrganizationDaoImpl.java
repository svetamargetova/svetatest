package consys.admin.user.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.dao.UserOrganizationDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class UserOrganizationDaoImpl extends GenericDaoImpl<UserOrganization> implements UserOrganizationDao {

    @Override
    public UserOrganization load(Long id) throws NoRecordException {
        return load(id, UserOrganization.class);
    }

    @Override
    public List<UserOrganization> listByUserUuid(String uuid) {
        Query q = session().createQuery("select uo from UserOrganization uo JOIN FETCH uo.organization where uo.user.uuid=:UUID and uo != uo.user.privateUserOrganization order by uo.dateFrom desc");
        q.setString("UUID", uuid);
        return q.list();
    }

    @Override
    public UserOrganization loadUserOrganizationByUuid(String uuid) throws NoRecordException {
        Query q = session().createQuery("from UserOrganization uo where uo.uuid=:UUID");
        q.setString("UUID", uuid);
        UserOrganization uo = (UserOrganization) q.uniqueResult();
        if (uo == null) {
            throw new NoRecordException();
        }
        return uo;
    }

    @Override
    public UserOrganization loadPrivateUserOrganizationForUser(String uuid) throws NoRecordException {
        Query q = session().createQuery("select u.privateUserOrganization from User u where u.uuid=:UUID");
        q.setString("UUID", uuid);
        UserOrganization uo = (UserOrganization) q.uniqueResult();
        if (uo == null) {
            throw new NoRecordException();
        }
        return uo;
    }

    @Override
    public UserOrganization loadDefaultUserOrganizationForUser(Long id) throws NoRecordException {
        Query q = session().createQuery("select u.defaultUserOrganization from User u where u.id=:USER_ID");
        q.setLong("USER_ID", id);
        UserOrganization uo = (UserOrganization) q.uniqueResult();
        if (uo == null) {
            throw new NoRecordException();
        }
        return uo;
    }

    @Override
    public UserOrganization loadDefaultUserOrganizationForUser(String uuid) throws NoRecordException {
        Query q = session().createQuery("select u.defaultUserOrganization from User u where u.uuid=:UUID");
        q.setString("UUID", uuid);
        UserOrganization uo = (UserOrganization) q.uniqueResult();
        if (uo == null) {
            throw new NoRecordException();
        }
        return uo;
    }

    @Override
    public void deleteUserOrganization(String uuid) throws NoRecordException {
        Query q = session().createQuery("delete from UserOrganization o where o.uuid=:UUID");
        q.setString("UUID", uuid);
        if (q.executeUpdate() != 1) {
            throw new NoRecordException("No record with uuid " + uuid);
        }
    }

    @Override
    public boolean isUserOrganizationPreffered(String uuid) {
        Query q = session().createQuery("select o.id from User o where o.defaultUserOrganization.uuid=:UUID");
        q.setString("UUID", uuid);
        Long id = (Long) q.uniqueResult();
        return id == null ? false : true;
    }

    @Override
    public boolean isUserOwnerOfUserOrganization(String userUuid, String userOrganizationUuid) {
        Query q = session().createQuery("select o.id from UserOrganization o where o.uuid=:UUID and o.user.uuid=:UUUID");
        q.setString("UUID", userOrganizationUuid);
        q.setString("UUUID", userUuid);
        Long id = (Long) q.uniqueResult();
        return id == null ? false : true;
    }

    @Override
    public String loadUuidById(Long id) {
        return (String) session().createQuery("select o.uuid from UserOrganization o where o.id=:USERO_ID").
                setLong("USERO_ID", id).
                uniqueResult();
    }

    @Override
    public List<UserOrganization> listUserOrganizationForUserUuids(List<String> userUuids) {
        List<UserOrganization> out = session().createQuery("select uo from User u, UserOrganization uo JOIN FETCH uo.user where u.defaultUserOrganization.id=uo.id and u.uuid IN (:UUIDS)").
                setParameterList("UUIDS", userUuids).
                list();
        return out;
    }

    @Override
    public UserOrganization loadDefaultUserOrganizationWithUserByUserUuid(String uuid) throws NoRecordException {
        Query q = session().createQuery("select uo from User u, UserOrganization uo JOIN FETCH uo.user as user where u.defaultUserOrganization.id=uo.id and u.uuid=:UUID");
        q.setString("UUID", uuid);
        UserOrganization uo = (UserOrganization) q.uniqueResult();
        if (uo == null) {
            throw new NoRecordException();
        }
        return uo;
    }

    @Override
    public UserOrganization loadDefaultUserOrganizationWithUserByUserId(Long id) throws NoRecordException{
        Query q = session().createQuery("select uo from User u, UserOrganization uo JOIN FETCH uo.user as user where u.defaultUserOrganization.id=uo.id and u.id=:UID");
        q.setLong("UID", id);
        UserOrganization uo = (UserOrganization) q.uniqueResult();
        if (uo == null) {
            throw new NoRecordException();
        }
        return uo;
    }
}
