package consys.admin.user.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.UsStateEnum;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class UserOrganization implements ConsysObject {

    private static final long serialVersionUID = -1531400841388076946L;
    private static final String EMPTY_ORGANIZATION = "";
    /** id uzivatele v organizaci */
    private Long id;
    /** uuid uo */
    private String uuid;
    /** organizece, za kterou uživatel vystupuje */
    private Organization organization;
    /** uživatel */
    private User user;
    /** pozice uzivatele v organizaci */
    private String position;
    /** datum od kdy uživatel vystupuje za organizaci */
    private Date dateFrom;
    /** datum do kdy uživatel vystupuje za organizaci */
    private Date dateTo;
    /** Aktualna pozicia */
    private boolean actual = false;

    /* adresa */
    private String street;
    private String city;
    private String zip;
    private CountryEnum state;
    private UsStateEnum uSState;

    /** datum od kdy uživatel vystupuje za organizaci */
    public Date getDateFrom() {
        return dateFrom;
    }

    /** datum od kdy uživatel vystupuje za organizaci */
    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    /** datum do kdy uživatel vystupuje za organizaci */
    public Date getDateTo() {
        return dateTo;
    }

    /** datum do kdy uživatel vystupuje za organizaci */
    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    /** id uzivatele v organizaci */
    public Long getId() {
        return id;
    }

    /** id uzivatele v organizaci */
    public void setId(Long id) {
        this.id = id;
    }

    /** organizace, za kterou uživatel vystupuje */
    public Organization getOrganization() {
        return organization;
    }

    /** organizace, za kterou uživatel vystupuje */
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    /** pozice uzivatele v organizaci */
    public String getPosition() {
        return position;
    }

    /** pozice uzivatele v organizaci */
    public void setPosition(String position) {
        this.position = position;
    }

    /** uživatel */
    public User getUser() {
        return user;
    }

    /** uživatel */
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return String.format("UserOrganization(uuid=%s)", uuid);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        UserOrganization uo = (UserOrganization) obj;
        return uo.getUuid().equals(uuid);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Vygeneruje kratke info o uzivatelovej organizacii vo formate
     * <b> {org_name} ( {from} - {to} ) </b>
     */
    public String getShortInfo() {
        if (organization != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(getDateFrom());
            String yearFrom = String.valueOf(calendar.get(Calendar.YEAR));
            String yearTo = null;
            if (getDateTo() != null) {
                calendar.setTime(getDateFrom());
                yearTo = String.valueOf(calendar.get(Calendar.YEAR));
            } else {
                yearTo = EMPTY_ORGANIZATION;
            }
            String orgName = organization.getName();
            return String.format("%s (%s — %s)", orgName, yearFrom, yearTo);
        } else {
            return EMPTY_ORGANIZATION;
        }




    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the state
     */
    public CountryEnum getCountry() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setCountry(CountryEnum state) {
        this.state = state;
    }

    /**
     * @return the uSState
     */
    public UsStateEnum getUsState() {
        return uSState;
    }

    /**
     * @param uSState the uSState to set
     */
    public void setUsState(UsStateEnum uSState) {
        this.uSState = uSState;
    }

    /**
     * @return the actual
     */
    public boolean isActual() {
        return actual;
    }

    /**
     * @param actual the actual to set
     */
    public void setActual(boolean actual) {
        this.actual = actual;
    }
}
