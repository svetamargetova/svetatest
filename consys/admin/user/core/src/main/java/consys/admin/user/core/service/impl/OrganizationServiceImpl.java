package consys.admin.user.core.service.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.OrganizationTypeEnum;
import consys.admin.user.core.dao.OrganizationDao;
import consys.admin.user.core.service.OrganizationService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.impl.AbstractService;
import java.util.List;

/**
 *
 * @author Palo
 */
public class OrganizationServiceImpl extends AbstractService implements OrganizationService {

    
    private OrganizationDao organizationDao;
    

    public OrganizationDao getOrganizationDao() {
        return organizationDao;
    }


    public void setOrganizationDao(OrganizationDao organizationDao) {
        this.organizationDao = organizationDao;
    }

    @Override
    public List<OrganizationTypeEnum> listOrganizationsTypes() {        
        Builder<OrganizationTypeEnum> builder = ImmutableList.builder();        
        OrganizationTypeEnum[] types = OrganizationTypeEnum.values();
        for (int i = 0; i < types.length; i++) {
            builder.add(types[i]);            
        }
        return builder.build();                
    }

    @Override
    public List<Organization> listByPrefix(String prefix,int limit){
        log().debug("List organization by prefix "+prefix);
        return organizationDao.listByPrefix(prefix, limit);
    }

    @Override
    public Organization loadByUuid(String uuid) throws NoRecordException {
        return organizationDao.loadByUuid(uuid);
    }

    @Override
    public OrganizationTypeEnum loadOrganizationTypeById(Integer id) throws NoRecordException {
        return OrganizationTypeEnum.fromId(id);
    }
}
