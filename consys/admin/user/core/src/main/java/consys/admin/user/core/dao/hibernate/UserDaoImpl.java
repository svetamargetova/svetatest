package consys.admin.user.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.dao.UserDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    @Override
    public User load(Long id) throws NoRecordException {
        //return load(id, User.class);
        Query q = session().
                createQuery("from User u left join fetch u.mergeWith where u.id=:ID").
                setLong("ID", id);
        User u = (User) q.uniqueResult();
        return u;
    }

    @Override
    public User loadByUuid(String uuid) throws NoRecordException {
        Query q = session().
                createQuery("from User u left join fetch u.mergeWith where u.uuid=:UUID").
                setString("UUID", uuid);
        User u = (User) q.uniqueResult();
        if (u == null) {
            throw new NoRecordException("User with UUID " + uuid + " not exists!");
        }
        return u;
    }

    @Override
    public User loadDetailedByUuid(String uuid) throws NoRecordException {
        Query q = session().
                createQuery("select u from User u left join fetch u.mergeWith JOIN FETCH u.defaultUserOrganization as duo LEFT JOIN FETCH duo.organization as duoo where u.uuid=:UUID").
                setString("UUID", uuid);
        User u = (User) q.uniqueResult();
        if (u == null) {
            throw new NoRecordException("User with UUID " + uuid + " not exists!");
        }
        return u;
    }

    @Override
    public String loadUserUuidByUsername(String email) throws NoRecordException {
        Query q = session().
                createQuery("select u.uuid from User u where lower(u.email)=:UNAME").
                setString("UNAME", email.toLowerCase());
        String u = (String) q.uniqueResult();
        if (u == null) {
            throw new NoRecordException("User with username " + email + " not exists!");
        }
        return u;
    }

    @Override
    public List<User> listUsersByUuids(List<String> uuids) throws NoRecordException {
        List<User> users = session().createQuery("from User u left join fetch u.mergeWith where u.uuid in (:UUIDS)").setParameterList("UUIDS", uuids).list();
        if (uuids.size() != users.size()) {
            throw new NoRecordException();
        }
        return users;
    }

    @Override
    public String loadEmailByUuid(String uuid) throws NoRecordException {
        Query q = session().
                createQuery("select u.email from User u where u.uuid=:UUID").
                setString("UUID", uuid);
        String u = (String) q.uniqueResult();
        if (u == null) {
            throw new NoRecordException("User with uuid " + uuid + " not exists!");
        }
        return u;
    }

    @Override
    public String loadProfileImagePrefix(String uuid) {
        return (String) session().
                createQuery("select u.portraitImageUuidPrefix from User u where u.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
    }
}
