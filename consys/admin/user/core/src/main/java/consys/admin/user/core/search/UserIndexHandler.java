package consys.admin.user.core.search;

import consys.admin.common.core.solr.IndexHandler;
import consys.admin.common.core.solr.schema.SolrSchemaProperties;
import consys.admin.user.core.bo.User;
import consys.common.utils.collection.Maps;
import java.util.Map;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserIndexHandler implements IndexHandler<User>, SolrSchemaProperties {

    @Override
    public Map<String, String> createDoc(User item) {
        Map<String, String> map = Maps.newHashMap();
        map.put(UUID, item.getUuid());
        map.put(EMAIL, item.getEmailOrMergeEmail());
        map.put(ORGANIZATION, item.getDefaultUserOrganization() == null ? "" : item.getDefaultUserOrganization().getShortInfo());
        map.put(USER_NAME, item.getName());
        return map;
    }
}
