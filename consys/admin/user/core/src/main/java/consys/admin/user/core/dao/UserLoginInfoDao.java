/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.user.core.dao;

import consys.common.core.dao.GenericDao;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface UserLoginInfoDao extends GenericDao<UserLoginInfo> {

    public UserLoginInfo loadByUsername(String username) throws NoRecordException;

    public UserLoginInfo loadByUserUuid(String uuid) throws NoRecordException;

    public UserLoginInfo loadByLostToken(String token) throws NoRecordException;
    
    public UserLoginInfo loadByTwitterId(Long twitterId) throws NoRecordException;
    
    public UserLoginInfo loadByLinkedInId(String linkedInId) throws NoRecordException;
    
    public UserLoginInfo loadByGoogleId(String googleId, String email) throws NoRecordException;

    public UserLoginInfo loadByFacebookIdOrEmail(Long facebookId, String email) throws NoRecordException;
    
    public List<UserLoginInfo> listDetailedUserLoginInfo(int firstItem, int itemsPerPage) throws NoRecordException;
    
    public int loadAllUserLoginInfoCount();
}
