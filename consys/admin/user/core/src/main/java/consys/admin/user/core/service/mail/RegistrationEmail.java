package consys.admin.user.core.service.mail;

import consys.admin.user.core.UserSystemProperties;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 *
 * @author Palo
 */
public class RegistrationEmail extends AbstractSystemNotification implements UserSystemProperties {
    
    private String key;    

    public RegistrationEmail(String key, UserLoginInfo info) {
        super(SystemMessage.NEW_ACCOUNT_REGISTRATION, info.getUser().getUuid());
        this.key = key;        
    }    

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        placeHolders.addPlaceHolder("activation_key", key);
    }
}
