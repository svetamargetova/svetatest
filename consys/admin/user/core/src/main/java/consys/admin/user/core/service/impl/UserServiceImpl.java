package consys.admin.user.core.service.impl;

import consys.admin.user.core.UserSystemProperties;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.dao.UserActivationDao;
import consys.admin.user.core.dao.UserDao;
import consys.admin.user.core.dao.UserLoginInfoDao;
import consys.admin.user.core.dao.UserOrganizationDao;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.core.service.exception.InvitationException;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.service.mail.ActivationEmail;
import consys.admin.user.core.service.mail.RegistrationEmail;
import consys.admin.user.core.service.mail.RepeatingActivationEmail;
import consys.admin.user.core.util.ActivationUtils;
import consys.admin.user.core.util.UserNotxUtils;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.impl.AbstractService;
import java.util.List;
import net.notx.ws.user.UserWebService;
import org.apache.commons.lang.StringUtils;

/**
 * @author Palo
 */
public class UserServiceImpl extends AbstractService implements UserService, UserSystemProperties {

    private UserDao userDao;
    private UserLoginInfoDao loginInfoDao;
    private UserActivationDao activationDao;
    private UserOrganizationDao userOrganizationDao;
    private UserWebService notxUserWebService;

    /*
     * ------------------------------------------------------------------------
     * ---- C R E A T E ----
     * ------------------------------------------------------------------------
     */
    @Override
    public void createUser(User user)
            throws RequiredPropertyNullException, UsernameExistsException {
        createUser(user, true);
    }

    private void createUser(User user, boolean createAtNotx)
            throws RequiredPropertyNullException, UsernameExistsException {
        if (user == null
                || StringUtils.isBlank(user.getEmailOrMergeEmail())
                || StringUtils.isBlank(user.getFirstName())
                || StringUtils.isBlank(user.getLastName())
                || StringUtils.isBlank(user.getUuid())) {
            throw new RequiredPropertyNullException();
        }

        try {
            getLoginInfoDao().loadByUsername(user.getEmailOrMergeEmail());
            log().error("Username '{}' already used!", user.getEmailOrMergeEmail());
            throw new UsernameExistsException();
        } catch (NoRecordException ex) {
            // vytvorime uzivatela
            getUserDao().create(user);

            // vytvorime defualnu organizaciu
            UserOrganization uodef = UserUtils.prepareUserOrganization(user);
            getUserOrganizationDao().create(uodef);

            // Nastavime dafultnu organizaciu
            user.setDefaultUserOrganization(uodef);
            user.setPrivateUserOrganization(uodef);
            userDao.update(user);
            // zalogujeme
            log().debug("Created: {}", user);
            if (createAtNotx) {
                // Zaregistrujeme uzivatela v notx
                getNotxUserWebService().createOrUpdateUser(UserNotxUtils.createRequest(user));
                log().debug("Created NotX: {}", user);
            }
        }
    }

    @Override
    public void createRegistredUser(UserLoginInfo uli, boolean withActivativation, boolean registerAtNotx)
            throws RequiredPropertyNullException, UsernameExistsException {
        createRegistredUser(uli, withActivativation, registerAtNotx, null);
    }

    @Override
    public void createRegistredUser(UserLoginInfo uli, boolean withActivativation, boolean registerAtNotx, String profilePictureUrl)
            throws RequiredPropertyNullException, UsernameExistsException {
        if (uli == null || uli.getUser() == null
                || StringUtils.isBlank(uli.getPassword())
                || StringUtils.isBlank(uli.getUser().getEmailOrMergeEmail())
                || StringUtils.isBlank(uli.getUser().getFirstName())
                || StringUtils.isBlank(uli.getUser().getLastName())
                || StringUtils.isBlank(uli.getUser().getUuid())) {
            throw new RequiredPropertyNullException();
        }

        try {
            getLoginInfoDao().loadByUsername(uli.getUser().getEmailOrMergeEmail());
            log().error("Username '{}' already used!", uli.getUser().getEmailOrMergeEmail());
            throw new UsernameExistsException();
        } catch (NoRecordException ex) {
            // Vytvorime uzivatela
            createUser(uli.getUser(), registerAtNotx);

            // Vytvorime login info
            getLoginInfoDao().create(uli);

            // Vytvorime obrazok uzivatela

            // Vytvorime aktivaciu
            if (withActivativation) {
                UserActivation a = ActivationUtils.createActivation(uli);
                a.setInvitation(false);
                getActivationDao().create(a);

                // Posleme email                
                RegistrationEmail e = new RegistrationEmail(a.getKey(), uli);
                e.sendNotification();
                // Log success
                log().debug("Created not activated: {}", uli.getUser());
            } else {
                // Log success
                log().debug("Created activated: {}", uli.getUser());
            }

        }
    }

    /**
     * Registracia uzivatela - regularna.
     */
    @Override
    public void createRegistredUser(UserLoginInfo uli) throws RequiredPropertyNullException, UsernameExistsException {
        createRegistredUser(uli, true, true);
    }

    /**
     * Aktivuje uzivatela podla kluca. Aktivaciou sa vytvori uzivatelovi
     * defaultna privatna UserOrganization a zaregistruje sa do Messaging
     * sluzby.
     *
     * @throws ActivationKeyException ak kluc neexistuje
     *
     * @throws InvitationException ak je aktivacia invitation=true, teda je
     * nutne aby uzivatel prevyplnil registraciu
     *
     * @throws RequiredPropertyNullException ak je key == null
     *
     * @throws ServiceExecutionFailed ak sa nepodarilo zaregistrovat uzivatela
     * do messaging sluzby
     */
    @Override
    public void activateUser(String key)
            throws ServiceExecutionFailed, RequiredPropertyNullException, ActivationKeyException, InvitationException {
        if (StringUtils.isBlank(key)) {
            throw new RequiredPropertyNullException();
        }
        try {
            // Load by key
            UserActivation a = getActivationDao().loadWithUserByActivationKey(key);
            // Jedna sa o pozvanku - musi sa presmerovat na prislusnu stranku
            if (a.isInvitation()) {
                log().info("User trying to activate with code that is Invitation");
                throw new InvitationException();
            }

            // Activation Procedure - vytvorenie defualtnej oranizacie a mesaging
            activateUserProcedure(a.getLoginInfo(), key);

            // Send email
            ActivationEmail ae = new ActivationEmail(a.getLoginInfo());
            ae.sendNotification();

            // Log success

            log().debug("{} : is active now.", a.getLoginInfo().getUser());

        } catch (NoRecordException ex) {
            log().debug("Activate failed, given activation key '{}' not exists! ", key);

            throw new ActivationKeyException();
        }
    }

    /**
     * Aktivuje pozvaneho uzivatela podla kluca. Aktivaciou sa vytvori
     * uzivatelovi defaultna privatna UserOrganization a zaregistruje sa do
     * Messaging sluzby. Zaroven sa mu nastavi heslo a dalsie udaje ktore
     * predvyplnil u poregistracie
     *
     * @throws ActivationKeyException ak kluc neexistuje
     *
     * @throws RequiredPropertyNullException ak je nejaka property blan
     *
     * @throws ServiceExecutionFailed ak sa nepodarilo zaregistrovat uzivatela
     * do messaging sluzby
     *
     */
    @Override
    public User activateInvitedUser(String firstName, String lastName, String password, String key)
            throws
            RequiredPropertyNullException,
            ServiceExecutionFailed,
            ActivationKeyException {
        try {
            // Load by key
            UserActivation a = getActivationDao().loadWithUserByActivationKey(key);
            // Jedna sa o pozvanku - musi sa presmerovat na prislusnu stranku
            if (!a.isInvitation()) {
                log().info("User trying to activate with code that is NOT an invitation");
                throw new ServiceExecutionFailed();
            }
            // Nastavime nove uzivatelske vlastnosti
            UserLoginInfo userInfo = a.getLoginInfo();
            userInfo.setPassword(UserUtils.hashPassword(password));
            userInfo.getUser().setFirstName(firstName);
            userInfo.getUser().setLastName(lastName);
            // userInfo sa updatuje v procedure
            // user sa updatuje v procedure
            // Vykoname proceduru aktivacie
            activateUserProcedure(userInfo, key);
            // Send email
            ActivationEmail ae = new ActivationEmail(a.getLoginInfo());
            ae.sendNotification();

            // Log success
            log().debug("{} : invited user is active now.", a.getLoginInfo().getUser());


            return userInfo.getUser();
        } catch (NoRecordException ex) {
            log().debug("Activate failed, given activation key '{}' not exists! ", key);
            throw new ActivationKeyException();
        }
    }

    private void activateUserProcedure(UserLoginInfo li, String key) throws ServiceExecutionFailed {
        // Set user Active
        li.setActive(true);
        getLoginInfoDao().update(li);
        getUserDao().update(li.getUser());
        log().debug("{} :Activating user ", li.getUser());
        // Delete activation record
        getActivationDao().deleteByActivationKey(key);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void generateActivationEmail(String userUuid)
            throws NoRecordException, RequiredPropertyNullException {
        UserActivation activation = getActivationDao().loadWithUserByUserUuid(userUuid);
        RepeatingActivationEmail email = new RepeatingActivationEmail(activation.getLoginInfo(), activation);
        email.sendNotification();
    }


    /*
     * ------------------------------------------------------------------------
     * ---- L O A D ----
     * ------------------------------------------------------------------------
     */
    @Override
    public User loadUserByUuid(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return userDao.loadByUuid(uuid);
    }

    @Override
    public User loadDetailedUserByUuid(String uuid) throws ServiceExecutionFailed, NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return userDao.loadDetailedByUuid(uuid);
    }

    @Override
    public User loadInvitedActivation(String token) throws RequiredPropertyNullException, NoRecordException, ActivationKeyException {
        if (StringUtils.isBlank(token)) {
            throw new RequiredPropertyNullException();
        }
        UserActivation activation = activationDao.loadWithUserByActivationKey(token);
        if (!activation.isInvitation()) {
            throw new ActivationKeyException();
        }
        // dotahnuti usera
        activation.getLoginInfo().getUser().getEmailOrMergeEmail();
        return activation.getLoginInfo().getUser();
    }

    @Override
    public String loadUserUuidByUsername(String username) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(username)) {
            throw new RequiredPropertyNullException();
        }
        return userDao.loadUserUuidByUsername(username);
    }

    @Override
    public String loadUserEmailByUuid(String uuid)
            throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return userDao.loadEmailByUuid(uuid);
    }

    @Override
    public String loadUserProfileImagePrefix(String userUuid)
            throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }

        String s = userDao.loadProfileImagePrefix(userUuid);
        if (StringUtils.isBlank(s)) {
            throw new NoRecordException();
        }
        return s;
    }

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ---- L I S T ----
     */
    /*
     * ------------------------------------------------------------------------
     */
    @Override
    public List<User> listUsersForUuids(List<String> uuids) throws NoRecordException {
        return userDao.listUsersByUuids(uuids);
    }
    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ---- U P D A T E ----
     */
    /*
     * ------------------------------------------------------------------------
     */

    @Override
    public void updateUser(User u) throws ServiceExecutionFailed {
        userDao.update(u);

        getNotxUserWebService().createOrUpdateUser(UserNotxUtils.createRequest(u));
        log().debug("Update user at NotX: {}", u);
    }

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ---- D E L E T E ----
     */
    /*
     * ------------------------------------------------------------------------
     */
    @Override
    public void deleteUser(User user) {
        log().debug("Delete: {}", user);
        UserOrganization privateUserOrg = user.getPrivateUserOrganization();
        user.setDefaultUserOrganization(null);
        user.setPrivateUserOrganization(null);
        userDao.update(user);
        if (privateUserOrg != null) {
            userOrganizationDao.delete(privateUserOrg);
        }
        userDao.delete(user);
    }

    /*
     * ========================================================================
     */
    /*
     * ---- SETTER & GETTER ----
     */
    /*
     * ========================================================================
     */
    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public UserLoginInfoDao getLoginInfoDao() {
        return loginInfoDao;
    }

    public void setLoginInfoDao(UserLoginInfoDao loginInfoDao) {
        this.loginInfoDao = loginInfoDao;
    }

    public UserActivationDao getActivationDao() {
        return activationDao;
    }

    public void setActivationDao(UserActivationDao activationDao) {
        this.activationDao = activationDao;
    }

    public UserOrganizationDao getUserOrganizationDao() {
        return userOrganizationDao;
    }

    public void setUserOrganizationDao(UserOrganizationDao userOrganizationDao) {
        this.userOrganizationDao = userOrganizationDao;
    }

    /**
     * @return the notxUserWebService
     */
    public UserWebService getNotxUserWebService() {
        return notxUserWebService;
    }

    /**
     * @param notxUserWebService the notxUserWebService to set
     */
    public void setNotxUserWebService(UserWebService notxUserWebService) {
        this.notxUserWebService = notxUserWebService;
    }

    @Override
    public void updateMergeLoginInfo(String toMergeUser, User user) {
        try {
            User u = userDao.loadByUuid(toMergeUser);
            user.setMergeWith(u);
            userDao.update(user);
        } catch (NoRecordException ex) {
        }
    }
}
