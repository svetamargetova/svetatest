package consys.admin.user.core.service;

import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import java.util.Date;
import java.util.List;
import org.apache.commons.mail.EmailException;

/**
 *
 * @author palo
 */
public interface UserLoginInfoService {
    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/

    void createLostPassword(String username)
            throws RequiredPropertyNullException, NoRecordException, EmailException;

    void createApiResetPassword(String username)
            throws RequiredPropertyNullException, NoRecordException, EmailException;

    void createUserLoginInfo(UserLoginInfo userLoginInfo)
            throws RequiredPropertyNullException;

    void createUserActivation(UserActivation userActivation)
            throws RequiredPropertyNullException;
    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/

    /**
     * Na zaklade uzivatelkseho UUID a vlozeneho hesla zmeni heslo.
     *
     * @param password nove heslo
     * @param userUuid uzivatel ktory chce zmenit heslo
     * 
     */
    void updatePassword(String password, String userUuid) throws RequiredPropertyNullException, NoRecordException;

    void updateLostPassword(String password, UserLoginInfo uli) throws RequiredPropertyNullException;

    void updateLostPasswordWithoutSendMail(String password, UserLoginInfo uli) throws RequiredPropertyNullException;

    void updateUserLoginInfo(UserLoginInfo info) throws RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    public UserLoginInfo loadUserLoginInfo(String username)
            throws RequiredPropertyNullException, NoRecordException;

    public UserLoginInfo loadUserLoginInfoByLostToken(String token)
            throws RequiredPropertyNullException, NoRecordException;

    public UserLoginInfo loadUserLoginInfoWithUserByUuid(String userUuid)
            throws RequiredPropertyNullException, NoRecordException;

    public UserLoginInfo loadUserLoginInfoByTwitterId(Long twitterId)
            throws RequiredPropertyNullException, NoRecordException;

    public UserLoginInfo loadUserLoginInfoByLinkedInId(String linkedInId)
            throws RequiredPropertyNullException, NoRecordException;

    public UserLoginInfo loadUserLoginInfoByGoogleId(String googleId, String email)
            throws RequiredPropertyNullException, NoRecordException;

    public UserLoginInfo loadUserLoginInfoByFacebookIdOrEmail(Long facebookId, String email)
            throws RequiredPropertyNullException, NoRecordException;

    public int loadAllUserLoginInfoCount();

    /*------------------------------------------------------------------------*/
    /* ----  L I S T  ---- */
    /*------------------------------------------------------------------------*/
    public List<UserActivation> listOlderActivationsThan(Date date);

    /**
     * Nacita detailny zonzma  userLoginInfo na zaklade strankovanich argumentov.
     * Detailny znamena ze userLoginfInfo <- user <- defaultUserOrganization <- organization
     * @param firstItem
     * @param itemsPerPage
     * @return
     * @throws RequiredPropertyNullException
     * @throws NoRecordException 
     */
    public List<UserLoginInfo> listDetailedUserLoginInfo(int firstItem, int itemsPerPage)
            throws RequiredPropertyNullException, NoRecordException;

    /*------------------------------------------------------------------------*/
    /* ---- D E L E T E ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Odstrani zaregistrovaneho pouzivatela. Posle mu email o tejto skutocnosti.
     * 
     * @param userActivation
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    public void deleteNotActivetedUser(UserActivation userActivation)
            throws RequiredPropertyNullException;
}
