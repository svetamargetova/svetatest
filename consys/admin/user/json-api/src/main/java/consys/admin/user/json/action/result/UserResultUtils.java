package consys.admin.user.json.action.result;

import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.User;
import consys.common.json.Result;
import java.io.IOException;
import java.util.List;
 
/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class UserResultUtils{
    
    public static void writeUser(Result result, User user) throws IOException{   
        result.writeObjectFieldStart("user");
        writeUserContent(result,user);
        result.writeEndObject();
    }
    
    public static void writeUsers(Result result, List<User> users) throws IOException{        
        result.writeArrayFieldStart("users");
        for(User user : users){
            result.writeStartObject();
            writeUserContent(result,user);
            result.writeEndObject();
        }        
        result.writeEndArray();
    }

    public static void writeUserContent(Result result, User user) throws IOException{
        result.writeStringField("name", user.getFullName());
        result.writeStringField("position", user.getDefaultUserOrganization().getPosition());
        result.writeStringField("resume", user.getBio());
        result.writeStringField("uuid", user.getUuid());
        result.writeStringField("portrait", user.getPortraitImageUuidPrefix());
        Organization organization = user.getDefaultUserOrganization().getOrganization();

        result.writeStringField("www", organization == null ? "" : organization.getWeb());
        result.writeStringField("company", organization == null ? "" : organization.getName());
        result.writeStringField("company-from", Result.formatDate(user.getDefaultUserOrganization().getDateFrom()));
        result.writeStringField("company-to", Result.formatDate(user.getDefaultUserOrganization().getDateTo()));
        result.writeStringField("email", user.getEmailOrMergeEmail());
    }
}
