package consys.admin.user.json.action.handler;

import consys.admin.user.core.service.UserLoginInfoService;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UserApiResetPasswordActionHandler implements ActionHandler, ActionHandlerConstants {

    @Autowired
    private UserLoginInfoService userLoginInfoService;

    @Override
    public String getActionName() {
        return "user-password-reset";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        if (!USER_API_HASH.equalsIgnoreCase(action.getStringParam("apikey"))) {
            throw new IOException("Missing or bad apikey");
        }
        try {
            userLoginInfoService.createApiResetPassword(action.getStringParam("email"));
        } catch (Exception ex) {
            throw new IOException("Missing user email");
        }
    }
}
