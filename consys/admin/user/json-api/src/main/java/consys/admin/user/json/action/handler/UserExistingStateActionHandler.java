package consys.admin.user.json.action.handler;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UserExistingStateActionHandler implements ActionHandler, ActionHandlerConstants {

    // konstanty
    private static final int NOT_REGISTERED = 0;
    private static final int SSO_REGISTERED = 1;
    private static final int TAKEPLACE_REGISTERED = 2;
    // sluzby
    @Autowired
    private UserLoginInfoService userLoginInfoService;

    @Override
    public String getActionName() {
        return "user-register-state";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        if (!USER_API_HASH.equalsIgnoreCase(action.getStringParam("apikey"))) {
            throw new IOException("Missing or bad apikey");
        }
        int state = -1;
        try {
            UserLoginInfo userInfo = userLoginInfoService.loadUserLoginInfo(action.getStringParam("email"));
            if (userInfo.getFacebookId() == null
                    && userInfo.getGoogleId() == null
                    && userInfo.getLinkedInId() == null
                    && userInfo.getTwitterId() == null) {
                state = TAKEPLACE_REGISTERED;
            } else {
                state = SSO_REGISTERED;
            }
        } catch (NoRecordException ex) {
            state = NOT_REGISTERED;
        } catch (RequiredPropertyNullException ex) {
            throw new IOException("Missing user email");
        }

        result.writeObjectFieldStart("user");
        result.writeNumberField("state", state);
        result.writeEndObject();
    }
}
