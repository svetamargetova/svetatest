package consys.admin.user.json.action.handler;

import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.admin.user.json.action.result.UserResultUtils;
import consys.common.core.exception.ConsysException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.common.json.error.ErrorCode;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserDetailActionHandler implements ActionHandler{
    private static final String PARAM_UUID = "uuid";
    private static final Logger logger = LoggerFactory.getLogger(UserDetailActionHandler.class);

    @Autowired
    private UserService userService;

    @Override
    public String getActionName() {
        return "user";
    }   

    @Override
    public void execute(Action action, Result result) throws IOException {
        try {
            String userUuid = action.getStringParam(PARAM_UUID);
            User user = userService.loadDetailedUserByUuid(userUuid);
            UserResultUtils.writeUser(result,user);
        } catch (RequiredPropertyNullException ex) {
            ErrorCode.error(result, ex, PARAM_UUID);
        } catch (ConsysException ex) {
            ErrorCode.error(result, ex);
        }
    }




}
