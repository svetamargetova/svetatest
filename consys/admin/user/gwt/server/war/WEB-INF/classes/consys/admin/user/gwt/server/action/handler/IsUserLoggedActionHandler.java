package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.common.gwt.client.rpc.action.IsUserLoggedAction;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.shared.action.ActionException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Palo
 */
public class IsUserLoggedActionHandler extends AbstractAnonymousActionHandler<IsUserLoggedAction, BooleanResult> {

    public IsUserLoggedActionHandler() {
        super(IsUserLoggedAction.class);
    }

    @Override
    public BooleanResult execute(IsUserLoggedAction action) throws ActionException {
        Authentication a = SecurityContextHolder.getContext().getAuthentication();        
        if(a instanceof AnonymousAuthenticationToken){
            return new BooleanResult(false);
        }else{
            return new BooleanResult(true);
        }                
    }
}
