package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.core.service.exception.InvitationException;
import consys.admin.user.gwt.client.action.ActivateUserAction;
import consys.admin.user.gwt.client.action.exception.ActivationKeyIsInvitationException;
import consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ActivateUserActionHandler extends AbstractAnonymousActionHandler<ActivateUserAction, VoidResult> {

    private UserService userService;

    public ActivateUserActionHandler() {
        super(ActivateUserAction.class);
    }

    @Override
    public VoidResult execute(ActivateUserAction action) throws ActionException {
        try {
            String key = action.getKey();
            logger.debug("Activating user by key "+key);
            if (key == null || key.isEmpty()) {
                throw new BadInputException();
            }
            getUserService().activateUser(action.getKey());
            return new VoidResult();              
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (InvitationException ex) {
            throw new ActivationKeyIsInvitationException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (ActivationKeyException ex) {
            throw new ActivationKeyNotFoundException();
        }
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required=true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
