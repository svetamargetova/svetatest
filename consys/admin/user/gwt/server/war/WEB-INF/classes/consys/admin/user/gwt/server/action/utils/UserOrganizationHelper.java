package consys.admin.user.gwt.server.action.utils;

import consys.admin.user.core.bo.UserOrganization;
import consys.common.gwt.shared.bo.ClientOrganization;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb;
import consys.common.utils.enums.CountryEnum;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.CountryService;
import consys.common.gwt.shared.bo.ClientAddress;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public class UserOrganizationHelper {

    private static final Logger logger = LoggerFactory.getLogger(UserOrganizationHelper.class);

    public static ClientUserOrgProfileThumb toClientUserOrgProfileThumb(UserOrganization uo) {
        ClientUserOrgProfileThumb cuo = new ClientUserOrgProfileThumb();
        cuo.setActualPosition(uo.isActual());
        cuo.setUuid(uo.getUuid());
        cuo.setFrom(uo.getDateFrom());
        cuo.setTo(uo.getDateTo());
        cuo.setOrgName(uo.getOrganization().getName());
        cuo.setPosition(uo.getPosition());
        // TODO: is preffered position
        return cuo;
    }

    public static ClientUserOrg toClientUserOrg(UserOrganization uo) {
        return toClientUserOrg(uo, false);
    }

    public static ClientUserOrg toClientUserOrg(UserOrganization uo, boolean withAddress) {
        ClientUserOrg cuo = new ClientUserOrg();
        cuo.setActualPosition(uo.isActual());
        cuo.setFromDate(uo.getDateFrom());
        ClientOrganization o = OrganizationHelper.toClientOrganization(uo.getUser(), uo.getOrganization());
        cuo.setOrganization(o);
        cuo.setPosition(uo.getPosition());
        cuo.setPreferredPosition(true);
        cuo.setToDate(uo.getDateTo());
        cuo.setUuid(uo.getUuid());

        ClientAddress c = new ClientAddress();
        // Ak je input true tak sa vklada aj adressa, inak nie
        if (withAddress) {
            c.setCity(uo.getCity());
            if (uo.getCountry() != null) {
                c.setLocation(uo.getCountry().getId());
                c.setLocationName(uo.getCountry().getName());
            }
            if (uo.getUsState() != null) {
                c.setStateUs(uo.getUsState().getId());
                c.setStateUsName(uo.getUsState().getName());
            }
            c.setStreet(uo.getStreet());
            c.setZip(uo.getZip());
        }
        cuo.setAddress(c);

        return cuo;
    }

    public static void toUserOrganization(UserOrganization uo, ClientUserOrg cuo, CountryService stateService) throws ServiceFailedException {
        uo.setActual(cuo.isActualPosition());
        uo.setDateFrom(cuo.getFromDate());
        uo.setDateTo(cuo.getToDate());
        uo.setPosition(cuo.getPosition());

        if (cuo.getAddress() == null) {
            cuo.setAddress(cuo.getOrganization().getAddress());
        }

        // address                
        long locid = cuo.getAddress().getLocation();
        if (locid > 0) {
            try {
                uo.setCountry(stateService.loadCountryById(cuo.getAddress().getLocation()));
            } catch (NoRecordException ex) {
                logger.error("Load State by ID failed");
                throw new ServiceFailedException();
            }
        }
        // STREET
        uo.setStreet(cuo.getAddress().getStreet());
        // USA STATE
        long stateid = cuo.getAddress().getStateUs();
        if (stateid > 0) {
            try {
                uo.setUsState(stateService.loadUsStateById(cuo.getAddress().getStateUs()));
            } catch (NoRecordException ex) {
                logger.error("Load US state by ID failed");
                throw new ServiceFailedException();
            }
        }
        // ZIP
        uo.setZip(cuo.getAddress().getZip());
        uo.setCity(cuo.getAddress().getCity());
    }
}
