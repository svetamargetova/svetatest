package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.gwt.client.action.ListUserOrgsAction;
import consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb;
import consys.admin.user.gwt.server.action.utils.UserOrganizationHelper;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class ListUserOrgsActionHandler extends AbstractRoleUserActionHandler<ListUserOrgsAction, ArrayListResult<ClientUserOrgProfileThumb>> {

    private UserOrganizationService uos;

    public ListUserOrgsActionHandler() {
        super(ListUserOrgsAction.class);
    }

    // Vynimky zadne?
    @Override
    public ArrayListResult<ClientUserOrgProfileThumb> execute(ListUserOrgsAction action) throws ActionException {
        try {
            String userUuid = getUserSessionContext().getUserUuid();
            List<UserOrganization> l = uos.listUserOrganizations(userUuid);
            ArrayList<ClientUserOrgProfileThumb> out = Lists.newArrayList();
            for (UserOrganization uo : l) {
                ClientUserOrgProfileThumb userOrg = UserOrganizationHelper.toClientUserOrgProfileThumb(uo);
                out.add(userOrg);
            }
            logger.debug("Loaded " + out.size() + " user organizations");
            ArrayListResult<ClientUserOrgProfileThumb> result = new ArrayListResult<ClientUserOrgProfileThumb>(out);
            return result;
        } catch (RequiredPropertyNullException ex) {
           throw new BadInputException();
        }
    }

    /**
     * @return the uos
     */
    public UserOrganizationService getUos() {
        return uos;
    }

    /**
     * @param uos the uos to set
     */
    @Autowired(required = true)
    public void setUos(UserOrganizationService uos) {
        this.uos = uos;
    }
}
