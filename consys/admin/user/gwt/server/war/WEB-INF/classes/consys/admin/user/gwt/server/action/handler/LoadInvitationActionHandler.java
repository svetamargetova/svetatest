package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.gwt.client.action.LoadInvitationAction;
import consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException;
import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadInvitationActionHandler extends AbstractAnonymousActionHandler<LoadInvitationAction, ClientRegistration>{

    private UserService userService;

    public LoadInvitationActionHandler() {
        super(LoadInvitationAction.class);
    }

    @Override
    public ClientRegistration execute(LoadInvitationAction action) throws ActionException {
        try {
            User user = userService.loadInvitedActivation(action.getToken());
            ClientRegistration registration = new ClientRegistration();
            registration.setContactEmail(user.getEmailOrMergeEmail());
            registration.setFirstName(user.getFirstName());
            registration.setLastName(user.getLastName());
            return registration;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (ActivationKeyException ex) {
            throw new ActivationKeyNotFoundException();
        }
    }

    /**
     * @return the userService
     */
    @Autowired
    @Required
    public void setUserService(UserService us) {
        userService = us;
    }

    
    




}
