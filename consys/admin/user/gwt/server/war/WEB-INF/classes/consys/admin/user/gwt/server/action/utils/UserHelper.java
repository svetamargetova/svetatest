package consys.admin.user.gwt.server.action.utils;

import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.gwt.shared.bo.ClientUser;

/**
 *
 * @author Palo
 */
public class UserHelper {
 
    public static ClientUser toClientUser(UserLoginInfo uli) {
        User u = uli.getUser();
        ClientUser cu = new ClientUser();
        cu.setBio(u.getBio());
        cu.setUuid(u.getUuid());
        cu.setFirstName(u.getFirstName());
        cu.setFrontDegree(u.getFrontDegree());
        cu.setLastName(u.getLastName());
        cu.setLoginEmail(u.getEmailOrMergeEmail());
        cu.setMiddleName(u.getMiddleName());
        cu.setRearDegree(u.getRearDegree());
        cu.setUserOrgDefault(u.getDefaultUserOrganization().getShortInfo());
        cu.setUuidImg(u.getPortraitImageUuidPrefix());
        cu.setGender(u.getGender());
        cu.setActivatedUser(uli.isActive());
        return cu;
    }
    
}
