package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.action.LoadUserThumbAction;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadUserThumbActionHandler extends AbstractRoleUserActionHandler<LoadUserThumbAction, ClientUserThumb>{

    private UserService userService;

    public LoadUserThumbActionHandler() {
        super(LoadUserThumbAction.class);
    }

    @Override
    public ClientUserThumb execute(LoadUserThumbAction action) throws ActionException {
        try {
            User u = userService.loadUserByUuid(action.getUuid());
            ClientUserThumb thumb = new ClientUserThumb();
            thumb.setName(u.getFullName());
            thumb.setUuid(u.getUuid());
            thumb.setUuidPortraitImagePrefix(u.getPortraitImageUuidPrefix());
            return thumb;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required=true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    




}
