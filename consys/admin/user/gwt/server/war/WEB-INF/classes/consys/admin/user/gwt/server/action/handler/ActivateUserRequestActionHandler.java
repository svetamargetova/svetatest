package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.service.UserService;
import consys.admin.user.gwt.client.action.ActivateUserRequestAction;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author pepa
 */
public class ActivateUserRequestActionHandler extends AbstractRoleUserActionHandler<ActivateUserRequestAction, VoidResult> {

    private UserService userService;

    public ActivateUserRequestActionHandler() {
        super(ActivateUserRequestAction.class);
    }

    @Override
    public VoidResult execute(ActivateUserRequestAction action) throws ActionException {
        try {
            getUserService().generateActivationEmail(action.getUserUuid());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Autowired
    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
