package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.common.utils.enums.CountryEnum;

import consys.common.core.service.CountryService;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.shared.action.ActionException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class ListLocationActionHandler  extends AbstractAnonymousActionHandler<ListLocationAction, SelectBoxData>{

    private CountryService stateService;


    public ListLocationActionHandler() {
        super(ListLocationAction.class);
    }

    
    
    @Override
    public SelectBoxData execute(ListLocationAction action) throws ActionException {
        List<CountryEnum> states = stateService.listCountries();
        SelectBoxData data = new SelectBoxData();
        for (CountryEnum state : states) {
            data.getList().add(new BoxItem(state.getName(),state.getId()));
        }        
        return data;
    }

    /**
     * @return the stateService
     */
    public CountryService getStateService() {
        return stateService;
    }

    /**
     * @param stateService the stateService to set
     */
    @Autowired(required=true)
    public void setStateService(CountryService stateService) {
        this.stateService = stateService;
    }

}
