package consys.admin.user.gwt.server.action.handler;
 
import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.bo.OrganizationTypeEnum;
import consys.admin.user.core.service.OrganizationService;
import consys.admin.user.gwt.client.action.ListOrganizationTypesAction;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.shared.action.ActionException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
 
/**
 *
 * @author Palo
 */
public class ListOrganizationTypesActionHandler extends AbstractAnonymousActionHandler<ListOrganizationTypesAction, SelectBoxData> {

    private OrganizationService organizationService;

    public ListOrganizationTypesActionHandler() {
        super(ListOrganizationTypesAction.class);
    }

    @Override
    public SelectBoxData execute(ListOrganizationTypesAction action) throws ActionException {

        List<OrganizationTypeEnum> types = getOrganizationService().listOrganizationsTypes();
        SelectBoxData data = new SelectBoxData();
        for (OrganizationTypeEnum t : types) {
            data.getList().add(new BoxItem(t.getName(), t.getId().intValue()));
        }        
        return data;
    }

    /**
     * @return the organizationService
     */
    public OrganizationService getOrganizationService() {
        return organizationService;
    }

    /**
     * @param organizationService the organizationService to set
     */
    @Autowired(required = true)
    public void setOrganizationService(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }
}
