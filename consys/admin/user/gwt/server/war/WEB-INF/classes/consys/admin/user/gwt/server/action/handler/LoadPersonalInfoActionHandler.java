package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.admin.user.gwt.client.action.LoadPersonalInfoAction;
import consys.common.gwt.shared.bo.ClientPersonalInfo;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.server.action.server.ActionHandler;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadPersonalInfoActionHandler extends AbstractRoleUserActionHandler<LoadPersonalInfoAction, ClientPersonalInfo> {

    @Autowired
    private UserService userService;

    public LoadPersonalInfoActionHandler() {
        super(LoadPersonalInfoAction.class);
    }

    @Override
    public ClientPersonalInfo execute(LoadPersonalInfoAction action) throws ActionException {
        try {
            User user = userService.loadUserByUuid(getUserSessionContext().getUserUuid());
            ClientPersonalInfo cpi = new ClientPersonalInfo();
            cpi.setCeliac(user.isCeliac());
            cpi.setVegetarian(user.isVegetarian());
            return cpi;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }        
    }
    
    

  
}
