package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.service.OrganizationService;
import consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction;
import consys.common.gwt.shared.bo.ClientOrganizationThumb;
import consys.admin.user.gwt.server.action.utils.OrganizationHelper;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxResult;
import consys.common.gwt.shared.action.ActionException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class ListOrganizationsByPrefixActionHandler extends AbstractAnonymousActionHandler<ListOrganizationsByPrefixAction, SelectBoxResult<ClientOrganizationThumb>> {

    private OrganizationService organizationService;

    public ListOrganizationsByPrefixActionHandler() {
        super(ListOrganizationsByPrefixAction.class);
    }

    @Override
    public SelectBoxResult<ClientOrganizationThumb> execute(ListOrganizationsByPrefixAction action) throws ActionException {
        List<Organization> os = getOrganizationService().listByPrefix(action.getPrefix(), action.getLimit());
        ArrayList<SelectBoxItem<ClientOrganizationThumb>> out = Lists.newArrayList();
        for (Organization organization : os) {
            ClientOrganizationThumb thumb = OrganizationHelper.toClientOrganizationThumb(organization);
            out.add(new SelectBoxItem<ClientOrganizationThumb>(thumb));
        }
        return new SelectBoxResult<ClientOrganizationThumb>(out);
    }

    /**
     * @return the organizationService
     */
    public OrganizationService getOrganizationService() {
        return organizationService;
    }

    /**
     * @param organizationService the organizationService to set
     */
    @Autowired(required=true)
    public void setOrganizationService(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }
}
