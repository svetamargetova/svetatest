package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.gwt.client.action.LoadUserOrgAction;
import consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.server.action.utils.UserOrganizationHelper;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class LoadUserOrgActionHandler extends AbstractRoleUserActionHandler<LoadUserOrgAction, ClientUserOrg> {

    private UserOrganizationService uos;

    public LoadUserOrgActionHandler() {
        super(LoadUserOrgAction.class);
    }

    @Override
    public ClientUserOrg execute(LoadUserOrgAction action) throws ActionException {
        try {
            UserOrganization uo = uos.loadByUuid(action.getUserOrgUuid());
            ClientUserOrg cuo = UserOrganizationHelper.toClientUserOrg(uo);
            boolean b = uos.isUserOrganizationPrefered(uo.getUuid());
            cuo.setPreferredPosition(b);
            return cuo;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new UserOrganizationUuidException();
        }
    }

    /**
     * @return the uos
     */
    public UserOrganizationService getUos() {
        return uos;
    }

    /**
     * @param uos the uos to set
     */
    @Autowired(required = true)
    public void setUos(UserOrganizationService uos) {
        this.uos = uos;
    }
}
