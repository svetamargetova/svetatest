package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUserThumbsActionHandler extends AbstractRoleUserActionHandler<ListUserThumbsAction, ArrayListResult<ClientUserThumb>>{

    private UserService userService;

    public ListUserThumbsActionHandler() {
        super(ListUserThumbsAction.class);
    }

    @Override
    public ArrayListResult<ClientUserThumb> execute(ListUserThumbsAction action) throws ActionException {
        try {
            List<User> users = userService.listUsersForUuids(action.getThumbs());
            ArrayList<ClientUserThumb> thumbs = Lists.newArrayList();
            for(User u : users){
                ClientUserThumb thumb = new ClientUserThumb();
                thumb.setName(u.getFullName());
                thumb.setUuid(u.getUuid());
                thumb.setUuidPortraitImagePrefix(u.getPortraitImageUuidPrefix());
                thumbs.add(thumb);
            }
            return new ArrayListResult<ClientUserThumb>(thumbs);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }

    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required=true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
  




}
