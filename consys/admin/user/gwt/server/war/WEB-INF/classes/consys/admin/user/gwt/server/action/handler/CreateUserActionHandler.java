package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.UserUtils;
import consys.admin.user.gwt.client.action.CreateUserAction;
import consys.common.gwt.client.widget.exception.UserExistsException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class CreateUserActionHandler extends AbstractAnonymousActionHandler<CreateUserAction, VoidResult> {

    private UserService userService;

    public CreateUserActionHandler() {
        super(CreateUserAction.class);
    }

    @Override
    public VoidResult execute(CreateUserAction action) throws ActionException {
        try {
            logger.debug("Create user {}",action.getClientRegistration().getContactEmail());
            ClientRegistration cr = action.getClientRegistration();
            UserLoginInfo info = UserUtils.prepareNewUser(cr.getContactEmail(), cr.getPassword());
            info.getUser().setFirstName(cr.getFirstName());
            info.getUser().setLastName(cr.getLastName());
            userService.createRegistredUser(info);
            return new VoidResult();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (UsernameExistsException ex) {
            throw new UserExistsException();
        }
        
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required = true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
