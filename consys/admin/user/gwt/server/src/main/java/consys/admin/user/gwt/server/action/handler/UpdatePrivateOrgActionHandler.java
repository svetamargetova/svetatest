package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.OrganizationService;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.gwt.client.action.UpdatePrivateOrgAction;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.server.action.utils.UserOrganizationHelper;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.CountryService;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class UpdatePrivateOrgActionHandler extends AbstractRoleUserActionHandler<UpdatePrivateOrgAction, VoidResult> {

    private UserService userService;
    private UserOrganizationService userOrganizationService;
    private OrganizationService organizationService;
    private CountryService stateService;

    public UpdatePrivateOrgActionHandler() {
        super(UpdatePrivateOrgAction.class);
    }

    @Override
    public VoidResult execute(UpdatePrivateOrgAction action) throws ActionException {
        ClientUserOrg cuo = action.getClientUserOrg();

        try {
            UserOrganization uo = userOrganizationService.loadPrivateUserOrg(getContextProvider().getUserContext().getUserUuid());
            UserOrganizationHelper.toUserOrganization(uo, cuo, getStateService());
            uo.setActual(true);
            userOrganizationService.updateUserOrganization(uo, cuo.isPreferredPosition());
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            logger.error("Updating user organization with bad uuid");
            throw new ServiceFailedException();
        }
        return VoidResult.RESULT();
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required = true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the userOrganizationService
     */
    public UserOrganizationService getUserOrganizationService() {
        return userOrganizationService;
    }

    /**
     * @param userOrganizationService the userOrganizationService to set
     */
    @Autowired(required = true)
    public void setUserOrganizationService(UserOrganizationService userOrganizationService) {
        this.userOrganizationService = userOrganizationService;
    }

    /**
     * @return the organizationService
     */
    public OrganizationService getOrganizationService() {
        return organizationService;
    }

    /**
     * @param organizationService the organizationService to set
     */
    @Autowired(required = true)
    public void setOrganizationService(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    /**
     * @return the stateService
     */
    public CountryService getStateService() {
        return stateService;
    }

    /**
     * @param stateService the stateService to set
     */
    @Autowired(required = true)
    public void setStateService(CountryService stateService) {
        this.stateService = stateService;
    }
}
