package consys.admin.user.gwt.server.action.utils;

import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.OrganizationService;
import consys.common.gwt.shared.bo.ClientOrganization;
import consys.common.gwt.shared.bo.ClientOrganizationThumb;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.CountryService;
import consys.common.gwt.shared.bo.ClientAddress;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public class OrganizationHelper {

    private static final Logger logger = LoggerFactory.getLogger(OrganizationHelper.class);

    public static synchronized ClientOrganization toClientOrganization(User u, Organization o) {
        if (o == null) {
            return null;
        }
        ClientOrganization co = new ClientOrganization();
        co.setAcronym(o.getAcronym());
        co.setFullName(o.getName());
        co.setOrganizationType(o.getOrganizationType() == null ? 0 : o.getOrganizationType().getId());
        co.setUniversalName(o.getInternacionalName());
        co.setUuid(o.getUuid());
        co.setWeb(o.getWeb());
        co.setOwner(u.getId().equals(o.getUserCreated().getId()));

        ClientAddress c = new ClientAddress();
        c.setCity(o.getCity());
        if (o.getCountry() != null) {
            c.setLocation(o.getCountry().getId());
            c.setLocationName(o.getCountry().getName());
        }
        if (o.getUsState() != null) {
            c.setStateUs(o.getUsState().getId());
            c.setStateUsName(o.getUsState().getName());
        }
        c.setStreet(o.getStreet());
        c.setZip(o.getZip());
        co.setAddress(c);

        return co;
    }

    public static synchronized ClientOrganizationThumb toClientOrganizationThumb(Organization o) {
        if (o == null) {
            return null;
        }
        ClientOrganizationThumb thumb = new ClientOrganizationThumb();

        StringBuilder sb = new StringBuilder();
        sb.append(o.getStreet());
        if (!StringUtils.isBlank(o.getCity()) || !StringUtils.isBlank(o.getZip())) {
            if (!StringUtils.isBlank(sb.toString())) {
                sb.append(", ");
            }
            sb.append(o.getCity());
            if (!StringUtils.isBlank(o.getZip())) {
                sb.append(" ");
                sb.append(o.getZip());
            }
        }
        if (o.getCountry() != null) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(o.getCountry().getName());
        }
        thumb.setAddress(sb.toString());
        thumb.setType(o.getOrganizationType() == null ? "" : o.getOrganizationType().getName());
        thumb.setUniversalName(o.getName());
        thumb.setUuid(o.getUuid());
        thumb.setWeb(o.getWeb());
        return thumb;
    }

    public static void toOrganization(Organization o, ClientOrganization co, OrganizationService organizationService, CountryService stateService) throws ServiceFailedException {
        // ACRONYM
        o.setAcronym(co.getAcronym());
        // CITY
        o.setCity(co.getAddress().getCity());
        // INTERNACIONAL NAME
        o.setInternacionalName(co.getUniversalName());
        // NAME
        o.setName(co.getFullName());
        // ORGANIZATION TYPE
        if (co.getOrganizationType() > 0 
                && (o.getOrganizationType() == null || o.getOrganizationType().getId() != co.getOrganizationType())) {
            try {
                o.setOrganizationType(organizationService.loadOrganizationTypeById(co.getOrganizationType()));
            } catch (NoRecordException ex) {
                logger.error("Load OrganizationType by ID failed");
                throw new ServiceFailedException();
            }

        }
        // LOCATION/STATE
        long locid = co.getAddress().getLocation();
        if (locid > 0
                && (o.getCountry() == null || locid != o.getCountry().getId())) {
            try {
                o.setCountry(stateService.loadCountryById(co.getAddress().getLocation()));
            } catch (NoRecordException ex) {
                logger.error("Load State by ID failed");
                throw new ServiceFailedException();
            }
        }
        // STREET
        o.setStreet(co.getAddress().getStreet());
        // USA STATE
        long stateid = co.getAddress().getStateUs();
        if (stateid > 0
                && (o.getUsState() == null || stateid != o.getUsState().getId())) {
            try {
                o.setUsState(stateService.loadUsStateById(co.getAddress().getStateUs()));
            } catch (NoRecordException ex) {
                logger.error("Load US state by ID failed");
                throw new ServiceFailedException();
            }
        }
        // WEB - URL FORMAT
        o.setWeb(co.getWeb());
        // ZIP
        o.setZip(co.getAddress().getZip());
    }
}
