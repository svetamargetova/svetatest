package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.OrganizationService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.gwt.client.action.LoadOrganizationAction;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.bo.ClientOrganization;
import consys.admin.user.gwt.server.action.utils.OrganizationHelper;
import consys.common.core.exception.NoRecordException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class LoadOrganizationActionHandler extends AbstractAnonymousActionHandler<LoadOrganizationAction, ClientOrganization> {

    private OrganizationService organizationService;
    private UserService userService;

    public LoadOrganizationActionHandler() {
        super(LoadOrganizationAction.class);
    }

    @Override
    public ClientOrganization execute(LoadOrganizationAction action) throws ActionException {
        Organization o;
        try {
            o = getOrganizationService().loadByUuid(action.getOrganizationUuid());
        } catch (NoRecordException ex) {
            logger.debug("Load organization by UUID failed");
            throw new ServiceFailedException();
        }

        User currentUser;
        try {

            currentUser = getUserService().loadUserByUuid(getContextProvider().getUserContext().getUserUuid());
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            logger.error("Load logged user failed by NoRecordsException");
            throw new ServiceFailedException();
        }

        ClientOrganization co = OrganizationHelper.toClientOrganization(currentUser, o);
        return co;
    }

    /**
     * @return the organizationService
     */
    public OrganizationService getOrganizationService() {
        return organizationService;
    }

    /**
     * @param organizationService the organizationService to set
     */
    @Autowired(required = true)
    public void setOrganizationService(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required = true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
