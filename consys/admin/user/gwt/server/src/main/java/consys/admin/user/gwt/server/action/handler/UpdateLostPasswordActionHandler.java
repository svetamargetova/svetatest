package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.gwt.client.action.UpdateLostPasswordAction;
import consys.admin.user.gwt.client.action.exception.UserNotExistsException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo
 */
public class UpdateLostPasswordActionHandler extends AbstractAnonymousActionHandler<UpdateLostPasswordAction, VoidResult> {

    private UserLoginInfoService userLoginInfoService;

    public UpdateLostPasswordActionHandler() {
        super(UpdateLostPasswordAction.class);
    }

    @Override
    public VoidResult execute(UpdateLostPasswordAction action) throws ActionException {
        try {
            UserLoginInfo loginInfo = userLoginInfoService.loadUserLoginInfoByLostToken(action.getToken());
            userLoginInfoService.updateLostPassword(action.getClientPassword(), loginInfo);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new UserNotExistsException();
        }
        return VoidResult.RESULT();
    }

    @Autowired(required = true)
    public void setUserLoginInfoService(UserLoginInfoService userLoginInfoService) {
        this.userLoginInfoService = userLoginInfoService;
    }
}
