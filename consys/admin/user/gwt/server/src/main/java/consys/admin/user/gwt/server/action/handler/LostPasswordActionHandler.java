package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.gwt.client.action.LostPasswordAction;
import consys.admin.user.gwt.client.action.exception.UserNotExistsException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class LostPasswordActionHandler extends AbstractAnonymousActionHandler<LostPasswordAction, VoidResult>{

    private UserLoginInfoService userLoginInfoService;

    public LostPasswordActionHandler() {
        super(LostPasswordAction.class);
    }

    @Override
    public VoidResult execute(LostPasswordAction action) throws ActionException {
        try {
            userLoginInfoService.createLostPassword(action.getEmail());
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new UserNotExistsException();
        } catch (EmailException ex) {
            throw new ServiceFailedException();
        }

        return VoidResult.RESULT();
    }

    
    @Autowired(required=true)
    public void setUserLoginInfoService(UserLoginInfoService userLoginInfoService) {
        this.userLoginInfoService = userLoginInfoService;
    }

}
