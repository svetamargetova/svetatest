package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.gwt.client.action.LoadPrivateUserOrgAction;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.server.action.utils.UserOrganizationHelper;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadPrivateUserOrgHandler extends AbstractAnonymousActionHandler<LoadPrivateUserOrgAction, ClientUserOrg> {

    private UserOrganizationService uos;

    @Autowired(required = true)
    public void setUos(UserOrganizationService uos) {
        this.uos = uos;
    }

    public LoadPrivateUserOrgHandler() {
        super(LoadPrivateUserOrgAction.class);
    }

    @Override
    public ClientUserOrg execute(LoadPrivateUserOrgAction action) throws ActionException {
        String userUuid = getUserSessionContext().getUserUuid();
        try {
            UserOrganization uo = uos.loadPrivateUserOrg(userUuid);
            ClientUserOrg cuo = UserOrganizationHelper.toClientUserOrg(uo, true);
            return cuo;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new ServiceFailedException();
        }
    }
}
