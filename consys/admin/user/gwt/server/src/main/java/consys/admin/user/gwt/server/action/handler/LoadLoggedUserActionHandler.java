package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.admin.user.gwt.server.action.utils.UserHelper;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class LoadLoggedUserActionHandler extends AbstractRoleUserActionHandler<LoadLoggedUserAction, ClientUser>{
    private UserLoginInfoService loginInfoService;

    public LoadLoggedUserActionHandler() {
        super(LoadLoggedUserAction.class);
    }

    @Override
    public ClientUser execute(LoadLoggedUserAction action) throws ActionException {
        try {
            String uuid = getUserSessionContext().getUserUuid();
            logger.debug("Load LoggerUser for: {}",uuid);
            UserLoginInfo uli = loginInfoService.loadUserLoginInfoWithUserByUuid(uuid);
            return UserHelper.toClientUser(uli);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();        
        } catch (NoRecordException ex) {
            throw new ServiceFailedException();
        }
    }

   

    /**
     * @param loginInfoService the userService to set
     */
    @Autowired(required=true)
    public void setUserLoginInfoService(UserLoginInfoService loginInfoService) {
        this.loginInfoService = loginInfoService;
    }

}
