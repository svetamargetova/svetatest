package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.gwt.client.action.DeleteUserOrgAction;
import consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class DeleteUserOrgActionHandler extends AbstractRoleUserActionHandler<DeleteUserOrgAction, VoidResult>{

    private UserOrganizationService uos;

    public DeleteUserOrgActionHandler() {
        super(DeleteUserOrgAction.class);
    }

    @Override
    public VoidResult execute(DeleteUserOrgAction action) throws ActionException {
        try {
            String userUuid = getUserSessionContext().getUserUuid();
            uos.deleteUserOrganization(userUuid,action.getUuid());
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new UserOrganizationUuidException();
        }
        return VoidResult.RESULT();
    }

    /**
     * @return the uos
     */
    public UserOrganizationService getUos() {
        return uos;
    }

    /**
     * @param uos the uos to set
     */
    @Autowired(required=true)
    public void setUos(UserOrganizationService uos) {
        this.uos = uos;
    }
}
