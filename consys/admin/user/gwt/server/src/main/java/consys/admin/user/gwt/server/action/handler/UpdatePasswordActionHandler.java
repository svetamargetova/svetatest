package consys.admin.user.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.gwt.client.action.UpdatePasswordAction;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class UpdatePasswordActionHandler extends AbstractRoleUserActionHandler<UpdatePasswordAction, VoidResult>{

    private UserLoginInfoService userLoginInfoService;

    public UpdatePasswordActionHandler() {
        super(UpdatePasswordAction.class);
    }

    @Override
    public VoidResult execute(UpdatePasswordAction action) throws ActionException {
        try {
            String userUuid = getUserSessionContext().getUserUuid();
            userLoginInfoService.updatePassword(action.getClientPassword(), userUuid);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
        return VoidResult.RESULT();
    }

    @Autowired(required=true)
    public void setUserLoginInfoService(UserLoginInfoService userLoginInfoService) {
        this.userLoginInfoService = userLoginInfoService;
    }

}
