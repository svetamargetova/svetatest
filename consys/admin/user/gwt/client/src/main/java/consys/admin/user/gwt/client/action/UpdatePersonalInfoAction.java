package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.bo.ClientPersonalInfo;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class UpdatePersonalInfoAction implements Action<VoidResult> {

    private static final long serialVersionUID = -6410116742233164723L;
    // date
    private boolean celiac;
    private boolean vegetarian;

    public UpdatePersonalInfoAction() {
    }

    public UpdatePersonalInfoAction(ClientPersonalInfo info) {
        this.celiac = info.isCeliac();
        this.vegetarian = info.isVegetarian();
    }

    public boolean isCeliac() {
        return celiac;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }
}
