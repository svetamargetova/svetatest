package consys.admin.user.gwt.client.action.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class UserOrganizationUuidException extends ActionException {
    private static final long serialVersionUID = 3821653098864009195L;

    

    public UserOrganizationUuidException() {
        super();
    }
}
