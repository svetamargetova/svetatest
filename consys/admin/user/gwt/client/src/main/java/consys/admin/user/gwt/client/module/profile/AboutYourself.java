package consys.admin.user.gwt.client.module.profile;

import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.user.gwt.client.module.profile.account.MyResume;
import consys.admin.user.gwt.client.module.profile.account.PersonInfoContent;
import consys.admin.user.gwt.client.module.profile.account.PersonalInfo;
import consys.admin.user.gwt.client.module.profile.account.PrivateOrgContent;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Formular pro nastaveni Bia uzivatele
 * @author pepa
 */
public class AboutYourself extends SimpleFormPanel implements MenuFormItem {

    public static final String NAME = AUMessageUtils.c.aboutYourself_title_bio();

    public AboutYourself() {
        super();

        addWidget(new PersonInfoContent());

        addWidget(Separator.addedStyle(StyleUtils.MARGIN_BOT_20));

        addWidget(new PrivateOrgContent());

        addWidget(Separator.addedStyle(StyleUtils.MARGIN_BOT_20));

        addWidget(new PersonalInfo());

        addWidget(Separator.addedStyle(StyleUtils.MARGIN_BOT_20));

        addWidget(new MyResume());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.setWidget(this);
    }
}
