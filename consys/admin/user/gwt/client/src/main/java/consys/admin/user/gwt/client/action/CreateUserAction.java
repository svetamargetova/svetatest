package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 * Zaregistruje uzivatela, vynimku:
 * 
 * UserExistsException - ak uzivatelkse meno existuje
 *
 * @author pepa 
 */
public class CreateUserAction implements Action<VoidResult> {

    private static final long serialVersionUID = -4359238149372926905L;
    private ClientRegistration cr;

    public CreateUserAction() {
    }

    public CreateUserAction(ClientRegistration cr) {
        this.cr = cr;
    }

    public ClientRegistration getClientRegistration() {
        return cr;
    }
}
