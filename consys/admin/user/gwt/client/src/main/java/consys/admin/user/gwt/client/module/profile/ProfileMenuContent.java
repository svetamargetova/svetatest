package consys.admin.user.gwt.client.module.profile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.ui.comp.panel.MenuFormPanel;

/**
 * Zalozkovy panel s nastavenimi v profilu uzivatele
 * @author pepa
 */
public class ProfileMenuContent extends MenuFormPanel {

    // instance
    private static ProfileMenuContent instance;

    private ProfileMenuContent() {
        super(AUMessageUtils.c.profileMenuContent_title());
        addItem(new AboutYourself());
        addItem(new Experience());
        addItem(new AccountMenu());
        initWidget();
    }

    /** vytvori asynchronne instanci */
    public static void getAsync(final AsyncModule<ProfileMenuContent> asyncModule) {
        GWT.runAsync(new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable reason) {
                asyncModule.onFail();
            }

            @Override
            public void onSuccess() {
                if (instance == null) {
                    instance = new ProfileMenuContent();
                }
                asyncModule.onSuccess(instance);
            }
        });
    }
}
