package consys.admin.user.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class ActivateUserAction implements Action<VoidResult> {

    private static final long serialVersionUID = -5167021366265822700L;
    private String key;

    public ActivateUserAction() {
    }

    public ActivateUserAction(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
