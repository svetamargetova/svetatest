package consys.admin.user.gwt.client.module.profile.account;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.LoadPersonalInfoAction;
import consys.admin.user.gwt.client.action.UpdatePersonalInfoAction;
import consys.common.gwt.shared.bo.ClientPersonalInfo;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 *
 * @author pepa
 */
public class PersonalInfo extends RUSimpleHelpFormPanel {

    // komponenty
    private ConsysCheckBox celiacBox;
    private ConsysCheckBox vegetarianBox;

    public PersonalInfo() {
        super(true);
        //addHelpTitle(AUMessageUtils.c.personalInfo_helpTitle());
        //addHelpText(AUMessageUtils.c.personalInfo_helpText());
    }

    @Override
    public Widget readForm() {
        final BaseForm bf = new BaseForm();
        bf.addStyleName(MARGIN_BOT_10);

        FlowPanel panel = new FlowPanel();
        panel.add(FormPanelUtils.getOneLinedReadMode(this, AUMessageUtils.c.personalInfo_title(), UIMessageUtils.c.const_edit()));
        panel.add(bf);

        EventBus.get().fireEvent(new DispatchEvent(new LoadPersonalInfoAction(),
                new AsyncCallback<ClientPersonalInfo>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                    }

                    @Override
                    public void onSuccess(ClientPersonalInfo result) {
                        bf.addOptional(0, UIMessageUtils.c.const_personalInfo_celiac(), yesNoLabel(result.isCeliac()));
                        bf.addOptional(1, UIMessageUtils.c.const_personalInfo_vegetarian(), yesNoLabel(result.isVegetarian()));
                    }
                }, this));

        return panel;
    }

    private Label yesNoLabel(boolean yes) {
        return new Label(UIMessageUtils.yesNo(yes));
    }

    @Override
    public Widget updateForm(String id) {
        Label titleLabel = StyleUtils.getStyledLabel(AUMessageUtils.c.personalInfo_title(), FONT_16PX);
        titleLabel.addStyleName(FONT_BOLD);
        titleLabel.addStyleName(MARGIN_BOT_20);

        celiacBox = new ConsysCheckBox();
        vegetarianBox = new ConsysCheckBox();

        final BaseForm bf = new BaseForm();
        bf.setStyleName(StyleUtils.MARGIN_BOT_10);
        bf.addOptional(0, UIMessageUtils.c.const_personalInfo_celiac(), celiacBox);
        bf.addOptional(1, UIMessageUtils.c.const_personalInfo_vegetarian(), vegetarianBox);
        bf.addActionMembers(2, FormPanelUtils.getUpdateButton(this, id), FormPanelUtils.getCancelButton(this));

        EventBus.get().fireEvent(new DispatchEvent(new LoadPersonalInfoAction(),
                new AsyncCallback<ClientPersonalInfo>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                    }

                    @Override
                    public void onSuccess(ClientPersonalInfo result) {
                        celiacBox.setValue(result.isCeliac());
                        vegetarianBox.setValue(result.isVegetarian());
                    }
                }, this));

        FlowPanel panel = new FlowPanel();
        panel.add(titleLabel);
        panel.add(bf);
        return panel;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ClientPersonalInfo data = new ClientPersonalInfo();
                data.setCeliac(celiacBox.getValue());
                data.setVegetarian(vegetarianBox.getValue());
                EventBus.get().fireEvent(new DispatchEvent(new UpdatePersonalInfoAction(data),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                onSuccessUpdate(AUMessageUtils.c.personalInfo_text_successPersonalInfoUpdate());
                            }
                        }, PersonalInfo.this));
            }
        };
    }
}
