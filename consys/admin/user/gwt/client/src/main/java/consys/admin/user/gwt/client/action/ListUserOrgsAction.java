package consys.admin.user.gwt.client.action;

import consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class ListUserOrgsAction implements Action<ArrayListResult<ClientUserOrgProfileThumb>> {

    private static final long serialVersionUID = 8020615598987833495L;
}
