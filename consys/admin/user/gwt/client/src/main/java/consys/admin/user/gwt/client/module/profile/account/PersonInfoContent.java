package consys.admin.user.gwt.client.module.profile.account;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.admin.user.gwt.client.action.UpdateUserAction;
import consys.common.gwt.shared.bo.ClientUser;
import consys.admin.user.gwt.client.event.UpdatedUserEvent;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.widget.validator.CWValidator;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 * Formular pro zadani zakladnich informacich o jmenu a titulech
 * @author pepa
 */
public class PersonInfoContent extends RUSimpleFormPanel implements UpdatedUserEvent.Handler {

    // komponenty prohlizeni
    private Label frontDegreeLabel;
    private Label nameLabel;
    private Label rearDegreeLabel;
    private Label positionLabel;
    private Label contactEmailLabel;
    // komponenty editace
    private BaseForm form;
    private TextBox firstNameBox;
    private TextBox middleNameBox;
    private TextBox lastNameBox;
    private TextBox frontDegreeBox;
    private TextBox rearDegreeBox;
    private TextBox emailBox;
    private RadioButton notSpecBox;
    private RadioButton manBox;
    private RadioButton womanBox;
    // data
    private AsyncCallback loadUserCallback;
    private AsyncCallback viewUserCallback;
    private ClientUser user;
    private boolean enterEmail;

    public PersonInfoContent() {
        super(true);
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        EventBus.get().addHandler(UpdatedUserEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(UpdatedUserEvent.TYPE, this);
    }

    @Override
    public Widget updateForm(String id) {
        form = new BaseForm();
        form.setStyleName(StyleUtils.MARGIN_BOT_10);

        firstNameBox = StyleUtils.getFormInput();
        middleNameBox = StyleUtils.getFormInput();
        lastNameBox = StyleUtils.getFormInput();
        frontDegreeBox = StyleUtils.getFormInputHalf();
        rearDegreeBox = StyleUtils.getFormInputHalf();
        emailBox = StyleUtils.getFormInput();
        notSpecBox = new RadioButton("gender");
        notSpecBox.setValue(true);
        manBox = new RadioButton("gender");
        womanBox = new RadioButton("gender");

        Label middleLabel = StyleUtils.getStyledLabel(AUMessageUtils.c.personInfoContent_form_rearDegree() + ":", StyleUtils.MARGIN_RIGHT_10);

        HorizontalPanel degreePanel = new HorizontalPanel();
        degreePanel.setWidth("100%");
        degreePanel.setStyleName(StyleUtils.HEIGHT_21);
        degreePanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        degreePanel.add(frontDegreeBox);
        degreePanel.add(middleLabel);
        degreePanel.add(rearDegreeBox);
        degreePanel.setCellHorizontalAlignment(middleLabel, HasAlignment.ALIGN_RIGHT);
        degreePanel.setCellHorizontalAlignment(rearDegreeBox, HasAlignment.ALIGN_RIGHT);

        HorizontalPanel genderPanel = new HorizontalPanel();
        genderPanel.setWidth("100%");
        genderPanel.setStyleName(StyleUtils.HEIGHT_21);
        genderPanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        genderPanel.add(notSpecBox);
        genderPanel.setCellHorizontalAlignment(notSpecBox, HasAlignment.ALIGN_RIGHT);
        genderPanel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_genderNotSpec(), StyleUtils.MARGIN_LEFT_5));
        genderPanel.add(manBox);
        genderPanel.setCellHorizontalAlignment(manBox, HasAlignment.ALIGN_RIGHT);
        genderPanel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_genderMan(), StyleUtils.MARGIN_LEFT_5));
        genderPanel.add(womanBox);
        genderPanel.setCellHorizontalAlignment(womanBox, HasAlignment.ALIGN_RIGHT);
        genderPanel.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_genderWoman(), StyleUtils.MARGIN_LEFT_5));

        form.addRequired(0, CWMessageUtils.c.const_firstName(), firstNameBox);
        form.addOptional(1, CWMessageUtils.c.const_middleName(), middleNameBox);
        form.addRequired(2, CWMessageUtils.c.const_lastName(), lastNameBox);
        form.addOptional(3, AUMessageUtils.c.personInfoContent_form_frontDegree(), degreePanel);
        form.addOptional(4, UIMessageUtils.c.const_gender(), genderPanel);
        form.addActionMembers(6, FormPanelUtils.getUpdateButton(this, id), FormPanelUtils.getCancelButton(this));

        loadUserCallback = new AsyncCallback<ClientUser>() {

            @Override
            public void onFailure(Throwable caught) {
                // TODO: co delat, kdyz bude chyba?
            }

            @Override
            public void onSuccess(ClientUser result) {
                user = result;
                firstNameBox.setText(result.getFirstName());
                middleNameBox.setText(result.getMiddleName());
                lastNameBox.setText(result.getLastName());
                frontDegreeBox.setText(result.getFrontDegree());
                rearDegreeBox.setText(result.getRearDegree());

                String email = result.getLoginEmail();
                if (email.contains("@")) {
                    emailBox.setText(email);
                    emailBox.setReadOnly(true);
                    form.addOptional(5, AUMessageUtils.c.personInfoContent_form_contactEmail(), emailBox);
                    enterEmail = false;
                } else {
                    emailBox.setReadOnly(false);
                    form.addRequired(5, AUMessageUtils.c.personInfoContent_form_contactEmail(), emailBox);
                    enterEmail = true;
                }

                switch (result.getGender()) {
                    case 0:
                        notSpecBox.setValue(true);
                        break;
                    case 1:
                        manBox.setValue(true);
                        break;
                    case 2:
                        womanBox.setValue(true);
                        break;
                }
            }
        };

        EventBus.get().fireEvent(new DispatchEvent(new LoadLoggedUserAction(), loadUserCallback, getSelf()));
        return form;
    }

    @Override
    public Widget readForm() {
        FlowPanel viewPanel = new FlowPanel();
        viewPanel.setStyleName(StyleUtils.MARGIN_BOT_10);
        viewPanel.addStyleName(FLOAT_LEFT);

        ProfileImageContent portraitPanel = new ProfileImageContent(getHiddableLabel());
        portraitPanel.addStyleName(FLOAT_RIGHT);
        EventBus.get().addHandler(UpdatedUserEvent.TYPE, portraitPanel);

        frontDegreeLabel = new Label() {

            @Override
            public void setText(String text) {
                super.setText(text);
                setVisible(!(text == null || text.isEmpty()));
            }
        };
        frontDegreeLabel.addStyleName(MARGIN_RIGHT_10);
        frontDegreeLabel.addStyleName(MARGIN_TOP_3);
        frontDegreeLabel.addStyleName(FLOAT_LEFT);
        nameLabel = StyleUtils.getStyledLabel("", FONT_16PX, FONT_BOLD, FLOAT_LEFT);
        rearDegreeLabel = StyleUtils.getStyledLabel("", MARGIN_LEFT_10, FLOAT_LEFT, MARGIN_TOP_3);

        ActionLabel edit = FormPanelUtils.getEditModeButton(this, null);
        edit.addStyleName(FLOAT_LEFT);
        edit.addStyleName(MARGIN_LEFT_10);
        edit.addStyleName(MARGIN_TOP_3);

        FlowPanel panel = new FlowPanel();
        panel.setWidth("400px");
        panel.add(frontDegreeLabel);
        panel.add(nameLabel);
        panel.add(rearDegreeLabel);
        panel.add(edit);
        panel.add(StyleUtils.clearDiv());
        viewPanel.add(panel);

        positionLabel = StyleUtils.getStyledLabel("", StyleUtils.MARGIN_LEFT_20, StyleUtils.MARGIN_TOP_10);
        viewPanel.add(positionLabel);

        contactEmailLabel = StyleUtils.getStyledLabel("", StyleUtils.MARGIN_LEFT_20, StyleUtils.MARGIN_TOP_10);
        viewPanel.add(contactEmailLabel);

        viewUserCallback = new AsyncCallback<ClientUser>() {

            @Override
            public void onFailure(Throwable caught) {
                // TODO: co udelat, kdyz nenajde?
            }

            @Override
            public void onSuccess(ClientUser result) {
                String rear = result.getRearDegree() == null ? "" : result.getRearDegree();
                frontDegreeLabel.setText(result.getFrontDegree());
                nameLabel.setText(rear.isEmpty() ? result.name() : result.name() + ",");
                rearDegreeLabel.setText(rear);
                positionLabel.setText(result.getUserOrgDefault());
                if (result.getLoginEmail().contains("@")) {
                    contactEmailLabel.setText(result.getLoginEmail());
                }
            }
        };

        EventBus.get().fireEvent(new DispatchEvent(new LoadLoggedUserAction(), viewUserCallback, getSelf()));

        FlowPanel wrapper = new FlowPanel();
        wrapper.setWidth(LayoutManager.LAYOUT_CONTENT_WIDTH);
        wrapper.add(viewPanel);
        wrapper.add(portraitPanel);
        wrapper.add(StyleUtils.clearDiv());
        wrapper.setVisible(true);
        return wrapper;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final ClientUser cu = generateClientUser();

                if (!CWValidator.updateUser(cu, getFailMessage())) {
                    return;
                }

                if(enterEmail && !ValidatorUtils.isValidEmailAddress(cu.getLoginEmail())) {
                    getFailMessage().addOrSetText(UIMessageUtils.c.const_error_invalidEmailAddress());
                    return;
                }

                EventBus.get().fireEvent(new DispatchEvent(new UpdateUserAction(cu), new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovany ActionExecutorem
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        onSuccessUpdate(AUMessageUtils.c.personInfoContent_text_successPersonalInfoUpdate());
                    }
                }, getSelf()));

            }
        };
    }

    /** vygeneruje ClientUser z dat ve formulari */
    private ClientUser generateClientUser() {
        ClientUser result = user;
        result.setFirstName(firstNameBox.getText().trim());
        result.setMiddleName(middleNameBox.getText().trim());
        result.setLastName(lastNameBox.getText().trim());
        result.setFrontDegree(frontDegreeBox.getText().trim());
        result.setRearDegree(rearDegreeBox.getText().trim());
        if (enterEmail) {
            result.setLoginEmail(emailBox.getText().trim());
        }

        int gender = 0;

        if (manBox.getValue()) {
            gender = 1;
        } else if (womanBox.getValue()) {
            gender = 2;
        }
        result.setGender(gender);
        return result;
    }

    @Override
    public void onUpdatedUser(UpdatedUserEvent event) {
        user = event.getUser();
        frontDegreeLabel.setText(user.getFrontDegree());
        nameLabel.setText(user.name());
        rearDegreeLabel.setText(user.getRearDegree());
    }
}
