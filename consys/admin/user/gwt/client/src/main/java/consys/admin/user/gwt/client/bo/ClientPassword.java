package consys.admin.user.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Objekt pro zmenu hesla
 * @author pepa
 */
public class ClientPassword implements IsSerializable {

    private String oldPassword;
    private String newPassword;
    private String confirmPassword;

    public ClientPassword() {
    }

    

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
