package consys.admin.user.gwt.client.head;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.admin.user.gwt.client.action.UpdateUserAction;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.admin.user.gwt.client.utils.AUResourceUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.shared.bo.ClientUser;

/**
 *
 * @author pepa
 */
public class FillEmailDialog extends SmartDialog {

    // komponenty
    private ConsysStringTextBox emailBox;
    private BaseForm form;

    @Override
    protected void generateContent(SimpleFormPanel panel) {
        emailBox = new ConsysStringTextBox(6, 255, AUMessageUtils.c.fillEmailDialog_text_email());

        form = new BaseForm();
        form.addRequired(0, AUMessageUtils.c.fillEmailDialog_text_email(), emailBox);
        form.addActionMembers(1, button(), null);

        panel.addWidget(StyleUtils.clearDiv("10px"));
        panel.addWidget(StyleUtils.getStyledLabel(AUMessageUtils.c.headProfileRow_action_notFillEmail(), AUResourceUtils.css().fillEmailDialogTitle()));
        panel.addWidget(form);
        panel.addWidget(StyleUtils.clearDiv("10px"));
    }

    private ActionImage button() {
        ActionImage button = ActionImage.getConfirmButton(AUMessageUtils.c.fillEmailDialog_action_updateEmail());
        button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new DispatchEvent(new LoadLoggedUserAction(), clientUserCallback(), FillEmailDialog.this));
            }
        });
        return button;
    }

    private AsyncCallback<ClientUser> clientUserCallback() {
        return new AsyncCallback<ClientUser>() {

            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(ClientUser result) {
                if (!form.validate(panel.getFailMessage())) {
                    return;
                }

                final String email = emailBox.getText();
                if (!ValidatorUtils.isValidEmailAddress(email)) {
                    panel.getFailMessage().setText(UIMessageUtils.c.const_error_invalidEmailAddress());
                    return;
                }

                result.setLoginEmail(email);
                EventBus.fire(new DispatchEvent(new UpdateUserAction(result), new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        // dialog se zavre z mista volani
                    }
                }, FillEmailDialog.this));
            }
        };
    }
}
