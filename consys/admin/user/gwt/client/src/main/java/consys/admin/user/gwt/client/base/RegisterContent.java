package consys.admin.user.gwt.client.base;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.CreateUserAction;
import consys.common.gwt.client.widget.exception.UserExistsException;
import consys.common.gwt.client.widget.AccountRegisterForm;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.widget.validator.PasswordValidator;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.comp.progress.Progress;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 * Prvni krok zadani uzivatele do systemu
 * @author pepa
 */
public class RegisterContent extends RootPanel {

    // komponenty
    private AccountRegisterForm registerComp;

    public RegisterContent() {
        super(AUMessageUtils.c.registerContent_title());

        SimpleFormWithHelpPanel mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        ActionImage button = ActionImage.getConfirmButton(AUMessageUtils.c.registerContent_button_registerAccount());
        button.addStyleName(StyleUtils.MARGIN_TOP_10);
        button.addClickHandler(confirmClickHandler());

        registerComp = new AccountRegisterForm("140px", true);
        registerComp.addActionMembers(AccountRegisterForm.ACTION_MEMBER_ROW, button, null);
        mainPanel.addWidget(registerComp);

        mainPanel.addHelpTitle(AUMessageUtils.c.registerContent_helpTitle());
        mainPanel.addHelpText(AUMessageUtils.c.registerContent_helpText1());
        mainPanel.addHelpText(AUMessageUtils.c.registerContent_helpText2());
        mainPanel.addHelpText(AUMessageUtils.c.registerContent_helpText3());
    }

    @Override
    protected void onLoad() {
        PanelDispatcher.get().generatePanel();
    }

    @Override
    protected Widget createFootPart() {
        Progress progress = Progress.register(1);
        progress.setStyleName(StyleUtils.MARGIN_TOP_20);
        progress.addStyleName(StyleUtils.MARGIN_BOT_10);
        return progress;
    }

    /** ClickHandler pro odeslani registrace */
    private ClickHandler confirmClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ClientRegistration registration = registerComp.generateRegistration();

                if (!PasswordValidator.permitedCharacters(registration.getPassword())) {
                    getFailMessage().setText(AUMessageUtils.c.changePasswordContent_error_invalidCharactersInPassword());
                    return;
                }

                if (!registerComp.validate(getFailMessage())) {
                    return;
                }

                if (!ValidatorUtils.isValidEmailAddress(registration.getContactEmail())) {
                    getFailMessage().addOrSetText(UIMessageUtils.c.const_error_invalidEmailAddress());
                    return;
                }

                DispatchEvent createUserEvent = new DispatchEvent(new CreateUserAction(registration), new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovany ActionExecutorem
                        if (caught instanceof UserExistsException) {
                            getFailMessage().setText(CWMessageUtils.c.registerContent_error_alreadyRegistered());
                        }
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        registerComp.clearData();
                        // prepnuti contentu
                        ChangeContentEvent event = new ChangeContentEvent(new ActivateContent());
                        EventBus.get().fireEvent(event);
                    }
                }, getSelf());
                EventBus.get().fireEvent(createUserEvent);
            }
        };
    }
}
