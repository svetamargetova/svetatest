package consys.admin.user.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 * Akce pro uzivateluv pozadavek na zaslani aktivacniho mailu
 * @author pepa
 */
public class ActivateUserRequestAction implements Action<VoidResult> {

    private static final long serialVersionUID = 4098361459685318246L;
    // data
    private String userUuid;

    public ActivateUserRequestAction() {
    }

    public ActivateUserRequestAction(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getUserUuid() {
        return userUuid;
    }
}
