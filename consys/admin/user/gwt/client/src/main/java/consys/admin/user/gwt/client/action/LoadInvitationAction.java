package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.gwt.shared.action.Action;

/**
 * Akce pro nacteni pozvanky
 * @author pepa
 */
public class LoadInvitationAction implements Action<ClientRegistration> {

    private static final long serialVersionUID = 4937637118403431061L;
    // data
    private String token;

    public LoadInvitationAction() {
    }

    public LoadInvitationAction(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
