package consys.admin.user.gwt.client.base.faq;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import consys.admin.user.gwt.client.utils.AUResourceUtils;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.RollAnimation;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBaseWrapper;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class FAQItem extends FlowPanel {

    // data
    private RollAnimation animation;

    public FAQItem(String q, String a) {
        // odpoved
        final HTML answer = new HTML(a);
        answer.addStyleName(AUResourceUtils.css().faqAnswer());
        final FlowPanel answerPanel = new FlowPanel();
        answerPanel.addStyleName(StyleUtils.MARGIN_BOT_10);
        answerPanel.add(answer);
        answerPanel.setVisible(false);

        // otazka
        final Label state = StyleUtils.getStyledLabel("+", AUResourceUtils.css().faqState());
        state.setWidth("8px");
        Label question = StyleUtils.getStyledLabel(q, AUResourceUtils.css().faqQuestion());
        question.setWidth("650px");
        FlowPanel questionPanel = new FlowPanel();
        questionPanel.add(state);
        questionPanel.add(question);
        questionPanel.add(StyleUtils.clearDiv());

        FocusPanel clickPanel = new FocusPanel(questionPanel);
        clickPanel.addStyleName(StyleUtils.HAND);
        clickPanel.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 6) + "px");
        clickPanel.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (animation == null) {
                    animation = new RollAnimation();
                }
                if (answerPanel.isVisible()) {
                    animation.rollWidget(answer, answerPanel, true);
                    state.setText("+");
                } else {
                    animation.rollWidget(answer, answerPanel, false);
                    state.setText("-");
                }
            }
        });

        ConsysBaseWrapper qWrapper = new ConsysBaseWrapper(clickPanel);
        qWrapper.addStyleName(StyleUtils.MARGIN_BOT_10);

        add(qWrapper);
        add(answerPanel);
    }
}
