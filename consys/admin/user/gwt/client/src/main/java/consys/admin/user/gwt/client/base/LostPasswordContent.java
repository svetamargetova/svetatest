package consys.admin.user.gwt.client.base;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TextBox;
import consys.admin.user.gwt.client.action.LostPasswordAction;
import consys.admin.user.gwt.client.action.exception.UserNotExistsException;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Formular pro vyzadani zmeny zapomenuteho hesla
 * @author pepa
 */
public class LostPasswordContent extends RootPanel {

    // komponenty
    private TextBox emailBox;

    public LostPasswordContent() {
        super(AUMessageUtils.c.lostPasswordContent_title());

        SimpleFormWithHelpPanel mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        emailBox = StyleUtils.getFormInput();

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_sendUpper());
        button.addStyleName(StyleUtils.MARGIN_TOP_10);
        button.addClickHandler(sendClickHandler());

        BaseForm form = new BaseForm();
        form.addContent(0, StyleUtils.getStyledLabel(AUMessageUtils.c.lostPasswordContent_text1(), MARGIN_RIGHT_20, MARGIN_BOT_10));
        form.addContent(1, StyleUtils.getStyledLabel(AUMessageUtils.c.lostPasswordContent_text2(), MARGIN_RIGHT_20, MARGIN_BOT_10));
        form.addContent(2, emailBox);
        form.addActionMembers(3, button, null);
        mainPanel.addWidget(form);

        mainPanel.addHelpTitle(AUMessageUtils.c.lostPasswordContent_helpTitle());
        mainPanel.addHelpText(AUMessageUtils.c.lostPasswordContent_helpText1());
        mainPanel.addHelpText(AUMessageUtils.c.lostPasswordContent_helpText2());
        mainPanel.addHelpText(AUMessageUtils.c.lostPasswordContent_helpText3());
    }

    /** vraci ClickHandler pro zaslani noveho hesla */
    private ClickHandler sendClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                DispatchEvent lostPasswordEvent = new DispatchEvent(new LostPasswordAction(emailBox.getText().trim()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovany ActionExecutorem
                                if (caught instanceof UserNotExistsException) {
                                    getSelf().getFailMessage().setText(AUMessageUtils.c.lostPasswordContent_error_userNotExists());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                emailBox.setText("");
                                getSuccessMessage().setText(AUMessageUtils.c.lostPasswordContent_text_lostPasswordSendSuccess());
                            }
                        }, getSelf());
                EventBus.get().fireEvent(lostPasswordEvent);
            }
        };
    }
}
