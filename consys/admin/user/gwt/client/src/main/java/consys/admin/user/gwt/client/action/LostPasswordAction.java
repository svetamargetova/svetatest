package consys.admin.user.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 * 
 * @author pepa
 */
public class LostPasswordAction implements Action<VoidResult> {

    private static final long serialVersionUID = 5175861516374340415L;
    private String email;

    public LostPasswordAction() {
    }

    public LostPasswordAction(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
