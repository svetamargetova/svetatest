package consys.admin.user.gwt.client.head;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.admin.user.gwt.client.action.ActivateUserRequestAction;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.admin.user.gwt.client.base.ActivateContent;
import consys.admin.user.gwt.client.event.UpdatedUserEvent;
import consys.admin.user.gwt.client.module.UserModule;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ServletConstants;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.event.UserLogoutEvent;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.layout.menu.MenuDispatcher;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;

/**
 * Panel zobrazeny v hlavicce aplikace (jmeno prihlaseneho uzivatele + odhlaseni)
 * @author pepa
 */
public class HeadProfileRow extends FlowPanel implements UpdatedUserEvent.Handler, UserLoginEvent.Handler, UserLogoutEvent.Handler {

    // komponenty
    private ActionLabel userName;
    private ActionLabel notActivated;
    private Label notActivatedSeparator;
    private FillEmailDialog fed;
    // data
    private String userUuid;

    public HeadProfileRow() {
        super();
        setStyleName(ResourceUtils.system().css().headProfile());
        DOM.setStyleAttribute(getElement(), "maxWidth", "570px");

        notActivated = new ActionLabel(AUMessageUtils.c.headProfileRow_action_accountNotActivated(), StyleUtils.FLOAT_LEFT);
        notActivated.setClickHandler(notActivatedHandler());
        notActivated.setVisible(false);
        DOM.setStyleAttribute(notActivated.getElement(), "color", "red");

        notActivatedSeparator = separator();

        userName = new ActionLabel(UIMessageUtils.c.const_loadingDots(), StyleUtils.FLOAT_LEFT);
        userName.setClickHandler(profileHandler());

        ActionLabel logout = new ActionLabel(AUMessageUtils.c.headProfileRow_action_logOut(), StyleUtils.FLOAT_LEFT);
        logout.setClickHandler(logoutUserHandler());

        add(notActivated);
        add(notActivatedSeparator);
        add(userName);
        add(StyleUtils.getStyledLabel("|", StyleUtils.MARGIN_HOR_5, StyleUtils.FLOAT_LEFT));
        add(logout);

        setVisible(false);
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(UpdatedUserEvent.TYPE, this);
        EventBus.get().addHandler(UserLoginEvent.TYPE, this);
        EventBus.get().addHandler(UserLogoutEvent.TYPE, this);
    }

    private Label separator() {
        Label label = StyleUtils.getStyledLabel("|", StyleUtils.MARGIN_HOR_5, StyleUtils.FLOAT_LEFT);
        label.setVisible(false);
        return label;
    }

    /** vraci ClickHandler, ktery prepne do profilu */
    private ClickHandler profileHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(UserModule.PROFILE);
            }
        };
    }

    /** vraci ClickHandler, ktery spusti akce pro aktivaci uzivatelskeho uctu */
    private ClickHandler notActivatedHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (userUuid != null) {
                    EventBus.get().fireEvent(new DispatchEvent(new ActivateUserRequestAction(userUuid),
                            new AsyncCallback<VoidResult>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    // TODO: vyhodit asi dialog, ze nejde poslat aktivacni mail
                                }

                                @Override
                                public void onSuccess(VoidResult result) {
                                    EventBus.get().fireEvent(new ChangeContentEvent(new ActivateContent(false,
                                            new ConsysAction() {

                                                @Override
                                                public void run() {
                                                    try {
                                                        ClientUser cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
                                                        cu.setActivatedUser(true);
                                                    } catch (NotInCacheException ex) {
                                                        // nemelo by nikdy nastat
                                                    }
                                                    refresh();
                                                    EventBus.get().fireEvent(new ChangeContentEvent(StyleUtils.getStyledLabel(
                                                            AUMessageUtils.c.headProfileRow_text_accountActivated(),
                                                            StyleUtils.FONT_17PX, StyleUtils.FONT_BOLD)));
                                                }
                                            })));
                                }
                            }, null));
                }
            }
        };
    }

    /** vraci ClickHandler, ktery spusti odhlaseni uzivatele */
    private ClickHandler logoutUserHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new UserLogoutEvent());
                History.newItem(MenuDispatcher.SIGN_UP);
                Window.Location.replace(ServletConstants.BASE_URL + "j_spring_security_logout");
            }
        };
    }

    @Override
    public void onUpdatedUser(UpdatedUserEvent event) {
        userName.setText(event.getUser().name());

        if (event.getUser().getLoginEmail().contains("@")) {
            if (fed != null) {
                fed.hide();
            }
        }
    }

    @Override
    public void onUserLogin(UserLoginEvent event) {
        refresh();
        setVisible(true);
    }

    @Override
    public void onUserLogout(UserLogoutEvent event) {
        setVisible(false);
        userName.setText(UIMessageUtils.c.const_loadingDots());
    }

    /** znovu nacte data o uzivateli */
    private void refresh() {
        EventBus.get().fireEvent(new DispatchEvent(new LoadLoggedUserAction(), new AsyncCallback<ClientUser>() {

            @Override
            public void onFailure(Throwable caught) {
                // chyby sefuje ActionExecutor
            }

            @Override
            public void onSuccess(ClientUser result) {
                userUuid = result.getUuid();
                userName.setText(result.name());
                notActivated.setVisible(!result.isActivatedUser());
                notActivatedSeparator.setVisible(!result.isActivatedUser());

                boolean hasFillEmail = result.getLoginEmail().contains("@");
                if (!hasFillEmail) {
                    showFillEmailDialog();
                }
            }
        }, null));
    }

    private void showFillEmailDialog() {
        fed = new FillEmailDialog();
        fed.setAllowEscapeClose(false);
        fed.showCentered();
    }
}
