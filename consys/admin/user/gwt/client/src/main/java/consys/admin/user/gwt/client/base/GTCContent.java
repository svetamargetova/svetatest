package consys.admin.user.gwt.client.base;

import consys.common.gwt.client.ui.comp.panel.RootPanel;

/**
 * Vseobecne obchodni podminky
 * @author pepa
 */
public class GTCContent extends RootPanel {

    public GTCContent() {
        super("General Terms and Conditions");
    }
}
