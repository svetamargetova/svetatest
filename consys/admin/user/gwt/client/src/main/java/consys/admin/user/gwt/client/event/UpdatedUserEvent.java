package consys.admin.user.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.shared.bo.ClientUser;

/**
 *
 * @author pepa
 */
public class UpdatedUserEvent extends GwtEvent<UpdatedUserEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private ClientUser user;

    public UpdatedUserEvent(ClientUser user) {
        this.user = user;
    }

    /** vraci aktualizovany objekt uzivatele */
    public ClientUser getUser() {
        return user;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onUpdatedUser(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onUpdatedUser(UpdatedUserEvent event);
    }
}
