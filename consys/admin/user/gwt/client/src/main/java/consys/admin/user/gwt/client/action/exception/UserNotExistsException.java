package consys.admin.user.gwt.client.action.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class UserNotExistsException extends ActionException {
    private static final long serialVersionUID = -1724958103914463932L;
 


    public UserNotExistsException() {
        super();
    }
}
