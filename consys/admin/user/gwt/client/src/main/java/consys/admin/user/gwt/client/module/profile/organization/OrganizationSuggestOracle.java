package consys.admin.user.gwt.client.module.profile.organization;


import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction;
import consys.common.gwt.shared.bo.ClientOrganizationThumb;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxResult;
import consys.common.gwt.shared.bo.ClientSuggestion;
import consys.common.gwt.client.ui.comp.ConsysSuggestOracle;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Palo
 */
public class OrganizationSuggestOracle extends ConsysSuggestOracle {

    private static final Logger logger = LoggerFactory.getLogger(OrganizationSuggestOracle.class);

    public OrganizationSuggestOracle() {
        super(1);
        setIsDisplayAsHTML(true);
    }

    @Override
    public void suggest(String query, int limit, final SuggestCallback callback) {
        EventBus.get().fireEvent(new DispatchEvent(new ListOrganizationsByPrefixAction(query, limit), new AsyncCallback<SelectBoxResult<ClientOrganizationThumb>>() {
 
            @Override
            public void onFailure(Throwable caught) {
                logger.debug("Failed download Organizations", caught);
            }

            @Override
            public void onSuccess(SelectBoxResult<ClientOrganizationThumb> result) {
                logger.debug("Successfully suggested " + result.getSelectBoxResult().size() + " organization");
                List<OrganizationSuggestion> suggestions = new ArrayList<OrganizationSuggestion>();
                suggestions.add(new CreateOrganizationSuggestion());

                for (SelectBoxItem<ClientOrganizationThumb> item : result.getSelectBoxResult()) {
                    suggestions.add(new OrganizationSuggestion(item.getItem()));
                }
                callback.reponse(suggestions);
            }
        }, null));
    }

    public static class OrganizationSuggestion implements ClientSuggestion<String>, CssStyles {

        private ClientOrganizationThumb thumb;

        public OrganizationSuggestion(ClientOrganizationThumb thumb) {
            this.thumb = thumb;
        }

        @Override
        public String getSuggestionId() {
            return getThumb().getUuid();
        }

        @Override
        public String getDisplayString() {
            Element wrapper = DOM.createDiv();
            wrapper.appendChild(StyleUtils.getStyledLabel(getThumb().getUniversalName(), FONT_12PX, FONT_BOLD, MARGIN_BOT_1).getElement());
            wrapper.appendChild(StyleUtils.getStyledLabel(getThumb().getAddress(), StyleUtils.FONT_11PX).getElement());
            wrapper.setAttribute("class", StyleUtils.Assemble(PADDING_VER_5, MARGIN_HOR_5));
            wrapper.getStyle().setProperty("textAlign", "left");
            return wrapper.getString();
        }

        @Override
        public String getReplacementString() {
            return getThumb().getUniversalName();
        }

        /**
         * @return the thumb
         */
        public ClientOrganizationThumb getThumb() {
            return thumb;
        }
    }

    public static final class CreateOrganizationSuggestion extends OrganizationSuggestion {

        public static final String CREATE_ORGANIZATION = "CREATE_ORGANIZATION_SUGGESTION";

        public CreateOrganizationSuggestion() {
            super(null);
        }

        @Override
        public String getSuggestionId() {
            return CREATE_ORGANIZATION;
        }

        @Override
        public String getDisplayString() {
            Element wrapper = DOM.createDiv();
            Label l = StyleUtils.getStyledLabel(AUMessageUtils.c.organizationSuggestOracle_text_createNewOrganization(),
                    StyleUtils.Assemble(FONT_BOLD, TEXT_BLUE, FONT_12PX));
            DOM.setStyleAttribute(l.getElement(), "fontStyle", "italic");
            DOM.setStyleAttribute(l.getElement(), "textAlign", "center");
            wrapper.appendChild(l.getElement());
            wrapper.setAttribute("class", StyleUtils.Assemble(PADDING_VER_5));
            return wrapper.getString();
        }

        @Override
        public String getReplacementString() {
            return "";
        }
    }
}
