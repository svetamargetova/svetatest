package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 * Potvrzeni pozvanky
 * @author pepa
 */
public class ConfirmInvitationAction implements Action<VoidResult> {

    private static final long serialVersionUID = 7472208055860951325L;
    // data
    private ClientRegistration registraion;
    private String token;

    public ConfirmInvitationAction() {
    }

    public ConfirmInvitationAction(String token, ClientRegistration registration) {
        this.token = token;
        this.registraion = registration;
    }

    public ClientRegistration getRegistraion() {
        return registraion;
    }

    public String getToken() {
        return token;
    }
}
