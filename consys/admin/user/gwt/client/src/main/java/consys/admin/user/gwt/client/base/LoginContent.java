package consys.admin.user.gwt.client.base;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.widget.LoginForm;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.comp.progress.Progress;
import consys.common.gwt.client.ui.utils.HistoryUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.client.widget.SSOLogin;
import consys.common.gwt.client.widget.utils.CWMessageUtils;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
public class LoginContent extends RootPanel {

    // konstanty
    public static final String PARAM_ACTIVATED = "activated";
    public static final String PARAM_AUTHFAIL = "authfail";
    public static final String PARAM_USERNAME = "username";
    public static final String PARAM_ACCOUNT_LOCKED = "acclocked";
    public static final String PARAM_LOGIN_DISABLED = "logdisabled";
    public static final String PARAM_PASS_CHANGED = "passchanged";
    public static final String PARAM_REDIRECT = "redirect";
    public static final String PARAM_REDIRECT_MESSAGE = "rmsg";
    public static final String PARAM_SSO_DENIED = "sso_denied";
    public static final String PARAM_SSO_UNSUPPORTED = "sso_unsupported";
    public static final String PARAM_SSO_FAILED = "sso_failed";
    public static final String PARAM_SSO_SERVICE = "service";
    // komponenty
    private SSOLogin ssoLogin;
    private SimplePanel foot;

    public LoginContent() {
        super(CWMessageUtils.c.loginContent_title());

        SimpleFormWithHelpPanel mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        ssoLogin = new SSOLogin();

        mainPanel.addWidget(ssoLogin);

        ssoLogin.getLoginComp().enableLostPasswordHint();
        ssoLogin.getLoginComp().enableNotRegistredHint();
        ssoLogin.getLoginComp().enableNotActivatedHint();

        mainPanel.addHelpTitle(AUMessageUtils.c.loginContent_helpTitle());
        mainPanel.addHelpText(AUMessageUtils.c.loginContent_helpText1());
        mainPanel.addHelpText(AUMessageUtils.c.loginContent_helpText2());
        mainPanel.addHelpText(AUMessageUtils.c.loginContent_helpText3());
    }

    @Override
    protected void onLoad() {
        checkHistoryToken();
    }

    @Override
    protected Widget createFootPart() {
        foot = new SimplePanel();
        return foot;
    }

    /** zkontroluje parametry v history tokenu a zaridi podle nich chování obsahu */
    private void checkHistoryToken() {
        if (HistoryUtils.hasParameters()) {
            HashMap<String, String> params = HistoryUtils.getParameters();
            if (params.get(PARAM_AUTHFAIL) != null) {
                getFailMessage().setText(CWMessageUtils.c.loginContent_error_authFail());
                String login = params.get(PARAM_USERNAME);
                if (login != null && ssoLogin.getLoginComp().getUserBox() != null) {
                    ssoLogin.getLoginComp().getUserBox().setText(login);
                }
                LayoutManager.get().showWaiting(false);
                ssoLogin.getLoginComp().getPasswordBox().setFocus(true);
            }
            if (params.get(PARAM_ACTIVATED) != null) {
                getSuccessMessage().setText(AUMessageUtils.c.loginContent_text_activationSuccessfull());
                Progress progress = Progress.register(3);
                progress.setStyleName(StyleUtils.MARGIN_TOP_20);
                progress.addStyleName(StyleUtils.MARGIN_BOT_10);
                foot.setWidget(progress);
            }
            if (params.get(PARAM_PASS_CHANGED) != null) {
                getSuccessMessage().setText(AUMessageUtils.c.loginContent_text_lostPasswordChanged());
            }
            if (params.get(PARAM_ACCOUNT_LOCKED) != null) {
                getFailMessage().setText(CWMessageUtils.c.loginContent_error_accountLocked());
                LayoutManager.get().showWaiting(false);
            }
            if (params.get(PARAM_LOGIN_DISABLED) != null) {
                getFailMessage().setText(CWMessageUtils.c.loginContent_error_loginDisabled());
                LayoutManager.get().showWaiting(false);
            }
            if (params.get(PARAM_SSO_DENIED) != null) {
                getFailMessage().setText(CWMessageUtils.m.loginContent_error_ssoDenied(params.get(PARAM_SSO_SERVICE)));
                LayoutManager.get().showWaiting(false);
            }
            if (params.get(PARAM_SSO_UNSUPPORTED) != null) {
                getFailMessage().setText(CWMessageUtils.c.loginContent_error_ssoUnsupported());
                LayoutManager.get().showWaiting(false);
            }
            if (params.get(PARAM_SSO_FAILED) != null) {
                getFailMessage().setText(CWMessageUtils.m.loginContent_error_ssoFailed(params.get(PARAM_SSO_SERVICE)));
                LayoutManager.get().showWaiting(false);
            }
            if (params.get(PARAM_REDIRECT) != null) {
                String redirect = params.get(PARAM_REDIRECT);
                if (StringUtils.isNotBlank(redirect)) {
                    String msg = params.get(PARAM_REDIRECT_MESSAGE);
                    if (StringUtils.isNotBlank(msg)) {
                        getSuccessMessage().setText(msg);
                    }
                    ssoLogin.getLoginComp().setSuccRedirect(redirect);
                }
            }
        }
    }
}
