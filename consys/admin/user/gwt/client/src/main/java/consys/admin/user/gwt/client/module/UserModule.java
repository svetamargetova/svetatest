package consys.admin.user.gwt.client.module;

import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.ListOrganizationTypesAction;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.admin.user.gwt.client.action.LoadPersonalInfoAction;
import consys.admin.user.gwt.client.action.UpdatePersonalInfoAction;
import consys.admin.user.gwt.client.action.UpdateUserAction;
import consys.admin.user.gwt.client.base.ActivateContent;
import consys.admin.user.gwt.client.base.ChangePasswordContent;
import consys.admin.user.gwt.client.base.GTCContent;
import consys.admin.user.gwt.client.base.InvitationContent;
import consys.admin.user.gwt.client.base.LoginContent;
import consys.admin.user.gwt.client.base.LostPasswordContent;
import consys.admin.user.gwt.client.base.RegisterContent;
import consys.admin.user.gwt.client.cache.handler.LoadLoggedUserHandler;
import consys.admin.user.gwt.client.cache.handler.LoadPersonalInfoHandler;
import consys.admin.user.gwt.client.cache.handler.OrganizationTypesListHandler;
import consys.admin.user.gwt.client.cache.handler.UpdatePersonalInfoHandler;
import consys.admin.user.gwt.client.cache.handler.UpdateUserHandler;
import consys.admin.user.gwt.client.head.HeadProfileRow;
import consys.admin.user.gwt.client.module.profile.ProfileMenuContent;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.CacheRegistrator;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.module.AsyncContent;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.action.ListUsStateAction;
import consys.common.gwt.client.rpc.action.cache.ListLocationCacheHandler;
import consys.common.gwt.client.rpc.action.cache.ListUsStateCacheHandler;
import consys.common.gwt.client.ui.CommonHistoryHandler;
import consys.common.gwt.client.ui.CommonHistoryItem;
import consys.common.gwt.client.ui.base.LoginSuccessContent;
import consys.common.gwt.client.ui.event.FireAfterLoginEvent;
import consys.common.gwt.client.ui.layout.LayoutHead;
import consys.common.gwt.client.ui.event.UnknownUriFragmentTokenEvent;
import consys.common.gwt.client.widget.CommonWidgetsModule;
import java.util.Date;

/**
 * Modul spravujici uzivateluv profil
 * @author pepa
 */
public class UserModule implements Module, UnknownUriFragmentTokenEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(UserModule.class);
    // konstanty        
    /** pro zobrazeni prihlasovaciho formu */
    public static final String LOG_IN = "LogIn";
    /** pro zobrazeni formu na zmenu zapomenuteho hesla */
    public static final String LOST_PASSWORD_CONFIRM = "ChangePassword";
    /** pro zobrazeni profilu prihlaseneho uzivatele */
    public static final String PROFILE = "Profile";
    public static final String GENERAL_TERMS_AND_CONDITIONS = "GTC";
    /** pro zobrazeni pozvanky (musi tez obsahovat spravny parametr tokenu) */
    public static final String INVITATION = "Invitation";
    /** pro prepnuti do pozvanek (pokracuje konstantou v EventModule (admin/event)) */
    public static final String INVITATIONS = "Invitations";
    /** nazev cookie ktery uchovava redirect uzivatelu prihlasovanych pres sso */
    public static final String SSO_LOGIN_COOKIE = "SSOLoginURL";

    public Widget getRootWidget() {
        AsyncContent content = new AsyncContent() {
            @Override
            public void runComponent() {
                ProfileMenuContent.getAsync(new AsyncModule<ProfileMenuContent>() {
                    @Override
                    public void onSuccess(ProfileMenuContent instance) {
                        ChangeContentEvent event = new ChangeContentEvent(instance);
                        EventBus.get().fireEvent(event);
                    }

                    @Override
                    public void onFail() {
                        // TODO: fail
                    }
                });
            }
        };
        return content;
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
        CommonHistoryHandler history = CommonHistoryHandler.get();
        history.registerOutToken(CommonWidgetsModule.REGISTER_ACCOUNT, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new RegisterContent();
            }
        });
        history.registerOutToken(CommonWidgetsModule.ACTIVATE_ACCOUNT, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new ActivateContent();
            }
        });
        history.registerOutToken(LOG_IN, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new LoginContent();
            }
        });
        history.registerOutToken(CommonWidgetsModule.LOST_PASSWORD, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new LostPasswordContent();
            }
        });
        history.registerOutToken(LOST_PASSWORD_CONFIRM, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new ChangePasswordContent();
            }
        });
        history.registerOutToken(GENERAL_TERMS_AND_CONDITIONS, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new GTCContent();
            }
        });
        history.registerOutToken(INVITATION, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new InvitationContent();
            }
        });

        // Registracia modulu
        menuRegistrator.registerMenuItem(new ModuleMenuItem(getRootWidget(), AUMessageUtils.c.userModule_mainMenu_profile(),
                PROFILE, 1));

        // Registracia event hadleru
        EventBus.get().addHandler(UnknownUriFragmentTokenEvent.TYPE, this);
        // pridani profilove radky do hlavicky aplikace
        LayoutHead.get().addToRow(new HeadProfileRow());
    }

    @Override
    public void registerModule() {
        CacheRegistrator.register(LoadLoggedUserAction.class, new LoadLoggedUserHandler());
        CacheRegistrator.register(UpdateUserAction.class, new UpdateUserHandler());
        CacheRegistrator.register(ListOrganizationTypesAction.class, new OrganizationTypesListHandler());
        CacheRegistrator.register(ListUsStateAction.class, new ListUsStateCacheHandler());
        CacheRegistrator.register(ListLocationAction.class, new ListLocationCacheHandler());
        CacheRegistrator.register(LoadPersonalInfoAction.class, new LoadPersonalInfoHandler());
        CacheRegistrator.register(UpdatePersonalInfoAction.class, new UpdatePersonalInfoHandler());
    }

    @Override
    public void unregisterModule() {
        CacheRegistrator.unregister(LoadLoggedUserAction.class);
        CacheRegistrator.unregister(UpdateUserAction.class);
        CacheRegistrator.unregister(ListOrganizationTypesAction.class);
        CacheRegistrator.unregister(ListUsStateAction.class);
        CacheRegistrator.unregister(ListLocationAction.class);
        CacheRegistrator.unregister(LoadPersonalInfoAction.class);
        CacheRegistrator.unregister(UpdatePersonalInfoAction.class);
    }

    @Override
    public void onUnknownUriFragment(UnknownUriFragmentTokenEvent event) {
        logger.debug(" catching unknown fragment and showing LogIn");
        History.newItem(LOG_IN);

        // platnost cookie, 30 sekund
        Date validTo = new Date();
        long timeTo = validTo.getTime() + (1000 * 30);
        validTo.setTime(timeTo);

        // cookie pokud bylo v url presmerovani nekam hloubeji, ulozime pro pripad SSO prihlaseni
        Cookies.setCookie(SSO_LOGIN_COOKIE, event.getFragmane(), validTo);

        FireAfterLoginEvent fale = new FireAfterLoginEvent(event.getFragmane());
        EventBus.get().fireEvent(fale);
    }
}
