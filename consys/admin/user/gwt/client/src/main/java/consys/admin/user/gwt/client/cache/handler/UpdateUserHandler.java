package consys.admin.user.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.admin.user.gwt.client.action.UpdateUserAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.admin.user.gwt.client.event.UpdatedUserEvent;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.rpc.result.VoidResult;

/**
 * Event vystreleny pri aktualizaci uzivatele, po uspesne aktualizaci ulozi do Cache a vyvola event UpdatedUserEvent
 * @author pepa
 */
public class UpdateUserHandler implements CacheHandler<UpdateUserAction>{

    @Override
    public void doAction(final UpdateUserAction action,final AsyncCallback callback, ActionExecutionDelegate panel) {
       ActionExecutor.execute(action, new AsyncCallback<VoidResult>() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess(VoidResult result) {
                // potvrzena aktualizace se ulozi do cache
                Cache.get().register(UserCacheAction.USER, action.getClientUser());
                // vystreli se event, ze byl user aktualizovan
                UpdatedUserEvent e = new UpdatedUserEvent(action.getClientUser());
                EventBus.get().fireEvent(e);

                callback.onSuccess(result);
            }
        }, panel);
    }

    
}
