package consys.admin.user.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.admin.user.gwt.client.message.AdminUserConstants;
import consys.admin.user.gwt.client.message.AdminUserMessages;

/**
 *
 * @author pepa
 */
public class AUMessageUtils {

    /** systemove konstanty modulu admin user */
    public static final AdminUserConstants c = (AdminUserConstants) GWT.create(AdminUserConstants.class);
    /** systemove zpravy modulu admin user */
    public static final AdminUserMessages m = (AdminUserMessages) GWT.create(AdminUserMessages.class);
}
