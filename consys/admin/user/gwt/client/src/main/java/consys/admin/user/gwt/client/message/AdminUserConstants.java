package consys.admin.user.gwt.client.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface AdminUserConstants extends Constants {

    String accountMenu_title_account();

    String activateContent_button_confirmAccount();

    String activateContent_error_activateFieldBadInput();

    String activateContent_error_activationFailed();

    String activateContent_helpText1();

    String activateContent_helpText2();

    String activateContent_helpText3();

    String activateContent_helpTitle();

    String activateContent_text1();

    String activateContent_text2();

    String activateContent_text3();

    String activateContent_title();

    String aboutYourself_helpText();

    String aboutYourself_helpTitle();

    String aboutYourself_text_pleaseEnterBio();

    String aboutYourself_text_successBioUpdate();

    String aboutYourself_title_bio();

    String changePasswordContent_button_changePassword();

    String changePasswordContent_error_invalidCharactersInPassword();

    String changePasswordContent_error_lostPasswordBadToken();

    String changePasswordContent_error_lostPasswordTokenMissing();

    String changePasswordContent_helpText1();

    String changePasswordContent_helpText2();

    String changePasswordContent_helpTitle();

    String changePasswordContent_title();

    String experience_title();

    String faqContent_answer1();

    String faqContent_answer2();

    String faqContent_answer3();

    String faqContent_answer4();

    String faqContent_answer5();

    String faqContent_answer6();

    String faqContent_answer7();

    String faqContent_answer8();

    String faqContent_answer9();

    String faqContent_answer10();

    String faqContent_question1();

    String faqContent_question2();

    String faqContent_question3();

    String faqContent_question4();

    String faqContent_question5();

    String faqContent_question6();

    String faqContent_question7();

    String faqContent_question8();

    String faqContent_question9();

    String faqContent_question10();

    String faqContent_title();

    String fillEmailDialog_action_updateEmail();

    String fillEmailDialog_text_email();

    String headProfileRow_action_accountNotActivated();

    String headProfileRow_action_logOut();

    String headProfileRow_action_notFillEmail();

    String headProfileRow_text_accountActivated();

    String invitationContent_button_registerAccount();

    String invitationContent_error_missingToken();

    String invitationContent_title();

    String joinPanel_text();

    String joinPanel_title();

    String loginContent_helpText1();

    String loginContent_helpText2();

    String loginContent_helpText3();

    String loginContent_helpTitle();

    String loginContent_text_activationSuccessfull();

    String loginContent_text_lostPasswordChanged();

    String loginPanel_action_lostPassword();

    String loginPanel_action_notActivated();

    String loginPanel_action_notRegistered();

    String loginPanel_button_logIn();

    String loginPanel_text();

    String lostPasswordContent_error_userNotExists();

    String lostPasswordContent_helpText1();

    String lostPasswordContent_helpText2();

    String lostPasswordContent_helpText3();

    String lostPasswordContent_helpTitle();

    String lostPasswordContent_text_lostPasswordSendSuccess();

    String lostPasswordContent_text1();

    String lostPasswordContent_text2();

    String lostPasswordContent_title();

    String myResume_title();

    String newUserOrganizationDialog_title();

    String organizationDetailForm_form_industry();

    String organizationDetailForm_form_internationalName();

    String organizationDetailForm_form_moreNameOptions();

    String organizationSuggestOracle_text_createNewOrganization();

    String passwordChangeContent_button_changePassword();

    String passwordChangeContent_error_badLengthOfOldPassword();

    String passwordChangeContent_error_oldPasswordIsEmpty();

    String passwordChangeContent_form_newPassword();

    String passwordChangeContent_form_oldPassword();

    String passwordChangeContent_text_successPasswordChange();

    String personalInfo_text_successPersonalInfoUpdate();

    String personalInfo_title();

    String personInfoContent_form_contactEmail();

    String personInfoContent_form_frontDegree();

    String personInfoContent_form_rearDegree();

    String personInfoContent_text_successPersonalInfoUpdate();

    String privateOrgContent_helpText();

    String privateOrgContent_helpTitle();

    String privateOrgContent_text_successAddressUpdate();

    String privateOrgContent_title_personalAddress();

    String profileImageContent_button_picture();

    String profileImageContent_error_profileUpdateFailed();

    String profileImageContent_text_profileUpdateSuccessfull();

    String profileMenuContent_title();

    String registerContent_button_registerAccount();

    String registerContent_helpText1();

    String registerContent_helpText2();

    String registerContent_helpText3();

    String registerContent_helpTitle();

    String registerContent_title();

    String userModule_mainMenu_profile();

    String userOrg_error_invalidIdOfExperience();

    String userOrg_error_userOrganizationNotExists();

    String userOrg_form_at();

    String userOrg_form_currentlyWork();

    String userOrg_form_position();

    String userOrg_form_preferredPosition();

    String userOrg_form_timePeriod();

    String userOrg_subtitle_currentlyWork();

    String userOrg_subtitle_pastPositions();

    String userOrg_text_areYouSure();

    String userOrg_text_at();

    String userOrg_text_presentLower();

    String userOrganizationsContent_error_experienceListFailed();

    String userOrganizationsContent_title();

    String userOrganizationsContent_text_experienceSucessfullyUpdated();
}
