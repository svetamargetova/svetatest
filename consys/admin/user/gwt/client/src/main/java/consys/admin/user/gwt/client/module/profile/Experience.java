package consys.admin.user.gwt.client.module.profile;

import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.user.gwt.client.module.profile.organization.UserOrganizationsContent;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;

/**
 *
 * @author pepa
 */
public class Experience extends SimpleFormPanel implements MenuFormItem {

    // konstanty
    public static final String NAME = AUMessageUtils.c.experience_title();
    // data
    private boolean initialized;

    public Experience() {
        super();
        initialized = false;
    }

    /** zinicializuje podcasti zalozky */
    public void init() {
        addWidget(new UserOrganizationsContent());
        initialized = true;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        if (!initialized) {
            init();
        }
        panel.setWidget(this);
    }
}
