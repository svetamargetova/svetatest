package consys.admin.user.gwt.client.action;

import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.common.gwt.shared.action.Action;

/**
 * Akce pro nacteni uzivatelovy soukrome organizace
 * @author pepa
 */
public class LoadPrivateUserOrgAction implements Action<ClientUserOrg> {

    private static final long serialVersionUID = -5896544771155548719L;

    public LoadPrivateUserOrgAction() {
    }
}
