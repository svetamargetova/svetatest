package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientOrganization;

/**
 *
 * @author pepa
 */
public class LoadOrganizationAction implements Action<ClientOrganization> {

    private static final long serialVersionUID = 7401552560715083279L;
    private String uuid;

    public LoadOrganizationAction() {
    }

    public LoadOrganizationAction(String uuid) {
        this.uuid = uuid;
    }

    public String getOrganizationUuid() {
        return uuid;
    }
}
