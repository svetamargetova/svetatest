package consys.admin.user.gwt.client.action;

/**
 * 
 * @author pepa
 */
public class UpdateLostPasswordAction extends UpdatePasswordAction {

    private static final long serialVersionUID = -4001455909297889654L;
   
    private String token;

    public UpdateLostPasswordAction() {
    }

    public UpdateLostPasswordAction(String pass,String token) {
        super(pass);
        this.token = token;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    
}
