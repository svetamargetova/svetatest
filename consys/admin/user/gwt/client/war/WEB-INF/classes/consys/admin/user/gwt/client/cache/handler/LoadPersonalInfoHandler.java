package consys.admin.user.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.admin.user.gwt.client.action.LoadPersonalInfoAction;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.rpc.action.cache.PersonalInfoCacheAction;
import consys.common.gwt.shared.bo.ClientPersonalInfo;

/**
 *
 * @author pepa
 */
public class LoadPersonalInfoHandler implements CacheHandler<LoadPersonalInfoAction> {

    private void cacheAction(final AsyncCallback callback) {
        Cache.get().doCacheAction(new PersonalInfoCacheAction() {

            @Override
            public void doAction(ClientPersonalInfo value) {
                callback.onSuccess(value);
            }

            @Override
            public void doTimeoutAction() {
                callback.onFailure(new Exception());
            }
        });
    }

    @Override
    public void doAction(LoadPersonalInfoAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
        if (!Cache.get().isRegistered(PersonalInfoCacheAction.PERSONAL_INFO)) {
            ActionExecutor.execute(action, new AsyncCallback<ClientPersonalInfo>() {

                @Override
                public void onFailure(Throwable caught) {
                    callback.onFailure(caught);
                }

                @Override
                public void onSuccess(ClientPersonalInfo result) {
                    Cache.get().register(PersonalInfoCacheAction.PERSONAL_INFO, result);
                    callback.onSuccess(result);
                }
            }, delegate);
        } else {
            cacheAction(callback);
        }
    }
}
