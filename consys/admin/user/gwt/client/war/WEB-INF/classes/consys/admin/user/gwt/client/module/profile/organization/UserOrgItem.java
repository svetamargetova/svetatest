package consys.admin.user.gwt.client.module.profile.organization;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Jedna polozka v prehledu uzivatelovych organizaci
 * @author pepa
 */
public class UserOrgItem {

    // konstanty
    private static DateTimeFormat dateFormat = DateTimeFormat.getFormat("y");
    // komponenty
    private Label label;
    private ActionLabel edit;
    private FlowPanel control;
    private SimplePanel removerPanel;
    // data
    private ClientUserOrgProfileThumb uo;
    private FlowPanel instance;

    public UserOrgItem(ClientUserOrgProfileThumb uo, ClickHandler editClickHandler) {
        super();
        this.uo = uo;
        label = StyleUtils.getStyledLabel(getInfo(), StyleUtils.Assemble(StyleUtils.MARGIN_RIGHT_20, StyleUtils.MARGIN_VER_3));

        edit = new ActionLabel(UIMessageUtils.c.const_edit(), StyleUtils.Assemble(StyleUtils.MARGIN_RIGHT_10,
                StyleUtils.FONT_10PX, StyleUtils.FLOAT_LEFT));
        edit.setClickHandler(editClickHandler);

        SimplePanel padd = new SimplePanel();
        padd.setSize("6px", "6px");
        padd.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        padd.addStyleName(StyleUtils.FLOAT_LEFT);

        removerPanel = new SimplePanel();
        removerPanel.addStyleName(StyleUtils.FLOAT_LEFT);

        control = new FlowPanel();
        control.addStyleName(StyleUtils.FLOAT_RIGHT);
        control.add(padd);
        control.add(edit);
        control.add(removerPanel);
        control.add(StyleUtils.clearDiv());
    }

    /** vytvori mazaci komponentu a nastavi ji zadanou akci */
    public void setDeleteAction(ConsysAction removeAction) {
        Image remove = new Image(ResourceUtils.system().removeCross());
        remove.setStyleName(StyleUtils.HAND);
        Remover remover = new Remover(remove, removeAction) {

            @Override
            protected void onQuestionSwap() {
                edit.setVisible(false);
            }

            @Override
            protected void onDefaultSwap() {
                edit.setVisible(true);
            }
        };

        removerPanel.setWidget(remover);
    }

    /**
     * @return the info
     */
    public String getInfo() {
        if (uo.isActualPosition()) {
            return uo.getPosition() + " " + AUMessageUtils.c.userOrg_text_at() + " " + uo.getOrgName() + " ("
                    + dateFormat.format(uo.getFrom()) + FormUtils.UNB_SPACE + FormUtils.MDASH + FormUtils.UNB_SPACE + ")";
        } else {
            return uo.getPosition() + " " + AUMessageUtils.c.userOrg_text_at() + " " + uo.getOrgName() + " ("
                    + dateFormat.format(uo.getFrom()) + FormUtils.UNB_SPACE + FormUtils.NDASH + FormUtils.UNB_SPACE
                    + dateFormat.format(uo.getTo()) + ")";
        }
    }

    public Label getLabel() {
        return label;
    }

    public Widget getControl() {
        return control;
    }

    public FlowPanel getInstance() {
        return instance;
    }

    public void setInstance(FlowPanel instance) {
        this.instance = instance;
    }

    /** vraci data polozky */
    public ClientUserOrgProfileThumb getUserOrg() {
        return uo;
    }
}
