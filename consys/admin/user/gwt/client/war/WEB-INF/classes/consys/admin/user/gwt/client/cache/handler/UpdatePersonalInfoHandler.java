package consys.admin.user.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.admin.user.gwt.client.action.UpdatePersonalInfoAction;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.rpc.action.cache.PersonalInfoCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.bo.ClientPersonalInfo;

/**
 *
 * @author pepa
 */
public class UpdatePersonalInfoHandler implements CacheHandler<UpdatePersonalInfoAction> {

    @Override
    public void doAction(final UpdatePersonalInfoAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
        ActionExecutor.execute(action, new AsyncCallback<VoidResult>() {

            @Override
            public void onFailure(Throwable caught) {
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess(VoidResult result) {
                // potvrzena aktualizace se ulozi do cache
                ClientPersonalInfo cpi = (ClientPersonalInfo) Cache.get().get(PersonalInfoCacheAction.PERSONAL_INFO);
                if (cpi != null) {
                    cpi.setCeliac(action.isCeliac());
                    cpi.setVegetarian(action.isVegetarian());
                    Cache.get().register(PersonalInfoCacheAction.PERSONAL_INFO, cpi);
                } else {
                    // nemelo by nikdy nastat, protoze vzdycky se nejdriv nacte a ulozi do cache nez jde editovat
                    Cache.get().unregister(PersonalInfoCacheAction.PERSONAL_INFO);
                }

                callback.onSuccess(result);
            }
        }, delegate);
    }
}
