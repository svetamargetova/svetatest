package consys.admin.user.gwt.client.action;

import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class LoadUserOrgAction implements Action<ClientUserOrg> {

    private static final long serialVersionUID = 7401552560715083279L;
    private String userOrgUuid;

    public LoadUserOrgAction() {
    }

    public LoadUserOrgAction(String userOrgUuid) {
        this.userOrgUuid = userOrgUuid;
    }

    public String getUserOrgUuid() {
        return userOrgUuid;
    }
}
