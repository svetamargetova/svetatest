package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.bo.ClientOrganizationThumb;
import consys.common.gwt.client.rpc.result.SelectBoxResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author Palo
 */
public class ListOrganizationsByPrefixAction  implements Action<SelectBoxResult<ClientOrganizationThumb>>{
    private static final long serialVersionUID = -2211737444010383020L;

    private String prefix;
    private int limit;

    public ListOrganizationsByPrefixAction(String prefix,int limit) {
        this.prefix = prefix;
        this.limit = limit;
    }

    public ListOrganizationsByPrefixAction() {
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

}
