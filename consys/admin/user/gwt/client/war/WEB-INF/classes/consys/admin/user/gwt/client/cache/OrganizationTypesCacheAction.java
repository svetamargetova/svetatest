package consys.admin.user.gwt.client.cache;

import consys.common.gwt.client.cache.CacheAction;
import consys.common.gwt.client.rpc.result.SelectBoxData;

/**
 * abstraktni cache akce pro provadeni akci s enum industry
 * @author pepa
 */
public abstract class OrganizationTypesCacheAction implements CacheAction<SelectBoxData> {

    // konstanty
    public static final String INDUSTRY = "organization_types";

    @Override
    public String getName() {
        return INDUSTRY;
    }

    @Override
    public boolean isCancel() {
        return false;
    }
}
