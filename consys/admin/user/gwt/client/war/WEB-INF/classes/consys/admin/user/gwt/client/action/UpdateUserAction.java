package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 * Palo: Tato akcie je handlovana v Event Server pretoze posiela nove udaje do
 * overseera.
 * @author pepa
 */
public class UpdateUserAction implements Action<VoidResult> {

    private static final long serialVersionUID = -6796716933862720604L;
    private ClientUser cu;

    public UpdateUserAction() {
    }

    public UpdateUserAction(ClientUser cu) {
        this.cu = cu;
    }

    public ClientUser getClientUser() {
        return cu;
    }
}
