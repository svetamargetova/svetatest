package consys.admin.user.gwt.client.module.profile.account;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.LoadPrivateUserOrgAction;
import consys.admin.user.gwt.client.action.UpdatePrivateOrgAction;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.AddressForm;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Formular pro zadani privatni organizace (uzivatel sam za sebe)
 * @author pepa
 */
public class PrivateOrgContent extends RUSimpleHelpFormPanel {

    // komponenty
    private AddressForm form;
    // data
    private ClientUserOrg uo;

    public PrivateOrgContent() {
        super(true);
        addHelpTitle(AUMessageUtils.c.privateOrgContent_helpTitle());
        addHelpText(AUMessageUtils.c.privateOrgContent_helpText());
    }

    @Override
    public Widget readForm() {
        final BaseForm bf = new BaseForm();
        bf.setStyleName(StyleUtils.MARGIN_BOT_10);

        DispatchEvent loadEvent = new DispatchEvent(new LoadPrivateUserOrgAction(), new AsyncCallback<ClientUserOrg>() {

            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovany ActionExecutorem
            }

            @Override
            public void onSuccess(ClientUserOrg result) {
                bf.addOptional(0, UIMessageUtils.c.const_address_location(), new Label(FormUtils.valueOrDash(result.getAddress().getLocationName())));
                if (result.getAddress().getStateUs() != 0) {
                    bf.addOptional(1, UIMessageUtils.c.const_address_state(), new Label(FormUtils.valueOrDash(result.getAddress().getStateUsName())));
                }
                bf.addOptional(2, UIMessageUtils.c.const_address_city(), new Label(FormUtils.valueOrDash(result.getAddress().getCity())));
                bf.addOptional(3, UIMessageUtils.c.const_address_street(), new Label(FormUtils.valueOrDash(result.getAddress().getStreet())));
                bf.addOptional(4, UIMessageUtils.c.const_address_zip(), new Label(FormUtils.valueOrDash(result.getAddress().getZip())));
            }
        }, getSelf());
        EventBus.get().fireEvent(loadEvent);

        FlowPanel panel = new FlowPanel();
        panel.add(FormPanelUtils.getOneLinedReadMode(this,
                AUMessageUtils.c.privateOrgContent_title_personalAddress(), UIMessageUtils.c.const_edit()));
        panel.add(bf);
        return panel;
    }

    @Override
    public Widget updateForm(String id) {
        Label titleLabel = StyleUtils.getStyledLabel(AUMessageUtils.c.privateOrgContent_title_personalAddress(), FONT_16PX);
        titleLabel.addStyleName(FONT_BOLD);
        titleLabel.addStyleName(MARGIN_BOT_20);

        form = new AddressForm(FormPanelUtils.getUpdateButton(this, id), FormPanelUtils.getCancelButton(this));
        form.setStyleName(StyleUtils.MARGIN_BOT_10);

        EventBus.get().fireEvent(new DispatchEvent(new LoadPrivateUserOrgAction(), new AsyncCallback<ClientUserOrg>() {

            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovany ActionExecutorem
            }

            @Override
            public void onSuccess(ClientUserOrg result) {
                uo = result;
                form.setAddress(uo.getAddress());
            }
        }, getSelf()));

        FlowPanel panel = new FlowPanel();
        panel.add(titleLabel);
        panel.add(form);
        return panel;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (uo == null) {
                    // jeste se nenacetlo, tak neni co potvrzovat
                    return;
                }

                uo.setAddress(form.getAddress());

                DispatchEvent editEvent = new DispatchEvent(new UpdatePrivateOrgAction(uo), new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovany ActionExecutorem
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        onSuccessUpdate(AUMessageUtils.c.privateOrgContent_text_successAddressUpdate());
                    }
                }, getSelf());
                EventBus.get().fireEvent(editEvent);
            }
        };
    }
}
