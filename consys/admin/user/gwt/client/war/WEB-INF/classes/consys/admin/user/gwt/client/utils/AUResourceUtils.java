package consys.admin.user.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.admin.user.gwt.client.utils.css.AUBundle;
import consys.admin.user.gwt.client.utils.css.AUCssResources;

/**
 *
 * @author pepa
 */
public class AUResourceUtils {

    private static AUBundle bundle;

    static {
        bundle = (AUBundle) GWT.create(AUBundle.class);
        bundle.css().ensureInjected();
    }

    public static AUCssResources css() {
        return bundle.css();
    }
}
