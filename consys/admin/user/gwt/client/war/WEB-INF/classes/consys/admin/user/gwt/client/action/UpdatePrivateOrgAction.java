package consys.admin.user.gwt.client.action;

import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class UpdatePrivateOrgAction implements Action<VoidResult> {

    private static final long serialVersionUID = -8858185092934365204L;
    private ClientUserOrg uo;

    public UpdatePrivateOrgAction() {
    }

    public UpdatePrivateOrgAction(ClientUserOrg uo) {
        this.uo = uo;
    }

    public ClientUserOrg getClientUserOrg() {
        return uo;
    }
}
