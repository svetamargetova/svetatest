package consys.admin.user.gwt.client.module.profile.organization;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import consys.admin.user.gwt.client.action.ListOrganizationTypesAction;
import consys.common.gwt.shared.bo.ClientOrganization;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.AddressForm;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.SuggestionBox;
import consys.common.gwt.client.ui.comp.panel.abst.ValidableComponent;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBaseWrapper;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import java.util.ArrayList;

/**
 * Formular detailu nastaveni organizace
 * @author pepa
 */
public class OrganizationDetailForm extends SimplePanel implements CssStyles, ValidableComponent {

    private VerticalPanel panel;
    private AddressForm addressForm;
    private SelectBox<Integer> industryBox;
    private ConsysStringTextBox fullName;
    private TextBox universalBox;
    private TextBox acronymBox;
    private TextBox websiteBox;
    private BaseForm formWeb;
    // data
    ClientOrganization organization;
    private SuggestionBox suggestBox;

    public OrganizationDetailForm() {
        super();
        fullName = new ConsysStringTextBox(1, 255, UIMessageUtils.c.const_name());
        fullName.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (suggestBox != null) {
                    suggestBox.setText(fullName.getText());
                }
            }
        });

        industryBox = new SelectBox<Integer>();
        industryBox.setRequired(false);
        industryBox.selectFirst(true);
        universalBox = StyleUtils.getFormInput();
        acronymBox = StyleUtils.getFormInput();
        addressForm = new AddressForm();
        websiteBox = StyleUtils.getFormInput();

        // Zakladni formular
        formWeb = new BaseForm();

        // 1|  FULL NAME
        formWeb.addRequired(0, UIMessageUtils.c.const_name(), fullName);

        // 2| More name options (+)
        ActionLabel showMoreNameLabel = new ActionLabel(AUMessageUtils.c.organizationDetailForm_form_moreNameOptions(), StyleUtils.Assemble(FONT_10PX, FONT_BOLD, TEXT_BLUE));

        showMoreNameLabel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                formWeb.insertRow(2);
                formWeb.addOptional(1, AUMessageUtils.c.organizationDetailForm_form_internationalName(), universalBox);
                formWeb.addOptional(2, UIMessageUtils.c.const_acronym(), acronymBox);
            }
        });
        formWeb.addContent(1, showMoreNameLabel);

        // 3| industry
        formWeb.addOptional(2, AUMessageUtils.c.organizationDetailForm_form_industry(), industryBox);

        // 4| address
        addressForm.addOptional(6, UIMessageUtils.c.const_website(), websiteBox);

        panel = new VerticalPanel();
        panel.add(formWeb);
        panel.add(addressForm);

        setWidget(new ConsysBaseWrapper(panel));

        EventBus.get().fireEvent(new DispatchEvent(new ListOrganizationTypesAction(), new AsyncCallback<SelectBoxData>() {

            @Override
            public void onFailure(Throwable caught) {
               
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
                for (BoxItem i : result.getList()) {
                    out.add(new SelectBoxItem<Integer>(i.getId(), i.getName()));
                }
                industryBox.setItems(out);
            }
        }, null));
    }

    /** nastavi focus na prvni polozku vstupu */
    public void setNameFocus() {
        fullName.setFocus(true);
        if (suggestBox != null) {
            suggestBox.setText(fullName.getText());
        }
    }

    /**
     * @return the organization
     */
    public ClientOrganization getOrganization() {
        // TODO: valid url?
        SelectBoxItem<Integer> industry = industryBox.getSelectedItem();

        organization.setWeb(websiteBox.getText().trim());
        organization.setUniversalName(universalBox.getText().trim());
        organization.setOrganizationType(industry == null ? 0 : industry.getItem());
        organization.setAddress(addressForm.getAddress());
        organization.setAcronym(acronymBox.getText().trim());
        organization.setFullName(fullName.getText().trim());
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(ClientOrganization organization) {
        this.organization = organization;
        websiteBox.setText(organization.getWeb());
        universalBox.setText(organization.getUniversalName());
        fullName.setText(organization.getFullName());
        if (organization.getOrganizationType() > 0) {
            industryBox.selectItem(organization.getOrganizationType());
        } else {
            industryBox.selectFirst(false);
        }
        addressForm.setAddress(organization.getAddress());
        acronymBox.setText(organization.getAcronym());
    }

    /** nastaveni suggest boxu pro zpetne zobrazovani hodnoty vstupu */
    public void setSuggestInput(SuggestionBox suggestBox) {
        this.suggestBox = suggestBox;
    }

    @Override
    public boolean doValidate(ConsysMessage fail) {
        return this.isVisible() ? formWeb.validate(fail) : true;
    }

    @Override
    public void onSuccess() {
    }

    @Override
    public void onFail() {
    }
}
