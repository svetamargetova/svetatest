package consys.admin.user.gwt.client.module.profile.account;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.admin.user.gwt.client.action.UpdateUserAction;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUser;

/**
 *
 * @author pepa
 */
public class MyResume extends RUSimpleHelpFormPanel {

    // konstanty
    public static final String NAME = AUMessageUtils.c.myResume_title();
    // komponenty
    private ConsysTextArea bioBox;
    private ActionImage saveBio;
    // data
    private AsyncCallback callback;
    private ClientUser cu;
    private String originalBio;

    public MyResume() {
        super(true);

        bioBox = new ConsysTextArea(2048, "480px", "180px", NAME);
        bioBox.setEmptyText(AUMessageUtils.c.aboutYourself_text_pleaseEnterBio());

        saveBio = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        saveBio.addStyleName(FLOAT_LEFT);
        saveBio.addStyleName(MARGIN_RIGHT_20);
        saveBio.addClickHandler(confirmUpdateClickHandler(null));

        callback = new AsyncCallback<ClientUser>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ClientUser result) {
                cu = result;
                String text = cu.getBio();
                originalBio = text;
                bioBox.setText(text);
            }
        };

        addHelpTitle(AUMessageUtils.c.aboutYourself_helpTitle());
        addHelpText(AUMessageUtils.c.aboutYourself_helpText());
    }

    @Override
    public Widget readForm() {
        bioBox.setEnabled(false);

        FlowPanel panel = new FlowPanel();
        panel.add(FormPanelUtils.getOneLinedReadMode(this, AUMessageUtils.c.myResume_title(), UIMessageUtils.c.const_edit()));
        panel.add(bioBox);

        EventBus.get().fireEvent(new DispatchEvent(new LoadLoggedUserAction(), callback, this));

        return panel;
    }

    @Override
    public Widget updateForm(String id) {
        bioBox.setEnabled(true);

        Label titleLabel = StyleUtils.getStyledLabel(AUMessageUtils.c.myResume_title(), FONT_16PX);
        titleLabel.addStyleName(FONT_BOLD);
        titleLabel.addStyleName(MARGIN_BOT_20);

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), FLOAT_LEFT + " " + MARGIN_TOP_3);
        cancel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                clearMessageBox();
                bioBox.setText(originalBio);
                setWidget(readForm());
            }
        });

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(saveBio);
        controlPanel.add(cancel);
        controlPanel.add(StyleUtils.clearDiv());

        FlowPanel panel = new FlowPanel();
        panel.add(titleLabel);
        panel.add(bioBox);
        panel.add(controlPanel);
        return panel;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final String text = bioBox.getText().trim();
                cu.setBio(text);
                EventBus.get().fireEvent(new DispatchEvent(new UpdateUserAction(cu), new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovany ActionExecutorem
                        bioBox.setText(originalBio);
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        originalBio = text;
                        onSuccessUpdate(AUMessageUtils.c.aboutYourself_text_successBioUpdate());
                    }
                }, MyResume.this));
            }
        };
    }
}
