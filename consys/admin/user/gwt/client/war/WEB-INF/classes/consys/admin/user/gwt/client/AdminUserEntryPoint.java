package consys.admin.user.gwt.client;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.RootPanel;
import consys.admin.user.gwt.client.action.ActivateUserAction;
import consys.admin.user.gwt.client.action.ActivateUserRequestAction;
import consys.admin.user.gwt.client.action.ConfirmInvitationAction;
import consys.admin.user.gwt.client.action.CreateUserOrgAction;
import consys.admin.user.gwt.client.action.DeleteUserOrgAction;
import consys.admin.user.gwt.client.action.ListOrganizationTypesAction;
import consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction;
import consys.admin.user.gwt.client.action.ListUserOrgsAction;
import consys.admin.user.gwt.client.action.LoadInvitationAction;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.admin.user.gwt.client.action.LoadOrganizationAction;
import consys.admin.user.gwt.client.action.LoadPersonalInfoAction;
import consys.admin.user.gwt.client.action.LoadPrivateUserOrgAction;
import consys.admin.user.gwt.client.action.LoadUserOrgAction;
import consys.admin.user.gwt.client.action.UpdatePasswordAction;
import consys.admin.user.gwt.client.action.UpdatePersonalInfoAction;
import consys.admin.user.gwt.client.action.UpdateUserAction;
import consys.admin.user.gwt.client.action.UpdateUserOrgAction;
import consys.admin.user.gwt.client.base.FAQContent;
import consys.common.gwt.shared.bo.ClientOrganization;
import consys.common.gwt.shared.bo.ClientOrganizationThumb;
import consys.admin.user.gwt.client.base.RegisterContent;
import consys.common.gwt.shared.bo.ClientPersonalInfo;
import consys.common.gwt.shared.bo.ClientRegistration;
import consys.common.gwt.shared.bo.ClientUser;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb;
import consys.admin.user.gwt.client.module.UserModule;
import consys.admin.user.gwt.client.panel.JoinPanel;
import consys.admin.user.gwt.client.panel.LoginPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ModuleEntryPoint;
import consys.common.gwt.shared.bo.ClientAddress;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.module.ModuleRegistrator;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.action.ListUsStateAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.SelectBoxResult;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.layout.menu.MenuDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class AdminUserEntryPoint extends ModuleEntryPoint {

    @Override
    public void initLayout() {
        LayoutManager layoutManager = LayoutManager.get();
        RootPanel.get().add(layoutManager);

        ModuleRegistrator.get().registerInModule(new UserModule());

        

        // nastavi cache, ze je neprihlaseny uzivatel
        Cache.get().register(Cache.LOGGED, Boolean.TRUE);
        // zinicializovani MenuDispatcheru a vylozeni menu
        MenuDispatcher.get().setSignUpWidget(new RegisterContent());
        MenuDispatcher.get().setFAQWidget(new FAQContent());
        MenuDispatcher.get().generateMenu();
        // zinicializovani PanelDispatcheru a vlozeni panelu
        PanelDispatcher pd = PanelDispatcher.get();
        pd.addLogoutPanelItem(new JoinPanel());
        pd.addLogoutPanelItem(new LoginPanel());        
        pd.generatePanel();

        EventBus.get().fireEvent(new UserLoginEvent());
    }

    @Override
    public void initMocks() {
        DateTimeFormat format = DateTimeFormat.getFormat("d.M.y");

        SelectBoxData locationList = new SelectBoxData();
        locationList.getList().add(new BoxItem("Chile", 34));
        locationList.getList().add(new BoxItem("Czech republic", 53));
        locationList.getList().add(new BoxItem("USA", 231));
        MockFactory.addActionMock(ListLocationAction.class, new ActionMock<SelectBoxData>(locationList));

        // ------
        SelectBoxData stateList = new SelectBoxData();
        stateList.getList().add(new BoxItem("DA", 34));
        stateList.getList().add(new BoxItem("CA", 44));
        stateList.getList().add(new BoxItem("LA", 54));
        MockFactory.addActionMock(ListUsStateAction.class, new ActionMock<SelectBoxData>(stateList));

        // ------
        ClientPersonalInfo personalInfo = new ClientPersonalInfo();
        personalInfo.setVegetarian(true);
        MockFactory.addActionMock(LoadPersonalInfoAction.class, new ActionMock<ClientPersonalInfo>(personalInfo));

        MockFactory.addActionMock(UpdatePersonalInfoAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ------
        ClientUser user = new ClientUser();
        user.setFirstName("Marcel");
        user.setMiddleName("Bohulibý");
        user.setLastName("Chroustalovič");
        user.setLoginEmail("marcel.chroustalspolecnost-jakasi.com");
        user.setFrontDegree("Prof. Ing.");
        user.setRearDegree("Phd.");
        user.setUserOrgDefault("Pošťák v Česká pošta (2000 -)");
        user.setActivatedUser(true);

        // ------
        MockFactory.addActionMock(LoadLoggedUserAction.class, new ActionMock<ClientUser>(user));

        // ------
        MockFactory.addActionMock(ActivateUserRequestAction.class, new ActionMock<VoidResult>(new VoidResult()));
        MockFactory.addActionMock(ActivateUserAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ------
        MockFactory.addActionMock(UpdateUserAction.class, new ActionMock<VoidResult>(new VoidResult()));

        // ------
        MockFactory.addActionMock(UpdatePasswordAction.class, new ActionMock<VoidResult>(new VoidResult()));

        // ------
        ArrayListResult<ClientUserOrgProfileThumb> userOrgs = new ArrayListResult<ClientUserOrgProfileThumb>();
        ClientUserOrgProfileThumb thumb = new ClientUserOrgProfileThumb();
        thumb.setActualPosition(true);
        thumb.setPosition("Developer,tester bla bla");
        thumb.setActualPosition(false);
        thumb.setFrom(format.parse("1.1.2007"));
        thumb.setTo(format.parse("1.1.2017"));
        thumb.setOrgName("ecommerce.cz, s.r.o.");
        thumb.setUuid("1");
        userOrgs.getArrayListResult().add(thumb);

        thumb = new ClientUserOrgProfileThumb();
        thumb.setActualPosition(true);
        thumb.setPosition("Developer,tester bla bla");
        thumb.setFrom(format.parse("1.1.2001"));
        thumb.setTo(format.parse("1.1.2018"));
        thumb.setOrgName("ecommerce.cz, s.r.o.");
        thumb.setUuid("2");
        userOrgs.getArrayListResult().add(thumb);

        thumb = new ClientUserOrgProfileThumb();
        thumb.setActualPosition(true);
        thumb.setPosition("Developer");
        thumb.setFrom(format.parse("1.1.2007"));
        thumb.setTo(format.parse("1.1.2017"));
        thumb.setOrgName("tesco tesco tesco");
        thumb.setUuid("3");
        userOrgs.getArrayListResult().add(thumb);

        thumb = new ClientUserOrgProfileThumb();
        thumb.setPosition("Developer,tester bla bla");
        thumb.setActualPosition(false);
        thumb.setFrom(format.parse("1.1.2007"));
        thumb.setTo(format.parse("1.1.2017"));
        thumb.setOrgName("acemcee acemcee");
        thumb.setUuid("4");
        userOrgs.getArrayListResult().add(thumb);

        thumb = new ClientUserOrgProfileThumb();
        thumb.setPosition("Developer,tester bla bla");
        thumb.setActualPosition(false);
        thumb.setFrom(format.parse("1.1.2007"));
        thumb.setTo(format.parse("1.1.2017"));
        thumb.setOrgName("home");
        thumb.setUuid("5");
        userOrgs.getArrayListResult().add(thumb);

        MockFactory.addActionMock(ListUserOrgsAction.class, new ActionMock<ArrayListResult<ClientUserOrgProfileThumb>>(userOrgs));
        //MockFactory.addActionMock(ListUserOrgsAction.class, new ActionMock(new ServiceFailedException()));

        // ------
        ClientUserOrg priv = new ClientUserOrg();
        ClientAddress address = new ClientAddress();
        address.setCity("Brno");
        priv.setAddress(address);

        MockFactory.addActionMock(LoadPrivateUserOrgAction.class, new ActionMock<ClientUserOrg>(priv));

        // ------
        ClientUserOrg cuo = new ClientUserOrg();
        cuo.setActualPosition(true);

        cuo.setFromDate(format.parse("1.8.2009"));
        cuo.setToDate(format.parse("20.11.2010"));
        ClientOrganization co = new ClientOrganization();
        co.setAcronym("ACMC");
        co.setFullName("acemcee, s.r.o");
        co.setUuid("123");
        co.setWeb("www.ecommerce.cz");
        co.setOwner(true);
        cuo.setOrganization(co);
        cuo.setPosition("Developer at fucking hell ecommerce.cz");
        cuo.setPreferredPosition(false);
        MockFactory.addActionMock(LoadUserOrgAction.class, new ActionMock<ClientUserOrg>(cuo));

        // ------
        MockFactory.addActionMock(LoadOrganizationAction.class, new ActionMock<ClientOrganization>(co));

        // ------
        MockFactory.addActionMock(UpdateUserOrgAction.class, new ActionMock<VoidResult>(new VoidResult()));

        // ------
        MockFactory.addActionMock(CreateUserOrgAction.class, new ActionMock<StringResult>(new StringResult("4")));

        // ------
        MockFactory.addActionMock(DeleteUserOrgAction.class, new ActionMock<VoidResult>(new VoidResult()));

        // ------
        MockFactory.addActionMock(ConfirmInvitationAction.class, new ActionMock<VoidResult>(new VoidResult()));

        // ------
        ClientRegistration regist = new ClientRegistration();
        regist.setContactEmail("my@mail.em");
        regist.setFirstName("First");
        regist.setLastName("Last");
        MockFactory.addActionMock(LoadInvitationAction.class, new ActionMock<ClientRegistration>(regist));

        // ------
        SelectBoxData orgTypes = new SelectBoxData();
        orgTypes.getList().add(new BoxItem("A", 1));
        orgTypes.getList().add(new BoxItem("AB", 2));
        orgTypes.getList().add(new BoxItem("AVC", 3));
        orgTypes.getList().add(new BoxItem("ABVX", 4));

        MockFactory.addActionMock(ListOrganizationTypesAction.class, new ActionMock<SelectBoxData>(orgTypes));

        // -----
        ArrayList<SelectBoxItem<ClientOrganizationThumb>> organizationThumbs = new ArrayList<SelectBoxItem<ClientOrganizationThumb>>();
        ClientOrganizationThumb thumb1 = new ClientOrganizationThumb();
        thumb1.setAddress("Capkova 21, Brno");
        thumb1.setType("Education");
        thumb1.setUniversalName("Fakulta informačních technologií, Vysoké učení technické v Brně");
        thumb1.setUuid("1");
        thumb1.setWeb("www.fit.vutbr.cz");
        organizationThumbs.add(new SelectBoxItem<ClientOrganizationThumb>(thumb1));

        thumb1 = new ClientOrganizationThumb();
        thumb1.setAddress("Capkova 21, Brno");
        thumb1.setType("Education");
        thumb1.setUniversalName("FIT VUT");
        thumb1.setUuid("2");
        thumb1.setWeb("www.fit.vutbr.cz");
        organizationThumbs.add(new SelectBoxItem<ClientOrganizationThumb>(thumb1));

        thumb1 = new ClientOrganizationThumb();
        thumb1.setAddress("Capkova 21, Brno");
        thumb1.setType("Education");
        thumb1.setUniversalName("FIT VUT");
        thumb1.setUuid("3");
        thumb1.setWeb("www.fit.vutbr.cz");
        organizationThumbs.add(new SelectBoxItem<ClientOrganizationThumb>(thumb1));

        thumb1 = new ClientOrganizationThumb();
        thumb1.setAddress("Capkova 21, Brno");
        thumb1.setType("Education");
        thumb1.setUniversalName("FIT VUT");
        thumb1.setUuid("4");
        thumb1.setWeb("www.fit.vutbr.cz");
        organizationThumbs.add(new SelectBoxItem<ClientOrganizationThumb>(thumb1));

        thumb1 = new ClientOrganizationThumb();
        thumb1.setAddress("Capkova 21, Brno");
        thumb1.setType("Education");
        thumb1.setUniversalName("FIT VUT");
        thumb1.setUuid("5");
        thumb1.setWeb("www.fit.vutbr.cz");
        organizationThumbs.add(new SelectBoxItem<ClientOrganizationThumb>(thumb1));

        MockFactory.addActionMock(ListOrganizationsByPrefixAction.class, new ActionMock<SelectBoxResult<ClientOrganizationThumb>>(new SelectBoxResult(organizationThumbs)));

    }
}
