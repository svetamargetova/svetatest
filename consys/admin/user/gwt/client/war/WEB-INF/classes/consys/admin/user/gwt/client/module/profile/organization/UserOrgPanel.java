package consys.admin.user.gwt.client.module.profile.organization;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Panel tvori prehled uzivatelovych organizaci a spravuje je
 * @author pepa
 */
public class UserOrgPanel extends SimpleFormPanel implements CssStyles {

    // komponenty
    private OrgWrapper currentFT;
    private OrgWrapper pastFT;

    public UserOrgPanel() {
        setDynamicWidth(true);

        currentFT = new OrgWrapper();
        pastFT = new OrgWrapper();

        addWidget(StyleUtils.getStyledLabel(AUMessageUtils.c.userOrg_subtitle_currentlyWork(), StyleUtils.FONT_BOLD));
        addWidget(currentFT);
        addWidget(StyleUtils.getStyledLabel(AUMessageUtils.c.userOrg_subtitle_pastPositions(), StyleUtils.FONT_BOLD));
        addWidget(pastFT);
    }

    /** vraci Label s popiskem o nenalezeni zaznamu */
    private Label noRecordLabel() {
        return new Label(UIMessageUtils.c.const_noRecordFound());
    }

    /**
     * prida polozku do panelu a zaradi do spravne casti
     * Sortovanie pride z DB
     */
    public void addItem(UserOrgItem item) {
        if (item.getUserOrg().isActualPosition()) {
            addRecord(currentFT, item);
        } else {
            addRecord(pastFT, item);
        }
    }

    private void addRecord(FlowPanel panel, UserOrgItem item) {
        FlowPanel instance = new FlowPanel();
        instance.add(item.getControl());
        instance.add(item.getLabel());
        instance.add(StyleUtils.clearDiv());
        item.setInstance(instance);
        panel.add(instance);
    }

    public FlowPanel getCurrentFT() {
        return currentFT;
    }

    public FlowPanel getPastFT() {
        return pastFT;
    }

    private class OrgWrapper extends FlowPanel {

        public OrgWrapper() {
            super();
            addStyleName(StyleUtils.MARGIN_LEFT_20);
            addStyleName(StyleUtils.MARGIN_VER_10);
            add(noRecordLabel());
        }

        @Override
        public boolean remove(Widget w) {
            boolean result = super.remove(w);
            if (getWidgetCount() == 0) {
                super.add(noRecordLabel());
            }
            return result;
        }

        @Override
        public void add(Widget w) {
            super.add(w);
            if (getWidgetCount() != 0 && getWidget(0) instanceof Label) {
                super.remove(0);
            }
        }
    }
}
