package consys.admin.user.gwt.client.base;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.admin.user.gwt.client.action.UpdateLostPasswordAction;
import consys.admin.user.gwt.client.action.exception.UserNotExistsException;
import consys.admin.user.gwt.client.module.UserModule;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.widget.validator.PasswordValidator;
import consys.common.gwt.client.widget.validator.PasswordValidator.PasswordStrengthEnum;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysPasswordTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.HistoryUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 * Zmena hesla pomoci odkazu z mailu
 * @author pepa
 */
public class ChangePasswordContent extends RootPanel {

    // konstanty
    public static final String PARAM_TOKEN = "token";
    // komponenty
    private ConsysStringTextBox fakePasswordBox;
    private ConsysPasswordTextBox passwordBox;
    private ConsysPasswordTextBox passwordAgainBox;
    private ConsysCheckBox showPassword;
    private BaseForm form;
    // data
    private String password;
    private String passwordAgain;
    private String token;

    public ChangePasswordContent() {
        super(AUMessageUtils.c.changePasswordContent_title());

        SimpleFormWithHelpPanel mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        if (HistoryUtils.hasParameters()) {
            token = HistoryUtils.getParameter(PARAM_TOKEN);
        }
        if (token == null || token.isEmpty()) {
            getFailMessage().setText(AUMessageUtils.c.changePasswordContent_error_lostPasswordTokenMissing());
            return;
        }

        fakePasswordBox = new ConsysStringTextBox("100px", 6, 255, CWMessageUtils.c.const_password());
        fakePasswordBox.setVisible(false);
        passwordBox = new ConsysPasswordTextBox("100px", 6, 255, CWMessageUtils.c.const_password());
        passwordAgainBox = new ConsysPasswordTextBox("100px", 6, 255, CWMessageUtils.c.const_confirmPassword()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                // validuje se pouze vzhledem ke shode passwordBox
                boolean result = true;
                if (!PasswordValidator.changePassword(password, passwordAgain)) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordsNotMatch());
                    result = false;
                }
                return result;
            }
        };

        showPassword = new ConsysCheckBox();
        showPassword.addStyleName(FLOAT_LEFT);

        FlowPanel checkPanel = new FlowPanel();
        checkPanel.addStyleName(FLOAT_RIGHT);
        checkPanel.addStyleName(MARGIN_TOP_3);
        checkPanel.add(showPassword);
        checkPanel.add(StyleUtils.getStyledLabel(CWMessageUtils.c.const_showPassword(), MARGIN_LEFT_5, FLOAT_LEFT));
        checkPanel.add(StyleUtils.clearDiv());

        ConsysFlowPanel passwordPanel = new ConsysFlowPanel() {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                if (!PasswordValidator.passwordStrength(password).equals(PasswordStrengthEnum.STRONG)) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordIsWeak());
                    return false;
                }
                if (password.contains(" ")) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordContainsSpace());
                    return false;
                }
                return true;
            }

            @Override
            public void onSuccess() {
                passwordBox.onSuccess();
                fakePasswordBox.onSuccess();
            }

            @Override
            public void onFail() {
                passwordBox.onFail();
                fakePasswordBox.onFail();
            }
        };
        passwordPanel.add(checkPanel);
        passwordPanel.add(passwordBox);
        passwordPanel.add(fakePasswordBox);
        passwordPanel.add(StyleUtils.clearDiv());

        Label passwordStrengthLabel = new Label();
        passwordStrengthLabel.setStyleName(FONT_BOLD);
        passwordStrengthLabel.addStyleName(FLOAT_RIGHT);

        ConsysFlowPanel passwordAgainPanel = new ConsysFlowPanel();
        passwordAgainPanel.add(passwordStrengthLabel);
        passwordAgainPanel.add(passwordAgainBox);
        passwordAgainPanel.add(StyleUtils.clearDiv());

        ActionImage button = ActionImage.getConfirmButton(AUMessageUtils.c.changePasswordContent_button_changePassword());
        button.addStyleName(StyleUtils.MARGIN_TOP_10);
        button.addClickHandler(confirmClickHandler());

        form = new BaseForm();
        form.addRequired(0, CWMessageUtils.c.const_password(), passwordPanel);
        form.addRequired(1, CWMessageUtils.c.const_confirmPassword(), passwordAgainPanel);
        form.addActionMembers(2, button, null);
        mainPanel.addWidget(form);

        initHandlers(passwordStrengthLabel);

        mainPanel.addHelpTitle(AUMessageUtils.c.changePasswordContent_helpTitle());
        mainPanel.addHelpText(AUMessageUtils.c.changePasswordContent_helpText1());
        mainPanel.addHelpText(AUMessageUtils.c.changePasswordContent_helpText2());
    }

    /** inicializuje handlery */
    private void initHandlers(final Label passwordStrengthLabel) {
        passwordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(passwordBox, passwordStrengthLabel));
        fakePasswordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(fakePasswordBox, passwordStrengthLabel));
        fakePasswordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                passwordBox.setText(fakePasswordBox.getText());
            }
        });
        passwordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                fakePasswordBox.setText(passwordBox.getText());
            }
        });
        showPassword.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                if (event.getValue()) {
                    passwordAgainBox.onSuccess();
                }
                fakePasswordBox.setVisible(event.getValue());
                passwordBox.setVisible(!event.getValue());
                passwordAgainBox.setEnabled(!event.getValue());
                passwordAgainBox.setText("");
            }
        });
    }

    /** ClickHandler pro potvrzeni noveho hesla, provede kontroly pred odeslanim */
    private ClickHandler confirmClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                password = passwordBox.getText().trim();

                if (!PasswordValidator.permitedCharacters(password)) {
                    getFailMessage().setText(AUMessageUtils.c.changePasswordContent_error_invalidCharactersInPassword());
                    return;
                }

                if (passwordAgainBox.isEnabled()) {
                    passwordAgain = passwordAgainBox.getText().trim();
                } else {
                    passwordAgain = fakePasswordBox.getText().trim();
                }
                if (!form.validate(getFailMessage())) {
                    return;
                }

                passwordBox.setText("");
                passwordAgainBox.setText("");
                fakePasswordBox.setText("");
                showPassword.setValue(false);

                EventBus.get().fireEvent(new DispatchEvent(new UpdateLostPasswordAction(password, token), new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        if (caught instanceof UserNotExistsException) {
                            getFailMessage().setText(AUMessageUtils.c.changePasswordContent_error_lostPasswordBadToken());
                        }
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        // prepnuti contentu
                        History.newItem(UserModule.LOG_IN + "?" + LoginContent.PARAM_PASS_CHANGED);
                    }
                }, getSelf()));


                password = "";
                passwordAgain = "";
            }
        };
    }
}
