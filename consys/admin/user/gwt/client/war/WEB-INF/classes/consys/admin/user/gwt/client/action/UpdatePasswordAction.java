package consys.admin.user.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class UpdatePasswordAction implements Action<VoidResult> {

    private static final long serialVersionUID = -4001455909297889654L;
    private String pass;

    public UpdatePasswordAction() {
    }

    public UpdatePasswordAction(String pass) {
        this.pass = pass;
    }

    public String getClientPassword() {
        return pass;
    }
}
