package consys.admin.user.gwt.client.action.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author pepa
 */
public class ActivationKeyNotFoundException extends ActionException {

    private static final long serialVersionUID = -8615829045050170652L;

    public ActivationKeyNotFoundException() {
        super();
    }
}
