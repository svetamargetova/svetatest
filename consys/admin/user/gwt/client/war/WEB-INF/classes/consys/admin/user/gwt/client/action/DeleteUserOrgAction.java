package consys.admin.user.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class DeleteUserOrgAction implements Action<VoidResult> {

    private static final long serialVersionUID = 7642456756711677245L;
    private String uuid;

    public DeleteUserOrgAction() {
    }

    public DeleteUserOrgAction(String id) {
        this.uuid = id;
    }

    public String getUuid() {
        return uuid;
    }
}
