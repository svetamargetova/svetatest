package consys.admin.user.gwt.client.action;

import consys.common.gwt.shared.bo.ClientPersonalInfo;
import consys.common.gwt.shared.action.Action;

/**
 * Akce pro nacteni osobnich informaci uzivatele (celiak, vegetarian, ...)
 * @author pepa
 */
public class LoadPersonalInfoAction implements Action<ClientPersonalInfo> {

    private static final long serialVersionUID = 8100886542563021852L;
}
