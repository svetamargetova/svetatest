package consys.admin.user.gwt.client.module.profile.organization;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.CreateUserOrgAction;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.CreateDialog;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * CreateDialog pro pridani nove uzivatelovy organizace
 * TODO: po uspesnem pridani reload
 *
 * @author pepa
 */
public class NewUserOrganizationDialog extends CreateDialog {

    // komponenty
    private UserOrgForm form;
    private FlowPanel failPanel;
    private Delegate delegate;

    public NewUserOrganizationDialog(Delegate delegate) {
        super(AUMessageUtils.c.newUserOrganizationDialog_title());
        this.delegate = delegate;
    }

    @Override
    protected Widget createForm() {
        FlowPanel fp = new FlowPanel();
        failPanel = new FlowPanel();
        form = new UserOrgForm() {

            @Override
            public SimpleFormPanel getFormPanel() {
                return NewUserOrganizationDialog.this.getFormPanel();
            }
        };
        fp.add(failPanel);
        fp.add(form);
        return fp;
    }

    @Override
    protected void onSave() {
        if (form.validate(getFailMessage())) {
            final ClientUserOrg uo = form.getClientUserOrg();
            DispatchEvent createEvent = new DispatchEvent(new CreateUserOrgAction(uo), new AsyncCallback<StringResult>() {

                @Override
                public void onFailure(Throwable caught) {
                    // obecne chyby zpracovany ActionExecutorem
                }

                @Override
                public void onSuccess(StringResult result) {
                    uo.setUuid(result.getStringResult());
                    hide();
                    delegate.onSuccessCreate(uo);
                }
            }, getFormPanel());
            EventBus.get().fireEvent(createEvent);
        }
    }

    @Override
    protected void onCancel() {
    }

    public interface Delegate {

        public void onSuccessCreate(ClientUserOrg result);
    }

    @Override
    public ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.addStyleName(StyleUtils.MARGIN_BOT_10);
        DOM.setStyleAttribute(failMessage.getElement(), "marginLeft", "90px");
        failPanel.add(failMessage);
        return failMessage;
    }
}
