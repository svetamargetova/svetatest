package consys.admin.user.gwt.client.utils.css;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
public interface AUCssResources extends CssResource {

    String faqState();

    String faqQuestion();

    String faqAnswer();
    
    String fillEmailDialogTitle();
}
