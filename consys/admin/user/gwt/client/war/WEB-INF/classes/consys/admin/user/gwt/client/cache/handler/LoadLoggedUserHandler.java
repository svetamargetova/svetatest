package consys.admin.user.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;

/**
 *
 * @author pepa
 */
public class LoadLoggedUserHandler implements CacheHandler<LoadLoggedUserAction> {

    private boolean serverRequest = false;


    /** cekani dokud nejsou data nahrana do cache */
    private void cacheAction(final AsyncCallback callback) {
        Cache.get().doCacheAction(new UserCacheAction() {

            @Override
            public void doAction(ClientUser value) {
                callback.onSuccess(value);
            }

            @Override
            public void doTimeoutAction() {
                callback.onFailure(new Exception());
            }
        });
    }

    @Override
    public void doAction(LoadLoggedUserAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
        if (!Cache.get().isRegistered(UserCacheAction.USER)) {
            if (serverRequest) {
                // uz se vola na server, pockame si na vysledek
                cacheAction(callback);
            }
            serverRequest = true;
            ActionExecutor.execute(action, new AsyncCallback<ClientUser>() {

                @Override
                public void onFailure(Throwable caught) {
                    callback.onFailure(caught);
                }

                @Override
                public void onSuccess(ClientUser result) {
                    Cache.get().register(UserCacheAction.USER, result);
                    serverRequest = false;
                    callback.onSuccess(result);
                }
            }, delegate);
        } else {
            cacheAction(callback);
        }
    }
}
