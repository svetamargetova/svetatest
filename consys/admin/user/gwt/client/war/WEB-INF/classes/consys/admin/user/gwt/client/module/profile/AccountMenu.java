package consys.admin.user.gwt.client.module.profile;

import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.user.gwt.client.module.profile.account.PasswordChangeContent;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;

/**
 * Formular pro nastavení hodnot uzivatelskeho uctu (heslo, jmena uzivatele, ...)
 * @author pepa
 */
public class AccountMenu extends SimpleFormPanel implements MenuFormItem {

    // konstanty
    public static final String NAME = AUMessageUtils.c.accountMenu_title_account();
    // data
    private boolean initialized;

    public AccountMenu() {
        super();
        initialized = false;
    }

    /** zinicializuje podcasti zalozky */
    public void init() {
        addWidget(new PasswordChangeContent());
        initialized = true;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        if (!initialized) {
            init();
        }
        panel.setWidget(this);
    }
}
