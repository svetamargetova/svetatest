package consys.admin.user.gwt.client.base;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.ConfirmInvitationAction;
import consys.admin.user.gwt.client.action.LoadInvitationAction;
import consys.common.gwt.shared.bo.ClientRegistration;
import consys.admin.user.gwt.client.module.UserModule;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.widget.validator.PasswordValidator;
import consys.common.gwt.client.widget.validator.PasswordValidator.PasswordStrengthEnum;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysPasswordTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.comp.progress.Progress;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.HistoryUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 *
 * @author pepa
 */
public class InvitationContent extends RootPanel {

    // konstaty
    public static final String TOKEN = "token";
    // komponenty
    private ConsysStringTextBox firstNameBox;
    private ConsysStringTextBox lastNameBox;
    private ConsysStringTextBox contactEmailBox;
    private ConsysStringTextBox fakePasswordBox;
    private ConsysPasswordTextBox passwordBox;
    private ConsysPasswordTextBox passwordAgainBox;
    private ConsysCheckBox showPassword;
    private BaseForm form;
    // data
    private ClientRegistration reg;
    private String token;

    public InvitationContent() {
        super(AUMessageUtils.c.invitationContent_title());
    }

    @Override
    protected void onLoad() {
        clear();
        token = HistoryUtils.getParameter(TOKEN);
        if (token != null && !token.isEmpty()) {
            DispatchEvent loadInvitation = new DispatchEvent(new LoadInvitationAction(token),
                    new AsyncCallback<ClientRegistration>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava actionExcecutor
                        }

                        @Override
                        public void onSuccess(ClientRegistration result) {
                            init(result);
                        }
                    }, this);
            EventBus.get().fireEvent(loadInvitation);
        } else {
            getFailMessage().setText(AUMessageUtils.c.invitationContent_error_missingToken());
        }
    }

    /** vytvori obsah formulare */
    private void init(ClientRegistration result) {
        SimpleFormWithHelpPanel mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        firstNameBox = new ConsysStringTextBox(1, 255, CWMessageUtils.c.const_firstName());
        firstNameBox.setText(result.getFirstName() == null ? "" : result.getFirstName());
        lastNameBox = new ConsysStringTextBox(1, 255, CWMessageUtils.c.const_lastName());
        lastNameBox.setText(result.getLastName() == null ? "" : result.getLastName());
        contactEmailBox = new ConsysStringTextBox(1, 255, UIMessageUtils.c.const_email()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                boolean result = super.doValidate(fail);
                if (result) {
                    result = ValidatorUtils.isValidEmailAddress(getText().trim());
                    if (!result) {
                        fail.addOrSetText(UIMessageUtils.c.const_error_invalidEmailAddress());
                    }
                }
                return result;
            }
        };
        contactEmailBox.setEnabled(false);
        contactEmailBox.setText(result.getContactEmail());
        fakePasswordBox = new ConsysStringTextBox("100px", 6, 255, CWMessageUtils.c.const_password());
        fakePasswordBox.setVisible(false);
        passwordBox = new ConsysPasswordTextBox("100px", 6, 255, CWMessageUtils.c.const_password());
        passwordAgainBox = new ConsysPasswordTextBox("100px", 6, 255, CWMessageUtils.c.const_confirmPassword()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                // validuje se pouze vzhledem ke shode passwordBox
                boolean result = true;
                if (!PasswordValidator.changePassword(reg.getPassword(), reg.getConfirmPassword())) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordsNotMatch());
                    result = false;
                }
                return result;
            }
        };

        showPassword = new ConsysCheckBox();
        showPassword.addStyleName(FLOAT_LEFT);

        FlowPanel checkPanel = new FlowPanel();
        checkPanel.addStyleName(FLOAT_RIGHT);
        checkPanel.addStyleName(MARGIN_TOP_3);
        checkPanel.add(showPassword);
        checkPanel.add(StyleUtils.getStyledLabel(CWMessageUtils.c.const_showPassword(), MARGIN_LEFT_5, FLOAT_LEFT));
        checkPanel.add(StyleUtils.clearDiv());

        ConsysFlowPanel passwordPanel = new ConsysFlowPanel() {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                if (!PasswordValidator.passwordStrength(reg.getPassword()).equals(PasswordStrengthEnum.STRONG)) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordIsWeak());
                    return false;
                }
                if (reg.getPassword().contains(" ")) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordContainsSpace());
                    return false;
                }
                return true;
            }

            @Override
            public void onSuccess() {
                passwordBox.onSuccess();
                fakePasswordBox.onSuccess();
            }

            @Override
            public void onFail() {
                passwordBox.onFail();
                fakePasswordBox.onFail();
            }
        };
        passwordPanel.add(checkPanel);
        passwordPanel.add(passwordBox);
        passwordPanel.add(fakePasswordBox);
        passwordPanel.add(StyleUtils.clearDiv());

        Label passwordStrengthLabel = new Label();
        passwordStrengthLabel.setStyleName(FONT_BOLD);
        passwordStrengthLabel.addStyleName(FLOAT_RIGHT);

        ConsysFlowPanel passwordAgainPanel = new ConsysFlowPanel();
        passwordAgainPanel.add(passwordStrengthLabel);
        passwordAgainPanel.add(passwordAgainBox);
        passwordAgainPanel.add(StyleUtils.clearDiv());

        HTML html = new HTML(CWMessageUtils.c.registerUser_text_agreeing());
        html.addStyleName(ResourceUtils.system().css().htmlAgreeing());
        html.addStyleName(MARGIN_TOP_10);
        html.setWidth("270px");

        ActionImage button = ActionImage.getConfirmButton(AUMessageUtils.c.invitationContent_button_registerAccount());
        button.addStyleName(MARGIN_TOP_10);
        button.addClickHandler(confirmClickHandler());

        form = new BaseForm();
        form.addRequired(0, CWMessageUtils.c.const_firstName(), firstNameBox);
        form.addRequired(1, CWMessageUtils.c.const_lastName(), lastNameBox);
        form.addOptional(2, UIMessageUtils.c.const_email(), contactEmailBox);
        form.addRequired(3, CWMessageUtils.c.const_password(), passwordPanel);
        form.addRequired(4, CWMessageUtils.c.const_confirmPassword(), passwordAgainPanel);
        form.addContent(5, html);
        form.addActionMembers(6, button, null);
        mainPanel.addWidget(form);

        mainPanel.addHelpTitle(AUMessageUtils.c.registerContent_helpTitle());
        mainPanel.addHelpText(AUMessageUtils.c.registerContent_helpText1());
        mainPanel.addHelpText(AUMessageUtils.c.registerContent_helpText2());
        mainPanel.addHelpText(AUMessageUtils.c.registerContent_helpText3());

        initHandlers(passwordStrengthLabel);
    }

    @Override
    protected Widget createFootPart() {
        Progress progress = Progress.register(1);
        progress.setStyleName(MARGIN_TOP_20);
        progress.addStyleName(MARGIN_BOT_10);
        return progress;
    }

    /** inicializuje handlery */
    private void initHandlers(final Label passwordStrengthLabel) {
        passwordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(passwordBox, passwordStrengthLabel));
        fakePasswordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(fakePasswordBox, passwordStrengthLabel));
        fakePasswordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                passwordBox.setText(fakePasswordBox.getText());
            }
        });
        passwordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                fakePasswordBox.setText(passwordBox.getText());
            }
        });
        showPassword.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                if (event.getValue()) {
                    passwordAgainBox.onSuccess();
                }
                fakePasswordBox.setVisible(event.getValue());
                passwordBox.setVisible(!event.getValue());
                passwordAgainBox.setEnabled(!event.getValue());
                passwordAgainBox.setText("");
            }
        });
    }

    /** ClickHandler pro odeslani registrace */
    private ClickHandler confirmClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ClientRegistration registration = generateRegistration();
                reg = registration;
                if (!form.validate(getFailMessage())) {
                    return;
                }

                DispatchEvent createUserEvent = new DispatchEvent(new ConfirmInvitationAction(token, registration),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovany ActionExecutorem
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // vynulovani formulare
                                firstNameBox.setText("");
                                lastNameBox.setText("");
                                fakePasswordBox.setText("");
                                passwordBox.setText("");
                                passwordAgainBox.setText("");
                                showPassword.setValue(Boolean.FALSE);
                                // prepnuti contentu
                                History.newItem(UserModule.INVITATIONS);
                            }
                        }, getSelf());
                EventBus.get().fireEvent(createUserEvent);
            }
        };
    }

    /** vygeneruje objekt ClientRegistration z dat ve formulari */
    private ClientRegistration generateRegistration() {
        ClientRegistration registration = new ClientRegistration();
        registration.setFirstName(firstNameBox.getText().trim());
        registration.setLastName(lastNameBox.getText().trim());
        registration.setPassword(passwordBox.getText().trim());

        if (passwordAgainBox.isEnabled()) {
            registration.setConfirmPassword(passwordAgainBox.getText().trim());
        } else {
            registration.setConfirmPassword(fakePasswordBox.getText().trim());
        }
        return registration;
    }
}
