package consys.admin.user.gwt.client.panel;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.widget.security.SpringSecurityAdministrationLoginHandler;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.panel.SSOLoginButtonPanel;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.widget.CommonWidgetsModule;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 * Polozka v Layout Panelu, ktera se stara o prihlaseni uzivatele
 * @author pepa
 */
public class LoginPanel extends PanelItem implements CssStyles {

    // konstanty
    public static final String PANEL_NAME = "LoginPanel";
    private static final String TEXT_LOGIN = UIMessageUtils.c.const_email();
    private static final String TEXT_PASSWORD = CWMessageUtils.c.const_password();
    // komponenty
    private TextBox login;
    private PasswordTextBox password;
    private TextBox fakePassword;
    private ConsysCheckBox checkBox;
    private SpringSecurityAdministrationLoginHandler handler = new SpringSecurityAdministrationLoginHandler() {

        @Override
        public void clearBeforeRedirect() {
            login.setText(TEXT_LOGIN);
            login.setFocus(false);

            password.setText("");
            password.setFocus(false);
        }

        @Override
        public String getLogin() {
            return login.getText().trim();
        }

        @Override
        public String getPassword() {
            return password.getText().trim();
        }

        @Override
        public boolean getRememberMe() {
            return checkBox.getValue();
        }

        @Override
        public String getSuccessRedirect() {
            return null;
        }
    };

    public LoginPanel() {
        super(false);
    }

    @Override
    public String getName() {
        return PANEL_NAME;
    }

    @Override
    public Widget getTitle() {
        return null;
    }

    @Override
    public Widget getContent() {
        VerticalPanel panel = new VerticalPanel();

        Label loginTextLabel = new Label(AUMessageUtils.c.loginPanel_text());
        loginTextLabel.setStyleName(StyleUtils.MARGIN_HOR_10);
        loginTextLabel.addStyleName(StyleUtils.MARGIN_TOP_5);
        loginTextLabel.addStyleName(StyleUtils.MARGIN_BOT_5);

        // vstup pro login (email)
        login = new TextBox();
        login.setText(TEXT_LOGIN);
        DOM.setElementAttribute(login.getElement(), "type", "email");
        DOM.setElementAttribute(login.getElement(), "name", "email");
        setCommonStyle(login);
        login.addStyleName(StyleUtils.MARGIN_BOT_5);

        // vstup pro heslo
        password = new PasswordTextBox();
        password.setVisible(false);
        setCommonStyle(password);

        // falesny vstup pro heslo (pri ziskani focusu se prepne zadavani na skutecny vstup)
        fakePassword = new TextBox();
        fakePassword.setText(TEXT_PASSWORD);
        setCommonStyle(fakePassword);

        // akcni prvky panelu (odesilaci tlacitko a checkbox)
        ActionImage loginButton = ActionImage.getConfirmButton(AUMessageUtils.c.loginPanel_button_logIn());
        loginButton.addClickHandler(loginUserHandler());

        checkBox = new ConsysCheckBox();
        checkBox.setStyleName(StyleUtils.MARGIN_RIGHT_5);

        HorizontalPanel actionPanel = new HorizontalPanel();
        actionPanel.setWidth("199px");
        actionPanel.setStyleName(StyleUtils.MARGIN_TOP_5);
        actionPanel.addStyleName(StyleUtils.MARGIN_HOR_10);
        actionPanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        actionPanel.add(loginButton);

        HorizontalPanel rememberPanel = new HorizontalPanel();
        rememberPanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        rememberPanel.add(checkBox);
        rememberPanel.add(new Label(CWMessageUtils.c.const_rememberMe()));

        actionPanel.add(rememberPanel);
        actionPanel.setCellHorizontalAlignment(rememberPanel, HasAlignment.ALIGN_RIGHT);

        panel.add(loginTextLabel);
        panel.add(login);
        panel.add(fakePassword);
        panel.add(password);
        panel.add(actionPanel);

        // servisni odkazy
        VerticalPanel servicePanel = new VerticalPanel();
        servicePanel.setSize("100%", "70px");
        servicePanel.setHorizontalAlignment(HasAlignment.ALIGN_LEFT);
        servicePanel.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        servicePanel.setStyleName(StyleUtils.MARGIN_TOP_10);

        ActionLabel lostPassword = new ActionLabel(AUMessageUtils.c.loginPanel_action_lostPassword(), StyleUtils.Assemble(FONT_12PX, MARGIN_LEFT_10));
        lostPassword.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CommonWidgetsModule.LOST_PASSWORD);
            }
        });
        ActionLabel notRegistered = new ActionLabel(AUMessageUtils.c.loginPanel_action_notRegistered(), StyleUtils.Assemble(FONT_12PX, MARGIN_LEFT_10));
        notRegistered.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CommonWidgetsModule.REGISTER_ACCOUNT);
            }
        });
        ActionLabel notActivated = new ActionLabel(AUMessageUtils.c.loginPanel_action_notActivated(), StyleUtils.Assemble(FONT_12PX, MARGIN_LEFT_10));
        notActivated.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CommonWidgetsModule.ACTIVATE_ACCOUNT);
            }
        });

        servicePanel.add(lostPassword);
        servicePanel.add(notRegistered);
        servicePanel.add(notActivated);
        panel.add(servicePanel);

        initHandlers();
        FlowPanel resultPanel = new FlowPanel();
        resultPanel.add(new SSOLoginButtonPanel());
        resultPanel.add(panel); // ie je mrcha hnusna, oskliva, smradlava
        return resultPanel;
    }

    private void initHandlers() {
        login.addFocusHandler(new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                if (login.getText().equals(TEXT_LOGIN)) {
                    login.setText("");
                }
            }
        });
        login.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (login.getText().trim().equals("")) {
                    login.setText(TEXT_LOGIN);
                }
            }
        });
        password.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                if (password.getText().trim().equals("")) {
                    password.setText("");
                    password.setVisible(false);
                    fakePassword.setVisible(true);
                }
            }
        });
        password.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    LayoutManager.get().showWaiting(true);
                    handler.doLogin();
                }
            }
        });
        fakePassword.addFocusHandler(new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                fakePassword.setVisible(false);
                password.setVisible(true);
                password.setFocus(true);
            }
        });
    }

    /** nastavi shodne styly pro vsechny vstupy */
    private void setCommonStyle(Widget widget) {
        widget.setStyleName(StyleUtils.BORDER);
        widget.addStyleName(StyleUtils.MARGIN_HOR_10);
        widget.addStyleName(ResourceUtils.system().css().inputPadd());
        widget.setSize("199px", "21px");
    }

    /** vraci ClickHandler, ktery spusti prihlaseni uzivatele */
    private ClickHandler loginUserHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                LayoutManager.get().showWaiting(true);
                handler.doLogin();
            }
        };
    }
}
