package consys.admin.user.gwt.client.module.profile.organization;

import com.google.gwt.event.dom.client.KeyUpEvent;
import consys.common.gwt.client.ui.comp.SuggestionBox;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import consys.admin.user.gwt.client.action.LoadOrganizationAction;
import consys.admin.user.gwt.client.action.LoadUserOrgAction;
import consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException;
import consys.common.gwt.shared.bo.ClientOrganization;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.client.module.profile.organization.OrganizationSuggestOracle.OrganizationSuggestion;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.MonthYearDatePicker;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientAddress;

/**
 * Spolecny formular pro vytvoreni a editaci
 * @author pepa
 */
public abstract class UserOrgForm extends BaseForm implements SelectionHandler<OrganizationSuggestion> {

    // komponenty
    private ConsysStringTextBox positionBox;
    private SuggestionBox atBox;
    private MonthYearDatePicker fromMonthYearBox;
    private MonthYearDatePicker toMonthYearBox;
    private ConsysCheckBox nowCheckBox;
    private ConsysCheckBox preferredBox;
    private OrganizationDetailForm detailForm;
    private VerticalPanel infoPanel;
    private ActionImage buttonEdit;
    private ActionLabel cancelEdit;
    private ActionLabel editOrganization;
    private Label toLabel;
    private Label presentLabel;
    private ConsysFlowPanel timePanel;
    // data
    private ClientUserOrg userOrg;
    private String atSave;

    public UserOrgForm() {
        this(null);
    }

    public UserOrgForm(String uuid) {
        super();
        init(uuid);
    }

    private void init(String uuid) {
        atBox = new SuggestionBox(new OrganizationSuggestOracle());
        atBox.addSelectionHandler((SelectionHandler) this);
        atBox.setRequired(true);
        atBox.getTextBox().addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                atSave = atBox.getText();
            }
        });
        atBox.setWidth("300px");

        positionBox = new ConsysStringTextBox("300px", 1, 255, AUMessageUtils.c.userOrg_form_position());
        nowCheckBox = new ConsysCheckBox();
        nowCheckBox.addStyleName(StyleUtils.FLOAT_LEFT);
        fromMonthYearBox = new MonthYearDatePicker();
        fromMonthYearBox.setRequired(true);
        fromMonthYearBox.addStyleName(StyleUtils.FLOAT_LEFT);
        toMonthYearBox = new MonthYearDatePicker();
        toMonthYearBox.setRequired(true);
        toMonthYearBox.addStyleName(StyleUtils.FLOAT_LEFT);
        preferredBox = new ConsysCheckBox();
        preferredBox.setEnabled(false);
        toLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.const_toLower(),
                StyleUtils.MARGIN_TOP_3, StyleUtils.MARGIN_HOR_5, StyleUtils.FLOAT_LEFT);
        presentLabel = StyleUtils.getStyledLabel(AUMessageUtils.c.userOrg_text_presentLower(),
                StyleUtils.MARGIN_TOP_3, StyleUtils.FLOAT_LEFT);

        FlowPanel nowPanel = new FlowPanel();
        nowPanel.add(nowCheckBox);
        nowPanel.add(StyleUtils.getStyledLabel(AUMessageUtils.c.userOrg_form_currentlyWork(), StyleUtils.MARGIN_LEFT_5, StyleUtils.FLOAT_LEFT));
        nowPanel.add(StyleUtils.clearDiv());

        timePanel = new ConsysFlowPanel();
        timePanel.add(fromMonthYearBox);
        timePanel.add(toLabel);
        timePanel.add(toMonthYearBox);
        timePanel.add(StyleUtils.clearDiv());
        addNowCheckBoxClickHandler();

        detailForm = new OrganizationDetailForm();
        detailForm.setVisible(false);
        detailForm.setSuggestInput(atBox);

        infoPanel = new VerticalPanel();
        infoPanel.setVisible(false);

        setStyleName(StyleUtils.MARGIN_LEFT_20);
        addRequired(0, AUMessageUtils.c.userOrg_form_position(), positionBox);

        editOrganization = FormPanelUtils.getEditModeButton(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showOrganizationSettings();
            }
        });
        editOrganization.setVisible(false);

        addRequired(1, AUMessageUtils.c.userOrg_form_at(), atBox);
        addActionWidget(1, editOrganization);

        addContent(2, infoPanel);
        addContent(3, detailForm);
        addRequired(4, AUMessageUtils.c.userOrg_form_timePeriod(), nowPanel);
        addContent(5, timePanel);
        addOptional(6, AUMessageUtils.c.userOrg_form_preferredPosition(), preferredBox);

        if (uuid != null) {
            buttonEdit = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
            buttonEdit.addClickHandler(confirmUpdateClickHandler());
            cancelEdit = new ActionLabel(UIMessageUtils.c.const_cancel());
            cancelEdit.setClickHandler(viewClickHandler());
            addActionMembers(7, buttonEdit, cancelEdit);
        }

        if (uuid == null) {
            userOrg = new ClientUserOrg();
        } else {
            DispatchEvent dataEvent = new DispatchEvent(new LoadUserOrgAction(uuid),
                    new AsyncCallback<ClientUserOrg>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovany ActionExecutorem
                            if (caught instanceof UserOrganizationUuidException) {
                                getFormPanel().getFailMessage().setText(AUMessageUtils.c.userOrg_error_invalidIdOfExperience());
                            }
                        }

                        @Override
                        public void onSuccess(ClientUserOrg result) {
                            setClientUserOrg(result);
                        }
                    }, getFormPanel());
            EventBus.get().fireEvent(dataEvent);
        }
    }

    private void addNowCheckBoxClickHandler() {
        nowCheckBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                if (event.getValue()) {
                    showToPresent(true);
                } else {
                    showToPresent(false);
                }
            }
        });
    }

    /** vygeneruje ClientUserOrg z dat ve formulari */
    public ClientUserOrg getClientUserOrg() {
        userOrg.setPosition(positionBox.getText().trim());
        userOrg.setActualPosition(nowCheckBox.getValue());
        userOrg.setFromDate(fromMonthYearBox.getDate());
        if (!userOrg.isActualPosition()) {
            userOrg.setToDate(toMonthYearBox.getDate());
        }
        userOrg.setPreferredPosition(preferredBox.getValue());
        if (detailForm.isVisible()) {
            userOrg.setOrganization(detailForm.getOrganization());
        } else {
            ClientOrganization organization = new ClientOrganization();
            organization.setWeb("");
            organization.setUniversalName("");
            organization.setOrganizationType(0);
            organization.setAcronym("");
            organization.setFullName(atBox.getText());

            ClientAddress address = new ClientAddress();
            address.setLocation(0);
            address.setCity("");
            address.setStreet("");
            address.setZip("");
            organization.setAddress(address);

            userOrg.setOrganization(organization);

        }
        return userOrg;
    }

    /** nastavi data do formulare */
    private void setClientUserOrg(ClientUserOrg data) {
        userOrg = data;
        positionBox.setText(data.getPosition());
        fromMonthYearBox.setDate(data.getFromDate());
        toMonthYearBox.setDate(data.getToDate());
        preferredBox.setValue(data.isPreferredPosition());
        atBox.setText(data.getOrganization().getFullName());
        editOrganization.setVisible(data.getOrganization() != null && data.getOrganization().isOwner());
        detailForm.setOrganization(data.getOrganization());
        nowCheckBox.setValue(data.isActualPosition());
        setOrganizationInfo(data.getOrganization());
        showToPresent(data.isActualPosition());
    }

    public void showToPresent(boolean present) {
        preferredBox.setEnabled(present);
        if (present && presentLabel.getParent() == null) {
            timePanel.clear();
            timePanel.add(fromMonthYearBox);
            timePanel.add(toLabel);
            timePanel.add(presentLabel);
            timePanel.add(StyleUtils.clearDiv());
        } else if (!present && toMonthYearBox.getParent() == null) {
            timePanel.clear();
            timePanel.add(fromMonthYearBox);
            timePanel.add(toLabel);
            timePanel.add(toMonthYearBox);
            timePanel.add(StyleUtils.clearDiv());
        }
    }

    @Override
    public void onSelection(SelectionEvent<OrganizationSuggestion> event) {
        OrganizationSuggestion suggestion = event.getSelectedItem();
        if (suggestion instanceof OrganizationSuggestion) {
            OrganizationSuggestion consysSuggestion = (OrganizationSuggestion) suggestion;
            if (consysSuggestion.getSuggestionId().equals(OrganizationSuggestOracle.CreateOrganizationSuggestion.CREATE_ORGANIZATION)) {
                ClientOrganization organization = new ClientOrganization();
                organization.setOwner(true);
                organization.setFullName(atSave);
                detailForm.setOrganization(organization);
                showOrganizationSettings();
            } else {
                // existujici organizace
                DispatchEvent loadEvent = new DispatchEvent(new LoadOrganizationAction(consysSuggestion.getThumb().getUuid()),
                        new AsyncCallback<ClientOrganization>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovany ActionExecutorem
                            }

                            @Override
                            public void onSuccess(ClientOrganization result) {
                                detailForm.setVisible(false);
                                setOrganizationInfo(result);
                                nowCheckBox.setFocus(true);
                                detailForm.setOrganization(result);
                                editOrganization.setVisible(result != null && result.isOwner());
                            }
                        }, null);
                EventBus.get().fireEvent(loadEvent);
            }
        }
    }

    private void setOrganizationInfo(ClientOrganization result) {
        StringBuffer sb = new StringBuffer();
        sb.append(result.getAcronym());
        if (result.getUniversalName() != null && result.getUniversalName().length() > 0) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(result.getUniversalName());
        }
        infoPanel.setVisible(true);
        infoPanel.clear();
        if (sb.length() > 0) {
            infoPanel.add(StyleUtils.getStyledLabel(sb.toString(), StyleUtils.FONT_10PX));
        }

        String address = result.getAddress() == null ? null : result.getAddress().toString();
        if (address != null && address.length() > 0) {
            infoPanel.add(StyleUtils.getStyledLabel(address, StyleUtils.FONT_10PX));
        }
        if (result.getWeb() != null && result.getWeb().length() > 0) {
            infoPanel.add(StyleUtils.getStyledLabel(result.getWeb(), StyleUtils.FONT_10PX));
        }
    }

    private void showOrganizationSettings() {
        // nova organizace
        infoPanel.setVisible(false);
        infoPanel.clear();
        atBox.getTextBox().setEnabled(false);
        detailForm.setVisible(true);
        editOrganization.setVisible(false);
        detailForm.setNameFocus();
    }

    /** panel ve kterem se zobrazi cekaci kolecko */
    public abstract SimpleFormPanel getFormPanel();

    /** ClickHandler pro potvrzeni editace */
    public ClickHandler confirmUpdateClickHandler() {
        throw new UnsupportedOperationException("Override to edit mode");
    }

    /** ClickHandler pro zruseni editace a prepnuti do prohlizeciho modu */
    public ClickHandler viewClickHandler() {
        throw new UnsupportedOperationException("Override to edit mode");
    }
}
