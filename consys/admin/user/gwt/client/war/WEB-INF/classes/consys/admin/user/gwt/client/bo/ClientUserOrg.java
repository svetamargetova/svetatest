package consys.admin.user.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientOrganization;
import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.bo.ClientAddress;
import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 * Objekt uzivatelovy organizace
 * @author pepa
 */
public class ClientUserOrg implements IsSerializable, Result {

    private static final long serialVersionUID = 8501867062215258505L;
    private String uuid;
    private String position;
    private ClientOrganization organization;
    private ClientAddress address;
    private Date fromDate;
    private Date toDate;
    private boolean actualPosition;
    private boolean preferredPosition;

    public ClientUserOrg() {
    }

    

    public boolean isActualPosition() {
        return actualPosition;
    }

    public void setActualPosition(boolean actualPosition) {
        this.actualPosition = actualPosition;
    }

    public ClientAddress getAddress() {
        return address;
    }

    public void setAddress(ClientAddress address) {
        this.address = address;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public ClientOrganization getOrganization() {
        return organization;
    }

    public void setOrganization(ClientOrganization organization) {
        this.organization = organization;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isPreferredPosition() {
        return preferredPosition;
    }

    public void setPreferredPosition(boolean preferredPosition) {
        this.preferredPosition = preferredPosition;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
