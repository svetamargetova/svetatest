package consys.admin.user.gwt.client.module.profile.account;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.LoadLoggedUserAction;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.constants.img.UserProfileImageEnum;
import consys.admin.user.gwt.client.event.UpdatedUserEvent;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.ServletConstants;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.HiddableLabel;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.ServletUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Formular pro zmenu profiloveho obrazku
 * @author pepa
 */
public class ProfileImageContent extends RUSimpleFormPanel implements UpdatedUserEvent.Handler {

    // konstanty
    public static final String WIDTH = "250px";
    // komponenty
    private Image portrait;
    private FormPanel formPanel;
    // data
    private ClientUser loggedUser;

    public ProfileImageContent(HiddableLabel hiddableLabel) {
        super(WIDTH, false);
        setHiddableLabel(hiddableLabel);
        setBodyWidth(WIDTH);
        addStyleName(OVERFLOW_HIDDEN);
    }

    /** aktualizace obrazku */
    private void refreshImage(ClientUser cu) {
        if (cu.getUuidImg() == null) {
            portrait.setResource(ResourceUtils.system().portraitBig());
        } else {
            String url = GWT.getModuleBaseURL() + ServletConstants.USER_PROFILE_IMG_LOAD + UserProfileImageEnum.PROFILE.getFullUuid(cu.getUuidImg());
            portrait.setUrl(url);
        }
    }

    @Override
    public Widget readForm() {
        FlowPanel viewPanel = new FlowPanel();
        viewPanel.setStyleName(StyleUtils.MARGIN_BOT_10);
        DOM.setStyleAttribute(viewPanel.getElement(), "marginRight", "50px");
        portrait = new Image();
        ActionImage editButton = ActionImage.getPersonButton(AUMessageUtils.c.profileImageContent_button_picture());
        editButton.addStyleName(StyleUtils.MARGIN_TOP_10);
        editButton.addClickHandler(toEditModeClickHandler(null));

        viewPanel.add(portrait);
        viewPanel.add(editButton);

        // pokusi sa znovunacitat usera
        EventBus.get().fireEvent(new DispatchEvent(new LoadLoggedUserAction(), new AsyncCallback<ClientUser>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ClientUser result) {
                loggedUser = result;
                refreshImage(result);
            }
        }, this));

        return viewPanel;
    }

    @Override
    public Widget updateForm(String id) {
        formPanel = new FormPanel();
        formPanel.setAction(ServletConstants.BASE_URL + ServletConstants.USER_PROFILE_IMG_SAVE);
        formPanel.setMethod(FormPanel.METHOD_POST);
        formPanel.setStyleName(StyleUtils.MARGIN_BOT_10);
        formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);
        formPanel.addSubmitHandler(new SubmitHandler() {

            @Override
            public void onSubmit(SubmitEvent event) {
                showWaiting(true);
            }
        });
        formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {

            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {
                String result = event.getResults();
                // odstraneni pripadnych tagu
                result = result.replaceAll(ServletUtils.TAG_MATCH, "").trim();
                result = result.replaceAll(ServletUtils.TAG_MATCH_CODE, "").trim();
                if (!ServletUtils.containHttpFail(result, getFailMessage())) {
                    // len posleme update, netahame ze serveru
                    loggedUser.setUuidImg(result);
                    EventBus.get().fireEvent(new UpdatedUserEvent(loggedUser));
                    ProfileImageContent.this.onSuccessUpdate(AUMessageUtils.c.profileImageContent_text_profileUpdateSuccessfull());
                } else {
                    ProfileImageContent.this.onFailedUpdate(AUMessageUtils.c.profileImageContent_error_profileUpdateFailed());
                }
                showWaiting(false);
            }
        });

        FlowPanel panel = FormUtils.sendFileWrapper("imageFile", 250, null,
                confirmUpdateClickHandler(null), toReadModeClickHandler(null));

        formPanel.setWidget(panel);
        return formPanel;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                formPanel.submit();
            }
        };
    }

    @Override
    public void onUpdatedUser(UpdatedUserEvent event) {
        refreshImage(event.getUser());
    }
}
