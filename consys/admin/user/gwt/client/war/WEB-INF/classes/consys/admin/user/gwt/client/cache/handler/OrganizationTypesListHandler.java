package consys.admin.user.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.admin.user.gwt.client.action.ListOrganizationTypesAction;
import consys.admin.user.gwt.client.cache.OrganizationTypesCacheAction;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.cache.CacheTimeoutException;
import consys.common.gwt.client.rpc.result.SelectBoxData;

/**
 *
 * @author Palo
 */
public class OrganizationTypesListHandler implements CacheHandler<ListOrganizationTypesAction>{

    @Override
    public void doAction(ListOrganizationTypesAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
         if (!Cache.get().isRegistered(OrganizationTypesCacheAction.INDUSTRY)) {
                        
            ActionExecutor.execute(action, new AsyncCallback<SelectBoxData>() {

                @Override
                public void onFailure(Throwable caught) {
                    callback.onFailure(caught);
                }

                @Override
                public void onSuccess(SelectBoxData result) {
                    Cache.get().register(OrganizationTypesCacheAction.INDUSTRY, result);
                    callback.onSuccess(result);
                }
            }, delegate);
        } else {
           Cache.get().doCacheAction(new OrganizationTypesCacheAction() {

                @Override
                public void doAction(SelectBoxData value) {
                    callback.onSuccess(value);
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(INDUSTRY));
                }
            });
        }
    }

}
