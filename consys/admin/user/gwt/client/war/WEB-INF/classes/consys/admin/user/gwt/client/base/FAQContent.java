package consys.admin.user.gwt.client.base;

import consys.admin.user.gwt.client.base.faq.FAQItem;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.ui.comp.panel.RootPanel;

/**
 *
 * @author pepa
 */
public class FAQContent extends RootPanel {

    public FAQContent() {
        super(AUMessageUtils.c.faqContent_title());

        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question1(), AUMessageUtils.c.faqContent_answer1()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question2(), AUMessageUtils.c.faqContent_answer2()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question3(), AUMessageUtils.c.faqContent_answer3()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question4(), AUMessageUtils.c.faqContent_answer4()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question5(), AUMessageUtils.c.faqContent_answer5()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question6(), AUMessageUtils.c.faqContent_answer6()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question7(), AUMessageUtils.c.faqContent_answer7()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question8(), AUMessageUtils.c.faqContent_answer8()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question9(), AUMessageUtils.c.faqContent_answer9()));
        addWidget(new FAQItem(AUMessageUtils.c.faqContent_question10(), AUMessageUtils.c.faqContent_answer10()));
    }
}
