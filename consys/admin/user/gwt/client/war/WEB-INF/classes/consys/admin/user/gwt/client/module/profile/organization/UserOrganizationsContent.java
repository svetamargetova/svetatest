package consys.admin.user.gwt.client.module.profile.organization;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.DeleteUserOrgAction;
import consys.admin.user.gwt.client.action.ListUserOrgsAction;
import consys.admin.user.gwt.client.action.UpdateUserOrgAction;
import consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.abst.CRUDSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Formular pro zadani uzivatelovych organizaci
 * @author pepa
 */
public class UserOrganizationsContent extends CRUDSimpleHelpFormPanel implements NewUserOrganizationDialog.Delegate {

    private static final Logger logger = LoggerFactory.getLogger(UserOrganizationsContent.class);
    // komponenty
    private UserOrgForm form;
    // Create form
    private NewUserOrganizationDialog newOrganization;
    // Read form
    private UserOrgPanel viewPanel;

    public UserOrganizationsContent() {
        super(true);
        addHelpTitle("For what are organization used?");
        addHelpText("When you will go to some event, the organizers want to know your afiliation. Takeplace provide automatic afiliton system which will save your time.");
    }

    @Override
    public Widget readForm() {
        Label title = StyleUtils.getStyledLabel(AUMessageUtils.c.userOrganizationsContent_title(), StyleUtils.FONT_16PX,
                StyleUtils.FLOAT_LEFT, StyleUtils.FONT_BOLD);

        ActionLabel add = new ActionLabel(UIMessageUtils.c.const_add(), StyleUtils.Assemble(StyleUtils.MARGIN_LEFT_20,
                StyleUtils.FONT_10PX, StyleUtils.MARGIN_TOP_3, StyleUtils.FLOAT_LEFT));
        add.setClickHandler(createClickHandler());

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(StyleUtils.MARGIN_BOT_10);
        panel.add(title);
        panel.add(add);
        panel.add(StyleUtils.clearDiv());
        showHelp();
        viewPanel = new UserOrgPanel();

        DispatchEvent listEvent = new DispatchEvent(new ListUserOrgsAction(), new AsyncCallback<ArrayListResult<ClientUserOrgProfileThumb>>() {

            @Override
            public void onFailure(Throwable caught) {
                logger.debug("Error", caught);
                getFailMessage().setText(AUMessageUtils.c.userOrganizationsContent_error_experienceListFailed());
            }

            @Override
            public void onSuccess(ArrayListResult<ClientUserOrgProfileThumb> result) {
                logger.debug("Downloaded " + result.getArrayListResult().size() + " experiences");
                for (ClientUserOrgProfileThumb uo : result.getArrayListResult()) {
                    String uuid = uo.getUuid();

                    UserOrgItem item = new UserOrgItem(uo, toEditModeClickHandler(uuid));
                    item.setDeleteAction(getDeleteAction(uuid, item));
                    viewPanel.addItem(item);
                }
            }
        }, getSelf());
        EventBus.get().fireEvent(listEvent);

        FlowPanel vp = new FlowPanel();
        vp.setWidth("95%");
        vp.add(panel);
        vp.add(viewPanel);
        return vp;
    }

    @Override
    public Widget createForm() {
        newOrganization = new NewUserOrganizationDialog(this);
        newOrganization.showCentered();
        return null;
    }

    @Override
    public Widget updateForm(final String id) {
        hideHelp();
        form = new UserOrgForm(id) {

            @Override
            public SimpleFormPanel getFormPanel() {
                return UserOrganizationsContent.this;
            }

            @Override
            public ClickHandler confirmUpdateClickHandler() {
                return UserOrganizationsContent.this.confirmUpdateClickHandler(null);

            }

            @Override
            public ClickHandler viewClickHandler() {
                return UserOrganizationsContent.this.toReadModeClickHandler(id);
            }
        };
        return form;
    }

    @Override
    public Widget deleteForm(String id) {
        return null;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (form.validate(getFailMessage())) {
                    ClientUserOrg uo = form.getClientUserOrg();
                    // TODO: validace
                    DispatchEvent editEvent = new DispatchEvent(new UpdateUserOrgAction(uo), new AsyncCallback<VoidResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                        }

                        @Override
                        public void onSuccess(VoidResult result) {
                            // TODO: aktualizace polozky v prehledu
                            onSuccessUpdate(AUMessageUtils.c.userOrganizationsContent_text_experienceSucessfullyUpdated());
                        }
                    }, UserOrganizationsContent.this);
                    EventBus.get().fireEvent(editEvent);
                }
            }
        };
    }

    @Override
    public ClickHandler confirmCreateClickHandler() {
        throw new UnsupportedOperationException("Not supported, use NewOrganizationDialog");
    }

    @Override
    public ClickHandler confirmDeleteClickHandler(final String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                DispatchEvent deleteEvent = new DispatchEvent(new DeleteUserOrgAction(id), new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        // TODO: odstraneni polozku v prehledu
                        clearMessageBox();
                        readForm();
                    }
                }, getSelf());
                EventBus.get().fireEvent(deleteEvent);
            }
        };
    }

    @Override
    public void onSuccessCreate(ClientUserOrg result) {
        ClientUserOrgProfileThumb thumb = new ClientUserOrgProfileThumb();
        thumb.setActualPosition(result.isActualPosition());
        thumb.setFrom(result.getFromDate());
        thumb.setOrgName(result.getOrganization().getFullName());
        thumb.setPosition(result.getPosition());
        thumb.setTo(result.getToDate());
        thumb.setUuid(result.getUuid());

        UserOrgItem item = new UserOrgItem(thumb, toEditModeClickHandler(result.getUuid()));
        item.setDeleteAction(getDeleteAction(result.getUuid(), item));

        viewPanel.addItem(item);
    }

    /** vytvori mazaci akci */
    private ConsysAction getDeleteAction(final String uuid, final UserOrgItem item) {
        return new ConsysAction() {

            @Override
            public void run() {
                EventBus.get().fireEvent(new DispatchEvent(new DeleteUserOrgAction(uuid),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                if (caught instanceof UserOrganizationUuidException) {
                                    viewPanel.getFailMessage().setText(AUMessageUtils.c.userOrg_error_userOrganizationNotExists());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                item.getInstance().removeFromParent();
                            }
                        }, viewPanel));
            }
        };
    }
}
