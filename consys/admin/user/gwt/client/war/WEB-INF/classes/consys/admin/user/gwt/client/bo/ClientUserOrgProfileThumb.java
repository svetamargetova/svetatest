package consys.admin.user.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 * Objekt ktory obsahuje informacie do profilu
 * @author pepa
 */
public class ClientUserOrgProfileThumb implements IsSerializable, Result {

    private static final long serialVersionUID = 2918710775435149115L;
    
    private String uuid;
    private String position;
    private String orgName;
    private Date from;
    private Date to;
    private boolean actualPosition;
    private boolean prefferedPosition;

    public ClientUserOrgProfileThumb() {
    }


    

    public boolean isActualPosition() {
        return actualPosition;
    }

    public void setActualPosition(boolean actualPosition) {
        this.actualPosition = actualPosition;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }    

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the prefferedPosition
     */
    public boolean isPrefferedPosition() {
        return prefferedPosition;
    }

    /**
     * @param prefferedPosition the prefferedPosition to set
     */
    public void setPrefferedPosition(boolean prefferedPosition) {
        this.prefferedPosition = prefferedPosition;
    }
}
