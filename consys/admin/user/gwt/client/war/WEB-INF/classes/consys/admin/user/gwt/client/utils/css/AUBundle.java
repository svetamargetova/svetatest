package consys.admin.user.gwt.client.utils.css;

import com.google.gwt.resources.client.ClientBundle;

/**
 *
 * @author pepa
 */
public interface AUBundle extends ClientBundle {

    @Source("au.css")
    public AUCssResources css();
}
