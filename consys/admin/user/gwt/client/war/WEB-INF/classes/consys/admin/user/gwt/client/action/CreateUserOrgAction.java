package consys.admin.user.gwt.client.action;

import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author pepa
 */
public class CreateUserOrgAction implements Action<StringResult> {

    private static final long serialVersionUID = -1522909846976305720L;
    private ClientUserOrg uo;

    public CreateUserOrgAction() {
    }

    public CreateUserOrgAction(ClientUserOrg uo) {
        this.uo = uo;
    }

    public ClientUserOrg getClientUserOrg() {
        return uo;
    }
}
