package consys.admin.user.gwt.client.base;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.ActivateUserAction;
import consys.admin.user.gwt.client.action.exception.ActivationKeyIsInvitationException;
import consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException;
import consys.admin.user.gwt.client.module.UserModule;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.comp.progress.Progress;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.HistoryUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;

/**
 * Formular pro aktivaci zaregistrovaneho uctu
 * @author pepa
 */
public class ActivateContent extends RootPanel {

    // konstanty
    private static final String PARAM_ID = "code";
    // komponenty
    private ConsysStringTextBox activateCodeBox;
    private SimplePanel footPanel;
    // data
    private ConsysAction successAction;

    public ActivateContent() {
        this(true, null);
    }

    public ActivateContent(boolean showFoot, ConsysAction successAction) {
        super(AUMessageUtils.c.activateContent_title());

        this.successAction = successAction;

        SimpleFormWithHelpPanel mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        Label text1 = StyleUtils.getStyledLabel(AUMessageUtils.c.activateContent_text1(), StyleUtils.MARGIN_BOT_10);
        Label text2 = StyleUtils.getStyledLabel(AUMessageUtils.c.activateContent_text2(), StyleUtils.MARGIN_BOT_10);
        Label text3 = StyleUtils.getStyledLabel(AUMessageUtils.c.activateContent_text3(), StyleUtils.MARGIN_BOT_10);

        activateCodeBox = new ConsysStringTextBox(32, 32, null) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                boolean result = getText().trim().length() == 32;
                if (!result) {
                    fail.setText(AUMessageUtils.c.activateContent_error_activateFieldBadInput());
                }
                return result;
            }
        };

        ActionImage button = ActionImage.getConfirmButton(AUMessageUtils.c.activateContent_button_confirmAccount());
        button.addStyleName(StyleUtils.MARGIN_TOP_10);
        button.addClickHandler(confirmClickHandler());

        BaseForm form = new BaseForm();
        form.addContent(0, text1);
        form.addContent(1, text2);
        form.addContent(2, text3);
        form.addContent(3, activateCodeBox);
        form.addActionMembers(4, button, null);
        mainPanel.addWidget(form);

        mainPanel.addHelpTitle(AUMessageUtils.c.activateContent_helpTitle());
        mainPanel.addHelpText(AUMessageUtils.c.activateContent_helpText1());
        mainPanel.addHelpText(AUMessageUtils.c.activateContent_helpText2());
        mainPanel.addHelpText(AUMessageUtils.c.activateContent_helpText3());

        fillActivateCode();

        footPanel.setVisible(showFoot);
    }

    @Override
    protected Widget createFootPart() {
        Progress progress = Progress.register(2);
        progress.setStyleName(StyleUtils.MARGIN_TOP_20);
        progress.addStyleName(StyleUtils.MARGIN_BOT_10);

        footPanel = new SimplePanel(progress);
        return footPanel;
    }

    /** pokud je zadan parametr s id aktivace je vyplneno do textboxu */
    private void fillActivateCode() {
        String value = HistoryUtils.getParameter(PARAM_ID);
        if (StringUtils.isNotBlank(value)) {            
            activateCodeBox.setText(value == null ? "" : value);
            activateAccount();
        }
    }

    /** vraci ClickHandler pro spusteni aktivace */
    private ClickHandler confirmClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                activateAccount();
            }
        };
    }

    private void activateAccount() {
        if (!activateCodeBox.doValidate(getFailMessage())) {
            return;
        }
        DispatchEvent activateEvent = new DispatchEvent(new ActivateUserAction(activateCodeBox.getText().trim()),
                new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovany ActionExecutorem
                        if (caught instanceof ActivationKeyNotFoundException) {
                            getFailMessage().setText(AUMessageUtils.c.activateContent_error_activationFailed());
                        } else if (caught instanceof ActivationKeyIsInvitationException) {
                            // prehodime ho automaticky na invitation
                            History.newItem(UserModule.INVITATION + "?" + InvitationContent.TOKEN + "=" + activateCodeBox.getText().trim());
                        }
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        // vynulovani formulare
                        activateCodeBox.setText("");
                        // prepnuti contentu
                        if (successAction == null) {
                            History.newItem(UserModule.LOG_IN + "?" + LoginContent.PARAM_ACTIVATED);
                        } else {
                            successAction.run();
                        }
                    }
                }, getSelf());
        EventBus.get().fireEvent(activateEvent);
    }
}
