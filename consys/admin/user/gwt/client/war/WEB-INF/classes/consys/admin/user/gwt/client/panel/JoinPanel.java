package consys.admin.user.gwt.client.panel;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Marketingovy panel
 * @author pepa
 */
public class JoinPanel extends PanelItem {

    // konstanty
    public static final String PANEL_NAME = "JoinPanel";

    public JoinPanel() {
        super(false);
    }

    @Override
    public String getName() {
        return PANEL_NAME;
    }

    @Override
    public Widget getTitle() {
        return null;
    }

    @Override
    public Widget getContent() {
        FlowPanel panel = new FlowPanel();
        panel.setStyleName(StyleUtils.PADDING_VER_5);

        Label titleLabel = new Label(AUMessageUtils.c.joinPanel_title());
        titleLabel.setStyleName(StyleUtils.FONT_19PX);
        titleLabel.addStyleName(StyleUtils.FONT_BOLD);
        titleLabel.addStyleName(StyleUtils.TEXT_BLUE);
        titleLabel.addStyleName(StyleUtils.MARGIN_HOR_10);
        titleLabel.addStyleName(StyleUtils.MARGIN_BOT_10);

        Label textLabel = new Label(AUMessageUtils.c.joinPanel_text());
        textLabel.setStyleName(StyleUtils.MARGIN_HOR_10);

        panel.add(titleLabel);
        panel.add(textLabel);

        return panel;
    }
}
