package consys.admin.user.gwt.client.module.profile.account;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.user.gwt.client.action.UpdatePasswordAction;
import consys.admin.user.gwt.client.bo.ClientPassword;
import consys.admin.user.gwt.client.utils.AUMessageUtils;
import consys.common.gwt.client.widget.validator.PasswordValidator;
import consys.common.gwt.client.widget.validator.PasswordValidator.PasswordStrengthEnum;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysPasswordTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleFormPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.client.widget.utils.CWMessageUtils;

/**
 * Formulář pro změnu hesla k účtu
 * @author pepa
 */
public class PasswordChangeContent extends RUSimpleFormPanel {

    // komponenty
    private BaseForm form;
    private Label passwordStrengthLabel;
    private ConsysPasswordTextBox oldPasswordBox;
    private ConsysPasswordTextBox newPasswordBox;
    private ConsysPasswordTextBox confirmPasswordBox;
    private ConsysStringTextBox fakeOldPasswordBox;
    private ConsysStringTextBox fakeNewPasswordBox;
    private ConsysCheckBox showPassword;
    // data
    private ClientPassword check;

    public PasswordChangeContent() {
        super(true);
    }

    @Override
    public Widget updateForm(String id) {
        form = new BaseForm();
        form.setStyleName(StyleUtils.MARGIN_BOT_10);

        passwordStrengthLabel = StyleUtils.getStyledLabel("", StyleUtils.FONT_BOLD, FLOAT_RIGHT);
        oldPasswordBox = new ConsysPasswordTextBox("100px", 6, 255, AUMessageUtils.c.passwordChangeContent_form_oldPassword()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                int result = ValidatorUtils.isLenght(getText().trim(), min, max);
                if (result != 0) {
                    if (!ValidatorUtils.isValidString(getText().trim())) {
                        fail.addOrSetText(AUMessageUtils.c.passwordChangeContent_error_oldPasswordIsEmpty());
                    } else {
                        fail.addOrSetText(AUMessageUtils.c.passwordChangeContent_error_badLengthOfOldPassword());
                    }
                    return false;
                }
                return true;
            }
        };
        newPasswordBox = new ConsysPasswordTextBox("100px", 6, 255, AUMessageUtils.c.passwordChangeContent_form_newPassword());
        fakeOldPasswordBox = new ConsysStringTextBox("100px", 6, 255, AUMessageUtils.c.passwordChangeContent_form_oldPassword());
        fakeNewPasswordBox = new ConsysStringTextBox("100px", 6, 255, AUMessageUtils.c.passwordChangeContent_form_newPassword());
        confirmPasswordBox = new ConsysPasswordTextBox("100px", 6, 255, CWMessageUtils.c.const_confirmPassword()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                // validuje se pouze vzhledem ke shode newPasswordBox
                boolean result = true;
                if (!PasswordValidator.changePassword(check.getNewPassword(), check.getConfirmPassword())) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordsNotMatch());
                    result = false;
                }
                return result;
            }
        };

        fakeOldPasswordBox.setVisible(false);
        fakeNewPasswordBox.setVisible(false);

        ConsysFlowPanel oldPasswordPanel = new ConsysFlowPanel() {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                return oldPasswordBox.doValidate(fail);
            }

            @Override
            public void onSuccess() {
                oldPasswordBox.onSuccess();
                fakeOldPasswordBox.onSuccess();
            }

            @Override
            public void onFail() {
                oldPasswordBox.onFail();
                fakeOldPasswordBox.onFail();
            }
        };
        oldPasswordPanel.add(oldPasswordBox);
        oldPasswordPanel.add(fakeOldPasswordBox);

        showPassword = new ConsysCheckBox();
        showPassword.addStyleName(FLOAT_LEFT);

        FlowPanel checkPanel = new FlowPanel();
        checkPanel.addStyleName(FLOAT_RIGHT);
        checkPanel.addStyleName(MARGIN_TOP_3);
        checkPanel.add(showPassword);
        checkPanel.add(StyleUtils.getStyledLabel(CWMessageUtils.c.const_showPassword(), MARGIN_LEFT_5, FLOAT_LEFT));
        checkPanel.add(StyleUtils.clearDiv());

        ConsysFlowPanel newPasswordPanel = new ConsysFlowPanel() {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                if (!PasswordValidator.passwordStrength(check.getNewPassword()).equals(PasswordStrengthEnum.STRONG)) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordIsWeak());
                    return false;
                }
                if (check.getNewPassword().contains(" ")) {
                    fail.addOrSetText(CWMessageUtils.c.const_passwordContainsSpace());
                    return false;
                }
                return true;
            }

            @Override
            public void onSuccess() {
                newPasswordBox.onSuccess();
                fakeNewPasswordBox.onSuccess();
            }

            @Override
            public void onFail() {
                newPasswordBox.onFail();
                fakeNewPasswordBox.onFail();
            }
        };
        newPasswordPanel.add(checkPanel);
        newPasswordPanel.add(newPasswordBox);
        newPasswordPanel.add(fakeNewPasswordBox);

        ConsysFlowPanel passwordAgainPanel = new ConsysFlowPanel();
        passwordAgainPanel.add(passwordStrengthLabel);
        passwordAgainPanel.add(confirmPasswordBox);
        passwordAgainPanel.add(StyleUtils.clearDiv());

        initHandlers();

        form.addRequired(0, AUMessageUtils.c.passwordChangeContent_form_oldPassword(), oldPasswordPanel);
        form.addRequired(1, AUMessageUtils.c.passwordChangeContent_form_newPassword(), newPasswordPanel);
        form.addRequired(2, CWMessageUtils.c.const_confirmPassword(), passwordAgainPanel);
        form.addActionMembers(3, FormPanelUtils.getUpdateButton(AUMessageUtils.c.passwordChangeContent_button_changePassword(),
                this, id), FormPanelUtils.getCancelButton(this));
        return form;
    }

    @Override
    public Widget readForm() {
        return FormPanelUtils.getOneLinedReadMode(this, CWMessageUtils.c.const_password(), UIMessageUtils.c.const_change());
    }

    private void initHandlers() {
        oldPasswordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                fakeOldPasswordBox.setText(oldPasswordBox.getText());
            }
        });
        fakeOldPasswordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                oldPasswordBox.setText(fakeOldPasswordBox.getText());
            }
        });
        newPasswordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(newPasswordBox, passwordStrengthLabel));
        newPasswordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                fakeNewPasswordBox.setText(newPasswordBox.getText());
            }
        });
        fakeNewPasswordBox.addKeyUpHandler(PasswordValidator.passwordStrengthHandler(fakeNewPasswordBox, passwordStrengthLabel));
        fakeNewPasswordBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                newPasswordBox.setText(fakeNewPasswordBox.getText());
            }
        });
        showPassword.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                if (event.getValue()) {
                    confirmPasswordBox.onSuccess();
                }
                boolean value = event.getValue();
                oldPasswordBox.setVisible(!value);
                fakeOldPasswordBox.setVisible(value);
                newPasswordBox.setVisible(!value);
                fakeNewPasswordBox.setVisible(value);
                confirmPasswordBox.setEnabled(!value);
                confirmPasswordBox.setText("");
            }
        });
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!PasswordValidator.permitedCharacters(newPasswordBox.getText().trim())) {
                    getFailMessage().setText(AUMessageUtils.c.changePasswordContent_error_invalidCharactersInPassword());
                    return;
                }

                ClientPassword pass = new ClientPassword();
                pass.setOldPassword(oldPasswordBox.getText().trim());
                pass.setNewPassword(newPasswordBox.getText().trim());
                pass.setConfirmPassword(confirmPasswordBox.getText().trim());
                if (confirmPasswordBox.isEnabled()) {
                    pass.setConfirmPassword(confirmPasswordBox.getText().trim());
                } else {
                    pass.setConfirmPassword(fakeNewPasswordBox.getText().trim());
                }
                check = pass;

                if (!form.validate(getFailMessage())) {
                    return;
                }

                DispatchEvent updatePasswordEvent = new DispatchEvent(new UpdatePasswordAction(pass.getNewPassword()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovany ActionExecutorem
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                onSuccessUpdate(AUMessageUtils.c.passwordChangeContent_text_successPasswordChange());
                            }
                        }, getSelf());
                EventBus.get().fireEvent(updatePasswordEvent);
            }
        };
    }
}
