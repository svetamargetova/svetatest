package consys.admin.payment.webservice.impl;

import consys.payment.webservice.EventWebService;
import consys.payment.ws.event.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventWebServiceImpl extends WebServiceGatewaySupport implements EventWebService {

    private static final Logger log = LoggerFactory.getLogger(EventWebServiceImpl.class);

    @Override
    public InitializeEventResponse initializeEvent(InitializeEventRequest request) {
        return (InitializeEventResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public InitializeCloneEventResponse initializeCloneEvent(InitializeCloneEventRequest request) {
        return (InitializeCloneEventResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public BuyLicenseResponse buyLicense(BuyLicenseRequest request) {
        return (BuyLicenseResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public BuyLicenseWithProfileResponse buyLicenseWithProfile(BuyLicenseWithProfileRequest request) {
        return (BuyLicenseWithProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadEventPaymentDetailsResponse loadEventPaymentDetails(LoadEventPaymentDetailsRequest request) {
        return (LoadEventPaymentDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public UpdateEventProfileResponse updateEventProfile(UpdateEventProfileRequest request) {
        return (UpdateEventProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public ListEventProfilesResponse listEventProfiles(ListEventProfilesRequest request) {
        return (ListEventProfilesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadEventPaymentProfileResponse loadEventPaymentProfile(LoadEventPaymentProfileRequest request) {
        return (LoadEventPaymentProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
