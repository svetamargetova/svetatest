package consys.admin.event.json.action.handler;

import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.service.EventService;
import consys.admin.event.json.action.result.EventResultUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.common.json.error.ErrorCode;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserEventsActionHandler implements ActionHandler {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(UserEventsActionHandler.class);
    // sluzby
    @Autowired
    private EventService eventService;

    @Override
    public String getActionName() {
        return "events";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        try {
            List<EventUserOrganization> euos = eventService.listAllEventUserOrganizations(action.getUserUuid(), null);
            EventResultUtils.writeEvents(result, euos);
        } catch (NoRecordException ex) {
            ErrorCode.error(result, ex, "User has no events.");
        } catch (RequiredPropertyNullException ex) {
            ErrorCode.error(result, ex, "Missing user uuid in action header");
        }
    }
}
