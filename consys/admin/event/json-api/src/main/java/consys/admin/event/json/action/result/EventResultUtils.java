package consys.admin.event.json.action.result;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.common.json.Result;
import java.io.IOException;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventResultUtils {


    public static void writeEvents(Result result,List<EventUserOrganization> organizations) throws IOException{     
        result.writeArrayFieldStart("events");        
        for (EventUserOrganization eventUserOrganization : organizations) {
            Event e = eventUserOrganization.getEvent();
            result.writeStartObject();
            result.writeStringField("uuid", e.getUuid());
            result.writeStringField("full-name", e.getFullName());
            result.writeStringField("acronym", e.getAcronym());
            result.writeNumberField("year", e.getYear());
            result.writeStringField("international-name", e.getUniversalName());
            result.writeStringField("short-name", e.getShortName());
            result.writeStringField("date-from", Result.formatDate(e.getFromDate()));
            result.writeStringField("date-to", Result.formatDate(e.getToDate()));
            result.writeStringField("logo-square", e.getPortraitLogoPrefix());
            result.writeStringField("logo-rectangle", e.getImageLogoPrefix());
            result.writeStringField("www", e.getWeb());
            result.writeStringField("description", e.getDescription());
            result.writeStringField("tags", e.getTags());
            result.writeStringField("overseer-contextpath", e.getOverseer().getClientSuffix());
            result.writeObjectFieldStart("location");
            result.writeStringField("country", e.getCountry().getName());
            result.writeStringField("code", e.getCountry().getCode());
            result.writeStringField("city", e.getCity());          
            result.writeEndObject();
            result.writeEndObject();
        }
        result.writeEndArray();
    }
}
