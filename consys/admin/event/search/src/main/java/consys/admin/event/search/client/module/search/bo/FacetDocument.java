/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search.bo;

import consys.admin.event.search.client.module.search.ResponseDocument;
import java.util.ArrayList;

/**
 *
 * @author Palo
 */
public class FacetDocument implements ResponseDocument{


    private ArrayList<FacetField> fields;
    private String name;

    public FacetDocument(String name) {
        fields = new ArrayList<FacetField>();
        this.name = name;
    }

    public void addField(String name, String count){
        getFields().add(new FacetField(name, count));
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the fields
     */
    public ArrayList<FacetField> getFields() {
        return fields;
    }

    public class FacetField{

        private String field;
        private String count;

        public FacetField(String field, String count) {
            this.field = field;
            this.count = count;
        }

        /**
         * @return the field
         */
        public String getField() {
            return field;
        }

        /**
         * @return the count
         */
        public String getCount() {
            return count;
        }

    }

}
