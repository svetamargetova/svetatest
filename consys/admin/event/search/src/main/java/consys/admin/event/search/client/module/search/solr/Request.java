/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr;

import com.allen_sauer.gwt.log.client.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Palo
 */
public class Request {

    private static final String REQUEST_SIGN = "?q=";
    protected static final String REQUEST_ADD = "&";


    private ArrayList<Request> query;

    public Request() {
        query = new ArrayList<Request>();            
    }
    
    public String get(){
        StringBuilder sb = new StringBuilder(REQUEST_SIGN);
        for (Iterator<Request> it = query.iterator(); it.hasNext();) {
            Request sR = it.next();
            sR.insertOpen(sb);
            Log.debug("add: "+sR.get());
            sb.append(sR.get());
            if(it.hasNext()) sb.append(REQUEST_ADD);
        }
        Log.debug("Generated request: "+sb.toString());
        return sb.toString();
    }

    public Request add(Request r){               
        query.add(r);
        return this;
    }

    public List<Request> loadRequestsOfType(Class clazz){
        List<Request> reqs = new ArrayList<Request>();
        for (Request request : query) {
            if(request.getClass().equals(clazz)){
                reqs.add(request);
            }
        }
        return reqs;
    }

    public int deleteRequest(Request r){
        int index = -1;
        for (int i = 0; i < query.size(); i++) {
            if(query.get(i).equals(r)){
                index = i;
                break;
            }
        }
        if(index >= 0){
            query.remove(index);
            return 1;
        }
        return 0;
    }

    /*
     * 
     */
    protected void insertOpen(StringBuilder url){
        
    }


}
