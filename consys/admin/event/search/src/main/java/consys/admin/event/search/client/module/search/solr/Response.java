/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.xml.client.CharacterData;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.Text;
import com.google.gwt.xml.client.XMLParser;
import consys.admin.event.search.client.module.search.ResponseCallback;
import consys.admin.event.search.client.module.search.ResponseDocument;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Palo
 */
public abstract class Response<T extends ResponseDocument> {

    private static final short STATUS_NOT_DEF = -1;
    private static final short STATUS_OK = 0;
    // TYPY
    protected static final String TYPE_LIST = "lst";
    protected static final String TYPE_ARRAY = "arr";
    protected static final String TYPE_INT = "int";
    protected static final String TYPE_STRING = "str";
    protected static final String TYPE_DOC = "doc";
    protected static final String TYPE_RESULT = "result";
    // MENA PROPERTIES PRE TAG NAME
    protected static final String PROP_RESPONSE_HEADER = "responseHeader";
    protected static final String PROP_STATUS = "status";
    protected static final String PROP_FACET_COUNT = "facet_counts";
    protected static final String PROP_FACET_FIELDS = "facet_fields";
    protected static final String PROP_RESPONSE = "response";
    //ATRIBUTY
    protected static final String ATR_NAME = "name";
    protected static final String ATR_RESULTS_NUM = "numFound";
    private Document doc;
    private List<T> documents;
    private ResponseCallback<T> responseCallback;

    public Response(ResponseCallback<T> responseCallback) {
        this.responseCallback = responseCallback;
        documents = new ArrayList<T>();
    }

    public void processError() {
        responseCallback.searchingError();
    }

    protected String getTextFromSolrArrayNode(Node node) {
        StringBuilder builder = new StringBuilder();
        if (node.getNodeName().equalsIgnoreCase(TYPE_ARRAY)) {
            NodeList list = node.getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                Node str = list.item(i);
                builder.append(getTextNodeValue(str.getFirstChild()) + " ");
            }
        }
        return builder.toString().trim();
    }

    /**
     * Vrati text ktory je v uzlu
     * <code>
     * <a>
     *  Text
     * </a>
     * </code>
     */
    protected String getTextNodeValueFromChild(Node node) {
        return getTextNodeValue(node.getFirstChild());
    }

    /**
     * Vrati text ktory uzol reprezentuje
     * <code>
     * Text
     * </code>
     */
    protected String getTextNodeValue(Node node) {
        if (node.getNodeType() == node.TEXT_NODE) {
            return ((Text) node).getData();
        } else if (node.getNodeType() == node.CDATA_SECTION_NODE) {
            return ((CharacterData) node).getData();
        }
        throw new IllegalArgumentException("Bad usage ~ " + node.getNodeName());
    }

    /**
     * Vrati text atributu elementu podla mena:
     * <code>
     * < attributeName="text" ..
     * </code>
     */
    protected String getTextAttribute(Node node, String attributeName) {
        return isElement(node) ? ((Element) node).getAttribute(attributeName) : null;
    }

    /**
     * Vrati priznak ci sa obsah atributu name rovna vlozenemu nameValue
     * <code>
     * < attributeName="text" ..
     * </code>
     */
    protected Boolean isNameAttributeEqual(Node node, String nameValue) {
        return getTextNameAttribute(node).equalsIgnoreCase(nameValue);
    }

    /**
     * Vrati text atributu name elementu:
     * <code>
     * < name="text" ..
     * </code>
     */
    protected String getTextNameAttribute(Node node) {
        return isElement(node) ? ((Element) node).getAttribute(ATR_NAME) : null;
    }

    /**
     * Vrati true ak je to element
     */
    protected boolean isElement(Node node) {
        return node.getNodeType() == node.ELEMENT_NODE;
    }

    /**
     * Vrati obsah uzlu ktory je identifikovany podla obsahu atributu name
     * v liste uzlov.
     * <code>
     * <xx>
     *   <a name="XXXX">returns</a>
     * </xx>
     * </code>
     */
    private String getTextValueFromList(NodeList list, String nameValue) {
        for (int j = 0; j < list.getLength(); j++) {
            Node child = list.item(j);
            if (isElement(child) && ((Element) child).getAttribute(ATR_NAME).equalsIgnoreCase(nameValue)) {
                return getTextNodeValue(child.getFirstChild());
            }
        }
        throw new NullPointerException("No node with name=\"" + nameValue + "\" in list");
    }

    public void doParse(String response) {
        try {
            short status = STATUS_NOT_DEF;            

            Log.debug("REPONSE:" + response);
            doc = XMLParser.parse(response);
            Element responseElement = doc.getDocumentElement();
            NodeList childs = responseElement.getChildNodes(); // 2 childy lst result
            for (int i = 0; i < childs.getLength(); i++) {
                Node node = childs.item(i);
                Log.debug("Processing type:" + node.getNodeName() + " name:" + node.getNodeName());
                // HLAVICKA
                if (node.getNodeName().equalsIgnoreCase(TYPE_LIST) && ((Element) node).getAttribute(ATR_NAME).equalsIgnoreCase(PROP_RESPONSE_HEADER)) {
                    status = Short.decode(getTextValueFromList(node.getChildNodes(), PROP_STATUS));
                    Log.debug("Search Status: " + status);
                    if (status != STATUS_OK) {
                        throw new IllegalStateException("Status" + status + " is NOT OK");
                    }


                } else {
                    if (isSearchedNode(node)) {
                       documents.addAll(parseNodeDocument(node));
                    }

                }

                /*if (node.getNodeName().equalsIgnoreCase(PROP_FACET_COUNT)) {

                documents = new ArrayList<T>(numOfDocs);
                Log.debug("FACET FOUND");
                NodeList list = node.getChildNodes();
                // Prvy je facet_queries


                // Druhy je facet_fields

                if (isNameAttributeEqual(list.item(1), PROP_FACET_FIELDS)) {
                NodeList facetList = (NodeList) list.item(1);
                numOfDocs = facetList.getLength();
                for (int j = 0; j < numOfDocs;) {
                Node child = facetList.item(j);
                Log.debug("Facet: "+getTextNameAttribute(child));
                if (child.getNodeName().equalsIgnoreCase(TYPE_ARRAY)) {
                getDocuments().add(parseDocument(child.getChildNodes()));
                j++;
                }
                }

                } else {
                throw new IllegalArgumentException("Second Atribute is not facet_fields!");
                }

                if (getDocuments().size() != numOfDocs) {
                throw new IllegalStateException("Expecting " + numOfDocs + " documents but readed " + getDocuments().size());
                }



                } else if (node.getNodeName().equalsIgnoreCase(TYPE_RESULT)) {

                numOfDocs = Integer.parseInt(getTextAttribute(node, ATR_RESULTS_NUM));
                documents = new ArrayList<T>(numOfDocs);
                Log.debug("Search hits: " + numOfDocs);
                NodeList list = node.getChildNodes();
                for (int j = 0; j < numOfDocs;) {
                Node child = list.item(j);
                if (child.getNodeName().equalsIgnoreCase(TYPE_DOC)) {
                getDocuments().add(parseDocument(child.getChildNodes()));
                j++;
                }
                }
                if (getDocuments().size() != numOfDocs) {
                throw new IllegalStateException("Expecting " + numOfDocs + " documents but readed " + getDocuments().size());
                }*/

            }
            responseCallback.searchingResults(documents);
        } catch (Exception e) {
            Log.error(e.toString());
            responseCallback.searchingError();
        }
    }

    public abstract boolean isSearchedNode(Node doc);

    public abstract List<T> parseNodeDocument(Node doc);

    /**
     * @return the documents
     */
    public List<T> getDocuments() {
        return documents;
    }
}
