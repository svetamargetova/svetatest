/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search.solr.request;

import consys.admin.event.search.client.module.search.solr.Request;



/**
 *
 * @author Palo
 */
public class RequestPage extends Request{

    private static final int START = 0;
    private static final int ROWS = 25;

    private int start = START;
    private int rows = ROWS;

    public RequestPage(int start, int rows) {
        this.start = start;
        this.rows = rows;
    }

    @Override
    public String get() {
        return "start="+getStart()+"&rows="+getRows();
    }

    /**
     * @return the start
     */
    public int getStart() {
        return start;
    }

    /**
     * @return the rows
     */
    public int getRows() {
        return rows;
    }

    /**
     * @param start the start to set
     */
    public void setStart(int start) {
        this.start = start;
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(int rows) {
        this.rows = rows;
    }







}
