/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search;


import java.util.List;

/**
 *
 * @author Palo
 */
public interface ResponseCallback<T extends ResponseDocument> {

    public void searchingResults(List<T> documents);
    public void searchingError();

}
