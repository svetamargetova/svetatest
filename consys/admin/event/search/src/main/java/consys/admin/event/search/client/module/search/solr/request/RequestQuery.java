/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr.request;

import consys.admin.event.search.client.module.search.solr.Request;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Palo
 */
public class RequestQuery extends Request {

    private Map<String, String> queries;

    public RequestQuery(String field, String value) {
        if (field == null || field.isEmpty() || value == null || value.isEmpty()) {
            throw new IllegalArgumentException("Field and value arguments are empty!");
        }

        queries = new HashMap<String, String>();
        queries.put(field, value);

    }

    @Override
    public String get() {
        StringBuilder sb = new StringBuilder();

        for (Iterator<Map.Entry<String, String>> it = queries.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String, String> r = it.next();
            sb.append(r.getKey() + ":\"" + r.getValue()+"\"");
            if (it.hasNext()) {
                sb.append(" AND ");
            }
        }
        return sb.toString();
    }

    public void addQuery(String field, String value) {
        queries.put(field, value);
    }
}
