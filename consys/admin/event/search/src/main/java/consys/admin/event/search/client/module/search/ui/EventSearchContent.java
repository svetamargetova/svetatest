package consys.admin.event.search.client.module.search.ui;

import com.allen_sauer.gwt.log.client.Log;
import consys.admin.event.search.client.module.search.*;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import consys.admin.event.search.client.module.search.bo.FacetDocument;
import consys.admin.event.search.client.module.search.solr.Request;
import consys.admin.event.search.client.module.search.solr.Search;
import consys.admin.event.search.client.module.search.solr.parser.FacetResponseParser;
import consys.admin.event.search.client.module.search.solr.request.RequestFacet;
import consys.admin.event.search.client.module.search.solr.request.RequestPage;
import consys.admin.event.search.client.module.search.solr.request.RequestQuery;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBaseWrapper;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Korenovy formular vyhledavani
 * @author Palo
 */
public class EventSearchContent extends SimpleFormPanel implements SolrConfigSchema {

    // instance
    private static EventSearchContent instance;
    // komponenty
    private TextBox tags;
    private VerticalPanel resultPanel;
    // model
    private Search search;

    List<SearchScenario> scenario;

    private EventSearchContent() {
        super();
        /* INIT SCENARIO */
        scenario = new ArrayList<SearchScenario>();
        scenario.add(new SearchScenario(SUBMISSION_TAGS_S, AUTHORS));
        scenario.add(new SearchScenario(AUTHORS_S, EVENT_YEAR));
        scenario.add(new SearchScenario(EVENT_YEAR, EVENT_NAME));
        scenario.add(new SearchScenario(EVENT_NAME, null));


        /* END INIT SCENARIO */
        search = new Search();

        HorizontalPanel searchPanel = new HorizontalPanel();

        // Pridame label
        Label title = new Label("Submission keywords:");
        title.setStyleName(StyleUtils.MARGIN_RIGHT_10);
        title.addStyleName(StyleUtils.MARGIN_BOT_5);
        title.addStyleName(ResourceUtils.system().css().formTitleTop());
        searchPanel.add(title);

        // Pridame text box
        tags = StyleUtils.getFormInput();
        tags.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        searchPanel.add(tags);


        // Pridame tlacitko
        ActionImage button = ActionImage.getConfirmButton("SEARCH");
        button.addClickHandler(searchClickHandler());
        searchPanel.add(button);

        // Nastujeme trosku
        searchPanel.setStyleName(StyleUtils.MARGIN_HOR_10);
        searchPanel.addStyleName(StyleUtils.MARGIN_VER_10);
        
        setWidget(new ConsysBaseWrapper(searchPanel));

        // Prisupneme separatorek
        Separator s = new Separator();
        s.addStyleName(StyleUtils.MARGIN_BOT_10);
        s.addStyleName(StyleUtils.MARGIN_TOP_10);
        addWidget(s);
        resultPanel = new VerticalPanel();
        addWidget(resultPanel);
    }

    /** ClickHandler pro spusteni hledani */
    private ClickHandler searchClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resultPanel.clear();
                final Request req = new Request();
                SearchScenario s = SearchScenario.getDept(0, scenario);
                req.add(new RequestQuery(s.getSearchField(), tags.getText())).
                        add(new RequestPage(0, 0)).
                        add(new RequestFacet(RequestFacet.LIMIT, "-1")).
                        add(new RequestFacet(RequestFacet.FIELD, s.getFacetField())).
                        add(new RequestFacet(RequestFacet.MIN_COUNT, "1"));


                ResponseCallback callback = new ResponseCallback<FacetDocument>() {

                    @Override
                    public void searchingError() {
                        Log.error("Error!!!!");
                    }

                    @Override
                    public void searchingResults(List<FacetDocument> documents) {

                        
                        for (FacetDocument.FacetField facetDocument : documents.get(0).getFields()) {                            
                            FacetSearchResult facet = new FacetSearchResult(facetDocument,scenario,1, req);
                            facet.addStyleName(StyleUtils.MARGIN_BOT_5);
                            resultPanel.add(facet);
                        }
                    }
                };
                search.search(req, new FacetResponseParser(callback));
            }
        };
    }    

    

    /** vytvori asynchronne instanci */
    public static void getAsync(final AsyncModule<EventSearchContent> asyncModule) {
        GWT.runAsync(new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable reason) {
                asyncModule.onFail();
            }

            @Override
            public void onSuccess() {
                if (instance == null) {
                    instance = new EventSearchContent();
                }
                asyncModule.onSuccess(instance);
            }
        });
    }


    

}
