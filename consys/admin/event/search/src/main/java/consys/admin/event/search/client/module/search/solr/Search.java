/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr;

import com.allen_sauer.gwt.log.client.Log;


import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;

import com.google.gwt.http.client.URL;

/**
 *
 * @author Palo
 */
public class Search {

    public void search(final Request request, final Response response) {


        final String req = getUrl(request);
        Log.debug("Request: " + req);

        final RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(req));
        builder.setCallback(new RequestCallback() {

            @Override
            public void onResponseReceived(com.google.gwt.http.client.Request req, com.google.gwt.http.client.Response res) {
                Log.debug("Status:" + res.getStatusText());
                Log.debug("Header:" + res.getHeadersAsString());
                Log.debug(res.getText());
                response.doParse(res.getText());
            }

            @Override
            public void onError(com.google.gwt.http.client.Request req, Throwable exception) {
                response.processError();
            }
        });
        try {
            builder.send();
        } catch (Exception ex) {
            Log.error("Exception:" + ex.getMessage());
        }
    }

    private String getUrl(Request request) {
        int mStart = GWT.getHostPageBaseURL().indexOf(GWT.getModuleName());
        StringBuilder url = new StringBuilder(GWT.getHostPageBaseURL().substring(0, mStart));
        url.append("search");
        url.append(request.get());
        return url.toString();
    }
}
