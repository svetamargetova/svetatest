/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr.parser;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import consys.admin.event.search.client.module.search.ResponseCallback;
import consys.admin.event.search.client.module.search.SolrConfigSchema;
import consys.admin.event.search.client.module.search.bo.SubmissionDocument;
import consys.admin.event.search.client.module.search.solr.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Palo
 */
public class SubmissionResponseParser extends Response<SubmissionDocument> implements SolrConfigSchema {

    public SubmissionResponseParser(ResponseCallback<SubmissionDocument> callback) {
        super(callback);
    }

    public SubmissionDocument parseDocument(NodeList doc) {
        SubmissionDocument searchedUser = new SubmissionDocument();
        Log.debug("Parse document");
        /*
        for (int i = 0; i < doc.getLength(); i++) {
        Node node = doc.item(i);
        if(isNameAttributeEqual(node, CONTACT_IMG)){
        searchedUser.setContactImgUuid(getTextNodeValueFromChild(node));
        } else if(isNameAttributeEqual(node, DEF_ORGANIZATION)){
        searchedUser.setDefaultOrganization(getTextFromSolrArrayNode(node));
        } else if(isNameAttributeEqual(node, DEF_POSITION)){
        searchedUser.setDefaultPosition(getTextFromSolrArrayNode(node));
        } else if(isNameAttributeEqual(node, EMAIL)){
        searchedUser.setEmail(getTextFromSolrArrayNode(node));
        } else if(isNameAttributeEqual(node, FULL_NAME)){
        searchedUser.setName(getTextFromSolrArrayNode(node));
        } else if(isNameAttributeEqual(node, UUID)){
        searchedUser.setUuid(getTextNodeValueFromChild(node));
        }
        }
         * */
        return searchedUser;
    }

    @Override
    public boolean isSearchedNode(Node doc) {
        if (isNameAttributeEqual(doc, PROP_RESPONSE)) {
            return true;
        }
        return false;
    }

    @Override
    public List<SubmissionDocument> parseNodeDocument(Node doc) {
        List<SubmissionDocument> docs = new ArrayList<SubmissionDocument>();

        NodeList nl = doc.getChildNodes();
        for (int i = 0; i < nl.getLength(); i++) {
            Log.debug("Processing document");
            SubmissionDocument sub = new SubmissionDocument();

            NodeList props = nl.item(i).getChildNodes();

            for (int j = 0; j < props.getLength(); j++) {
                Node node = props.item(j);
                
                if (isNameAttributeEqual(node, SUBMISSION_TITLE)) {
                    sub.setSubmissonTitle(getTextFromSolrArrayNode(node));
                }else if (isNameAttributeEqual(node, SUBMISSION_ABSTRACT)) {
                    sub.setSubmissonAbstract(getTextFromSolrArrayNode(node));
                }else if (isNameAttributeEqual(node, SUBMISSION_SECTION)) {
                    sub.setSubmissonSection(getTextFromSolrArrayNode(node));
                }else if (isNameAttributeEqual(node, SUBMISSION_TAGS)) {
                    sub.setSubmissonKeywords(getTextFromSolrArrayNode(node));
                }else if (isNameAttributeEqual(node, SUBMISSION_TEXT)) {
                    sub.setSubmissonText(getTextFromSolrArrayNode(node));
                }else if (isNameAttributeEqual(node, SUBMISSION_TYPE)) {
                    sub.setSubmissonType(getTextNodeValueFromChild(node));
                }else if (isNameAttributeEqual(node, UUID)) {
                    sub.setSubmissonUuid(getTextNodeValueFromChild(node));
                }


            }
            docs.add(sub);
        }
        return docs;
    }
}
