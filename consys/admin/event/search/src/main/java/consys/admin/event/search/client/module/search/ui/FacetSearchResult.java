/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.ui;

import com.allen_sauer.gwt.log.client.Log;
import consys.admin.event.search.client.module.search.ResponseCallback;
import consys.admin.event.search.client.module.search.SearchScenario;
import consys.admin.event.search.client.module.search.bo.FacetDocument;
import consys.admin.event.search.client.module.search.solr.Request;
import consys.admin.event.search.client.module.search.solr.parser.FacetResponseParser;
import consys.admin.event.search.client.module.search.solr.request.RequestFacet;
import consys.admin.event.search.client.module.search.solr.request.RequestQuery;
import consys.common.gwt.client.ui.comp.panel.RollOutPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.List;

/**
 *
 * @author Palo
 */
public class FacetSearchResult extends SearchResult {

    private FacetDocument.FacetField current;
    private List<SearchScenario> scenario;

    public FacetSearchResult(FacetDocument.FacetField field, List<SearchScenario> scenario, int depth, Request request) {
        super(field.getField() + " (" + field.getCount() + ")", request, depth);
        this.current = field;
        this.scenario = scenario;



    }

    @Override
    public void search() {

        SearchScenario s = SearchScenario.getDept(getDepth(), scenario);

        // najde query a upravi ju tak ze vybranu hodnotu vlozi ako query
        RequestQuery rq = (RequestQuery) getRequest().loadRequestsOfType(RequestQuery.class).get(0);
        rq.addQuery(s.getSearchField(), current.getField());


        // najde facet requesty a vyhodi field request
        List<Request> rfs = getRequest().loadRequestsOfType(RequestFacet.class);

        for (Request request : rfs) {
            RequestFacet f = (RequestFacet) request;
            if (f.getType() == RequestFacet.FIELD) {
                f.setField(s.getFacetField());
                break;
            }
        }


        ResponseCallback callback = new ResponseCallback<FacetDocument>() {

            @Override
            public void searchingError() {
                Log.error("Error!!!!");
            }

            @Override
            public void searchingResults(List<FacetDocument> documents) {
                getContent().showWaiting(false);
                SearchScenario s = SearchScenario.getDept(getDepth() + 1, scenario);
                if (s.getFacetField() == null) {
                    for (FacetDocument.FacetField facetDocument : documents.get(0).getFields()) {
                        SubmissionSearchResult facet = new SubmissionSearchResult(facetDocument, scenario, getDepth() + 1, getRequest());
                        facet.addStyleName(StyleUtils.MARGIN_BOT_5);
                        facet.addStyleName(StyleUtils.MARGIN_TOP_5);
                        getContent().addWidget(facet);
                    }
                    
                } else {
                    for (FacetDocument.FacetField facetDocument : documents.get(0).getFields()) {
                        FacetSearchResult facet = new FacetSearchResult(facetDocument, scenario, getDepth() + 1, getRequest());
                        facet.addStyleName(StyleUtils.MARGIN_BOT_5);
                        facet.addStyleName(StyleUtils.MARGIN_TOP_5);
                        getContent().addWidget(facet);
                    }
                }
            }
        };



        getSearch().search(getRequest(), new FacetResponseParser(callback));



    }
}
