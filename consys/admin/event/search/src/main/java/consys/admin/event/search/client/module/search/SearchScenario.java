/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search;

import java.util.List;

/**
 *
 * @author Palo
 */
public class SearchScenario {


    public static SearchScenario getDept(int d, List<SearchScenario> l){
        return l.get(d);
    }

    private String searchField;
    private String facetField;

    public SearchScenario(String searchField, String facetField) {
        this.searchField = searchField;
        this.facetField = facetField;
    }


    


    /**
     * @return the facetField
     */
    public String getFacetField() {
        return facetField;
    }

    /**
     * @return the searchField
     */
    public String getSearchField() {
        return searchField;
    }

}
