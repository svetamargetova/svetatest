/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr.request;

import consys.admin.event.search.client.module.search.solr.Request;

/**
 *
 * @author Palo
 */
public class RequestFacet extends Request {

    private static final String FACET = "facet";
    private static final String FACET_FIELD = "facet.field=";
    private static final String FACET_LIMIT = "facet.limit=";
    private static final String FACET_MIN = "facet.mincount=";
    private static final String FACET_OPEN = "facet=true";
    public static final int FIELD = 0;
    public static final int LIMIT = 1;
    public static final int MIN_COUNT = 2;
    private String field;
    private int facet;

    public RequestFacet(int type, String field) {
        this.field = field;
        this.facet = type;
    }

    @Override
    public String get() {
        switch (facet) {
            case FIELD:
                return FACET_FIELD + getField();
            case LIMIT:
                return FACET_LIMIT + getField();
            case MIN_COUNT:
                return FACET_MIN + getField();
        }
        throw new IllegalArgumentException("Unknown FACET type!");
    }

    public int getType() {
        return facet;
    }

    @Override
    protected void insertOpen(StringBuilder url) {
        if (url.indexOf(FACET) == -1) {
            url.append(FACET_OPEN + REQUEST_ADD);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (this.getClass().equals(obj.getClass())) {
            return true;
        }

        RequestFacet r = (RequestFacet) obj;
        if (r.getType() == this.getType() && r.getField().equalsIgnoreCase(getField())) {
            return true;
        }

        return false;

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.getField() != null ? this.getField().hashCode() : 0);
        hash = 59 * hash + this.facet;
        return hash;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @param field the field to set
     */
    public void setField(String field) {
        this.field = field;
    }

}
