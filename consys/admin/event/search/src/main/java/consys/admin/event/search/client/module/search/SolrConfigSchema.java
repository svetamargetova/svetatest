/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search;

/**
 *
 * @author Palo
 */
public interface SolrConfigSchema {
    
        
    public static final String UUID = "uuid";
    public static final String SCORE = "score";
    public static final String EVENT_NAME = "event_name";
    public static final String EVENT_NAME_S = "event_name_search";
    public static final String EVENT_YEAR = "event_year";
    public static final String AUTHORS = "submission_authors";    
    public static final String AUTHORS_S = "submission_authors_search";
    public static final String SUBMISSION_TAGS = "submission_tags";
    public static final String SUBMISSION_TAGS_S = "submission_tags_search";
    public static final String SUBMISSION_TITLE = "submission_title";
    public static final String SUBMISSION_TITLE_S = "submission_title_search";
    public static final String SUBMISSION_TYPE = "submission_type";
    public static final String SUBMISSION_TYPE_S = "submission_type_search";
    public static final String SUBMISSION_SECTION = "submission_section";
    public static final String SUBMISSION_ABSTRACT = "submission_abstract";
    public static final String SUBMISSION_TEXT= "submission_text";

}
