/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search.solr.request;

import consys.admin.event.search.client.module.search.solr.Request;

/**
 *
 * @author Palo
 */
public class RequestSort extends Request{

    /**
     * @return the order
     */
    public ORDER getOrder() {
        return order;
    }

    public enum ORDER{
        ASCENDING(" asc"),
        DESCENDING(" desc");

        private String order;

        private ORDER(String order) {
            this.order = order;
        }

        /**
         * @return the order
         */
        public String getOrder() {
            return order;
        }
    };

    private ORDER order;
    private String field;

    public RequestSort(ORDER order, String field) {
        this.order = order;
        this.field = field;
    }

    @Override
    public String get() {
        return "sort=" + field + order.getOrder();
    }
}
