package consys.admin.event.search.client;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import consys.admin.event.search.client.module.EventSearchModule;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.module.ModuleRegistrator;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.layout.menu.MenuDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;

/**
 * Vstupny Bod GWT pre testovanie modulu
 * @author Palo
 */
public class AdminEventSearchEntryPoint implements EntryPoint {

    @Override
    public void onModuleLoad() {
        Log.setUncaughtExceptionHandler();
        Log.debug("AdminEventSearchEntryPoint: start");

        LayoutManager layoutManager = LayoutManager.get();
        RootPanel.get().add(layoutManager);

        ModuleRegistrator.get().register(new EventSearchModule(), 0);

        // nastavi cache, ze je neprihlaseny uzivatel
        Cache.get().register(Cache.LOGGED, Boolean.TRUE);
        // zinicializovani MenuDispatcheru a vylozeni menu
        MenuDispatcher.get().generateMenu();
        // zinicializovani PanelDispatcheru a vlozeni panelu
        PanelDispatcher.get().generatePanel();
    }
}
