/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search.bo;

import consys.admin.event.search.client.module.search.ResponseDocument;

/**
 *
 * @author Palo
 */
public class SubmissionDocument implements ResponseDocument{


    private String eventName;
    private String eventYear;
    
    private String submissonUuid;
    private String submissonTitle;
    private String submissonAbstract;
    private String submissonText;
    private String submissonAuthors;
    private String submissonKeywords;
    private String submissonType;
    private String submissonSection;

    /**
     * @return the eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * @param eventName the eventName to set
     */
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    /**
     * @return the eventYear
     */
    public String getEventYear() {
        return eventYear;
    }

    /**
     * @param eventYear the eventYear to set
     */
    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    /**
     * @return the submissonUuid
     */
    public String getSubmissonUuid() {
        return submissonUuid;
    }

    /**
     * @param submissonUuid the submissonUuid to set
     */
    public void setSubmissonUuid(String submissonUuid) {
        this.submissonUuid = submissonUuid;
    }

    /**
     * @return the submissonTitle
     */
    public String getSubmissonTitle() {
        return submissonTitle;
    }

    /**
     * @param submissonTitle the submissonTitle to set
     */
    public void setSubmissonTitle(String submissonTitle) {
        this.submissonTitle = submissonTitle;
    }

    /**
     * @return the submissonAbstract
     */
    public String getSubmissonAbstract() {
        return submissonAbstract;
    }

    /**
     * @param submissonAbstract the submissonAbstract to set
     */
    public void setSubmissonAbstract(String submissonAbstract) {
        this.submissonAbstract = submissonAbstract;
    }

    /**
     * @return the submissonText
     */
    public String getSubmissonText() {
        return submissonText;
    }

    /**
     * @param submissonText the submissonText to set
     */
    public void setSubmissonText(String submissonText) {
        this.submissonText = submissonText;
    }

    /**
     * @return the submissonAuthors
     */
    public String getSubmissonAuthors() {
        return submissonAuthors;
    }

    /**
     * @param submissonAuthors the submissonAuthors to set
     */
    public void setSubmissonAuthors(String submissonAuthors) {
        this.submissonAuthors = submissonAuthors;
    }

    /**
     * @return the submissonKeywords
     */
    public String getSubmissonKeywords() {
        return submissonKeywords;
    }

    /**
     * @param submissonKeywords the submissonKeywords to set
     */
    public void setSubmissonKeywords(String submissonKeywords) {
        this.submissonKeywords = submissonKeywords;
    }

    /**
     * @return the submissonType
     */
    public String getSubmissonType() {
        return submissonType;
    }

    /**
     * @param submissonType the submissonType to set
     */
    public void setSubmissonType(String submissonType) {
        this.submissonType = submissonType;
    }

    /**
     * @return the submissonSection
     */
    public String getSubmissonSection() {
        return submissonSection;
    }

    /**
     * @param submissonSection the submissonSection to set
     */
    public void setSubmissonSection(String submissonSection) {
        this.submissonSection = submissonSection;
    }




}
