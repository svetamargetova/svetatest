package consys.admin.event.search.client.module;

import com.google.gwt.user.client.ui.Widget;
import consys.admin.event.search.client.module.search.ui.EventSearchContent;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.ChangeContentEvent;
import consys.common.gwt.client.module.AsyncContent;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.module.Module;

/**
 * Modul pro vyhledavani v eventech
 * @author pepa
 */
public class EventSearchModule implements Module {

    private AsyncContent content;

    @Override
    public Widget getRootWidget() {
        content = new AsyncContent() {

            @Override
            public void runComponent() {
                EventSearchContent.getAsync(new AsyncModule<EventSearchContent>() {

                    @Override
                    public void onSuccess(EventSearchContent instance) {
                        ChangeContentEvent event = new ChangeContentEvent(instance);
                        EventBus.get().fireEvent(event);
                    }

                    @Override
                    public void onFail() {
                        // TODO: fail
                    }
                });
            }
        };
        return content;
    }

    @Override
    public String getTitle() {
        return "Search";
    }

    @Override
    public String getToken() {
        return "Search";
    }
}
