/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.ui;

import com.allen_sauer.gwt.log.client.Log;
import consys.admin.event.search.client.module.search.ResponseCallback;
import consys.admin.event.search.client.module.search.SearchScenario;
import consys.admin.event.search.client.module.search.bo.FacetDocument;
import consys.admin.event.search.client.module.search.bo.SubmissionDocument;
import consys.admin.event.search.client.module.search.solr.Request;
import consys.admin.event.search.client.module.search.solr.parser.FacetResponseParser;
import consys.admin.event.search.client.module.search.solr.parser.SubmissionResponseParser;
import consys.admin.event.search.client.module.search.solr.request.RequestFacet;
import consys.admin.event.search.client.module.search.solr.request.RequestPage;
import consys.admin.event.search.client.module.search.solr.request.RequestQuery;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Palo
 */
public class SubmissionSearchResult extends SearchResult {

    private FacetDocument.FacetField current;
    private List<SearchScenario> scenario;

    public SubmissionSearchResult(FacetDocument.FacetField field, List<SearchScenario> scenario, int depth, Request request) {
        super(field.getField() + " (" + field.getCount() + ")", request, depth);
        this.current = field;
        this.scenario = scenario;
    }

    @Override
    public void search() {
        SearchScenario s = SearchScenario.getDept(getDepth(), scenario);

        // najde query a upravi ju tak ze vybranu hodnotu vlozi ako query
        RequestQuery rq = (RequestQuery) getRequest().loadRequestsOfType(RequestQuery.class).get(0);
        rq.addQuery(s.getSearchField(), current.getField());

        RequestPage rp = (RequestPage) getRequest().loadRequestsOfType(RequestPage.class).get(0);
        rp.setRows(20);


        ResponseCallback callback = new ResponseCallback<SubmissionDocument>() {

            @Override
            public void searchingError() {
                Log.error("Error!!!!");
            }

            
            @Override
            public void searchingResults(List<SubmissionDocument> documents) {
                getContent().showWaiting(false);
                boolean even = false;
                for (Iterator<SubmissionDocument> it = documents.iterator(); it.hasNext();) {
                    SubmissionDocument submissionDocument = it.next();
                    ResultItem item = new ResultItem(even, submissionDocument);
                    getContent().addWidget(item);
                }
                
            }
        };



        getSearch().search(getRequest(), new SubmissionResponseParser(callback));
    }
}
