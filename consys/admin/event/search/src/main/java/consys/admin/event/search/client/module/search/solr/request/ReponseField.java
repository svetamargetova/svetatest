/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr.request;

import consys.admin.event.search.client.module.search.solr.Request;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Palo
 */
public class ReponseField extends Request {

    private ArrayList<String> fields;

    public ReponseField(ArrayList<String> fields) {
        if (fields == null || fields.isEmpty()) {
            throw new IllegalArgumentException("Empty response fields array!");
        }
        this.fields = fields;
    }

    @Override
    public String get() {
        StringBuilder sb = new StringBuilder("fl=");

        for (Iterator<String> it = fields.iterator(); it.hasNext();) {
            String string = it.next();
            sb.append(string);
            if (it.hasNext()) {
                sb.append(",");
            }
        }

        return sb.toString();
    }
}
