/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search.solr.parser;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import consys.admin.event.search.client.module.search.ResponseCallback;
import consys.admin.event.search.client.module.search.bo.FacetDocument;
import consys.admin.event.search.client.module.search.solr.Response;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Palo
 */
public class FacetResponseParser extends Response<FacetDocument>{

    public FacetResponseParser(ResponseCallback<FacetDocument> callback) {
        super(callback);
    }

    
    @Override
    public boolean isSearchedNode(Node doc) {        
        if(isNameAttributeEqual(doc, PROP_FACET_COUNT)){            
            return true;
        }        
        return false;
    }

    @Override
    public List<FacetDocument> parseNodeDocument(Node doc) {

        List<FacetDocument> docs = new ArrayList<FacetDocument>();
        Log.debug("Processing FACETS");
        // vybereme rocno facet_fields
        NodeList nl = doc.getChildNodes().item(1).getChildNodes();
        for(int i = 0 ; i < nl.getLength(); i++){
            NodeList facetResult = nl.item(i).getChildNodes();
            Log.debug("Facet: "+ getTextNameAttribute(nl.item(i)));
            // Nastavime facet podla ktoreho sa to robilo
            FacetDocument document = new FacetDocument(getTextNameAttribute(nl.item(i)));
            for(int j = 0; j < facetResult.getLength(); j++){
                Node node = facetResult.item(j);
                document.addField(getTextNameAttribute(node), getTextNodeValue(node.getFirstChild()));
            }
            docs.add(document);            
        }
        return docs;
    }

}
