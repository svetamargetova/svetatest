/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.server.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Palo
 */
public class SearchForwardServlet extends HttpServlet {

    private static final long serialVersionUID = -5384734935457748070L;
    private static final String SOLR_URL = "http://localhost:8983/solr/select?";

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) {
        {
            InputStream is = null;
            String line = null;
            BufferedReader br = null;



            Logger.getLogger(SearchForwardServlet.class.getName()).log(Level.INFO,"Query "+req.getQueryString());

            try {
                URL solr = new URL(SOLR_URL+req.getQueryString());

                is = solr.openStream();


                br = new BufferedReader(new InputStreamReader(is));

                while ((line = br.readLine()) != null) {
                    resp.getOutputStream().print(line);
                }

                
            } catch (MalformedURLException ex) {
                resp.setStatus(resp.SC_NOT_FOUND);
                Logger.getLogger(SearchForwardServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                resp.setStatus(resp.SC_NOT_FOUND);
                Logger.getLogger(SearchForwardServlet.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                    Logger.getLogger(SearchForwardServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }


    }
}
