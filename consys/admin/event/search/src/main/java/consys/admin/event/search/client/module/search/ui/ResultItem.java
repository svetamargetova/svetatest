package consys.admin.event.search.client.module.search.ui;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import consys.admin.event.search.client.module.search.bo.SubmissionDocument;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class ResultItem extends VerticalPanel {

    public ResultItem(boolean even, SubmissionDocument item) {
        super();
        setSize("100%", "50px");
        if (!even) {
            setStyleName(StyleUtils.BACKGROUND_LIGHT_GRAY);
        }

        FlexTable fx = new FlexTable();
        fx.setCellPadding(4);
        fx.setCellSpacing(4);
        fx.setWidget(0, 0, StyleUtils.getStyledLabel("ISBN:", StyleUtils.FONT_10PX,StyleUtils.FONT_BOLD,StyleUtils.TEXT_BLUE));
        fx.setWidget(1, 0, StyleUtils.getStyledLabel("Title:", StyleUtils.FONT_10PX,StyleUtils.FONT_BOLD,StyleUtils.TEXT_BLUE));
        fx.setWidget(2, 0, StyleUtils.getStyledLabel("Type:", StyleUtils.FONT_10PX,StyleUtils.FONT_BOLD,StyleUtils.TEXT_BLUE));
        fx.setWidget(3, 0, StyleUtils.getStyledLabel("Keywords:", StyleUtils.FONT_10PX,StyleUtils.FONT_BOLD,StyleUtils.TEXT_BLUE));
        fx.setWidget(4, 0, StyleUtils.getStyledLabel("Abstract:", StyleUtils.FONT_10PX,StyleUtils.FONT_BOLD,StyleUtils.TEXT_BLUE));
        fx.setWidget(5, 0, StyleUtils.getStyledLabel("Section:", StyleUtils.FONT_10PX,StyleUtils.FONT_BOLD,StyleUtils.TEXT_BLUE));
        fx.setWidget(6, 0, StyleUtils.getStyledLabel("Text:", StyleUtils.FONT_10PX,StyleUtils.FONT_BOLD,StyleUtils.TEXT_BLUE));

        fx.setWidget(0, 1, StyleUtils.getStyledLabel(item.getSubmissonUuid(), StyleUtils.FONT_10PX,StyleUtils.FONT_NORMAL,StyleUtils.TEXT_DEFAULT));
        fx.setWidget(1, 1, StyleUtils.getStyledLabel(item.getSubmissonTitle(), StyleUtils.FONT_10PX,StyleUtils.FONT_NORMAL,StyleUtils.TEXT_DEFAULT));
        fx.setWidget(2, 1, StyleUtils.getStyledLabel(item.getSubmissonType(), StyleUtils.FONT_10PX,StyleUtils.FONT_NORMAL,StyleUtils.TEXT_DEFAULT));
        fx.setWidget(3, 1, StyleUtils.getStyledLabel(item.getSubmissonKeywords(), StyleUtils.FONT_10PX,StyleUtils.FONT_NORMAL,StyleUtils.TEXT_DEFAULT));
        fx.setWidget(4, 1, StyleUtils.getStyledLabel(item.getSubmissonAbstract(), StyleUtils.FONT_10PX,StyleUtils.FONT_NORMAL,StyleUtils.TEXT_DEFAULT));
        fx.setWidget(5, 1, StyleUtils.getStyledLabel(item.getSubmissonSection(), StyleUtils.FONT_10PX,StyleUtils.FONT_NORMAL,StyleUtils.TEXT_DEFAULT));
        fx.setWidget(6, 1, StyleUtils.getStyledLabel(item.getSubmissonText(), StyleUtils.FONT_10PX,StyleUtils.FONT_NORMAL,StyleUtils.TEXT_DEFAULT));

        add(fx);
    }
}
