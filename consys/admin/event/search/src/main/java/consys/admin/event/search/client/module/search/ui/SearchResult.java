/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.search.client.module.search.ui;

import consys.admin.event.search.client.module.search.SolrConfigSchema;
import consys.admin.event.search.client.module.search.solr.Request;

import consys.admin.event.search.client.module.search.solr.Search;
import consys.common.gwt.client.ui.comp.panel.RollOutPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;

/**
 *
 * @author Palo
 */
public abstract class SearchResult extends RollOutPanel implements SolrConfigSchema{

    private Request request;
    private SimpleFormPanel content;
    private int depth;
    private Search search;


    public SearchResult(String title, Request request, int depth) {
        super(title);
        this.request = request;
        this.depth = depth;
        content = new SimpleFormPanel("100%");        
        this.setContent(content);
        search = new Search();
    }

    @Override
    protected void rollPanel() {
        super.rollPanel();
        if(isRolled()){
            getContent().showWaiting(true);
            search();
        }
    }
        

    public abstract void search();

    /**
     * @return the request
     */
    public Request getRequest() {
        return request;
    }

    /**
     * @return the content
     */
    public SimpleFormPanel getContent() {
        return content;
    }

    /**
     * @return the search
     */
    public Search getSearch() {
        return search;
    }

    /**
     * @return the depth
     */
    public int getDepth() {
        return depth;
    }
}
