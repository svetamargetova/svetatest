/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.search.client.module.search.solr.request;

import consys.admin.event.search.client.module.search.solr.Request;

/**
 *
 * @author Palo
 */
public class RequestFilter extends Request {

    private String field;
    private String value;

    public RequestFilter(String field, String value) {
        if (field == null || field.isEmpty() || value == null || value.isEmpty()) {
            throw new IllegalArgumentException("Field and value arguments are empty!");
        }

        this.field = field;
        this.value = value;
    }

    @Override
    public String get() {
        return "fq=" + field + ":" + value;
    }
}
