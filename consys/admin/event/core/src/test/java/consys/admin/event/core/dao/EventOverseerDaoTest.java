package consys.admin.event.core.dao;

import consys.admin.event.core.AbstractEventAdminTest;
import consys.admin.event.core.bo.EventOverseer;
import consys.common.core.exception.NoRecordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */
public class EventOverseerDaoTest extends AbstractEventAdminTest{

    private EventOverseerDao overseerDao;

    @Test(groups={"dao"})
    public void testLoadByMinEvents() throws NoRecordException{
        EventOverseer eo1 = new EventOverseer();
        eo1.setEvents(3);
        eo1.setName("o1.app.takeplace.eu");
        eo1.setClientSuffix("o1.app.takeplace.eu");
        eo1.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo1);

        eo1 = new EventOverseer();
        eo1.setEvents(2);
        eo1.setName("o2.app.takeplace.eu");
        eo1.setClientSuffix("o2.app.takeplace.eu");
        eo1.setWebServicesUrl("o2.app.takeplace.eu");
        overseerDao.create(eo1);

        eo1 = new EventOverseer();
        eo1.setEvents(1);
        eo1.setName("o3.app.takeplace.eu");
        eo1.setClientSuffix("o3.app.takeplace.eu");
        eo1.setWebServicesUrl("o3.app.takeplace.eu");
        overseerDao.create(eo1);

        eo1 = new EventOverseer();
        eo1.setEvents(2);
        eo1.setName("o4.app.takeplace.eu");
        eo1.setClientSuffix("o4.app.takeplace.eu");
        eo1.setWebServicesUrl("o4.app.takeplace.eu");
        overseerDao.create(eo1);

        EventOverseer el = overseerDao.loadWithMinEvents();
        assertNotNull(el);
        assertTrue(el.getEvents()==1);
    }


    @Test(groups={"dao"})
    public void testLoadByName() throws NoRecordException{
        EventOverseer eo1 = new EventOverseer();
        eo1.setEvents(3);
        eo1.setName("o1.app.takeplace.eu");
        eo1.setClientSuffix("o1.app.takeplace.eu");
        eo1.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo1);

        EventOverseer e = overseerDao.loadByName(eo1.getName());
        assertEquals(eo1, e);
    }

    /**
     * @param overseerDao the overseerDao to set
     */
    @Autowired(required=true)
    public void setOverseerDao(EventOverseerDao overseerDao) {
        this.overseerDao = overseerDao;
    }

}
