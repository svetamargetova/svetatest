/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.core.service;

import consys.admin.event.core.service.exception.InvitationAlreadyResolved;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.core.service.exception.InvitationException;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.common.core.exception.ServiceExecutionFailed;
import org.apache.commons.mail.EmailException;
import org.testng.annotations.Test;
import consys.admin.event.core.AbstractEventAdminTest;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.bo.EventType;
import consys.admin.event.core.bo.Invitation;
import consys.admin.event.core.bo.InvitationStateEnum;
import consys.admin.event.core.bo.thumb.EventInvitationThumb;
import consys.admin.event.core.bo.thumb.UserInvitationRequest;
import consys.admin.event.core.dao.EventDao;
import consys.admin.event.core.dao.EventOverseerDao;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.dao.UserActivationDao;
import consys.admin.user.core.dao.UserDao;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.util.UserUtils;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.NotX;
import consys.common.core.service.NotxService;
import consys.common.utils.UuidProvider;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author palo
 */
public class EventInvitationServiceTest extends AbstractEventAdminTest {

    private EventOverseerDao overseerDao;
    private EventDao eventDao;
    private UserDao userDao;
    private EventInvitationService eventInvitationService;
    private UserService userService;
    private UserActivationDao userActivationDao;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setUserActivationDao(UserActivationDao userActivationDao) {
        this.userActivationDao = userActivationDao;
    }

    @Autowired
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    @Autowired
    public void setEventInvitationService(EventInvitationService eventInvitationService) {
        this.eventInvitationService = eventInvitationService;
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Autowired
    public void setOverseerDao(EventOverseerDao overseerDao) {
        this.overseerDao = overseerDao;
    }

    @Test(groups = {"service"})
    public void testCreateForExistingUser() throws RequiredPropertyNullException, NoRecordException {
        NotX.setNotificationService(mock(NotxService.class));
        
        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e1 = new Event();
        e1.setType(EventType.CONFERENCE);
        e1.setAcronym("ABS");
        e1.setTimeZone("Europe/Prague");
        e1.setUuid(UuidProvider.getUuid());
        e1.setOverseer(eo);
        eventDao.create(e1);

        Event e2 = new Event();
        e2.setType(EventType.CONFERENCE);
        e2.setAcronym("ABS");
        e2.setTimeZone("Europe/Prague");
        e2.setUuid(UuidProvider.getUuid());
        e2.setOverseer(eo);
        eventDao.create(e2);

        User from = UserUtils.createUser();
        from.setFirstName("1");
        from.setLastName("2");
        from.setEmail("3");
        userDao.create(from);

        User to = UserUtils.createUser();
        to.setFirstName("4");
        to.setLastName("5");
        to.setEmail("6");
        userDao.create(to);

        UserInvitationRequest req1 = new UserInvitationRequest(from.getUuid(), to.getUuid(), e1.getUuid(), EventUserOrganizationRelation.VISITOR, null);
        req1.setWithInvitationConfirm(true);
        UserInvitationRequest req2 = new UserInvitationRequest(from.getUuid(), to.getUuid(), e2.getUuid(), EventUserOrganizationRelation.VISITOR, null);
        req2.setWithInvitationConfirm(true);

        eventInvitationService.createUserInvivation(req1);
        eventInvitationService.createUserInvivation(req2);

        List<Invitation> i1 = eventInvitationService.listUserActiveInvitations(to.getUuid());
        List<Invitation> i2 = eventInvitationService.listUserInvitations(to.getUuid());
        assertEquals(i1.size(), 2);
        assertEquals(i2.size(), 2);

        List<EventInvitationThumb> i3 = eventInvitationService.listUserActiveInvitations(to.getUuid(), 0, 10);
        List<EventInvitationThumb> i4 = eventInvitationService.listUserInvitations(to.getUuid(), 0, 10);
        assertEquals(i3.size(), 2);
        assertEquals(i4.size(), 2);
    }

    @Test(groups = {"service"})
    public void testCreateForNewUser() throws RequiredPropertyNullException, NoRecordException {
        NotX.setNotificationService(mock(NotxService.class));
        
        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e1 = new Event();
        e1.setType(EventType.CONFERENCE);
        e1.setAcronym("ABS");
        e1.setTimeZone("Europe/Prague");
        e1.setUuid(UuidProvider.getUuid());
        e1.setOverseer(eo);
        eventDao.create(e1);

        User from = UserUtils.createUser();
        from.setFirstName("1");
        from.setLastName("2");
        from.setEmail("3");
        userDao.create(from);

        User to = UserUtils.createUser();
        to.setFirstName("4");
        to.setLastName("5");
        to.setEmail("6");
        userDao.create(to);

        // TEST 1 - Vytvorenie len s jednym menom
        logger.info("------- TEST 1: vytvorenie s jednym menom");
        UserInvitationRequest req1 = new UserInvitationRequest(from.getUuid(), "email1@domain.com", "Palo", e1.getUuid(), EventUserOrganizationRelation.VISITOR, null);
        eventInvitationService.createUserInvivation(req1);

        // TEST 2 - vytvorenie s dvomi menami
        logger.info("------- TEST 2: vytvorenie s menom z dvoch slov");
        UserInvitationRequest req2 = new UserInvitationRequest(from.getUuid(), "email2@domain.com", "Palo Palo", e1.getUuid(), EventUserOrganizationRelation.VISITOR, null);
        eventInvitationService.createUserInvivation(req2);

        // TEST 3 - vytvorenie s viac menami
        logger.info("------- TEST 3: vytvorenie s menom z 3 slov");
        UserInvitationRequest req3 = new UserInvitationRequest(from.getUuid(), "email4@domain.com", "Palo Palo Palo Palo", e1.getUuid(), EventUserOrganizationRelation.VISITOR, null);
        eventInvitationService.createUserInvivation(req3);

        // TEST 4 - vytvorenie s existujucim emailom
        logger.info("------- TEST 4: vytvorenie s existujucim menom");
        UserInvitationRequest req4 = new UserInvitationRequest(from.getUuid(), to.getEmailOrMergeEmail(), to.getName(), e1.getUuid(), EventUserOrganizationRelation.VISITOR, null);
        eventInvitationService.createUserInvivation(req4);

        String uuid = userDao.loadUserUuidByUsername("email4@domain.com");
        User u = userDao.loadByUuid(uuid);
    }

    @Test(groups = {"service"})
    public void testResolveInvitation() throws RequiredPropertyNullException, NoRecordException, ServiceExecutionFailed, UsernameExistsException, EmailException, ActivationKeyException, InvitationAlreadyResolved, InvitationException {
        NotX.setNotificationService(mock(NotxService.class));

        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        // vytvorime event
        Event e1 = new Event();
        e1.setType(EventType.CONFERENCE);
        e1.setAcronym("ABS");
        e1.setUuid(UuidProvider.getUuid());
        e1.setTimeZone("Europe/Prague");
        e1.setOverseer(eo);
        eventDao.create(e1);

        Event e2 = new Event();
        e2.setType(EventType.CONFERENCE);
        e2.setAcronym("ABS");
        e2.setUuid(UuidProvider.getUuid());
        e2.setOverseer(eo);
        e2.setTimeZone("Europe/Prague");
        eventDao.create(e2);

        // od koho poslane
        User from = UserUtils.createUser();
        from.setFirstName("1");
        from.setLastName("2");
        from.setEmail("3");
        userDao.create(from);

        // vytorime plnohodnotneho uzivatela
        UserLoginInfo info = UserUtils.prepareNewUser("palo", "palo");
        info.getUser().setFirstName("Palo");
        info.getUser().setLastName("Palo");
        userService.createRegistredUser(info);
        UserActivation ua = userActivationDao.loadByUserLoginInfo(info);
        userService.activateUser(ua.getKey());




        UserInvitationRequest req1 = new UserInvitationRequest(from.getUuid(), info.getUser().getUuid(), e1.getUuid(), EventUserOrganizationRelation.REGISTRED, null);
        req1.setWithInvitationConfirm(true);
        eventInvitationService.createUserInvivation(req1);

        UserInvitationRequest req2 = new UserInvitationRequest(from.getUuid(), info.getUser().getUuid(), e2.getUuid(), EventUserOrganizationRelation.REGISTRED, null);
        req2.setWithInvitationConfirm(true);
        eventInvitationService.createUserInvivation(req2);

        List<Invitation> iu1 = eventInvitationService.listUserInvitations(info.getUser().getUuid());
        assertEquals(iu1.size(), 2);


        // TEST 1 - pre jeden event primeme pozvanku
        eventInvitationService.updateInvitation(InvitationStateEnum.ACCEPTED, iu1.get(0).getUuid(), info.getUser().getUuid());

        // TEST 2 - pre druhy event odmietneme pozvanku
        eventInvitationService.updateInvitation(InvitationStateEnum.DECLINED, iu1.get(1).getUuid(), info.getUser().getUuid());

        // TEST 3 - opakovane odmietnutie
        try {
            eventInvitationService.updateInvitation(InvitationStateEnum.DECLINED, iu1.get(1).getUuid(), info.getUser().getUuid());
            fail("Opakovane resolovanie chybne");
        } catch (InvitationAlreadyResolved e) {
        }

    }
}
