package consys.admin.event.core;

import consys.admin.common.test.AbstractAdminDaoTestCase;
import org.springframework.test.context.ContextConfiguration;

/**
 *
 * @author Palo
 */
@ContextConfiguration(locations = {"event-spring.xml", "classpath:consys/admin/common/core/admin-common-spring.xml","classpath:consys/admin/user/core/user-spring.xml"})
public abstract class AbstractEventAdminTest extends AbstractAdminDaoTestCase  {

}
