package consys.admin.event.core.service;

import consys.admin.event.core.AbstractEventAdminTest;
import consys.admin.user.core.UserSystemProperties;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.bo.SystemProperty;
import consys.common.core.dao.SystemPropertyDao;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.NotX;
import consys.common.core.service.NotxService;
import consys.common.utils.date.DateProvider;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventUserOrganizationTest extends AbstractEventAdminTest {

    private SystemPropertyDao systemPropertyDao;
    private UserLoginInfoService loginInfoService;
    private UserService userService;
    private EventUserOrganizationService eventUserOrganizationService;

    @Autowired(required = true)
    public void setEventUserOrganizationService(EventUserOrganizationService eventUserOrganizationService) {
        this.eventUserOrganizationService = eventUserOrganizationService;
    }

    @Autowired(required = true)
    public void setSystemPropertyDao(SystemPropertyDao systemPropertyDao) {
        this.systemPropertyDao = systemPropertyDao;
    }

    @Autowired(required = true)
    public void setLoginInfoService(UserLoginInfoService loginInfoService) {
        this.loginInfoService = loginInfoService;
    }

    @Autowired(required = true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Test(groups = {"service"})
    public void testGenerateActivationManagement() throws ServiceExecutionFailed, UsernameExistsException, NoRecordException, RequiredPropertyNullException {
        NotX.setNotificationService(mock(NotxService.class));

        // Ulozime si property
        SystemProperty sp = systemPropertyDao.loadByKey(UserSystemProperties.REP_ACT_EMAIL_DATE_INTERVAL);
        sp.setValue("4");
        systemPropertyDao.update(sp);
        sp = systemPropertyDao.loadByKey(UserSystemProperties.REP_ACT_EMAIL_MAX_DAYS);
        sp.setValue("35");
        systemPropertyDao.update(sp);

        // vytvorime jedneho OK usera
        UserLoginInfo uli = UserUtils.prepareNewUser("palo", "ABC");
        uli.getUser().setFirstName("a");
        uli.getUser().setLastName("a");
        userService.createRegistredUser(uli);

        // Vytvorime usera ktory je mimo intervalu
        uli = UserUtils.prepareNewUser("pepa", "ABC");
        uli.getUser().setFirstName("a");
        uli.getUser().setLastName("a");
        Calendar c = GregorianCalendar.getInstance();
        DateProvider.substractDays(c, 5);
        uli.setRegisterDate(c.getTime());
        userService.createRegistredUser(uli);

        // Vytvorime usera ktory je mimo max days
        uli = UserUtils.prepareNewUser("jura", "ABC");
        uli.getUser().setFirstName("a");
        uli.getUser().setLastName("a");
        c = GregorianCalendar.getInstance();
        DateProvider.substractDays(c, 37);
        uli.setRegisterDate(c.getTime());
        userService.createRegistredUser(uli);

        eventUserOrganizationService.generateActivationManagement();
        try {
            loginInfoService.loadUserLoginInfo("jura");
            fail();
        } catch (NoRecordException ex) {
            //ok
        }
    }
}
