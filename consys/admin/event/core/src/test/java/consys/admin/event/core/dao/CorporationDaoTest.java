package consys.admin.event.core.dao;

import consys.admin.event.core.AbstractEventAdminTest;
import consys.admin.event.core.bo.Corporation;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class CorporationDaoTest extends AbstractEventAdminTest {

    // konstanty
    private static final String PREFIX = "bmw";
    // servicy
    @Autowired
    private CorporationDao corporationDao;

    @Test(groups = {"dao"})
    public void testDao() {
        assertNotNull(corporationDao);
    }

    @Test(groups = {"dao"})
    public void testCheckEvent() {
        boolean found = corporationDao.checkRegisteredCorporation(PREFIX);
        assertEquals(found, false, "Naslo neexistujici korporaci");

        Corporation c = new Corporation();
        c.setName("Bayerische Motoren Werke");
        c.setUrlPrefix(PREFIX);

        corporationDao.create(c);

        found = corporationDao.checkRegisteredCorporation(PREFIX);
        assertEquals(found, true, "Nenaslo existujici korporaci");
    }

    @Test(groups = {"dao"})
    public void loadCorporation() {
        Corporation c = new Corporation();
        c.setName("Bayerische Motoren Werke");
        c.setUrlPrefix(PREFIX);

        corporationDao.create(c);

        Corporation c1 = corporationDao.loadByPrefix(PREFIX);
        if (c1 == null) {
            fail("Nenacetla se korporace podle prefixu");
        }

        assertEquals(c1.getName(), c.getName());
        assertEquals(c1.getId(), c.getId());
    }
}
