package consys.admin.event.core.service;

import consys.admin.common.test.service.mock.OverseerWebServiceMock;
import consys.admin.event.core.AbstractEventAdminTest;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.bo.EventType;
import consys.admin.event.core.bo.thumb.BundleOrderRequest;
import consys.admin.event.core.dao.EventDao;
import consys.admin.event.core.dao.EventOverseerDao;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.UuidProvider;
import consys.event.registration.ws.RegisterPaymentProfile;
import consys.event.registration.ws.RegisterUserIntoEventResponse;
import consys.payment.service.exception.IbanFormatException;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OuterRegistrationServiceTest extends AbstractEventAdminTest {

    private static final Logger logger = LoggerFactory.getLogger(OuterRegistrationServiceTest.class);
    @Autowired
    private UserService userService;
    @Autowired
    private EventService eventService;
    @Autowired
    private EventOverseerDao overseerDao;
    @Autowired
    private EventDao eventDao;
    @Autowired
    private OuterRegistrationService outerRegistrationService;
    @Autowired
    private OverseerWebServiceMock webServiceMock;

    @Test(groups = {"service"})
    public void testRegistredUser() throws NoRecordException, UsernameExistsException, ServiceExecutionFailed, RequiredPropertyNullException, BundleCapacityFullException, CouponCapacityException, IbanFormatException, CouponCodeException, CouponOutOfDateException {

        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e1 = new Event();
        e1.setType(EventType.CONFERENCE);
        e1.setAcronym("ABS");
        e1.setTimeZone("Europe/Prague");
        e1.setUuid(UuidProvider.getUuid());
        e1.setOverseer(eo);
        eventDao.create(e1);

        UserLoginInfo uli = UserUtils.prepareNewUser("email", "passs");
        uli.getUser().setFirstName("A");
        uli.getUser().setLastName("A");
        webServiceMock.clear();

        RegisterUserIntoEventResponse resp = new RegisterUserIntoEventResponse();
        resp.setResult(true);

        webServiceMock.addResponse(resp);

        RegisterPaymentProfile pp = new RegisterPaymentProfile();
        BundleOrderRequest m = new BundleOrderRequest("aaa");
        outerRegistrationService.createUserEventRegistration(uli, "aaa", null, pp, m, null, e1.getUuid(),
                new HashMap<String, String>());
    }
}
