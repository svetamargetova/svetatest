package consys.admin.event.core.dao;

import consys.admin.event.core.AbstractEventAdminTest;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.bo.EventType;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.bo.thumb.EventUserOrganizationThumb;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.dao.UserDao;
import consys.admin.user.core.dao.UserOrganizationDao;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.common.utils.UuidProvider;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */
public class EventUserOrganizationDaoTest extends AbstractEventAdminTest {

    // DAO
    private EventUserOrganizationDao euod;
    private EventDao eventDao;
    private EventOverseerDao overseerDao;
    private UserDao userDao;
    private UserOrganizationDao userOrganizationDao;
    // Test Data
    private EventUserOrganization testEUO;
    private User testUser;

    @Test(groups = {"dao"})
    public void testDao() {
        assertNotNull(euod);
        assertNotNull(eventDao);
        assertNotNull(overseerDao);
        assertNotNull(userDao);
        assertNotNull(userOrganizationDao);
    }

    @Test(groups = {"dao"})
    public void testCreateEUO() {
        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setOverseer(eo);
        e.setUuid(UuidProvider.getUuid());
        e.setAcronym("Abc");
        e.setTimeZone("Europe/Prague");
        eventDao.create(e);

        User u = UserUtils.createUser();
        u.setEmail("Palko");
        u.setFirstName("Ivan");
        u.setLastName("N");
        userDao.create(u);

        UserOrganization uo = UserUtils.prepareUserOrganization(u);
        userOrganizationDao.create(uo);

        EventUserOrganization euo = new EventUserOrganization();
        euo.setEvent(e);
        euo.setUuid(UuidProvider.getUuid());
        euo.setRelation(EventUserOrganizationRelation.OWNER);
        euo.setUo(uo);
        euod.create(euo);
        testEUO = euo;
        testUser = u;
    }

    @Test(groups = {"dao"})
    public void testListUserEvents() throws NoRecordException {
        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        User u = UserUtils.createUser();
        u.setEmail("Palko");
        u.setFirstName("Ivan");
        u.setLastName("N");
        userDao.create(u);

        UserOrganization uo = UserUtils.prepareUserOrganization(u);
        userOrganizationDao.create(uo);

        User u1 = UserUtils.createUser();
        u1.setEmail("Ivan");
        u1.setFirstName("Ivan");
        u1.setLastName("N");
        userDao.create(u1);

        UserOrganization uo1 = UserUtils.prepareUserOrganization(u1);
        userOrganizationDao.create(uo1);

        Calendar c = Calendar.getInstance();
        // ------- Event 1 - future
        Event e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setOverseer(eo);
        e.setUuid(UuidProvider.getUuid());
        e.setAcronym("Abc");
        c.set(Calendar.YEAR, 2020);
        e.setFromDate(c.getTime());
        e.setTimeZone("Europe/Prague");
        e.setToDate(c.getTime());
        eventDao.create(e);

        EventUserOrganization euo = new EventUserOrganization();
        euo.setEvent(e);
        euo.setRelation(EventUserOrganizationRelation.OWNER);
        euo.setUo(uo);
        euo.setUuid(UuidProvider.getUuid());
        euod.create(euo);

        // ------- Event 2 - future
        e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setOverseer(eo);
        e.setUuid(UuidProvider.getUuid());
        e.setAcronym("Abc");
        c.set(Calendar.YEAR, 2020);
        e.setFromDate(c.getTime());
        e.setTimeZone("Europe/Prague");
        e.setToDate(c.getTime());
        eventDao.create(e);

        euo = new EventUserOrganization();
        euo.setEvent(e);
        euo.setUuid(UuidProvider.getUuid());
        euo.setRelation(EventUserOrganizationRelation.OWNER);
        euo.setUo(uo);
        euod.create(euo);

        // ------- Event 3 - future
        e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setOverseer(eo);
        e.setUuid(UuidProvider.getUuid());
        e.setAcronym("Abc");
        e.setTimeZone("Europe/Prague");
        e.setFromDate(c.getTime());
        e.setToDate(c.getTime());
        eventDao.create(e);

        euo = new EventUserOrganization();
        euo.setEvent(e);
        euo.setUuid(UuidProvider.getUuid());
        euo.setRelation(EventUserOrganizationRelation.OWNER);
        euo.setUo(uo);
        euod.create(euo);

        // ------- Event 4 - past
        e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setOverseer(eo);
        e.setUuid(UuidProvider.getUuid());
        e.setAcronym("Abc");
        c.set(Calendar.YEAR, 2000);
        e.setFromDate(c.getTime());
        e.setToDate(c.getTime());
        e.setTimeZone("Europe/Prague");
        eventDao.create(e);

        euo = new EventUserOrganization();
        euo.setEvent(e);
        euo.setUuid(UuidProvider.getUuid());
        euo.setRelation(EventUserOrganizationRelation.OWNER);
        euo.setUo(uo);
        euod.create(euo);

        List l = eventDao.listUserEvents(u.getUuid(), true, 0, 10, null);
        assertTrue(l.size() == 3);
        l = eventDao.listUserEvents(u.getUuid(), false, 0, 10, null);
        assertTrue(l.size() == 1);
        List<EventUserOrganization> orgs = eventDao.listUserAllEventsWithOverseer(u.getUuid(), null);
        assertEquals(orgs.size(), 4);


        euod.listUserOrganizationForEvent(Lists.newArrayList(uo.getUser().getUuid()), e.getUuid());

        Map<String, List<EventUserOrganizationThumb>> overseers = euod.listOverseersWithEventsForUser(u.getId());
        assertEquals(overseers.size(), 1);
        assertEquals(overseers.values().iterator().next().size(), 4);
    }

    @Test(groups = {"dao"})
    public void testLoadEUOByEventUuid() throws NoRecordException {
        testCreateEUO();
        EventUserOrganization leuod = euod.loadEventUserOrganizationByEventUuid(testEUO.getEvent().getUuid(), testUser.getId());
        assertEquals(leuod, testEUO);
    }

    /**
     * @param euod the euod to set
     */
    @Autowired(required = true)
    public void setEuod(EventUserOrganizationDao euod) {
        this.euod = euod;
    }

    /**
     * @param eventDao the eventDao to set
     */
    @Autowired(required = true)
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    /**
     * @param overseerDao the overseerDao to set
     */
    @Autowired(required = true)
    public void setOverseerDao(EventOverseerDao overseerDao) {
        this.overseerDao = overseerDao;
    }

    /**
     * @param userDao the userDao to set
     */
    @Autowired(required = true)
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * @param userOrganizationDao the userOrganizationDao to set
     */
    @Autowired(required = true)
    public void setUserOrganizationDao(UserOrganizationDao userOrganizationDao) {
        this.userOrganizationDao = userOrganizationDao;
    }
}
