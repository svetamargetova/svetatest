package consys.admin.event.core.dao;

import consys.admin.event.core.AbstractEventAdminTest;
import consys.admin.event.core.bo.Corporation;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.bo.EventType;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.OrganizationTypeEnum;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.dao.OrganizationDao;
import consys.admin.user.core.dao.UserDao;
import consys.admin.user.core.util.OrganizationUtils;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.UuidProvider;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */
public class EventDaoTest extends AbstractEventAdminTest {

    @Autowired
    private EventDao eventDao;
    @Autowired
    private EventOverseerDao overseerDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private OrganizationDao organizationDao;
    @Autowired
    private CorporationDao corporationDao;

    @Test(groups = {"dao"})
    public void testDao() {
        assertNotNull(eventDao);
    }

    @Test(groups = {"dao"})
    public void testCreateEvent() throws NoRecordException {
        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e = new Event();
        e.setType(EventType.BROKERAGE);
        e.setAcronym("ABS");
        e.setUuid(UuidProvider.getUuid());
        e.setTimeZone("Europe/Prague");
        e.setOverseer(eo);
        eventDao.create(e);

        e = new Event();
        e.setType(EventType.BROKERAGE);
        e.setAcronym("ABC");
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        e.setTimeZone("Europe/Prague");
        eventDao.create(e);

        e = new Event();
        e.setType(EventType.BROKERAGE);
        e.setAcronym("ABD");
        e.setTimeZone("Europe/Prague");
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        eventDao.create(e);

        e = new Event();
        e.setAcronym("ABE");
        e.setType(EventType.BROKERAGE);
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        e.setTimeZone("Europe/Prague");
        eventDao.create(e);

        e = new Event();
        e.setType(EventType.BROKERAGE);
        e.setAcronym("ABF");
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        e.setTimeZone("Europe/Prague");
        eventDao.create(e);

        EventOverseer eoo = overseerDao.loadForEvent(e.getUuid());
        assertEquals(eo, eoo);

    }

    @Test(groups = {"dao"})
    public void testCreateEventWithOrganizators() {
        User user = UserUtils.createUser();
        user.setFirstName("1");
        user.setLastName("2");
        user.setEmail("3");
        userDao.create(user);

        Organization o = OrganizationUtils.createOrganization("a", "a", "a");
        o.setOrganizationType(OrganizationTypeEnum.ANIMATION);
        o.setUserCreated(user);
        organizationDao.create(o);



        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setAcronym("ABS");
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        e.setTimeZone("Europe/Prague");
        e.getOrganizers().add(o);
        eventDao.create(e);
    }

    @Test(groups = {"dao"})
    public void testCreateAndLoadByUuidEvent() throws NoRecordException {
        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setAcronym("ABS");
        e.setUuid(UuidProvider.getUuid());
        e.setTimeZone("Europe/Prague");
        e.setOverseer(eo);
        eventDao.create(e);

        Event el = eventDao.loadByUuid(e.getUuid());

        assertEquals(e, el);

        EventThumb et = eventDao.loadEventThumb(e.getUuid());
        assertEquals(et.getOverseer(), e.getOverseer().getClientSuffix());
    }

    @Test(groups = {"dao"})
    public void testCreateAndLoadByAcronymPrefixEventAndChronologically() {
        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Calendar c = GregorianCalendar.getInstance();

        Event e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setAcronym("ABS");
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        e.setVisible(true);
        e.setTimeZone("Europe/Prague");
        // minulost
        c.set(2000, 1, 1);
        e.setToDate(c.getTime());
        e.setVisible(true);
        eventDao.create(e);

        e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setAcronym("ABC");
        e.setUuid(UuidProvider.getUuid());
        e.setTimeZone("Europe/Prague");
        e.setOverseer(eo);
        e.setVisible(true);
        // minulost
        c.set(2000, 2, 1);
        e.setToDate(c.getTime());
        eventDao.create(e);

        e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setAcronym("ABD");
        e.setUuid(UuidProvider.getUuid());
        e.setTimeZone("Europe/Prague");
        e.setOverseer(eo);
        e.setVisible(true);
        // buducnost
        c.set(2020, 1, 1);
        e.setToDate(c.getTime());
        eventDao.create(e);

        e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setTimeZone("Europe/Prague");
        e.setAcronym("ABFA");
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        e.setVisible(true);
        // buducnost
        c.set(2020, 1, 2);
        e.setToDate(c.getTime());
        eventDao.create(e);

        e = new Event();
        e.setType(EventType.CONFERENCE);
        e.setTimeZone("Europe/Prague");
        e.setAcronym("ABFAA");
        e.setUuid(UuidProvider.getUuid());
        e.setOverseer(eo);
        e.setVisible(true);
        // buducnost
        c.set(2020, 2, 2);
        e.setToDate(c.getTime());
        eventDao.create(e);

        List<Event> ees = eventDao.listByAcronym("AB");
        assertTrue(ees.size() == 5, "Nerovana sa pocet 1");

        ees = eventDao.listByAcronym("ABF");
        assertTrue(ees.size() == 2, "Nerovana sa pocet 2");

        List<EventThumb> thumbs = eventDao.listChronologically(true, 0, 4, null);
        assertTrue(thumbs.size() == 3, "Chronologicaly nevartil spravny(3) pocet " + ees.size());
        assertTrue(thumbs.get(0).getAcronym().equalsIgnoreCase("ABD"), "Prvy neni spravny");
        assertTrue(thumbs.get(1).getAcronym().equalsIgnoreCase("ABFA"), "Druhy neni spravny");
        assertTrue(thumbs.get(2).getAcronym().equalsIgnoreCase("ABFAA"), "Prvy neni spravny");
    }

    @Test(groups = {"dao"})
    public void testCreateCorporationEvent() {
        Corporation c = new Corporation();
        c.setName("Bayerische Motoren Werke");
        c.setUrlPrefix("test");
        corporationDao.create(c);

        EventOverseer eo = new EventOverseer();
        eo.setName("o1.app.takeplace.eu");
        eo.setClientSuffix("o1.app.takeplace.eu");
        eo.setWebServicesUrl("o1.app.takeplace.eu");
        overseerDao.create(eo);

        Event e = new Event();
        e.setType(EventType.BROKERAGE);
        e.setAcronym("ABS");
        e.setUuid(UuidProvider.getUuid());
        e.setTimeZone("Europe/Prague");
        e.setOverseer(eo);
        e.setOwner(c);
        eventDao.create(e);

        Event e1 = null;
        try {
            e1 = eventDao.load(e.getId());
        } catch (Exception ex) {
            fail("Nevytvorila se korporatni akce");
        }
        assertEquals(e1.getOwner().getId(), c.getId());

        List<Event> testEvents = eventDao.listAllEventsWithOverseer("test");
        assertEquals(testEvents.size(), 1);

        List<Event> emptyEvents = eventDao.listAllEventsWithOverseer("");
        assertEquals(emptyEvents.size(), 0);
        List<Event> emptyEvents1 = eventDao.listAllEventsWithOverseer(null);
        assertEquals(emptyEvents1.size(), 0);
    }
}
