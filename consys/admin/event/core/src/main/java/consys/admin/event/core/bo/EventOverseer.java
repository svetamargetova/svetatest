package consys.admin.event.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author Palo
 */
public class EventOverseer implements  ConsysObject{
    private static final long serialVersionUID = -8611068856719050209L;

    private Long id;
    private String name;
    private String clientSuffix;
    private String webServicesUrl;
    private int events = 0;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the events
     */
    public int getEvents() {
        return events;
    }

    /**
     * @param events the events to set
     */
    public void setEvents(int events) {
        this.events = events;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        EventOverseer e = (EventOverseer) obj;
        return e.getName().equalsIgnoreCase(name);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "EventOverseer[name="+name+" count="+events+"]";
    }

    /**
     * @return the clientSuffix
     */
    public String getClientSuffix() {
        return clientSuffix;
    }

    /**
     * @param clientSuffix the clientSuffix to set
     */
    public void setClientSuffix(String clientSuffix) {
        this.clientSuffix = clientSuffix;
    }

    /**
     * @return the webServicesUrl
     */
    public String getWebServicesUrl() {
        return webServicesUrl;
    }

    /**
     * @param webServicesUrl the webServicesUrl to set
     */
    public void setWebServicesUrl(String webServicesUrl) {
        this.webServicesUrl = webServicesUrl;
    }
}
