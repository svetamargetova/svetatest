package consys.admin.event.core.service;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.thumb.EventMaintenanceThumb;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.user.core.bo.UserOrganization;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ReadOnlyPropertyException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import java.util.List;

/**
 *
 * @author palo
 */
public interface EventService {

    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    public void createEvent(Event e, UserOrganization owner)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /** naklonuje vstupni event */
    public Event cloneEvent(Event e, UserOrganization owner) throws ServiceExecutionFailed, RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  L O A D ---- */
    /*------------------------------------------------------------------------*/
    public EventOverseer loadEventOverseer(String eventUuuid)
            throws NoRecordException, RequiredPropertyNullException;

    public Event loadEvent(String eventUuid)
            throws NoRecordException, RequiredPropertyNullException;

    public Event loadEventDetails(String eventUuid)
            throws NoRecordException, RequiredPropertyNullException;

    public EventThumb loadEventThumb(String eventUuid)
            throws NoRecordException, RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  L I S T  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita zoznam strankovany zoznam event chronologicky spatne ak <code>upcoming=false</code>
     * inak v buducnosti. Nacitane eventy maju priznak visible. 
     * @param upcoming
     * @param from
     * @param limit
     * @return 
     */
    public List<EventThumb> listEventsChronologically(boolean upcoming, int from, int limit, String corporationPrefix);

    /**
     * Nacita zoznam strankovany zoznam event chronologicky spatne ak <code>upcoming=false</code>
     * inak v buducnosti. Priznakom <code>onlyVisible</code> je mozne ovplyvnit ci 
     * sa maju zobrazit vsetky alebo len viditelne.
     * @param upcoming
     * @param from
     * @param limit
     * @return 
     */
    public List<EventThumb> listEventsChronologically(boolean upcoming, boolean onlyVisible, int from, int limit, String corporationPrefix);

    /**
     * Nacita pocet eventov vzladom k paramterom
     * 
     * @param upcoming
     * @param onlyVisible
     * @return 
     */
    public int listEventsChronologicallyCount(boolean upcoming, boolean onlyVisible, String corporationPrefix);

    public List<EventMaintenanceThumb> listEventsWithOverseer(int from, int limit);

    public List<EventOverseer> listOverseers();

    /**
     * Na zaklade casu vrati eventy. Mozne filtracie abecedne
     */
    public List<EventThumb> listUserEvents(String userUuid, boolean upcoming, int from, int limit, String corporationPrefix)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Vylistuje vsetky uzivatelske eventy s dotiahnutym
     * @param userUuid uzivatelove uuid
     * @return zoznam eventov s overseerom
     * @throws NoRecordException ak nema ziadne
     * @throws RequiredPropertyNullException ak chyba user uuid
     */
    public List<EventUserOrganization> listAllEventUserOrganizations(String userUuid, String corporationPrefix)
            throws NoRecordException, RequiredPropertyNullException;

    /** Nacte vsechny eventy */
    public List<Event> listAllEvents(String corporationPrefix);

    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Upravy detailne vlastnosti eventu a propaguje ich do overseera.
     *      
     * @param e
     * @throws RequiredPropertyNullException
     * @throws ServiceExecutionFailed
     */
    public void updateEventDetails(Event e)
            throws RequiredPropertyNullException, ServiceExecutionFailed, ReadOnlyPropertyException;

    /**
     * Upravy detailne vlastnosti eventu a propaguje ich do overseera.
     * 
     * @param user
     * @param e
     * @throws RequiredPropertyNullException
     * @throws ServiceExecutionFailed
     */
    public void updateEventDetails(Long user, Event e)
            throws RequiredPropertyNullException, ServiceExecutionFailed, ReadOnlyPropertyException;

    /**
     * Aktivuje event podla uuid. Tato sluzba je volana delegatom z platobneho portalu
     * @param eventUuid
     * @throws RequiredPropertyNullException
     * @throws ServiceExecutionFailed 
     */
    public void updateEventActivate(String eventUuid)
            throws RequiredPropertyNullException, NoRecordException;

    /** Provede aktualizaci property detailu na overseerovi */
    public void updateEventDetailsProperty(Event e);

    /*------------------------------------------------------------------------*/
    /* ----  D E L E T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Zmaze event z databaze.
     */
    public void deleteEvent(String uuid)
            throws RequiredPropertyNullException, NoRecordException;
}
