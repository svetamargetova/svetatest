package consys.admin.event.core.bo.thumb;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventUserOrganizationThumb {
    private String organizationName;
    private String eventUuid;

    public EventUserOrganizationThumb(String organizationName, String eventUuid) {
        this.organizationName = organizationName;
        this.eventUuid = eventUuid;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public String getOrganizationName() {
        return organizationName;
    }
}
