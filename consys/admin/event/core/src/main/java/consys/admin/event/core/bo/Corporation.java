package consys.admin.event.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author pepa
 */
public class Corporation implements ConsysObject {

    private static final long serialVersionUID = 3552606873735968104L;
    // data
    private Long id;
    private String name;
    private String description;
    private String urlPrefix;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlPrefix() {
        return urlPrefix;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }
}
