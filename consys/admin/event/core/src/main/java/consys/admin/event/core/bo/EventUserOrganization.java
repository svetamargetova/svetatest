package consys.admin.event.core.bo;

import consys.admin.user.core.bo.UserOrganization;
import consys.common.core.bo.ConsysObject;
import java.util.Date;

/**
 *
 * @author Palo
 */
public class EventUserOrganization implements ConsysObject{
    private static final long serialVersionUID = 6019083798346337330L;

    
    private String uuid;
    private Event event;
    private UserOrganization uo;
    private Date lastVisit;
    private EventUserOrganizationRelation relation;
    

    /**
     * @return the event
     */
    public Event getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     * @return the uo
     */
    public UserOrganization getUo() {
        return uo;
    }

    /**
     * @param uo the uo to set
     */
    public void setUo(UserOrganization uo) {
        this.uo = uo;
    }

    /**
     * @return the relation
     */
    public EventUserOrganizationRelation getRelation() {
        return relation;
    }

    /**
     * @param relation the relation to set
     */
    public void setRelation(EventUserOrganizationRelation relation) {
        this.relation = relation;
    }    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        EventUserOrganization e = (EventUserOrganization) obj;
        return uuid.equalsIgnoreCase(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("EventUserOrganization[event=%s user=%s relation=%s]", event,uo,relation);
    }



   
    /**
     * @return the lastVisit
     */
    public Date getLastVisit() {
        return lastVisit;
    }

    /**
     * @param lastVisit the lastVisit to set
     */
    public void setLastVisit(Date lastVisit) {
        this.lastVisit = lastVisit;
    }
    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }



}
