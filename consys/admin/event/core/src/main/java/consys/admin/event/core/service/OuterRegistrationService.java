package consys.admin.event.core.service;

import consys.admin.event.core.bo.thumb.BundleOrderRequest;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.registration.ws.RegisterPaymentProfile;
import consys.payment.service.exception.IbanFormatException;
import java.util.HashMap;
import java.util.List;

/**
 * Rozhranie deklarujuce sluzby ktore su pouzivane pri spracovani event registracii
 * zaregistrovanych ci novych uzivatelov, ktory nie su prihlaseny.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface OuterRegistrationService {

    public User createUserEventRegistration(UserLoginInfo newUserLoginInfo, String temporalAffiliation,
            String imageUrl, RegisterPaymentProfile paymentProfile, BundleOrderRequest mainBundle,
            List<BundleOrderRequest> bundles, String eventUuid, HashMap<String, String> answers)
            throws
            NoRecordException,
            UsernameExistsException,
            ServiceExecutionFailed,
            RequiredPropertyNullException,
            BundleCapacityFullException,
            CouponCapacityException,
            CouponCodeException,
            IbanFormatException,
            CouponOutOfDateException;
}
