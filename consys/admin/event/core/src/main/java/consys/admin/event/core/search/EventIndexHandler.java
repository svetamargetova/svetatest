package consys.admin.event.core.search;

import consys.admin.common.core.solr.IndexHandler;
import consys.admin.event.core.bo.Event;
import consys.common.utils.collection.Maps;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventIndexHandler implements IndexHandler<Event>{
  private static final Logger logger = LoggerFactory.getLogger(EventIndexHandler.class);

    @Override
    public Map<String, String> createDoc(Event item) {
        Map<String,String> map = Maps.newHashMap();
        map.put(UUID, item.getUuid());
        map.put(USER_NAME, item.getFullName());
        map.put(TAGS, item.getTags());
        return map;
    }




}
