package consys.admin.event.core.service;

import consys.admin.event.core.bo.Corporation;

/**
 *
 * @author pepa
 */
public interface CorporationService {

    /** zkontroluje jestli je korporace ulozena u nas v databazi */
    boolean checkRegisteredCorporation(String urlPrefix);

    /** nacte korporaci podle prefixu */
    Corporation loadCorporationByPrefix(String urlPrefix);
}
