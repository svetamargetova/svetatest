/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.core.dao;

import consys.admin.event.core.bo.EventOverseer;
import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;


/**
 *
 * @author palo
 */
public interface EventOverseerDao extends GenericDao<EventOverseer> {

    public EventOverseer loadByName(String name) throws NoRecordException;

    public EventOverseer loadForEvent(String eventUuid) throws NoRecordException;

    public EventOverseer loadWithMinEvents() throws NoRecordException;
}
