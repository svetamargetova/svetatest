package consys.admin.event.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.bo.thumb.EventUserOrganizationThumb;
import consys.admin.event.core.dao.EventUserOrganizationDao;
import consys.admin.user.core.bo.UserOrganization;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Palo
 */
public class EventUserOrganizationDaoImpl extends GenericDaoImpl<EventUserOrganization> implements EventUserOrganizationDao {

    @Override
    public EventUserOrganization load(Long id) throws NoRecordException {
        return load(id, EventUserOrganization.class);
    }

    @Override
    public EventUserOrganization loadEventUserOrganizationByEventUuid(String eventUuid, Long userId) throws NoRecordException {
        EventUserOrganization out = (EventUserOrganization) session().
                createQuery("from EventUserOrganization euo where euo.event.uuid=:EVENT_UUID and euo.uo.user.id=:USER_ID").
                setString("EVENT_UUID", eventUuid).
                setLong("USER_ID", userId).
                uniqueResult();

        if (out == null) {
            throw new NoRecordException("No EventUserOrganization for event " + eventUuid + " user id " + userId);
        }
        return out;
    }

    @Override
    public EventUserOrganization loadEventUserOrganizationByEventUuid(String eventUuid, String userUuid) throws NoRecordException {
        EventUserOrganization out = (EventUserOrganization) session().
                createQuery("from EventUserOrganization euo where euo.event.uuid=:EVENT_UUID and euo.uo.user.uuid=:USER_ID").
                setString("EVENT_UUID", eventUuid).
                setString("USER_UUID", userUuid).
                uniqueResult();

        if (out == null) {
            throw new NoRecordException("No EventUserOrganization for event " + eventUuid + " user uuid " + userUuid);
        }
        return out;
    }

    @Override
    public List<UserOrganization> listUserOrganizationForEvent(List<String> userUuids, String eventUuid) throws NoRecordException {
        List<UserOrganization> out = session().createQuery("select uo from EventUserOrganization euo, UserOrganization uo JOIN FETCH uo.user as user where euo.event.uuid=:E_UUID and euo.uo.id=uo.id and user.uuid IN (:UUIDS)").
                setString("E_UUID", eventUuid).
                setParameterList("UUIDS", userUuids).
                list();
        if (out.size() != userUuids.size()) {
            throw new NoRecordException();
        }
        return out;
    }

    @Override
    public UserOrganization loadUserOrganizationForEvent(String userUuid, String eventUuid) throws NoRecordException {
        UserOrganization uo = (UserOrganization) session().
                createQuery("select uo from EventUserOrganization euo, UserOrganization uo JOIN FETCH uo.user as user where euo.event.uuid=:E_UUID and euo.uo.id=uo.id and user.uuid=:UUID").
                setString("E_UUID", eventUuid).
                setString("UUID", userUuid).
                uniqueResult();
        if (uo == null) {
            throw new NoRecordException();
        }
        return uo;

    }

    @Override
    public Map<String, List<EventUserOrganizationThumb>> listOverseersWithEventsForUser(Long userId) {
        List<Object[]> userEvents = session().
                createQuery("select e.overseer.webServicesUrl,e.uuid, uo from EventUserOrganization uoe,Event e,UserOrganization uo LEFT JOIN FETCH uo.organization where uoe.uo.id=uo.id and uo.user.id=:USER_ID and uoe.event.id=e.id").
                setLong("USER_ID", userId).
                list();
        Map<String, List<EventUserOrganizationThumb>> out = Maps.newHashMap();
        for (Object[] e : userEvents) {
            String overseer = (String) e[0];
            String event = (String) e[1];
            List<EventUserOrganizationThumb> events = out.get(overseer);
            if (events == null) {
                events = Lists.newArrayList();
                out.put(overseer, events);
            }
            UserOrganization uo = (UserOrganization) e[2];
            events.add(new EventUserOrganizationThumb(uo.getShortInfo(), event));
        }
        return out;
    }

    @Override
    public Map<String, List<String>> listOverseersForUpdateUserDetails(Long userId) {
        List<Object[]> userEvents = session().
                createQuery("select e.overseer.webServicesUrl,e.uuid from EventUserOrganization uoe,Event e,UserOrganization uo where uoe.uo.id=uo.id and uo.user.id=:USER_ID and uoe.event.id=e.id").
                setLong("USER_ID", userId).
                list();
        Map<String, List<String>> out = Maps.newHashMap();
        for (Object[] e : userEvents) {
            String overseer = (String) e[0];
            String event = (String) e[1];
            List<String> events = out.get(overseer);
            if (events == null) {
                events = Lists.newArrayList();
                out.put(overseer, events);
            }
            events.add(event);
        }
        return out;
    }

    @Override
    public Map<String, List<String>> listOverseersForUpdateOrganization(Long userOrganizationId) {
        List<Object[]> userEvents = session().
                createQuery("select e.overseer.webServicesUrl,e.uuid from EventUserOrganization uoe,Event e,UserOrganization uo where uoe.uo.id=uo.id and uo.id=:UO_ID and uoe.event.id=e.id").
                setLong("UO_ID", userOrganizationId).
                list();
        Map<String, List<String>> out = Maps.newHashMap();
        for (Object[] e : userEvents) {
            String overseer = (String) e[0];
            String event = (String) e[1];
            List<String> events = out.get(overseer);
            if (events == null) {
                events = Lists.newArrayList();
                out.put(overseer, events);
            }
            events.add(event);
        }
        return out;
    }

    @Override
    public void deleteEventUserOrganizationsForEvent(Long eventId) {
        List<EventUserOrganization> euos = session().
                createQuery("select euo from EventUserOrganization euo where euo.event.id=:ID").
                setLong("ID", eventId).
                list();

        for (EventUserOrganization eventUserOrganization : euos) {
            delete(eventUserOrganization);
        }
    }

    @Override
    public List<EventUserOrganization> listEventUserOrganizationsWithUser(Long id) {
        return session().
                createQuery("select uo from EventUserOrganization euo left join fetch euo.uo as uo left join fetch uo.user where euo.event.id=:ID").
                setLong("ID", id).
                list();
    }

    @Override
    public List<EventUserOrganization> listEventUserOrganizationAdminsForEvent(Long eventId) {
        return session().createQuery("from EventUserOrganization euo left join fetch euo.uo where euo.event.id=:ID and (euo.relation=:REL1 or euo.relation=:REL2)").
                setLong("ID", eventId).setInteger("REL1", EventUserOrganizationRelation.ADMINISTRATOR.getId()).
                setInteger("REL2", EventUserOrganizationRelation.OWNER.getId()).list();
    }
}
