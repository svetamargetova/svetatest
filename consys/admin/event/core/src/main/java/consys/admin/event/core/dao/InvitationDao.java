/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.core.dao;

import consys.admin.event.core.bo.Invitation;
import consys.admin.event.core.bo.thumb.EventInvitationThumb;
import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface InvitationDao extends GenericDao<Invitation>{

    public Invitation loadByUuid(String uuid) throws NoRecordException;

    public Invitation loadWithTargetEventAndUserByUuid(String uuid) throws NoRecordException;

    public List<Invitation> listUserActiveInvitations(String userUuid) throws NoRecordException;

    public List<Invitation> listUserAllInvivations(String userUuid) throws NoRecordException;

    public List<EventInvitationThumb> listUserActiveInvitations(String userUuid, int from, int limit)
            throws NoRecordException;

    public List<EventInvitationThumb> listUserInvitations(String userUuid, int from, int limit)
            throws NoRecordException;

}
