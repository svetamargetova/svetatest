/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.core.service.image;

import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.impl.ImageService;
import consys.common.utils.UuidProvider;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class UserImageService extends ImageService {
    
    private String userImageBucket;
        
    @Autowired
    private UserService userService;
    @Autowired
    private EventUserOrganizationService eventUserOrganizationService;

    public String createImageForUserWithoutUserUpdate(String userUuid, String imageUrl) 
            throws MalformedURLException, IOException, NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException{                    
        URL url = new URL(imageUrl);
        HttpURLConnection connection = (HttpURLConnection)  url.openConnection();
        connection.setRequestMethod("HEAD");
        connection.connect();        
        String contentType = connection.getContentType();
        log().debug("Create image from URL: {} {}",imageUrl,contentType);
        
        String name;                
        if(imageUrl.startsWith("http://")){
            name = imageUrl.substring("http://".length());
        }else if(imageUrl.startsWith("https://")){
            name = imageUrl.substring("https://".length());
        }else {
            name = imageUrl;
        }                
        
        BufferedImage bf = ImageIO.read(new URL(imageUrl));
        String subContentType = contentType.substring(6);
        return createOrUpdateUserImage(userUuid, name, bf, contentType, subContentType);            
    }
    
    public String createOrUpdateUserImage(String userUuid, String name, BufferedImage bi, String contentType, String subContentType)
            throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException {
        try {
            User u = userService.loadUserByUuid(userUuid);
            String imageUuid = UuidProvider.getUuid();
            try {
                // listova 56x56
                int width = UserProfileImageEnum.LIST.getWidth();
                int height = UserProfileImageEnum.LIST.getHeight();
                String iuuid = UserProfileImageEnum.LIST.getFullUuid(imageUuid);
                createImage(width, height, bi, name, contentType, subContentType, iuuid,userImageBucket);

                // profilova 120x120
                width = UserProfileImageEnum.PROFILE.getWidth();
                height = UserProfileImageEnum.PROFILE.getHeight();
                iuuid = UserProfileImageEnum.PROFILE.getFullUuid(imageUuid);
                createImage(width, height, bi, name, contentType, subContentType, iuuid,userImageBucket);

                // original
                iuuid = UserProfileImageEnum.ORIGINAL.getFullUuid(imageUuid);
                createImage(bi, name, contentType, subContentType, iuuid,userImageBucket);

            } catch (IOException ex) {
                throw new ServiceExecutionFailed();
            }
            String toDelete = u.getPortraitImageUuidPrefix();

            u.setPortraitImageUuidPrefix(imageUuid);
            eventUserOrganizationService.updateUser(u);
            // teraz mazeme 
            if (StringUtils.isNotBlank(toDelete)) {
                deleteImage(UserProfileImageEnum.ORIGINAL.getFullUuid(toDelete),userImageBucket);
                deleteImage(UserProfileImageEnum.LIST.getFullUuid(toDelete),userImageBucket);
                deleteImage(UserProfileImageEnum.PROFILE.getFullUuid(toDelete),userImageBucket);
            }
            return imageUuid;
        } catch (RequiredPropertyNullException ex) {
            throw new ServiceExecutionFailed();
        }
    }

    

    /**
     * @return the userImageBucket
     */
    public String getUserImageBucket() {
        return userImageBucket;
    }

    /**
     * @param userImageBucket the userImageBucket to set
     */
    public void setUserImageBucket(String userImageBucket) {
        this.userImageBucket = userImageBucket;
    }
}
