package consys.admin.event.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.event.core.bo.Corporation;
import consys.admin.event.core.dao.CorporationDao;
import consys.common.core.exception.NoRecordException;
import org.hibernate.Query;

/**
 *
 * @author pepa
 */
public class CorporationDaoImpl extends GenericDaoImpl<Corporation> implements CorporationDao {

    @Override
    public Corporation load(Long id) throws NoRecordException {
        return load(id, Corporation.class);
    }

    @Override
    public boolean checkRegisteredCorporation(String urlPrefix) {
        Corporation corporation = loadByPrefix(urlPrefix);
        return corporation != null;
    }

    @Override
    public Corporation loadByPrefix(String prefix) {
        Query q = session().createQuery("from Corporation c where c.urlPrefix=:PREFIX").setString("PREFIX", prefix);
        return (Corporation) q.uniqueResult();
    }
}
