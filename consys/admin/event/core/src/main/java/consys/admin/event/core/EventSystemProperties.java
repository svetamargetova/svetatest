/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.core;

/**
 *
 * @author palo
 */
public interface EventSystemProperties {

    /* Regulerna podminena pozvanka uzivatela do akcie  */
    public static final String EVENT_USER_CONFIRM_INVITATION_TEXT    = "event_user_confirm_invitation_text";
    public static final String EVENT_USER_CONFIRM_INVITATION_TITLE   = "event_user_confirm_invitation_title";

    /* Regulerna pozvanka uzivatela do akcie bez potvrdenia */
    public static final String EVENT_USER_WITHOUT_CONFIRM_INVITATION_TEXT    = "event_user_without_confirm_invitation_text";
    public static final String EVENT_USER_WITHOUT_CONFIRM_INVITATION_TITLE   = "event_user_without_confirm_invitation_title";

    /* Pozvanka noveho uzivatela do akcie s nutnym potvrdenim */
    public static final String EVENT_NEW_USER_CONFIRM_INVITATION_TEXT    = "event_new_user_confirm_invitation_text";
    public static final String EVENT_NEW_USER_CONFIRM_INVITATION_TITLE   = "event_new_user_confirm_invitation_title";

    /* Pozvanka noveho uzivatela do akcie bez potvrdenia */
    public static final String EVENT_NEW_USER_WITHOUT_CONFIRM_INVITATION_TEXT    = "event_new_user_without_confirm_invitation_text";
    public static final String EVENT_NEW_USER_WITHOUT_CONFIRM_INVITATION_TITLE   = "event_new_user_without_confirm_invitation_title";

    /* Kladna odpoved na pozvanku tj. ACCEPTED  */
    public static final String EVENT_INVITATION_ACCEPTED_INVITOR_TEXT    = "event_invitation_accepted_invitor_text";
    public static final String EVENT_INVITATION_ACCEPTED_INVITOR_TITLE   = "event_invitation_accepted_invitor_title";

    /* Kladna odpoved na pozvanku tj. ACCEPTED  */
    public static final String EVENT_INVITATION_ACCEPTED_INVITED_TEXT    = "event_invitation_accepted_invited_text";
    public static final String EVENT_INVITATION_ACCEPTED_INVITED_TITLE   = "event_invitation_accepted_invited_title";

    /* Zaporna odpoved na pozvanku tj. DECLINED  */
    public static final String EVENT_INVITATION_DECLINED_TEXT    = "event_invitation_declined_text";
    public static final String EVENT_INVITATION_DECLINED_TITLE   = "event_invitation_declined_title";
}
