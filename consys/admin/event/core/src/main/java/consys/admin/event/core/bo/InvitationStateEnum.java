/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.event.core.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public enum InvitationStateEnum {
    NO_RESPONSE(1),
    ACCEPTED(2),
    DECLINED(3),
    TIMEOUTED(4);

    private static final Logger logger = LoggerFactory.getLogger(EventUserOrganizationRelation.class);
    private Integer id;


    private InvitationStateEnum(Integer id) {
        this.id = id;
    }


    public static InvitationStateEnum fromId(Integer i){
        switch(i){
            case 1: return NO_RESPONSE;
            case 2: return ACCEPTED;
            case 3: return DECLINED;
            case 4: return TIMEOUTED;
        }
        logger.error("FromInt failed! Given id "+i+"has no enum member!");
        throw new RuntimeException();
    }

    public Integer getId(){
        return id;
    }

}
