package consys.admin.event.core.service.mail;

import consys.admin.event.core.bo.Invitation;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 * Notifikacia o akceptovanej pozvanke
 * @author Palo
 */
public class EventInvitationAcceptedToInvited extends AbstractSystemNotification  {

    Invitation invitation;

    public EventInvitationAcceptedToInvited(Invitation invitation) {
        super(SystemMessage.INVITATION_ACCEPTED_INVITED, invitation.getTo().getUuid());
        this.invitation = invitation;

    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        placeHolders.addPlaceHolder("salutation", invitation.getTo().getName());
        placeHolders.addPlaceHolder("email", invitation.getTo().getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("event_name", invitation.getEvent().getFullName());
    }
}
