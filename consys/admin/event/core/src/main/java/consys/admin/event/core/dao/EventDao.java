package consys.admin.event.core.dao;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface EventDao extends GenericDao<Event> {

    /**
     * Nacita prvu uroven eventu
     */
    public Event loadByUuid(String uuid) throws NoRecordException;

    /**
     * Nacita event s overseerom
     */
    public Event loadDetailByUuid(String uuid) throws NoRecordException;

    public EventThumb loadEventThumb(String uuid) throws NoRecordException;

    public List<Event> listByAcronym(String acronym);

    public List<EventThumb> listChronologically(boolean upcoming, int from, int limit, String corporationPrefix);

    public List<EventThumb> listChronologically(boolean upcoming, boolean onlyVisible, int from, int limit, String corporationPrefix);

    public int listEventsChronologicallyCount(boolean upcoming, boolean onlyVisible, String corporationPrefix);

    public List<EventThumb> listUserEvents(String userUuid, boolean upcoming, int start, int limit, String corporationPrefix) throws NoRecordException;

    public List<EventUserOrganization> listUserAllEventsWithOverseer(String userUuid, String corporationPrefix) throws NoRecordException;

    public List<Event> listAllEventsWithOverseer(String corporationPrefix);
}
