package consys.admin.event.core.bo.thumb;

import consys.admin.event.core.bo.EventUserOrganizationRelation;
import org.apache.commons.lang.StringUtils;

/**
 * Request pre pozvanie uzivatela do eventu.
 * @author palo
 */
public final class UserInvitationRequest {

    private String from;
    private String event;
    private String uuid;
    private String name;
    private String email;
    private String message;
    private EventUserOrganizationRelation relation;
    /** Priznak ze sa ma vytvorit pozvanka  */
    private boolean withInvitationConfirm = false;

    public UserInvitationRequest() {
    }

    /**
     * Vytvori novu pozvanku pre existujuceho uzivatela
     */
    public UserInvitationRequest(String from, String toUuid, String event, EventUserOrganizationRelation relation, String message) {
        this.from = from;
        this.event = event;
        this.message = message;
        this.relation = relation;
        this.uuid = toUuid;
    }

    /**
     * Vytvori pozvanku pre noveho uzivatela
     */
    public UserInvitationRequest(String from, String email, String name, String event, EventUserOrganizationRelation relation, String message) {
        this.from = from;
        this.event = event;
        this.name = name;
        this.email = email;
        this.message = message;
        this.relation = relation;
    }

    public boolean isExistingUser() {
        return StringUtils.isNotBlank(uuid);
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the relation
     */
    public EventUserOrganizationRelation getRelation() {
        return relation;
    }

    /**
     * @param relation the relation to set
     */
    public void setRelation(EventUserOrganizationRelation relation) {
        this.relation = relation;
    }

    /**
     * @return the withInvitationConfirm
     */
    public boolean isWithInvitationConfirm() {
        return withInvitationConfirm;
    }

    /**
     * @param withInvitationConfirm the withInvitationConfirm to set
     */
    public void setWithInvitationConfirm(boolean withInvitationConfirm) {
        this.withInvitationConfirm = withInvitationConfirm;
    }

    
    @Override
    public String toString() {
        return String.format("UserInvitationRequest[from=%s to_event=%s to_email=%s to_name=%s to_uuid=%s]",from,event,email,name,uuid);
    }
    
    
}
