package consys.admin.event.core.service.mail;

import consys.admin.event.core.bo.Event;
import consys.admin.user.core.bo.User;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 * Notifikacia o regularnej pozvanke do akcie
 * @author Palo
 */
public class UserWithoutConfirmInvitationEmail extends AbstractSystemNotification {

    private String message;
    private User invited;
    private User invitor;
    private Event event;

    public UserWithoutConfirmInvitationEmail(User invited,User invitor, Event event,String message) {
        super(SystemMessage.INVITATION_NO_CONFIRM, invited.getUuid());
        this.message = message;
        this.invited = invited;
        this.invitor = invitor;
        this.event = event;        
    }       

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        placeHolders.addPlaceHolder("salutation", invited.getName());
        placeHolders.addPlaceHolder("email",invited.getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("event_name",event.getFullName());
        placeHolders.addPlaceHolder("from",invitor.getName());
        placeHolders.addPlaceHolder("from_email",invitor.getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("message",message);
    }
}
