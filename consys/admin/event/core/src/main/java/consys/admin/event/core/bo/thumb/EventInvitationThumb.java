package consys.admin.event.core.bo.thumb;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.InvitationStateEnum;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventInvitationThumb {
    private String uuid;
    private EventThumb event;
    private InvitationStateEnum state;

    /**
     * @return the event
     */
    public EventThumb getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(EventThumb event) {
        this.event = event;
    }

    /**
     * @return the state
     */
    public InvitationStateEnum getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(InvitationStateEnum state) {
        this.state = state;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }






}
