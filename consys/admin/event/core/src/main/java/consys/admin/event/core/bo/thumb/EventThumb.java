package consys.admin.event.core.bo.thumb;

import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventThumb {

    private Date from;
    private Date to;
    private String fullName;
    private String logoUuid;
    private String profileLogoUuid;
    private String uuid;
    private String web;
    private int year;
    private String acronym;
    private String overseer;
    private String description;
    private boolean active;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the logoUuid
     */
    public String getLogoUuid() {
        return logoUuid;
    }

    /**
     * @param logoUuid the logoUuid to set
     */
    public void setLogoUuid(String logoUuid) {
        this.logoUuid = logoUuid;
    }

    public String getProfileLogoUuid() {
        return profileLogoUuid;
    }

    public void setProfileLogoUuid(String profileLogoUuid) {
        this.profileLogoUuid = profileLogoUuid;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the acronym
     */
    public String getAcronym() {
        return acronym;
    }

    /**
     * @param acronym the acronym to set
     */
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    /**
     * @return the overseer
     */
    public String getOverseer() {
        return overseer;
    }

    /**
     * @param overseer the overseer to set
     */
    public void setOverseer(String overseer) {
        this.overseer = overseer;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }
}
