package consys.admin.event.core.bo.thumb;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventMaintenanceThumb {

    
    private String fullName;    
    private String uuid;        
    private String acronym;
    private String overseerWsUrl;
    

   

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

   

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

   

    /**
     * @return the acronym
     */
    public String getAcronym() {
        return acronym;
    }

    /**
     * @param acronym the acronym to set
     */
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    /**
     * @return the overseerWsUrl
     */
    public String getOverseerWsUrl() {
        return overseerWsUrl;
    }

    /**
     * @param overseerWsUrl the overseerWsUrl to set
     */
    public void setOverseerWsUrl(String overseerWsUrl) {
        this.overseerWsUrl = overseerWsUrl;
    }





}
