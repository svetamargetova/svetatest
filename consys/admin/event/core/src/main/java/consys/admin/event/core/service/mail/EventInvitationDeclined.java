package consys.admin.event.core.service.mail;

import consys.admin.event.core.bo.Invitation;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 * Notifikacia o odmietnutej pozvanke
 * @author Palo
 */
public class EventInvitationDeclined extends AbstractSystemNotification {

    Invitation invitation;

    public EventInvitationDeclined(Invitation invitation) {
        super(SystemMessage.INVITATION_DECLINED, invitation.getFrom().getUuid());
        this.invitation = invitation;
        
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        placeHolders.addPlaceHolder("salutation", invitation.getFrom().getName());
        placeHolders.addPlaceHolder("email", invitation.getFrom().getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("event_name", invitation.getEvent().getFullName());
        placeHolders.addPlaceHolder("invited_name",invitation.getTo().getName());
        placeHolders.addPlaceHolder("invited_email",invitation.getTo().getEmailOrMergeEmail());
    }
}
