package consys.admin.event.core.service.mail;

import consys.admin.event.core.bo.Invitation;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 * Notifikacia o regularnej pozvanke do akcie
 * @author Palo
 */
public class UserConfirmInvitationEmail extends AbstractSystemNotification {

    private Invitation invitation;
    private String message;

    public UserConfirmInvitationEmail(Invitation invitation, String message) {
        super(SystemMessage.INVITATION_WITH_CONFIRM, invitation.getTo().getUuid());
        this.invitation = invitation;
        this.message = message;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        placeHolders.addPlaceHolder("salutation", invitation.getTo().getName());
        placeHolders.addPlaceHolder("email",invitation.getTo().getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("event_name",invitation.getEvent().getFullName());
        placeHolders.addPlaceHolder("from",invitation.getFrom().getName());
        placeHolders.addPlaceHolder("from_email",invitation.getFrom().getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("message",message);
    }
}
