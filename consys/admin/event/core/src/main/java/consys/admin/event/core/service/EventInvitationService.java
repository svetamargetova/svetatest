/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.core.service;

import consys.admin.event.core.bo.Invitation;
import consys.admin.event.core.bo.InvitationStateEnum;
import consys.admin.event.core.bo.thumb.EventInvitationThumb;
import consys.admin.event.core.bo.thumb.UserInvitationRequest;
import consys.admin.event.core.service.exception.InvitationAlreadyResolved;
import consys.admin.user.core.bo.User;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface EventInvitationService {

    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Pozvanka do eventu jedneho uzivatela na zaklade pozvanky.
     */
    public User createUserInvivation(UserInvitationRequest req)
            throws NoRecordException, RequiredPropertyNullException;

    
    /**
     * Spracovanie zoznamu pozvanok do eventu
     */
    public List<User> createUsersInvivations(List<UserInvitationRequest> requests)
            throws NoRecordException, RequiredPropertyNullException;

    
    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita pozvanku s dotiahnutymi uzivatelmi a eventom.
     * @throws NoRecordException ak pre uuid a userUuid neexistuje pozvanka
     * @throws RequiredPropertyNullException ak nejaky vstup je blank
     */
    public Invitation loadDetailedInvitation(String uuid, String userUuid)
            throws NoRecordException,
            RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  L I S T  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita uzivatelove <strong>aktivne</strong> pozvanky. Tj. v stave
     * NO_RESPONSE. Zoradene podla datumu pozvanky.
     */
    public List<Invitation> listUserActiveInvitations(String userUuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita uzivatelove pozvanky. Zoradene podla datumu pozvanky.
     */
    public List<Invitation> listUserInvitations(String userUuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita uzivatelove <strong>aktivne</strong> pozvanky. Tj. v stave
     * NO_RESPONSE. Zoradene podla datumu pozvanky. 
     */
    public List<EventInvitationThumb> listUserActiveInvitations(String userUuid, int from, int limit)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita uzivatelove pozvanky. Zoradene podla datumu pozvanky.
     */
    public List<EventInvitationThumb> listUserInvitations(String userUuid, int from, int limit)
            throws NoRecordException, RequiredPropertyNullException;


    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Resolvuje pozvanku do ziadaneho stavu. Ak:
     * DECLINED: posle notifikaciu info invitation.from o tom ze User declined
     * ACCEPTED: posle notifikaciu info invitation.from o accep, vytvori ziadany 
     *   relaciu medzi eventom a invotation.to
     *
     * @throws InvitationAlreadyResolved ak je pozvanka uz resolvovana
     * @throws NoRecordException ak pre vlozene invitationUuid neexistuje pozvanka
     * @throws RequiredPropertyNullException ak je invitationUuid null
     * 
     */
    public void updateInvitation(InvitationStateEnum stateEnum, String invitationUuid, String ownerUuid)
            throws NoRecordException, RequiredPropertyNullException, InvitationAlreadyResolved;


    /*------------------------------------------------------------------------*/
    /* ----  D E L E T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Odstrani vsetky pozvanky ktore su adresovane uzivatelovi. Posle sa email
     * odosielatelovy o tejto skutocnosti ze z dovodu neaktivyti bol uzivatel
     * odstraneny.
     * 
     * @param toUserInvitation
     * @throws NoRecordException
     */
    public void deleteAllInvitations(User toUserInvitation) throws NoRecordException;
}
