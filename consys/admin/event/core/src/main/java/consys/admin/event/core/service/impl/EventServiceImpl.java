package consys.admin.event.core.service.impl;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.bo.thumb.EventMaintenanceThumb;
import consys.admin.event.core.dao.EventDao;
import consys.admin.event.core.dao.EventOverseerDao;
import consys.admin.event.core.dao.EventUserOrganizationDao;
import consys.admin.event.core.service.EventService;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.ws.overseer.EventWebServiceEnum;
import consys.admin.ws.overseer.service.OverseerWebService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.MessagingService;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.date.DateProvider;
import consys.common.utils.UuidProvider;
import consys.common.utils.enums.Currency;
import consys.event.common.ws.EventDescriptor;
import consys.payment.webservice.EventWebService;
import consys.common.ws.utils.WSConvertUtils;
import consys.event.maintenance.ws.*;
import consys.payment.ws.event.InitializeCloneEventRequest;
import consys.payment.ws.event.InitializeCloneEventResponse;
import consys.payment.ws.event.InitializeEventRequest;
import consys.payment.ws.event.InitializeEventResponse;
import java.util.*;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Palo
 */
public class EventServiceImpl extends AbstractService implements EventService {

    private EventDao eventDao;
    private EventOverseerDao overseerDao;
    private EventUserOrganizationDao userOrganizationDao;
    private OverseerWebService overseerWebService;
    private MessagingService messagingService;
    private EventWebService eventWebService;

    @Override
    public void createEvent(Event e, UserOrganization owner) throws ServiceExecutionFailed, RequiredPropertyNullException {
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(DateProvider.getCurrentDate());
        int year = c.get(Calendar.YEAR);
        if (StringUtils.isBlank(e.getAcronym()) || StringUtils.isBlank(e.getFullName()) || e.getYear() < year || e.getCountry() == null
                || owner == null) {
            throw new RequiredPropertyNullException();
        }
        try {


            log().debug("Creating NEW event: {}", e);

            String series = identifyEventSeries(e.getAcronym());

            // vytvorime event
            EventOverseer o = getOverseerDao().loadWithMinEvents();
            e.setOverseer(o);

            log().debug("... overseer loaded");

            e.setUuid(UuidProvider.getUuid());
            e.setCanLogIn(true);
            e.setVisible(false);
            e.setRegistredDate(DateProvider.getCurrentDate());
            e.setSeries(series);
            getEventDao().create(e);
            o.setEvents(o.getEvents() + 1);
            getOverseerDao().update(o);
            log().debug("... event created");
            // vytvorime relation
            EventUserOrganization euo = new EventUserOrganization();
            euo.setEvent(e);
            euo.setUuid(UuidProvider.getUuid());
            euo.setUo(owner);
            euo.setRelation(EventUserOrganizationRelation.OWNER);
            getEventUserOrganizationDao().create(euo);

            log().debug("... event <--> user relation created");


            // inicializujeme event na payment portale - ak tu zlyha este je ok                                                          
            InitializeEventRequest request = EventWebService.OBJECT_FACTORY.createInitializeEventRequest();
            request.setCountry(e.getCountry().getId());
            request.setEventUuid(e.getUuid());

            log().info("Intializing event on PaymentProtal: {}", request);
            InitializeEventResponse initEventResponse = eventWebService.initializeEvent(request);

            if (!initEventResponse.isSuccess()) {
                throw new ServiceExecutionFailed("Create event on PaymenPortal failed!");
            }
            // prevolame web servicu aby sa vytvorila db kde ako vlastnik sa nastavi user org
            // todo: doplnit udaje z action gwt.

            log().debug("... sending request to overseer");

            // WEBSERVICE: Vytvorime objekt pro webservicu            
            boolean emptyOrganization = owner.getUser().getDefaultUserOrganization().getOrganization() == null;
            consys.event.common.ws.EventUserOrganization euows = new consys.event.common.ws.EventUserOrganization();
            euows.setUuid(owner.getUser().getUuid());
            euows.setFullName(owner.getUser().getFullName());
            euows.setPosition(owner.getUser().getDefaultUserOrganization().getPosition());
            euows.setOrganization(emptyOrganization ? "" : owner.getUser().getDefaultUserOrganization().getOrganization().getName());
            euows.setLastName(owner.getUser().getLastName());
            euows.setPotraitImgPrefixUuid(owner.getUser().getPortraitImageUuidPrefix());
            euows.setEmail(owner.getUser().getEmailOrMergeEmail());

            getMessagingService().registerEntity(e.getUuid(), "noreply@takeplace.eu");

            CreateEventRequest eventRequest = new CreateEventRequest();

            EventDescriptor ed = new EventDescriptor();
            ed.setAcronym(e.getAcronym());
            ed.setName(e.getFullName());
            ed.setTimeZone(e.getTimeZone());
            ed.setUuid(e.getUuid());
            ed.setCountry(e.getCountry().getId());
            if (e.getOwner() == null) {
                ed.setCorporationPrefix("");
            } else {
                ed.setCorporationPrefix(e.getOwner().getUrlPrefix());
            }

            eventRequest.setEvent(ed);
            eventRequest.setCreater(euows);
            switch (initEventResponse.getEventCurrency()) {
                case CZK:
                    eventRequest.setCurrencyId(Currency.CZK.toId());
                    break;
                case EUR:
                    eventRequest.setCurrencyId(Currency.EUR.toId());
                    break;
                case USD:
                    eventRequest.setCurrencyId(Currency.USD.toId());
                    break;
                default:
                    throw new NullPointerException("There is missing Currency in response of init event on payment portal?");
            }

            try {
                getOverseerWebService().sendAndReceive(EventWebServiceEnum.Maintenance, o.getWebServicesUrl(), eventRequest);

            } catch (Exception ex) {
                log().error("Exception ", ex);
                throw new ServiceExecutionFailed("Overseer URL error! " + o.getName());
            }

            log().info("... Create event {} complete on {} ", e, o);
        } catch (NoRecordException ex) {
            log().error("Can't create new event, missing overseer!!!");
            throw new ServiceExecutionFailed("Can't create new event, missing overseer!!!");
        }
    }

    @Override
    public Event cloneEvent(Event event, UserOrganization owner) throws ServiceExecutionFailed, RequiredPropertyNullException {
        try {
            EventUserOrganization euo = getEventUserOrganizationDao().loadEventUserOrganizationByEventUuid(event.getUuid(), owner.getUser().getId());
            EventUserOrganizationRelation userRelation = euo.getRelation();
            if (!userRelation.equals(EventUserOrganizationRelation.OWNER) && !userRelation.equals(EventUserOrganizationRelation.ADMINISTRATOR)) {
                log().info("User isn't owner or administration of event");
                throw new RequiredPropertyNullException();
            }
        } catch (NoRecordException ex) {
            log().error("Can't load user organization");
            throw new ServiceExecutionFailed();
        }

        log().debug("Clone event: {}", event);

        try {
            // ADMINISTRACE

            EventOverseer o = getOverseerDao().loadForEvent(event.getUuid());

            List<Organization> organizers = new ArrayList<Organization>();
            for (Organization org : event.getOrganizers()) {
                organizers.add(org);
            }

            // urceni cisla serie
            String series = identifyEventSeries(event.getAcronym());

            Event e = new Event();
            e.setAcronym(event.getAcronym());
            e.setCanLogIn(true);
            e.setCity(e.getCity());
            e.setCountry(event.getCountry());
            e.setDescription(event.getDescription());
            e.setFromDate(null);
            e.setFullName(event.getFullName());
            e.setImageLogoPrefix(event.getImageLogoPrefix());
            e.setOrganizers(organizers);
            e.setOverseer(o);
            e.setPortraitLogoPrefix(event.getPortraitLogoPrefix());
            e.setRegistredDate(DateProvider.getCurrentDate());
            e.setSeries(series);
            e.setShortName(event.getShortName());
            e.setStreet(event.getStreet());
            e.setPlace(event.getPlace());
            e.setTags(event.getTags());
            e.setTimeZone(event.getTimeZone());
            e.setToDate(null);
            e.setType(event.getType());
            e.setUniversalName(event.getUniversalName());
            e.setUsState(event.getUsState());
            e.setUuid(UuidProvider.getUuid());
            e.setVisible(false);
            e.setWeb(event.getWeb());
            e.setYear(event.getYear());
            e.setOwner(event.getOwner());

            getEventDao().create(e);
            o.setEvents(o.getEvents() + 1);
            getOverseerDao().update(o);
            log().debug("... event cloned in administration");

            /*EventUserOrganization euo = new EventUserOrganization();
             euo.setEvent(e);
             euo.setUuid(UuidProvider.getUuid());
             euo.setUo(owner);
             euo.setRelation(EventUserOrganizationRelation.OWNER);
             getEventUserOrganizationDao().create(euo);
             log().debug("... event <--> user relation created (owner)");*/

            List<EventUserOrganization> euos = getEventUserOrganizationDao().listEventUserOrganizationAdminsForEvent(event.getId());
            for (EventUserOrganization euoa : euos) {
                EventUserOrganization uo = new EventUserOrganization();
                uo.setEvent(e);
                uo.setUuid(UuidProvider.getUuid());
                uo.setUo(euoa.getUo());
                uo.setRelation(euoa.getRelation());
                getEventUserOrganizationDao().create(uo);
                log().debug("... event <--> user relation created ({})", euoa.getRelation().toString());
            }

            // OVERSEER
            CloneEventRequest cloneRequest = new CloneEventRequest();
            cloneRequest.setOriginalEventUuid(event.getUuid());
            cloneRequest.setCloneEventUuid(e.getUuid());
            cloneRequest.setSeries(e.getSeries());

            log().debug("... sending request to overseer");
            try {
                CloneEventResponse response = getOverseerWebService().sendAndReceive(
                        EventWebServiceEnum.Maintenance, o.getWebServicesUrl(), cloneRequest);
                if (!response.isSuccess()) {
                    throw new Exception("Overseer not clone event");
                }
            } catch (Exception ex) {
                log().error("Exception ", ex);
                throw new ServiceExecutionFailed("Overseer URL error! " + o.getName());
            }

            log().debug("... event cloned in overseer");
            log().info("... clone event {} complete on {} ", event, o);

            // PAYMENT
            InitializeCloneEventRequest request = EventWebService.OBJECT_FACTORY.createInitializeCloneEventRequest();
            request.setOriginalEventUuid(event.getUuid());
            request.setCloneEventUuid(e.getUuid());

            log().info("... intializing clone event on PaymentProtal: {}", request);
            InitializeCloneEventResponse initEventResponse = eventWebService.initializeCloneEvent(request);

            if (!initEventResponse.isSuccess()) {
                throw new ServiceExecutionFailed("Clone event on PaymenPortal failed!");
            }

            // MESSAGING
            getMessagingService().registerEntity(e.getUuid(), "noreply@takeplace.eu");

            return e;
        } catch (NoRecordException ex) {
            log().error("Can't clone event, missing overseer!!!");
            throw new ServiceExecutionFailed("Can't clone event, missing overseer!!!");
        }
    }

    /** urci hodnotu nasledujici serie */
    private String identifyEventSeries(String acronym) {
        List<Event> sameAcronymEvents = getEventDao().listByAcronym(acronym);
        int maxSeries = sameAcronymEvents.size();
        for (Event e : sameAcronymEvents) {
            try {
                int series = Integer.parseInt(e.getSeries());
                if (series > maxSeries) {
                    maxSeries = series;
                }
            } catch (Exception ex) {
                // neni cislo, coz by melo byt, ale nevadi
            }
        }
        return String.valueOf(maxSeries + 1);
    }

    @Override
    public List<EventThumb> listEventsChronologically(boolean upcoming, int from, int limit, String corporationPrefix) {
        return eventDao.listChronologically(upcoming, from, limit, corporationPrefix);
    }

    @Override
    public List<EventThumb> listEventsChronologically(boolean upcoming, boolean onlyVisible, int from, int limit, String corporationPrefix) {
        return eventDao.listChronologically(upcoming, onlyVisible, from, limit, corporationPrefix);
    }

    @Override
    public int listEventsChronologicallyCount(boolean upcoming, boolean onlyVisible, String corporationPrefix) {
        return eventDao.listEventsChronologicallyCount(upcoming, onlyVisible, corporationPrefix);
    }

    @Override
    public List<EventThumb> listUserEvents(String userUuid, boolean upcoming, int from, int limit, String corporationPrefix) throws NoRecordException {
        return eventDao.listUserEvents(userUuid, upcoming, from, limit, corporationPrefix);
    }

    @Override
    public List<EventMaintenanceThumb> listEventsWithOverseer(int from, int limit) {
        throw new NullPointerException("Not yet implemented");
    }

    @Override
    public List<EventOverseer> listOverseers() {
        return overseerDao.listAll(EventOverseer.class);
    }

    @Override
    public List<EventUserOrganization> listAllEventUserOrganizations(String userUuid, String corporationPrefix)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return eventDao.listUserAllEventsWithOverseer(userUuid, corporationPrefix);
    }

    @Override
    public List<Event> listAllEvents(String corporationPrefix) {
        return eventDao.listAllEventsWithOverseer(corporationPrefix);
    }

    @Override
    public EventOverseer loadEventOverseer(String eventUuuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(eventUuuid)) {
            throw new RequiredPropertyNullException();
        }
        return overseerDao.loadForEvent(eventUuuid);

    }

    @Override
    public Event loadEventDetails(String eventUuid) throws NoRecordException {
        return eventDao.loadDetailByUuid(eventUuid);
    }

    @Override
    public Event loadEvent(String eventUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyNullException();
        }
        return eventDao.loadByUuid(eventUuid);
    }

    @Override
    public EventThumb loadEventThumb(String eventUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyNullException("Missing event UUID");
        }
        return eventDao.loadEventThumb(eventUuid);

    }

    /* ------------------------------------------------------------------------ */
    /* ---- U P D A T E ---- */
    /* ------------------------------------------------------------------------ */
    @Override
    public void updateEventDetails(Event e)
            throws RequiredPropertyNullException, ServiceExecutionFailed {
        if (e == null) {
            throw new RequiredPropertyNullException();
        }
        log().debug("Update: {}", e);
        // test ci timezona je validna
        TimeZone tz = TimeZone.getTimeZone(e.getTimeZone());
        if (tz == null) {
            throw new ServiceExecutionFailed("TimeZone " + e.getTimeZone() + " is invalid.");
        }

        // Check na prava?
        eventDao.update(e, true);

        // Update na overseerovi
        updateEventDetailsProperty(e);
    }

    @Override
    public void updateEventDetails(Long userId, Event e) throws RequiredPropertyNullException, ServiceExecutionFailed {

        if (userId == null || userId < 1L || e == null) {
            throw new RequiredPropertyNullException();
        }

        updateEventDetails(e);
    }

    @Override
    public void updateEventActivate(String eventUuid)
            throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyNullException("Missing event uuid");
        }

        Event event = eventDao.loadByUuid(eventUuid);
        event.setVisible(true);
        event.setCanLogIn(true);
        eventDao.update(event);
    }

    @Override
    public void updateEventDetailsProperty(Event e) {
        Date from = e.getFromDate();
        if (from != null && e.getFromTime() != null) {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(from);
            c.set(GregorianCalendar.HOUR_OF_DAY, e.getFromTime().intValue() / 60);
            c.set(GregorianCalendar.MINUTE, e.getFromTime().intValue() % 60);
            from = c.getTime();
        }

        Date to = e.getToDate();
        if (to != null && e.getToTime() != null) {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(to);
            c.set(GregorianCalendar.HOUR_OF_DAY, e.getToTime().intValue() / 60);
            c.set(GregorianCalendar.MINUTE, e.getToTime().intValue() % 60);
            to = c.getTime();
        }

        EventDescriptor ed = new EventDescriptor();
        ed.setAcronym(e.getAcronym());
        ed.setCity(e.getCity());
        ed.setCountry(e.getCountry() == null ? 0 : e.getCountry().getId());
        ed.setDescription(e.getDescription());
        ed.setFromDate(WSConvertUtils.toXmlCalendar(from));
        ed.setImageLogoPrefix(e.getImageLogoPrefix());
        ed.setImageProfileLogoPrefix(e.getPortraitLogoPrefix());
        ed.setName(e.getFullName());
        ed.setShortName(e.getShortName());
        ed.setStreet(e.getStreet());
        ed.setPlace(e.getPlace());
        ed.setTags(e.getTags());
        ed.setTimeZone(e.getTimeZone());
        ed.setToDate(WSConvertUtils.toXmlCalendar(to));
        ed.setUniversalName(e.getUniversalName());
        ed.setUuid(e.getUuid());
        ed.setWeb(e.getWeb());
        ed.setZip("");
        ed.setYear(e.getYear());
        ed.setSeries(StringUtils.isNotBlank(e.getSeries()) ? e.getSeries() : "1");
        if (e.getOwner() == null) {
            ed.setCorporationPrefix("");
        } else {
            ed.setCorporationPrefix(e.getOwner().getUrlPrefix());
        }

        UpdateEventRequest updateEventRequest = new UpdateEventRequest();
        updateEventRequest.setEventDescriptor(ed);

        getOverseerWebService().sendAndReceive(EventWebServiceEnum.Maintenance, e.getOverseer().getWebServicesUrl(), updateEventRequest);
    }

    /* ------------------------------------------------------------------------ */
    /* ---- D E L E T E ---- */
    /* ------------------------------------------------------------------------ */
    /**
     * Zmaze event z databaze.
     */
    @Override
    public void deleteEvent(String uuid)
            throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException("Missing event uuid");
        }
        // zmazeme event na administracii
        Event event = eventDao.loadDetailByUuid(uuid);
        log().info("Delete {}", event);
        userOrganizationDao.deleteEventUserOrganizationsForEvent(event.getId());
        eventDao.delete(event);

        // zmazeme event v overseerovi
        DeleteEventRequest der = new DeleteEventRequest();
        der.setEventUuid(event.getUuid());
        getOverseerWebService().sendAndReceive(EventWebServiceEnum.Maintenance, event.getOverseer().getWebServicesUrl(), der);

    }

    /**
     * @return the eventDao
     */
    public EventDao getEventDao() {
        return eventDao;
    }

    /**
     * @param eventDao the eventDao to set
     */
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    /**
     * @return the overseerDao
     */
    public EventOverseerDao getOverseerDao() {
        return overseerDao;
    }

    /**
     * @param overseerDao the overseerDao to set
     */
    public void setOverseerDao(EventOverseerDao overseerDao) {
        this.overseerDao = overseerDao;
    }

    /**
     * @return the userOrganizationDao
     */
    public EventUserOrganizationDao getEventUserOrganizationDao() {
        return userOrganizationDao;
    }

    /**
     * @param userOrganizationDao the userOrganizationDao to set
     */
    public void setUserOrganizationDao(EventUserOrganizationDao userOrganizationDao) {
        this.userOrganizationDao = userOrganizationDao;
    }

    /**
     * @return the overseerWebService
     */
    public OverseerWebService getOverseerWebService() {
        return overseerWebService;
    }

    /**
     * @param overseerWebService the overseerWebService to set
     */
    public void setOverseerWebService(OverseerWebService overseerWebService) {
        this.overseerWebService = overseerWebService;
    }

    public MessagingService getMessagingService() {
        return messagingService;
    }

    public void setMessagingService(MessagingService messagingService) {
        this.messagingService = messagingService;
    }

    /**
     * @return the eventWebService
     */
    public EventWebService getEventWebService() {
        return eventWebService;
    }

    /**
     * @param eventWebService the eventWebService to set
     */
    public void setEventWebService(EventWebService eventWebService) {
        this.eventWebService = eventWebService;
    }
}
