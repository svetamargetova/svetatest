package consys.admin.event.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.event.core.dao.EventDao;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.date.DateProvider;
import consys.common.utils.collection.Lists;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class EventDaoImpl extends GenericDaoImpl<Event> implements EventDao {

    @Override
    public Event load(Long id) throws NoRecordException {
        return load(id, Event.class);
    }

    @Override
    public Event loadDetailByUuid(String uuid) throws NoRecordException {
        Event e = (Event) session().createQuery("from Event e JOIN FETCH e.overseer LEFT JOIN FETCH e.owner where e.uuid=:UUID").setString("UUID", uuid).uniqueResult();
        if (e == null) {
            throw new NoRecordException("Event with uuid " + uuid + " not exists!");
        }
        return e;
    }

    @Override
    public Event loadByUuid(String uuid) throws NoRecordException {
        Event e = (Event) session().createQuery("from Event e LEFT JOIN FETCH e.owner where e.uuid=:UUID").setString("UUID", uuid).uniqueResult();
        if (e == null) {
            throw new NoRecordException("Event with uuid " + uuid + " not exists!");
        }
        return e;
    }

    @Override
    public EventThumb loadEventThumb(String uuid) throws NoRecordException {
        List<Object[]> ee = session().
                createQuery("select e.uuid, e.web, e.fromDate, e.toDate, e.fullName, e.imageLogoPrefix, e.year, e.acronym, o.clientSuffix, e.description,e.visible,e.portraitLogoPrefix from Event e left join e.overseer as o where e.uuid=:UUID").
                setString("UUID", uuid).
                list();
        if (ee == null || ee.isEmpty()) {
            throw new NoRecordException("Event with uuid " + uuid + " not exists!");
        }

        Object[] o = ee.get(0);
        EventThumb thumb = new EventThumb();
        thumb.setUuid((String) o[0]);
        thumb.setWeb((String) o[1]);
        thumb.setFrom((Date) o[2]);
        thumb.setTo((Date) o[3]);
        thumb.setFullName((String) o[4]);
        thumb.setLogoUuid((String) o[5]);
        thumb.setProfileLogoUuid((String) o[11]);
        thumb.setYear((Integer) o[6]);
        thumb.setAcronym((String) o[7]);
        thumb.setOverseer((String) o[8]);
        thumb.setDescription((String) o[9]);
        thumb.setActive((Boolean) o[10]);
        return thumb;
    }

    @Override
    public List<Event> listByAcronym(String acronym) {
        Query q = session().createQuery("from Event e LEFT JOIN FETCH e.owner where e.acronym like :acronym");
        q.setString("acronym", acronym + "%");
        return q.list();
    }

    @Override
    public List<EventThumb> listChronologically(boolean upcoming, boolean onlyVisible, int from, int limit, String corporationPrefix) {
        final boolean emptyPrefix = emptyPrefix(corporationPrefix);

        StringBuilder hql = new StringBuilder("select e.uuid, e.web, e.fromDate, e.toDate, e.fullName, e.imageLogoPrefix, e.year, e.acronym from Event e where ");

        if (!emptyPrefix) {
            hql.append("e.owner is not null and e.owner.urlPrefix=:PREFIX and ");
        } else {
            hql.append("e.owner is null and ");
        }

        if (onlyVisible) {
            hql.append(" e.visible = true and ");
        }

        if (upcoming) {
            hql.append(" (e.toDate >= :THIS_DATE or e.toDate is null) ");
        } else {
            hql.append(" e.toDate < :THIS_DATE order by e.fromDate desc ");

        }

        Query q = session().createQuery(hql.toString());
        q.setDate("THIS_DATE", DateProvider.getCurrentDate());
        if (!emptyPrefix) {
            q.setString("PREFIX", corporationPrefix);
        }
        q.setFirstResult(from);
        q.setMaxResults(limit);
        List<EventThumb> thumbs = Lists.newArrayList();
        List<Object[]> events = q.list();
        for (Object[] o : events) {
            EventThumb thumb = new EventThumb();
            thumb.setUuid((String) o[0]);
            thumb.setWeb((String) o[1]);
            thumb.setFrom((Date) o[2]);
            thumb.setTo((Date) o[3]);
            thumb.setFullName((String) o[4]);
            thumb.setLogoUuid((String) o[5]);
            thumb.setYear((Integer) o[6]);
            thumb.setAcronym((String) o[7]);
            thumbs.add(thumb);
        }
        return thumbs;

    }

    @Override
    public List<EventThumb> listChronologically(boolean upcoming, int from, int limit, String corporationPrefix) {
        return listChronologically(upcoming, true, from, limit, corporationPrefix);
    }

    @Override
    public int listEventsChronologicallyCount(boolean upcoming, boolean onlyVisible, String corporationPrefix) {
        final boolean emptyPrefix = emptyPrefix(corporationPrefix);

        StringBuilder hql = new StringBuilder("select count(e.id) from Event e where ");

        if (!emptyPrefix) {
            hql.append("e.owner is not null and e.owner.urlPrefix=:PREFIX and ");
        } else {
            hql.append("e.owner is null and ");
        }

        if (onlyVisible) {
            hql.append(" e.visible = true and ");
        }

        if (upcoming) {
            hql.append(" (e.toDate >= :THIS_DATE or e.toDate is null) ");
        } else {
            hql.append(" e.toDate < :THIS_DATE ");

        }

        Query q = session().createQuery(hql.toString());
        q.setDate("THIS_DATE", DateProvider.getCurrentDate());
        if (!emptyPrefix) {
            q.setString("PREFIX", corporationPrefix);
        }
        return ((Long) q.uniqueResult()).intValue();
    }

    /**
     * Nacita uzivatelove eventy.
     * POZN: aj tie ktore nemaju vlozeny fromDate, su visibile=FALSE
     */
    @Override
    public List<EventThumb> listUserEvents(String userUuid, boolean upcoming, int start, int limit, String corporationPrefix) throws NoRecordException {
        final boolean emptyPrefix = emptyPrefix(corporationPrefix);
        Query q;
        if (upcoming) {
            // attending
            StringBuilder sb = new StringBuilder("select e.uuid,e.web,e.fromDate,e.toDate,e.fullName, ");
            sb.append("e.imageLogoPrefix,e.year,e.acronym from EventUserOrganization euo join euo.event as e ");
            sb.append("where euo.uo.user.uuid=:UUID and euo.relation > 0 ");
            if (!emptyPrefix) {
                sb.append("and e.owner is not null and e.owner.urlPrefix=:PREFIX ");
            } else {
                sb.append("and e.owner is null ");
            }
            sb.append("and (e.toDate >= :THIS_DATE or e.toDate is null) order by e.toDate,e.acronym");
            q = session().createQuery(sb.toString());
        } else {
            // attended
            StringBuilder sb = new StringBuilder("select e.uuid,e.web,e.fromDate,e.toDate,e.fullName, ");
            sb.append("e.imageLogoPrefix,e.year,e.acronym from EventUserOrganization euo join euo.event as e ");
            sb.append("where euo.uo.user.uuid=:UUID and euo.relation > 0 and e.toDate < :THIS_DATE ");
            if (!emptyPrefix) {
                sb.append("and e.owner is not null and e.owner.urlPrefix=:PREFIX ");
            } else {
                sb.append("and e.owner is null ");
            }
            sb.append("order by e.toDate,e.acronym");
            q = session().createQuery(sb.toString());
        }

        q.setString("UUID", userUuid).setDate("THIS_DATE", DateProvider.getCurrentDate());
        if (!emptyPrefix) {
            q.setString("PREFIX", corporationPrefix);
        }
        List<Object[]> events = q.setFirstResult(start).setMaxResults(limit).list();
        if (events == null || events.isEmpty()) {
            throw new NoRecordException("User with UUID " + userUuid + " has no events");
        }

        List<EventThumb> thumbs = Lists.newArrayList();
        for (Object[] o : events) {
            EventThumb thumb = new EventThumb();
            thumb.setUuid((String) o[0]);
            thumb.setWeb((String) o[1]);
            thumb.setFrom((Date) o[2]);
            thumb.setTo((Date) o[3]);
            thumb.setFullName((String) o[4]);
            thumb.setLogoUuid((String) o[5]);
            thumb.setYear((Integer) o[6]);
            thumb.setAcronym((String) o[7]);
            thumbs.add(thumb);
        }
        return thumbs;
    }

    @Override
    public List<EventUserOrganization> listUserAllEventsWithOverseer(String userUuid, String corporationPrefix) throws NoRecordException {
        final boolean emptyPrefix = emptyPrefix(corporationPrefix);
        StringBuilder sb = new StringBuilder("select euo from EventUserOrganization euo join fetch euo.event as e ");
        sb.append("join fetch e.overseer where euo.uo.user.uuid=:USER_UUID ");
        if (!emptyPrefix) {
            sb.append("and e.owner is not null and e.owner.urlPrefix=:PREFIX ");
        } else {
            sb.append("and e.owner is null");
        }

        Query q = session().createQuery(sb.toString());
        q.setString("USER_UUID", userUuid);
        if (!emptyPrefix) {
            q.setString("PREFIX", corporationPrefix);
        }

        List<EventUserOrganization> out = q.list();
        if (out == null || out.isEmpty()) {
            throw new NoRecordException();
        }
        return out;
    }

    @Override
    public List<Event> listAllEventsWithOverseer(String corporationPrefix) {
        final boolean emptyPrefix = emptyPrefix(corporationPrefix);
        StringBuilder sb = new StringBuilder("from Event e JOIN FETCH e.overseer LEFT JOIN FETCH e.owner where ");
        if (!emptyPrefix) {
            sb.append("e.owner is not null and e.owner.urlPrefix=:PREFIX ");
        } else {
            sb.append("e.owner is null");
        }

        Query q = session().createQuery(sb.toString());
        if (!emptyPrefix) {
            q.setString("PREFIX", corporationPrefix);
        }
        return q.list();
    }

    /** pokud je prefix prazdny nebo null, vraci false */
    private boolean emptyPrefix(String prefix) {
        return StringUtils.isBlank(prefix);
    }
}
