/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.core.service;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.bo.thumb.SelectedEvent;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import java.util.List;
import java.util.Map;

/**
 *
 * @author palo
 */
public interface EventUserOrganizationService {


    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    public UserOrganization loadUserOrganizationWithUserForEvent(String userUuid, String eventUuid)
            throws NoRecordException, RequiredPropertyNullException;


     /*------------------------------------------------------------------------*/
    /* ---- L I S T  ---- */
    /*------------------------------------------------------------------------*/
    public List<UserOrganization> listEventUserOrganizations(List<String> uuids, String eventUuid)
            throws NoRecordException, RequiredPropertyNullException;

    public Map<EventUserOrganizationRelation, List<EventUserOrganization>> listEventUserOrganizations(String userUuid)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

     /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Load UserOrganizationEvent. Ak neexistuje tak
     * mu vytvori s relation VISITOR ak neexistuje len upravi cas
     */
    public SelectedEvent updateAndLoadSelectedEventByEventUuid(String eventUuid, Long userId)
            throws ServiceExecutionFailed, NoRecordException, RequiredPropertyNullException;

    public void activateInvitedUser(String firstName, String lastName, String password, String key)
            throws
            RequiredPropertyNullException,
            ServiceExecutionFailed,
            ActivationKeyException;

    /**
     * Update uzivatela sa nachadza v event module z dovodu toho ze kazdy update
     * aktualizuje ak je to nutne overseere.
     */
    public void updateUser(User u)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Update uzivatelovej organizacie sa nachadza v event module z dovodu toho ze kazdy update
     * aktualizuje ak je to nutne overseere.
     */
    public void updateUserOrganization(UserOrganization uo, boolean preferred)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Zmeni vztah uzivatelov k eventu podla zadanej relacie. Ak je vlozena message
     * tak sa posle uzivatelom o tom sprava.
     */
    public void updateEventUserOrganizationRelation(List<String> userUuids, String eventUuid, String message, EventUserOrganizationRelation newRelation)
            throws NoRecordException, RequiredPropertyNullException;       

    public void updateEventUserOrganizationRelation(List<User> user, Event event,String message, EventUserOrganizationRelation newRelation)
            throws NoRecordException, RequiredPropertyNullException;

    public void updateEventUserOrganizationRelation(String userUuids, String eventUuid, String message, EventUserOrganizationRelation newRelation)
            throws NoRecordException, RequiredPropertyNullException;

    public void updateEventUserOrganizationRelation(User user, Event event, String message, EventUserOrganizationRelation newRelation)
            throws NoRecordException, RequiredPropertyNullException;

    public void updateEventUserOrganizationRelation(User user, Event event, EventUserOrganizationRelation newRelation)
            throws NoRecordException, RequiredPropertyNullException;


    public void generateActivationManagement();

}
