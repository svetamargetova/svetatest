package consys.admin.event.core.bo;

import consys.admin.user.core.bo.Organization;
import consys.common.core.bo.ConsysObject;
import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.UsStateEnum;
import consys.common.utils.collection.Lists;
import java.util.Date;
import java.util.List;

/**
 * Bazovy objekt obsahujuci identifikacne informacie o akcii.
 * @author Palo
 */
//@Indexed(handler=EventIndexHandler.class)
public class Event implements ConsysObject {

    private static final long serialVersionUID = 3749762525284420092L;
    private Long id;
    private String uuid;
    // Akronym akcie
    private String acronym;
    // Medzinardone meno
    private String universalName;
    // Od
    private Date fromDate;
    private Integer fromTime;
    // do
    private Date toDate;
    private Integer toTime;
    // internetova adresa
    private String web;
    // ulice
    private String street;
    // misto konani
    private String place;
    // mesto
    private String city;
    // prefix obdlznikoveho loga
    private String imageLogoPrefix;
    // prefix obdlznikoveho loga
    private String portraitLogoPrefix;
    // rok konania -- datum od musi byt v roku konanie
    private int year; // 4 znaky
    // seria I. II.
    private String series;
    // Stat konania
    private CountryEnum country;
    // Stat americky
    private UsStateEnum uSState;
    // plne meno v povodnom jazyku
    private String fullName;
    // kratke meno
    private String shortName;
    // tagy
    private String tags;
    // popis
    private String description;
    // casova zona
    private String timeZone;
    // overseer kde je event spravovany
    private EventOverseer overseer;
    // organizatory
    private List<Organization> organizers;
    // typ eventu
    private EventType type;
    // datum registracie eventu
    private Date registredDate;
    /** Ci je viditelny ve vseckych */
    private boolean visible = true;
    /** Ci je mozne sa vubec prokliknut - default ANO */
    private boolean canLogIn = true;
    /** vlastnik akce (pokud neni null, je akce vlastnena nami evidovanou korporaci) */
    private Corporation owner;

    public Event() {
        organizers = Lists.newArrayList();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the acronym
     */
    public String getAcronym() {
        return acronym;
    }

    /**
     * @param acronym the acronym to set
     */
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    /**
     * @return the fromDate
     */
    public Date getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate the fromDate to set
     */
    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Integer getFromTime() {
        return fromTime;
    }

    public void setFromTime(Integer fromTime) {
        this.fromTime = fromTime;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getToTime() {
        return toTime;
    }

    public void setToTime(Integer toTime) {
        this.toTime = toTime;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the imageLogoPrefix
     */
    public String getImageLogoPrefix() {
        return imageLogoPrefix;
    }

    /**
     * @param imageLogoPrefix the imageLogoPrefix to set
     */
    public void setImageLogoPrefix(String imageLogoPrefix) {
        this.imageLogoPrefix = imageLogoPrefix;
    }

    /**
     * @return the state
     */
    public CountryEnum getCountry() {
        return country;
    }

    /**
     * @param state the state to set
     */
    public void setCountry(CountryEnum state) {
        this.country = state;
    }

    /**
     * @return the overseer
     */
    public EventOverseer getOverseer() {
        return overseer;
    }

    /**
     * @param overseer the overseer to set
     */
    public void setOverseer(EventOverseer overseer) {
        this.overseer = overseer;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Event e = (Event) obj;
        return e.getUuid().equalsIgnoreCase(uuid);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Event[acronym=%s name=%s year=%d]", getAcronym(), getFullName(), getYear());
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the series
     */
    public String getSeries() {
        return series;
    }

    /**
     * @param series the series to set
     */
    public void setSeries(String series) {
        this.series = series;
    }

    /**
     * @return the internationalName
     */
    public String getUniversalName() {
        return universalName;
    }

    /**
     * @param internationalName the internationalName to set
     */
    public void setUniversalName(String internationalName) {
        this.universalName = internationalName;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the shortName
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return the organizers
     */
    public List<Organization> getOrganizers() {
        return organizers;
    }

    /**
     * @param organizers the organizers to set
     */
    public void setOrganizers(List<Organization> organizers) {
        this.organizers = organizers;
    }

    /**
     * @return the uSState
     */
    public UsStateEnum getUsState() {
        return uSState;
    }

    /**
     * @param uSState the uSState to set
     */
    public void setUsState(UsStateEnum uSState) {
        this.uSState = uSState;
    }

    /**
     * @return the visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * @return the tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(String tags) {
        this.tags = tags;
    }

    /**
     * @return the type
     */
    public EventType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EventType type) {
        this.type = type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the registredDate
     */
    public Date getRegistredDate() {
        return registredDate;
    }

    /**
     * @param registredDate the registredDate to set
     */
    public void setRegistredDate(Date registredDate) {
        this.registredDate = registredDate;
    }

    /**
     * @return the canLogIn
     */
    public boolean isCanLogIn() {
        return canLogIn;
    }

    /**
     * @param canLogIn the canLogIn to set
     */
    public void setCanLogIn(boolean canLogIn) {
        this.canLogIn = canLogIn;
    }

    /**
     * @return the portraitLogoPrefix
     */
    public String getPortraitLogoPrefix() {
        return portraitLogoPrefix;
    }

    /**
     * @param portraitLogoPrefix the portraitLogoPrefix to set
     */
    public void setPortraitLogoPrefix(String portraitLogoPrefix) {
        this.portraitLogoPrefix = portraitLogoPrefix;
    }

    /**
     * @return the timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Corporation getOwner() {
        return owner;
    }

    public void setOwner(Corporation owner) {
        this.owner = owner;
    }
}
