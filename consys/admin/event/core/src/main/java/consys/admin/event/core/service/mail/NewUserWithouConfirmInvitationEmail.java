package consys.admin.event.core.service.mail;

import consys.admin.event.core.bo.Event;
import consys.admin.user.core.bo.User;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 * Notifikacia uzivatelovi ktora posuva nepodmienenu notifikaciu.
 * @author Palo
 */
public class NewUserWithouConfirmInvitationEmail extends AbstractSystemNotification {

    private String token;
    private String message;
    private User invited;
    private User invitor;
    private Event event;

    public NewUserWithouConfirmInvitationEmail(String token, User invited, User invitor, Event event, String message) {
        super(SystemMessage.INVITATION_NEW_USER_NO_CONFIRM, invited.getUuid());
        this.token = token;
        this.invited = invited;
        this.invitor = invitor;
        this.event = event;
        this.message = message;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        placeHolders.addPlaceHolder("salutation", invited.getName());
        placeHolders.addPlaceHolder("email", invited.getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("event_name", event.getFullName());
        placeHolders.addPlaceHolder("from", invitor.getName());
        placeHolders.addPlaceHolder("from_email", invitor.getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("token", token);
        placeHolders.addPlaceHolder("message", message);
    }
}
