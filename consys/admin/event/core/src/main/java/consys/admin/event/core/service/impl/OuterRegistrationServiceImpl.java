package consys.admin.event.core.service.impl;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.bo.thumb.BundleOrderRequest;
import consys.admin.event.core.service.EventService;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.event.core.service.OuterRegistrationService;
import consys.admin.event.core.service.image.UserImageService;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.ws.overseer.EventWebServiceEnum;
import consys.admin.ws.overseer.service.OverseerWebService;
import consys.common.core.exception.*;
import consys.common.core.service.impl.AbstractService;
import consys.event.common.ws.EventUserOrganization;
import consys.event.registration.ws.ParticipantAnswer;
import consys.event.registration.ws.ProductOrder;
import consys.event.registration.ws.RegisterPaymentProfile;
import consys.event.registration.ws.RegisterUserIntoEventRequest;
import consys.event.registration.ws.RegisterUserIntoEventResponse;
import consys.payment.service.exception.IbanFormatException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OuterRegistrationServiceImpl extends AbstractService implements OuterRegistrationService {

    @Autowired
    private OverseerWebService overseerWebService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserLoginInfoService userLoginInfoService;
    @Autowired
    private EventService eventService;
    @Autowired
    private EventUserOrganizationService eventUserOrganizationService;
    @Autowired
    private UserImageService userImageService;

    @Override
    public User createUserEventRegistration(UserLoginInfo newUserLoginInfo, String temporalAffiliation,
            String imageUrl, RegisterPaymentProfile paymentProfile, BundleOrderRequest mainBundle,
            List<BundleOrderRequest> bundles, String eventUuid, HashMap<String, String> answers)
            throws NoRecordException, UsernameExistsException, ServiceExecutionFailed, RequiredPropertyNullException, BundleCapacityFullException, CouponCapacityException, IbanFormatException, CouponCodeException, CouponOutOfDateException {
        // test na new userlogininfo
        if (StringUtils.isBlank(newUserLoginInfo.getPassword())) {
            throw new RequiredPropertyNullException("Missing user password");
        }

        if (mainBundle == null || StringUtils.isBlank(mainBundle.getBundleUuid()) || StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyNullException("Missing event or budnes");
        }

        // Vytvorime uzivatela
        if (newUserLoginInfo.isSsoUser()) {
            // test na to ci uz taky existuje ak ano tak len update informacii
            try {
                UserLoginInfo existing = loadUserLoginInfoFromSso(newUserLoginInfo);
                newUserLoginInfo = existing;
            } catch (NoRecordException e) {
                // novy uzivatel
                log().debug("Creating new SSO user: {}", newUserLoginInfo.getUser());
                newUserLoginInfo.setActive(true);
                userService.createRegistredUser(newUserLoginInfo, false, true);
                if (StringUtils.isNotBlank(imageUrl)) {
                    try {
                        String imageUuid = userImageService.createImageForUserWithoutUserUpdate(newUserLoginInfo.getUser().getUuid(), imageUrl);
                        newUserLoginInfo.getUser().setPortraitImageUuidPrefix(imageUuid);
                    } catch (MalformedURLException ex) {
                        log().error("Can't load image for {} from {}", newUserLoginInfo, imageUrl);
                    } catch (IOException ex) {
                        log().error("Can't load image for {} from {}", newUserLoginInfo, imageUrl);
                    }
                }
            }
        } else {
            // novy uzivatel
            log().debug("Creating new TAKEPLACE user: {}", newUserLoginInfo.getUser());
            userService.createRegistredUser(newUserLoginInfo);
        }

        // Vytvorime vztah medzi eventom a uzivatelom                               
        Event event = eventService.loadEventDetails(eventUuid);
        log().debug("Creating connection {} to {}", newUserLoginInfo.getUser(), event.getFullName());
        eventUserOrganizationService.updateEventUserOrganizationRelation(newUserLoginInfo.getUser(), event, EventUserOrganizationRelation.REGISTRED);

        // Posleme do overseera
        EventUserOrganization euo = new EventUserOrganization();
        euo.setFullName(newUserLoginInfo.getUser().getFullName());
        euo.setLastName(newUserLoginInfo.getUser().getLastName());
        euo.setUuid(newUserLoginInfo.getUser().getUuid());
        euo.setOrganization(temporalAffiliation);
        euo.setPotraitImgPrefixUuid(newUserLoginInfo.getUser().getPortraitImageUuidPrefix());

        // nastavime hlavny balik
        ProductOrder porder = new ProductOrder();
        porder.setUuid(mainBundle.getBundleUuid());
        porder.setDiscountCode(mainBundle.getDiscountCode());
        porder.setQuantity(mainBundle.getQuantity());

        // vytvorime seznam odpovedi
        List<ParticipantAnswer> participantAnswers = new ArrayList<ParticipantAnswer>();
        for (Entry<String, String> answer : answers.entrySet()) {
            ParticipantAnswer pa = new ParticipantAnswer();
            pa.setUuid(answer.getKey());
            pa.setAnswer(answer.getValue());
            participantAnswers.add(pa);
        }

        RegisterUserIntoEventRequest request = new RegisterUserIntoEventRequest();
        request.setEventUuid(eventUuid);
        request.setPaymentProfile(paymentProfile);
        request.setEventUserOrganization(euo);
        request.setUserEmail(newUserLoginInfo.getUser().getEmailOrMergeEmail());
        request.setMainBundle(porder);
        request.getAnswers().addAll(participantAnswers);

        // nastavime subbaliky
        if (bundles != null) {
            for (BundleOrderRequest order : bundles) {
                porder = new ProductOrder();
                porder.setUuid(order.getBundleUuid());
                porder.setDiscountCode(order.getDiscountCode());
                porder.setQuantity(order.getQuantity());
                request.getSubBundleOrders().add(porder);
            }
        }

        RegisterUserIntoEventResponse response = overseerWebService.sendAndReceive(EventWebServiceEnum.Registration, event.getOverseer().getWebServicesUrl(), request);
        if (!response.isResult()) {
            // musime spracovat vynimky
            switch (response.getException()) {
                case BUNDLE_CAPACITY_FULL_EXCEPTION:
                    throw new BundleCapacityFullException("Bundle has limited capacity and is already full");
                case COUPON_ALREADY_USED_EXCEPTION:
                    throw new CouponCapacityException("Coupon is oneTimeDiscount type and is already used in active registration.");
                case EVENT_NOT_ACTIVE_EXCEPTION:
                    throw new ServiceExecutionFailed("Event is not activated. If event is not activated then no registrations can be created!");
                case IBAN_FORMAT_EXCEPTION:
                    throw new IbanFormatException("Iban has bad format for selected country");
                case NO_RECORD_EXCEPTION:
                    throw new NoRecordException("NoRecord exception thrown in overseer. Maybe missing event, package");
                case REQUIRED_PROPERTY_EXCEPTION:
                    throw new RequiredPropertyNullException("NoRecord exception thrown in overseer. Maybe missing event, package");
                case COUPON_CODE_EXCEPTION:
                    throw new CouponCodeException("Given code doesn't exists!");
                case COUPON_EXPIRED_EXPIRED:
                    throw new CouponOutOfDateException(null);
                case SERVICE_EXECUTION_FAILED:
                default:
                    throw new ServiceExecutionFailed("Exception " + response.getException().toString());
            }
        }

        return userService.loadUserByUuid(newUserLoginInfo.getUser().getUuid());
    }

    private UserLoginInfo loadUserLoginInfoFromSso(UserLoginInfo uli) throws RequiredPropertyNullException, NoRecordException {
        UserLoginInfo info = null;
        boolean update = false;
        if (uli.getFacebookId() != null) {
            info = userLoginInfoService.loadUserLoginInfoByFacebookIdOrEmail(uli.getFacebookId(), uli.getUser().getEmailOrMergeEmail());
            if (info.getFacebookId() == null) {
                info.setFacebookId(uli.getFacebookId());
                update = true;
            }
        } else if (uli.getTwitterId() != null) {
            info = userLoginInfoService.loadUserLoginInfoByTwitterId(uli.getTwitterId());
        } else if (StringUtils.isNotBlank(uli.getGoogleId())) {
            info = userLoginInfoService.loadUserLoginInfoByGoogleId(uli.getGoogleId(), uli.getUser().getEmailOrMergeEmail());
            if (info.getGoogleId() == null) {
                info.setGoogleId(uli.getGoogleId());
                update = true;
            }
        } else if (StringUtils.isNotBlank(uli.getLinkedInId())) {
            info = userLoginInfoService.loadUserLoginInfoByLinkedInId(uli.getLinkedInId());
        } else {
            throw new NoRecordException();
        }

        if (update) {
            userLoginInfoService.updateUserLoginInfo(info);
        }
        return info;
    }
}
