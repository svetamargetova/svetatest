package consys.admin.event.core.service.impl;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.bo.thumb.SelectedEvent;
import consys.admin.event.core.dao.EventDao;
import consys.admin.event.core.dao.EventUserOrganizationDao;
import consys.admin.event.core.service.EventInvitationService;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.UserSystemProperties;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.core.service.mail.RepeatingActivationEmail;
import consys.admin.ws.overseer.EventWebServiceEnum;
import consys.admin.ws.overseer.service.OverseerWebService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.event.user.ws.UpdateUserDetailRequest;
import consys.event.user.ws.UpdateUserOrganizationRequest;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Palo
 */
public class EventUserOrganizationServiceImpl extends AbstractService implements EventUserOrganizationService {
    // Event module services

    private EventDao eventDao;
    private EventUserOrganizationDao eventUserOrganizationDao;
    private EventInvitationService eventInvitationService;
    // User module services
    private UserOrganizationService userOrganizationService;
    private UserLoginInfoService userLoginInfoService;
    private UserOrganizationService organizationService;
    private UserService userService;
    // Universal services
    private OverseerWebService overseerWebService;
    private SystemPropertyService systemPropertyService;

    /* ------------------------------------------------------------------------ */
    /* ---- U P D A T E ---- */
    /* ------------------------------------------------------------------------ */
    @Override
    public void activateInvitedUser(String firstName, String lastName, String password, String key)
            throws
            RequiredPropertyNullException,
            ServiceExecutionFailed,
            ActivationKeyException {
        // najskor vykoname update a potom rozosleme info do vsetkych eventov.
        User user = userService.activateInvitedUser(firstName, lastName, password, key);
        updateUserInfoOnOverseers(user);
    }

    @Override
    public void updateEventUserOrganizationRelation(List<String> userUuids, String eventUuid, String message, EventUserOrganizationRelation newRelation) throws NoRecordException, RequiredPropertyNullException {
        List<User> users = userService.listUsersForUuids(userUuids);
        Event event = eventDao.loadByUuid(eventUuid);
        updateEventUserOrganizationRelation(users, event, message, newRelation);
    }

    @Override
    public void updateEventUserOrganizationRelation(List<User> user, Event event, String message, EventUserOrganizationRelation newRelation) throws NoRecordException, RequiredPropertyNullException {
        for (User user1 : user) {
            updateEventUserOrganizationRelation(user1, event, message, newRelation);
        }
    }

    @Override
    public void updateEventUserOrganizationRelation(String userUuid, String eventUuid, String message, EventUserOrganizationRelation newRelation) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyNullException();
        }
        Event event = eventDao.loadByUuid(eventUuid);
        User u = userService.loadUserByUuid(userUuid);
        updateEventUserOrganizationRelation(u, event, message, newRelation);
    }

    @Override
    public void updateEventUserOrganizationRelation(User user, Event event, String message, EventUserOrganizationRelation newRelation) throws NoRecordException, RequiredPropertyNullException {
        updateEventUserOrganizationRelation(user, event, newRelation);
        // TODO: poslat message

    }

    @Override
    public void updateEventUserOrganizationRelation(User user, Event event, EventUserOrganizationRelation newRelation)
            throws NoRecordException, RequiredPropertyNullException {
        if (user == null || event == null || newRelation == null) {
            throw new RequiredPropertyNullException();
        }

        try {
            EventUserOrganization euo = eventUserOrganizationDao.loadEventUserOrganizationByEventUuid(event.getUuid(), user.getId());

            if (!euo.getRelation().equals(EventUserOrganizationRelation.OWNER)) {
                log().debug("Updating user event relation {}", euo);
                euo.setRelation(newRelation);
                eventUserOrganizationDao.update(euo);
            }
        } catch (NoRecordException ex) {
            EventUserOrganization euo = new EventUserOrganization();
            euo.setEvent(event);
            euo.setUuid(UuidProvider.getUuid());
            euo.setLastVisit(DateProvider.getCurrentDate());
            euo.setRelation(newRelation);
            euo.setUo(getUserOrganizationService().loadDefaultUserOrganizationByUserId(user.getId()));
            log().debug("Creating user event relation {}", euo);

            eventUserOrganizationDao.create(euo);
        }

    }

    /**
     * Servica nacita userOrgEvent pre daneho userid a eventid. V pripade ze ne-
     * existuje tak ho vytvori s priznakom VISITOR.
     */
    @Override
    public SelectedEvent updateAndLoadSelectedEventByEventUuid(String eventUuid, Long userId)
            throws ServiceExecutionFailed, NoRecordException, RequiredPropertyNullException {

        if (StringUtils.isBlank(eventUuid) || userId == null || userId <= 0) {
            throw new RequiredPropertyNullException();
        }

        SelectedEvent selectedEvent = new SelectedEvent();
        Event event = eventDao.loadByUuid(eventUuid);
        selectedEvent.setEvent(event);
        EventUserOrganization euo;
        try {
            euo = eventUserOrganizationDao.loadEventUserOrganizationByEventUuid(eventUuid, userId);

            log().debug("Last visit :", euo.getLastVisit());

            euo.setLastVisit(DateProvider.getCurrentDate());
            eventUserOrganizationDao.update(euo);
            selectedEvent.setRelation(euo.getRelation());
        } catch (NoRecordException nre) {

            log().debug("New visitor in: {} ", event);

            euo = new EventUserOrganization();
            euo.setEvent(event);
            euo.setUuid(UuidProvider.getUuid());
            euo.setLastVisit(DateProvider.getCurrentDate());
            euo.setRelation(EventUserOrganizationRelation.VISITOR);
            euo.setUo(getUserOrganizationService().loadDefaultUserOrganizationByUserId(userId));
            selectedEvent.setRelation(EventUserOrganizationRelation.VISITOR);
            eventUserOrganizationDao.create(euo);
        }

        return selectedEvent;
    }

    @Override
    public void updateUser(User u) throws ServiceExecutionFailed, RequiredPropertyNullException {
        log().debug("Update: {}", u);
        getUserService().updateUser(u);

        // Po update informujeme overseerov o zmene        
        updateUserInfoOnOverseers(u);

    }

    private void updateUserInfoOnOverseers(User u) {
        UpdateUserDetailRequest request = new UpdateUserDetailRequest();
        request.setFullName(u.getFullName());
        request.setUuid(u.getUuid());
        request.setLastName(u.getLastName());
        request.setCeliac(u.isCeliac());
        request.setVegetarian(u.isVegetarian());
        request.setEmail(u.getEmailOrMergeEmail());
        request.setProfileImagePrefix(u.getPortraitImageUuidPrefix());

        Map<String, List<String>> usersEvents = getEventUserOrganizationDao().listOverseersForUpdateUserDetails(u.getId());
        for (Map.Entry<String, List<String>> events : usersEvents.entrySet()) {
            try {

                log().debug("Actualizing user details in event: {} ", events.getKey());

                for (String event : events.getValue()) {
                    request.getEvents().add(event);
                }
                getOverseerWebService().sendAndReceive(EventWebServiceEnum.User, events.getKey(), request);
            } catch (Exception e) {
                log().error("Update User details in overseer " + events.getKey() + " failed!", e);
            }
        }
    }

    @Override
    public void updateUserOrganization(UserOrganization uo, boolean preferred) throws ServiceExecutionFailed, RequiredPropertyNullException {
        if (uo == null || StringUtils.isBlank(uo.getUuid())) {
            throw new RequiredPropertyNullException();
        }
        if (log().isDebugEnabled()) {
            log().debug("Updating UserOrganization on Administration");
        }
        organizationService.updateUserOrganization(uo, preferred);
        // Ak vsetko prebehlo ok tak aktualizujeme vsetky eventy na zmenu pozicie.
        // Po update informujeme overseerov o zmene
        if (log().isDebugEnabled()) {
            log().debug("Updating UserOrganization on Overseers : " + uo);
        }
        updateUserOrganizationOnOverseers(uo);
    }

    private void updateUserOrganizationOnOverseers(UserOrganization uo) {
        UpdateUserOrganizationRequest request = new UpdateUserOrganizationRequest();
        request.setAffiliation(uo.getShortInfo());
        request.setUuid(uo.getUser().getUuid());

        Map<String, List<String>> usersEvents = getEventUserOrganizationDao().listOverseersForUpdateOrganization(uo.getId());
        for (Map.Entry<String, List<String>> events : usersEvents.entrySet()) {
            try {
                if (log().isDebugEnabled()) {
                    log().debug("Actualizing user organization to overseer " + events.getKey());
                }
                for (String event : events.getValue()) {
                    request.getEvents().add(event);
                }
                getOverseerWebService().sendAndReceive(EventWebServiceEnum.User, events.getKey(), request);
            } catch (Exception e) {
                log().error("Update User organization in overseer " + events.getKey() + " failed!", e);
            }
        }
    }

    /**
     * Servica ktora vygeneruje upozornenia pre uzivatelov ktory sa este nekati-
     * vovali. Poziadavka na aktivaciu sa autoamticky preposiela pokial nepre-
     * krocila systemovu property
     * <code>REP_ACT_EMAIL_MAX_DAYS</code>. Ak ano
     * na email je poslana sprava ze jeho aktivacia sa stala neaktivna a zmaze
     * sa cely ucet.
     */
    @Override
    public void generateActivationManagement() {
        final int dateInterval = getSystemPropertyService().getInteger(UserSystemProperties.REP_ACT_EMAIL_DATE_INTERVAL);
        final int maxdays = getSystemPropertyService().getInteger(UserSystemProperties.REP_ACT_EMAIL_MAX_DAYS);
        if (dateInterval == 0 || maxdays == 0) {
            log().error("System Properties for automatic activation management not set!");
            return;
        }

        Calendar olderThanCal = GregorianCalendar.getInstance();
        DateProvider.substractDays(olderThanCal, dateInterval);
        Calendar maxDaysCal = GregorianCalendar.getInstance();
        DateProvider.substractDays(maxDaysCal, maxdays);

        log().info("Starting automatic activation management");
        log().info(" ... notification older than: " + olderThanCal.getTime().toString());
        log().info(" ... delete       older than: " + maxDaysCal.getTime().toString());

        List<UserActivation> activations = userLoginInfoService.listOlderActivationsThan(olderThanCal.getTime());
        Calendar registrationCal = GregorianCalendar.getInstance();

        for (UserActivation ua : activations) {
            registrationCal.setTime(ua.getLoginInfo().getRegisterDate());
            UserLoginInfo loginInfo = ua.getLoginInfo();

            if (!ua.isInvitation() && maxDaysCal.after(registrationCal)) {
                // neni zvany a vyprsel mu cas -> smazat
                try {
                    deleteOldUserRegistrations(ua);
                } catch (Exception ex) {
                    log().error("Can't delete activation out of date interval" + ua.getLoginInfo(), ex);
                }
            } else {
                if (ua.isInvitation() && maxDaysCal.after(registrationCal)) {
                    // TODO: co udelat se zvanym, ktery se neaktivoval? zatim posilame do nekonecna
                    log().warn("Can't delete user activation now - INVITED - not yet specified" + ua.getLoginInfo());
                }

                int dayPast = DateProvider.daysBetween(GregorianCalendar.getInstance(), registrationCal);
                if (dayPast % dateInterval == 0) {
                    RepeatingActivationEmail email = new RepeatingActivationEmail(loginInfo, ua);
                    email.sendNotification();
                }
            }
        }
    }

    /* ------------------------------------------------------------------------ */
    /* ---- L I S T ---- */
    /* ------------------------------------------------------------------------ */
    @Override
    public Map<EventUserOrganizationRelation, List<EventUserOrganization>> listEventUserOrganizations(String userUuid) throws ServiceExecutionFailed {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<UserOrganization> listEventUserOrganizations(List<String> uuids, String eventUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(eventUuid) || uuids.isEmpty()) {
            throw new RequiredPropertyNullException();
        }
        return eventUserOrganizationDao.listUserOrganizationForEvent(uuids, eventUuid);
    }

    /* ------------------------------------------------------------------------ */
    /* ---- L O A D ---- */
    /* ------------------------------------------------------------------------ */
    @Override
    public UserOrganization loadUserOrganizationWithUserForEvent(String userUuid, String eventUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(eventUuid) || StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return eventUserOrganizationDao.loadUserOrganizationForEvent(userUuid, eventUuid);
    }

    /* ------------------------------------------------------------------------ */
    /* ---- U T I L S ---- */
    /* ------------------------------------------------------------------------ */
    private void deleteOldUserRegistrations(UserActivation ua) throws NoRecordException, RequiredPropertyNullException {
        if (ua.isInvitation()) {
            // delete invitations

//            eventInvitationService.deleteAllInvitations(ua.getLoginInfo().getUser());
            return;
        }
        userLoginInfoService.deleteNotActivetedUser(ua);
    }

    /* ======================================================================== */
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    public EventDao getEventDao() {
        return eventDao;
    }

    /**
     * @return the eventUserOrganizationDao
     */
    public EventUserOrganizationDao getEventUserOrganizationDao() {
        return eventUserOrganizationDao;
    }

    /**
     * @param eventUserOrganizationDao the eventUserOrganizationDao to set
     */
    public void setEventUserOrganizationDao(EventUserOrganizationDao eventUserOrganizationDao) {
        this.eventUserOrganizationDao = eventUserOrganizationDao;
    }

    public OverseerWebService getOverseerWebService() {
        return overseerWebService;
    }

    public void setOverseerWebService(OverseerWebService overseerWebService) {
        this.overseerWebService = overseerWebService;
    }

    /**
     * @return the organizationService
     */
    public UserOrganizationService getOrganizationService() {
        return organizationService;
    }

    /**
     * @param organizationService the organizationService to set
     */
    public void setOrganizationService(UserOrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the userOrganizationService
     */
    public UserOrganizationService getUserOrganizationService() {
        return userOrganizationService;
    }

    /**
     * @param userOrganizationService the userOrganizationService to set
     */
    public void setUserOrganizationService(UserOrganizationService userOrganizationService) {
        this.userOrganizationService = userOrganizationService;
    }

    /**
     * @return the systemPropertyService
     */
    public SystemPropertyService getSystemPropertyService() {
        return systemPropertyService;
    }

    /**
     * @param systemPropertyService the systemPropertyService to set
     */
    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    /**
     * @return the userLoginInfoService
     */
    public UserLoginInfoService getUserLoginInfoService() {
        return userLoginInfoService;
    }

    /**
     * @param userLoginInfoService the userLoginInfoService to set
     */
    public void setUserLoginInfoService(UserLoginInfoService userLoginInfoService) {
        this.userLoginInfoService = userLoginInfoService;
    }

    /**
     * @return the eventInvitationService
     */
    public EventInvitationService getEventInvitationService() {
        return eventInvitationService;
    }

    /**
     * @param eventInvitationService the eventInvitationService to set
     */
    public void setEventInvitationService(EventInvitationService eventInvitationService) {
        this.eventInvitationService = eventInvitationService;
    }
}
