package consys.admin.event.core.bo.thumb;

/**
 * Objekt ktory uchovava poziadavku v objednavke
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class BundleOrderRequest {

    // data
    private String bundleUuid;
    private String discountCode;
    private int quantity;

    public BundleOrderRequest(String bundleUuid, String discountCode, int quantity) {
        this.bundleUuid = bundleUuid;
        this.discountCode = discountCode;
        this.quantity = quantity;
    }

    public BundleOrderRequest(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }
}
