package consys.admin.event.core.service.mail;

import consys.admin.event.core.bo.Invitation;
import consys.admin.user.core.bo.UserActivation;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import net.notx.client.PlaceHolders;

/**
 * Notifikacia o pozvanke noveho uzivatela s potvrdenim pozvanky
 * @author Palo
 */
public class NewUserConfirmInvitationEmail extends AbstractSystemNotification {

    private Invitation invitation;
    private UserActivation activation;
    private String message;

    public NewUserConfirmInvitationEmail(Invitation invitation, UserActivation activation, String message) {
        super(SystemMessage.INVITATION_NEW_USER_WITH_CONFIRM, invitation.getTo().getUuid());
        this.invitation = invitation;
        this.message = message;
        this.activation = activation;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        placeHolders.addPlaceHolder("salutation", invitation.getTo().getName());
        placeHolders.addPlaceHolder("email",invitation.getTo().getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("event_name",invitation.getEvent().getFullName());
        placeHolders.addPlaceHolder("from",invitation.getFrom().getName());
        placeHolders.addPlaceHolder("from_email",invitation.getFrom().getEmailOrMergeEmail());
        placeHolders.addPlaceHolder("token",activation.getKey());
        placeHolders.addPlaceHolder("message",message);
    }
}
