/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.event.core.bo.Invitation;
import consys.admin.event.core.bo.InvitationStateEnum;
import consys.admin.event.core.bo.thumb.EventInvitationThumb;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.event.core.dao.InvitationDao;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author palo
 */
public class InvitationDaoImpl extends GenericDaoImpl<Invitation> implements InvitationDao {

    @Override
    public Invitation load(Long id) throws NoRecordException {
        return load(id, Invitation.class);
    }

    @Override
    public Invitation loadWithTargetEventAndUserByUuid(String uuid) throws NoRecordException {
        Invitation i = (Invitation) session().
                createQuery("select i from Invitation i JOIN FETCH i.event JOIN FETCH i.to JOIN FETCH i.from where i.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (i == null) {
            throw new NoRecordException("No invitation with UUID " + uuid);
        }
        return i;
    }

    @Override
    public Invitation loadByUuid(String uuid) throws NoRecordException {
        Invitation i = (Invitation) session().
                createQuery("from Invitation i where i.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (i == null) {
            throw new NoRecordException("No invitation with UUID " + uuid);
        }
        return i;
    }

    @Override
    public List<Invitation> listUserActiveInvitations(String userUuid) throws NoRecordException {
        List<Invitation> i = session().
                createQuery("from Invitation i where i.to.uuid=:UUID and i.state=1 order by i.invitationDate").
                setString("UUID", userUuid).
                list();
        if (i == null || i.isEmpty()) {
            throw new NoRecordException("There are no active invitations for user with uuid " + userUuid);
        }
        return i;
    }

    @Override
    public List<Invitation> listUserAllInvivations(String userUuid) throws NoRecordException {
        List<Invitation> i = session().
                createQuery("from Invitation i where i.to.uuid=:UUID order by i.invitationDate").
                setString("UUID", userUuid).
                list();
        if (i == null || i.isEmpty()) {
            throw new NoRecordException("There are no invitations for user with uuid " + userUuid);
        }
        return i;
    }

    @Override
    public List<EventInvitationThumb> listUserActiveInvitations(String userUuid, int from, int limit) throws NoRecordException {
        Query q = session().
                createQuery("select e.uuid, e.web, e.fromDate, e.toDate, e.fullName, e.imageLogoPrefix, e.year,e.acronym,i.state,i.uuid from Invitation i join i.event as e where i.to.uuid=:UUID and i.state=1 order by i.invitationDate").
                setString("UUID", userUuid);
        q.setFirstResult(from);
        q.setMaxResults(limit);
        List<EventInvitationThumb> thumbs = Lists.newArrayList();
        List<Object[]> invitations = q.list();
        for (Object[] o : invitations) {
            EventInvitationThumb ithumb = new EventInvitationThumb();
            EventThumb thumb = new EventThumb();
            thumb.setUuid((String) o[0]);
            thumb.setWeb((String) o[1]);
            thumb.setFrom((Date) o[2]);
            thumb.setTo((Date) o[3]);
            thumb.setFullName((String) o[4]);
            thumb.setLogoUuid((String) o[5]);
            thumb.setYear((Integer) o[6]);
            thumb.setAcronym((String) o[7]);
            ithumb.setEvent(thumb);
            ithumb.setState((InvitationStateEnum) o[8]);
            ithumb.setUuid((String) o[9]);
            thumbs.add(ithumb);
        }
        return thumbs;
    }

    @Override
    public List<EventInvitationThumb> listUserInvitations(String userUuid, int from, int limit) throws NoRecordException {
        Query q = session().
                createQuery("select e.uuid, e.web, e.fromDate, e.toDate, e.fullName, e.imageLogoPrefix, e.year,e.acronym,i.state,i.uuid from Invitation i join i.event as e where i.to.uuid=:UUID order by i.invitationDate").
                setString("UUID", userUuid);
        q.setFirstResult(from);
        q.setMaxResults(limit);
        List<EventInvitationThumb> thumbs = Lists.newArrayList();
        List<Object[]> invitations = q.list();
        for (Object[] o : invitations) {
            EventInvitationThumb ithumb = new EventInvitationThumb();
            EventThumb thumb = new EventThumb();
            thumb.setUuid((String) o[0]);
            thumb.setWeb((String) o[1]);
            thumb.setFrom((Date) o[2]);
            thumb.setTo((Date) o[3]);
            thumb.setFullName((String) o[4]);
            thumb.setLogoUuid((String) o[5]);
            thumb.setYear((Integer) o[6]);
            thumb.setAcronym((String) o[7]);
            ithumb.setEvent(thumb);
            ithumb.setState((InvitationStateEnum) o[8]);
            ithumb.setUuid((String) o[9]);
            thumbs.add(ithumb);
        }
        return thumbs;
    }
}
