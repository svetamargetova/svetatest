package consys.admin.event.core.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public enum EventType {
    CONFERENCE(0),BROKERAGE(1);

    private static final Logger logger = LoggerFactory.getLogger(EventType.class);

    private Integer id;

    private EventType(Integer id) {
        this.id = id;
    }

    public static EventType fromId(Integer i){
        switch(i){
            case 0: return CONFERENCE;
            case 1: return BROKERAGE;
        }
        logger.error("FromInt failed! Given id "+i+"has no enum member!");
        throw new RuntimeException();
    }

    public Integer getId(){
        return id;
    }

}
