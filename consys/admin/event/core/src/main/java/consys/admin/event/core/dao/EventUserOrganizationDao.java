package consys.admin.event.core.dao;

import consys.admin.event.core.bo.EventUserOrganization;
import consys.admin.event.core.bo.thumb.EventUserOrganizationThumb;
import consys.admin.user.core.bo.UserOrganization;
import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author palo
 */
public interface EventUserOrganizationDao extends GenericDao<EventUserOrganization> {

    /**
     * Nacita EventUserOrganization pre dany event uuid a ID uzivatela
     */
    public EventUserOrganization loadEventUserOrganizationByEventUuid(String eventUuid, Long userId)
            throws NoRecordException;

    /**
     * Nacita EventUserOrganization pre dany event uuid a uuid uzivatela
     */
    public EventUserOrganization loadEventUserOrganizationByEventUuid(String eventUuid, String userUuid)
            throws NoRecordException;

    public List<UserOrganization> listUserOrganizationForEvent(List<String> userUuids, String eventUuid) throws NoRecordException;

    public UserOrganization loadUserOrganizationForEvent(String userUuids, String eventUuid) throws NoRecordException;

    /**
     * Metoda vracia ako kluc nazov overseera a k nemu objekt ucghovavajuci uuid
     * eventu a nazov organizacie ktora je vo vztahu s user<->event
     */
    public Map<String, List<EventUserOrganizationThumb>> listOverseersWithEventsForUser(Long userId);

    public Map<String, List<String>> listOverseersForUpdateUserDetails(Long userId);

    public Map<String, List<String>> listOverseersForUpdateOrganization(Long userOrganizationId);

    public List<EventUserOrganization> listEventUserOrganizationsWithUser(Long id);

    List<EventUserOrganization> listEventUserOrganizationAdminsForEvent(Long eventId);

    public void deleteEventUserOrganizationsForEvent(Long eventId);
}
