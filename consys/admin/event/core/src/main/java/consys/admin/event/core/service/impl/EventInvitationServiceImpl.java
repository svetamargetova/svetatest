/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.core.service.impl;

import consys.admin.event.core.bo.Event;

import consys.admin.event.core.bo.Invitation;
import consys.admin.event.core.bo.InvitationStateEnum;
import consys.admin.event.core.bo.thumb.EventInvitationThumb;
import consys.admin.event.core.bo.thumb.UserInvitationRequest;
import consys.admin.event.core.dao.InvitationDao;
import consys.admin.event.core.service.EventInvitationService;
import consys.admin.event.core.service.EventService;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.event.core.service.exception.InvitationAlreadyResolved;
import consys.admin.event.core.service.mail.EventInvitationAcceptedToInvited;
import consys.admin.event.core.service.mail.EventInvitationAcceptedToInvitor;
import consys.admin.event.core.service.mail.EventInvitationDeclined;
import consys.admin.event.core.service.mail.NewUserConfirmInvitationEmail;
import consys.admin.event.core.service.mail.NewUserWithouConfirmInvitationEmail;
import consys.admin.event.core.service.mail.UserConfirmInvitationEmail;
import consys.admin.event.core.service.mail.UserWithoutConfirmInvitationEmail;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserActivation;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.ActivationUtils;
import consys.admin.user.core.util.UserUtils;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.service.MessagingService;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.date.DateProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.common.utils.UuidProvider;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author palo
 */
public class EventInvitationServiceImpl extends AbstractService implements EventInvitationService {

    public static final String NAME_SEPARATOR = " ";
    private UserService userService;
    private UserLoginInfoService loginInfoService;
    private InvitationDao invitationDao;
    private EventService eventService;        
    private EventUserOrganizationService eventUserOrganizationService;

    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Na zaklade requestu nacita uzivatela alebo ho vytvori. V pripade ze priznak
     * inviteWithConfirm == true tak sa vytvori objekt Invitation a posle sa uziva
     * telovi o tom informacia.
     * Ak je false tak sa automaticky vytvori ziadana vazba medzi eventom a
     * uzivatelom.
     *
     * Notifikacie su:
     * existujuci pouzivatel + invitationConfirm
     * 
     * novy uzivatel
     * novy uzivatel + invitaiton confirm
     *
     */
    @Override
    public User createUserInvivation(UserInvitationRequest req)
            throws NoRecordException, RequiredPropertyNullException {
        List<User> us = createUsersInvivations(Lists.newArrayList(req));
        return us.get(0);
    }

    /**
     * Spracovanie zoznamu pozvanok do eventu. Detail vyssie
     */
    @Override
    public List<User> createUsersInvivations(List<UserInvitationRequest> reqs)
            throws NoRecordException, RequiredPropertyNullException {
        // emaile ktore sa nakoniec poslu ak vsetko OK
        List<AbstractSystemNotification> mailsToSend = Lists.newArrayList();
        // vysledny uzivatelia
        List<User> users = Lists.newArrayList();
        // lokalna cache eventu
        Map<String, Event> eventLocalCache = Maps.newHashMap();
        // lokalna cache nacitanych uzivatelu
        Map<String, User> usersLocalCache = Maps.newHashMap();


        for (UserInvitationRequest req : reqs) {
            if (req.isExistingUser()) {
                users.add(processExistingUser(req, usersLocalCache, eventLocalCache, mailsToSend));
            } else {
                users.add(processNewUser(req, usersLocalCache, eventLocalCache, mailsToSend));
            }
        }
        // posleme vsetky emaile
        for (AbstractSystemNotification mail : mailsToSend) {
            mail.sendNotification();
        }
        return users;
    }

    /*------------------------------------------------------------------------*/
    /* ---- U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateInvitation(InvitationStateEnum stateEnum, String invitationUuid, String ownerUuid)
            throws NoRecordException, RequiredPropertyNullException, InvitationAlreadyResolved {

        if (StringUtils.isBlank(invitationUuid) || StringUtils.isBlank(ownerUuid)) {
            throw new RequiredPropertyNullException();
        }

        Invitation i = invitationDao.loadWithTargetEventAndUserByUuid(invitationUuid);
        // ked to je uz resolvovane alebo neni spravny stav tak jakeby byla resolvovana
        if (i.getResponseDate() != null || stateEnum.equals(InvitationStateEnum.NO_RESPONSE)) {
            throw new InvitationAlreadyResolved();
        }
        // Ak to neresolvuje vlastnik tak jakeby neexistovala
        if (!i.getTo().getUuid().equalsIgnoreCase(ownerUuid)) {
            throw new NoRecordException();
        }

        i.setResponseDate(DateProvider.getCurrentDate());
        i.setState(stateEnum);
        switch (stateEnum) {
            case ACCEPTED: {
                // posle sa info o accept a vytvori sa relacia
                getEventUserOrganizationService().updateEventUserOrganizationRelation(i.getTo(), i.getEvent(), null, i.getRelation());
                // EMAIL
                EventInvitationAcceptedToInvited mailToInvited = new EventInvitationAcceptedToInvited(i);
                EventInvitationAcceptedToInvitor mailToInvitor = new EventInvitationAcceptedToInvitor(i);
                mailToInvited.sendNotification();
                mailToInvitor.sendNotification();
                break;
            }
            case DECLINED: {
                // posle sa info o declined
                EventInvitationDeclined mail = new EventInvitationDeclined(i);
                mail.sendNotification();
                break;

            }
            case TIMEOUTED: {
                // posle sa info o timeout ??                
                break;
            }
            default:
                return;
        }
        invitationDao.update(i);
    }

    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita pozvanku s dotiahnutymi uzivatelmi a eventom.
     * @throws NoRecordException ak pre uuid a userUuid neexistuje pozvanka
     * @throws RequiredPropertyNullException ak nejaky vstup je blank
     */
    @Override
    public Invitation loadDetailedInvitation(String uuid, String userUuid)
            throws NoRecordException,
            RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid) || StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        Invitation i = invitationDao.loadWithTargetEventAndUserByUuid(uuid);
        if (!i.getTo().getUuid().equalsIgnoreCase(userUuid)) {
            throw new NoRecordException();
        }
        return i;

    }
    /*------------------------------------------------------------------------*/
    /* ----  L I S T  ---- */
    /*------------------------------------------------------------------------*/

    @Override
    public List<Invitation> listUserActiveInvitations(String userUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return invitationDao.listUserActiveInvitations(userUuid);
    }

    @Override
    public List<Invitation> listUserInvitations(String userUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return invitationDao.listUserAllInvivations(userUuid);
    }

    /**
     * Nacita uzivatelove <strong>aktivne</strong> pozvanky. Tj. v stave
     * NO_RESPONSE. Zoradene podla datumu pozvanky.
     */
    @Override
    public List<EventInvitationThumb> listUserActiveInvitations(String userUuid, int from, int limit)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid) || from < 0 || limit < 1) {
            throw new RequiredPropertyNullException();
        }
        return invitationDao.listUserActiveInvitations(userUuid, from, limit);

    }

    /**
     * Nacita uzivatelove pozvanky. Zoradene podla datumu pozvanky.
     */
    @Override
    public List<EventInvitationThumb> listUserInvitations(String userUuid, int from, int limit)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid) || from < 0 || limit < 1) {
            throw new RequiredPropertyNullException();
        }
        return invitationDao.listUserInvitations(userUuid, from, limit);

    }

    /*------------------------------------------------------------------------*/
    /* ----  D E L E T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Odstrani vsetky pozvanky ktore su adresovane uzivatelovi. Posle sa email
     * odosielatelovy o tejto skutocnosti ze z dovodu neaktivyti bol uzivatel
     * odstraneny.
     *
     * @param toUserInvitation
     * @throws NoRecordException
     */
    @Override
    public void deleteAllInvitations(User toUserInvitation) throws NoRecordException{
        List<Invitation> invitations = invitationDao.listUserAllInvivations(toUserInvitation.getUuid());
        if(!invitations.isEmpty()){
            for (Invitation invitation : invitations) {
                if(log().isDebugEnabled()){
                    log().debug("Delete invitation "+invitation);
                }
                invitationDao.delete(invitation);
            }
        }
    }

    /*------------------------------------------------------------------------*/
    /* ----  H E L P     M E T H O D S  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Pomocna metoda spracuje request pre existujuceho uzivatela. Na zaklade toho
     * ci sa jedna o pozvanku s potvrdenim vytvori pozvanku alebo priamo nastavi
     * relaciu.
     *
     * @param eventLocalCache  lokalna cache eventov v ramci volania
     * @param mailsToSend zoznam emailov ktore sa maju na konci spracovania poslat
     * @param req konkretny request
     * @param usersLocalCache  lokalna cache uz nacitanych uzivatelov
     *
     * @return uzivatel
     */
    private User processExistingUser(UserInvitationRequest req, Map<String, User> usersLocalCache, Map<String, Event> eventLocalCache, List<AbstractSystemNotification> mailsToSend)
            throws RequiredPropertyNullException, NoRecordException {
        User invited = findOrLoadUser(req.getUuid(), usersLocalCache);
        User invitor = findOrLoadUser(req.getFrom(), usersLocalCache);
        Event event = findOrLoadEvent(req.getEvent(), eventLocalCache);

        if (req.isWithInvitationConfirm()) {
            
            Invitation invitation = createInvitation(invitor, invited, event, req.getMessage(), req.getRelation());
            // poslani notifikacie na email
            UserConfirmInvitationEmail email = new UserConfirmInvitationEmail(invitation, req.getMessage());
            mailsToSend.add(email);
        } else {
            if (req.getRelation() != null) {
                eventUserOrganizationService.updateEventUserOrganizationRelation(invited, event, req.getRelation());
                UserWithoutConfirmInvitationEmail email = new UserWithoutConfirmInvitationEmail(invited,invitor,event,req.getMessage());
                mailsToSend.add(email);
            }
        }
        return invited;
    }

    /**
     * Pomocna metoda spracuje request pre noveho uzivatela. Najskor overi ci sa
     * uz uzivatel nenachadza pod rovnakym emailom v takeplace. Ak nie vytvori
     * uzivatela a spracuje pozvanku a nastavy email.
     *
     * @param eventLocalCache  lokalna cache eventov v ramci volania
     * @param mailsToSend zoznam emailov ktore sa maju na konci spracovania poslat
     * @param req konkretny request
     * @param usersLocalCache  lokalna cache uz nacitanych uzivatelov
     *
     * @return novy vytovreny uzivatel
     */
    private User processNewUser(UserInvitationRequest req, Map<String, User> usersLocalCache, Map<String, Event> eventLocalCache, List<AbstractSystemNotification> mailsToSend)
            throws RequiredPropertyNullException, NoRecordException {
        try {
            String existingUserUuid = checkUserExists(req);
            req.setUuid(existingUserUuid);
            return processExistingUser(req, usersLocalCache, eventLocalCache, mailsToSend);
        } catch (NoRecordException e) {
            UserActivation activation = createNewUser(req.getEmail(), req.getName());
            Event event = findOrLoadEvent(req.getEvent(), eventLocalCache);
            User invitor = findOrLoadUser(req.getFrom(), usersLocalCache);

            /* Pozvanka s potvrdenim pre noveho pouzivatela */
            if (req.isWithInvitationConfirm()) {                
                Invitation invitation = createInvitation(invitor, activation.getLoginInfo().getUser(), event, req.getMessage(), req.getRelation());
                NewUserConfirmInvitationEmail email = new NewUserConfirmInvitationEmail(invitation, activation, req.getMessage());
                mailsToSend.add(email);
            } else {
                if (req.getRelation() != null) {
                    /* Pozvanka s potvrdenim pre noveho pouzivatela */
                    eventUserOrganizationService.updateEventUserOrganizationRelation(activation.getLoginInfo().getUser(), event, req.getRelation());
                    NewUserWithouConfirmInvitationEmail email = new NewUserWithouConfirmInvitationEmail(activation.getKey(),activation.getLoginInfo().getUser(),invitor,event,req.getEmail());
                    mailsToSend.add(email);
                }
            }
            return activation.getLoginInfo().getUser();
        }
    }

    private String checkUserExists(UserInvitationRequest req) throws RequiredPropertyNullException, NoRecordException {
        String uuid = getUserService().loadUserUuidByUsername(req.getEmail());
        log().debug("Invite new user => existing user: username {}  already registred",req.getEmail());        
        return uuid;
    }

    private Event findOrLoadEvent(String eventUuid, Map<String, Event> localCache) throws NoRecordException, RequiredPropertyNullException {
        Event event = localCache.get(eventUuid);
        if (event == null) {
            event = eventService.loadEvent(eventUuid);
            localCache.put(eventUuid, event);
        }
        return event;
    }

    private User findOrLoadUser(String uuid, Map<String, User> localCache) throws RequiredPropertyNullException, NoRecordException {
        User user = localCache.get(uuid);
        if (user == null) {
            user = userService.loadUserByUuid(uuid);
            localCache.put(uuid, user);
        }
        return user;
    }

    private Invitation createInvitation(User from, User to, Event event, String message, EventUserOrganizationRelation rel) {
        Invitation invitation = new Invitation();
        invitation.setEvent(event);
        invitation.setFrom(from);
        invitation.setTo(to);
        invitation.setInvitationDate(DateProvider.getCurrentDate());
        invitation.setUuid(UuidProvider.getUuid());
        invitation.setMessage(message);
        invitation.setRelation(rel);
        invitation.setState(InvitationStateEnum.NO_RESPONSE);        
        log().debug("Create {}", invitation);        
        getInvitationDao().create(invitation);
        return invitation;

    }

    /**
     * Metoda vytvori na zaklade vlozeneho mena a emailu noveho uzivatela. Vy-
     * generuje 20 miestne heslo ako mock dokym sa uzivatel nepotvrdi. Meno
     * uzivatela sa snazi autoamticky reslovovat na zaklde poctu slov.
     *
     * @param email noveho uzivatela
     * @param meno noveho uzivatela
     */
    private UserActivation createNewUser(String email, String name) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(name) || StringUtils.isBlank(email)) {
            throw new RequiredPropertyNullException();
        }

        // Vytvorime uzivatela
        UserLoginInfo userLoginInfo = UserUtils.prepareNewUser(email, UserUtils.generatePassword());       
        
        // Skusime vyparsovat meno uzivatela.
        String[] names = name.split(NAME_SEPARATOR);
        switch (names.length) {
            // Meno je len jedno , nastavime last aj first
            case 1:
                userLoginInfo.getUser().setFirstName(name);
                userLoginInfo.getUser().setLastName(name);
                break;
            // nastavime first a last standardne
            case 2:
                userLoginInfo.getUser().setFirstName(names[0]);
                userLoginInfo.getUser().setLastName(names[1]);
                break;
            // nastavime first, middle , last
            case 3:
                userLoginInfo.getUser().setFirstName(names[0]);
                userLoginInfo.getUser().setMiddleName(names[1]);
                userLoginInfo.getUser().setLastName(names[2]);
                break;
            // nastavime first [middle] last
            default:
                StringBuilder sb = new StringBuilder();
                for (int i = 1; i < names.length - 2; i++) {
                    sb.append(names[i]);
                }
                userLoginInfo.getUser().setFirstName(names[0]);
                userLoginInfo.getUser().setMiddleName(sb.toString());
                userLoginInfo.getUser().setLastName(names[names.length - 1]);
                break;
        }

        
        log().debug("Creating {}", userLoginInfo.getUser());
        
        try {
            getUserService().createRegistredUser(userLoginInfo,false,true);
        } catch (UsernameExistsException ex) {
            // ignore , test na zaciatku
        }
        
        // Vytvorime aktivaciu
        UserActivation a = ActivationUtils.createActivation(userLoginInfo);
        a.setInvitation(true);
        getLoginInfoService().createUserActivation(a);
        return a;
    }

    /*========================================================================*/
    /**
     * @return the invitationDao
     */
    public InvitationDao getInvitationDao() {
        return invitationDao;
    }

    /**
     * @param invitationDao the invitationDao to set
     */
    public void setInvitationDao(InvitationDao invitationDao) {
        this.invitationDao = invitationDao;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
    
    /**
     * @return the eventUserOrganizationService
     */
    public EventUserOrganizationService getEventUserOrganizationService() {
        return eventUserOrganizationService;
    }

    /**
     * @param eventUserOrganizationService the eventUserOrganizationService to set
     */
    public void setEventUserOrganizationService(EventUserOrganizationService eventUserOrganizationService) {
        this.eventUserOrganizationService = eventUserOrganizationService;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the loginInfoService
     */
    public UserLoginInfoService getLoginInfoService() {
        return loginInfoService;
    }

    /**
     * @param loginInfoService the loginInfoService to set
     */
    public void setLoginInfoService(UserLoginInfoService loginInfoService) {
        this.loginInfoService = loginInfoService;
    }
}
