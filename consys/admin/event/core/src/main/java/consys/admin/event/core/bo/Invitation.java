package consys.admin.event.core.bo;

import consys.admin.user.core.bo.User;
import consys.common.core.bo.ConsysObject;
import java.util.Date;

/**
 * Business objekt pozvanky do akcie. Identifikuje uzivatela od koho je pozvanka
 * poslana, komu je poslana, do akeho eventu a do akej relacie [registred,
 * administrator,owner].
 * Defaultne sa beru uzivatelia a na zaklade vysledku tj. ACCEPTED sa vytvori
 * ziadana relacia nad preferovanym objektom uzivatela.
 *
 * @author palo
 */
public class Invitation implements ConsysObject{
    private static final long serialVersionUID = -4026803991937420962L;
    
    private Long id;
    private String uuid;
    // Poziadavka
    private User from;
    private User to;
    private Date invitationDate;
    private String message;
    private Event event;
    private InvitationStateEnum state;
    private EventUserOrganizationRelation relation;
    // Priznaky odpovede
    private Date responseDate;
    

    public Invitation() {
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the from
     */
    public User getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(User from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public User getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(User to) {
        this.to = to;
    }

    /**
     * @return the invitationDate
     */
    public Date getInvitationDate() {
        return invitationDate;
    }

    /**
     * @param invitationDate the invitationDate to set
     */
    public void setInvitationDate(Date invitationDate) {
        this.invitationDate = invitationDate;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the event
     */
    public Event getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     * @return the state
     */
    public InvitationStateEnum getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(InvitationStateEnum state) {
        this.state = state;
    }

    /**
     * @return the reponseDate
     */
    public Date getResponseDate() {
        return responseDate;
    }

    /**
     * @param reponseDate the reponseDate to set
     */
    public void setResponseDate(Date reponseDate) {
        this.responseDate = reponseDate;
    }


    @Override
    public String toString() {
        return String.format("Invitation[from=%s to=%s where=%s]", from,to,event);
    }

    @Override
    public boolean equals(Object obj) {
         if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Invitation e = (Invitation) obj;
        return e.getUuid().equalsIgnoreCase(uuid);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    /**
     * @return the relation
     */
    public EventUserOrganizationRelation getRelation() {
        return relation;
    }

    /**
     * @param relation the relation to set
     */
    public void setRelation(EventUserOrganizationRelation relation) {
        this.relation = relation;
    }
}
