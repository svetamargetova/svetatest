package consys.admin.event.core.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public enum EventUserOrganizationRelation {

    // Len sa proklikol do eventu
    VISITOR(0),
    // Je v evente registrovany ako participant alebo tam ma prispevok.
    REGISTRED(1),
    // Je v evente v nejakom vybore
    ADMINISTRATOR(2),
    // Vlastnik eventu
    OWNER(3),
    // Cekajici na zaregistrovani z duvodu ze byla vycerpana kapacita
    WAITING(4);
    // logger
    private static final Logger logger = LoggerFactory.getLogger(EventUserOrganizationRelation.class);
    // data
    private Integer id;

    private EventUserOrganizationRelation(Integer id) {
        this.id = id;
    }

    public static EventUserOrganizationRelation fromId(Integer i) {
        switch (i) {
            case 0:
                return VISITOR;
            case 1:
                return REGISTRED;
            case 2:
                return ADMINISTRATOR;
            case 3:
                return OWNER;
            case 4:
                return WAITING;
        }
        logger.error("FromInt failed! Given id " + i + " has no enum member!");
        throw new RuntimeException();
    }

    public Integer getId() {
        return id;
    }

    public boolean isValuableThen(EventUserOrganizationRelation r) {
        return id > r.id;
    }
}
