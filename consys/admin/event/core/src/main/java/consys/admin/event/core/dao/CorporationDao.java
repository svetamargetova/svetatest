package consys.admin.event.core.dao;

import consys.admin.event.core.bo.Corporation;
import consys.common.core.dao.GenericDao;

/**
 *
 * @author pepa
 */
public interface CorporationDao extends GenericDao<Corporation> {

    /** zkontroluje jestli je korporace ulozena u nas v databazi */
    boolean checkRegisteredCorporation(String urlPrefix);

    /** nacte korporaci podle prefixu */
    Corporation loadByPrefix(String prefix);
}
