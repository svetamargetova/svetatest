package consys.admin.event.core.service.impl;

import consys.admin.event.core.bo.Corporation;
import consys.admin.event.core.dao.CorporationDao;
import consys.admin.event.core.service.CorporationService;

/**
 *
 * @author pepa
 */
public class CorporationServiceImpl implements CorporationService {

    private CorporationDao corporationDao;

    @Override
    public boolean checkRegisteredCorporation(String urlPrefix) {
        return corporationDao.checkRegisteredCorporation(urlPrefix);
    }

    @Override
    public Corporation loadCorporationByPrefix(String urlPrefix) {
        return corporationDao.loadByPrefix(urlPrefix);
    }

    public void setCorporationDao(CorporationDao corporationDao) {
        this.corporationDao = corporationDao;
    }
}
