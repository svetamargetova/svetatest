package consys.admin.event.core.bo.thumb;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.common.core.bo.ConsysThumb;

/**
 *
 * @author Palo
 */
public class SelectedEvent implements ConsysThumb{
    private static final long serialVersionUID = 1460362969318282371L;

    private Event event;
    private EventUserOrganizationRelation relation;

    /**
     * @return the event
     */
    public Event getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     * @return the relation
     */
    public EventUserOrganizationRelation getRelation() {
        return relation;
    }

    /**
     * @param relation the relation to set
     */
    public void setRelation(EventUserOrganizationRelation relation) {
        this.relation = relation;
    }
}
