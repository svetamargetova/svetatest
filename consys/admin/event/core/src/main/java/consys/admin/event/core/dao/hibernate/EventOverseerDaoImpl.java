package consys.admin.event.core.dao.hibernate;

import consys.admin.common.core.dao.hibernate.GenericDaoImpl;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.dao.EventOverseerDao;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class EventOverseerDaoImpl extends GenericDaoImpl<EventOverseer> implements EventOverseerDao {

    @Override
    public EventOverseer load(Long id) throws NoRecordException {
        return load(id, EventOverseer.class);
    }

    @Override
    public EventOverseer loadByName(String name) throws NoRecordException {
        Query q = session().createQuery("from EventOverseer e where e.name=:ONAME").
                setParameter("ONAME", name);
        EventOverseer eo = (EventOverseer) q.uniqueResult();
        return throwIfNull(eo, "EventOverseer with name " + name + " not exists!");
    }

    @Override
    public EventOverseer loadWithMinEvents() throws NoRecordException {
        Query q = session().createQuery("from EventOverseer e where e.events=(select min(eo.events) from EventOverseer eo)");
        List<EventOverseer> l = q.list();
        if (l.size() > 0) {
            return l.get(0);
        } else {
            throw new NoRecordException("No eventOverseer record!!");
        }


    }

    @Override
    public EventOverseer loadForEvent(String eventUuid) throws NoRecordException {
        EventOverseer overseer = (EventOverseer) session().createQuery("select e.overseer from Event e where e.uuid=:E_UUID").
                setParameter("E_UUID", eventUuid).uniqueResult();
        if (overseer == null) {
            throw new NoRecordException("Event has no overseer?");
        }
        return overseer;
    }
}
