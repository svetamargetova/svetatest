package consys.admin.event.core.service.exception;

import consys.common.core.exception.ConsysException;

/**
 * Vynimka vyhodena ked sa uzivatel snazi opakovane spracovat pozvanku
 * @author Palo
 */
public class InvitationAlreadyResolved extends ConsysException{
    private static final long serialVersionUID = 5159223164205894532L;
    

}
