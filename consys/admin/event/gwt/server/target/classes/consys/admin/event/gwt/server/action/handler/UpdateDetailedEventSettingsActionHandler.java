package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.admin.event.gwt.server.action.utils.EventUtils;
import consys.common.utils.enums.CountryEnum;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ReadOnlyPropertyException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.CountryService;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ReadOnlyException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateDetailedEventSettingsActionHandler extends AbstractRoleUserActionHandler<UpdateDetailedEventSettingsAction, VoidResult> {

    private EventService eventService;
    private CountryService stateService;

    public UpdateDetailedEventSettingsActionHandler() {
        super(UpdateDetailedEventSettingsAction.class);
    }

    @Override
    public VoidResult execute(UpdateDetailedEventSettingsAction action) throws ActionException {
        try {
            Event e = eventService.loadEventDetails(action.getEventUuid());
            e.setCity(action.getCity());
            e.setStreet(action.getStreet());
            e.setPlace(action.getPlace());
            e.setWeb(action.getWeb());
            if (action.getFrom() == null) {
                e.setFromDate(null);
                e.setFromTime(0);
            } else {
                e.setFromDate(EventUtils.fromClientDateToServerDate(action.getFrom().getDate()));
                e.setFromTime(action.getFrom().getTime().getTime());
            }
            if (action.getTo() == null) {
                e.setToDate(null);
                e.setToTime(0);
            } else {
                e.setToDate(EventUtils.fromClientDateToServerDate(action.getTo().getDate()));
                e.setToTime(action.getTo().getTime().getTime());
            }
            e.setTimeZone(action.getTimeZone());
            e.setCountry(CountryEnum.fromId(action.getLocation()));
            eventService.updateEventDetails(getUserSessionContext().getUserId(), e);
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (ReadOnlyPropertyException ex) {
            throw new ReadOnlyException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    /**
     * @param eventService the eventService to set
     */
    @Autowired(required = true)
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @param stateService the stateService to set
     */
    @Autowired(required = true)
    public void setStateService(CountryService stateService) {
        this.stateService = stateService;
    }
}
