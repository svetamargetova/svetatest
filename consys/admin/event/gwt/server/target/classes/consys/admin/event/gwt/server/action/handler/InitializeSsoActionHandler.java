/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.AdminSessionActionHandler;
import consys.admin.event.gwt.server.sso.SsoLockCache;
import consys.common.gwt.client.rpc.action.InitializeSsoAction;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class InitializeSsoActionHandler extends AdminSessionActionHandler<InitializeSsoAction, StringResult> {
   
    @Autowired
    private SsoLockCache lockCache;
    
    @Override
    public Class<InitializeSsoAction> getActionType() {
        return InitializeSsoAction.class;
    }

    @Override
    public StringResult execute(InitializeSsoAction action, HttpSession session) {   
        SsoLockCache.SsoLockItem item = lockCache.createLock();                
        return new StringResult(item.getToken());
    }
}
