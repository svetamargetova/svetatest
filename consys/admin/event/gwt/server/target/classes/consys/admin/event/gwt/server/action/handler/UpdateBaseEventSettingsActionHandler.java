package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ReadOnlyPropertyException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ReadOnlyException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateBaseEventSettingsActionHandler extends AbstractRoleUserActionHandler<UpdateBaseEventSettingsAction,VoidResult>{

    private EventService eventService;

    public UpdateBaseEventSettingsActionHandler() {
        super(UpdateBaseEventSettingsAction.class);
    }



    @Override
    public VoidResult execute(UpdateBaseEventSettingsAction action) throws ActionException {
        try {
            Event e = eventService.loadEventDetails(action.getEventUuid());
            e.setAcronym(action.getAcronym());
            e.setFullName(action.getFullName());
            //e.setSeries(action.getSeries()); // serie se nastavuje automaticky
            e.setShortName(action.getShortName());
            e.setUniversalName(action.getUniversalName());
            e.setYear(action.getYear());
            eventService.updateEventDetails(getUserSessionContext().getUserId(),e);
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (ReadOnlyPropertyException ex) {
            throw new ReadOnlyException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    /**
     * @param eventService the eventService to set
     */
    @Autowired(required=true)
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
    




}
