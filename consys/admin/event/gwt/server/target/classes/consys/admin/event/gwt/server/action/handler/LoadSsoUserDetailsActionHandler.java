/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.AdminSessionActionHandler;
import consys.admin.event.gwt.server.sso.SsoLockCache;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class LoadSsoUserDetailsActionHandler extends AdminSessionActionHandler<LoadSsoUserDetailsAction, ClientSsoUserDetails> {

    private static final long WAIT = 1000 * 60 * 5;
    @Autowired
    private SsoLockCache ssoLockCache;
    @Autowired
    private UserLoginInfoService loginService;

    @Override
    public ClientSsoUserDetails execute(LoadSsoUserDetailsAction action, HttpSession session) throws NoRecordsForAction {

        SsoLockCache.SsoLockItem lock = ssoLockCache.getLockForToken(action.getToken());
        if (lock == null) {
            throw new NoRecordsForAction();
        }
        synchronized (lock.lock) {
            try {
                lock.lock.wait(WAIT);
            } catch (InterruptedException ex) {
            }
        }

        // skusime nacitat
        lock = ssoLockCache.getLockForToken(action.getToken());

        if (lock != null && lock.getUserDetails() != null) {
            ClientSsoUserDetails details = (ClientSsoUserDetails) lock.getUserDetails();
            UserLoginInfo loginInfo = null;
            try {
                switch (lock.getUserDetails().getFromSso()) {
                    case FACEBOOK:
                        loginInfo = loginService.loadUserLoginInfoByFacebookIdOrEmail(details.getSsoId(), details.getLoginEmail());
                        break;
                    case GOOGLE_PLUS:
                        loginInfo = loginService.loadUserLoginInfoByGoogleId(details.getSsoStringId(), details.getLoginEmail());
                        break;
                    case LINKEDIN:
                        loginInfo = loginService.loadUserLoginInfoByLinkedInId(details.getSsoStringId());
                        break;
                    case TWITTER:
                        loginInfo = loginService.loadUserLoginInfoByTwitterId(details.getSsoId());
                        break;
                }
            } catch (Exception ex) {
                // uzivatel se nepodaril nacist, jede se dal
            }
            if (loginInfo != null) {
                details.setUserUuid(loginInfo.getUser().getUuid());
                details.setFirstName(loginInfo.getUser().getFirstName());
                details.setLastName(loginInfo.getUser().getLastName());
                details.setLoginEmail(loginInfo.getUser().getEmailOrMergeEmail());
            }
            return details;
        } else {
            throw new NoRecordsForAction();
        }
    }

    @Override
    public Class<LoadSsoUserDetailsAction> getActionType() {
        return LoadSsoUserDetailsAction.class;
    }
}
