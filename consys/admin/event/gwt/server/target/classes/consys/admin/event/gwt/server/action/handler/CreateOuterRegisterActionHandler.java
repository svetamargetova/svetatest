package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.common.server.security.AuthenticationProvider;
import consys.admin.common.server.security.SsoAuthenticationToken;
import consys.admin.event.core.bo.thumb.BundleOrderRequest;
import consys.admin.event.core.service.OuterRegistrationService;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.*;
import consys.common.gwt.client.rpc.action.CreateOuterRegisterAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.widget.exception.UserExistsException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.common.gwt.shared.exception.*;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.event.registration.ws.RegisterPaymentProfile;
import consys.payment.service.exception.IbanFormatException;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateOuterRegisterActionHandler extends AbstractAnonymousActionHandler<CreateOuterRegisterAction, VoidResult> {

    @Autowired
    private OuterRegistrationService outerRegistrationService;
    @Autowired
    private AuthenticationProvider authenticationProvider;

    public CreateOuterRegisterActionHandler() {
        super(CreateOuterRegisterAction.class);
    }

    @Override
    public VoidResult execute(CreateOuterRegisterAction action) throws ActionException {
        try {
            ClientSsoUserDetails cr = action.getRegistration();
            UserLoginInfo info;
            if (cr.getFromSso() != null) {
                info = UserUtils.prepareNewUser(cr.getLoginEmail(), UuidProvider.getUuid());
                switch (cr.getFromSso()) {
                    case FACEBOOK:
                        info.setFacebookId(cr.getSsoId());
                        break;
                    case GOOGLE_PLUS:
                        info.setGoogleId(cr.getSsoStringId());
                        break;
                    case LINKEDIN:
                        info.setLinkedInId(cr.getSsoStringId());
                        break;
                    case TWITTER:
                        info.setTwitterId(cr.getSsoId());
                        break;
                }
            } else {
                info = UserUtils.prepareNewUser(cr.getLoginEmail(), cr.getPassword());
            }

            info.getUser().setFirstName(cr.getFirstName());
            info.getUser().setLastName(cr.getLastName());
            info.getUser().setMiddleName(cr.getMiddleName());
            info.getUser().setGender(cr.getGender());
            info.getUser().setBio(cr.getBio());

            RegisterPaymentProfile rpp = new RegisterPaymentProfile();
            rpp.setCity(action.getPaymentProfile().getCity());
            rpp.setCountry(action.getPaymentProfile().getCountry());
            rpp.setIBAN(action.getPaymentProfile().getAccountNo());
            rpp.setName(action.getPaymentProfile().getName());
            rpp.setRegistrationNo(action.getPaymentProfile().getRegNo());
            rpp.setSWIFT(action.getPaymentProfile().getBankNo());
            rpp.setStreet(action.getPaymentProfile().getStreet());
            rpp.setVatNo(action.getPaymentProfile().getVatNo());
            rpp.setZip(action.getPaymentProfile().getZip());

            BundleOrderRequest mainBundle = new BundleOrderRequest(action.getPackageUuid(),
                    action.getDiscountTicket(), action.getQuantity());
            List<BundleOrderRequest> subbundles = Lists.newArrayListWithCapacity(action.getSubbundles().size());
            for (ProductThumb p : action.getSubbundles()) {
                subbundles.add(new BundleOrderRequest(p.getProductUuid(), p.getDiscountCode(), p.getQuantity()));
            }

            User user = outerRegistrationService.createUserEventRegistration(info, preprocessOrganization(cr),
                    cr.getProfilePictureUrl(), rpp, mainBundle, subbundles, action.getEventUuid(), action.getAnswers());

            // Ak OK - prihlasime uzivatela aby sme ho mohli redirektovat
            authenticationProvider.authenticate(new SsoAuthenticationToken(user.getUuid(), user.getId()));
            return VoidResult.RESULT();
        } catch (CouponCapacityException ex) {
            throw new CouponUsedException();
        } catch (CouponOutOfDateException ex) {
            throw new CouponExpiredException();
        } catch (CouponCodeException ex) {
            throw new CouponInvalidException();
        } catch (BundleCapacityFullException ex) {
            throw new BundleCapacityException();
        } catch (IbanFormatException ex) {
            throw new IbanException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction(ex.getMessage());
        } catch (UsernameExistsException ex) {
            throw new UserExistsException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    public String preprocessOrganization(ClientSsoUserDetails cr) {
        if (StringUtils.isNotBlank(cr.getAffiliation())) {
            if (cr.getAffiliation().startsWith("at ")) {
                return cr.getAffiliation().substring(3);
            }
        }
        return cr.getAffiliation();
    }
}
