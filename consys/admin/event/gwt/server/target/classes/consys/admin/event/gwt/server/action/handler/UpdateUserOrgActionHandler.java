package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.bo.Organization;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.OrganizationService;
import consys.admin.user.core.service.UserOrganizationService;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.util.OrganizationUtils;
import consys.admin.user.gwt.client.action.UpdateUserOrgAction;
import consys.admin.user.gwt.client.bo.ClientUserOrg;
import consys.admin.user.gwt.server.action.utils.OrganizationHelper;
import consys.admin.user.gwt.server.action.utils.UserOrganizationHelper;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.CountryService;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo 
 */
public class UpdateUserOrgActionHandler extends AbstractRoleUserActionHandler<UpdateUserOrgAction, VoidResult> {

    private UserService userService;
    private UserOrganizationService userOrganizationService;
    private OrganizationService organizationService;
    private CountryService stateService;
    private EventUserOrganizationService eventUserOrganization;

    public UpdateUserOrgActionHandler() {
        super(UpdateUserOrgAction.class);
    }

    @Override
    public VoidResult execute(UpdateUserOrgAction action) throws ActionException {
        ClientUserOrg cuo = action.getClientUserOrg();
        Organization o = null;
        User currentUser = null;
        boolean owner = false;

        try {
            /*
             * Ak je organizacia nova tak sa vytvori nova organizacia a nacita sa aktualny
             * uzivatel ktory sa nastavi ako vlasnitk.
             * Ak je organizacia uz existujuca tak sa nacita a porovna sa s vlasntikom.
             * Ak je vlastnikom tak sa vytiahne instancia user ak nie je nacita sa uzivatel
             */
            if (cuo.getOrganization().getUuid() != null) {
                try {
                    o = getOrganizationService().loadByUuid(cuo.getOrganization().getUuid());
                    if (o.getUserCreated().getUuid().equalsIgnoreCase(getContextProvider().getUserContext().getUserUuid())) {
                        currentUser = o.getUserCreated();
                        owner = true;
                    } else {
                        try {
                            currentUser = userService.loadUserByUuid(getContextProvider().getUserContext().getUserUuid());
                            owner = false;


                        } catch (NoRecordException ex) {
                            logger.error("Load logged user failed by NoRecordsException");
                            throw new ServiceFailedException();
                        }
                    }
                } catch (NoRecordException ex) {
                    logger.error("Load Organization by UUID failed");
                    throw new ServiceFailedException();
                }
            } else {
                try {
                    currentUser = userService.loadUserByUuid(getContextProvider().getUserContext().getUserUuid());
                    o = OrganizationUtils.createOrganization();
                    o.setUserCreated(currentUser);
                    owner = true;
                } catch (NoRecordException ex) {
                    logger.error("Load logged user failed by NoRecordsException");
                    throw new ServiceFailedException();
                }
            }

            // Ak je uzivatel vlastnik tak sa spracuje organizacia
            if (owner) {
                OrganizationHelper.toOrganization(o, cuo.getOrganization(), organizationService, stateService);
            }


            // validace
            logger.debug("Validating right for update");
            if (!userOrganizationService.isUserOwnerOfUserOrganization(getContextProvider().getUserContext().getUserUuid(), cuo.getUuid())) {
                logger.error("User " + currentUser + " is trying to update different experience " + cuo.getUuid());
                throw new ServiceFailedException();
            }


            UserOrganization uo = null;
            try {
                uo = userOrganizationService.loadByUuid(cuo.getUuid());
                UserOrganizationHelper.toUserOrganization(uo, cuo, getStateService());
                uo.setOrganization(o);
                eventUserOrganization.updateUserOrganization(uo, cuo.isPreferredPosition());
            } catch (ServiceExecutionFailed ex) {
                logger.error("Updating user organization failed", ex);
                throw new ServiceFailedException();
            } catch (NoRecordException ex) {
                logger.error("Updating user organization with bad uuid");
                throw new ServiceFailedException();
            }
            return VoidResult.RESULT();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required = true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the userOrganizationService
     */
    public UserOrganizationService getUserOrganizationService() {
        return userOrganizationService;
    }

    /**
     * @param userOrganizationService the userOrganizationService to set
     */
    @Autowired(required = true)
    public void setUserOrganizationService(UserOrganizationService userOrganizationService) {
        this.userOrganizationService = userOrganizationService;
    }

    /**
     * @return the organizationService
     */
    public OrganizationService getOrganizationService() {
        return organizationService;
    }

    /**
     * @param organizationService the organizationService to set
     */
    @Autowired(required = true)
    public void setOrganizationService(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    /**
     * @return the stateService
     */
    public CountryService getStateService() {
        return stateService;
    }

    /**
     * @param stateService the stateService to set
     */
    @Autowired(required = true)
    public void setStateService(CountryService stateService) {
        this.stateService = stateService;
    }

    /**
     * @param eventUserOrganization the eventUserOrganization to set
     */
    @Autowired(required = true)
    public void setEventUserOrganization(EventUserOrganizationService eventUserOrganization) {
        this.eventUserOrganization = eventUserOrganization;
    }
}
