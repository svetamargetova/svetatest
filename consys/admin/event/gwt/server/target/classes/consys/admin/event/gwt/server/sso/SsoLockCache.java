/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.gwt.server.sso;

import consys.common.core.cache.EhCacheFactory;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.utils.UuidProvider;
import java.io.Serializable;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Cache ktora drzi zamky pre jednotlive tokeny
 * <p/>
 * @author palo
 */
public class SsoLockCache implements InitializingBean {

    public static final String CACHE_NAME = "sso-synchronization-token-cache";    
    private Ehcache cache;
    @Autowired
    private EhCacheFactory cacheFactory;

    public SsoLockItem getLockForToken(String token) {
        Element e = cache.get(token);
        if (e != null) {
            return (SsoLockItem) e.getValue();
        } else {
            return null;
        }
    }

    public SsoLockItem createLock() {
        SsoLockItem item = new SsoLockItem();
        cache.put(new Element(item.token, item));
        return item;
    }

    public void destroy(String token) {
        cache.remove(token);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        CacheConfiguration cacheConfig = new CacheConfiguration(CACHE_NAME, 1000).overflowToDisk(false).
                eternal(false).
                diskPersistent(false).
                // no matter what proste 1 hodina minut
                timeToLiveSeconds(60 * 60).
                // ak neni zpristupneni 1 hodina tak cancel 
                timeToIdleSeconds(60 * 60);

        cache = cacheFactory.create(CACHE_NAME, cacheConfig);
        cacheFactory.initializeCache(cache);       
    }

    public class SsoLockItem implements Serializable {

        public final String token;
        public final Object lock;
        private ClientSsoUserDetails userDetails;

        public SsoLockItem() {
            token = UuidProvider.getUuid();
            lock = new Object();
        }

        public Object getLock() {
            return lock;
        }

        public String getToken() {
            return token;
        }

        /**
         * @return the userDetails
         */
        public ClientSsoUserDetails getUserDetails() {
            return userDetails;
        }

        /**
         * @param userDetails the userDetails to set
         */
        public void setUserDetails(ClientSsoUserDetails userDetails) {
            this.userDetails = userDetails;
        }
    }
}
