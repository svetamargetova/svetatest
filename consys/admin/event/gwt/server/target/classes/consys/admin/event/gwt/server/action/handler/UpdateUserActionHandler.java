package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.admin.user.gwt.client.action.UpdateUserAction;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo 
 */
public class UpdateUserActionHandler extends AbstractRoleUserActionHandler<UpdateUserAction, VoidResult> {

    private UserService userService;
    private EventUserOrganizationService eventUserOrganization;

    public UpdateUserActionHandler() {
        super(UpdateUserAction.class);
    }

    @Override
    public VoidResult execute(UpdateUserAction action) throws ActionException {
        try {
            String userUuid = getUserSessionContext().getUserUuid();
            User u = userService.loadUserByUuid(userUuid);
            ClientUser cu = action.getClientUser();
            // TODO: validace - nikdy never klientovi
            u.setBio(cu.getBio());
            u.setFirstName(cu.getFirstName());
            u.setFrontDegree(cu.getFrontDegree());
            u.setLastName(cu.getLastName());
            u.setMiddleName(cu.getMiddleName());
            u.setRearDegree(cu.getRearDegree());
            u.setGender(cu.getGender());

            String anotherUserUuid = null;

            if (!u.getEmailOrMergeEmail().contains("@") && !StringUtils.isEmpty(cu.getLoginEmail())) {
                if (ValidatorUtils.isValidEmailAddress(cu.getLoginEmail())) {
                    // kontrola jestli uz takovy email neexistuje?
                    try {
                        anotherUserUuid = userService.loadUserUuidByUsername(cu.getLoginEmail());
                    } catch (NoRecordException ex) {
                        // uzivatel s timto mailem neexistuje, muzeme ho v klidu priradit
                        u.setEmail(cu.getLoginEmail());
                    }
                } else {
                    throw new BadInputException();
                }
            }

            switch (cu.getGender()) {
                case 0:
                    u.setGenderType(User.Gender.NOT_SPECIFIED);
                    break;
                case 1:
                    u.setGenderType(User.Gender.MAN);
                    break;
                case 2:
                    u.setGenderType(User.Gender.WOMAN);
                    break;
            }
            eventUserOrganization.updateUser(u);

            if (anotherUserUuid != null) {
                // existuje update emailu sso uzivatele a takovy mail uz existuje
                userService.updateMergeLoginInfo(anotherUserUuid, u);
            }

            return VoidResult.RESULT();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (NoRecordException ex) {
            throw new ServiceFailedException();
        }
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    @Autowired(required = true)
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @param eventUserOrganization the eventUserOrganization to set
     */
    @Autowired(required = true)
    public void setEventUserOrganization(EventUserOrganizationService eventUserOrganization) {
        this.eventUserOrganization = eventUserOrganization;
    }
}
