package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.event.core.service.CorporationService;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.CheckCorporationAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CheckCorporationActionHandler extends AbstractAnonymousActionHandler<CheckCorporationAction, BooleanResult> {

    // servicy
    @Autowired
    private CorporationService corporationService;

    public CheckCorporationActionHandler() {
        super(CheckCorporationAction.class);
    }

    @Override
    public BooleanResult execute(CheckCorporationAction action) throws ActionException {
        if (action.getPrefix() == null || action.getPrefix().isEmpty()) {
            return new BooleanResult(false);
        }
        BooleanResult result = new BooleanResult(corporationService.checkRegisteredCorporation(action.getPrefix()));
        logger.debug("Check corporation " + action.getPrefix() + ": " + result.isBool());
        return result;
    }
}
