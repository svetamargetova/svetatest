package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.Invitation;
import consys.admin.event.core.service.EventInvitationService;
import consys.admin.event.gwt.client.action.LoadEventInvitationAction;
import consys.admin.event.gwt.client.bo.EventInvitation;
import consys.admin.event.gwt.client.bo.InvitationState;
import consys.admin.event.gwt.server.action.utils.EventUtils;
import consys.admin.user.core.bo.User;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadEventInvitationActionHandler extends AbstractRoleUserActionHandler<LoadEventInvitationAction,EventInvitation>{

    private EventInvitationService invitationService;

    public LoadEventInvitationActionHandler() {
        super(LoadEventInvitationAction.class);
    }

    @Override
    public EventInvitation execute(LoadEventInvitationAction action) throws ActionException {
        try {
            Invitation i = invitationService.loadDetailedInvitation(action.getUuid(), getUserSessionContext().getUserUuid());
            EventInvitation invitation = new EventInvitation();
            invitation.setEvent(EventUtils.toClientEvent(i.getEvent()));
            invitation.setFrom(toClientUser(i.getFrom()));
            invitation.setInvitationDate(i.getInvitationDate());
            invitation.setMessage(i.getMessage());
            invitation.setReponseDate(i.getResponseDate());
            switch (i.getState()) {
                    case ACCEPTED:
                        invitation.setState(InvitationState.ACCEPTED);
                        break;
                    case DECLINED:
                        invitation.setState(InvitationState.DECLINED);
                        break;
                    case NO_RESPONSE:
                        invitation.setState(InvitationState.NO_RESPONSE);
                        break;
                    case TIMEOUTED:
                        invitation.setState(InvitationState.TIMEOUTED);
                        break;
                }            
            return invitation;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    public static ClientUser toClientUser(User u) {
        ClientUser cu = new ClientUser();
        cu.setBio(u.getBio());
        cu.setUuid(u.getUuid());
        cu.setFirstName(u.getFirstName());
        cu.setFrontDegree(u.getFrontDegree());
        cu.setLastName(u.getLastName());
        cu.setLoginEmail(u.getEmailOrMergeEmail());
        cu.setMiddleName(u.getMiddleName());
        cu.setRearDegree(u.getRearDegree());
        cu.setUserOrgDefault(u.getDefaultUserOrganization().getShortInfo());
        cu.setUuidImg(u.getPortraitImageUuidPrefix());
        cu.setGender(u.getGender());
        return cu;
    }
    

    /**
     * @return the invitationService
     */
    @Autowired
    @Required
     public void setInvitationService(EventInvitationService invitationService) {
        this.invitationService = invitationService;
    }    


}
