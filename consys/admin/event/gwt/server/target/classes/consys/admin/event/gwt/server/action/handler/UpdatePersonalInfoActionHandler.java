package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.admin.user.gwt.client.action.UpdatePersonalInfoAction;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdatePersonalInfoActionHandler extends AbstractRoleUserActionHandler<UpdatePersonalInfoAction, VoidResult> {

    @Autowired
    private EventUserOrganizationService service;
    
    @Autowired
    private UserService userService;
    
    public UpdatePersonalInfoActionHandler() {
        super(UpdatePersonalInfoAction.class);
    }

    @Override
    public VoidResult execute(UpdatePersonalInfoAction action) throws ActionException {
        try {
            User user = userService.loadUserByUuid(getUserSessionContext().getUserUuid());
            user.setCeliac(action.isCeliac());
            user.setVegetarian(action.isVegetarian());
            service.updateUser(user);
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
        
    }
}
