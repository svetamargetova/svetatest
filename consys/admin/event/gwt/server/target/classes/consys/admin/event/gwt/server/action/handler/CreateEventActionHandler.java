package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.Corporation;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventType;
import consys.admin.event.core.service.CorporationService;
import consys.admin.event.core.service.EventService;
import consys.admin.event.gwt.client.action.CreateEventAction;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.UserOrganizationService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.CountryService;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author Palo
 */
public class CreateEventActionHandler extends AbstractRoleUserActionHandler<CreateEventAction, StringResult> {

    // sluzby
    private EventService eventService;
    private UserOrganizationService userOrganizationService;
    private CountryService stateService;
    @Autowired
    private CorporationService corporationService;

    public CreateEventActionHandler() {
        super(CreateEventAction.class);
    }

    @Override
    public StringResult execute(CreateEventAction action) throws ActionException {
        try {
            Corporation c = null;
            if (StringUtils.isNotBlank(action.getPrefix())) {
                c = corporationService.loadCorporationByPrefix(action.getPrefix());
            }
            Event e = new Event();
            e.setOwner(c);
            e.setAcronym(action.getAcronym());
            e.setFullName(action.getFullname());
            e.setYear(action.getYear());
            e.setShortName(String.format("%s %s", action.getAcronym(), action.getYear()));
            e.setTimeZone(action.getTimeZone());
            switch (action.getType()) {
                case BROKERAGE:
                    e.setType(EventType.BROKERAGE);
                    break;
                case CONFERENCE:
                    e.setType(EventType.CONFERENCE);
                    break;
                default:
                    e.setType(EventType.CONFERENCE);
            }
            e.setCountry(stateService.loadCountryById(action.getCountryId()));
            UserOrganization uo = userOrganizationService.loadPrivateUserOrg(getContextProvider().getUserContext().getUserUuid());
            eventService.createEvent(e, uo);
            return new StringResult(e.getUuid());
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    public EventService getEventService() {
        return eventService;
    }

    @Autowired
    @Required
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    public UserOrganizationService getUserOrganizationService() {
        return userOrganizationService;
    }

    @Autowired
    @Required
    public void setUserOrganizationService(UserOrganizationService userOrganizationService) {
        this.userOrganizationService = userOrganizationService;
    }

    @Autowired
    @Required
    public void setStateService(CountryService stateService) {
        this.stateService = stateService;
    }
}
