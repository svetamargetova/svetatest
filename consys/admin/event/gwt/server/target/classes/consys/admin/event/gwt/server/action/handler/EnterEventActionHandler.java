package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.EventUserOrganizationRelation;
import consys.admin.event.core.bo.thumb.SelectedEvent;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.event.gwt.client.action.EnterEventAction;
import consys.admin.event.gwt.server.action.utils.EventUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author Palo
 */
public class EnterEventActionHandler extends AbstractRoleUserActionHandler<EnterEventAction, ClientCurrentEvent>{

    private EventUserOrganizationService service;

    public EnterEventActionHandler() {
        super(EnterEventAction.class);
    }

    @Override
    public ClientCurrentEvent execute(EnterEventAction action) throws ActionException {
        try {
            ClientCurrentEvent cce = new ClientCurrentEvent();
            SelectedEvent e = getService().updateAndLoadSelectedEventByEventUuid(action.getEventUuid(), getUserSessionContext().getUserId());
            cce.setOverseerUrl(e.getEvent().getOverseer().getClientSuffix());
            cce.setEvent(EventUtils.toClientEvent(e.getEvent()));
            cce.setJustVisitor(e.getRelation() == EventUserOrganizationRelation.VISITOR);
            return cce;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    /**
     * @return the service
     */
    public EventUserOrganizationService getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    @Autowired
    @Required
    public void setService(EventUserOrganizationService service) {
        this.service = service;
    }

}
