package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.event.core.service.EventService;
import consys.admin.event.gwt.client.action.ListAllEventsAction;
import consys.admin.event.gwt.client.bo.EventListResult;
import consys.admin.event.gwt.server.action.utils.EventUtils;
import consys.common.gwt.shared.action.ActionException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class ListAllEventsActionHandler extends AbstractRoleUserActionHandler<ListAllEventsAction, EventListResult> {

    private EventService eventService;

    public ListAllEventsActionHandler() {
        super(ListAllEventsAction.class);
    }

    @Override
    public EventListResult execute(ListAllEventsAction action) throws ActionException {
        List<EventThumb> events = eventService.listEventsChronologically(action.isUpcoming(), action.getFrom(),
                action.getLimit(), action.getPrefix());
        EventListResult result = new EventListResult();
        for (EventThumb e : events) {
            result.getList().add(EventUtils.toClientEventThumb(e));
        }
        result.setHasMore(events.size() == action.getLimit());
        return result;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    @Autowired(required = true)
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
}
