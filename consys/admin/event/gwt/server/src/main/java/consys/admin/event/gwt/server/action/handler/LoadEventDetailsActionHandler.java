package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.admin.event.gwt.server.action.utils.EventUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.client.action.LoadEventDetailsAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadEventDetailsActionHandler extends AbstractRoleUserActionHandler<LoadEventDetailsAction,ClientEvent>{
    
    @Autowired
    private EventService eventService;

    public LoadEventDetailsActionHandler() {
        super(LoadEventDetailsAction.class);
    }



    @Override
    public ClientEvent execute(LoadEventDetailsAction action) throws ActionException {
        try {
            Event e = eventService.loadEventDetails(action.getEventUuid());
            return EventUtils.toClientEvent(e);            
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }    
}
