package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.service.exception.ActivationKeyException;
import consys.admin.user.gwt.client.action.ConfirmInvitationAction;
import consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConfirmInvitationActionHandler extends AbstractAnonymousActionHandler<ConfirmInvitationAction, VoidResult>{

    private EventUserOrganizationService eventUserOrganization;

    public ConfirmInvitationActionHandler() {
        super(ConfirmInvitationAction.class);
    }

    @Override
    public VoidResult execute(ConfirmInvitationAction action) throws ActionException {
        try {
            String firstName = action.getRegistraion().getFirstName();
            String lastName = action.getRegistraion().getLastName();
            String password = action.getRegistraion().getPassword();
            eventUserOrganization.activateInvitedUser(firstName, lastName, password, action.getToken());
            return VoidResult.RESULT();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (ActivationKeyException ex) {
            throw new ActivationKeyNotFoundException();
        }
    }

    /**
     * @return the userService
     */
    @Autowired
    @Required
    public void setEventUserOrganizationService(EventUserOrganizationService us) {
        eventUserOrganization = us;
    }
}
