package consys.admin.event.gwt.server.action.utils;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.common.gwt.shared.bo.ClientDate;
import consys.common.gwt.shared.bo.ClientDateTime;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.bo.ClientEventThumb;
import consys.common.gwt.shared.bo.ClientTime;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Palo
 */
public class EventUtils {

    public static ClientEventThumb toClientEventThumb(EventThumb e) {
        return toClientEventThumb(e, false);
    }

    public static ClientEventThumb toClientEventThumb(EventThumb e, boolean fillDescription) {
        ClientEventThumb thumb = new ClientEventThumb();
        toClientEventThumb(e, thumb, fillDescription);
        return thumb;
    }

    public static void toClientEventThumb(EventThumb e, ClientEventThumb thumb, boolean fillDescription) {
        thumb.setFrom(fromServerDateTimeToClientDateTime(e.getFrom(), null));
        thumb.setTo(fromServerDateTimeToClientDateTime(e.getTo(), null));
        thumb.setFullName(e.getFullName());
        thumb.setLogoUuidPrefix(e.getLogoUuid());
        thumb.setProfileLogoUuidPrefix(e.getProfileLogoUuid());
        thumb.setUuid(e.getUuid());
        thumb.setWeb(e.getWeb());
        thumb.setYear(e.getYear());
        thumb.setOverseer(e.getOverseer());
        if (fillDescription) {
            thumb.setDescription(e.getDescription());
        }
    }

    public static ClientEvent toClientEvent(Event e) {
        ClientEvent ce = new ClientEvent();
        ce.setAcronym(e.getAcronym());
        ce.setCity(e.getCity());
        ce.setStreet(e.getStreet());
        ce.setPlace(e.getPlace());
        ce.setDescription(e.getDescription());
        ce.setFullName(e.getFullName());
        ce.setFrom(fromServerDateTimeToClientDateTime(e.getFromDate(), e.getFromTime()));
        ce.setLocation(e.getCountry() != null ? e.getCountry().getId() : 0);
        ce.setLogoUuidPrefix(e.getImageLogoPrefix());
        ce.setProfileLogoUuidPrefix(e.getPortraitLogoPrefix());
        ce.setSeries(e.getSeries());
        ce.setShortName(e.getShortName());
        ce.setTags(e.getTags());
        ce.setTo(fromServerDateTimeToClientDateTime(e.getToDate(), e.getToTime()));
        ce.setUniversalName(e.getUniversalName());
        ce.setUuid(e.getUuid());
        ce.setUsState(e.getUsState() != null ? e.getUsState().getId() : 0);
        ce.setVisible(e.isVisible());
        ce.setWeb(e.getWeb());
        ce.setYear(e.getYear());
        return ce;
    }

    public static Date fromClientDateToServerDate(ClientDate cd) {
        if (cd == null) {
            return null;
        }
        GregorianCalendar c = new GregorianCalendar(cd.getYear(), cd.getMonth() - 1, cd.getDay());
        return c.getTime();
    }

    public static Date fromClientDateTimeToServerDateTime(ClientDateTime cdt) {
        if (cdt == null || cdt.getDate().getYear() == ClientDate.DEFAULT_YEAR) {
            return null;
        }
        GregorianCalendar c = new GregorianCalendar(cdt.getDate().getYear(), cdt.getDate().getMonth() - 1,
                cdt.getDate().getDay(), cdt.getTime().getHours(), cdt.getTime().getMinutes());
        return c.getTime();
    }

    public static ClientDate fromServerDateToClientDate(Date date) {
        if (date == null) {
            return null;
        }
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);

        ClientDate cd = new ClientDate();
        cd.setYear(c.get(GregorianCalendar.YEAR));
        cd.setMonth(c.get(GregorianCalendar.MONTH) + 1);
        cd.setDay(c.get(GregorianCalendar.DAY_OF_MONTH));
        return cd;
    }

    public static ClientDateTime fromServerDateTimeToClientDateTime(Date date, Integer time) {
        if (date == null) {
            return null;
        }
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(date);

        ClientDate cd = new ClientDate();
        cd.setYear(c.get(GregorianCalendar.YEAR));
        cd.setMonth(c.get(GregorianCalendar.MONTH) + 1);
        cd.setDay(c.get(GregorianCalendar.DAY_OF_MONTH));

        ClientTime ct = new ClientTime(time == null ? 0 : time);

        ClientDateTime cdt = new ClientDateTime();
        cdt.setDate(cd);
        cdt.setTime(ct);

        return cdt;
    }
}
