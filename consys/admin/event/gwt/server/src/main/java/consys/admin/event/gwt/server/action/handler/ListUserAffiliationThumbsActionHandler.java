package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.event.core.service.EventUserOrganizationService;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.UserOrganizationService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUserAffiliationThumbsActionHandler extends AbstractAnonymousActionHandler<ListUserAffiliationThumbsAction, ArrayListResult<ClientUserWithAffiliationThumb>> {

    private UserOrganizationService userOrganizationService;
    private EventUserOrganizationService eventUserOrganizationService;

    public ListUserAffiliationThumbsActionHandler() {
        super(ListUserAffiliationThumbsAction.class);
    }

    @Override
    public ArrayListResult<ClientUserWithAffiliationThumb> execute(ListUserAffiliationThumbsAction action) throws ActionException {
        try {
            List<UserOrganization> uos = null;
            if (StringUtils.isNotBlank(action.getEventUuid())) {
                uos = eventUserOrganizationService.listEventUserOrganizations(action.getThumbs(), action.getEventUuid());
            } else {
                uos = userOrganizationService.listUserOrganizationForUserUuids(action.getThumbs());
            }
            ArrayList<ClientUserWithAffiliationThumb> thumbs = Lists.newArrayList();
            for (UserOrganization uo : uos) {
                ClientUserWithAffiliationThumb thumb = new ClientUserWithAffiliationThumb();
                thumb.setName(uo.getUser().getFullName());
                thumb.setPosition(uo.getShortInfo());
                thumb.setUuid(uo.getUuid());
                thumb.setUuidPortraitImagePrefix(uo.getUser().getPortraitImageUuidPrefix());
                thumbs.add(thumb);
            }
            return new ArrayListResult<ClientUserWithAffiliationThumb>(thumbs);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    /**
     * @param eventUserOrganizationService the eventUserOrganizationService to set
     */
    @Autowired(required = true)
    public void setEventUserOrganizationService(EventUserOrganizationService eventUserOrganizationService) {
        this.eventUserOrganizationService = eventUserOrganizationService;
    }

    /**
     * @param userOrganizationService the userOrganizationService to set
     */
    @Autowired(required = true)
    public void setUserOrganizationService(UserOrganizationService userOrganizationService) {
        this.userOrganizationService = userOrganizationService;
    }
}
