package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.InvitationStateEnum;
import consys.admin.event.core.service.EventInvitationService;
import consys.admin.event.core.service.exception.InvitationAlreadyResolved;
import consys.admin.event.gwt.client.action.ResolveInvitationAction;
import consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ResolveInvitationActionHandler extends AbstractRoleUserActionHandler<ResolveInvitationAction, VoidResult> {

    private EventInvitationService invitationService;

    public ResolveInvitationActionHandler() {
        super(ResolveInvitationAction.class);
    }

    @Override
    public VoidResult execute(ResolveInvitationAction action) throws ActionException {
        try {
            invitationService.updateInvitation(
                    action.isInvitationAccepted() ? InvitationStateEnum.ACCEPTED : InvitationStateEnum.DECLINED,
                    action.getInvitationUuid(),
                    getUserSessionContext().getUserUuid());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (InvitationAlreadyResolved ex) {
            throw new ActivationKeyNotFoundException();
        }
    }

    /**
     * @return the invitationService
     */
    @Autowired
    @Required
     public void setInvitationService(EventInvitationService invitationService) {
        this.invitationService = invitationService;
    }    
}
