package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.admin.user.core.bo.UserOrganization;
import consys.admin.user.core.service.UserOrganizationService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.action.CloneEventAction;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CloneEventActionHandler extends AbstractRoleUserActionHandler<CloneEventAction, StringResult> {

    // servicy
    @Autowired
    private EventService eventService;
    @Autowired
    private UserOrganizationService userOrganizationService;

    public CloneEventActionHandler() {
        super(CloneEventAction.class);
    }

    @Override
    public StringResult execute(CloneEventAction action) throws ActionException {
        try {
            logger.debug("loading event to clone");
            // nacteme puvodni event
            Event event = eventService.loadEvent(action.getEventUuid());

            logger.debug("loading user organization to clone");
            // nacteme uzivatelovu organizaci pro kontrolu
            UserOrganization uo = userOrganizationService.loadPrivateUserOrg(getContextProvider().getUserContext().getUserUuid());
            
            logger.debug("start cloning");
            // nechame naklonovat event
            Event clonedEvent = eventService.cloneEvent(event, uo);
            logger.debug("end cloning");
            
            return new StringResult(clonedEvent.getUuid());
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
