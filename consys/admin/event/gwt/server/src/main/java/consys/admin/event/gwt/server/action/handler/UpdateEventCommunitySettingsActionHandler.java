package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ReadOnlyPropertyException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ReadOnlyException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateEventCommunitySettingsActionHandler extends AbstractRoleUserActionHandler<UpdateEventCommunitySettingsAction,VoidResult>{

    private EventService eventService;

    public UpdateEventCommunitySettingsActionHandler() {
        super(UpdateEventCommunitySettingsAction.class);
    }

    @Override
    public VoidResult execute(UpdateEventCommunitySettingsAction action) throws ActionException {
        try {
            Event e = eventService.loadEventDetails(action.getEventUuid());
            e.setTags(action.getTags());
            e.setDescription(action.getDescription());
            e.setWeb(action.getWeb());
            eventService.updateEventDetails(getUserSessionContext().getUserId(),e);
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (ReadOnlyPropertyException ex) {
            throw new ReadOnlyException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    /**
     * @param eventService the eventService to set
     */
    @Autowired(required=true)
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
    




}
