package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.thumb.EventInvitationThumb;
import consys.admin.event.core.service.EventInvitationService;
import consys.admin.event.gwt.client.action.ListMyEventInvitationsAction;
import consys.admin.event.gwt.client.bo.EventInvitationListResult;
import consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb;
import consys.admin.event.gwt.client.bo.InvitationState;
import consys.admin.event.gwt.server.action.utils.EventUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListMyEventInvitationsActionHandler extends AbstractRoleUserActionHandler<ListMyEventInvitationsAction, EventInvitationListResult> {

    private EventInvitationService invitationService;

    public ListMyEventInvitationsActionHandler() {
        super(ListMyEventInvitationsAction.class);
    }

    @Override
    public EventInvitationListResult execute(ListMyEventInvitationsAction action) throws ActionException {
        try {
            List<EventInvitationThumb> thumbs = null;
            if (action.listActiveInvitations()) {
                thumbs = invitationService.listUserActiveInvitations(getUserSessionContext().getUserUuid(), action.getFrom(), action.getLimit());
            } else {
                thumbs = invitationService.listUserInvitations(getUserSessionContext().getUserUuid(), action.getFrom(), action.getLimit());
            }
            ArrayList<InvitationThumb> out = Lists.newArrayList();
            for (EventInvitationThumb et : thumbs) {
                InvitationThumb thumb = new InvitationThumb();
                thumb.setUuid(et.getUuid());
                switch (et.getState()) {
                    case ACCEPTED:
                        thumb.setState(InvitationState.ACCEPTED);
                        break;
                    case DECLINED:
                        thumb.setState(InvitationState.DECLINED);
                        break;
                    case NO_RESPONSE:
                        thumb.setState(InvitationState.NO_RESPONSE);
                        break;
                    case TIMEOUTED:
                        thumb.setState(InvitationState.TIMEOUTED);
                        break;
                }
                thumb.setEvent(EventUtils.toClientEventThumb(et.getEvent()));
                out.add(thumb);
            }
            return new EventInvitationListResult(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    /**
     * 
     */
    @Autowired
    @Required
    public void setInvitationService(EventInvitationService invitationService) {
        this.invitationService = invitationService;
    }    
}
