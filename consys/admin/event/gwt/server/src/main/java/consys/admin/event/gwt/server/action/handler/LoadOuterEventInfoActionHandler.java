package consys.admin.event.gwt.server.action.handler;

import consys.admin.common.gwt.server.action.server.handler.AbstractAnonymousActionHandler;
import consys.admin.common.gwt.server.action.server.handler.AbstractRoleUserActionHandler;
import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.event.core.service.EventService;
import consys.admin.event.gwt.server.action.utils.EventUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.ClientEventThumb;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.client.action.LoadOuterEventInfoAction;
import consys.event.common.gwt.client.bo.ClientOuterEventData;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadOuterEventInfoActionHandler extends AbstractAnonymousActionHandler<LoadOuterEventInfoAction, ClientOuterEventData> {

    @Autowired
    private EventService eventService;

    public LoadOuterEventInfoActionHandler() {
        super(LoadOuterEventInfoAction.class);
    }

    @Override
    public ClientOuterEventData execute(LoadOuterEventInfoAction action) throws ActionException {
        try {
            ClientOuterEventData out = new ClientOuterEventData();
            EventThumb e = eventService.loadEventThumb(action.getEventUuid());            
            EventUtils.toClientEventThumb(e, out,action.isFillDescription());
            out.setActive(e.isActive());
            return out;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
