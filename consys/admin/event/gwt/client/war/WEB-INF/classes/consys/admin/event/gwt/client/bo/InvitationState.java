package consys.admin.event.gwt.client.bo;

public enum InvitationState {

    NO_RESPONSE, ACCEPTED, DECLINED, TIMEOUTED
}
