package consys.admin.event.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.admin.event.gwt.client.action.CreateEventAction;

/**
 *
 * @author Palo
 */
public class NewEventCreated extends GwtEvent<NewEventCreated.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private CreateEventAction action;
    private String uuid;

    public NewEventCreated(CreateEventAction action,String uuid) {
        this.action = action;
        this.uuid = uuid;
    }

    public CreateEventAction getAction() {
        return action;
    }

    public String getUuid() {
        return uuid;
    }

    

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onCreatedNewEvent(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onCreatedNewEvent(NewEventCreated event);
    }


}
