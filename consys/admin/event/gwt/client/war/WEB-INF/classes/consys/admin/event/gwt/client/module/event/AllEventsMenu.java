package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.event.gwt.client.action.ListAllEventsAction;
import consys.admin.event.gwt.client.bo.EventListResult;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.admin.event.gwt.client.utils.AdminEventResources;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.CorporateUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Vylistovani a prehled vsech eventu v systemu. Pouzity:
 * - pokial spristupnene z Menu cez All events, tak sa vypise X eventov kde na
 * scrolovanie a zobrazenie sa nacita dalsi X. Nacitava sa podla datumu
 *
 * - ak cez search tak sa zobrazia podla search
 * @author pepa
 */
public class AllEventsMenu extends SimpleFormPanel implements MenuFormItem, EventsPanel.Delegate {

    // konstanty
    public static final String NAME = AEMessageUtils.c.allEventsMenu_title_allEvents();
    // komponenty
    private EventsPanel eventsPanel;
    // data
    private boolean isAttending = true;
    private EventFilterSelector selected;

    public AllEventsMenu() {
        super();
        setWidth("100%");

        AdminEventResources.INSTANCE.css().ensureInjected();

        final EventFilterSelector upcoming = new EventFilterSelector(true, "attending", AEMessageUtils.c.myEventsMenu_action_upcoming()) {
            @Override
            public void fireSelect(EventFilterSelector selector) {
                dataAction(getTag());
            }
        };
        upcoming.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                upcoming.selected();
                if (selected != upcoming) {
                    selected.unselected();
                }
                selected = upcoming;
            }
        });
        selected = upcoming;
        final EventFilterSelector previous = new EventFilterSelector(false, "attended", AEMessageUtils.c.myEventsMenu_action_previous()) {
            @Override
            public void fireSelect(EventFilterSelector selector) {
                dataAction(getTag());
            }
        };
        previous.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                previous.selected();
                if (selected != previous) {
                    selected.unselected();
                }
                selected = previous;
            }
        });

        addWidget(StyleUtils.getStyledLabel(AEMessageUtils.c.myEventsMenu_text_show() + ":", AdminEventResources.INSTANCE.css().show()));
        addWidget(upcoming);
        addWidget(StyleUtils.getStyledLabel("|", AdminEventResources.INSTANCE.css().showSeparator()));
        addWidget(previous);
        addWidget(StyleUtils.clearDiv());

        eventsPanel = new EventsPanel(this);
        addWidget(eventsPanel);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.setWidget(this);
    }

    @Override
    public void loadNext(int from, int limit, AbstractEventsPanel panel) {
        String prefix = CorporateUtils.corporationPrefix();
        EventBus.get().fireEvent(new DispatchEvent(new ListAllEventsAction(isAttending, from, limit, prefix),
                new AsyncCallback<EventListResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(EventListResult result) {
                        eventsPanel.setItems(result.getList(), result.isHasMore());
                    }
                }, eventsPanel));
    }

    /** zmena parametru action pro nacteni seznamu + vyvolani nacteni */
    private void dataAction(String tag) {
        isAttending = tag.equals("attending");
        eventsPanel.resetAndLoadFirstPage();
    }
}
