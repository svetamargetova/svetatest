package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.event.gwt.client.utils.EventUtils;
import consys.common.gwt.client.ServletConstants;
import consys.common.gwt.client.event.EventEnterRequest;
import consys.common.constants.img.EventLogoImageEnum;
import consys.common.gwt.shared.bo.ClientEventThumb;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Jedna polozka v seznamech eventu
 * @author pepa
 */
public class EventItem extends Composite implements CssStyles {

    // konstanty
    private static final String WIDTH = "227px";
    // komponenty
    private SimplePanel mainPanel;
    private FocusPanel focusPanel;

    public EventItem(final ClientEventThumb event) {
        FlowPanel content = new FlowPanel();
        content.add(logo(event));
        content.add(commonStyledLabel(event.getFullName(), FONT_BOLD, MARGIN_TOP_5));
        content.add(commonStyledLabel(date(event)));
        content.add(commonStyledLabel(event.getWeb()));

        mainPanel = new SimplePanel();
        mainPanel.addStyleName(StyleUtils.Assemble(HAND, BORDER_LIGHT_GRAY));
        mainPanel.setWidget(content);
        DOM.setStyleAttribute(mainPanel.getElement(), "minHeight", "90px");

        focusPanel = new FocusPanel(mainPanel);
        focusPanel.setWidth(WIDTH);
        initWidget(focusPanel);

        sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        initHandlers(event);
    }

    /** zinicializuje handlery po najeti nad polozku a po odjeti z polozky */
    private void initHandlers(final ClientEventThumb eventThumb) {
        addHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                mainPanel.addStyleName(StyleUtils.BORDER_GRAY);
                mainPanel.removeStyleName(StyleUtils.BORDER_LIGHT_GRAY);
            }
        }, MouseOverEvent.getType());
        addHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                mainPanel.addStyleName(StyleUtils.BORDER_LIGHT_GRAY);
                mainPanel.removeStyleName(StyleUtils.BORDER_GRAY);
            }
        }, MouseOutEvent.getType());
        focusPanel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent cevent) {
                History.newItem(EventEnterRequest.EVENT + "?" + EventEnterRequest.PARAM_EVENT_UUID + "=" + eventThumb.getUuid());
            }
        });
    }

    private Label commonStyledLabel(String text, String... styles) {
        Label label = StyleUtils.getStyledLabel(text, MARGIN_HOR_3, MARGIN_BOT_3, FONT_11PX, ALIGN_LEFT);
        for (int i = 0; i < styles.length; i++) {
            label.addStyleName(styles[i]);
        }
        return label;
    }

    private SimplePanel logo(ClientEventThumb event) {
        Image logo = new Image();
        if (event.getLogoUuidPrefix() == null) {
            logo.setResource(ResourceUtils.system().confLogoSmall());
        } else {
            logo.setUrl(ServletConstants.BASE_URL
                    + ServletConstants.EVENT_LOGO_IMG_LOAD
                    + EventLogoImageEnum.LIST.getFullUuid(event.getLogoUuidPrefix()));
        }

        SimplePanel panel = new SimplePanel();
        panel.addStyleName(ALIGN_LEFT);
        panel.setWidget(logo);
        return panel;
    }

    private String date(ClientEventThumb event) {
        String date = String.valueOf(event.getYear());
        if (event.getFrom() != null) {
            date = EventUtils.eventDate(event);
        }
        return date;
    }
}
