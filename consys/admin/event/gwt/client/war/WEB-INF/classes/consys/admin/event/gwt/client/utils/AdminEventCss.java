package consys.admin.event.gwt.client.utils;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
public interface AdminEventCss extends CssResource {

    String show();

    String showSeparator();
}
