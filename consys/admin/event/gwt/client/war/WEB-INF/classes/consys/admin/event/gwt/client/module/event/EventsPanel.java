package consys.admin.event.gwt.client.module.event;

import consys.common.gwt.shared.bo.ClientEventThumb;

/**
 *
 * @author pepa
 */
public class EventsPanel extends AbstractEventsPanel<EventItem, ClientEventThumb> {

    public EventsPanel(Delegate delegate) {
        super(delegate);
    }

    public EventsPanel(Delegate delegate, int[] loadLimits) {
        super(delegate, loadLimits);
    }

    @Override
    public EventItem getNewItem(ClientEventThumb data) {
        return new EventItem(data);
    }
}
