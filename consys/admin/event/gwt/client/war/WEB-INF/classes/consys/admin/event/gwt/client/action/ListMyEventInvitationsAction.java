package consys.admin.event.gwt.client.action;

import consys.admin.event.gwt.client.bo.EventInvitationListResult;
import consys.common.gwt.shared.action.Action;

/**
 * Nacita zoznam thumbov pozvanok podla priznaku active.
 * active == true - nacita aktivne este nevyhodnotene pozvanky
 * active == false - nacita uz vyhodnotene alebo timeoutovane pozvanky
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListMyEventInvitationsAction implements Action<EventInvitationListResult> {

    private static final long serialVersionUID = -5345750058707254561L;
    private boolean active;
    private int from;
    private int limit;

    public ListMyEventInvitationsAction() {
    }

    public ListMyEventInvitationsAction(boolean active, int from, int limit) {
        this.active = active;
        this.from = from;
        this.limit = limit;
    }

    public boolean listActiveInvitations() {
        return active;
    }

    /**
     * @return the from
     */
    public int getFrom() {
        return from;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }
}
