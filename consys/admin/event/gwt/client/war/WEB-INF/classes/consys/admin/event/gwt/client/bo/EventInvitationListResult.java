package consys.admin.event.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.ClientEventThumb;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventInvitationListResult implements Result {

    private static final long serialVersionUID = -2383577978812609615L;
    private ArrayList<InvitationThumb> invitations;

    public EventInvitationListResult() {
    }


    public EventInvitationListResult(ArrayList<InvitationThumb> invitations) {
        this.invitations = invitations;
    }

    public ArrayList<InvitationThumb> getInvitations() {
        return invitations;
    }

    public static class InvitationThumb implements Serializable{
        private static final long serialVersionUID = 4626849338160981220L;

        private ClientEventThumb event;
        private InvitationState state;
        private String uuid;

        public InvitationThumb() {
        }

        

        /**
         * @return the event
         */
        public ClientEventThumb getEvent() {
            return event;
        }

        /**
         * @param event the event to set
         */
        public void setEvent(ClientEventThumb event) {
            this.event = event;
        }

        /**
         * @return the state
         */
        public InvitationState getState() {
            return state;
        }

        /**
         * @param state the state to set
         */
        public void setState(InvitationState state) {
            this.state = state;
        }

        /**
         * @return the uuid
         */
        public String getUuid() {
            return uuid;
        }

        /**
         * @param uuid the uuid to set
         */
        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
    }
}
