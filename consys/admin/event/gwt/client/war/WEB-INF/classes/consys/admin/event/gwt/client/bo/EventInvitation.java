package consys.admin.event.gwt.client.bo;


import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.bo.ClientUser;
import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventInvitation implements Result{
    private static final long serialVersionUID = 7668144714509217052L;

    private String uuid;
    private ClientUser from;
    private ClientEvent event;
    private Date invitationDate;
    private Date reponseDate;
    private InvitationState state;
    private String message;

    public EventInvitation() {
    }

    

    /**
     * @return the from
     */
    public ClientUser getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(ClientUser from) {
        this.from = from;
    }

    /**
     * @return the event
     */
    public ClientEvent getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(ClientEvent event) {
        this.event = event;
    }

    /**
     * @return the invitationDate
     */
    public Date getInvitationDate() {
        return invitationDate;
    }

    /**
     * @param invitationDate the invitationDate to set
     */
    public void setInvitationDate(Date invitationDate) {
        this.invitationDate = invitationDate;
    }

    /**
     * @return the reponseDate
     */
    public Date getReponseDate() {
        return reponseDate;
    }

    /**
     * @param reponseDate the reponseDate to set
     */
    public void setReponseDate(Date reponseDate) {
        this.reponseDate = reponseDate;
    }

    /**
     * @return the state
     */
    public InvitationState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(InvitationState state) {
        this.state = state;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    







}
