package consys.admin.event.gwt.client.module.event;

import consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb;

/**
 *
 * @author pepa
 */
public class InvitationsPanel extends AbstractEventsPanel<InvitationsItem, InvitationThumb> {

    public InvitationsPanel(Delegate delegate) {
        super(delegate);
    }

    public InvitationsPanel(Delegate delegate, int[] loadLimits) {
        super(delegate, loadLimits);
    }

    @Override
    public InvitationsItem getNewItem(InvitationThumb data) {
        return new InvitationsItem(data, this);
    }
}
