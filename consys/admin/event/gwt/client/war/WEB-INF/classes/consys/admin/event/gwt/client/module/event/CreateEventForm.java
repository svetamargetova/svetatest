package consys.admin.event.gwt.client.module.event;

import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.ui.comp.panel.RootPanel;

/**
 *
 * @author Palo
 */
public class CreateEventForm extends RootPanel {

    public static final String CREATE_EVENT_TOKEN = "CreateEvent";

    public CreateEventForm() {
        super(AEMessageUtils.c.createEventForm_title_createEvent());
    }

    @Override
    protected void onLoad() {
        setWidget(new CreateEventPanel());
    }
}
