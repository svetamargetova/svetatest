package consys.admin.event.gwt.client.module;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.event.gwt.client.action.EnterEventAction;
import consys.admin.event.gwt.client.cache.handler.EnterEventActionCacheHandler;
import consys.admin.event.gwt.client.module.event.CreateEventForm;
import consys.admin.event.gwt.client.module.event.EventEntranceContent;
import consys.admin.event.gwt.client.module.event.EventMenuContent;
import consys.admin.event.gwt.client.module.event.InvitationsMenu;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.CacheRegistrator;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.event.EventEnterRequest;
import consys.common.gwt.client.module.AsyncContent;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.ui.CommonHistoryHandler;
import consys.common.gwt.client.ui.CommonHistoryItem;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.shared.bo.ClientCurrentEvent;

/**
 * Modul spravujici prehled a nastaveni eventu
 * @author pepa
 */
public class EventModule implements Module, EventEnterRequest.Handler {

    // konstanty
    /** zobrazi eventy */
    public static final String EVENTS = "Events";
    /** zobrazi pozvanky */
    public static final String INVITATIONS = "Invitations";

    @Override
    public void onEventEntrance(EventEnterRequest event) {
        DispatchEvent enter = new DispatchEvent(new EnterEventAction(event.getEventUuid()), new AsyncCallback<ClientCurrentEvent>() {

            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovany ActionExecutorem
                LayoutManager.get().showWaiting(false);
            }

            @Override
            public void onSuccess(ClientCurrentEvent result) {
                LayoutManager.get().showWaiting(false);
            }
        }, null);
        LayoutManager.get().showWaiting(true);
        EventBus.get().fireEvent(enter);
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
        // Registracia do hlavneho menu
        AsyncContent content = new AsyncContent() {

            @Override
            public void runComponent() {
                EventMenuContent.getAsync(new AsyncModule<EventMenuContent>() {

                    @Override
                    public void onSuccess(final EventMenuContent instance) {
                        ChangeContentEvent event = new ChangeContentEvent(instance);
                        EventBus.get().fireEvent(event);
                    }

                    @Override
                    public void onFail() {
                        // TODO: fail
                    }
                });
            }
        };

        menuRegistrator.registerMenuItem(new ModuleMenuItem(content, AEMessageUtils.c.const_events(), EVENTS, 0));
    }

    @Override
    public void registerModule() {
        EventBus.get().addHandler(EventEnterRequest.TYPE, this);
        CacheRegistrator.register(EnterEventAction.class, new EnterEventActionCacheHandler());

        // Registracia CommonHistory
        CommonHistoryHandler history = CommonHistoryHandler.get();

        history.registerOutToken(CreateEventForm.CREATE_EVENT_TOKEN, new CommonHistoryItem() {

            @Override
            public Widget createInstace() {
                return new CreateEventForm();
            }
        });
        history.registerOutToken(EventEnterRequest.EVENT, new CommonHistoryItem() {

            @Override
            public Widget createInstace() {
                return new EventEntranceContent();
            }
        });
        history.registerLoggedInToken(INVITATIONS, new CommonHistoryItem() {

            @Override
            public Widget createInstace() {
                return new AsyncContent() {

                    @Override
                    public void runComponent() {
                        EventMenuContent.getAsync(new AsyncModule<EventMenuContent>() {

                            @Override
                            public void onSuccess(final EventMenuContent instance) {
                                instance.showItem(InvitationsMenu.NAME);
                                ChangeContentEvent event = new ChangeContentEvent(instance);
                                EventBus.get().fireEvent(event);
                            }

                            @Override
                            public void onFail() {
                                // TODO: fail
                            }
                        });
                    }
                };
            }
        });
    }

    @Override
    public void unregisterModule() {
        EventBus.get().removeHandler(EventEnterRequest.TYPE, this);
        CacheRegistrator.unregister(EnterEventAction.class);
    }
}
