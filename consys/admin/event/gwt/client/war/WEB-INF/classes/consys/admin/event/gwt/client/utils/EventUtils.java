package consys.admin.event.gwt.client.utils;

import com.google.gwt.i18n.client.DateTimeFormat;
import consys.common.gwt.client.ui.comp.MonthYearDatePicker;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.shared.bo.ClientEventThumb;
import java.util.Date;

/**
 *
 * @author Palo
 */
public class EventUtils {

    private static final DateTimeFormat day = DateTimeFormat.getFormat("d");

    public static String createEventDateNiceFormat(Date from, Date to) {
        String result = "";
        if (from != null && to != null) {
            // je nastaveny datum od i datum do
            final int fromMonth = from.getMonth();
            final int toMonth = to.getMonth();
            if (fromMonth == toMonth) {
                // rozsah je ve stejnem mesici
                final String dayFrom = day.format(from);
                final String dayTo = day.format(to);

                result += MonthYearDatePicker.MONTHS[fromMonth];
                if (dayFrom.equals(dayTo)) {
                    // ve stejny den
                    result += FormUtils.UNB_SPACE + dayFrom;
                } else {
                    // v ruzne dny
                    result += FormUtils.UNB_SPACE + dayFrom + FormUtils.NDASH + dayTo;
                }
            } else {
                // v ruznych mesicich
                result += MonthYearDatePicker.MONTHS[fromMonth] + FormUtils.UNB_SPACE + day.format(from);
                result += FormUtils.UNB_SPACE + FormUtils.NDASH + FormUtils.UNB_SPACE;
                result += MonthYearDatePicker.MONTHS[toMonth] + FormUtils.UNB_SPACE + day.format(to);
            }

            // vypis roku, pripadne rozsah roku
            final int fromYear = DateTimeUtils.getYear(from);
            final int toYear = DateTimeUtils.getYear(to);
            result += ", " + (fromYear == toYear ? fromYear : fromYear + "-" + toYear);
        } else if (from != null && to == null) {
            // je nastaven jen datum od
            final int fromMonth = from.getMonth();
            final int fromYear = DateTimeUtils.getYear(from);

            result += MonthYearDatePicker.MONTHS[fromMonth] + FormUtils.UNB_SPACE + day.format(from);
            result += ", " + fromYear;
        }
        return result;
    }

    /** datum konani akce */
    public static String eventDate(ClientEventThumb thumb) {
        StringBuilder sb = new StringBuilder();
        sb.append(thumb.getFrom() == null ? "n/a" : DateTimeUtils.getDate(thumb.getFrom().getDate()));
        if (!thumb.isInSameDay() && thumb.getTo() != null) {
            sb.append(" - ");
            sb.append(DateTimeUtils.getDate(thumb.getTo().getDate()));
        }
        return sb.toString();
    }
}
