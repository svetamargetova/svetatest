/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.event.gwt.client.message;

import com.google.gwt.i18n.client.Messages;

/**
 *
 * @author palo
 */
public interface AdminEventMessages extends Messages {

    String createEventPanel_error_yearBoxError(int from, int to);

    String createEventPanel_info_noTimeZone(String country);

    String createEventPanel_info_manyTimeZones(String country);
    
    String eventsPanel_action_loadNextEvents(int count);
}
