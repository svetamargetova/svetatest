package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.event.gwt.client.action.ListMyEventsAction;
import consys.admin.event.gwt.client.bo.EventListResult;
import consys.admin.event.gwt.client.event.NewEventCreated;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.admin.event.gwt.client.utils.AdminEventResources;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventEnterRequest;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.CorporateUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;

/**
 * Prehled eventu, ve kterych je uzivatel zaregistrovany nebo je jejich spravcem
 * @author pepa
 */
public class MyEventsMenu extends SimpleFormPanel implements MenuFormItem, NewEventCreated.Handler, EventsPanel.Delegate {

    // konstanty
    public static final String NAME = AEMessageUtils.c.myEventsMenu_title_myEvents();
    // komponenty
    private EventsPanel eventsPanel;
    // data
    private boolean isAttending = true;
    private EventFilterSelector selected;

    public MyEventsMenu() {
        super();
        setBodyWidth("100%");
        EventBus.get().addHandler(NewEventCreated.TYPE, this);

        AdminEventResources.INSTANCE.css().ensureInjected();

        final EventFilterSelector upcoming = new EventFilterSelector(true, "attending", AEMessageUtils.c.myEventsMenu_action_upcoming()) {
            @Override
            public void fireSelect(EventFilterSelector selector) {
                dataAction(getTag());
            }
        };
        upcoming.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                upcoming.selected();
                if (selected != upcoming) {
                    selected.unselected();
                }
                selected = upcoming;
            }
        });
        selected = upcoming;
        final EventFilterSelector previous = new EventFilterSelector(false, "attended", AEMessageUtils.c.myEventsMenu_action_previous()) {
            @Override
            public void fireSelect(EventFilterSelector selector) {
                dataAction(getTag());
            }
        };
        previous.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                previous.selected();
                if (selected != previous) {
                    selected.unselected();
                }
                selected = previous;
            }
        });

        addWidget(StyleUtils.getStyledLabel(AEMessageUtils.c.myEventsMenu_text_show() + ":", AdminEventResources.INSTANCE.css().show()));
        addWidget(upcoming);
        addWidget(StyleUtils.getStyledLabel("|", AdminEventResources.INSTANCE.css().showSeparator()));
        addWidget(previous);
        addWidget(StyleUtils.clearDiv());

        eventsPanel = new EventsPanel(this);
        addWidget(eventsPanel);
    }

    /** zinicializuje podcasti zalozky */
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.setWidget(this);
    }

    @Override
    public void onCreatedNewEvent(NewEventCreated event) {
        clearMessageBox();
        // proklik do eventu po vytvoreni
        Cache.get().register(Cache.NEW_EVENT_CREATED, event.getUuid());
        History.newItem(EventEnterRequest.EVENT + "?" + EventEnterRequest.PARAM_EVENT_UUID + "=" + event.getUuid());
        EventBus.get().fireEvent(new ChangeContentEvent(new EventEntranceContent()));
    }

    @Override
    public void loadNext(int from, int limit, AbstractEventsPanel panel) {
        String prefix = CorporateUtils.corporationPrefix();
        EventBus.get().fireEvent(new DispatchEvent(new ListMyEventsAction(isAttending, from, limit, prefix),
                new AsyncCallback<EventListResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        if (caught instanceof NoRecordsForAction) {
                            clearMessageBox();
                            if (isAttending) {
                                ConsysMessage m = getSuccessMessage(false);
                                m.setText(AEMessageUtils.c.myEventsMenu_text_myEventsEmpty(), true);
                            }
                        }
                    }

                    @Override
                    public void onSuccess(EventListResult result) {
                        eventsPanel.setItems(result.getList(), result.isHasMore());
                    }
                }, eventsPanel));
    }

    /** zmena parametru action pro nacteni seznamu + vyvolani nacteni */
    private void dataAction(String tag) {
        isAttending = tag.equals("attending");
        eventsPanel.resetAndLoadFirstPage();
    }
}
