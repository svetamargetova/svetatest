package consys.admin.event.gwt.client.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface AdminEventConstants extends Constants {

    String allEventsMenu_title_allEvents();

    String createEventForm_title_createEvent();

    String createEventPanel_button_addEvent();

    String createEventPanel_form_fullName();
    
    String const_addEvent();

    String const_events();

    String invitationDetailDialog_text_invited();

    String invitationDetailDialog_text_message();

    String invitationDetailDialog_text_responsed();

    String invitationDetailDialog_text_state();

    String invitationsItem_text_accepted();

    String invitationsItem_text_declined();

    String invitationsItem_text_notResponse();

    String invitationsItem_text_state();

    String invitationsItem_text_timeouted();

    String invitationsItem_text_unknownState();

    String invitationsMenu_action_active();

    String invitationsMenu_action_inactive();

    String invitationsMenu_title_invitations();

    String myEventsMenu_action_previous();

    String myEventsMenu_action_upcoming();

    String myEventsMenu_text_myEventsEmpty();

    String myEventsMenu_text_show();

    String myEventsMenu_title_myEvents();
}
