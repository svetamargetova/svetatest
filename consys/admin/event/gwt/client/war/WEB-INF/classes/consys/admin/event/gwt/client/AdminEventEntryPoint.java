package consys.admin.event.gwt.client;

import com.google.gwt.user.client.ui.RootPanel;
import consys.admin.event.gwt.client.action.CreateEventAction;
import consys.admin.event.gwt.client.action.ListAllEventsAction;
import consys.admin.event.gwt.client.action.ListMyEventInvitationsAction;
import consys.admin.event.gwt.client.action.ListMyEventsAction;
import consys.admin.event.gwt.client.action.LoadEventInvitationAction;
import consys.admin.event.gwt.client.action.ResolveInvitationAction;
import consys.admin.event.gwt.client.bo.EventInvitation;
import consys.admin.event.gwt.client.bo.EventInvitationListResult;
import consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb;
import consys.admin.event.gwt.client.bo.EventListResult;
import consys.admin.event.gwt.client.bo.InvitationState;
import consys.admin.event.gwt.client.module.EventModule;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ModuleEntryPoint;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.module.ModuleRegistrator;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.action.ListUsStateAction;
import consys.common.gwt.shared.bo.ClientEventThumb;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.ui.layout.menu.MenuDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.bo.ClientDate;
import consys.common.gwt.shared.bo.ClientDateTime;
import consys.common.gwt.shared.bo.ClientEvent;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class AdminEventEntryPoint extends ModuleEntryPoint {

    @Override
    public void initMocks() {
        EventListResult myEvents = new EventListResult();
        myEvents.setHasMore(true);

        ClientDateTime cdt1 = new ClientDateTime();
        cdt1.setDate(new ClientDate(12,12,2000));
        
        ClientEventThumb eventThumb = new ClientEventThumb();
        eventThumb.setUuid("1");
        eventThumb.setFrom(cdt1);
        eventThumb.setTo(cdt1);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);

        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("2");

        ClientDateTime cdt2 = new ClientDateTime();
        cdt2.setDate(new ClientDate(31,12,2010));
        
        ClientDateTime cdt3 = new ClientDateTime();
        cdt3.setDate(new ClientDate(5,1,2011));
        
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        eventThumb.setFrom(cdt2);
        eventThumb.setTo(cdt3);
        myEvents.getList().add(eventThumb);

        ClientDateTime cdt4 = new ClientDateTime();
        cdt4.setDate(new ClientDate(12,12,2010));
        
        ClientDateTime cdt5 = new ClientDateTime();
        cdt5.setDate(new ClientDate(20,11,2011));
        
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt1);
        eventThumb.setTo(null/*new Date(2011, 1, 5)*/);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt2);
        eventThumb.setTo(cdt3);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);
        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("3");
        eventThumb.setFrom(cdt4);
        eventThumb.setTo(cdt5);
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.acemcee.com");
        myEvents.getList().add(eventThumb);

        eventThumb = new ClientEventThumb();
        eventThumb.setUuid("4");
        eventThumb.setFullName("Digital geometry of masters of rock");
        eventThumb.setWeb("www.dsdadasdas.sk");
        eventThumb.setYear(2012);
        myEvents.getList().add(eventThumb);

        MockFactory.addActionMock(ListMyEventsAction.class, new ActionMock<EventListResult>(myEvents));
        //MockFactory.addActionMock(ListMyEventsAction.class, new ActionMock(new NoRecordsForAction()));
        MockFactory.addActionMock(ListAllEventsAction.class, new ActionMock<EventListResult>(myEvents));

        // -----
        MockFactory.addActionMock(CreateEventAction.class, new ActionMock<StringResult>(new StringResult("1")));
        // -----
        ArrayList<InvitationThumb> its = new ArrayList<InvitationThumb>();
        InvitationThumb it = new InvitationThumb();
        it.setUuid("123546657657768");
        it.setEvent(eventThumb);
        it.setState(InvitationState.NO_RESPONSE);
        its.add(it);
        its.add(it);
        its.add(it);
        its.add(it);
        its.add(it);
        EventInvitationListResult ir = new EventInvitationListResult(its);
        MockFactory.addActionMock(ListMyEventInvitationsAction.class, new ActionMock<EventInvitationListResult>(ir));

        // -----
        ClientEvent event = new ClientEvent();
        event.setAcronym("MOBERA");
        event.setYear(2011);
        EventInvitation ei = new EventInvitation();
        ei.setEvent(event);
        ei.setInvitationDate(new Date());
        ei.setMessage("invitation message for user");
        ei.setState(InvitationState.DECLINED);
        ei.setReponseDate(new Date());
        MockFactory.addActionMock(LoadEventInvitationAction.class, new ActionMock<EventInvitation>(ei));

        // -----
        MockFactory.addActionMock(ResolveInvitationAction.class, new ActionMock<VoidResult>(new VoidResult()));

        SelectBoxData locationList = new SelectBoxData();
        locationList.getList().add(new BoxItem("China", 46));
        locationList.getList().add(new BoxItem("Czech republic", 60));
        locationList.getList().add(new BoxItem("COSTA_RICA", 54));
        MockFactory.addActionMock(ListLocationAction.class, new ActionMock<SelectBoxData>(locationList));

        // ------
        SelectBoxData stateList = new SelectBoxData();
        stateList.getList().add(new BoxItem("DA", 34));
        stateList.getList().add(new BoxItem("CA", 44));
        stateList.getList().add(new BoxItem("LA", 54));
        MockFactory.addActionMock(ListUsStateAction.class, new ActionMock<SelectBoxData>(stateList));
    }

    @Override
    public void initLayout() {

        // Inicializuje Layout
        LayoutManager layoutManager = LayoutManager.get();
        RootPanel.get().add(layoutManager);
        MenuDispatcher.get();

        ModuleRegistrator.get().registerInModule(new EventModule());

        // nastavi cache, ze je neprihlaseny uzivatel
        Cache.get().register(Cache.LOGGED, Boolean.TRUE);
        EventBus.get().fireEvent(new UserLoginEvent());
        // zinicializovani PanelDispatcheru a vlozeni panelu
        PanelDispatcher.get().generatePanel();
    }
}
