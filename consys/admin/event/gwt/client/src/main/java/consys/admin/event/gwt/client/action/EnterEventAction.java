package consys.admin.event.gwt.client.action;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientCurrentEvent;

/**
 *
 * @author Palo 
 */
public class EnterEventAction implements Action<ClientCurrentEvent> {

    private static final long serialVersionUID = -8200869009367347005L;
    private String eventUuid;

    public EnterEventAction() {
    }

    public EnterEventAction(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getEventUuid() {
        return eventUuid;
    }
}
