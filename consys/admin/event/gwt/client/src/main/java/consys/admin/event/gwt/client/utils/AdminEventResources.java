package consys.admin.event.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;

/**
 *
 * @author pepa
 */
public interface AdminEventResources extends ClientBundle {

    public static final AdminEventResources INSTANCE = GWT.create(AdminEventResources.class);

    @Source("adminEvent.css")
    public AdminEventCss css();
}
