package consys.admin.event.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.admin.event.gwt.client.action.EnterEventAction;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.event.EventEntered;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import java.util.Date;

/**
 *
 * @author Palo 
 */
public class EnterEventActionCacheHandler implements CacheHandler<EnterEventAction> {

    private static final Logger logger = LoggerFactory.getLogger(EnterEventActionCacheHandler.class);

    @Override
    public void doAction(EnterEventAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
        logger.debug("Sending EnterEventAction");
        ActionExecutor.execute(action, new AsyncCallback<ClientCurrentEvent>() {

            @Override
            public void onFailure(Throwable caught) {
                logger.debug("Event enter failed!");
                callback.onFailure(caught);
            }

            @Override
            public void onSuccess(ClientCurrentEvent result) {
                Cache.get().register(CurrentEventCacheAction.CURRENT_EVENT, result);
                // vystreli se event, ze byl user aktualizovan
                logger.debug("Event " + result.getEvent().getAcronym() + " sucessfully entered");
                EventEntered eventEntered = new EventEntered(result);
                EventBus.get().fireEvent(eventEntered);
                callback.onSuccess(result);
            }
        }, delegate);
    }
}
