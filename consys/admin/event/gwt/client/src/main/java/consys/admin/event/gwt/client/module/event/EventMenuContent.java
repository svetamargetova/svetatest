package consys.admin.event.gwt.client.module.event;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import consys.admin.event.gwt.client.event.NewEventCreated;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.panel.MenuFormPanel;

/**
 * Zalozkovy panel s prehledy a nastavenim eventu
 * @author pepa
 */
public class EventMenuContent extends MenuFormPanel implements NewEventCreated.Handler {

    // instance
    private static EventMenuContent instance;

    private EventMenuContent() {
        super(AEMessageUtils.c.const_events());
        EventBus.get().addHandler(NewEventCreated.TYPE, this);

        ActionImage addEvent = ActionImage.getPlusButton(AEMessageUtils.c.const_addEvent(), true);
        addEvent.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CreateEventForm.CREATE_EVENT_TOKEN);
            }
        });
        addLeftControll(addEvent);

        addItem(new MyEventsMenu());
        addItem(new AllEventsMenu());
        addItem(new InvitationsMenu());
        initWidget();
    }

    /** vytvori asynchronne instanci */
    public static void getAsync(final AsyncModule<EventMenuContent> asyncModule) {
        GWT.runAsync(new RunAsyncCallback() {

            @Override
            public void onFailure(Throwable reason) {
                asyncModule.onFail();
            }

            @Override
            public void onSuccess() {
                if (instance == null) {
                    instance = new EventMenuContent();
                }
                asyncModule.onSuccess(instance);
            }
        });
    }

    @Override
    public void onCreatedNewEvent(NewEventCreated event) {
        showItem(MyEventsMenu.NAME);
    }
}
