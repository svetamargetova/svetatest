package consys.admin.event.gwt.client.action;

import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientEventType;

/**
 * Vytvoreni akcie.
 * @return vracia UUID eventu
 * 
 * @author pepa
 */
public class CreateEventAction implements Action<StringResult> {

    private static final long serialVersionUID = -2033915070480413097L;
    private String acronym;
    private String fullname;
    private Integer countryId;
    private String timeZone;
    private ClientEventType type = ClientEventType.CONFERENCE;
    private int year;
    private String prefix;

    public CreateEventAction() {
    }

    public CreateEventAction(String acronym, String fullname, Integer countryId, int year, String prefix) {
        this.acronym = acronym;
        this.fullname = fullname;
        this.countryId = countryId;
        this.year = year;
        this.prefix = prefix;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getFullname() {
        return fullname;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public int getYear() {
        return year;
    }

    public ClientEventType getType() {
        return type;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
