package consys.admin.event.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientEventThumb;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author Palo
 */
public class EventListResult implements Result {
    private static final long serialVersionUID = 2716999679539308265L;

    private ArrayList<ClientEventThumb> list;
    private boolean hasMore;

    public EventListResult() {
        list = new ArrayList<ClientEventThumb>();
    }

    public EventListResult(ArrayList<ClientEventThumb> list, boolean hasMore) {
        this.list = list;
        this.hasMore = hasMore;
    }

    

    /**
     * @return the list
     */
    public ArrayList<ClientEventThumb> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(ArrayList<ClientEventThumb> list) {
        this.list = list;
    }

    /**
     * @return the hasMore
     */
    public boolean isHasMore() {
        return hasMore;
    }

    /**
     * @param hasMore the hasMore to set
     */
    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

}
