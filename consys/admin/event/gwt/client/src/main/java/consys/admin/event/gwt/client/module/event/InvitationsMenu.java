package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.event.gwt.client.action.ListMyEventInvitationsAction;
import consys.admin.event.gwt.client.bo.EventInvitationListResult;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.admin.event.gwt.client.utils.AdminEventResources;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Eventy, do kterych je uzivatel pozvany a zatim se nezaregistroval nebo nebyl zaregistrovan
 * @author pepa
 */
public class InvitationsMenu extends SimpleFormPanel implements MenuFormItem, InvitationsPanel.Delegate {

    // konstanty
    public static final String NAME = AEMessageUtils.c.invitationsMenu_title_invitations();
    // komponenty
    private InvitationsPanel invitationsPanel;
    // data
    private boolean isActive = true;
    private EventFilterSelector selected;

    public InvitationsMenu() {
        AdminEventResources.INSTANCE.css().ensureInjected();

        final EventFilterSelector active = new EventFilterSelector(true, "active",
                AEMessageUtils.c.invitationsMenu_action_active()) {

            @Override
            public void fireSelect(EventFilterSelector selector) {
                dataAction(getTag());
            }
        };
        active.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                active.selected();
                if (selected != active) {
                    selected.unselected();
                }
                selected = active;
            }
        });
        selected = active;
        final EventFilterSelector inactive = new EventFilterSelector(false, "inactive",
                AEMessageUtils.c.invitationsMenu_action_inactive()) {

            @Override
            public void fireSelect(EventFilterSelector selector) {
                dataAction(getTag());
            }
        };
        inactive.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                inactive.selected();
                if (selected != inactive) {
                    selected.unselected();
                }
                selected = inactive;
            }
        });

        addWidget(StyleUtils.getStyledLabel(AEMessageUtils.c.myEventsMenu_text_show() + ":", AdminEventResources.INSTANCE.css().show()));
        addWidget(active);
        addWidget(StyleUtils.getStyledLabel("|", AdminEventResources.INSTANCE.css().showSeparator()));
        addWidget(inactive);
        addWidget(StyleUtils.clearDiv());

        invitationsPanel = new InvitationsPanel(this);
        addWidget(invitationsPanel);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.setWidget(this);
    }

    @Override
    public void loadNext(int from, int limit, AbstractEventsPanel panel) {
        EventBus.get().fireEvent(new DispatchEvent(new ListMyEventInvitationsAction(isActive, from, limit),
                new AsyncCallback<EventInvitationListResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava actionExecutor
                    }

                    @Override
                    public void onSuccess(EventInvitationListResult result) {
                        invitationsPanel.setItems(result.getInvitations(), false);
                    }
                }, invitationsPanel));
    }

    /** zmena parametru action pro nacteni seznamu + vyvolani nacteni */
    private void dataAction(String tag) {
        isActive = tag.equals("active");
        invitationsPanel.resetAndLoadFirstPage();
    }
}
