package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb;
import consys.admin.event.gwt.client.bo.InvitationState;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.ServletConstants;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientEventThumb;
import consys.common.constants.img.EventLogoImageEnum;

/**
 *
 * @author pepa
 */
public class InvitationsItem extends Composite {

    // konstanty
    private static final String WIDTH = "227px";
    // komponenty
    private FocusPanel focusPanel;
    private FlowPanel content;
    // data
    private InvitationsPanel parent;

    public InvitationsItem(InvitationThumb thumb, InvitationsPanel parent) {
        this.parent = parent;

        content = new FlowPanel();
        content.addStyleName(StyleUtils.BORDER_LIGHT_GRAY);
        content.add(logo(thumb.getEvent()));
        content.add(StyleUtils.getStyledLabel(thumb.getEvent().getFullName(), StyleUtils.MARGIN_HOR_3, StyleUtils.MARGIN_VER_5,
                StyleUtils.FONT_BOLD, StyleUtils.FONT_11PX, StyleUtils.ALIGN_LEFT));
        content.add(StyleUtils.getStyledLabel(AEMessageUtils.c.invitationsItem_text_state() + ":",
                StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_HOR_3, StyleUtils.FONT_11PX));

        content.add(StyleUtils.getStyledLabel(getStateString(thumb.getState()), StyleUtils.FLOAT_LEFT, StyleUtils.FONT_11PX));
        content.add(StyleUtils.clearDiv("5px"));

        focusPanel = new FocusPanel();
        focusPanel.setWidth(WIDTH);
        focusPanel.addStyleName(StyleUtils.HAND);
        focusPanel.setWidget(content);
        initWidget(focusPanel);

        sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        initHandlers(thumb);
    }

    /** zinicializuje handlery po najeti nad polozku a po odjeti z polozky */
    private void initHandlers(final InvitationThumb thumb) {
        addHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                content.addStyleName(StyleUtils.BORDER_GRAY);
                content.removeStyleName(StyleUtils.BORDER_LIGHT_GRAY);
            }
        }, MouseOverEvent.getType());
        addHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                content.addStyleName(StyleUtils.BORDER_LIGHT_GRAY);
                content.removeStyleName(StyleUtils.BORDER_GRAY);
            }
        }, MouseOutEvent.getType());
        focusPanel.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent cevent) {
                InvitationDetailDialog d = new InvitationDetailDialog(thumb.getUuid(), parent);
                d.showCentered();
            }
        });
    }

    /** vraci lokalizovany text stavu */
    public static String getStateString(InvitationState state) {
        switch (state) {
            case ACCEPTED:
                return AEMessageUtils.c.invitationsItem_text_accepted();
            case DECLINED:
                return AEMessageUtils.c.invitationsItem_text_declined();
            case NO_RESPONSE:
                return AEMessageUtils.c.invitationsItem_text_notResponse();
            case TIMEOUTED:
                return AEMessageUtils.c.invitationsItem_text_timeouted();
            default:
                return AEMessageUtils.c.invitationsItem_text_unknownState();
        }
    }

    private SimplePanel logo(ClientEventThumb event) {
        Image logo = new Image();
        if (event.getLogoUuidPrefix() == null) {
            logo.setResource(ResourceUtils.system().confLogoSmall());
        } else {
            logo.setUrl(ServletConstants.BASE_URL + ServletConstants.EVENT_LOGO_IMG_LOAD
                    + EventLogoImageEnum.LIST.getFullUuid(event.getLogoUuidPrefix()));
        }

        SimplePanel panel = new SimplePanel();
        panel.addStyleName(StyleUtils.ALIGN_LEFT);
        panel.setWidget(logo);
        return panel;
    }
}
