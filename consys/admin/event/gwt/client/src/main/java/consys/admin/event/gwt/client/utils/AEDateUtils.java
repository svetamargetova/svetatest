package consys.admin.event.gwt.client.utils;

import consys.common.gwt.client.ui.comp.MonthYearDatePicker;
import consys.common.gwt.client.ui.utils.FormUtils;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class AEDateUtils {

    /**
     * vytvoří string s datumem konání eventu ve formátu "měsíc, od-do"
     * @param withYear je přepínač na volbu zda má být zobrazen i rok
     * @return string s datam, pokud je datum od nebo do null vraci prazdny retezec
     */
    public static String getEventDate(Date from, Date to, boolean withYear) {
        String result = "";
        if (from != null && to != null) {
            int fromMonth = from.getMonth();
            int toMonth = to.getMonth();
            if (fromMonth == toMonth) {
                result += MonthYearDatePicker.MONTHS[fromMonth];
                result += FormUtils.UNB_SPACE + from.getDate() + FormUtils.NDASH + to.getDate();
            } else {
                result += MonthYearDatePicker.MONTHS[fromMonth] + " " + from.getDate();
                result += FormUtils.UNB_SPACE + FormUtils.NDASH + FormUtils.UNB_SPACE;
                result += MonthYearDatePicker.MONTHS[toMonth] + " " + from.getDate();
            }
            if (withYear) {
                int fromYear = from.getYear();
                int toYear = to.getYear();
                // korekce datumu
                fromYear = fromYear < 2000 && fromYear > 100 ? fromYear - 100 + 2000 : fromYear;
                toYear = toYear < 2000 && toYear > 100 ? toYear - 100 + 2000 : toYear;
                result += ", " + (fromYear == toYear ? fromYear : fromYear + FormUtils.NDASH + toYear);
            }
        }
        return result;
    }
}
