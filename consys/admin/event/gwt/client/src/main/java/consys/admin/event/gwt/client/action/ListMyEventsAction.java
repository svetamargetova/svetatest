package consys.admin.event.gwt.client.action;

import consys.admin.event.gwt.client.bo.EventListResult;
import consys.common.gwt.shared.action.Action;

/**
 *
 * @author Palo
 */
public class ListMyEventsAction implements Action<EventListResult> {

    private static final long serialVersionUID = 980925696181664943L;
    // data
    private boolean upcoming;
    private int from;
    private int limit;
    private String prefix;

    public ListMyEventsAction() {
    }

    public ListMyEventsAction(boolean upcoming, int from, int limit, String prefix) {
        this.upcoming = upcoming;
        this.from = from;
        this.limit = limit;
        this.prefix = prefix;
    }

    public boolean isUpcoming() {
        return upcoming;
    }

    public int getFrom() {
        return from;
    }

    public int getLimit() {
        return limit;
    }

    public String getPrefix() {
        return prefix;
    }
}
