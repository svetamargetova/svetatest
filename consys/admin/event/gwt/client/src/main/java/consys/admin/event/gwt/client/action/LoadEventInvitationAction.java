package consys.admin.event.gwt.client.action;

import consys.admin.event.gwt.client.bo.EventInvitation;
import consys.common.gwt.shared.action.Action;

/**
 * Podla UUID nacita detail pozvanky do eventu.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadEventInvitationAction implements Action<EventInvitation> {
    private static final long serialVersionUID = 2748450332341088027L;

    private String uuid;

    public LoadEventInvitationAction() {
    }

    public LoadEventInvitationAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
