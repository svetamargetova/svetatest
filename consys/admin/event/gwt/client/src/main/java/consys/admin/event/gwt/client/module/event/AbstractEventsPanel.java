package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.StyleUtils;
import java.util.ArrayList;

/**
 *
 * @author Palo
 */
public abstract class AbstractEventsPanel<T extends Widget, D> extends FlexTable implements ActionExecutionDelegate {

    private int[] loadLimits;
    private int loadLimitIdx = -1;
    int row = 0;
    // data
    private Image waiting;
    private Delegate delegate;

    public AbstractEventsPanel(Delegate delegate) {
        this(delegate, new int[]{15, 24, 48});
    }

    public AbstractEventsPanel(Delegate delegate, int[] loadLimits) {
        if (loadLimits == null || loadLimits.length == 0) {
            throw new IllegalArgumentException("Musi byt nastaveny aspon jeden limit!");
        }
        this.loadLimits = loadLimits;
        this.delegate = delegate;
        setWidth("100%");
    }

    @Override
    protected void onLoad() {
        reset();
        loadNextPage();
    }

    private int nextLimit() {
        loadLimitIdx = loadLimits.length - 1 == loadLimitIdx ? loadLimitIdx : loadLimitIdx + 1;
        return loadLimits[loadLimitIdx];
    }

    private int getNextLimit() {
        int next = loadLimits.length - 1 == loadLimitIdx ? loadLimitIdx : loadLimitIdx + 1;
        return loadLimits[next];
    }

    public void reset() {
        this.clear(true);
        loadLimitIdx = -1;
        row = 0;
    }

    /** vlozeni polozek do panelu */
    public void setItems(ArrayList<D> items, boolean hasMore) {
        //reset();
        for (int itemIdx = 0; itemIdx < items.size(); itemIdx++) {
            T item = getNewItem(items.get(itemIdx));
            item.addStyleName(StyleUtils.MARGIN_BOT_20);
            int nrow = row + itemIdx / 3;
            int col = itemIdx % 3;
            setWidget(nrow, col, item);
            switch (col) {
                case 0:
                    getCellFormatter().setAlignment(nrow, col, HasAlignment.ALIGN_LEFT, HasAlignment.ALIGN_TOP);
                    break;
                case 1:
                    if (row == 0 && items.size() == 2) {
                        getCellFormatter().setAlignment(nrow, col, HasAlignment.ALIGN_LEFT, HasAlignment.ALIGN_TOP);
                        setWidth("");
                        item.addStyleName(StyleUtils.MARGIN_LEFT_10);
                    } else {
                        getCellFormatter().setAlignment(nrow, col, HasAlignment.ALIGN_CENTER, HasAlignment.ALIGN_TOP);
                    }
                    break;
                case 2:
                    getCellFormatter().setAlignment(nrow, col, HasAlignment.ALIGN_RIGHT, HasAlignment.ALIGN_TOP);
                    break;
            }
        }
        row += items.size() / 3;

        if (hasMore) {
            ActionLabel loadNextAction = new ActionLabel(false, AEMessageUtils.m.eventsPanel_action_loadNextEvents(
                    getNextLimit()), StyleUtils.Assemble(StyleUtils.FONT_BOLD, StyleUtils.TEXT_BLUE));
            setWidget(row + 1, 1, loadNextAction);
            getCellFormatter().setAlignment(row + 1, 1, HasAlignment.ALIGN_CENTER, HasAlignment.ALIGN_TOP);
            loadNextAction.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    loadNextPage();
                }
            });

        }
    }

    public void resetAndLoadFirstPage() {
        reset();
        loadNextPage();
    }

    private void loadNextPage() {
        delegate.loadNext(row * 3, nextLimit(), AbstractEventsPanel.this);
    }

    @Override
    public void actionStarted() {
        if (waiting == null) {
            waiting = new Image(Waiting.IMAGE_NAME);
        }
        setWidget(row + 1, 1, waiting);
        getCellFormatter().setAlignment(row + 1, 1, HasAlignment.ALIGN_CENTER, HasAlignment.ALIGN_TOP);
    }

    @Override
    public void actionEnds() {
        if (waiting != null) {
            waiting.removeFromParent();
        }
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
    }

    public abstract T getNewItem(D o);

    public interface Delegate {

        public void loadNext(int from, int limit, AbstractEventsPanel panel);
    }
}
