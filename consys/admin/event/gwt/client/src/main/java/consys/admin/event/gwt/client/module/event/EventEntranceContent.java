package consys.admin.event.gwt.client.module.event;

import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventEnterRequest;
import consys.common.gwt.client.rpc.action.cache.CurrentEventUserRightsCacheAction;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.HistoryUtils;
import java.util.HashMap;

/**
 * Panel ktory je zaregistrovany ako Event
 * @author pepa
 */
public class EventEntranceContent extends SimpleFormPanel {

    public EventEntranceContent() {
        super();
        showWaiting(true);
        HashMap<String, String> parameters = HistoryUtils.getParameters();
        Cache.get().unregister(CurrentEventUserRightsCacheAction.CURRENT_USER_EVENT_RIGHTS);
        Cache.get().clearSelectedTabs();
        // zalogovany uzivatel        
        EventEnterRequest event = new EventEnterRequest(parameters.get(EventEnterRequest.PARAM_EVENT_UUID));
        EventBus.get().fireEvent(event);
        showWaiting(false);
    }
}
