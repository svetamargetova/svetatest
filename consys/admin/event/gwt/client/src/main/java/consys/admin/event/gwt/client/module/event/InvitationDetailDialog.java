package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.admin.event.gwt.client.action.LoadEventInvitationAction;
import consys.admin.event.gwt.client.action.ResolveInvitationAction;
import consys.admin.event.gwt.client.bo.EventInvitation;
import consys.admin.event.gwt.client.bo.InvitationState;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImageBig;
import consys.common.gwt.client.ui.comp.ClientDate;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public class InvitationDetailDialog extends SmartDialog {

    // data
    private String invitationUuid;
    private InvitationsPanel parent;

    public InvitationDetailDialog(String invitationUuid, InvitationsPanel parent) {
        super();
        this.invitationUuid = invitationUuid;
        this.parent = parent;
    }

    @Override
    protected void generateContent(final SimpleFormPanel panel) {
        DOM.setStyleAttribute(panel.getElement(), "minHeight", "100px");
        EventBus.get().fireEvent(new DispatchEvent(new LoadEventInvitationAction(invitationUuid),
                new AsyncCallback<EventInvitation>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava actionExecutor
                    }

                    @Override
                    public void onSuccess(EventInvitation result) {
                        initContent(result, panel);
                    }
                }, this));
    }

    /** vygeneruje vlastni obsah podle dodanych dat */
    private void initContent(EventInvitation invitation, SimpleFormPanel panel) {
        panel.clear();
        panel.addWidget(closeCross());

        generateTexts(invitation, panel);

        panel.addWidget(controlPanel(invitation));
    }

    private void generateTexts(EventInvitation invitation, SimpleFormPanel panel) {
        String title = invitation.getEvent().getAcronym() + " " + invitation.getEvent().getYear();

        Label message = StyleUtils.getStyledLabel(invitation.getMessage(), StyleUtils.MARGIN_VER_5, StyleUtils.FLOAT_LEFT);
        message.setWidth("300px");

        panel.addWidget(StyleUtils.getStyledLabel(title, StyleUtils.MARGIN_LEFT_20, StyleUtils.FONT_16PX,
                StyleUtils.FONT_BOLD, StyleUtils.PADDING_VER_10));
        panel.addWidget(commonStyledLabel(AEMessageUtils.c.invitationDetailDialog_text_state()));
        panel.addWidget(StyleUtils.getStyledLabel(InvitationsItem.getStateString(invitation.getState()),
                StyleUtils.FLOAT_LEFT));
        panel.addWidget(StyleUtils.clearDiv("5px"));
        panel.addWidget(commonStyledLabel(AEMessageUtils.c.invitationDetailDialog_text_invited()));
        panel.addWidget(new ClientDate(invitation.getInvitationDate(), StyleUtils.FLOAT_LEFT));
        if (!canResponse(invitation.getState()) && invitation.getReponseDate() != null) {
            panel.addWidget(StyleUtils.clearDiv("5px"));
            panel.addWidget(commonStyledLabel(AEMessageUtils.c.invitationDetailDialog_text_responsed()));
            panel.addWidget(new ClientDate(invitation.getReponseDate(), StyleUtils.FLOAT_LEFT));
        }
        panel.addWidget(StyleUtils.clearDiv());
        panel.addWidget(StyleUtils.getStyledLabel(AEMessageUtils.c.invitationDetailDialog_text_message() + ":",
                StyleUtils.MARGIN_LEFT_20, StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_RIGHT_5, StyleUtils.MARGIN_VER_5));
        panel.addWidget(message);
        panel.addWidget(StyleUtils.clearDiv());
    }

    private Label commonStyledLabel(String text) {
        return StyleUtils.getStyledLabel(text + ":", StyleUtils.MARGIN_LEFT_20, StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_RIGHT_5);
    }

    private FlowPanel controlPanel(EventInvitation invitation) {
        final boolean canResponse = canResponse(invitation.getState());

        ActionImageBig acc = ActionImageBig.acceptButton();
        acc.setEnabled(canResponse);
        acc.addStyleName(StyleUtils.FLOAT_LEFT);
        acc.addStyleName(StyleUtils.MARGIN_RIGHT_20);
        acc.addClickHandler(resultClickHandler(true));

        ActionImageBig dec = ActionImageBig.declineButton();
        dec.setEnabled(canResponse);
        dec.addStyleName(StyleUtils.FLOAT_LEFT);
        dec.addClickHandler(resultClickHandler(false));

        FlowPanel cpanel = new FlowPanel();
        cpanel.addStyleName(StyleUtils.PADDING_VER_10);
        cpanel.addStyleName(StyleUtils.MARGIN_LEFT_20);
        cpanel.add(acc);
        cpanel.add(dec);
        cpanel.add(StyleUtils.clearDiv());

        return cpanel;
    }

    private boolean canResponse(InvitationState state) {
        return !(state.equals(InvitationState.ACCEPTED)
                || state.equals(InvitationState.DECLINED)
                || state.equals(InvitationState.TIMEOUTED));
    }

    private Image closeCross() {
        Image close = new Image(ResourceUtils.system().removeCross());
        close.setStyleName(StyleUtils.HAND);
        close.addStyleName(StyleUtils.FLOAT_RIGHT);
        close.addStyleName(StyleUtils.MARGIN_TOP_10);
        close.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        close.addClickHandler(hideClickHandler());
        return close;
    }

    private ClickHandler resultClickHandler(final boolean accept) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new DispatchEvent(new ResolveInvitationAction(invitationUuid, accept),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava actionExecutor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                hide();
                                parent.resetAndLoadFirstPage();
                            }
                        }, InvitationDetailDialog.this));
            }
        };
    }
}
