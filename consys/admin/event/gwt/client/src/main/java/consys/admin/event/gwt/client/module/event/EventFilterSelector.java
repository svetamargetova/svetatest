package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 *
 * @author pepa
 */
public abstract class EventFilterSelector extends SimplePanel {

    // data
    private String tag;
    private String text;

    public EventFilterSelector(boolean selected, String tag, String text) {
        this.tag = tag;
        this.text = text;
        setStyleName(CssStyles.INLINE);
        addStyleName(CssStyles.FLOAT_LEFT);
        if (!selected) {
            unselected();
        } else {
            StyleUtils.addStyleNameIfNotSet(this, ResourceUtils.system().css().dataListCommonOrdererOrderSelected());
        }
    }

    public void addClickHandler(ClickHandler handler) {
        addDomHandler(handler, ClickEvent.getType());
    }

    @Override
    protected void onLoad() {
        Label l = new Label(text);
        setWidget(l);
    }

    public void selected() {
        removeStyleName(ResourceUtils.system().css().dataListCommonOrdererOrder());
        StyleUtils.addStyleNameIfNotSet(this, ResourceUtils.system().css().dataListCommonOrdererOrderSelected());

        fireSelect(this);
    }

    public void unselected() {
        removeStyleName(ResourceUtils.system().css().dataListCommonOrdererOrderSelected());
        StyleUtils.addStyleNameIfNotSet(this, ResourceUtils.system().css().dataListCommonOrdererOrder());
    }

    public String getTag() {
        return tag;
    }

    public abstract void fireSelect(EventFilterSelector selector);
}
