package consys.admin.event.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.admin.event.gwt.client.message.AdminEventConstants;
import consys.admin.event.gwt.client.message.AdminEventMessages;

/**
 *
 * @author pepa
 */
public class AEMessageUtils {

    /** systemove konstanty modulu admin user */
    public static final AdminEventConstants c = (AdminEventConstants) GWT.create(AdminEventConstants.class);

    public static final AdminEventMessages m = (AdminEventMessages) GWT.create(AdminEventMessages.class);
}
