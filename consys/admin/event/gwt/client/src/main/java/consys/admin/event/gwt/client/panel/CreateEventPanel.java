package consys.admin.event.gwt.client.panel;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.admin.event.gwt.client.module.event.CreateEventForm;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.utils.CssStyles;

/**
 * Polozka v Layout Panelu, ktera se stara o prihlaseni uzivatele
 * @author pepa
 */
public class CreateEventPanel extends PanelItem implements CssStyles {

    // konstanty
    public static final String PANEL_NAME = "CreateEventPanel";
    
    
    public CreateEventPanel() {
        super(false); 
    }

    @Override
    public String getName() {
        return PANEL_NAME;
    }

    @Override
    public Widget getTitle() {
        return null;
    }

    @Override
    public Widget getContent() {
        SimplePanel panel = new SimplePanel();
        panel.setHeight("100px");
        ActionImage addEvent = ActionImage.getPlusButton(AEMessageUtils.c.const_addEvent(), true);
        addEvent.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                History.newItem(CreateEventForm.CREATE_EVENT_TOKEN);
            }
        });        
        addEvent.addStyleName(CssStyles.MARGIN_LEFT_20);
        panel.setWidget(addEvent);
        return panel;
    }
}
