package consys.admin.event.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

/**
 * Vyhodnotenie aktivnej pozvanky.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ResolveInvitationAction implements Action<VoidResult> {
    private static final long serialVersionUID = 5978691820456796140L;

    private String uuid;
    private boolean accept;

    public ResolveInvitationAction() {
    }

    public ResolveInvitationAction(String uuid, boolean accept) {
        this.uuid = uuid;
        this.accept = accept;
    }

    public String getInvitationUuid() {
        return uuid;
    }

    public boolean isInvitationAccepted() {
        return accept;
    }
}
