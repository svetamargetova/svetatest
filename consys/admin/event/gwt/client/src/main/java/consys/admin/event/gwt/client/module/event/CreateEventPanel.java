package consys.admin.event.gwt.client.module.event;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.admin.event.gwt.client.action.CreateEventAction;
import consys.admin.event.gwt.client.event.NewEventCreated;
import consys.admin.event.gwt.client.module.EventModule;
import consys.admin.event.gwt.client.utils.AEMessageUtils;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.AddressForm;
import consys.common.gwt.client.ui.comp.ConsysIntTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.ChangeValueEvent.Handler;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.CorporateUtils;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.TimeZoneProvider;
import consys.common.gwt.client.utils.TimeZoneUtils;
import java.util.ArrayList;
import java.util.Date;

/**
 * Formular pro pridani eventu
 * @author pepa
 */
public class CreateEventPanel extends SimpleFormWithHelpPanel {

    // konstanty
    public static final String NAME = AEMessageUtils.c.const_addEvent();
    // komponenty
    private Form form;
    private ConsysStringTextBox acronymBox;
    private ConsysIntTextBox yearBox;
    private ConsysStringTextBox fullNameBox;
    private SelectBox<Integer> countryBox;
    private SelectBox<String> timeZoneBox;

    public CreateEventPanel() {
        super(false);
        init();
        //addHelpTitle("<help_title>");
        //addHelpText("Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. ");
    }

    @Override
    protected void onLoad() {
        EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava event exectuor
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
                for (BoxItem e : result.getList()) {
                    out.add(new SelectBoxItem<Integer>(e.getId(), e.getName()));
                }
                countryBox.setEnabled(false);
                countryBox.setItems(out);
                AddressForm.initCountryBox(countryBox, new ConsysAction() {
                    @Override
                    public void run() {
                        TimeZone[] zones = TimeZoneProvider.getZonesForCountryById(countryBox.getSelectedItem().getItem());
                        initZoneBox(zones);
                    }
                });
            }
        }, this));
    }

    /** zinicializuje podcasti zalozky */
    private void init() {
        form = new Form();

        // akronym ackie
        acronymBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.const_acronym());
        acronymBox.setMaxLength(128);

        // plny nazev akce
        fullNameBox = new ConsysStringTextBox(1, 128, AEMessageUtils.c.createEventPanel_form_fullName());
        fullNameBox.setMaxLength(256);

        // Stat v kterem sa bude event konat
        timeZoneBox = new SelectBox<String>();
        countryBox = new SelectBox<Integer>();
        countryBox.addChangeValueHandler(new Handler<SelectBoxItem<Integer>>() {
            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                LoggerFactory.log(this.getClass(), "Country: " + event.getValue().getItem());
                TimeZone[] zones = TimeZoneProvider.getZonesForCountryById(event.getValue().getItem());
                initZoneBox(zones);
            }
        });

        int year = DateTimeUtils.getYear(new Date());
        yearBox = new ConsysIntTextBox("60px", year, 2061, UIMessageUtils.c.const_date_year());
        yearBox.setMaxLength(4);
        yearBox.setText(String.valueOf(year));

        form.addRequired(0, AEMessageUtils.c.createEventPanel_form_fullName(), fullNameBox);
        form.addRequired(1, UIMessageUtils.c.const_acronym(), acronymBox);
        form.addRequired(2, UIMessageUtils.c.const_address_location(), countryBox);
        form.addRequired(3, UIMessageUtils.c.const_date_year(), yearBox);

        ActionImage button = ActionImage.getConfirmButton(AEMessageUtils.c.createEventPanel_button_addEvent());
        button.addClickHandler(confirmClickHandler());

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                goBack();
            }
        });
        form.addActionMembers(4, button, cancel);
        addWidget(form);
    }

    private void goBack() {
        History.newItem(EventModule.EVENTS);
    }

    private void initZoneBox(TimeZone[] zones) {
        // nenasli sme casovu zonu takze ukazeme dalsi select
        if (zones == null || zones.length == 0) {
            initTimeZoneBox(TimeZoneUtils.toIdStringList(TimeZoneProvider.getTimeZonesNames()), AEMessageUtils.m.createEventPanel_info_noTimeZone(countryBox.getSelectedItem().getName()), true);
        } // je vicej casovych zon takze sa musi specifikovat
        else if (zones.length > 1) {
            initTimeZoneBox(TimeZoneUtils.toIdStringArray(zones), AEMessageUtils.m.createEventPanel_info_manyTimeZones(countryBox.getSelectedItem().getName()), true);
        } else {
            initTimeZoneBox(TimeZoneUtils.toIdStringArray(zones), null, false);
        }
    }

    private void initTimeZoneBox(String[] zones, String text, boolean show) {
        // initialize timezone box
        ArrayList<SelectBoxItem<String>> boxItems = new ArrayList<SelectBoxItem<String>>();
        for (int i = 0; i < zones.length; i++) {
            boxItems.add(new SelectBoxItem<String>(zones[i]));
        }
        timeZoneBox.setItems(boxItems);
        timeZoneBox.selectItem(zones[0]);

        if (timeZoneBox.isAttached()) {
            form.removeRow(3);
        }

        if (show) {
            FlowPanel panel = new FlowPanel();
            panel.add(timeZoneBox);
            panel.add(StyleUtils.getStyledLabel(text, StyleUtils.FONT_11PX, StyleUtils.TEXT_GRAY, StyleUtils.MARGIN_RIGHT_20));
            form.insertRequiredRowBeforeRow(3, UIMessageUtils.c.const_timeZone(), panel);
        }
    }

    /** 
     * ClickHandler potvrzujici vytvoreni
     */
    private ClickHandler confirmClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                clearMessageBox();

                if (form.validate(getFailMessage())) {
                    int year = yearBox.getTextInt();
                    String prefix = CorporateUtils.corporationPrefix();
                    final CreateEventAction action = new CreateEventAction(acronymBox.getText(), fullNameBox.getText(),
                            countryBox.getSelectedItem().getItem().intValue(), year, prefix);
                    // zistime casovu zonu klienta
                    action.setTimeZone(timeZoneBox.getSelectedItem().getItem());
                    EventBus.get().fireEvent(new DispatchEvent(action, new AsyncCallback<StringResult>() {
                        @Override
                        public void onFailure(Throwable caught) {
                        }

                        @Override
                        public void onSuccess(StringResult result) {
                            NewEventCreated event = new NewEventCreated(action, result.getStringResult());
                            EventBus.get().fireEvent(event);
                        }
                    }, getSelf()));
                }
            }
        };
    }
}
