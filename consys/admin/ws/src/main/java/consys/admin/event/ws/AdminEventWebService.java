package consys.admin.event.ws;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface AdminEventWebService {

    public final static String NAMESPACE = "http://takeplace.eu/admin/event";

    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public LoadUsersToEventResponse loadUserToEvent(LoadUsersToEventRequest request);

    public UpdateUserRelationToEventResponse updateUserRelationToEvent(UpdateUserRelationToEventRequest request);

    public LoadEventOverseerResponse loadEventOverseer(LoadEventOverseerRequest request);

    
    


}
