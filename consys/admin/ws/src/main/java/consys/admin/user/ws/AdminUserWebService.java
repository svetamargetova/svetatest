package consys.admin.user.ws;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface AdminUserWebService {

    public final static String NAMESPACE = "http://takeplace.eu/admin/user";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public LoadUserEmailResponse loadUserEmail(LoadUserEmailRequest request);
}
