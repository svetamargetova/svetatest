//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.01 at 04:53:34 PM CET 
//


package consys.admin.event.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * 
 *                         Upravi relaciu uzivatela vhladom k eventu.
 *                         
 * 
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userUuids",
    "eventUuid",
    "newRelation",
    "message"
})
@XmlRootElement(name = "UpdateUserRelationToEventRequest")
public class UpdateUserRelationToEventRequest
    implements Equals, HashCode, ToString
{

    @XmlList
    @XmlElement(required = true)
    protected List<String> userUuids;
    @XmlElement(required = true)
    protected String eventUuid;
    @XmlElement(required = true)
    protected Relation newRelation;
    @XmlElement(required = true, nillable = true)
    protected String message;

    /**
     * Gets the value of the userUuids property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userUuids property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserUuids().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getUserUuids() {
        if (userUuids == null) {
            userUuids = new ArrayList<String>();
        }
        return this.userUuids;
    }

    /**
     * Gets the value of the eventUuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * Sets the value of the eventUuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventUuid(String value) {
        this.eventUuid = value;
    }

    /**
     * Gets the value of the newRelation property.
     * 
     * @return
     *     possible object is
     *     {@link Relation }
     *     
     */
    public Relation getNewRelation() {
        return newRelation;
    }

    /**
     * Sets the value of the newRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Relation }
     *     
     */
    public void setNewRelation(Relation value) {
        this.newRelation = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<String> theUserUuids;
            theUserUuids = this.getUserUuids();
            strategy.appendField(locator, this, "userUuids", buffer, theUserUuids);
        }
        {
            String theEventUuid;
            theEventUuid = this.getEventUuid();
            strategy.appendField(locator, this, "eventUuid", buffer, theEventUuid);
        }
        {
            Relation theNewRelation;
            theNewRelation = this.getNewRelation();
            strategy.appendField(locator, this, "newRelation", buffer, theNewRelation);
        }
        {
            String theMessage;
            theMessage = this.getMessage();
            strategy.appendField(locator, this, "message", buffer, theMessage);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof UpdateUserRelationToEventRequest)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final UpdateUserRelationToEventRequest that = ((UpdateUserRelationToEventRequest) object);
        {
            List<String> lhsUserUuids;
            lhsUserUuids = this.getUserUuids();
            List<String> rhsUserUuids;
            rhsUserUuids = that.getUserUuids();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "userUuids", lhsUserUuids), LocatorUtils.property(thatLocator, "userUuids", rhsUserUuids), lhsUserUuids, rhsUserUuids)) {
                return false;
            }
        }
        {
            String lhsEventUuid;
            lhsEventUuid = this.getEventUuid();
            String rhsEventUuid;
            rhsEventUuid = that.getEventUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "eventUuid", lhsEventUuid), LocatorUtils.property(thatLocator, "eventUuid", rhsEventUuid), lhsEventUuid, rhsEventUuid)) {
                return false;
            }
        }
        {
            Relation lhsNewRelation;
            lhsNewRelation = this.getNewRelation();
            Relation rhsNewRelation;
            rhsNewRelation = that.getNewRelation();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "newRelation", lhsNewRelation), LocatorUtils.property(thatLocator, "newRelation", rhsNewRelation), lhsNewRelation, rhsNewRelation)) {
                return false;
            }
        }
        {
            String lhsMessage;
            lhsMessage = this.getMessage();
            String rhsMessage;
            rhsMessage = that.getMessage();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "message", lhsMessage), LocatorUtils.property(thatLocator, "message", rhsMessage), lhsMessage, rhsMessage)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<String> theUserUuids;
            theUserUuids = this.getUserUuids();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "userUuids", theUserUuids), currentHashCode, theUserUuids);
        }
        {
            String theEventUuid;
            theEventUuid = this.getEventUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "eventUuid", theEventUuid), currentHashCode, theEventUuid);
        }
        {
            Relation theNewRelation;
            theNewRelation = this.getNewRelation();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "newRelation", theNewRelation), currentHashCode, theNewRelation);
        }
        {
            String theMessage;
            theMessage = this.getMessage();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "message", theMessage), currentHashCode, theMessage);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
