
Takeplace sluzba pre vyhladavanie
SOLR 4.1
 
=============================
Obsah tejto zlozky sa musi nahrat na miesto kde bude ulozena konfiguracia pre 
SOLR 4.1


Basic Directory Structure
-------------------------

   tomcat/ 
        Obsahuje cestu kam sa ma vlozit solr.xml  ktore definuje cestu ku
        konfiguracii. 

   conf/
        This directory is mandatory and must contain your solrconfig.xml
        and schema.xml.  Any other optional configuration files would also 
        be kept here.
        Nastavenie UMIESTNENIA DAT sa robi v solrconfig -> dataDir

   data/
        This directory is the default location where Solr will keep your
        index, and is used by the replication scripts for dealing with
        snapshots.  You can override this location in the solrconfig.xml
        and scripts.conf files. Solr will create this directory if it
        does not already exist.

   lib/
        This directory is optional.  If it exists, Solr will load any Jars
        found in this directory and use them to resolve any "plugins"
        specified in your solrconfig.xml or schema.xml (ie: Analyzers,
        Request Handlers, etc...).  Alternatively you can use the <lib>
        syntax in solrconfig.xml to direct Solr to your plugins.  See the
        example solrconfig.xml file for details.

   bin/
        This directory is optional.  It is the default location used for
        keeping the replication scripts.
  
