/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.core.solr.schema;

/**
 *
 * @author palo
 */
public interface SolrSchemaProperties {
    
    public static final String UUID = "uuid";    
    public static final String USER_NAME = "user_name";
    public static final String EVENT_NAME = "event_name";    
    public static final String EMAIL = "email";    
    public static final String TAGS = "tags";    
    public static final String ORGANIZATION = "organization";
}
