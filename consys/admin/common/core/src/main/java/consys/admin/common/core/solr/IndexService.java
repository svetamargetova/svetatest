/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.common.core.solr;

import java.util.Map;

/**
 * Servica ktora spracuva indexovanie.
 * @author palo
 */
public interface IndexService {

    public void doIndex(Map<String,String> attributes);

}
