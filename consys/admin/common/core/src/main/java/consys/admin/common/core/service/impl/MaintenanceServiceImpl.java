/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.core.service.impl;

import consys.admin.common.core.dao.MaintenanceDao;
import consys.admin.common.core.service.MaintenanceService;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author palo
 */
public class MaintenanceServiceImpl implements MaintenanceService{

    private MaintenanceDao maintenanceDao;
    
    @Override
    public void updateDatabase(String sql) {
        if(StringUtils.isBlank(sql)){
            throw new NullPointerException("Missing SQL");
        }
        maintenanceDao.executeSql(sql);
    }

    /**
     * @return the maintenanceDao
     */
    public MaintenanceDao getMaintenanceDao() {
        return maintenanceDao;
    }

    /**
     * @param maintenanceDao the maintenanceDao to set
     */
    public void setMaintenanceDao(MaintenanceDao maintenanceDao) {
        this.maintenanceDao = maintenanceDao;
    }
    
}
