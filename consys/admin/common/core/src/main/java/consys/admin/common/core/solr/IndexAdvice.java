package consys.admin.common.core.solr;

import consys.common.core.bo.ConsysObject;
import java.lang.reflect.Method;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.AfterReturningAdvice;

/**
 * Zistuje nad metodami ci sa jedna o update, save, delete a na tom zakalde sa zachova.
 * @author Palo
 */
public class IndexAdvice implements AfterReturningAdvice {

    private static final Logger logger = LoggerFactory.getLogger(IndexAdvice.class);
    private static final String SAVE_METHOD = "save";
    private static final String UPDATE_METHOD = "update";
    private static final String DELETE_METHOD = "delete";
    private IndexService indexService;
    private IndexHandlerRegistry handlerRegistry;

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        if (args.length > 0 && args[0] instanceof ConsysObject) {
            ConsysObject o = (ConsysObject) args[0];
            if (SAVE_METHOD.equalsIgnoreCase(method.getName()) || UPDATE_METHOD.equalsIgnoreCase(method.getName())) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Save or Update method checking whether to index.");
                }
                IndexHandler ih = handlerRegistry.getHandlerForClass(o.getClass());
                if (ih == null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("IndexHandler for class not found.");
                    }
                    return;
                }
                indexService.doIndex(ih.createDoc(o));
            }
        }
    }

    /**
     * @return the indexService
     */
    public IndexService getIndexService() {
        return indexService;
    }

    /**
     * @param indexService the indexService to set
     */
    public void setIndexService(IndexService indexService) {
        this.indexService = indexService;
    }

    /**
     * @return the handlerRegistry
     */
    public IndexHandlerRegistry getHandlerRegistry() {
        return handlerRegistry;
    }

    /**
     * @param handlerRegistry the handlerRegistry to set
     */
    public void setHandlerRegistry(IndexHandlerRegistry handlerRegistry) {
        this.handlerRegistry = handlerRegistry;
    }
}
