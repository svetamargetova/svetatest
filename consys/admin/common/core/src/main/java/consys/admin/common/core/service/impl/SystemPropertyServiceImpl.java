/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.core.service.impl;

import consys.common.core.dao.SystemPropertyDao;
import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.impl.AbstractService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Servica si drzi v pamati vsetky systemove properties a na vyziadanie ich
 * znova nacita. Treba respektovat defaultne hodnoty danych properties
 * <p/>
 * @author Palo
 */
public class SystemPropertyServiceImpl extends AbstractService implements SystemPropertyService {

    private SystemPropertyDao dao;
    // konstanty
    public static final String DEFAULT_EMPTY_STRING = "";
    public static final Integer DEFAULT_EMPTY_INTEGER = 0;
    public static final Boolean DEFAULT_EMPTY_BOOLEAN = Boolean.FALSE;
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ss";

    @Override
    public String getString(String key) {
        String value = getValue(key);
        return value == null ? DEFAULT_EMPTY_STRING : value;
    }

    @Override
    public Integer getInteger(String key) {
        String value = getValue(key);
        return value == null ? DEFAULT_EMPTY_INTEGER : Integer.valueOf(value);
    }

    @Override
    public Boolean getBoolean(String key) {
        String value = getValue(key);
        return value == null ? DEFAULT_EMPTY_BOOLEAN : Boolean.valueOf(value);
    }

    @Override
    public Date getDate(String key) {
        String value = getValue(key);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT);
            return value == null ? null : sdf.parse(value);
        } catch (ParseException ex) {
            throw new RuntimeException("Can't parse string!", ex);
        }
    }

    @Override
    public void createStringProperty(String key, String value) {
        setValue(key, value);
    }

    @Override
    public synchronized void createIntegerProperty(String key, Integer value) {
        setValue(key, Integer.toString(value));
    }

    @Override
    public synchronized void createBooleanProperty(String key, Boolean value) {
        setValue(key, Boolean.toString(value));
    }

    @Override
    public void createDateProperty(String key, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT);
        setValue(key, sdf.format(date));
    }

    private void setValue(String key, String value) {
        SystemProperty s = new SystemProperty();
        s.setKey(key);
        s.setValue(value);
        dao.create(s);
    }

    private String getValue(String key) {
        try {
            SystemProperty p = dao.loadByKey(key);
            return p.getValue();
        } catch (NoRecordException ex) {
            log().info("Property key " + key + " not in database!");
            return null;
        }
    }

    @Override
    public List<SystemProperty> listProperiesToClient() {
        return dao.listByToClient(true);
    }

    @Override
    public List<SystemProperty> listAllProperties() {
        return dao.listAll(SystemProperty.class);
    }

    @Override
    public void updateStringProperty(String key, String value) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        s.setValue(value);
        dao.update(s);
    }

    @Override
    public void updateIntegerProperty(String key, Integer value) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        if (value == null) {
            s.setValue("0");
        } else {
            s.setValue(Integer.toString(value));
        }
        dao.update(s);
    }

    @Override
    public void updateBooleanProperty(String key, Boolean value) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        s.setValueFromBoolean(value);
        dao.update(s);
    }

    @Override
    public SystemProperty loadSystemProperty(String key) throws NoRecordException {
        return dao.loadByKey(key);
    }

    @Override
    public void updateProperty(String key, String newValue) throws IllegalPropertyValue, NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        if (newValue == null) {
            s.setValue("");
        } else {
            s.setValue(newValue);
        }
        dao.update(s);
    }

    @Override
    public void updateDateProperty(String key, Date date) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT);
        s.setValue(sdf.format(date));
        dao.update(s);
    }

    /**
     * @return the dao
     */
    public SystemPropertyDao getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(SystemPropertyDao dao) {
        this.dao = dao;
    }

    @Override
    public void updateProperties(Map<String, Object> properties) throws IllegalPropertyValue, NoRecordException {
        for (Map.Entry<String, Object> entry : properties.entrySet()) {            
            if (entry.getValue() instanceof Date) {
                updateDateProperty(entry.getKey(), (Date) entry.getValue());
            } else if (entry.getValue() instanceof Integer) {
                updateIntegerProperty(entry.getKey(), (Integer) entry.getValue());
            } else if (entry.getValue() instanceof Boolean) {
                updateBooleanProperty(entry.getKey(), (Boolean) entry.getValue());
            } else {
                updateProperty(entry.getKey(), (String) entry.getValue());
            }

        }
    }

    @Override
    public List<SystemProperty> listProperties(List<String> keys) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
