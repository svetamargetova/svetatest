/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.core.dao.hibernate;

import consys.admin.common.core.dao.MaintenanceDao;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 *
 * @author palo
 */
public class MaintenanceDaoImpl extends HibernateDaoSupport implements MaintenanceDao{

    private static final Logger logger = LoggerFactory.getLogger(MaintenanceDaoImpl.class);
    
    
    @Override
    public void executeSql(final String sql) {
        logger.info("Executing SQL: {}",sql);
        getSession().doWork(new Work() {

            @Override
            public void execute(Connection connection) throws SQLException {
                try{
                Statement s = connection.createStatement();
                s.execute(sql);
                }catch(SQLException e){
                    logger.error("SQL ERROR: ",e);
                    throw e;
                }
            }
        });                
    }            
}
