package consys.admin.common.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SystemPropertyAutoInstallImpl implements ApplicationListener,SystemPropertyAutoInstall {

    private static final Logger logger = LoggerFactory.getLogger(SystemPropertyAutoInstallImpl.class);
    private Map<String, Object> properties;
    private String name;
    private SystemPropertyService systemPropertyService;
    private boolean installed;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the systemPropertyService
     */
    public SystemPropertyService getSystemPropertyService() {
        return systemPropertyService;
    }

    /**
     * @param systemPropertyService the systemPropertyService to set
     */
    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    /**
     * @return the properties
     */
    public Map<String, Object> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }

    @Override
    public void install() {
        if(installed){
            return;
        }
        
        if (properties == null) {
            return;
        }
        if (systemPropertyService == null) {
            throw new NullPointerException("SystemPropertyService is missing!");
        }
        if (logger.isInfoEnabled()) {
            logger.info("Installing properties: " + name);
        }
        for (Map.Entry<String, Object> e : properties.entrySet()) {
            try {
                SystemProperty sp = systemPropertyService.loadSystemProperty(e.getKey());
            } catch (NoRecordException ex) {                
                logger.info("   + {} : {}", e.getKey() , e.getValue().toString());
                
                if (e.getValue() instanceof Number) {
                    systemPropertyService.createIntegerProperty(e.getKey(), ((Number) e.getValue()).intValue());
                } else if (e.getValue() instanceof Boolean) {
                    systemPropertyService.createBooleanProperty(e.getKey(), (Boolean) e.getValue());
                } else if (e.getValue() instanceof String) {
                    systemPropertyService.createStringProperty(e.getKey(), (String) e.getValue());
                }
            }
        }
        installed = true;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextRefreshedEvent) {
            install();
        }
    }
}
