/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.common.core.dao.hibernate;

import consys.common.core.dao.SystemPropertyDao;
import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class SystemPropertyDaoImpl extends GenericDaoImpl<SystemProperty> implements SystemPropertyDao{

    @Override
    public SystemProperty load(Long id) throws NoRecordException {
        return load(id, SystemProperty.class);
    }


    @Override
    public SystemProperty loadByKey(String key) throws NoRecordException{
         
            String hql = "from SystemProperty p where p.key=:aKey";
            Query q = session().createQuery(hql);
            q.setString("aKey", key);
            SystemProperty p = (SystemProperty)q.uniqueResult();
            if(p == null){
                throw new NoRecordException("No system property for key:"+key);
            }
            return p;        
    }

    @Override
    public List<SystemProperty> listByToClient(boolean toClient) {
        return session().
                createQuery("from SystemProperty p where p.toClient = :TO_CLIENT").
                setBoolean("TO_CLIENT", toClient).
                list();
    }
}
