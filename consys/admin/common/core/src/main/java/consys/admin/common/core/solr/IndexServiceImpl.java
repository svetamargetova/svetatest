package consys.admin.common.core.solr;

import consys.admin.common.core.CommonSystemProperties;
import consys.common.core.service.SystemPropertyService;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public class IndexServiceImpl implements IndexService {

    private static final Logger logger = LoggerFactory.getLogger(IndexServiceImpl.class);
    // SolrServer is thread save
    private SolrServer solrServer;
    private SystemPropertyService propertyService;

    @Override
    public void doIndex(Map<String, String> attributes) {
        try {

            SolrInputDocument doc1 = new SolrInputDocument();
            for (Map.Entry<String, String> e : attributes.entrySet()) {
                doc1.addField(e.getKey(), e.getValue());
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Indexing: ");
                for (Map.Entry<String, String> e : attributes.entrySet()) {
                    logger.debug("   " + e.getKey() + " : " + e.getValue());
                }
            }

            SolrServer server = getSolrServer();
            server.add(doc1);
            server.commit();
            if (logger.isDebugEnabled()) {
                logger.debug("Indexing done");
            }
        } catch (SolrServerException ex) {
            logger.error("SolrServerException", ex);
        } catch (IOException ex) {
            logger.error("Exception", ex);
        }
    }

    private SolrServer getSolrServer() throws MalformedURLException, IOException {
        if (solrServer == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("SOLR Server not set yet. Starting..");
            }
            String address = getPropertyService().getString(CommonSystemProperties.SOLR_SERVER_ADDRESS);
            if (logger.isDebugEnabled()) {
                logger.debug("SOLR Server URL: " + address);
            }
            solrServer = new CommonsHttpSolrServer(address);
        }
        return solrServer;
    }

    /**
     * @return the propertyService
     */
    public SystemPropertyService getPropertyService() {
        return propertyService;
    }

    /**
     * @param propertyService the propertyService to set
     */
    public void setPropertyService(SystemPropertyService propertyService) {
        this.propertyService = propertyService;
    }
}
