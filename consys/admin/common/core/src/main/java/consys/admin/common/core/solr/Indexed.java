/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.common.core.solr;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Anotacia ktora zabezpeci ze entita bude indexovatelna. Musi sa ale nastavit
 * classa ktora zabezpeci spracovanie entity.
 * @author palo
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Indexed {
    /**
     * Handler class pro indexovanu entitu
     */
    Class handler();   
}
