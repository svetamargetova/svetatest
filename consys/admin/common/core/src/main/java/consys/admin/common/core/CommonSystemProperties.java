package consys.admin.common.core;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CommonSystemProperties {

    public static final String SOLR_SERVER_ADDRESS = "solr_server_address";

    public static final String ALLOW_LOGIN_TO_TAKEPLACE = "allow_login_to_takeplace";
}
