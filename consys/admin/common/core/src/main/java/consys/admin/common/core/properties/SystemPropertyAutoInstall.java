package consys.admin.common.core.properties;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SystemPropertyAutoInstall {

    public void install();
}
