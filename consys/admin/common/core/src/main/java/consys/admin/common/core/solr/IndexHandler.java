/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.common.core.solr;

import consys.admin.common.core.solr.schema.SolrSchemaProperties;
import consys.common.core.bo.ConsysObject;
import java.util.Map;

/**
 *
 * @author palo
 */
public interface IndexHandler<T extends ConsysObject>  extends SolrSchemaProperties{

    public Map<String,String> createDoc(T item);

}
