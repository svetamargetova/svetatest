package consys.admin.common.core.solr;

import consys.common.core.bo.ConsysObject;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.annotation.AnnotationUtils;

/**
 * Register vsetkych tried ktore maju anotaciu na Indexovanie.
 * @author Palo
 */
public class IndexHandlerRegistry implements ApplicationListener, ApplicationContextAware {

    private static final long serialVersionUID = -7970899473862094024L;
    // Konstatny atributov v Indexed anotacii
    private static final String INDEXER_HANDLER = "handler";    
    private static final Logger logger = LoggerFactory.getLogger(IndexHandlerRegistry.class);
    // Context
    private ApplicationContext ctx;
    // Data
    private Map<Class, IndexHandler> handlers;
    
    
    public IndexHandlerRegistry() {
        logger.info("Creating Handler Registry");        
        handlers = new HashMap<Class, IndexHandler>();
    }

    public int getRegisterCount() {
        return handlers.size();
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextRefreshedEvent ) {
            logger.info("Initializing indexer");            

            Map<String, ConsysObject> map = ctx.getBeansOfType(ConsysObject.class);            
            for (Map.Entry<String, ConsysObject> e : map.entrySet()) {
                ConsysObject co = e.getValue();
                logger.debug("  ... " + e.getKey());
                Annotation a = AnnotationUtils.findAnnotation(co.getClass(), Indexed.class);
                if (a != null) {
                    logger.debug("  "+co.getClass().getSimpleName()+" is annotated by Indexer");                    
                    Class handlerClass = (Class) AnnotationUtils.getValue(a, INDEXER_HANDLER);
                    try {
                        logger.info("   " + co.getClass().getSimpleName() + " <- " + handlerClass.getSimpleName());
                        IndexHandler ih = (IndexHandler) handlerClass.newInstance();                        
                        handlers.put(co.getClass(), ih);
                    } catch (InstantiationException ex) {
                        logger.error("Exception when creating index handler!", ex);
                    } catch (IllegalAccessException ex) {
                        logger.error("Exception when creating index handler!", ex);
                    }

                } else {
                    logger.debug("  ... is annotated by Indexer");
                }
            }           
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    public IndexHandler getHandlerForClass(Class clazz){
        return handlers.get(clazz);
    }
}
