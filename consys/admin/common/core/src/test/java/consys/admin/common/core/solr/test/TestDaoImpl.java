package consys.admin.common.core.solr.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public class TestDaoImpl implements TestDao{

    private static final Logger logger = LoggerFactory.getLogger(TestDaoImpl.class);

    @Override
    public void update(TestObject to) {
        logger.info("UPDATE");
    }

}
