package consys.admin.common.core.service;

import org.springframework.transaction.annotation.Transactional;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.admin.common.test.AbstractAdminDaoTestCase;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Palo
 */
@ContextConfiguration(locations={"../admin-common-spring.xml"})
public class SystemPropertyServiceTest extends AbstractAdminDaoTestCase{

    private SystemPropertyService service;


    @Test(groups = {"service"})
    @Transactional()
    public void testCreate() throws NoRecordException{

        assertNotNull(" Service null ",service);
        service.createBooleanProperty("bool", Boolean.TRUE);

        assertTrue("Neni boolean",service.getBoolean("bool"));

        Date d = DateProvider.getCurrentDate();
        service.createDateProperty("date", d);
        Date d1 = service.getDate("date");        
        Date d2 = DateUtils.addDays(d1, 5);
        service.updateDateProperty("date", d2);
        Date d2l = service.getDate("date");
        assertEquals(d2, d2l);

    }

    
    /**
     * @param service the service to set
     */
    @Autowired
    @Required
    public void setService(SystemPropertyService service) {
        this.service = service;
    }




    

}
