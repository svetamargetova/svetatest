package consys.admin.common.core.solr.test;

import consys.admin.common.core.solr.IndexHandler;
import consys.common.utils.collection.Maps;
import java.util.Map;

/**
 *
 * @author Palo
 */
public class TestHandler implements IndexHandler<TestObject>{

    @Override
    public Map<String, String> createDoc(TestObject item) {
        return Maps.newHashMap();
    }

}
