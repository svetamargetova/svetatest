package consys.admin.common.core.solr;

import org.springframework.transaction.annotation.Transactional;
import consys.admin.common.core.solr.test.TestDao;
import consys.admin.common.core.solr.test.TestObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import static  org.testng.Assert.*;

/**
 *
 * @author Palo
 */
@ContextConfiguration(locations={"solr-spring.xml"})
public class SearchInitializationTest extends AbstractTestNGSpringContextTests {

    private IndexHandlerRegistry registry;
    private TestDao dao;

    /**
     * @param registry the registry to set
     */
    @Autowired
    @Required
    public void setRegistry(IndexHandlerRegistry registry) {
        this.registry = registry;
    }

    @Autowired
    @Required
    public void setDao(TestDao dao) {
        this.dao = dao;
    }



    @Test(groups = {"search"})
    @Transactional()
    public void testRegistryCount(){
        assertNotNull(registry);
        assertTrue(registry.getRegisterCount()==1);
    }


    @Test(groups = {"search"})
    @Transactional()
    public void testIndexing(){
        dao.update(new TestObject());
    }




}
