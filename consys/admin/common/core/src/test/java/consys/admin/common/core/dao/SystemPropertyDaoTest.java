package consys.admin.common.core.dao;

import org.springframework.transaction.annotation.Transactional;
import consys.common.core.dao.SystemPropertyDao;
import consys.admin.common.test.AbstractAdminDaoTestCase;
import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Palo
 */
@ContextConfiguration(locations={"../admin-common-spring.xml"})
public class SystemPropertyDaoTest extends AbstractAdminDaoTestCase{

    private SystemPropertyDao dao;

    /**
     * @param dao the dao to set
     */
    @Autowired
    @Required
    public void setDao(SystemPropertyDao dao) {
        this.dao = dao;
    }

    @Test(groups = {"dao"})
    public void createSystemProperty(){
        SystemProperty sp = new SystemProperty();
        sp.setKey("keyd");
        sp.setValue("value");
        dao.create(sp);
    }

    @Test(groups = {"dao"})    
    public void loadByKey() throws NoRecordException{
        createSystemProperty();
        SystemProperty spo = new SystemProperty();
        spo.setKey("keyd");
        spo.setValue("value");
        SystemProperty sp = dao.loadByKey("keyd");
        assertEquals(sp, spo);
    }

}
