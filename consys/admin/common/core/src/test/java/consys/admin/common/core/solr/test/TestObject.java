package consys.admin.common.core.solr.test;

import consys.admin.common.core.solr.Indexed;
import consys.common.core.bo.ConsysObject;

/**
 *
 * @author Palo
 */
@Indexed(handler=TestHandler.class)
public class TestObject implements ConsysObject{
    private static final long serialVersionUID = 2255297019475785462L;

}
