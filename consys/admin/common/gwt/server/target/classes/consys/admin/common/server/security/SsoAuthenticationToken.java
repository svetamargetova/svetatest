/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.server.security;


import consys.common.utils.collection.Lists;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.GrantedAuthorityImpl;

/**
 *
 * @author palo
 */
public class SsoAuthenticationToken extends AbstractAuthenticationToken{

    
    private static final long serialVersionUID = 1L;
    
    private final String consysUserUuid;
    private final Long consysUserId;    
    
    
    public SsoAuthenticationToken(String userUuid, Long userId) {
        super(Lists.newArrayList(new GrantedAuthorityImpl("ROLE_USER")));                                        
        this.consysUserId = userId;
        this.consysUserUuid = userUuid;
        super.setAuthenticated(true);
    }

    public Long getConsysUserId() {
        return consysUserId;
    }

    public String getConsysUserUuid() {
        return consysUserUuid;
    }        

       
    //~ Methods ========================================================================================================

    @Override
    public Object getCredentials() {
        return this.consysUserId;
    }

    @Override
    public Object getPrincipal() {
        return "SSO";
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                "Cannot set this token to trusted - use constructor containing GrantedAuthority[]s instead");
        }

        super.setAuthenticated(true);
    }          
}
