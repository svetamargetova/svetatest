package consys.admin.common.gwt.server;


import consys.admin.common.gwt.server.session.UserSessionContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author Palo
 */
public class ContextProvider implements ApplicationContextAware{

    private ApplicationContext context;
    private String sessionCtxBeanName;

    public UserSessionContext getUserContext(){
        return (UserSessionContext) context.getBean(getSessionCtxBeanName(),UserSessionContext.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    /**
     * @return the sessionCtxBeanName
     */
    public String getSessionCtxBeanName() {        
        return sessionCtxBeanName;
    }

    /**
     * @param sessionCtxBeanName the sessionCtxBeanName to set
     */
    public void setSessionCtxBeanName(String sessionCtxBeanName) {
        this.sessionCtxBeanName = sessionCtxBeanName;
    }

}
