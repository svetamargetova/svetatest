package consys.admin.common.gwt.server.action.server.handler;

import consys.admin.common.gwt.server.ContextProvider;
import consys.admin.common.gwt.server.AdminActionHandler;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;
import consys.admin.common.gwt.server.session.UserSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Simple abstract super-class for {@link AdminActionHandler} implementations that
 * forces the {@link Action} class to be passed in as a constructor to the
 * handler. It's arguable if this is any simpler than just implementing the
 * {@link AdminActionHandler} and its {@link #getActionType()} directly.
 * 
 * @author David Peterson
 * 
 * @param <A>
 *            The {@link Action} implementation.
 * @param <R>
 *            The {@link Result} implementation.
 */
public abstract class AbstractRoleUserActionHandler<A extends Action<R>, R extends Result> implements RoleUserActionHandler<A, R>{

    private final Class<A> actionType;
    private ContextProvider contextProvider;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public AbstractRoleUserActionHandler( Class<A> actionType ) {
        this.actionType = actionType;
    }

    @Override
    public Class<A> getActionType() {
        return actionType;
    }

     
    public UserSessionContext getUserSessionContext() {
        return contextProvider.getUserContext();
    }

    /**
     * @return the contextProvider
     */
    public ContextProvider getContextProvider() {
        return contextProvider;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    @Autowired(required=true)
    public void setContextProvider(ContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }

}
