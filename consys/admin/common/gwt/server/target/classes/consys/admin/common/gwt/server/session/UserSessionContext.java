package consys.admin.common.gwt.server.session;

import java.io.Serializable;

/**
 *
 * @author Palo
 */
public class UserSessionContext implements Serializable{
    private static final long serialVersionUID = -4625740081258060833L;

    private String userUuid;
    private Long userId;
    

    /**
     * @return the userUuid
     */
    public String getUserUuid() {
        return userUuid;
    }

    /**
     * @param userUuid the userUuid to set
     */
    public void initContext(String userUuid,Long userId) {
        this.userUuid = userUuid;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Uuid="+getUserUuid();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        UserSessionContext e = (UserSessionContext) obj;
        return e.getUserUuid().equalsIgnoreCase(userUuid);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.userUuid != null ? this.userUuid.hashCode() : 0);
        return hash;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }




}
