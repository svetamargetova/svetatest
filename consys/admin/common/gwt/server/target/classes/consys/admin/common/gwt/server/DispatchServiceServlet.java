/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.gwt.server;

import com.google.gwt.user.server.rpc.RPC;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.RPCRequest;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.google.gwt.user.server.rpc.SerializationPolicy;
import com.google.gwt.user.server.rpc.SerializationPolicyLoader;
import consys.common.gwt.client.rpc.service.DispatchService;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.exception.UncaughtException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * DispatchServle prevola akciu GWT.
 *
 *
 * @author palo
 */
public class DispatchServiceServlet extends RemoteServiceServlet implements DispatchService {

    private static final long serialVersionUID = 2281668758186963114L;
    private static final Logger logger = LoggerFactory.getLogger(DispatchServiceServlet.class);
    private SpringBeanContextHandlerRegistry handlerRegistry;

    /**
     * PO inicializacii vlozi do contetextu
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
        logger.info("GWT Dispatch servlet initialized... ");
    }

    /*
     * TODO: SpringSecurity exceptions
     */
    @Override
    public Result execute(Action<?> action) throws ActionException {
        try {                        
            logger.debug("Dispatching {}",action.getClass().getName());
            AdminActionHandler handler = handlerRegistry.findHandler(action); 
            if(handler == null){
                // zkuzisme nacitat session handler
                AdminSessionActionHandler sessionHandler = handlerRegistry.findSessionHandler(action); 
                return sessionHandler.execute(action, perThreadRequest.get().getSession());
            }else{
                return handler.execute(action);
            }
        } catch (NullPointerException e) {
            String error = "Null pointer!!! " + action.getClass().getName() + ": " + e.getMessage();
            logger.error(error, e);
            throw new UncaughtException(error);
        } catch (ActionException a) {
            logger.debug("Action exception" + a);
            throw a;
        } catch (Exception e) {
            String error = "Exception while executing " + action.getClass().getName() + ": " + e.getMessage();
            logger.error(error, e);
            throw new UncaughtException(error);
        }
    }

    @Override
    protected SerializationPolicy doGetSerializationPolicy(
            HttpServletRequest request, String moduleBaseURL, String strongName) {
        logger.info("INFO: Loading serialization policy "+strongName + " for module base "+moduleBaseURL);
        return loadSerializationPolicy(this, request, moduleBaseURL, strongName);
    }

    private SerializationPolicy loadSerializationPolicy(HttpServlet servlet,
            HttpServletRequest request, String moduleBaseURL, String strongName) {
        // The request can tell you the path of the web app relative to the
        // container root.
        String contextPath = request.getContextPath();

        String modulePath = null;
        if (moduleBaseURL != null) {
            try {
                modulePath = new URL(moduleBaseURL).getPath();
            } catch (MalformedURLException ex) {
                // log the information, we will default
                servlet.log("Malformed moduleBaseURL: " + moduleBaseURL, ex);
            }
        }

        SerializationPolicy serializationPolicy = null;

        /*
         * Check that the module path must be in the same web app as the servlet
         * itself. If you need to implement a scheme different than this, override
         * this method.
         */



        if (modulePath == null ) {
            String message = "ERROR: The module path requested, "
                    + modulePath
                    + ", is null.";
            servlet.log(message);
        } else {


            // Strip off the context path from the module base URL. It should be a
            // strict prefix.
            // Open the RPC resource file and read its contents.
             
            String serializationPolicyFilePath = null;

            if(!modulePath.startsWith(contextPath)){
                logger.info("INFO: modulePath " + modulePath + " is different than the this context path " + contextPath);
                logger.info("INFO: using /takeplace/*.gwt.rpc");
                serializationPolicyFilePath = SerializationPolicyLoader.getSerializationPolicyFileName("/takeplace/"+strongName);
            }else{
                logger.info("INFO: modulePath " + modulePath + " is same as context path " + contextPath);
                String contextRelativePath = modulePath.substring(contextPath.length());
                serializationPolicyFilePath = SerializationPolicyLoader.getSerializationPolicyFileName(contextRelativePath
                    + strongName);
                
            }

            InputStream is = servlet.getServletContext().getResourceAsStream(
                    serializationPolicyFilePath);
            try {
                if (is != null) {
                    try {
                        serializationPolicy = SerializationPolicyLoader.loadFromStream(is,
                                null);
                    } catch (ParseException e) {
                        servlet.log("ERROR: Failed to parse the policy file '"
                                + serializationPolicyFilePath + "'", e);
                    } catch (IOException e) {
                        servlet.log("ERROR: Could not read the policy file '"
                                + serializationPolicyFilePath + "'", e);
                    }
                } else {
                    String message = "ERROR: The serialization policy file '"
                            + serializationPolicyFilePath
                            + "' was not found; did you forget to include it in this deployment?";
                    servlet.log(message);
                }
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        // Ignore this error
                    }
                }
            }
        }

        return serializationPolicy;
    }

    @Override
    public String processCall(String payload) throws SerializationException {
        try {
            RPCRequest rpcRequest = RPC.decodeRequest(payload, this.getClass(), this);
            // delegate work to the spring injected service
            return RPC.invokeAndEncodeResponse(this, rpcRequest.getMethod(), rpcRequest.getParameters(), rpcRequest.getSerializationPolicy());
        } catch (IncompatibleRemoteServiceException e) {
            logger.debug("IncompatibleRemoteServiceException", e);
            return RPC.encodeResponseForFailure(null, e);
        } catch (Exception e) {
            logger.error("Exception", e);
            return RPC.encodeResponseForFailure(null, e);
        }
    }

    /**
     * @param handlerRegistry the handlerRegistry to set
     */
    @Autowired
    @Required
    public void setHandlerRegistry(SpringBeanContextHandlerRegistry handlerRegistry) {
        this.handlerRegistry = handlerRegistry;
    }
}
