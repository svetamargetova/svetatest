package consys.admin.common.gwt.server.action.server.handler;


import consys.admin.common.gwt.server.AdminActionHandler;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;
import org.springframework.security.access.annotation.Secured;




/**
 * Instances of this interface will handle specific types of {@link Action}
 * classes.
 * 
 * @author David Peterson
 * @author Pavol Gressa
 */
public interface RoleUserActionHandler<A extends Action<R>, R extends Result> extends AdminActionHandler<A, R> {

    @Secured({"ROLE_USER"})
    @Override
    R execute( A action) throws ActionException;
}
