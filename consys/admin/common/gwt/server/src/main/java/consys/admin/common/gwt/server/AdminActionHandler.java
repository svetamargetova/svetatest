package consys.admin.common.gwt.server;




import consys.common.gwt.server.action.server.ActionHandler;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;



/**
 * Instances of this interface will handle specific types of {@link Action}
 * classes.
 *
 * This class is annotated by @Component because of automatic application context
 * injecting.
 * 
 * @author David Peterson
 * @author Pavol Gressa
 */

public interface AdminActionHandler<A extends Action<R>, R extends Result> extends ActionHandler<A,R>{

   
    
}
