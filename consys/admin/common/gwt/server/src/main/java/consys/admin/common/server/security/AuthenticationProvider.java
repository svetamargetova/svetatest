/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.server.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author palo
 */
public class AuthenticationProvider {

    @Autowired
    private AuthenticationManager authenticationManager;

        
    public boolean authenticate(Authentication auth) {
        try {
            Authentication authResult = authenticationManager.authenticate(auth);
            SecurityContextHolder.getContext().setAuthentication(authResult);
            return true;
        } catch (AuthenticationException ex) {
            return false;
        }
    }
}
