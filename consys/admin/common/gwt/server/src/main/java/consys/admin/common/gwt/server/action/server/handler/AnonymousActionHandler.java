package consys.admin.common.gwt.server.action.server.handler;


import consys.admin.common.gwt.server.AdminActionHandler;
import consys.common.gwt.server.action.server.*;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;




/**
 * Instances of this interface will handle specific types of {@link Action}
 * classes.
 * 
 * @author David Peterson
 * @author Pavol Gressa
 */
public interface AnonymousActionHandler<A extends Action<R>, R extends Result> extends AdminActionHandler<A, R> {
    
    @Override
    R execute( A action) throws ActionException;
}
