package consys.admin.common.gwt.server.action.server.handler;

import consys.admin.common.gwt.server.ContextProvider;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;
import consys.admin.common.gwt.server.session.UserSessionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Simple abstract super-class for {@link ActionHandler} implementations that
 * forces the {@link Action} class to be passed in as a constructor to the
 * handler. It's arguable if this is any simpler than just implementing the
 * {@link ActionHandler} and its {@link #getActionType()} directly.
 * 
 * @author David Peterson
 * 
 * @param <A>
 *            The {@link Action} implementation.
 * @param <R>
 *            The {@link Result} implementation.
 */
public abstract class AbstractAnonymousActionHandler<A extends Action<R>, R extends Result> implements AnonymousActionHandler<A, R>{

    private final Class<A> actionType;
    private ContextProvider contextProvider;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public AbstractAnonymousActionHandler( Class<A> actionType ) {
        this.actionType = actionType;
    }

    @Override
    public Class<A> getActionType() {
        return actionType;
    }

    
    public UserSessionContext getUserSessionContext() {
        return contextProvider.getUserContext();
    }

    /**
     * @return the contextProvider
     */
    public ContextProvider getContextProvider() {
        return contextProvider;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    @Autowired(required=true)
    public void setContextProvider(ContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }



}
