/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.gwt.server;

import consys.common.gwt.server.action.server.ActionHandler;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;
import javax.servlet.http.HttpSession;

/**
 *
 * @author palo
 */
public abstract class AdminSessionActionHandler<A extends Action<R>, R extends Result> implements ActionHandler<A,R>{

    @Override
    public R execute(A action) throws ActionException {
        throw new UnsupportedOperationException("This is action handler that uses sessions. Use execute(action,session)");
    }
    
    public abstract R execute(A action, HttpSession session)  throws ActionException;        
    
}
