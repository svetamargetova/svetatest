package consys.admin.common.gwt.server;

import consys.common.gwt.server.action.server.ActionHandlerRegistry;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;

public abstract class AdminActionHandlerRegistry extends ActionHandlerRegistry{

    /**
     * Searches the registry and returns the first handler which supports the
     * specied action, or <code>null</code> if none is available.
     * 
     * @param action
     *            The action.
     * @return The handler.
     */
    public abstract <A extends Action<R>, R extends Result> AdminActionHandler<A, R> findHandler( A action );
    
}
