/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.gwt.server;

import consys.common.gwt.server.action.server.ActionHandler;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;
import consys.common.utils.collection.Maps;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 * HandlerRegister zalozeny na tom ze vsetky AdminActionHandler instancie su automaticky
 * umiestnene v ApplicationContexte. Takze po jeho nabehnutimozeme ich mozeme
 * autoamticky zaregistrovat vsetky naraz.
 * 
 * @author palo
 */
@Component
public class SpringBeanContextHandlerRegistry extends AdminActionHandlerRegistry {

    private final Map<Class<? extends Action<?>>, ActionHandler<?, ?>> sessionHandlers = Maps.newHashMap(); 
    
    
    
    @Override
    public <A extends Action<R>, R extends Result> AdminActionHandler<A, R> findHandler(A action) {
        return (AdminActionHandler<A, R>) getHandlers().get(action.getClass());
    }
    
    public <A extends Action<R>, R extends Result> AdminSessionActionHandler<A, R> findSessionHandler(A action) {
        return (AdminSessionActionHandler<A, R>) sessionHandlers.get(action.getClass());
    }
    
    @Override
    protected void registerActionHandlers(Map<String, ActionHandler> actionHandlers) {
        for (Map.Entry<String, ActionHandler> e : actionHandlers.entrySet()) {            
            if(e.getValue() instanceof AdminSessionActionHandler){
                registerHandler(e.getValue(), sessionHandlers);
            }else{            
                registerHandler(e.getValue(), getHandlers());
            }
        }
    }

    
    
    
    
    @Override
    public void clearHandlers() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
