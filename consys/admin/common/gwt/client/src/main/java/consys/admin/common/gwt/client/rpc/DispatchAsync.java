package consys.admin.common.gwt.client.rpc;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import consys.common.gwt.client.rpc.AbstractDispatchAsync;
import consys.common.gwt.client.rpc.service.DispatchService;
import consys.common.gwt.client.rpc.service.DispatchServiceAsync;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.action.Result;


/**
 * This class is the default implementation of {@link DispatchAsync}, which is
 * essentially the client-side access to the {@link Dispatch} class on the
 * server-side.
 *
 * @author David Peterson
 */
public class DispatchAsync extends AbstractDispatchAsync {

    private static DispatchServiceAsync realService;

    public DispatchAsync() {
        super();
        realService = (DispatchServiceAsync) GWT.create(DispatchService.class);
        ((ServiceDefTarget) realService).setServiceEntryPoint(GWT.getModuleBaseURL() + "dispatch");        
    }

    @Override
    public <A extends Action<R>, R extends Result> void execute( final A action, final AsyncCallback<R> callback ) {
        realService.execute( action, new AsyncCallback<Result>() {
            @Override
            public void onFailure( Throwable caught ) {
                DispatchAsync.this.onFailure( action, caught, callback );
            }

            @Override
            public void onSuccess( Result result ) {
                DispatchAsync.this.onSuccess( action, (R) result, callback );
            }
        } );
    }



}
