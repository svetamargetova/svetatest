package consys.admin.common.test;



import consys.admin.common.test.listener.LoggingListener;
import consys.admin.common.test.listener.TranscationalExecutionListener;
import consys.common.core.service.NotX;
import consys.common.core.service.NotxService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeSuite;
import static org.mockito.Mockito.mock;


/**
 *
 * @author Palo
 */
@TestExecutionListeners(inheritListeners=true,value = {LoggingListener.class,TranscationalExecutionListener.class,DirtiesContextTestExecutionListener.class})
@ContextConfiguration(locations={"data-source.xml","admin-spring-test.xml"})
@TransactionConfiguration(transactionManager="transactionManager")
@Transactional()
public abstract class AbstractAdminDaoTestCase extends AbstractTestNGSpringContextTests{
                

}
