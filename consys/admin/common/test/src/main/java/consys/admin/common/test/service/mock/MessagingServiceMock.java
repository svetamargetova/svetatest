/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.common.test.service.mock;

import consys.common.core.service.MessagingService;

/**
 *
 * @author palo
 */
public class MessagingServiceMock implements MessagingService{

    @Override
    public MessagingStateEnum registerEntity(String uuid, String email) {
        return MessagingStateEnum.OK;
    }

    @Override
    public MessagingStateEnum sendMail(String from, String to, String subject, String msg, boolean isHTML) {
        return MessagingStateEnum.OK;
    }

}
