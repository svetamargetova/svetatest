/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.test.service.mock;

import net.notx.ws.tagging.*;
import net.notx.ws.user.*;

/**
 *
 * @author palo
 */
public class NotxWebservicesMock implements UserWebService,TaggingWebService{

    @Override
    public CreateOrUpdateUserResponse createOrUpdateUser(CreateOrUpdateUserRequest createOrUpdateUserRequest) {
        return new CreateOrUpdateUserResponse();
    }

    @Override
    public LoadUserResponse loadUser(LoadUserRequest loadUserRequest) {
        return new LoadUserResponse();
    }

    @Override
    public TagResponse createTags(TagRequest tagRequest) {
        return new TagResponse();
    }

    @Override
    public UnTagResponse removeTags(UnTagRequest unTagRequest) {
        return new UnTagResponse();
    }
    
}
