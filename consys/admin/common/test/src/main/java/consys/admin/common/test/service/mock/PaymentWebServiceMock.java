package consys.admin.common.test.service.mock;

import consys.payment.webservice.EventWebService;
import consys.payment.webservice.OrderWebService;
import consys.payment.webservice.ProductWebService;
import consys.payment.webservice.ProfileWebService;
import consys.payment.webservice.TransferWebService;
import consys.payment.ws.bo.OrderState;
import consys.payment.ws.event.*;
import consys.payment.ws.order.CancelOrderRequest;
import consys.payment.ws.order.CancelOrderResponse;
import consys.payment.ws.order.CancelOrdersRequest;
import consys.payment.ws.order.CancelOrdersResponse;
import consys.payment.ws.order.ConfirmOrderRequest;
import consys.payment.ws.order.ConfirmOrderResponse;
import consys.payment.ws.order.CreateOrderRequest;
import consys.payment.ws.order.CreateOrderResponse;
import consys.payment.ws.order.CreateOrderWithProfileRequest;
import consys.payment.ws.order.CreateOrderWithProfileResponse;
import consys.payment.ws.order.LoadOrderRequest;
import consys.payment.ws.order.LoadOrderResponse;
import consys.payment.ws.product.CreateEventProductRequest;
import consys.payment.ws.product.CreateEventProductResponse;
import consys.payment.ws.product.CreateEventProductsRequest;
import consys.payment.ws.product.CreateEventProductsResponse;
import consys.payment.ws.product.DeleteEventProductsRequest;
import consys.payment.ws.product.DeleteEventProductsResponse;
import consys.payment.ws.product.LoadLicenseProductRequest;
import consys.payment.ws.product.LoadLicenseProductResponse;
import consys.payment.ws.product.LoadProductRequest;
import consys.payment.ws.product.LoadProductResponse;
import consys.payment.ws.product.UpdateEventProductRequest;
import consys.payment.ws.product.UpdateEventProductResponse;
import consys.payment.ws.product.UpdateEventProductsRequest;
import consys.payment.ws.product.UpdateEventProductsResponse;
import consys.payment.ws.profile.ListUserProfilesRequest;
import consys.payment.ws.profile.ListUserProfilesResponse;
import consys.payment.ws.profile.LoadOrderProfileRequest;
import consys.payment.ws.profile.LoadOrderProfileResponse;
import consys.payment.ws.profile.LoadProfileRequest;
import consys.payment.ws.profile.LoadProfileResponse;
import consys.payment.ws.transfer.ListTransfersRequest;
import consys.payment.ws.transfer.ListTransfersResponse;
import consys.payment.ws.transfer.LoadTransferRequest;
import consys.payment.ws.transfer.LoadTransferResponse;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PaymentWebServiceMock implements EventWebService, ProductWebService, ProfileWebService, TransferWebService, OrderWebService {

    @Override
    public BuyLicenseResponse buyLicense(BuyLicenseRequest request) {
        return new BuyLicenseResponse();
    }

    @Override
    public UpdateEventProfileResponse updateEventProfile(UpdateEventProfileRequest request) {
        return new UpdateEventProfileResponse();
    }

    @Override
    public ListEventProfilesResponse listEventProfiles(ListEventProfilesRequest request) {
        return new ListEventProfilesResponse();
    }

    @Override
    public CreateEventProductResponse createProduct(CreateEventProductRequest request) {
        return new CreateEventProductResponse();
    }

    @Override
    public CreateEventProductsResponse createProducts(CreateEventProductsRequest request) {
        return new CreateEventProductsResponse();
    }

    @Override
    public UpdateEventProductResponse updateProduct(UpdateEventProductRequest request) {
        return new UpdateEventProductResponse();
    }

    @Override
    public UpdateEventProductsResponse updateProducts(UpdateEventProductsRequest request) {
        return new UpdateEventProductsResponse();
    }

    @Override
    public LoadProfileResponse loadProfile(LoadProfileRequest request) {
        return new LoadProfileResponse();
    }

    @Override
    public ListUserProfilesResponse listUserProfiles(ListUserProfilesRequest request) {
        return new ListUserProfilesResponse();
    }

    @Override
    public LoadProductResponse loadProduct(LoadProductRequest request) {
        return new LoadProductResponse();
    }

    @Override
    public InitializeEventResponse initializeEvent(InitializeEventRequest request) {
        return new InitializeEventResponse();
    }

    @Override
    public InitializeCloneEventResponse initializeCloneEvent(InitializeCloneEventRequest request) {
        return new InitializeCloneEventResponse();
    }

    @Override
    public DeleteEventProductsResponse deleteEventProducts(DeleteEventProductsRequest request) {
        return new DeleteEventProductsResponse();
    }

    @Override
    public BuyLicenseWithProfileResponse buyLicenseWithProfile(BuyLicenseWithProfileRequest request) {
        return new BuyLicenseWithProfileResponse();
    }

    @Override
    public ListTransfersResponse listEventTransfers(ListTransfersRequest request) {
        return new ListTransfersResponse();
    }

    @Override
    public LoadTransferResponse loadEventTransfer(LoadTransferRequest request) {
        return new LoadTransferResponse();
    }

    @Override
    public CreateOrderResponse createOrder(CreateOrderRequest request) {
        return new CreateOrderResponse();
    }

    @Override
    public CreateOrderWithProfileResponse createOrderWithProfile(CreateOrderWithProfileRequest request) {
        return new CreateOrderWithProfileResponse();
    }

    @Override
    public CancelOrderResponse cancelOrder(CancelOrderRequest request) {
        return new CancelOrderResponse();
    }

    @Override
    public LoadOrderResponse loadOrder(LoadOrderRequest request) {
        return new LoadOrderResponse();
    }

    @Override
    public CancelOrdersResponse cancelOrders(CancelOrdersRequest request) {
        return new CancelOrdersResponse();
    }

    @Override
    public LoadEventPaymentDetailsResponse loadEventPaymentDetails(LoadEventPaymentDetailsRequest request) {
        return new LoadEventPaymentDetailsResponse();
    }

    @Override
    public ConfirmOrderResponse confirmOrder(ConfirmOrderRequest request) {
        ConfirmOrderResponse or = new ConfirmOrderResponse();
        or.setOrderState(OrderState.CONFIRMED_ORGANIZATOR);
        return or;
    }

    @Override
    public LoadEventPaymentProfileResponse loadEventPaymentProfile(LoadEventPaymentProfileRequest request) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public LoadLicenseProductResponse loadLicenseProduct(LoadLicenseProductRequest request) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public LoadOrderProfileResponse loadOrderProfile(LoadOrderProfileRequest request) {
        return new LoadOrderProfileResponse();
    }
}
