/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.admin.common.test.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

/**
 *
 * @author palo
 */
public class LoggingListener extends AbstractTestExecutionListener {

    private static final Logger logger = LoggerFactory.getLogger(LoggingListener.class);

    public static String getClassName(Class c) {
        String FQClassName = c.getName();
        int firstChar;
        firstChar = FQClassName.lastIndexOf('.') + 1;
        if (firstChar > 0) {
            FQClassName = FQClassName.substring(firstChar);
        }
        return FQClassName;
    }

    @Override
    public void beforeTestMethod(TestContext testContext) throws Exception {
                
        logger.info("----------------------------------------------------------");
        logger.info("-- TEST: " + "[" + getClassName(testContext.getTestClass()) + "]  -  "+testContext.getTestMethod().getName());
        logger.info("----------------------------------------------------------");

    }
}
