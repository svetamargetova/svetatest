/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.admin.common.test.service.mock;

import consys.admin.ws.overseer.EventWebServiceEnum;
import consys.admin.ws.overseer.service.OverseerWebService;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;



/**
 *
 * @author palo
 */
public class OverseerWebServiceMock implements OverseerWebService{
            
    
    private List<Object> queueResponseList = new ArrayList<Object>();
    
    public void clear(){
        queueResponseList.clear();
    }
    
    public void addResponse(Object o){
        queueResponseList.add(o);
    }
    
    @Override
    public <T> T sendAndReceive(EventWebServiceEnum ws, String overseerUri, Object payload) {        
        return (T) queueResponseList.remove(0);
    }

   

}
