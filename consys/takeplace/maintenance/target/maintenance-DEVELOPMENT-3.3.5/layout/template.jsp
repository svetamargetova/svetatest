<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/layout/taglibs.jsp" %>
<%@page import="java.util.Enumeration" %>
<%@page import="javax.servlet.http.HttpSession" %>


<stripes:layout-definition>
    <html>
        <head>
            <title>Takeplace - Maintenance</title>
            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
            <script type="text/javascript" src="${ctx}/layout/jquery-ui-1.8.13.custom.min.js"></script>            
            <link rel="stylesheet" type="text/css" href="${ctx}/layout/jquery-ui-1.8.13.custom.css">
            <link rel="stylesheet" type="text/css" href="${ctx}/layout/styles.css">
            <script type="text/javascript">
                $(document).ready(function(){
                <stripes:layout-component name="documentReady"/>
                    });
            </script>
        </head>
        <body>     

            <div style="width: 100%; text-align: right;">
                <%
                    String userName = (request.getUserPrincipal() == null) ? null : request.getUserPrincipal().getName();
                %>
                <%= "<span style='font-size:8pt'> Logged User: " + userName + "</span>"%>

            </div>
            <div id="header">                                    
                <div class="top-panel-item"> <stripes:link name="management" href="/management/management.jsp">Management</stripes:link></div>
                <div class="top-panel-item"> <stripes:link name="monitoring" href="/monitoring/monitoring.jsp">Monitoring</stripes:link></div>                        

            </div>                
            <div id="subheader">
                <stripes:layout-component name="subnavigation"/>
            </div>
            <div>               
                <div id="content">
                    <div class="sectionTitle">${title}</div>
                    <stripes:messages/>
                    <stripes:layout-component name="contents"/>
                </div>                
            </div>
        </div>            
    </body>
</html>
</stripes:layout-definition>
