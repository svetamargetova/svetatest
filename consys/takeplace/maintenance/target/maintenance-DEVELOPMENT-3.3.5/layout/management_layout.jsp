<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>


<stripes:layout-definition>
    <stripes:layout-render name="/layout/template.jsp">
        <stripes:layout-component name="subnavigation">        
            <div class="top-subpanel-item"> <stripes:link name="Home"           beanclass="consys.maintenance.stripes.bean.management.ManagementActionBean" >Home</stripes:link></div>        
            <div class="top-subpanel-item"> <stripes:link name="userMangement"  beanclass="consys.maintenance.stripes.bean.management.UsersActionBean" >Users</stripes:link></div>        
            <div class="top-subpanel-item"> <stripes:link name="eventManagement" beanclass="consys.maintenance.stripes.bean.management.EventsActionBean">Events</stripes:link></div>                           
        </stripes:layout-component>        
    </stripes:layout-render>
</stripes:layout-definition>