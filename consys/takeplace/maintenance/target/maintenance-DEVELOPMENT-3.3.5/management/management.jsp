<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/management_layout.jsp">



    <stripes:layout-component name="contents">
        
        <stripes:form action="/management/Management.action">                                    
            <stripes:errors globalErrorsOnly="true"/>                        
            
            <div class="actions">                     
                
                <div class="action">
                    Reindex Users in SOLR:
                    <stripes:submit name="reindexSolr" value="Reindex"/>
                    <stripes:errors field="reindexSolr"/>
                </div>                
                <div class="action">
                    Reindex Users in NotX:
                    <stripes:submit name="reindexUsersNotx" value="Reindex"/> <strong>OR</strong> <stripes:submit name="testAllUsersInNotx" value="Test users"/>
                    <stripes:errors field="reindexUsersNotx"/>
                    <stripes:errors field="testAllUsersInNotx"/>
                    <c:if test="${not empty actionBean.users}">                         
                        <c:forEach var="user" items="${actionBean.users}">
                            <div>
                                ${user}
                            </div>
                        </c:forEach>
                    </c:if>
                </div>                
                <div class="action">
                    Reinstall Templates in NotX:
                    <stripes:submit name="reinstallTemplatesNotx" value="Reinstall"/>
                    <stripes:errors field="reinstallTemplatesNotx"/>
                </div>     
                <div class="action">
                    Recreate event profile pages:
                    <stripes:submit name="recreateEventProfilePages" value="Recreate"/>
                    <stripes:errors field="recreateEventProfilePages"/>
                </div>     
                <div class="action">
                    Refresh events detail property:
                    <stripes:submit name="updateEventDetailProperty" value="Update"/>
                    <stripes:errors field="updateEventDetailProperty"/>
                </div>
               <div class="action">
                    Migrations management:
                    <stripes:link name="adminMigrationManagement" beanclass="consys.maintenance.stripes.bean.management.AdminMigrationsActionBean">Administration</stripes:link>
                    <stripes:link name="eventsMigrationManagement" beanclass="consys.maintenance.stripes.bean.management.EventsMigrationActionBean">Events</stripes:link>                    
                </div>           
                
                <div class="action">
                    Version migrations:
                    <ul>
                        <li><strong>2.8.0</strong> Migrate images into S3: <stripes:submit name="migrateImagesIntoS3" value="Migrate"/></li>
                    </ul>                                                            
                </div>
                
                <div class="clear"> </div>
            </div>            
        </stripes:form>    




    </stripes:layout-component>
</stripes:layout-render>