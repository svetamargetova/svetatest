<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/management_layout.jsp">

    <stripes:layout-component name="contents">

        <stripes:errors/>
        <stripes:form action="/management/EventDetail.action" focus="">
            <stripes:hidden name="event.id"/>
            <table>
                <tbody>
                    
                    <tr>
                        <td><stripes:label for="eid" name="common.uuid" /> </td>
                        <td><stripes:text id="eid" name="event.uuid" /></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="eregistred" name="management.event.registredDate" /> </td>
                        <td><stripes:text id="eregistred" name="event.registredDate" formatPattern="dateTime"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="eac" name="management.event.acronym" /></td>
                        <td><stripes:text id="eac" name="event.acronym" /></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="efull" name="management.event.fullName" /></td>
                        <td><stripes:text id="efull" name="event.fullName"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="eshort" name="management.event.shortName" /></td>
                        <td><stripes:text id="eshort" name="event.shortName"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="euniv" name="management.event.universalName" /></td>
                        <td><stripes:text id="euniv" name="event.universalName"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="efrom" name="management.event.from" /></td>
                        <td><stripes:text id="efrom" name="event.fromDate"  formatPattern="date"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="eto" name="management.event.to" /></td>
                        <td><stripes:text id="eto" name="event.toDate" formatPattern="date"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="eweb" name="management.event.web" /></td>
                        <td><stripes:text id="eweb" name="event.web"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="ecity" name="common.address.city" /></td>
                        <td><stripes:text id="ecity" name="event.city"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="eyear" name="management.event.year" /></td>
                        <td><stripes:text id="eyear" name="event.year"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="eseries" name="management.event.series" /></td>
                        <td><stripes:text id="eseries" name="event.series"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="ecountry" name="common.address.country" /></td>
                        <td><stripes:text id="ecountry" name="event.country.name"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="etimezone" name="management.event.timeZone" /></td>
                        <td><stripes:text id="etimezone" name="event.timeZone"/></td>
                    </tr>
                    
                    <tr>
                        <td><stripes:label for="evisible" name="management.event.visible" /></td>
                        <td><stripes:checkbox id="evisible" name="event.visible"/></td>
                    </tr>
                    
                    <tr>
                        <td><stripes:label for="ecanlog" name="management.event.canLogIn" /></td>
                        <td><stripes:checkbox id="ecanlog" name="event.canLogIn"/></td>
                    </tr>


                    <tr>
                        <td><stripes:label for="edescription" name="common.description" /></td>
                        <td><stripes:textarea id="edescription" name="event.description"/></td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <stripes:submit   class="todo"  name="save" value="Save"/>
                            <stripes:submit    name="delete" value="Delete"/>
                        </td>
                    </tr>
                    
                </tbody>
            </table>    
        </stripes:form>


    </stripes:layout-component>
</stripes:layout-render>