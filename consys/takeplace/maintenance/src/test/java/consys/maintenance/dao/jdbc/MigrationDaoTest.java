/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.dao.jdbc;

import consys.maintenance.service.*;
import consys.maintenance.bo.EventMigration;
import consys.common.core.exception.NoRecordException;
import consys.maintenance.bo.EventMigrationResult;
import consys.maintenance.bo.Migration;
import consys.maintenance.config.AbstractMaintenanceTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author palo
 */
public class MigrationDaoTest extends AbstractMaintenanceTest{
   
    
    @Autowired
    private MigrationService migrationService;
    @Autowired
    private MigrationDaoImpl migrationDaoImpl;
    
    
    
    @Test(groups={"dao"})    
    public void eventMigrationDaoTest() throws NoRecordException{    
        
        
        Migration m3 = new Migration();
        m3.setDescription("Lala");
        m3.setName("Name");
        m3.setSqlScript("select * from abc;");
        EventMigration am3 = new EventMigration();
        am3.setMigration(m3);        
        migrationService.createEventMigration(am3);
                
        
        EventMigrationResult emr1 = new EventMigrationResult();
        emr1.setOverseerName("o name");
        emr1.setOverseerUrl("o name");
        emr1.setEvents(new String[]{"Alfa","Beta","Gamma","Delta"});
        am3.getResults().add(emr1);
        migrationDaoImpl.updateEventMigration(am3);
        
        am3 = migrationDaoImpl.loadEventMigrationDetail(am3.getUuid());
        assertEquals(am3.getResults().size(), 1);
        assertNotNull(am3.getResults().get(0).getEvents());
        assertEquals(am3.getResults().get(0).getEvents().length, 4);
        assertEquals(am3.getResults().get(0).getEvents()[0], "Alfa");
        assertEquals(am3.getResults().get(0).getEvents()[1], "Beta");
        assertEquals(am3.getResults().get(0).getEvents()[2], "Gamma");
        assertEquals(am3.getResults().get(0).getEvents()[3], "Delta");
        
        am3.getMigration().setName("New Name");
        migrationDaoImpl.updateEventMigration(am3);
        am3 = migrationDaoImpl.loadEventMigrationDetail(am3.getUuid());
        assertEquals(am3.getMigration().getName(), "New Name");
        
        
        
    }
    
    
    
}
