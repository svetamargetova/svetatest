/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.service;

import consys.maintenance.bo.thumb.EventMigrationListItem;
import consys.maintenance.bo.EventMigration;
import java.util.List;
import consys.common.core.exception.NoRecordException;
import consys.maintenance.bo.AdminMigration;
import consys.maintenance.bo.Migration;
import consys.maintenance.bo.thumb.AdminMigrationListItem;
import consys.maintenance.config.AbstractMaintenanceTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author palo
 */
public class MigrationServiceTest extends AbstractMaintenanceTest{
   
    
    @Autowired
    private MigrationService migrationService;
    
    @Test(groups={"service"})    
    public void adminMigrationCRUDTest() throws NoRecordException{
    
        Migration m1 = new Migration();
        m1.setDescription("Lala");
        m1.setName("Name");
        m1.setSqlScript("select * from users;"+
"\n"+
"\nselect * from users;"+
"\nselect * from users;"+
"\r\n"+
"\t"+
"select * from users;");
        AdminMigration am1 = new AdminMigration();
        am1.setMigration(m1);        
        migrationService.createAdminMigration(am1);
        
                        
        AdminMigration aml = migrationService.loadAdminMigrationDetail(am1.getUuid());        
        assertEquals(am1, aml);
        
        
        
        Migration m2 = new Migration();
        m2.setDescription("Lala");
        m2.setName("Name");
        m2.setSqlScript("select * from abc;");
        AdminMigration am2 = new AdminMigration();
        am2.setMigration(m2);        
        migrationService.createAdminMigration(am2);
        
        Migration m3 = new Migration();
        m3.setDescription("Lala");
        m3.setName("Name");
        m3.setSqlScript("select * from abc;");
        AdminMigration am3 = new AdminMigration();
        am3.setMigration(m3);        
        migrationService.createAdminMigration(am3);
        
        List<AdminMigrationListItem> items = migrationService.listAdminMigrations(false, 0, 20);
        assertEquals(items.size(), 3);
        assertEquals(migrationService.loadAdminMigrationsCount(false), 3);           
    
    }
    
    
    @Test(groups={"service"})    
    public void eventMigrationCRUDTest() throws NoRecordException{    
        Migration m1 = new Migration();
        m1.setDescription("Lala");
        m1.setName("Name");
        m1.setSqlScript("select * from abc;");
        EventMigration am1 = new EventMigration();
        am1.setMigration(m1);        
        migrationService.createEventMigration(am1);
        
        EventMigration aml = migrationService.loadEventMigrationDetail(am1.getUuid());        
        assertEquals(am1, aml);
        
        Migration m2 = new Migration();
        m2.setDescription("Lala");
        m2.setName("Name");
        m2.setSqlScript("select * from abc;");
        EventMigration am2 = new EventMigration();
        am2.setMigration(m2);        
        migrationService.createEventMigration(am2);
        
        Migration m3 = new Migration();
        m3.setDescription("Lala");
        m3.setName("Name");
        m3.setSqlScript("select * from abc;");
        EventMigration am3 = new EventMigration();
        am3.setMigration(m3);        
        migrationService.createEventMigration(am3);
        
        List<EventMigrationListItem> items = migrationService.listEventMigrations(false, 0, 20);
        assertEquals(items.size(), 3);
        assertEquals(migrationService.loadEventMigrationsCount(false), 3);           
    
    }
    
    
    
}
