/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.config;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author palo
 */
/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@TestExecutionListeners(inheritListeners=true,value = {TransactionalTestExecutionListener.class})
@ContextConfiguration(locations={"spring-context.xml","spring-datasource-test.xml"})
@TransactionConfiguration(defaultRollback=true,transactionManager="mTxManager")
@Transactional
public abstract class AbstractMaintenanceTest extends AbstractTestNGSpringContextTests{
    
}
