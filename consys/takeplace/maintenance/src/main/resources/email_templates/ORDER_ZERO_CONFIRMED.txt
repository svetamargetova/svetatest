Dear $salutation$,

your order $order_uuid$ at $event_name$ has been confirmed.

Order details:
--------------

Registration number:  $registration_uuid$
Order date:           $order_date$
Total:                $total$


Ordered items:
--------------
$items_plain$                            


Your account details:
    Name:   $salutation$
    E-mail: $email$


Best regards,
Takeplace Team


Please do not reply to this message. The email address is used for email notifications
only so you will receive no response. If you have any question, please contact
support@takeplace.eu.
    

This message was addressed to $email$. Please read our Terms of use 
(http://www.takeplace.eu/terms_of_use_en.pdf).
                
