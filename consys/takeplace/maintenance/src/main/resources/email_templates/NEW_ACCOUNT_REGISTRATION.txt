Dear $salutation$,

thank you for registering with Takeplace! Before you get started you need to 
activate your account.

Please click the following link:

${takeplace.email.url}/takeplace/takeplace.html?cmd=ActivateAccount&code=$activation_key$

Alternativelly, you can access the page:

${takeplace.email.url}/takeplace/takeplace.html?cmd=ActivateAccount

and enter the activation key: $activation_key$

Your account details:
    Name:   $salutation$
    E-mail: $email$


If clicking the link doesn't seem to work, you can copy and paste the link 
into your browser's address window, or retype it there.
Best regards,
Takeplace Team


Please do not reply to this message. The email address is used for email notifications
only so you will receive no response. If you have any question, please contact
support@takeplace.eu.
    

This message was addressed to $email$. Please read our Terms of use 
(http://www.takeplace.eu/terms_of_use_en.pdf).
                
