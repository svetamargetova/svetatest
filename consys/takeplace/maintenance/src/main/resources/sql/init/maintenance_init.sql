--
-- PostgreSQL database dump
--

-- Started on 2011-10-17 14:08:28 CEST

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 1807 (class 1262 OID 45772)
-- Name: consys_2_maintenance; Type: DATABASE; Schema: -; Owner: maintenance
--

CREATE ROLE maintenance LOGIN ENCRYPTED PASSWORD 'datamaster' NOSUPERUSER NOINHERIT NOCREATEDB NOCREATEROLE;

CREATE DATABASE consys_2_maintenance WITH OWNER = maintenance TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US' LC_CTYPE = 'en_US';


ALTER DATABASE consys_2_maintenance OWNER TO maintenance;

\connect consys_2_maintenance

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 457 (class 2612 OID 16386)
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: postgres
--

CREATE PROCEDURAL LANGUAGE plpgsql;


ALTER PROCEDURAL LANGUAGE plpgsql OWNER TO postgres;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 143 (class 1259 OID 45806)
-- Dependencies: 6
-- Name: admin_migration; Type: TABLE; Schema: public; Owner: maintenance; Tablespace: 
--

CREATE TABLE admin_migration (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    id_migration bigint NOT NULL
);


ALTER TABLE public.admin_migration OWNER TO maintenance;

--
-- TOC entry 144 (class 1259 OID 45820)
-- Dependencies: 6
-- Name: admin_migration_seq; Type: SEQUENCE; Schema: public; Owner: maintenance
--

CREATE SEQUENCE admin_migration_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.admin_migration_seq OWNER TO maintenance;

--
-- TOC entry 141 (class 1259 OID 45783)
-- Dependencies: 6
-- Name: event_migration; Type: TABLE; Schema: public; Owner: maintenance; Tablespace: 
--

CREATE TABLE event_migration (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    id_migration bigint NOT NULL
);


ALTER TABLE public.event_migration OWNER TO maintenance;

--
-- TOC entry 142 (class 1259 OID 45786)
-- Dependencies: 6
-- Name: event_migration_result; Type: TABLE; Schema: public; Owner: maintenance; Tablespace: 
--

CREATE TABLE event_migration_result (
    id bigint NOT NULL,
    overseer_url character varying(128) NOT NULL,
    overseer_name character varying(128) NOT NULL,
    events character varying[],
    id_event_migration bigint NOT NULL
);


ALTER TABLE public.event_migration_result OWNER TO maintenance;

--
-- TOC entry 146 (class 1259 OID 45824)
-- Dependencies: 6
-- Name: event_migration_result_seq; Type: SEQUENCE; Schema: public; Owner: maintenance
--

CREATE SEQUENCE event_migration_result_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.event_migration_result_seq OWNER TO maintenance;

--
-- TOC entry 145 (class 1259 OID 45822)
-- Dependencies: 6
-- Name: event_migration_seq; Type: SEQUENCE; Schema: public; Owner: maintenance
--

CREATE SEQUENCE event_migration_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.event_migration_seq OWNER TO maintenance;

--
-- TOC entry 140 (class 1259 OID 45773)
-- Dependencies: 6
-- Name: migration; Type: TABLE; Schema: public; Owner: maintenance; Tablespace: 
--

CREATE TABLE migration (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    name character varying(200) NOT NULL,
    description text,
    created timestamp without time zone NOT NULL,
    migrated timestamp without time zone,
    sql_script text
);


ALTER TABLE public.migration OWNER TO maintenance;

--
-- TOC entry 147 (class 1259 OID 45826)
-- Dependencies: 6
-- Name: migration_seq; Type: SEQUENCE; Schema: public; Owner: maintenance
--

CREATE SEQUENCE migration_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.migration_seq OWNER TO maintenance;

--
-- TOC entry 1799 (class 2606 OID 45833)
-- Dependencies: 143 143
-- Name: PK_admin_migration; Type: CONSTRAINT; Schema: public; Owner: maintenance; Tablespace: 
--

ALTER TABLE ONLY admin_migration
    ADD CONSTRAINT "PK_admin_migration" PRIMARY KEY (id);


--
-- TOC entry 1793 (class 2606 OID 45837)
-- Dependencies: 141 141
-- Name: PK_event_migration; Type: CONSTRAINT; Schema: public; Owner: maintenance; Tablespace: 
--

ALTER TABLE ONLY event_migration
    ADD CONSTRAINT "PK_event_migration" PRIMARY KEY (id);


--
-- TOC entry 1797 (class 2606 OID 45851)
-- Dependencies: 142 142
-- Name: PK_event_migration_result; Type: CONSTRAINT; Schema: public; Owner: maintenance; Tablespace: 
--

ALTER TABLE ONLY event_migration_result
    ADD CONSTRAINT "PK_event_migration_result" PRIMARY KEY (id);


--
-- TOC entry 1789 (class 2606 OID 45829)
-- Dependencies: 140 140
-- Name: PK_migration; Type: CONSTRAINT; Schema: public; Owner: maintenance; Tablespace: 
--

ALTER TABLE ONLY migration
    ADD CONSTRAINT "PK_migration" PRIMARY KEY (id);


--
-- TOC entry 1801 (class 2606 OID 45835)
-- Dependencies: 143 143
-- Name: UQ_admin_migration_uuid; Type: CONSTRAINT; Schema: public; Owner: maintenance; Tablespace: 
--

ALTER TABLE ONLY admin_migration
    ADD CONSTRAINT "UQ_admin_migration_uuid" UNIQUE (uuid);


--
-- TOC entry 1795 (class 2606 OID 45839)
-- Dependencies: 141 141
-- Name: UQ_event_migration_uuid; Type: CONSTRAINT; Schema: public; Owner: maintenance; Tablespace: 
--

ALTER TABLE ONLY event_migration
    ADD CONSTRAINT "UQ_event_migration_uuid" UNIQUE (uuid);


--
-- TOC entry 1791 (class 2606 OID 45831)
-- Dependencies: 140 140
-- Name: UQ_migration_uuid; Type: CONSTRAINT; Schema: public; Owner: maintenance; Tablespace: 
--

ALTER TABLE ONLY migration
    ADD CONSTRAINT "UQ_migration_uuid" UNIQUE (uuid);


--
-- TOC entry 1804 (class 2606 OID 45845)
-- Dependencies: 143 143 1798
-- Name: FK_admin_migration_id_migration; Type: FK CONSTRAINT; Schema: public; Owner: maintenance
--

ALTER TABLE ONLY admin_migration
    ADD CONSTRAINT "FK_admin_migration_id_migration" FOREIGN KEY (id_migration) REFERENCES migration(id);


--
-- TOC entry 1802 (class 2606 OID 45840)
-- Dependencies: 143 1798 141
-- Name: FK_event_migration_id_migration; Type: FK CONSTRAINT; Schema: public; Owner: maintenance
--

ALTER TABLE ONLY event_migration
    ADD CONSTRAINT "FK_event_migration_id_migration" FOREIGN KEY (id_migration) REFERENCES migration(id);


--
-- TOC entry 1803 (class 2606 OID 45852)
-- Dependencies: 142 141 1792
-- Name: FK_event_migration_result_id_event_migration; Type: FK CONSTRAINT; Schema: public; Owner: maintenance
--

ALTER TABLE ONLY event_migration_result
    ADD CONSTRAINT "FK_event_migration_result_id_event_migration" FOREIGN KEY (id_event_migration) REFERENCES event_migration(id);


--
-- TOC entry 1809 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--


REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;


-- Completed on 2011-10-17 14:08:28 CEST

--
-- PostgreSQL database dump complete
--

