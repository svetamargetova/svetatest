/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean.management;

import consys.common.core.exception.ConsysException;
import consys.maintenance.bo.thumb.AdminMigrationListItem;
import consys.maintenance.service.MigrationService;
import consys.maintenance.stripes.bean.ListActionBean;
import consys.maintenance.stripes.displaytag.PaginatedListAdapter;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author palo
 */
public class AdminMigrationsActionBean extends ListActionBean<AdminMigrationListItem> {

    @SpringBean
    private MigrationService migrationService;
    private boolean alreadyMigrated = false;
    private PaginatedListAdapter<AdminMigrationListItem> migrations;    

    public AdminMigrationsActionBean() {
        super("/management/administrationMigrations.jsp");
    }

    /**
     * @return the alreadyMigrated
     */
    public boolean isAlreadyMigrated() {
        return alreadyMigrated;
    }

    /**
     * @param alreadyMigrated the alreadyMigrated to set
     */
    public void setAlreadyMigrated(boolean alreadyMigrated) {
        this.alreadyMigrated = alreadyMigrated;
    }
    
    @DontValidate
    public Resolution newMigration(){
        return new ForwardResolution(AdminMigrationDetailActionBean.class);
    }
    
    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws ConsysException {
        getItems().setList(migrationService.listAdminMigrations(isAlreadyMigrated(), firstItemIndex, itemsPerPage));
        getItems().setTotal(migrationService.loadAdminMigrationsCount(isAlreadyMigrated()));

    }   
}
