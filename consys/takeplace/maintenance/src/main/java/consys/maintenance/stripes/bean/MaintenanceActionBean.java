package consys.maintenance.stripes.bean;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class MaintenanceActionBean implements ActionBean {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private MaintenanceActionBeanContext context;

    @Override
    public void setContext(ActionBeanContext context) {
        this.context = (MaintenanceActionBeanContext) context;
    }

    /** Gets the ActionBeanContext set by Stripes during initialization. */
    @Override
    public MaintenanceActionBeanContext getContext() {
        return this.context;
    }
    
}
