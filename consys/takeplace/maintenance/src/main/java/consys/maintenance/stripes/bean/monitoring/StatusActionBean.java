/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean.monitoring;

import consys.maintenance.stripes.bean.MaintenanceActionBean;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 *
 * @author palo
 */
public class StatusActionBean extends MaintenanceActionBean {

    @DontValidate
    public Resolution show() {
        return new ForwardResolution("/monitoring/status.jsp");
    }
}
