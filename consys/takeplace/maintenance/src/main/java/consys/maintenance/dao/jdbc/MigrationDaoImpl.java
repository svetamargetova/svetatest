/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.dao.jdbc;

import consys.common.core.exception.NoRecordException;
import consys.maintenance.bo.AdminMigration;
import consys.maintenance.bo.EventMigration;
import consys.maintenance.bo.EventMigrationResult;
import consys.maintenance.bo.Migration;
import consys.maintenance.bo.thumb.AdminMigrationListItem;
import consys.maintenance.bo.thumb.EventMigrationListItem;
import consys.maintenance.dao.MigrationDao;
import consys.maintenance.dao.jdbc.postgresql.PostgreSQLStringArray;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

/**
 *
 * @author palo
 */
public class MigrationDaoImpl extends AbstractJdbcDao implements MigrationDao {

    
    
    private static final Logger logger = LoggerFactory.getLogger(MigrationDaoImpl.class);
    
    private SimpleJdbcInsert migrationInsert;
    private SimpleJdbcInsert adminMigrationInsert;
    private SimpleJdbcInsert eventMigrationInsert;
    private SimpleJdbcInsert eventMigrationResultInsert;

    @Override
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
        migrationInsert = new SimpleJdbcInsert(getDataSource()).withTableName("migration").
                //usingGeneratedKeyColumns("id").
                usingColumns("id", "uuid", "name", "description", "created", "migrated", "sql_script");
        adminMigrationInsert = new SimpleJdbcInsert(getDataSource()).withTableName("admin_migration").
                //usingGeneratedKeyColumns("id").
                usingColumns("id", "uuid", "id_migration");
        eventMigrationInsert = new SimpleJdbcInsert(getDataSource()).withTableName("event_migration").
                usingColumns("id", "uuid", "id_migration");
        eventMigrationResultInsert = new SimpleJdbcInsert(getDataSource()).withTableName("event_migration_result").
                usingColumns("id", "id_event_migration", "events", "overseer_url", "overseer_name");
        migrationInsert.compile();
        adminMigrationInsert.compile();
        eventMigrationInsert.compile();
        eventMigrationResultInsert.compile();

    }
    /*
     * 
     *  A D M I N   M I G R A T I O N    D A O 
     * 
     */
    private static final String adminMigrateionMigratedSQL = "select am.uuid, m.name, m.description, m.created, m.migrated from admin_migration am left join migration m on m.id=am.id_migration where m.migrated is not null order by m.migrated desc limit ? offset ?";
    private static final String adminMigrateionNotMigratedSQL = "select am.uuid, m.name, m.description, m.created, m.migrated from admin_migration am left join migration m on m.id=am.id_migration where m.migrated is null order by m.created asc limit ? offset ?";

    @Override
    public void insertAdminMigration(AdminMigration adminMigration) {
        insertMigration(adminMigration.getMigration());
        Map<String, Object> parameters = new HashMap<String, Object>(3);
        adminMigration.setId(seqNext("admin_migration_seq"));
        parameters.put("id", adminMigration.getId());
        parameters.put("uuid", adminMigration.getUuid());
        parameters.put("id_migration", adminMigration.getMigration().getId());
        adminMigrationInsert.execute(parameters);

    }
    
    @Override
    public void updateAdminMigration(AdminMigration eventMigration) {
        updateMigration(eventMigration.getMigration());
    }

    @Override
    public AdminMigration loadAdminMigrationDetail(String uuid) throws NoRecordException {
        return getJdbcTemplate().queryForObject("select am.uuid as amuuid, am.id as amid, m.* from admin_migration am left join migration m on m.id=am.id_migration where am.uuid=?",
                new RowMapper<AdminMigration>() {

                    @Override
                    public AdminMigration mapRow(ResultSet rs, int rowNum) throws SQLException {
                        AdminMigration am = new AdminMigration();
                        am.setMigration(mapMigrationRow(rs));
                        am.setId(rs.getLong("amid"));
                        am.setUuid(rs.getString("amuuid"));
                        return am;
                    }
                }, uuid);
    }

    @Override
    public List<AdminMigrationListItem> listAdminMigrations(boolean alreadyMigrated, int startPage, int itemsPerPage) {
        return getJdbcTemplate().query((alreadyMigrated ? adminMigrateionMigratedSQL : adminMigrateionNotMigratedSQL),
                new RowMapper<AdminMigrationListItem>() {

                    @Override
                    public AdminMigrationListItem mapRow(ResultSet rs, int rowNum) throws SQLException {
                        AdminMigrationListItem item = new AdminMigrationListItem();
                        item.setUuid(rs.getString(1));
                        item.setName(rs.getString(2));
                        item.setDescription(rs.getString(3));
                        item.setCreated(rs.getTimestamp(4));
                        item.setMigrated(rs.getTimestamp(5));
                        return item;
                    }
                }, new Object[]{Integer.valueOf(itemsPerPage), Integer.valueOf(startPage)});
    }

    @Override
    public int loadAdminMigrationsCount(boolean alreadyMigrated) {
        return getJdbcTemplate().queryForInt("select count(am.id) from admin_migration am left join migration m on m.id=am.id_migration where m.migrated is " + (alreadyMigrated ? " not null " : " null "));
    }
    /*
     * 
     *  E V E N T   M I G R A T I O N    D A O 
     * 
     */
    private static final String eventMigrateionMigratedSQL = "select am.uuid, m.name, m.description, m.created, m.migrated from event_migration am left join migration m on m.id=am.id_migration where m.migrated is not null order by m.migrated desc limit ? offset ?";
    private static final String eventMigrateionNotMigratedSQL = "select am.uuid, m.name, m.description, m.created, m.migrated from event_migration am left join migration m on m.id=am.id_migration where m.migrated is null order by m.created asc limit ? offset ?";

    @Override
    public List<EventMigrationListItem> listEventMigrations(boolean alreadyMigrated, int startPage, int itemsPerPage) {
        return getJdbcTemplate().query((alreadyMigrated ? eventMigrateionMigratedSQL : eventMigrateionNotMigratedSQL),
                new RowMapper<EventMigrationListItem>() {

                    @Override
                    public EventMigrationListItem mapRow(ResultSet rs, int rowNum) throws SQLException {
                        EventMigrationListItem item = new EventMigrationListItem();
                        item.setUuid(rs.getString(1));
                        item.setName(rs.getString(2));
                        item.setDescription(rs.getString(3));
                        item.setCreated(rs.getTimestamp(4));
                        item.setMigrated(rs.getTimestamp(5));
                        return item;
                    }
                }, new Object[]{Integer.valueOf(itemsPerPage), Integer.valueOf(startPage)});
    }

    @Override
    public int loadEventMigrationsCount(boolean alreadyMigrated) {
        return getJdbcTemplate().queryForInt("select count(am.id) from event_migration am left join migration m on m.id=am.id_migration where m.migrated is " + (alreadyMigrated ? " not null " : " null "));
    }

    @Override
    public EventMigration loadEventMigrationDetail(String uuid) throws NoRecordException {
        EventMigration eventMigration = getJdbcTemplate().queryForObject("select em.uuid as emuuid, em.id as emid, m.*  from event_migration em left join migration m on m.id=em.id_migration where em.uuid=?",
                new RowMapper<EventMigration>() {

                    @Override
                    public EventMigration mapRow(ResultSet rs, int rowNum) throws SQLException {
                        EventMigration am = new EventMigration();
                        am.setMigration(mapMigrationRow(rs));
                        am.setId(rs.getLong("emid"));
                        am.setUuid(rs.getString("emuuid"));
                        return am;
                    }
                }, uuid);


        List<EventMigrationResult> results = getJdbcTemplate().query("select * from event_migration_result emr where emr.id_event_migration=?",
                new RowMapper<EventMigrationResult>() {

                    @Override
                    public EventMigrationResult mapRow(ResultSet rs, int rowNum) throws SQLException {
                        EventMigrationResult migration = new EventMigrationResult();
                        migration.setEvents((String[]) rs.getArray("events").getArray());
                        migration.setId(rs.getLong("id"));
                        migration.setOverseerName(rs.getString("overseer_name"));
                        migration.setOverseerUrl(rs.getString("overseer_url"));
                        return migration;
                    }
                }, eventMigration.getId());

        eventMigration.setResults(results);
        return eventMigration;
    }

    @Override
    public void insertEventMigration(EventMigration eventMigration) {
        insertMigration(eventMigration.getMigration());
        Map<String, Object> parameters = new HashMap<String, Object>(3);
        eventMigration.setId(seqNext("event_migration_seq"));
        parameters.put("id", eventMigration.getId());
        parameters.put("uuid", eventMigration.getUuid());
        parameters.put("id_migration", eventMigration.getMigration().getId());
        eventMigrationInsert.execute(parameters);
                        
    }

    @Override
    public void updateEventMigration(EventMigration eventMigration) {
        updateMigration(eventMigration.getMigration());
        for(EventMigrationResult r : eventMigration.getResults()){
            if(r.getId() == null){                
               insertEventMigrationResult(eventMigration, r);
            }else{
                logger.info("Update 'EventMigrationResult' is not supported");
            }        
        }
    }              

    @Override
    public void updateMigration(Migration m) {
       int result = getJdbcTemplate().update("update migration set migrated=? ,sql_script=?, name=?, description=? where id=?", m.getMigrated(), m.getSqlScript(), m.getName(), m.getDescription(), m.getId());
       if(result == 0){
           throw new NullPointerException("There is no migration to update!");
       }
    }

    private void insertMigration(Migration m) {
        //usingColumns("id","uuid","name","description","created","migrated","sql_script");  
        Map<String, Object> parameters = new HashMap<String, Object>(6);
        m.setId(seqNext("migration_seq"));
        parameters.put("id", m.getId());
        parameters.put("uuid", m.getUuid());
        parameters.put("name", m.getName());
        parameters.put("description", m.getDescription());
        parameters.put("created", m.getCreated());
        parameters.put("migrated", m.getMigrated());
        parameters.put("sql_script", m.getSqlScript());
        migrationInsert.execute(parameters);
    }

    private Migration mapMigrationRow(ResultSet rs) throws SQLException {
        Migration migration = new Migration();
        migration.setCreated(rs.getTimestamp("created"));
        migration.setDescription(rs.getString("description"));
        migration.setId(rs.getLong("id"));        
        migration.setMigrated(rs.getTimestamp("migrated"));        
        migration.setName(rs.getString("name"));
        migration.setSqlScript(rs.getString("sql_script"));
        migration.setUuid(rs.getString("uuid"));
        return migration;
    }    
    
    
    private void insertEventMigrationResult(EventMigration em, EventMigrationResult m) {
        //usingColumns("id","uuid","name","description","created","migrated","sql_script");          
        m.setId(seqNext("event_migration_result_seq"));                
        MapSqlParameterSource source = new MapSqlParameterSource();
        source.addValue("id", m.getId(), Types.BIGINT);
        source.addValue("events", new PostgreSQLStringArray(m.getEvents()), Types.ARRAY);        
        source.addValue("overseer_name", m.getOverseerName(), Types.VARCHAR);        
        source.addValue("overseer_url", m.getOverseerUrl(),Types.VARCHAR);        
        source.addValue("id_event_migration", em.getId(),Types.BIGINT);        
        eventMigrationResultInsert.execute(source);
    }

    
    
}
