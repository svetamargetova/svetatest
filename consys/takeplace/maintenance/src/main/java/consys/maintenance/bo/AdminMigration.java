/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.bo;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 *
 * @author palo
 */
public class AdminMigration implements Serializable {

    private Long id;
    private String uuid;
    private Migration migration;
    

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the migration
     */
    public Migration getMigration() {
        return migration;
    }

    /**
     * @param migration the migration to set
     */
    public void setMigration(Migration migration) {
        this.migration = migration;
    }

  

    @Override
    public boolean equals(Object o) {
        if (o instanceof AdminMigration) {
            AdminMigration e = (AdminMigration) o;
            return new EqualsBuilder().append(this.uuid, e.uuid).append(this.migration, e.getMigration()).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }
}
