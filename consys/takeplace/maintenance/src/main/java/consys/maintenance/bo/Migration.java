/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.bo;

import java.io.Serializable;
import java.util.Date;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 *
 * @author palo
 */
public class Migration implements Serializable{
    
    private Long id;    
    private String uuid;
    private String name;
    private String description;
    private Date created;
    private Date migrated;
    private String sqlScript;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the migrated
     */
    public Date getMigrated() {
        return migrated;
    }

    /**
     * @param migrated the migrated to set
     */
    public void setMigrated(Date migrated) {
        this.migrated = migrated;
    }

    /**
     * @return the sqlScript
     */
    public String getSqlScript() {
        return sqlScript;
    }

    /**
     * @param sqlScript the sqlScript to set
     */
    public void setSqlScript(String sqlScript) {
        this.sqlScript = sqlScript;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof Migration) {
            Migration e = (Migration) o;
            return new EqualsBuilder().append(this.uuid, e.uuid).isEquals();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }
    
    
    
}
