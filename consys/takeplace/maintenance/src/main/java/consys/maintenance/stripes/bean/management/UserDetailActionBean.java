/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean.management;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.maintenance.stripes.bean.MaintenanceActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;


/**
 *
 * @author palo
 */
public class UserDetailActionBean extends MaintenanceActionBean{
    
   @SpringBean
    private UserLoginInfoService loginInfoService;

    private String uuid;
    private UserLoginInfo user;

    @DontValidate
    @DefaultHandler
    public Resolution preEdit() throws NoRecordException, RequiredPropertyNullException{
        user = loginInfoService.loadUserLoginInfoWithUserByUuid(getUuid());
        return new ForwardResolution("/management/userDetail.jsp");
    }

    public UserLoginInfo getUser() {
        return user;
    }

    public void setUser(UserLoginInfo user) {
        this.user = user;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
