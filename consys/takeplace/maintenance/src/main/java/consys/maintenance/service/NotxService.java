/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.service;

import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import java.util.List;


/**
 *
 * @author palo
 */
public interface NotxService {
    
    public void reindexUsers();

    /**
     * Testuje vsetkych uzivatelov a v liste vrati zoznam uzivatelov ktory nie su
     * v integrite.
     * 
     * @return 
     */
    public List<User> testIntegrity();
    
    
    public void installUser(User user);
    
    
    public void installEmailTemplates();
    
}
