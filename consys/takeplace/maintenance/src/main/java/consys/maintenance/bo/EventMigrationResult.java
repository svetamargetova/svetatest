/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.bo;

/**
 *
 * @author palo
 */
public class EventMigrationResult {
    
    private Long id;    
    private String overseerUrl;
    private String overseerName;
    private String[] events;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the overseerUuid
     */
    public String getOverseerUrl() {
        return overseerUrl;
    }

    /**
     * @param overseerUuid the overseerUuid to set
     */
    public void setOverseerUrl(String overseerUuid) {
        this.overseerUrl = overseerUuid;
    }

    /**
     * @return the overseerName
     */
    public String getOverseerName() {
        return overseerName;
    }

    /**
     * @param overseerName the overseerName to set
     */
    public void setOverseerName(String overseerName) {
        this.overseerName = overseerName;
    }

    /**
     * @return the events
     */
    public String[] getEvents() {
        return events;
    }

    /**
     * @param events the events to set
     */
    public void setEvents(String[] events) {
        this.events = events;
    }
    
   
}
