/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.bo.thumb;

import java.util.Date;

/**
 *
 * @author palo
 */
public class AdminMigrationListItem {

    private String uuid;
    private String name;
    private String description;
    private Date created;
    private Date migrated;

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the migrated
     */
    public Date getMigrated() {
        return migrated;
    }

    /**
     * @param migrated the migrated to set
     */
    public void setMigrated(Date migrated) {
        this.migrated = migrated;
    }
}
