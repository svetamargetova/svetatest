/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.service;

import consys.common.core.exception.NoRecordException;
import consys.maintenance.bo.AdminMigration;
import consys.maintenance.bo.EventMigration;
import consys.maintenance.bo.thumb.AdminMigrationListItem;
import consys.maintenance.bo.thumb.EventMigrationListItem;
import java.util.List;

/**
 *
 * @author palo
 */
public interface MigrationService {

    /**
     * Vylistuje vsetky migracie vzhladom na filter. Filter:
     * <ol>
     * <li> <code>alreadyMigrated == true</code> vrati vsetky uz vykonane migracie od najaktualnejsej
     * <li> <code>alreadyMigrated == false</code> vrati vsetky nevykonane migracie zoradene podla datumu vlozenia
     * </ol>
     * @param migrated
     * @param startPage
     * @param itemsPerPage
     * @return 
     */
    public List<AdminMigrationListItem> listAdminMigrations(boolean alreadyMigrated, int startPage, int itemsPerPage);

    /**
     * Vrati pocet vsetkych migracii vzhalodm na filter
     * @param alreadyMigrated
     * @return 
     */
    public int loadAdminMigrationsCount(boolean alreadyMigrated);

    /**
     * Vrati vsetky eventovkse migracie vzhladom na filter.     
     * <ol>
     * <li> <code>alreadyMigrated == true</code> vrati vsetky uz vykonane migracie od najaktualnejsej
     * <li> <code>alreadyMigrated == false</code> vrati vsetky nevykonane migracie zoradene podla datumu vlozenia
     * </ol>
     * @param alreadyMigrated
     * @param startPage
     * @param itemsPerPage
     * @return 
     */
    public List<EventMigrationListItem> listEventMigrations(boolean alreadyMigrated, int startPage, int itemsPerPage);

    /**
     * Vrati pocet eventovskych migracii vzhaldom na filter.
     * @param alreadyMigrated
     * @return 
     */
    public int loadEventMigrationsCount(boolean alreadyMigrated);

    /**
     * Nacita detailne eventovsku migraciu podla uuid
     * @param uuid eventovskej migracie
     * @return migracia
     * @throws NoRecordException ak taka neexistuje
     */
    public EventMigration loadEventMigrationDetail(String uuid) throws NoRecordException;

    /**
     * Nacita detailne adminovsku migraciu podla uuid
     * @param uuid administrtorskej migracie
     * @return migracia
     * @throws NoRecordException ak taka neexistuje
     */
    public AdminMigration loadAdminMigrationDetail(String uuid) throws NoRecordException;

    /**
     * Vlozi novu administracnu migraciu
     * 
     * @param adminMigration 
     */
    public void createAdminMigration(AdminMigration adminMigration);

    /**
     * Vlozi novu eventovsku migraciu
     * @param adminMigration 
     */
    public void createEventMigration(EventMigration adminMigration);
    
    public void updateEventMigration(EventMigration eventMigration);
    
    public void updateAdminMigration(AdminMigration adminMigration);

    public void executeEventMigration(EventMigration adminMigration);

    public void executeAdminMigration(AdminMigration adminMigration);
}
