package consys.maintenance.stripes;

import java.util.Date;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import net.sourceforge.stripes.exception.StripesRuntimeException;
import net.sourceforge.stripes.format.Formatter;
import org.apache.commons.lang.time.FastDateFormat;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DateFormatter implements Formatter<Date> {

    /** Maintains a map of named formats that can be used instead of patterns. */
    protected static final Map<String, FastDateFormat> namedPatterns = new HashMap<String, FastDateFormat>();

    static {
        namedPatterns.put("date", FastDateFormat.getInstance("dd.MM.yyyy"));
        namedPatterns.put("datetime", FastDateFormat.getInstance("dd.MM.yyyy hh:mm:ss"));

    }
    private String formatType;
    private String formatPattern;
    private Locale locale;
    private FastDateFormat format;

    /** Sets the format type to be used to render dates as Strings. */
    @Override
    public void setFormatType(String formatType) {
        this.formatType = formatType;
    }

    /** Gets the format type to be used to render dates as Strings. */
    public String getFormatType() {
        return formatType;
    }

    /** Sets the named format string or date pattern to use to format the date. */
    @Override
    public void setFormatPattern(String formatPattern) {
        this.formatPattern = formatPattern;
    }

    /** Gets the named format string or date pattern to use to format the date. */
    public String getFormatPattern() {
        return formatPattern;
    }

    /** Sets the locale that output String should be in. */
    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /** Gets the locale that output String should be in. */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Constructs the DateFormat used for formatting, based on the values passed to the
     * various setter methods on the class.  If the formatString is one of the named formats
     * then a DateFormat instance is created of the specified type and format, otherwise
     * a SimpleDateFormat is constructed using the pattern provided and the formatType is ignored.
     *
     * @throws StripesRuntimeException if the formatType is not one of 'date', 'time' or 'datetime'.
     */
    @Override
    public void init() {
        // Default these values if they were not supplied
        if (formatPattern == null) {
            formatPattern = "date";
        }

        if (formatType == null) {
            formatType = "date";
        }

        String lcFormatString = formatPattern.toLowerCase();
        String lcFormatType = formatType.toLowerCase();

        // Now figure out how to construct our date format for our locale
        if (namedPatterns.containsKey(lcFormatString)) {

            if (lcFormatType.equals("date")) {
                format = namedPatterns.get(lcFormatString);;
            } else if (lcFormatType.equals("datetime")) {
                format = namedPatterns.get(lcFormatString);
            } else {
                throw new StripesRuntimeException("Invalid formatType for Date: " + formatType
                        + ". Allowed types are 'date', 'time' and 'datetime'.");
            }
        } else {
            format = FastDateFormat.getInstance(formatPattern, locale);
        }
    }

    /**
     * Gets the date format that will format the date. Subclasses that wish to alter the date format
     * should override init(), call super.init(), and then obtain the date format object.
     */
    public FastDateFormat getDateFormat() {
        return this.format;
    }

    /**
     * Sets the date format that will format the date. Subclasses that wish to set the date format
     * should override init() and then set the date format object.
     */
    public void setDateFormat(FastDateFormat dateFormat) {
        this.format = dateFormat;
    }

    /** Formats a Date as a String using the rules supplied when the formatter was built. */
    @Override
    public String format(Date input) {
        return this.format.format(input);
    }
}
