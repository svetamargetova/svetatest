/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean.management;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.maintenance.bo.EventMigration;
import consys.maintenance.service.MigrationService;
import consys.maintenance.stripes.bean.MaintenanceActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import net.sourceforge.stripes.validation.DateTypeConverter;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidateNestedProperties;

/**
 *
 * @author palo
 */
public class EventMigrationDetailActionBean extends MaintenanceActionBean {

    @SpringBean
    private MigrationService migrationService;
    private String uuid;
    @ValidateNestedProperties(value = {
        @Validate(field = "migration.name", minlength= 1, maxlength= 255),
        @Validate(field = "migration.description", minlength= 1, maxlength=255),
        @Validate(field = "migration.sqlScript", minlength= 1),
        @Validate(field = "migration.created", required=false, converter=DateTypeConverter.class),
        @Validate(field = "migration.migrated", required=false,converter=DateTypeConverter.class),
        @Validate(field = "migration.uuid", required=false),
        @Validate(field = "migration.id", required=false),
        @Validate(field = "id", required=false),
        @Validate(field = "uuid", required=false)})
    private EventMigration migration;

    @DontValidate
    @DefaultHandler
    public Resolution show() throws NoRecordException, RequiredPropertyNullException {
        if (uuid == null) {
            logger.debug("Creating NEW Event Migration");
        } else {
            logger.debug("Loading migration with uuid {}", uuid);
            setMigration(migrationService.loadEventMigrationDetail(uuid));
        }
        return new ForwardResolution("/management/eventMigrationDetail.jsp");
    }

   public Resolution create() {
        migrationService.createEventMigration(migration);
        return back();

    }

    
    public Resolution update() {
        migrationService.updateEventMigration(migration);
        return back();
    }

    @DontValidate
    public Resolution back() {
        return new ForwardResolution(EventsMigrationActionBean.class);
    }

    
    public Resolution migrate() {
        migrationService.executeEventMigration(migration);
        return back();
    }


    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the migration
     */
    public EventMigration getMigration() {
        return migration;
    }

    /**
     * @param migration the migration to set
     */
    public void setMigration(EventMigration migration) {
        this.migration = migration;
    }
}
