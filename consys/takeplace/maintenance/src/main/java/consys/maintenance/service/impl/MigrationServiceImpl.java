/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.service.impl;

import consys.admin.common.core.service.MaintenanceService;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.service.EventService;
import consys.admin.ws.overseer.EventWebServiceEnum;
import consys.admin.ws.overseer.service.OverseerWebService;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.event.maintenance.ws.MigrateEventDatabasesRequest;
import consys.event.maintenance.ws.MigrateEventDatabasesResponse;
import consys.event.maintenance.ws.MigrationResponseItem;
import consys.maintenance.bo.AdminMigration;
import consys.maintenance.bo.EventMigration;
import consys.maintenance.bo.EventMigrationResult;
import consys.maintenance.bo.Migration;
import consys.maintenance.bo.thumb.AdminMigrationListItem;
import consys.maintenance.bo.thumb.EventMigrationListItem;
import consys.maintenance.dao.MigrationDao;
import consys.maintenance.service.MigrationService;
import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public class MigrationServiceImpl implements MigrationService {

    private static final Logger logger = LoggerFactory.getLogger(MigrationServiceImpl.class);
    private MigrationDao migrationDao;
    private MaintenanceService maintenanceService;
    private EventService eventService;
    private OverseerWebService overseerWebService;

    /*------------------------------------------------------------------------*/
    /* ---  A D M I N I S T A R T I O N - L I S T --- */
    /*------------------------------------------------------------------------*/
    @Override
    public List<AdminMigrationListItem> listAdminMigrations(boolean alreadyMigrated, @Min(value = 0) int startPage, @Min(value = 0) int itemsPerPage) {
        return migrationDao.listAdminMigrations(alreadyMigrated, startPage, itemsPerPage);
    }

    /*------------------------------------------------------------------------*/
    /* ---  A D M I N I S T A R T I O N - L O A D --- */
    /*------------------------------------------------------------------------*/
    @Override
    public int loadAdminMigrationsCount(boolean alreadyMigrated) {
        return migrationDao.loadAdminMigrationsCount(alreadyMigrated);
    }

    @Override
    public AdminMigration loadAdminMigrationDetail(@NotNull String uuid) throws NoRecordException {
        AdminMigration am = migrationDao.loadAdminMigrationDetail(uuid);
        if (am == null) {
            throw new NoRecordException("There is no AdminMigration with uuid " + uuid);
        }
        return am;
    }

    /*------------------------------------------------------------------------*/
    /* ---  A D M I N I S T A R T I O N   -   C R E A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void createAdminMigration(AdminMigration adminMigration) {
        adminMigration.setUuid(UuidProvider.getUuid());
        initMigrationEntity(adminMigration.getMigration());
        migrationDao.insertAdminMigration(adminMigration);
    }

    /*------------------------------------------------------------------------*/
    /* ---  A D M I N I S T A R T I O N   -   U P D A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateAdminMigration(AdminMigration adminMigration) {
        validateMigrationSql(adminMigration.getMigration());
        migrationDao.updateAdminMigration(adminMigration);
    }

    @Override
    public void executeAdminMigration(AdminMigration adminMigration) {
        logger.info("Migrating Administration Database with: {} {} ", adminMigration.getMigration().getName(), adminMigration.getMigration().getSqlScript());
        maintenanceService.updateDatabase(adminMigration.getMigration().getSqlScript());
        adminMigration.getMigration().setMigrated(DateProvider.getCurrentDate());
        migrationDao.updateAdminMigration(adminMigration);
    }
    /*------------------------------------------------------------------------*/
    /* ---  E V E N T - L I S T --- */
    /*------------------------------------------------------------------------*/

    @Override
    public List<EventMigrationListItem> listEventMigrations(boolean alreadyMigrated, @Min(value = 0) int startPage, @Min(value = 0) int itemsPerPage) {
        return migrationDao.listEventMigrations(alreadyMigrated, startPage, itemsPerPage);
    }

    /*------------------------------------------------------------------------*/
    /* ---  E V E N T - L O A D --- */
    /*------------------------------------------------------------------------*/
    @Override
    public int loadEventMigrationsCount(boolean alreadyMigrated) {
        return migrationDao.loadEventMigrationsCount(alreadyMigrated);
    }

    @Override
    public EventMigration loadEventMigrationDetail(@NotNull String uuid) throws NoRecordException {
        EventMigration eventMigration = migrationDao.loadEventMigrationDetail(uuid);
        if (eventMigration == null) {
            throw new NoRecordException("No EventMigration object with uuid: " + uuid);
        }
        return eventMigration;
    }

    /*------------------------------------------------------------------------*/
    /* ---  E V E N T - C R E A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void createEventMigration(EventMigration eventMigration) {
        eventMigration.setUuid(UuidProvider.getUuid());
        initMigrationEntity(eventMigration.getMigration());
        migrationDao.insertEventMigration(eventMigration);
    }

    /*------------------------------------------------------------------------*/
    /* ---  E V E N T - U P D A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateEventMigration(EventMigration eventMigration) {
        validateMigrationSql(eventMigration.getMigration());
        migrationDao.updateEventMigration(eventMigration);
    }

    @Override
    public void executeEventMigration(EventMigration eventMigration) {
        logger.info("Migrating Events Database with: {} {} ", eventMigration.getMigration().getName(), eventMigration.getMigration().getSqlScript());
        List<EventOverseer> os = getEventService().listOverseers();
        try {
            for (EventOverseer eventOverseer : os) {

                EventMigrationResult migrationResult = new EventMigrationResult();
                migrationResult.setOverseerName(eventOverseer.getName());
                migrationResult.setOverseerUrl(eventOverseer.getWebServicesUrl());

                // Call webservice
                MigrateEventDatabasesRequest request = new MigrateEventDatabasesRequest();
                request.setMigrationName(eventMigration.getMigration().getName());
                request.setSqlScript(eventMigration.getMigration().getSqlScript());
                logger.info("Migrate events at: {} ", eventOverseer.getName());
                MigrateEventDatabasesResponse response = getOverseerWebService().sendAndReceive(EventWebServiceEnum.Maintenance, eventOverseer.getWebServicesUrl(), request);
                logger.info("Migration results:");
                String[] events = new String[response.getMigratedEventsList().size()];
                for (int i = 0; i < events.length; i++) {
                    MigrationResponseItem item = response.getMigratedEventsList().get(i);
                    StringBuilder msg = new StringBuilder();
                    if (item.getFinished() == null) {
                        msg.append("ERROR: ").append(item.getEventName()).append(" ").append(item.getEventDatabase()).append(" ").append(item.getError());
                        logger.info(msg.toString());
                    } else {
                        msg.append("Success: ").append(item.getEventName()).append(" ").append(item.getEventDatabase()).append(" ").append(item.getFinished().toGregorianCalendar().getTime());
                        logger.info(msg.toString());
                    }
                    events[i] = msg.toString();
                }
                migrationResult.setEvents(events);
                eventMigration.getResults().add(migrationResult);

            }
            eventMigration.getMigration().setMigrated(DateProvider.getCurrentDate());
            migrationDao.updateEventMigration(eventMigration);
        } catch (Exception e) {
            logger.error("Error when migrating: ", e);
        }

    }

    /*------------------------------------------------------------------------*/
    /* ---  U T I L S --- */
    /*------------------------------------------------------------------------*/
    private void initMigrationEntity(Migration m) {
        m.setCreated(DateProvider.getCurrentDate());
        m.setUuid(UuidProvider.getUuid());
        validateMigrationSql(m);

    }

    private void validateMigrationSql(Migration m) {
        m.setSqlScript(m.getSqlScript().replaceAll("[\\n|\\t|\\r]", ""));
    }

    /**
     * @return the migrationDao
     */
    public MigrationDao getMigrationDao() {
        return migrationDao;
    }

    /**
     * @param migrationDao the migrationDao to set
     */
    public void setMigrationDao(MigrationDao migrationDao) {
        this.migrationDao = migrationDao;
    }

    /**
     * @return the maintenanceService
     */
    public MaintenanceService getMaintenanceService() {
        return maintenanceService;
    }

    /**
     * @param maintenanceService the maintenanceService to set
     */
    public void setMaintenanceService(MaintenanceService maintenanceService) {
        this.maintenanceService = maintenanceService;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @return the overseerWebService
     */
    public OverseerWebService getOverseerWebService() {
        return overseerWebService;
    }

    /**
     * @param overseerWebService the overseerWebService to set
     */
    public void setOverseerWebService(OverseerWebService overseerWebService) {
        this.overseerWebService = overseerWebService;
    }
}
