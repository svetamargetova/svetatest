package consys.maintenance.stripes.bean.management;

import consys.admin.common.core.solr.IndexHandler;
import consys.admin.common.core.solr.IndexHandlerRegistry;
import consys.admin.common.core.solr.IndexService;
import consys.admin.event.core.bo.Event;
import consys.admin.event.core.bo.EventOverseer;
import consys.admin.event.core.service.EventService;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.ws.overseer.EventWebServiceEnum;
import consys.admin.ws.overseer.service.OverseerWebService;
import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.bo.Image;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.util.ImageHelper;
import consys.event.maintenance.ws.RecreateEventProfilePageRequest;
import consys.maintenance.service.NotxService;
import consys.maintenance.stripes.bean.MaintenanceActionBean;
import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import javax.imageio.ImageIO;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;
import org.apache.commons.io.output.ByteArrayOutputStream;

/**
 *
 * @author palo
 */
public class ManagementActionBean extends MaintenanceActionBean {

    @SpringBean
    private IndexService indexService;
    @SpringBean
    private IndexHandlerRegistry handlerRegistry;
    @SpringBean
    private UserLoginInfoService loginInfoService;
    @SpringBean
    private NotxService notxService;
    @SpringBean
    private EventService eventService;
    @SpringBean
    private AwsFileStorageService fileStorageService;
    @SpringBean
    private OverseerWebService overseerWebService;
    private List<User> users;

    @DontValidate
    @DefaultHandler
    public Resolution show() {
        return new ForwardResolution("/management/management.jsp");
    }

    public Resolution recreateEventProfilePages() {
        List<EventOverseer> overseers = eventService.listOverseers();
        for (EventOverseer eventOverseer : overseers) {
            logger.info("Migrating overseer - {}", eventOverseer.getName());
            overseerWebService.sendAndReceive(EventWebServiceEnum.Maintenance, eventOverseer.getWebServicesUrl(), new RecreateEventProfilePageRequest());
        }
        return new ForwardResolution("/management/management.jsp");
    }

    public Resolution reinstallTemplatesNotx() {
        notxService.installEmailTemplates();
        return new ForwardResolution("/management/management.jsp");
    }

    public Resolution testAllUsersInNotx() throws RequiredPropertyNullException, NoRecordException {
        users = notxService.testIntegrity();
        logger.debug("Users not in Notx");
        for (User user : users) {
            logger.debug("Reindexing  {}", user);
            notxService.installUser(user);
        }

        return new ForwardResolution("/management/management.jsp");
    }

    public Resolution reindexUsersNotx() throws RequiredPropertyNullException, NoRecordException {
        notxService.reindexUsers();
        return new ForwardResolution("/management/management.jsp");
    }

    @DontValidate
    public Resolution reindexSolr() throws RequiredPropertyNullException, NoRecordException {
        logger.info("Reindexing users in SOLR");


        List<UserLoginInfo> users = null;
        IndexHandler<User> handler = handlerRegistry.getHandlerForClass(User.class);
        if (handler == null) {
            throw new NullPointerException("IndexHandler for User.class has not been found!");
        }


        int allUsers = loginInfoService.loadAllUserLoginInfoCount();
        int itemsPerPage = 50;
        int firstItem = 0;
        for (int i = 0; i < allUsers; i += itemsPerPage) {
            users = loginInfoService.listDetailedUserLoginInfo(i, itemsPerPage);
            logger.info("Indexing " + firstItem + " -> " + itemsPerPage + firstItem);

            for (UserLoginInfo info : users) {
                indexService.doIndex(handler.createDoc(info.getUser()));
            }
            firstItem += itemsPerPage;
        }

        return getContext().getSourcePageResolution();
    }

    public Resolution updateEventDetailProperty() {
        logger.info("Updating all events detail property");

        List<Event> events = eventService.listAllEvents(null);
        for (Event e : events) {
            eventService.updateEventDetailsProperty(e);
        }

        return getContext().getSourcePageResolution();
    }

    public Resolution migrateImagesIntoS3() throws RequiredPropertyNullException, NoRecordException, ServiceExecutionFailed, IOException {


        throw new IllegalArgumentException("Already migrated");

//        
//        logger.info("Migrating images into S3");
//        logger.info("Migrating users .... ");
//        migrateUsersProfilePictures();
//        logger.info("Migrating events .... ");
//        migrateEventsPictures();
        //   return getContext().getSourcePageResolution();
    }

    protected void createImage(Image image, int width, int height, String uuid) throws IOException, ServiceExecutionFailed {
        BufferedImage buffImage = ImageIO.read(new ByteArrayInputStream(image.getByteData()));

        // zmena na velikost
        if (buffImage.getWidth() != width && buffImage.getHeight() != height) {
            Dimension dim = ImageHelper.getScaledSize(buffImage.getWidth(), buffImage.getHeight(), width, height);
            buffImage = ImageHelper.getScaledInstance(buffImage, dim.width, dim.height, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
        }

        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (!ImageIO.write(buffImage, image.getContentType().substring(image.getContentType().indexOf('/') + 1), bout)) {
            throw new IOException();
        }
        ByteArrayInputStream stream = new ByteArrayInputStream(bout.toByteArray());

        Enumeration e = getContext().getServletContext().getInitParameterNames();
        while (e.hasMoreElements()) {
            logger.info("Param: {}", e.nextElement());
        }
        fileStorageService.createObject(getContext().getServletContext().getInitParameter("Image.S3.Bucket"), uuid, image.getFileName(), image.getContentType(), AwsFileStorageService.Cache.CACHE.getCache(), false, stream, bout.size());
    }

    /**
     * @return the users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }
}
