/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.displaytag;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.servlet.jsp.PageContext;
import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

/**
 *
 * @author palo
 */
public class DateDecorator implements  DisplaytagColumnDecorator{

    private static final FastDateFormat format = FastDateFormat.getInstance("dd.MM.yyyy", Calendar.getInstance().getTimeZone(), Locale.getDefault());
    
    @Override
    public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
        if(columnValue instanceof Date){
            return format.format((Date)columnValue);
        }else{
            return "No Date";
        }
    }
    
}
