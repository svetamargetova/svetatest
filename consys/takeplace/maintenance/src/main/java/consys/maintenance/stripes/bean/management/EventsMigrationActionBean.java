/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean.management;

import consys.maintenance.bo.thumb.EventMigrationListItem;
import consys.maintenance.service.MigrationService;
import consys.maintenance.stripes.bean.ListActionBean;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author palo
 */
public class EventsMigrationActionBean extends ListActionBean<EventMigrationListItem> {

    @SpringBean
    private MigrationService migrationService;
    private boolean alreadyMigrated = false;
    private String uuid;

    public EventsMigrationActionBean() {
        super("/management/eventsMigration.jsp");
    }

    /**
     * @return the alreadyMigrated
     */
    public boolean isAlreadyMigrated() {
        return alreadyMigrated;
    }

    /**
     * @param alreadyMigrated the alreadyMigrated to set
     */
    public void setAlreadyMigrated(boolean alreadyMigrated) {
        this.alreadyMigrated = alreadyMigrated;
    }

    
    @DontValidate
    public Resolution newMigration() {
        return new ForwardResolution(EventMigrationDetailActionBean.class);
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) {
        getItems().setList(migrationService.listEventMigrations(isAlreadyMigrated(), firstItemIndex, itemsPerPage));
        getItems().setTotal(migrationService.loadAdminMigrationsCount(isAlreadyMigrated()));
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
