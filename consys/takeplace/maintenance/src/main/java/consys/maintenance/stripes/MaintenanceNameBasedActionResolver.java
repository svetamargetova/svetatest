/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes;

import java.util.Collections;
import java.util.Set;
import net.sourceforge.stripes.controller.NameBasedActionResolver;
import net.sourceforge.stripes.util.Literal;

/**
 *
 * @author palo
 */
public class MaintenanceNameBasedActionResolver extends NameBasedActionResolver {
    
    public static final Set<String> packages =
        Collections.unmodifiableSet(Literal.set("web", "www", "stripes", "action", "bean"));

    
    @Override
    public Set<String> getBasePackages() { return packages; }
    
    
}
