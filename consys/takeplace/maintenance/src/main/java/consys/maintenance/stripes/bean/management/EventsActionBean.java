package consys.maintenance.stripes.bean.management;

import consys.admin.event.core.bo.thumb.EventThumb;
import consys.admin.event.core.service.EventService;
import consys.maintenance.stripes.bean.ListActionBean;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author palo
 */
public class EventsActionBean extends ListActionBean<EventThumb> {

    @SpringBean
    private EventService eventService;
    private boolean upcoming = true;
    private boolean onlyVisible = false;

    public EventsActionBean() {
        super("/management/events.jsp");
    }

    @Override
    protected void reloadList(int firstItem, int itemsPerPage) {
        getItems().setList(eventService.listEventsChronologically(isUpcoming(), isOnlyVisible(), firstItem, itemsPerPage, null));
        getItems().setTotal(eventService.listEventsChronologicallyCount(isUpcoming(), isOnlyVisible(), null));
    }

    /**
     * @return the upcoming
     */
    public boolean isUpcoming() {
        return upcoming;
    }

    /**
     * @param upcoming the upcoming to set
     */
    public void setUpcoming(boolean upcoming) {
        this.upcoming = upcoming;
    }

    /**
     * @return the onlyVisible
     */
    public boolean isOnlyVisible() {
        return onlyVisible;
    }

    /**
     * @param onlyVisible the onlyVisible to set
     */
    public void setOnlyVisible(boolean onlyVisible) {
        this.onlyVisible = onlyVisible;
    }
}
