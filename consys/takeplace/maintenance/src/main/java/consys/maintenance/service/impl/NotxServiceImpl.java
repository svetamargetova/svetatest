/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.service.impl;

import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.core.util.UserNotxUtils;
import consys.common.core.notx.SystemMessage;
import consys.common.utils.collection.Lists;
import consys.maintenance.service.NotxService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import net.notx.ws.template.*;
import net.notx.ws.user.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class NotxServiceImpl implements NotxService {

    private static final Logger logger = LoggerFactory.getLogger(NotxServiceImpl.class);
    @Autowired
    private UserLoginInfoService loginInfoService;
    @Autowired
    private UserWebService notxUserWebservice;
    @Autowired
    private TemplateWebService notxTemplateWebService;

    @Override
    public void installUser(User user) {
        CreateOrUpdateUserResponse resp = notxUserWebservice.createOrUpdateUser(UserNotxUtils.createRequest(user));
        if (StringUtils.isNotBlank(resp.getError())) {
            logger.error("Failed to index: {}, reason: {}", user, resp.getError());
        }
        
    }

    @Override
    public void reindexUsers() {
        int allUsers = loginInfoService.loadAllUserLoginInfoCount();
        logger.info("Reindexing users '{}' in NotX", allUsers);
        int itemsPerPage = 50;
        int firstItem = 0;
        List<UserLoginInfo> users = null;
        try {
            for (int i = allUsers; i > 0;) {

                users = loginInfoService.listDetailedUserLoginInfo(firstItem, itemsPerPage);
                logger.info("Indexing to NotX {} - {}", firstItem, (users.size() == itemsPerPage ? itemsPerPage : users.size()) + firstItem);

                for (UserLoginInfo userLoginInfo : users) {
                    CreateOrUpdateUserResponse resp = notxUserWebservice.createOrUpdateUser(UserNotxUtils.createRequest(userLoginInfo.getUser()));
                    if (StringUtils.isNotBlank(resp.getError())) {
                        logger.error("Failed to index: {}, reason: {}", userLoginInfo, resp.getError());
                    }
                    --i;
                }
                firstItem += itemsPerPage;
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException("An error occured! " + ex.getMessage());
        }
    }

    @Override
    public List<User> testIntegrity() {
        List<User> bad = Lists.newArrayList();

        int allUsers = loginInfoService.loadAllUserLoginInfoCount();
        logger.info("Testing users '{}' in NotX", allUsers);
        int itemsPerPage = 50;
        int firstItem = 0;
        List<UserLoginInfo> users = null;
        try {
            for (int i = allUsers; i > 0;) {                
                users = loginInfoService.listDetailedUserLoginInfo(firstItem, itemsPerPage);
                logger.info("Testing {} - {}: {}",new Object[]{firstItem, firstItem+itemsPerPage ,users.size()});
                for (UserLoginInfo userLoginInfo : users) {
                    LoadUserRequest req = UserWebService.OBJECT_FACTORY.createLoadUserRequest();
                    req.setUserId(userLoginInfo.getUser().getUuid());
                    LoadUserResponse resp = notxUserWebservice.loadUser(req);
                    if (StringUtils.isNotBlank(resp.getError())) {
                        logger.error("NotX error: {} {}", userLoginInfo, resp.getError());
                        bad.add(userLoginInfo.getUser());
                    } else if (resp.getUser() == null
                            || resp.getUser().getContacts().isEmpty()
                            || !resp.getUser().getContacts().get(0).getValue().equalsIgnoreCase(userLoginInfo.getUser().getEmailOrMergeEmail())) {
                        logger.error("NotX integrity changed: {}, notx: {}", userLoginInfo.getUser(), resp.getUser());
                        //bad.add(userLoginInfo.getUser());
                    }
                    --i;
                }
                firstItem += itemsPerPage;
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException("An error occured! ",ex);
        }
        return bad;
    }

    @Override
    public void installEmailTemplates() {

        // -- Administrace --
        addTemplate(SystemMessage.ACCOUNT_LOST_PASSWORD, "Takeplace - Reset password");
        addTemplate(SystemMessage.ACCOUNT_LOST_PASSWORD_CHANGE, "Takeplace - Password sucessfully changed");
        addTemplate(SystemMessage.INVITATION_ACCEPTED_INVITED, "Takeplace - Invitation to $event_name$ has been accepted");
        addTemplate(SystemMessage.INVITATION_ACCEPTED_INVITOR, "Takeplace - Invitation to $event_name$ has been accepted by $invited_name$");
        addTemplate(SystemMessage.INVITATION_DECLINED, "Takeplace - Invitation to $event_name$ has been declined by $invited_name$");
        addTemplate(SystemMessage.INVITATION_NEW_USER_NO_CONFIRM, "Takeplace - You have been invited to $event_name$");
        addTemplate(SystemMessage.INVITATION_NEW_USER_WITH_CONFIRM, "Takeplace - You have been invited to $event_name$");
        addTemplate(SystemMessage.INVITATION_NO_CONFIRM, "Takeplace - You have been invited to $event_name$");
        addTemplate(SystemMessage.INVITATION_WITH_CONFIRM, "Takeplace - You have been invited to $event_name$");
        addTemplate(SystemMessage.NEW_ACCOUNT_ACTIVATION, "Takeplace - Account has been activated");
        addTemplate(SystemMessage.NEW_ACCOUNT_REGISTRATION, "Takeplace - Account registration");
        addTemplate(SystemMessage.NEW_ACCOUNT_REPEATING_ACTIVATION, "Takeplace - Account activation request");

        // -- Event - Registration --
        addTemplate(SystemMessage.NEW_ORDER, "Takeplace — New order at $event_name$");
        addTemplate(SystemMessage.NEW_ORDER_EXPIRATION_WARNING, "Takeplace — Order at $event_name$ will expire soon");
        addTemplate(SystemMessage.NEW_REGISTRATION, "Takeplace — Registred at $event_name$");
        addTemplate(SystemMessage.NEW_REGISTRATION_EXPIRATION_WARNING, "Takeplace — Registration at $event_name$ will expire soon");
        addTemplate(SystemMessage.ORDER_CANCELED_BY_OWNER, "Takeplace — You have canceled order at $event_name$");
        addTemplate(SystemMessage.ORDER_CANCELED_BY_SYSTEM, "Takeplace — Order at $event_name$ has been canceled");
        addTemplate(SystemMessage.ORDER_CONFIRMED, "Takeplace — Order at $event_name$ has been confirmed");
        addTemplate(SystemMessage.ORDER_ZERO_CONFIRMED, "Takeplace — Order at $event_name$ has been confirmed");
        addTemplate(SystemMessage.REGISTRATION_CANCELED_BY_OWNER, "Takeplace — You have canceled registration at $event_name$");
        addTemplate(SystemMessage.REGISTRATION_CANCELED_BY_SYSTEM, "Takeplace - Registration at $event_name$ has been canceled");
        addTemplate(SystemMessage.REGISTRATION_CONFIRMED, "Takeplace — Registration at $event_name$ has been confirmed");
        addTemplate(SystemMessage.REGISTRATION_ZERO_CONFIRMED, "Takeplace — Registration at $event_name$ has been confirmed");

        // -- Event - Contribution --
        addTemplate(SystemMessage.CONTRIBUTION_ACCEPTED, "Takeplace — Your contribution '$title$' at $event_name$ has been accepted");
        addTemplate(SystemMessage.CONTRIBUTION_REJECTED, "Takeplace — Your contribution '$title$' at $event_name$ has been rejected");




    }

    private void addTemplate(SystemMessage type, String subject) {

        EngineMutation mutation = TemplateWebService.OBJECT_FACTORY.createEngineMutation();
        mutation.setEngine("email");
        mutation.setLang("en");

        // nazov emailu
        TemplatePart subjectPart = TemplateWebService.OBJECT_FACTORY.createTemplatePart();
        subjectPart.setName("subject");
        subjectPart.setValue(subject);
        mutation.getParts().add(subjectPart);

        // html body
        TemplatePart htmlBodyPart = TemplateWebService.OBJECT_FACTORY.createTemplatePart();
        htmlBodyPart.setName("html_body");
        htmlBodyPart.setValue(readTextFileOnClassPath("email_templates/" + type.name() + ".html"));
        mutation.getParts().add(htmlBodyPart);

        // text body
        TemplatePart plainBodyPart = TemplateWebService.OBJECT_FACTORY.createTemplatePart();
        plainBodyPart.setName("text_body");
        plainBodyPart.setValue(readTextFileOnClassPath("email_templates/" + type.name() + ".txt"));
        mutation.getParts().add(plainBodyPart);

        CreateTemplateRequest req = TemplateWebService.OBJECT_FACTORY.createCreateTemplateRequest();

        Template t = new Template();
        t.setName(type.name());
        t.setDesciption(type.name());
        t.getMutations().add(mutation);
        req.setTemplate(t);
        notxTemplateWebService.createTemplate(req);
    }

    public String readTextFileOnClassPath(String fileName) {
        StringBuilder contents = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(fileName), "UTF-8"));
            String text = null;

            while ((text = reader.readLine()) != null) {
                contents.append(text).append(
                        System.getProperty("line.separator"));
            }

        } catch (Exception e) {
            throw new IllegalArgumentException("Somethng wrong!", e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException("Somethng wrong!", e);
            }
        }
        return contents.toString();
    }
}
