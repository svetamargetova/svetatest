/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean.management;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.maintenance.stripes.bean.ListActionBean;
import consys.maintenance.stripes.displaytag.PaginatedListAdapter;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author palo
 */
public class UsersActionBean extends ListActionBean<UserLoginInfo> {

    
    @SpringBean
    private UserLoginInfoService loginInfoService;


    public UsersActionBean() {
        super("/management/users.jsp");
    }
            

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws RequiredPropertyNullException, NoRecordException {
        getItems().setList(loginInfoService.listDetailedUserLoginInfo(firstItemIndex, itemsPerPage));                
        getItems().setTotal(loginInfoService.loadAllUserLoginInfoCount());
    }           
}
