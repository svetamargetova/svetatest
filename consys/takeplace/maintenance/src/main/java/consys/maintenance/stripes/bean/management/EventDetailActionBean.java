/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean.management;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ReadOnlyPropertyException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.maintenance.stripes.bean.MaintenanceActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author palo
 */
public class EventDetailActionBean extends MaintenanceActionBean{
            
    @SpringBean
    private EventService eventService;
    
    private String uuid;
    
    private Event event;
    
    @DontValidate
    @DefaultHandler
    public Resolution preEdit() throws NoRecordException, RequiredPropertyNullException{
        event = eventService.loadEvent(uuid);
        return new ForwardResolution("/management/eventDetail.jsp");
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the event
     */
    public Event getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(Event event) {
        this.event = event;
    }
    
    @DontValidate
    public Resolution save() throws RequiredPropertyNullException, ServiceExecutionFailed, ReadOnlyPropertyException{
        eventService.updateEventDetails(event);
        return new ForwardResolution("/management/events.jsp"); 
    }
    
    @DontValidate
    public Resolution delete() throws RequiredPropertyNullException, NoRecordException{
        eventService.deleteEvent(event.getUuid());
        return new ForwardResolution("/management/Events.action"); 
    }

    
    
}
