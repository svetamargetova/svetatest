package consys.maintenance.stripes;

import consys.maintenance.stripes.bean.MaintenanceActionBean;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sourceforge.stripes.controller.StripesConstants;
import net.sourceforge.stripes.exception.DefaultExceptionHandler;
import net.sourceforge.stripes.validation.SimpleError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ExceptionHandler extends DefaultExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @Override
    public void handle(Throwable throwable, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MaintenanceActionBean bean = (MaintenanceActionBean) request.getAttribute(StripesConstants.REQ_ATTR_ACTION_BEAN);
        logger.error("Handle exception: ", throwable);
        if (bean != null) {
            try {
                Throwable e = unwrap(throwable);
                StringBuilder sb = new StringBuilder(e.getMessage()+"\n");
                while(e.getCause() != null){
                    e = e.getCause();
                    sb.append(e.getMessage()).append("\n");                    
                }
                
                bean.getContext().getValidationErrors().addGlobalError(
                        new SimpleError("Exception: " + sb.toString()));
                bean.getContext().getSourcePageResolution().execute(request, response);
            } catch (Exception ex) {
            }
        } 
    }
}
