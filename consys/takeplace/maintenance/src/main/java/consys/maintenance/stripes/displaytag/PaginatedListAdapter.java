/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.displaytag;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;

/**
 *
 * @author palo
 */
public class PaginatedListAdapter<T> implements PaginatedList {

    /** default page size */
    private static int DEFAULT_PAGE_SIZE = 50;
    /** current page index, starts at 0 */
    private int index;
    /** number of results per page */
    private int pageSize = DEFAULT_PAGE_SIZE;
    /** total results (records, not pages) */
    private int fullListSize;
    /** list of results in the current page */
    private List<T> list;
    /** default sorting order */
    private SortOrderEnum sortDirection = SortOrderEnum.ASCENDING;
    /** sort criteria (sorting property name) */
    private String sortCriterion;
    

    public PaginatedListAdapter() {
        // empty
    }

    /**
     * Factory-style constructor. Initializes properties with a request attributes
     */
    public PaginatedListAdapter(HttpServletRequest request) {
        sortCriterion = request.getParameter("sort");
        sortDirection = "desc".equals(request.getParameter("dir")) ? SortOrderEnum.DESCENDING : SortOrderEnum.ASCENDING;
        pageSize = DEFAULT_PAGE_SIZE;
        String page = request.getParameter("page");
        index = page == null ? 0 : Integer.parseInt(page) - 1;
    }

    /**
     * @return A readable description of this instance state for logs
     */
    @Override
    public String toString() {
        return "PageResponse { index = " + index + ", pageSize = " + pageSize + ", total = " + fullListSize + ", items="+ list.size() + "}";
    }

    public int getFirstRecordIndex() {
        return index * pageSize;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public List<T> getList() {
        return list;
    }

    public void setList(List<T> results) {
        this.list = results;
    }

    @Override
    public int getFullListSize() {
        return fullListSize;
    }

    public void setTotal(int total) {
        this.fullListSize = total;
    }

    public int getTotalPages() {
        return (int) Math.ceil(((double) fullListSize) / pageSize);
    }

    @Override
    public int getObjectsPerPage() {
        return pageSize;
    }

    @Override
    public int getPageNumber() {
        return index + 1;
    }

    @Override
    public String getSearchId() {
        // unimplemented
        return null;
    }

    @Override
    public String getSortCriterion() {
        return sortCriterion;
    }

    @Override
    public SortOrderEnum getSortDirection() {
        return sortDirection;
    }

    public String getSqlSortDirection() {
        return SortOrderEnum.DESCENDING.equals(sortDirection) ? "desc" : "asc";
    }

    public void setSortCriterion(String sortCriterion) {
        this.sortCriterion = sortCriterion;
    }

    public void setSortDirection(SortOrderEnum sortDirection) {
        this.sortDirection = sortDirection;
    }

    /**
     * Adds the order by parameter to a query. If there is already an
     * order by, <b>prepends</b> the current order to the one included
     * in the query
     */
    public String addOrderBy(String query) {
        // no order by
        if (sortCriterion == null) {
            return query;
        }
        int pos = locateOrderBy(query);
        // order by one criteria
        if (pos == -1) {
            return query + " order by " + sortCriterion + " " + getSqlSortDirection();
        }
        // order by two criteria
        return query.substring(0, pos) + sortCriterion + " " + getSqlSortDirection() + ", " + query.substring(pos);
    }

    /**
     * Locates the "order by" clause, if there is one
     * @param query the QL query to inspect
     * @return The position of the first character after the "order by" clause if there is one, -1 if not.
     */
    private int locateOrderBy(String query) {
        int pos = query.lastIndexOf(" order ");
        if (pos != -1) {
            pos = query.lastIndexOf("by ", pos + 7);
            if (pos != -1) {
                return pos + 3;
            }
        }
        return pos;

    }
}
