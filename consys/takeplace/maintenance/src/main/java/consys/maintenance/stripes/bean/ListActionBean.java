/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes.bean;

import consys.common.core.exception.ConsysException;
import consys.maintenance.stripes.displaytag.PaginatedListAdapter;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 *
 * @author palo
 */
public abstract class ListActionBean<T> extends MaintenanceActionBean {

    private PaginatedListAdapter<T> items;
    private String jspPath;

    public ListActionBean(String jspPath) {
        this.jspPath = jspPath;
    }

    @DontValidate
    @DefaultHandler
    public Resolution show() throws ConsysException {
        setItems(new PaginatedListAdapter<T>());
        reloadList(getFirstRecord(),getItemsPerPage());
        return new ForwardResolution(jspPath);
    }

    @DontValidate
    public Resolution list() throws ConsysException {
        setItems(new PaginatedListAdapter<T>(getContext().getRequest()));
        reloadList(getFirstRecord(),getItemsPerPage());
        return new ForwardResolution(jspPath);
    }

    public int getFirstRecord() {
        return items.getFirstRecordIndex();
    }

    public int getItemsPerPage() {
        return items.getObjectsPerPage();
    }

    public int getfullListSize() {
        return items.getFullListSize();
    }

    protected abstract void reloadList(int firstItemIndex, int itemsPerPage) throws ConsysException;

    /**
     * @return the items
     */
    public PaginatedListAdapter<T> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(PaginatedListAdapter<T> items) {
        this.items = items;
    }
}
