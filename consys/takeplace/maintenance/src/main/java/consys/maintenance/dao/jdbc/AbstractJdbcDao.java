/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.dao.jdbc;


import javax.sql.DataSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

/**
 *
 * @author palo
 */
public class AbstractJdbcDao {
    private DataSource dataSource;
    private SimpleJdbcTemplate jdbcTemplate;
    
    /**
     * @return the dataSource
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * @param dataSource the dataSource to set
     */
    public void setDataSource(DataSource dataSource) {        
        this.dataSource = dataSource;
        this.jdbcTemplate = new SimpleJdbcTemplate(dataSource);
    }

    /**
     * @return the jdbcTemplate
     */
    public SimpleJdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
    
    
    public Long seqNext(String seqName){
        return jdbcTemplate.queryForLong("select nextval(?)", seqName);
    }
    
}
