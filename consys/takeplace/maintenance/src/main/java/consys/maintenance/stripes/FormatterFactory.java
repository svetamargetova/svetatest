/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.maintenance.stripes;

import java.util.Date;
import java.util.Locale;
import net.sourceforge.stripes.config.Configuration;
import net.sourceforge.stripes.format.DefaultFormatterFactory;
import net.sourceforge.stripes.format.Formatter;

/**
 *
 * @author palo
 */
public class FormatterFactory extends DefaultFormatterFactory {

    /** Stores a reference to the configuration and configures the default formatters. */
    @Override
    public void init(Configuration configuration) throws Exception {
        super.init(configuration);
        // pretazeni vlastneho formattera
        add(Date.class, DateFormatter.class);
    }

    @Override
    public Formatter<?> getFormatter(Class<?> clazz, Locale locale, String formatType, String formatPattern) {
        

        return super.getFormatter(clazz, locale, formatType, formatPattern);
    }
}
