<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/management_layout.jsp">

    <stripes:layout-component name="contents">
        
        <div>
            <stripes:form action="/management/Users.action" >
                <display:table name="${actionBean.items}" id="result" sort="external"   partialList="true" pagesize="50" size="${actionBean.fullListSize}" requestURI="/management/Users.action?list">

                    <display:column style="width:100%" property="user.fullName" title="Name" />         
                    <display:column property="user.email" title="Email" />         
                    <display:column property="user.defaultUserOrganization.shortInfo" title="Oragnization" />         
                    <display:column property="registerDate" title="Registred" decorator="consys.maintenance.stripes.displaytag.DateTimeDecorator"/>         
                    <display:column property="active" title="Active" />         
                    <display:column property="deleted" title="Deleted" />
                    <display:column title="Action" >
                        <stripes:link href="/management/UserDetail.action">
                            <stripes:param name="uuid" value="${result.user.uuid}"/>Edit</stripes:link>
                    </display:column>
                       
                </display:table>
            </stripes:form>


        </div>









    </stripes:layout-component>
</stripes:layout-render>