<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/management_layout.jsp">

    <stripes:layout-component name="contents">

        <div>            
            <stripes:form action="/management/Events.action" >
                <div class="actions">
                    <div class="action">
                        <stripes:label for="fupcoming" name="only.upcoming"/>
                        <stripes:checkbox id="fupcoming" name="upcoming" />
                    </div>                                        
                    <div class="action">
                        <stripes:label for="fvisible" name="only.visible"/>
                        <stripes:checkbox id="fvisible" name="onlyVisible" />
                    </div> 
                    <div class="action">
                        <stripes:submit name="list" value="Reload"/>
                    </div> 
                    
                </div>
                <div class="clear"></div>

                <display:table name="${actionBean.items}" id="result" sort="external"   partialList="true" pagesize="50" size="${actionBean.fullListSize}" requestURI="/management/Events.action?list">  

                    <display:column property="acronym" title="Acronym" />
                    <display:column style="width:100%" property="fullName" title="Name" />                             
                    <display:column property="web"  title="Web" />         
                    <display:column property="from" title="From" decorator="consys.maintenance.stripes.displaytag.DateDecorator"/>         
                    <display:column property="to"   title="To" decorator="consys.maintenance.stripes.displaytag.DateDecorator"/>         
                    <display:column property="year" title="Year" />         
                    <display:column title="Action" >
                        <stripes:link href="/management/EventDetail.action">
                            <stripes:param name="uuid" value="${result.uuid}"/>Edit
                        </stripes:link>
                    </display:column>         
                </display:table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>