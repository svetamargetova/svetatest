<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/management_layout.jsp">

    <stripes:layout-component name="contents">



        <stripes:form action="/management/EventMigrationDetail.action" focus="">
            <h3>
                <c:choose>
                    <c:when test="${actionBean.migration eq null}">
                        <fmt:message key="management.migration.title.event.new"/>
                    </c:when>     
                    <c:when test="${actionBean.migration.migration.migrated eq null}">
                        <fmt:message key="management.migration.title.event.edit"/>
                    </c:when>
                    <c:otherwise>                       
                        <fmt:message key="management.migration.title.event.view"/>                       
                    </c:otherwise>                    
                </c:choose>              
            </h3>

            <stripes:errors/>
            <table>
                <tbody>
                    <c:if test="${actionBean.migration ne null}">
                        <tr>
                            <td><stripes:label for="mid" name="common.uuid" /> </td>
                            <td><stripes:text id="mid" name="migration.uuid" readonly="true"/></td>
                        </tr>
                    </c:if>

                    <tr>
                        <td><stripes:label for="mname" name="management.migration.name" /> </td>
                        <td><stripes:text id="mname" name="migration.migration.name" readonly="${actionBean.migration.migration.migrated ne null}" /></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="mdesc" name="management.migration.description" /> </td>
                        <td><stripes:text id="mdesc" name="migration.migration.description" readonly="${actionBean.migration.migration.migrated ne null}"/></td>
                    </tr>

                    <c:if test="${actionBean.migration ne null}">
                        <tr>
                            <td><stripes:label for="mcreated" name="management.migration.created" /> </td>
                            <td><stripes:text id="mcreated" name="migration.migration.created" formatPattern="datetime" readonly="true"/></td>
                        </tr>

                        <tr>
                            <td><stripes:label for="mmigrated" name="management.migration.migrated" /> </td>
                            <td><stripes:text id="mmigrated" name="migration.migration.migrated" formatPattern="datetime" readonly="true" /></td>
                        </tr>
                    </c:if> 

                    <tr>
                        <td><stripes:label for="mscript" name="management.migration.script" /></td>
                        <td><stripes:textarea id="mscript" name="migration.migration.sqlScript" readonly="${actionBean.migration.migration.migrated ne null}"/></td>
                    </tr>
                    <c:if test="${actionBean.migration.migration.migrated ne null}">
                        <tr>
                            <td><stripes:label name="management.migration.event.results" /></td>
                            <td>
                                <c:forEach var="result" items="${actionBean.migration.results}">
                                    <div>
                                        <table>
                                            <tr>
                                                <td><stripes:label name="management.migration.event.overseer.name"/></td>
                                                <td>${result.overseerName}</td>
                                            </tr>
                                            <tr>
                                                <td><stripes:label name="management.migration.event.overseer.url"/></td>
                                                <td>${result.overseerUrl}</td>
                                            </tr>
                                            <tr>
                                                <td><stripes:label name="management.migration.event.overseer.events"/></td>
                                                <td>
                                                    <c:forEach var="event" items="${result.events}">
                                                        <div style="font-style: italic">${event}</div>                                       
                                                    </c:forEach>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </c:forEach>                                                                    
                            </td>
                        </tr>
                    </c:if>

                    <tr><td colspan="2">
                            <c:choose>
                                <c:when test="${actionBean.migration eq null}">
                                    <stripes:submit   name="create" value="create"/>
                                    <stripes:submit   name="back"   value="cancel"/>    
                                </c:when>
                                <c:when test="${actionBean.migration.migration.migrated eq null}">
                                    <stripes:text id="mid"   name="migration.migration.id" style="display:none"/>
                                    <stripes:text id="muuid" name="migration.migration.uuid" style="display:none"/>
                                    <stripes:text id="amid"  name="migration.id" style="display:none"/>
                                    <stripes:submit   name="update" value="update"/>
                                    <stripes:submit   name="migrate" value="migrate"/>
                                    <stripes:submit   name="back" value="back"/>    
                                </c:when>
                                <c:otherwise>                                    
                                    <stripes:submit   name="back" value="back"/>    
                                </c:otherwise>  
                            </c:choose>         

                        </td></tr>
                </tbody>
            </table>    
        </stripes:form>


    </stripes:layout-component>
</stripes:layout-render>