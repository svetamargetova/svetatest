<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/management_layout.jsp">

    <stripes:layout-component name="contents">

        <div>            
            <stripes:form action="/management/AdminMigrations.action" >
                <div class="actions">
                    <div class="action">                        
                        <stripes:submit id="newMigration" name="newMigration"/>                                                    
                    </div>                                                                                
                </div>
                <div class="clear"></div>

                <div class="action">
                    <stripes:label for="falreadyMigrated" name="management.migration.filter.migrated"/>
                    <stripes:checkbox id="falreadyMigrated" name="alreadyMigrated" />
                </div> 
                <div class="action">
                    <stripes:submit name="list" value="Reload"/>
                </div>
                <div class="clear"></div>

                <display:table name="${actionBean.items}" id="result" sort="external"   partialList="true" pagesize="50" size="${actionBean.fullListSize}" requestURI="/management/AdminMigrations.action?list">                      
                    <display:column property="uuid" title="Uuid"/>    
                    <display:column style="width:80%" property="name" title="Name" />                                                 
                    <display:column property="created" title="Created" decorator="consys.maintenance.stripes.displaytag.DateDecorator"/>         
                    <display:column property="migrated"   title="Migrated" decorator="consys.maintenance.stripes.displaytag.DateDecorator"/>                             
                    <display:column title="Action" style="width:100%">
                        <c:choose>
                            <c:when test="${result.migrated eq null}">
                                <stripes:link href="/management/AdminMigrationDetail.action" style="display:block">
                                    <stripes:param name="uuid" value="${result.uuid}"/>
                                    <fmt:message key="edit"/>
                                </stripes:link>                                                             
                            </c:when>    
                            <c:otherwise>
                                <stripes:link href="/management/AdminMigrationDetail.action">
                                    <stripes:param name="uuid" value="${result.uuid}"/>
                                    <fmt:message key="view"/>
                                </stripes:link>
                            </c:otherwise>
                        </c:choose>
                    </display:column>         
                </display:table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>