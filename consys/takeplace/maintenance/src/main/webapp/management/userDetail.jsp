<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/layout/taglibs.jsp" %>

<stripes:layout-render name="/layout/management_layout.jsp">

    <stripes:layout-component name="contents">

        <stripes:errors/>
        <stripes:form action="/management/UserDetail.action" focus="">
            <table>
                <tbody>
                    <tr>
                        <td><stripes:label for="uid" name="common.uuid" /> </td>
                        <td><stripes:text id="uid" name="user.user.uuid" /></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="ufirst" name="management.user.firstName" /> </td>
                        <td><stripes:text id="ufirst" name="user.user.firstName"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="umiddle" name="management.user.middleName" /> </td>
                        <td><stripes:text id="umiddle" name="user.user.middleName"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="ulast" name="management.user.lastName" /> </td>
                        <td><stripes:text id="ulast" name="user.user.lastName"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="ufd" name="management.user.frontDegree" /> </td>
                        <td><stripes:text id="ufd" name="user.user.frontDegree"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="urd" name="management.user.rearDegree" /> </td>
                        <td><stripes:text id="urd" name="user.user.rearDegree"/></td>
                    </tr>

                     <tr>
                        <td><stripes:label for="ugender" name="management.user.gender" /> </td>
                        <td><stripes:text id="ugender" name="user.user.genderType.name"/></td>
                    </tr>

                    <tr>
                        <td><stripes:label for="uemail" name="management.user.email" /> </td>
                        <td><stripes:text id="uemail" name="user.user.email"/></td>
                    </tr>
                                       
                    <tr>
                        <td><stripes:label for="uactive" name="management.user.active" /></td>
                        <td><stripes:checkbox id="uactive" name="user.active"/></td>
                    </tr>
                    
                    <tr>
                        <td><stripes:label for="udeleted" name="management.user.deleted" /></td>
                        <td><stripes:checkbox id="udeleted" name="user.deleted"/></td>
                    </tr>

                    <tr>
                        <td>
                            <stripes:submit id="changePassword" class="todo" name="passwordChange">
                            </stripes:submit>
                        </td>
                        <td>
                            <stripes:submit  class="todo"  name="save" value="Save"/>
                        </td>
                    </tr>
                </tbody>
            </table>    
        </stripes:form>


    </stripes:layout-component>
</stripes:layout-render>