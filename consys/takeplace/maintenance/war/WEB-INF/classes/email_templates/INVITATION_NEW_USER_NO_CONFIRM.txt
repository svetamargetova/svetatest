Dear $salutation$,

you have been invited by $from$ ($from_email$) to join $event_name$ at Takeplace.
Takeplace (www.takeplace.eu) is the leading web service for online event 
management and maintenance of professional communities.

To complete your Takeplace account please click the following link to complete your
personal profile and log in:

http://hekuba.fi.muni.cz:17777/takeplace/takeplace.html?cmd=Invitation&token=$token$

Alternativelly, you can access the page:

http://hekuba.fi.muni.cz:17777/takeplace/takeplace.html?cmd=ActivateAccount

and enter the invitation key: $token$

Your account details:
    Name:   $salutation$
    E-mail: $email$

$message$


If clicking the link doesn't seem to work, you can copy and paste the link 
into your browser's address window, or retype it there.
Best regards,
Takeplace Team


Please do not reply to this message. The email address is used for email notifications
only so you will receive no response. If you have any question, please contact
support@takeplace.eu.
    

This message was addressed to $email$. Please read our Terms of use 
(http://www.takeplace.eu/terms_of_use_en.pdf).
                
