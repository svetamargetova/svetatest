Dear $salutation$,

$invited_name$ ($invited_email$) has accepted your invitation to $event_name$ 
at Takeplace.

Your account details:
    Name:   $salutation$
    E-mail: $email$


Best regards,
Takeplace Team


Please do not reply to this message. The email address is used for email notifications
only so you will receive no response. If you have any question, please contact
support@takeplace.eu.
    

This message was addressed to $email$. Please read our Terms of use 
(http://www.takeplace.eu/terms_of_use_en.pdf).
                
       
