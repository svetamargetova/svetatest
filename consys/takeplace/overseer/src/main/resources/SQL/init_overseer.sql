-- Overseer role
CREATE ROLE overseer WITH LOGIN ENCRYPTED PASSWORD 'password' CREATEDB CREATEROLE;

-- Tabulka obsahujuca eventy
--DROP TABLE event
--DROP SEQUENCE event_seq;


CREATE TABLE event
(
  id bigint NOT NULL,
  event_uuid character varying(36) NOT NULL,
  db_pass character varying(255) NOT NULL,
  db_user character varying(255) NOT NULL,
  db_url character varying(255) NOT NULL,
  db_created timestamp without time zone NOT NULL,
  acronym character varying(128),
  last_activity timestamp without time zone,
  priority_to timestamp without time zone,
  CONSTRAINT pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE event OWNER TO overseer;

CREATE SEQUENCE event_seq START 1 NO MAXVALUE;
ALTER TABLE event_seq OWNER TO overseer;

