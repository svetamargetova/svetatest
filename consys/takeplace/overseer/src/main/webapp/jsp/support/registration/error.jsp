<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Takeplace Support Error</title>
    </head>
    <body>
        <h1>Configuration Error</h1>

        <h2>We're really sorry but you have not created support request properly.</h2>
        <p>
            Request have to consist from: https://app.takeplace.eu/&lt;overseer_name&gt;/support/registration/bundles.do?eid=&lt;event_uuid&gt;&&lt;api_key&gt;
        </p>
        <p>
            Error: ${err_msg}
        </p>
    </body>
</html