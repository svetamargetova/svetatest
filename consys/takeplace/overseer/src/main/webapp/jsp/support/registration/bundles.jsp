<%@page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bundles</title>

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
             Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


        <!-- Mobile viewport optimized: j.mp/bplateviewport -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="noindex, follow" name="robots">
        <meta name="googlebot" content="nosnippet,noarchive"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/registration.css" />
    </head>
    <body>
        <div id="bundleList">
            <c:forEach items="${bundles}" var="bundle">
                <div class="bundle">
                    <div class="infoBox">
                        <div class="title">
                            <span>${bundle.title}</span>
                        </div>
                        <div class="description">
                            <span>${bundle.description}</span>
                        </div>
                    </div>
                    <div class="priceBox">
                        <div class="price">
                            <c:choose>
                                <c:when test="${currency == 'EUR'}">&euro; ${bundle.price}</c:when>
                                <c:when test="${currency == 'USD'}">$ ${bundle.price}</c:when>
                                <c:when test="${currency == 'CZK'}">${bundle.price} Kč</c:when>
                                <c:otherwise>${bundle.price}</c:otherwise>
                            </c:choose>
                        </div>
                        <c:choose>
                            <c:when test="${prefix != ''}">
                                <div class="btn"><a target="_blank" href="https://${prefix}.takeplace.eu/register/register.html?cmd=Register&eid=${eid}&api_key=${api_key}&pid=${bundle.bundleUuid}">${choose}</a><span></span></div>
                            </c:when>
                            <c:otherwise>
                                <div class="btn"><a target="_blank" href="https://app.takeplace.eu/register/register.html?cmd=Register&eid=${eid}&api_key=${api_key}&pid=${bundle.bundleUuid}">${choose}</a><span></span></div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="clear"></div>
                </div>
            </c:forEach>
        </div>
    </body>
</html>