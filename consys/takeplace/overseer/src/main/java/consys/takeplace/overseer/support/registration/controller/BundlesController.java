package consys.takeplace.overseer.support.registration.controller;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.common.utils.collection.Lists;
import consys.common.utils.enums.Currency;
import consys.event.common.api.properties.CommonProperties;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class BundlesController extends AbstractController {

    // konstanty
    public static final String BUNDLES_JSP = "bundles";
    public static final String MODEL_API_KEY = "api_key";
    public static final String MODEL_BUNDLES = "bundles";
    public static final String MODEL_EVENT_ID = "eid";
    public static final String CORPORATION_PREFIX = "prefix";
    public static final String LOCALE = "locale";
    public static final String CHOOSE = "choose";
    public static final String CURRENCY = "currency";
    // sluzby
    private RegistrationBundleService bundleService;
    private SystemPropertyService systemPropertyService;

    public void setBundleService(RegistrationBundleService bundleService) {
        this.bundleService = bundleService;
    }

    public RegistrationBundleService getBundleService() {
        return bundleService;
    }

    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String apiKey = request.getParameter(MODEL_API_KEY);
        String prefix = request.getParameter(CORPORATION_PREFIX);
        if (StringUtils.isBlank(prefix)) {
            prefix = "";
        }
        String locale = request.getParameter(LOCALE);

        ModelAndView model;

        if (StringUtils.isBlank(apiKey)) {
            model = new ModelAndView("error");
            model.addObject("err_msg", "Missing API key - API key can be found in general administration");
        } else {
            try {
                List<RegistrationOption> options = getBundleService().listMainRegistrationBundlesOptions();
                model = new ModelAndView(BUNDLES_JSP);
                // vyfiltrujeme vsetky ktore ktore su uz mimo
                List<RegistrationOption> filtredOptions = Lists.newArrayList();
                for (RegistrationOption registrationOption : options) {
                    // ak je uz neaktivny
                    if (registrationOption.getToDate() == null) {
                        continue;
                    }
                    // ak je plny
                    if (registrationOption.getCapacity() > 0 && registrationOption.getCapacity() <= registrationOption.getRegistred()) {
                        continue;
                    }
                    filtredOptions.add(registrationOption);
                }

                SystemProperty currency = systemPropertyService.loadSystemProperty(CommonProperties.EVENT_CURRENCY);

                model.addObject(MODEL_BUNDLES, filtredOptions);
                model.addObject(MODEL_EVENT_ID, request.getParameter(MODEL_EVENT_ID));
                model.addObject(MODEL_API_KEY, apiKey);
                model.addObject(CORPORATION_PREFIX, prefix);
                model.addObject(CHOOSE, choose(locale));
                model.addObject(CURRENCY, Currency.valueOf(currency.getValue()));
            } catch (NoRecordException e) {
                model = new ModelAndView("error");
                model.addObject("err_msg", "There are no bundles yet.");
            }
        }
        return model;
    }

    /** lokalizovany popisek tlacitka */
    private String choose(String locale) {
        final String enChoose = "Choose";
        if (StringUtils.isBlank(locale)) {
            return enChoose;
        } else if (locale.equalsIgnoreCase("en")) {
            return enChoose;
        } else if (locale.equalsIgnoreCase("cs")) {
            return "Vybrat";
        } else {
            return enChoose;
        }
    }
}
