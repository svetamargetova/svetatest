package consys.event.overall.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.client.action.LoadEventImportantDatesAction;
import consys.event.common.gwt.client.bo.ClientEventImportantDates;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionService;
import consys.event.registration.core.service.RegistrationBundleService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadEventImportantDatesActionHandler implements EventActionHandler<LoadEventImportantDatesAction, ClientEventImportantDates> {

    @Autowired
    private RegistrationBundleService registrationBundleService;
    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<LoadEventImportantDatesAction> getActionType() {
        return LoadEventImportantDatesAction.class;
    }

    @Override
    public ClientEventImportantDates execute(LoadEventImportantDatesAction action) throws ActionException {
        // registrace
        Date registrationFrom = registrationBundleService.loadFirstRegistrationBegin();
        Date registrationTo = registrationBundleService.loadLastRegistrationEnd();
        // prispevky
        Date contributionFrom = submissionService.loadSubmissionBegindSend();
        Date contributionTo = submissionService.loadSubmissionEndSend();

        // naplneni objektu
        ClientEventImportantDates dates = new ClientEventImportantDates();
        dates.setRegistrationFrom(registrationFrom);
        dates.setRegistrationTo(registrationTo);
        dates.setContributionFrom(contributionFrom);
        dates.setContributionTo(contributionTo);
        return dates;
    }
}
