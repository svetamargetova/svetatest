
$(document).ready(function(){  
    loadRegistrationOptions();
    loadSubmissionOptions();
});

var czechDict = {};
czechDict["registration"] = "Registrace";
czechDict["register"] = "REGISTRUJ";
czechDict["contributions"] = "P\u0159ísp\u011bvky";
czechDict["free"] = "ZDARMA";

var englishDict = {};
englishDict["registration"] = "Registration";
englishDict["register"] = "REGISTER";
englishDict["contributions"] = "Contributions";
englishDict["free"] = "FREE";

var dict = {};
dict["cs"] = czechDict;
dict["en"] = englishDict;

function drawRegistrations(options) {
    var tag = document.getElementById("registration");
    if(tag != null && options.length > 0) {
        var map = dict[localization];
        var result = "<h2>" + map.registration + "</h2>";
        result += '<div class="dataDiv">';
        for(var i = 0 ; i < options.length; i++){
            var option = options[i];
            // ak je price -1 potom je to uz zavrite
            if(option.price != -1) {
                if(i != 0) {
                    result += '<div class="rSeparator"></div>';
                }
                result += '<div><div class="date1r">';
                result += option.title;
                result += '';
                result += '</div><div class="date2r">';
                result += price(option.price, map);
                result += '</div><div class="date3r"><a href="';
                result += registrationUrl;
                result += '/takeplace/takeplace.html?cmd=Register&eid=';
                result += eventUuid;
                result += '&api_key=';
                result += eventUuid;
                result += '&pid=';
                result += option.uuid;
                result += '"><img src="http://static.takeplace.eu/register-';
                result += localization;
                result += '.png" alt="';
                result += map.register;
                result += '" border="0"></a></div><div class="clear"></div></div>';
            }
        }
        result += '</div>';
        tag.innerHTML = result;
    }
}

function price(value, map) {
    if(value == 0) {
        return map.free;
    }
    switch(currency) {
        case 'CZK':
            return value + " K\u010d";
        case 'USD':
            return "$ " + value;
        case 'EUR':
            return "\u20ac " + value;
    }
}

function drawSubmissions(options) {
    var tag = document.getElementById('contribution');
    if(tag != null && options.length > 0) {
        var map = dict[localization];
        var result = "<h2>" + map.contributions + "</h2>";
        result += '<div class="dataDiv">';
        for(var i = 0 ; i < options.length; i++){
            if(i != 0) {
                result += '<div class="rSeparator"></div>';
            }
            var option = options[i];
            result += '<div><div>';
            result += option.title;
            result += '</div><div class="contributionDescription">';
            result += option.description;
            result += '</div></div>';
        }
        result += '</div>';
        tag.innerHTML = result;
    }
}
   
function loadRegistrationOptions(){
    var content = {
        login : "API", 
        password : "API", 
        aid : 1 , 
        event:eventUuid, 
        action:"registration-options"
    };
                                                     
    jQuery.ajax({
        url: overseerApiUrl,
        type: "POST",                    
        dataType: "json",                    
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(content),                                        
        success: function(result) {
            // {"uuid":"3dafa1a8bd9e45a6ab607be26f062e7b","title":"Ticket","description":"toto je tiket","capacity":0,"registred":0,"price":200}
            drawRegistrations(result.body.options);
        },
                    
        "error": function(d,msg) {                            
        }
    }); 
                
}
            
function loadSubmissionOptions(){
    var content = {
        login : "API", 
        password : "API", 
        aid : 1 , 
        event:eventUuid, 
        action:"submission-options"
    };
                                                     
    jQuery.ajax({
        url: overseerApiUrl,
        type: "POST",                    
        dataType: "json",                    
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(content),                                        
        success: function(result) {
            // {"uuid":"3dafa1a8bd9e45a6ab607be26f062e7b","title":"Ticket","description":"toto je tiket"}
            drawSubmissions(result.body.options);
        },
                    
        "error": function(d,msg) {                            
        }
    }); 
                
}


