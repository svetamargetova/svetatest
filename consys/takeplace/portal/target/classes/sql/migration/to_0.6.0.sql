--
--   INVOICE_ITEM primary key => ID
--
-- pridani noveho columnu do invoice_item => long id
ALTER TABLE invoice_item ADD COLUMN id bigint;

-- automaticke nastaveni seckych existujucich items na nejake id
UPDATE invoice_item set id=id_invoice;

-- id not null
ALTER TABLE invoice_item ALTER COLUMN id SET NOT NULL;


-- odstraneni primary key
ALTER TABLE invoice_item DROP CONSTRAINT invoice_item_pkey;

-- nastaveni id jak primary key
ALTER TABLE invoice_item ADD CONSTRAINT invoice_item_pkey PRIMARY KEY (id);




