
-- odstraneni column id_acmc_profile
ALTER TABLE transfer DROP COLUMN id_acmc_profile;

-- Odstraneni omedzena not null na transfer invoice , lebo ta sa vytvori az ked sa bude prevadzat.
ALTER TABLE transfer ALTER COLUMN id_commission_invoice DROP NOT NULL;

-- Odstraneni omedzena not null na transfer date
ALTER TABLE transfer ALTER COLUMN transfer_date DROP NOT NULL;

-- Pridani column pro produkty
-- nova column
ALTER TABLE product ADD COLUMN category integer;

-- nastaveni seckych produkt category na 2 - ticket
UPDATE product set category=2;

-- nastaveni ze category je not-null
ALTER TABLE product ALTER COLUMN category SET NOT NULL;








