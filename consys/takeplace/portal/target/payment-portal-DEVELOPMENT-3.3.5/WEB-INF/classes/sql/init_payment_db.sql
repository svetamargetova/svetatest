--
-- Name: event; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE event (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    invoice_client_number character varying(4),
    active boolean,
    activation_date timestamp without time zone,
    activation_end_date timestamp without time zone,
    create_date timestamp without time zone NOT NULL,
    id_license_order bigint,
    CONSTRAINT id PRIMARY KEY (id)
);


ALTER TABLE public.event OWNER TO consys;

--
-- Name: event_payment_profile; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE event_payment_profile (
    id_event bigint NOT NULL,
    id_payment_profile bigint NOT NULL,
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    order_invoice_counter integer,
    order_invoice_series_counter integer,
    transfer_invoice_counter integer,
    active boolean,
    bank_invoice_to_profile boolean,
    note character varying(512),
    currency integer,
    vat integer,
    order_unit_fee_bank numeric(8,4),
    order_share_fee_bank numeric(5,2),
    order_unit_fee_online numeric(8,4),
    order_share_fee_online numeric(5,2),
    order_unit_fee_organizator numeric(8,4),
    order_share_fee_organizator numeric(5,2),
    order_max_fee numeric(8,4) NOT NULL,
    CONSTRAINT event_payment_profile_pkey PRIMARY KEY (id_event , id_payment_profile , id)
);


ALTER TABLE public.event_payment_profile OWNER TO consys;

--
-- Name: event_pp_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE event_pp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_pp_seq OWNER TO consys;

--
-- Name: event_pp_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('event_pp_seq', 1, true);


--
-- Name: event_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE event_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_seq OWNER TO consys;

--
-- Name: event_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('event_seq', 1, true);


--
-- Name: invoice; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE invoice (
    id bigint NOT NULL,
    invoice_number character varying(10) NOT NULL,
    confirmation_source integer,
    issue_date timestamp without time zone NOT NULL,
    taxable_fulfillment_date timestamp without time zone NOT NULL,
    id_order bigint NOT NULL,
    id_transaction bigint,
    CONSTRAINT invoice_pkey PRIMARY KEY (id )
);


ALTER TABLE public.invoice OWNER TO consys;

--
-- Name: invoice_item_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE invoice_item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invoice_item_seq OWNER TO consys;

--
-- Name: invoice_item_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('invoice_item_seq', 1, true);


--
-- Name: invoice_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE invoice_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invoice_seq OWNER TO consys;

--
-- Name: invoice_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('invoice_seq', 1, true);


--
-- Name: order_approve; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE order_approve (
    id bigint NOT NULL,
    id_order bigint NOT NULL,
    create_date timestamp without time zone NOT NULL,
    id_transaction bigint,
    generate_invoice boolean,
    to_order_state integer NOT NULL,
    CONSTRAINT order_approve_pkey PRIMARY KEY (id )
);


ALTER TABLE public.order_approve OWNER TO consys;

--
-- Name: order_approve_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE order_approve_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_approve_seq OWNER TO consys;

--
-- Name: order_approve_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('order_approve_seq', 1, true);


--
-- Name: order_item; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE order_item (
    id bigint NOT NULL,
    id_order bigint NOT NULL,
    id_product bigint NOT NULL,
    quantity integer NOT NULL,
    name character varying(128) NOT NULL,
    discount integer NOT NULL,
    unit_price numeric(13,4),
    vat_rate numeric(5,2),
    idx_item integer,
    CONSTRAINT order_item_pkey PRIMARY KEY (id )
);


ALTER TABLE public.order_item OWNER TO consys;

--
-- Name: order_item_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE order_item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_item_seq OWNER TO consys;

--
-- Name: order_item_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('order_item_seq', 1, true);


--
-- Name: order_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE order_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_seq OWNER TO consys;

--
-- Name: order_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('order_seq', 1, true);


--
-- Name: orders; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE orders (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    order_state integer NOT NULL,
    order_number character varying(10) NOT NULL,
    id_supplier_event_payment_profile bigint NOT NULL,
    id_customer_payment_profile bigint NOT NULL,
    order_type integer NOT NULL,
    purchase_date timestamp without time zone NOT NULL,
    due_date timestamp without time zone NOT NULL,
    confirmed_date timestamp without time zone,
    price numeric(10,4),
    unit_fee numeric(10,4),
    share_fee numeric(10,4),
    note text,
    id_invoice bigint,
    CONSTRAINT orders_pkey PRIMARY KEY (id )
);


ALTER TABLE public.orders OWNER TO consys;

--
-- Name: payment_profile; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE payment_profile (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    actual_revision bigint,
    CONSTRAINT payment_profile_pkey PRIMARY KEY (id )
);


ALTER TABLE public.payment_profile OWNER TO consys;

--
-- Name: payment_profile_revision; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE payment_profile_revision (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    revision_date timestamp without time zone NOT NULL,
    id_parent bigint NOT NULL,
    name character varying(128),
    street character varying(128),
    city character varying(128),
    zip character varying(20),
    id_country integer,
    reg_no character varying(48),
    vat_no character varying(48),
    account_iban character varying(48),
    accoun_bank_swift character varying(48),
    CONSTRAINT payment_profile_revision_pkey PRIMARY KEY (id )
);


ALTER TABLE public.payment_profile_revision OWNER TO consys;

--
-- Name: product; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE product (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    name character varying(256) NOT NULL,
    category integer NOT NULL,
    price numeric(13,4) NOT NULL,
    vat_rate numeric(5,2) NOT NULL,
    id_event_profile bigint NOT NULL,
    CONSTRAINT product_pkey PRIMARY KEY (id )
);


ALTER TABLE public.product OWNER TO consys;

--
-- Name: product_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE product_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_seq OWNER TO consys;

--
-- Name: product_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('product_seq', 1, true);


--
-- Name: profile_rev_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE profile_rev_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profile_rev_seq OWNER TO consys;

--
-- Name: profile_rev_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('profile_rev_seq', 1, true);


--
-- Name: profile_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE profile_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profile_seq OWNER TO consys;

--
-- Name: profile_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('profile_seq', 1, true);


--
-- Name: system_property; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE system_property (
    id bigint NOT NULL,
    key character varying(128) NOT NULL,
    value text,
    CONSTRAINT system_property_pkey PRIMARY KEY (id )
);


ALTER TABLE public.system_property OWNER TO consys;

--
-- Name: system_property_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE system_property_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.system_property_seq OWNER TO consys;

--
-- Name: system_property_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('system_property_seq', 1, true);


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE transaction (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    transaction_id character varying(50),
    create_date timestamp without time zone NOT NULL,
    transaction_date timestamp without time zone NOT NULL,
    amount numeric(10,4),
    fees numeric(10,4),
    confirmation_channel integer NOT NULL,
    currency integer,
    note text,
    CONSTRAINT transaction_pkey PRIMARY KEY (id )
);


ALTER TABLE public.transaction OWNER TO consys;

--
-- Name: transaction_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE transaction_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_seq OWNER TO consys;

--
-- Name: transaction_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('transaction_seq', 1, false);


--
-- Name: transfer; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE transfer (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    transfer_date timestamp without time zone,
    regular_transfer boolean,
    transfer_state integer NOT NULL,
    note text,
    third_party_fees_sum numeric(13,4),
    total_sum numeric(13,4),
    fees_sum numeric(13,4),
    id_commission_order bigint,
    id_event_payment_profile bigint NOT NULL,
    CONSTRAINT transfer_pkey PRIMARY KEY (id )
);


ALTER TABLE public.transfer OWNER TO consys;

--
-- Name: transfer_order; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE transfer_order (
    id_transfer bigint NOT NULL,
    id_order bigint NOT NULL,
    idx_order integer NOT NULL,
    CONSTRAINT transfer_order_pkey PRIMARY KEY (id_transfer , id_order )
);


ALTER TABLE public.transfer_order OWNER TO consys;

--
-- Name: transfer_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE transfer_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transfer_seq OWNER TO consys;

--
-- Name: transfer_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('transfer_seq', 1, true);


--
-- Name: user_payment_profile; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE user_payment_profile (
    id_user bigint NOT NULL,
    id_payment_profile bigint NOT NULL,
    idx_profile integer NOT NULL,
    CONSTRAINT user_payment_profile_pkey PRIMARY KEY (id_user , id_payment_profile )
);


ALTER TABLE public.user_payment_profile OWNER TO consys;

--
-- Name: user_seq; Type: SEQUENCE; Schema: public; Owner: consys
--

CREATE SEQUENCE user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_seq OWNER TO consys;

--
-- Name: user_seq; Type: SEQUENCE SET; Schema: public; Owner: consys
--

SELECT pg_catalog.setval('user_seq', 1, true);


--
-- Name: users; Type: TABLE; Schema: public; Owner: consys; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    uuid character varying(32) NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (id )
);


ALTER TABLE public.users OWNER TO consys;
