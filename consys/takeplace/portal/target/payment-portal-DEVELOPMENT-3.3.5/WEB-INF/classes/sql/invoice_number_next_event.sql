CREATE OR REPLACE FUNCTION invoice_number_next_event() RETURNS varchar AS
$BODY$
DECLARE

	_sp	RECORD;
	_number int;
        _vnumber varchar;

BEGIN
	-- Najskor nacitame na update event number inkrementujeme a updatneme
	select * into _sp from system_property where key='ACTUAL_CLIENT_NUMBER' FOR UPDATE;
	_number := CAST(_sp.value as int) + 1;       
       
        IF _number < 10 THEN
			_vnumber := '000' || _number;
		ELSIF  _number < 100 THEN
			_vnumber := '00' || _number;
		ELSIF  _number < 1000 THEN
			_vnumber := '0' || _number;
                ELSIF  _number <= 9999 THEN
			_vnumber := CAST(_number as varchar);
		ELSE
			RAISE EXCEPTION 'MAX client number ' ,_number; 
		END IF;


	update system_property set value=CAST(_number AS varchar) where id=_sp.id;


        RETURN _vnumber;
END;
$BODY$
LANGUAGE plpgsql