<stripes:layout-component name="subnavigation">
    <div class="top-subpanel-item">
        <stripes:link href="/bean/order/OrderListItem.action">
            <stripes:param name="uuid" value="${actionBean.uuid}"/>
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Detail
        </stripes:link>
    </div>
    <c:if test="${actionBean.order.state == 'WAITING_TO_APPROVE'}">
        <div class="top-subpanel-item">
            <stripes:link beanclass="consys.payment.stripes.bean.order.OrderListActionBean" event="actionApprove">
                <stripes:param name="uuid" value="${actionBean.uuid}"/>
                <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Approve
            </stripes:link>
        </div>
    </c:if>
    <c:if test="${actionBean.order.state == 'ACTIVE'}">
        <div class="top-subpanel-item">
            <stripes:link beanclass="consys.payment.stripes.bean.order.OrderListActionBean" event="actionConfirm">
                <stripes:param name="uuid" value="${actionBean.uuid}"/>
                <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Confirm
            </stripes:link>
        </div>
    </c:if>           
    <c:if test="${actionBean.order.state == 'CONFIRMED_ORGANIZATOR' or actionBean.order.state == 'CONFIRMED_SYSTEM' }">
        <div class="top-subpanel-item">
            <stripes:link beanclass="consys.payment.stripes.bean.order.OrderListActionBean" event="actionRefund">
                <stripes:param name="uuid" value="${actionBean.uuid}"/>
                <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Refund
            </stripes:link>
        </div>
    </c:if>


    <c:if test="${actionBean.order.state == 'WAITING_TO_APPROVE' || actionBean.order.state == 'ACTIVE'}">
        <div class="top-subpanel-item">
            <stripes:link beanclass="consys.payment.stripes.bean.order.OrderListActionBean" event="actionCancel">
                <stripes:param name="uuid" value="${actionBean.uuid}"/>
                <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Cancel order
            </stripes:link>
        </div>
    </c:if>
</stripes:layout-component>
