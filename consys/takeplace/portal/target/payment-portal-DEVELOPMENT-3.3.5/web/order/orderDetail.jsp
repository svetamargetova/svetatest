<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/order/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <stripes:form action="/bean/order/OrderListItem.action" >
            <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
            <stripes:hidden name="invoice" value="${actionBean.invoice}" />
            <h1>
                <c:choose>
                    <c:when test="${actionBean.invoice}">
                        Invoice detail
                    </c:when>
                    <c:otherwise>
                        Order detail
                    </c:otherwise>
                </c:choose>
            </h1>
            <table class="leftRightForm">
                <c:if test="${actionBean.invoice}">
                    <tr>
                        <th>Invoice number</th>
                        <td>${actionBean.order.invoice.invoiceNumber}</td>
                    </tr>
                    <tr>
                        <th>Issue date</th>
                        <td><fmt:formatDate value="${actionBean.order.invoice.issueDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                    </tr>
                    <tr>
                        <th>Taxable fulfillment date</th>
                        <td><fmt:formatDate value="${actionBean.order.invoice.taxableFulfillmentDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                    </tr>
                    <tr>
                        <td colspan="2"><h2>Order</h2></td>
                    </tr>
                </c:if>
                <tr>
                    <th>Order UUID</th>
                    <td>${actionBean.order.uuid}</td>
                </tr>
                <tr>
                    <th>Order number</th>
                    <td>${actionBean.order.orderNumber}</td>
                </tr>
                <tr>
                    <th>Product category</th>
                    <td>${actionBean.order.orderType}</td>
                </tr>
                <tr>
                    <th>Currency</th>
                    <td>${actionBean.order.supplier.currency}</td>
                </tr>
                <tr>
                    <th>Supplier</th>
                    <td>${actionBean.order.supplier.event.uuid}</td>
                </tr>
                <tr>
                    <th>Customer</th>
                    <td>${actionBean.order.customer.name} (${actionBean.order.customer.city}, ${actionBean.order.customer.street})</td>
                </tr>
                <tr>
                    <th>Purchase date</th>
                    <td><fmt:formatDate value="${actionBean.order.purchaseDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>Due date</th>
                    <td><fmt:formatDate value="${actionBean.order.dueDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>Confirmed date</th>
                    <td><fmt:formatDate value="${actionBean.order.confirmedDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>Price</th>
                    <td><fmt:formatNumber value="${actionBean.order.price}"  pattern="###,###,##0.00" /></td>
                </tr>
                <tr>
                    <th>Unit fee</th>
                    <td><fmt:formatNumber value="${actionBean.order.unitFee}"  pattern="###,###,##0.00" /></td>
                </tr>
                <tr>
                    <th>Share fee</th>
                    <td><fmt:formatNumber value="${actionBean.order.shareFee}"  pattern="###,###,##0.00" /></td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td>${actionBean.order.note}</td>
                </tr>
                <tr>
                    <th>Items</th>
                    <td>
                        <display:table name="${actionBean.order.items}" id="item" sort="external" partialList="false" pagesize="50"
                                       size="${actionBean.listSize}" class="display">
                            <display:column property="id" title="Id" />
                            <display:column property="name" title="Name" />
                            <display:column property="quantity" title="Quantity" />
                            <display:column property="unitPrice" title="Unit price" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                            <display:column property="vatRate" title="VAT rate" decorator="consys.payment.stripes.displaytag.PercentDecorator" />
                            <display:column property="discount" title="Discount" />
                        </display:table>
                    </td>
                </tr>
                <tr>
                    <th>Pro forma / Invoice</th>
                    <td><a href="${ctx}/payment/download/invoice?invoice=${actionBean.order.uuid}">Download</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td><stripes:submit name="back" value="Cancel" /></td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>