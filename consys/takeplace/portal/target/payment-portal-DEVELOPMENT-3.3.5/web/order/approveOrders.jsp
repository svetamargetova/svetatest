<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <c:choose>
        <c:when test="${actionBean.eventUuid!=null}">
            <%@ include file="/web/event/subnavigationEvent.jsp" %>
        </c:when>
        <c:otherwise>
            <%@ include file="/web/order/subnavigationApprove.jsp" %>
        </c:otherwise>
    </c:choose>
    <stripes:layout-component name="contents">
        <stripes:form action="/bean/order/ApproveOrderList.action" >
            <h1>Approve orders</h1>
            <div style="margin-top:10px">
                <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
                <display:table name="${actionBean.items}" id="order" sort="external" partialList="true" pagesize="50"
                               size="${actionBean.fullListSize}" requestURI="/bean/order/ApproveOrderList.action?list" class="display">
                    <display:column property="order.orderNumber" title="Order number" />
                    <display:column property="order.state" title="State" />
                    <display:column property="order.purchaseDate" title="Purchase date" decorator="consys.payment.stripes.displaytag.DateDecorator" />
                    <display:column value="${order.order.state};${order.order.dueDate}" title="Due date" decorator="consys.payment.stripes.displaytag.DueDateDecorator" />
                    <display:column property="order.price" title="Price" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <display:column property="order.supplier.currency" title="Currency" />
                    <display:column title="Action" >
                        <stripes:link href="/bean/order/ApproveOrderListItem.action">
                            <stripes:param name="approveId" value="${order.id}" />
                            <stripes:param name="uuid" value="${order.order.uuid}"/>
                            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Approve
                        </stripes:link>
                    </display:column>
                </display:table>
            </div>
            <c:if test="${actionBean.eventUuid!=null}">
                <div style="margin-top:10px">
                    <stripes:submit name="back" value="Back on events" />
                </div>
            </c:if>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>