<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <c:choose>
        <c:when test="${actionBean.eventUuid!=null}">
            <%@ include file="/web/event/subnavigationEvent.jsp" %>
        </c:when>
        <c:otherwise>
            <%@ include file="/web/order/subnavigationApprove.jsp" %>
        </c:otherwise>
    </c:choose>
    <stripes:layout-component name="contents">
        <stripes:form action="/bean/order/OrderList.action" >
            <h1>Orders</h1>
            <table>
                <tr>
                    <td colspan="4">Purchased</td>
                </tr>
                <tr>
                    <td>From:</td>
                    <td><stripes:text name="pFrom"><fmt:formatDate value="${actionBean.pFrom}" pattern="dd.MM.yyyy"/></stripes:text></td>
                    <td style="padding-left: 10px">To:</td>
                    <td><stripes:text name="pTo"><fmt:formatDate value="${actionBean.pTo}" pattern="dd.MM.yyyy"/></stripes:text></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-top:10px">Confirmed</td>
                </tr>
                <tr>
                    <td>From:</td>
                    <td><stripes:text name="cFrom"><fmt:formatDate value="${actionBean.cFrom}" pattern="dd.MM.yyyy"/></stripes:text></td>
                    <td style="padding-left: 10px">To:</td>
                    <td><stripes:text name="cTo"><fmt:formatDate value="${actionBean.cTo}" pattern="dd.MM.yyyy"/></stripes:text></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-top:10px;">State</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3">
                        <stripes:select name="stateFilter" value="${actionBean.stateFilter}">
                            <stripes:options-enumeration enum="consys.payment.bo.enums.OrderFilterEnum"/>
                        </stripes:select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-top:10px;">UUID or orderNumber</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3"><stripes:text name="number">${actionBean.number}</stripes:text></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top:10px;"><stripes:submit name="list" value="Refresh"/></td>
                </tr>
            </table>
            <div style="margin-top:10px">
                <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
                <display:table name="${actionBean.items}" id="order" sort="external" partialList="true" pagesize="50"
                               size="${actionBean.fullListSize}" requestURI="/bean/order/OrderList.action?list" class="displayO"
                               decorator="consys.payment.stripes.displaytag.OrderTableDecorator">
                    <display:column property="orderNumber" title="Order number" />
                    <display:column property="state" title="State" />
                    <display:column property="purchaseDate" title="Purchase date" decorator="consys.payment.stripes.displaytag.DateDecorator" />
                    <display:column property="dueDate" title="Due date" decorator="consys.payment.stripes.displaytag.DateDecorator" />
                    <display:column property="price" title="Price" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <display:column property="supplier.currency" title="Currency" />
                    <display:column title="Action" >
                        <stripes:link href="/bean/order/OrderListItem.action">
                            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>
                            <stripes:param name="uuid" value="${order.uuid}"/>
                            <stripes:param name="invoice" value="false"/>Detail
                        </stripes:link>
                    </display:column>
                </display:table>
            </div>
            <c:if test="${actionBean.eventUuid!=null}">
                <div style="margin-top:10px">
                    <stripes:submit name="back" value="Back on events" />
                </div>
            </c:if>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>