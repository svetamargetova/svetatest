<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/order/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <stripes:form action="/bean/order/OrderList.action" >
            <stripes:hidden name="uuid" value="${actionBean.uuid}"/>
            <h1>Confirm order</h1>
            <table class="leftRightForm">
                <tr>
                    <th>Order number</th>
                    <td>${actionBean.order.orderNumber}</td>
                </tr>
                <tr>
                    <th>Generate invoice</th>
                    <td><stripes:checkbox name="generateInvoice"/></td>
                </tr>
                <tr>
                    <th>Notify user</th>
                    <td><stripes:checkbox name="notifyUser"/></td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td><stripes:textarea name="note" rows="8"></stripes:textarea></td>
                </tr>
                <tr>
                    <th></th>
                    <td><stripes:submit name="confirmConfirm" value="Confirm"/><stripes:submit name="list" value="Cancel"/></td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>