<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/web/layout/taglibs.jsp" %>
<%@page import="java.util.Enumeration" %>
<%@page import="javax.servlet.http.HttpSession" %>


<stripes:layout-definition>
    <html>
        <head>
            <title>Takeplace - Payment</title>
            <link rel="stylesheet" type="text/css" href="${ctx}/web/layout/styles.css">
            <script type="text/javascript">
                $(document).ready(function(){
                <stripes:layout-component name="documentReady"/>
                    });
            </script>
        </head>
        <body>
        <div class="centerPart">
            <div style="width: 100%; text-align: right;">
                <%
                    String userName = (request.getUserPrincipal() == null) ? null : request.getUserPrincipal().getName();
                %>
                <%= "<span style='font-size:8pt'> Logged User: " + userName + "</span>"%>

            </div>
            <div id="header">
                <div class="top-panel-item"> <stripes:link name="transfer" beanclass="consys.payment.stripes.bean.transfer.TransferListActionBean">Transfers</stripes:link></div>
                <div class="top-panel-item"> <stripes:link name="order" beanclass="consys.payment.stripes.bean.order.OrderListActionBean">Orders</stripes:link></div>
                <div class="top-panel-item"> <stripes:link name="transactions" beanclass="consys.payment.stripes.bean.transaction.TransactionListActionBean">Transactions</stripes:link></div>
                <div class="top-panel-item"> <stripes:link name="event" beanclass="consys.payment.stripes.bean.event.EventListActionBean">Events</stripes:link></div>
                <div class="top-panel-item"> <stripes:link name="settings" beanclass="consys.payment.stripes.bean.settings.SystemSettingsActionBean">System settings</stripes:link></div>
            </div>
            <div id="subheader">
                <stripes:layout-component name="subnavigation"/>
            </div>
            <div>
                <div id="content">
                    <div class="sectionTitle">${title}</div>
                    <stripes:messages/>
                    <stripes:layout-component name="contents"/>
                </div>
            </div>
        </div>
    </body>
</html>
</stripes:layout-definition>
