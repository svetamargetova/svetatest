<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <c:choose>
        <c:when test="${actionBean.profileUuid==null}">
            <%@ include file="/web/event/subnavigationEvent.jsp" %>
        </c:when>
        <c:otherwise>
            <%@ include file="/web/event/subnavigation.jsp" %>
        </c:otherwise>
    </c:choose>
    <stripes:layout-component name="contents">
        <h1>
            <c:choose>
                <c:when test="${actionBean.profileUuid==null}">
                    Event products
                </c:when>
                <c:otherwise>
                    Event payment profile products
                </c:otherwise>
            </c:choose>
        </h1>
        <stripes:form action="/bean/event/EventProductsList.action" >
            <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
            <stripes:hidden name="profileUuid" value="${actionBean.profileUuid}" />
            <display:table name="${actionBean.items}" id="product" sort="external" partialList="true" pagesize="50"
                           size="${actionBean.fullListSize}" requestURI="/bean/event/EventProductsList.action?list" class="display">
                <display:column property="uuid" title="UUID" />
                <display:column property="name" title="Name" />
                <display:column property="price" title="Price" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                <display:column property="vatRate" title="VAT rate" decorator="consys.payment.stripes.displaytag.PercentDecorator" />
            </display:table>
            <div style="margin-top:10px">
                <c:choose>
                    <c:when test="${actionBean.profileUuid==null}">
                        <stripes:submit name="back" value="Back on events" />
                    </c:when>
                    <c:otherwise>
                        <stripes:submit name="back" value="Back on event payment profiles" />
                    </c:otherwise>
                </c:choose>
            </div>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>