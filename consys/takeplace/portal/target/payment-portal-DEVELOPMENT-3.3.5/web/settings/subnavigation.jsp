<stripes:layout-component name="subnavigation">
    <div class="top-subpanel-item"> <stripes:link name="czk" beanclass="consys.payment.stripes.bean.settings.ProfileSettingsActionBean"><stripes:param name="type" value="CZK"/>CZK</stripes:link></div>
    <div class="top-subpanel-item"> <stripes:link name="eur" beanclass="consys.payment.stripes.bean.settings.ProfileSettingsActionBean"><stripes:param name="type" value="EUR"/>EUR</stripes:link></div>
    <div class="top-subpanel-item"> <stripes:link name="usd" beanclass="consys.payment.stripes.bean.settings.ProfileSettingsActionBean"><stripes:param name="type" value="USD"/>USD</stripes:link></div>
    <div class="top-subpanel-item"> <stripes:link name="products" beanclass="consys.payment.stripes.bean.settings.SystemProductListActionBean">Products</stripes:link></div>
</stripes:layout-component>