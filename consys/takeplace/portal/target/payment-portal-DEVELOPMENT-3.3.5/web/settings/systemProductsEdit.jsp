<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/settings/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <div>
            <stripes:form action="/bean/settings/SystemProductListItem.action" >
                <stripes:hidden name="currency" value="${actionBean.currency}" />
                <stripes:hidden name="uuid" value="${actionBean.product.uuid}" />

                <table class="leftRightForm">
                    <tr>
                        <th>Product UUID:</th>
                        <td>${actionBean.product.uuid}</td>
                    </tr>
                    <tr>
                        <th>Name:</th>
                        <td><stripes:text name="product.name">${actionBean.product.name}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>Category:</th>
                        <td>${actionBean.product.category}</td>
                    </tr>
                    <tr>
                        <th>Price:</th>
                        <td><stripes:text name="product.price">${actionBean.product.price}</stripes:text> ${actionBean.currency}</td>
                    </tr>
                    <tr>
                        <th>Vat rate:</th>
                        <td><stripes:text name="product.vatRate">${actionBean.product.vatRate}</stripes:text> %</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><stripes:submit name="update" value="Update"/><stripes:submit name="back" value="Cancel"/></td>
                    </tr>
                </table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>