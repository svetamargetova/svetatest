<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/settings/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <div>
            <stripes:form action="/bean/settings/SystemProductList.action" >
                <stripes:select name="currency" value="${actionBean.currency}">
                    <stripes:options-enumeration enum="consys.common.utils.enums.Currency" />
                </stripes:select>
                <stripes:submit name="list" value="Refresh"/>

                <display:table name="${actionBean.items}" id="product" sort="external" partialList="true" pagesize="50"
                               size="${actionBean.fullListSize}" requestURI="/bean/settings/SystemProductList.action?list" class="display">
                    <display:column property="name" title="Name" />
                    <display:column property="category" title="Product category" />
                    <display:column property="price" title="Price" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <display:column property="vatRate" title="VAT rate" decorator="consys.payment.stripes.displaytag.PercentDecorator" />
                    <display:column title="Action" >
                        <stripes:link href="/bean/settings/SystemProductListItem.action">
                            <stripes:param name="uuid" value="${product.uuid}"/>
                            <stripes:param name="currency" value="${actionBean.currency}"/>Edit
                        </stripes:link>
                    </display:column>
                </display:table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>