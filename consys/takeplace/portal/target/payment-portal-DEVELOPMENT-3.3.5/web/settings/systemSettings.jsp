<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <stripes:layout-component name="subnavigation">
        <%@ include file="/web/settings/subnavigation.jsp" %>
    </stripes:layout-component>
    <stripes:layout-component name="contents">
        <h1>System settings</h1>
    </stripes:layout-component>
</stripes:layout-render>