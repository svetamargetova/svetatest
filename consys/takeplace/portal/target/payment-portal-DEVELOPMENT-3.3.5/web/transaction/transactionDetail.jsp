<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/transaction/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <h1>Transaction detail</h1>
        <stripes:form action="/bean/transaction/TransactionListItem.action" >
            <stripes:hidden name="uuid" value="${actionBean.uuid}" />
            <table class="leftRightForm">
                <tr>
                    <th>UUID:</th>
                    <td>${actionBean.transaction.uuid}</td>
                </tr>
                <tr>
                    <th>Transaction ID:</th>
                    <td>${actionBean.transaction.transactionId}</td>
                </tr>
                <tr>
                    <th>Created date:</th>
                    <td><fmt:formatDate value="${actionBean.transaction.createdDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>Transaction date:</th>
                    <td><fmt:formatDate value="${actionBean.transaction.transactionDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>Amount:</th>
                    <td><fmt:formatNumber value="${actionBean.transaction.amount}"  pattern="### ### ##0.00" /></td>
                </tr>
                <tr>
                    <th>Fees:</th>
                    <td><fmt:formatNumber value="${actionBean.transaction.fees}"  pattern="### ### ##0.00" /></td>
                </tr>
                <tr>
                    <th>Confirmation channel:</th>
                    <td>${actionBean.transaction.confirmationChannel}</td>
                </tr>
                <tr>
                    <th>Note:</th>
                    <td><stripes:textarea name="note" rows="8">${actionBean.transaction.note}</stripes:textarea></td>
                </tr>
                <tr>
                    <th></th>
                    <td><stripes:submit name="update" value="Update"/><stripes:submit name="back" value="Cancel"/></td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>