<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <c:if test="${actionBean.eventUuid!=null && actionBean.profileUuid==null}">
        <%@ include file="/web/event/subnavigationEvent.jsp" %>
    </c:if>
    <c:if test="${actionBean.eventUuid!=null && actionBean.profileUuid!=null}">
        <%@ include file="/web/event/subnavigation.jsp" %>
    </c:if>
    <stripes:layout-component name="contents">
        <h1>Transfers</h1>
        <stripes:form action="/bean/transfer/TransferList.action">
            <stripes:errors/>
            <c:if test="${actionBean.eventUuid==null}">
                <div style="margin: 10px">
                    <stripes:label for="status.filter"/>
                    <stripes:select title="status.filter" name="actualEnum"  >
                        <stripes:options-enumeration enum="consys.payment.bo.enums.TransferState"/>
                    </stripes:select>
                    <stripes:submit name="list" value="Refresh"/>
                </div>
            </c:if>
            <div style="margin-top:10px">
                <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
                <stripes:hidden name="profileUuid" value="${actionBean.profileUuid}" />
                <display:table name="${actionBean.items}" id="transfer" sort="external" partialList="true" pagesize="50"
                               size="${actionBean.fullListSize}" requestURI="/bean/transfer/TransferList.action?list" class="display">
                    <display:column property="id" title="ID" />
                    <display:column property="transferDate" title="Transfer date" decorator="consys.payment.stripes.displaytag.DateTimeDecorator" />
                    <display:column property="state" title="State" />
                    <display:column property="regular" title="Regular transfer" />
                    <display:column property="eventPaymentProfile.currency" title="Currency" />
                    <display:column property="totalSum" title="Total sum" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <display:column property="feesSum" title="Fees sum" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <display:column property="thirdPartyFeesSum" title="Third party fees sum" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <c:if test="${actionBean.eventUuid==null}">
                        <display:column title="Action">
                            <stripes:link href="/bean/transfer/TransferListItemDetail.action">
                                <stripes:param name="uuid" value="${transfer.uuid}"/>Edit
                            </stripes:link>
                        </display:column>
                    </c:if>
                </display:table>
            </div>
            <c:if test="${actionBean.eventUuid!=null}">
                <div style="margin-top:10px">
                    <c:if test="${actionBean.eventUuid!=null && actionBean.profileUuid==null}">
                        <stripes:submit name="back" value="Back on events" />
                    </c:if>
                    <c:if test="${actionBean.eventUuid!=null && actionBean.profileUuid!=null}">
                        <stripes:submit name="back" value="Back on profiles" />
                    </c:if>
                </div>
            </c:if>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>