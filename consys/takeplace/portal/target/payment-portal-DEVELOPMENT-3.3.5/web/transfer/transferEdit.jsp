<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <stripes:layout-component name="contents">
        <stripes:errors/>
        <h1 style="margin-bottom:15px">Transfer detail</h1>
        <stripes:form action="/bean/transfer/TransferListItemDetail.action">
            <stripes:hidden name="uuid"/>
            <table class="leftRightForm">
                <tr>
                    <th>Transfer UUID:</th>
                    <td>${actionBean.transfer.uuid}</td>
                </tr>
                <tr>
                    <th>Transfer date:</th>
                    <td><fmt:formatDate value="${actionBean.transfer.transferDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>State:</th>
                    <td>${actionBean.transfer.state}</td>
                </tr>
                <tr>
                    <th>Regular transfer:</th>
                    <td>${actionBean.transfer.regular}</td>
                </tr>
                <tr>
                    <th>Currency:</th>
                    <td>${actionBean.transfer.eventPaymentProfile.currency}</td>
                </tr>
                <tr>
                    <th>Total sum:</th>
                    <td><fmt:formatNumber value="${actionBean.transfer.totalSum}" pattern="### ### ##0.00"/></td>
                </tr>
                <tr>
                    <th>Fees sum:</th>
                    <td><fmt:formatNumber value="${actionBean.transfer.feesSum}" pattern="### ### ##0.00"/></td>
                </tr>
                <tr>
                    <th>Third party fees sum:</th>
                    <td><fmt:formatNumber value="${actionBean.transfer.thirdPartyFeesSum}" pattern="### ### ##0.00"/></td>
                </tr>
                <tr>
                    <th>Note</th>
                    <td><stripes:textarea name="transfer.note" rows="5"/></td>
                </tr>
                <c:if test="${actionBean.transfer.state == 'CLOSED_AND_PREPARED' || actionBean.transfer.state == 'TRANSFERED'}">
                    <tr>
                        <th>Transfer Invoice:</th>
                        <td><a href="${ctx}/payment/download/invoice?invoice=${actionBean.transfer.uuid}">Download</a></td>
                    </tr>
                </c:if>
                <tr>
                    <th></th>
                    <td>
                        <stripes:submit name="back" value="Back"/>
                        <c:if test="${actionBean.transfer.state == 'ACTIVE'}">
                            <stripes:submit name="makePrepared" value="Make prepared"/>
                            <stripes:submit name="makePreparedPrevious" value="Make prepared to end of previous month"/>
                        </c:if>
                        <c:if test="${actionBean.transfer.state == 'CLOSED_AND_PREPARED'}">
                            <stripes:submit name="makeTransfered" value="Make transfered"/>
                            <stripes:submit name="makeTransferedPrevious" value="Make transfered to end of previous month"/>
                            <stripes:submit name="recalculate" value="Recalculate transfer"/>
                        </c:if>
                    </td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>