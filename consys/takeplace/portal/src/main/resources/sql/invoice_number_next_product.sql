CREATE OR REPLACE FUNCTION invoice_number_next_product(iuuid varchar) RETURNS varchar AS
$BODY$
DECLARE

	_epp	RECORD;	
	_number int;
	_i_number varchar;
        _enumber varchar;

BEGIN		
	select * into _epp from event_payment_profile where id=(select id_supplier_event_payment_profile from orders where uuid=iuuid) FOR UPDATE;
	_number := _epp.order_invoice_counter + 1;
        select invoice_client_number into _enumber from event where event.id=_epp.id_event;
	IF _number < 10000 THEN

		IF _number < 10 THEN
			_i_number := _epp.order_invoice_series_counter || _enumber ||'000'|| _number;
		ELSIF  _number < 100 THEN
			_i_number := _epp.order_invoice_series_counter || _enumber ||'00'|| _number;
		ELSIF  _number < 1000 THEN
			_i_number := _epp.order_invoice_series_counter || _enumber ||'0'|| _number;
		ELSE
			_i_number := _epp.order_invoice_series_counter || _enumber || _number;
		END IF;
		update event_payment_profile set order_invoice_counter=_number where id=_epp.id;
	ELSE
		-- Musime povysit radu
                _number = 1;
                _i_number := _epp.invoice_series_counter + 1 || _enumber ||'000'|| _number;
                update event_payment_profile set order_invoice_counter=_number, invoice_series_counter=(_epp.invoice_series_counter + 1)   where id=_epp.id;
	END IF;
        
        
        RETURN to_char(current_date,'y') || _i_number;
END;
$BODY$
LANGUAGE plpgsql