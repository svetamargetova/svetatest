-- create language 'plpgsql';
CREATE OR REPLACE FUNCTION invoice_number_next_commission(iuuid varchar) RETURNS varchar AS
$BODY$
DECLARE

	_event	RECORD;
        _event_id BIGINT;
	_number int;
	_i_number varchar;
        _s_number varchar;

BEGIN
        -- Najskor nacitame na update event number inkrementujeme a updatneme
	select e.id into _event_id from orders as ord left join payment_profile_revision as r on r.id=ord.id_customer_payment_profile left join payment_profile as pp on pp.id=id_parent left join event_payment_profile as epp on epp.id_payment_profile=pp.id left join event as e on e.id=epp.id_event where ord.uuid=iuuid;
	select * into _event from event where id=_event_id FOR UPDATE;
        select (count(*) + 1) into _number from invoice where invoice_number like (to_char(current_date,'y') || '6%');
        _s_number = '6';
	IF _number < 10000 THEN
		IF _number < 10 THEN
			_i_number := _event.invoice_client_number ||'000'|| _number;
		ELSIF  _number < 100 THEN
			_i_number := _event.invoice_client_number ||'00'|| _number;
		ELSIF  _number < 1000 THEN
			_i_number := _event.invoice_client_number ||'0'|| _number;
		ELSE
			_i_number := _event.invoice_client_number || _number;
		END IF;
	ELSE
		-- TOTO BY NEMELO NASTAT ale pro istotu
                _s_number = '7';
	END IF;

        RETURN to_char(current_date,'y') || _s_number ||_i_number;
END;
$BODY$
LANGUAGE plpgsql