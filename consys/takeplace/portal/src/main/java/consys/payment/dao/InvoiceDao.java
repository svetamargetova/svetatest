/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao;

import consys.payment.bo.Invoice;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public interface InvoiceDao extends PersistedObjectDao<Invoice> {
   
    public String nextInvoiceClientNumber();

    public String nextInvoiceProductNumber(String invoiceUuid);

    /**
     * Pre dany event vygeneruje cislo faktury pre proviziu
     * 
     * @param event
     * @return
     */
    public String nextInvoiceCommissionNumber(String invoiceUuid);
    
    public List<Invoice> listEventInvoices(String eventUuid, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage);

    public int listEventInvoicesCount(String eventUuid, Date confirmedFrom, Date confirmedTo, String number);                
}
