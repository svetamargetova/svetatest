package consys.payment.bo.enums;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum TransferState {

    /** Predpripravene ale este neoznacene k transferu */
    ACTIVE,
    /** Pripravene k transferu */
    CLOSED_AND_PREPARED,
    /** Transferovane */
    TRANSFERED,
    /** Chyba pri transfere */
    ERR;

    public Integer toId() {
        switch (this) {
            case ACTIVE:
                return 1;
            case CLOSED_AND_PREPARED:
                return 2;
            case TRANSFERED:
                return 3;
            case ERR:
                return 4;
            default:
                throw new IllegalArgumentException("Type " + this.name() + " has not ID mapping");
        }
    }

    public static TransferState fromId(Integer i) {
        if (i == null) {
            return null;
        }
        switch (i) {
            case 1:
                return ACTIVE;
            case 2:
                return CLOSED_AND_PREPARED;
            case 3:
                return TRANSFERED;
            case 4:
                return ERR;
            default:
                return null;
        }
    }
}
