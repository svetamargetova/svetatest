/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo.enums;

/**
 *
 * @author palo
 */
public enum ConfirmationChannel {

    BANK_TRANSFER,
    ONLINE_PAY_PAL,
    SYSTEM;

    public Integer toId() {
        switch (this) {
            case BANK_TRANSFER:
                return 1;
            case ONLINE_PAY_PAL:
                return 2;
            case SYSTEM:
                return 3;
            default:
                throw new IllegalArgumentException("Type " + this.name() + " has not ID mapping");
        }
    }

    public static ConfirmationChannel fromId(Integer i) {
        switch (i) {
            case 1:
                return BANK_TRANSFER;
            case 2:
                return ONLINE_PAY_PAL;
            case 3:
                return SYSTEM;
            default:
                return null;
        }
    }
}
