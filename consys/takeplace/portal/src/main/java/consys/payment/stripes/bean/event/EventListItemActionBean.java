package consys.payment.stripes.bean.event;

import consys.payment.bo.Event;
import consys.payment.service.EventService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EventListItemActionBean extends PaymentActionBean {

    @SpringBean
    private EventService eventService;
    // data
    private Event event;
    private String eventUuid;

    @DontValidate
    @DefaultHandler
    public Resolution preView() throws NoRecordException, RequiredPropertyException {
        event = eventService.loadEvent(eventUuid);
        return new ForwardResolution("/web/event/eventDetail.jsp");
    }

    public Resolution back() {
        return new ForwardResolution(EventListActionBean.class);
    }

    public Event getEvent() {
        return event;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }
}
