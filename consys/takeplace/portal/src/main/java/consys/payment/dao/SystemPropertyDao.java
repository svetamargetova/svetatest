/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.dao;

import consys.payment.bo.SystemProperty;
import consys.payment.service.exception.NoRecordException;

/**
 *
 * @author palo
 */
public interface SystemPropertyDao extends PersistedObjectDao<SystemProperty>{

    public SystemProperty loadByKey(String key) throws NoRecordException;

}
