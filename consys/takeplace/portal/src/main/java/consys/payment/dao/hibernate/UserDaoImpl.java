package consys.payment.dao.hibernate;

import consys.payment.bo.User;
import consys.payment.dao.UserDao;
import consys.payment.service.exception.NoRecordException;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserDaoImpl extends BusinessObjectDaoImpl<User> implements UserDao{

    @Override
    public User loadUserByUuid(String uuid) throws NoRecordException {
        User e = (User) session().
                createQuery("from User e where e.uuid = :UUID").
                setString("UUID", uuid).
                uniqueResult();
        if(e == null){
            throw new NoRecordException("No user with uuid "+uuid+" found");
        }
        return e;
        
    }

    @Override
    public User loadUserWithProfilesByUuid(String uuid) throws NoRecordException {
        User e = (User) session().
                createQuery("select e from User e left join fetch e.profiles where e.uuid = :UUID").
                setString("UUID", uuid).
                uniqueResult();
        if(e == null){
            throw new NoRecordException("No user with uuid " + uuid + " found");
        }
        return e;
    }

}
