package consys.payment.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class Service {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    

}
