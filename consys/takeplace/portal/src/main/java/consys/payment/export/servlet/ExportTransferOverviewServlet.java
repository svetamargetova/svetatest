package consys.payment.export.servlet;

import consys.payment.bo.Transfer;
import consys.payment.export.xsl.XlsTransferOverviewGenerator;
import consys.payment.service.TransferService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author pepa
 */
public class ExportTransferOverviewServlet extends HttpServlet implements DownloadServletFields {

    private static final long serialVersionUID = -4146353622776309432L;
    // logger
    private static final Logger logger = LoggerFactory.getLogger(ExportTransferOverviewServlet.class);
    // konstanty
    private static final String TRANSFER_PARAM = "invoice";
    private static final String EVENT_PARAM = "eid";
    // data
    private TransferService transferService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String transferUuid = request.getParameter(TRANSFER_PARAM);
            if (StringUtils.isBlank(transferUuid)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Transfer UUID is missing");
                }
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            String eventUuid = request.getParameter(EVENT_PARAM);
            if (StringUtils.isBlank(eventUuid)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Event UUID is missing");
                }
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            // nacteni transferu
            Transfer transfer = getTransferService().loadTransferWithOrders(transferUuid);
            // nastaveni hlavicek odpovedi
            headerSettings(response, transfer);

            XlsTransferOverviewGenerator generator = new XlsTransferOverviewGenerator();
            generator.createXls(response.getOutputStream(), transfer);
        } catch (RequiredPropertyException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Invoice UUID is missing");
            }
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch (NoRecordException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Invoice UUID is missing");
            }
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch (Exception ex) {
            logger.error("What the hell happend!?", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /** nacteni potrebne servicy */
    public TransferService getTransferService() {
        if (transferService == null) {
            transferService = (TransferService) WebApplicationContextUtils.getWebApplicationContext(
                    getServletContext()).getBean("transferService", TransferService.class);
        }
        return transferService;
    }

    /** nastaveni hlavicek odpovedi */
    private void headerSettings(HttpServletResponse response, Transfer transfer) {
        StringBuilder contentDisposition = new StringBuilder();
        contentDisposition.append(INLINE_FILENAME);

        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
        contentDisposition.append("export_transfer_overview_");
        if (transfer.getTransferDate() != null) {
            contentDisposition.append(format.format(transfer.getTransferDate()));
        }
        contentDisposition.append(transfer.getUuid()).append(".xls");
        contentDisposition.append(END_NAME);

        response.setContentType(CONTENT_TYPE_XLS);
        response.setHeader(CONTENT_DISPOSITION, contentDisposition.toString());
        response.setHeader(CACHE_CONTROL, MAXAGE_1S);
        response.setHeader(PRAGMA, PUBLIC);
    }
}
