package consys.payment.dao.hibernate;

import consys.payment.bo.Product;
import consys.payment.dao.ProductDao;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProductDaoImpl extends BusinessObjectDaoImpl<Product> implements ProductDao {

    @Override
    public Product loadProductByUuid(String uuid) throws NoRecordException {
        Product p = (Product) session().
                createQuery("select p from Product p where p.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (p == null) {
            throw new NoRecordException("No product with uuid " + uuid);
        }
        return p;
    }    


    @Override
    public List<Product> loadProductsByUuid(List<String> uuids) throws NoRecordException {
        List<Product> p = session().
                createQuery("select p from Product p where p.uuid in (:UUIDS)").
                setParameterList("UUIDS", uuids).
                list();
        if (p == null || p.size() != uuids.size()) {
            throw new NoRecordException("Products missing in result IN " + uuids.size() + " Results: " + p.size());
        }
        return p;
    }

    @Override
    public List<Product> loadEventProducts(String eventUuid) throws NoRecordException {
        List<Product> p = session().
                createQuery("select p from Product p where p.profile.event.uuid=:UUID order by p.name").
                setString("UUID", eventUuid).
                list();
        if (p == null) {
            throw new NoRecordException("No products for event " + eventUuid);
        }
        return p;
    }

    @Override
    public List<Product> listEventProductsByUuid(String eventUuid, List<String> productUuids) {
        List<Product> p = session().
                createQuery("select p from Product p LEFT JOIN FETCH p.profile as profile where profile.event.uuid=:E_UUID and p.uuid in (:P_UUID)").
                setString("E_UUID", eventUuid).
                setParameterList("P_UUID", productUuids).
                list();
        return p;
    }

    @Override
    public int loadProductUsageCount(String productUuid) {
        List<Long> count = session().createQuery("select count(*) from OrderItem item where item.product.uuid=:P_UUID").
                setString("P_UUID", productUuid).
                list();
        return count.get(0).intValue();
    }

    @Override
    public List<Product> listEventPaymentProfileProducts(String eventPaymentProfileUuid) {
        List<Product> p = session().
                createQuery("select p from Product p where p.profile.uuid=:UUID").
                setString("UUID", eventPaymentProfileUuid).                
                list();
        return p;
    }
}
