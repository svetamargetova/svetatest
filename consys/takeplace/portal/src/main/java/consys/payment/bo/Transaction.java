/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo;

import consys.payment.bo.enums.ConfirmationChannel;
import consys.common.utils.enums.Currency;
import consys.payment.utils.PaymentUtils;
import java.math.BigDecimal;
import java.util.Date;
import org.apache.commons.lang.StringUtils;

/**
 * Entita ktora reprezentuje transakciu z nejakeho platobneho zdroja. Transakcia 
 * moze byt zparovana z niekolkymi fakturami.
 * @author palo
 */
public class Transaction implements BusinessObject{
    private static final long serialVersionUID = 2312660709625890740L;
    
    private Long id;
    /** UUID transferu */
    private String uuid;
    /** Identifikator transakcie v zdroji */
    private String transactionId;
    /** Datum vytvorenia transakcie */
    private Date createdDate;
    /** Datum transakcie v zdroji */
    private Date transactionDate;
    /** Hodnota ktora tato transakcia obsahuje */
    private BigDecimal amount;
    /** Poplatok tretej strany */
    private BigDecimal fees = BigDecimal.ZERO;
    /** Kanal z ktoreho prisla tato transakcia */
    private ConfirmationChannel confirmationChannel;
    /** Mena transakcie  */
    private Currency currency;
    /** Poznamka */
    private String note;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * UUID transferu
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID transferu
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Identifikator transakcie v zdroji
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Identifikator transakcie v zdroji
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Datum vytvorenia transakcie
     * @return the craetedDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Datum vytvorenia transakcie
     * @param craetedDate the craetedDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Datum transakcie v zdroji
     * @return the transactionDate
     */
    public Date getTransactionDate() {
        return transactionDate;
    }

    /**
     * Datum transakcie v zdroji
     * @param transactionDate the transactionDate to set
     */
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * Hodnota ktora tato transakcia obsahuje
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Hodnota ktora tato transakcia obsahuje
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }   

    /**
     * Poznamka
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * Poznamka
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
          
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Event e = (Event) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    /**
     * Poplatok tretej strany
     * @return the fees
     */
    public BigDecimal getFees() {
        return fees;
    }

    /**
     * Poplatok tretej strany
     * @param fees the fees to set
     */
    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    /**
     * Kanal z ktoreho prisla tato transakcia
     * @return the confirmationChannel
     */
    public ConfirmationChannel getConfirmationChannel() {
        return confirmationChannel;
    }

    /**
     * Kanal z ktoreho prisla tato transakcia
     * @param confirmationChannel the confirmationChannel to set
     */
    public void setConfirmationChannel(ConfirmationChannel confirmationChannel) {
        this.confirmationChannel = confirmationChannel;
    }

    @Override
    public String toString() {
        return String.format("Transaction[uuid=%s transactionId=%s transactionDate=%s amount=%s currency=%s]", uuid,transactionId,transactionDate,amount.toPlainString(),currency);
    }

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void appendToNote(String note) {
        if (StringUtils.isBlank(note)) {
            return;
        }        
        setNote(PaymentUtils.craeteNote(this.note, note));
    }
    
    
    
}
