/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.bo;

import java.util.Set;

/**
 *
 * @author palo
 */
public class PaymentProfile implements BusinessObject{
    private static final long serialVersionUID = 4383411486145919841L;

    private Long id;
    /** UUID profilu */
    private String uuid;
    /** Set vsetkych revizii */
    private Set<PaymentProfileRevision> revisions;
    /** Aktualna revizia profilu */
    private PaymentProfileRevision actualRevision;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.getUuid() != null ? this.getUuid().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
         if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        final PaymentProfile other = (PaymentProfile) obj;
        
        return uuid.equalsIgnoreCase(other.uuid);
    }

    @Override
    public String toString() {
        return String.format("PaymentProfile[uuid = %s revision = %s]", uuid,actualRevision);
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * UUID profilu
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID profilu
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Set vsetkych revizii
     * @return the revisions
     */
    public Set<PaymentProfileRevision> getRevisions() {
        return revisions;
    }

    /**
     * Set vsetkych revizii
     * @param revisions the revisions to set
     */
    public void setRevisions(Set<PaymentProfileRevision> revisions) {
        this.revisions = revisions;
    }

    /**
     * Aktualna revizia profilu
     * @return the actualRevision
     */
    public PaymentProfileRevision getActualRevision() {
        return actualRevision;
    }

    /**
     * Aktualna revizia profilu
     * @param actualRevision the actualRevision to set
     */
    public void setActualRevision(PaymentProfileRevision actualRevision) {
        this.actualRevision = actualRevision;
    }


}
