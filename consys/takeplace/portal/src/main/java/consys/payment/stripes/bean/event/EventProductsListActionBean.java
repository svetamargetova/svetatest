package consys.payment.stripes.bean.event;

import consys.payment.bo.Product;
import consys.payment.service.ProductService;
import consys.payment.stripes.bean.ListActionBean;
import java.util.List;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EventProductsListActionBean extends ListActionBean<Product> {

    @SpringBean
    private ProductService productService;
    // data
    private String eventUuid;
    private String profileUuid;

    public EventProductsListActionBean() {
        super("/web/event/eventProducts.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        List<Product> products;
        if (profileUuid != null) {
            products = productService.listAllEventPaymentProfileProducts(profileUuid);
        } else {
            products = productService.listAllEventProducts(eventUuid);
        }
        getItems().setList(products);
        getItems().setTotal(products.size());
    }

    public Resolution back() {
        RedirectResolution resolution;
        if (profileUuid != null) {
            resolution = new RedirectResolution(EventPaymentProfileListActionBean.class);
            resolution.addParameter("eventUuid", eventUuid);
        } else {
            resolution = new RedirectResolution(EventListActionBean.class);
        }
        return resolution;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getProfileUuid() {
        return profileUuid;
    }

    public void setProfileUuid(String profileUuid) {
        this.profileUuid = profileUuid;
    }
}
