package consys.payment.stripes.bean.settings;

import consys.payment.SystemProperties;
import consys.payment.bo.Product;
import consys.common.utils.enums.Currency;
import consys.payment.service.ProductService;
import consys.payment.stripes.bean.ListActionBean;
import java.util.List;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class SystemProductListActionBean extends ListActionBean<Product> implements SystemProperties {

    @SpringBean
    private ProductService productService;
    //data
    private Currency currency;

    public SystemProductListActionBean() {
        super("/web/settings/systemProducts.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        String uuid = "";
        if (currency == null) {
            currency = Currency.CZK;
        }

        switch (currency) {
            case CZK:
                uuid = CZK_ACMC_PROFILE_UUID;
                break;
            case EUR:
                uuid = EUR_ACMC_PROFILE_UUID;
                break;
            case USD:
                uuid = USD_ACMC_PROFILE_UUID;
                break;
        }

        List<Product> products = productService.listAllEventPaymentProfileProducts(uuid);
        getItems().setList(products);
        getItems().setTotal(products.size());
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
