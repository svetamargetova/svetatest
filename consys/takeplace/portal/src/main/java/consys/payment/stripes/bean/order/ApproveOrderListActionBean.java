package consys.payment.stripes.bean.order;

import consys.payment.bo.ApproveOrder;
import consys.payment.service.OrderService;
import consys.payment.stripes.bean.ListActionBean;
import consys.payment.stripes.bean.event.EventListActionBean;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class ApproveOrderListActionBean extends ListActionBean<ApproveOrder> {

    @SpringBean
    private OrderService orderService;
    // data
    private String eventUuid;

    public ApproveOrderListActionBean() {
        super("/web/order/approveOrders.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        if (eventUuid != null) {
            getItems().setList(orderService.listApproveOrdersForEvent(eventUuid, firstItemIndex, itemsPerPage));
            getItems().setTotal(orderService.loadApproveOrdersForEventCount(eventUuid));
        } else {
            getItems().setList(orderService.listApproveOrders(firstItemIndex, itemsPerPage));
            getItems().setTotal(orderService.loadApproveOrdersCount());
        }
    }

    /** pouziva se jen z eventoveho approve */
    public Resolution back() {
        return new RedirectResolution(EventListActionBean.class);
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }
}
