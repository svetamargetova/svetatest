package consys.payment.dao;

import consys.payment.bo.ApproveOrder;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ApproveOrderDao extends PersistedObjectDao<ApproveOrder> {

    public ApproveOrder loadApproveOrderByOrderUuid(String uuid)  throws NoRecordException;

    public ApproveOrder loadWithOrderPaymentDetails(Long id) throws NoRecordException;

    public List<ApproveOrder> listApproveOrders(int startItem, int itemsPerPage);

    public int loadApproveOrdersCount();

    public List<ApproveOrder> listApproveOrdersForEvent(String eventUuid, int startItem, int itemsPerPage);

    public int loadApproveOrdersForEventCount(String eventUuid);
}
