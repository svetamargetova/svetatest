/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.dao;

import consys.payment.bo.Product;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface ProductDao extends PersistedObjectDao<Product>{


    public Product loadProductByUuid(String uuid) throws NoRecordException;
    
    public List<Product> loadProductsByUuid(List<String> uuid) throws NoRecordException;

    public List<Product> loadEventProducts(String evebtUuid) throws NoRecordException;

    public List<Product> listEventProductsByUuid(String eventUuid, List<String> productUuids);
    
    public List<Product> listEventPaymentProfileProducts(String eventPaymentProfileUuid);

    public int loadProductUsageCount(String productUuid);


    

}
