package consys.payment.webservice.impl;

import consys.payment.bo.Order;
import consys.payment.bo.Transfer;
import consys.payment.bo.enums.TransferState;
import consys.payment.service.TransferService;

import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.utils.PaymentUtils;
import consys.payment.webservice.TransferWebService;
import consys.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.transfer.ListTransfersRequest;
import consys.payment.ws.transfer.ListTransfersResponse;
import consys.payment.ws.transfer.LoadTransferRequest;
import consys.payment.ws.transfer.LoadTransferResponse;
import consys.payment.ws.transfer.TransferHead;
import consys.payment.ws.transfer.TransferItem;
import java.util.List;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class TransferWebServiceImpl extends AbstractWebServiceImpl implements TransferWebService {

    private TransferService transferService;

    @Override
    @PayloadRoot(localPart = "ListTransfersRequest", namespace = NAMESPACE)
    public ListTransfersResponse listEventTransfers(ListTransfersRequest request) {
        ListTransfersResponse response = OBJECT_FACTORY.createListTransfersResponse();
        List<Transfer> transfers = getTransferService().listTransfersByEventUuid(request.getEventUuid(), request.getStartIndex(), request.getItemsPerPage());
        for (Transfer transfer : transfers) {
            TransferHead th = convertToTransferHead(transfer);
            response.getTransfers().add(th);
        }
        response.setAllCount(getTransferService().loadTransfersCountByEventUuid(request.getEventUuid()));
        return response;
    }

    private TransferHead convertToTransferHead(Transfer transfer) {
        TransferHead th = OBJECT_FACTORY.createTransferHead();
        th.setFees(transfer.getFeesSum());
        th.setDate(ConvertUtils.toXmlCalendar(transfer.getTransferDate()));
        th.setThirdPartyFees(transfer.getThirdPartyFeesSum());
        if (!transfer.getState().equals(TransferState.ACTIVE)) {
            th.setInfo(transfer.getCommissionOrder().getOrderNumber());
        } else {
            th.setInfo(null);
        }

        switch (transfer.getState()) {
            case ACTIVE:
                th.setState(consys.payment.ws.transfer.TransferState.ACTIVE);
                break;
            case ERR:
                th.setState(consys.payment.ws.transfer.TransferState.ERROR);
                break;
            case CLOSED_AND_PREPARED:
                th.setState(consys.payment.ws.transfer.TransferState.PREPARED);
                break;
            case TRANSFERED:
                th.setState(consys.payment.ws.transfer.TransferState.TRANSFERED);
                break;

        }
        th.setTotal(transfer.getTotalSum());
        th.setUuid(transfer.getUuid());
        return th;
    }

    @Override
    @PayloadRoot(localPart = "LoadTransferRequest", namespace = NAMESPACE)
    public LoadTransferResponse loadEventTransfer(LoadTransferRequest request) {
        LoadTransferResponse response = OBJECT_FACTORY.createLoadTransferResponse();
        try {
            Transfer t = getTransferService().loadTransferWithOrders(request.getTransferUuid());
            TransferHead th = convertToTransferHead(t);
            response.setTransfer(th);

            List<Order> orders = PaymentUtils.transferOrdersWithoutDuplicity(t.getOrders());

            for (Order order : orders) {
                TransferItem item = OBJECT_FACTORY.createTransferItem();

                item.setDate(ConvertUtils.toXmlCalendar(order.getConfirmedDate()));
                item.setFees(order.getFees());
                item.setThirPartyFees(order.getThirdPartyFee());
                item.setTotal(order.getPrice()); // zatim ne
                item.setUuid(order.getUuid());
                item.setInfo(createOrderInfo(order));
                response.getItems().add(item);
            }
        } catch (NoRecordException ex) {
            throw new NullPointerException(ex.getMessage());
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException();
        }
        return response;
    }

    private String createOrderInfo(Order invoice) {
        return invoice.getOrderType().name();
    }

    /**
     * @return the transferService
     */
    public TransferService getTransferService() {
        return transferService;
    }

    /**
     * @param transferService the transferService to set
     */
    public void setTransferService(TransferService transferService) {
        this.transferService = transferService;
    }
}
