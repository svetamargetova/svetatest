package consys.payment.stripes.bean.event;

import consys.payment.bo.Invoice;
import consys.payment.service.OrderService;
import consys.payment.stripes.bean.ListActionBean;
import java.util.Date;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EventInvoiceListActionBean extends ListActionBean<Invoice> {

    @SpringBean
    private OrderService orderService;
    // data
    private String eventUuid;
    private Date from;
    private Date to;
    private String number;

    public EventInvoiceListActionBean() {
        super("/web/event/eventInvoices.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        getItems().setList(orderService.listEventInvoices(eventUuid, from, to, number, firstItemIndex, itemsPerPage));
        getItems().setTotal(orderService.listEventInvoicesCount(eventUuid, from, to, number));
    }

    public Resolution back() {
        return new RedirectResolution(EventListActionBean.class);
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
