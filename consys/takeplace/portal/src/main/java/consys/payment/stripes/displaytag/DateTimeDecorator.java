package consys.payment.stripes.displaytag;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.servlet.jsp.PageContext;
import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

/**
 *
 * @author pepa
 */
public class DateTimeDecorator implements DisplaytagColumnDecorator {

    private static final FastDateFormat format = FastDateFormat.getInstance("dd.MM.yyyy HH:mm", Calendar.getInstance().getTimeZone(), Locale.getDefault());

    @Override
    public Object decorate(Object o, PageContext pc, MediaTypeEnum mte) throws DecoratorException {
        if (o instanceof Date) {
            return format.format((Date) o);
        } else {
            return "No Date";
        }
    }
}
