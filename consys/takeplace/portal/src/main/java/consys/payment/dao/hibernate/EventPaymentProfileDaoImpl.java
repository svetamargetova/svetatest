/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao.hibernate;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.dao.EventPaymentProfileDao;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public class EventPaymentProfileDaoImpl extends BusinessObjectDaoImpl<EventPaymentProfile> implements EventPaymentProfileDao {

    @Override
    public List<EventPaymentProfile> listEventPaymentProfiles(String eventUuid) throws NoRecordException {
        List<EventPaymentProfile> list = session().
                createQuery("select epp from EventPaymentProfile epp where epp.event.uuid = :UUID").
                setString("UUID", eventUuid).
                list();
        if (list.isEmpty()) {
            throw new NoRecordException("No payment profiles for event with UUID " + eventUuid);
        }
        return list;
    }

    @Override
    public List<EventPaymentProfile> listEventPaymentProfilesWithActualRevision(String uuid) throws NoRecordException {
        List<EventPaymentProfile> list = session().
                createQuery("select epp from EventPaymentProfile epp join fetch epp.paymentProfile pp left join fetch pp.actualRevision where epp.event.uuid = :UUID").
                setString("UUID", uuid).
                list();
        if (list.isEmpty()) {
            throw new NoRecordException("No payment profiles for event with UUID " + uuid);
        }
        return list;
    }

    @Override
    public List<EventPaymentProfile> listEventPaymentProfilesWithActualRevisionAndLicenseDetailed(String eventUuid) throws NoRecordException {
        List<EventPaymentProfile> list = session().
                createQuery("select epp from EventPaymentProfile epp join fetch epp.paymentProfile pp left join fetch pp.actualRevision join fetch epp.event as event left join fetch event.licenseOrder where epp.event.uuid = :UUID").
                setString("UUID", eventUuid).
                list();
        if (list.isEmpty()) {
            throw new NoRecordException("No payment profiles for event with UUID " + eventUuid);
        }
        return list;
    }

    @Override
    public EventPaymentProfile loadEventPaymentProfile(String uuid) throws NoRecordException {
        EventPaymentProfile epp = (EventPaymentProfile) session().
                createQuery("select epp from EventPaymentProfile epp join fetch epp.paymentProfile pp left join fetch pp.actualRevision where epp.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (epp == null) {
            throw new NoRecordException("No EventPaymentProfile with uuid " + uuid);
        }
        return epp;
    }

    @Override
    public EventPaymentProfile loadEventPaymentProfileWithEvent(String uuid) throws NoRecordException {
        EventPaymentProfile epp = (EventPaymentProfile) session().
                createQuery("select epp from EventPaymentProfile epp join fetch epp.paymentProfile pp left join fetch pp.actualRevision join fetch epp.event where epp.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (epp == null) {
            throw new NoRecordException("No EventPaymentProfile with uuid " + uuid);
        }
        return epp;
    }
}
