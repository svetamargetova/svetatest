package consys.payment.service;

import consys.payment.bo.PaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.User;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface UserService {

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    /**
     * Vytvori noveho uzivatela v systeme
     * @param uuid
     * @return
     * @throws RequiredPropertyException 
     */
    public User createUser(String uuid)
            throws RequiredPropertyException;

    /**
     * Vytvori novy platobny profil pre uzivatela v systeme. Ak uzivatel neexistuje
     * v systeme este tak vytovri aj noveho uzivatela
     * @param user
     * @param newProfile
     * @throws RequiredPropertyException
     * @throws IbanFormatException 
     */
    public void createNewPaymentProfile(User user, PaymentProfileRevision newProfile)
            throws RequiredPropertyException, IbanFormatException;

   
    /**
     * Vytvori novy platobny profil pre uzivatela v systeme. Ak uzivatel neexistuje
     * @param userUuid
     * @param newProfile
     * @throws RequiredPropertyException
     * @throws IbanFormatException 
     */
    public void createNewPaymentProfile(String userUuid, PaymentProfileRevision newProfile)
            throws RequiredPropertyException, IbanFormatException;
    
    
    /**
     * Vytvori novu reviziu platobneho profilu pre uzivatela v systeme. Ak uzivatel neexistuje
     * @param userPaymentProfile platobny profil ku ktoremu sa ma pridat revizia
     * @param revision nova revizia
     * @throws RequiredPropertyException
     * @throws IbanFormatException 
     */
    public void createNewPaymentProfileRevision(PaymentProfile userPaymentProfile, PaymentProfileRevision revision)
            throws RequiredPropertyException, IbanFormatException;

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    /**
     * Na zaklade toho ci je platobny profil uzivatela pouzity pri nejakej aktivnej
     * alebo potrvrdenej objednavke tak vytvori novu reviziu profilu alebo upravi
     * stavajuci profil. V pripade ze uzivatel neexistuje tak ho vytvori.
     * 
     * @param userPaymentProfile
     * @param userUuid
     * @param newRevision
     * @throws RequiredPropertyException
     * @throws IbanFormatException 
     */
    public void updateOrCreateNewPaymentProfileRevision(String userUuid, String userPaymentProfile, PaymentProfileRevision newRevision)
            throws RequiredPropertyException, IbanFormatException;
    
   
    
    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    public User loadUser(String uuid)
            throws NoRecordException, RequiredPropertyException;

    public User loadUserWithProfiles(String uuid)
            throws NoRecordException, RequiredPropertyException;
}
