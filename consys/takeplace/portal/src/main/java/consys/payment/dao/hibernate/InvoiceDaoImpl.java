package consys.payment.dao.hibernate;

import consys.payment.bo.Invoice;
import consys.payment.dao.InvoiceDao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.Work;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class InvoiceDaoImpl extends BusinessObjectDaoImpl<Invoice> implements InvoiceDao {

    @Override
    public String nextInvoiceClientNumber() {
        ClientNumberWork work = new ClientNumberWork();
        session().doWork(work);
        return work.getClientNumber();
    }

    @Override
    public String nextInvoiceProductNumber(final String invoiceUuid) {
        InvoiceNumberWork work = new InvoiceNumberWork(invoiceUuid);
        session().doWork(work);
        return work.getInvoiceNumber();
    }

    @Override
    public String nextInvoiceCommissionNumber(String orderUuid) {
        InvoiceCommissionWork work = new InvoiceCommissionWork(orderUuid);
        session().doWork(work);
        return work.getInvoiceNumber();
    }

    @Override
    public List<Invoice> listEventInvoices(String eventUuid, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage) {
        Criteria c = prepareListEventOrdersCriteria(eventUuid, confirmedFrom, confirmedTo, number);
        c.addOrder(org.hibernate.criterion.Order.asc("issueDate"));
        c.setFirstResult(startItem);
        c.setMaxResults(itemsPerPage);
        return c.list();
    }

    @Override
    public int listEventInvoicesCount(String eventUuid, Date confirmedFrom, Date confirmedTo, String number) {
        Criteria c = prepareListEventOrdersCriteria(eventUuid, confirmedFrom, confirmedTo, number);
        c.setProjection(Projections.rowCount());
        return ((Long) c.list().get(0)).intValue();
    }

    private Criteria prepareListEventOrdersCriteria(String event, Date confirmedFrom, Date confirmedTo, String number) {

        Criteria c = session().createCriteria(Invoice.class);

        if (confirmedFrom != null) {
            c.add(Restrictions.ge("issueDate", confirmedFrom));
        }

        if (confirmedTo != null) {
            c.add(Restrictions.le("issueDate", confirmedTo));
        }

        if (StringUtils.isNotBlank(number)) {
            c.add(Restrictions.eq("invoiceNumber", number));
        }

        if (StringUtils.isNotBlank(event)) {
            c.createAlias("order", "or").
                    createAlias("or.supplier", "sp").
                    createAlias("sp.event", "event").
                    add(Restrictions.eq("event.uuid", event));
        }

        c.setFetchMode("order", FetchMode.JOIN);
        c.setFetchMode("order.supplier", FetchMode.JOIN);
        return c;
    }

    private class InvoiceCommissionWork implements Work {

        private String in;
        private String invoiceNumber;

        public InvoiceCommissionWork(String in) {
            this.in = in;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            CallableStatement call = connection.prepareCall("{ ? = call public.invoice_number_next_commission(?) }");
            call.registerOutParameter(1, Types.VARCHAR); // or whatever it is
            call.setString(2, in);
            call.execute();
            invoiceNumber = call.getString(1); // propagate this back to enclosing class
        }

        public String getInvoiceNumber() {
            return invoiceNumber;
        }
    }

    private class InvoiceNumberWork implements Work {

        private String in;
        private String invoiceNumber;

        public InvoiceNumberWork(String in) {
            this.in = in;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            CallableStatement call = connection.prepareCall("{ ? = call public.invoice_number_next_product(?) }");
            call.registerOutParameter(1, Types.VARCHAR); // or whatever it is
            call.setString(2, in);
            call.execute();
            invoiceNumber = call.getString(1); // propagate this back to enclosing class
        }

        /**
         * @return the invoiceNumber
         */
        public String getInvoiceNumber() {
            return invoiceNumber;
        }
    }

    private class ClientNumberWork implements Work {

        private String clientNumber;

        @Override
        public void execute(Connection connection) throws SQLException {
            CallableStatement call = connection.prepareCall("{ ? = call public.invoice_number_next_event() }");
            call.registerOutParameter(1, Types.VARCHAR); // or whatever it is                
            call.execute();
            clientNumber = call.getString(1); // propagate this back to enclosing class
        }

        /**
         * @return the invoiceNumber
         */
        public String getClientNumber() {
            return clientNumber;
        }
    }
}
