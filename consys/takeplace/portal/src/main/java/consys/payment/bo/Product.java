/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.bo;

import consys.payment.bo.enums.ProductCategory;
import java.math.BigDecimal;

/**
 * Jeden zaznam produktu v ramci sluzby
 * @author palo
 */
public class Product implements BusinessObject{
    private static final long serialVersionUID = -1226935685203076615L;

    private Long id;
    /** UUID produktu */
    private String uuid;
    /** Nazov produktu */
    private String name;
    /** Kategoria produktu */
    private ProductCategory category;
    /** Cena */
    private BigDecimal price;
    /** Profil ku ktoremu je to priradene */
    private EventPaymentProfile profile;    
    /** Dan z daneho produktu */
    private BigDecimal vatRate;
    


    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * UUID produktu
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID produktu
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Nazov produktu
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Nazov produktu
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Product e = (Product) obj;

        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Product[name=%s category=%s price=%s]", name,category,price.toPlainString());
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the profile
     */
    public EventPaymentProfile getProfile() {
        return profile;
    }

    /**
     * @param profile the profile to set
     */
    public void setProfile(EventPaymentProfile profile) {
        this.profile = profile;
    }
        
    /**
     * @return the category
     */
    public ProductCategory getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    /**
     * @return the vatRate
     */
    public BigDecimal getVatRate() {
        return vatRate;
    }

    /**
     * @param vatRate the vatRate to set
     */
    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

   
}
