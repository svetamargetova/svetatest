/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo;

import java.util.Date;

/**
 * Objekt mapujuci event 
 * @author palo
 */
public class Event implements BusinessObject {

    private static final long serialVersionUID = 5902924604862734922L;
    private Long id;
    /** UUID eventu v administracii */
    private String uuid;
    /** Priznak ci je event aktivny */
    private boolean active;
    /** Odkaz na licencnu fakturu */
    private Order licenseOrder;
    /** Datum aktivacie */
    private Date activationDate;
    /** Datum ukoncenia aktivacie */
    private Date activationEndDate;
    /** Datum vytvorenia profilu */
    private Date createDate;
    /** Klietnske cislo ktore sluzia kao prefix na vsetkych fakturach eventu */
    private String invoiceClientNumber;


    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * UUID eventu v administracii
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID eventu v administracii
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Priznak ci je event aktivny
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Priznak ci je event aktivny
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Datum aktivacie
     * @return the activationDate
     */
    public Date getActivationDate() {
        return activationDate;
    }

    /**
     * Datum aktivacie
     * @param activationDate the activationDate to set
     */
    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    @Override
    public String toString() {
        return String.format("Event[uuid=%s activated=%s client=%s]", uuid, active, getInvoiceClientNumber());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Event e = (Event) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    /**
     * Datum vytvorenia profilu
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * Datum vytvorenia profilu
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * Odkaz na licencnu fakturu
     * @return the licenseOrder
     */
    public Order getLicenseOrder() {
        return licenseOrder;
    }

    /**
     * Odkaz na licencnu fakturu
     * @param licenseOrder the licenseOrder to set
     */
    public void setLicenseOrder(Order licenseOrder) {
        this.licenseOrder = licenseOrder;
    }

    /**
     * Klietnske cislo ktore sluzia kao prefix na vsetkych fakturach eventu
     * @return the invoiceClientNumber
     */
    public String getInvoiceClientNumber() {
        return invoiceClientNumber;
    }

    /**
     * Klietnske cislo ktore sluzia kao prefix na vsetkych fakturach eventu
     * @param invoiceClientNumber the invoiceClientNumber to set
     */
    public void setInvoiceClientNumber(String invoiceClientNumber) {
        this.invoiceClientNumber = invoiceClientNumber;
    }

    /**
     * @return the activationEndDate
     */
    public Date getActivationEndDate() {
        return activationEndDate;
    }

    /**
     * @param activationEndDate the activationEndDate to set
     */
    public void setActivationEndDate(Date activationEndDate) {
        this.activationEndDate = activationEndDate;
    }
}
