package consys.payment.webservice.impl;

import consys.common.utils.enums.CountryEnum;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.service.EventService;
import consys.payment.service.OrderService;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.EventProfileNotFilledException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.webservice.EventWebService;
import consys.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.event.*;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class EventWebServiceImpl extends AbstractWebServiceImpl implements EventWebService {

    private static final Logger logger = LoggerFactory.getLogger(EventWebServiceImpl.class);
    private EventService eventService;
    private OrderService orderService;

    @Override
    @PayloadRoot(localPart = "InitializeEventRequest", namespace = NAMESPACE)
    public InitializeEventResponse initializeEvent(InitializeEventRequest request) {
        InitializeEventResponse response = OBJECT_FACTORY.createInitializeEventResponse();
        try {
            Event event = getEventService().createEvent(request.getEventUuid(), CountryEnum.fromId(request.getCountry()));
            EventPaymentProfile epp = getEventService().loadEventDefaultPaymentProfileDetailed(event.getUuid());
            response.setSuccess(true);
            response.setEventCurrency(ConvertUtils.toWsCurrency(epp.getCurrency()));
        } catch (NoRecordException ex) {
            response.setSuccess(false);
        } catch (RequiredPropertyException ex) {
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @PayloadRoot(localPart = "InitializeCloneEventRequest", namespace = NAMESPACE)
    public InitializeCloneEventResponse initializeCloneEvent(InitializeCloneEventRequest request) {
        InitializeCloneEventResponse response = OBJECT_FACTORY.createInitializeCloneEventResponse();

        try {
            EventPaymentProfile originalProfile = getEventService().loadEventDefaultPaymentProfileDetailed(request.getOriginalEventUuid());
            getEventService().createCloneEvent(originalProfile, request.getCloneEventUuid());
            response.setSuccess(true);
        } catch (RequiredPropertyException ex) {
            logger.error("Bad property to clone event ", ex);
            response.setSuccess(false);
        } catch (NoRecordException ex) {
            logger.error("Cloning event not found ", ex);
            response.setSuccess(false);
        }

        return response;
    }

    @Override
    @PayloadRoot(localPart = "BuyLicenseRequest", namespace = NAMESPACE)
    public BuyLicenseResponse buyLicense(BuyLicenseRequest request) {
        BuyLicenseResponse response = OBJECT_FACTORY.createBuyLicenseResponse();
        try {
            getOrderService().createLicenseOrder(request.getEventUuid(), request.getProfileUuid());
        } catch (NoRecordException ex) {
            logger.error("Event is already activated ", ex);
            response.setException(BuyLicenseExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (RequiredPropertyException ex) {
            logger.error("Missing required property ", ex);
            response.setException(BuyLicenseExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        } catch (EventProfileNotFilledException ex) {
            logger.error("Event Default profile is not filled ", ex);
            response.setException(BuyLicenseExceptionEnum.EVENT_PROFILE_NOT_FILLED_EXCEPTION);
        } catch (EventLicenseAlreadyPurchased ex) {
            logger.error("Event license is already purchased ", ex);
            response.setException(BuyLicenseExceptionEnum.EVENT_LICENSE_ALREADY_PURCHASED);
        }
        return response;
    }

    @Override
    @PayloadRoot(localPart = "BuyLicenseWithProfileRequest", namespace = NAMESPACE)
    public BuyLicenseWithProfileResponse buyLicenseWithProfile(BuyLicenseWithProfileRequest request) {
        BuyLicenseWithProfileResponse response = OBJECT_FACTORY.createBuyLicenseWithProfileResponse();
        try {
            PaymentProfileRevision rev = ConvertUtils.toCoreProfileRevision(request.getProfile());
            getOrderService().createLicenseOrder(request.getEventUuid(), rev.getUuid(), rev);
        } catch (IbanFormatException ex) {
            logger.error("New profile revision has bad IBAN ", ex);
            response.setException(BuyLicenseExceptionEnum.IBAN_FORMAT_EXCEPTION);
        } catch (NoRecordException ex) {
            logger.error("Event is already activated ", ex);
            response.setException(BuyLicenseExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (RequiredPropertyException ex) {
            logger.error("Missing required property ", ex);
            response.setException(BuyLicenseExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        } catch (EventProfileNotFilledException ex) {
            logger.error("Event Default profile is not filled ", ex);
            response.setException(BuyLicenseExceptionEnum.EVENT_PROFILE_NOT_FILLED_EXCEPTION);
        } catch (EventLicenseAlreadyPurchased ex) {
            logger.error("Event license is already purchased ", ex);
            response.setException(BuyLicenseExceptionEnum.EVENT_LICENSE_ALREADY_PURCHASED);
        }
        return response;
    }

    @Override
    public LoadEventPaymentProfileResponse loadEventPaymentProfile(LoadEventPaymentProfileRequest request) {
        LoadEventPaymentProfileResponse resp = OBJECT_FACTORY.createLoadEventPaymentProfileResponse();
        try {
            EventPaymentProfile eventPaymentProfile = eventService.loadEventPaymentProfile(request.getEventProfileUuid());
            resp.setProfile(ConvertUtils.toWsEventProfile(eventPaymentProfile));
            return resp;
        } catch (NoRecordException ex) {
            return resp;
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "LoadEventPaymentDetailsRequest", namespace = NAMESPACE)
    public LoadEventPaymentDetailsResponse loadEventPaymentDetails(LoadEventPaymentDetailsRequest request) {
        LoadEventPaymentDetailsResponse resp = OBJECT_FACTORY.createLoadEventPaymentDetailsResponse();
        try {
            boolean eventSet = false;
            List<EventPaymentProfile> profiles = getEventService().listEventPaymentProfilesDetailed(request.getEventUuid());
            for (EventPaymentProfile eventPaymentProfile : profiles) {
                if (!eventSet) {
                    eventSet = true;
                    Event event = eventPaymentProfile.getEvent();
                    resp.setEventActiveFrom(ConvertUtils.toXmlCalendar(event.getActivationDate()));
                    resp.setEventActiveTill(ConvertUtils.toXmlCalendar(event.getActivationEndDate()));
                    resp.setInvoiceClientNumber(event.getInvoiceClientNumber());
                    if (event.getLicenseOrder() != null) {
                        resp.setOrderUuid(event.getLicenseOrder().getUuid());
                        resp.setOrderConfirmedDate(ConvertUtils.toXmlCalendar(event.getLicenseOrder().getConfirmedDate()));
                        resp.setOrderDueDate(ConvertUtils.toXmlCalendar(event.getLicenseOrder().getDueDate()));
                        resp.setOrderPurchasedDate(ConvertUtils.toXmlCalendar(event.getLicenseOrder().getPurchaseDate()));
                    }
                    resp.getProfiles().add(ConvertUtils.toWsEventProfile(eventPaymentProfile));
                }
            }
            return resp;
        } catch (NoRecordException ex) {
            return resp;
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "UpdateEventProfileRequest", namespace = NAMESPACE)
    public UpdateEventProfileResponse updateEventProfile(UpdateEventProfileRequest request) {
        UpdateEventProfileResponse response = OBJECT_FACTORY.createUpdateEventProfileResponse();
        try {
            EventPaymentProfile epp = getEventService().loadEventPaymentProfile(request.getProfileUuid());
            epp.setCurrency(ConvertUtils.toCoreCurrency(request.getCurrency()));
            epp.setInvoiceNote(request.getInvoiceNote());
            epp.setOrderBankTransferInvoicedToThisProfile(request.isBankInvoicedToProfile());

            PaymentProfileRevision rev = ConvertUtils.toCoreProfileRevision(request.getProfile());
            getEventService().updateEventPaymentProfileRevision(epp, rev);
            return response;
        } catch (NoRecordException ex) {
            logger.error("There is no event profile with inserted uuids", ex);
            response.setException(PaymentProfileExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (RequiredPropertyException ex) {
            logger.error("Missing required properties", ex);
            response.setException(PaymentProfileExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        } catch (IbanFormatException ex) {
            logger.error("Bank account IBAN format error", ex);
            response.setException(PaymentProfileExceptionEnum.IBAN_FORMAT_EXCEPTION);
        }
        return response;

    }

    @Override
    @PayloadRoot(localPart = "ListEventProfilesRequest", namespace = NAMESPACE)
    public ListEventProfilesResponse listEventProfiles(ListEventProfilesRequest request) {
        try {
            List<EventPaymentProfile> profiles = getEventService().listEventPaymentProfiles(request.getEventUuid());
            ListEventProfilesResponse response = OBJECT_FACTORY.createListEventProfilesResponse();
            for (EventPaymentProfile profile : profiles) {
                response.getProfiles().add(ConvertUtils.toWsEventProfile(profile));
            }
            return response;
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Event with inserted uuid exists", ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @return the orderService
     */
    public OrderService getOrderService() {
        return orderService;
    }

    /**
     * @param orderService the orderService to set
     */
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
}
