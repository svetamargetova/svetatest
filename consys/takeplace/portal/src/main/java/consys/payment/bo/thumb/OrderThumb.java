package consys.payment.bo.thumb;

import consys.payment.bo.enums.OrderState;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OrderThumb {

    private String uuid;
    private String orderNumber;
    private OrderState orderState;
    private Date createDate;
    private Date dueDate;
    private BigDecimal price;
    private boolean suspicious;

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the orderState
     */
    public OrderState getOrderState() {
        return orderState;
    }

    /**
     * @param orderState the orderState to set
     */
    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the suspicious
     */
    public boolean isSuspicious() {
        return suspicious;
    }

    /**
     * @param suspicious the suspicious to set
     */
    public void setSuspicious(boolean suspicious) {
        this.suspicious = suspicious;
    }








}
