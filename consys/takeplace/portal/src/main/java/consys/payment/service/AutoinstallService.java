package consys.payment.service;

import org.springframework.context.ApplicationListener;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface AutoinstallService extends ApplicationListener{

    public void installDefaults();
    
}
