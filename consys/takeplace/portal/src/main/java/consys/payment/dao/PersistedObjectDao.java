/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao;

import consys.payment.bo.BusinessObject;
import consys.payment.service.exception.NoRecordException;

/**
 *
 * @author palo
 */
public interface PersistedObjectDao<T extends BusinessObject> {

    public void create(T o);

    public void create(T o, boolean flush);

    public void update(T o);

    public void update(T o, boolean flush);

    public void delete(T o);

    public void savePoint();

    public T load(Long id, Class<T> clazz) throws NoRecordException;
}
