/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao;

import consys.payment.bo.Order;
import consys.payment.bo.enums.OrderFilterEnum;
import consys.payment.service.exception.NoRecordException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public interface OrderDao extends PersistedObjectDao<Order> {

    public Order loadFullyByUuid(String uuid) throws NoRecordException;

    public Order loadFullyByOrderNumber(String number) throws NoRecordException;

    public List<Order> listEventOrders(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage);

    public int loadEventOrdersCount(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number);
}
