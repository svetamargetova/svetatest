/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao;

import consys.payment.bo.PaymentProfile;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface PaymentProfileDao extends PersistedObjectDao<PaymentProfile> {

    public List<PaymentProfile> listEventProfiles(String uuid)
            throws NoRecordException;

    public List<PaymentProfile> listUserProfiles(String uuid)
            throws NoRecordException;

    public PaymentProfile loadProfile(String profileUuid)
            throws NoRecordException;

    public PaymentProfile loadEventProfile(String eventUuid, String profileUuid)
            throws NoRecordException;

    public PaymentProfile loadUserProfile(String userUuid, String profileUuid)
            throws NoRecordException;

    public PaymentProfile loadEventProfileWithActualRevision(String eventUuid, String profileUuid)
            throws NoRecordException;

    public PaymentProfile loadUserProfileWithActualRevision(String userUuid, String profileUuid)
            throws NoRecordException;   
    
    public boolean isEventPaymentProfileUsedInActiveOrder(String paymentProfileUuid);
    
    public boolean isUserPaymentProfileUsedInActiveOrder(String paymentProfileUuid);
}
