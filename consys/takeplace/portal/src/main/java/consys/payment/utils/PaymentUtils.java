package consys.payment.utils;

import consys.common.utils.date.DateProvider;
import consys.payment.bo.Order;
import consys.payment.bo.enums.InvoiceProcessType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PaymentUtils {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(PaymentUtils.class);
    // konstanty
    private static final FastDateFormat NOTE_FORMAT = FastDateFormat.getInstance("dd.MM.yyyy hh:mm:ss");

    /** Ak sa typy rovnaju alebo je t1 vacsie tak vraciame t1 inak sa vracia t2 */
    public static InvoiceProcessType resolveFinalProcessType(InvoiceProcessType t1, InvoiceProcessType t2) {
        if (t2 == null) {
            // akce zdarma
            logger.warn("Event for free, InvoiceProcessType t2 is null");
            return t1;
        }

        final int t1id = t1.toId();
        final int t2id = t2.toId();

        return (t1id >= t2id ? t1 : t2);
    }

    public static String craeteNote(String currentNote, String appendNote) {
        if (StringUtils.isBlank(appendNote)) {
            return currentNote;
        }

        StringBuilder sb;
        if (StringUtils.isNotBlank(currentNote)) {
            sb = new StringBuilder(currentNote);
            sb.append("\r\n");
        } else {
            sb = new StringBuilder();
        }
        sb.append(NOTE_FORMAT.format(DateProvider.getCurrentDate()));
        sb.append(": ");
        sb.append(appendNote);
        return sb.toString();
    }

    /** vygeneruje novy seznam bez duplicitnich objednavek */
    public static List<Order> transferOrdersWithoutDuplicity(List<Order> transferOrders) {
        List<Order> orders = new ArrayList<Order>(transferOrders);
        HashSet<Order> hs = new HashSet<Order>();
        hs.addAll(orders);
        hs.remove(null);
        orders.clear();
        orders.addAll(hs);
        return orders;
    }
}
