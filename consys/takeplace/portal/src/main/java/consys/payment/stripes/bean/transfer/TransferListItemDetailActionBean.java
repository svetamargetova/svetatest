package consys.payment.stripes.bean.transfer;

import consys.payment.bo.Transfer;
import consys.payment.service.TransferService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.PaymentException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import java.io.IOException;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class TransferListItemDetailActionBean extends PaymentActionBean {

    @SpringBean
    private TransferService transferService;
    private Transfer transfer;
    private String uuid;

    @DontValidate
    @DefaultHandler
    public Resolution preEdit() throws RequiredPropertyException, NoRecordException {
        this.transfer = transferService.loadTransferDetail(getUuid());
        return new ForwardResolution("/web/transfer/transferEdit.jsp");
    }

    public Resolution back() throws IOException {
        return new RedirectResolution(TransferListActionBean.class);
    }

    public Resolution makePrepared() throws IOException, RequiredPropertyException, NoRecordException, PaymentException {
        Transfer t = transferService.loadTransferDetail(uuid);
        transferService.updateActiveTransferToPreparedState(t.getEventPaymentProfile().getUuid(), false);
        return new RedirectResolution(TransferListActionBean.class);
    }
    
    public Resolution makePreparedPrevious() throws IOException, RequiredPropertyException, NoRecordException, PaymentException {
        Transfer t = transferService.loadTransferDetail(uuid);
        transferService.updateActiveTransferToPreparedState(t.getEventPaymentProfile().getUuid(), true);
        return new RedirectResolution(TransferListActionBean.class);
    }

    public Resolution makeTransfered() throws IOException, RequiredPropertyException, NoRecordException, PaymentException {
        transferService.updatePreparedTransferToTransfered(uuid, false);
        return new RedirectResolution(TransferListActionBean.class);
    }
    
    public Resolution makeTransferedPrevious() throws IOException, RequiredPropertyException, NoRecordException, PaymentException {
        transferService.updatePreparedTransferToTransfered(uuid, true);
        return new RedirectResolution(TransferListActionBean.class);
    }
    
    public Resolution recalculate() throws Exception {
        transferService.updateTransferCalculateFees(uuid);
        return new RedirectResolution(TransferListActionBean.class);
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public TransferService getTransferService() {
        return transferService;
    }

    public void setTransferService(TransferService transferService) {
        this.transferService = transferService;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
