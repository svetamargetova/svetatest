package consys.payment.channel.paypal.ipn.enums;

/**
 *
 * cart: Transaction created by a customer:
 *  - Via the PayPal Shopping Cart feature.
 *  - Via Express Checkout when the cart contains multiple items.
 *
 * express_checkout: Transaction created by Express Checkout
 *  when the customer’s cart contains a single item.
 *
 * send-money: Transaction created by customer from the Send
 *  Money tab on the PayPal website.
 *
 * virtual_terminal: Transaction created with Virtual
 *  Terminal.
 *
 * web-accept: Transaction created by customer via Buy Now,
 *  Donation, or Auction Smart Logos.
 *
 * NOTE: In some cases, txn_type is blank when the variable
 *  reason_code is chargeback.
 *
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum PPTransactionType {
    adjustment,
    cart,
    express_checkout,
    masspay,
    merch_pmt,
    new_case,
    recurring_payment,
    recurring_payment_profile_created,
    send_money,
    subscr_cancel,
    subscr_eot,
    subscr_failed,
    subscr_modify,
    subscr_payment,
    subscr_signup,
    virtual_terminal,
    web_accept; // Spracovane z webu
}
