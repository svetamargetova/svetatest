/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.dao;


import consys.payment.bo.EventPaymentProfile;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface EventPaymentProfileDao extends PersistedObjectDao<EventPaymentProfile>{

    /**
     * Nacita vsetky event platobne profilu s aktulanou revizou
     * @param eventUuid
     * @return
     * @throws NoRecordException 
     */
    public List<EventPaymentProfile> listEventPaymentProfiles(String eventUuid) throws NoRecordException;
    public List<EventPaymentProfile> listEventPaymentProfilesWithActualRevision(String eventUuid) throws NoRecordException;

    public List<EventPaymentProfile> listEventPaymentProfilesWithActualRevisionAndLicenseDetailed(String eventUuid) throws NoRecordException;

    /**
     * Nacita event payment profil s aktualnou revizou platobneho profilu.
     * @param uuid profilu
     * @return event platobny profil s aktualneou revizou platobneho profilu
     * @throws NoRecordException 
     */
    public EventPaymentProfile loadEventPaymentProfile(String uuid) throws NoRecordException;
    
    /**
     * Nacita event payment profil s aktualnou revizou platobneho profilu a eventom
     * @param uuid profilu
     * @return event platobny profil s aktualneou revizou platobneho profilu a eventom
     * @throws NoRecordException 
     */
    public EventPaymentProfile loadEventPaymentProfileWithEvent(String uuid) throws NoRecordException;

}
