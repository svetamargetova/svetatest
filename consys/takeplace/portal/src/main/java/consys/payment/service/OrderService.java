/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service;

import consys.payment.bo.ApproveOrder;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Invoice;
import consys.payment.bo.Order;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.Transaction;
import consys.payment.bo.enums.OrderFilterEnum;
import consys.payment.bo.enums.OrderState;
import consys.payment.bo.thumb.ProductOrder;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.EventProfileNotFilledException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.OrderAlreadyCanceledException;
import consys.payment.service.exception.OrderAlreadyPayedException;
import consys.payment.service.exception.ProductIncompatibilityException;
import consys.payment.service.exception.RequiredPropertyException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public interface OrderService {
    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/

    public Order createOrder(String orderUuid, List<ProductOrder> items, String customerProfileUuid, String supplierProfileUuid)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException;

    public Order createOrder(String orderUuid, List<ProductOrder> items, String customerProfileUuid, EventPaymentProfile supplierProfile)
            throws NoRecordException, RequiredPropertyException, IbanFormatException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException;

    public Order createOrder(String orderUuid, List<ProductOrder> items, PaymentProfileRevision customerProfile, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException;

    /**
     * Vytvori objednavku na produkty kde customer bude uzivatel, ktoreho platobny
     * profil sa natiahne ako defaultny profil, resp. vytvori sanovy prazdny.
     * @param orderUuid uuid registracie ci inej objednavku
     * @param items polozky ktore sa maju objednat
     * @param userUuid uuid uzivatela
     * @param supplier event platobny profil
     * @return nova objednavka
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws EventNotActiveException
     * @throws ProductIncompatibilityException
     * @throws EventProfileNotFilledException
     * @throws IbanFormatException
     */
    public Order createUserOrder(String orderUuid, List<ProductOrder> items, String userUuid, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException;

    /**
     * Vytvori objednavku na produkty kde customer bude uzivatel, ktoreho platobny
     * profil bude upraveny novou reviziou. Ak nebola stavajuca revizia pouzita
     * tak stavajucu revizii upravi.
     * @param orderUuid uuid registracie ci inej objednavku
     * @param items polozky ktore sa maju objednat
     * @param userUuid uuid uzivatela
     * @param userPaymentProfileUuid uuid platobneho profilu
     * @param userNewProfileRevision nova revizia profilu
     * @param supplier  event platobny profil
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws EventNotActiveException
     * @throws ProductIncompatibilityException
     * @throws EventProfileNotFilledException
     * @throws IbanFormatException
     */
    public Order createUserOrder(String orderUuid, List<ProductOrder> items, String userUuid, String userPaymentProfileUuid, PaymentProfileRevision userNewProfileRevision, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException;

    /**
     * Vytvori objednavku na produkty kde customer bude uzivatel, ktoreho platobny
     * profil bude upraveny novou reviziou. Ak nebola stavajuca revizia pouzita
     * tak stavajucu revizii upravi.
     * @param orderUuid uuid registracie ci inej objednavku
     * @param items polozky ktore sa maju objednat
     * @param userUuid uuid uzivatela
     * @param userPaymentProfileUuid uuid platobneho profilu uzivatela
     * @param supplier  event platobny profil
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws EventNotActiveException
     * @throws ProductIncompatibilityException
     * @throws EventProfileNotFilledException
     * @throws IbanFormatException
     */
    public Order createUserOrder(String orderUuid, List<ProductOrder> items, String userUuid, String userPaymentProfileUuid, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException;

    /**
     * Zakupenie licencie pre dany even definovanym profilom. Profil musi byt
     * spravne nainicializovany.
     * 
     * @throws NoRecordException ak event neexistuje alebo neexistuje profil
     * @throws RequiredPropertyException ak chyba nejaky vstup
     */
    public Order createLicenseOrder(String eventUuid, String profileUuid)
            throws NoRecordException, RequiredPropertyException, EventProfileNotFilledException, EventLicenseAlreadyPurchased;

    /**
     * Zakupenie licencie pre dany even upravenym profilom. Profil musi byt spravne
     * nainicializovany.     
     * @throws NoRecordException ak event neexistuje alebo neexistuje profil
     * @throws RequiredPropertyException ak chyba nejaky vstup
     */
    public Order createLicenseOrder(String eventUuid, String profileUuid, PaymentProfileRevision revision)
            throws NoRecordException, RequiredPropertyException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, IbanFormatException;

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    /**
     * Oznaci objednavku za potvrdenu pouzitim transakcie - objednakvia bude mat
     * stav Confirmed_System. Ak nebudu peniaze dostatocne nebude objednavka prijata
     * Ak su peniaze dostatocne vytvori sa entita Invoice a vygeneruje faktura pre
     * dany typ eventu a typu produktu.     
     *
     * 
     * @param order  objednavka
     * @param transaction transakcia     
     * @return faktura
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws OrderApproveException ak je nieco zle
     */
    public Invoice updateOrderToConfirmed(Order order, Transaction transaction)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Oznaci objednavky za potvrdene. Ak nebude stacit hodnota bude potreba schvalit.
     *
     * @param order
     * @param transaction
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws OrderApproveException
     */
    public List<Invoice> updateOrdersToConfirmed(List<Order> order, Transaction transaction)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Oznaci objednavku za potvrdenu organizatorom. V tomto pripade sa nebude generovat
     * faktura.
     * 
     * @param orderNumber cislo objednavky
     * @param userUuid uuid uzivatela ktory potvrdil
     * @param note poznmka od organizatora
     * @return objednavka
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws OrderAlreadyPayedException
     * @throws OrderAlreadyCanceledException 
     */
    public Order updateOrderToConfirmedByOrganizator(String orderNumber, String userUuid, String note)
            throws NoRecordException, RequiredPropertyException;

    public Order updateOrderToConfirmedByOrganizator(Order order, String userUuid, String note)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Oznaci objednavku za potvrdenu administratorom systemu. Toto oznacenie
     * je mozne len ak je objednavka v stave active. Ak je objednavka v stave
     * waitnign to approve, je potreba vyuzit specializovanu sluzbu.
     * 
     * @param order
     * @param generateInvoice
     * @param note     
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws OrderAlreadyPayedException
     * @throws OrderAlreadyCanceledException 
     */
    public void updateOrderToConfirmedByAdministrator(Order order, boolean generateInvoice, boolean notifyOwner, String note)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Oznaci objednavku zo stavu waiting to approve na zaklade priznaku <code>approve</code>.
     * Ak je <code>true</code> potom oznaci objednavku za potvrdenu a notifikuje
     * dalsie subsystemy. V opacnom pripade da objednavku do stavu canceled.
     *
     *
     * @param approveOrderId id entity approve order
     * @param approve priznak ci sa ma potvrdit alebo nie
     * @param note poznamka ktora sa ma pridat objednavke
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public void updateOrderApprove(Long approveOrderId, boolean approve, String note)
            throws RequiredPropertyException, NoRecordException;

    /** provede aktualizaci dat objednavky */
    void updateOrder(Order order) throws RequiredPropertyException;
    
    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    public Order loadOrderByUuid(String uuid)
            throws NoRecordException, RequiredPropertyException;

    public Order loadOrderByNumber(String order)
            throws NoRecordException, RequiredPropertyException;

    public ApproveOrder loadApproveOrderByOrderUuid(String uuid)
            throws NoRecordException, RequiredPropertyException;

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    public List<Order> listEventOrders(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage);

    public int loadEventOrdersCount(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number);

    public List<ApproveOrder> listApproveOrders(int startItem, int itemsPerPage);

    public int loadApproveOrdersCount();

    public List<ApproveOrder> listApproveOrdersForEvent(String eventUuid, int startItem, int itemsPerPage);

    public int loadApproveOrdersForEventCount(String eventUuid);

    public List<Invoice> listEventInvoices(String eventUuid, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage);

    public int listEventInvoicesCount(String eventUuid, Date confirmedFrom, Date confirmedTo, String number);


    /*------------------------------------------------------------------------*/
    /* ---  D E L E T E --- */
    /*------------------------------------------------------------------------*/
    /**
     * Vybranu objednavku prevede do stavu zrusenia na zaklade priznaku 
     * <code>toState</code>. Priznak <code>notify</code> ak je true tak notifikuje
     * podla typu objednavky administraciu alebo overseera.
     * 
     * 
     * 
     * @param orderUuid
     * @param toState
     * @param note
     * @param notify
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws OrderAlreadyPayedException 
     */
    public void cancelOrder(String orderUuid, OrderState toState, String note, boolean notify)
            throws NoRecordException, RequiredPropertyException, OrderAlreadyPayedException;

    public void cancelOrders(List<String> orderUuids, OrderState toState, String note, boolean notify)
            throws NoRecordException, RequiredPropertyException, OrderAlreadyPayedException;

    public void cancelPaidOrder(Order order, String note, boolean notify)
            throws NoRecordException, RequiredPropertyException;
}
