package consys.payment.service.impl;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.common.utils.enums.Currency;
import consys.payment.bo.Order;
import consys.payment.bo.Transaction;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.bo.enums.OrderState;
import consys.payment.channel.bank.BankTransaction;
import consys.payment.channel.paypal.ipn.PaypalTransaction;
import consys.payment.dao.TransactionDao;
import consys.payment.service.OrderService;
import consys.payment.service.TransactionService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author palo
 */
public class TransactionServiceImpl extends Service implements TransactionService {

    private TransactionDao transactionDao;
    private OrderService orderService;

    @Override
    public void createBankTransactions(List<BankTransaction> bankTransactions)
            throws RequiredPropertyException, NoRecordException {
        for (BankTransaction bankTransaction : bankTransactions) {

            Transaction t = new Transaction();
            t.setAmount(bankTransaction.getAmount());
            t.setConfirmationChannel(ConfirmationChannel.BANK_TRANSFER);
            t.setFees(BigDecimal.ZERO);
            t.setTransactionDate(bankTransaction.getDate());
            t.setTransactionId(bankTransaction.getReferenceId());
            t.setCurrency(bankTransaction.getCurrency());
            t.appendToNote(Joiner.on(", ").join(bankTransaction.getVariableSymbol()));
            createTransaction(t);
            // ak je aspon jeden symbol
            if (bankTransaction.getVariableSymbol().size() == 1) {
                try {
                    Order order = getOrderService().loadOrderByNumber(bankTransaction.getVariableSymbol().get(0));
                    logger.info("Processing one bank transaction: {} to confirm {}", t, order);
                    getOrderService().updateOrderToConfirmed(order, t);
                } catch (NoRecordException ex) {
                    t.appendToNote(String.format("Transakcia obsahuje nejaky cisleny kod(%s) ktory nie je pravdepodobne cislo objednavky ", bankTransaction.getVariableSymbol().get(0)));
                } catch(Exception e){
                    t.appendToNote("Exception pri spracovani");
                }
            } else if (bankTransaction.getVariableSymbol().size() > 1) {
                // ak je symbolov viacej skusime nacitat objednakvy a porovnat

                List<Order> orders = Lists.newArrayListWithCapacity(bankTransaction.getVariableSymbol().size());
                for (String number : bankTransaction.getVariableSymbol()) {
                    try {
                        orders.add(getOrderService().loadOrderByNumber(number));
                    } catch (NoRecordException ex) {
                        t.appendToNote(String.format("Transakcia obsahuje nejaky cisleny kod(%s) ktory nie je pravdepodobne cislo objednavky ", number));
                    }
                }

                logger.info("Processing bank transaction: {} to confirm more {} orders ", t, orders.size());
                getOrderService().updateOrdersToConfirmed(orders, t);
            }
        }
    }

    @Override
    public void createPaypalTransaction(PaypalTransaction paypalTransaction)
            throws RequiredPropertyException {
        if (paypalTransaction.paymentGross().compareTo(BigDecimal.ZERO) > 0 && StringUtils.isNotBlank(paypalTransaction.invoice())) {
            Transaction t = new Transaction();
            t.setConfirmationChannel(ConfirmationChannel.ONLINE_PAY_PAL);
            t.setAmount(paypalTransaction.paymentGross());
            t.setFees(paypalTransaction.paymentFee());
            t.setTransactionId(paypalTransaction.transactionId());
            t.setTransactionDate(paypalTransaction.paymentDate());
            switch (paypalTransaction.paymentCurrency()) {
                case CZK:
                    t.setCurrency(Currency.CZK);
                    break;
                case EUR:
                    t.setCurrency(Currency.EUR);
                    break;
                case USD:
                    t.setCurrency(Currency.USD);
                    break;
                default:
                    t.appendToNote(String.format("Transakcia obsahuje nepodorovanu menu: %s", paypalTransaction.paymentCurrency().name()));
            }

            createTransaction(t);
            try {
                Order order = getOrderService().loadOrderByUuid(paypalTransaction.invoice());
                getOrderService().updateOrderToConfirmed(order, t);
            } catch (NoRecordException ex) {
                logger.info("Paypal transaction not linked to order: {} ", paypalTransaction.invoice());
            }
        } else {
            logger.info("Paypal {} not processed because amount {} <= 0 or missing order uuid", paypalTransaction.transactionId(), paypalTransaction.paymentGross().toPlainString());
        }
    }

    @Override
    public void createTransaction(Transaction transaction) throws RequiredPropertyException {
        if (transaction.getConfirmationChannel() == null || transaction.getAmount() == null || transaction.getTransactionDate() == null) {
            throw new RequiredPropertyException("Missing channel or amount or transaction date");
        }
        transaction.setUuid(UuidProvider.getUuid());
        transaction.setCreatedDate(DateProvider.getCurrentDate());
        getTransactionDao().create(transaction);
    }

    @Override
    public void createTransactions(List<Transaction> transactions) throws RequiredPropertyException {
        for (Transaction transaction : transactions) {
            createTransaction(transaction);
        }
    }

    @Override
    public Transaction loadTransactionByUuid(String uuid) throws NoRecordException {
        if (StringUtils.isBlank(uuid)) {
            throw new NoRecordException("Missing uuid parameter");
        }
        return getTransactionDao().loadTransactionByUuid(uuid);
    }

    @Override
    public Transaction loadTransactionByTransactionId(String transactionId) throws NoRecordException {
        if (StringUtils.isBlank(transactionId)) {
            throw new NoRecordException("Missing transactionId parameter");
        }
        return getTransactionDao().loadTransactionByTransactionId(transactionId);
    }

    @Override
    public List<Transaction> listTransactions(ConfirmationChannel channel, Date from, Date to, int startItem, int itemsPerPage) {
        return getTransactionDao().listTransactions(channel, from, to, startItem, itemsPerPage);
    }

    @Override
    public int listTransactionsCount(ConfirmationChannel channel, Date from, Date to) {
        return getTransactionDao().listTransactionsCount(channel, from, to);
    }

    @Override
    public void refundedPaypalTransaction(PaypalTransaction paypalTransaction) throws RequiredPropertyException {
        try {            
            Order order = getOrderService().loadOrderByUuid(paypalTransaction.invoice());
            logger.info("PayPal refunding: {}",order);
            
            if (order.getInvoice() != null) {
                Transaction t = order.getInvoice().getTransaction();
                if (t == null) {
                    // vytvorime novu transakciu ak nema invoice ziadnu 
                    t = new Transaction();
                    t.setConfirmationChannel(ConfirmationChannel.ONLINE_PAY_PAL);
                    t.setAmount(paypalTransaction.paymentGross());
                    t.setFees(paypalTransaction.paymentFee());
                    t.setTransactionId(paypalTransaction.transactionId());
                    t.setTransactionDate(paypalTransaction.paymentDate());
                    switch (paypalTransaction.paymentCurrency()) {
                        case CZK:
                            t.setCurrency(Currency.CZK);
                            break;
                        case EUR:
                            t.setCurrency(Currency.EUR);
                            break;
                        case USD:
                            t.setCurrency(Currency.USD);
                            break;
                        default:
                            t.appendToNote(String.format("Transakcia obsahuje nepodorovanu menu: %s", paypalTransaction.paymentCurrency().name()));
                    }                    
                    createTransaction(t);
                }
                
                t.appendToNote("Refundovanie objednavky.");
                transactionDao.update(t);
            }

            orderService.cancelPaidOrder(order,"Refundovane z PayPalu.", true);            
        } catch (NoRecordException ex) {
            logger.error("No Order for refunded transaction with invoice: {}",paypalTransaction.invoice());
            throw new RequiredPropertyException("No order for " + paypalTransaction.invoice());
        }
    }
    
    /**
     * @return the transactionDao
     */
    public TransactionDao getTransactionDao() {
        return transactionDao;
    }

    /**
     * @param transactionDao the transactionDao to set
     */
    public void setTransactionDao(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    /**
     * @return the orderService
     */
    public OrderService getOrderService() {
        return orderService;
    }

    /**
     * @param orderService the orderService to set
     */
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }
}
