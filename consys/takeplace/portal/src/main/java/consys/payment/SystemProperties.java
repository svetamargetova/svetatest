/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment;

import consys.common.utils.enums.CountryEnum;
import java.math.BigDecimal;

/**
 *
 * @author palo
 */
public interface SystemProperties {
    //
    public static final String ACMC_EVENT_UUID_NAME = "DEFAULT_ACEMCEE_EVENT_";
    /********************************************************/
    /* Defaultne hodnoty ACEMCEE platobneho profilu pre CZK */
    /********************************************************/
    public static final String CZK_ACMC_PRODUCT_LICENSE_UUID = "CZK_ACMC_LICENSE_UUID";    
    public static final String CZK_ACMC_PRODUCT_COMMISSION_UUID = "CZK_ACMC_COMMISSION_UUID";   
    public static final String CZK_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID = "CZK_ACMC_THIRD_PARTY_FEE_UUID";
    public static final String CZK_ACMC_PROFILE_UUID = "CZK_ACMC_PROFILE_UUID";
    public static final String CZK_ACMC_NAME = "Acemcee, s.r.o.";
    public static final String CZK_ACMC_STREET = "U Vodárny 3032/2a";
    public static final String CZK_ACMC_CITY = "Brno";
    public static final String CZK_ACMC_ZIP = "616 00";
    public static final CountryEnum CZK_ACMC_COUNTRY = CountryEnum.CZECH_REPUBLIC;
    public static final String CZK_ACMC_DIC_NO = "CZ29208602";
    public static final String CZK_ACMC_ICO_NO = "29208602";
    public static final String CZK_ACMC_BANK_IBAN = "CZ34 2700 0000 0021 0629 6953";
    public static final String CZK_ACMC_BANK_LOCAL = "2106296953 / 2700";
    public static final String CZK_ACMC_BANK_NAME = "UniCredit Bank";
    public static final String CZK_ACMC_BANK_SWIFT = "BACXCZPP";
    public static final String CZK_ACMC_CONST_SYBOL = "-";
    public static final String CZK_ACMC_SPEC_SYMBOL = "-";        
    //
    /********************************************************/
    /* Defaultne hodnoty ACEMCEE platobneho profilu pre EUR */
    /********************************************************/
    public static final String EUR_ACMC_PRODUCT_LICENSE_UUID = "EUR_ACMC_LICENSE_UUID";    
    public static final String EUR_ACMC_PRODUCT_COMMISSION_UUID = "EUR_ACMC_COMMISSION_UUID";   
    public static final String EUR_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID = "EUR_ACMC_THIRD_PARTY_FEE_UUID";
    public static final String EUR_ACMC_PROFILE_UUID = "EUR_ACMC_PROFILE_UUID";
    public static final String EUR_ACMC_NAME = "Acemcee, s.r.o.";
    public static final String EUR_ACMC_STREET = "U Vodárny 3032/2a";
    public static final String EUR_ACMC_CITY = "Brno";
    public static final String EUR_ACMC_ZIP = "616 00";
    public static final CountryEnum EUR_ACMC_COUNTRY = CountryEnum.CZECH_REPUBLIC;
    public static final String EUR_ACMC_DIC_NO = "CZ29208602";
    public static final String EUR_ACMC_ICO_NO = "29208602";
    public static final String EUR_ACMC_BANK_IBAN = "CZ34 2700 0000 0021 0629 6953";
    public static final String EUR_ACMC_BANK_LOCAL = "2106296953 / 2700";
    public static final String EUR_ACMC_BANK_NAME = "UniCredit Bank";
    public static final String EUR_ACMC_BANK_SWIFT = "BACXCZPP";
    public static final String EUR_ACMC_CONST_SYBOL = "-";
    public static final String EUR_ACMC_SPEC_SYMBOL = "-";
    //
    /********************************************************/
    /* Defaultne hodnoty ACEMCEE platobneho profilu pre EUR */
    /********************************************************/
    public static final String USD_ACMC_PRODUCT_LICENSE_UUID = "USD_ACMC_LICENSE_UUID";    
    public static final String USD_ACMC_PRODUCT_COMMISSION_UUID = "USD_ACMC_COMMISSION_UUID";   
    public static final String USD_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID = "USD_ACMC_THIRD_PARTY_FEE_UUID";
    public static final String USD_ACMC_PROFILE_UUID = "USD_ACMC_PROFILE_UUID";
    public static final String USD_ACMC_NAME = "Acemcee, s.r.o.";
    public static final String USD_ACMC_STREET = "U Vodárny 3032/2a";
    public static final String USD_ACMC_CITY = "Brno";
    public static final String USD_ACMC_ZIP = "616 00";
    public static final CountryEnum USD_ACMC_COUNTRY = CountryEnum.CZECH_REPUBLIC;
    public static final String USD_ACMC_DIC_NO = "CZ29208602";
    public static final String USD_ACMC_ICO_NO = "29208602";
    public static final String USD_ACMC_BANK_IBAN = "CZ34 2700 0000 0021 0629 6953";
    public static final String USD_ACMC_BANK_LOCAL = "2106296953 / 2700";
    public static final String USD_ACMC_BANK_NAME = "UniCredit Bank";
    public static final String USD_ACMC_BANK_SWIFT = "BACXCZPP";
    public static final String USD_ACMC_CONST_SYBOL = "-";
    public static final String USD_ACMC_SPEC_SYMBOL = "-";       
    //
    /****************************/
    /* Aktualne proforma cislo  */
    /****************************/        
    public static final String ACTUAL_PROFORMA_NUMBER = "ACTUAL_PROFORMA_NUMBER";
    /**********************************/
    /* Aktualne klietske cislo eventu */
    /**********************************/    
    public static final String ACTUAL_CLIENT_NUMBER = "ACTUAL_CLIENT_NUMBER";
    /************************************/
    /* Defaultny cas splatnosti v dnoch */
    /************************************/    
    public static final int INVOICE_DUE_DATE = 14;
    /***********************************/
    /* Defaultny cas aktivnosti eventu */
    /***********************************/
    public static final int EVENT_ACTIVE = 15;
    //
    /********************************/
    /* Defaultne hodnoty fees - CZK */
    /********************************/
    public static final BigDecimal ORDER_UNIT_FEE_CZK = new BigDecimal(25);
    public static final BigDecimal ORDER_SHARE_FEE_CZK = new BigDecimal("2.9");
    public static final BigDecimal ORDER_MAX_FEE_CZK = new BigDecimal(250);
    //
    /********************************/
    /* Defaultne hodnoty fees - USD */
    /********************************/
    public static final BigDecimal ORDER_UNIT_FEE_USD = new BigDecimal("0.99");
    public static final BigDecimal ORDER_SHARE_FEE_USD = new BigDecimal("2.9");
    public static final BigDecimal ORDER_MAX_FEE_USD = new BigDecimal("9.95");
    //
    /********************************/
    /* Defaultne hodnoty fees - EUR */
    /********************************/
    public static final BigDecimal ORDER_UNIT_FEE_EUR = new BigDecimal("0.99");
    public static final BigDecimal ORDER_SHARE_FEE_EUR = new BigDecimal("2.9");
    public static final BigDecimal ORDER_MAX_FEE_EUR = new BigDecimal("9.95");
}
