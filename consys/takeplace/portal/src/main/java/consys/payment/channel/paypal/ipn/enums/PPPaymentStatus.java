package consys.payment.channel.paypal.ipn.enums;

/**
 *
 * The status of the payment:
 *
 * Canceled_Reversal: A reversal has been canceled. For
 * example, you won a dispute with the customer, and the funds for
 * the transaction that was reversed have been returned to you.
 *
 * Completed: The payment has been completed, and the funds
 * have been added successfully to your account balance.
 *
 * Denied: You denied the payment. This happens only if the
 * payment was previously pending because of possible reasons
 * described for the PendingReason element.
 *
 * Expired: This authorization has expired and cannot be
 * captured.
 *
 * Failed: The payment has failed. This happens only if the
 * payment was made from your customer’s bank account.
 *
 * Pending: The payment is pending. See pending_reason for
 * more information.
 *
 * Refunded: You refunded the payment.
 *
 * Reversed: A payment was reversed due to a chargeback or
 * other type of reversal. The funds have been removed from your
 * account balance and returned to the buyer. The reason for the
 * reversal is specified in the ReasonCode element.
 *
 * Processed: A payment has been accepted.
 *
 * Voided: This authorization has been voided.
 *
 *
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum PPPaymentStatus {

    Canceled_Reversal,
    Completed, 
    Denied, 
    Expired,
    Failed,
    Pending,
    Processed,
    Refunded,
    Reversed,
    Voided;
}
