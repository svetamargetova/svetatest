/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.dao.hibernate;

import consys.payment.bo.BusinessObject;
import consys.payment.dao.PersistedObjectDao;
import consys.payment.service.exception.NoRecordException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 *
 * @author palo
 */
public class BusinessObjectDaoImpl<T extends BusinessObject> extends HibernateDaoSupport implements PersistedObjectDao<T>{


    
    protected Session session() {
        Session session = getSession(false);
        if (session == null) {
            throw new SessionException("Session is NULL!");
        } else if (!session.getTransaction().isActive()) {
            throw new SessionException("Transaction is not Active!");
        }
        return session;
    }

    @Override
    public void create(T o) {
        create(o, true);
    }

    @Override
    public void create(T o, boolean flush) {
        session().save(o);
        if (flush) {
            session().flush();
        }
    }

    @Override
    public void update(T o) {
        update(o, true);
    }

    @Override
    public void update(T o, boolean flush) {
        session().update(o);
        if (flush) {
            session().flush();
        }
    }

    @Override
    public void delete(T o) {
        session().delete(o);
        session().flush();
    }

    @Override
    public T load(Long id, Class<T> clazz) throws NoRecordException {
        T l = (T) session().get(clazz, id);
        if (l == null) {
            throw new NoRecordException("No record for " + clazz.getSimpleName() + " with id " + id);
        }
        return l;
    }

    @Override
    public void savePoint() {
        getSessionFactory().getCurrentSession().flush();
        getSessionFactory().getCurrentSession().getTransaction().commit();
        getSessionFactory().getCurrentSession().getTransaction().begin();
    }

}
