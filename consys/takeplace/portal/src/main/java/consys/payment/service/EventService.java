/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service;

import consys.common.utils.enums.CountryEnum;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Order;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.service.exception.EventAlreadyActiveException;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.EventLicenseNotPayedException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface EventService {

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    /**
     * Vytvorenie noveho eventu v payment portale a inicializacia vychodzieho
     * platobneho profilu
     * @param event novy event    
     * @throws RequiredPropertyException ak chybaju povinne udaje
     */
    public Event createEvent(String uuid, CountryEnum country)
            throws RequiredPropertyException;
    
    /**
     * Vytvorenie noveho eventu v payment portale
     * @param event novy event    
     * @throws RequiredPropertyException ak chybaju povinne udaje
     */
    public Event createEvent(String uuid, boolean createDefaultProfile, CountryEnum country)
            throws RequiredPropertyException;

    public void createEventPaymentProfile(Event event, EventPaymentProfile epp)
            throws RequiredPropertyException;
    
    /** vytvori klon nastaveni puvodniho eventu (jen platebni informace; faktury, produkty, .... se neklonuji!) */
    public Event createCloneEvent(EventPaymentProfile originalProfile, String cloneEventUuid) throws RequiredPropertyException, NoRecordException;
    
    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    
    /**
     * Nacita platobny profil podla jeho ID
     * @param id
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException 
     */
    public EventPaymentProfile loadEventPaymentProfile(Long id)
            throws NoRecordException, RequiredPropertyException;
    
    /**
     * Nacita platobny profil podla jeho UUID
     * @param uuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException 
     */
    public EventPaymentProfile loadEventPaymentProfile(String uuid)
            throws NoRecordException, RequiredPropertyException;
    /**
     * Nacita vychodzi event platobny profil. AK ma event viac profilov (>1) potom
     * zoberie hned prvy. Nacitany profil je detailne nacitany teda automaticky obsahuje
     * platobny profil a aktualnu reviziu
     * 
     * @param eventUuid uuid eventu
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException 
     */
    public EventPaymentProfile loadEventDefaultPaymentProfileDetailed(String eventUuid)
            throws NoRecordException, RequiredPropertyException;
    
     /**
     * Nacita vychodzi event platobny profil. AK ma event viac profilov (>1) potom
     * zoberie hned prvy. 
     * 
     * @param eventUuid uuid eventu
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException 
     */
    public EventPaymentProfile loadEventDefaultPaymentProfile(String eventUuid)
            throws NoRecordException, RequiredPropertyException;
    
    
    
    
    /**
     * Nacita event podla uuid
     * @param uuid eventu
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException 
     */
    public Event loadEvent(String uuid)
            throws NoRecordException, RequiredPropertyException;    
    
    /**
     * Nacita event podla cisla objednavky.
     * 
     * @param orderNumber cislo objednavky
     * @return event ktory ma objednavku na licenciu s cislom <code>orderNumber</code>
     * @throws NoRecordException
     * @throws RequiredPropertyException 
     */
    public Event loadEventByLicenseOrderNumber(String orderNumber)
            throws NoRecordException, RequiredPropertyException;  
    /**
     * Nacita celkovy poceteventov podla obmedzeni
     * @param active
     * @param fromDate
     * @param toDate
     * @return 
     */
    public int loadAllEventsCount(boolean active,Date fromDate,Date toDate);
    
            
    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    public List<EventPaymentProfile> listEventPaymentProfiles(String eventUuid)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Nacita zoznam vsetkych platobnych profilov eventu s dotiahnutym detailom a
     * informaciami o evente a objednavke licencie ak je.
     * @param eventUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public List<EventPaymentProfile> listEventPaymentProfilesDetailed(String eventUuid)
            throws NoRecordException, RequiredPropertyException;
    
    /**
     * Vylistuje vsetky eventy podliehajuce filtru a strankovaniu. Povinny argument
     * je boolean active, co je obmedzenie na aktivne eventy, nasledne fromDate a 
     * toDate nie su povinne. Je to obmedzenie na vytvorenie eventu, teda kedy bol
     * event vytvoreny
     * @param active ci je event aktivny, ak je aktivny znamena to ze ma zaplatenu licenciu
     * @param fromDate >= kedy bol event registrovany
     * @param toDate <= registracia eventu
     * @param startItem
     * @param itemsPerPage
     * @return
     * @throws NoRecordException 
     */
    public List<Event> listEvents(boolean active,Date fromDate,Date toDate, int startItem, int itemsPerPage);
    
    
    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E  --- */
    /*------------------------------------------------------------------------*/
    
    /**
     * Aktivuje event nastavenim klientskeho cisla. Nekontroluje ci je objednavka
     * zaplatena, resp. vytvorena. Tatu sluzbu by mal volat interne len system.
     * 
     * @param event
     * @param clientNumber
     * @throws NoRecordException
     * @throws EventLicenseNotPayedException
     * @throws RequiredPropertyException
     * @throws EventAlreadyActiveException 
     */
    public void updateEventActivateNow(String event, String clientNumber)
            throws NoRecordException, RequiredPropertyException, EventAlreadyActiveException;

    /**
     * Aktivuje event nastavenim klientskeho cisla. Nekontroluje ci je objednavka
     * zaplatena, resp. vytvorena. Tatu sluzbu by mal volat interne len system.
     * 
     * @param event
     * @param clientNumber
     * @throws NoRecordException
     * @throws EventLicenseNotPayedException
     * @throws RequiredPropertyException
     * @throws EventAlreadyActiveException 
     */
    
    public void updateEventActivateNow(Event event, String clientNumber)
            throws NoRecordException, RequiredPropertyException, EventAlreadyActiveException;
   
           
    /**
     * Upravi vlastnosti v event profile. Neupravuje payment profil.
     * 
     * @param eventPaymentProfile
     * @throws RequiredPropertyException 
     */
    public void updateEventPaymentProfile(EventPaymentProfile eventPaymentProfile)
            throws RequiredPropertyException;

    /**
     * Upravi ako event prrofil tak aj platobny profil. Pri uprave platobneho profilu sa 
     * testuje ci je dana revizia uz niekde pouzita ak ano potom sa vytvori nova revizia.
     * @param eventPaymentProfile
     * @param newProfile
     * @throws RequiredPropertyException 
     */
    public void updateEventPaymentProfileRevision(EventPaymentProfile eventPaymentProfile, PaymentProfileRevision newProfile)
            throws RequiredPropertyException,IbanFormatException;
    
    /**
     * Nastavi eventu objednavku na licenciu. Ak event taku objednavku uz ma tak 
     * vynimka.
     * @param event
     * @param order
     * @throws EventLicenseAlreadyPurchased 
     */
    public void updateEventLicenseOrder(Event event, Order order)
            throws RequiredPropertyException,EventLicenseAlreadyPurchased;
            
}
