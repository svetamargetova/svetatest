/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service.fees;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Order;
import consys.payment.bo.OrderItem;
import consys.payment.bo.enums.ProductCategory;

/**
 * Rozhranie k implementacii fees strategie. 
 * @author palo
 */
public interface OrderItemFeeStrategy {
        
    /**
     * Pred kazdym vyhodnotenim fee strategie sa zisti ci je fee strategie urcena
     * pre dany typ polozky na objednavke.
     * 
     * @param category
     * @return 
     */
    public boolean canHandleOrder(ProductCategory category);
    
    public void computeFee(EventPaymentProfile orderProfile, OrderItem item, Order order);
    
}
