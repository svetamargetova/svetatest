package consys.payment.stripes;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sourceforge.stripes.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ExceptionHandler implements net.sourceforge.stripes.exception.ExceptionHandler {


    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);
    

    @Override
    public void handle(Throwable throwable, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.error("Error! ",throwable);
    }

    @Override
    public void init(Configuration configuration) throws Exception {

    }

}
