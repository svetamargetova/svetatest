package consys.payment.stripes.bean.transfer;

import consys.payment.bo.Transfer;
import consys.payment.bo.enums.TransferState;
import consys.payment.service.TransferService;
import consys.payment.stripes.bean.ListActionBean;
import consys.payment.stripes.bean.event.EventListActionBean;
import consys.payment.stripes.bean.event.EventPaymentProfileListActionBean;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class TransferListActionBean extends ListActionBean<Transfer> {

    @SpringBean
    private TransferService transferService;
    // data
    private TransferState actualEnum = TransferState.ACTIVE;
    private String eventUuid;
    private String profileUuid;

    public TransferListActionBean() {
        super("/web/transfer/transferOverview.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        if (eventUuid != null) {
            if (profileUuid != null) {
                getItems().setList(transferService.listTransfers(profileUuid, firstItemIndex, itemsPerPage));
                getItems().setTotal(transferService.loadTransfersCount(profileUuid));
            } else {
                getItems().setList(transferService.listTransfersByEventUuid(eventUuid, firstItemIndex, itemsPerPage));
                getItems().setTotal(transferService.loadTransfersCountByEventUuid(eventUuid));
            }
        } else {
            getItems().setList(transferService.listTransfers(actualEnum, firstItemIndex, itemsPerPage));
            getItems().setTotal(transferService.loadTransfersCount(actualEnum));
        }
    }

    public Resolution back() {
        RedirectResolution resolution;
        if (profileUuid == null) {
            resolution = new RedirectResolution(EventListActionBean.class);
        } else {
            resolution = new RedirectResolution(EventPaymentProfileListActionBean.class);
            resolution.addParameter("eventUuid", eventUuid);
        }
        return resolution;
    }

    public TransferState getActualEnum() {
        return actualEnum;
    }

    public void setActualEnum(TransferState actualEnum) {
        this.actualEnum = actualEnum;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getProfileUuid() {
        return profileUuid;
    }

    public void setProfileUuid(String profileUuid) {
        this.profileUuid = profileUuid;
    }
}
