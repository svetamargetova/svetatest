package consys.payment.utils;

import consys.payment.bo.Product;
import consys.payment.service.exception.RequiredPropertyException;
import java.math.BigDecimal;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProductUtils {

    public static void validate(Product product) throws RequiredPropertyException {

         if (StringUtils.isBlank(product.getUuid())) {
            throw new RequiredPropertyException("Product is missing UUID");
        }

        if (StringUtils.isBlank(product.getName())) {
            throw new RequiredPropertyException("Product is missing name");
        }

        if (product.getPrice() == null || product.getPrice().compareTo(BigDecimal.ZERO) == -1) {
            throw new RequiredPropertyException("Product price is < 0 or missing " + product.getPrice());
        }

        if (product.getProfile() == null) {
            throw new RequiredPropertyException("Product is missing profile");
        }
        
    }
}
