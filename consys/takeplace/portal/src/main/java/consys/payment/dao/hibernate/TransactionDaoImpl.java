/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao.hibernate;

import consys.payment.bo.Transaction;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.dao.TransactionDao;
import consys.payment.service.exception.NoRecordException;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author palo
 */
public class TransactionDaoImpl extends BusinessObjectDaoImpl<Transaction> implements TransactionDao {

    @Override
    public Transaction loadTransactionByUuid(String uuid) throws NoRecordException {
        Transaction t = (Transaction) session().
                createQuery("from Transaction t where t.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Transaction with uuid " + uuid);
        }
        return t;
    }

    @Override
    public Transaction loadTransactionByTransactionId(String transactionId) throws NoRecordException {
        Transaction t = (Transaction) session().
                createQuery("from Transaction t where t.transactionId=:ID").
                setString("ID", transactionId).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Transaction with transactionId " + transactionId);
        }
        return t;
    }

    @Override
    public List<Transaction> listTransactions(ConfirmationChannel channel, Date from, Date to, int startItem, int itemsPerPage) {
        Criteria c = prepareCriteria(channel, from, to);
        c.addOrder(Order.asc("transactionDate"));
        c.setFirstResult(startItem);
        c.setMaxResults(itemsPerPage);
        return c.list();
    }

    @Override
    public int listTransactionsCount(ConfirmationChannel channel, Date from, Date to) {
        Criteria c = prepareCriteria(channel, from, to);
        c.setProjection(Projections.rowCount());
        return ((Long) c.list().get(0)).intValue();
    }

    private Criteria prepareCriteria(ConfirmationChannel channel, Date from, Date to) {
        boolean hasChannel = channel != null;
        boolean hasFromDate = from != null;
        boolean hasToDate = to != null;


        Criteria c = session().createCriteria(Transaction.class);
        if (hasChannel) {
            c.add(Restrictions.eq("confirmationChannel", channel));
        }
        if (hasFromDate) {
            c.add(Restrictions.ge("transactionDate", from));
        }
        if (hasToDate) {
            c.add(Restrictions.le("transactionDate", to));
        }
        return c;

    }
}
