package consys.payment.stripes.bean.event;

import consys.payment.bo.Event;
import consys.payment.service.EventService;
import consys.payment.stripes.bean.ListActionBean;
import java.util.Date;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EventListActionBean extends ListActionBean<Event> {

    @SpringBean
    private EventService eventService;
    // data
    private boolean active = true;
    private Date from;
    private Date to;

    public EventListActionBean() {
        super("/web/event/events.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        getItems().setList(eventService.listEvents(active, from, to, firstItemIndex, itemsPerPage));
        getItems().setTotal(eventService.loadAllEventsCount(active, from, to));
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
