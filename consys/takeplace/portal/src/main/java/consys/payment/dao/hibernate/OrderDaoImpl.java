/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao.hibernate;

import consys.payment.bo.Order;
import consys.payment.bo.enums.OrderFilterEnum;
import consys.payment.bo.enums.OrderState;
import consys.payment.dao.OrderDao;
import consys.payment.service.exception.NoRecordException;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author palo
 */
public class OrderDaoImpl extends BusinessObjectDaoImpl<Order> implements OrderDao {

    @Override
    public Order loadFullyByUuid(String uuid) throws NoRecordException {
        Order order = (Order) session().
                createQuery("select o from Order o JOIN FETCH o.supplier JOIN FETCH o.customer JOIN FETCH o.items where o.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (order == null) {
            throw new NoRecordException("No order with uuid " + uuid);
        }
        return order;
    }

    @Override
    public Order loadFullyByOrderNumber(String number) throws NoRecordException {
        Order invoice = (Order) session().
                createQuery("select o from Order o JOIN FETCH o.supplier JOIN FETCH o.customer JOIN FETCH o.items where o.orderNumber=:NUMBER").
                setString("NUMBER", number).
                uniqueResult();
        if (invoice == null) {
            throw new NoRecordException("No order with number " + number);
        }
        return invoice;
    }

    @Override
    public List<Order> listEventOrders(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage) {
        Criteria c = prepareListEventOrdersCriteria(eventUuid, stateFilter, purchasedFrom, purchasedTo, confirmedFrom, confirmedTo, number);
        c.addOrder(org.hibernate.criterion.Order.desc("purchaseDate"));
        c.setFirstResult(startItem);
        c.setMaxResults(itemsPerPage);
        return c.list();
    }

    @Override
    public int loadEventOrdersCount(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number) {
        Criteria c = prepareListEventOrdersCriteria(eventUuid, stateFilter, purchasedFrom, purchasedTo, confirmedFrom, confirmedTo, number);
        c.setProjection(Projections.rowCount());
        return ((Long) c.list().get(0)).intValue();
    }

    private Criteria prepareListEventOrdersCriteria(String event, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number) {

        Criteria c = session().createCriteria(Order.class);
        switch (stateFilter) {
            case ONLY_ACTIVE:
                c.add(Restrictions.eq("state", OrderState.ACTIVE));
                break;
            case ONLY_CONFIRMED:
                c.add(Restrictions.between("state", OrderState.CONFIRMED_ORGANIZATOR, OrderState.CONFIRMED_ZERO));
                break;           
            case WAITING_TO_APPROVE:
                c.add(Restrictions.eq("state", OrderState.WAITING_TO_APPROVE));
                break;
            case ALL:
            default:
                // nic
                break;
        }

        if (purchasedFrom != null) {
            c.add(Restrictions.ge("purchaseDate", purchasedFrom));
        }

        if (purchasedTo != null) {
            c.add(Restrictions.le("purchaseDate", purchasedTo));
        }

        if (confirmedFrom != null) {
            c.add(Restrictions.ge("confirmedDate", confirmedFrom));
        }

        if (confirmedTo != null) {
            c.add(Restrictions.le("confirmedDate", confirmedTo));
        }

        if (StringUtils.isNotBlank(number)) {
            int length = number.length();
            if (length == 10) { // OrderNumber
                c.add(Restrictions.eq("orderNumber", number));
            } else if (length == 32) { // UUID
                c.add(Restrictions.eq("uuid", number));
            } else { // asi neco z teho
                c.add(Restrictions.or(Restrictions.like("orderNumber", number), Restrictions.like("uuid", number)));
            }
        }

        if (StringUtils.isNotBlank(event)) {
            c.createAlias("supplier", "sp").
                    createAlias("sp.event", "event").
                    add(Restrictions.eq("event.uuid", event));
        }

        c.setFetchMode("supplier", FetchMode.JOIN);
        c.setFetchMode("supplier.event", FetchMode.JOIN);

        return c;
    }
}
