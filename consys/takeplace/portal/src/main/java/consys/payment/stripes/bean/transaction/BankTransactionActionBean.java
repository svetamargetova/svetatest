package consys.payment.stripes.bean.transaction;

import consys.common.utils.enums.Currency;
import consys.payment.channel.bank.BankTransaction;
import consys.payment.service.TransactionService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.FileBean;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class BankTransactionActionBean extends PaymentActionBean {

    @SpringBean
    private TransactionService transactionService;
    // data
    private FileBean file;
    private String uploadMessage;
    private Pattern pattern;

    public BankTransactionActionBean() {
        pattern = Pattern.compile("[0-9]{10}");
    }

    @DontValidate
    @DefaultHandler
    public Resolution show() {
        return new ForwardResolution("/web/transaction/bankTransaction.jsp");
    }

    public Resolution upload() throws IOException, RequiredPropertyException, ParseException, NoRecordException {
        Reader r = file.getReader("UTF-8");
        BufferedReader br = new BufferedReader(r);
        List<BankTransaction> transactions = new ArrayList<BankTransaction>();

        String s;
        int counter = 0;

        while ((s = br.readLine()) != null) {
            // preskocime hlavicky
            if (counter > 2) {
                String[] splits = s.split(";", 24);
                transactions.add(parseTransaction(splits));
            }
            counter++;
        }

        uploadMessage = "";
        if (!transactions.isEmpty()) {
            transactionService.createBankTransactions(transactions);
            uploadMessage = "Upload file was processed";
        }

        return new ForwardResolution("/web/transaction/bankTransaction.jsp");
    }

    /** rozparsuje polozky pole */
    private BankTransaction parseTransaction(String[] splits) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        BigDecimal amount = new BigDecimal(splits[1].replace(",", "."));
        Currency currency = Currency.valueOf(splits[2]);
        Date date = dateFormat.parse(splits[3]);
        StringBuffer note = new StringBuffer();
        note.append(splits[13]);
        note.append("|");
        note.append(splits[14]);
        note.append("|");
        note.append(splits[15]);
        note.append("|");
        note.append(splits[16]);
        note.append("|");
        note.append(splits[17]);
        note.append("|");
        note.append(splits[18]);
        note.append("|");
        String notes = note.toString();
        String referenceId = splits[23];
        String variableSymbol = splits[20];

        List<String> variableSymbols = new ArrayList<String>();

        if (variableSymbol.isEmpty()) {
            // variabilni symbol je prazdny, projdeme poznamky
            List<String> vss = detectVariableSymbol(notes);
            for (String vs : vss) {
                variableSymbols.add(vs);
            }
            if (vss.isEmpty()) {
                // nepodaril se rozpoznat variabilni symbol v poznamkach
                // zkusime jeste sjednotit poznamky (odstranime oddelovace)
                vss = detectVariableSymbol(notes.replaceAll("\\|", ""));
                for (String vs : vss) {
                    variableSymbols.add(vs);
                }
            }
            if (vss.isEmpty()) {
                // nepodaril se rozpoznat zadny variabilni symbol
                variableSymbols.add(variableSymbol);
            }
        } else {
            // variabilni symbol byl nalezen na spravnem miste
            variableSymbols.add(variableSymbol);
        }

        BankTransaction transaction = new BankTransaction();
        transaction.setAmount(amount);
        transaction.setCurrency(currency);
        transaction.setDate(date);
        transaction.setNote(notes);
        transaction.setReferenceId(referenceId);
        transaction.setVariableSymbol(variableSymbols);
        return transaction;
    }

    /** pokusi se najit variabilni symbol v poznamkach */
    private List<String> detectVariableSymbol(String note) {
        List<String> list = new ArrayList<String>();
        Matcher m1 = pattern.matcher(note);
        while (m1.find()) {
            list.add(m1.group());
        }
        return list;
    }

    public Resolution back() {
        return new ForwardResolution(TransactionListActionBean.class);
    }

    public FileBean getFile() {
        return file;
    }

    public void setFile(FileBean file) {
        this.file = file;
    }

    public String getUploadMessage() {
        return uploadMessage;
    }
}
