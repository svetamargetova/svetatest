package consys.payment.stripes.bean.order;

import consys.payment.bo.Order;
import consys.payment.service.OrderService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import consys.payment.stripes.bean.event.EventInvoiceListActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class OrderListItemActionBean extends PaymentActionBean {

    @SpringBean
    private OrderService orderService;
    // data
    private String uuid;
    private Order order;
    private boolean invoice;
    private String eventUuid;

    @DontValidate
    @DefaultHandler
    public Resolution preView() throws NoRecordException, RequiredPropertyException {
        order = orderService.loadOrderByUuid(uuid);
        return new ForwardResolution("/web/order/orderDetail.jsp");
    }

    public Resolution back() {
        RedirectResolution resolution;
        if (eventUuid != null) {
            if (invoice) {
                resolution = new RedirectResolution(EventInvoiceListActionBean.class);
            } else {
                resolution = new RedirectResolution(OrderListActionBean.class);
            }
            resolution.addParameter("eventUuid", eventUuid);
        } else {
            resolution = new RedirectResolution(OrderListActionBean.class);
        }
        return resolution;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public boolean isInvoice() {
        return invoice;
    }

    public void setInvoice(boolean invoice) {
        this.invoice = invoice;
    }

    public int getListSize() {
        return order.getItems() != null ? order.getItems().size() : 0;
    }

    public Order getOrder() {
        return order;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
