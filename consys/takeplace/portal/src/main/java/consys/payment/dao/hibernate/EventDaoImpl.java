package consys.payment.dao.hibernate;

import consys.payment.bo.Event;
import consys.payment.dao.EventDao;
import consys.payment.service.exception.NoRecordException;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventDaoImpl extends BusinessObjectDaoImpl<Event> implements EventDao {

   

    
    
    @Override
    public Event loadEventByUuid(String uuid) throws NoRecordException {
        Event e = (Event) session().
                createQuery("from Event e where e.uuid = :UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (e == null) {
            throw new NoRecordException("No event with uuid " + uuid + " found");
        }
        return e;

    }

    @Override
    public Event loadEventWithProfilesByUuid(String uuid) throws NoRecordException {
        Event e = (Event) session().
                createQuery("select e from Event e left join fetch e.profiles where e.uuid = :UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (e == null) {
            throw new NoRecordException("No event with uuid " + uuid + " found");
        }
        return e;
    }

    @Override
    public Event loadEventWithLicenseInvoiceByUuid(String uuid) throws NoRecordException {
        Event e = (Event) session().
                createQuery("select e from Event e LEFT JOIN FETCH e.licenseInvoice where e.uuid = :UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (e == null) {
            throw new NoRecordException("No event with uuid " + uuid + " found");
        }
        return e;
    }

    @Override
    public Event loadEventByProfile(Long id) throws NoRecordException {
        Event e = (Event) session().
                createQuery("select e from Event e left join fetch e.profiles as profile where profile.id = :ID").
                setLong("ID", id).
                uniqueResult();
        if (e == null) {
            throw new NoRecordException("No event with profile ID " + id + " found");
        }
        return e;
    }

    @Override
    public int loadAllEventsCount(boolean active, Date fromDate, Date toDate) {
        boolean cFromDate = fromDate != null;
        boolean cToDate = toDate != null;
        StringBuilder sb = new StringBuilder("select count(e.id) from Event e where e.active=:ACTIVE");
        if (cFromDate) {
            sb.append(" and e.createDate >= :fromDate ");
        }
        if (cToDate) {
            sb.append(" and e.createDate <= :toDate ");
        }
        Query query = session().createQuery(sb.toString());
        query.setBoolean("ACTIVE", active);
        if (cFromDate) {
            query.setDate("fromDate", fromDate);
        }
        if (cToDate) {
            query.setDate("toDate", toDate);
        }
        return ((Long)query.uniqueResult()).intValue();




    }

    @Override
    public List<Event> listEvents(boolean active, Date fromDate, Date toDate, int startItem, int itemsPerPage)  {
        boolean cFromDate = fromDate != null;
        boolean cToDate = toDate != null;
        StringBuilder sb = new StringBuilder("select e from Event e where e.active=:ACTIVE");
        if (cFromDate) {
            sb.append(" and e.createDate >= :fromDate ");
        }
        if (cToDate) {
            sb.append(" and e.createDate <= :toDate ");
        }
        sb.append(" order by e.createDate desc");
        Query query = session().createQuery(sb.toString());
        query.setBoolean("ACTIVE", active);
        if (cFromDate) {
            query.setDate("fromDate", fromDate);
        }
        if (cToDate) {
            query.setDate("toDate", toDate);
        }
        return query.setFirstResult(startItem).setMaxResults(itemsPerPage).list();
    }

    @Override
    public Event loadEventByLicenseOrderNumber(String orderNumber) throws NoRecordException {
        Event e = (Event) session().
                createQuery("select e from Event e where e.licenseOrder.orderNumber = :ONUMBER").
                setString("ONUMBER", orderNumber).
                uniqueResult();
        if (e == null) {
            throw new NoRecordException("No event with uuid " + orderNumber + " found");
        }
        return e;
    }
}
