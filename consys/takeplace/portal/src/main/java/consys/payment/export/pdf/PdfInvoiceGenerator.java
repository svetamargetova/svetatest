package consys.payment.export.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.Currency;
import consys.event.export.common.Formats;
import consys.event.export.pdf.FontProvider;
import consys.event.export.pdf.FontProvider.FontStyle;
import consys.payment.SystemProperties;
import consys.payment.bo.*;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.bo.enums.OrderState;
import consys.payment.bo.enums.ProductCategory;
import consys.payment.service.AcemceeService;
import consys.payment.utils.InvoiceItemRecord;
import consys.payment.utils.InvoiceRecord;
import consys.payment.utils.InvoiceRecord.InvoiceVatRecord;
import consys.payment.utils.PaymentUtils;
import consys.payment.utils.ProfileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

/**
 * Verze 2 generatoru faktur
 *
 * Faktura je generovana chybne. Je potreba opravit generovanie tak ze ak: Je
 * VAT_EU:
 *
 * - addProductRowRecord: Hack - Vat bude vzdy 0% a Unit a Net price su rovnake
 * - addTotalAmount: Hack na to aby sa zobrazila total suma
 *
 *
 *
 * @author pepa
 */
public class PdfInvoiceGenerator {

    // servicy
    private AcemceeService acemceeService;
    // konstanty
    public static final float PAGE_WIDTH = 595f;
    public static final float PAGE_HEIGHT = 842f;
    public static final float PAGE_WIDTH_CONST = PAGE_WIDTH / 210f;
    public static final float PAGE_HEIGHT_CONST = PAGE_HEIGHT / 297f;
    private static final BaseColor GRAY_COLOR = new BaseColor(210, 211, 213);
    private static final float[] PRODUCT_SEP_WIDTHS = new float[]{xps(16), xps(63), xps(17), xps(21), xps(14), xps(21), xps(21)};
    private static final float[] PRODUCT_SEP_WIDTHS_OT = new float[]{xps(16), xps(102), xps(17), xps(21), xps(21)};
    private static final float PRODUCT_ROW_HEIGHT = yps(5f);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
    private static final String PERCENT = " %";
    // data
    private Document document;
    private PdfWriter writer;
    private DecimalFormat decimalFormat;
    private Currency currency;
    private int pages;
    private InvoiceRecord record;
    // data - rizeni generatoru
    private boolean isProforma;
    private Order order;
    private Invoice invoice;
    private float[] sepWidths;

    public PdfInvoiceGenerator() {
    }

    private void addMetadata(InvoiceProcessType vatType) {
        if (isProforma) {
            document.addTitle(Pdf.get(Pdf.TAKEPLACE_PRO_FORMA_INVOICE));
        } else {
            document.addTitle((vatType.equals(InvoiceProcessType.VAT_AD)
                    ? Pdf.get(Pdf.INVOICE_ACCOUNTING_DOCUMENT)
                    : Pdf.get(Pdf.INVOICE_TAX_DOCUMENT)));
        }
        document.addAuthor("Takeplace");
        document.addCreator("Takeplace");
        document.setMargins(0, 0, 0, 0);
    }

    /** prepocet na horizontalni floaty z mm */
    public static float xps(float mm) {
        return PAGE_WIDTH_CONST * mm;
    }

    /** prepocet na vertikalni floaty z mm */
    public static float yps(float mm) {
        return PAGE_HEIGHT_CONST * mm;
    }

    /** vlozi sedy ramecek + No. + cislo faktury/proformafaktury */
    private void addInvoiceNumber(String number) throws Exception {
        Rectangle invoiceRect = new Rectangle(xps(146), yps(274), PAGE_WIDTH, PAGE_HEIGHT);
        invoiceRect.setBackgroundColor(GRAY_COLOR);
        document.add(invoiceRect);

        PdfPCell no = new PdfPCell(new Phrase(Pdf.get(Pdf.NO), FontProvider.font(FontStyle.ARIAL_14_BOLD)));
        no.setBorder(PdfPCell.NO_BORDER);
        PdfPCell nbr = new PdfPCell(new Phrase(number, FontProvider.font(FontStyle.ARIAL_14_BOLD)));
        nbr.setBorder(PdfPCell.NO_BORDER);

        PdfPTable table = new PdfPTable(new float[]{13, 37});
        table.setTotalWidth(xps(50));
        table.addCell(no);
        table.addCell(nbr);
        table.writeSelectedRows(0, -1, xps(157), yps(290), writer.getDirectContent());
    }

    /** vlozi text podle parametru bud ze je to proforma nebo normalni faktura */
    private void addInvoiceOrProForma(InvoiceProcessType vatType) throws Exception {
        // obsah
        String value = isProforma ? Pdf.get(Pdf.PRO_FORMA_INVOICE) : (vatType.equals(InvoiceProcessType.VAT_AD)
                ? Pdf.get(Pdf.INVOICE_ACCOUNTING_DOCUMENT)
                : Pdf.get(Pdf.INVOICE_TAX_DOCUMENT));
        Phrase name = new Phrase(value, FontProvider.font(FontStyle.ARIAL_14_BOLD));

        // pozice
        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(xps(12), yps(274), xps(104), yps(282));
        ct1.addElement(name);
        ct1.go();
    }

    /** vlozi hlavicku */
    private void addHead(InvoiceProcessType vatType) throws Exception {
        // vlozeni hlavniho loga takeplace
        writer.getDirectContent().addTemplate(templateBigTakeplace(), xps(2), yps(-2));
        addInvoiceOrProForma(vatType);
        addInvoiceNumber(isProforma ? order.getOrderNumber() : invoice.getInvoiceNumber());
    }

    /** vlozi dodavatele */
    private void addSupplier(EventPaymentProfile profile, InvoiceProcessType vatType) throws Exception {
        PaymentProfileRevision supplier = profile.getPaymentProfile().getActualRevision();

        // obsah
        Phrase title = new Phrase(Pdf.get(Pdf.SUPPLIER) + ":", FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS));
        Paragraph name = new Paragraph(supplier.getName(), FontProvider.font(FontStyle.ARIAL_9_BOLD));
        name.setSpacingBefore(yps(2));
        Font regularFont = FontProvider.font(FontStyle.ARIAL_9_REGULAR);
        Paragraph values = new Paragraph(regularFont.getSize() * 1.2f, supplier.getStreet() + "\n"
                + supplier.getZip() + " " + supplier.getCity() + "\n"
                + supplier.getCountry().getName(), regularFont);
        values.setSpacingBefore(yps(1));

        // pozice
        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(xps(12), yps(209), xps(98), yps(270));
        ct1.addElement(title);
        ct1.addElement(name);
        ct1.addElement(values);
        ct1.go();

        final boolean showVAT = showVAT(vatType);
        String supplierRegNo = StringUtils.isBlank(supplier.getRegNo()) ? "---" : supplier.getRegNo();
        String supplierVatNo = StringUtils.isBlank(supplier.getVatNo()) ? "---" : supplier.getVatNo();

        // comp a vat cislo
        PdfPCell cell1 = new PdfPCell(new Phrase(Pdf.get(Pdf.COMP_NO) + ":", FontProvider.font(FontStyle.ARIAL_9_BOLD)));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPadding(0f);
        PdfPCell cell2 = new PdfPCell(new Phrase(supplierRegNo, FontProvider.font(FontStyle.ARIAL_9_REGULAR)));
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setPadding(0f);
        PdfPCell cell3 = new PdfPCell(new Phrase(showVAT ? Pdf.get(Pdf.VAT_NO) + ":" : "", FontProvider.font(FontStyle.ARIAL_9_BOLD)));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPadding(0f);
        PdfPCell cell4 = new PdfPCell(new Phrase(showVAT ? supplierVatNo : "", FontProvider.font(FontStyle.ARIAL_9_REGULAR)));
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setPadding(0f);

        PdfPTable table = new PdfPTable(4);
        table.setTotalWidth(new float[]{xps(19), xps(28), xps(15), xps(32)});
        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.writeSelectedRows(0, -1, xps(12), yps(232), writer.getDirectContent());

        if (vatType.equals(InvoiceProcessType.VAT_AD)) {
            Phrase noVatPayer = new Phrase(Pdf.get(Pdf.NON_PAYER_OF_VAT), FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS));

            ColumnText ct2 = new ColumnText(writer.getDirectContent());
            ct2.setSimpleColumn(xps(12), yps(200), xps(98), yps(220));
            ct2.addElement(noVatPayer);
            ct2.go();
        }
    }

    /** vlozi odberatele */
    private void addCustomer(PaymentProfileRevision customer) throws Exception {
        // obsah
        Phrase title = new Phrase(Pdf.get(Pdf.CUSTOMER) + ":", FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS));
        Paragraph name = new Paragraph(customer.getName(), FontProvider.font(FontStyle.ARIAL_10_BOLD));
        name.setSpacingBefore(yps(2));
        Font regularFont = FontProvider.font(FontStyle.ARIAL_10_BOLD);
        Paragraph values = new Paragraph(regularFont.getSize() * 1.2f, customer.getStreet() + "\n"
                + customer.getZip() + " " + customer.getCity() + "\n"
                + customer.getCountry().getName(), regularFont);
        values.setSpacingBefore(yps(1));

        // pozice
        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(xps(104), yps(190), PAGE_WIDTH - xps(12), yps(270));
        ct1.addElement(title);
        ct1.addElement(name);
        ct1.addElement(values);
        ct1.go();

        String customerRegNo = StringUtils.isBlank(customer.getRegNo()) ? "---" : customer.getRegNo();
        String customerVatNo = StringUtils.isBlank(customer.getVatNo()) ? "---" : customer.getVatNo();

        // comp a vat cislo
        PdfPCell cell1 = new PdfPCell(new Phrase(Pdf.get(Pdf.COMP_NO) + ":", FontProvider.font(FontStyle.ARIAL_9_BOLD)));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPadding(0f);
        PdfPCell cell2 = new PdfPCell(new Phrase(customerRegNo, FontProvider.font(FontStyle.ARIAL_9_REGULAR)));
        cell2.setBorder(PdfPCell.NO_BORDER);
        cell2.setPadding(0f);
        PdfPCell cell3 = new PdfPCell(new Phrase(Pdf.get(Pdf.VAT_NO) + ":", FontProvider.font(FontStyle.ARIAL_9_BOLD)));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPadding(0f);
        PdfPCell cell4 = new PdfPCell(new Phrase(customerVatNo, FontProvider.font(FontStyle.ARIAL_9_REGULAR)));
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setPadding(0f);

        PdfPTable table = new PdfPTable(4);
        table.setTotalWidth(new float[]{xps(19), xps(28), xps(15), xps(32)});
        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.writeSelectedRows(0, -1, xps(104), yps(232), writer.getDirectContent());
    }

    /** vraci true pokud se ma zobrazovat DPH */
    private boolean showVAT(InvoiceProcessType vatType) {
        return (!vatType.equals(InvoiceProcessType.VAT_OT) && !vatType.equals(InvoiceProcessType.VAT_AD));
    }

    /** cislo objednavky a datum zakoupeni */
    private void addOrderNumberAndPurchaseDate(String orderNo, Date purchaseDate) throws Exception {
        Font fontBold = FontProvider.font(FontStyle.ARIAL_9_BOLD);
        Font fontRegular = FontProvider.font(FontStyle.ARIAL_9_REGULAR);
        PdfPCell cell1 = new PdfPCell(new Phrase(Pdf.get(Pdf.ORDER_NO) + ":", fontBold));
        cell1.setBorder(PdfPCell.NO_BORDER);
        cell1.setPaddingLeft(0f);
        PdfPCell cell2 = new PdfPCell(new Phrase(orderNo, fontRegular));
        cell2.setBorder(PdfPCell.NO_BORDER);
        PdfPCell cell3 = new PdfPCell(new Phrase(Pdf.get(Pdf.PURCHASE_DATE) + ":", fontBold));
        cell3.setBorder(PdfPCell.NO_BORDER);
        cell3.setPaddingLeft(0f);
        cell3.setPaddingTop(yps(1));
        PdfPCell cell4 = new PdfPCell(new Phrase(dateFormat.format(purchaseDate), fontRegular));
        cell4.setBorder(PdfPCell.NO_BORDER);
        cell4.setPaddingTop(yps(1));

        PdfPTable table = new PdfPTable(2);
        table.setTotalWidth(new float[]{xps(27), xps(40)});
        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.writeSelectedRows(0, -1, xps(104), yps(222), writer.getDirectContent());
    }

    /** oddelovac mezi obchodujicimi stranami a infrmacemi o platbe */
    private void addSeparatorOne() throws DocumentException {
        document.add(drawSeparator(0, PAGE_WIDTH, yps(208), true));
    }

    /** vlozi sedy ramecek + vsechny texty detailu o platbe */
    private void addPaymentDetails(final String variableCode) throws Exception {
        // ramecek
        Rectangle detailRect = new Rectangle(0, yps(162), xps(92), yps(208));
        detailRect.setBackgroundColor(GRAY_COLOR);
        document.add(detailRect);

        final String bankLocal;
        final String bankName;
        final String bankIban;
        final String bankSwift;
        final String constSymbol;
        final String specSymbol;

        if (!order.getSupplier().isOrderBankTransferInvoicedToThisProfile()) {
            PaymentProfileRevision ppr = acemceeService.loadEventPaymentProfile(currency).getPaymentProfile().getActualRevision();
            bankLocal = "-";
            bankName = "-";
            bankIban = ppr.getAccountIban();
            bankSwift = ppr.getAccountBankSwift();
            constSymbol = "-";
            specSymbol = "-";
        } else {
            PaymentProfileRevision ppr = order.getSupplier().getPaymentProfile().getActualRevision();
            bankLocal = "-";
            bankName = "-";
            bankIban = ppr.getAccountIban();
            bankSwift = ppr.getAccountBankSwift();
            constSymbol = "-";
            specSymbol = "-";
        }

        // obsah
        Font textFont = FontProvider.font(FontStyle.ARIAL_9_BOLD);
        Phrase title = new Phrase(Pdf.get(Pdf.PAYMENT_DETAILS) + ":", FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS));
        Paragraph names1 = new Paragraph(textFont.getSize() * 1.2f, Pdf.get(Pdf.ACCOUNT_NO) + ":\n"
                + Pdf.get(Pdf.BANK) + ":\n"
                + Pdf.get(Pdf.IBAN) + ":\n"
                + Pdf.get(Pdf.SWIFT_CODE) + ":", textFont);
        names1.setSpacingAfter(yps(2));
        Phrase names2 = new Phrase(textFont.getSize() * 1.2f, Pdf.get(Pdf.VARIABLE_CODE) + ":\n"
                + Pdf.get(Pdf.CONSTANT_CODE) + ":\n"
                + Pdf.get(Pdf.SPECIFIC_CODE) + ":", textFont);
        Paragraph values1 = new Paragraph(textFont.getSize() * 1.2f, bankLocal + "\n"
                + bankName + "\n"
                + bankIban + "\n"
                + bankSwift, textFont);
        values1.setSpacingAfter(yps(2));
        Phrase values2 = new Phrase(textFont.getSize() * 1.2f, variableCode + "\n"
                + constSymbol + "\n"
                + specSymbol, textFont);

        // pozice
        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(xps(12), yps(190), xps(92), yps(205));
        ct1.addElement(title);
        ct1.go();
        ColumnText ct2 = new ColumnText(writer.getDirectContent());
        ct2.setSimpleColumn(xps(12), yps(162), xps(38), yps(196));
        ct2.addElement(names1);
        ct2.addElement(names2);
        ct2.go();
        ColumnText ct3 = new ColumnText(writer.getDirectContent());
        ct3.setSimpleColumn(xps(38), yps(162), xps(92), yps(196));
        ct3.addElement(values1);
        ct3.addElement(values2);
        ct3.go();
    }

    /** vlozi sedy ramecek + datum splatnosti a dalsi stri radky bloku vedle detailu */
    private void addDueDatePart(InvoiceProcessType vatType, Date dueDate, Date issueDate, Date taxableDate) throws Exception {
        // ramecek
        Rectangle detailRect = new Rectangle(xps(146), yps(194), PAGE_WIDTH, yps(208));
        detailRect.setBackgroundColor(GRAY_COLOR);
        document.add(detailRect);

        // obsah
        Phrase names1 = new Phrase(Pdf.get(Pdf.DUE_DATE) + ":", FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS));
        Phrase values1 = new Phrase(dateFormat.format(dueDate), FontProvider.font(FontStyle.ARIAL_11_BOLD));

        String taxableValue;
        String paymentMethodValue;

        if (isProforma) {
            taxableValue = "–";
            paymentMethodValue = "–";
        } else {
            taxableValue = dateFormat.format(taxableDate);
            paymentMethodValue = paymentMethod(invoice.getConfirmationSource());
        }

        Font textFontBold = FontProvider.font(FontStyle.ARIAL_9_BOLD);
        Font textFontRegular = FontProvider.font(FontStyle.ARIAL_9_REGULAR);

        String title;
        String value;
        if (showVAT(vatType)) {
            title = Pdf.get(Pdf.ISSUE_DATE) + ":\n"
                    + Pdf.get(Pdf.TAXABLE_FULFILMENT_DATE) + ":\n"
                    + Pdf.get(Pdf.PAYMENT_METHOD) + ":";
            value = dateFormat.format(issueDate) + "\n"
                    + taxableValue + "\n"
                    + paymentMethodValue;
        } else {
            title = Pdf.get(Pdf.ISSUE_DATE) + ":\n"
                    + Pdf.get(Pdf.PAYMENT_METHOD) + ":";
            value = dateFormat.format(issueDate) + "\n"
                    + paymentMethodValue;
        }

        Paragraph names2 = new Paragraph(textFontBold.getSize() * 2.5f, title, textFontBold);
        names2.setSpacingBefore(yps(8));
        Paragraph values2 = new Paragraph(textFontRegular.getSize() * 2.5f, value, textFontRegular);
        values2.setSpacingBefore(yps(8));

        // pozice
        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(xps(104), yps(162), xps(154), yps(205));
        ct1.addElement(names1);
        ct1.addElement(names2);
        ct1.go();
        ColumnText ct2 = new ColumnText(writer.getDirectContent());
        ct2.setSimpleColumn(xps(154), yps(162), PAGE_WIDTH - xps(12), yps(205));
        ct2.addElement(values1);
        ct2.addElement(values2);
        ct2.go();
    }

    /** vypise text ze se fakturuje, ... */
    private void addInvoiceSaleAndDelivery() throws Exception {
        // text nad tabulkou prehledu
        Phrase text = new Phrase(Pdf.get(Pdf.WE_INVOICE_AT) + ":",
                FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS));

        // pozice
        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(xps(12), yps(143), PAGE_WIDTH, yps(154));
        ct1.addElement(text);
        ct1.go();
    }

    /** oddelovac mezi seznamem produktu a prehledem */
    private float addProductsHeader(float yPos) throws Exception {
        Font fontBold = FontProvider.font(FontStyle.ARIAL_9_BOLD);
        Paragraph pt1 = new Paragraph(Pdf.get(Pdf.ITEM_NO) + ":", fontBold);
        pt1.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt2 = new Paragraph(Pdf.get(Pdf.DESCRIPTION) + ":", fontBold);
        pt2.setAlignment(Paragraph.ALIGN_LEFT);
        Paragraph pt3 = new Paragraph(Pdf.get(Pdf.QUANTITY) + ":", fontBold);
        pt3.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt4 = new Paragraph(Pdf.get(Pdf.UNIT_PRICE) + ":", fontBold);
        pt4.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt5 = new Paragraph(Pdf.get(Pdf.VAT_RATE) + ":", fontBold);
        pt5.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt6 = new Paragraph(Pdf.get(Pdf.VAT_AMOUNT) + ":", fontBold);
        pt6.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt7 = new Paragraph(Pdf.get(Pdf.NET_PRICE) + ":", fontBold);
        pt7.setAlignment(Paragraph.ALIGN_CENTER);

        Paragraph[] paragraphs = sepWidths.length == 7
                ? new Paragraph[]{pt1, pt2, pt3, pt4, pt5, pt6, pt7} : new Paragraph[]{pt1, pt2, pt3, pt4, pt7};

        // pozice
        float yPosDown = yPos - (2 * PRODUCT_ROW_HEIGHT);
        float yPosUp = yPos;
        float space = yps(2);
        float actual = xps(12);
        float s = actual;

        for (int i = 0; i < sepWidths.length; i++) {
            if (i != 0) {
                s += space;
            }
            ColumnText ct = new ColumnText(writer.getDirectContent());
            ct.setSimpleColumn(s, yPosDown, s += sepWidths[i], yPosUp);
            ct.addElement(paragraphs[i]);
            ct.go();
        }

        // oddelovac mezi zahlavim a daty tabulky
        return addProductsSeparator(yPosDown - yps(1.9f), false);
    }

    /** vlozeni separatoru kolem produktu, vraci pozici spodni hrany oddelovace */
    private float addProductsSeparator(float sepYpos, boolean bold) throws Exception {
        float space = yps(2);
        float actual = xps(12);

        for (int i = 0; i < sepWidths.length; i++) {
            document.add(drawSeparator(actual, sepWidths[i], sepYpos, bold));
            actual += space + sepWidths[i];
        }

        return sepYpos;
    }

    /** vlozi prehled produktu, vraci pozici konce prehledu produktu (pod separatorem) */
    private float addProducts(InvoiceProcessType vatType) throws Exception {
        addProductsHeader(yps(147));
        final float startRowPosition = yps(133);
        float rowPosition = startRowPosition;
        int row = 1;
        for (InvoiceItemRecord iir : record.getRecords()) {
            if (rowPosition < yps(50)) {
                addProductsSeparator(rowPosition - yps(4), false);
                newPage();
                rowPosition = addProductsHeader(yps(267)) - yps(1);
            }

            final int r = addProductRowRecord(row, iir, rowPosition, vatType);
            rowPosition -= r * PRODUCT_ROW_HEIGHT;
            row++;
        }
        //return addProductsSeparator(startRowPosition - (rows * PRODUCT_ROW_HEIGHT) - yps(4), true);
        return addProductsSeparator(rowPosition - yps(4), true);
    }

    /** prida jeden zaznam na fakturu, vraci na kolik radku zaznam je */
    private int addProductRowRecord(int row, InvoiceItemRecord iir, float yPos, InvoiceProcessType vatType) throws Exception {
        BigDecimal unitPrice;
        BigDecimal vat;
        BigDecimal vatRate;

        unitPrice = iir.getUnitPrice();
        vat = iir.getVatAmount();
        vatRate = iir.getVatRate();

        String name = iir.getName();
        BigDecimal netPrice;

        boolean ticketAndOT = ProductCategory.TICKET.equals(order.getOrderType()) && InvoiceProcessType.VAT_OT.equals(vatType);
        boolean ticketAndAD = ProductCategory.TICKET.equals(order.getOrderType()) && InvoiceProcessType.VAT_AD.equals(vatType);

        if (ticketAndOT || ticketAndAD) {
            netPrice = iir.getNetPriceWithVat();
        } else {
            netPrice = iir.getNetPrice();
        }

        Font fontRegular = FontProvider.font(FontStyle.ARIAL_9_REGULAR);
        Paragraph pt1 = new Paragraph(String.valueOf(row), fontRegular);
        pt1.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt2 = new Paragraph(name, fontRegular);
        pt2.setAlignment(Paragraph.ALIGN_LEFT);
        Paragraph pt3 = new Paragraph(String.valueOf(iir.getQuantity()), fontRegular);
        pt3.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt4 = new Paragraph(Formats.assignCurrency(currency, decimalFormat.format(unitPrice)), fontRegular);
        pt4.setAlignment(Paragraph.ALIGN_RIGHT);
        Paragraph pt5 = new Paragraph(vatRate + PERCENT, fontRegular);
        pt5.setAlignment(Paragraph.ALIGN_CENTER);
        Paragraph pt6 = new Paragraph(Formats.assignCurrency(currency, decimalFormat.format(vat)), fontRegular);
        pt6.setAlignment(Paragraph.ALIGN_RIGHT);
        Paragraph pt7 = new Paragraph(Formats.assignCurrency(currency, decimalFormat.format(netPrice)), fontRegular);
        pt7.setAlignment(Paragraph.ALIGN_RIGHT);

        int rowCount = 1;
        float space = yps(2);
        float actual = xps(12);
        float yPosDown = yPos - PRODUCT_ROW_HEIGHT;
        float yPosUp = yPos;

        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(actual, yPosDown, actual += sepWidths[0], yPosUp);
        ct1.addElement(pt1);
        ct1.go();

        ColumnText ct2 = new ColumnText(writer.getDirectContent());
        ct2.setSimpleColumn(actual += space, yPosDown, actual += sepWidths[1], yPosUp);
        float y = ct2.getYLine();
        ct2.addElement(pt2);
        if (ct2.go(true) == ColumnText.NO_MORE_TEXT) {
            ct2.setYLine(y);
            ct2.addElement(pt2);
            ct2.go();
        } else {
            do {
                rowCount++;
                ct2.setYLine(y);
                ct2.setSimpleColumn(actual - sepWidths[1], yPos - (rowCount * PRODUCT_ROW_HEIGHT), actual, yPosUp);
                ct2.addElement(pt2);
            } while (ct2.go(true) == ColumnText.NO_MORE_COLUMN);
            ct2.setYLine(y);
            ct2.addElement(pt2);
            ct2.go();
        }

        // fuj fuj fuj - korekce poctu radku
        // TODO: predelat aby to fungovalo uz samo o sobe (je chybna podminka v do-while, funguje jen do tri radku)
        if (rowCount == 3) {
            rowCount = 2;
        } else if (rowCount == 5) {
            rowCount = 3;
        } else if (rowCount == 7) {
            rowCount = 4;
        } else if (rowCount == 9) {
            rowCount = 5;
        }

        ColumnText ct3 = new ColumnText(writer.getDirectContent());
        ct3.setSimpleColumn(actual += space, yPosDown, actual += sepWidths[2], yPosUp);
        ct3.addElement(pt3);
        ct3.go();
        ColumnText ct4 = new ColumnText(writer.getDirectContent());
        ct4.setSimpleColumn(actual += space, yPosDown, actual += sepWidths[3], yPosUp);
        ct4.addElement(pt4);
        ct4.go();

        if (sepWidths.length == 7) {
            ColumnText ct5 = new ColumnText(writer.getDirectContent());
            ct5.setSimpleColumn(actual += space, yPosDown, actual += sepWidths[4], yPosUp);
            ct5.addElement(pt5);
            ct5.go();
            ColumnText ct6 = new ColumnText(writer.getDirectContent());
            ct6.setSimpleColumn(actual += space, yPosDown, actual += sepWidths[5], yPosUp);
            ct6.addElement(pt6);
            ct6.go();
        }

        ColumnText ct7 = new ColumnText(writer.getDirectContent());
        ct7.setSimpleColumn(actual += space, yPosDown, actual += sepWidths[sepWidths.length - 1], yPosUp);
        ct7.addElement(pt7);
        ct7.go();

        return rowCount;
    }

    /** vlozi celkovy zaklad bez dane */
    private void addSubtotal(InvoiceProcessType vatType, float yPos) throws Exception {
        Paragraph value = new Paragraph(Formats.assignCurrency(currency,
                decimalFormat.format(record.getSubTotal())), FontProvider.font(FontStyle.ARIAL_11_BOLD));
        value.setAlignment(Paragraph.ALIGN_RIGHT);

        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(xps(104), yPos - yps(25), xps(160), yPos - yps(3));
        ct1.addElement(new Phrase(Pdf.get(Pdf.SUBTOTAL) + ":", FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS)));
        if (showVAT(vatType)) {
            ct1.addElement(new Phrase("(" + Pdf.get(Pdf.EXCLUDING_VAT) + ")", FontProvider.font(FontStyle.ARIAL_8_ITALICS)));
        }
        ct1.go();
        ColumnText ct2 = new ColumnText(writer.getDirectContent());
        ct2.setSimpleColumn(xps(160), yPos - yps(25), PAGE_WIDTH - xps(12), yPos - yps(3));
        ct2.addElement(value);
        ct2.go();
    }

    /** vlozeni separatoru kolem prehleduVAT, vraci pozici spodni hrany oddelovace */
    private float addVATSeparator(float actual, float sepYpos, boolean bold) throws Exception {
        final float space = yps(2);
        document.add(drawSeparator(actual, PRODUCT_SEP_WIDTHS[4], sepYpos, bold));
        actual += space + PRODUCT_SEP_WIDTHS[4];
        document.add(drawSeparator(actual, PRODUCT_SEP_WIDTHS[5], sepYpos, bold));
        actual += space + PRODUCT_SEP_WIDTHS[5];
        document.add(drawSeparator(actual, PRODUCT_SEP_WIDTHS[6], sepYpos, bold));
        actual += space + PRODUCT_SEP_WIDTHS[6];
        return sepYpos;
    }

    /** vlozi prehled */
    private float addOverview(InvoiceProcessType vatType, EventPaymentProfile epp, float yPos) throws Exception {
        // poznamka vedle prehledu (pouziva se pro staty eu, ...)
        if (vatType.equals(InvoiceProcessType.VAT_EU)) {
            final String countryName = epp.getPaymentProfile().getActualRevision().getCountry().getName();
            final String noteText1 = String.format("The place of the supply of the service provided according to the Articles 9 and 10b of Czech VAT Act No. 235/2004 Coll., which is in line with the Council Directive 2006/112/ES, is %s.", countryName);
            final String noteText2 = "You are required to declare and pay the associated value-added tax (using the “reverse-charge” procedure) according to the Council Directive 2006/112/EC.";
            Phrase note1 = new Phrase(noteText1, FontProvider.font(FontStyle.ARIAL_8_ITALICS));
            Phrase note2 = new Phrase(noteText2, FontProvider.font(FontStyle.ARIAL_8_ITALICS));

            ColumnText ct1 = new ColumnText(writer.getDirectContent());
            ct1.setSimpleColumn(xps(12), yPos - yps(35), xps(90), yPos - yps(18));
            ct1.addElement(note1);
            ct1.go();

            ColumnText ct2 = new ColumnText(writer.getDirectContent());
            ct2.setSimpleColumn(xps(12), yPos - yps(57), xps(90), yPos - yps(37));
            ct2.addElement(note2);
            ct2.go();
        }

        // obsah
        if (showVAT(vatType)) {
            ColumnText ct3 = new ColumnText(writer.getDirectContent());
            ct3.setSimpleColumn(xps(104), yPos - yps(40), xps(160), yPos - yps(18));
            ct3.addElement(new Phrase(Pdf.get(Pdf.VAT_OVERVIEW) + ":", FontProvider.font(FontStyle.ARIAL_11_BOLD_ITALICS)));
            ct3.go();

            final float space = yps(2);
            final float startActual = yps(12)
                    + PRODUCT_SEP_WIDTHS[0]
                    + PRODUCT_SEP_WIDTHS[1]
                    + PRODUCT_SEP_WIDTHS[2]
                    + PRODUCT_SEP_WIDTHS[3]
                    + (space * 4);
            float startRowPosition = addVATSeparator(startActual, yPos - yps(30f), false);
            final float yPosDown = yPos - yps(29.5f);
            final float yPosUp = yPos - yps(18);
            float actual = startActual;

            Font fontBold = FontProvider.font(FontStyle.ARIAL_9_BOLD);
            Paragraph pt1 = new Paragraph(Pdf.get(Pdf.VAT_RATE) + ":", fontBold);
            pt1.setAlignment(Paragraph.ALIGN_CENTER);
            ColumnText ct4 = new ColumnText(writer.getDirectContent());
            ct4.setSimpleColumn(actual, yPosDown, actual += PRODUCT_SEP_WIDTHS[4], yPosUp);
            ct4.addElement(pt1);
            ct4.go();
            Paragraph pt2 = new Paragraph(Pdf.get(Pdf.VAT_BASE) + ":", fontBold);
            pt2.setAlignment(Paragraph.ALIGN_CENTER);
            ColumnText ct5 = new ColumnText(writer.getDirectContent());
            ct5.setSimpleColumn(actual += space, yPosDown, actual += PRODUCT_SEP_WIDTHS[5], yPosUp);
            ct5.addElement(pt2);
            ct5.go();
            Paragraph pt3 = new Paragraph(Pdf.get(Pdf.VAT_AMOUNT) + ":", fontBold);
            pt3.setAlignment(Paragraph.ALIGN_CENTER);
            ColumnText ct6 = new ColumnText(writer.getDirectContent());
            ct6.setSimpleColumn(actual += space, yPosDown, actual += PRODUCT_SEP_WIDTHS[6], yPosUp);
            ct6.addElement(pt3);
            ct6.go();

            float nextRowPos = startRowPosition;

            for (BigDecimal bd : record.getVatRates()) {
                InvoiceVatRecord vr = record.getVatTableRecord(bd);
                nextRowPos = addVATRowRecord(startActual, vr.getVatRate(), vr.getVatBase(), vr.getVatAmount(), nextRowPos);
                nextRowPos += yps(2);
            }
            nextRowPos -= yps(2);

            float endPosition = addVATSeparator(startActual, nextRowPos, true);

            Font fontBold10 = FontProvider.font(FontStyle.ARIAL_10_BOLD);
            ColumnText ct7 = new ColumnText(writer.getDirectContent());
            ct7.setSimpleColumn(startActual, endPosition - yps(25), PAGE_WIDTH - xps(12), endPosition - yps(3));
            ct7.addElement(new Phrase(Pdf.get(Pdf.VAT_CHARGED) + ":", fontBold10));
            ct7.go();
            ColumnText ct8 = new ColumnText(writer.getDirectContent());
            ct8.setSimpleColumn(xps(160), endPosition - yps(25), PAGE_WIDTH - xps(12), endPosition - yps(3));
            Paragraph pt4 = new Paragraph(Formats.assignCurrency(currency,
                    decimalFormat.format(record.getVatCharged())), fontBold10);
            pt4.setAlignment(Paragraph.ALIGN_RIGHT);
            ct8.addElement(pt4);
            ct8.go();

            return endPosition - yps(20);
        } else {
            return yPos - yps(20);
        }
    }

    private float addVATRowRecord(float actual, BigDecimal rate, BigDecimal base, BigDecimal amount, float yPos) throws Exception {
        final float space = xps(2);
        final float yPosDown = yPos;
        final float yPosUp = yPos - yps(7);
        Font fontRegular = FontProvider.font(FontStyle.ARIAL_9_REGULAR);
        Paragraph pt1 = new Paragraph(rate + PERCENT, fontRegular);
        pt1.setAlignment(Paragraph.ALIGN_CENTER);
        ColumnText ct1 = new ColumnText(writer.getDirectContent());
        ct1.setSimpleColumn(actual, yPosDown, actual += PRODUCT_SEP_WIDTHS[4], yPosUp);
        ct1.addElement(pt1);
        ct1.go();
        Paragraph pt2 = new Paragraph(Formats.assignCurrency(currency, decimalFormat.format(base)), fontRegular);
        pt2.setAlignment(Paragraph.ALIGN_RIGHT);
        ColumnText ct2 = new ColumnText(writer.getDirectContent());
        ct2.setSimpleColumn(actual += space, yPosDown, actual += PRODUCT_SEP_WIDTHS[5], yPosUp);
        ct2.addElement(pt2);
        ct2.go();
        Paragraph pt3 = new Paragraph(Formats.assignCurrency(currency, decimalFormat.format(amount)), fontRegular);
        pt3.setAlignment(Paragraph.ALIGN_RIGHT);
        ColumnText ct3 = new ColumnText(writer.getDirectContent());
        ct3.setSimpleColumn(actual += space, yPosDown, actual += PRODUCT_SEP_WIDTHS[6], yPosUp);
        ct3.addElement(pt3);
        ct3.go();
        return yPos - yps(8);
    }

    /** vlozi celkovou sumu */
    private float addTotalAmount(float yPos) throws Exception {
        Rectangle invoiceRect = new Rectangle(xps(98), yPos - yps(14), PAGE_WIDTH, yPos);
        invoiceRect.setBackgroundColor(GRAY_COLOR);
        document.add(invoiceRect);

        PdfPCell ta = new PdfPCell(new Phrase(Pdf.get(Pdf.TOTAL_AMOUNT) + ":", FontProvider.font(FontStyle.ARIAL_13_BOLD_ITALICS)));
        ta.setPadding(0f);
        ta.setBorder(PdfPCell.NO_BORDER);
        PdfPCell value = new PdfPCell(new Phrase(Formats.assignCurrency(currency,
                decimalFormat.format(record.getTotal())),
                FontProvider.font(FontStyle.ARIAL_13_BOLD)));
        value.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        value.setPadding(0f);
        value.setBorder(PdfPCell.NO_BORDER);

        PdfPTable table = new PdfPTable(2);
        table.setTotalWidth(new float[]{xps(40), xps(54)});
        table.addCell(ta);
        table.addCell(value);
        table.writeSelectedRows(0, -1, xps(104), yPos - yps(4.5f), writer.getDirectContent());

        return yPos - yps(14);
    }

    /** vypise ze je faktura uz zaplacena */
    private float addAlreadyPaid(float yPos) throws Exception {
        // Already paid
        PdfPCell ap = new PdfPCell(new Phrase(Pdf.get(Pdf.ALREADY_PAID) + ":", FontProvider.font(FontStyle.ARIAL_13_BOLD_ITALICS)));
        ap.setPadding(0f);
        ap.setBorder(PdfPCell.NO_BORDER);
        PdfPCell valueAP = new PdfPCell(new Phrase(Formats.assignCurrency(currency,
                decimalFormat.format(record.getTotal())),
                FontProvider.font(FontStyle.ARIAL_13_BOLD)));
        valueAP.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        valueAP.setPadding(0f);
        valueAP.setBorder(PdfPCell.NO_BORDER);

        PdfPTable tableAP = new PdfPTable(2);
        tableAP.setTotalWidth(new float[]{xps(40), xps(54)});
        tableAP.addCell(ap);
        tableAP.addCell(valueAP);
        tableAP.writeSelectedRows(0, -1, xps(104), yPos - yps(4.5f), writer.getDirectContent());

        yPos -= yps(14);

        // Total due
        Rectangle tdRect = new Rectangle(xps(98), yPos - yps(14), PAGE_WIDTH, yPos);
        tdRect.setBackgroundColor(GRAY_COLOR);
        document.add(tdRect);

        PdfPCell td = new PdfPCell(new Phrase(Pdf.get(Pdf.TOTAL_DUE) + ":", FontProvider.font(FontStyle.ARIAL_13_BOLD_ITALICS)));
        td.setPadding(0f);
        td.setBorder(PdfPCell.NO_BORDER);
        PdfPCell valueTD = new PdfPCell(new Phrase(Formats.assignCurrency(currency,
                decimalFormat.format(new BigDecimal(0))),
                FontProvider.font(FontStyle.ARIAL_13_BOLD)));
        valueTD.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        valueTD.setPadding(0f);
        valueTD.setBorder(PdfPCell.NO_BORDER);

        PdfPTable tableTD = new PdfPTable(2);
        tableTD.setTotalWidth(new float[]{xps(40), xps(54)});
        tableTD.addCell(td);
        tableTD.addCell(valueTD);
        tableTD.writeSelectedRows(0, -1, xps(104), yPos - yps(4.5f), writer.getDirectContent());

        return yPos - yps(14);
    }

    /** vypise poznamku dodavatele */
    private void addSupplierNote(String note, float yPos) throws Exception {
        Phrase noteP = new Phrase(note, FontProvider.font(FontStyle.ARIAL_8_ITALICS));
        ColumnText ct = new ColumnText(writer.getDirectContent());
        ct.setSimpleColumn(xps(12), yPos - yps(23), PAGE_WIDTH - xps(12), yPos - yps(5));
        ct.addElement(noteP);
        ct.go();
    }

    /** vlozi paticku */
    private void addFoot() throws Exception {
        // vlozeni maleho loga takeplace
        writer.getDirectContent().addTemplate(templateSmallTakeplace(), xps(87.5f), yps(4));
        // vlozeni maleho loga acemcee
        writer.getDirectContent().addTemplate(templateSmallAcemcee(), xps(144), yps(3.6f));
        Paragraph doNotPay = new Paragraph(Pdf.get(Pdf.FOOT_TEXT),
                FontProvider.font(FontStyle.ARIAL_8_REGULAR));
        doNotPay.setAlignment(Paragraph.ALIGN_CENTER);
        ColumnText ct = new ColumnText(writer.getDirectContent());
        ct.setSimpleColumn(0, 0, PAGE_WIDTH, yps(10));
        ct.addElement(doNotPay);
        ct.go();
    }

    /** vygeneruje pro forma fakturu */
    public void createPdfOrder(OutputStream os, Order order, boolean closeOutputStream) throws Exception {
        this.order = order;
        this.invoice = null;
        isProforma = true;

        generate(os, closeOutputStream);
    }

    /** vygeneruje fakturu */
    public void createPdfInvoice(OutputStream os, Invoice invoice, boolean closeOutputStream) throws Exception {
        this.order = invoice.getOrder();
        this.invoice = invoice;
        isProforma = false;

        generate(os, closeOutputStream);
    }

    // vygeneruje obsah podle nastavenych parametru
    private void generate(OutputStream os, boolean closeOutputStream) throws Exception {
        record = new InvoiceRecord(order);
        pages = 1;
        currency = order.getSupplier().getCurrency();
        document = new Document(PageSize.A4);
        writer = PdfWriter.getInstance(document, os);

        final InvoiceProcessType customerProcessType = ProfileUtils.resolveProcessType(order.getCustomer().getCountry());
        InvoiceProcessType resolvedVatType = PaymentUtils.resolveFinalProcessType(order.getSupplier().getVatType(), customerProcessType);

        if (order.getOrderType().equals(ProductCategory.TICKET_COMMISSION)) {
            resolvedVatType = InvoiceProcessType.VAT_CZ;
        }

        final InvoiceProcessType vatType = resolvedVatType;

        sepWidths = !vatType.equals(InvoiceProcessType.VAT_OT) && !vatType.equals(InvoiceProcessType.VAT_AD) ? PRODUCT_SEP_WIDTHS : PRODUCT_SEP_WIDTHS_OT;

        PageHelper ph = new PageHelper(vatType);
        writer.setPageEvent(ph);

        initNumberFormat();
        addMetadata(vatType);

        document.open();

        addSupplier(order.getSupplier(), vatType);
        addCustomer(order.getCustomer());

        addOrderNumberAndPurchaseDate(order.getOrderNumber(), order.getPurchaseDate());

        if (isProforma) {
            addDueDatePart(vatType, order.getDueDate(), order.getPurchaseDate(), null);
        } else {
            addDueDatePart(vatType, order.getDueDate(), invoice.getIssueDate(), invoice.getTaxableFulfillmentDate());
        }

        addPaymentDetails(order.getOrderNumber());
        addSeparatorOne();

        addInvoiceSaleAndDelivery();

        final int limit = !isProforma ? 115 : 95;

        float endProducts = addProducts(vatType);
        if (endProducts < yps(limit)) {
            // odstrankovani, protoze se nevleze prehled
            addSubtotal(vatType, endProducts);
            newPage();
            endProducts = yps(267);
        } else {
            addSubtotal(vatType, endProducts);
        }

        float endOverview = addOverview(vatType, order.getSupplier(), endProducts);
        float endTotalAmount = addTotalAmount(endOverview);
        float endAlreadyPaid = endTotalAmount;

        if (!isProforma) {
            endAlreadyPaid = addAlreadyPaid(endTotalAmount);
        }

        if (!StringUtils.isEmpty(order.getSupplier().getInvoiceNote())) {
            if (endAlreadyPaid < yps(33)) {
                newPage();
                endAlreadyPaid = yps(267);
            }
            addSupplierNote(order.getSupplier().getInvoiceNote(), endAlreadyPaid);
        }

        if (closeOutputStream) {
            writer.flush();
            document.close();
        }
    }

    /** vytvori novou stranku */
    private void newPage() throws Exception {
        pages++;
        document.newPage();
    }

    /** nastavi formatovani cisel */
    private void initNumberFormat() {
        decimalFormat = Formats.decimalFormat(currency);
    }

    /** nacteni velkeho loga */
    private PdfTemplate templateBigTakeplace() throws Exception {
        PdfImportedPage page = writer.getImportedPage(new PdfReader(
                this.getClass().getResourceAsStream("/pdf/takeplace-big.pdf")), 1);
        return page;
    }

    /** nacteni maleho loga */
    private PdfTemplate templateSmallTakeplace() throws Exception {
        PdfImportedPage page = writer.getImportedPage(new PdfReader(
                this.getClass().getResourceAsStream("/pdf/takeplace-small.pdf")), 1);
        return page;
    }

    /** nacteni maleho loga acemcee */
    private PdfTemplate templateSmallAcemcee() throws Exception {
        PdfImportedPage page = writer.getImportedPage(new PdfReader(
                this.getClass().getResourceAsStream("/pdf/acemcee-small.pdf")), 1);
        return page;
    }

    /** helper pro spravu stranky a pomoc pri psani poctu stranek */
    private class PageHelper extends PdfPageEventHelper {

        // komponenty
        private PdfTemplate template;
        // data
        private int page;
        private InvoiceProcessType vatType;

        public PageHelper(InvoiceProcessType vatType) {
            this.vatType = vatType;
            page = 1;
        }

        @Override
        public void onOpenDocument(PdfWriter writer, Document document) {
            super.onOpenDocument(writer, document);
            template = writer.getDirectContent().createTemplate(xps(10), yps(10));
        }

        @Override
        public void onCloseDocument(PdfWriter writer, Document document) {
            super.onCloseDocument(writer, document);
            try {
                template.beginText();
                FontProvider.changeFont(FontStyle.ARIAL_8_REGULAR, template);
                template.showText(String.valueOf(writer.getPageNumber() - 1));
                template.endText();
            } catch (Exception ex) {
                throw new IllegalStateException("Problem input total page number");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            super.onStartPage(writer, document);
            try {
                // TODO: proc se vola jeste o stranku navic? -> pak vyhazuje chybu
                if (page <= pages) {
                    addHead(vatType);
                    addFoot();
                    page++;
                }
            } catch (Exception ex) {
                throw new IllegalStateException("Bad number count");
            }
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            super.onEndPage(writer, document);
            try {
                Paragraph doNotPay = new Paragraph(Pdf.get(Pdf.PAGE) + " " + writer.getPageNumber() + " "
                        + Pdf.get(Pdf.OF) + " ", FontProvider.font(FontStyle.ARIAL_8_REGULAR));
                doNotPay.setAlignment(Paragraph.ALIGN_RIGHT);
                ColumnText ct = new ColumnText(writer.getDirectContent());
                ct.setSimpleColumn(0, 0, xps(24.25f), yps(10));
                ct.addElement(doNotPay);
                ct.go();
                writer.getDirectContent().addTemplate(template, xps(25.5f), yps(5.8f));
            } catch (Exception ex) {
                throw new IllegalStateException("Problem input page number");
            }
        }
    }

    /** vykresli caru oddelovace */
    public static Rectangle drawSeparator(float from, float width, float sepYpos, boolean bold) {
        float sepYdown = sepYpos;
        float sepYup = sepYpos + (bold ? yps(.8f) : yps(.4f));
        Rectangle separator = new Rectangle(from, sepYdown, from + width, sepYup);
        separator.setBackgroundColor(BaseColor.BLACK);
        return separator;
    }

    /** vraci textovou reprezentaci typu platby */
    public static String paymentMethod(ConfirmationChannel type) {
        switch (type) {
            case BANK_TRANSFER:
                return "Bank Transfer";
            case ONLINE_PAY_PAL:
            default:
                return "Online";
        }
    }

    public void setAcemceeService(AcemceeService acemceeService) {
        this.acemceeService = acemceeService;
    }

    /** protestovaci ucely */
    public static void main(String[] args) {
        Invoice invoice = new Invoice();
        invoice.setInvoiceNumber("1200010002");
        invoice.setIssueDate(DateProvider.getCurrentDate());
        invoice.setConfirmationSource(ConfirmationChannel.BANK_TRANSFER);
        invoice.setTaxableFulfillmentDate(DateUtils.addDays(DateProvider.getCurrentDate(), 14));

        Order order = new Order();
        order.setOrderType(ProductCategory.TICKET);
        order.setState(OrderState.ACTIVE);
        order.setOrderNumber("100000004");
        order.setDueDate(DateUtils.addDays(DateProvider.getCurrentDate(), 14));
        order.setConfirmedDate(DateUtils.addDays(DateProvider.getCurrentDate(), 4));
        order.setPurchaseDate(new Date());

        invoice.setOrder(order);

        ArrayList<OrderItem> items = new ArrayList<OrderItem>();
        for (int i = 1; i <= 2; i++) {
            OrderItem ii = new OrderItem();
            ii.setName("Řádný účastník s účastí na společenském večeru " + (i % 3 == 0 ? "trochu delší text navíc " + i : i));
            //ii.setName("Záznam");
            if (i % 2 == 0) {
                ii.setQuantity(1);
            } else {
                ii.setQuantity(2);
            }
            Product p = new Product();
            p.setCategory(ProductCategory.TICKET);
            ii.setProduct(p);
            ii.setUnitPrice(new BigDecimal(9999));
            ii.setDiscount(5);
            if (i % 4 == 0) {
                ii.setVatRate(new BigDecimal(15));
            } else if (i % 3 == 0) {
                ii.setVatRate(new BigDecimal(3));
            } else {
                ii.setVatRate(new BigDecimal(20));
            }
            items.add(ii);
        }
        order.setItems(items);

        PaymentProfileRevision client = new PaymentProfileRevision();
        client.setAccountBankSwift("swift");
        client.setAccountIban("iban");
        client.setCity("Brno");
        client.setCountry(CountryEnum.CZECH_REPUBLIC);
        client.setName("Jan Novák");
        client.setRegNo("");
        client.setRevisionDate(new Date());
        client.setStreet("Česká 23");
        client.setVatNo("");
        client.setZip("60200");

        PaymentProfileRevision acemcee = new PaymentProfileRevision();
        acemcee.setAccountBankSwift(SystemProperties.CZK_ACMC_BANK_SWIFT);
        acemcee.setAccountIban(SystemProperties.CZK_ACMC_BANK_IBAN);
        acemcee.setCity(SystemProperties.CZK_ACMC_CITY);
        acemcee.setCountry(SystemProperties.CZK_ACMC_COUNTRY);
        acemcee.setName(SystemProperties.CZK_ACMC_NAME);
        acemcee.setRegNo(SystemProperties.CZK_ACMC_ICO_NO);
        acemcee.setRevisionDate(new Date());
        acemcee.setStreet(SystemProperties.CZK_ACMC_STREET);
        acemcee.setVatNo(SystemProperties.CZK_ACMC_DIC_NO);
        acemcee.setZip(SystemProperties.CZK_ACMC_ZIP);

        PaymentProfileRevision supplier = new PaymentProfileRevision();
        supplier.setAccountBankSwift("BACXCZPP :-)");
        supplier.setAccountIban("CZ34 2700 0000 0021 0629 6953");
        supplier.setCity("Brno");
        supplier.setCountry(CountryEnum.CZECH_REPUBLIC);
        supplier.setName("Acemcee");
        supplier.setRegNo("29208602");
        supplier.setRevisionDate(new Date());
        supplier.setStreet("U Vodárny 3032 / 2a");
        supplier.setVatNo("CZ29208602");
        supplier.setZip("616 00");

        String noteText = "";
        for (int i = 1; i <= 128; i++) {
            if (i < 10) {
                noteText += " --" + i;
            } else if (i < 100) {
                noteText += " -" + i;
            } else {
                noteText += " " + i;
            }
        }

        PaymentProfile pp = new PaymentProfile();
        pp.setActualRevision(supplier);

        EventPaymentProfile supplierEventProfile = new EventPaymentProfile();
        supplierEventProfile.setPaymentProfile(pp);
        supplierEventProfile.setCurrency(Currency.CZK);
        supplierEventProfile.setVatType(InvoiceProcessType.VAT_AD);
        supplierEventProfile.setInvoiceNote(noteText);
        supplierEventProfile.setOrderBankTransferInvoicedToThisProfile(true);

        order.setCustomer(client);
        order.setSupplier(supplierEventProfile);

        try {
            PdfInvoiceGenerator gen = new PdfInvoiceGenerator();
            boolean proforma = false;

            if (SystemUtils.IS_OS_LINUX) {
                if (proforma) {
                    gen.createPdfOrder(new FileOutputStream(new File("/tmp/fss-proforma.pdf")), order, true);
                } else {
                    gen.createPdfInvoice(new FileOutputStream(new File("/tmp/fss.pdf")), invoice, true);
                }
            } else if (SystemUtils.IS_OS_WINDOWS) {
                if (proforma) {
                    gen.createPdfOrder(new FileOutputStream(new File("c:\\data\\out-proforma.pdf")), order, true);
                } else {
                    gen.createPdfInvoice(new FileOutputStream(new File("c:\\data\\out.pdf")), invoice, true);
                }
            } else if (SystemUtils.IS_OS_MAC_OSX) {
                if (proforma) {
                    gen.createPdfOrder(new FileOutputStream(new File("/Users/palo/Acemcee/fss-proforma.pdf")), order, true);
                } else {
                    gen.createPdfInvoice(new FileOutputStream(new File("/Users/palo/Acemcee/fss.pdf")), invoice, true);
                }
            }
        } catch (Exception ex) {
            System.err.println("Chyba " + ex.getLocalizedMessage());
        }
    }
}
