package consys.payment.channel.paypal.ipn.enums;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum PPCurrency {

    AUD,
    //          Australian Dollar
    BRL,
    //          Brazilian Real This currency is supported as a payment currency and a currency balance for in-country PayPal accounts only.
    CAD,
    //          Canadian Dollar
    CHF,
    //          Swiss Franc
    CZK,
    //          Czech Koruna
    DKK,
    //          Danish Krone
    EUR,
    //          Euro
    GBP,
    //          Pound Sterling
    HKD,
    //          Hong Kong Dollar
    HUF,
    //          Hungarian Forint
    ILS,
    //          Israeli New Sheqel
    JPY,
    //          Japanese Yen
    MXN,
    //          Mexican Peso
    MYR,
    //          Malaysian Ringgit This currency is supported as a payment currency and a currency balance for in-country PayPal accounts only.
    NOK,
    //          Norwegian Krone
    NZD,
    //          New Zealand Dollar
    PHP,
    //          Philippine Peso
    PLN,
    //          Polish Zloty
    SEK,
    //          Swedish Krona
    SGD,
    //          Singapore Dollar
    THB,
    //          Thai Baht
    TWD,
    //          Taiwan New Dollar
    USD
//          U.S.
}
