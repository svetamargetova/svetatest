package consys.payment.dao;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface AutoinstallDao {

    public boolean installScript(String script);

    

}
