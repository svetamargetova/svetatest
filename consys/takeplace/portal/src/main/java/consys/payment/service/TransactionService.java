package consys.payment.service;

import consys.payment.bo.Transaction;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.channel.bank.BankTransaction;
import consys.payment.channel.paypal.ipn.PaypalTransaction;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public interface  TransactionService {
        
    
    public void createBankTransactions(List<BankTransaction> bankTransactions) 
            throws NoRecordException, RequiredPropertyException;
    
    public void createPaypalTransaction(PaypalTransaction paypalTransaction) 
            throws RequiredPropertyException;
    
    /**
     * Vytvori transakciu a pokusi sa z nej ziskat cislo objednakvy pripadne
     * uuid.
     * @param transaction nova transakcia
     * @throws RequiredPropertyException 
     */
    public void createTransaction(Transaction transaction) 
            throws RequiredPropertyException;
    
    /**
     * Vytvori transakcie a pokusi sa z nich ziskat cislo objednavky pripadne
     * uuid
     * @param transaction nova tranaskcia
     * @throws RequiredPropertyException 
     */
    public void createTransactions(List<Transaction> transaction) 
            throws RequiredPropertyException;
    
    
    public Transaction loadTransactionByUuid(String uuid) 
            throws NoRecordException;
    
    public Transaction loadTransactionByTransactionId(String transactionId) 
            throws NoRecordException;
    
    public List<Transaction> listTransactions(ConfirmationChannel channel, Date from, Date to, int startItem, int itemsPerPage);
    
    public int listTransactionsCount(ConfirmationChannel channel, Date from, Date to); 

    void refundedPaypalTransaction(PaypalTransaction paypalTransaction) throws RequiredPropertyException;
}
