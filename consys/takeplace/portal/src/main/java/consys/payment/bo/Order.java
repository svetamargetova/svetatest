package consys.payment.bo;

import consys.payment.bo.enums.OrderState;
import consys.payment.bo.enums.ProductCategory;
import consys.common.utils.BigDecimalUtils;
import consys.payment.utils.PaymentUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 * Entita reprezentuje jednu objednavku.
 * 
 * @author palo
 */
public class Order implements BusinessObject {

    private static final long serialVersionUID = 195957604103179206L;
    // data
    private Long id;
    /** Stav objednavky */
    private OrderState state;
    /** UUID faktury */
    private String uuid;
    /** Cislo objednavky */
    private String orderNumber;
    /** Event payment profil */
    private EventPaymentProfile supplier;
    /** Odebaratel - profil */
    private PaymentProfileRevision customer;
    /** Polozky objednavky */
    private List<OrderItem> items;
    /** Typ objednavky */
    private ProductCategory orderType;
    /** Datum vytvorenie objednavky */
    private Date purchaseDate;
    /** Datum dokedy musi byt objednavka splatena */
    private Date dueDate;
    /** Datum kedy bola oznacena objednavka za potvrdenu. Ak budeme generovat fakturu tak sa tento datum rovna s issue date */
    private Date confirmedDate;
    /** Celkova hodnota objednavky */
    private BigDecimal price;
    /** Jednotkovy poplatok za objednavku */
    private BigDecimal unitFee;
    /** Percentualny poplatok za objednavku */
    private BigDecimal shareFee;
    /** Poznamka */
    private String note;
    /** Faktura - ak bola vygenerovana */
    private Invoice invoice;

    public boolean isActive() {
        return getState() == OrderState.ACTIVE;
    }

    public boolean isConfirmed() {
        return getState() == OrderState.CONFIRMED_ORGANIZATOR
                || getState() == OrderState.CONFIRMED_SYSTEM
                || getState() == OrderState.CONFIRMED_ZERO;

    }

    public boolean isCanceled() {
        return getState() == OrderState.CANCELED_ORGANIZATOR
                || getState() == OrderState.CANCELED_OWNER
                || getState() == OrderState.CANCELED_SYSTEM;
    }

    public boolean isZeroConfirmed() {
        return getState() == OrderState.CONFIRMED_ZERO;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Stav objednavky
     * @return the state
     */
    public OrderState getState() {
        return state;
    }

    /**
     * Stav objednavky
     * @param state the state to set
     */
    public void setState(OrderState state) {
        this.state = state;
    }

    /**
     * UUID faktury
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID faktury
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Cislo objednavky
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Cislo objednavky
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * Event payment profil
     * @return the supplier
     */
    public EventPaymentProfile getSupplier() {
        return supplier;
    }

    /**
     * Event payment profil
     * @param supplier the supplier to set
     */
    public void setSupplier(EventPaymentProfile supplier) {
        this.supplier = supplier;
    }

    /**
     * Odebaratel - profil
     * @return the customer
     */
    public PaymentProfileRevision getCustomer() {
        return customer;
    }

    /**
     * Odebaratel - profil
     * @param customer the customer to set
     */
    public void setCustomer(PaymentProfileRevision customer) {
        this.customer = customer;
    }

    /**
     * Polozky objednavky
     * @return the items
     */
    public List<OrderItem> getItems() {
        return items;
    }

    /**
     * Polozky objednavky
     * @param items the items to set
     */
    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    /**
     * Typ objednavky
     * @return the orderType
     */
    public ProductCategory getOrderType() {
        return orderType;
    }

    /**
     * Typ objednavky
     * @param orderType the orderType to set
     */
    public void setOrderType(ProductCategory orderType) {
        this.orderType = orderType;
    }

    /**
     * Datum vytvorenie objednavky
     * @return the purchaseDate
     */
    public Date getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * Datum vytvorenie objednavky
     * @param purchaseDate the purchaseDate to set
     */
    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    /**
     * Datum dokedy musi byt objednavka splatena
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Datum dokedy musi byt objednavka splatena
     * @param dueDate the dueDate to set
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Celkova hodnota objednavky
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Celkova hodnota objednavky
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Jednotkovy poplatok za objednavku
     * @return the unitFee
     */
    public BigDecimal getUnitFee() {
        return unitFee;
    }

    public void addToUnitFee(BigDecimal value) {
        setUnitFee(unitFee.add(value).setScale(4, RoundingMode.HALF_UP));
    }

    /**
     * Jednotkovy poplatok za objednavku
     * @param unitFee the unitFee to set
     */
    public void setUnitFee(BigDecimal unitFee) {
        this.unitFee = unitFee;
    }

    /**
     * Percentualny poplatok za objednavku
     * @return the shareFee
     */
    public BigDecimal getShareFee() {
        return shareFee;
    }

    public void addToShareFee(BigDecimal value) {
        setShareFee(shareFee.add(value).setScale(4, RoundingMode.HALF_UP));
    }

    /**
     * Percentualny poplatok za objednavku
     * @param shareFee the shareFee to set
     */
    public void setShareFee(BigDecimal shareFee) {
        this.shareFee = shareFee;
    }

    public BigDecimal getThirdPartyFee() {
        if (invoice != null && invoice.getTransaction() != null) {
            return invoice.getTransaction().getFees();
        } else {
            return BigDecimal.ZERO;
        }
    }

    /**
     * Vrati <code>unitFees + shareFee</code>
     * @return
     */
    public BigDecimal getFees() {
        return unitFee.add(shareFee, BigDecimalUtils.DEFAULT);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Order e = (Order) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Order[uuid=%s orderNumber=%s type=%s state=%s price=%s]", uuid, orderNumber, orderType, state, price.toPlainString());
    }

    /**
     * Poznamka
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * Poznamka
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    public void appendToNote(String note) {
        if (StringUtils.isBlank(note)) {
            return;
        }
        setNote(PaymentUtils.craeteNote(this.note, note));
    }

    /**
     * Datum kedy bola oznacena objednavka za potvrdenu. Ak budeme generovat fakturu tak sa tento datum rovna s issue date
     * @return the confirmedDate
     */
    public Date getConfirmedDate() {
        return confirmedDate;
    }

    /**
     * Datum kedy bola oznacena objednavka za potvrdenu. Ak budeme generovat fakturu tak sa tento datum rovna s issue date
     * @param confirmedDate the confirmedDate to set
     */
    public void setConfirmedDate(Date confirmedDate) {
        this.confirmedDate = confirmedDate;
    }

    /**
     * Faktura - ak bola vygenerovana
     * @return the invoice
     */
    public Invoice getInvoice() {
        return invoice;
    }

    /**
     * Faktura - ak bola vygenerovana
     * @param invoice the invoice to set
     */
    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
