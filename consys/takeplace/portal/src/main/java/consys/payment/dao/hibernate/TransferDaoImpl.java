package consys.payment.dao.hibernate;

import consys.payment.bo.Transfer;
import consys.payment.bo.enums.TransferState;
import consys.payment.dao.TransferDao;
import consys.payment.service.exception.NoRecordException;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TransferDaoImpl extends BusinessObjectDaoImpl<Transfer> implements TransferDao {

    @Override
    public Transfer loadActualTransfer(String eventUuid) throws NoRecordException {
        Transfer t = (Transfer) session().
                createQuery("select t from Transfer t where t.eventPaymentProfile.uuid=:UUID and t.state=:STATE").
                setString("UUID", eventUuid).
                setParameter("STATE", TransferState.ACTIVE).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Active transfer waiting for event " + eventUuid);
        }
        return t;
    }

    @Override
    public Transfer loadActualTransferWithEvent(String eventUuid) throws NoRecordException {
        Transfer t = (Transfer) session().
                createQuery("select t from Transfer t join fetch t.eventPaymentProfile where t.eventPaymentProfile.uuid=:UUID and t.state=:STATE").
                setString("UUID", eventUuid).
                setParameter("STATE", TransferState.ACTIVE).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Active transfer waiting for event profile " + eventUuid);
        }
        return t;
    }

    @Override
    public Transfer loadActualTransferWithRowLock(String eventUuid) throws NoRecordException {
        Transfer t = (Transfer) session().
                createQuery("select t from Transfer t where t.eventPaymentProfile.uuid=:UUID  and t.state=:STATE").
                setParameter("STATE", TransferState.ACTIVE).
                setString("UUID", eventUuid).
                setLockMode("t", LockMode.PESSIMISTIC_WRITE).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Active transfer waiting for event " + eventUuid);
        }
        return t;
    }

    @Override
    public Transfer loadTransferDetailWithUuid(String uuid) throws NoRecordException {
        Transfer t = (Transfer) session().
                createQuery("select t from Transfer t join fetch t.eventPaymentProfile left join fetch t.commissionOrder where t.eventPaymentProfile.uuid=:UUID and t.state=:STATE").
                setString("UUID", uuid).
                setParameter("STATE", TransferState.ACTIVE).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Transfer with uuid" + uuid);
        }
        return t;
    }

    @Override
    public Transfer loadPreparedTransferDetailForEvent(String transferUuid) throws NoRecordException {
        Transfer t = (Transfer) session().
                createQuery("select t from Transfer t LEFT JOIN FETCH t.commissionOrder as o LEFT JOIN FETCH o.items "
                + "where t.uuid=:UUID and t.state=2").
                setString("UUID", transferUuid).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Prepared transfer with uuid " + transferUuid);
        }
        return t;
    }

    @Override
    public Transfer loadTransferWithCommissionDetail(String transferUuid) throws NoRecordException {
        Query query = session().createQuery("from Transfer t LEFT JOIN FETCH t.commissionOrder as o LEFT JOIN FETCH o.items "
                + "LEFT JOIN FETCH o.invoice WHERE t.uuid=:UUID");
        query.setString("UUID", transferUuid);
        Transfer t = (Transfer) query.uniqueResult();
        if (t == null) {
            throw new NoRecordException("None transfer witch uuid " + transferUuid);
        }
        return t;
    }

    @Override
    public Transfer loadTransferWithOrders(String transferUuid) throws NoRecordException {
        Transfer t = (Transfer) session().
                createQuery("select t from Transfer t LEFT JOIN FETCH t.orders as i LEFT JOIN FETCH i.customer "
                + "LEFT JOIN FETCH i.invoice LEFT JOIN FETCH t.eventPaymentProfile as epp LEFT JOIN FETCH t.commissionOrder where t.uuid=:T_UUID").
                setString("T_UUID", transferUuid).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Transfer with uuid" + transferUuid);
        }
        return t;
    }

    @Override
    public Transfer loadTransferWithOrdersDetailed(String transferUuid) throws NoRecordException {
        Transfer t = (Transfer) session().
                createQuery("select t from Transfer t LEFT JOIN FETCH t.orders as i LEFT JOIN FETCH i.customer "
                + "LEFT JOIN FETCH i.invoice LEFT JOIN FETCH t.eventPaymentProfile as epp LEFT JOIN FETCH t.commissionOrder "
                + "LEFT JOIN FETCH i.items itms LEFT JOIN FETCH itms.product "
                + "where t.uuid=:T_UUID").
                setString("T_UUID", transferUuid).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Transfer with uuid" + transferUuid);
        }
        return t;
    }

    @Override
    public Transfer loadPlainTransfer(String transferUuid, boolean toUpdate) throws NoRecordException {
        Query q = session().
                createQuery("select t from Transfer t where t.uuid=:T_UUID").
                setString("T_UUID", transferUuid);
        if (toUpdate) {
            q.setLockMode("t", LockMode.PESSIMISTIC_WRITE);
        }
        Transfer t = (Transfer) q.uniqueResult();
        if (t == null) {
            throw new NoRecordException("No Transfer with uuid" + transferUuid);
        }
        return t;
    }

    @Override
    public List<Transfer> listTransfersByEventPaymentProfile(String eventPaymentProfileUuid, int firstResult, int itemsPerPage) {
        Criteria c = prepareListCriteria(eventPaymentProfileUuid, null);
        c.setFirstResult(firstResult);
        c.setMaxResults(itemsPerPage);
        return c.list();
    }

    @Override
    public List<Transfer> listTransfersByEventPaymentProfile(String eventUuid, TransferState state, int firstResult, int itemsPerPage) {
        Criteria c = prepareListCriteria(eventUuid, state);
        c.setFirstResult(firstResult);
        c.setMaxResults(itemsPerPage);
        return c.list();
    }

    @Override
    public List<Transfer> listTransfersByEventUuid(String eventUuid, int firstResult, int itemsPerPage) {
        Criteria c = session().createCriteria(Transfer.class);

        if (StringUtils.isNotBlank(eventUuid)) {
            c.createAlias("eventPaymentProfile.event", "event").add(Restrictions.eq("event.uuid", eventUuid));
        }

        c.setFetchMode("eventPaymentProfile", FetchMode.JOIN);
        c.setFirstResult(firstResult);
        c.setMaxResults(itemsPerPage);
        return c.list();
    }

    @Override
    public List<Transfer> listTransfers(TransferState state, int firstResult, int itemsPerPage) {
        Criteria c = prepareListCriteria(null, state);
        c.setFirstResult(firstResult);
        c.setMaxResults(itemsPerPage);
        return c.list();
    }

    @Override
    public int loadTransfersCount(String eventProfileUuid) {
        Criteria c = prepareListCriteria(eventProfileUuid, null);
        c.setProjection(Projections.rowCount());
        return ((Long) c.uniqueResult()).intValue();
    }

    @Override
    public int loadTransfersCount(String eventProfileUuid, TransferState inState) {
        Criteria c = prepareListCriteria(eventProfileUuid, inState);
        c.setProjection(Projections.rowCount());
        return ((Long) c.uniqueResult()).intValue();
    }

    @Override
    public int loadTransfersCount(TransferState inState) {
        Criteria c = prepareListCriteria(null, inState);
        c.setProjection(Projections.rowCount());
        return ((Long) c.uniqueResult()).intValue();
    }

    @Override
    public int loadTransfersCountByEventUuid(String eventUuid) {
        Criteria c1 = session().createCriteria(Transfer.class).setProjection(Projections.rowCount());
        Criteria c2 = c1.createCriteria("eventPaymentProfile", "epp");
        Criteria c3 = c2.createCriteria("epp.event", "event");
        c3.add(Restrictions.eq("event.uuid", eventUuid));
        return ((Long) c3.uniqueResult()).intValue();
    }

    private Criteria prepareListCriteria(String eventProfileUuid, TransferState inState) {
        Criteria c = session().createCriteria(Transfer.class);

        if (inState != null) {
            c.add(Restrictions.eq("state", inState));
        }

        if (StringUtils.isNotBlank(eventProfileUuid)) {
            c.createAlias("eventPaymentProfile", "epp").
                    add(Restrictions.eq("epp.uuid", eventProfileUuid));
        }

        c.setFetchMode("eventPaymentProfile", FetchMode.JOIN);
        return c;
    }
}
