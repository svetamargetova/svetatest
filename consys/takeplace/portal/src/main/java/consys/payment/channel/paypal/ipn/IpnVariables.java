package consys.payment.channel.paypal.ipn;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface IpnVariables {


    public static final String test_ipn = "test_ipn";
    public static final String verify_sign = "verify_sign";
    /* ------------ Buyer Information ----------------------------------------*/
    public static final String address_city = "address_city";
    public static final String address_country = "address_country";
    public static final String address_country_code = "address_country_code";
    public static final String address_state = "address_state";
    public static final String address_status = "address_status";
    public static final String address_streetv = "address_streetv";
    public static final String address_zip = "address_zip";
    public static final String first_name = "first_name";
    public static final String last_name = "last_name";
    public static final String payer_business_name = "payer_business_name";
    public static final String payer_email = "payer_email";
    public static final String payer_id = "payer_id";
    public static final String payer_status = "payer_status";
    public static final String contact_phone = "contact_phone";
    public static final String residence_country = "residence_country";
    /* ------------ Basic Information ----------------------------------------*/
    public static final String business = "business";
    public static final String item_name = "item_name";
    public static final String item_number = "item_number";
    public static final String quantity = "quantity";
    public static final String receiver_email = "receiver_email";
    public static final String receiver_id = "receiver_id";
    /* ------------ Advanced and Custom Information --------------------------*/
    public static final String custom = "custom";
    public static final String invoice = "invoice";
    public static final String memo = "memo";
    public static final String tax = "tax";
    /* ------------ Website Payments Standard --------------------------*/
    /* Note: Bez Shopping Cart properties */
    public static final String payment_date = "payment_date";
    public static final String payment_status = "payment_status";
    public static final String payment_type = "payment_type";
    public static final String pending_reason = "pending_reason";
    public static final String reason_code = "reason_code";
    public static final String txn_id = "txn_id";
    public static final String txn_type = "txn_type";
    public static final String exchange_rate = "exchange_rate";
    public static final String mc_currency = "mc_currency";
    public static final String mc_fee = "mc_fee";
    /**
     * Full amount of the customer's payment, before transaction fee is subtracted.
     */
    public static final String mc_gross = "mc_gross";


    


}
