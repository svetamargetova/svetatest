package consys.payment.utils;

import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.Currency;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.utils.validator.IBANValidator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProfileUtils {

    private static final Logger logger = LoggerFactory.getLogger(ProfileUtils.class);

    public static void validateProfile(PaymentProfileRevision rev, boolean event)
            throws RequiredPropertyException, IbanFormatException {

        if (StringUtils.isBlank(rev.getCity())) {
            throw new RequiredPropertyException("City is missing");
        }
        if (rev.getCountry() == null) {
            throw new RequiredPropertyException("Country is missing");
        }
        if (StringUtils.isBlank(rev.getName())) {
            throw new RequiredPropertyException("Name is missing");
        }
        if (StringUtils.isBlank(rev.getStreet())) {
            throw new RequiredPropertyException("Street is missing");
        }
        if (StringUtils.isBlank(rev.getZip())) {
            throw new RequiredPropertyException("ZIP is missing");
        }

        if (event
                && (StringUtils.isBlank(rev.getAccountIban())
                || StringUtils.isBlank(rev.getAccountBankSwift()))) {
            throw new RequiredPropertyException("PaymentProfile is missing Account number and Swift code");
        }
        if (StringUtils.isNotBlank(rev.getAccountIban()) && !IBANValidator.isValid(rev.getAccountIban())) {
            throw new IbanFormatException("IBAN account number is in bad format!");
        }
    }

    public static String trimAllWhitespace(String str) {
        if (StringUtils.isBlank(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        int index = 0;
        while (sb.length() > index) {
            if (Character.isWhitespace(sb.charAt(index))) {
                sb.deleteCharAt(index);
            } else {
                index++;
            }
        }
        return sb.toString();
    }

    /** Urci typ zpracovani podle zeme */
    public static InvoiceProcessType resolveProcessType(CountryEnum country) {
        if (country == null) {
            logger.warn("CountryEnum is null");
            return null;
        }
        switch (country) {
            case CZECH_REPUBLIC:
                return InvoiceProcessType.VAT_CZ;
            case AUSTRIA:
            case BELGIUM:
            case BULGARIA:
            case CYPRUS:
            case DENMARK:
            case ESTONIA:
            case FINLAND:
            case FRANCE:
            case GERMANY:
            case GREECE:
            case HUNGARY:
            case IRELAND:
            case ITALY:
            case LATVIA:
            case LITHUANIA:
            case LUXEMBOURG:
            case MALTA:
            case NETHERLANDS:
            case POLAND:
            case PORTUGAL:
            case ROMANIA:
            case SLOVAKIA:
            case SLOVENIA:
            case SPAIN:
            case SWEDEN:
            case UNITED_KINGDOM:
                return InvoiceProcessType.VAT_EU;
            default:
                return InvoiceProcessType.VAT_OT;
        }
    }

    /** Urci menu podle zeme */
    public static Currency resolveCurrency(CountryEnum country) {
        switch (country) {
            case CZECH_REPUBLIC:
                return Currency.CZK;
            case AUSTRIA:
            case BELGIUM:
            case BULGARIA:
            case CYPRUS:
            case DENMARK:
            case ESTONIA:
            case FINLAND:
            case FRANCE:
            case GERMANY:
            case GREECE:
            case HUNGARY:
            case IRELAND:
            case ITALY:
            case LATVIA:
            case LITHUANIA:
            case LUXEMBOURG:
            case MALTA:
            case NETHERLANDS:
            case POLAND:
            case PORTUGAL:
            case ROMANIA:
            case SLOVAKIA:
            case SLOVENIA:
            case SPAIN:
            case SWEDEN:
            case UNITED_KINGDOM:
                return Currency.EUR;
            default:
                return Currency.USD;
        }
    }
}
