package consys.payment.service;

import consys.payment.bo.Order;
import consys.payment.bo.Transfer;
import consys.payment.bo.enums.TransferState;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.PaymentException;
import consys.payment.service.exception.RequiredPropertyException;
import java.util.List;

/**
 * Sluzba spravujuca transfere - entity ktorych ucelom je vytvorit rozdielove
 * hodnoty prestavujuce peniaze prevedene spat organizatorovy a ktore zostanu u nas.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface TransferService {

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita vsetky transfere v danom stave strankovane
     * @param inState
     * @param startItem
     * @param itemsPerPage
     * @return
     */
    public List<Transfer> listTransfers(TransferState inState, int startItem, int itemsPerPage);

    public int loadTransfersCount(TransferState inState);

    /**
     * Nacita vsetky transfere v danom stave pro konkretny event
     *
     * @param eventProfileUuid event profil
     * @param inState ziadany stav
     * @param startItem zaciatok item stranky
     * @param itemsPerPage pocet items na stranku
     * @return
     */
    public List<Transfer> listTransfers(String eventProfileUuid, TransferState inState, int startItem, int itemsPerPage);

    public int loadTransfersCount(String eventProfileUuid, TransferState inState);

    /**
     * Nacita vsetky transfere bez ohladu na stav
     * 
     * @param eventProfileUuid event profil
     * @param startItem zaciatok item stranky
     * @param itemsPerPage pocet items na stranku
     * @return
     */
    public List<Transfer> listTransfers(String eventProfileUuid, int startItem, int itemsPerPage);

    public int loadTransfersCount(String eventProfileUuid);

    public List<Transfer> listTransfersByEventUuid(String eventUuid, int startItem, int itemsPerPage);

    public int loadTransfersCountByEventUuid(String eventUuid);

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita transfer ktory je pre dany event prave aktualny. Je mozne ze tento
     * transfer bude vytvoreny ak neexistuje. Pozor tato metoda nastavi ROW_LOCK
     * takze nebude mozne updatovat trasnfer. Je to z dovodu konkurentneho pristupu
     * k sumam[total, fees, commission] ktore sa neustale pripocitavaju.
     * <p>
     * V pripade ze je potreba nacitat len transfer tak staci zavolat
     * {@link TransferService#loadActualTransfer(java.lang.String) loadActualTransfer}
     *
     *
     * @param eventProfileUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public Transfer loadActualTransferWithLock(String eventProfileUuid)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Nacita aktualny aktivny transfer pre event. Tato metoda je len na READ mod.
     * V pripade uprav ciastok je treba pouzit so zamkom.
     * 
     * @param eventProfileUuid uuid platobneho profilu
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public Transfer loadActualTransfer(String eventProfileUuid)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Nacita konkretny transfer s dotiahnutymi fakturami
     * @param eventUuid
     * @param transferUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public Transfer loadTransferWithOrders(String transferUuid)
            throws NoRecordException, RequiredPropertyException;

    public Transfer loadTransferDetail(String transferUuid)
            throws NoRecordException, RequiredPropertyException;

    Transfer loadTransferWithCommissionDetail(String transferUuid) throws NoRecordException, RequiredPropertyException;
    
    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    /**
     *
     * Prevedie transfer do stava CLOSED_AND_PREPARED. Takze vytvori objednavku
     * k prevodu.
     * <p>
     * Stale je zachovany zakladny princip vytvorenia faktury. Takze je najskor
     * objednavka vytvorena a je jej priradene cislo, nasledne po prevode do stavu
     * TRANSFERED sa vygeneruje invoice number.
     *
     *
     * @param eventUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public Transfer updateActiveTransferToPreparedState(String eventUuid, boolean toPreviousMonth)
            throws NoRecordException, RequiredPropertyException, PaymentException;

    /**
     * Prevedie transfer do ziadaneho stavu. Je mozne ak transfer nie je v stave
     * TRANSFERED , ERROR dat do stavu WAIT
     * @param transferUuid
     * @param toState
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public Transfer updateTransferState(String transferUuid, TransferState toState)
            throws NoRecordException, RequiredPropertyException, PaymentException;

    /**
     * Prevede transfer do stavu TRANSFERED a necha vygenerovat fakturu
     * 
     * @param eventUuid je uuid profilu eventu
     * @throws NoRecordException
     * @throws PaymentException
     * @throws RequiredPropertyException
     */
    public Transfer updatePreparedTransferToTransfered(String eventUuid, boolean toPreviousMonth)
            throws NoRecordException, RequiredPropertyException, PaymentException;

    /**
     * Prida objednavku k Transferu. Objednavka musi byt potvrdena a nemoze mat
     * nastaveny priznak {@link Order#suspicious}.
     *
     * @param order objednavka ktora sa prida do transferu
     * @throws NoRecordException
     * @throws RequiredPropertyException
     */
    public void updateTransferAddOrder(Order order)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Odcita objednavku z Transferu.
     * @param order
     * @throws NoRecordException
     * @throws RequiredPropertyException 
     */
    public void updateTransferRemoveOrder(Order order)
            throws NoRecordException, RequiredPropertyException;
    
    /** provede prepocitani poplatku */
    void updateTransferCalculateFees(String transferUuid) throws Exception;
}
