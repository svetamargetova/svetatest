package consys.payment.export.servlet;

import consys.payment.bo.Order;
import consys.payment.bo.Transfer;
import consys.payment.bo.enums.OrderState;

import consys.payment.export.pdf.PdfInvoiceGenerator;
import consys.payment.service.AcemceeService;
import consys.payment.service.OrderService;
import consys.payment.service.TransferService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DownloadInvoiceServlet extends HttpServlet implements DownloadServletFields {

    private static final long serialVersionUID = 4675783911089360883L;
    // logger
    private static final Logger logger = LoggerFactory.getLogger(DownloadInvoiceServlet.class);
    // konstanty
    private static final String INVOICE_PARAM = "invoice";
    private static final String ORDER_PREFIX = "proforma-";
    // data
    private OrderService orderService;
    private AcemceeService acemceeService;
    private TransferService transferService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String uuid = request.getParameter(INVOICE_PARAM);
            if (StringUtils.isBlank(uuid)) {
                logger.debug("Invoice UUID is missing");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }

            Order order = getOrderService().loadOrderByUuid(uuid);
            if (order.getState().equals(OrderState.CANCELED_SYSTEM)) {
                // muze se jednat o tansferovou objednavku, ktera byla pregenerovana
                try {
                    Transfer transfer = getTransferService().loadTransferWithCommissionDetail(uuid);
                    order = transfer.getCommissionOrder();
                } catch (NoRecordException ex) {
                    // nebyla to transferova objednavka
                } catch (RequiredPropertyException ex) {
                    // uz by vyletelo pri nacitani objednavky
                }
            }

            StringBuilder contentDisposition = new StringBuilder();
            contentDisposition.append(ATTACHMENT_FILENAME);
            if (order.getInvoice() == null) {
                contentDisposition.append(ORDER_PREFIX);
                contentDisposition.append(order.getOrderNumber());
            } else {
                contentDisposition.append(order.getInvoice().getInvoiceNumber());
            }
            contentDisposition.append(END_NAME);
            response.setContentType(CONTENT_TYPE_PDF);
            response.setHeader(CONTENT_DISPOSITION, contentDisposition.toString());
            response.setHeader(CACHE_CONTROL, MAXAGE_1S);
            response.setHeader(PRAGMA, PUBLIC);
            if (order.getInvoice() == null) {
                logger.debug("Generate PDF Order");
                PdfInvoiceGenerator generator = new PdfInvoiceGenerator();
                generator.setAcemceeService(getAcemceeService());
                generator.createPdfOrder(response.getOutputStream(), order, true);

            } else {
                logger.debug("Generate PDF Invoice");
                PdfInvoiceGenerator generator = new PdfInvoiceGenerator();
                generator.setAcemceeService(getAcemceeService());
                generator.createPdfInvoice(response.getOutputStream(), order.getInvoice(), true);
            }


            response.getOutputStream().flush();
            response.getOutputStream().close();


        } catch (RequiredPropertyException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Invoice UUID is missing");
            }
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch (NoRecordException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Invoice UUID is missing");
            }
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch (Exception ex) {
            logger.error("What the hell happend!?", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @return the orderService
     */
    public OrderService getOrderService() {
        if (orderService == null) {
            orderService = (OrderService) WebApplicationContextUtils.getWebApplicationContext(getServletContext()).getBean("orderService", OrderService.class);
        }

        return orderService;
    }

    /**
     * @param orderService the orderService to set
     */
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * @return the exportService
     */
    public AcemceeService getAcemceeService() {
        if (acemceeService == null) {
            acemceeService = (AcemceeService) WebApplicationContextUtils.getWebApplicationContext(getServletContext()).getBean("acemceeService", AcemceeService.class);
        }

        return acemceeService;
    }

    public TransferService getTransferService() {
        if (transferService == null) {
            transferService = (TransferService) WebApplicationContextUtils.getWebApplicationContext(getServletContext()).getBean("transferService", TransferService.class);
        }
        return transferService;
    }
}
