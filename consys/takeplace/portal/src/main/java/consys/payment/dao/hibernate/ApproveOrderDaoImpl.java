package consys.payment.dao.hibernate;

import consys.payment.bo.ApproveOrder;
import consys.payment.dao.ApproveOrderDao;
import consys.payment.service.exception.NoRecordException;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ApproveOrderDaoImpl extends BusinessObjectDaoImpl<ApproveOrder> implements ApproveOrderDao {

    @Override
    public ApproveOrder loadWithOrderPaymentDetails(Long id) throws NoRecordException {
        ApproveOrder a = (ApproveOrder) session().
                createQuery("select ap from ApproveOrder ap join fetch ap.order as o join fetch o.items as item join fetch item.product join fetch o.customer join fetch o.supplier where ap.id=:ID").
                setLong("ID", id).uniqueResult();
        if (a == null) {
            throw new NoRecordException("No ApproveOrder with id " + id);
        }
        return a;
    }

    @Override
    public List<ApproveOrder> listApproveOrders(int startItem, int itemsPerPage) {
        return prepareListCriteria(null).setFirstResult(startItem).setMaxResults(itemsPerPage).list();
    }

    @Override
    public int loadApproveOrdersCount() {
        return ((Long) prepareListCriteria(null).setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    @Override
    public List<ApproveOrder> listApproveOrdersForEvent(String eventUuid, int startItem, int itemsPerPage) {
        return prepareListCriteria(eventUuid).setFirstResult(startItem).setMaxResults(itemsPerPage).list();
    }

    @Override
    public int loadApproveOrdersForEventCount(String eventUuid) {
        return ((Long) prepareListCriteria(eventUuid).setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }

    private Criteria prepareListCriteria(String eventUuid) {
        Criteria c = session().createCriteria(ApproveOrder.class);
        if (StringUtils.isNotBlank(eventUuid)) {
            c.createAlias("order", "ord").
                    createAlias("ord.supplier", "sp").
                    createAlias("sp.event", "event").
                    add(Restrictions.eq("event.uuid", eventUuid));
        }
        return c;
    }

    @Override
    public ApproveOrder loadApproveOrderByOrderUuid(String uuid) throws NoRecordException {
        ApproveOrder a = (ApproveOrder) session().
                createQuery("select ap from ApproveOrder ap where ap.order.uuid=:UUID").
                setString("UUID", uuid).uniqueResult();
        if (a == null) {
            throw new NoRecordException("No ApproveOrder with uuid " + uuid);
        }
        return a;
    }
}
