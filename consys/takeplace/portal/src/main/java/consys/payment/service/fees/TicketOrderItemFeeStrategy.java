/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service.fees;

import consys.common.utils.BigDecimalUtils;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Order;
import consys.payment.bo.OrderItem;
import consys.payment.bo.enums.ProductCategory;
import java.math.BigDecimal;

/**
 * Strtegia za jednotku tiketu. Ak je item zadarmo tak automaticku sa necharguje nic.
 * V opacnom priapde sa charguje na zaklade toho z akeho zdroja bol tiket oznaceny.
 * @author palo
 */
public class TicketOrderItemFeeStrategy implements OrderItemFeeStrategy {

    @Override
    public boolean canHandleOrder(ProductCategory category) {
        return category.equals(ProductCategory.TICKET);
    }
    
    @Override
    public void computeFee(EventPaymentProfile orderProfile, OrderItem item, Order order) {
       
        // ak je zadarmo tak nechargujeme
        if (item.getDiscount() == 100 || item.getUnitPrice().compareTo(BigDecimal.ZERO) == 0) {
            return;
        } else {
            BigDecimal unitFee = BigDecimal.ZERO;
            BigDecimal shareFee = BigDecimal.ZERO;
            switch (order.getState()) {
                // ak je potvrdena objednavka organizatorom potom chargujeme podla profilu
                case CONFIRMED_ORGANIZATOR:
                    unitFee = orderProfile.getOrderUnitFeeOrganizator();
                    shareFee = orderProfile.getOrderShareFeeOrganizator();
                    break;

                // ak je objednavka potvrdena systemom potom cahrgujeme podla toho ci online alebo banka
                case CONFIRMED_SYSTEM: {
                    if (order.getInvoice() != null && order.getInvoice().getConfirmationSource() != null) {
                        switch (order.getInvoice().getConfirmationSource()) {
                            case BANK_TRANSFER:
                                unitFee = orderProfile.getOrderUnitFeeBank();
                                shareFee = orderProfile.getOrderShareFeeBank();
                                break;
                            case SYSTEM:
                            case ONLINE_PAY_PAL:
                                unitFee = orderProfile.getOrderUnitFeeOnline();
                                shareFee = orderProfile.getOrderShareFeeOnline();
                                break;
                        }
                        break;
                    }else{
                         unitFee = orderProfile.getOrderUnitFeeOnline();
                         shareFee = orderProfile.getOrderShareFeeOnline();
                    }
                }
                default:
                    unitFee = orderProfile.getOrderUnitFeeOnline();
                    shareFee = orderProfile.getOrderShareFeeOnline();
            }
            order.addToUnitFee(unitFee.multiply(new BigDecimal(item.getQuantity())));
            
            // vypocets            
            order.addToShareFee(BigDecimalUtils.countPercent(item.getUnitPriceDiscounted(), shareFee).multiply(new BigDecimal(item.getQuantity())));
            
        }
    }
}
