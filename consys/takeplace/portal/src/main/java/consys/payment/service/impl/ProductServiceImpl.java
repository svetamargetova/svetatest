package consys.payment.service.impl;

import consys.common.utils.collection.Lists;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Product;
import consys.payment.bo.enums.ProductCategory;
import consys.payment.dao.ProductDao;
import consys.payment.service.EventService;
import consys.payment.service.ProductService;
import consys.payment.service.UserService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.utils.ProductUtils;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProductServiceImpl extends Service implements ProductService {

    // - Service
    private EventService eventService;
    private UserService userService;
    // - Dao
    private ProductDao productDao;


    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void createEventProduct(EventPaymentProfile profile, Product product) throws NoRecordException, RequiredPropertyException {
        logger.debug("Creating {}", product);
        product.setProfile(profile);
        ProductUtils.validate(product);
        getProductDao().create(product);
    }

    @Override
    public void createEventProduct(String eventUuid, Product product)
            throws RequiredPropertyException, NoRecordException {
        if (StringUtils.isBlank(eventUuid) || product == null) {
            throw new RequiredPropertyException("Missing UUID od event");
        }

        EventPaymentProfile epp = eventService.loadEventDefaultPaymentProfile(eventUuid);
        createEventProduct(epp, product);
    }

    @Override
    public void createEventProducts(String eventUuid, List<Product> products) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid) || CollectionUtils.isEmpty(products)) {
            throw new RequiredPropertyException("Missing event uuid or products collections is empty");
        }
        EventPaymentProfile epp = eventService.loadEventDefaultPaymentProfile(eventUuid);
        createEventProducts(epp, products);
    }

    @Override
    public void createEventProducts(EventPaymentProfile profile, List<Product> product) throws NoRecordException, RequiredPropertyException {
        for (Product product1 : product) {
            createEventProduct(profile, product1);
        }
    }

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateEventProduct(EventPaymentProfile profile, Product product)
            throws NoRecordException, RequiredPropertyException {
        if (profile == null || product == null) {
            throw new RequiredPropertyException("Missing event profile or product");
        }
        if (!profile.getId().equals(product.getProfile().getId())) {
            throw new NoRecordException("Trying to update product of other profile!");
        }
        ProductUtils.validate(product);
        logger.debug("Updating {}", product);
        productDao.update(product);

    }

    @Override
    public void updateEventProduct(String eventUuid, Product readOnlyProduct)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid) || readOnlyProduct == null) {
            throw new RequiredPropertyException("Missing event , product or profile");
        }

        // znovunacitame
        Product updateProduct = productDao.loadProductByUuid(readOnlyProduct.getUuid());
        updateProduct.setCategory(readOnlyProduct.getCategory());
        updateProduct.setName(readOnlyProduct.getName());
        updateProduct.setPrice(readOnlyProduct.getPrice());
        updateProduct.setVatRate(readOnlyProduct.getVatRate());
        updateEventProduct(updateProduct.getProfile(), updateProduct);
    }

    @Override
    public void updateEventProducts(EventPaymentProfile eventPaymentProfile, List<Product> products)
            throws NoRecordException, RequiredPropertyException {
        if (products == null) {
            throw new RequiredPropertyException("Product list null");
        }

        if (products.isEmpty()) {
            return;
        }

        for (Product product1 : products) {
            updateEventProduct(eventPaymentProfile, product1);
        }
    }

    @Override
    public void updateEventProducts(String event, List<Product> readOnlyProducts)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(event) || readOnlyProducts == null) {
            throw new RequiredPropertyException("Missing event , product");
        }

        if (readOnlyProducts.isEmpty()) {
            return;
        }


        List<String> uuids = Lists.newArrayList();
        for (Product product : readOnlyProducts) {
            uuids.add(product.getUuid());
        }

        List<Product> loadedProducts = productDao.listEventProductsByUuid(event, uuids);
        if (loadedProducts.size() != readOnlyProducts.size()) {
            throw new NoRecordException("Trying to update product of other events! Requested " + readOnlyProducts.size() + ", owned " + loadedProducts.size());
        }
        EventPaymentProfile epp = eventService.loadEventPaymentProfile(loadedProducts.get(0).getProfile().getId());
        // je potreba nasetovat hodnoty ktere su zavisle 
        for (Product p : loadedProducts) {
            Product updatingProduct = null;
            for (Product pp : readOnlyProducts) {
                if (pp.getUuid().equalsIgnoreCase(p.getUuid())) {
                    updatingProduct = pp;
                    break;
                }
            }
            if (updatingProduct == null) {
                throw new NullPointerException("This can't happen but ..");
            }

            // presetujeme hodnoty
            p.setCategory(updatingProduct.getCategory());
            p.setName(updatingProduct.getName());
            p.setPrice(updatingProduct.getPrice());
            p.setVatRate(updatingProduct.getVatRate());
            ProductUtils.validate(p);
        }
        updateEventProducts(epp, loadedProducts);
    }

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    @Override
    public Product loadProduct(String productUuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(productUuid)) {
            throw new RequiredPropertyException("Missing product uuid");
        }
        return getProductDao().loadProductByUuid(productUuid);
    }

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    @Override
    public List<Product> listAllEventProducts(String eventUuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Missing event uuid");
        }
        return getProductDao().loadEventProducts(eventUuid);
    }

    @Override
    public List<Product> listProducts(List<String> productUuid) throws NoRecordException, RequiredPropertyException {
        if (CollectionUtils.isEmpty(productUuid)) {
            return Lists.newArrayList();
        }
        return getProductDao().loadProductsByUuid(productUuid);
    }

    @Override
    public List<Product> listAllEventPaymentProfileProducts(String eventPaymentProfileUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventPaymentProfileUuid)) {
            throw new RequiredPropertyException("Missing eventPaymentProfileUuid");
        }
        return getProductDao().listEventPaymentProfileProducts(eventPaymentProfileUuid);
    }

    /*------------------------------------------------------------------------*/
    /* ---  D E L E T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public int deleteProducts(String eventUuid, List<String> productsUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Missing event or product");
        }
        if (CollectionUtils.isEmpty(productsUuid)) {
            return 0;
        }

        Event event = eventService.loadEvent(eventUuid);
        List<Product> products = productDao.loadProductsByUuid(productsUuid);
        for (Product product : products) {
            deleteProduct(event, product);
        }
        return products.size();
    }

    @Override
    public boolean deleteProduct(Event event, Product product) throws RequiredPropertyException {
        if (event == null || product == null) {
            throw new RequiredPropertyException("Missing event or product");
        }
        // test na to ci je nekde produkt pouzity uz

        int count = productDao.loadProductUsageCount(product.getUuid());
        if (count == 0) {
            logger.debug("Deleting unused product {} from event {}", product, event);
            productDao.delete(product);
            return true;
        } else {
            logger.debug("Can't delete {}. Used {} times.", product, count);
            return false;
        }
    }

    /*========================================================================*/
    /* GETTER SETTER */
    /*========================================================================*/
    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @return the productDao
     */
    public ProductDao getProductDao() {
        return productDao;
    }

    /**
     * @param productDao the productDao to set
     */
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
