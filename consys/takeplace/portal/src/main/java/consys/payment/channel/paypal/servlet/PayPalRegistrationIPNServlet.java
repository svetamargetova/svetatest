package consys.payment.channel.paypal.servlet;

import consys.payment.channel.paypal.ipn.AbstractPayPalIpnServlet;
import consys.payment.channel.paypal.ipn.PaypalTransaction;
import consys.payment.service.TransactionService;
import consys.payment.service.exception.RequiredPropertyException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PayPalRegistrationIPNServlet extends AbstractPayPalIpnServlet {

    public static final String SEPARATOR = "#";
    private static final long serialVersionUID = 690385237726211776L;
    private static final String LOGGER_NAME = "consys.payment.paypal.servlet";
    @Autowired
    private TransactionService transactionService;

    public PayPalRegistrationIPNServlet() {
        super(LOGGER_NAME);

    }

    @Override
    protected void processIpnMessage(PaypalTransaction ipn) {
        switch (ipn.paymentStatus()) {
            case Completed:
                completed(ipn);
                break;
            case Refunded:
                refunded(ipn);
                break;
        }
    }

    private void completed(PaypalTransaction ipn) {
        String registrationUuid = ipn.invoice();

        if (StringUtils.isBlank(registrationUuid)) {
            logger.error("Order UUID is missing. Maybe it's not payment related to registration. Payment transaction ID: " + ipn.transactionId());
            return;
        }

        try {
            if (ipn.testIpn()) {
                logger.info("TEST-IPN ... just testing processing (Completed)");
            } else {
                transactionService.createPaypalTransaction(ipn);
            }
        } catch (RequiredPropertyException ex) {
            // TODO: poslat email
            logger.error("Missing some property", ex);
        }
    }

    private void refunded(PaypalTransaction ipn) {
        try {
            if (ipn.testIpn()) {
                logger.info("TEST-IPN ... just testing processing (Refunded)");
            } else {
                transactionService.refundedPaypalTransaction(ipn);
            }
        } catch (RequiredPropertyException ex) {
            // TODO: poslat email
            logger.error("Missing some property", ex);
        }
    }

    @Override
    protected void processPending(String pendingReason) {
        // Nic .. nezajima nas toto
    }

    @Override
    protected void processChargeBack(String pendingReason) {
        // Nic nezajima nas toto
    }
}
