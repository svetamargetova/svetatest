package consys.payment.webservice.impl;

import java.util.Map;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractWebServiceImpl implements ApplicationContextAware,InitializingBean {
    
    private ApplicationContext applicationContext;


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception{
        
    }

    protected <T> T getBean(String beanName) {
        return (T) applicationContext.getParent().getBean(beanName);
    }

    protected <T> T getBeanOfType(Class<T> clazz) {
        Map<String, T> beans = applicationContext.getParent().getBeansOfType(clazz);
        if (beans.isEmpty()) {
            throw new NullPointerException("No beans in context for type " + clazz);
        }
        return beans.values().iterator().next();
    }
}
