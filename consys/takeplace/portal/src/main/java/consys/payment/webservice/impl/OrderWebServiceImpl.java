package consys.payment.webservice.impl;

import consys.common.utils.collection.Lists;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Order;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.enums.OrderState;
import consys.payment.bo.thumb.ProductOrder;
import consys.payment.service.EventService;
import consys.payment.service.OrderService;

import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.EventProfileNotFilledException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.OrderAlreadyPayedException;
import consys.payment.service.exception.ProductIncompatibilityException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.webservice.OrderWebService;
import consys.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.bo.ConfirmationChannel;
import consys.payment.ws.bo.OrderedItem;
import consys.payment.ws.order.CancelOrderExceptionEnum;
import consys.payment.ws.order.CancelOrderRequest;
import consys.payment.ws.order.CancelOrderResponse;
import consys.payment.ws.order.CancelOrdersRequest;
import consys.payment.ws.order.CancelOrdersResponse;
import consys.payment.ws.order.ConfirmOrderRequest;
import consys.payment.ws.order.ConfirmOrderResponse;
import consys.payment.ws.order.CreateOrderExceptionEnum;
import consys.payment.ws.order.CreateOrderRequest;
import consys.payment.ws.order.CreateOrderResponse;
import consys.payment.ws.order.CreateOrderWithProfileRequest;
import consys.payment.ws.order.CreateOrderWithProfileResponse;
import consys.payment.ws.order.LoadOrderRequest;
import consys.payment.ws.order.LoadOrderResponse;
import consys.payment.ws.order.OrderItem;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class OrderWebServiceImpl extends AbstractWebServiceImpl implements OrderWebService {

    private static final Logger logger = LoggerFactory.getLogger(OrderWebServiceImpl.class);
    private OrderService orderService;
    private EventService eventService;

    @Override
    @PayloadRoot(localPart = "CreateOrderRequest", namespace = NAMESPACE)
    public CreateOrderResponse createOrder(CreateOrderRequest request) {
        CreateOrderResponse response = OBJECT_FACTORY.createCreateOrderResponse();
        try {

            List<ProductOrder> items = Lists.newArrayListWithCapacity(request.getOrderItems().size());
            for (OrderItem item : request.getOrderItems()) {
                items.add(new ProductOrder(item.getProductUuid(), item.getQuantity(), item.getDiscount()));
            }

            EventPaymentProfile epp;
            if (StringUtils.isNotBlank(request.getEventPaymentProfileUuid())) {
                epp = getEventService().loadEventPaymentProfile(request.getEventPaymentProfileUuid());
            } else {
                epp = getEventService().loadEventDefaultPaymentProfileDetailed(request.getEventUuid());
            }

            String customerUuid = customerUuid(request.getUserUuid(), request.getManagedUserUuid());

            Order order = getOrderService().createUserOrder(
                    request.getOrderUuid(),
                    items,
                    customerUuid,
                    request.getUserProfileUuid(),
                    epp);
            response.setFinalPrice(order.getPrice());
            response.setConfirmed(order.isConfirmed());
        } catch (EventProfileNotFilledException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        } catch (IbanFormatException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.IBAN_FORMAT_EXCEPTION);
        } catch (ProductIncompatibilityException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.PRODUCT_INCOMPATIBILITY_EXCEPTION);
        } catch (EventNotActiveException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.EVENT_NOT_ACTIVE_EXCEPTION);
        } catch (NoRecordException ex) {
            response.setException(CreateOrderExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (RequiredPropertyException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        }
        return response;
    }

    @Override
    @PayloadRoot(localPart = "CreateOrderWithProfileRequest", namespace = NAMESPACE)
    public CreateOrderWithProfileResponse createOrderWithProfile(CreateOrderWithProfileRequest request) {
        CreateOrderWithProfileResponse response = OBJECT_FACTORY.createCreateOrderWithProfileResponse();
        try {
            PaymentProfileRevision userRev = ConvertUtils.toCoreProfileRevision(request.getUserProfile());

            List<ProductOrder> items = Lists.newArrayListWithCapacity(request.getOrderItems().size());
            for (OrderItem item : request.getOrderItems()) {
                items.add(new ProductOrder(item.getProductUuid(), item.getQuantity(), item.getDiscount()));
            }

            EventPaymentProfile epp;
            if (StringUtils.isNotBlank(request.getEventPaymentProfileUuid())) {
                epp = getEventService().loadEventPaymentProfile(request.getEventPaymentProfileUuid());
            } else {
                epp = getEventService().loadEventDefaultPaymentProfileDetailed(request.getEventUuid());
            }

            String customerUuid = customerUuid(request.getUserUuid(), request.getManagedUserUuid());

            Order order = getOrderService().createUserOrder(
                    request.getOrderUuid(),
                    items,
                    customerUuid,
                    request.getUserProfileUuid(),
                    userRev,
                    epp);
            response.setFinalPrice(order.getPrice());
            response.setConfirmed(order.isConfirmed());
        } catch (EventProfileNotFilledException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        } catch (IbanFormatException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.IBAN_FORMAT_EXCEPTION);
        } catch (ProductIncompatibilityException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.PRODUCT_INCOMPATIBILITY_EXCEPTION);
        } catch (EventNotActiveException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.EVENT_NOT_ACTIVE_EXCEPTION);
        } catch (NoRecordException ex) {
            response.setException(CreateOrderExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (RequiredPropertyException ex) {
            logger.error("Create order failed.", ex);
            response.setException(CreateOrderExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        }
        return response;
    }

    /** z pozadavku urci jestli se jedna o uuid uzivatele pozadavku nebo spravovaneho uzivatele */
    private String customerUuid(String userUuid, String managedUserUuid) {
        if (StringUtils.isBlank(managedUserUuid) || StringUtils.isBlank(userUuid) || userUuid.equals(managedUserUuid)) {
            return userUuid;
        } else {
            return managedUserUuid;
        }
    }

    @Override
    @PayloadRoot(localPart = "ConfirmOrderRequest", namespace = NAMESPACE)
    public ConfirmOrderResponse confirmOrder(ConfirmOrderRequest request) {
        ConfirmOrderResponse resp = OBJECT_FACTORY.createConfirmOrderResponse();
        try {
            Order o = getOrderService().updateOrderToConfirmedByOrganizator(request.getOrderUuid(), request.getUserUuid(), request.getNote());
            resp.setOrderState(ConvertUtils.toWsOrderState(o.getState()));
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
        return resp;
    }

    @Override
    @PayloadRoot(localPart = "CancelOrderRequest", namespace = NAMESPACE)
    public CancelOrderResponse cancelOrder(CancelOrderRequest request) {
        CancelOrderResponse response = OBJECT_FACTORY.createCancelOrderResponse();
        try {
            OrderState toState = null;
            switch (request.getReason()) {
                case ORGANIZATOR:
                    toState = OrderState.CANCELED_ORGANIZATOR;
                    break;
                case OWNER:
                    toState = OrderState.CANCELED_OWNER;
                    break;
                case TIMEOUTED:
                    toState = OrderState.CANCELED_SYSTEM;
                    break;
            }
            getOrderService().cancelOrder(request.getOrderUuid(), toState, request.getNote(), false);
        } catch (OrderAlreadyPayedException ex) {
            logger.error("Order is already payed! Can't be canceled by this way." + request.getOrderUuid(), ex);
            response.setException(CancelOrderExceptionEnum.ORDER_ALREADY_PAYED_EXCEPTION);
        } catch (NoRecordException ex) {
            logger.error("No Order with uuid " + request.getOrderUuid(), ex);
            response.setException(CancelOrderExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (RequiredPropertyException ex) {
            logger.error("Missing required properties", ex);
            response.setException(CancelOrderExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        }
        return response;
    }

    @Override
    @PayloadRoot(localPart = "CancelOrdersRequest", namespace = NAMESPACE)
    public CancelOrdersResponse cancelOrders(CancelOrdersRequest request) {
        CancelOrdersResponse response = OBJECT_FACTORY.createCancelOrdersResponse();
        try {
            OrderState toState = null;
            switch (request.getReason()) {
                case ORGANIZATOR:
                    toState = OrderState.CANCELED_ORGANIZATOR;
                    break;
                case OWNER:
                    toState = OrderState.CANCELED_OWNER;
                    break;
                case TIMEOUTED:
                    toState = OrderState.CANCELED_SYSTEM;
                    break;
            }

            getOrderService().cancelOrders(request.getOrderUuids(), toState, request.getNote(), false);
        } catch (OrderAlreadyPayedException ex) {
            logger.error("Some order is already payed! Can't be canceled all orders", ex);
            response.setException(CancelOrderExceptionEnum.ORDER_ALREADY_PAYED_EXCEPTION);
        } catch (NoRecordException ex) {
            logger.error("Given uuids contains order that not exists!", ex);
            response.setException(CancelOrderExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (RequiredPropertyException ex) {
            logger.error("Missing required properties", ex);
            response.setException(CancelOrderExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        }
        return response;
    }

    @Override
    @PayloadRoot(localPart = "LoadOrderRequest", namespace = NAMESPACE)
    public LoadOrderResponse loadOrder(LoadOrderRequest request) {
        LoadOrderResponse resp = OBJECT_FACTORY.createLoadOrderResponse();
        try {
            Order order = getOrderService().loadOrderByUuid(request.getOrderUuid());
            resp.setOrder(new consys.payment.ws.bo.Order());

            resp.getOrder().setConfirmedDate(ConvertUtils.toXmlCalendar(order.getConfirmedDate()));
            resp.getOrder().setDueDate(ConvertUtils.toXmlCalendar(order.getDueDate()));
            resp.getOrder().setPurchasedDate(ConvertUtils.toXmlCalendar(order.getPurchaseDate()));
            resp.getOrder().setFees(order.getShareFee().add(order.getUnitFee()));
            resp.getOrder().setPrice(order.getPrice());
            resp.getOrder().setThirPartyFees(order.getThirdPartyFee());
            resp.getOrder().setCurrency(ConvertUtils.toWsCurrency(order.getSupplier().getCurrency()));
            resp.getOrder().setProformaNumber(order.getOrderNumber());
            resp.getOrder().setState(ConvertUtils.toWsOrderState(order.getState()));
            for (consys.payment.bo.OrderItem oi : order.getItems()) {
                OrderedItem oi1 = new OrderedItem();
                oi1.setDiscount(oi.getDiscount());
                oi1.setName(oi.getName());
                oi1.setQuantity(oi.getQuantity());
                oi1.setUnitPrice(oi.getUnitPrice());
                oi1.setVat(oi.getVatRate());
                resp.getOrder().getItems().add(oi1);
            }

            if (order.getInvoice() != null) {
                resp.getOrder().setIssueDate(ConvertUtils.toXmlCalendar(order.getInvoice().getIssueDate()));
                resp.getOrder().setTaxDate(ConvertUtils.toXmlCalendar(order.getInvoice().getTaxableFulfillmentDate()));
                resp.getOrder().setInvoiceNumber(order.getInvoice().getInvoiceNumber());
                switch (order.getInvoice().getConfirmationSource()) {
                    case BANK_TRANSFER:
                        resp.getOrder().setConfirmed(ConfirmationChannel.BANK);
                        break;
                    case ONLINE_PAY_PAL:
                        resp.getOrder().setConfirmed(ConfirmationChannel.PAY_PAL);
                        break;
                    case SYSTEM:
                        resp.getOrder().setConfirmed(ConfirmationChannel.SYSTEM);
                        break;
                }
            } else if (order.isConfirmed()) {
                resp.getOrder().setConfirmed(ConfirmationChannel.SYSTEM);
            }
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Invoice with uuid " + request.getOrderUuid(), ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
        return resp;
    }

    /**
     * @return the orderService
     */
    public OrderService getOrderService() {
        return orderService;
    }

    /**
     * @param orderService the orderService to set
     */
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }
}
