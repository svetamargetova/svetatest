package consys.payment.dao.hibernate;

import consys.payment.bo.PaymentProfileRevision;
import consys.payment.dao.PaymentProfileRevisionDao;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PaymentProfileRevisionDaoImpl extends BusinessObjectDaoImpl<PaymentProfileRevision> implements PaymentProfileRevisionDao {

    @Override
    public List<PaymentProfileRevision> listEventProfiles(String uuid) throws NoRecordException {
        List<PaymentProfileRevision> profiles = session().
                createQuery("select rev from Event e join e.profiles as profile join profile.actualRevision as rev where e.uuid=:UUID").
                setString("UUID", uuid).
                list();
        if (profiles == null || profiles.isEmpty()) {
            throw new NoRecordException("No profiles has been found for event with uuid " + uuid);
        }
        return profiles;
    }

    @Override
    public List<PaymentProfileRevision> listUserProfiles(String uuid) throws NoRecordException {
        List<PaymentProfileRevision> profiles = session().
                createQuery("select rev from User e join e.profiles as profile join profile.actualRevision as rev where e.uuid=:UUID").
                setString("UUID", uuid).
                list();
        if (profiles == null || profiles.isEmpty()) {
            throw new NoRecordException("No profiles has been found for user with uuid " + uuid);
        }
        return profiles;
    }

    @Override
    public Integer loadRevisionUsedCount(String revisionUuid) {
        List<Long> list = session().
                createQuery("select count(i.id) from Invoice i join i.supplier as s join i.customer as c where c.uuid=:R_UUID or s.uuid=:R_UUID").
                setString("R_UUID", revisionUuid).list();
        return list.get(0).intValue();
    }
}
