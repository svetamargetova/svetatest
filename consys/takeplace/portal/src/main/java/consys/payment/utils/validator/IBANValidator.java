package consys.payment.utils.validator;


import java.math.BigInteger;
import java.util.ResourceBundle;
import org.springframework.util.StringUtils;

/**
 * User: <a href="http://javaunderhood.wordpress.com">Maciej Kreft</a>
 */
public class IBANValidator {
    public static final String IBAN = "validator/IBAN";
    
  
    public static boolean isValid(Object v){
        if (v == null) return true;
        else if (!(v instanceof String)) return false;
        
        String iban = StringUtils.trimAllWhitespace(String.valueOf(v));
        return isCheckDigitValid(iban);
    }

    public static boolean isCheckDigitValid(String iban) {
        int validIBANLength = getValidIBANLength(iban);
        if (validIBANLength < 4) return false;
        if (iban.length() != validIBANLength) return false;

        BigInteger numericIBAN = getNumericIBAN(iban, false);

        int checkDigit = numericIBAN.mod(new BigInteger("97")).intValue();
        return checkDigit == 1;
    }

    private static int getValidIBANLength(String countryCode) {
        String code = countryCode.substring(0, 2).toUpperCase();
        String length = ResourceBundle.getBundle(IBAN).getString("length." + code);
        if (length == null) return -1;
        return Integer.valueOf(length);
    }

    private static BigInteger getNumericIBAN(String iban, boolean isCheckDigitAtEnd) {
        String endCheckDigitIBAN = iban;
        if (!isCheckDigitAtEnd) {
            endCheckDigitIBAN = iban.substring(4) + iban.substring(0, 4);
        }
        StringBuilder numericIBAN = new StringBuilder();
        for (int i = 0; i < endCheckDigitIBAN.length(); i++) {
            if (Character.isDigit(endCheckDigitIBAN.charAt(i))) {
                numericIBAN.append(endCheckDigitIBAN.charAt(i));
            } else {
                numericIBAN.append(10 + getAlphabetPosition(endCheckDigitIBAN.charAt(i)));
            }
        }

        return new BigInteger(numericIBAN.toString());
    }

    private static int getAlphabetPosition(char letter) {
        return Character.valueOf(Character.toUpperCase(letter)).compareTo('A');
    }
}
