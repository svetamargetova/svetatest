package consys.payment.export.servlet;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface DownloadServletFields {

    public static final String NO_CACHE = "no-cache";
    public static final String MAXAGE_1S = "maxage=1";
    public static final String CONTENT_TYPE_PDF = "application/pdf";
    public static final String CONTENT_TYPE_XLS = "application/vnd.ms-excel";
    public static final String CONTENT_TYPE_ZIP = "application/zip";
    public static final String CACHE_CONTROL = "Cache-control";
    public static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
    public static final String INLINE_FILENAME = "inline; filename=\"";
    public static final String CONTENT_DISPOSITION = "Content-Disposition";
    public static final String END_NAME = "\"";
    public static final String PRAGMA = "Pragma";
    public static final String PUBLIC = "public";
}
