/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao;

import consys.payment.bo.PaymentProfileRevision;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface PaymentProfileRevisionDao extends PersistedObjectDao<PaymentProfileRevision> {

    public List<PaymentProfileRevision> listEventProfiles(String uuid)
            throws NoRecordException;

    public List<PaymentProfileRevision> listUserProfiles(String uuid)
            throws NoRecordException;

    public Integer loadRevisionUsedCount(String revisionUuid);
}
