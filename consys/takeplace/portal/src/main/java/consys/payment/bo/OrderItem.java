/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo;

import consys.common.utils.BigDecimalUtils;
import java.math.BigDecimal;
import java.math.MathContext;

/**
 *
 * @author palo
 */
public class OrderItem implements BusinessObject {

    private static final long serialVersionUID = -7693328854482496323L;
    private Long id;
    /** Ku ktorej tato jedna vec patri */
    private Order order;
    /** Identifikuje produkt */
    private Product product;
    /** Pocet poloziek na fakture */
    private int quantity = 0;
    /** Jednotkova cena s DPH. */
    private BigDecimal unitPrice;
    /** Vat na tento typ produktu */
    private BigDecimal vatRate;
    /** Percentualne zlava */
    private int discount = 0;
    /** Kopia mena produktu - historicka zavislost */
    private String name;

    /**
     * Kopia mena produktu - historicka zavislost
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Kopia mena produktu - historicka zavislost
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the vatRate
     */
    public BigDecimal getVatRate() {
        return vatRate;
    }

    /**
     * @param vatRate the vatRate to set
     */
    public void setVatRate(BigDecimal vatRate) {
        this.vatRate = vatRate;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Ku ktorej tato jedna vec patri
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * Ku ktorej tato jedna vec patri
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * Identifikuje produkt
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Identifikuje produkt
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * Pocet poloziek na fakture
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Pocet poloziek na fakture
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Jednotkova cena. Zkopirovana z Product alebo nastavena rucne
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * Jednotkova cena. Zkopirovana z Product alebo nastavena rucne
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }
        
    /**
     * Percentualne zlava
     * @return the discount
     */
    public int getDiscount() {
        return discount;
    }

    /**
     * Percentualne zlava
     * @param discount the discount to set
     */
    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        OrderItem e = (OrderItem) obj;
        if (e.getId() == null || id == null) {
            return false;
        }
        return id.equals(e.getId());
    }

   

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("InvoiceItem[product=%s quantity=%s unitPrice=%s discount=%d]", product, quantity, unitPrice,discount);
    }

    /* Doplnkove gettery pre spocitane hodnot */

    /**
     * Vrati cenu jednotky so zlavou.
     * @return vysledna cena jednotky.
     */
    public BigDecimal getUnitPriceDiscounted() {
        return countDiscountFrom(unitPrice, discount);
    }        
    
    private static BigDecimal countDiscountFrom(BigDecimal base, int discount){
        if (discount == 100) {
            return BigDecimal.ZERO;
        } else if (discount == 0) {
            return base;
        } else {
            return BigDecimalUtils.countPercentage(base, new BigDecimal(discount));
        }
    }

    /**
     * Vrati celkovu sumu tejto polozky. <code> UnitPriceDiscounted * quantity</code>
     * @return
     */
    public BigDecimal getTotal(){
        if (getDiscount() == 100) {
            return BigDecimal.ZERO;
        } else  {
            return getUnitPriceDiscounted().multiply(new BigDecimal(quantity),BigDecimalUtils.DEFAULT);
        }
        
    }

}
