package consys.payment.utils;

import consys.payment.SystemProperties;
import consys.payment.bo.SystemProperty;
import consys.payment.service.exception.NoRecordException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProformaProvider extends HibernateDaoSupport{
        
    private static final Logger logger = LoggerFactory.getLogger(ProformaProvider.class);
    
    private int lastUsedProformaNumber;
    private SystemProperty lastProformaNumberProperty;
    // -- konstanty
    private static NumberFormat PROFORMA_FORMAT = new DecimalFormat("1000000000");




    public synchronized String getNextProformaNumber() {
        // nastavime novu hodnotu proformy
        int newProformaNumber = lastUsedProformaNumber + 1;
        // vytvorime string                
        
        String number = PROFORMA_FORMAT.format(newProformaNumber);
        // updatujeme do DB
        lastProformaNumberProperty.setValue(String.format("%d",newProformaNumber));
        getSession().update(lastProformaNumberProperty);
        // nastavime ze je to aktualne cislo
        lastUsedProformaNumber = newProformaNumber;
        return number;
    }

    void initializeProvider() {       
        try {
            lastProformaNumberProperty = loadByKey(SystemProperties.ACTUAL_PROFORMA_NUMBER);
            logger.info("  system property sucessfully laoded");
        } catch (NoRecordException ex) {
            // este nebylo vytvorene? tak vytvorime a nastavime 0;
            logger.info("  system property does not exists yet.. creating with base value 0");
            SystemProperty p = new SystemProperty();
            p.setKey(SystemProperties.ACTUAL_PROFORMA_NUMBER);
            p.setValue("0");
            getSession(false).save(p);
            lastProformaNumberProperty = p;
        }
        try {
            lastUsedProformaNumber = Integer.parseInt(lastProformaNumberProperty.getValue());
            logger.info("  pro forma number sucessfully initialized. Last used: {}", lastUsedProformaNumber);
        } catch (NumberFormatException e) {
            logger.error("Can't convert proforma number! {}", lastProformaNumberProperty);
            lastUsedProformaNumber = 0;
        }
    }

  public SystemProperty loadByKey(String key) throws NoRecordException {
        SystemProperty prop = (SystemProperty) getSession(false).
                createQuery("select s from SystemProperty s where s.key = :KKEY").
                setString("KKEY", key).
                uniqueResult();
        if (prop == null) {
            throw new NoRecordException("No SYstemPropety with key " + key);
        }
        return prop;
    }
}
