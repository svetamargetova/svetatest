package consys.payment.service.impl;

import com.google.common.collect.Lists;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Maps;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.payment.SystemProperties;
import consys.payment.bo.*;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.bo.enums.OrderState;
import consys.payment.bo.enums.TransferState;
import consys.payment.bo.thumb.ProductOrder;
import consys.payment.dao.InvoiceDao;
import consys.payment.dao.OrderDao;
import consys.payment.dao.TransferDao;
import consys.payment.service.AcemceeService;
import consys.payment.service.EventService;
import consys.payment.service.OrderService;
import consys.payment.service.TransferService;
import consys.payment.service.exception.*;
import consys.payment.service.fees.FeeStrategyService;
import consys.payment.utils.PaymentUtils;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TransferServiceImpl extends Service implements TransferService {

    private EventService eventService;
    private AcemceeService acemceeService;
    private TransferDao transferDao;
    private OrderService orderService;
    private InvoiceDao invoiceDao;
    @Autowired
    private FeeStrategyService feeStrategyService;
    @Autowired
    private OrderDao orderDao;

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public Transfer updateActiveTransferToPreparedState(String eventUuid, boolean toPreviousMonth) throws NoRecordException, RequiredPropertyException, PaymentException {
        try {
            if (StringUtils.isBlank(eventUuid)) {
                throw new RequiredPropertyException("Missing event uuid ");
            }
            Transfer t = transferDao.loadActualTransferWithEvent(eventUuid);
            addTransactionOrderWithInvoice(t, toPreviousMonth);
            // Nastavime stav prepated
            t.setState(TransferState.CLOSED_AND_PREPARED);
            t.appendToNote("Closed & Prepared");
            // Nativme datum generovania
            t.setTransferDate(DateProvider.getCurrentDate());
            // Update
            transferDao.update(t);
            return t;
        } catch (EventNotActiveException ex) {
            throw new PaymentException("Event is not active?!", ex);
        } catch (ProductIncompatibilityException ex) {
            throw new PaymentException("Product incompatibility ?!", ex);
        } catch (EventProfileNotFilledException ex) {
            throw new PaymentException("Event profile not filled ?!", ex);
        } catch (IbanFormatException ex) {
            throw new PaymentException("Bad iban?!", ex);
        }
    }

    private void addTransactionOrderWithInvoice(Transfer t, boolean toPreviousMonth) throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException {
        EventPaymentProfile epp = t.getEventPaymentProfile();
        EventPaymentProfile acmcpp = acemceeService.loadEventPaymentProfile(epp.getCurrency());
        ProductOrder feesOrderItem = new ProductOrder(acemceeService.loadCommissionProductUuid(epp.getCurrency()), 1, 0, t.getFeesSum());
        ProductOrder thirdPartyFeesOrderItem = new ProductOrder(acemceeService.loadThirdPartyFeeProductUuid(epp.getCurrency()), 1, 0, t.getThirdPartyFeesSum());
        Order order = orderService.createOrder(t.getUuid(), Lists.newArrayList(feesOrderItem, thirdPartyFeesOrderItem), epp.getPaymentProfile().getActualRevision(), acmcpp);
        if (toPreviousMonth) {
            Date previous = DateProvider.previousMonthLastDayDate(DateProvider.getCurrentDate());
            order.setPurchaseDate(previous);
            order.setDueDate(DateUtils.addDays(previous, SystemProperties.INVOICE_DUE_DATE));
            orderService.updateOrder(order);
        }
        t.setCommissionOrder(order);
    }

    @Override
    public Transfer updatePreparedTransferToTransfered(String transferUuid, boolean toPreviousMonth) throws NoRecordException, RequiredPropertyException, PaymentException {
        if (StringUtils.isBlank(transferUuid)) {
            throw new RequiredPropertyException("Missing transfer uuid ");
        }
        Transfer t = transferDao.loadPreparedTransferDetailForEvent(transferUuid);
        // Nastavime stav transfered
        t.setState(TransferState.TRANSFERED);
        t.appendToNote("Transfered");
        // Update
        transferDao.update(t);
        // vytvoreni faktury
        Invoice invoice = new Invoice();
        invoice.setConfirmationSource(ConfirmationChannel.SYSTEM);
        invoice.setOrder(t.getCommissionOrder());
        Date now = DateProvider.getCurrentDate();
        if (toPreviousMonth) {
            invoice.setIssueDate(invoice.getOrder().getPurchaseDate());
            invoice.setTaxableFulfillmentDate(invoice.getOrder().getPurchaseDate());
        } else {
            invoice.setIssueDate(now);
            invoice.setTaxableFulfillmentDate(now);
        }
        invoice.setInvoiceNumber(invoiceDao.nextInvoiceCommissionNumber(t.getCommissionOrder().getUuid()));
        t.getCommissionOrder().setInvoice(invoice);
        // potvrzeni faktury
        orderService.updateOrderToConfirmedByAdministrator(t.getCommissionOrder(), false, false, "Transfered order");
        return t;
    }

    @Override
    public Transfer updateTransferState(String transferUuid, TransferState toState)
            throws NoRecordException, RequiredPropertyException, PaymentException {
        if (StringUtils.isBlank(transferUuid) || toState == null) {
            throw new RequiredPropertyException("Missing transfer uuid or state");
        }

        Transfer t = transferDao.loadPlainTransfer(transferUuid, true);
        if (t.getState().equals(TransferState.ACTIVE)) {
            String msg = String.format("Trying to update active transfer %s. Update transfer to state {} by using proper method.", t, TransferState.CLOSED_AND_PREPARED.name());
            logger.warn(msg);
            throw new PaymentException(msg);
        }

        if (t.getState().equals(TransferState.CLOSED_AND_PREPARED) && toState.equals(TransferState.TRANSFERED)) {
            t.setTransferDate(DateProvider.getCurrentDate());
            // zavolame asi invoice service.??


        }

        t.setState(toState);
        transferDao.update(t);
        return t;
    }

    @Override
    public void updateTransferAddOrder(Order order) throws NoRecordException, RequiredPropertyException {
        if (order == null || !order.isConfirmed()) {
            throw new RequiredPropertyException("Order is in bad state! Should be PAID");
        }

        if (!order.getState().equals(OrderState.CONFIRMED_ZERO)) {
            Transfer t = loadActualTransferWithLock(order.getSupplier().getUuid());
            if (!t.getOrders().contains(order)) {
                // pridame do transferu
                t.getOrders().add(order);
                // pripicitame fees
                t.addToFeesSum(order.getUnitFee());
                t.addToFeesSum(order.getShareFee());
                // pripicitame celkovu sumu - ale len ak je to potvrdene bankov
                if (!order.getState().equals(OrderState.CONFIRMED_ORGANIZATOR)) {
                    t.addToTotalSum(order.getPrice());
                }
                // pripocitame poplatky tretim stranam
                t.addToThirdPartyFeesSum(order.getThirdPartyFee());
                transferDao.update(t);
            }
        }
    }

    private SimpleDateFormat getNoteFormat() {
        return new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
    }

    private String getUpdateNote(String note, String prefix) {
        StringBuilder sb = new StringBuilder(StringUtils.isBlank(note) ? "" : note);
        sb.append("\n").append(prefix).append(":").append(getNoteFormat().format(new Date()));
        return sb.toString();
    }

    @Override
    public void updateTransferRemoveOrder(Order order) throws NoRecordException, RequiredPropertyException {
        if (order == null && order.isConfirmed()) {
            throw new RequiredPropertyException("Order is in bad state! Should be PAID");
        }


        if (!order.getState().equals(OrderState.CONFIRMED_ZERO)) {
            Transfer t = loadActualTransferWithLock(order.getSupplier().getUuid());
            // pridame do transferu
            t.getOrders().remove(order);
            // pripicitame fees
            t.removeFromFeesSum(order.getUnitFee());
            t.removeFromFeesSum(order.getShareFee());
            // pripicitame celkovu sumu - ale len ak je to potvrdene bankov
            if (!order.getState().equals(OrderState.CONFIRMED_ORGANIZATOR)) {
                t.removeFromTotalSum(order.getPrice());
            }
            // pripocitame poplatky tretim stranam
            t.removeFromThirdPartyFeesSum(order.getThirdPartyFee());
            transferDao.update(t);
        }
    }

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    @Override
    public int loadTransfersCount(TransferState inState) {
        return transferDao.loadTransfersCount(inState);
    }

    @Override
    public int loadTransfersCount(String eventProfileUuid, TransferState inState) {
        return transferDao.loadTransfersCount(eventProfileUuid, inState);
    }

    @Override
    public int loadTransfersCount(String eventProfileUuid) {
        return transferDao.loadTransfersCount(eventProfileUuid);
    }

    @Override
    public int loadTransfersCountByEventUuid(String eventUuid) {
        return transferDao.loadTransfersCountByEventUuid(eventUuid);
    }

    @Override
    public Transfer loadActualTransferWithLock(String eventPaymentUuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventPaymentUuid)) {
            throw new RequiredPropertyException("Missing event payment profile");
        }
        try {
            return transferDao.loadActualTransferWithRowLock(eventPaymentUuid);
        } catch (NoRecordException ex) {
            // toto nastane pri prvej zaplatenej objednavke
            return createAndPrepareNewTransfer(eventPaymentUuid);
        }
    }

    @Override
    public Transfer loadActualTransfer(String eventUuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Missing event uuid");
        }
        try {
            return transferDao.loadActualTransfer(eventUuid);
        } catch (NoRecordException ex) {
            return createAndPrepareNewTransfer(eventUuid);
        }
    }

    @Override
    public Transfer loadTransferWithOrders(String transferUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(transferUuid)) {
            throw new RequiredPropertyException("Missing transfer uuid");
        }
        return transferDao.loadTransferWithOrders(transferUuid);

    }

    @Override
    public Transfer loadTransferDetail(String transferUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(transferUuid)) {
            throw new RequiredPropertyException("Missing event uuid or transfer uuid");
        }
        return transferDao.loadTransferWithOrders(transferUuid);

    }

    @Override
    public Transfer loadTransferWithCommissionDetail(String transferUuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(transferUuid)) {
            throw new RequiredPropertyException("Missing event uuid or transfer uuid");
        }
        return transferDao.loadTransferWithCommissionDetail(transferUuid);
    }

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    @Override
    public List<Transfer> listTransfers(TransferState inState, int start, int count) {
        return transferDao.listTransfers(inState, start, count);
    }

    @Override
    public List<Transfer> listTransfers(String eventUuid, TransferState inState, int start, int count) {
        return transferDao.listTransfersByEventPaymentProfile(eventUuid, inState, start, count);
    }

    @Override
    public List<Transfer> listTransfers(String eventUuid, int start, int count) {
        return transferDao.listTransfersByEventPaymentProfile(eventUuid, start, count);
    }

    @Override
    public List<Transfer> listTransfersByEventUuid(String eventUuid, int startItem, int itemsPerPage) {
        return transferDao.listTransfersByEventUuid(eventUuid, startItem, itemsPerPage);
    }

    /*------------------------------------------------------------------------*/
    /* ---  H E L P E R  --- */
    /*------------------------------------------------------------------------*/
    private Transfer createAndPrepareNewTransfer(String eventPaymentProfileUuid) throws NoRecordException, RequiredPropertyException {
        EventPaymentProfile epp = eventService.loadEventPaymentProfile(eventPaymentProfileUuid);
        logger.info("Creating new transfer for {}", epp);
        Transfer t = new Transfer();
        t.setEventPaymentProfile(epp);
        t.setRegular(true);
        t.setState(TransferState.ACTIVE);
        t.setUuid(UuidProvider.getUuid());
        t.setOrders(new ArrayList<Order>());
        transferDao.create(t);
        return t;
    }

    /**
     * Metoda spravuje pripradene fakture k transferu a vytvori commission fakturu
     * @param t
     */
    private Collection<OrderItem> processTransferInvoices(Transfer t) {
        Map<String, OrderItem> transferInvoiceItems = Maps.newHashMap();

        BigDecimal payPalFees = BigDecimal.ZERO;
        BigDecimal bankFees = BigDecimal.ZERO;

//        for (Invoice invoice : t.getInvoices()) {
//            switch (invoice.getPaymentType()) {
//                case BANK:
//                    bankFees = bankFees.add(invoice.getFeesSum()).setScale(2, RoundingMode.HALF_UP);
//                    break;
//                case PAY_PAL:
//                    payPalFees = payPalFees.add(invoice.getFeesSum()).setScale(2, RoundingMode.HALF_UP);
//                    break;
//            }
//            // spracujeme aj faktury ktore su ZERO.
//            for (OrderItem invoiceItem : invoice.getItems()) {
//                // Nacitame si produkt podla uuid produktu
//                OrderItem commissionItem = transferInvoiceItems.get(getInvoiceItemProductUuid(invoiceItem));
//                // Ak takyto produkt este nemame v proviziach tak si ho vytvorime
//                if (commissionItem == null) {
//                    commissionItem = createCommissionInvoiceItem(invoiceItem, t);
//                    // Prilozime tento item do mapy itemov
//                    transferInvoiceItems.put(getInvoiceItemProductUuid(invoiceItem), commissionItem);
//                }
//                // len zvysime quantitu
//                commissionItem.setQuantity(commissionItem.getQuantity() + invoiceItem.getQuantity());
//            }
//        }


        OrderItem commissionItem = new OrderItem();
        commissionItem.setDiscount(0);
//        commissionItem.setProduct((Product) getSystemPropertyService().getInjected(SystemProperties.ACMC_PRODUCT_PAY_PAL_FEE_UUID));
        commissionItem.setName(commissionItem.getProduct().getName());
        commissionItem.setQuantity(1);
        commissionItem.setUnitPrice(payPalFees);
//        commissionItem.setVatRate(SystemProperties.ACMC_COMMISSION_VAT_RATE);
        transferInvoiceItems.put(commissionItem.getName(), commissionItem);
        return transferInvoiceItems.values();
    }

    /**
     * Vytvori novu zaznam pre commission invoice. Vtgeneruje meno
     * Commission - [NAME], nastavi quantity na 0 a vyposita proviziu za dany item.
     *
     * @param r
     * @param t
     * @return
     */
    private OrderItem createCommissionInvoiceItem(OrderItem r, Transfer t) {
        // Nasjkor prepocitame vsetky vlastnosti
        //InvoiceItemRecord itemRecord = new InvoiceItemRecord(r);
        // vytvorime zaznam do faktury
        OrderItem commissionItem = new OrderItem();
        // ziadna zlava - default
        commissionItem.setDiscount(0);
        // Nazveme tento item: Commission - [NAME]
//        Product commissionProduct = getSystemPropertyService().getInjected(SystemProperties.ACMC_PRODUCT_COMMISSION_UUID);
//        commissionItem.setProduct(commissionProduct);
//        commissionItem.setName(commissionProduct.getName() + itemRecord.getName());
        // defaultne quantita nula
        commissionItem.setQuantity(0);
//        ProductCategoryProcessor p = getCommissionProcessorService().getProcessor(itemRecord.getProductCategory(), t.getEvent().getCurrency());
//        // Provize dame ze su spolu tj. unit_cahrge + percent_charge
//        commissionItem.setUnitPrice(getCommissionProcessorService().getCommisionForOneUnit(p, itemRecord));
//        // Dan je pre ACEMCEE
//        commissionItem.setVatRate(SystemProperties.ACMC_COMMISSION_VAT_RATE);

        return commissionItem;
    }

    /**
     * Ziskame z invoice item string ktory bude unikatny vzhladom k produktu a jeho cene.
     * @param item
     * @return
     */
    private String getInvoiceItemProductUuid(OrderItem item) {
        return item.getProduct().getUuid() + item.getUnitPrice().toPlainString() + item.getDiscount();
    }

    /**
     * Nacita defau;tny - prvy , payment profil pro dany event.
     *
     * @param event
     * @return
     * @throws NoRecordException
     */
    private PaymentProfileRevision loadEventDefaultPaymentProfile(Event event) throws NoRecordException {
//        if (event.getProfiles().isEmpty()) {
//            throw new NoRecordException("Event has no profiles!");
//        }
//        return event.getProfiles().get(0).getActualRevision();
        return null;
    }

    /*------------------------------------------------------------------------*/
    /* ---  GETTER / SETTER  --- */
    /*------------------------------------------------------------------------*/
    /**
     * @return the TransferDao
     */
    public TransferDao getTransferDao() {
        return transferDao;
    }

    /**
     * @param TransferDao the TransferDao to set
     */
    public void setTransferDao(TransferDao TransferDao) {
        this.transferDao = TransferDao;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @return the acemceeService
     */
    public AcemceeService getAcemceeService() {
        return acemceeService;
    }

    /**
     * @param acemceeService the acemceeService to set
     */
    public void setAcemceeService(AcemceeService acemceeService) {
        this.acemceeService = acemceeService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setInvoiceDao(InvoiceDao invoiceDao) {
        this.invoiceDao = invoiceDao;
    }

    @Override
    public void updateTransferCalculateFees(String transferUuid) throws Exception {
        logger.info("Running transfer recalculate");
        Transfer transfer = transferDao.loadTransferWithOrdersDetailed(transferUuid);

        // vynulujeme hodnoty v transferu
        transfer.setTotalSum(BigDecimal.ZERO);
        transfer.setFeesSum(BigDecimal.ZERO);
        transfer.setThirdPartyFeesSum(BigDecimal.ZERO);

        List<Order> orders = PaymentUtils.transferOrdersWithoutDuplicity(transfer.getOrders());
        for (Order order : orders) {
            // prepocitame castky poplatku na objednavce
            order.setUnitFee(BigDecimal.ZERO);
            order.setShareFee(BigDecimal.ZERO);
            feeStrategyService.processFees(order);
            orderDao.update(order);

            // pripocitame fees
            transfer.addToFeesSum(order.getUnitFee());
            transfer.addToFeesSum(order.getShareFee());
            // pripocitame celkovou sumu - ale jen pokud je to potvrdene bankou/paypalem
            if (!order.getState().equals(OrderState.CONFIRMED_ORGANIZATOR)) {
                transfer.addToTotalSum(order.getPrice());
            }
            // pripocitame poplatky tretim stranam
            transfer.addToThirdPartyFeesSum(order.getThirdPartyFee());
        }

        // updavime poplatkovou objednavku
        StringBuilder sb = new StringBuilder("Transfer order recalculated: transferId(");
        sb.append(transfer.getId());
        sb.append(")");

        EventPaymentProfile epp = transfer.getEventPaymentProfile();
        EventPaymentProfile acmcpp = acemceeService.loadEventPaymentProfile(epp.getCurrency());
        ProductOrder feesOrderItem = new ProductOrder(acemceeService.loadCommissionProductUuid(epp.getCurrency()), 1, 0, transfer.getFeesSum());
        ProductOrder thirdPartyFeesOrderItem = new ProductOrder(acemceeService.loadThirdPartyFeeProductUuid(epp.getCurrency()), 1, 0, transfer.getThirdPartyFeesSum());

        Order orderToCancel = transfer.getCommissionOrder();
        orderService.cancelOrder(orderToCancel.getUuid(), OrderState.CANCELED_SYSTEM, sb.toString(), false);

        Order order = orderService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(feesOrderItem, thirdPartyFeesOrderItem),
                epp.getPaymentProfile().getActualRevision(), acmcpp);
        transfer.setCommissionOrder(order);

        transferDao.update(transfer);
        logger.info("Transfer recalculate finished");
    }
}
