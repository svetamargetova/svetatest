/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo.enums;

/**
 *
 * @author palo
 */
public enum OrderState {
    ACTIVE,  // Objednavka je aktivna    
    CANCELED_SYSTEM, // Objednavka je zrusena z dovodu vyprsania platnosti
    CANCELED_OWNER, // Objednavka je zrusena uzivatelom
    CANCELED_ORGANIZATOR, // Objednavka je zrusena organizatorom
    CONFIRMED_ORGANIZATOR, // Objednavka je potvrdena organizatorem
    CONFIRMED_ZERO, // Objednavka je potvrdena organizatorem
    CONFIRMED_SYSTEM, // Objednavka je potvrdena nejakym kanalem
    WAITING_TO_APPROVE, // Objednavka caka na schvalenie. Pouzitie typicky ak prislo malo penazi
    ORDER_REFUNDED; // Objednavka bola refundovana
    
   

    public Integer toId(){
        switch(this){
            case ACTIVE: return 1;
            case CANCELED_SYSTEM: return 2;
            case CANCELED_OWNER: return 3;
            case CANCELED_ORGANIZATOR: return 4;
            case CONFIRMED_ORGANIZATOR: return 5;
            case CONFIRMED_SYSTEM: return 6;            
            case CONFIRMED_ZERO: return 7;            
            case WAITING_TO_APPROVE: return 8;            
            case ORDER_REFUNDED: return 9;            
            default: return null;
        }
    }

    public static OrderState fromId(Integer i){        
        switch(i){
            case 1: return ACTIVE;
            case 2: return CANCELED_SYSTEM;
            case 3: return CANCELED_OWNER;
            case 4: return CANCELED_ORGANIZATOR;
            case 5: return CONFIRMED_ORGANIZATOR;
            case 6: return CONFIRMED_SYSTEM;      
            case 7: return CONFIRMED_ZERO;
            case 8: return WAITING_TO_APPROVE;
            case 9: return ORDER_REFUNDED;
            default: return null;
        }
    }
    
}
