package consys.payment.export.pdf;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pepa
 */
public class Pdf {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(Pdf.class);
    // instance
    private static Pdf instance = new Pdf();
    // konstanty
    public static final String ACCOUNT_NO = "accountNo";
    public static final String ALREADY_PAID = "alreadyPaid";
    public static final String BANK = "bank";
    public static final String COMP_NO = "compNo";
    public static final String CONSTANT_CODE = "constantCode";
    public static final String CUSTOMER = "customer";
    public static final String DESCRIPTION = "description";
    public static final String DUE_DATE = "dueDate";
    public static final String EXCLUDING_VAT = "excludingVat";
    public static final String FOOT_TEXT = "footText";
    public static final String IBAN = "iban";
    public static final String INVOICE_ACCOUNTING_DOCUMENT = "invoiceAccountingDocument";
    public static final String INVOICE_TAX_DOCUMENT = "invoiceTaxDocument";
    public static final String ISSUE_DATE = "issueDate";
    public static final String ITEM_NO = "itemNo";
    public static final String NET_PRICE = "netPrice";
    public static final String NO = "no";
    public static final String NON_PAYER_OF_VAT = "nonPayerOfVAT";
    public static final String OF = "of";
    public static final String ORDER_NO = "orderNo";
    public static final String PAGE = "page";
    public static final String PAYMENT_DETAILS = "paymentDetails";
    public static final String PAYMENT_METHOD = "paymentMethod";
    public static final String PRO_FORMA_INVOICE = "proFormaInvoice";
    public static final String PURCHASE_DATE = "purchaseDate";
    public static final String QUANTITY = "quantity";
    public static final String SPECIFIC_CODE = "specificCode";
    public static final String SUBTOTAL = "subtotal";
    public static final String SUPPLIER = "supplier";
    public static final String SWIFT_CODE = "swiftCode";
    public static final String TAKEPLACE_INVOICE = "takeplaceInvoice";
    public static final String TAKEPLACE_PRO_FORMA_INVOICE = "takeplaceProFormaInvoice";
    public static final String TAXABLE_FULFILMENT_DATE = "taxableFulfilmentDate";
    public static final String TOTAL_AMOUNT = "totalAmount";
    public static final String TOTAL_DUE = "totalDue";
    public static final String UNIT_PRICE = "unitPrice";
    public static final String VARIABLE_CODE = "variableCode";
    public static final String VAT_AMOUNT = "vatAmount";
    public static final String VAT_BASE = "vatBase";
    public static final String VAT_CHARGED = "vatCharged";
    public static final String VAT_NO = "vatNo";
    public static final String VAT_OVERVIEW = "vatOverview";
    public static final String VAT_RATE = "vatRate";
    public static final String WE_INVOICE_AT = "weInvoiceAt";
    // data
    private static Map<String, String> texts;

    private Pdf() {
        texts = new HashMap<String, String>();

        try {
            InputStream inputStream = getClass().getResourceAsStream("Pdf.properties");

            // nacteni properties
            Properties properties = new Properties();
            properties.load(inputStream);

            for (Entry<Object, Object> entry : properties.entrySet()) {
                texts.put(entry.getKey().toString(), entry.getValue().toString());
            }
        } catch (IOException ex) {
            logger.error("Reading Pdf.properties", ex);
        }
    }

    public static synchronized Pdf get() {
        return instance;
    }

    public static synchronized String get(String key) {
        return instance.value(key);
    }

    /** vrací hodnoty, jako klic pouzivat konstanty teto tridy */
    public String value(String key) {
        return texts.get(key);
    }
}
