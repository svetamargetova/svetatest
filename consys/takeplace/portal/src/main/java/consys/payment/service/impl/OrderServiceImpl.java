package consys.payment.service.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import consys.common.utils.BigDecimalUtils;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.payment.SystemProperties;
import consys.payment.bo.*;
import consys.payment.bo.enums.*;
import consys.payment.bo.thumb.ProductOrder;
import consys.payment.dao.ApproveOrderDao;
import consys.payment.dao.InvoiceDao;
import consys.payment.dao.OrderDao;
import consys.payment.service.*;
import consys.payment.service.exception.*;
import consys.payment.service.fees.FeeStrategyService;
import consys.payment.utils.InvoiceRecord;
import consys.payment.utils.PaymentUtils;
import consys.payment.utils.ProfileUtils;
import consys.payment.utils.ProformaProvider;
import consys.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.delegate.DelegateOrderStateChangedRequest;
import consys.payment.ws.delegate.DelegateOrderStateChangedResponse;
import consys.payment.ws.delegate.State;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author palo
 */
public class OrderServiceImpl extends Service implements OrderService {

    private OrderDao orderDao;
    private ApproveOrderDao approveOrderDao;
    private InvoiceDao invoiceDao;
    private EventService eventService;
    private SystemPropertyService propertyService;
    private AcemceeService acemceeService;
    private ProductService productService;
    private ProformaProvider proformaProvider;
    private PaymentProfileService paymentProfileService;
    private UserService userService;
    private FeeStrategyService feeStrategryService;
    private TransferService transferService;
    private DelegateWebService delegateWebService;

    /* ------------------------------------------------------------------------ */
    /* --- C R E A T E --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public Order createOrder(String orderUuid, List<ProductOrder> items, PaymentProfileRevision customer, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException {

        if (StringUtils.isBlank(orderUuid) || CollectionUtils.isEmpty(items) || customer == null || supplier == null) {
            throw new RequiredPropertyException("Missing paramters");
        }

        // test na to ci je event uz aktivovany
        if (!supplier.isActive()) {
            throw new EventNotActiveException("Event is not active yet!");
        }

        // Spravi si list produktov bez duplikatov
        List<String> productUuids = Lists.newArrayListWithCapacity(items.size());
        for (ProductOrder item : items) {
            if (!productUuids.contains(item.getProductUuid())) {
                productUuids.add(item.getProductUuid());
            }
        }

        List<Product> products = getProductService().listProducts(productUuids);
        // test na to ze vsecky produkty maju rovnaky platobny profil - tj. aj event
        Product referenceProduct = products.get(0);
        // resolvujeme kategoriu
        ProductCategory category = referenceProduct.getCategory();
        for (Product product : products) {
            if (!product.getProfile().getId().equals(referenceProduct.getProfile().getId())) {
                throw new ProductIncompatibilityException("Products has differencet payment profiles! " + product.getProfile() + " vs. " + referenceProduct.getProfile());
            }
        }
        logger.debug("Creating new order {}", orderUuid);

        Date now = DateProvider.getCurrentDate();
        Order order = new Order();
        order.setUuid(orderUuid);
        order.setOrderType(category);
        order.setCustomer(customer);
        order.setSupplier(supplier);
        order.setPurchaseDate(now);
        order.setDueDate(DateUtils.addDays(now, SystemProperties.INVOICE_DUE_DATE));
        order.setUnitFee(BigDecimal.ZERO);
        order.setShareFee(BigDecimal.ZERO);
        order.setPrice(BigDecimal.ZERO);
        order.setOrderNumber(getProformaProvider().getNextProformaNumber());

        InvoiceProcessType finalVatProcessType;
        if (order.getCustomer().getCountry() == null) {
            finalVatProcessType = order.getSupplier().getVatType();
        } else {
            InvoiceProcessType customerProcessType = ProfileUtils.resolveProcessType(order.getCustomer().getCountry());
            finalVatProcessType = PaymentUtils.resolveFinalProcessType(order.getSupplier().getVatType(), customerProcessType);
        }

        final boolean czechProcessing = InvoiceProcessType.VAT_CZ.equals(finalVatProcessType);
        final boolean noVatPayerProcessing = InvoiceProcessType.VAT_AD.equals(finalVatProcessType);
        final boolean ticketCategory = ProductCategory.TICKET.equals(category);

        Product product = null;
        Builder<OrderItem> invoiceItems = ImmutableList.builder();

        for (ProductOrder itemOrder : items) {
            // Najdeme product
            for (Product p : products) {
                if (p.getUuid().equalsIgnoreCase(itemOrder.getProductUuid())) {
                    logger.debug("Ordering {}", p);
                    product = p;
                    break;
                }
            }
            if (product == null) {
                throw new NoRecordException("Product '" + itemOrder.getProductUuid() + "' has not been found!");
            }

            // Vypocet
            if (itemOrder.getDiscount() > 100 || itemOrder.getDiscount() < 0) {
                throw new RequiredPropertyException("Discount is out of bounds 0 <= discount <= 100 : " + itemOrder.getDiscount());
            }

            // Vytvorime invoice item
            OrderItem item = new OrderItem();
            item.setName(product.getName());
            item.setProduct(product);
            item.setQuantity(itemOrder.getQuatity());

            if (noVatPayerProcessing && ticketCategory) {
                // neplatce a jde o vstupenku
                item.setUnitPrice(itemOrder.getPrice() != null ? itemOrder.getPrice() : product.getPrice());
                item.setVatRate(BigDecimal.ZERO);
            } else if (czechProcessing || ticketCategory) {
                // ceske zpracovani nebo vstupenka
                item.setUnitPrice(itemOrder.getPrice() != null ? itemOrder.getPrice() : product.getPrice());
                item.setVatRate(product.getVatRate());
            } else {
                // vsechno ostatni
                item.setUnitPrice(itemOrder.getPrice() != null ? itemOrder.getPrice() : product.getPrice());
                item.setVatRate(product.getVatRate());
                /*BigDecimal price = itemOrder.getPrice() != null ? itemOrder.getPrice() : product.getPrice();
                item.setUnitPrice(countUnitPriceWithoutVat(price, product.getVatRate()));
                item.setVatRate(BigDecimal.ZERO);*/
            }

            item.setDiscount(itemOrder.getDiscount());
            item.setOrder(order);
            invoiceItems.add(item);
        }
        order.setItems(invoiceItems.build());

        // Vytvorime vypocet 
        InvoiceRecord record = new InvoiceRecord(order);
        logger.debug("{} total: {} subtotal: {}", new Object[]{order, record.getTotal(), record.getSubTotal()});
        order.setPrice(record.getTotal());


        if (order.getPrice().compareTo(BigDecimal.ZERO) == 0) {
            order.setState(OrderState.CONFIRMED_ZERO);
            order.setConfirmedDate(DateProvider.getCurrentDate());
        } else {
            order.setState(OrderState.ACTIVE);
        }


        if (!order.isZeroConfirmed()) {
            try {
                ProfileUtils.validateProfile(customer, false);
            } catch (IbanFormatException ex) {
                // ak tam neni iban tak je zle throw
                throw new EventProfileNotFilledException("Event profile Iban missing!");
            } catch (RequiredPropertyException ex) {
                // ak neni nejaka property throw
                throw new EventProfileNotFilledException("Event profile missing required properties ", ex);
            }
        }

        logger.info("Creating {}", order);
        orderDao.create(order);
        return order;
    }

    /**
     * Vrati jednotkovu cenu bez VAT ktora je nasledne ponizena o zlavu.
     *
     * @return
     */
    public BigDecimal countUnitPriceWithoutVat(BigDecimal unitPrice, BigDecimal vatRate) {
        // velkost vatu      
        final BigDecimal vatAmount = unitPrice.multiply(BigDecimalUtils.computeVatCoefficient(vatRate), BigDecimalUtils.DEFAULT);

        // odpocet vatu od unit
        return unitPrice.subtract(vatAmount, BigDecimalUtils.DEFAULT);
    }

    @Override
    public Order createUserOrder(String orderUuid, List<ProductOrder> items, String userUuid, String userPaymentProfileUuid, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException {
        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(userPaymentProfileUuid)) {
            throw new RequiredPropertyException("Missing userUuid or payment profile uuid");
        }
        PaymentProfile profile = paymentProfileService.loadUserProfile(userUuid, userPaymentProfileUuid);
        return createOrder(orderUuid, items, profile.getActualRevision(), supplier);
    }

    @Override
    public Order createUserOrder(String orderUuid, List<ProductOrder> items, String userUuid, String userPaymentProfileUuid, PaymentProfileRevision customerProfile, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyException("Missing userUuid");
        }
        getUserService().updateOrCreateNewPaymentProfileRevision(userUuid, userPaymentProfileUuid, customerProfile);
        return createOrder(orderUuid, items, customerProfile, supplier);
    }

    @Override
    public Order createUserOrder(String orderUuid, List<ProductOrder> items, String userUuid, EventPaymentProfile supplier)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException, IbanFormatException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyException("Missing userUuid");
        }
        // todo, test na to ci je ta objednavka nula.
        User user = userService.loadUserWithProfiles(userUuid);
        if (CollectionUtils.isEmpty(user.getProfiles())) {
            throw new EventProfileNotFilledException(user + " has no event profile");
        }
        return createOrder(orderUuid, items, user.getProfiles().get(0).getActualRevision(), supplier);
    }

    @Override
    public Order createOrder(String orderUuid, List<ProductOrder> items, String customerProfileUuid, String supplierProfileUuid)
            throws NoRecordException, RequiredPropertyException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException {
        if (StringUtils.isBlank(customerProfileUuid) || StringUtils.isBlank(supplierProfileUuid)) {
            throw new RequiredPropertyException("Missing customer or supplier profile UUIDs");
        }
        EventPaymentProfile supplier = eventService.loadEventPaymentProfile(supplierProfileUuid);
        PaymentProfile customer = getPaymentProfileService().loadPaymentProfile(customerProfileUuid);
        return createOrder(orderUuid, items, customer.getActualRevision(), supplier);
    }

    @Override
    public Order createOrder(String orderUuid, List<ProductOrder> items, String customerProfileUuid, EventPaymentProfile supplierProfile)
            throws NoRecordException, RequiredPropertyException, IbanFormatException, EventNotActiveException, ProductIncompatibilityException, EventProfileNotFilledException {
        if (StringUtils.isBlank(customerProfileUuid)) {
            throw new RequiredPropertyException("Missing customer profile UUIDs");
        }
        PaymentProfile customer = getPaymentProfileService().loadPaymentProfile(customerProfileUuid);
        return createOrder(orderUuid, items, customer.getActualRevision(), supplierProfile);
    }

    @Override
    public Order createLicenseOrder(String eventUuid, String profileUuid, PaymentProfileRevision revision)
            throws NoRecordException, RequiredPropertyException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, IbanFormatException {
        if (StringUtils.isBlank(eventUuid) || StringUtils.isBlank(profileUuid) || revision == null) {
            throw new RequiredPropertyException("Required properties");
        }
        EventPaymentProfile epp = getEventService().loadEventPaymentProfile(profileUuid);
        if (!epp.getEvent().getUuid().equalsIgnoreCase(eventUuid)) {
            throw new RequiredPropertyException("EventPaymentProfile event uuid " + epp.getEvent().getUuid() + " differs from identifier from paramter: " + eventUuid);
        }
        getEventService().updateEventPaymentProfileRevision(epp, revision);
        return orderLicense(epp);
    }

    @Override
    public Order createLicenseOrder(String eventUuid, String profileUuid)
            throws NoRecordException, RequiredPropertyException, EventProfileNotFilledException, EventLicenseAlreadyPurchased {

        if (StringUtils.isBlank(eventUuid) || StringUtils.isBlank(profileUuid)) {
            throw new RequiredPropertyException("Missing uuids");
        }

        EventPaymentProfile epp = getEventService().loadEventPaymentProfile(profileUuid);
        if (!epp.getEvent().getUuid().equalsIgnoreCase(eventUuid)) {
            throw new RequiredPropertyException("EventPaymentProfile event uuid " + epp.getEvent().getUuid() + " differs from identifier from paramter: " + eventUuid);
        }
        return orderLicense(epp);
    }

    private Order orderLicense(EventPaymentProfile eventPaymentProfile) throws RequiredPropertyException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, NoRecordException {

        // test na to ci je uz licenzia zakupena
        if (eventPaymentProfile.getEvent().getLicenseOrder() != null) {
            throw new EventLicenseAlreadyPurchased("License is already ordered for this event");
        }

        try {
            ProfileUtils.validateProfile(eventPaymentProfile.getPaymentProfile().getActualRevision(), false);
        } catch (IbanFormatException ex) {
            // ak tam neni iban tak je zle throw
            throw new EventProfileNotFilledException("Event profile Iban missing!");
        } catch (RequiredPropertyException ex) {
            // ak neni nejaka property throw
            throw new EventProfileNotFilledException("Event profile missing required properties ", ex);
        }
        EventPaymentProfile acemceeProfile = acemceeService.loadEventPaymentProfile(eventPaymentProfile.getCurrency());
        Product acemceeLicenseProduct = acemceeService.loadLicenseProduct(eventPaymentProfile.getCurrency());
        ProductOrder licenseOrder = new ProductOrder(acemceeLicenseProduct.getUuid(), 1, 0);
        try {
            Order order = createOrder(eventPaymentProfile.getEvent().getUuid(), Lists.newArrayList(licenseOrder), eventPaymentProfile.getPaymentProfile().getActualRevision(), acemceeProfile);
            eventService.updateEventLicenseOrder(eventPaymentProfile.getEvent(), order);
            activateIfIsZeroOrder(order);
            return order;
        } catch (EventNotActiveException ena) {
            throw new NoRecordException("wtf", ena);
        } catch (ProductIncompatibilityException ena) {
            throw new NoRecordException("wtf", ena);
        }
    }

    /** aktivuje event pokud byla licence za 0 */
    private void activateIfIsZeroOrder(Order order) throws NoRecordException, RequiredPropertyException {
        if (order.getState().equals(OrderState.CONFIRMED_ZERO)) {
            String generatedClientNumber = invoiceDao.nextInvoiceClientNumber();
            generateAndSetInvoiceNumber(generatedClientNumber, order);

            try {
                Event event = getEventService().loadEventByLicenseOrderNumber(order.getOrderNumber());
                getEventService().updateEventActivateNow(event, generatedClientNumber);
            } catch (EventAlreadyActiveException ex) {
                // nehrozi, licence byla prave zakoupena
            }

            // zavolam na administraci, at se aktivuje event
            DelegateOrderStateChangedRequest req = delegateWebService.OBJECT_FACTORY.createDelegateOrderStateChangedRequest();
            req.setOrderUuid(order.getUuid());
            req.setToState(State.CONFIRMED);
            getDelegateWebService().delegateAdminOrderStateChanged(req);
        }
    }

    /* ------------------------------------------------------------------------ */
    /* --- U P D A T E --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public List<Invoice> updateOrdersToConfirmed(List<Order> orders, Transaction transaction)
            throws RequiredPropertyException, NoRecordException {

        if (orders.isEmpty() || transaction == null) {
            throw new RequiredPropertyException("Orders are empty or missing transaction");
        }

        ImmutableList.Builder<Invoice> invoices = ImmutableList.builder();
        BigDecimal price = BigDecimal.ZERO;
        StringBuilder sb = new StringBuilder("Objednavky: ");

        for (Order order : orders) {
            price = price.add(order.getPrice(), BigDecimalUtils.DEFAULT);
            sb.append(order.getOrderNumber());
            sb.append(" ");
        }

        int cmp = transaction.getAmount().compareTo(price);
        if (cmp == 0) {
            // ok oznacime teda secky za zaplatene
            String note = String.format("%s potvrdzuje niekolko objednavok naraz: %s", transaction, sb.toString());
            for (Order order : orders) {
                invoices.add(confirmOrderWithTransaction(order, transaction, false, note));
            }
        } else {
            // oznacime ich ze nemaju dostatcne peniaze plus sa autoamticky zisttia ostatne veci
            String note;
            if (cmp > 0) {
                note = String.format("%s obsahuje vyssiu hodnotu k potvrdeniu vsetkych objednavok: ", transaction, sb.toString());
            } else {
                note = String.format("%s neobsahuje dostatocnu hodnotu na potvrdenie vsetkych objednavok: ", transaction, sb.toString());
            }

            for (Order order : orders) {
                invoices.add(confirmOrderWithTransaction(order, transaction, true, note));
            }
        }
        return invoices.build();
    }

    @Override
    public Invoice updateOrderToConfirmed(Order order, Transaction transaction)
            throws RequiredPropertyException, NoRecordException {

        if (order == null || transaction == null) {
            throw new RequiredPropertyException("Missing order or transaction");
        }

        boolean approve = false;
        String note = null;

        int cmp = transaction.getAmount().compareTo(order.getPrice());
        if (cmp > 0) {
            approve = true;
            note = String.format("%s obsahuje viacej penazi ako by malo prist.", transaction);
        } else if (cmp < 0) {
            approve = true;
            note = String.format("%s obsahuje menej penazi ako by malo prist.", transaction);
        }

        if (order.getDueDate().before(DateProvider.getCurrentDate())) {
            approve = true;
            note = String.format("%s plati objednavku ktora je po splatnosti.", transaction);
        }

        return confirmOrderWithTransaction(order, transaction, approve, note);
    }

    @Override
    public Order updateOrderToConfirmedByOrganizator(String orderUuid, String userUuid, String note)
            throws NoRecordException, RequiredPropertyException {

        if (StringUtils.isBlank(orderUuid)) {
            throw new RequiredPropertyException("Missing orderUuid");
        }
        Order order = loadOrderByUuid(orderUuid);
        return updateOrderToConfirmedByOrganizator(order, userUuid, note);
    }

    @Override
    public Order updateOrderToConfirmedByOrganizator(Order order, String userUuid, String note1)
            throws RequiredPropertyException, NoRecordException {

        if (StringUtils.isBlank(userUuid) || order == null) {
            throw new RequiredPropertyException("Missing user uuid or order");
        }

        String note2;
        boolean approve;
        if (order.isCanceled()) {
            note2 = String.format("%s je oznacena ako zrusena ale uzivatel(%s) ju oznacil ako potvrdenu", order, userUuid);
            approve = true;
        } else if (order.isConfirmed()) {
            note2 = String.format("%s je oznacena ako potvrdena ale uzivatel(%s) ju oznacil ako potvrdenu", order, userUuid);
            approve = true;
        } else if (order.getState().equals(OrderState.WAITING_TO_APPROVE)) {
            note2 = String.format("%s je oznacena v stave WAITING_TO_APPROVE ale je opat uzivatel(%s) potvrdena.", order, userUuid);
            approve = true;
        } else {
            note2 = String.format("Uzivatel(%s) oznacil objednavku za potvrdenu.", userUuid);
            approve = false;
        }

        confirmOrder(order, OrderState.CONFIRMED_ORGANIZATOR, false, approve, note1, note2);
        return order;
    }

    /**
     * Oznaci objednavku za potvrdenu administratorom systemu.
     * <p/>
     * @param order
     * @param generateInvoice
     * @param note
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws OrderAlreadyPayedException
     * @throws OrderAlreadyCanceledException
     */
    @Override
    public void updateOrderToConfirmedByAdministrator(Order order, boolean generateInvoice, boolean notifyOwner, String note)
            throws RequiredPropertyException, NoRecordException {
        String msg = String.format("Objednavka je oznacena administratorom za potvrdenu s priznakom generateInvoice=%s a poznamkou: %s", generateInvoice, note);

        if (generateInvoice) {
            Invoice invoice = prepareInvoice(order, null);
        }
        confirmOrder(order, OrderState.CONFIRMED_SYSTEM, notifyOwner, false, msg);
    }

    @Override
    public void updateOrderApprove(Long approveOrderId, boolean approve, String note)
            throws RequiredPropertyException, NoRecordException {

        if (approveOrderId == null || approveOrderId <= 0) {
            throw new RequiredPropertyException("Approve order ID is missing or null");
        }

        ApproveOrder ap = approveOrderDao.loadWithOrderPaymentDetails(approveOrderId);

        if (approve) {
            logger.info("Approving {}", ap.getOrder());
            if (ap.isGenerateInvoice() || ap.getTransaction() != null) {
                Invoice invoice = prepareInvoice(ap.getOrder(), ap.getTransaction());
            }
            confirmOrder(ap.getOrder(), ap.getToOrderState(), true, false, note);
            approveOrderDao.delete(ap);
        } else {
            logger.info("NOT approving {}", ap.getOrder());
            // zrusime objekt approve
            Order order = ap.getOrder();
            approveOrderDao.delete(ap);
            // zmenime stav
            order.setState(OrderState.CANCELED_SYSTEM);
            order.appendToNote("Objednavka nebola administratorom potvrdena. S poznamkou: ");
            order.appendToNote(note);
            orderDao.update(order);
            delegateOrderChanged(order);
        }
    }

    private Invoice confirmOrderWithTransaction(Order order, Transaction transaction, boolean approve, String note1) throws NoRecordException {

        String note2 = null;
        String note3 = null;

        if (order.isCanceled()) {
            note2 = String.format("%s je oznacena ako zrusena ale transakcia obsahuje informacie o jej zaplateni.", order);
            approve = true;
        } else if (order.isConfirmed()) {
            note2 = String.format("%s je oznacena ako potvrdena ale transakcia obsahuje informacie o jej zaplateni.", order);
            approve = true;
        } else if (order.getState().equals(OrderState.WAITING_TO_APPROVE)) {
            note2 = String.format("%s je oznacena v stave WAITING_TO_APPROVE ale je opat dalsou transakciou potvrdena.", order);
            approve = true;
        }

        if (transaction.getCurrency() == null) {
            note3 = String.format("%s neobsahuje podporovanu menu.", transaction);
            approve = true;
        } else if (!transaction.getCurrency().equals(order.getSupplier().getCurrency())) {
            note3 = String.format("%s obsahuje inu menu v akej je objednavka.", transaction);
            approve = true;
        }

        // vygenerujeme fakturu
        Invoice invoice = prepareInvoice(order, transaction);
        confirmOrder(order, OrderState.CONFIRMED_SYSTEM, true, approve, note1, note2, note3);
        return invoice;
    }

    private void confirmOrder(Order order, OrderState confirmState, boolean notify, boolean needToApprove, String... notes) throws NoRecordException {
        // nastavime poznamky        
        for (int i = 0; i < notes.length; i++) {
            if (StringUtils.isNotBlank(notes[i])) {
                order.appendToNote(notes[i]);
            }
        }
        logger.info("Confirm {} to {} with need to approve: {}", new Object[]{order, confirmState, needToApprove});

        if (needToApprove) {
            logger.info("Need approval {}", order);
            ApproveOrder approveOrder = new ApproveOrder();
            approveOrder.setCreateDate(DateProvider.getCurrentDate());
            approveOrder.setOrder(order);
            approveOrder.setToOrderState(confirmState);
            approveOrder.setGenerateInvoice(order.getInvoice() != null);
            if (order.getInvoice() != null) {
                approveOrder.setTransaction(order.getInvoice().getTransaction());
                order.setInvoice(null);
            }
            // upravime stav
            order.setState(OrderState.WAITING_TO_APPROVE);
            orderDao.update(order);

            // aktualizujeme
            approveOrderDao.create(approveOrder);
        } else {
            logger.info("{} -> {}", confirmState, order);

            // spracujeme fee strategie
            logger.debug("Processing fee strategies {}", order);
            order.setState(confirmState);
            order.setConfirmedDate(DateProvider.getCurrentDate());
            feeStrategryService.processFees(order);

            OrderConfirmationHandler handler = null;

            // spracuejme dodatocne akcie
            switch (order.getOrderType()) {
                case LICENSE:
                    handler = new LicenseConfirmationHandler();
                    break;
                case TICKET:
                    handler = new EventProductConfirmationHandler();
                default:
                    // supisiocn
                    break;
            }
            //
            if (handler != null) {
                handler.beforeSaveUpdate(order);
            }        //
            if (order.getInvoice() != null) {
                logger.debug("Creating {}", order.getInvoice());
                invoiceDao.create(order.getInvoice());
            }
            // aktualizujeme
            orderDao.update(order);
            //
            if (handler != null) {
                handler.afterSaveUpdate(order);
            }
        }

        // notifikujeme
        if (notify) {
            delegateOrderChanged(order);
        }
    }

    @Override
    public void updateOrder(Order order) throws RequiredPropertyException {
        if (order == null) {
            throw new RequiredPropertyException("Order is null");
        }
        orderDao.update(order);
    }

    /* ------------------------------------------------------------------------ */
    /* --- L O A D --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public Order loadOrderByUuid(String uuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("Missing uuid");
        }
        return orderDao.loadFullyByUuid(uuid);
    }

    @Override
    public Order loadOrderByNumber(String orderNumber) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(orderNumber)) {
            throw new RequiredPropertyException("Missing orderNumber");
        }
        return orderDao.loadFullyByOrderNumber(orderNumber);
    }

    @Override
    public ApproveOrder loadApproveOrderByOrderUuid(String uuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("Missing order uuid");
        }
        return approveOrderDao.loadApproveOrderByOrderUuid(uuid);
    }

    /* ------------------------------------------------------------------------ */
    /* --- L I S T --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public List<Order> listEventOrders(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage) {
        return orderDao.listEventOrders(eventUuid, stateFilter, purchasedFrom, purchasedTo, confirmedFrom, confirmedTo, number, startItem, itemsPerPage);
    }

    @Override
    public int loadEventOrdersCount(String eventUuid, OrderFilterEnum stateFilter, Date purchasedFrom, Date purchasedTo, Date confirmedFrom, Date confirmedTo, String number) {
        return orderDao.loadEventOrdersCount(eventUuid, stateFilter, purchasedFrom, purchasedTo, confirmedFrom, confirmedTo, number);
    }

    @Override
    public List<ApproveOrder> listApproveOrders(int startItem, int itemsPerPage) {
        return approveOrderDao.listApproveOrders(startItem, itemsPerPage);
    }

    @Override
    public int loadApproveOrdersCount() {
        return approveOrderDao.loadApproveOrdersCount();
    }

    @Override
    public List<ApproveOrder> listApproveOrdersForEvent(String eventUuid, int startItem, int itemsPerPage) {
        return approveOrderDao.listApproveOrdersForEvent(eventUuid, startItem, itemsPerPage);
    }

    @Override
    public int loadApproveOrdersForEventCount(String eventUuid) {
        return approveOrderDao.loadApproveOrdersForEventCount(eventUuid);
    }

    @Override
    public List<Invoice> listEventInvoices(String eventUuid, Date confirmedFrom, Date confirmedTo, String number, int startItem, int itemsPerPage) {
        return invoiceDao.listEventInvoices(eventUuid, confirmedFrom, confirmedTo, number, startItem, itemsPerPage);
    }

    @Override
    public int listEventInvoicesCount(String eventUuid, Date confirmedFrom, Date confirmedTo, String number) {
        return invoiceDao.listEventInvoicesCount(eventUuid, confirmedFrom, confirmedTo, number);
    }


    /* ------------------------------------------------------------------------ */
    /* --- D E L E T E --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public void cancelOrder(String orderUuid, OrderState toState, String note, boolean notify)
            throws NoRecordException, RequiredPropertyException, OrderAlreadyPayedException {
        if (StringUtils.isBlank(orderUuid) || toState == null) {
            throw new RequiredPropertyException("Missing order uuid or state");
        }

        // test na ojebavku 
        switch (toState) {
            case ACTIVE:
            case CONFIRMED_ORGANIZATOR:
            case CONFIRMED_SYSTEM:
            case CONFIRMED_ZERO:
                throw new RequiredPropertyException("Invalid cancel state " + toState + " for order " + orderUuid);
        }

        Order order = loadOrderByUuid(orderUuid);
        if (order.isConfirmed() && !order.isZeroConfirmed()) {
            throw new OrderAlreadyPayedException("Can't cancel order" + orderUuid + " that has been already marked as confirmed");
        }
        if (order.isCanceled()) {
            throw new NoRecordException("Order " + orderUuid + " is already marked as canceled");
        }

        logger.debug("Cancel order: {}", order);
        order.setState(toState);
        order.appendToNote(note);
        orderDao.update(order);

        if (notify) {
            delegateOrderChanged(order);
        }
    }

    @Override
    public void cancelOrders(List<String> orderUuids, OrderState toState, String note, boolean notify)
            throws NoRecordException, RequiredPropertyException, OrderAlreadyPayedException {
        if (CollectionUtils.isEmpty(orderUuids)) {
            throw new RequiredPropertyException("Missing collection of order uuids");
        }
        for (String string : orderUuids) {
            cancelOrder(string, toState, note, notify);
        }
    }

    @Override
    public void cancelPaidOrder(Order order, String note, boolean notify)
            throws NoRecordException, RequiredPropertyException {
        if (order == null) {
            throw new RequiredPropertyException("Missing order");
        }

        // test na ojebavku
        switch (order.getState()) {
            case CONFIRMED_ORGANIZATOR:
            case CONFIRMED_SYSTEM:
            case CONFIRMED_ZERO:
                break;
            default:
                throw new RequiredPropertyException("Invalid cancel state for order " + order);
        }



        OrderConfirmationHandler handler = null;

        // spracuejme dodatocne akcie
        switch (order.getOrderType()) {
            case TICKET:
                handler = new EventProductRefundHandler();
            default:
                // supisiocn
                break;
        }


        if (handler != null) {
            handler.beforeSaveUpdate(order);
        }


        order.setState(OrderState.ORDER_REFUNDED);
        order.appendToNote(note);

        // aktualizujeme
        orderDao.update(order);

        //
        if (handler != null) {
            handler.afterSaveUpdate(order);
        }



        // odpocitani z transferu                
        if (notify) {
            delegateOrderChanged(order);
        }
    }

    /* ------------------------------------------------------------------------ */
    /* --- U T I L S --- */
    /* ------------------------------------------------------------------------ */
    private Invoice prepareInvoice(Order order, Transaction transaction) {
        Invoice invoice = new Invoice();
        if (transaction != null) {
            invoice.setConfirmationSource(transaction.getConfirmationChannel());
            invoice.setTransaction(transaction);
        } else {
            invoice.setConfirmationSource(ConfirmationChannel.SYSTEM);
        }
        invoice.setIssueDate(DateProvider.getCurrentDate());
        invoice.setOrder(order);
        invoice.setTaxableFulfillmentDate(DateProvider.getCurrentDate());
        order.setInvoice(invoice);
        // test na to ci ma transakcia spravnu cenu


        return invoice;
    }

    public void delegateOrderChanged(Order order) throws NoRecordException {

        DelegateOrderStateChangedRequest req = delegateWebService.OBJECT_FACTORY.createDelegateOrderStateChangedRequest();
        req.setEventUuid(order.getSupplier().getEvent().getUuid());
        req.setNotify(true);
        req.setChangeDate(ConvertUtils.toXmlCalendar(DateProvider.getCurrentDate()));
        req.setOrderUuid(order.getUuid());
        switch (order.getState()) {
            case ACTIVE:
                throw new NullPointerException("Cant notify about active order");
            case CANCELED_ORGANIZATOR:
            case CANCELED_OWNER:
            case CANCELED_SYSTEM:
            case ORDER_REFUNDED:
                req.setToState(State.CANCELED);
                break;
            case CONFIRMED_ORGANIZATOR:
            case CONFIRMED_SYSTEM:
            case CONFIRMED_ZERO:
                req.setToState(State.CONFIRMED);
                break;
            case WAITING_TO_APPROVE:
                req.setToState(State.NEED_TO_APPROVE);
                break;
        }
        DelegateOrderStateChangedResponse resp;
        switch (order.getOrderType()) {
            case LICENSE:
                resp = getDelegateWebService().delegateAdminOrderStateChanged(req);
                break;
            default:
                resp = getDelegateWebService().delegateEventOrderStateChanged(req);
                break;
        }

        if (resp.isProcessed()) {
            logger.info("Delegate about change of {} ", order);
        } else {
            logger.error("Delegate about {} FAILED ", order);
            throw new NoRecordException("Order not processed by overseer");
        }
    }

    /**
     * @return the orderDao
     */
    public OrderDao getOrderDao() {
        return orderDao;
    }

    /**
     * @param orderDao the orderDao to set
     */
    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @return the propertyService
     */
    public SystemPropertyService getPropertyService() {
        return propertyService;
    }

    /**
     * @param propertyService the propertyService to set
     */
    public void setPropertyService(SystemPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    /**
     * @return the acemceeService
     */
    public AcemceeService getAcemceeService() {
        return acemceeService;
    }

    /**
     * @param acemceeService the acemceeService to set
     */
    public void setAcemceeService(AcemceeService acemceeService) {
        this.acemceeService = acemceeService;
    }

    /**
     * @return the productService
     */
    public ProductService getProductService() {
        return productService;
    }

    /**
     * @param productService the productService to set
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * @return the proformaProvider
     */
    public ProformaProvider getProformaProvider() {
        return proformaProvider;
    }

    /**
     * @param proformaProvider the proformaProvider to set
     */
    public void setProformaProvider(ProformaProvider proformaProvider) {
        this.proformaProvider = proformaProvider;
    }

    /**
     * @return the paymentProfileService
     */
    public PaymentProfileService getPaymentProfileService() {
        return paymentProfileService;
    }

    /**
     * @param paymentProfileService the paymentProfileService to set
     */
    public void setPaymentProfileService(PaymentProfileService paymentProfileService) {
        this.paymentProfileService = paymentProfileService;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the feeStrategryService
     */
    public FeeStrategyService getFeeStrategryService() {
        return feeStrategryService;
    }

    /**
     * @param feeStrategryService the feeStrategryService to set
     */
    public void setFeeStrategryService(FeeStrategyService feeStrategryService) {
        this.feeStrategryService = feeStrategryService;
    }

    /**
     * @return the invoiceDao
     */
    public InvoiceDao getInvoiceDao() {
        return invoiceDao;
    }

    /**
     * @param invoiceDao the invoiceDao to set
     */
    public void setInvoiceDao(InvoiceDao invoiceDao) {
        this.invoiceDao = invoiceDao;
    }

    /**
     * @return the approveOrderDao
     */
    public ApproveOrderDao getApproveOrderDao() {
        return approveOrderDao;
    }

    /**
     * @param approveOrderDao the approveOrderDao to set
     */
    public void setApproveOrderDao(ApproveOrderDao approveOrderDao) {
        this.approveOrderDao = approveOrderDao;
    }

    /**
     * @return the transferService
     */
    public TransferService getTransferService() {
        return transferService;
    }

    /**
     * @param transferService the transferService to set
     */
    public void setTransferService(TransferService transferService) {
        this.transferService = transferService;
    }

    /**
     * @return the delegateWebService
     */
    public DelegateWebService getDelegateWebService() {
        return delegateWebService;
    }

    /**
     * @param delegateWebService the delegateWebService to set
     */
    public void setDelegateWebService(DelegateWebService delegateWebService) {
        this.delegateWebService = delegateWebService;
    }

    /** pokud neni faktura null, nastavi ji cislo */
    private void generateAndSetInvoiceNumber(String clientNumber, Order order) {
        if (order.getInvoice() != null) {
            // vygenerujeme rok
            char year = FastDateFormat.getInstance("yy").format(new Date()).charAt(1);
            // vygenerujeme cislo licencnej faktury
            String invoiceNumber = String.format("1%c0000%s", year, clientNumber);
            // nastavime
            order.getInvoice().setInvoiceNumber(invoiceNumber);
        }
    }

    private interface OrderConfirmationHandler {

        public void beforeSaveUpdate(Order order);

        public void afterSaveUpdate(Order order);
    }

    private class LicenseConfirmationHandler implements OrderConfirmationHandler {

        private String generatedClientNumber;

        @Override
        public void beforeSaveUpdate(Order order) {
            // zistime klientske cislo aktualne
            generatedClientNumber = invoiceDao.nextInvoiceClientNumber();
            generateAndSetInvoiceNumber(generatedClientNumber, order);
        }

        @Override
        public void afterSaveUpdate(Order order) {
            try {
                Event event = getEventService().loadEventByLicenseOrderNumber(order.getOrderNumber());
                getEventService().updateEventActivateNow(event, generatedClientNumber);
            } catch (NoRecordException ex) {
                // toto nehrozi aby sa nenasel event
            } catch (RequiredPropertyException ex) {
                // toto nehorzi lebo objednavka to ma nativne
            } catch (EventAlreadyActiveException ex) {
                String msg = String.format("%s je oznacena licencna objednavka a event k nej je oznaceny ako uz aktivovany. ", order);
                order.appendToNote(msg);
                logger.warn(order.getNote());
                orderDao.update(order);
            }
        }
    }

    private class EventProductConfirmationHandler implements OrderConfirmationHandler {

        @Override
        public void beforeSaveUpdate(Order order) {
            if (order.getInvoice() != null) {
                order.getInvoice().setInvoiceNumber(invoiceDao.nextInvoiceProductNumber(order.getUuid()));
            }
        }

        @Override
        public void afterSaveUpdate(Order order) {
            try {
                // prihodeni na transfer
                transferService.updateTransferAddOrder(order);
            } catch (NoRecordException ex) {
                // toto by nemelo radsi nastat
            } catch (RequiredPropertyException ex) {
                // toto by nemelo radsi nastat
            }
        }
    }

    private class EventProductRefundHandler implements OrderConfirmationHandler {

        @Override
        public void beforeSaveUpdate(Order order) {
            try {
                // prihodeni na transfer                
                transferService.updateTransferRemoveOrder(order);
            } catch (NoRecordException ex) {
                // toto by nemelo radsi nastat
            } catch (RequiredPropertyException ex) {
                // toto by nemelo radsi nastat
            }
        }

        @Override
        public void afterSaveUpdate(Order order) {
        }
    }
}
