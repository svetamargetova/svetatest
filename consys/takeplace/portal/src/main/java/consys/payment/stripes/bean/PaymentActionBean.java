package consys.payment.stripes.bean;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pepa
 */
public class PaymentActionBean implements ActionBean {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private PaymentActionBeanContext context;

    @Override
    public void setContext(ActionBeanContext context) {
        this.context = (PaymentActionBeanContext) context;
    }

    @Override
    public PaymentActionBeanContext getContext() {
        return context;
    }
}
