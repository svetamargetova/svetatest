/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Product;
import consys.common.utils.enums.Currency;

/**
 * Sluzba ktora poskytuje konstanty vztahujuce sa k Acemcee
 * @author palo
 */
public interface AcemceeService {
    
    
    public EventPaymentProfile loadEventPaymentProfile(Currency currency);

    public Product loadLicenseProduct(Currency currency);
    
    public Product loadCommissionProduct(Currency currency);
    
    public Product loadThirdPartyFeeProduct(Currency currency);

    public String loadLicenseProductUuid(Currency currency);

    public String loadCommissionProductUuid(Currency currency);

    public String loadThirdPartyFeeProductUuid(Currency currency);
    
}
