package consys.payment.stripes.bean.event;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.service.EventService;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EditEventPaymentProfileRevisionActionBean extends PaymentActionBean {

    @SpringBean
    private EventService eventService;
    // data
    private String eventUuid;
    private String profileUuid;
    private PaymentProfileRevision revision;

    @DontValidate
    @DefaultHandler
    public Resolution preEdit() throws NoRecordException, RequiredPropertyException {
        EventPaymentProfile epp = eventService.loadEventPaymentProfile(profileUuid);
        revision = epp.getPaymentProfile().getActualRevision();
        return new ForwardResolution("/web/event/eventProfileRevisionEdit.jsp");
    }

    public Resolution update() throws NoRecordException, RequiredPropertyException, IbanFormatException {
        EventPaymentProfile profile = eventService.loadEventPaymentProfile(profileUuid);
        eventService.updateEventPaymentProfileRevision(profile, revision);
        return back();
    }

    public Resolution back() {
        RedirectResolution resolution = new RedirectResolution(EventPaymentProfileRevisionActionBean.class);
        resolution.addParameter("eventUuid", eventUuid);
        resolution.addParameter("profileUuid", profileUuid);
        return resolution;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public PaymentProfileRevision getRevision() {
        return revision;
    }

    public void setRevision(PaymentProfileRevision revision) {
        this.revision = revision;
    }

    public String getProfileUuid() {
        return profileUuid;
    }

    public void setProfileUuid(String profileUuid) {
        this.profileUuid = profileUuid;
    }
}
