package consys.payment.bo.enums;

/**
 *
 * @author palo
 */
public enum InvoiceProcessType {

    /** Zpracovani pro ceskou republiku */
    VAT_CZ,
    /** Zpracovani pro evropskou unii */
    VAT_EU,
    /** Zpracovani pro ostatni zeme */
    VAT_OT,
    /** Zpracovani pro neplatce DPH - Accounting document */
    VAT_AD;

    public Integer toId() {
        switch (this) {
            case VAT_CZ:
                return 1;
            case VAT_EU:
                return 2;
            case VAT_OT:
                return 3;
            case VAT_AD:
                return 4;
            default:
                throw new IllegalArgumentException("Type " + this.name() + " has not ID mapping");
        }
    }

    public static InvoiceProcessType fromId(Integer i) {
        if (i == null) {
            return null;
        }
        switch (i) {
            case 1:
                return VAT_CZ;
            case 2:
                return VAT_EU;
            case 3:
                return VAT_OT;
            case 4:
                return VAT_AD;
            default:
                return null;
        }
    }
}
