package consys.payment.dao;

import consys.payment.bo.Transfer;
import consys.payment.bo.enums.TransferState;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author palo
 */
public interface TransferDao extends PersistedObjectDao<Transfer> {

    public Transfer loadActualTransfer(String eventProfileUuid) throws NoRecordException;

    /**
     * Nacita transfer so zamkom na update.
     * @param transferUuid
     * @param toUpdate
     * @return
     * @throws NoRecordException
     */
    public Transfer loadPlainTransfer(String transferUuid, boolean toUpdate) throws NoRecordException;

    public Transfer loadActualTransferWithRowLock(String eventProfileUuid) throws NoRecordException;

    public Transfer loadActualTransferWithEvent(String eventProfileUuid) throws NoRecordException;

    public Transfer loadTransferDetailWithUuid(String uuid) throws NoRecordException;

    Transfer loadTransferWithCommissionDetail(String transferUuid) throws NoRecordException;

    public Transfer loadTransferWithOrders(String transferUuid) throws NoRecordException;

    Transfer loadTransferWithOrdersDetailed(String transferUuid) throws NoRecordException;

    public Transfer loadPreparedTransferDetailForEvent(String transferUuid) throws NoRecordException;

    public List<Transfer> listTransfersByEventPaymentProfile(String eventProfileUuid, int start, int max);

    public int loadTransfersCount(String eventProfileUuid);

    public List<Transfer> listTransfersByEventPaymentProfile(String eventProfileUuid, TransferState state, int start, int max);

    public int loadTransfersCount(String eventProfileUuid, TransferState inState);

    public List<Transfer> listTransfers(TransferState state, int start, int max);

    public int loadTransfersCount(TransferState inState);

    public List<Transfer> listTransfersByEventUuid(String eventUuid, int startItem, int itemsPerPage);

    public int loadTransfersCountByEventUuid(String eventUuid);
}
