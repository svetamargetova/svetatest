package consys.payment.utils;

import consys.common.utils.date.DateProvider;
import consys.common.utils.enums.CountryEnum;
import consys.payment.SystemProperties;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.PaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.Product;
import consys.payment.bo.SystemProperty;
import consys.common.utils.enums.Currency;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.bo.enums.ProductCategory;
import consys.payment.dao.AutoinstallDao;
import consys.payment.dao.ProductDao;
import consys.payment.service.AcemceeService;
import consys.payment.service.AutoinstallService;
import consys.payment.service.EventService;
import consys.payment.service.PaymentProfileService;
import consys.payment.service.SystemPropertyService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.service.impl.Service;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 *  
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class AutoinstallServiceImpl extends Service implements AutoinstallService, SystemProperties {

    private boolean initialized = false;
    private AutoinstallDao autoinstallDao;
    private ProformaProvider proformaProvider;
    private PaymentProfileService profileService;
    private SystemPropertyService systemPropertyService;
    private ProductDao productDao;
    private EventService eventService;
    

    private Event loadAcemceeEvent() {
        try {
            return getEventService().loadEvent(ACMC_EVENT_UUID_NAME);
        } catch (NoRecordException ex) {
            logger.info("Creatimg default Acemcee Event {}", ACMC_EVENT_UUID_NAME);
            try {
                Event event = getEventService().createEvent(ACMC_EVENT_UUID_NAME, false, CountryEnum.CZECH_REPUBLIC);
                getEventService().updateEventActivateNow(event, "");
                return event;
            } catch (Exception ex1) {
                logger.error("Fail", ex1);
            }
        } catch (RequiredPropertyException ex) {
            logger.error("Fail", ex);
        }
        throw new NullPointerException("Failed..");
    }

    public void installDefaultAcemceeProfiles() throws RequiredPropertyException {
        // instalacia czk profilu                                                 
        try {
            logger.info("Test for Existence: {}", CZK_ACMC_PROFILE_UUID);
            getEventService().loadEventPaymentProfile(CZK_ACMC_PROFILE_UUID);
        } catch (NoRecordException ex) {
            logger.info("Creatimg default CZK payment profile");
            Event event = loadAcemceeEvent();
            PaymentProfile acmcProfile = new PaymentProfile();

            PaymentProfileRevision rev = new PaymentProfileRevision();
            rev.setAccountBankSwift(SystemProperties.CZK_ACMC_BANK_SWIFT);
            rev.setAccountIban(SystemProperties.CZK_ACMC_BANK_IBAN);
            rev.setCity(SystemProperties.CZK_ACMC_CITY);
            rev.setCountry(SystemProperties.CZK_ACMC_COUNTRY);
            rev.setName(SystemProperties.CZK_ACMC_NAME);
            rev.setParent(acmcProfile);
            rev.setRegNo(SystemProperties.CZK_ACMC_ICO_NO);
            rev.setRevisionDate(DateProvider.getCurrentDate());
            rev.setStreet(SystemProperties.CZK_ACMC_STREET);//               
            rev.setVatNo(SystemProperties.CZK_ACMC_DIC_NO);
            rev.setZip(SystemProperties.CZK_ACMC_ZIP);
            rev.setUuid(CZK_ACMC_PROFILE_UUID);
            acmcProfile.setActualRevision(rev);

            EventPaymentProfile eventCzkProfile = new EventPaymentProfile();
            eventCzkProfile.setActive(true);
            eventCzkProfile.setCurrency(Currency.CZK);
            eventCzkProfile.setEvent(event);
            eventCzkProfile.setOrderBankTransferInvoicedToThisProfile(true);
            eventCzkProfile.setPaymentProfile(acmcProfile);
            eventCzkProfile.setUuid(CZK_ACMC_PROFILE_UUID);
            eventCzkProfile.setVatType(InvoiceProcessType.VAT_CZ);
            getEventService().createEventPaymentProfile(event, eventCzkProfile);
        }


        try {
            logger.info("Test for Existence: {}", EUR_ACMC_PROFILE_UUID);
            getEventService().loadEventPaymentProfile(EUR_ACMC_PROFILE_UUID);
        } catch (NoRecordException ex) {
            logger.info("Creatimg default EUR payment profile");
            Event event = loadAcemceeEvent();
            PaymentProfile acmcProfile = new PaymentProfile();

            PaymentProfileRevision rev = new PaymentProfileRevision();
            rev.setAccountBankSwift(SystemProperties.EUR_ACMC_BANK_SWIFT);
            rev.setAccountIban(SystemProperties.EUR_ACMC_BANK_IBAN);
            rev.setCity(SystemProperties.EUR_ACMC_CITY);
            rev.setCountry(SystemProperties.EUR_ACMC_COUNTRY);
            rev.setName(SystemProperties.EUR_ACMC_NAME);
            rev.setParent(acmcProfile);
            rev.setRegNo(SystemProperties.EUR_ACMC_ICO_NO);
            rev.setRevisionDate(DateProvider.getCurrentDate());
            rev.setStreet(SystemProperties.EUR_ACMC_STREET);//               
            rev.setVatNo(SystemProperties.EUR_ACMC_DIC_NO);
            rev.setZip(SystemProperties.EUR_ACMC_ZIP);
            rev.setUuid(EUR_ACMC_PROFILE_UUID);
            acmcProfile.setActualRevision(rev);

            EventPaymentProfile eventEurProfile = new EventPaymentProfile();
            eventEurProfile.setActive(true);
            eventEurProfile.setCurrency(Currency.EUR);
            eventEurProfile.setEvent(event);
            eventEurProfile.setOrderBankTransferInvoicedToThisProfile(true);
            eventEurProfile.setPaymentProfile(acmcProfile);
            eventEurProfile.setUuid(EUR_ACMC_PROFILE_UUID);
            eventEurProfile.setVatType(InvoiceProcessType.VAT_EU);
            getEventService().createEventPaymentProfile(event, eventEurProfile);
        }

        try {
            logger.info("Test for Existence: {}", USD_ACMC_PROFILE_UUID);
            getEventService().loadEventPaymentProfile(USD_ACMC_PROFILE_UUID);
        } catch (NoRecordException ex) {
            logger.info("Creatimg default USD payment profile");
            Event event = loadAcemceeEvent();
            PaymentProfile acmcProfile = new PaymentProfile();

            PaymentProfileRevision rev = new PaymentProfileRevision();
            rev.setAccountBankSwift(SystemProperties.USD_ACMC_BANK_SWIFT);
            rev.setAccountIban(SystemProperties.USD_ACMC_BANK_IBAN);
            rev.setCity(SystemProperties.USD_ACMC_CITY);
            rev.setCountry(SystemProperties.USD_ACMC_COUNTRY);
            rev.setName(SystemProperties.USD_ACMC_NAME);
            rev.setParent(acmcProfile);
            rev.setRegNo(SystemProperties.USD_ACMC_ICO_NO);
            rev.setRevisionDate(DateProvider.getCurrentDate());
            rev.setStreet(SystemProperties.USD_ACMC_STREET);//               
            rev.setVatNo(SystemProperties.USD_ACMC_DIC_NO);
            rev.setZip(SystemProperties.USD_ACMC_ZIP);
            rev.setUuid(USD_ACMC_PROFILE_UUID);
            acmcProfile.setActualRevision(rev);

            EventPaymentProfile eventCzkProfile = new EventPaymentProfile();
            eventCzkProfile.setActive(true);
            eventCzkProfile.setCurrency(Currency.USD);
            eventCzkProfile.setEvent(event);
            eventCzkProfile.setOrderBankTransferInvoicedToThisProfile(true);
            eventCzkProfile.setPaymentProfile(acmcProfile);
            eventCzkProfile.setUuid(USD_ACMC_PROFILE_UUID);
            eventCzkProfile.setVatType(InvoiceProcessType.VAT_OT);
            getEventService().createEventPaymentProfile(event, eventCzkProfile);

        }
    }

    public void installInvoiceClientNumber() {
        try {
            SystemProperty sp = systemPropertyService.loadByKey(SystemProperties.ACTUAL_CLIENT_NUMBER);
            logger.info("Sucessfully loaded actual client number: {}", sp.getValue());
        } catch (NoRecordException ex) {
            logger.info("Acutal client number not exists yet.. initializing to 0");
            systemPropertyService.createOrUpdateSystemPropety(SystemProperties.ACTUAL_CLIENT_NUMBER, "0");
        }
    }

    public void installDefaultCZK() throws NoRecordException, RequiredPropertyException {
        EventPaymentProfile czkEpp = eventService.loadEventPaymentProfile(CZK_ACMC_PROFILE_UUID);

        Product commisson = new Product();
        commisson.setUuid(CZK_ACMC_PRODUCT_COMMISSION_UUID);
        commisson.setCategory(ProductCategory.TICKET_COMMISSION);
        commisson.setName("Participation fees ");
        commisson.setPrice(BigDecimal.ZERO);
        commisson.setProfile(czkEpp);
        commisson.setVatRate(new BigDecimal(20));
        installNativeProduct(commisson);

        Product paypalFees = new Product();
        paypalFees.setUuid(CZK_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID);
        paypalFees.setCategory(ProductCategory.THIRD_PARTY_FEE);
        paypalFees.setName("Transaction costs connected with collection of participation fees");
        paypalFees.setPrice(BigDecimal.ZERO);
        paypalFees.setProfile(czkEpp);
        paypalFees.setVatRate(new BigDecimal(20));
        installNativeProduct(paypalFees);

        Product licenseProduct = new Product();
        licenseProduct.setUuid(CZK_ACMC_PRODUCT_LICENSE_UUID);
        licenseProduct.setCategory(ProductCategory.LICENSE);
        licenseProduct.setName("Takeplace Event License");
        licenseProduct.setPrice(new BigDecimal(12500));
        licenseProduct.setProfile(czkEpp);
        licenseProduct.setVatRate(new BigDecimal(20));
        installNativeProduct(licenseProduct);
    }
    
    public void installDefaultEUR() throws NoRecordException, RequiredPropertyException {
        EventPaymentProfile eurEpp = eventService.loadEventPaymentProfile(EUR_ACMC_PROFILE_UUID);

        Product commisson = new Product();
        commisson.setUuid(EUR_ACMC_PRODUCT_COMMISSION_UUID);
        commisson.setCategory(ProductCategory.TICKET_COMMISSION);
        commisson.setName("Participation fees ");
        commisson.setPrice(BigDecimal.ZERO);
        commisson.setProfile(eurEpp);
        commisson.setVatRate(new BigDecimal(20));
        installNativeProduct(commisson);

        Product paypalFees = new Product();
        paypalFees.setUuid(EUR_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID);
        paypalFees.setCategory(ProductCategory.THIRD_PARTY_FEE);
        paypalFees.setName("Transaction costs connected with collection of participation fees");
        paypalFees.setPrice(BigDecimal.ZERO);
        paypalFees.setProfile(eurEpp);
        paypalFees.setVatRate(new BigDecimal(20));
        installNativeProduct(paypalFees);

        Product licenseProduct = new Product();
        licenseProduct.setUuid(EUR_ACMC_PRODUCT_LICENSE_UUID);
        licenseProduct.setCategory(ProductCategory.LICENSE);
        licenseProduct.setName("Takeplace Event License");
        licenseProduct.setPrice(new BigDecimal(500));
        licenseProduct.setProfile(eurEpp);
        licenseProduct.setVatRate(new BigDecimal(20));
        installNativeProduct(licenseProduct);
    }

    public void installDefaultUSD() throws NoRecordException, RequiredPropertyException {
        EventPaymentProfile usdEpp = eventService.loadEventPaymentProfile(USD_ACMC_PROFILE_UUID);

        Product commisson = new Product();
        commisson.setUuid(USD_ACMC_PRODUCT_COMMISSION_UUID);
        commisson.setCategory(ProductCategory.TICKET_COMMISSION);
        commisson.setName("Participation fees ");
        commisson.setPrice(BigDecimal.ZERO);
        commisson.setProfile(usdEpp);
        commisson.setVatRate(new BigDecimal(20));
        installNativeProduct(commisson);

        Product paypalFees = new Product();
        paypalFees.setUuid(USD_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID);
        paypalFees.setCategory(ProductCategory.THIRD_PARTY_FEE);
        paypalFees.setName("Transaction costs connected with collection of participation fees");
        paypalFees.setPrice(BigDecimal.ZERO);
        paypalFees.setProfile(usdEpp);
        paypalFees.setVatRate(new BigDecimal(20));
        installNativeProduct(paypalFees);

        Product licenseProduct = new Product();
        licenseProduct.setUuid(USD_ACMC_PRODUCT_LICENSE_UUID);
        licenseProduct.setCategory(ProductCategory.LICENSE);
        licenseProduct.setName("Takeplace Event License");
        licenseProduct.setPrice(new BigDecimal(500));
        licenseProduct.setProfile(usdEpp);
        licenseProduct.setVatRate(new BigDecimal(20));
        installNativeProduct(licenseProduct);
    }

    
    public void installNativeProduct(Product p) {
        try {
            productDao.loadProductByUuid(p.getUuid());
            logger.info("Product {} already installed - if update needed, use User Interface", p);
        } catch (NoRecordException ex) {
            logger.info("Installing {} ", p);
            productDao.create(p);
        }
    }

    public void installInvoiceNumberSQLScript() {
        String script = loadFileTemplate("/sql/invoice_number_next_commission.sql");
        logger.info("Installing: /sql/invoice_number_next_commission.sql");
        autoinstallDao.installScript(script);
        
        script = loadFileTemplate("/sql/invoice_number_next_event.sql");
        logger.info("Installing: /sql/invoice_number_next_event.sql");
        autoinstallDao.installScript(script);
        
        script = loadFileTemplate("/sql/invoice_number_next_product.sql");
        logger.info("Installing: /sql/invoice_number_next_product.sql");
        autoinstallDao.installScript(script);
        
        
    }

    public void initializeProFormaNumber() {
        proformaProvider.initializeProvider();
    }

    @Override
    public void installDefaults() {
        try {
            initialized = true;
            installInvoiceNumberSQLScript();
            initializeProFormaNumber();
            installInvoiceClientNumber();
            installDefaultAcemceeProfiles();
            installDefaultCZK();
            installDefaultEUR();
            installDefaultUSD();
        } catch (Exception ex) {
            logger.error("Fail", ex);
            throw new IllegalArgumentException("Failed to autoinstall");            
        }
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextRefreshedEvent && !initialized) {
            installDefaults();
        }
    }

    /**
     * @return the proformaProvider
     */
    public ProformaProvider getProformaProvider() {
        return proformaProvider;
    }

    /**
     * @param proformaProvider the proformaProvider to set
     */
    public void setProformaProvider(ProformaProvider proformaProvider) {
        this.proformaProvider = proformaProvider;
    }

    /**
     * @return the autoinstallDao
     */
    public AutoinstallDao getAutoinstallDao() {
        return autoinstallDao;
    }

    /**
     * @param autoinstallDao the autoinstallDao to set
     */
    public void setAutoinstallDao(AutoinstallDao autoinstallDao) {
        this.autoinstallDao = autoinstallDao;
    }

    /**
     * @return the profileService
     */
    public PaymentProfileService getProfileService() {
        return profileService;
    }

    /**
     * @param profileService the profileService to set
     */
    public void setProfileService(PaymentProfileService profileService) {
        this.profileService = profileService;
    }

    /**
     * @return the productDao
     */
    public ProductDao getProductDao() {
        return productDao;
    }

    /**
     * @param productDao the productDao to set
     */
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    /**
     * @return the systemPropertyService
     */
    public SystemPropertyService getSystemPropertyService() {
        return systemPropertyService;
    }

    /**
     * @param systemPropertyService the systemPropertyService to set
     */
    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

     static String loadFileTemplate(String resourceName) throws IllegalStateException {
        InputStream is = null;
        try {
            is = AutoinstallServiceImpl.class.getResourceAsStream(resourceName);
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            return writer.toString();
        } catch (Exception e) {
            throw new IllegalStateException("No body!", e);
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
            }
        }

    }
    
}
