package consys.payment.stripes.bean.order;

import consys.payment.bo.Order;
import consys.payment.service.OrderService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class ApproveOrderListItemActionBean extends PaymentActionBean {

    @SpringBean
    private OrderService orderService;
    // data
    private String uuid;
    private String eventUuid;
    private Order order;
    private Long approveId;
    private boolean approve;
    private String note;

    @DontValidate
    @DefaultHandler
    public Resolution preView() throws NoRecordException, RequiredPropertyException {
        order = orderService.loadOrderByUuid(uuid);
        return new ForwardResolution("/web/order/approveOrdersDetail.jsp");
    }

    public Resolution confirmApprove() throws RequiredPropertyException, NoRecordException {
        orderService.updateOrderApprove(approveId, approve, note);
        return back();
    }

    public Resolution back() {
        RedirectResolution resolution = new RedirectResolution(ApproveOrderListActionBean.class);
        if (eventUuid != null) {
            resolution.addParameter("eventUuid", eventUuid);
        }
        return resolution;
    }

    public int getListSize() {
        return order.getItems() != null ? order.getItems().size() : 0;
    }

    public Order getOrder() {
        return order;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    public Long getApproveId() {
        return approveId;
    }

    public void setApproveId(Long approveId) {
        this.approveId = approveId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
