package consys.payment.service;

import consys.payment.ws.delegate.DelegateOrderStateChangedRequest;
import consys.payment.ws.delegate.DelegateOrderStateChangedResponse;
import consys.payment.ws.delegate.ObjectFactory;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface DelegateWebService {

    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
    
    /**
     * Deleguje o zmene nejakeho overseera
     * @param request
     * @return
     */
    public DelegateOrderStateChangedResponse delegateEventOrderStateChanged(DelegateOrderStateChangedRequest request);

    /**
     * Deleguje o zmene administraciu
     * @param request
     * @return
     */
    public DelegateOrderStateChangedResponse delegateAdminOrderStateChanged(DelegateOrderStateChangedRequest request);



}
