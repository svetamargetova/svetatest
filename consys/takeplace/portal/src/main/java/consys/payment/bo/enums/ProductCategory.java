package consys.payment.bo.enums;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum ProductCategory {

    /** Licencia - Acemcee */
    LICENSE,
    /** Tiket - registracny modul */
    TICKET,
    /** Provizia - Acemce */
    TICKET_COMMISSION,
    /** Nahrada THIRD_PARTY  */
    THIRD_PARTY_FEE;

    public Integer toId() {
        switch (this) {
            case LICENSE:
                return 1;
            case TICKET:
                return 2;
            case TICKET_COMMISSION:
                return 3;
            case THIRD_PARTY_FEE:
                return 4;
            default:
                throw new IllegalArgumentException("Type " + this.name() + " has not ID mapping");
        }
    }

    public static ProductCategory fromId(Integer i) {
        switch (i) {
            case 1:
                return LICENSE;
            case 2:
                return TICKET;
            case 3:
                return TICKET_COMMISSION;
            case 4:
                return THIRD_PARTY_FEE;
            default:
                return null;
        }
    }
}
