package consys.payment.service;

import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Product;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.ProductAlreadyUsedException;
import consys.payment.service.exception.RequiredPropertyException;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ProductService {

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    public void createEventProduct(String eventUuid, Product product)
            throws NoRecordException, RequiredPropertyException;

    public void createEventProduct(EventPaymentProfile profile, Product product)
            throws NoRecordException, RequiredPropertyException;

    public void createEventProducts(String eventUuid, List<Product> product)
            throws NoRecordException, RequiredPropertyException;

    public void createEventProducts(EventPaymentProfile profile, List<Product> product)
            throws NoRecordException, RequiredPropertyException;


    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    public void updateEventProduct(String eventUuid, Product product)
            throws NoRecordException, RequiredPropertyException;

    public void updateEventProduct(EventPaymentProfile profile, Product product)
            throws NoRecordException, RequiredPropertyException;

    public void updateEventProducts(EventPaymentProfile eventPaymentProfile, List<Product> product)
            throws NoRecordException, RequiredPropertyException;

    public void updateEventProducts(String event, List<Product> product)
            throws NoRecordException, RequiredPropertyException;

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    public Product loadProduct(String productUuid)
            throws NoRecordException, RequiredPropertyException;

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    public List<Product> listAllEventProducts(String eventUuid)
            throws NoRecordException, RequiredPropertyException;
    
    public List<Product> listAllEventPaymentProfileProducts(String eventPaymentProfileUuid)
            throws NoRecordException, RequiredPropertyException;
    

    public List<Product> listProducts(List<String> productUuid)
            throws NoRecordException, RequiredPropertyException;

    /*------------------------------------------------------------------------*/
    /* ---  D E L E T E --- */
    /*------------------------------------------------------------------------*/
    public int deleteProducts(String eventUuid, List<String> productsUuid)
            throws NoRecordException, RequiredPropertyException;

    /**
     * Odstrani produkt ak nie je nikde uz priradeny k fakture
     *
     * @param event Event ku ktoremu patri produkt
     * @param product Produkt ktory ma byt odstraneny
     * @return priznak ci bol zmazany. True ak nie je nikde uz na fakture.
     * @throws NoRecordException
     * @throws RequiredPropertyException
     * @throws ProductAlreadyUsedException
     */
    public boolean deleteProduct(Event event, Product product)
            throws NoRecordException, RequiredPropertyException;
}
