package consys.payment.stripes.bean.event;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.service.EventService;
import consys.payment.stripes.bean.ListActionBean;
import java.util.List;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EventPaymentProfileListActionBean extends ListActionBean<EventPaymentProfile> {

    @SpringBean
    private EventService eventService;
    // data
    private String eventUuid;

    public EventPaymentProfileListActionBean() {
        super("/web/event/eventProfile.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        List<EventPaymentProfile> profiles = eventService.listEventPaymentProfiles(eventUuid);
        getItems().setList(profiles);
        getItems().setTotal(profiles.size());
    }

    public Resolution back() {
        return new ForwardResolution(EventListActionBean.class);
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }
}
