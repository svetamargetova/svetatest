/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service.fees;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Order;
import consys.payment.bo.enums.ProductCategory;
import java.math.BigDecimal;

public class TicketOrderFeeStrategy implements OrderFeeStrategy {

    @Override
    public boolean canHandleOrder(ProductCategory category) {
        return category.equals(ProductCategory.TICKET);
    }

    @Override
    public void computeFee(EventPaymentProfile orderProfile, Order order) {
        // ak je to viac ako max tak nastavime do share 0 a do Unit max order
        if (order.getFees().compareTo(orderProfile.getOrderMaxFee()) > 0) {
            order.setUnitFee(orderProfile.getOrderMaxFee());
            order.setShareFee(BigDecimal.ZERO);
        }
    }
}
