package consys.payment.channel.paypal.ipn;

import consys.common.utils.date.DateProvider;
import consys.payment.channel.paypal.ipn.enums.PPCurrency;
import consys.payment.channel.paypal.ipn.enums.PPPaymentStatus;
import consys.payment.channel.paypal.ipn.enums.PPTransactionType;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *  Sprava IPN obsahujuca vsetky informacie ktore prisli
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PaypalTransaction {

    private static final Logger logger = LoggerFactory.getLogger(PaypalTransaction.class);
    public static final String IPN = "IpnMessage  ";
    public static final String EQ = " = ";
    public static final String B = "  ";
    public static final String ONE = "1";
    private SimpleDateFormat ppFormat = new SimpleDateFormat("HH:mm:ss MMM d, yyyy z", Locale.ENGLISH);
    // Zakladne data
    private Map<String, String> input;
    private StringBuilder message;
    private Date transactionDate;

    public PaypalTransaction() {
        input = new HashMap<String, String>();
        message = new StringBuilder(IPN);
    }

    void put(String paramName, String paramValue) {
        input.put(paramName, paramValue);
        message.append(paramName).append(EQ).append(paramValue).append(B);
        if (paramName.equalsIgnoreCase(IpnVariables.payment_date)) {
            try {
                transactionDate = ppFormat.parse(input.get(IpnVariables.payment_date));
            } catch (ParseException ex) {
                logger.error("Failed to parse Paypal IPN date: {}", input.get(IpnVariables.payment_date));
                transactionDate = DateProvider.getCurrentDate();
            }

        }

    }

    public String verifySign() {
        return input.get(IpnVariables.verify_sign);
    }

    public boolean testIpn() {
        String test = input.get(IpnVariables.test_ipn);
        if (StringUtils.isNotBlank(test)) {
            return test.equals(ONE);
        }
        return false;
    }
    // nachadza sa v custom

    public String custom() {
        return input.get(IpnVariables.custom);
    }

    public PPTransactionType transactionType() {
        return PPTransactionType.valueOf(input.get(IpnVariables.txn_type));
    }

    public String transactionId() {
        return input.get(IpnVariables.txn_id);
    }

    public PPPaymentStatus paymentStatus() {
        return PPPaymentStatus.valueOf(input.get(IpnVariables.payment_status));
    }

    public BigDecimal paymentGross() {
        return new BigDecimal(input.get(IpnVariables.mc_gross));
    }

    public BigDecimal paymentFee() {
        return new BigDecimal(input.get(IpnVariables.mc_fee));
    }

    public String pendingReasonPayment() {
        return input.get(IpnVariables.pending_reason);
    }

    public String chargeBackPayment() {
        return input.get(IpnVariables.reason_code);
    }

    public PPCurrency paymentCurrency() {
        return PPCurrency.valueOf(input.get(IpnVariables.mc_currency));
    }

    public Date paymentDate() {
        return transactionDate;

    }

    public String itemNumber() {
        return input.get(IpnVariables.item_number);
    }

    public String invoice() {
        return input.get(IpnVariables.invoice);
    }

    @Override
    public String toString() {
        return message.toString();
    }
}
