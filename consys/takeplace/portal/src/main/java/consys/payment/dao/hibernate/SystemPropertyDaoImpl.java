package consys.payment.dao.hibernate;

import consys.payment.bo.SystemProperty;
import consys.payment.dao.SystemPropertyDao;
import consys.payment.service.exception.NoRecordException;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SystemPropertyDaoImpl extends BusinessObjectDaoImpl<SystemProperty> implements SystemPropertyDao {

    @Override
    public SystemProperty loadByKey(String key) throws NoRecordException {
        SystemProperty prop = (SystemProperty) session().
                createQuery("select s from SystemProperty s where s.key = :KKEY").
                setString("KKEY", key).
                uniqueResult();
        if (prop == null) {
            throw new NoRecordException("No SYstemPropety with key " + key);
        }
        return prop;
    }
}
