package consys.payment.export.xsl;

import consys.payment.bo.*;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.utils.PaymentUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.*;
import org.apache.commons.lang.SystemUtils;

/**
 *
 * @author pepa
 */
public class XlsTransferOverviewGenerator {

    // konstanty
    private final SimpleDateFormat periodFormat = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
    private final SimpleDateFormat exportedFormat1 = new SimpleDateFormat("d.M.yyyy", Locale.ENGLISH);
    private final SimpleDateFormat exportedFormat2 = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    // konstanty - fonty
    private final WritableFont ARIAL_10_BOLD = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
    private final WritableFont ARIAL_10_ITALICS = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, true);
    private final WritableFont ARIAL_10_REGULAR = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
    private final WritableFont ARIAL_16_BOLD = new WritableFont(WritableFont.ARIAL, 16, WritableFont.BOLD);
    // data
    private WritableSheet overview;
    private NumberFormat numberFormat;

    /** vlozi titulek do listu */
    private void addTitle() throws Exception {
        WritableCellFormat format = new WritableCellFormat(ARIAL_16_BOLD);
        Label titleLabel = new Label(0, 0, "Summary of payments in Takeplace"/*, format*/);
        overview.mergeCells(0, 0, 6, 0);
        overview.addCell(titleLabel);
    }

    /** vlozi nazev akce */
    private void addAccount(String name) throws Exception {
        WritableCellFormat format1 = new WritableCellFormat(ARIAL_10_REGULAR);
        format1.setAlignment(Alignment.RIGHT);
        WritableCellFormat format2 = new WritableCellFormat(ARIAL_10_BOLD);
        Label actionTitle = new Label(0, 1, "Account:"/*, format1*/);
        Label actionValue = new Label(1, 1, name/*, format2*/);
        overview.mergeCells(1, 1, 2, 1);
        overview.addCell(actionTitle);
        overview.addCell(actionValue);
    }

    /** vlozi obdobi */
    private void addPeriod(String period) throws Exception {
        WritableCellFormat format1 = new WritableCellFormat(ARIAL_10_REGULAR);
        format1.setAlignment(Alignment.RIGHT);
        WritableCellFormat format2 = new WritableCellFormat(ARIAL_10_BOLD);
        Label periodTitle = new Label(3, 1, "Period:"/*, format1*/);
        Label periodValue = new Label(4, 1, period/*, format2*/);
        overview.mergeCells(4, 1, 6, 1);
        overview.addCell(periodTitle);
        overview.addCell(periodValue);
    }

    /** vlozi nadpis nad prehledovou tabulkou transferu */
    private void addTransfersTitle() throws Exception {
        WritableCellFormat format = new WritableCellFormat(ARIAL_10_BOLD);
        overview.mergeCells(1, 2, 2, 2);
        overview.addCell(new Label(1, 2, "RECEIVED PAYMENTS"/*, format */));
    }

    /** vlozi zahlavi tabulky */
    private void addHeader() throws Exception {
        int col = 0;
        final int row = 3;
        WritableCellFormat format = new WritableCellFormat(ARIAL_10_ITALICS);
        format.setBorder(Border.ALL, BorderLineStyle.THIN);

        overview.addCell(new Label(col++, row, "Order"/*, format*/));
        overview.addCell(new Label(col++, row, "Name"/*, format*/));
        overview.addCell(new Label(col++, row, "Description"/*, format*/));
        overview.addCell(new Label(col++, row, "Price"/*, format*/));
        overview.addCell(new Label(col++, row, "Registration\ndate"/*, format*/));
        overview.addCell(new Label(col++, row, "Paid\ndate"/*, format*/));
        overview.addCell(new Label(col++, row, "Payment\nmethod"/*, format*/));
    }

    /**
     * vlozi vsechny zaznamy ze seznamu
     * @return cislo nasledujiciho radku za koncem seznamu
     */
    private int addRecords(Transfer transfer) throws Exception {
        int col = 0;
        int row = 4;
        WritableCellFormat formatOrder = new WritableCellFormat(ARIAL_10_REGULAR);
        formatOrder.setBorder(Border.ALL, BorderLineStyle.THIN);
        formatOrder.setAlignment(Alignment.RIGHT);
        WritableCellFormat format = new WritableCellFormat(ARIAL_10_REGULAR);
        format.setBorder(Border.ALL, BorderLineStyle.THIN);
        format.setWrap(true);
        WritableCellFormat formatValue = new WritableCellFormat(ARIAL_10_REGULAR, numberFormat);
        formatValue.setBorder(Border.ALL, BorderLineStyle.THIN);

        List<Order> orders = PaymentUtils.transferOrdersWithoutDuplicity(transfer.getOrders());
        for (Order i : orders) {
            String paymentType = i.getInvoice() == null
                    ? "organizer"
                    : paymentType(i.getInvoice().getConfirmationSource());
            overview.addCell(new Label(col++, row, String.valueOf(row - 3)/*, formatOrder*/));
            overview.addCell(new Label(col++, row, i.getCustomer().getName()/*, format*/));
            overview.addCell(new Label(col++, row, i.getItems().get(0).getName()/*, format*/));
            overview.addCell(new Label(col++, row, i.getPrice().toPlainString()/*, formatValue*/));
            overview.addCell(new Label(col++, row, exportedFormat1.format(i.getPurchaseDate())/*, format*/));
            overview.addCell(new Label(col++, row, exportedFormat1.format(i.getConfirmedDate())/*, format*/));
            overview.addCell(new Label(col++, row, paymentType/*, format*/));
            row++;
            col = 0;
        }

        return row;
    }

    private String paymentType(ConfirmationChannel c) {
        switch (c) {
            case BANK_TRANSFER:
                return "bank transfer";
            case ONLINE_PAY_PAL:
                return "online";
            case SYSTEM:
                return "online";
        }
        return "";
    }

    /** zapise celkovou castku */
    private int addTotal(final int row, BigDecimal total) throws Exception {
        WritableCellFormat format1 = new WritableCellFormat(ARIAL_10_BOLD);
        format1.setAlignment(Alignment.RIGHT);
        WritableCellFormat format2 = new WritableCellFormat(ARIAL_10_BOLD, numberFormat);
        overview.addCell(new Label(2, row, "TOTAL:"/*, format1*/));
        overview.addCell(new Label(3, row, total.toString()/*, format2*/));
        return row + 1;
    }

    /** vygeneruje kdo a kdy provedl export */
    private void addGeneratedBy(final int row, Date date) throws Exception {
        final int r = row + 1;
        String value = "Generated by Takeplace event management service by ACEMCEE, s. r. o., ";
        value += exportedFormat1.format(date) + " in " + exportedFormat2.format(date) + ".";
        WritableCellFormat format = new WritableCellFormat(ARIAL_10_REGULAR);
        Label titleLabel = new Label(0, r, value/*, format*/);
        overview.mergeCells(0, r, 6, r);
        overview.addCell(titleLabel);
    }

    /** vytvori ze zadaneho prehledu xls */
    public void createXls(OutputStream os, Transfer transfer, boolean closeOutputStream) throws Exception {
        // priprava dat a nastaveni promennych
        String f = "#0.00";
        switch (transfer.getOrders().get(0).getSupplier().getCurrency()) {
            case CZK:
                f = "#0.00 Kč";
                break;
            case EUR:
                f = "€ #0.00";
                break;
            case USD:
                f = "$ #0.00";
                break;
        }
        numberFormat = new NumberFormat(f, NumberFormat.COMPLEX_FORMAT);

        List<Order> invoices = PaymentUtils.transferOrdersWithoutDuplicity(transfer.getOrders());
        final String period = periodFormat.format(invoices.get(0).getConfirmedDate())
                + " - " + periodFormat.format(invoices.get(invoices.size() - 1).getConfirmedDate());

        // iniciace dokumentu a listu
        WritableWorkbook workbook = Workbook.createWorkbook(os);
        overview = workbook.createSheet(period, 0);

        // nastaveni sirky sloupcu (10 je cca 2cm)
        overview.setColumnView(0, 8);
        overview.setColumnView(1, 20);
        overview.setColumnView(2, 20);
        overview.setColumnView(3, 11);
        overview.setColumnView(4, 11);
        overview.setColumnView(5, 11);
        overview.setColumnView(6, 13);

        // vlastni obsah
        addTitle();
        // TODO: generovani profilu na fakturu
        //addAccount(transfer.getEventPaymentProfile().getPaymentProfile().getActualRevision().getAccountIban());
        addPeriod(period);
        addTransfersTitle();
        addHeader();
        int row = addRecords(transfer);
        row = addTotal(row, transfer.getTotalSum());
        addGeneratedBy(row, new Date());

        // zapsani (+ uzavreni streamu)
        workbook.write();
        if (closeOutputStream) {
            workbook.close();
        }
    }

    /** vytvori ze zadaneho prehledu xls */
    public void createXls(OutputStream os, Transfer transfer) throws Exception {
        createXls(os, transfer, true);
    }

    /** testovaci vygenerovani xls */
    public static void main(String[] args) {
        XlsTransferOverviewGenerator gen = new XlsTransferOverviewGenerator();
        try {
            if (SystemUtils.IS_OS_LINUX) {
                gen.createXls(new FileOutputStream(new File("/tmp/fss.xls")), getTestTransfer());
            } else if (SystemUtils.IS_OS_WINDOWS) {
                gen.createXls(new FileOutputStream(new File("c:\\data\\out.xls")), getTestTransfer());
            } else if (SystemUtils.IS_OS_MAC_OSX) {
                gen.createXls(new FileOutputStream(new File("/Users/palo/Acemcee/fss.xls")), getTestTransfer());
            }
        } catch (Exception ex) {
            System.err.println("Chyba " + ex.getLocalizedMessage());
        }
    }

    /** vraci testovaci transfer */
    public static Transfer getTestTransfer() {
        Transfer t = new Transfer();
        t.setTotalSum(new BigDecimal(500));

        Event event = new Event();
        PaymentProfileRevision revision = new PaymentProfileRevision();
        revision.setAccountIban("CZ12 3241 2341 2134 AAAA 1231 1232 1123 8213");
        PaymentProfile profile = new PaymentProfile();
        profile.setActualRevision(revision);
        List<PaymentProfile> profiles = new ArrayList<PaymentProfile>();
        profiles.add(profile);
//        //TODO
//        //event.setProfiles(profiles);
//        t.setEvent(event);
//
//        Product product = new Product();
//        product.setCategory(ProductCategory.TICKET);
//        OrderItem ii = new OrderItem();
//        ii.setName("Jméno položky");
//        ii.setProduct(product);
//        ii.setQuantity(1);
//        ii.setUnitPrice(new BigDecimal(0));
//        ii.setVatRate(new BigDecimal(20));
//        List<OrderItem> items = new ArrayList<OrderItem>();
//        items.add(ii);
//
//        PaymentProfileRevision cust1 = new PaymentProfileRevision();
//        cust1.setName("Pavol Grešša");
//        Invoice inv1 = new Invoice();
//        inv1.setItems(items);
//        inv1.setCurrency(Currency.EUR);
//        inv1.setCustomer(cust1);
//        inv1.setPaymentType(ConfirmationChannel.PAY_PAL);
//        inv1.setPaidDate(new Date());
//        inv1.setIssueDate(new Date());
//        t.getInvoices().add(inv1);
//
//        PaymentProfileRevision cust2 = new PaymentProfileRevision();
//        cust2.setName("Josef Hubr");
//        Invoice inv2 = new Invoice();
//        inv2.setItems(items);
//        inv2.setCurrency(Currency.EUR);
//        inv2.setCustomer(cust2);
//        inv2.setPaymentType(ConfirmationChannel.BANK);
//        inv2.setPaidDate(new Date(new Date().getTime() - 123000000));
//        inv2.setIssueDate(new Date());
//        t.getInvoices().add(inv2);

        return t;
    }
}
