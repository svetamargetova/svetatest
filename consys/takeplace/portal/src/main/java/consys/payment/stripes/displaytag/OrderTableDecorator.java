package consys.payment.stripes.displaytag;

import consys.payment.bo.Order;
import consys.payment.bo.enums.OrderState;
import org.displaytag.decorator.TableDecorator;

/**
 *
 * @author pepa
 */
public class OrderTableDecorator extends TableDecorator {

    @Override
    public String addRowClass() {
        Object o = getCurrentRowObject();
        if (o instanceof Order) {
            Order order = (Order) o;

            switch (order.getState()) {
                case ACTIVE:
                    return "d_active";
                case CANCELED_ORGANIZATOR:
                case CANCELED_OWNER:
                case CANCELED_SYSTEM:
                    return "d_canceled";
                case CONFIRMED_ORGANIZATOR:
                case CONFIRMED_SYSTEM:
                case CONFIRMED_ZERO:
                    return "d_paid";
                case WAITING_TO_APPROVE:
                    return "d_waiting";
                default:
                    return null;
            }
        } else {
            return null;
        }
    }
}
