package consys.payment.service.impl;

import com.google.common.collect.MapMaker;
import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.LoadEventOverseerRequest;
import consys.payment.service.DelegateWebService;
import consys.payment.webservice.WebServiceDelegateGateway;
import consys.payment.webservice.EventOverseerService;
import consys.payment.ws.delegate.*;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DelegateWebServiceImpl implements DelegateWebService{

    private static final Logger log = LoggerFactory.getLogger(EventOverseerService.class);

    private static final int MAX_EVENT_SIZE = 100;
    private static final int INIT_SIZE = 50;
    private ConcurrentMap<String, String> eventOverseersIds;
    private AdminEventWebService adminEventWebService;
    private WebServiceDelegateGateway gateway;
    private String adminWSCtxPath;

    public DelegateWebServiceImpl() throws Exception {
        eventOverseersIds = new MapMaker().maximumSize(MAX_EVENT_SIZE).initialCapacity(INIT_SIZE).makeMap();
        gateway = new WebServiceDelegateGateway();
    }

    
    @Override
    public DelegateOrderStateChangedResponse delegateEventOrderStateChanged(DelegateOrderStateChangedRequest request) {
        return (DelegateOrderStateChangedResponse) gateway.getWebServiceTemplate().marshalSendAndReceive(getDelegatePath(request.getEventUuid()), request);
    }

    @Override
    public DelegateOrderStateChangedResponse delegateAdminOrderStateChanged(DelegateOrderStateChangedRequest request) {
        return (DelegateOrderStateChangedResponse) gateway.getWebServiceTemplate().marshalSendAndReceive(getAdminWSCtxPath(),request);
    }

    private String add(String event, String overseer) {
        if(overseer.endsWith("/")){
            overseer = overseer.substring(0,overseer.length() - 1);
        } 
        overseer = String.format("%s/ws/delegate", overseer);
        log.info("Cache path: {} : {}",event,overseer);
        eventOverseersIds.put(event, overseer);
        return overseer;
    }    

    public String getDelegatePath(String eventUuid) {
        String path = eventOverseersIds.get(eventUuid);
        if(path == null){
            LoadEventOverseerRequest r = AdminEventWebService.OBJECT_FACTORY.createLoadEventOverseerRequest();
            r.setEventUuid(eventUuid);            
            path = add(eventUuid, getAdminEventWebService().loadEventOverseer(r).getOverseerWebServiceUrl());
        }
        return path;
    }

    /**
     * @return the adminEventWebService
     */
    public AdminEventWebService getAdminEventWebService() {
        return adminEventWebService;
    }

    /**
     * @param adminEventWebService the adminEventWebService to set
     */
    public void setAdminEventWebService(AdminEventWebService adminEventWebService) {
        this.adminEventWebService = adminEventWebService;
    }

    /**
     * @return the adminWSCtxPath
     */
    public String getAdminWSCtxPath() {
        return adminWSCtxPath;
    }

    /**
     * @param adminWSCtxPath the adminWSCtxPath to set
     */
    public void setAdminWSCtxPath(String adminWSCtxPath) {
        if(adminWSCtxPath.endsWith("/")){
            adminWSCtxPath = adminWSCtxPath.substring(0,adminWSCtxPath.length() - 1);
        }        
        this.adminWSCtxPath =  String.format("%s/ws/delegate", adminWSCtxPath);
        log.info("Administration Delegate WebService Path: {}",this.adminWSCtxPath);
    }




}
