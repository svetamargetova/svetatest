package consys.payment.bo;

import consys.payment.bo.enums.TransferState;
import consys.payment.utils.PaymentUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 * Entita predstavuje prevod penaznych prostreidkov z nasho drzania organizatorovy
 *
 * @author palo
 */
public class Transfer implements BusinessObject {

    private static final long serialVersionUID = -1617622775765542882L;
    private Long id;
    /** UUID transferu */
    private String uuid;
    /** Datum transferu */
    private Date transferDate;
    /** Ci sa jedna o regularny pravidelny transfer alebo transfer na vyziadanie */
    private boolean regular;
    /** Faktura transferu */
    private Order commissionOrder;
    /** Faktury ktore su pokryte transferom */
    private List<Order> orders;
    /** Stav transferu */
    private TransferState state;
    /** Event ku ktoremu patri transfer */
    private EventPaymentProfile eventPaymentProfile;
    /** Poznamka */
    private String note;
    /** Celkova hodnota vsetkych objednavok */
    private BigDecimal totalSum = BigDecimal.ZERO;
    /** Celkova hodnota provizie */
    private BigDecimal feesSum = BigDecimal.ZERO;
    /** Celkova hodnota poplatkov tretich stran */
    private BigDecimal thirdPartyFeesSum = BigDecimal.ZERO;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * UUID transferu
     * <p/>
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID transferu
     * <p/>
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Datum transferu
     * <p/>
     * @return the transferDate
     */
    public Date getTransferDate() {
        return transferDate;
    }

    /**
     * Datum transferu
     * <p/>
     * @param transferDate the transferDate to set
     */
    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    /**
     * Ci sa jedna o regularny pravidelny transfer alebo transfer na vyziadanie
     * <p/>
     * @return the regular
     */
    public boolean isRegular() {
        return regular;
    }

    /**
     * Ci sa jedna o regularny pravidelny transfer alebo transfer na vyziadanie
     * <p/>
     * @param regular the regular to set
     */
    public void setRegular(boolean regular) {
        this.regular = regular;
    }

    /**
     * Faktura transferu
     * <p/>
     * @return the commissionInvoice
     */
    public Order getCommissionOrder() {
        return commissionOrder;
    }

    /**
     * Faktura transferu
     * <p/>
     * @param commissionInvoice the commissionInvoice to set
     */
    public void setCommissionOrder(Order commissionInvoice) {
        this.commissionOrder = commissionInvoice;
    }

    /**
     * Stav transferu
     * <p/>
     * @return the state
     */
    public TransferState getState() {
        return state;
    }

    /**
     * Stav transferu
     * <p/>
     * @param state the state to set
     */
    public void setState(TransferState state) {
        this.state = state;
    }

    /**
     * Poznamka
     * <p/>
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * Poznamka
     * <p/>
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * Prevypocitana hodnoda - sucet vsetkych faktur
     * <p/>
     * @return the totalSum
     */
    public BigDecimal getTotalSum() {
        return totalSum;
    }

    /**
     * Prevypocitana hodnoda - sucet vsetkych faktur
     * <p/>
     * @param totalSum the totalSum to set
     */
    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public void addToTotalSum(BigDecimal add) {
        setTotalSum(totalSum.add(add).setScale(4, RoundingMode.HALF_UP));
    }

    public void removeFromTotalSum(BigDecimal add) {
        setTotalSum(totalSum.subtract(add).setScale(4, RoundingMode.HALF_UP));
    }

    /**
     * Prevypocitana hodnoda - celkovo strhnute za fees
     * <p/>
     * @return the feesSum
     */
    public BigDecimal getFeesSum() {
        return feesSum;
    }

    /**
     * Prevypocitana hodnoda - celkovo strhnute za fees
     * <p/>
     * @param feesSum the feesSum to set
     */
    public void setFeesSum(BigDecimal feesSum) {
        this.feesSum = feesSum;
    }

    public void addToFeesSum(BigDecimal add) {
        setFeesSum(feesSum.add(add).setScale(4, RoundingMode.HALF_UP));
    }

    public void removeFromFeesSum(BigDecimal add) {
        setFeesSum(feesSum.subtract(add).setScale(4, RoundingMode.HALF_UP));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Transfer e = (Transfer) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Transfer[uuid=%s state=%s total=%s  fees=%s thirdPartyFees=%s]", uuid, state, totalSum, feesSum, thirdPartyFeesSum);
    }

    /**
     * Faktury ktore su pokryte transferom
     * <p/>
     * @return the orders
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * Faktury ktore su pokryte transferom
     * <p/>
     * @param orders the orders to set
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    /**
     * Event ku ktoremu patri transfer
     * <p/>
     * @return the eventPaymentProfile
     */
    public EventPaymentProfile getEventPaymentProfile() {
        return eventPaymentProfile;
    }

    /**
     * Event ku ktoremu patri transfer
     * <p/>
     * @param eventPaymentProfile the eventPaymentProfile to set
     */
    public void setEventPaymentProfile(EventPaymentProfile eventPaymentProfile) {
        this.eventPaymentProfile = eventPaymentProfile;
    }

    /**
     * Celkova hodnota poplatkov tretich stran
     * <p/>
     * @return the thirdPartyFeesSum
     */
    public BigDecimal getThirdPartyFeesSum() {
        return thirdPartyFeesSum;
    }

    /**
     * Celkova hodnota poplatkov tretich stran
     * <p/>
     * @param thirdPartyFeesSum the thirdPartyFeesSum to set
     */
    public void setThirdPartyFeesSum(BigDecimal thirdPartyFeesSum) {
        this.thirdPartyFeesSum = thirdPartyFeesSum;
    }

    public void addToThirdPartyFeesSum(BigDecimal add) {
        setThirdPartyFeesSum(thirdPartyFeesSum.add(add).setScale(4, RoundingMode.HALF_UP));
    }

    public void removeFromThirdPartyFeesSum(BigDecimal add) {
        setThirdPartyFeesSum(thirdPartyFeesSum.subtract(add).setScale(4, RoundingMode.HALF_UP));
    }

    public void appendToNote(String note) {
        if (StringUtils.isBlank(note)) {
            return;
        }
        setNote(PaymentUtils.craeteNote(this.note, note));
    }
}
