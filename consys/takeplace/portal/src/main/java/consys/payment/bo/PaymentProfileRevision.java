package consys.payment.bo;

import consys.common.utils.enums.CountryEnum;
import java.util.Date;
import org.apache.commons.lang.StringUtils;

/**
 * Payment profile revizia. Hlavny payment profil obsahuje len odkaz na aktualnu
 * reviziu
 * @author palo
 */
public class PaymentProfileRevision implements BusinessObject {

    private static final long serialVersionUID = 9088362181870966243L;
    /** id radku */
    private Long id;
    /** UUID profilu */
    private String uuid;
    /** Datum kedy bola tato revizia vytvorena */
    private Date revisionDate;
    /** Payment profil ku ktoremu revizia patri */
    private PaymentProfile parent;
    /** Meno adresata */
    private String name;
    /** Ulica */
    private String street;
    /** Mesto */
    private String city;
    /** ZIP */
    private String zip;
    /** Zem - posiela sa len nazov */
    private CountryEnum country;
    /** ICO */
    private String regNo;
    /** DIC */
    private String vatNo;
    /** Cislo bankoveho uctu IBAN */
    private String accountIban;
    /** SWIFT kod banky */
    private String accountBankSwift;
    /** Zakaznik je neplatce DPH */
    private Boolean noVatPayer;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the country
     */
    public CountryEnum getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(CountryEnum country) {
        this.country = country;
    }

    /**
     * @return the regNo
     */
    public String getRegNo() {
        return regNo;
    }

    /**
     * @param regNo the regNo to set
     */
    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    /**
     * @return the vatNo
     */
    public String getVatNo() {
        return vatNo;
    }

    /**
     * @param vatNo the vatNo to set
     */
    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        PaymentProfileRevision e = (PaymentProfileRevision) obj;
        if (StringUtils.isBlank(e.getUuid())) {
            throw new NullPointerException("Object equaling has uuid null " + e);
        }
        if (StringUtils.isBlank(uuid)) {
            throw new NullPointerException("Owner has uuid null " + this);
        }
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("PaymentProfile[name=%s street=%s city=%s zip=%s country=%s vat=%s reg=%s account=%s swift=%s]", name, street, city, zip, country, vatNo, regNo, accountIban, accountBankSwift);
    }

    /**
     * Datum kedy bola tato revizia vytvorena
     * @return the revisionDate
     */
    public Date getRevisionDate() {
        return revisionDate;
    }

    /**
     * Datum kedy bola tato revizia vytvorena
     * @param revisionDate the revisionDate to set
     */
    public void setRevisionDate(Date revisionDate) {
        this.revisionDate = revisionDate;
    }

    /**
     * Payment profil ku ktoremu revizia patri
     * @return the parent
     */
    public PaymentProfile getParent() {
        return parent;
    }

    /**
     * Payment profil ku ktoremu revizia patri
     * @param parent the parent to set
     */
    public void setParent(PaymentProfile parent) {
        this.parent = parent;
    }

    /**
     * Cislo bankoveho uctu IBAN
     * @return the accountIban
     */
    public String getAccountIban() {
        return accountIban;
    }

    /**
     * Cislo bankoveho uctu IBAN
     * @param accountIban the accountIban to set
     */
    public void setAccountIban(String accountIban) {
        this.accountIban = accountIban;
    }

    /**
     * SWIFT kod banky
     * @return the accountBankSwift
     */
    public String getAccountBankSwift() {
        return accountBankSwift;
    }

    /**
     * SWIFT kod banky
     * @param accountBankSwift the accountBankSwift to set
     */
    public void setAccountBankSwift(String accountBankSwift) {
        this.accountBankSwift = accountBankSwift;
    }

    public Boolean getNoVatPayer() {
        if (noVatPayer == null) {
            noVatPayer = false;
        }
        return noVatPayer;
    }

    public void setNoVatPayer(Boolean noVatPayer) {
        this.noVatPayer = noVatPayer;
    }
}
