package consys.payment.dao.hibernate;

import consys.payment.bo.PaymentProfile;
import consys.payment.dao.PaymentProfileDao;
import consys.payment.service.exception.NoRecordException;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PaymentProfileDaoImpl extends BusinessObjectDaoImpl<PaymentProfile> implements PaymentProfileDao {

    @Override
    public List<PaymentProfile> listEventProfiles(String uuid) throws NoRecordException {
        List<PaymentProfile> profiles = session().
                createQuery("select profile from Event e join e.profiles as profile  JOIN FETCH profile.actualRevision e.uuid=:UUID").
                setString("UUID", uuid).
                list();
        if (profiles == null || profiles.isEmpty()) {
            throw new NoRecordException("No profiles has been found for event with uuid " + uuid);
        }
        return profiles;
    }

    @Override
    public List<PaymentProfile> listUserProfiles(String uuid) throws NoRecordException {
        List<PaymentProfile> profiles = session().
                createQuery("select profile from User e join e.profiles as profile JOIN FETCH profile.actualRevision where e.uuid=:UUID").
                setString("UUID", uuid).
                list();
        if (profiles == null || profiles.isEmpty()) {
            throw new NoRecordException("No profiles has been found for user with uuid " + uuid);
        }
        return profiles;
    }

    @Override
    public PaymentProfile loadEventProfile(String eventUuid, String profileUuid) throws NoRecordException {
        PaymentProfile pp = (PaymentProfile) session().
                createQuery("select p from Event e join e.profiles as p JOIN FETCH p.actualRevision where e.uuid=:E_UUID and p.uuid=:P_UUID").
                setString("E_UUID", eventUuid).
                setString("P_UUID", profileUuid).
                uniqueResult();
        if(pp == null){
            throw new NoRecordException("No PaymentProfile with "+profileUuid+" for event "+eventUuid);
        }
        return pp;
    }

    @Override
    public PaymentProfile loadUserProfile(String userUuid, String profileUuid) throws NoRecordException {
        PaymentProfile pp = (PaymentProfile) session().
                createQuery("select p from User u join u.profiles as p JOIN FETCH p.actualRevision where u.uuid=:U_UUID and p.uuid=:P_UUID").
                setString("U_UUID", userUuid).
                setString("P_UUID", profileUuid).
                uniqueResult();
        if(pp == null){
            throw new NoRecordException("No PaymentProfile with "+profileUuid+" for user "+userUuid);
        }
        return pp;
    }

    @Override
    public PaymentProfile loadEventProfileWithActualRevision(String eventUuid, String profileUuid) throws NoRecordException {
        PaymentProfile pp = (PaymentProfile) session().
                createQuery("select p from Event e join e.profiles as p left join fetch p.actualRevision where e.uuid=:E_UUID and p.uuid=:P_UUID").
                setString("E_UUID", eventUuid).
                setString("P_UUID", profileUuid).
                uniqueResult();
        if(pp == null){
            throw new NoRecordException("No PaymentProfile with "+profileUuid+" for event "+eventUuid);
        }
        return pp;
    }

    @Override
    public PaymentProfile loadUserProfileWithActualRevision(String userUuid, String profileUuid) throws NoRecordException {
        PaymentProfile pp = (PaymentProfile) session().
                createQuery("select p from User u join u.profiles as p left join fetch p.actualRevision where u.uuid=:U_UUID and p.uuid=:P_UUID").
                setString("U_UUID", userUuid).
                setString("P_UUID", profileUuid).
                uniqueResult();
        if(pp == null){
            throw new NoRecordException("No PaymentProfile with "+profileUuid+" for user "+userUuid);
        }
        return pp;
    }
    

    @Override
    public PaymentProfile loadProfile(String profileUuid) throws NoRecordException {
          PaymentProfile profiles = (PaymentProfile) session().
                createQuery("select p from PaymentProfile p LEFT JOIN FETCH p.actualRevision where p.uuid=:UUID").
                setString("UUID", profileUuid).
                uniqueResult();
        if (profiles == null) {
            throw new NoRecordException("No profile with uuid "+profileUuid);
        }
        return profiles;
    }
    
    @Override
    public boolean isEventPaymentProfileUsedInActiveOrder(String eventPaymentProfile) {
       return  ((Long)session().
               createQuery("select count(o.id) from EventPaymentProfile p, Order o where p.uuid=:UUID and o.supplier.paymentProfile.actualRevision.id=p.paymentProfile.actualRevision.id and (o.state = 1 or o.state=5 or o.state=6)").
               setString("UUID", eventPaymentProfile).
               uniqueResult())  > 0;               
    }

    @Override
    public boolean isUserPaymentProfileUsedInActiveOrder(String userPaymentProfile) {
        return  ((Long)session().
               createQuery("select count(o.id) from User u join u.profiles as p, Order o where p.uuid=:UUID and o.customer.id=p.actualRevision.id and (o.state = 1 or o.state=5 or o.state=6)").
               setString("UUID", userPaymentProfile).
               uniqueResult()) > 0;
               
    }
}
