/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo;

/**
 *
 * @author palo
 */
public class SystemProperty implements BusinessObject {
    private static final long serialVersionUID = 8072196774478537932L;

    private Long id;
    /** Kluc property */
    private String key;
    /** Hodnota property */
    private String value;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Kluc property
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Kluc property
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Hodnota property
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Hodnota property
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        SystemProperty e = (SystemProperty) obj;

        return key.equalsIgnoreCase(e.getKey());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.key != null ? this.key.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("SystemProperty[ key=%s  value=%s ]", key,value);
    }


}
