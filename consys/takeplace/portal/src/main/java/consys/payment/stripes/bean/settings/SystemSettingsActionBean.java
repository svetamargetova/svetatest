package consys.payment.stripes.bean.settings;

import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;

/**
 *
 * @author pepa
 */
public class SystemSettingsActionBean extends PaymentActionBean {

    @DontValidate
    @DefaultHandler
    public Resolution show() {
        return new ForwardResolution("/web/settings/systemSettings.jsp");
    }
}
