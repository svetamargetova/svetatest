package consys.payment.service.impl;

import consys.common.utils.collection.Lists;
import consys.payment.bo.PaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.User;
import consys.payment.dao.UserDao;
import consys.payment.service.PaymentProfileService;
import consys.payment.service.UserService;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserServiceImpl extends Service implements UserService {

    // - Services
    private PaymentProfileService profileService;
    // - Dao
    private UserDao userDao;

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public User createUser(String uuid) throws RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("User uuid is missing");
        }
        User user = new User();
        user.setUuid(uuid);
        logger.debug("Creating {}", user);
        userDao.create(user);
        return user;
    }

    @Override
    public void createNewPaymentProfile(User user, PaymentProfileRevision newProfile)
            throws RequiredPropertyException, IbanFormatException {
        if (user == null || newProfile == null) {
            throw new RequiredPropertyException("Event or profile is null");
        }

        // Vytvorime novy profil        
        logger.debug("Creating {} new PaymentProfile with revision {}", user, newProfile);
        PaymentProfile newUserProile = new PaymentProfile();
        newUserProile.setRevisions(new HashSet<PaymentProfileRevision>());
        getProfileService().createProfile(newUserProile);
        // Vytvorime novu reviziu profilu
        newProfile.setParent(newUserProile);
        getProfileService().createProfileRevision(newProfile, false);
        // Nasavime profilu novu reviziu
        newUserProile.getRevisions().add(newProfile);
        newUserProile.setActualRevision(newProfile);
        getProfileService().updateProfile(newUserProile);
        // Nastavime profil eventu
        user.getProfiles().add(newUserProile);
        updateUser(user);
    }

    @Override
    public void createNewPaymentProfile(String userUuid, PaymentProfileRevision newProfile)
            throws RequiredPropertyException, IbanFormatException {
        if (StringUtils.isBlank(userUuid) || newProfile == null) {
            throw new RequiredPropertyException("Missing UUID od event");
        }
        // Nacitame alebo vytvorime usera
        User user = createOrLoadUser(userUuid);
        // prevolame dalej existujucu sluzbu
        createNewPaymentProfile(user, newProfile);
    }

   
    @Override
    public void createNewPaymentProfileRevision(PaymentProfile paymentProfile, PaymentProfileRevision revision)
            throws RequiredPropertyException, IbanFormatException {
        if (paymentProfile == null || revision == null) {
            throw new RequiredPropertyException("Missing profile or revision");
        }

        // Vytvorime novu reviziu profilu
        revision.setParent(paymentProfile);
        profileService.createProfileRevisionWithoutValidation(revision);

        logger.debug("Creating User new {} for profile with uuid {} ", revision, paymentProfile.getUuid());
        // Nasavime profilu novu reviziu
        paymentProfile.getRevisions().add(revision);
        paymentProfile.setActualRevision(revision);
        profileService.updateProfile(paymentProfile);

    }

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateOrCreateNewPaymentProfileRevision(String userUuid, String userPaymentProfileUuid, PaymentProfileRevision newProfile)
            throws RequiredPropertyException, IbanFormatException {
        if (StringUtils.isBlank(userUuid) || newProfile == null) {
            throw new RequiredPropertyException("Missing UUID od revision");
        }
        try {
            // skusime najprv ci vubec existuje uzivatel
            User user = loadUser(userUuid);
            if (StringUtils.isBlank(userPaymentProfileUuid)) {
                throw new NoRecordException("Throwed to create new payment profile");
            }
            boolean isRevisionUsed = profileService.isUserPaymentProfileUsedInActiveOrder(userPaymentProfileUuid);
            if (isRevisionUsed) {
                PaymentProfile pp = profileService.loadUserProfile(userUuid, userPaymentProfileUuid);                
                createNewPaymentProfileRevision(pp, newProfile);
            } else {
                logger.debug("Update User[{}] actual revision with {}", userUuid, newProfile);
                try {
                    PaymentProfile pp = profileService.loadUserProfileWithActualRevision(userUuid, userPaymentProfileUuid);
                    PaymentProfileRevision revision = pp.getActualRevision();
                    revision.setAccountBankSwift(newProfile.getAccountBankSwift());
                    revision.setAccountIban(newProfile.getAccountIban());
                    revision.setCity(newProfile.getCity());
                    revision.setCountry(newProfile.getCountry());
                    revision.setName(newProfile.getName());
                    revision.setRegNo(newProfile.getRegNo());
                    revision.setStreet(newProfile.getStreet());
                    revision.setVatNo(newProfile.getVatNo());
                    revision.setZip(newProfile.getZip());
                    newProfile.setUuid(revision.getUuid());
                    newProfile.setId(revision.getId());
                    profileService.updateProfileRevision(revision, false);
                } catch (NoRecordException ex) {
                    throw new RequiredPropertyException(ex);
                }
            }
        } catch (NoRecordException ex) {
            createNewPaymentProfile(userUuid, newProfile);
        }
    }

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    @Override
    public User loadUser(String uuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("Uuid is missing");
        }
        return userDao.loadUserByUuid(uuid);
    }

    @Override
    public User loadUserWithProfiles(String uuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("Uuid is missing");
        }
        return userDao.loadUserWithProfilesByUuid(uuid);
    }

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    /*------------------------------------------------------------------------*/
    /* ---  U T I L S --- */
    /*------------------------------------------------------------------------*/
    private User createOrLoadUser(String userUuid) throws RequiredPropertyException {
        User user = null;
        try {
            user = loadUserWithProfiles(userUuid);
        } catch (NoRecordException e) {


            user = createUser(userUuid);
        }
        if (user.getProfiles() == null) {
            List<PaymentProfile> profiles = Lists.newArrayList();
            user.setProfiles(profiles);
        }
        return user;
    }

    private void updateUser(User user) throws RequiredPropertyException {
        if (user == null || StringUtils.isBlank(user.getUuid())) {
            throw new RequiredPropertyException("User uuid is missing");
        }
        userDao.update(user);
    }
    /*========================================================================*/
    /* GETTER SETTER */
    /*========================================================================*/

    /**
     * @return the userDao
     */
    public UserDao getUserDao() {
        return userDao;
    }

    /**
     * @param userDao the userDao to set
     */
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * @return the profileService
     */
    public PaymentProfileService getProfileService() {
        return profileService;
    }

    /**
     * @param profileService the profileService to set
     */
    public void setProfileService(PaymentProfileService profileService) {
        this.profileService = profileService;
    }
}
