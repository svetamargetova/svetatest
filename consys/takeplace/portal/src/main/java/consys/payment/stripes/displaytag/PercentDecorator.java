package consys.payment.stripes.displaytag;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import javax.servlet.jsp.PageContext;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

/**
 *
 * @author pepa
 */
public class PercentDecorator implements DisplaytagColumnDecorator {

    private static final DecimalFormat format = new DecimalFormat("##0.000");

    @Override
    public Object decorate(Object o, PageContext pc, MediaTypeEnum mte) throws DecoratorException {
        if (o instanceof BigDecimal) {
            return "<div class='d_percent'>"+format.format(((BigDecimal) o).doubleValue())+"</div>";
        } else {
            return "No BigDecimal";
        }
    }
}
