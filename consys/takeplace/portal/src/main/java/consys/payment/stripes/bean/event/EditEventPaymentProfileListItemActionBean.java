package consys.payment.stripes.bean.event;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.service.EventService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EditEventPaymentProfileListItemActionBean extends PaymentActionBean {

    @SpringBean
    private EventService eventService;
    // data
    private String eventUuid;
    private String profileUuid;
    private EventPaymentProfile profile;

    @DontValidate
    @DefaultHandler
    public Resolution preEdit() throws NoRecordException, RequiredPropertyException {
        profile = eventService.loadEventPaymentProfile(profileUuid);
        return new ForwardResolution("/web/event/eventProfileDetailEdit.jsp");
    }

    public Resolution update() throws NoRecordException, RequiredPropertyException {
        EventPaymentProfile p = eventService.loadEventPaymentProfile(profileUuid);
        p.setOrderUnitFeeBank(profile.getOrderUnitFeeBank());
        p.setOrderShareFeeBank(profile.getOrderShareFeeBank());
        p.setOrderUnitFeeOnline(profile.getOrderUnitFeeOnline());
        p.setOrderShareFeeOnline(profile.getOrderShareFeeOnline());
        p.setOrderUnitFeeOrganizator(profile.getOrderUnitFeeOrganizator());
        p.setOrderShareFeeOrganizator(profile.getOrderShareFeeOrganizator());
        p.setOrderMaxFee(profile.getOrderMaxFee());
        eventService.updateEventPaymentProfile(p);
        return back();
    }

    public Resolution back() {
        RedirectResolution resolution = new RedirectResolution(EventPaymentProfileListItemActionBean.class);
        resolution.addParameter("eventUuid", eventUuid);
        resolution.addParameter("profileUuid", profileUuid);
        return resolution;
    }

    public EventPaymentProfile getProfile() {
        return profile;
    }

    public void setProfile(EventPaymentProfile profile) {
        this.profile = profile;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getProfileUuid() {
        return profileUuid;
    }

    public void setProfileUuid(String profileUuid) {
        this.profileUuid = profileUuid;
    }
}
