package consys.payment.stripes.bean.settings;

import consys.payment.bo.Product;
import consys.common.utils.enums.Currency;
import consys.payment.service.ProductService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class SystemProductListItemActionBean extends PaymentActionBean {

    @SpringBean
    private ProductService productService;
    // data
    private Product product;
    private String uuid;
    private Currency currency;

    @DontValidate
    @DefaultHandler
    public Resolution preEdit() throws NoRecordException, RequiredPropertyException {
        product = productService.loadProduct(uuid);
        return new ForwardResolution("/web/settings/systemProductsEdit.jsp");
    }

    public Resolution back() {
        ForwardResolution resolution = new ForwardResolution(SystemProductListActionBean.class);
        resolution.addParameter("currency", currency);
        return resolution;
    }

    public Resolution update() throws NoRecordException, RequiredPropertyException {
        Product p = productService.loadProduct(uuid);

        p.setName(product.getName());
        p.setPrice(product.getPrice());
        p.setVatRate(product.getVatRate());

        productService.updateEventProduct(p.getProfile(), p);
        return back();
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
