package consys.payment.webservice.impl;

import consys.payment.bo.Order;
import consys.payment.bo.PaymentProfile;
import consys.payment.service.OrderService;
import consys.payment.service.PaymentProfileService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.webservice.ProfileWebService;
import consys.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.bo.Profile;
import consys.payment.ws.profile.ListUserProfilesRequest;
import consys.payment.ws.profile.ListUserProfilesResponse;
import consys.payment.ws.profile.LoadOrderProfileRequest;
import consys.payment.ws.profile.LoadOrderProfileResponse;
import consys.payment.ws.profile.LoadProfileRequest;
import consys.payment.ws.profile.LoadProfileResponse;
import java.util.List;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class ProfileWebServiceImpl extends AbstractWebServiceImpl implements ProfileWebService {

    private PaymentProfileService paymentProfileService;
    private OrderService orderService;

    @Override
    @PayloadRoot(localPart = "LoadOrderProfileRequest", namespace = NAMESPACE)
    public LoadOrderProfileResponse loadOrderProfile(LoadOrderProfileRequest request) {
        try {
            Order order = orderService.loadOrderByUuid(request.getOrderUuid());
            LoadOrderProfileResponse response = OBJECT_FACTORY.createLoadOrderProfileResponse();
            response.setProfile(ConvertUtils.toWsProfile(order.getSupplier().getPaymentProfile()));
            return response;
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No order with inserted uuid");
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing order uuid");
        }
    }

    @Override
    @PayloadRoot(localPart = "LoadProfileRequest", namespace = NAMESPACE)
    public LoadProfileResponse loadProfile(LoadProfileRequest request) {
        try {
            PaymentProfile profile = getPaymentProfileService().loadPaymentProfile(request.getProfileUuid());
            LoadProfileResponse response = OBJECT_FACTORY.createLoadProfileResponse();
            response.setProfile(ConvertUtils.toWsProfile(profile));
            return response;
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No PaymentProfile with inserted uuid");
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing uuid", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "ListUserProfilesRequest", namespace = NAMESPACE)
    public ListUserProfilesResponse listUserProfiles(ListUserProfilesRequest request) {
        ListUserProfilesResponse resp = OBJECT_FACTORY.createListUserProfilesResponse();
        try {
            String userUuid;
            if (request.getManagedUserUuid() != null) {
                userUuid = request.getManagedUserUuid();
            } else {
                userUuid = request.getUserUuid();
            }
            List<PaymentProfile> profile = getPaymentProfileService().listUserProfile(userUuid);
            for (PaymentProfile paymentProfile : profile) {
                Profile pro = ConvertUtils.toWsProfile(paymentProfile);
                resp.getProfiles().add(pro);
            }
            return resp;
        } catch (NoRecordException ex) {
            return resp;
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing uuid", ex);
        }
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public PaymentProfileService getPaymentProfileService() {
        return paymentProfileService;
    }

    public void setPaymentProfileService(PaymentProfileService paymentProfileService) {
        this.paymentProfileService = paymentProfileService;
    }
}
