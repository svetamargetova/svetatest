package consys.payment.service;

import consys.payment.bo.PaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface PaymentProfileService {

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/

    public void createProfileRevisionWithoutValidation(PaymentProfileRevision rev) throws RequiredPropertyException;

    public void createProfileRevision(PaymentProfileRevision rev, boolean isEventProfile)
            throws RequiredPropertyException,IbanFormatException;

    public void createProfile(PaymentProfile profile)
            throws RequiredPropertyException;

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
   
    /**
     * @throws RequiredPropertyException ak profile nema nastavenu ziadnu reviziiu 
     */
    public void updateProfile(PaymentProfile profile) throws RequiredPropertyException;

    public void updateProfileRevision(PaymentProfileRevision revision, boolean event)
            throws RequiredPropertyException, IbanFormatException;

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/

    public PaymentProfile loadPaymentProfile(String uuid) 
            throws NoRecordException,RequiredPropertyException;  

    public PaymentProfile loadEventProfile(String eventUuid , String profileUuid)
            throws NoRecordException,RequiredPropertyException;

    public PaymentProfile loadUserProfile(String userUuid , String profileUuid)
            throws NoRecordException,RequiredPropertyException;

    public PaymentProfile loadEventProfileWithActualRevision(String eventUuid , String profileUuid)
            throws NoRecordException,RequiredPropertyException;

    public PaymentProfile loadUserProfileWithActualRevision(String userUuid , String profileUuid)
            throws NoRecordException,RequiredPropertyException;

    public Integer loadProfileRevisionUsedCount(String revisionUuid) throws RequiredPropertyException;
    
    public boolean isEventPaymentProfileUsedInActiveOrder(String paymentProfileUuid)
            throws RequiredPropertyException;
    
    public boolean isUserPaymentProfileUsedInActiveOrder(String paymentProfileUuid)
            throws RequiredPropertyException;

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    public List<PaymentProfile> listEventProfile(String eventUuid)
            throws NoRecordException,RequiredPropertyException;

    public List<PaymentProfile> listUserProfile(String userUuid)
            throws NoRecordException,RequiredPropertyException;

    /*------------------------------------------------------------------------*/
    /* ---  D E L E T E --- */
    /*------------------------------------------------------------------------*/
    public void deleteEventProfile(String eventUuid)
            throws NoRecordException,RequiredPropertyException;

    public void deleteUserProfile(String userUuid)
            throws NoRecordException,RequiredPropertyException;

}
