package consys.payment.utils;

import consys.common.utils.BigDecimalUtils;
import consys.payment.bo.OrderItem;
import consys.payment.bo.enums.ProductCategory;
import java.math.BigDecimal;

/**
 *
 * <p>
 * Z vlozeneho itemu vypocita jednotlive polozky ktore ulozi do schranky.
 * Unit price ktora sa nachadza v <code>item</code> je konecna suma so zapo
 * citanym VAT a nezapocitanym DISCOUNT.
 * <p>
 * Objekt je Immutable, takze po prvom nastaveni sa vypocitaju vsekty vlastnosti
 * <p>
 * Schranka pro spoctiane vlastnosti jedneho invoice itemu:
 *<ul>
 * <li> Nazev polozky
 * <li> Kvantita
 * <li> Dan na typ polozky
 * <li> Unit price - kolko stoji jedna polozka
 * <li> VAT Amount - kolko sa zaplati dan za vsecky polozky. Tj. <Unit>*<quantity>*<VAT_RATE>
 * <li> NET Price - kolko je cista cena bez dane Tj. <Unit>*<quantity>
 * <li> TOTAL Price - kolko je cista cena s danou 
 *</ul>
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class InvoiceItemRecord {

    private final OrderItem item;
    /** Dan z jednotky */
    private final BigDecimal unitVatAmount;
    /** Cena jednotky bez dane */
    private final BigDecimal unitNetPrice;
    /** Quantita v big decimal */
    private final BigDecimal quantity;
    /** Cena jednotky bez dane */
    private final BigDecimal netPriceWithVat;

    public InvoiceItemRecord(OrderItem item) {

        this.item = item;

        quantity = new BigDecimal(item.getQuantity(), BigDecimalUtils.DEFAULT);

        // ak je potlacenie vypoctu tak vraciame DPH ako NULU
        unitVatAmount = item.getUnitPriceDiscounted().multiply(BigDecimalUtils.computeVatCoefficient(item.getVatRate()), BigDecimalUtils.DEFAULT);

        // ak je potlacenie vypoctu tak vraciame len net sumu
        unitNetPrice = item.getUnitPriceDiscounted().subtract(unitVatAmount, BigDecimalUtils.DEFAULT);

        // cena celkem bez odectene dane
        netPriceWithVat = item.getUnitPriceDiscounted();
    }

    /**
     * Nazov polozky
     * @return the name
     */
    public String getName() {
        if (item.getDiscount() > 0) {
            return String.format("%s (discount %d%%)", item.getName(), item.getDiscount());
        } else {
            return item.getName();
        }
    }

    /**
     * Kolko poloziek
     * @return the quantity
     */
    public int getQuantity() {
        return item.getQuantity();
    }

    /**
     * Kolko je discount - 20%
     * @return the discount
     */
    public int getDiscount() {
        return item.getDiscount();
    }

    /**
     * Zaklad dane -  v percentach 20%
     * V pripade potlacenia vypoctu dane sa vracia 0.
     * @return the vatRate
     */
    public BigDecimal getVatRate() {
        return item.getVatRate();
    }

    /**
     * Kategoria produktu
     * @return the productCategory
     */
    public ProductCategory getProductCategory() {
        return item.getProduct().getCategory();
    }

    @Override
    public String toString() {
        return String.format("Item[ name=%s quantity=%d discount=%d rate=%s unit=%s unitVat=%s unitNet=%s ]",
                getName(), getQuantity(), getDiscount(), getVatRate().toPlainString(), getUnitPrice().toPlainString(), getUnitVatAmount().toPlainString(), getUnitNetPrice().toPlainString());
    }

    /*------------------------------------------------------------------------*/
    /*----- U N I T  -----*/
    /*------------------------------------------------------------------------*/
    /**
     * Vysledna cena jednotky po zlavneni s danou.
     * Ak je potlacenie vypoctu dane tak sa vrati unitNetPrice
     */
    public BigDecimal getUnitPrice() {
        return item.getUnitPriceDiscounted();
    }

    /**
     * Vysledna cena celej polozky. <code> unitPrice * quantity </code>
     * Ak je potlacenie vypoctu dane tak sa vrati unitNetPrice
     */
    public BigDecimal getPrice() {
        return item.getTotal();
    }

    /*------------------------------------------------------------------------*/
    /*----- D A N E  -----*/
    /*------------------------------------------------------------------------*/
    /**
     * Kolko je dan na jednu unitu
     * @return the unitVatAmount
     */
    public BigDecimal getUnitVatAmount() {
        return unitVatAmount;
    }

    /**
     * Celkova dan z polozky: <code> unitVatAmount * quantity </code>
     * @return
     */
    public BigDecimal getVatAmount() {
        return unitVatAmount.multiply(quantity, BigDecimalUtils.DEFAULT);
    }

    /*------------------------------------------------------------------------*/
    /*----- U N I T  B E Z   D A N E -- NET  -----*/
    /*------------------------------------------------------------------------*/
    /**
     * Cena jednotky bez dane
     * @return
     */
    public BigDecimal getUnitNetPrice() {
        return unitNetPrice;
    }

    /**
     * Cena celej polozky bez dane: <code> unitNetPrice * quantity </code>
     * @return
     */
    public BigDecimal getNetPrice() {
        return unitNetPrice.multiply(quantity, BigDecimalUtils.DEFAULT);
    }

    /** Cena cele polozky s dani */
    public BigDecimal getNetPriceWithVat() {
        return netPriceWithVat.multiply(quantity, BigDecimalUtils.DEFAULT);
    }
}
