package consys.payment.stripes.displaytag;

import consys.payment.bo.enums.OrderState;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.servlet.jsp.PageContext;
import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

/**
 *
 * @author pepa
 */
public class DueDateDecorator implements DisplaytagColumnDecorator {

    private static final FastDateFormat format = FastDateFormat.getInstance("dd.MM.yyyy", Calendar.getInstance().getTimeZone(), Locale.getDefault());
    private static final SimpleDateFormat parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S", Locale.getDefault());
    private static final String NO_DATE = "No due date";

    @Override
    public Object decorate(Object o, PageContext pc, MediaTypeEnum mte) throws DecoratorException {
        if (o == null) {
            return NO_DATE;
        }
        String[] splits = o.toString().split(";");
        if (splits.length != 2) {
            return NO_DATE;
        }
        try {
            Date date = parse.parse(splits[1]);
            OrderState state = OrderState.valueOf(splits[0]);
            String value = format.format(date);

            if (state.equals(OrderState.WAITING_TO_APPROVE) && date.getTime() < System.currentTimeMillis()) {
                value = "<div class='d_dueDate'>" + value + "</div>";
            }
            return value;
        } catch (Exception ex) {
            return NO_DATE;
        }
    }
}
