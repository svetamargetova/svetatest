package consys.payment.webservice.impl;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Product;

import consys.payment.service.AcemceeService;
import consys.payment.service.EventService;
import consys.payment.service.ProductService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.webservice.ProductWebService;
import consys.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.product.CreateEventProductRequest;
import consys.payment.ws.product.CreateEventProductResponse;
import consys.payment.ws.product.CreateEventProductsRequest;
import consys.payment.ws.product.CreateEventProductsResponse;
import consys.payment.ws.product.DeleteEventProductsRequest;
import consys.payment.ws.product.DeleteEventProductsResponse;
import consys.payment.ws.product.LoadLicenseProductRequest;
import consys.payment.ws.product.LoadLicenseProductResponse;
import consys.payment.ws.product.LoadProductRequest;
import consys.payment.ws.product.LoadProductResponse;
import consys.payment.ws.product.UpdateEventProductRequest;
import consys.payment.ws.product.UpdateEventProductResponse;
import consys.payment.ws.product.UpdateEventProductsRequest;
import consys.payment.ws.product.UpdateEventProductsResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class ProductWebServiceImpl extends AbstractWebServiceImpl implements ProductWebService {

    private EventService eventService;
    private AcemceeService acemceeService;
    private ProductService productService;

    @Override
    @PayloadRoot(localPart = "CreateEventProductRequest", namespace = NAMESPACE)
    public CreateEventProductResponse createProduct(CreateEventProductRequest request) {
        try {
            getProductService().createEventProduct(request.getEventUuid(), ConvertUtils.toCoreProduct(request.getProduct()));
            return OBJECT_FACTORY.createCreateEventProductResponse();
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Event with inserted uuid exists", ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "CreateEventProductsRequest", namespace = NAMESPACE)
    public CreateEventProductsResponse createProducts(CreateEventProductsRequest request) {
        try {
            getProductService().createEventProducts(request.getEventUuid(), ConvertUtils.toCoreProducts(request.getProducts()));
            return OBJECT_FACTORY.createCreateEventProductsResponse();
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Event with inserted uuid exists", ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "UpdateEventProductRequest", namespace = NAMESPACE)
    public UpdateEventProductResponse updateProduct(UpdateEventProductRequest request) {
        try {
            getProductService().updateEventProduct(request.getEventUuid(), ConvertUtils.toCoreProduct(request.getProduct()));            
            return OBJECT_FACTORY.createUpdateEventProductResponse();
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Event with inserted uuid exists", ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "UpdateEventProductsRequest", namespace = NAMESPACE)
    public UpdateEventProductsResponse updateProducts(UpdateEventProductsRequest request) {
        try {
            getProductService().updateEventProducts(request.getEventUuid(), ConvertUtils.toCoreProducts(request.getProducts()));
            return OBJECT_FACTORY.createUpdateEventProductsResponse();
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Event with inserted uuid exists", ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "LoadProductRequest", namespace = NAMESPACE)
    public LoadProductResponse loadProduct(LoadProductRequest request) {
        try {
            Product product = getProductService().loadProduct(request.getProductUuid());
            LoadProductResponse resp = OBJECT_FACTORY.createLoadProductResponse();
            resp.setProduct(ConvertUtils.toWsProduct(product));
            return resp;
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Product with inserted uuid " + request.getProductUuid(), ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
    }

    @Override
    @PayloadRoot(localPart = "LoadLicenseProductRequest", namespace = NAMESPACE)
    public LoadLicenseProductResponse loadLicenseProduct(LoadLicenseProductRequest request) {
        LoadLicenseProductResponse resp = OBJECT_FACTORY.createLoadLicenseProductResponse();
        try {
            EventPaymentProfile profile = eventService.loadEventPaymentProfile(request.getEventProfileUuid());
            Product p = acemceeService.loadLicenseProduct(profile.getCurrency());
            resp.setLicense(ConvertUtils.toWsProduct(p));
            return resp; 
        } catch (NoRecordException ex) {
            throw new NullPointerException("NoRecords for event profile");
        } catch (RequiredPropertyException ex) {
            throw new NullPointerException("Missing profile uuid");
        }                               
    }

    @Override
    @PayloadRoot(localPart = "DeleteEventProductsRequest", namespace = NAMESPACE)
    public DeleteEventProductsResponse deleteEventProducts(DeleteEventProductsRequest request) {
        DeleteEventProductsResponse response = OBJECT_FACTORY.createDeleteEventProductsResponse();
        try {
            getProductService().deleteProducts(request.getEventUuid(), request.getProducts());
        } catch (NoRecordException ex) {
            throw new IllegalArgumentException("No Event or products", ex);
        } catch (RequiredPropertyException ex) {
            throw new IllegalArgumentException("Missing required properties", ex);
        }
        return response;
    }

    /**
     * @return the productService
     */
    public ProductService getProductService() {
        return productService;
    }

    /**
     * @param productService the productService to set
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @return the acemceeService
     */
    public AcemceeService getAcemceeService() {
        return acemceeService;
    }

    /**
     * @param acemceeService the acemceeService to set
     */
    public void setAcemceeService(AcemceeService acemceeService) {
        this.acemceeService = acemceeService;
    }
}
