package consys.payment.stripes.bean.settings;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.common.utils.enums.Currency;
import consys.payment.service.AcemceeService;
import consys.payment.service.EventService;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class ProfileSettingsActionBean extends PaymentActionBean {

    @SpringBean
    private AcemceeService acemceeService;
    @SpringBean
    private EventService eventService;
    /** typ profilu podle meny */
    private String type;
    private PaymentProfileRevision revision;

    @DontValidate
    @DefaultHandler
    public Resolution show() {
        EventPaymentProfile profile = acemceeService.loadEventPaymentProfile(Currency.valueOf(type));
        revision = profile.getPaymentProfile().getActualRevision();
        return new ForwardResolution("/web/settings/profileSettings.jsp");
    }

    public Resolution edit() {
        EventPaymentProfile profile = acemceeService.loadEventPaymentProfile(Currency.valueOf(type));
        revision = profile.getPaymentProfile().getActualRevision();
        return new ForwardResolution("/web/settings/profileSettingsEdit.jsp");
    }

    public Resolution update() throws RequiredPropertyException, IbanFormatException {
        EventPaymentProfile profile = acemceeService.loadEventPaymentProfile(Currency.valueOf(type));
        eventService.updateEventPaymentProfileRevision(profile, revision);
        return new ForwardResolution("/web/settings/profileSettings.jsp");
    }

    public Resolution back() {
        return show();
    }

    public PaymentProfileRevision getRevision() {
        return revision;
    }

    public void setRevision(PaymentProfileRevision revision) {
        this.revision = revision;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
