package consys.payment.stripes.bean.order;

import consys.payment.bo.ApproveOrder;
import consys.payment.bo.Order;
import consys.payment.bo.enums.OrderFilterEnum;
import consys.payment.bo.enums.OrderState;
import consys.payment.service.OrderService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.OrderAlreadyPayedException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.ListActionBean;
import consys.payment.stripes.bean.event.EventListActionBean;
import java.util.Date;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class OrderListActionBean extends ListActionBean<Order> {

    @SpringBean
    protected OrderService orderService;
    // konstanty
    protected final String actionCancelJsp;
    protected final String actionConfirmJsp;
    protected final String actionApproveJsp;
    protected final String actionRefundJsp;
    // data
    protected String uuid;
    protected String note;
    protected Order order;
    protected boolean generateInvoice = true;
    protected boolean notifyUser = true;
    protected boolean approve = false;
    protected String eventUuid;
    // data - filtr
    protected String number = ""; // uuid nebo cislo objednavky
    protected Date pFrom; // zakoupeno-vytvoreno od
    protected Date pTo; // zakoupeno-vytvoreno do
    protected Date cFrom; // potvrzeno od
    protected Date cTo; // potvrzeno do
    protected OrderFilterEnum stateFilter = OrderFilterEnum.ALL; // filtr stavu

    public OrderListActionBean() {
        super("/web/order/orders.jsp");
        actionCancelJsp = "/web/order/cancelOrder.jsp";
        actionConfirmJsp = "/web/order/confirmOrder.jsp";
        actionApproveJsp = "/web/order/approveOrder.jsp";
        actionRefundJsp = "/web/order/refundOrder.jsp";
    }

    public OrderListActionBean(String orderJsp, String actionCancelJsp, String actionConfirmJsp, String actionApproveJsp, String actionRefundJsp) {
        super(orderJsp);
        this.actionCancelJsp = actionCancelJsp;
        this.actionConfirmJsp = actionConfirmJsp;
        this.actionApproveJsp = actionApproveJsp;
        this.actionRefundJsp = actionRefundJsp;
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        getItems().setList(orderService.listEventOrders(eventUuid, stateFilter, pFrom, pTo, cFrom, cTo, number, firstItemIndex, itemsPerPage));
        getItems().setTotal(orderService.loadEventOrdersCount(eventUuid, stateFilter, pFrom, pTo, cFrom, cTo, number));
    }

    public Resolution actionCancel() throws Exception {
        order = orderService.loadOrderByUuid(uuid);
        return new ForwardResolution(actionCancelJsp);
    }

    public Resolution confirmCancel() throws NoRecordException, RequiredPropertyException, OrderAlreadyPayedException, Exception {
        orderService.cancelOrder(uuid, OrderState.CANCELED_SYSTEM, note,true);
        return list();
    }

    public Resolution actionApprove() throws Exception {
        order = orderService.loadOrderByUuid(uuid);
        return new ForwardResolution(actionApproveJsp);
    }
    
    
    public Resolution actionRefund() throws Exception {
        order = orderService.loadOrderByUuid(uuid);        
        return new ForwardResolution(actionRefundJsp);
    }
    
    public Resolution confirmRefund() throws Exception {
        order = orderService.loadOrderByUuid(uuid);        
        orderService.cancelPaidOrder(order, note, notifyUser);
        return new ForwardResolution(actionApproveJsp);
    }
        
    public Resolution confirmApprove() throws NoRecordException, RequiredPropertyException, Exception {
        ApproveOrder ao = orderService.loadApproveOrderByOrderUuid(uuid);
        orderService.updateOrderApprove(ao.getId(), approve, note);
        return list();
    }

    public Resolution actionConfirm() throws NoRecordException, RequiredPropertyException {
        order = orderService.loadOrderByUuid(uuid);
        return new ForwardResolution(actionConfirmJsp);
    }

    public Resolution confirmConfirm() throws NoRecordException, RequiredPropertyException, Exception {        
        order = orderService.loadOrderByUuid(uuid);
        orderService.updateOrderToConfirmedByAdministrator(order, generateInvoice, notifyUser,note);
        return list();
    }

    /** pouziva se jen z eventu, pokud i jinde, je potreba doplnit presmerovani */
    public Resolution back() {
        return new RedirectResolution(EventListActionBean.class);
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    public boolean isGenerateInvoice() {
        return generateInvoice;
    }

    public void setGenerateInvoice(boolean generateInvoice) {
        this.generateInvoice = generateInvoice;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isNotifyUser() {
        return notifyUser;
    }

    public void setNotifyUser(boolean notifyUser) {
        this.notifyUser = notifyUser;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Date getcFrom() {
        return cFrom;
    }

    public void setcFrom(Date cFrom) {
        this.cFrom = cFrom;
    }

    public Date getcTo() {
        return cTo;
    }

    public void setcTo(Date cTo) {
        this.cTo = cTo;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getpFrom() {
        return pFrom;
    }

    public void setpFrom(Date pFrom) {
        this.pFrom = pFrom;
    }

    public Date getpTo() {
        return pTo;
    }

    public void setpTo(Date pTo) {
        this.pTo = pTo;
    }

    public OrderFilterEnum getStateFilter() {
        return stateFilter;
    }

    public void setStateFilter(OrderFilterEnum stateFilter) {
        this.stateFilter = stateFilter;
    }
}
