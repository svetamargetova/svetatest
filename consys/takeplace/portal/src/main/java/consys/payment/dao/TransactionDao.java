/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao;

import consys.payment.bo.Transaction;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.service.exception.NoRecordException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public interface TransactionDao extends PersistedObjectDao<Transaction> {
    
    public Transaction loadTransactionByUuid(String uuid) throws NoRecordException;
    
    public Transaction loadTransactionByTransactionId(String transactionId) throws NoRecordException;
    
    public List<Transaction> listTransactions(ConfirmationChannel channel, Date from, Date to, int startItem, int itemsPerPage);
    
    public int listTransactionsCount(ConfirmationChannel channel, Date from, Date to);
    
    
}
