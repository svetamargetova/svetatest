package consys.payment.webservice;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author Palo
 */
public class WebServiceDelegateGateway extends WebServiceGatewaySupport {

    public WebServiceDelegateGateway() throws Exception {
        super();        
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("consys.payment.ws.delegate");
        marshaller.afterPropertiesSet();
        setMarshaller(marshaller);
        setUnmarshaller(marshaller);
        afterPropertiesSet();        
    }
}
