package consys.payment.service.impl;

import consys.payment.SystemProperties;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Product;
import consys.common.utils.enums.Currency;
import consys.payment.service.AcemceeService;
import consys.payment.service.EventService;
import consys.payment.service.ProductService;
import consys.payment.service.SystemPropertyService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;

/**
 *
 * @author palo
 */
public class AcemceeServiceImpl extends Service implements AcemceeService, SystemProperties {

    private SystemPropertyService systemPropertyService;
    private EventService eventService;
    private ProductService productService;

    @Override
    public EventPaymentProfile loadEventPaymentProfile(Currency currency) {
        try {
            switch (currency) {
                case CZK:
                    return getEventService().loadEventPaymentProfile(CZK_ACMC_PROFILE_UUID);
                case EUR:
                    return getEventService().loadEventPaymentProfile(EUR_ACMC_PROFILE_UUID);
                case USD:
                    return getEventService().loadEventPaymentProfile(USD_ACMC_PROFILE_UUID);

            }
        } catch (RequiredPropertyException ex) {
        } catch (NoRecordException ex) {
        }
        throw new NullPointerException("Missing something");
    }

    @Override
    public Product loadLicenseProduct(Currency currency) {
        try {
            switch (currency) {
                case CZK:
                    return getProductService().loadProduct(CZK_ACMC_PRODUCT_LICENSE_UUID);
                case EUR:
                    return getProductService().loadProduct(EUR_ACMC_PRODUCT_LICENSE_UUID);
                case USD:
                    return getProductService().loadProduct(USD_ACMC_PRODUCT_LICENSE_UUID);

            }
        } catch (RequiredPropertyException ex) {
        } catch (NoRecordException ex) {
        }
        throw new NullPointerException("Missing something");
    }

    @Override
    public Product loadCommissionProduct(Currency currency) {

        try {
            switch (currency) {
                case CZK:
                    return getProductService().loadProduct(CZK_ACMC_PRODUCT_COMMISSION_UUID);
                case EUR:
                    return getProductService().loadProduct(EUR_ACMC_PRODUCT_COMMISSION_UUID);
                case USD:
                    return getProductService().loadProduct(USD_ACMC_PRODUCT_COMMISSION_UUID);

            }
        } catch (RequiredPropertyException ex) {
        } catch (NoRecordException ex) {
        }
        throw new NullPointerException("Missing something");
    }

    @Override
    public Product loadThirdPartyFeeProduct(Currency currency) {
        try {
            switch (currency) {
                case CZK:
                    return getProductService().loadProduct(CZK_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID);
                case EUR:
                    return getProductService().loadProduct(EUR_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID);
                case USD:
                    return getProductService().loadProduct(USD_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID);

            }
        } catch (RequiredPropertyException ex) {
        } catch (NoRecordException ex) {
        }
        throw new NullPointerException("Missing something");
    }

    /**
     * @return the systemPropertyService
     */
    public SystemPropertyService getSystemPropertyService() {
        return systemPropertyService;
    }

    /**
     * @param systemPropertyService the systemPropertyService to set
     */
    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {
        return eventService;
    }

    /**
     * @param eventService the eventService to set
     */
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    /**
     * @return the productService
     */
    public ProductService getProductService() {
        return productService;
    }

    /**
     * @param productService the productService to set
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public String loadLicenseProductUuid(Currency currency) {
        switch (currency) {
            case CZK:
                return CZK_ACMC_PRODUCT_LICENSE_UUID;
            case EUR:
                return EUR_ACMC_PRODUCT_LICENSE_UUID;
            case USD:
                return USD_ACMC_PRODUCT_LICENSE_UUID;
            default:
                return null;
        }
    }

    @Override
    public String loadCommissionProductUuid(Currency currency) {
        switch (currency) {
            case CZK:
                return CZK_ACMC_PRODUCT_COMMISSION_UUID;
            case EUR:
                return EUR_ACMC_PRODUCT_COMMISSION_UUID;
            case USD:
                return USD_ACMC_PRODUCT_COMMISSION_UUID;
            default:
                return null;
        }
    }

    @Override
    public String loadThirdPartyFeeProductUuid(Currency currency) {
        switch (currency) {
            case CZK:
                return CZK_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID;
            case EUR:
                return EUR_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID;
            case USD:
                return USD_ACMC_PRODUCT_THIRD_PARTY_FEE_UUID;
            default:
                return null;
        }
    }
}
