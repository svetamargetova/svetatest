package consys.payment.export.servlet;

import consys.payment.bo.Invoice;
import consys.payment.bo.Order;
import consys.payment.bo.Transfer;
import consys.payment.bo.enums.TransferState;
import consys.payment.export.pdf.PdfInvoiceGenerator;
import consys.payment.service.AcemceeService;
import consys.payment.service.TransferService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.utils.PaymentUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DownloadTransferServlet extends HttpServlet implements DownloadServletFields {

    private static final long serialVersionUID = 4675783911089360883L;
    // logger
    private static final Logger logger = LoggerFactory.getLogger(DownloadTransferServlet.class);
    // konstanty
    private static final String TRANSFER_PARAM = "invoice";
    private static final String EVENT_PARAM = "eid";
    // data
    private TransferService transferService;
    private PdfInvoiceGenerator generator = new PdfInvoiceGenerator();
    // servicy
    private AcemceeService acemceeService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String transferUuid = request.getParameter(TRANSFER_PARAM);
            if (StringUtils.isBlank(transferUuid)) {
                logger.debug("Transfer UUID is missing");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            String eventUuid = request.getParameter(EVENT_PARAM);
            if (StringUtils.isBlank(eventUuid)) {
                logger.debug("Event UUID is missing");

                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            // Nacitame transfer
            Transfer transfer = getTransferService().loadTransferWithOrders(transferUuid);

            // Natavime hlavicku downloadu
            StringBuilder contentDisposition = new StringBuilder();
            contentDisposition.append(INLINE_FILENAME);

            SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
            contentDisposition.append("transfer_pack_");
            if (transfer.getTransferDate() != null) {
                contentDisposition.append(format.format(transfer.getTransferDate()));
            }
            contentDisposition.append(transfer.getUuid()).append(".zip");

            contentDisposition.append(END_NAME);
            response.setContentType(CONTENT_TYPE_ZIP);
            response.setHeader(CONTENT_DISPOSITION, contentDisposition.toString());
            response.setHeader(CACHE_CONTROL, MAXAGE_1S);
            response.setHeader(PRAGMA, PUBLIC);

            // vytvorime ZIP
            ZipArchiveOutputStream zipOutputStream = new ZipArchiveOutputStream(response.getOutputStream());
            zipOutputStream.setFallbackToUTF8(true);

            // write transfer invoice
            if (transfer.getState().equals(TransferState.CLOSED_AND_PREPARED)) {
                writeOrderIntoZip(transfer.getCommissionOrder(), zipOutputStream);
            }
            if (transfer.getState().equals(TransferState.TRANSFERED)) {
                writeInvoiceIntoZip(transfer.getCommissionOrder().getInvoice(), zipOutputStream);
            }
            // write secky dalsi invoices
            List<Order> orders = PaymentUtils.transferOrdersWithoutDuplicity(transfer.getOrders());
            for (Order invoice : orders) {
                if (invoice.getInvoice() != null) {
                    writeInvoiceIntoZip(invoice.getInvoice(), zipOutputStream);
                }
            }

            zipOutputStream.close();

        } catch (RequiredPropertyException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Invoice UUID is missing");
            }
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch (NoRecordException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Invoice UUID is missing");
            }
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        } catch (Exception ex) {
            logger.error("What the hell happend!?", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    private void writeInvoiceIntoZip(Invoice invoice, ZipArchiveOutputStream zipOutputStream) throws Exception {
        // vytvorime archiv
        ZipArchiveEntry entry = new ZipArchiveEntry(invoice.getInvoiceNumber() + ".pdf");
        // vlozime
        zipOutputStream.putArchiveEntry(entry);
        // temp outputstream do ktoreho sa nageneruje pdf
        ByteArrayOutputStream tmpOutputStream = new ByteArrayOutputStream();
        // vytvorime pdf
        generator.setAcemceeService(getAcemceeService());
        generator.createPdfInvoice(tmpOutputStream, invoice, true);
        // zapiseme pdf
        zipOutputStream.write(tmpOutputStream.toByteArray());
        // zatvorime archive
        zipOutputStream.closeArchiveEntry();
    }

    private void writeOrderIntoZip(Order order, ZipArchiveOutputStream zipOutputStream) throws Exception {
        // vytvorime archiv
        ZipArchiveEntry entry = new ZipArchiveEntry("proforma-" + order.getOrderNumber() + ".pdf");
        // vlozime
        zipOutputStream.putArchiveEntry(entry);
        // temp outputstream do ktoreho sa nageneruje pdf
        ByteArrayOutputStream tmpOutputStream = new ByteArrayOutputStream();
        // vytvorime pdf
        generator.setAcemceeService(getAcemceeService());
        generator.createPdfOrder(tmpOutputStream, order, true);
        // zapiseme pdf
        zipOutputStream.write(tmpOutputStream.toByteArray());
        // zatvorime archive
        zipOutputStream.closeArchiveEntry();
    }

    public TransferService getTransferService() {
        if (transferService == null) {
            transferService = (TransferService) WebApplicationContextUtils.getWebApplicationContext(getServletContext()).getBean("transferService", TransferService.class);
        }
        return transferService;
    }

    public AcemceeService getAcemceeService() {
        if (acemceeService == null) {
            acemceeService = (AcemceeService) WebApplicationContextUtils.getWebApplicationContext(getServletContext()).getBean("acemceeService", AcemceeService.class);
        }
        return acemceeService;
    }
}
