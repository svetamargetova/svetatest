/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo;

import consys.common.utils.enums.Currency;
import consys.payment.SystemProperties;
import consys.payment.bo.enums.InvoiceProcessType;
import java.math.BigDecimal;

/**
 *
 * @author palo
 */
public class EventPaymentProfile implements BusinessObject{
    private static final long serialVersionUID = -4864320112772613639L;
   
    private Long id;
    /** UUID profilu je rovnake ako uuid platboneho profilu */
    private String uuid;
    /** Event ku ktoremu patri tento platobny event profil */
    private Event event;
    /** Platobny profil */
    private PaymentProfile paymentProfile;
    /** Hodnota kolko sa berie za kazdu objednavku ak je potvrdena z bankovej transakcie */
    private BigDecimal orderUnitFeeBank;
    /** Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena z bankovej transakcie */
    private BigDecimal orderShareFeeBank;
    /** Hodnota kolko sa berie za kazdu objednavku ak je potvrdena z online transakcie */
    private BigDecimal orderUnitFeeOnline;
    /** Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena z online transakcie */
    private BigDecimal orderShareFeeOnline;    
    /** Hodnota kolko sa berie za kazdu objednavku ak je potvrdena organizatorom */
    private BigDecimal orderUnitFeeOrganizator;
    /** Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena organizatorom */
    private BigDecimal orderShareFeeOrganizator;  
    /** Maximalna hodnota ktoru si bereme ako proviziu */
    private BigDecimal orderMaxFee;        
    /** Mena eventu */
    private Currency currency;
    /** Typ spracovania dane  */
    private InvoiceProcessType vatType;    
    /** Citac pre produkty na fakturach */
    private int orderInvoiceCounter = 0;
    /** Citac rady */
    private int orderInvoiceSeriesCounter = 2;
    /** Citac na prevody na fakturach */
    private int transferInvoiceCounter = 0;
    /** Priznak ci je profil stale aktivny */
    private boolean active;
    /** Priznak ze tento profil sa ma objavit na fakture v informaciach o bankovom prevode */
    private boolean orderBankTransferInvoicedToThisProfile;
    /** Poznamka na fakture */
    private String invoiceNote;
    

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Platobny profil
     * @return the paymentProfile
     */
    public PaymentProfile getPaymentProfile() {
        return paymentProfile;
    }

    /**
     * Platobny profil
     * @param paymentProfile the paymentProfile to set
     */
    public void setPaymentProfile(PaymentProfile paymentProfile) {
        this.paymentProfile = paymentProfile;
    }

    
    /**
     * Mena eventu
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * Mena eventu, pri zmene meny sa automaticky nastavuje aj zmena chargovacieho 
     * modelu
     * @param currency the currency to set
     */
    public void setCurrency(Currency currency) {        
        switch(currency){
            case CZK:
                setOrderUnitFeeBank(SystemProperties.ORDER_UNIT_FEE_CZK);
                setOrderShareFeeBank(SystemProperties.ORDER_SHARE_FEE_CZK);
                setOrderUnitFeeOnline(SystemProperties.ORDER_UNIT_FEE_CZK);
                setOrderShareFeeOnline(SystemProperties.ORDER_SHARE_FEE_CZK); 
                setOrderUnitFeeOrganizator(SystemProperties.ORDER_UNIT_FEE_CZK);
                setOrderShareFeeOrganizator(SystemProperties.ORDER_SHARE_FEE_CZK);
                setOrderMaxFee(SystemProperties.ORDER_MAX_FEE_CZK);
                break;
            case EUR:
                setOrderUnitFeeBank(SystemProperties.ORDER_UNIT_FEE_EUR);
                setOrderShareFeeBank(SystemProperties.ORDER_SHARE_FEE_EUR);
                setOrderUnitFeeOnline(SystemProperties.ORDER_UNIT_FEE_EUR);
                setOrderShareFeeOnline(SystemProperties.ORDER_SHARE_FEE_EUR);
                setOrderUnitFeeOrganizator(SystemProperties.ORDER_UNIT_FEE_EUR);
                setOrderShareFeeOrganizator(SystemProperties.ORDER_SHARE_FEE_EUR);
                setOrderMaxFee(SystemProperties.ORDER_MAX_FEE_EUR);
                break;
            case USD:
                setOrderUnitFeeBank(SystemProperties.ORDER_UNIT_FEE_USD);
                setOrderShareFeeBank(SystemProperties.ORDER_SHARE_FEE_USD);
                setOrderUnitFeeOnline(SystemProperties.ORDER_UNIT_FEE_USD);
                setOrderShareFeeOnline(SystemProperties.ORDER_SHARE_FEE_USD); 
                setOrderUnitFeeOrganizator(SystemProperties.ORDER_UNIT_FEE_USD);
                setOrderShareFeeOrganizator(SystemProperties.ORDER_SHARE_FEE_USD); 
                setOrderMaxFee(SystemProperties.ORDER_MAX_FEE_USD);
                break;
        }                
        this.currency = currency;
    }

    /**
     * Typ spracovania dane
     * @return the vatType
     */
    public InvoiceProcessType getVatType() {
        return vatType;
    }

    /**
     * Typ spracovania dane
     * @param vatType the vatType to set
     */
    public void setVatType(InvoiceProcessType vatType) {
        this.vatType = vatType;
    }

    
    /**
     * Citac pre produkty na fakturach
     * @return the orderInvoiceCounter
     */
    public int getOrderInvoiceCounter() {
        return orderInvoiceCounter;
    }

    /**
     * Citac pre produkty na fakturach
     * @param orderInvoiceCounter the orderInvoiceCounter to set
     */
    public void setOrderInvoiceCounter(int orderInvoiceCounter) {
        this.orderInvoiceCounter = orderInvoiceCounter;
    }

    /**
     * Citac rady
     * @return the orderInvoiceSeriesCounter
     */
    public int getOrderInvoiceSeriesCounter() {
        return orderInvoiceSeriesCounter;
    }

    /**
     * Citac rady
     * @param orderInvoiceSeriesCounter the orderInvoiceSeriesCounter to set
     */
    public void setOrderInvoiceSeriesCounter(int orderInvoiceSeriesCounter) {
        this.orderInvoiceSeriesCounter = orderInvoiceSeriesCounter;
    }

    /**
     * Citac na prevody na fakturach
     * @return the transferInvoiceCounter
     */
    public int getTransferInvoiceCounter() {
        return transferInvoiceCounter;
    }

    /**
     * Citac na prevody na fakturach
     * @param transferInvoiceCounter the transferInvoiceCounter to set
     */
    public void setTransferInvoiceCounter(int transferInvoiceCounter) {
        this.transferInvoiceCounter = transferInvoiceCounter;
    }

    /**
     * Event ku ktoremu patri tento platobny event profil
     * @return the event
     */
    public Event getEvent() {
        return event;
    }

    /**
     * Event ku ktoremu patri tento platobny event profil
     * @param event the event to set
     */
    public void setEvent(Event event) {
        this.event = event;
    }

    /**
     * Priznak ci je profil stale aktivny
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Priznak ci je profil stale aktivny
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Priznak ze tento profil sa ma objavit na fakture v informaciach o bankovom prevode
     * @return the orderBankTransferInvoicedToThisProfile
     */
    public boolean isOrderBankTransferInvoicedToThisProfile() {
        return orderBankTransferInvoicedToThisProfile;
    }

    /**
     * Priznak ze tento profil sa ma objavit na fakture v informaciach o bankovom prevode
     * @param orderBankTransferInvoicedToThisProfile the orderBankTransferInvoicedToThisProfile to set
     */
    public void setOrderBankTransferInvoicedToThisProfile(boolean orderBankTransferInvoicedToThisProfile) {
        this.orderBankTransferInvoicedToThisProfile = orderBankTransferInvoicedToThisProfile;
    }

    /**
     * Hodnota kolko sa berie za kazdu objednavku ak je potvrdena z bankovej transakcie
     * @return the orderUnitFeeBank
     */
    public BigDecimal getOrderUnitFeeBank() {
        return orderUnitFeeBank;
    }

    /**
     * Hodnota kolko sa berie za kazdu objednavku ak je potvrdena z bankovej transakcie
     * @param orderUnitFeeBank the orderUnitFeeBank to set
     */
    public void setOrderUnitFeeBank(BigDecimal orderUnitFeeBank) {
        this.orderUnitFeeBank = orderUnitFeeBank;
    }

    /**
     * Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena z bankovej transakcie
     * @return the orderShareFeeBank
     */
    public BigDecimal getOrderShareFeeBank() {
        return orderShareFeeBank;
    }

    /**
     * Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena z bankovej transakcie
     * @param orderShareFeeBank the orderShareFeeBank to set
     */
    public void setOrderShareFeeBank(BigDecimal orderShareFeeBank) {
        this.orderShareFeeBank = orderShareFeeBank;
    }

    /**
     * Hodnota kolko sa berie za kazdu objednavku ak je potvrdena z online transakcie
     * @return the orderUnitFeeOnline
     */
    public BigDecimal getOrderUnitFeeOnline() {
        return orderUnitFeeOnline;
    }

    /**
     * Hodnota kolko sa berie za kazdu objednavku ak je potvrdena z online transakcie
     * @param orderUnitFeeOnline the orderUnitFeeOnline to set
     */
    public void setOrderUnitFeeOnline(BigDecimal orderUnitFeeOnline) {
        this.orderUnitFeeOnline = orderUnitFeeOnline;
    }

    /**
     * Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena z online transakcie
     * @return the orderShareFeeOnline
     */
    public BigDecimal getOrderShareFeeOnline() {
        return orderShareFeeOnline;
    }

    /**
     * Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena z online transakcie
     * @param orderShareFeeOnline the orderShareFeeOnline to set
     */
    public void setOrderShareFeeOnline(BigDecimal orderShareFeeOnline) {
        this.orderShareFeeOnline = orderShareFeeOnline;
    }

    /**
     * Poznamka na fakture
     * @return the note
     */
    public String getInvoiceNote() {
        return invoiceNote;
    }

    /**
     * Poznamka na fakture
     * @param note the note to set
     */
    public void setInvoiceNote(String note) {
        this.invoiceNote = note;
    }

    /**
     * UUID profilu je rovnake ako uuid platboneho profilu
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID profilu je rovnake ako uuid platboneho profilu
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    
    
    @Override
    public String toString() {
        return String.format("EventPaymentProfile[uuid=%s activated=%s bank-unit=%s bank-share=%s online-unit=%s online-share=%s]", uuid, active,orderUnitFeeBank.toPlainString(),orderShareFeeBank.toPlainString(),orderUnitFeeOnline.toPlainString(), orderShareFeeOnline.toPlainString());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Event e = (Event) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    /**
     * Hodnota kolko sa berie za kazdu objednavku ak je potvrdena organizatorom
     * @return the orderUnitFeeOrganizator
     */
    public BigDecimal getOrderUnitFeeOrganizator() {
        return orderUnitFeeOrganizator;
    }

    /**
     * Hodnota kolko sa berie za kazdu objednavku ak je potvrdena organizatorom
     * @param orderUnitFeeOrganizator the orderUnitFeeOrganizator to set
     */
    public void setOrderUnitFeeOrganizator(BigDecimal orderUnitFeeOrganizator) {
        this.orderUnitFeeOrganizator = orderUnitFeeOrganizator;
    }

    /**
     * Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena organizatorom
     * @return the orderShareFeeOrganizator
     */
    public BigDecimal getOrderShareFeeOrganizator() {
        return orderShareFeeOrganizator;
    }

    /**
     * Percentualna hodnota ktora udava kolko sa ztrhne z kazdej objednavky  ak je objednavka potvrdena organizatorom
     * @param orderShareFeeOrganizator the orderShareFeeOrganizator to set
     */
    public void setOrderShareFeeOrganizator(BigDecimal orderShareFeeOrganizator) {
        this.orderShareFeeOrganizator = orderShareFeeOrganizator;
    }

    /**
     * Maximalna hodnota ktoru si bereme ako proviziu
     * @return the orderMaxFee
     */
    public BigDecimal getOrderMaxFee() {
        return orderMaxFee;
    }

    /**
     * Maximalna hodnota ktoru si bereme ako proviziu
     * @param orderMaxFee the orderMaxFee to set
     */
    public void setOrderMaxFee(BigDecimal orderMaxFee) {
        this.orderMaxFee = orderMaxFee;
    }
    
}
