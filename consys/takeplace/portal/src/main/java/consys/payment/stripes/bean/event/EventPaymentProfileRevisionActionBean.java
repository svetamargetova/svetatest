package consys.payment.stripes.bean.event;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.service.EventService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EventPaymentProfileRevisionActionBean extends PaymentActionBean {

    @SpringBean
    private EventService eventService;
    // data
    private String eventUuid;
    private String profileUuid;
    private PaymentProfileRevision revision;

    @DontValidate
    @DefaultHandler
    public Resolution preView() throws NoRecordException, RequiredPropertyException {
        EventPaymentProfile epp = eventService.loadEventPaymentProfile(profileUuid);
        revision = epp.getPaymentProfile().getActualRevision();
        return new ForwardResolution("/web/event/eventProfileRevision.jsp");
    }

    public Resolution edit() {
        RedirectResolution resolution = new RedirectResolution(EditEventPaymentProfileRevisionActionBean.class);
        resolution.addParameter("eventUuid", eventUuid);
        resolution.addParameter("profileUuid", profileUuid);
        return resolution;
    }

    public Resolution back() {
        RedirectResolution resolution = new RedirectResolution(EventPaymentProfileListActionBean.class);
        resolution.addParameter("eventUuid", eventUuid);
        return resolution;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public PaymentProfileRevision getRevision() {
        return revision;
    }

    public String getProfileUuid() {
        return profileUuid;
    }

    public void setProfileUuid(String profileUuid) {
        this.profileUuid = profileUuid;
    }
}
