package consys.payment.utils;

import consys.common.utils.BigDecimalUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import consys.payment.bo.Order;
import consys.payment.bo.OrderItem;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.bo.enums.ProductCategory;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p> Wrappovi objekt ktory datovo mapuje polozky na fakture. Nejedna sa o
 * podorbnosti faktury ale vypocty ktore suvisia s polozkami na fakture <p>
 * Objekt je immutable
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public final class InvoiceRecord {

    private final ImmutableList<InvoiceItemRecord> records;
    private final Map<BigDecimal, InvoiceVatRecord> vatRecords;
    // 
    private BigDecimal subTotal = BigDecimal.ZERO;
    private BigDecimal total = BigDecimal.ZERO;
    private BigDecimal vatCharged = BigDecimal.ZERO;

    public InvoiceRecord(Order order) {

        ImmutableList.Builder<InvoiceItemRecord> recordsBuilder = ImmutableList.builder();
        vatRecords = Maps.newHashMap();
        InvoiceProcessType vatType = PaymentUtils.resolveFinalProcessType(order.getSupplier().getVatType(), ProfileUtils.resolveProcessType(order.getCustomer().getCountry()));
        boolean ticketOT = ProductCategory.TICKET.equals(order.getOrderType()) && InvoiceProcessType.VAT_OT.equals(vatType);
        boolean ticketAD = ProductCategory.TICKET.equals(order.getOrderType()) && InvoiceProcessType.VAT_AD.equals(vatType);

        // nainicializujeme
        for (OrderItem item : order.getItems()) {
            // vytvorime schranku
            InvoiceItemRecord record = new InvoiceItemRecord(item);
            // Ziskame VatTabulku
            InvoiceVatRecord vatRecord = getVatTableRecord(record.getVatRate());
            // Pridame zaklad dane - cistu cenu
            vatRecord.addVatItem(record.getNetPrice(), record.getVatAmount());
            // pridame do zaznamov
            recordsBuilder.add(record);
            // upravime
            total = total.add(record.getPrice(), BigDecimalUtils.DEFAULT);
            subTotal = subTotal.add(ticketOT || ticketAD ? record.getNetPriceWithVat() : record.getNetPrice(), BigDecimalUtils.DEFAULT);
            vatCharged = vatCharged.add(record.getVatAmount(), BigDecimalUtils.DEFAULT);
        }
        records = recordsBuilder.build();
    }

    /**
     * Celkova cena objednavky bez dane
     */
    public BigDecimal getSubTotal() {
        return subTotal;
    }

    /**
     * Celkova cena objednavky s danou
     */
    public BigDecimal getTotal() {
        return total;
    }

    public BigDecimal getVatCharged() {
        return vatCharged;
    }

    /**
     * Vrati zoznam zaznamov na fakture s predpocitanymi valstnostami. Zoznam je
     * immutable
     *
     * @return the records
     */
    public ImmutableList<InvoiceItemRecord> getRecords() {
        return records;
    }

    /**
     * @return the vatRecords
     */
    public InvoiceVatRecord getVatTableRecord(BigDecimal vatRate) {
        InvoiceVatRecord rec = vatRecords.get(vatRate);
        if (rec == null) {
            rec = new InvoiceVatRecord(vatRate);
            vatRecords.put(vatRate, rec);
        }
        return rec;
    }

    /** vraci serazeny seznam vsech vat rate, od nejmensi */
    public List<BigDecimal> getVatRates() {
        List<BigDecimal> result = Lists.newArrayListWithCapacity(vatRecords.size());
        for (BigDecimal bd : vatRecords.keySet()) {
            result.add(bd);
        }
        Collections.sort(result);
        return Collections.unmodifiableList(result);
    }

    public class InvoiceVatRecord {

        /** Vyska dane */
        private final BigDecimal vatRate;
        /** Zaklad dane */
        private BigDecimal vatBase = BigDecimal.ZERO;
        /** Dan zo zakladu */
        private BigDecimal vatAmount = BigDecimal.ZERO;

        public InvoiceVatRecord(BigDecimal vatRate) {
            this.vatRate = vatRate;
        }

        /**
         * Dan
         *
         * @return the vatRate
         */
        public BigDecimal getVatRate() {
            return vatRate;
        }

        /**
         * Zaklad dane
         *
         * @return the vatBase
         */
        public BigDecimal getVatBase() {
            return vatBase;
        }

        /**
         * Zaklad dane
         *
         * @param vatBase the vatBase to set
         */
        void addVatItem(BigDecimal vatBase, BigDecimal vatAmount) {
            this.vatBase = this.vatBase.add(vatBase, BigDecimalUtils.DEFAULT);
            this.vatAmount = this.vatAmount.add(vatAmount, BigDecimalUtils.DEFAULT);
        }

        /**
         * Kolko dan zo zakladu - vypocita sa na zaklade </br>
         *
         * @return the vatAmount
         */
        public BigDecimal getVatAmount() {
            return vatAmount;
        }
    }
}
