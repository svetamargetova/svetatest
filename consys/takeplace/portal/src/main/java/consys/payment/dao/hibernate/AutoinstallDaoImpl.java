package consys.payment.dao.hibernate;

import consys.payment.dao.AutoinstallDao;
import java.sql.Connection;
import java.sql.SQLException;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class AutoinstallDaoImpl extends HibernateDaoSupport implements AutoinstallDao{
               
    @Override
    public boolean installScript(final String script) {        
        getSession().doWork(new Work() {

            @Override
            public void execute(Connection connection) throws SQLException {
                connection.createStatement().execute(script);
            }
        });
        return true;
    }
    




}
