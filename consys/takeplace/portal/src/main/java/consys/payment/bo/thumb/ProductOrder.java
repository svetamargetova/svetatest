package consys.payment.bo.thumb;

import java.math.BigDecimal;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProductOrder {

    private String productUuid;
    private int quatity;
    private int discount;
    private BigDecimal price;

    public ProductOrder(String productUuid, int quatity, int discount) {
        this.productUuid = productUuid;
        this.quatity = quatity;
        this.discount = discount;
    }

    public ProductOrder(String productUuid, int quatity, int discount, BigDecimal price) {
        this.productUuid = productUuid;
        this.quatity = quatity;
        this.discount = discount;
        this.price = price;
    }

    public String getProductUuid() {
        return productUuid;
    }

    public int getDiscount() {
        return discount;
    }

    public int getQuatity() {
        return quatity;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
