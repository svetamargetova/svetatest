package consys.payment.stripes.bean.transaction;

import consys.payment.bo.Transaction;
import consys.payment.service.TransactionService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class TransactionListItemActionBean extends PaymentActionBean {

    @SpringBean
    private TransactionService transactionService;
    // data
    private Transaction transaction;
    private String uuid;
    private String note;

    @DontValidate
    @DefaultHandler
    public Resolution preView() throws NoRecordException {
        transaction = transactionService.loadTransactionByUuid(uuid);
        return new ForwardResolution("/web/transaction/transactionDetail.jsp");
    }

    public Resolution update() {
        // TODO: aktualizovat note a source
        return back();
    }

    public Resolution back() {
        return new ForwardResolution(TransactionListActionBean.class);
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
