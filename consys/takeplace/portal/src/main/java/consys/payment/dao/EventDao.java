/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.payment.dao;


import consys.payment.bo.Event;
import consys.payment.service.exception.NoRecordException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public interface EventDao extends PersistedObjectDao<Event>{

    
        
    public Event loadEventWithLicenseInvoiceByUuid(String uuid) throws NoRecordException;

    public Event loadEventByUuid(String uuid) throws NoRecordException;

    public Event loadEventWithProfilesByUuid(String uuid) throws NoRecordException;

    public Event loadEventByProfile(Long id) throws NoRecordException;

    public Event loadEventByLicenseOrderNumber(String orderNumber) throws NoRecordException;
    
    public int loadAllEventsCount(boolean active, Date fromDate, Date toDate);
             
    public List<Event> listEvents(boolean active, Date fromDate, Date toDate, int startItem, int itemsPerPage);


}
