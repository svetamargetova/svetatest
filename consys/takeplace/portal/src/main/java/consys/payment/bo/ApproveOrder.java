package consys.payment.bo;

import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.bo.enums.OrderState;
import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ApproveOrder implements BusinessObject{
    private static final long serialVersionUID = 5141064014926958646L;

    private Long id;
    private Order order;
    private Transaction transaction;
    private OrderState toOrderState;
    private boolean generateInvoice;
    private Date createDate;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * @return the transaction
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * @param transaction the transaction to set
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }    

    /**
     * @return the createDate
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate the createDate to set
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the toOrderState
     */
    public OrderState getToOrderState() {
        return toOrderState;
    }

    /**
     * @param toOrderState the toOrderState to set
     */
    public void setToOrderState(OrderState toOrderState) {
        this.toOrderState = toOrderState;
    }

    /**
     * @return the generateInvoice
     */
    public boolean isGenerateInvoice() {
        return generateInvoice;
    }

    /**
     * @param generateInvoice the generateInvoice to set
     */
    public void setGenerateInvoice(boolean generateInvoice) {
        this.generateInvoice = generateInvoice;
    }





}
