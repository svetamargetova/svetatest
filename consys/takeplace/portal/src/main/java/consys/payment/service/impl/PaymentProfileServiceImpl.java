package consys.payment.service.impl;

import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.payment.bo.PaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.dao.PaymentProfileDao;
import consys.payment.dao.PaymentProfileRevisionDao;
import consys.payment.service.PaymentProfileService;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.utils.ProfileUtils;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PaymentProfileServiceImpl extends Service implements PaymentProfileService {

    // -- Dao
    private PaymentProfileDao paymentProfileDao;
    private PaymentProfileRevisionDao profileRevisionDao;


    /*
     * ------------------------------------------------------------------------     
     * --- C R E A T E ---     
     * ------------------------------------------------------------------------
     */
    @Override
    public void createProfileRevisionWithoutValidation(PaymentProfileRevision rev)
            throws RequiredPropertyException {

        if (rev.getParent() == null) {
            throw new RequiredPropertyException("No Parent set for profile revision" + rev);
        }
        rev.setUuid(UuidProvider.getUuid());
        rev.setRevisionDate(DateProvider.getCurrentDate());
        if (logger.isDebugEnabled()) {
            logger.debug("Creating profile revision " + rev);
        }
        profileRevisionDao.create(rev);

    }

    @Override
    public void createProfileRevision(PaymentProfileRevision rev, boolean isEventProfile)
            throws RequiredPropertyException, IbanFormatException {

        if (isEventProfile) {
            ProfileUtils.validateProfile(rev, isEventProfile);
        }
        createProfileRevisionWithoutValidation(rev);
    }

    @Override
    public void createProfile(PaymentProfile profile) throws RequiredPropertyException {
        profile.setUuid(UuidProvider.getUuid());
        paymentProfileDao.create(profile);
    }


    /*
     * ------------------------------------------------------------------------    
     * --- U P D A T E ---     
     * ------------------------------------------------------------------------
     */
    @Override
    public void updateProfile(PaymentProfile profile) throws RequiredPropertyException {
        if (profile == null || profile.getActualRevision() == null || CollectionUtils.isEmpty(profile.getRevisions())) {
            throw new RequiredPropertyException("Missing at least one revision");
        }
        paymentProfileDao.update(profile);
    }

    @Override
    public void updateProfileRevision(PaymentProfileRevision revision, boolean event)
            throws RequiredPropertyException, IbanFormatException {
        if (revision == null) {
            throw new RequiredPropertyException("Missing at revision");
        }
        if (event) {
            ProfileUtils.validateProfile(revision, event);
        }
        revision.setRevisionDate(DateProvider.getCurrentDate());
        profileRevisionDao.update(revision);
    }

    /*
     * ------------------------------------------------------------------------     
     * --- L O A D ---     
     * ------------------------------------------------------------------------
     */
    @Override
    public Integer loadProfileRevisionUsedCount(String revisionUuid)
            throws RequiredPropertyException {
        if (StringUtils.isBlank(revisionUuid)) {
            throw new RequiredPropertyException("Missing profile  UUID");
        }
        return profileRevisionDao.loadRevisionUsedCount(revisionUuid);
    }

    @Override
    public PaymentProfile loadPaymentProfile(String uuid) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("Missing profile  UUID");
        }
        return paymentProfileDao.loadProfile(uuid);
    }

    @Override
    public PaymentProfile loadEventProfile(String eventUuid, String profileUuid)
            throws NoRecordException, RequiredPropertyException {

        if (StringUtils.isBlank(eventUuid) || StringUtils.isBlank(profileUuid)) {
            throw new RequiredPropertyException("Missing profile or event UUID");
        }
        return paymentProfileDao.loadEventProfile(eventUuid, profileUuid);
    }

    @Override
    public PaymentProfile loadUserProfile(String userUuid, String profileUuid)
            throws NoRecordException, RequiredPropertyException {

        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(profileUuid)) {
            throw new RequiredPropertyException("Missing profile or user UUID");
        }
        return paymentProfileDao.loadUserProfile(userUuid, profileUuid);
    }

    @Override
    public PaymentProfile loadEventProfileWithActualRevision(String eventUuid, String profileUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid) || StringUtils.isBlank(profileUuid)) {
            throw new RequiredPropertyException("Missing profile or user UUID");
        }
        return paymentProfileDao.loadEventProfileWithActualRevision(eventUuid, profileUuid);
    }

    @Override
    public PaymentProfile loadUserProfileWithActualRevision(String userUuid, String profileUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(profileUuid)) {
            throw new RequiredPropertyException("Missing profile or user UUID");
        }
        return paymentProfileDao.loadUserProfileWithActualRevision(userUuid, profileUuid);

    }

    @Override
    public boolean isEventPaymentProfileUsedInActiveOrder(String paymentProfileUuid) throws RequiredPropertyException {
        if (StringUtils.isBlank(paymentProfileUuid)) {
            throw new RequiredPropertyException("Missing revisionUuid param");
        }
        return paymentProfileDao.isEventPaymentProfileUsedInActiveOrder(paymentProfileUuid);
    }

    @Override
    public boolean isUserPaymentProfileUsedInActiveOrder(String paymentProfileUuid) throws RequiredPropertyException {
        if (StringUtils.isBlank(paymentProfileUuid)) {
            throw new RequiredPropertyException("Missing revisionUuid param");
        }
        return paymentProfileDao.isUserPaymentProfileUsedInActiveOrder(paymentProfileUuid);
    }

    /*
     * ------------------------------------------------------------------------     
     * --- L I S T ---     
     * ------------------------------------------------------------------------
     */
    @Override
    public List<PaymentProfile> listEventProfile(String eventUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Event uuid is null or missing");
        }
        return paymentProfileDao.listEventProfiles(eventUuid);
    }

    @Override
    public List<PaymentProfile> listUserProfile(String userUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyException("User uuid is null or missing");
        }
        return paymentProfileDao.listUserProfiles(userUuid);
    }

    /*
     * ------------------------------------------------------------------------     
     * --- D E L E T E ---     
     * ------------------------------------------------------------------------
     */
    @Override
    public void deleteEventProfile(String eventUuid)
            throws NoRecordException, RequiredPropertyException {
    }

    @Override
    public void deleteUserProfile(String userUuid)
            throws NoRecordException, RequiredPropertyException {
    }


    /*
     * ------------------------------------------------------------------------
     */
    /*
     * --- U T I L S ---
     */
    /*
     * ------------------------------------------------------------------------
     */
    /*
     * ========================================================================
     */
    /*
     * GETTER SETTER
     */
    /*
     * ========================================================================
     */
    /**
     * @return the paymentProfileDao
     */
    public PaymentProfileDao getPaymentProfileDao() {
        return paymentProfileDao;
    }

    /**
     * @param paymentProfileDao the paymentProfileDao to set
     */
    public void setPaymentProfileDao(PaymentProfileDao paymentProfileDao) {
        this.paymentProfileDao = paymentProfileDao;
    }

    /**
     * @return the profileRevisionDao
     */
    public PaymentProfileRevisionDao getProfileRevisionDao() {
        return profileRevisionDao;
    }

    /**
     * @param profileRevisionDao the profileRevisionDao to set
     */
    public void setProfileRevisionDao(PaymentProfileRevisionDao profileRevisionDao) {
        this.profileRevisionDao = profileRevisionDao;
    }
}
