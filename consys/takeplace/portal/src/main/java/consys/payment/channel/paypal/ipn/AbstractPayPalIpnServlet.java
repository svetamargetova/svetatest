package consys.payment.channel.paypal.ipn;

import consys.payment.channel.paypal.ipn.enums.PPIpnState;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * Servlet na spracovanie PayPal IPN notfifikacii.
 *
 * Defaultne loguje kazdu spravu ktora pride do loggeru.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractPayPalIpnServlet extends HttpServlet{

    public static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
    public static final String CONTENT_TYPE = "Content-Type";
    private static final String CMD_NOTIFY_VALIDATE = "cmd=_notify-validate";
    private static final String AMPER = "&";
    private static final String EQ = "=";
    private static final String ENCODING = "UTF-8";
    // URL na paypal
    private static final String PPURL_SAND_BOX = "https://www.sandbox.paypal.com/cgi-bin/webscr";
    private static final String PPURL_LIVE = "https://www.paypal.com/cgi-bin/webscr";
    // Init param - Paypal prostreti
    private static final String ENVIROMENT = "PayPalEnvironment";
    private static final String ENVIROMENT_LIVE = "live";
    

    
    private String TARGET_PP_URL = null;
    private static final long serialVersionUID = -757454337424357051L;
    protected Logger logger;

    /**
     * Defaultny konstruktor s logovanim podla triedy
     */
    public AbstractPayPalIpnServlet() {
        logger = LoggerFactory.getLogger(this.getClass());  
        
    }

    /**
     * Logovanie do specialneho suboru
     */
    public AbstractPayPalIpnServlet(String loggerName) {
        logger = LoggerFactory.getLogger(loggerName);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String env = config.getInitParameter(ENVIROMENT);
        if (StringUtils.isNotBlank(env) && env.equalsIgnoreCase(ENVIROMENT_LIVE)) {
            TARGET_PP_URL = PPURL_LIVE;
            logger.info("Intializing PayPal IPN servlet to LIVE");
        } else {
            TARGET_PP_URL = PPURL_SAND_BOX;
            logger.info("Intializing PayPal IPN servlet to SAND_BOX");
        }
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);        
    }

    private String getPaypalUrl() {
        return TARGET_PP_URL;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            request.setCharacterEncoding(ENCODING);
            response.setCharacterEncoding(ENCODING);
            Enumeration en = request.getParameterNames();
            
            StringBuilder validation = new StringBuilder(CMD_NOTIFY_VALIDATE);
            while (en.hasMoreElements()) {
                String paramName = (String) en.nextElement();
                String paramValue = request.getParameter(paramName);
                validation.append(AMPER).append(paramName).append(EQ).append(URLEncoder.encode(paramValue, ENCODING));
            }

            // post back to PayPal system to validate
            // NOTE: change http: to https: in the following URL to verify using SSL (for increased security).
            // using HTTPS requires either Java 1.4 or greater, or Java Secure Socket Extension (JSSE)
            // and configured for older versions.

            URL u = new URL(getPaypalUrl());
            URLConnection uc = u.openConnection();
            uc.setDoOutput(true);
            uc.setRequestProperty(CONTENT_TYPE, APPLICATION_X_WWW_FORM_URLENCODED);
            PrintWriter pw = new PrintWriter(uc.getOutputStream());
            pw.println(validation.toString());
            pw.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(uc.getInputStream()));
            String data = in.readLine();
            in.close();

            // Spracuejem odpoved
            PaypalTransaction ipn = new PaypalTransaction();

            Enumeration params = request.getParameterNames();

            String name = null;
            String value = null;
            while (params.hasMoreElements()) {
                name = (String) params.nextElement();
                value = request.getParameter(name);
                ipn.put(name, value);
            }
            
            if (data.equals(PPIpnState.VERIFIED.name())) {
                
                String problems = ipn.pendingReasonPayment();
                if(StringUtils.isNotBlank(problems)){
                    processPending(problems);
                    logger.info("[VERIFIED - PENDING] {}",ipn);
                }
                problems = ipn.chargeBackPayment();
                if(StringUtils.isNotBlank(problems)){
                    processChargeBack(problems);
                    logger.info("[VERIFIED - CHARGING BACK] {}", ipn);
                }                
                logger.info("[VERIFIED] {}", ipn);
                processIpnMessage(ipn);
            } else if (data.equals(PPIpnState.INVALID.name())) {
                logger.info("[INVALID] {}",ipn);
            } else {
                logger.info("[ERROR] {}", ipn);
            }            
        } catch (Exception ex) {
            logger.error("[Error] in IPN processing", ex);
        }
    }

    
    protected <T> T getBeanOfType(Class<T> clazz) {
        Map<String, T> beans = WebApplicationContextUtils.getWebApplicationContext(getServletContext()).getBeansOfType(clazz);
        if (beans.isEmpty()) {
            throw new NullPointerException("No beans in context for type " + clazz);
        }
        return beans.values().iterator().next();
    }



    protected abstract void processIpnMessage(PaypalTransaction ipn);

    /**
     * This variable is set only if payment_status = Pending.
     *
     * address: The payment is pending because your customer did
     * not include a confirmed shipping address and your Payment
     * Receiving Preferences is set yo allow you to manually accept or
     * deny each of these payments. To change your preference, go to
     * the Preferences section of your Profile.
     *
     * authorization: You set <PaymentAction>
     * Authorization</PaymentAction> on
     * SetExpressCheckoutRequest and have not yet captured
     * funds.
     *
     * echeck: The payment is pending because it was made by an
     * eCheck that has not yet cleared.
     *
     * intl: The payment is pending because you hold a non-U.S.
     * account and do not have a withdrawal mechanism. You must
     * manually accept or deny this payment from your Account
     * Overview.
     *
     * multi-currency: You do not have a balance in the currency
     * sent, and you do not have your Payment Receiving Preferences
     * set to automatically convert and accept this payment. You must
     * manually accept or deny this payment.
     *
     * unilateral: The payment is pending because it was made to
     * an email address that is not yet registered or confirmed.
     *
     * upgrade: The payment is pending because it was made via
     * credit card and you must upgrade your account to Business or
     * Premier status in order to receive the funds. upgrade can also
     * mean that you have reached the monthly limit for transactions on
     * your account.
     *
     * verify: The payment is pending because you are not yet
     * verified. You must verify your account before you can accept this
     * payment.
     *
     * other: The payment is pending for a reason other than those
     * listed above. For more information, contact PayPal Customer
     * Service.
     */
    protected abstract void processPending(String pendingReason);

    /**
     * This variable is set if payment_status =Reversed,
     * Refunded, or Cancelled_Reversal
     *
     * chargeback: A reversal has occurred on this transaction due to
     * a chargeback by your customer.
     *
     * guarantee: A reversal has occurred on this transaction due to
     * your customer triggering a money-back guarantee.
     *
     * buyer-complaint: A reversal has occurred on this transaction
     * due to a complaint about the transaction from your customer.
     *
     * refund: A reversal has occurred on this transaction because you
     * have given the customer a refund.
     *
     * other: A reversal has occurred on this transaction due to a
     * reason not listed above.
     */
    protected abstract void processChargeBack(String pendingReason);
}
