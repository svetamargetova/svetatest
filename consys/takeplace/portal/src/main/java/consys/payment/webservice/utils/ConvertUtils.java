package consys.payment.webservice.utils;

import consys.common.utils.collection.Lists;
import consys.common.utils.enums.CountryEnum;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.PaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.common.utils.enums.Currency;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.bo.enums.OrderState;
import consys.payment.bo.enums.ProductCategory;
import consys.payment.ws.bo.EventProfile;
import consys.payment.ws.bo.InvoiceVat;
import consys.payment.ws.bo.ObjectFactory;
import consys.payment.ws.bo.Product;
import consys.payment.ws.bo.Profile;
import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConvertUtils {

    private static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public static Integer toInteger(BigInteger bigInt) {
        return bigInt == null ? null : new Integer(bigInt.intValue());
    }

    public static PaymentProfileRevision toCoreProfileRevision(Profile profile) {
        PaymentProfileRevision rev = new PaymentProfileRevision();
        rev.setAccountBankSwift(profile.getSWIFT());
        rev.setAccountIban(profile.getIBAN());
        rev.setCity(profile.getCity());
        rev.setCountry(CountryEnum.fromId(profile.getCountry()));
        rev.setName(profile.getName());
        rev.setRegNo(profile.getRegistrationNo());
        rev.setStreet(profile.getStreet());
        rev.setVatNo(profile.getVatNo());
        rev.setZip(profile.getZip());
        rev.setUuid(profile.getUuid());
        rev.setNoVatPayer(profile.isNoVatPayer());
        return rev;
    }

    public static Profile toWsProfile(PaymentProfile p) {
        Profile pr = OBJECT_FACTORY.createProfile();
        pr.setCity(p.getActualRevision().getCity());
        pr.setCountry(p.getActualRevision().getCountry() == null ? 0 : p.getActualRevision().getCountry().getId());
        pr.setIBAN(p.getActualRevision().getAccountIban());
        pr.setName(p.getActualRevision().getName());
        pr.setRegistrationNo(p.getActualRevision().getRegNo());
        pr.setSWIFT(p.getActualRevision().getAccountBankSwift());
        pr.setStreet(p.getActualRevision().getStreet());
        pr.setUuid(p.getUuid());
        pr.setVatNo(p.getActualRevision().getVatNo());
        pr.setZip(p.getActualRevision().getZip());
        pr.setNoVatPayer(p.getActualRevision().getNoVatPayer());
        return pr;
    }

    public static EventProfile toWsEventProfile(EventPaymentProfile p) {
        EventProfile pr = OBJECT_FACTORY.createEventProfile();
        pr.setUuid(p.getUuid());
        pr.setActive(p.isActive());
        pr.setBankInvoicedToProfile(p.isOrderBankTransferInvoicedToThisProfile());
        pr.setCurrency(toWsCurrency(p.getCurrency()));
        pr.setInvoiceNote(p.getInvoiceNote());
        pr.setInvoiceVat(toWsVatType(p.getVatType()));
        pr.setOrderShareFeeBank(p.getOrderShareFeeBank());
        pr.setOrderShareFeeOnline(p.getOrderShareFeeOnline());
        pr.setOrderShareFeeOrganizator(p.getOrderShareFeeOrganizator());
        pr.setOrderUnitFeeBank(p.getOrderUnitFeeBank());
        pr.setOrderUnitFeeOnline(p.getOrderUnitFeeOnline());
        pr.setOrderUnitFeeOrganizator(p.getOrderUnitFeeOrganizator());
        pr.setProfile(toWsProfile(p.getPaymentProfile()));
        return pr;
    }

    public static consys.payment.bo.Product toCoreProduct(Product p) {
        consys.payment.bo.Product pp = new consys.payment.bo.Product();
        switch (p.getCategory()) {
            case LICENSE:
                pp.setCategory(ProductCategory.LICENSE);
                break;
            case TICKET:
                pp.setCategory(ProductCategory.TICKET);
                break;
        }
        pp.setName(p.getName());
        pp.setPrice(p.getUnitPrice());
        pp.setUuid(p.getUuid());
        pp.setVatRate(p.getVat());
        return pp;
    }

    public static consys.payment.ws.bo.Currency toWsCurrency(Currency currencyEnum) {
        switch (currencyEnum) {
            case CZK:
                return consys.payment.ws.bo.Currency.CZK;
            case EUR:
                return consys.payment.ws.bo.Currency.EUR;
            case USD:
                return consys.payment.ws.bo.Currency.USD;
            default:
                throw new NullPointerException("There is missing Currency in invoice?");
        }
    }

    public static Currency toCoreCurrency(consys.payment.ws.bo.Currency currency) {
        switch (currency) {
            case CZK:
                return Currency.CZK;
            case EUR:
                return Currency.EUR;
            case USD:
                return Currency.USD;
            default:
                throw new NullPointerException("There is missing Currency in invoice?");
        }
    }

    public static InvoiceVat toWsVatType(InvoiceProcessType vat) {
        switch (vat) {
            case VAT_CZ:
                return InvoiceVat.VAT_CZ;
            case VAT_EU:
                return InvoiceVat.VAT_EU;
            case VAT_OT:
                return InvoiceVat.VAT_OT;
            case VAT_AD:
                return InvoiceVat.VAT_AD;
            default:
                throw new NullPointerException("There is missing VAT in invoice?");
        }
    }

    public static InvoiceProcessType toCoreVatType(InvoiceVat vat) {
        switch (vat) {
            case VAT_CZ:
                return InvoiceProcessType.VAT_CZ;
            case VAT_EU:
                return InvoiceProcessType.VAT_EU;
            case VAT_OT:
                return InvoiceProcessType.VAT_OT;
            case VAT_AD:
                return InvoiceProcessType.VAT_AD;
            default:
                throw new NullPointerException("There is missing VAT in invoice?");
        }
    }

    public static List<consys.payment.bo.Product> toCoreProducts(List<Product> pps) {
        List<consys.payment.bo.Product> out = Lists.newArrayList();
        for (Product p : pps) {
            out.add(toCoreProduct(p));
        }
        return out;
    }

    public static Product toWsProduct(consys.payment.bo.Product p) {
        Product pp = new Product();
//        switch (p.getCategory()) {
//            case LICENSE:
//                pp.setCategory(ProductCategory.LICENSE);
//                break;
//            case TICKET:
//                pp.setCategory(ProductCategory.TICKET);
//                break;
//        }
        pp.setName(p.getName());
        pp.setUnitPrice(p.getPrice());
        pp.setUuid(p.getUuid());
        return pp;
    }

    public static XMLGregorianCalendar toXmlCalendar(Date date) {
        if (date == null) {
            return null;
        }
        try {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(date);
            XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            return date2;
        } catch (DatatypeConfigurationException ex) {
            throw new IllegalArgumentException("Can't convert Date => XMLGregorianCalendar", ex);
        }
    }

    public static Date toDate(XMLGregorianCalendar cal) {
        if (cal == null) {
            return null;
        } else {
            return cal.toGregorianCalendar().getTime();
        }
    }

    public static consys.payment.ws.bo.OrderState toWsOrderState(OrderState state) {
        switch (state) {
            case ACTIVE:
                return consys.payment.ws.bo.OrderState.ACTIVE;
            case CANCELED_ORGANIZATOR:
                return consys.payment.ws.bo.OrderState.CANCELED_ORGANIZATOR;
            case CANCELED_OWNER:
                return consys.payment.ws.bo.OrderState.CANCELED_OWNER;
            case CANCELED_SYSTEM:
                return consys.payment.ws.bo.OrderState.CANCELED_SYSTEM;
            case CONFIRMED_ORGANIZATOR:
                return consys.payment.ws.bo.OrderState.CONFIRMED_ORGANIZATOR;
            case CONFIRMED_SYSTEM:
                return consys.payment.ws.bo.OrderState.CONFIRMED_SYSTEM;
            case CONFIRMED_ZERO:
                return consys.payment.ws.bo.OrderState.CONFIRMED_ZERO;
            case WAITING_TO_APPROVE:
                return consys.payment.ws.bo.OrderState.WAITING_TO_APPROVE;
        }
        return null;
    }
}
