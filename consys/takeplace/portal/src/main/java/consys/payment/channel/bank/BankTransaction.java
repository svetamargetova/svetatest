package consys.payment.channel.bank;

import consys.common.utils.enums.Currency;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public class BankTransaction {

    private Date date;
    private Currency currency;
    private BigDecimal amount;
    private List<String> variableSymbol;
    private String referenceId;
    private String note;

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return the currency
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the variableSymbol
     */
    public List<String> getVariableSymbol() {
        return variableSymbol;
    }

    /**
     * @param variableSymbol the variableSymbol to set
     */
    public void setVariableSymbol(List<String> variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    /**
     * @return the referenceId
     */
    public String getReferenceId() {
        return referenceId;
    }

    /**
     * @param referenceId the referenceId to set
     */
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
}
