package consys.payment.bo.enums;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum OrderFilterEnum {
    
    ONLY_CONFIRMED,ONLY_ACTIVE,ALL, WAITING_TO_APPROVE;



}
