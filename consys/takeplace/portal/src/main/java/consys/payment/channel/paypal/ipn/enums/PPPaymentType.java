package consys.payment.channel.paypal.ipn.enums;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum PPPaymentType {

    eCheck,instant
}
