package consys.payment.service.impl;

import consys.payment.bo.SystemProperty;
import consys.payment.dao.SystemPropertyDao;
import consys.payment.service.SystemPropertyService;
import consys.payment.service.exception.NoRecordException;

/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SystemPropertyServiceImpl extends Service implements SystemPropertyService {

    // -- Dao
    private SystemPropertyDao systemPropertyDao;
    

    @Override
    public SystemProperty loadByKey(String key) throws NoRecordException {
        return getSystemPropertyDao().loadByKey(key);
    }

    @Override
    public void createOrUpdateSystemPropety(String key, String value) {
        try {
            SystemProperty sp = loadByKey(key);
            sp.setValue(value);
            getSystemPropertyDao().update(sp);
        } catch (NoRecordException ex) {
            SystemProperty sp = new SystemProperty();
            sp.setKey(key);
            sp.setValue(value);
            getSystemPropertyDao().create(sp);
        }
    }

    /**
     * @return the systemPropertyDao
     */
    public SystemPropertyDao getSystemPropertyDao() {
        return systemPropertyDao;
    }

    /**
     * @param systemPropertyDao the systemPropertyDao to set
     */
    public void setSystemPropertyDao(SystemPropertyDao systemPropertyDao) {
        this.systemPropertyDao = systemPropertyDao;
    }

    
    @Override
    public void injectCached(String key, Object o) {
      
    }

    @Override
    public <T> T getInjected(String key) {
       return null;

    }
}
