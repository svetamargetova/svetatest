package consys.payment.service.impl;

import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.LoadEventOverseerRequest;
import consys.admin.event.ws.LoadEventOverseerResponse;
import consys.admin.event.ws.LoadUsersToEventRequest;
import consys.admin.event.ws.LoadUsersToEventResponse;
import consys.admin.event.ws.UpdateUserRelationToEventRequest;
import consys.admin.event.ws.UpdateUserRelationToEventResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class AdminEventWebServiceImpl extends WebServiceGatewaySupport implements AdminEventWebService{

    // neimplementujeme
    @Override
    public LoadUsersToEventResponse loadUserToEvent(LoadUsersToEventRequest request) {
        throw new UnsupportedOperationException("Not supported yet - No meaning in Payment Portal");
    }

    // neimplementujeme
    @Override
    public UpdateUserRelationToEventResponse updateUserRelationToEvent(UpdateUserRelationToEventRequest request) {
        throw new UnsupportedOperationException("Not supported yet - No meaning in Payment Portal");
    }

    @Override
    public LoadEventOverseerResponse loadEventOverseer(LoadEventOverseerRequest request) {
        return (LoadEventOverseerResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }    
}
