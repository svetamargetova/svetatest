package consys.payment.stripes.bean.transaction;

import consys.payment.bo.Transaction;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.service.TransactionService;
import consys.payment.stripes.bean.ListActionBean;
import java.util.Date;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class TransactionListActionBean extends ListActionBean<Transaction> {

    @SpringBean
    private TransactionService transactionService;
    // data
    private ConfirmationChannel channel;
    private Date from;
    private Date to;

    public TransactionListActionBean() {
        super("/web/transaction/transactions.jsp");
    }

    @Override
    protected void reloadList(int firstItemIndex, int itemsPerPage) throws Exception {
        if (channel == null) {
            channel = ConfirmationChannel.ONLINE_PAY_PAL;
        }

        getItems().setList(transactionService.listTransactions(channel, from, to, firstItemIndex, itemsPerPage));
        getItems().setTotal(transactionService.listTransactionsCount(channel, from, to));
    }

    public ConfirmationChannel getChannel() {
        return channel;
    }

    public void setChannel(ConfirmationChannel channel) {
        this.channel = channel;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
