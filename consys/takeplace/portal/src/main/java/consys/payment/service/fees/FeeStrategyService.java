/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service.fees;

import com.google.common.collect.ImmutableList;
import consys.payment.bo.Order;
import consys.payment.bo.OrderItem;
import consys.payment.service.impl.Service;
import java.util.List;

/**
 *
 * @author palo
 */
public class FeeStrategyService extends Service {

    List<OrderItemFeeStrategy> orderItemStrategies;
    List<OrderFeeStrategy> orderStrategies;

    public FeeStrategyService() {
        orderItemStrategies = ImmutableList.<OrderItemFeeStrategy>builder().add(new TicketOrderItemFeeStrategy()).build();
        orderStrategies = ImmutableList.<OrderFeeStrategy>builder().add(new TicketOrderFeeStrategy()).build();
    }

    public void processFees(Order order) {
        for (OrderItem item : order.getItems()) {
            for (OrderItemFeeStrategy feeStrategy : orderItemStrategies) {
                if (feeStrategy.canHandleOrder(item.getProduct().getCategory())) {
                    feeStrategy.computeFee(order.getSupplier(), item, order);
                }
            }
        }

        for (OrderFeeStrategy orderFeeStrategy : orderStrategies) {
            if (orderFeeStrategy.canHandleOrder(order.getItems().get(0).getProduct().getCategory())) {
                orderFeeStrategy.computeFee(order.getSupplier(), order);
            }
        }

    }
}
