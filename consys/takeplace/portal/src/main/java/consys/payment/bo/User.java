/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.bo;

import java.util.List;

/**
 *
 * @author palo
 */
public class User implements BusinessObject {

    private static final long serialVersionUID = 7490886323582777135L;
    private Long id;
    /** UUID uzivatela v administracii */
    private String uuid;
    /** Profily pouzivatele */
    private List<PaymentProfile> profiles;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * UUID uzivatela v administracii
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID uzivatela v administracii
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Profily pouzivatele
     * @return the profiles
     */
    public List<PaymentProfile> getProfiles() {
        return profiles;
    }

    /**
     * Profily pouzivatele
     * @param profiles the profiles to set
     */
    public void setProfiles(List<PaymentProfile> profiles) {
        this.profiles = profiles;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        User e = (User) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("User[uuid=%s]", uuid);
    }
}
