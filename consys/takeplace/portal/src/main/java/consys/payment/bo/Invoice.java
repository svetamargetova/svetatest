package consys.payment.bo;

import consys.payment.bo.enums.ConfirmationChannel;
import java.util.Date;

/**
 * Entita ktora reprezentuje fakturu, teda vystaveny platobny doklad na nejaku 
 * objednavku.
 * 
 * @author palo
 */
public class Invoice implements BusinessObject {

    private static final long serialVersionUID = 3173152620783782450L;
    // data
    private Long id;
    /** Objednavka ku ktorej patri faktura */
    private Order order;
    /** Cislo faktury */
    private String invoiceNumber;
    /** Datum vystavenia faktury */
    private Date issueDate;
    /** Datum zdanitelneho plneni */
    private Date taxableFulfillmentDate;
    /** Transackia ktora potvrdila objednavku */
    private Transaction transaction;
    /** Aky je zdroj potvrdenia */
    private ConfirmationChannel confirmationSource;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Objednavka ku ktorej patri faktura
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * Objednavka ku ktorej patri faktura
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * Cislo faktury
     * @return the invoiceNumber
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Cislo faktury
     * @param invoiceNumber the invoiceNumber to set
     */
    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    /**
     * Datum vystavenia faktury
     * @return the issueDate
     */
    public Date getIssueDate() {
        return issueDate;
    }

    /**
     * Datum vystavenia faktury
     * @param issueDate the issueDate to set
     */
    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Datum zdanitelneho plneni
     * @return the taxableFulfillmentDate
     */
    public Date getTaxableFulfillmentDate() {
        return taxableFulfillmentDate;
    }

    /**
     * Datum zdanitelneho plneni
     * @param taxableFulfillmentDate the taxableFulfillmentDate to set
     */
    public void setTaxableFulfillmentDate(Date taxableFulfillmentDate) {
        this.taxableFulfillmentDate = taxableFulfillmentDate;
    }

    /**
     * Transackia ktora potvrdila objednavku
     * @return the transaction
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * Transackia ktora potvrdila objednavku
     * @param transaction the transaction to set
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * Aky je zdroj potvrdenia
     * @return the confirmationSource
     */
    public ConfirmationChannel getConfirmationSource() {
        return confirmationSource;
    }

    /**
     * Aky je zdroj potvrdenia
     * @param confirmationSource the confirmationSource to set
     */
    public void setConfirmationSource(ConfirmationChannel confirmationSource) {
        this.confirmationSource = confirmationSource;
    }
}
