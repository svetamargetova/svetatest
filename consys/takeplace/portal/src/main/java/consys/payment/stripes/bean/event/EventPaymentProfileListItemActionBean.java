package consys.payment.stripes.bean.event;

import consys.payment.bo.EventPaymentProfile;
import consys.payment.service.EventService;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.stripes.bean.PaymentActionBean;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.DontValidate;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.integration.spring.SpringBean;

/**
 *
 * @author pepa
 */
public class EventPaymentProfileListItemActionBean extends PaymentActionBean {

    @SpringBean
    private EventService eventService;
    // data
    private String eventUuid;
    private String profileUuid;
    private EventPaymentProfile profile;

    @DontValidate
    @DefaultHandler
    public Resolution preView() throws NoRecordException, RequiredPropertyException {
        profile = eventService.loadEventPaymentProfile(profileUuid);
        return new ForwardResolution("/web/event/eventProfileDetail.jsp");
    }

    public Resolution edit() {
        ForwardResolution resolution = new ForwardResolution(EditEventPaymentProfileListItemActionBean.class);
        resolution.addParameter("eventUuid", eventUuid);
        resolution.addParameter("profileUuid", profileUuid);
        return resolution;
    }

    public Resolution back() {
        RedirectResolution resolution = new RedirectResolution(EventPaymentProfileListActionBean.class);
        resolution.addParameter("eventUuid", eventUuid);
        return resolution;
    }

    public EventPaymentProfile getProfile() {
        return profile;
    }

    public String getProfileUuid() {
        return profileUuid;
    }

    public void setProfileUuid(String profileUuid) {
        this.profileUuid = profileUuid;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }
}
