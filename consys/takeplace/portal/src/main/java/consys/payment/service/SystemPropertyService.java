package consys.payment.service;

import consys.payment.bo.SystemProperty;
import consys.payment.service.exception.NoRecordException;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SystemPropertyService {
    
    public SystemProperty loadByKey(String key) throws NoRecordException;

    public void createOrUpdateSystemPropety(String key, String value);

    public void injectCached(String key, Object o);

    public <T> T getInjected(String key);
}
