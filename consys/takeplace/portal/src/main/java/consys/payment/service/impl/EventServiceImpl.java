package consys.payment.service.impl;

import consys.common.utils.collection.Sets;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.common.utils.enums.CountryEnum;
import consys.payment.SystemProperties;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Order;
import consys.payment.bo.PaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.dao.EventDao;
import consys.payment.dao.EventPaymentProfileDao;
import consys.payment.service.EventService;
import consys.payment.service.PaymentProfileService;
import consys.payment.service.exception.EventAlreadyActiveException;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.utils.ProfileUtils;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventServiceImpl extends Service implements EventService {

    // -- DAO
    private EventDao eventDao;
    private EventPaymentProfileDao paymentProfileDao;
    // -- Services        
    private PaymentProfileService profileService;

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public Event createEvent(String uuid, CountryEnum country) throws RequiredPropertyException {
        return createEvent(uuid, true, country);
    }

    @Override
    public Event createEvent(String uuid, boolean createDefaultProfile, CountryEnum country)
            throws RequiredPropertyException {

        if (StringUtils.isBlank(uuid) || country == null) {
            throw new RequiredPropertyException("Event Uuid is missing or country");
        }
        logger.info("New event: {}", uuid);
        // Vytvorime event
        Event event = new Event();
        event.setUuid(uuid);
        event.setActive(false);
        event.setCreateDate(DateProvider.getCurrentDate());
        logger.debug("Creating {}", event);
        eventDao.create(event);

        if (createDefaultProfile) {
            // Vytvorime novy platobny profil, prazdny 
            PaymentProfileRevision revision = new PaymentProfileRevision();
            revision.setCountry(country);
            PaymentProfile newEventProfile = new PaymentProfile();
            Set<PaymentProfileRevision> revs = Sets.newHashSet();
            newEventProfile.setRevisions(revs);
            logger.debug("Creating default payment profile revision");
            profileService.createProfile(newEventProfile);
            revision.setParent(newEventProfile);
            profileService.createProfileRevisionWithoutValidation(revision);
            newEventProfile.getRevisions().add(revision);
            newEventProfile.setActualRevision(revision);
            profileService.updateProfile(newEventProfile);

            // Vytvorime platobny event platobny profil
            EventPaymentProfile defaultProfile = new EventPaymentProfile();
            defaultProfile.setEvent(event);
            defaultProfile.setActive(false);
            defaultProfile.setUuid(newEventProfile.getUuid());
            defaultProfile.setOrderBankTransferInvoicedToThisProfile(false);
            defaultProfile.setVatType(ProfileUtils.resolveProcessType(country));
            defaultProfile.setCurrency(ProfileUtils.resolveCurrency(country));
            logger.debug("Creating {}", defaultProfile);
            logger.debug("Default {} fees settings: unit {} share {}", new Object[]{country.getName(), defaultProfile.getOrderUnitFeeBank().toPlainString(), defaultProfile.getOrderShareFeeBank().toPlainString()});

            defaultProfile.setPaymentProfile(newEventProfile);
            paymentProfileDao.create(defaultProfile);
        }
        return event;
    }

    @Override
    public void createEventPaymentProfile(Event event, EventPaymentProfile epp)
            throws RequiredPropertyException {
        if (event == null || epp == null) {
            throw new RequiredPropertyException("Missing event or profile");
        }
        epp.setEvent(event);
        epp.setActive(event.isActive());

        // Vytvorime novy platobny profil, prazdny 
        PaymentProfileRevision revision = epp.getPaymentProfile().getActualRevision();
        epp.getPaymentProfile().setActualRevision(null);
        profileService.createProfile(epp.getPaymentProfile());
        revision.setParent(epp.getPaymentProfile());
        profileService.createProfileRevisionWithoutValidation(revision);
        if (CollectionUtils.isEmpty(epp.getPaymentProfile().getRevisions())) {
            epp.getPaymentProfile().setRevisions(new HashSet<PaymentProfileRevision>(1));
        }
        epp.getPaymentProfile().getRevisions().add(revision);
        epp.getPaymentProfile().setActualRevision(revision);
        profileService.updateProfile(epp.getPaymentProfile());
        paymentProfileDao.create(epp);
    }

    @Override
    public Event createCloneEvent(EventPaymentProfile originalProfile, String cloneEventUuid) throws RequiredPropertyException, NoRecordException {
        logger.info("Cloning event: {} -> {}", originalProfile.getEvent().getUuid(), cloneEventUuid);
        if (originalProfile == null || StringUtils.isBlank(cloneEventUuid)) {
            throw new RequiredPropertyException("Clone event uuid is missing or not load original event");
        }

        // Vytvorime event
        Event event = new Event();
        event.setUuid(cloneEventUuid);
        event.setActive(false);
        event.setCreateDate(DateProvider.getCurrentDate());
        logger.debug("Creating {}", event);
        eventDao.create(event);

        // Naklonujeme profil

        PaymentProfileRevision originalRevision = originalProfile.getPaymentProfile().getActualRevision();
        CountryEnum country = originalRevision.getCountry();

        // Vytvorime novy platobny profil, prazdny 
        PaymentProfile newEventProfile = new PaymentProfile();

        PaymentProfileRevision revision = new PaymentProfileRevision();
        revision.setAccountBankSwift(originalRevision.getAccountBankSwift());
        revision.setAccountIban(originalRevision.getAccountIban());
        revision.setCity(originalRevision.getCity());
        revision.setCountry(country);
        revision.setName(originalRevision.getName());
        revision.setParent(newEventProfile);
        revision.setRegNo(originalRevision.getRegNo());
        revision.setStreet(originalRevision.getStreet());
        revision.setVatNo(originalRevision.getVatNo());
        revision.setZip(originalRevision.getZip());
        revision.setNoVatPayer(originalRevision.getNoVatPayer() == null ? false : originalRevision.getNoVatPayer());

        Set<PaymentProfileRevision> revs = Sets.newHashSet();
        newEventProfile.setRevisions(revs);

        logger.debug("Creating cloned default payment profile revision");
        profileService.createProfile(newEventProfile);
        profileService.createProfileRevisionWithoutValidation(revision);

        newEventProfile.getRevisions().add(revision);
        newEventProfile.setActualRevision(revision);
        profileService.updateProfile(newEventProfile);

        // Vytvorime platobny event platobny profil
        EventPaymentProfile defaultProfile = new EventPaymentProfile();
        defaultProfile.setEvent(event);
        defaultProfile.setActive(false);
        defaultProfile.setUuid(newEventProfile.getUuid());
        defaultProfile.setOrderBankTransferInvoicedToThisProfile(originalProfile.isOrderBankTransferInvoicedToThisProfile());
        defaultProfile.setVatType(originalProfile.getVatType());
        defaultProfile.setCurrency(originalProfile.getCurrency());
        defaultProfile.setInvoiceNote(originalProfile.getInvoiceNote());
        defaultProfile.setOrderMaxFee(originalProfile.getOrderMaxFee());
        defaultProfile.setOrderShareFeeBank(originalProfile.getOrderShareFeeBank());
        defaultProfile.setOrderShareFeeOnline(originalProfile.getOrderShareFeeOnline());
        defaultProfile.setOrderShareFeeOrganizator(originalProfile.getOrderShareFeeOrganizator());
        defaultProfile.setOrderUnitFeeBank(originalProfile.getOrderUnitFeeBank());
        defaultProfile.setOrderUnitFeeOnline(originalProfile.getOrderUnitFeeOnline());
        defaultProfile.setOrderUnitFeeOrganizator(originalProfile.getOrderUnitFeeOrganizator());
        defaultProfile.setTransferInvoiceCounter(0);

        logger.debug("Creating clone {}", defaultProfile);
        logger.debug("Default {} fees settings: unit {} share {}", new Object[]{country.getName() == null ? "Zero" : country.getName(),
                    defaultProfile.getOrderUnitFeeBank().toPlainString(), defaultProfile.getOrderShareFeeBank().toPlainString()});

        defaultProfile.setPaymentProfile(newEventProfile);
        paymentProfileDao.create(defaultProfile);

        return event;
    }

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    @Override
    public EventPaymentProfile loadEventPaymentProfile(Long id)
            throws NoRecordException, RequiredPropertyException {
        if (id == null || id < 0) {
            throw new RequiredPropertyException("Missing or ID is negative");
        }
        return paymentProfileDao.load(id, EventPaymentProfile.class);
    }

    @Override
    public EventPaymentProfile loadEventPaymentProfile(String uuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("Missing payment profile uuid");
        }
        return paymentProfileDao.loadEventPaymentProfile(uuid);
    }

    @Override
    public Event loadEvent(String uuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyException("Uuid is missing");
        }
        return eventDao.loadEventByUuid(uuid);
    }

    @Override
    public int loadAllEventsCount(boolean active, Date fromDate, Date toDate) {
        return eventDao.loadAllEventsCount(active, fromDate, toDate);
    }

    @Override
    public EventPaymentProfile loadEventDefaultPaymentProfileDetailed(String eventUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Uuid is missing");
        }
        return listEventPaymentProfiles(eventUuid).get(0);
    }

    @Override
    public EventPaymentProfile loadEventDefaultPaymentProfile(String eventUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Uuid is missing");
        }
        return paymentProfileDao.listEventPaymentProfiles(eventUuid).get(0);
    }

    @Override
    public Event loadEventByLicenseOrderNumber(String orderNumber) throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(orderNumber)) {
            throw new RequiredPropertyException("Missing order number");
        }
        return eventDao.loadEventByLicenseOrderNumber(orderNumber);
    }

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    @Override
    public List<EventPaymentProfile> listEventPaymentProfiles(String eventUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Missing eventUuid param");
        }
        return paymentProfileDao.listEventPaymentProfilesWithActualRevision(eventUuid);
    }

    @Override
    public List<EventPaymentProfile> listEventPaymentProfilesDetailed(String eventUuid)
            throws NoRecordException, RequiredPropertyException {
        if (StringUtils.isBlank(eventUuid)) {
            throw new RequiredPropertyException("Missing eventUuid param");
        }
        return paymentProfileDao.listEventPaymentProfilesWithActualRevisionAndLicenseDetailed(eventUuid);
    }

    @Override
    public List<Event> listEvents(boolean active, Date fromDate, Date toDate, int startItem, int itemsPerPage) {
        return eventDao.listEvents(active, fromDate, toDate, startItem, itemsPerPage);
    }

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateEventPaymentProfile(EventPaymentProfile eventPaymentProfile)
            throws RequiredPropertyException {
        paymentProfileDao.update(eventPaymentProfile);
    }

    @Override
    public void updateEventPaymentProfileRevision(EventPaymentProfile eventPaymentProfile, PaymentProfileRevision newProfile)
            throws RequiredPropertyException, IbanFormatException {
        eventPaymentProfile.setVatType(newProfile.getNoVatPayer() == null
                ? ProfileUtils.resolveProcessType(newProfile.getCountry())
                : (newProfile.getNoVatPayer() ? InvoiceProcessType.VAT_AD : ProfileUtils.resolveProcessType(newProfile.getCountry())));
        updateEventPaymentProfile(eventPaymentProfile);
        boolean isProfileUsed = profileService.isEventPaymentProfileUsedInActiveOrder(eventPaymentProfile.getUuid());
        // ak 0 potom zmenime aktualnu reviziu profilu, v opacnom pripade vytvorime novu reviziu
        if (!isProfileUsed) {
            PaymentProfileRevision revision = eventPaymentProfile.getPaymentProfile().getActualRevision();
            revision.setAccountBankSwift(newProfile.getAccountBankSwift());
            revision.setAccountIban(newProfile.getAccountIban());
            revision.setCity(newProfile.getCity());
            revision.setCountry(newProfile.getCountry());
            revision.setName(newProfile.getName());
            revision.setRegNo(newProfile.getRegNo());
            revision.setStreet(newProfile.getStreet());
            revision.setVatNo(newProfile.getVatNo());
            revision.setZip(newProfile.getZip());
            revision.setNoVatPayer(newProfile.getNoVatPayer() == null ? false : newProfile.getNoVatPayer());
            newProfile.setId(revision.getId());
            newProfile.setUuid(revision.getUuid());
            newProfile.setParent(revision.getParent());
            profileService.updateProfileRevision(revision, true);
        } else {
            newProfile.setParent(eventPaymentProfile.getPaymentProfile());
            profileService.createProfileRevision(newProfile, true);
            eventPaymentProfile.getPaymentProfile().setActualRevision(newProfile);
            profileService.updateProfile(eventPaymentProfile.getPaymentProfile());
        }
    }

    @Override
    public void updateEventActivateNow(String event, String clientNumber)
            throws NoRecordException, RequiredPropertyException, EventAlreadyActiveException {
        if (StringUtils.isBlank(event)) {
            throw new RequiredPropertyException("Missing event uuid");
        }
        Event e = loadEvent(event);
        updateEventActivateNow(e, clientNumber);

    }

    @Override
    public void updateEventActivateNow(Event event, String clientNumber)
            throws NoRecordException, RequiredPropertyException, EventAlreadyActiveException {

        if (event == null) {
            throw new RequiredPropertyException("Event is missing");
        }

        if (event.isActive()) {
            throw new EventAlreadyActiveException("Event already activated from " + event.getActivationDate());
        }

        logger.info("Activating {} with client number {}", event, clientNumber);
        event.setActivationDate(DateProvider.getCurrentDate());
        event.setActivationEndDate(DateUtils.addMonths(event.getActivationDate(), SystemProperties.EVENT_ACTIVE));
        event.setActive(true);
        event.setInvoiceClientNumber(clientNumber);
        updateEvent(event);

        try {
            List<EventPaymentProfile> profiles = listEventPaymentProfiles(event.getUuid());
            for (EventPaymentProfile eventPaymentProfile : profiles) {
                eventPaymentProfile.setActive(true);
                logger.info("Activating {}", eventPaymentProfile);
                updateEventPaymentProfile(eventPaymentProfile);
            }
        } catch (NoRecordException ex) {
        }
    }

    @Override
    public void updateEventLicenseOrder(Event event, Order order)
            throws EventLicenseAlreadyPurchased, RequiredPropertyException {
        if (event == null || order == null) {
            throw new RequiredPropertyException("Missing event or order");
        }
        if (event.getLicenseOrder() != null) {
            throw new EventLicenseAlreadyPurchased(event + " has already purchased license order " + event.getLicenseOrder());
        }
        event.setLicenseOrder(order);
        eventDao.update(event);
    }


    /*------------------------------------------------------------------------*/
    /* ---  U T I L S  --- */
    /*------------------------------------------------------------------------*/
    private void updateEvent(Event event)
            throws RequiredPropertyException {
        if (event == null || StringUtils.isBlank(event.getUuid()) || event.getId() == null || event.getId() <= 0L) {
            throw new RequiredPropertyException("Event properties are invalid");
        }
        eventDao.update(event);
    }

    /*========================================================================*/
    /* GETTER SETTER */
    /*========================================================================*/
    /**
     * @return the eventDao
     */
    public EventDao getEventDao() {
        return eventDao;
    }

    /**
     * @param eventDao the eventDao to set
     */
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    /**
     * @return the profileService
     */
    public PaymentProfileService getProfileService() {
        return profileService;
    }

    /**
     * @param profileService the profileService to set
     */
    public void setProfileService(PaymentProfileService profileService) {
        this.profileService = profileService;
    }

    /**
     * @return the paymentProfileDao
     */
    public EventPaymentProfileDao getPaymentProfileDao() {
        return paymentProfileDao;
    }

    /**
     * @param paymentProfileDao the paymentProfileDao to set
     */
    public void setPaymentProfileDao(EventPaymentProfileDao paymentProfileDao) {
        this.paymentProfileDao = paymentProfileDao;
    }
}
