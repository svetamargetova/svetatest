/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.dao;

import consys.payment.bo.User;
import consys.payment.service.exception.NoRecordException;

/**
 *
 * @author palo
 */
public interface UserDao extends PersistedObjectDao<User> {

    public User loadUserByUuid(String uuid) throws NoRecordException;

    public User loadUserWithProfilesByUuid(String uuid) throws NoRecordException;
}
