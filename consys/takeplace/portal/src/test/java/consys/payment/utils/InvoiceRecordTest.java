package consys.payment.utils;

import com.google.common.collect.Lists;
import consys.common.utils.enums.CountryEnum;
import consys.payment.bo.*;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.bo.enums.ProductCategory;
import java.math.BigDecimal;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class InvoiceRecordTest {


    private static final Logger log = LoggerFactory.getLogger(InvoiceRecordTest.class);

    @Test(groups = {"service"})
    public void testInvoiceItemRecords() {

        Product p = new Product();
        p.setCategory(ProductCategory.TICKET);
        OrderItem item1 = new OrderItem();
        item1.setDiscount(0);
        item1.setName("A");
        item1.setQuantity(1);
        item1.setUnitPrice(new BigDecimal("100"));
        item1.setVatRate(new BigDecimal("20"));
        item1.setProduct(p);

        OrderItem item2 = new OrderItem();
        item2.setDiscount(0);
        item2.setName("A");
        item2.setQuantity(1);
        item2.setUnitPrice(new BigDecimal("350"));
        item2.setVatRate(new BigDecimal("20"));
        item2.setProduct(p);


        InvoiceItemRecord r1 = new InvoiceItemRecord(item1);
        assertEquals(r1.getDiscount(), 0);
        assertEquals(r1.getName(),"A");
        assertEquals(r1.getQuantity(), 1);
        assertEquals(r1.getUnitPrice().compareTo(new BigDecimal("100")),0,"Unit Price "+r1.getUnitPrice().toPlainString());
        assertEquals(r1.getUnitNetPrice().compareTo(new BigDecimal("83.33")),0,"Unit Net price "+r1.getUnitNetPrice().toPlainString());
        assertEquals(r1.getUnitVatAmount().compareTo(new BigDecimal("16.67")),0,"Unit Vat Amount "+r1.getUnitVatAmount().toPlainString());
        assertEquals(r1.getPrice().compareTo(new BigDecimal("100")),0,"Price "+r1.getPrice().toPlainString());
        assertEquals(r1.getNetPrice().compareTo(new BigDecimal("83.33")),0,"Net price "+r1.getNetPrice().toPlainString());
        assertEquals(r1.getVatAmount().compareTo(new BigDecimal("16.67")),0,"Vat Amount "+r1.getVatAmount().toPlainString());

        item1.setQuantity(3);
        r1 = new InvoiceItemRecord(item1);
        assertEquals(r1.getDiscount(), 0);
        assertEquals(r1.getName(),"A");
        assertEquals(r1.getQuantity(), 3);
        assertEquals(r1.getUnitPrice().compareTo(new BigDecimal("100")),0,"Unit Price "+r1.getUnitPrice().toPlainString());
        assertEquals(r1.getUnitNetPrice().compareTo(new BigDecimal("83.33")),0,"Unit Net price "+r1.getUnitNetPrice().toPlainString());
        assertEquals(r1.getUnitVatAmount().compareTo(new BigDecimal("16.67")),0,"Unit Vat Amount "+r1.getUnitVatAmount().toPlainString());
        assertEquals(r1.getPrice().compareTo(new BigDecimal("300")),0,"Price "+r1.getPrice().toPlainString());
        assertEquals(r1.getNetPrice().compareTo(new BigDecimal("249.99")),0,"Net price "+r1.getNetPrice().toPlainString());
        assertEquals(r1.getVatAmount().compareTo(new BigDecimal("50.01")),0,"Vat Amount "+r1.getVatAmount().toPlainString());

        item1.setDiscount(25);
        r1 = new InvoiceItemRecord(item1);
        assertEquals(r1.getDiscount(), 25);        
        assertEquals(r1.getQuantity(), 3);
        assertEquals(r1.getUnitPrice().compareTo(new BigDecimal("75")),0,"Unit Price "+r1.getUnitPrice().toPlainString());
        assertEquals(r1.getUnitNetPrice().compareTo(new BigDecimal("62.4975")),0,"Unit Net price "+r1.getUnitNetPrice().toPlainString());
        assertEquals(r1.getUnitVatAmount().compareTo(new BigDecimal("12.5025")),0,"Unit Vat Amount "+r1.getUnitVatAmount().toPlainString());
        assertEquals(r1.getPrice().compareTo(new BigDecimal("225")),0,"Price "+r1.getPrice().toPlainString());
        assertEquals(r1.getNetPrice().compareTo(new BigDecimal("187.4925")),0,"Net price "+r1.getNetPrice().toPlainString());
        assertEquals(r1.getVatAmount().compareTo(new BigDecimal("37.5075")),0,"Vat Amount "+r1.getVatAmount().toPlainString());


        InvoiceItemRecord r2 = new InvoiceItemRecord(item2);
        assertEquals(r2.getDiscount(), 0);
        assertEquals(r2.getName(),"A");
        assertEquals(r2.getQuantity(), 1);
        assertEquals(r2.getUnitPrice().compareTo(new BigDecimal("350")),0,"Unit Price "+r2.getUnitPrice().toPlainString());
        assertEquals(r2.getUnitNetPrice().compareTo(new BigDecimal("291.655")),0,"Unit Net price "+r2.getUnitNetPrice().toPlainString());
        assertEquals(r2.getUnitVatAmount().compareTo(new BigDecimal("58.345")),0,"Unit Vat Amount "+r2.getUnitVatAmount().toPlainString());
        assertEquals(r2.getPrice().compareTo(new BigDecimal("350")),0,"Price "+r2.getPrice().toPlainString());
        assertEquals(r2.getNetPrice().compareTo(new BigDecimal("291.655")),0,"Net price "+r2.getNetPrice().toPlainString());
        assertEquals(r2.getVatAmount().compareTo(new BigDecimal("58.345")),0,"Vat Amount "+r2.getVatAmount().toPlainString());

        item2.setDiscount(30);
        item2.setQuantity(4);
        r2 = new InvoiceItemRecord(item2);
        assertEquals(r2.getDiscount(), 30);
        assertEquals(r2.getQuantity(), 4);
        assertEquals(r2.getUnitPrice().compareTo(new BigDecimal("245")),0,"Unit Price "+r2.getUnitPrice().toPlainString());
        assertEquals(r2.getUnitNetPrice().compareTo(new BigDecimal("204.1585")),0,"Unit Net price "+r2.getUnitNetPrice().toPlainString());
        assertEquals(r2.getUnitVatAmount().compareTo(new BigDecimal("40.8415")),0,"Unit Vat Amount "+r2.getUnitVatAmount().toPlainString());
        assertEquals(r2.getPrice().compareTo(new BigDecimal("980")),0,"Price "+r2.getPrice().toPlainString());
        assertEquals(r2.getNetPrice().compareTo(new BigDecimal("816.634")),0,"Net price "+r2.getNetPrice().toPlainString());
        assertEquals(r2.getVatAmount().compareTo(new BigDecimal("163.366")),0,"Vat Amount "+r2.getVatAmount().toPlainString());


        OrderItem item3 = new OrderItem();
        item3.setDiscount(0);
        item3.setName("A");
        item3.setQuantity(35);
        item3.setUnitPrice(new BigDecimal("1250"));
        item3.setVatRate(new BigDecimal("9"));
        item3.setProduct(p);

        InvoiceItemRecord r3 = new InvoiceItemRecord(item3);
        assertEquals(r3.getDiscount(), 0);
        assertEquals(r3.getQuantity(), 35);
        assertEquals(r3.getUnitPrice().compareTo(new BigDecimal("1250")),0,"Unit Price "+r3.getUnitPrice().toPlainString());
        assertEquals(r3.getUnitNetPrice().compareTo(new BigDecimal("1146.75")),0,"Unit Net price "+r3.getUnitNetPrice().toPlainString());
        assertEquals(r3.getUnitVatAmount().compareTo(new BigDecimal("103.25")),0,"Unit Vat Amount "+r3.getUnitVatAmount().toPlainString());
        assertEquals(r3.getPrice().compareTo(new BigDecimal("43750")),0,"Price "+r3.getPrice().toPlainString());
        assertEquals(r3.getNetPrice().compareTo(new BigDecimal("40136.25")),0,"Net price "+r3.getNetPrice().toPlainString());
        assertEquals(r3.getVatAmount().compareTo(new BigDecimal("3613.75")),0,"Vat Amount "+r3.getVatAmount().toPlainString());
        
        OrderItem item4 = new OrderItem();
        item4.setDiscount(0);
        item4.setName("A");
        item4.setQuantity(1);
        item4.setUnitPrice(new BigDecimal("12500"));
        item4.setVatRate(new BigDecimal("20"));
        item4.setProduct(p);
        InvoiceItemRecord r4 = new InvoiceItemRecord(item4);
        assertEquals(r4.getDiscount(), 0);
        assertEquals(r4.getQuantity(), 1);
        assertEquals(r4.getUnitPrice().compareTo(new BigDecimal("12500")),0,"Unit Price "+r4.getUnitPrice().toPlainString());
        assertEquals(r4.getUnitNetPrice().compareTo(new BigDecimal("10416.25")),0,"Unit Net price "+r4.getUnitNetPrice().toPlainString());
        assertEquals(r4.getUnitVatAmount().compareTo(new BigDecimal("2083.75")),0,"Unit Vat Amount "+r4.getUnitVatAmount().toPlainString());
        


    }


    @Test(groups = {"service"}, dependsOnMethods={"testInvoiceItemRecords"})
    public void testInvoiceRecord() {

        Product p = new Product();
        p.setCategory(ProductCategory.TICKET);
        OrderItem item1 = new OrderItem();
        item1.setDiscount(0);
        item1.setName("A");
        item1.setQuantity(3);
        item1.setUnitPrice(new BigDecimal("100"));
        item1.setVatRate(new BigDecimal("20"));
        item1.setProduct(p);

        OrderItem item2 = new OrderItem();
        item2.setDiscount(30);
        item2.setQuantity(4);
        item2.setName("A");        
        item2.setUnitPrice(new BigDecimal("350"));
        item2.setVatRate(new BigDecimal("20"));
        item2.setProduct(p);

        OrderItem item3 = new OrderItem();
        item3.setDiscount(0);
        item3.setName("A");
        item3.setQuantity(35);
        item3.setUnitPrice(new BigDecimal("1250"));
        item3.setVatRate(new BigDecimal("9"));
        item3.setProduct(p);
                

        Invoice invoice = mock(Invoice.class);
        Order order = mock(Order.class);
        EventPaymentProfile epp = mock(EventPaymentProfile.class);
        when(epp.getVatType()).thenReturn(InvoiceProcessType.VAT_CZ);
        
        PaymentProfileRevision ppr = mock(PaymentProfileRevision.class);
        when(ppr.getCountry()).thenReturn(CountryEnum.CZECH_REPUBLIC);
        
        when(invoice.getOrder()).thenReturn(order);
        when(order.getItems()).thenReturn(Lists.newArrayList(item1,item2,item3));
        when(order.getSupplier()).thenReturn(epp);
        when(order.getCustomer()).thenReturn(ppr);

        InvoiceRecord record = new InvoiceRecord(invoice.getOrder());
        assertEquals(record.getSubTotal().compareTo(new BigDecimal("41202.874")),0,"Subtotal "+record.getSubTotal().toPlainString());
        assertEquals(record.getTotal().compareTo(new BigDecimal("45030")),0,"Total "+record.getTotal().toPlainString());
        assertEquals(record.getVatCharged().compareTo(new BigDecimal("3827.126")),0,"Vatcharged "+record.getVatCharged().toPlainString());
        assertEquals(record.getVatTableRecord(new BigDecimal(20)).getVatBase().compareTo(new BigDecimal("1066.624")),0,"Vat base table for 20: "+record.getVatTableRecord(new BigDecimal(20)).getVatBase().toPlainString());
        assertEquals(record.getVatTableRecord(new BigDecimal(20)).getVatAmount().compareTo(new BigDecimal("213.376")),0,"Vat amount table for 20: "+record.getVatTableRecord(new BigDecimal(20)).getVatAmount().toPlainString());
        assertEquals(record.getVatTableRecord(new BigDecimal(9)).getVatBase().compareTo(new BigDecimal("40136.25")),0,"Vat table for 9: "+record.getVatTableRecord(new BigDecimal(9)).getVatBase().toPlainString());
        assertEquals(record.getVatTableRecord(new BigDecimal(9)).getVatAmount().compareTo(new BigDecimal("3613.75")),0,"Vat table for 9: "+record.getVatTableRecord(new BigDecimal(9)).getVatAmount().toPlainString());                             
    }


}
