package consys.payment.service;

import consys.common.utils.UuidProvider;
import java.util.List;
import consys.payment.bo.enums.ProductCategory;
import consys.common.utils.collection.Lists;
import consys.common.utils.enums.CountryEnum;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.OrderAlreadyCanceledException;
import consys.payment.service.exception.OrderAlreadyPayedException;
import consys.payment.service.exception.EventAlreadyActiveException;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.EventProfileNotFilledException;
import consys.payment.bo.Event;
import consys.payment.bo.Product;
import consys.payment.config.AbstractServiceTest;
import consys.payment.service.EventService;
import consys.payment.service.ProductService;
import consys.payment.service.exception.EventLicenseNotPayedException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.PropertyReadOnlyException;
import consys.payment.service.exception.RequiredPropertyException;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProductServiceTest extends AbstractServiceTest {

    @Autowired
    private EventService eventService;    
    @Autowired
    private ProductService productService;
    
    @Test(groups = {"service"})
    public void testCreateProductWithNewEventOrder() throws RequiredPropertyException, IbanFormatException, NoRecordException, PropertyReadOnlyException, EventLicenseNotPayedException {


        Event e1 = eventService.createEvent(UuidProvider.getUuid(), CountryEnum.CZECH_REPUBLIC);
        Event e2 = eventService.createEvent(UuidProvider.getUuid(), CountryEnum.SLOVAKIA);
       

        Product e1p1 = new Product();
        e1p1.setCategory(ProductCategory.TICKET);
        e1p1.setName("Product name 1");
        e1p1.setVatRate(BigDecimal.ZERO);
        e1p1.setPrice(new BigDecimal(1200));
        e1p1.setUuid(UuidProvider.getUuid());
        productService.createEventProduct(e1.getUuid(), e1p1);
        
        Product e1p2 = new Product();
        e1p2.setCategory(ProductCategory.TICKET);
        e1p2.setName("Product name 2");
        e1p2.setVatRate(BigDecimal.ZERO);
        e1p2.setPrice(new BigDecimal(1400));
        e1p2.setUuid(UuidProvider.getUuid());
        productService.createEventProduct(e1.getUuid(), e1p2);
        
        List<Product> products = productService.listAllEventProducts(e1.getUuid());
        assertEquals(products.size(), 2);
        assertEquals(products.get(0), e1p1);
        assertEquals(products.get(1), e1p2);
        products = productService.listAllEventPaymentProfileProducts(eventService.loadEventDefaultPaymentProfileDetailed(e1.getUuid()).getUuid());
        assertEquals(products.size(), 2);
        assertEquals(products.get(0), e1p1);
        assertEquals(products.get(1), e1p2);
        
        
        Product e2p1 = new Product();
        e2p1.setCategory(ProductCategory.TICKET);
        e2p1.setName("Product name 1");
        e2p1.setVatRate(BigDecimal.ZERO);
        e2p1.setPrice(new BigDecimal(1200));
        e2p1.setUuid(UuidProvider.getUuid());
        
        
        Product e2p2 = new Product();
        e2p2.setCategory(ProductCategory.TICKET);
        e2p2.setName("Product name 2");
        e2p2.setVatRate(BigDecimal.ZERO);
        e2p2.setPrice(new BigDecimal(1400));
        e2p2.setUuid(UuidProvider.getUuid());
        productService.createEventProducts(e2.getUuid(), Lists.newArrayList(e2p1,e2p2));
        products = productService.listAllEventProducts(e2.getUuid());
        assertEquals(products.size(), 2);
        assertEquals(products.get(0), e2p1);
        assertEquals(products.get(1), e2p2);
        
        
        e2p1.setName("Other name");
        productService.updateEventProduct(e2.getUuid(), e2p1);
        Product e2p1l = productService.loadProduct(e2p1.getUuid());
        assertEquals(e2p1.getName(), e2p1l.getName());
        
                
        e1p1.setName("1");
        e1p2.setName("2");
        productService.updateEventProducts(e1.getUuid(), Lists.newArrayList(e1p1,e1p2));
        products = productService.listAllEventProducts(e1.getUuid());
        assertEquals(products.size(), 2);
        assertEquals(products.get(0).getName(), "1");
        assertEquals(products.get(1).getName(), "2");
        
        
    }
    
    /**
     * Test realneho scenara:
     *
     * 1] Uzivatel vytvori nejake registracne baliky
     * 2] Chce si kupit licenciu
     * 3] error - nema vyplneny profil
     * 4] Vyplni profil
     * 5] Chce si kupit licenciu
     * 6] Event ma vystavnu objednavku na licenciu
     * 7] Event ma jeden profil a jeho jednu reviziu
     *       Test: pokad nema profil zadnu objednavku tak sa upravuje revizia
     * 8] pokus o opetovne kupeni licencie - fail
     * 9] pokus o aktivaciu eventu
     * 10] potvrdeni kupy licencie
     * 11] event ma pripradene cislo
     * 12] aktivacia eventu
     *
     */
    @Test(groups = {"service"})
    public void testBuyLicense() throws RequiredPropertyException, IbanFormatException, NoRecordException, PropertyReadOnlyException, EventLicenseNotPayedException, EventAlreadyActiveException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, OrderAlreadyPayedException, OrderAlreadyCanceledException {
//        String event = UuidProvider.getUuid();
//        eventService.createEvent(event, InvoiceProcessType.VAT_CZ, new BigDecimal(20), Currency.EUR);
//
//         1]
//        Product p = new Product();
//        p.setCategory(ProductCategory.TICKET);
//        p.setName("Product name");
//        p.setPrice(new BigDecimal(1200));
//        p.setUuid(UuidProvider.getUuid());
//        productService.updateEventAddProduct(event, p);
//         2]
//        Event e = eventService.loadEventWithProfiles(event);
//
//        try {
//            eventService.buyLicense(event, e.getProfiles().get(0).getUuid());
//            fail();
//        } catch (EventProfileNotFilledException ex) {
//            // 3]
//        }
//         4]
//        PaymentProfileRevision profileRev = new PaymentProfileRevision();
//        profileRev.setAccountBankSwift("123");
//        profileRev.setAccountIban("BE68539007547034");
//        profileRev.setCity("City");
//        profileRev.setCountry(CountryEnum.ECUADOR);
//        profileRev.setName("Name");
//        profileRev.setRegNo("123");
//        profileRev.setStreet("Street");
//        profileRev.setVatNo("123");
//        profileRev.setZip("123");
//        eventService.updateEventAddProfileRevision(event, e.getProfiles().get(0).getUuid(), profileRev);
//
//        // 5]
//        eventService.buyLicense(event, e.getProfiles().get(0).getUuid());
//        // 6]
//        e = eventService.loadEventWithProfiles(event);
//        assertNotNull(e.getLicenseInvoice());
//        assertEquals(e.getLicenseInvoice().getStateType(), InvoiceStateEnum.ACTIVE);
//         7] 
//        assertEquals(e.getProfiles().size(), 1);
//        assertEquals(e.getProfiles().get(0).getRevisions().size(), 1);
//        // 8] pokus o opetovne kupeni licencie - fail
//        try {
//            eventService.buyLicense(event, e.getProfiles().get(0).getUuid());
//            fail();
//        } catch (EventLicenseAlreadyPurchased ex) {
//        }
//        // 9]
//        try {
//            eventService.updateEventActivateNow(event);
//            fail();
//        } catch (EventLicenseNotPayedException ex) {
//        }
//
//
//        // 10]
//        invoiceService.updateOrderToPaid(event, "TX123", new BigDecimal(500), BigDecimal.ZERO, DateProvider.getCurrentDate(), InvoicePaymentTypeEnum.BANK);
//        // 11]
//        e = eventService.loadEvent(event);
//        assertTrue(StringUtils.isNotBlank(e.getInvoiceNumber()));
//        assertNotNull(e.getLicenseInvoice());
//        assertEquals(e.getLicenseInvoice().getStateType(), InvoiceStateEnum.PAID);
//        // 12]
//        eventService.updateEventActivateNow(event);
//         13]
//        try {
//            eventService.updateEventActivateNow(event);
//            fail();
//        } catch (EventAlreadyActiveException ex) {
//        }
    }

   
}
