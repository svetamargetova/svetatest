package consys.payment.service;

import consys.payment.service.exception.ProductIncompatibilityException;
import java.util.List;
import java.util.Date;
import consys.payment.bo.enums.ProductCategory;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.common.utils.enums.CountryEnum;
import consys.payment.bo.Event;
import consys.payment.bo.Invoice;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.Product;
import consys.payment.bo.Transfer;
import consys.common.utils.enums.Currency;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.payment.bo.enums.InvoiceProcessType;
import consys.payment.bo.enums.TransferState;
import consys.payment.bo.thumb.ProductOrder;
import consys.payment.config.AbstractServiceTest;
import consys.payment.service.EventService;
import consys.payment.service.ProductService;
import consys.payment.service.TransferService;
import consys.payment.service.UserService;
import consys.payment.service.exception.EventAlreadyActiveException;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.EventLicenseNotPayedException;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.EventProfileNotFilledException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.OrderAlreadyCanceledException;
import consys.payment.service.exception.OrderAlreadyPayedException;
import consys.payment.service.exception.PropertyReadOnlyException;
import consys.payment.service.exception.RequiredPropertyException;
import consys.payment.utils.InvoiceItemRecord;
import consys.payment.utils.InvoiceRecord;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TransferServiceTest extends AbstractServiceTest {

    private EventService eventService;
    private ProductService productService;
    private TransferService transferService;
    private UserService userService;
    
    @Autowired
    @Required
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    @Required
    public void setTransferService(TransferService transferService) {
        this.transferService = transferService;
    }

    @Autowired
    @Required
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @Autowired
    @Required
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Test(groups = {"service"})
    public void testCreateOrder() throws RequiredPropertyException, IbanFormatException, NoRecordException, PropertyReadOnlyException, EventLicenseNotPayedException, EventAlreadyActiveException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, EventNotActiveException, OrderAlreadyPayedException, OrderAlreadyCanceledException, ProductIncompatibilityException {

//        String eventUuid = UuidProvider.getUuid();
//        logger.debug("------------ Preparing");
//         Event profile
//        PaymentProfileRevision profileRev = new PaymentProfileRevision();
//        profileRev.setAccountBankSwift("123");
//        profileRev.setAccountIban("BE68539007547034");
//        profileRev.setCity("City");
//        profileRev.setCountry(CountryEnum.CHRISTMAS_ISLAND);
//        profileRev.setName("Name");
//        profileRev.setRegNo("123");
//        profileRev.setStreet("Street");
//        profileRev.setVatNo("123");
//        profileRev.setZip("123");
//        eventService.updateEventAddNewProfile(eventUuid, profileRev);
//
//        Event e = eventService.loadEventWithProfiles(eventUuid);
//        eventService.updateEventVatType(eventUuid, InvoiceProcessType.VAT_CZ);
//        eventService.updateEventCurrency(eventUuid, Currency.CZK);
//        eventService.updateEventVatRate(eventUuid, BigDecimal.ZERO);
//        eventService.buyLicense(eventUuid, profileRev.getParent().getUuid());
//        invoiceService.updateOrderToPaid(eventUuid, "123", new BigDecimal(500), BigDecimal.ZERO, DateProvider.getCurrentDate(), ConfirmationChannel.BANK);
//        eventService.updateEventActivateNow(eventUuid);
//        e = eventService.loadEventWithProfiles(eventUuid);
//        logger.info("Active licensed event - " + e);
//
//        Product p1 = new Product();
//        p1.setCategory(ProductCategory.TICKET);
//        p1.setName("Product name");
//        p1.setPrice(new BigDecimal(1200));
//        p1.setUuid(UuidProvider.getUuid());
//        productService.updateEventAddProduct(e, p1);
//
//         User profile
//        String userUuid = UuidProvider.getUuid();
//
//        PaymentProfileRevision userProfile = new PaymentProfileRevision();
//        userProfile.setCity("City");
//        userProfile.setCountry(CountryEnum.GABON);
//        userProfile.setName("Name");
//        userProfile.setRegNo("123");
//        userProfile.setStreet("Street");
//        userProfile.setVatNo("123");
//        userProfile.setZip("123");
//        userService.updateUserAddNewProfile(userUuid, userProfile);
//
//        ProductOrder p1Order = new ProductOrder(p1.getUuid(), 0);
//
//        Invoice invoice1 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(p1Order), userUuid, userProfile.getParent().getUuid());
//        Invoice invoice2 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(p1Order), userUuid, userProfile.getParent().getUuid());
//        Invoice invoice3 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(p1Order), userUuid, userProfile.getParent().getUuid());
//        Invoice invoice4 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(p1Order), userUuid, userProfile.getParent().getUuid());
//        Invoice invoice5 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(p1Order), userUuid, userProfile.getParent().getUuid());
//        
//
//         Test na nacitani transferu pro aktualny event
//         Result: Transfer sa vytvori ale nic neobsahuje
//        logger.info("Testing - load actual event transfer with nothing in it");
//        Transfer t = transferService.loadActualTransfer(e.getUuid());
//        assertEquals(t.getFeesSum(), BigDecimal.ZERO);
//        assertEquals(t.getTotalSum(), BigDecimal.ZERO);
//        assertEquals(t.getCommissionSum(), BigDecimal.ZERO);
//        assertEquals(t.getTransferDate(), null);
//        assertEquals(t.getState(), TransferState.WAIT);
//        assertEquals(t.getCommissionInvoice(), null);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 0);
//
//
//         Test nacitani seznamu transferu
//         Result: WAIT - 1
//                 PREPARED - 0 a pod.
//        logger.info("Testing - load event transfers as empty ");
//        assertEquals(transferService.listTransfers(TransferState.WAIT, 0, 20).size(), 1);
//        assertEquals(transferService.listTransfers(TransferState.PREPARED, 0, 20).size(), 0);
//        assertEquals(transferService.listTransfers(TransferState.TRANSFERED, 0, 20).size(), 0);
//        assertEquals(transferService.listTransfers(TransferState.ERR, 0, 20).size(), 0);
//
//         Zaplatime produkt jeden. Nacitame aktualny transfer a bude obsahovat jeden zadnam a spravne hodnoty
//         Result: Transfer sa vytvori ale nic neobsahuje
//        logger.info("Testing - pay for one order");
//        invoiceService.updateOrderToPaid(invoice1.getUuid(), "T1", new BigDecimal(1200), BigDecimal.ZERO, DateProvider.getCurrentDate(), ConfirmationChannel.BANK);
//        logger.info("Testing - test load ");
//        t = transferService.loadActualTransfer(e.getUuid());
//        assertEquals(t.getFeesSum().compareTo(BigDecimal.ZERO), 0);
//        assertEquals(t.getTotalSum().compareTo(new BigDecimal(1200)), 0);
//        // z 1200 = 25 + 1200*0.025
//        assertEquals(t.getCommissionSum().compareTo(new BigDecimal(55)), 0, "Commission 55 ale " + t.getCommissionSum().toPlainString());
//        assertEquals(t.getTransferDate(), null);
//        assertEquals(t.getState(), TransferState.WAIT);
//        assertEquals(t.getCommissionInvoice(), null);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 1);
//
//         Zaplatime dalsi objednavky ale cez paypal, takze si nasimulujeme fees
//         Result: Transfer obsahuje sprane hodnoty
//        logger.info("Testing - pay for next order with fee");
//        invoiceService.updateOrderToPaid(invoice2.getUuid(), "asd", new BigDecimal(1185), new BigDecimal(15), DateProvider.getCurrentDate(), ConfirmationChannel.PAY_PAL);
//        t = transferService.loadActualTransfer(e.getUuid());
//        assertEquals(t.getFeesSum().compareTo(new BigDecimal(15)), 0);
//        assertEquals(t.getTotalSum().compareTo(new BigDecimal(2400)), 0);
//        //  (25 + 1200*0.025) * 2 + 15kc fee
//        assertEquals(t.getCommissionSum().compareTo(new BigDecimal(25 * 2 + 30 * 2)), 0);
//        assertEquals(t.getTransferDate(), null);
//        assertEquals(t.getState(), TransferState.WAIT);
//        assertEquals(t.getCommissionInvoice(), null);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 2);
//
//
//         Zaplatime za dalsi objednavky
//         Result: Transfer obsahuje sprane hodnoty
//        logger.info("Testing - pay for next order with fee");
//        invoiceService.updateOrderToPaid(invoice3.getUuid(), "3", new BigDecimal(1185), new BigDecimal(15), DateProvider.getCurrentDate(), ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice4.getUuid(), "4", new BigDecimal(1185), new BigDecimal(15), DateProvider.getCurrentDate(), ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice5.getUuid(), "5", new BigDecimal(1185), new BigDecimal(15), DateProvider.getCurrentDate(), ConfirmationChannel.PAY_PAL);
//        t = transferService.loadActualTransfer(e.getUuid());
//        assertEquals(t.getFeesSum().compareTo(new BigDecimal(15 * 4)), 0);
//        assertEquals(t.getTotalSum().compareTo(new BigDecimal(1200 * 5)), 0);
//        assertEquals(t.getCommissionSum().compareTo(new BigDecimal(25 * 5 + 30 * 5)), 0);
//        assertEquals(t.getTransferDate(), null);
//        assertEquals(t.getState(), TransferState.WAIT);
//        assertEquals(t.getCommissionInvoice(), null);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 5);
    }

    /**     
     * Test na vytvoreni transferu, vygenerovani spravnych hodnot na fakture a
     * hodnoty prevedenych prostredku:
     * PayPal: 2.3 CZK
     * Vat: 20%
     * Produkty:
     * ---------
     *  P1 : 1200 CZK
     *  P2 : 800  CZK
     *  P3 : 400  CZK
     *
     * Zakupene produkty:
     * -----------------
     * 1] 3 x P1 = 3600
     * 2] 1 x P1 (50%) = 600
     * 3] 2 x P2  = 1600
     * 4] 2 x P2 (100%) = 0
     * 5] 10 x P3 = 4 000
     *
     * Total: 9800 ,
     * PayPal fees = 16 x  2.3
     * Commission: 16 x 25 + 245
     * 
     *
     */
    @Test(groups = {"service"})
    public void testTransferPreparedTestAndInvoice() throws RequiredPropertyException, IbanFormatException, NoRecordException, PropertyReadOnlyException, EventLicenseNotPayedException, EventAlreadyActiveException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, EventNotActiveException, OrderAlreadyPayedException, OrderAlreadyCanceledException, ProductIncompatibilityException {

//        String eventUuid = UuidProvider.getUuid();
//        logger.info("------------ Preparing Event UUID " + eventUuid);
//         Event profile
//        PaymentProfileRevision profileRev = new PaymentProfileRevision();
//        profileRev.setAccountBankSwift("123");
//        profileRev.setAccountIban("BE68539007547034");
//        profileRev.setCity("City");
//        profileRev.setCountry(CountryEnum.CHRISTMAS_ISLAND);
//        profileRev.setName("Name");
//        profileRev.setRegNo("123");
//        profileRev.setStreet("Street");
//        profileRev.setVatNo("123");
//        profileRev.setZip("123");
//        eventService.updateEventAddNewProfile(eventUuid, profileRev);
//        logger.info("------------ Event Created ");
//        Event e = eventService.loadEventWithProfiles(eventUuid);
//        eventService.updateEventVatType(eventUuid, InvoiceProcessType.VAT_CZ);
//        eventService.updateEventCurrency(eventUuid, Currency.CZK);
//        eventService.updateEventVatRate(eventUuid, new BigDecimal(20));
//        eventService.buyLicense(eventUuid, profileRev.getParent().getUuid());
//        invoiceService.updateOrderToPaid(eventUuid, "123", new BigDecimal(500), BigDecimal.ZERO, DateProvider.getCurrentDate(), ConfirmationChannel.BANK);
//        eventService.updateEventActivateNow(eventUuid);
//        e = eventService.loadEventWithProfiles(eventUuid);
//        logger.info("------------ Event Activated ");
//
//        logger.info("------------ Creating P1 : 1200 ");
//        Product p1 = new Product();
//        p1.setCategory(ProductCategory.TICKET);
//        p1.setName("P1");
//        p1.setPrice(new BigDecimal(1200));
//        p1.setUuid(UuidProvider.getUuid());
//        productService.updateEventAddProduct(e, p1);
//
//        logger.info("------------ Creating P2 : 800 ");
//        Product p2 = new Product();
//        p2.setCategory(ProductCategory.TICKET);
//        p2.setName("P2");
//        p2.setPrice(new BigDecimal(800));
//        p2.setUuid(UuidProvider.getUuid());
//        productService.updateEventAddProduct(e, p2);
//
//        logger.info("------------ Creating P3 : 400 ");
//        Product p3 = new Product();
//        p3.setCategory(ProductCategory.TICKET);
//        p3.setName("P3");
//        p3.setPrice(new BigDecimal(400));
//        p3.setUuid(UuidProvider.getUuid());
//        productService.updateEventAddProduct(e, p3);
//
//        logger.info("------------ Creating 10 Users and ordering ticekts");
//         User profile 1
//        String user1 = UuidProvider.getUuid();
//        PaymentProfileRevision user1P = generateUserProfile();
//        userService.updateUserAddNewProfile(user1, user1P);
//         User profile 2
//        String user2 = UuidProvider.getUuid();
//        PaymentProfileRevision user2P = generateUserProfile();
//        userService.updateUserAddNewProfile(user2, user2P);
//         User profile 1
//        String user3 = UuidProvider.getUuid();
//        PaymentProfileRevision user3P = generateUserProfile();
//        userService.updateUserAddNewProfile(user3, user3P);
//         User profile 1
//        String user4 = UuidProvider.getUuid();
//        PaymentProfileRevision user4P = generateUserProfile();
//        userService.updateUserAddNewProfile(user4, user4P);
//         User profile 1
//        String user5 = UuidProvider.getUuid();
//        PaymentProfileRevision user5P = generateUserProfile();
//        userService.updateUserAddNewProfile(user5, user5P);
//         User profile 1
//        String user6 = UuidProvider.getUuid();
//        PaymentProfileRevision user6P = generateUserProfile();
//        userService.updateUserAddNewProfile(user6, user6P);
//         User profile 1
//        String user7 = UuidProvider.getUuid();
//        PaymentProfileRevision user7P = generateUserProfile();
//        userService.updateUserAddNewProfile(user7, user7P);
//         User profile 1
//        String user8 = UuidProvider.getUuid();
//        PaymentProfileRevision user8P = generateUserProfile();
//        userService.updateUserAddNewProfile(user8, user8P);
//         User profile 1
//        String user9 = UuidProvider.getUuid();
//        PaymentProfileRevision user9P = generateUserProfile();
//        userService.updateUserAddNewProfile(user9, user9P);
//
//
//
//        Invoice invoice1 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user1, user1P.getParent().getUuid());
//        Invoice invoice2 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p1.getUuid(), 0)), user2, user2P.getParent().getUuid());
//        Invoice invoice3 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user3, user3P.getParent().getUuid());
//        Invoice invoice4 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p2.getUuid(), 0)), user4, user4P.getParent().getUuid());
//        Invoice invoice5 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p2.getUuid(), 100)), user5, user5P.getParent().getUuid());
//        Invoice invoice6 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user6, user6P.getParent().getUuid());
//        Invoice invoice7 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p2.getUuid(), 0)), user7, user7P.getParent().getUuid());
//        Invoice invoice8 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user8, user8P.getParent().getUuid());
//        Invoice invoice9 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user9, user9P.getParent().getUuid());
//        Invoice invoice10 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user1, user1P.getParent().getUuid());
//        Invoice invoice11 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p2.getUuid(), 100)), user2, user2P.getParent().getUuid());
//        Invoice invoice12 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user3, user3P.getParent().getUuid());
//        Invoice invoice13 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user4, user4P.getParent().getUuid());
//        Invoice invoice14 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p1.getUuid(), 0)), user5, user5P.getParent().getUuid());
//        Invoice invoice15 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user6, user6P.getParent().getUuid());
//        Invoice invoice16 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p1.getUuid(), 50)), user7, user7P.getParent().getUuid());
//        Invoice invoice17 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p3.getUuid(), 0)), user8, user8P.getParent().getUuid());
//        Invoice invoice18 = invoiceService.createOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(p1.getUuid(), 0)), user9, user9P.getParent().getUuid());
//
//
//
//        // Test na nacitani transferu pro aktualny event
//        // Result: Transfer sa vytvori ale nic neobsahuje
//        logger.info("Testing - load actual event transfer with nothing in it");
//        Transfer t = transferService.loadActualTransfer(e.getUuid());
//        assertEquals(t.getFeesSum(), BigDecimal.ZERO);
//        assertEquals(t.getTotalSum(), BigDecimal.ZERO);
//        assertEquals(t.getCommissionSum(), BigDecimal.ZERO);
//        assertEquals(t.getTransferDate(), null);
//        assertEquals(t.getState(), TransferState.WAIT);
//        assertEquals(t.getCommissionInvoice(), null);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 2); //-- zaplatene uz tym ze su 100
//
//        BigDecimal PP = new BigDecimal("2.3");
//        Date now = DateProvider.getCurrentDate();
//        // Zaplatime produkt jeden. Nacitame aktualny transfer a bude obsahovat jeden zadnam a spravne hodnoty
//        // Result: Transfer sa vytvori ale nic neobsahuje
//        logger.info("------------ Paying for all tickets");
//        invoiceService.updateOrderToPaid(invoice1.getUuid(),   "T1", new BigDecimal("397.7"),  PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice2.getUuid(),   "T2", new BigDecimal("1197.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice3.getUuid(),   "T3", new BigDecimal("397.7"),  PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice4.getUuid(),   "T4", new BigDecimal("797.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice6.getUuid(),   "T6", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice7.getUuid(),   "T7", new BigDecimal("797.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice8.getUuid(),   "T8", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice9.getUuid(),   "T9", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice10.getUuid(), "T10", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice12.getUuid(), "T12", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice13.getUuid(), "T13", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice14.getUuid(), "T14", new BigDecimal("1197.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice15.getUuid(), "T15", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice16.getUuid(), "T16", new BigDecimal("597.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice17.getUuid(), "T17", new BigDecimal("397.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        invoiceService.updateOrderToPaid(invoice18.getUuid(), "T18", new BigDecimal("1197.7"), PP, now, ConfirmationChannel.PAY_PAL);
//        // Prevedeme transfer
//        logger.info("Testing - Transfer in wait state ");
//        t = transferService.loadActualTransfer(e.getUuid());
//        assertEquals(t.getFeesSum().compareTo(new BigDecimal("36.8")), 0, "Fees: "+t.getFeesSum().toPlainString());
//        assertEquals(t.getTotalSum().compareTo(new BigDecimal("9800")), 0, "Total: "+t.getTotalSum().toPlainString());
//        assertEquals(t.getCommissionSum().compareTo(new BigDecimal("645")), 0, "Commission : "+t.getCommissionSum().toPlainString());
//        assertEquals(t.getTransferDate(), null);
//        assertEquals(t.getState(), TransferState.WAIT);
//        assertEquals(t.getCommissionInvoice(), null);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 18);
//
//        logger.info("------------ Transfer to PREPERAD STATE");
//        transferService.updateActiveTransferToPreparedState(eventUuid);
//        logger.info("Testing - Transfer - should be empty ");
//        t = transferService.loadActualTransfer(e.getUuid());
//        assertEquals(t.getFeesSum(), BigDecimal.ZERO);
//        assertEquals(t.getTotalSum(), BigDecimal.ZERO);
//        assertEquals(t.getCommissionSum(), BigDecimal.ZERO);
//        assertEquals(t.getTransferDate(), null);
//        assertEquals(t.getState(), TransferState.WAIT);
//        assertEquals(t.getCommissionInvoice(), null);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 0);
//        logger.info("Testing - prepared transfer ");
//        t = transferService.loadPreparedTransfer(eventUuid);
//        assertEquals(t.getFeesSum().compareTo(new BigDecimal("36.8")), 0, "Fees: "+t.getFeesSum().toPlainString());
//        assertEquals(t.getTotalSum().compareTo(new BigDecimal("9800")), 0, "Total: "+t.getTotalSum().toPlainString());
//        assertEquals(t.getCommissionSum().compareTo(new BigDecimal("645")), 0, "Commission : "+t.getCommissionSum().toPlainString());        
//        assertEquals(t.getState(), TransferState.PREPARED);
//        assertEquals(t.getEvent(), e);
//        assertEquals(t.getInvoices().size(), 18);
//        assertNotNull(t.getCommissionInvoice());
//        assertNotNull(t.getTransferDate());
//         6 polozek na fakture
//        logger.info("Testing - prepared transfer invoice");
//        Invoice invoice = t.getCommissionInvoice();
//        e = eventService.loadEvent(eventUuid);
//        assertEquals(invoice.getCurrency(), e.getCurrency());
//        assertEquals(invoice.getFeesSum(), BigDecimal.ZERO);
//        assertEquals(invoice.getInvoiceNumber(), "16"+e.getInvoiceNumber()+"0001");
//        assertEquals(invoice.getItems().size(), 6);
//        InvoiceRecord record = new InvoiceRecord(invoice);
//        for(InvoiceItemRecord item : record.getRecords()){
//            logger.info(item);
//        }
//
//         pokusime sa nacitat
//        logger.info("Testing - load event transfers - should be 2");
//        List<Transfer> transfers = transferService.listTransfers(eventUuid, 0, 10);
//        assertEquals(transfers.size(), 2);
//         prvym musi byt prave aktualny
//        assertEquals(transfers.get(0), transferService.loadActualTransfer(eventUuid));
//        assertEquals(transferService.loadAllTransfersCount(eventUuid), new Long(2));
//        logger.info("Testing - load event empty transfer detail");
//        t = transferService.loadTransferDetail(eventUuid, transfers.get(0).getUuid());
//        assertEquals(t, transfers.get(0));
//
//        logger.info("Testing - load event prepared transfer detail");
//        t = transferService.loadTransferDetail(eventUuid, transfers.get(1).getUuid());
//        assertEquals(t, transfers.get(1));

        

        



    }

    private PaymentProfileRevision generateUserProfile() {
        PaymentProfileRevision userProfile = new PaymentProfileRevision();
        userProfile.setCity("City");
        userProfile.setCountry(CountryEnum.GABON);
        userProfile.setName("Name");
        userProfile.setRegNo("123");
        userProfile.setStreet("Street");
        userProfile.setVatNo("123");
        userProfile.setZip("123");
        return userProfile;
    }
}
