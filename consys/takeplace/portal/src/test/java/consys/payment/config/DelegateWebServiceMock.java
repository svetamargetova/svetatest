package consys.payment.config;


import consys.payment.service.DelegateWebService;
import consys.payment.ws.delegate.*;
/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DelegateWebServiceMock implements DelegateWebService{



    @Override
    public DelegateOrderStateChangedResponse delegateEventOrderStateChanged(DelegateOrderStateChangedRequest request) {
        DelegateOrderStateChangedResponse r = OBJECT_FACTORY.createDelegateOrderStateChangedResponse();
        r.setProcessed(true);
        return r;
    }

    @Override
    public DelegateOrderStateChangedResponse delegateAdminOrderStateChanged(DelegateOrderStateChangedRequest request) {
        DelegateOrderStateChangedResponse r = OBJECT_FACTORY.createDelegateOrderStateChangedResponse();
        r.setProcessed(true);
        return r;
    }

}
