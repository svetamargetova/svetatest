/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.data;

import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.common.utils.enums.CountryEnum;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Order;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.Product;
import consys.payment.bo.Transaction;
import consys.payment.bo.User;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.common.utils.enums.Currency;
import consys.payment.bo.enums.ProductCategory;
import consys.payment.bo.thumb.ProductOrder;
import consys.payment.config.AbstractServiceTest;
import consys.payment.dao.hibernate.OrderDaoImpl;
import consys.payment.service.AcemceeService;
import consys.payment.service.EventService;
import consys.payment.service.OrderService;
import consys.payment.service.PaymentProfileService;
import consys.payment.service.ProductService;
import consys.payment.service.TransactionService;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.EventProfileNotFilledException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.NoRecordException;
import consys.payment.service.exception.OrderAlreadyCanceledException;
import consys.payment.service.exception.OrderAlreadyPayedException;

import consys.payment.service.exception.ProductIncompatibilityException;
import consys.payment.service.exception.RequiredPropertyException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.testng.annotations.Test;

/**
 *
 * @author palo
 */
@TransactionConfiguration(defaultRollback = false)
public class GenerateTestData extends AbstractServiceTest {

    private static final int EVENTS = 100;
    private static final int MAX_PRODUCTS_PER_EVENT = 20;
    private static final int MAX_PRODUCT_PRICE = 5000;
    private static final int MAX_ORDERS_PER_EVENT = 10;
    @Autowired
    private EventService eventService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private PaymentProfileService profileService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private OrderDaoImpl orderDaoImpl;
    @Autowired
    private AcemceeService acemceeService;
    List<EventPaymentProfile> activeEvents;
    List<User> users;
    private static final Random R = new Random();

    @Test(groups = {"data"})
    public void generateData() throws RequiredPropertyException, NoRecordException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, IbanFormatException, OrderAlreadyPayedException, OrderAlreadyCanceledException, EventNotActiveException, ProductIncompatibilityException {
        BigDecimal vat = new BigDecimal(20);
        for (int i = 0; i < EVENTS; i++) {
            Event event = eventService.createEvent(UuidProvider.getUuid(), CountryEnum.CZECH_REPUBLIC);

            EventPaymentProfile epp = eventService.loadEventDefaultPaymentProfileDetailed(event.getUuid());
            PaymentProfileRevision e1pp = new PaymentProfileRevision();
            e1pp.setAccountBankSwift("123");
            e1pp.setAccountIban("BE68539007547034");
            e1pp.setCity("City");
            e1pp.setCountry(CountryEnum.CZECH_REPUBLIC);
            e1pp.setName("Name");
            e1pp.setRegNo("123");
            e1pp.setStreet("Street");
            e1pp.setVatNo("123");
            e1pp.setZip("123");
            Order order = orderService.createLicenseOrder(event.getUuid(), epp.getUuid(), e1pp);
            order = orderService.loadOrderByUuid(event.getUuid());
            // potvrdena
            boolean confirmed = false;
            if (R(100) % 2 == 0) {
                confirmed = true;
                switch (R(2)) {
                    case 0:
                        Transaction t = transaction(12500);
                        orderService.updateOrderToConfirmed(order, t);
                        break;
                    case 1:
                        orderService.updateOrderToConfirmedByAdministrator(order, false, false,null);
                        break;
                    default:
                        break;
                }
            }

            List<Product> products = Lists.newArrayListWithCapacity(MAX_PRODUCTS_PER_EVENT);
            for (int j = 0; j < R(MAX_PRODUCTS_PER_EVENT); j++) {
                Product product = new Product();
                product.setCategory(ProductCategory.TICKET);
                product.setName(String.format("%s - Product name %s", event.getUuid(), j));
                product.setVatRate(vat);
                product.setPrice(new BigDecimal(R(MAX_PRODUCT_PRICE)));
                product.setUuid(UuidProvider.getUuid());
                productService.createEventProduct(epp, product);
                products.add(product);
            }

            if (confirmed && !products.isEmpty()) {
                for (int j = 0; j < R(MAX_ORDERS_PER_EVENT); j++) {
                    String userUuid = UuidProvider.getUuid();
                    PaymentProfileRevision userProfile = new PaymentProfileRevision();
                    userProfile.setCity("City");
                    userProfile.setCountry(CountryEnum.CZECH_REPUBLIC);
                    userProfile.setName("Name");
                    userProfile.setStreet("Street");
                    userProfile.setVatNo("123");
                    userProfile.setZip("123");

                    List<ProductOrder> orders = Lists.newArrayList();
                    // 5 polozek na obednavku
                    for (int m = 0; m < 5; m++) {
                        int quantity = R(5);
                        int discount = 0;
                        if (R(100) % 2 == 0) {
                            discount = R(100);
                        }
                        Product p = products.get(R(products.size()));
                        orders.add(new ProductOrder(p.getUuid(), quantity, discount));
                    }


                    Order order1 = orderService.createUserOrder(UuidProvider.getUuid(), orders, userUuid, null, userProfile, epp);


                    // zaplatime

                    if (R(100) % 2 == 0) {
                        switch (R(4)) {
                            case 0:
                                Transaction t = transaction(order1.getPrice(), R(order.getPrice().intValue()));
                                orderService.updateOrderToConfirmed(order1, t);
                                break;
                            case 1:
                                orderService.updateOrderToConfirmedByAdministrator(order1, false,false, null);
                                break;
                            case 2:
                                orderService.updateOrderToConfirmedByOrganizator(order1, userUuid, null);
                                break;
                            case 3:
                                Transaction t2 = transaction(new BigDecimal(R(order1.getPrice().intValue())), R(order.getPrice().intValue()));
                                orderService.updateOrderToConfirmed(order1, t2);
                                break;
                            default:
                                break;
                        }
                    }


                }
                orderDaoImpl.getSessionFactory().getCurrentSession().getTransaction().commit();
                orderDaoImpl.getSessionFactory().getCurrentSession().getTransaction().begin();
            }

        }
    }

    public int R(int n) {
        return R.nextInt(n);
    }

    public Transaction transaction(int value) throws RequiredPropertyException {
        return transaction(new BigDecimal(value), R(value));
    }

    public Transaction transaction(BigDecimal value, int fees) throws RequiredPropertyException {
        Transaction transaction = new Transaction();
        transaction.setAmount(value);
        transaction.setCurrency(Currency.CZK);
        transaction.setFees(new BigDecimal(fees));
        transaction.setTransactionDate(DateProvider.getCurrentDate());
        transaction.setConfirmationChannel(ConfirmationChannel.ONLINE_PAY_PAL);
        transactionService.createTransaction(transaction);
        return transaction;

    }
}
