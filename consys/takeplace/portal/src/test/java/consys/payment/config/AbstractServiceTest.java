package consys.payment.config;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@TestExecutionListeners(inheritListeners=true,value = {LoggingListener.class,TransactionalTestExecutionListener.class})
@ContextConfiguration(locations={"spring-context.xml","spring-hibernate-test.xml","spring-delegate-mock-service.xml"})
@Transactional
public abstract class AbstractServiceTest extends AbstractTestNGSpringContextTests{

    






}
