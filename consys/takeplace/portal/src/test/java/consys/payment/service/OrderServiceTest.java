/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service;

import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.payment.bo.Order;
import consys.payment.service.exception.EventAlreadyActiveException;
import consys.payment.service.exception.EventLicenseNotPayedException;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.OrderAlreadyCanceledException;
import consys.payment.service.exception.OrderAlreadyPayedException;

import consys.payment.service.exception.ProductIncompatibilityException;
import consys.payment.service.exception.PropertyReadOnlyException;
import consys.common.utils.enums.CountryEnum;
import consys.payment.bo.ApproveOrder;
import consys.payment.service.exception.EventLicenseAlreadyPurchased;
import consys.payment.service.exception.NoRecordException;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.Invoice;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.Product;
import consys.payment.bo.Transaction;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.common.utils.enums.Currency;
import consys.payment.SystemProperties;
import consys.payment.bo.*;
import consys.payment.bo.enums.OrderFilterEnum;
import consys.payment.bo.enums.OrderState;
import consys.payment.bo.enums.ProductCategory;
import consys.payment.bo.thumb.ProductOrder;
import consys.payment.channel.bank.BankTransaction;
import consys.payment.config.AbstractServiceTest;
import consys.payment.dao.hibernate.OrderDaoImpl;
import consys.payment.service.exception.EventProfileNotFilledException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.RequiredPropertyException;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OrderServiceTest extends AbstractServiceTest {

    @Autowired
    private EventService eventService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private PaymentProfileService profileService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private OrderDaoImpl orderDaoImpl;
    @Autowired
    private AcemceeService acemceeService;
    @Autowired
    private TransferService transferService;
    
    private Event event1, event2;
    private Product e1p1, e1p2, e1p3, e1p4;

    public void prepareData()
            throws RequiredPropertyException, NoRecordException, EventLicenseAlreadyPurchased, EventProfileNotFilledException, IbanFormatException, EventLicenseNotPayedException, EventAlreadyActiveException, OrderAlreadyPayedException, OrderAlreadyCanceledException {
        // dva eventy
        event1 = eventService.createEvent(UuidProvider.getUuid(), CountryEnum.CZECH_REPUBLIC);
        event2 = eventService.createEvent(UuidProvider.getUuid(), CountryEnum.SLOVAKIA);

        EventPaymentProfile epp = eventService.loadEventDefaultPaymentProfileDetailed(event1.getUuid());
        PaymentProfileRevision e1pp = new PaymentProfileRevision();
        e1pp.setAccountBankSwift("123");
        e1pp.setAccountIban("BE68539007547034");
        e1pp.setCity("City");
        e1pp.setCountry(CountryEnum.CZECH_REPUBLIC);
        e1pp.setName("Name");
        e1pp.setRegNo("123");
        e1pp.setStreet("Street");
        e1pp.setVatNo("123");
        e1pp.setZip("123");

        try {
            orderService.createLicenseOrder(event1.getUuid(), epp.getUuid());
            fail();
        } catch (EventProfileNotFilledException epnfe) {
        }

        assertFalse(profileService.isEventPaymentProfileUsedInActiveOrder(epp.getUuid()));
        eventService.updateEventPaymentProfileRevision(epp, e1pp);
        eventService.updateEventActivateNow(event1, "123");
        
        /* 
         OBJEDNAVKA LICENCIE NIE JE NUTNA NADALEJ 
        Order order = orderService.createLicenseOrder(event1.getUuid(), epp.getUuid(), e1pp);


        Order lorder = orderService.loadOrderByUuid(event1.getUuid());
        assertEquals(lorder.getState(), OrderState.ACTIVE);
        assertEquals(lorder.getItems().size(), 1);
        assertEquals(lorder.getOrderType(), ProductCategory.LICENSE);
        assertEquals(lorder.getPrice().compareTo(new BigDecimal(12500)), 0);                

        event1 = eventService.loadEvent(event1.getUuid());
        orderService.updateOrderToConfirmedByAdministrator(lorder, false, null);
        lorder = orderService.loadOrderByUuid(event1.getUuid());
        assertEquals(lorder.getState(), OrderState.CONFIRMED_SYSTEM);
         **/

        // 

        e1p1 = new Product();
        e1p1.setCategory(ProductCategory.TICKET);
        e1p1.setName("Product name 1");
        e1p1.setPrice(new BigDecimal(1000));
        e1p1.setVatRate(BigDecimal.ZERO);
        e1p1.setUuid(UuidProvider.getUuid());
        productService.createEventProduct(epp, e1p1);


        e1p2 = new Product();
        e1p2.setCategory(ProductCategory.TICKET);
        e1p2.setName("Product name 2");
        e1p2.setVatRate(BigDecimal.ZERO);
        e1p2.setPrice(new BigDecimal(1400));
        e1p2.setUuid(UuidProvider.getUuid());
        productService.createEventProduct(epp, e1p2);

        e1p3 = new Product();
        e1p3.setCategory(ProductCategory.TICKET);
        e1p3.setName("Product name 3 - FREE");
        e1p3.setVatRate(BigDecimal.ZERO);
        e1p3.setPrice(BigDecimal.ZERO);
        e1p3.setUuid(UuidProvider.getUuid());
        productService.createEventProduct(epp, e1p3);

        e1p4 = new Product();
        e1p4.setCategory(ProductCategory.THIRD_PARTY_FEE);
        e1p4.setName("Product name 4 ");
        e1p4.setVatRate(BigDecimal.ZERO);
        e1p4.setPrice(new BigDecimal(12500));
        e1p4.setUuid(UuidProvider.getUuid());
        productService.createEventProduct(epp, e1p4);

        // kupime si licenciu na event 2

        epp = eventService.loadEventDefaultPaymentProfileDetailed(event2.getUuid());
        e1pp = new PaymentProfileRevision();
        e1pp.setAccountBankSwift("123");
        e1pp.setAccountIban("BE68539007547034");
        e1pp.setCity("City");
        e1pp.setCountry(CountryEnum.CZECH_REPUBLIC);
        e1pp.setName("Name");
        e1pp.setRegNo("123");
        e1pp.setStreet("Street");
        e1pp.setVatNo("123");
        e1pp.setZip("123");
        eventService.updateEventPaymentProfileRevision(epp, e1pp);
        eventService.updateEventActivateNow(event2, "1234");
        
        /*
        order = orderService.createLicenseOrder(event2.getUuid(), epp.getUuid(), e1pp);

        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(500));
        transaction.setCurrency(Currency.EUR);
        transaction.setTransactionDate(DateProvider.getCurrentDate());
        transaction.setConfirmationChannel(ConfirmationChannel.ONLINE_PAY_PAL);
        transactionService.createTransaction(transaction);

        orderService.updateOrderToConfirmed(order, transaction);         
         */

    }

    @Test(groups = {"service"})
    public void testCreateUserOrderAndPayForIt() throws RequiredPropertyException, IbanFormatException, NoRecordException, PropertyReadOnlyException, EventLicenseNotPayedException, EventAlreadyActiveException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, EventNotActiveException, OrderAlreadyPayedException, OrderAlreadyCanceledException, ProductIncompatibilityException {
        prepareData();


        EventPaymentProfile e1pp = eventService.loadEventDefaultPaymentProfileDetailed(event1.getUuid());

        String userUuid = UuidProvider.getUuid();
        PaymentProfileRevision userProfile = new PaymentProfileRevision();
        userProfile.setCity("City");
        userProfile.setCountry(CountryEnum.CZECH_REPUBLIC);
        userProfile.setName("Name");
        userProfile.setStreet("Street");
        userProfile.setVatNo("123");
        userProfile.setZip("123");

        try {
            orderService.createUserOrder(userUuid, Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 0)), userUuid, e1pp);
            fail();
        } catch (NoRecordException e) {
        }

        Order order = orderService.createUserOrder(userUuid, Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 0)), userUuid, null, userProfile, e1pp);
        order = orderService.loadOrderByNumber(order.getOrderNumber());
        assertEquals(order.getCustomer().getCountry(), userProfile.getCountry());
        assertEquals(order.getCustomer().getCity(), userProfile.getCity());
        assertEquals(order.getCustomer().getName(), userProfile.getName());
        assertEquals(order.getCustomer().getStreet(), userProfile.getStreet());
        assertEquals(order.getOrderType(), ProductCategory.TICKET);
        assertEquals(order.getPrice().compareTo(e1p1.getPrice()), 0);
        assertEquals(order.getState(), OrderState.ACTIVE);

        assertTrue(profileService.isUserPaymentProfileUsedInActiveOrder(userProfile.getParent().getUuid()));
        assertTrue(profileService.isEventPaymentProfileUsedInActiveOrder(e1pp.getUuid()));

        orderService.cancelOrder(order.getUuid(), OrderState.CANCELED_OWNER, null,false);

        assertFalse(profileService.isUserPaymentProfileUsedInActiveOrder(userProfile.getParent().getUuid()));
        assertFalse(profileService.isEventPaymentProfileUsedInActiveOrder(e1pp.getUuid()));

        Order zeroOrder = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p3.getUuid(), 1, 0)), userUuid, e1pp);
        assertEquals(zeroOrder.getPrice().compareTo(BigDecimal.ZERO), 0);
        assertEquals(zeroOrder.getState(), OrderState.CONFIRMED_ZERO);


        orderService.cancelOrder(zeroOrder.getUuid(), OrderState.CANCELED_ORGANIZATOR, "Canceled canceled",false);


        order = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 100), new ProductOrder(e1p1.getUuid(), 3, 25)), userUuid, userProfile.getParent().getUuid(), e1pp);
        order = orderService.loadOrderByUuid(order.getUuid());
        assertEquals(order.getPrice().compareTo(new BigDecimal(2250)), 0, order.getPrice().toPlainString());
        assertEquals(order.getState(), OrderState.ACTIVE);
        assertEquals(order.getItems().size(), 2);


        orderService.updateOrderToConfirmedByAdministrator(order, true, false, "test");
        order = orderService.loadOrderByNumber(order.getOrderNumber());

        assertEquals(order.getPrice().compareTo(new BigDecimal(2250)), 0, order.getPrice().toPlainString());
        assertEquals(order.getUnitFee().compareTo(new BigDecimal(75)), 0, order.getUnitFee().toPlainString());
        assertEquals(order.getShareFee().compareTo(new BigDecimal("65.25")), 0, order.getShareFee().toPlainString());
        assertEquals(order.getState(), OrderState.CONFIRMED_SYSTEM);
        assertEquals(order.getItems().size(), 2);

        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(2250));
        transaction.setFees(new BigDecimal(32));
        transaction.setCurrency(Currency.CZK);
        transaction.setTransactionDate(DateProvider.getCurrentDate());
        transaction.setConfirmationChannel(ConfirmationChannel.ONLINE_PAY_PAL);
        transactionService.createTransaction(transaction);


        //
        order = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 100), new ProductOrder(e1p1.getUuid(), 3, 25)), userUuid, userProfile.getParent().getUuid(), e1pp);
        order = orderService.loadOrderByUuid(order.getUuid());
        assertEquals(order.getItems().size(), 2);


        orderDaoImpl.getSessionFactory().getCache().evictEntityRegions();
        orderDaoImpl.getSessionFactory().getCurrentSession().flush();

        orderService.updateOrderToConfirmed(order, transaction);
        order = orderService.loadOrderByNumber(order.getOrderNumber());
        assertEquals(order.getUnitFee().compareTo(new BigDecimal(75)), 0, order.getUnitFee().toPlainString());
        assertEquals(order.getShareFee().compareTo(new BigDecimal("65.25")), 0, order.getShareFee().toPlainString());
        assertEquals(order.getState(), OrderState.CONFIRMED_SYSTEM);
        
        Transfer transfer = transferService.loadActualTransfer(e1pp.getUuid());
        assertEquals(transfer.getThirdPartyFeesSum().compareTo(transaction.getFees()), 0, transfer.getThirdPartyFeesSum().toPlainString());
        assertEquals(transfer.getFeesSum().compareTo(new BigDecimal("280.5")), 0, transfer.getFeesSum().toPlainString());        
        
                
        // refundujeme
        orderService.cancelPaidOrder(order, "", false);
        
        order = orderService.loadOrderByNumber(order.getOrderNumber());        
        assertEquals(order.getState(), OrderState.ORDER_REFUNDED);
        
        transfer = transferService.loadActualTransfer(e1pp.getUuid());
        assertEquals(transfer.getThirdPartyFeesSum().compareTo(BigDecimal.ZERO), 0, transfer.getThirdPartyFeesSum().toPlainString());
        assertEquals(transfer.getFeesSum().compareTo(new BigDecimal("140.25")), 0, transfer.getFeesSum().toPlainString());        
        
        // Test na max order fee
        transaction = new Transaction();
        transaction.setAmount(new BigDecimal(4500));
        transaction.setFees(new BigDecimal(32));
        transaction.setCurrency(Currency.CZK);
        transaction.setTransactionDate(DateProvider.getCurrentDate());
        transaction.setConfirmationChannel(ConfirmationChannel.ONLINE_PAY_PAL);
        transactionService.createTransaction(transaction);


        //
        order = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 100), new ProductOrder(e1p1.getUuid(), 6, 25)), userUuid, userProfile.getParent().getUuid(), e1pp);
        order = orderService.loadOrderByUuid(order.getUuid());
        assertEquals(order.getItems().size(), 2);


        orderDaoImpl.getSessionFactory().getCache().evictEntityRegions();
        orderDaoImpl.getSessionFactory().getCurrentSession().flush();

        orderService.updateOrderToConfirmed(order, transaction);
        order = orderService.loadOrderByNumber(order.getOrderNumber());
        assertEquals(order.getUnitFee().compareTo(SystemProperties.ORDER_MAX_FEE_CZK), 0, order.getUnitFee().toPlainString());
        assertEquals(order.getShareFee().compareTo(BigDecimal.ZERO), 0, order.getShareFee().toPlainString());
        assertEquals(order.getState(), OrderState.CONFIRMED_SYSTEM);
        
    }

    @Test(groups = {"service"})
    public void testFeeComputationStrategies()
            throws RequiredPropertyException, IbanFormatException, NoRecordException, PropertyReadOnlyException, EventLicenseNotPayedException, EventAlreadyActiveException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, EventNotActiveException, OrderAlreadyPayedException, OrderAlreadyCanceledException, ProductIncompatibilityException {
        prepareData();

        EventPaymentProfile e1pp = eventService.loadEventDefaultPaymentProfileDetailed(event1.getUuid());

        String userUuid = UuidProvider.getUuid();
        PaymentProfileRevision userProfile = new PaymentProfileRevision();
        userProfile.setCity("City");
        userProfile.setCountry(CountryEnum.CZECH_REPUBLIC);
        userProfile.setName("Name");
        userProfile.setStreet("Street");
        userProfile.setVatNo("123");
        userProfile.setZip("123");



        Order order1 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 0), new ProductOrder(e1p4.getUuid(), 1, 0)), userUuid, null, userProfile, e1pp);
        Order order2 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 2, 0)), userUuid, userProfile.getParent().getUuid(), userProfile, e1pp);
        Order order3 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 0), new ProductOrder(e1p2.getUuid(), 1, 0)), userUuid, userProfile.getParent().getUuid(), userProfile, e1pp);


        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(10000));
        transaction.setCurrency(Currency.CZK);
        transaction.setTransactionDate(DateProvider.getCurrentDate());
        transaction.setConfirmationChannel(ConfirmationChannel.ONLINE_PAY_PAL);
        transactionService.createTransaction(transaction);


        orderService.updateOrderToConfirmed(order1, transaction);



        Order order1l = orderService.loadOrderByUuid(order1.getUuid());
        assertEquals(order1l.getState(), OrderState.WAITING_TO_APPROVE);
        assertEquals(order1l.getPrice().compareTo(new BigDecimal(13500)), 0, order1l.getPrice().toPlainString());


        transaction = new Transaction();
        transaction.setAmount(new BigDecimal(10000));
        transaction.setTransactionDate(DateProvider.getCurrentDate());
        transaction.setCurrency(Currency.CZK);
        transaction.setConfirmationChannel(ConfirmationChannel.BANK_TRANSFER);
        transactionService.createTransaction(transaction);

        orderService.updateOrderToConfirmed(order2, transaction);

        Order order2l = orderService.loadOrderByUuid(order2.getUuid());
        assertEquals(order2l.getState(), OrderState.WAITING_TO_APPROVE);
        assertEquals(order2l.getPrice().compareTo(new BigDecimal(2000)), 0, order2l.getPrice().toPlainString());


        orderService.updateOrderToConfirmedByOrganizator(order3, UuidProvider.getUuid(), "test");

        List<Order> orders = orderService.listEventOrders(e1pp.getEvent().getUuid(), OrderFilterEnum.ALL, null, null, null, null, null, 0, 10);
        assertEquals(orders.size(), 3);
        assertEquals(orderService.loadEventOrdersCount(e1pp.getEvent().getUuid(), OrderFilterEnum.ALL, null, null, null, null, null), 3);

        orders = orderService.listEventOrders(e1pp.getEvent().getUuid(), OrderFilterEnum.WAITING_TO_APPROVE, null, null, null, null, null, 0, 10);
        assertEquals(orders.size(), 2);
        assertEquals(orderService.loadEventOrdersCount(e1pp.getEvent().getUuid(), OrderFilterEnum.WAITING_TO_APPROVE, null, null, null, null, null), 2);

        orders = orderService.listEventOrders(e1pp.getEvent().getUuid(), OrderFilterEnum.ONLY_CONFIRMED, null, null, null, null, null, 0, 10);
        assertEquals(orders.size(), 1);
        assertEquals(orderService.loadEventOrdersCount(e1pp.getEvent().getUuid(), OrderFilterEnum.ONLY_CONFIRMED, null, null, null, null, null), 1);

        orders = orderService.listEventOrders(e1pp.getEvent().getUuid(), OrderFilterEnum.ONLY_CONFIRMED, null, null, null, null, order3.getOrderNumber(), 0, 10);
        assertEquals(orders.size(), 1);
        assertEquals(orderService.loadEventOrdersCount(e1pp.getEvent().getUuid(), OrderFilterEnum.ONLY_CONFIRMED, null, null, null, null, order3.getOrderNumber()), 1);

        orders = orderService.listEventOrders(e1pp.getEvent().getUuid(), OrderFilterEnum.ONLY_CONFIRMED, null, null, null, null, order3.getUuid(), 0, 10);
        assertEquals(orders.size(), 1);
        assertEquals(orderService.loadEventOrdersCount(e1pp.getEvent().getUuid(), OrderFilterEnum.ONLY_CONFIRMED, null, null, null, null, order3.getUuid()), 1);

    }

    @Test(groups = {"service"})
    public void testMultiplyOrderConfirmationByOneTransaction()
            throws RequiredPropertyException, IbanFormatException, NoRecordException, PropertyReadOnlyException, EventLicenseNotPayedException, EventAlreadyActiveException, EventProfileNotFilledException, EventLicenseAlreadyPurchased, EventNotActiveException, OrderAlreadyPayedException, OrderAlreadyCanceledException, ProductIncompatibilityException {
        prepareData();

        EventPaymentProfile e1pp = eventService.loadEventDefaultPaymentProfileDetailed(event1.getUuid());

        String userUuid = UuidProvider.getUuid();
        PaymentProfileRevision userProfile = new PaymentProfileRevision();
        userProfile.setCity("City");
        userProfile.setCountry(CountryEnum.CZECH_REPUBLIC);
        userProfile.setName("Name");
        userProfile.setStreet("Street");
        userProfile.setVatNo("123");
        userProfile.setZip("123");

        // Test kedy to projde, teda transakcia bude obsahovat presnu ciastku.
        Order order1 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 0), new ProductOrder(e1p4.getUuid(), 1, 0)), userUuid, null, userProfile, e1pp);
        Order order2 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 2, 0)), userUuid, userProfile.getParent().getUuid(), userProfile, e1pp);

        BankTransaction transaction = new BankTransaction();
        transaction.setCurrency(Currency.CZK);
        transaction.setDate(DateProvider.getCurrentDate());
        transaction.setAmount(new BigDecimal(15500));
        transaction.setReferenceId("XNFED");
        transaction.setVariableSymbol(Lists.newArrayList(order1.getOrderNumber(), order2.getOrderNumber()));
        transactionService.createBankTransactions(Lists.newArrayList(transaction));

        order1 = orderService.loadOrderByUuid(order1.getUuid());
        assertEquals(order1.getState(), OrderState.CONFIRMED_SYSTEM);
        assertNotNull(order1.getInvoice());
        assertEquals(order1.getInvoice().getConfirmationSource(), ConfirmationChannel.BANK_TRANSFER);

        order2 = orderService.loadOrderByUuid(order2.getUuid());
        assertEquals(order2.getState(), OrderState.CONFIRMED_SYSTEM);
        assertNotNull(order2.getInvoice());
        assertEquals(order2.getInvoice().getConfirmationSource(), ConfirmationChannel.BANK_TRANSFER);

        // Test na to ze jedna transakci aobsahuje dve cisla ale hodnota transakcie je mala
        Order order3 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 0), new ProductOrder(e1p2.getUuid(), 1, 0)), userUuid, userProfile.getParent().getUuid(), e1pp);
        Order order4 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 2, 0), new ProductOrder(e1p2.getUuid(), 2, 0)), userUuid, userProfile.getParent().getUuid(), e1pp);

        transaction = new BankTransaction();
        transaction.setCurrency(Currency.CZK);
        transaction.setDate(DateProvider.getCurrentDate());
        transaction.setAmount(new BigDecimal(5000));
        transaction.setReferenceId("XNFESD");
        transaction.setVariableSymbol(Lists.newArrayList(order3.getOrderNumber(), order4.getOrderNumber()));
        transactionService.createBankTransactions(Lists.newArrayList(transaction));

        order3 = orderService.loadOrderByUuid(order3.getUuid());
        assertEquals(order3.getState(), OrderState.WAITING_TO_APPROVE);
        assertNull(order3.getInvoice());

        order4 = orderService.loadOrderByUuid(order4.getUuid());
        assertEquals(order4.getState(), OrderState.WAITING_TO_APPROVE);
        assertNull(order4.getInvoice());

        // Test na to transakcie obsahuje spravne hodnoty ale ma inu menu
        Order order5 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 1, 0), new ProductOrder(e1p2.getUuid(), 1, 0)), userUuid, userProfile.getParent().getUuid(), e1pp);
        Order order6 = orderService.createUserOrder(UuidProvider.getUuid(), Lists.newArrayList(new ProductOrder(e1p1.getUuid(), 2, 0), new ProductOrder(e1p2.getUuid(), 2, 0)), userUuid, userProfile.getParent().getUuid(), e1pp);

        transaction = new BankTransaction();
        transaction.setCurrency(Currency.EUR);
        transaction.setDate(DateProvider.getCurrentDate());
        transaction.setAmount(new BigDecimal(7200));
        transaction.setReferenceId("XNFWWWD");
        transaction.setVariableSymbol(Lists.newArrayList(order5.getOrderNumber(), order6.getOrderNumber()));
        transactionService.createBankTransactions(Lists.newArrayList(transaction));

        order5 = orderService.loadOrderByUuid(order5.getUuid());
        assertEquals(order5.getState(), OrderState.WAITING_TO_APPROVE);
        assertNull(order5.getInvoice());


        order6 = orderService.loadOrderByUuid(order6.getUuid());
        assertEquals(order5.getState(), OrderState.WAITING_TO_APPROVE);
        assertNull(order5.getInvoice());

        List<ApproveOrder> approves = orderService.listApproveOrders(0, 10);
        assertEquals(approves.size(), 4);
        assertEquals(orderService.loadApproveOrdersCount(), 4);

        approves = orderService.listApproveOrdersForEvent(e1pp.getEvent().getUuid(), 0, 10);
        assertEquals(approves.size(), 4);
        assertEquals(orderService.loadApproveOrdersForEventCount(e1pp.getEvent().getUuid()), 4);

        // approvneme
        ApproveOrder ap = orderService.loadApproveOrderByOrderUuid(order6.getUuid());
        orderService.updateOrderApprove(ap.getId(), true, null);

        Order approvedOrder = orderService.loadOrderByUuid(ap.getOrder().getUuid());
        assertEquals(approvedOrder.getState(), OrderState.CONFIRMED_SYSTEM);
        assertNotNull(approvedOrder.getInvoice());
        assertEquals(approvedOrder.getInvoice().getConfirmationSource(), ConfirmationChannel.BANK_TRANSFER);

        // ne-approvneme
        ap = approves.get(1);
        orderService.updateOrderApprove(ap.getId(), false, null);

        Order notApprovedOrder = orderService.loadOrderByUuid(ap.getOrder().getUuid());
        assertEquals(notApprovedOrder.getState(), OrderState.CANCELED_SYSTEM);

        approves = orderService.listApproveOrdersForEvent(e1pp.getEvent().getUuid(), 0, 10);
        assertEquals(approves.size(), 2);
        assertEquals(orderService.loadApproveOrdersForEventCount(e1pp.getEvent().getUuid()), 2);

    }
}
