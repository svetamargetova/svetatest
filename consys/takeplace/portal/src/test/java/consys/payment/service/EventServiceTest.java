package consys.payment.service;

import consys.common.utils.UuidProvider;
import consys.common.utils.enums.Currency;
import consys.common.utils.enums.CountryEnum;
import consys.payment.SystemProperties;
import consys.payment.service.exception.NoRecordException;
import consys.payment.bo.Event;
import consys.payment.bo.EventPaymentProfile;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.config.AbstractServiceTest;
import consys.payment.service.EventService;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.RequiredPropertyException;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventServiceTest extends AbstractServiceTest {

    @Autowired
    private EventService eventService;
    
    
       

    @Test(groups = {"service"})
    public void testEventCreate() throws RequiredPropertyException, NoRecordException {
        
        
        String uuid = UuidProvider.getUuid();
        // Ceska republika
        Event event = eventService.createEvent(uuid, CountryEnum.CZECH_REPUBLIC);        
        List<EventPaymentProfile> eventPaymentProfiles = eventService.listEventPaymentProfiles(event.getUuid());
        
                
        Event event123 = eventService.createEvent(UuidProvider.getUuid(), CountryEnum.CZECH_REPUBLIC);
        eventPaymentProfiles = eventService.listEventPaymentProfiles(event123.getUuid());
        assertNotNull(eventPaymentProfiles);        
        assertEquals(eventPaymentProfiles.size(), 1);
        
        
        
        eventPaymentProfiles = eventService.listEventPaymentProfiles(event.getUuid());        
        assertNotNull(eventPaymentProfiles);        
        assertEquals(eventPaymentProfiles.size(), 1);
        assertEquals(eventPaymentProfiles.get(0).getCurrency(), Currency.CZK);
        assertEquals(eventPaymentProfiles.get(0).getOrderShareFeeBank(), SystemProperties.ORDER_SHARE_FEE_CZK);
        assertEquals(eventPaymentProfiles.get(0).getOrderShareFeeOnline(), SystemProperties.ORDER_SHARE_FEE_CZK);
        assertEquals(eventPaymentProfiles.get(0).getOrderUnitFeeBank(), SystemProperties.ORDER_UNIT_FEE_CZK);
        assertEquals(eventPaymentProfiles.get(0).getOrderUnitFeeOnline(), SystemProperties.ORDER_UNIT_FEE_CZK);
        assertNotNull(eventPaymentProfiles.get(0).getPaymentProfile());
        assertNotNull(eventPaymentProfiles.get(0).getPaymentProfile().getRevisions());
        assertEquals(eventPaymentProfiles.get(0).getPaymentProfile().getRevisions().size(),1);
                        
        
        // nekdo z EU
        uuid = UuidProvider.getUuid();
        event = eventService.createEvent(uuid, CountryEnum.SLOVAKIA);
        eventPaymentProfiles = eventService.listEventPaymentProfiles(event.getUuid());
        assertNotNull(eventPaymentProfiles);        
        assertEquals(eventPaymentProfiles.size(), 1);
        assertEquals(eventPaymentProfiles.get(0).getCurrency(), Currency.EUR);
        assertEquals(eventPaymentProfiles.get(0).getOrderShareFeeBank(), SystemProperties.ORDER_SHARE_FEE_EUR);
        assertEquals(eventPaymentProfiles.get(0).getOrderShareFeeOnline(), SystemProperties.ORDER_SHARE_FEE_EUR);
        assertEquals(eventPaymentProfiles.get(0).getOrderUnitFeeBank(), SystemProperties.ORDER_UNIT_FEE_EUR);
        assertEquals(eventPaymentProfiles.get(0).getOrderUnitFeeOnline(), SystemProperties.ORDER_UNIT_FEE_EUR);
        assertNotNull(eventPaymentProfiles.get(0).getPaymentProfile());
        assertNotNull(eventPaymentProfiles.get(0).getPaymentProfile().getRevisions());
        assertEquals(eventPaymentProfiles.get(0).getPaymentProfile().getRevisions().size(),1);
        
        // nekdo ze sveta
        uuid = UuidProvider.getUuid();
        event = eventService.createEvent(uuid, CountryEnum.CUBA);
        eventPaymentProfiles = eventService.listEventPaymentProfiles(event.getUuid());
        assertNotNull(eventPaymentProfiles);        
        assertEquals(eventPaymentProfiles.size(), 1);
        assertEquals(eventPaymentProfiles.get(0).getCurrency(), Currency.USD);
        assertEquals(eventPaymentProfiles.get(0).getOrderShareFeeBank(), SystemProperties.ORDER_SHARE_FEE_USD);
        assertEquals(eventPaymentProfiles.get(0).getOrderShareFeeOnline(), SystemProperties.ORDER_SHARE_FEE_USD);
        assertEquals(eventPaymentProfiles.get(0).getOrderUnitFeeBank(), SystemProperties.ORDER_UNIT_FEE_USD);
        assertEquals(eventPaymentProfiles.get(0).getOrderUnitFeeOnline(), SystemProperties.ORDER_UNIT_FEE_USD);
        assertNotNull(eventPaymentProfiles.get(0).getPaymentProfile());
        assertNotNull(eventPaymentProfiles.get(0).getPaymentProfile().getRevisions());
        assertEquals(eventPaymentProfiles.get(0).getPaymentProfile().getRevisions().size(),1);
        
        // listovani eventu - +1 event defaultny
        List<Event> events = eventService.listEvents(false, null, null, 0, 10);
        assertEquals(events.size(), 4);
        assertEquals(eventService.loadAllEventsCount(false, null, null),4);
       
    }

    @Test(groups = {"service"}, description="Test vytvorenia a spravy event payment profilu")
    public void testEventProfiles() throws RequiredPropertyException, IbanFormatException, NoRecordException {

       
        String uuid = UuidProvider.getUuid();
        // Ceska republika
        Event event = eventService.createEvent(uuid, CountryEnum.CZECH_REPUBLIC);        
        List<EventPaymentProfile> eventPaymentProfiles = eventService.listEventPaymentProfiles(event.getUuid());
        String ppuuid = eventPaymentProfiles.get(0).getUuid();
        EventPaymentProfile epp = eventService.loadEventPaymentProfile(ppuuid);
        assertEquals(epp.getCurrency(), Currency.CZK);
        assertEquals(epp.getOrderShareFeeBank(), SystemProperties.ORDER_SHARE_FEE_CZK);
        assertEquals(epp.getOrderShareFeeOnline(), SystemProperties.ORDER_SHARE_FEE_CZK);
        assertEquals(epp.getOrderUnitFeeBank(), SystemProperties.ORDER_UNIT_FEE_CZK);
        assertEquals(epp.getOrderUnitFeeOnline(), SystemProperties.ORDER_UNIT_FEE_CZK);
        assertNotNull(epp.getPaymentProfile());
        assertNotNull(epp.getPaymentProfile().getRevisions());
        assertEquals(epp.getPaymentProfile().getRevisions().size(),1);
        
        epp.setCurrency(Currency.EUR);        
        epp.setInvoiceNote("Note");
        eventService.updateEventPaymentProfile(epp);  
        
        epp = eventService.loadEventPaymentProfile(epp.getUuid());
        assertEquals(epp.getOrderShareFeeBank(), SystemProperties.ORDER_SHARE_FEE_EUR);
        assertEquals(epp.getOrderShareFeeOnline(), SystemProperties.ORDER_SHARE_FEE_EUR);
        assertEquals(epp.getOrderUnitFeeBank(), SystemProperties.ORDER_UNIT_FEE_EUR);
        assertEquals(epp.getOrderUnitFeeOnline(), SystemProperties.ORDER_UNIT_FEE_EUR);
        
                
        PaymentProfileRevision rev1 = new PaymentProfileRevision();
        rev1.setAccountBankSwift("123");
        rev1.setAccountIban("BE68539007547034");
        rev1.setCity("City");
        rev1.setCountry(CountryEnum.CZECH_REPUBLIC);
        rev1.setName("Name");
        rev1.setRegNo("123");
        rev1.setStreet("Street");
        rev1.setVatNo("123");
        rev1.setZip("123");
        eventService.updateEventPaymentProfileRevision(epp, rev1);
        
        epp = eventService.loadEventPaymentProfile(epp.getUuid());
        assertEquals(epp.getPaymentProfile().getRevisions().size(), 1);
        assertEquals(epp.getPaymentProfile().getActualRevision().getAccountBankSwift(), rev1.getAccountBankSwift());
        assertEquals(epp.getPaymentProfile().getActualRevision().getAccountIban(), rev1.getAccountIban());
        assertEquals(epp.getPaymentProfile().getActualRevision().getCity(), rev1.getCity());

        
        // TODO vytvorenie orderu aktivneho a noveho profilu
        
    }

    @Test(groups = {"service"})
    public void testEventProduct() throws RequiredPropertyException, IbanFormatException, NoRecordException {

//        String eventUuid = UuidProvider.getUuid();
//        logger.debug("------------ Preparing");
//        PaymentProfileRevision profileRev = new PaymentProfileRevision();
//        profileRev.setAccountBankSwift("123");
//        profileRev.setAccountIban("BE68539007547034");
//        profileRev.setCity("City");
//        profileRev.setCountry(CountryEnum.HAITI);
//        profileRev.setName("Name");
//        profileRev.setRegNo("123");
//        profileRev.setStreet("Street");
//        profileRev.setVatNo("123");
//        profileRev.setZip("123");
//        eventService.updateEventAddNewProfile(eventUuid, profileRev);
//        Event e = eventService.loadEventWithProfiles(eventUuid);
////        String profileUuid = e.getProfiles().get(0).getUuid();
//
//        Product p1 = new Product();
//        p1.setCategory(ProductCategory.TICKET);
//        p1.setName("Product name");
//        p1.setPrice(new BigDecimal(-23));
//        try {
//            logger.debug("------------ Test 1 : negative price");
//            productService.updateEventAddProduct(e, p1);
//            fail();
//        } catch (RequiredPropertyException ex) {
//        }
//        logger.debug("------------ Test 2 : create product with default profile");
//        p1.setPrice(new BigDecimal(1200));
//        p1.setUuid(UuidProvider.getUuid());
//        productService.updateEventAddProduct(e, p1);
//
//        logger.debug("------------ Test 3 : create product");
//        Product p2 = new Product();
//        p2.setCategory(ProductCategory.TICKET);
//        p2.setUuid(UuidProvider.getUuid());
//        p2.setName("Product name");
//        p2.setPrice(new BigDecimal(12300));
//        e = eventService.loadEventWithProfiles(eventUuid);
////        productService.updateEventAddProduct(eventUuid, profileUuid, p2);
//
//        logger.debug("------------ Test 4 : load all with products");
//        List<Product> pe = productService.listAllEventProducts(eventUuid);
//        assertEquals(pe.size(), 2);
//
//        logger.debug("------------ Test 5 : update product");
//        Product pl = pe.get(0);
//        pl.setName("Name name");
//        productService.updateEventProduct(e, pl);
//
//        logger.debug("------------ Test 6 : batch update of products");
//        Product p1l = pe.get(0);
//        Product p2l = pe.get(1);
//        p1l.setName("Name name");
//        p2l.setName("Name name");
//        productService.updateEventProducts(e, Lists.newArrayList(p1l,p2l));
    }
}
