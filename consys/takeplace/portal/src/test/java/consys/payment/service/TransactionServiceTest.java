/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.payment.service;

import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.payment.bo.Transaction;
import consys.payment.bo.enums.ConfirmationChannel;
import consys.common.utils.enums.Currency;
import consys.payment.config.AbstractServiceTest;
import consys.payment.service.TransactionService;
import consys.payment.service.exception.RequiredPropertyException;
import java.math.BigDecimal;
import static org.testng.Assert.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

/**
 *
 * @author palo
 */
public class TransactionServiceTest extends AbstractServiceTest {

    @Autowired
    private TransactionService transactionService;

    public Transaction generate(ConfirmationChannel channel, int dateOffset) {
        Transaction t = new Transaction();
        t.setAmount(new BigDecimal(350));
        t.setConfirmationChannel(channel);
        t.setCurrency(Currency.CZK);
        t.setTransactionDate(DateUtils.addDays(DateProvider.getCurrentDate(), dateOffset));
        return t;
    }

    @Test(groups = {"service"})
    public void testTransactionCreateAndLoad() throws RequiredPropertyException {
        transactionService.createTransaction(generate(ConfirmationChannel.BANK_TRANSFER, -3));
        transactionService.createTransaction(generate(ConfirmationChannel.ONLINE_PAY_PAL, -10));
        transactionService.createTransaction(generate(ConfirmationChannel.BANK_TRANSFER, -10));
        transactionService.createTransaction(generate(ConfirmationChannel.BANK_TRANSFER, -10));
        transactionService.createTransaction(generate(ConfirmationChannel.BANK_TRANSFER, -3));
        transactionService.createTransaction(generate(ConfirmationChannel.ONLINE_PAY_PAL, 0));

        int count = transactionService.listTransactionsCount(ConfirmationChannel.BANK_TRANSFER, null, null);
        List<Transaction> transactions = transactionService.listTransactions(ConfirmationChannel.BANK_TRANSFER, null, null, 0, 10);
        assertEquals(count, 4);
        assertEquals(transactions.size(), 4);

        count = transactionService.listTransactionsCount(null, null, null);
        transactions = transactionService.listTransactions(null, null, null, 0, 10);
        assertEquals(count, 6);
        assertEquals(transactions.size(), 6);

        count = transactionService.listTransactionsCount(null, DateUtils.addDays(DateProvider.getCurrentDate(), -4), null);
        transactions = transactionService.listTransactions(null, DateUtils.addDays(DateProvider.getCurrentDate(), -4), null, 0, 10);
        assertEquals(count, 3);
        assertEquals(transactions.size(), 3);

        count = transactionService.listTransactionsCount(null, DateUtils.addDays(DateProvider.getCurrentDate(), -12), DateUtils.addDays(DateProvider.getCurrentDate(), -5));
        transactions = transactionService.listTransactions(null, DateUtils.addDays(DateProvider.getCurrentDate(), -12), DateUtils.addDays(DateProvider.getCurrentDate(), -5), 0, 10);
        assertEquals(count, 3);
        assertEquals(transactions.size(), 3);

        count = transactionService.listTransactionsCount(ConfirmationChannel.ONLINE_PAY_PAL, DateUtils.addDays(DateProvider.getCurrentDate(), -12), DateUtils.addDays(DateProvider.getCurrentDate(), -5));
        transactions = transactionService.listTransactions(ConfirmationChannel.ONLINE_PAY_PAL, DateUtils.addDays(DateProvider.getCurrentDate(), -12), DateUtils.addDays(DateProvider.getCurrentDate(), -5), 0, 10);
        assertEquals(count, 1);
        assertEquals(transactions.size(), 1);

    }
}
