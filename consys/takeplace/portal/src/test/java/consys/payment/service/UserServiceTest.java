package consys.payment.service;

import consys.common.utils.UuidProvider;
import consys.common.utils.enums.CountryEnum;
import consys.payment.service.exception.NoRecordException;
import consys.payment.bo.PaymentProfileRevision;
import consys.payment.bo.User;
import consys.payment.config.AbstractServiceTest;
import consys.payment.service.UserService;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.service.exception.RequiredPropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserServiceTest extends AbstractServiceTest {

    @Autowired
    private UserService userService;

    
    

    @Test(groups = {"service"})
    public void testUserCreate() throws RequiredPropertyException, NoRecordException {
        try {
            userService.createUser(null);
            fail();
        } catch (RequiredPropertyException e) {
        }

        String uuid = UuidProvider.getUuid();

        userService.createUser(uuid);


        User e1 = userService.loadUser(uuid);

        User e2 = userService.loadUserWithProfiles(uuid);

        assertNotNull(e1);
        assertNotNull(e2);
        assertEquals(e1, e2);
        assertEquals(e1.hashCode(), e2.hashCode());
        try {
            userService.loadUser(null);
            fail();
        } catch (RequiredPropertyException e) {
        }
        try {
            userService.loadUserWithProfiles(null);
            fail();
        } catch (RequiredPropertyException e) {
        }

    }

    @Test(groups = {"service"})
    public void testEventProfiles() throws RequiredPropertyException, IbanFormatException, NoRecordException{


        String uuid = UuidProvider.getUuid();

        PaymentProfileRevision rev1 = new PaymentProfileRevision();
        rev1.setCity("City");
        rev1.setCountry(CountryEnum.ALBANIA);
        rev1.setName("Name");
        rev1.setRegNo("123");
        rev1.setStreet("Street");
        rev1.setVatNo("123");
        rev1.setZip("123");
        userService.createNewPaymentProfile(uuid, rev1);
        User e = userService.loadUserWithProfiles(uuid);

        assertEquals(e.getProfiles().size(), 1);
        assertEquals(e.getProfiles().get(0), rev1.getParent());
        assertEquals(e.getProfiles().get(0).getActualRevision(), rev1);
        assertEquals(e.getProfiles().get(0).getRevisions().size(), 1);
        assertEquals(e.getProfiles().get(0).getRevisions().iterator().next(), rev1);

        PaymentProfileRevision rev2 = new PaymentProfileRevision();
        rev2.setAccountBankSwift("123");
        rev2.setAccountIban("BE68539007547034");
        rev2.setCity("City");
        rev2.setCountry(CountryEnum.HAITI);
        rev2.setName("Name");
        rev2.setRegNo("123");
        rev2.setStreet("Street");
        rev2.setVatNo("123");
        rev2.setZip("123");
        
        userService.updateOrCreateNewPaymentProfileRevision(uuid,rev1.getParent().getUuid(), rev2);
        e = userService.loadUserWithProfiles(uuid);

        assertEquals(e.getProfiles().size(), 1);
        assertEquals(e.getProfiles().get(0), rev1.getParent());
        assertEquals(e.getProfiles().get(0).getActualRevision(), rev2);
        assertEquals(e.getProfiles().get(0).getRevisions().size(), 1);
    }
}
