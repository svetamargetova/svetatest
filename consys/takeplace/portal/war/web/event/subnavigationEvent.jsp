<stripes:layout-component name="subnavigation">
    <div class="top-subpanel-item">
        <stripes:link href="/bean/event/EventListItem.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Details
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/event/EventPaymentProfileList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Profiles
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/event/EventProductsList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Products
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/order/OrderList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Orders
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/order/ApproveOrderList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Approve orders
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/event/EventInvoiceList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Invoices
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/transfer/TransferList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>Transfers
        </stripes:link>
    </div>
</stripes:layout-component>



