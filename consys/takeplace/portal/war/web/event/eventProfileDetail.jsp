<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/event/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <h1>Event payment profile detail</h1>
        <stripes:form action="/bean/event/EventPaymentProfileListItem.action" >
            <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
            <stripes:hidden name="profileUuid" value="${actionBean.profileUuid}" />
            <table class="leftRightForm">
                <tr>
                    <th>UUID</th>
                    <td>${actionBean.profile.uuid}</td>
                </tr>
                <tr>
                    <th>Active</th>
                    <td>${actionBean.profile.active}</td>
                </tr>
                <tr>
                    <th>Currency</th>
                    <td>${actionBean.profile.currency}</td>
                </tr>
                <tr>
                    <th>VAT type</th>
                    <td>${actionBean.profile.vatType}</td>
                </tr>
                <tr>
                    <th>Order bank transfer invoice to this profile</th>
                    <td>${actionBean.profile.orderBankTransferInvoicedToThisProfile}</td>
                </tr>
                <tr>
                    <th>Order unit fee bank</th>
                    <td><fmt:formatNumber value="${actionBean.profile.orderUnitFeeBank}"  pattern="###,###,##0.00" /> ${actionBean.profile.currency}</td>
                </tr>
                <tr>
                    <th>Share unit fee bank</th>
                    <td><fmt:formatNumber value="${actionBean.profile.orderShareFeeBank}"  pattern="###,###,##0.00" /> %</td>
                </tr>
                <tr>
                    <th>Order unit fee online</th>
                    <td><fmt:formatNumber value="${actionBean.profile.orderUnitFeeOnline}"  pattern="###,###,##0.00" /> ${actionBean.profile.currency}</td>
                </tr>
                <tr>
                    <th>Share unit fee online</th>
                    <td><fmt:formatNumber value="${actionBean.profile.orderShareFeeOnline}"  pattern="###,###,##0.00" /> %</td>
                </tr>
                <tr>
                    <th>Order unit fee organizator</th>
                    <td><fmt:formatNumber value="${actionBean.profile.orderUnitFeeOrganizator}"  pattern="###,###,##0.00" /> ${actionBean.profile.currency}</td>
                </tr>
                <tr>
                    <th>Share unit fee organizator</th>
                    <td><fmt:formatNumber value="${actionBean.profile.orderShareFeeOrganizator}"  pattern="###,###,##0.00" /> %</td>
                </tr>
                <tr>
                    <th>Max fee</th>
                    <td><fmt:formatNumber value="${actionBean.profile.orderMaxFee}"  pattern="###,###,##0.00" /> ${actionBean.profile.currency}</td>
                </tr>
                <tr>
                    <th>Invoice note</th>
                    <td>${actionBean.profile.invoiceNote}</td>
                </tr>
                <tr>
                    <th>Order invoice counter</th>
                    <td>${actionBean.profile.orderInvoiceCounter}</td>
                </tr>
                <tr>
                    <th>Order invoice series counter</th>
                    <td>${actionBean.profile.orderInvoiceSeriesCounter}</td>
                </tr>
                <tr>
                    <th>Transfer invoice counter</th>
                    <td>${actionBean.profile.transferInvoiceCounter}</td>
                </tr>
                <tr>
                    <td></td>
                    <td><stripes:submit name="edit" value="Edit" /><stripes:submit name="back" value="Cancel" /></td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>