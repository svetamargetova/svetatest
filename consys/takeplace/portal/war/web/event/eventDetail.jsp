<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/event/subnavigationEvent.jsp" %>
    <stripes:layout-component name="contents">
        <h1>Event detail</h1>
        <stripes:form action="/bean/event/EventListItem.action" >
            <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
            <table class="leftRightForm">
                <tr>
                    <th>UUID</th>
                    <td>${actionBean.event.uuid}</td>
                </tr>
                <tr>
                    <th>Active</th>
                    <td>${actionBean.event.active}</td>
                </tr>
                <tr>
                    <th>Create date</th>
                    <td><fmt:formatDate value="${actionBean.event.createDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>Activation date</th>
                    <td><fmt:formatDate value="${actionBean.event.activationDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <th>Order</th>
                    <td>
                        <table class="leftRightForm">
                            <tr>
                                <th>State</th>
                                <td>${actionBean.event.licenseOrder.state}</td>
                            </tr>
                            <tr>
                                <th>Order number</th>
                                <td>${actionBean.event.licenseOrder.orderNumber}</td>
                            </tr>
                            <tr>
                                <th>Order type</th>
                                <td>${actionBean.event.licenseOrder.orderType}</td>
                            </tr>
                            <tr>
                                <th>Purchase date</th>
                                <td><fmt:formatDate value="${actionBean.event.licenseOrder.purchaseDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                            </tr>
                            <tr>
                                <th>Due date</th>
                                <td><fmt:formatDate value="${actionBean.event.licenseOrder.dueDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                            </tr>
                            <tr>
                                <th>Confirmed date</th>
                                <td><fmt:formatDate value="${actionBean.event.licenseOrder.confirmedDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td><fmt:formatNumber value="${actionBean.event.licenseOrder.price}"  pattern="###,###,##0.00" /></td>
                            </tr>
                            <tr>
                                <th>Unit fee</th>
                                <td><fmt:formatNumber value="${actionBean.event.licenseOrder.unitFee}"  pattern="###,###,##0.00" /></td>
                            </tr>
                            <tr>
                                <th>Share fee</th>
                                <td><fmt:formatNumber value="${actionBean.event.licenseOrder.shareFee}"  pattern="###,###,##0.00" /></td>
                            </tr>
                            <tr>
                                <th>Note</th>
                                <td>${actionBean.event.licenseOrder.note}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><stripes:submit name="back" value="Cancel" /></td>
                </tr>
            </table>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>