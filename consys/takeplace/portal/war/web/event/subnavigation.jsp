<stripes:layout-component name="subnavigation">
    <div class="top-subpanel-item">
        <stripes:link href="/bean/event/EventPaymentProfileListItem.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>
            <stripes:param name="profileUuid" value="${actionBean.profileUuid}"/>Details
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/event/EventPaymentProfileRevision.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>
            <stripes:param name="profileUuid" value="${actionBean.profileUuid}"/>Revision
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/event/EventProductsList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>
            <stripes:param name="profileUuid" value="${actionBean.profileUuid}"/>Products
        </stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link href="/bean/transfer/TransferList.action">
            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>
            <stripes:param name="profileUuid" value="${actionBean.profileUuid}"/>Transfers
        </stripes:link>
    </div>
</stripes:layout-component>