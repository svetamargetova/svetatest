<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <stripes:layout-component name="contents">
        <h1>Events</h1>
        <stripes:form action="/bean/event/EventList.action" >
            <table>
                <tr>
                    <td>From:</td>
                    <td><stripes:text name="from"><fmt:formatDate value="${actionBean.from}" pattern="dd.MM.yyyy"/></stripes:text></td>
                </tr>
                <tr>
                    <td>To:</td>
                    <td><stripes:text name="to"><fmt:formatDate value="${actionBean.to}" pattern="dd.MM.yyyy"/></stripes:text></td>
                </tr>
                <tr>
                    <td>Active</td>
                    <td><stripes:checkbox name="active"></stripes:checkbox></td>
                </tr>
                <tr>
                    <td></td>
                    <td><stripes:submit name="list" value="Refresh"/></td>
                </tr>
            </table>
            <div style="margin-top:10px">
                <display:table name="${actionBean.items}" id="event" sort="external" partialList="true" pagesize="50"
                               size="${actionBean.fullListSize}" requestURI="/bean/event/EventList.action?list" class="display">
                    <display:column property="uuid" title="UUID" />
                    <display:column property="invoiceClientNumber" title="Invoice client number (prefix)" />
                    <display:column property="active" title="Active" />
                    <display:column title="Actions" >
                        <stripes:link href="/bean/event/EventListItem.action">
                            <stripes:param name="eventUuid" value="${event.uuid}"/>Details
                        </stripes:link>
                    </display:column>
                </display:table>
            </div>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>