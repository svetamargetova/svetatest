<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/event/subnavigationEvent.jsp" %>
    <stripes:layout-component name="contents">
        <h1>Event invoices</h1>
        <stripes:form action="/bean/event/EventInvoiceList.action">
            <table>
                <tr>
                    <td colspan="4" style="padding-top:10px">Confirmed</td>
                </tr>
                <tr>
                    <td>From:</td>
                    <td><stripes:text name="from"><fmt:formatDate value="${actionBean.from}" pattern="dd.MM.yyyy"/></stripes:text></td>
                    <td style="padding-left: 10px">To:</td>
                    <td><stripes:text name="to"><fmt:formatDate value="${actionBean.to}" pattern="dd.MM.yyyy"/></stripes:text></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-top:10px;">Number</td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3"><stripes:text name="number">${actionBean.number}</stripes:text></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-top:10px;"><stripes:submit name="list" value="Refresh"/></td>
                </tr>
            </table>
            <div style="margin-top:10px">
                <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
                <display:table name="${actionBean.items}" id="invoice" sort="external" partialList="true" pagesize="50"
                               size="${actionBean.fullListSize}" requestURI="/bean/event/EventInvoiceList.action?list" class="display">
                    <display:column property="invoiceNumber" title="Invoice number" />
                    <display:column property="confirmationSource" title="Confirmation source" />
                    <display:column property="issueDate" title="Issue date" decorator="consys.payment.stripes.displaytag.DateTimeDecorator" />
                    <display:column property="taxableFulfillmentDate" title="Taxable fulfillment date" decorator="consys.payment.stripes.displaytag.DateTimeDecorator" />
                    <display:column title="Action" >
                        <stripes:link href="/bean/order/OrderListItem.action">
                            <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>
                            <stripes:param name="uuid" value="${invoice.order.uuid}"/>
                            <stripes:param name="invoice" value="true"/>Detail
                        </stripes:link>
                    </display:column>
                </display:table>
            </div>
            <div style="margin-top:10px">
                <stripes:submit name="back" value="Back on events" />
            </div>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>