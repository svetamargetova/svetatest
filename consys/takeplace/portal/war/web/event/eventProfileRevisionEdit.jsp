<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>


<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/event/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <div>
            <stripes:errors/>
            <h1>Actual event payment profile revision</h1>
            <stripes:form action="/bean/event/EditEventPaymentProfileRevision.action" >
                <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
                <stripes:hidden name="profileUuid" value="${actionBean.profileUuid}" />
                <table class="leftRightForm">
                    <tr>
                        <th>UUID:</th>
                        <td>${actionBean.revision.uuid}</td>
                    </tr>
                    <tr>
                        <th>Creation date:</th>
                        <td><fmt:formatDate value="${actionBean.revision.revisionDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                    </tr>
                    <tr>
                        <th>Name:</th>
                        <td><stripes:text name="revision.name">${actionBean.revision.name}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>Street:</th>
                        <td><stripes:text name="revision.street">${actionBean.revision.street}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>City:</th>
                        <td><stripes:text name="revision.city">${actionBean.revision.city}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>Zip:</th>
                        <td><stripes:text name="revision.zip">${actionBean.revision.zip}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>Country:</th>
                        <td>
                            <stripes:select name="revision.country" value="${actionBean.revision.country}">
                                <stripes:options-enumeration enum="consys.common.utils.enums.CountryEnum" />
                            </stripes:select>
                        </td>
                    </tr>
                    <tr>
                        <th>ICO:</th>
                        <td><stripes:text name="revision.regNo">${actionBean.revision.regNo}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>DIC:</th>
                        <td><stripes:text name="revision.vatNo">${actionBean.revision.vatNo}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>IBAN:</th>
                        <td><stripes:text name="revision.accountIban">${actionBean.revision.accountIban}</stripes:text></td>
                    </tr>
                    <tr>
                        <th>SWIFT:</th>
                        <td><stripes:text name="revision.accountBankSwift">${actionBean.revision.accountBankSwift}</stripes:text></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><stripes:submit name="update" value="Update"/><stripes:submit name="back" value="Cancel"/></td>
                    </tr>
                </table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>