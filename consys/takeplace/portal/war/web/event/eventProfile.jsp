<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/event/subnavigationEvent.jsp" %>
    <stripes:layout-component name="contents">
        <stripes:form action="/bean/event/EventPaymentProfileList.action" >
            <h1>Event payment profiles for ${actionBean.eventUuid}</h1>
            <stripes:hidden name="eventUuid" value="${actionBean.eventUuid}" />
            <display:table name="${actionBean.items}" id="profile" sort="external" partialList="true" pagesize="50"
                           size="${actionBean.fullListSize}" requestURI="/bean/event/EventPaymentProfileList.action?list" class="display">
                <display:column property="uuid" title="UUID" />
                <display:column property="active" title="Active" />
                <display:column property="currency" title="Currency" />
                <display:column property="vatType" title="VAT type" />
                <display:column property="orderBankTransferInvoicedToThisProfile" title="On invoice" />
                <display:column title="Actions" >
                    <stripes:link href="/bean/event/EventPaymentProfileListItem.action">
                        <stripes:param name="eventUuid" value="${actionBean.eventUuid}"/>
                        <stripes:param name="profileUuid" value="${profile.uuid}"/>Details
                    </stripes:link>
                </display:column>
            </display:table>
            <div style="margin-top:10px">
                <stripes:submit name="back" value="Back on events" />
            </div>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>