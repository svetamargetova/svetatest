<stripes:layout-component name="subnavigation">
    <div class="top-subpanel-item">
        <stripes:link name="order" beanclass="consys.payment.stripes.bean.order.OrderListActionBean">Orders</stripes:link>
    </div>
    <div class="top-subpanel-item">
        <stripes:link name="approveorder" beanclass="consys.payment.stripes.bean.order.ApproveOrderListActionBean">Approve orders</stripes:link>
    </div>
</stripes:layout-component>
