<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/transaction/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <h1>Bank transaction</h1>
        <stripes:form action="/bean/transaction/BankTransaction.action">
            ${actionBean.uploadMessage}
            <table class="leftRightForm">
                <tr>
                    <th>Bank transaction file to upload</th>
                    <td><stripes:file name="file"/></td>
                </tr>
                <tr>
                    <th></th>
                    <td><stripes:submit name="upload" value="Upload"/></td>
                </tr>
            </table>
            <div style="margin-top:10px"><stripes:submit name="back" value="Back on transactions"/></div>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>