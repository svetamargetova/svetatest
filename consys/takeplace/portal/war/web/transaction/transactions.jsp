<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>

<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/transaction/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <h1>Transactions</h1>
        <stripes:form action="/bean/transaction/TransactionList.action" >
            <table class="leftRightForm">
                <tr>
                    <td>Channel:</td>
                    <td>
                        <stripes:select name="channel" value="${actionBean.channel}">
                            <stripes:options-enumeration enum="consys.payment.bo.enums.ConfirmationChannel" />
                        </stripes:select>
                    </td>
                </tr>
                <tr>
                    <td>From:</td>
                    <td><stripes:text name="from"><fmt:formatDate value="${actionBean.from}" pattern="dd.MM.yyyy"/></stripes:text></td>
                </tr>
                <tr>
                    <td>To:</td>
                    <td><stripes:text name="to"><fmt:formatDate value="${actionBean.to}" pattern="dd.MM.yyyy"/></stripes:text></td>
                </tr>
                <tr>
                    <td></td>
                    <td><stripes:submit name="list" value="Refresh"/></td>
                </tr>
            </table>

            <div style="margin-top:10px">
                <display:table name="${actionBean.items}" id="transaction" sort="external" partialList="true" pagesize="50"
                               size="${actionBean.fullListSize}" requestURI="/bean/transaction/TransactionList.action?list" class="display">
                    <display:column property="createdDate" title="Create date" decorator="consys.payment.stripes.displaytag.DateTimeDecorator" />
                    <display:column property="transactionDate" title="Transaction date" decorator="consys.payment.stripes.displaytag.DateTimeDecorator" />
                    <display:column property="amount" title="Amount" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <display:column property="fees" title="Fees" decorator="consys.payment.stripes.displaytag.PriceDecorator" />
                    <display:column title="Action" >
                        <stripes:link href="/bean/transaction/TransactionListItem.action">
                            <stripes:param name="uuid" value="${transaction.uuid}"/>Details
                        </stripes:link>
                    </display:column>
                </display:table>
            </div>
        </stripes:form>
    </stripes:layout-component>
</stripes:layout-render>