<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ include file="/web/layout/taglibs.jsp" %>


<stripes:layout-render name="/web/layout/template.jsp">
    <%@ include file="/web/settings/subnavigation.jsp" %>
    <stripes:layout-component name="contents">
        <div>
            <stripes:form action="/bean/settings/ProfileSettings.action" >
                <h1>Revision for currency: ${actionBean.type}</h1>
                <stripes:hidden name="type" value="${actionBean.type}" />
                <table class="leftRightForm">
                    <tr>
                        <th>Profile UUID:</th>
                        <td>${actionBean.revision.uuid}</td>
                    </tr>
                    <tr>
                        <th>Creation date:</th>
                        <td><fmt:formatDate value="${actionBean.revision.revisionDate}" pattern="dd.MM.yyyy HH:mm"/></td>
                    </tr>
                    <tr>
                        <th>Name:</th>
                        <td>${actionBean.revision.name}</td>
                    </tr>
                    <tr>
                        <th>Street:</th>
                        <td>${actionBean.revision.street}</td>
                    </tr>
                    <tr>
                        <th>City:</th>
                        <td>${actionBean.revision.city}</td>
                    </tr>
                    <tr>
                        <th>Zip:</th>
                        <td>${actionBean.revision.zip}</td>
                    </tr>
                    <tr>
                        <th>Country:</th>
                        <td>${actionBean.revision.country}</td>
                    </tr>
                    <tr>
                        <th>ICO:</th>
                        <td>${actionBean.revision.regNo}</td>
                    </tr>
                    <tr>
                        <th>DIC:</th>
                        <td>${actionBean.revision.vatNo}</td>
                    </tr>
                    <tr>
                        <th>IBAN:</th>
                        <td>${actionBean.revision.accountIban}</td>
                    </tr>
                    <tr>
                        <th>SWIFT:</th>
                        <td>${actionBean.revision.accountBankSwift}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><stripes:submit name="edit" value="Edit"/></td>
                    </tr>
                </table>
            </stripes:form>
        </div>
    </stripes:layout-component>
</stripes:layout-render>