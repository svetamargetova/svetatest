-- 16.02.2011
-- premenovani tabulky
ALTER TABLE conference.artefact_type  RENAME TO submission_artefact_type;

-- premenovani nazvu stlpca v tabulka preffered topics
ALTER TABLE conference.reviewer_preferred_topic RENAME COLUMN submission_id TO id_topic;
ALTER TABLE conference.reviewer_preferred_topic RENAME COLUMN user_id TO id_user_event;

-- premenovani preklepu v tabulke
ALTER TABLE conference.submission_type_artefact RENAME COLUMN num_of_reviers TO num_of_reviewers;

-- premenovani nazvu stlpca v tabulke review_criteria
ALTER TABLE conference.review_criteria RENAME COLUMN submissiontypeartefact TO id_submission_type_artefact;
ALTER TABLE conference.review_criteria RENAME COLUMN mix_points TO min_points;
