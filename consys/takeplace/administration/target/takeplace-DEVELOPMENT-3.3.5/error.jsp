<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="content-language" content="en"/>
        <meta http-equiv="imagetoolbar" content="no"/>
        <link rel="shortcut icon" href="http://www.takeplace.eu/img/favicon.ico" type="image/x-icon"/>
        <meta name="googlebot" content="nosnippet,noarchive"/>
        <title>Takeplace - Organizing Events</title>
        <meta name="Description" content="" />
        <meta name="copyright" content="Acemcee, s.r.o."/>
        <meta name="robots" content="noindex, follow"/>
        <meta name="Author" content="Acemcee, s.r.o."/>
        <link href="http://www.takeplace.eu/style.css" rel="stylesheet" type="text/css"/>

    </head>
    <body id="content">
        <div id="container"> <a href="http://www.takeplace.eu/en-home" title="Takeplace.eu - homepage"><img src="http://www.takeplace.eu/img/logo.gif" alt="Takeplace.eu"/></a>




            <div id="menu">
                <ul>
                    <li>
                        <a class="m4" href="http://www.takeplace.eu/en-home" title="Home">Home</a>
                    </li>
                    <li>
                        <a class="m4" href="http://www.takeplace.eu/en-feature" title="Features">Features</a>
                    </li>
                    <li>
                        <a class="m4" href="http://www.takeplace.eu/en-customers" title="Customers">Customers</a>
                    </li>
                    <li>
                        <a class="m4" href="http://www.takeplace.eu/en-press" title="Press &amp Media">Press &amp Media</a>
                    </li>
                    <li>
                        <span class="m4" title="Support">Support</span>
                    </li>
                    <li>
                        <a class="m4" href="http://www.takeplace.eu/en-about" title="About us">About us</a>
                    </li>
                </ul>
            </div>
        </div>
        <div id="container2">

            <div id="left"class="box">
                <h2>Oops, we're really sorry but something terrible happened.</h2>
                <p>
                    If you see this page often please contact us on <a href="mailto:support@takeplace.eu">support@takeplace.eu</a>.
                </p>
                <p>You can now proceed to:</p>
                
                <a href="http://www.takeplace.eu">Takeplace homepage</a><br>
                <a href="https://app.takeplace.eu">or return back</a><br>
                
            </div>

            <%--
                 301 - Moved Permanently
                 307 - Temporary Redirect
                 401 - Unauthorized
                 403 - Forbidden
                 404 - NotFound
                 408 - RequestTimeout
            --%>

            <div class="clear"></div>
        </div>




        <div id="container3"><div class="clear"></div><div class="separator"></div>
            <p class="text"><em>TAKEPLACE</em> &nbsp;| &nbsp; <a href="http://www.takeplace.eu/en-home">Home</a>&nbsp; | &nbsp;<a href="http://www.takeplace.eu/en-feature">Features</a>&nbsp; | &nbsp;<a href="http://www.takeplace.eu/en-customers">Customers</a>&nbsp; | &nbsp;<a href="http://www.takeplace.eu/en-press">Press &amp Media</a>&nbsp; | &nbsp;<span class="disable">Support</span>&nbsp; | &nbsp;<a href="http://www.takeplace.eu/en-about">About us</a></p>
            <p class="text1">&copy; 2009–2011 &#149; <a href="http://www.acemcee.com"><span class="black">ACEMCEE</span></a> &#149; All rights reserved.</p>
        </div>
    </body></html>
