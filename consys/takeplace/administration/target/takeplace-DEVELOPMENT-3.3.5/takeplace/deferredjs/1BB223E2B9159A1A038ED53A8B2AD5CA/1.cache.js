function $$init_225(){
}

function $runCallbacks(){
  var $e0, e, handler, next;
  while (isNotNull(callbacksHead)) {
    handler = getUncaughtExceptionHandler();
    next = callbacksHead;
    callbacksHead = callbacksHead.next;
    isNull(callbacksHead) && (callbacksTail = null);
    if (isNull(handler)) {
      $onSuccess_6(next.callback);
    }
     else {
      try {
        $onSuccess_6(next.callback);
      }
       catch ($e0) {
        $e0 = caught_0($e0);
        if (instanceOf($e0, 25)) {
          e = $e0;
          handler.onUncaughtException(e);
        }
         else 
          throw $e0;
      }
    }
  }
}

function AsyncLoader1_0(){
  Object_1.call(this);
  $$init_225();
}

function onLoad_0(){
  instance_4 = new AsyncLoader1_0;
  $fragmentHasLoaded(($clinit_11() , BROWSER_LOADER), 1);
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks1', 'begin');
  instance_4.runCallbacks();
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks1', 'end');
}

function AsyncLoader1(){
}

_ = AsyncLoader1_0.prototype = AsyncLoader1.prototype = new Object_0;
_.getClass$ = function getClass_226(){
  return Lcom_google_gwt_lang_asyncloaders_AsyncLoader1_2_classLit;
}
;
_.runCallbacks = function runCallbacks(){
  $runCallbacks();
}
;
_.castableTypeMap$ = {};
function $setAlignment(this$static, row, column, hAlign, vAlign){
  $setHorizontalAlignment(this$static, row, column, hAlign);
  $setVerticalAlignment(this$static, row, column, vAlign);
}

function $setAutoHorizontalAlignment(this$static, autoAlignment){
  this$static.autoHorizontalAlignment = autoAlignment;
  $updateHorizontalAlignment(this$static);
}

function $setHorizontalAlignment_0(this$static, align){
  $setAutoHorizontalAlignment(this$static, align);
}

function $$init_450(){
}

function ListAllEventsAction_0(upcoming, from, limit, prefix){
  Object_1.call(this);
  $$init_450();
  this.upcoming = upcoming;
  this.from = from;
  this.limit = limit;
  this.prefix = prefix;
}

function ListAllEventsAction(){
}

_ = ListAllEventsAction_0.prototype = ListAllEventsAction.prototype = new Object_0;
_.getClass$ = function getClass_451(){
  return Lconsys_admin_event_gwt_client_action_ListAllEventsAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 345:1};
_.from = 0;
_.limit = 0;
_.prefix = null;
_.upcoming = false;
function $$init_451(){
}

function ListMyEventInvitationsAction_0(active, from, limit){
  Object_1.call(this);
  $$init_451();
  this.active = active;
  this.from = from;
  this.limit = limit;
}

function ListMyEventInvitationsAction(){
}

_ = ListMyEventInvitationsAction_0.prototype = ListMyEventInvitationsAction.prototype = new Object_0;
_.getClass$ = function getClass_452(){
  return Lconsys_admin_event_gwt_client_action_ListMyEventInvitationsAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 346:1};
_.active = false;
_.from = 0;
_.limit = 0;
function $$init_452(){
}

function ListMyEventsAction_0(upcoming, from, limit, prefix){
  Object_1.call(this);
  $$init_452();
  this.upcoming = upcoming;
  this.from = from;
  this.limit = limit;
  this.prefix = prefix;
}

function ListMyEventsAction(){
}

_ = ListMyEventsAction_0.prototype = ListMyEventsAction.prototype = new Object_0;
_.getClass$ = function getClass_453(){
  return Lconsys_admin_event_gwt_client_action_ListMyEventsAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 347:1};
_.from = 0;
_.limit = 0;
_.prefix = null;
_.upcoming = false;
function $$init_453(){
}

function LoadEventInvitationAction_0(uuid){
  Object_1.call(this);
  $$init_453();
  this.uuid_0 = uuid;
}

function LoadEventInvitationAction(){
}

_ = LoadEventInvitationAction_0.prototype = LoadEventInvitationAction.prototype = new Object_0;
_.getClass$ = function getClass_454(){
  return Lconsys_admin_event_gwt_client_action_LoadEventInvitationAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 348:1};
_.uuid_0 = null;
function $$init_454(){
}

function ResolveInvitationAction_0(uuid, accept){
  Object_1.call(this);
  $$init_454();
  this.uuid_0 = uuid;
  this.accept = accept;
}

function ResolveInvitationAction(){
}

_ = ResolveInvitationAction_0.prototype = ResolveInvitationAction.prototype = new Object_0;
_.getClass$ = function getClass_455(){
  return Lconsys_admin_event_gwt_client_action_ResolveInvitationAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 349:1};
_.accept = false;
_.uuid_0 = null;
function $getEvent(this$static){
  return this$static.event_0;
}

function $getInvitationDate(this$static){
  return this$static.invitationDate;
}

function $getMessage_1(this$static){
  return this$static.message_0;
}

function $getReponseDate(this$static){
  return this$static.reponseDate;
}

function $getState(this$static){
  return this$static.state;
}

function $getInvitations(this$static){
  return this$static.invitations;
}

function $getEvent_0(this$static){
  return this$static.event_0;
}

function $getState_0(this$static){
  return this$static.state;
}

function $getUuid(this$static){
  return this$static.uuid_0;
}

function $getList(this$static){
  return this$static.list;
}

function $isHasMore(this$static){
  return this$static.hasMore;
}

function $getUuid_0(this$static){
  return this$static.uuid_0;
}

_ = AdminEventConstants_.prototype;
_.allEventsMenu_title_allEvents = function allEventsMenu_title_allEvents(){
  return 'All events';
}
;
_.invitationDetailDialog_text_invited = function invitationDetailDialog_text_invited(){
  return 'Invited';
}
;
_.invitationDetailDialog_text_message = function invitationDetailDialog_text_message(){
  return 'Message';
}
;
_.invitationDetailDialog_text_responsed = function invitationDetailDialog_text_responsed(){
  return 'Responsed';
}
;
_.invitationDetailDialog_text_state = function invitationDetailDialog_text_state(){
  return 'Current state';
}
;
_.invitationsItem_text_accepted = function invitationsItem_text_accepted(){
  return 'Accepted';
}
;
_.invitationsItem_text_declined = function invitationsItem_text_declined(){
  return 'Declined';
}
;
_.invitationsItem_text_notResponse = function invitationsItem_text_notResponse(){
  return 'No response';
}
;
_.invitationsItem_text_state = function invitationsItem_text_state(){
  return 'State';
}
;
_.invitationsItem_text_timeouted = function invitationsItem_text_timeouted(){
  return 'Timeouted';
}
;
_.invitationsItem_text_unknownState = function invitationsItem_text_unknownState(){
  return 'Unknown state';
}
;
_.invitationsMenu_action_active = function invitationsMenu_action_active(){
  return 'Active';
}
;
_.invitationsMenu_action_inactive = function invitationsMenu_action_inactive(){
  return 'Inactive';
}
;
_.myEventsMenu_action_previous = function myEventsMenu_action_previous(){
  return 'Previous';
}
;
_.myEventsMenu_action_upcoming = function myEventsMenu_action_upcoming(){
  return 'Upcoming';
}
;
_.myEventsMenu_text_myEventsEmpty = function myEventsMenu_text_myEventsEmpty(){
  return 'This list of events is empty at the moment. You can register into an existing event in the \u201CAll events\u201D list, search for an event via the search box, or start a new event by clicking the \u201CAdd event\u201D button.';
}
;
_.myEventsMenu_text_show = function myEventsMenu_text_show(){
  return 'Show';
}
;
_.myEventsMenu_title_myEvents = function myEventsMenu_title_myEvents(){
  return 'My events';
}
;
_ = AdminEventMessages_.prototype;
_.eventsPanel_action_loadNextEvents = function eventsPanel_action_loadNextEvents(arg0){
  return $toString_12($append_4($append_2($append_4(new StringBuffer_0, 'Load next '), arg0), ' items'));
}
;
function $$init_475(this$static){
}

function $getNextLimit(this$static){
  var next;
  next = this$static.loadLimits.length - 1 == this$static.loadLimitIdx?this$static.loadLimitIdx:this$static.loadLimitIdx + 1;
  return this$static.loadLimits[next];
}

function $loadNextPage(this$static){
  this$static.delegate.loadNext(this$static.row * 3, $nextLimit(this$static), this$static);
}

function $nextLimit(this$static){
  this$static.loadLimitIdx = this$static.loadLimits.length - 1 == this$static.loadLimitIdx?this$static.loadLimitIdx:this$static.loadLimitIdx + 1;
  return this$static.loadLimits[this$static.loadLimitIdx];
}

function $reset(this$static){
  $clear_2(this$static, true);
  this$static.loadLimitIdx = -1;
  this$static.row = 0;
}

function $resetAndLoadFirstPage(this$static){
  $reset(this$static);
  $loadNextPage(this$static);
}

function $setItems(this$static, items, hasMore){
  var col, item, itemIdx, loadNextAction, nrow;
  for (itemIdx = 0; itemIdx < $size(items); ++itemIdx) {
    item = this$static.getNewItem($get(items, itemIdx));
    item.addStyleName('marB20');
    nrow = this$static.row + narrow_int(itemIdx / 3);
    col = itemIdx % 3;
    $setWidget(this$static, nrow, col, item);
    switch (col) {
      case 0:
        $setAlignment($getCellFormatter(this$static), nrow, col, ($clinit_89() , ALIGN_LEFT), ($clinit_90() , ALIGN_TOP));
        break;
      case 1:
        if (this$static.row == 0 && $size(items) == 2) {
          $setAlignment($getCellFormatter(this$static), nrow, col, ($clinit_89() , ALIGN_LEFT), ($clinit_90() , ALIGN_TOP));
          this$static.setWidth('');
          item.addStyleName('marL10');
        }
         else {
          $setAlignment($getCellFormatter(this$static), nrow, col, ($clinit_89() , ALIGN_CENTER), ($clinit_90() , ALIGN_TOP));
        }

        break;
      case 2:
        $setAlignment($getCellFormatter(this$static), nrow, col, ($clinit_89() , ALIGN_RIGHT), ($clinit_90() , ALIGN_TOP));
    }
  }
  this$static.row += narrow_int($size(items) / 3);
  if (hasMore) {
    loadNextAction = new ActionLabel_3(false, ($clinit_116() , m_1).eventsPanel_action_loadNextEvents($getNextLimit(this$static)), Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['fontBold', 'textConsysBlue'])));
    $setWidget(this$static, this$static.row + 1, 1, loadNextAction);
    $setAlignment($getCellFormatter(this$static), this$static.row + 1, 1, ($clinit_89() , ALIGN_CENTER), ($clinit_90() , ALIGN_TOP));
    $setClickHandler(loadNextAction, new AbstractEventsPanel$1_0(this$static));
  }
}

function AbstractEventsPanel_0(delegate){
  AbstractEventsPanel_1.call(this, delegate, initValues(_3I_classLit, {57:1, 60:1}, -1, [15, 24, 48]));
}

function AbstractEventsPanel_1(delegate, loadLimits){
  FlexTable_0.call(this);
  $$init_475(this);
  if (jsEquals(loadLimits, null) || loadLimits.length == 0) {
    throw new IllegalArgumentException_1('Musi byt nastaveny aspon jeden limit!');
  }
  this.loadLimits = loadLimits;
  this.delegate = delegate;
  this.setWidth('100%');
}

function AbstractEventsPanel(){
}

_ = AbstractEventsPanel.prototype = new FlexTable;
_.actionEnds = function actionEnds(){
  isNotNull(this.waiting) && $removeFromParent(this.waiting);
}
;
_.actionStarted = function actionStarted(){
  isNull(this.waiting) && (this.waiting = new Image_3('img/loader/loading.gif'));
  $setWidget(this, this.row + 1, 1, this.waiting);
  $setAlignment($getCellFormatter(this), this.row + 1, 1, ($clinit_89() , ALIGN_CENTER), ($clinit_90() , ALIGN_TOP));
}
;
_.getClass$ = function getClass_476(){
  return Lconsys_admin_event_gwt_client_module_event_AbstractEventsPanel_2_classLit;
}
;
_.onLoad = function onLoad_8(){
  $reset(this);
  $loadNextPage(this);
}
;
_.setFailMessage = function setFailMessage(value){
}
;
_.castableTypeMap$ = {2:1, 30:1, 31:1, 45:1, 46:1, 47:1, 49:1, 147:1, 319:1, 337:1};
_.delegate = null;
_.loadLimitIdx = -1;
_.loadLimits = null;
_.row = 0;
_.waiting = null;
function $$init_476(){
}

function AbstractEventsPanel$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_476();
}

function AbstractEventsPanel$1(){
}

_ = AbstractEventsPanel$1_0.prototype = AbstractEventsPanel$1.prototype = new Object_0;
_.getClass$ = function getClass_477(){
  return Lconsys_admin_event_gwt_client_module_event_AbstractEventsPanel$1_2_classLit;
}
;
_.onClick = function onClick_6(event_0){
  $loadNextPage(this.this$0);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $clinit_112(){
  $clinit_112 = nullMethod;
  $clinit_6();
  NAME = ($clinit_116() , c_0).allEventsMenu_title_allEvents();
}

function $$init_478(this$static){
}

function $dataAction(this$static, tag){
  this$static.isAttending = $equals_8(tag, 'attending');
  $resetAndLoadFirstPage(this$static.eventsPanel);
}

function AllEventsMenu_0(){
  $clinit_112();
  var previous, upcoming;
  SimpleFormPanel_0.call(this);
  $$init_478(this);
  this.setWidth('100%');
  ($clinit_117() , INSTANCE_4).css_1().ensureInjected();
  upcoming = new AllEventsMenu$1_0(this, true, 'attending', ($clinit_116() , c_0).myEventsMenu_action_upcoming());
  $addClickHandler_3(upcoming, new AllEventsMenu$2_0(this, upcoming));
  this.selected = upcoming;
  previous = new AllEventsMenu$3_0(this, false, 'attended', ($clinit_116() , c_0).myEventsMenu_action_previous());
  $addClickHandler_3(previous, new AllEventsMenu$4_0(this, previous));
  this.addWidget(getStyledLabel(($clinit_116() , c_0).myEventsMenu_text_show() + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [($clinit_117() , INSTANCE_4).css_1().show_0()])));
  this.addWidget(upcoming);
  this.addWidget(getStyledLabel('|', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [($clinit_117() , INSTANCE_4).css_1().showSeparator()])));
  this.addWidget(previous);
  this.addWidget(clearDiv_0());
  this.eventsPanel = new EventsPanel_0(this);
  this.addWidget(this.eventsPanel);
}

function AllEventsMenu(){
}

_ = AllEventsMenu_0.prototype = AllEventsMenu.prototype = new SimpleFormPanel;
_.getClass$ = function getClass_479(){
  return Lconsys_admin_event_gwt_client_module_event_AllEventsMenu_2_classLit;
}
;
_.getName = function getName_3(){
  return NAME;
}
;
_.getNote = function getNote(){
  return null;
}
;
_.loadNext = function loadNext(from, limit, panel){
  var prefix;
  prefix = corporationPrefix_0();
  $fireEvent_3(get_10(), new DispatchEvent_0(new ListAllEventsAction_0(this.isAttending, from, limit, prefix), new AllEventsMenu$5_0(this), this.eventsPanel));
}
;
_.onShow = function onShow(panel){
  panel.setWidget(this);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.eventsPanel = null;
_.isAttending = true;
_.selected = null;
var NAME;
function $$init_479(){
}

function $addClickHandler_3(this$static, handler){
  $addDomHandler(this$static, handler, getType_10());
}

function $getTag_1(this$static){
  return this$static.tag;
}

function $selected(this$static){
  this$static.removeStyleName(system_0().css_7().dataListCommonOrdererOrder());
  addStyleNameIfNotSet(this$static, system_0().css_7().dataListCommonOrdererOrderSelected());
  this$static.fireSelect(this$static);
}

function $unselected(this$static){
  this$static.removeStyleName(system_0().css_7().dataListCommonOrdererOrderSelected());
  addStyleNameIfNotSet(this$static, system_0().css_7().dataListCommonOrdererOrder());
}

function EventFilterSelector_0(selected, tag, text){
  SimplePanel_0.call(this);
  $$init_479();
  this.tag = tag;
  this.text = text;
  this.setStyleName('inline');
  this.addStyleName('floatL');
  selected?addStyleNameIfNotSet(this, system_0().css_7().dataListCommonOrdererOrderSelected()):$unselected(this);
}

function EventFilterSelector(){
}

_ = EventFilterSelector.prototype = new SimplePanel;
_.getClass$ = function getClass_480(){
  return Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit;
}
;
_.onLoad = function onLoad_9(){
  var l_0;
  l_0 = new Label_1(this.text);
  this.setWidget(l_0);
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1};
_.tag = null;
_.text = null;
function $$init_480(){
}

function AllEventsMenu$1_0(this$0, $anonymous0, $anonymous1, $anonymous2){
  $clinit_6();
  this.this$0 = this$0;
  EventFilterSelector_0.call(this, $anonymous0, $anonymous1, $anonymous2);
  $$init_480();
}

function AllEventsMenu$1(){
}

_ = AllEventsMenu$1_0.prototype = AllEventsMenu$1.prototype = new EventFilterSelector;
_.fireSelect = function fireSelect(selector){
  $dataAction(this.this$0, $getTag_1(this));
}
;
_.getClass$ = function getClass_481(){
  return Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$1_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1};
_.this$0 = null;
function $$init_481(){
}

function AllEventsMenu$2_0(this$0, val$upcoming){
  this.this$0 = this$0;
  this.val$upcoming = val$upcoming;
  Object_1.call(this);
  $$init_481();
}

function AllEventsMenu$2(){
}

_ = AllEventsMenu$2_0.prototype = AllEventsMenu$2.prototype = new Object_0;
_.getClass$ = function getClass_482(){
  return Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$2_2_classLit;
}
;
_.onClick = function onClick_7(event_0){
  $selected(this.val$upcoming);
  jsNotEquals(this.this$0.selected, this.val$upcoming) && $unselected(this.this$0.selected);
  this.this$0.selected = this.val$upcoming;
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$upcoming = null;
function $$init_482(){
}

function AllEventsMenu$3_0(this$0, $anonymous0, $anonymous1, $anonymous2){
  $clinit_6();
  this.this$0 = this$0;
  EventFilterSelector_0.call(this, $anonymous0, $anonymous1, $anonymous2);
  $$init_482();
}

function AllEventsMenu$3(){
}

_ = AllEventsMenu$3_0.prototype = AllEventsMenu$3.prototype = new EventFilterSelector;
_.fireSelect = function fireSelect_0(selector){
  $dataAction(this.this$0, $getTag_1(this));
}
;
_.getClass$ = function getClass_483(){
  return Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$3_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1};
_.this$0 = null;
function $$init_483(){
}

function AllEventsMenu$4_0(this$0, val$previous){
  this.this$0 = this$0;
  this.val$previous = val$previous;
  Object_1.call(this);
  $$init_483();
}

function AllEventsMenu$4(){
}

_ = AllEventsMenu$4_0.prototype = AllEventsMenu$4.prototype = new Object_0;
_.getClass$ = function getClass_484(){
  return Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$4_2_classLit;
}
;
_.onClick = function onClick_8(event_0){
  $selected(this.val$previous);
  jsNotEquals(this.this$0.selected, this.val$previous) && $unselected(this.this$0.selected);
  this.this$0.selected = this.val$previous;
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$previous = null;
function $$init_484(){
}

function $onSuccess_3(this$static, result){
  $setItems(this$static.this$0.eventsPanel, $getList(result), $isHasMore(result));
}

function AllEventsMenu$5_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_484();
}

function AllEventsMenu$5(){
}

_ = AllEventsMenu$5_0.prototype = AllEventsMenu$5.prototype = new Object_0;
_.getClass$ = function getClass_485(){
  return Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$5_2_classLit;
}
;
_.onFailure = function onFailure_1(caught){
}
;
_.onSuccess = function onSuccess_3(result){
  $onSuccess_3(this, dynamicCast(result, 356));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_496(){
}

function $commonStyledLabel(text, styles){
  var i, label;
  label = getStyledLabel(text, initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marH3', 'marB3', 'f11px', 'al']));
  for (i = 0; i < styles.length; ++i) {
    label.addStyleName(styles[i]);
  }
  return label;
}

function $date(event_0){
  var date;
  date = valueOf_61($getYear_2(event_0));
  isNotNull($getFrom_2(event_0)) && (date = eventDate(event_0));
  return date;
}

function $initHandlers(this$static, eventThumb){
  $addHandler(this$static, new EventItem$1_0(this$static), getType_20());
  $addHandler(this$static, new EventItem$2_0(this$static), getType_19());
  $addClickHandler_0(this$static.focusPanel, new EventItem$3_0(this$static, eventThumb));
}

function $logo(event_0){
  var logo, panel;
  logo = new Image_1;
  jsEquals($getLogoUuidPrefix_0(event_0), null)?$setResource(logo, system_0().confLogoSmall()):$setUrl_0(logo, ($clinit_148() , BASE_URL) + 'user/profile/load?id=' + $getFullUuid(($clinit_137() , LIST), $getLogoUuidPrefix_0(event_0)));
  panel = new SimplePanel_0;
  panel.addStyleName('al');
  panel.setWidget(logo);
  return panel;
}

function EventItem_0(event_0){
  var content_0;
  Composite_0.call(this);
  $$init_496();
  content_0 = new FlowPanel_0;
  content_0.add_1($logo(event_0));
  content_0.add_1($commonStyledLabel($getFullName_0(event_0), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['fontBold', 'marT5'])));
  content_0.add_1($commonStyledLabel($date(event_0), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [])));
  content_0.add_1($commonStyledLabel($getWeb_0(event_0), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [])));
  this.mainPanel = new SimplePanel_0;
  this.mainPanel.addStyleName(Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['cursorHand', 'borderLGray'])));
  this.mainPanel.setWidget(content_0);
  setStyleAttribute($getElement(this.mainPanel), 'minHeight', '90px');
  this.focusPanel = new FocusPanel_1(this.mainPanel);
  this.focusPanel.setWidth('227px');
  $initWidget(this, this.focusPanel);
  this.sinkEvents(48);
  $initHandlers(this, event_0);
}

function EventItem(){
}

_ = EventItem_0.prototype = EventItem.prototype = new Composite;
_.getClass$ = function getClass_497(){
  return Lconsys_admin_event_gwt_client_module_event_EventItem_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.focusPanel = null;
_.mainPanel = null;
function $$init_497(){
}

function EventItem$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_497();
}

function EventItem$1(){
}

_ = EventItem$1_0.prototype = EventItem$1.prototype = new Object_0;
_.getClass$ = function getClass_498(){
  return Lconsys_admin_event_gwt_client_module_event_EventItem$1_2_classLit;
}
;
_.onMouseOver = function onMouseOver(event_0){
  this.this$0.mainPanel.addStyleName('inputBorderGray');
  this.this$0.mainPanel.removeStyleName('borderLGray');
}
;
_.castableTypeMap$ = {82:1, 101:1};
_.this$0 = null;
function $$init_498(){
}

function EventItem$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_498();
}

function EventItem$2(){
}

_ = EventItem$2_0.prototype = EventItem$2.prototype = new Object_0;
_.getClass$ = function getClass_499(){
  return Lconsys_admin_event_gwt_client_module_event_EventItem$2_2_classLit;
}
;
_.onMouseOut = function onMouseOut(event_0){
  this.this$0.mainPanel.addStyleName('borderLGray');
  this.this$0.mainPanel.removeStyleName('inputBorderGray');
}
;
_.castableTypeMap$ = {81:1, 101:1};
_.this$0 = null;
function $$init_499(){
}

function EventItem$3_0(this$0, val$eventThumb){
  this , this$0;
  this.val$eventThumb = val$eventThumb;
  Object_1.call(this);
  $$init_499();
}

function EventItem$3(){
}

_ = EventItem$3_0.prototype = EventItem$3.prototype = new Object_0;
_.getClass$ = function getClass_500(){
  return Lconsys_admin_event_gwt_client_module_event_EventItem$3_2_classLit;
}
;
_.onClick = function onClick_11(cevent){
  newItem_0('Event?event=' + $getUuid_8(this.val$eventThumb));
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.val$eventThumb = null;
function $$init_501(){
}

function EventMenuContent_0(){
  $clinit_6();
  var addEvent;
  MenuFormPanel_0.call(this, ($clinit_116() , c_0).const_events());
  $$init_501();
  $addHandler_5(get_10(), ($clinit_111() , TYPE_29), this);
  addEvent = getPlusButton(($clinit_116() , c_0).const_addEvent(), true);
  $addClickHandler_4(addEvent, new EventMenuContent$1_0(this));
  $addLeftControll(this, addEvent);
  $addItem(this, new MyEventsMenu_0);
  $addItem(this, new AllEventsMenu_0);
  $addItem(this, new InvitationsMenu_0);
  $initWidget_0(this);
}

function EventMenuContent(){
}

_ = EventMenuContent_0.prototype = EventMenuContent.prototype = new MenuFormPanel;
_.getClass$ = function getClass_502(){
  return Lconsys_admin_event_gwt_client_module_event_EventMenuContent_2_classLit;
}
;
_.onCreatedNewEvent = function onCreatedNewEvent(event_0){
  $showItem(this, ($clinit_115() , NAME_1));
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 101:1, 147:1, 358:1, 359:1};
var instance_10 = null;
function $$init_502(){
}

function EventMenuContent$1_0(this$0){
  this , this$0;
  Object_1.call(this);
  $$init_502();
}

function EventMenuContent$1(){
}

_ = EventMenuContent$1_0.prototype = EventMenuContent$1.prototype = new Object_0;
_.getClass$ = function getClass_503(){
  return Lconsys_admin_event_gwt_client_module_event_EventMenuContent$1_2_classLit;
}
;
_.onClick = function onClick_12(event_0){
  newItem_0('CreateEvent');
}
;
_.castableTypeMap$ = {67:1, 101:1};
function $onSuccess_6(this$static){
  isNull(($clinit_6() , instance_10)) && ($clinit_6() , instance_10 = new EventMenuContent_0);
  this$static.val$asyncModule.onSuccess(($clinit_6() , instance_10));
}

function $$init_504(){
}

function $getNewItem(data){
  return new EventItem_0(data);
}

function EventsPanel_0(delegate){
  $clinit_6();
  AbstractEventsPanel_0.call(this, delegate);
  $$init_504();
}

function EventsPanel(){
}

_ = EventsPanel_0.prototype = EventsPanel.prototype = new AbstractEventsPanel;
_.getClass$ = function getClass_505(){
  return Lconsys_admin_event_gwt_client_module_event_EventsPanel_2_classLit;
}
;
_.getNewItem = function getNewItem(data){
  return $getNewItem(dynamicCast(data, 351));
}
;
_.castableTypeMap$ = {2:1, 30:1, 31:1, 45:1, 46:1, 47:1, 49:1, 147:1, 319:1, 337:1};
function $$init_507(){
}

function $canResponse(state){
  return !($equals_0(state, ($clinit_108() , ACCEPTED)) || $equals_0(state, ($clinit_108() , DECLINED)) || $equals_0(state, ($clinit_108() , TIMEOUTED)));
}

function $closeCross(this$static){
  var close_0;
  close_0 = new Image_2(system_0().removeCross());
  close_0.setStyleName('cursorHand');
  close_0.addStyleName('floatR');
  close_0.addStyleName('marT10');
  close_0.addStyleName('marR10');
  $addClickHandler_2(close_0, $hideClickHandler(this$static));
  return close_0;
}

function $commonStyledLabel_0(text){
  return getStyledLabel(text + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL20', 'floatL', 'marR5']));
}

function $controlPanel(this$static, invitation){
  var acc, canResponse, cpanel, dec;
  canResponse = $canResponse($getState(invitation));
  acc = acceptButton();
  $setEnabled_4(acc, canResponse);
  acc.addStyleName('floatL');
  acc.addStyleName('marR20');
  $addClickHandler_5(acc, $resultClickHandler(this$static, true));
  dec = declineButton();
  $setEnabled_4(dec, canResponse);
  dec.addStyleName('floatL');
  $addClickHandler_5(dec, $resultClickHandler(this$static, false));
  cpanel = new FlowPanel_0;
  cpanel.addStyleName('padV10');
  cpanel.addStyleName('marL20');
  cpanel.add_1(acc);
  cpanel.add_1(dec);
  cpanel.add_1(clearDiv_0());
  return cpanel;
}

function $generateTexts(invitation, panel){
  var message, title;
  title = $getAcronym($getEvent(invitation)) + ' ' + $getYear_1($getEvent(invitation));
  message = getStyledLabel($getMessage_1(invitation), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marV5', 'floatL']));
  message.setWidth('300px');
  panel.addWidget(getStyledLabel(title, initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL20', 'f16px', 'fontBold', 'padV10'])));
  panel.addWidget($commonStyledLabel_0(($clinit_116() , c_0).invitationDetailDialog_text_state()));
  panel.addWidget(getStyledLabel(getStateString($getState(invitation)), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['floatL'])));
  panel.addWidget(clearDiv_1('5px'));
  panel.addWidget($commonStyledLabel_0(($clinit_116() , c_0).invitationDetailDialog_text_invited()));
  panel.addWidget(new ClientDate_0($getInvitationDate(invitation), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['floatL'])));
  if (!$canResponse($getState(invitation)) && isNotNull($getReponseDate(invitation))) {
    panel.addWidget(clearDiv_1('5px'));
    panel.addWidget($commonStyledLabel_0(($clinit_116() , c_0).invitationDetailDialog_text_responsed()));
    panel.addWidget(new ClientDate_0($getReponseDate(invitation), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['floatL'])));
  }
  panel.addWidget(clearDiv_0());
  panel.addWidget(getStyledLabel(($clinit_116() , c_0).invitationDetailDialog_text_message() + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL20', 'floatL', 'marR5', 'marV5'])));
  panel.addWidget(message);
  panel.addWidget(clearDiv_0());
}

function $initContent(this$static, invitation, panel){
  panel.clear();
  panel.addWidget($closeCross(this$static));
  $generateTexts(invitation, panel);
  panel.addWidget($controlPanel(this$static, invitation));
}

function $resultClickHandler(this$static, accept){
  return new InvitationDetailDialog$2_0(this$static, accept);
}

function InvitationDetailDialog_0(invitationUuid, parent_0){
  $clinit_95();
  SmartDialog_0.call(this);
  $$init_507();
  this.invitationUuid = invitationUuid;
  this.parent_0 = parent_0;
}

function InvitationDetailDialog(){
}

_ = InvitationDetailDialog_0.prototype = InvitationDetailDialog.prototype = new SmartDialog;
_.generateContent = function generateContent(panel){
  setStyleAttribute($getElement(panel), 'minHeight', '100px');
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadEventInvitationAction_0(this.invitationUuid), new InvitationDetailDialog$1_0(this, panel), this));
}
;
_.getClass$ = function getClass_508(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 11:1, 19:1, 45:1, 46:1, 47:1, 49:1, 134:1, 147:1, 337:1};
_.invitationUuid = null;
_.parent_0 = null;
function $$init_508(){
}

function $onSuccess_7(this$static, result){
  $initContent(this$static.this$0, result, this$static.val$panel);
}

function InvitationDetailDialog$1_0(this$0, val$panel){
  this.this$0 = this$0;
  this.val$panel = val$panel;
  Object_1.call(this);
  $$init_508();
}

function InvitationDetailDialog$1(){
}

_ = InvitationDetailDialog$1_0.prototype = InvitationDetailDialog$1.prototype = new Object_0;
_.getClass$ = function getClass_509(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog$1_2_classLit;
}
;
_.onFailure = function onFailure_5(caught){
}
;
_.onSuccess = function onSuccess_6(result){
  $onSuccess_7(this, dynamicCast(result, 355));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
_.val$panel = null;
function $$init_509(){
}

function InvitationDetailDialog$2_0(this$0, val$accept){
  this.this$0 = this$0;
  this.val$accept = val$accept;
  Object_1.call(this);
  $$init_509();
}

function InvitationDetailDialog$2(){
}

_ = InvitationDetailDialog$2_0.prototype = InvitationDetailDialog$2.prototype = new Object_0;
_.getClass$ = function getClass_510(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog$2_2_classLit;
}
;
_.onClick = function onClick_13(event_0){
  $fireEvent_3(get_10(), new DispatchEvent_0(new ResolveInvitationAction_0(this.this$0.invitationUuid, this.val$accept), new InvitationDetailDialog$2$1_0(this), this.this$0));
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$accept = false;
function $$init_510(){
}

function $onSuccess_8(this$static){
  this$static.this$1.this$0.hide_0();
  $resetAndLoadFirstPage(this$static.this$1.this$0.parent_0);
}

function InvitationDetailDialog$2$1_0(this$1){
  this.this$1 = this$1;
  Object_1.call(this);
  $$init_510();
}

function InvitationDetailDialog$2$1(){
}

_ = InvitationDetailDialog$2$1_0.prototype = InvitationDetailDialog$2$1.prototype = new Object_0;
_.getClass$ = function getClass_511(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog$2$1_2_classLit;
}
;
_.onFailure = function onFailure_6(caught){
}
;
_.onSuccess = function onSuccess_7(result){
  $onSuccess_8(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
function $$init_511(){
}

function $initHandlers_0(this$static, thumb){
  $addHandler(this$static, new InvitationsItem$1_0(this$static), getType_20());
  $addHandler(this$static, new InvitationsItem$2_0(this$static), getType_19());
  $addClickHandler_0(this$static.focusPanel, new InvitationsItem$3_0(this$static, thumb));
}

function $logo_0(event_0){
  var logo, panel;
  logo = new Image_1;
  jsEquals($getLogoUuidPrefix_0(event_0), null)?$setResource(logo, system_0().confLogoSmall()):$setUrl_0(logo, ($clinit_148() , BASE_URL) + 'user/profile/load?id=' + $getFullUuid(($clinit_137() , LIST), $getLogoUuidPrefix_0(event_0)));
  panel = new SimplePanel_0;
  panel.addStyleName('al');
  panel.setWidget(logo);
  return panel;
}

function InvitationsItem_0(thumb, parent_0){
  Composite_0.call(this);
  $$init_511();
  this.parent_0 = parent_0;
  this.content_0 = new FlowPanel_0;
  this.content_0.addStyleName('borderLGray');
  this.content_0.add_1($logo_0($getEvent_0(thumb)));
  this.content_0.add_1(getStyledLabel($getFullName_0($getEvent_0(thumb)), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marH3', 'marV5', 'fontBold', 'f11px', 'al'])));
  this.content_0.add_1(getStyledLabel(($clinit_116() , c_0).invitationsItem_text_state() + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['floatL', 'marH3', 'f11px'])));
  this.content_0.add_1(getStyledLabel(getStateString($getState_0(thumb)), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['floatL', 'f11px'])));
  this.content_0.add_1(clearDiv_1('5px'));
  this.focusPanel = new FocusPanel_0;
  this.focusPanel.setWidth('227px');
  this.focusPanel.addStyleName('cursorHand');
  this.focusPanel.setWidget(this.content_0);
  $initWidget(this, this.focusPanel);
  this.sinkEvents(48);
  $initHandlers_0(this, thumb);
}

function getStateString(state){
  $clinit_6();
  switch ($ordinal(state)) {
    case 1:
      return ($clinit_116() , c_0).invitationsItem_text_accepted();
    case 2:
      return ($clinit_116() , c_0).invitationsItem_text_declined();
    case 0:
      return ($clinit_116() , c_0).invitationsItem_text_notResponse();
    case 3:
      return ($clinit_116() , c_0).invitationsItem_text_timeouted();
    default:return ($clinit_116() , c_0).invitationsItem_text_unknownState();
  }
}

function InvitationsItem(){
}

_ = InvitationsItem_0.prototype = InvitationsItem.prototype = new Composite;
_.getClass$ = function getClass_512(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsItem_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.content_0 = null;
_.focusPanel = null;
_.parent_0 = null;
function $$init_512(){
}

function InvitationsItem$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_512();
}

function InvitationsItem$1(){
}

_ = InvitationsItem$1_0.prototype = InvitationsItem$1.prototype = new Object_0;
_.getClass$ = function getClass_513(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsItem$1_2_classLit;
}
;
_.onMouseOver = function onMouseOver_0(event_0){
  this.this$0.content_0.addStyleName('inputBorderGray');
  this.this$0.content_0.removeStyleName('borderLGray');
}
;
_.castableTypeMap$ = {82:1, 101:1};
_.this$0 = null;
function $$init_513(){
}

function InvitationsItem$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_513();
}

function InvitationsItem$2(){
}

_ = InvitationsItem$2_0.prototype = InvitationsItem$2.prototype = new Object_0;
_.getClass$ = function getClass_514(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsItem$2_2_classLit;
}
;
_.onMouseOut = function onMouseOut_0(event_0){
  this.this$0.content_0.addStyleName('borderLGray');
  this.this$0.content_0.removeStyleName('inputBorderGray');
}
;
_.castableTypeMap$ = {81:1, 101:1};
_.this$0 = null;
function $$init_514(){
}

function InvitationsItem$3_0(this$0, val$thumb){
  this.this$0 = this$0;
  this.val$thumb = val$thumb;
  Object_1.call(this);
  $$init_514();
}

function InvitationsItem$3(){
}

_ = InvitationsItem$3_0.prototype = InvitationsItem$3.prototype = new Object_0;
_.getClass$ = function getClass_515(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsItem$3_2_classLit;
}
;
_.onClick = function onClick_14(cevent){
  var d;
  d = new InvitationDetailDialog_0($getUuid(this.val$thumb), this.this$0.parent_0);
  $showCentered(d);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$thumb = null;
function $$init_515(this$static){
}

function $dataAction_0(this$static, tag){
  this$static.isActive = $equals_8(tag, 'active');
  $resetAndLoadFirstPage(this$static.invitationsPanel);
}

function InvitationsMenu_0(){
  $clinit_114();
  var active, inactive;
  SimpleFormPanel_0.call(this);
  $$init_515(this);
  ($clinit_117() , INSTANCE_4).css_1().ensureInjected();
  active = new InvitationsMenu$1_0(this, true, 'active', ($clinit_116() , c_0).invitationsMenu_action_active());
  $addClickHandler_3(active, new InvitationsMenu$2_0(this, active));
  this.selected = active;
  inactive = new InvitationsMenu$3_0(this, false, 'inactive', ($clinit_116() , c_0).invitationsMenu_action_inactive());
  $addClickHandler_3(inactive, new InvitationsMenu$4_0(this, inactive));
  this.addWidget(getStyledLabel(($clinit_116() , c_0).myEventsMenu_text_show() + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [($clinit_117() , INSTANCE_4).css_1().show_0()])));
  this.addWidget(active);
  this.addWidget(getStyledLabel('|', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [($clinit_117() , INSTANCE_4).css_1().showSeparator()])));
  this.addWidget(inactive);
  this.addWidget(clearDiv_0());
  this.invitationsPanel = new InvitationsPanel_0(this);
  this.addWidget(this.invitationsPanel);
}

function InvitationsMenu(){
}

_ = InvitationsMenu_0.prototype = InvitationsMenu.prototype = new SimpleFormPanel;
_.getClass$ = function getClass_516(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsMenu_2_classLit;
}
;
_.getName = function getName_4(){
  return NAME_0;
}
;
_.getNote = function getNote_0(){
  return null;
}
;
_.loadNext = function loadNext_0(from, limit, panel){
  $fireEvent_3(get_10(), new DispatchEvent_0(new ListMyEventInvitationsAction_0(this.isActive, from, limit), new InvitationsMenu$5_0(this), this.invitationsPanel));
}
;
_.onShow = function onShow_0(panel){
  panel.setWidget(this);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.invitationsPanel = null;
_.isActive = true;
_.selected = null;
function $$init_516(){
}

function InvitationsMenu$1_0(this$0, $anonymous0, $anonymous1, $anonymous2){
  $clinit_6();
  this.this$0 = this$0;
  EventFilterSelector_0.call(this, $anonymous0, $anonymous1, $anonymous2);
  $$init_516();
}

function InvitationsMenu$1(){
}

_ = InvitationsMenu$1_0.prototype = InvitationsMenu$1.prototype = new EventFilterSelector;
_.fireSelect = function fireSelect_1(selector){
  $dataAction_0(this.this$0, $getTag_1(this));
}
;
_.getClass$ = function getClass_517(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$1_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1};
_.this$0 = null;
function $$init_517(){
}

function InvitationsMenu$2_0(this$0, val$active){
  this.this$0 = this$0;
  this.val$active = val$active;
  Object_1.call(this);
  $$init_517();
}

function InvitationsMenu$2(){
}

_ = InvitationsMenu$2_0.prototype = InvitationsMenu$2.prototype = new Object_0;
_.getClass$ = function getClass_518(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$2_2_classLit;
}
;
_.onClick = function onClick_15(event_0){
  $selected(this.val$active);
  jsNotEquals(this.this$0.selected, this.val$active) && $unselected(this.this$0.selected);
  this.this$0.selected = this.val$active;
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$active = null;
function $$init_518(){
}

function InvitationsMenu$3_0(this$0, $anonymous0, $anonymous1, $anonymous2){
  $clinit_6();
  this.this$0 = this$0;
  EventFilterSelector_0.call(this, $anonymous0, $anonymous1, $anonymous2);
  $$init_518();
}

function InvitationsMenu$3(){
}

_ = InvitationsMenu$3_0.prototype = InvitationsMenu$3.prototype = new EventFilterSelector;
_.fireSelect = function fireSelect_2(selector){
  $dataAction_0(this.this$0, $getTag_1(this));
}
;
_.getClass$ = function getClass_519(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$3_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1};
_.this$0 = null;
function $$init_519(){
}

function InvitationsMenu$4_0(this$0, val$inactive){
  this.this$0 = this$0;
  this.val$inactive = val$inactive;
  Object_1.call(this);
  $$init_519();
}

function InvitationsMenu$4(){
}

_ = InvitationsMenu$4_0.prototype = InvitationsMenu$4.prototype = new Object_0;
_.getClass$ = function getClass_520(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$4_2_classLit;
}
;
_.onClick = function onClick_16(event_0){
  $selected(this.val$inactive);
  jsNotEquals(this.this$0.selected, this.val$inactive) && $unselected(this.this$0.selected);
  this.this$0.selected = this.val$inactive;
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$inactive = null;
function $$init_520(){
}

function $onSuccess_9(this$static, result){
  $setItems(this$static.this$0.invitationsPanel, $getInvitations(result), false);
}

function InvitationsMenu$5_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_520();
}

function InvitationsMenu$5(){
}

_ = InvitationsMenu$5_0.prototype = InvitationsMenu$5.prototype = new Object_0;
_.getClass$ = function getClass_521(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$5_2_classLit;
}
;
_.onFailure = function onFailure_7(caught){
}
;
_.onSuccess = function onSuccess_8(result){
  $onSuccess_9(this, dynamicCast(result, 350));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_521(){
}

function $getNewItem_0(this$static, data){
  return new InvitationsItem_0(data, this$static);
}

function InvitationsPanel_0(delegate){
  $clinit_6();
  AbstractEventsPanel_0.call(this, delegate);
  $$init_521();
}

function InvitationsPanel(){
}

_ = InvitationsPanel_0.prototype = InvitationsPanel.prototype = new AbstractEventsPanel;
_.getClass$ = function getClass_522(){
  return Lconsys_admin_event_gwt_client_module_event_InvitationsPanel_2_classLit;
}
;
_.getNewItem = function getNewItem_0(data){
  return $getNewItem_0(this, dynamicCast(data, 352));
}
;
_.castableTypeMap$ = {2:1, 30:1, 31:1, 45:1, 46:1, 47:1, 49:1, 147:1, 319:1, 337:1};
function $clinit_115(){
  $clinit_115 = nullMethod;
  $clinit_6();
  NAME_1 = ($clinit_116() , c_0).myEventsMenu_title_myEvents();
}

function $$init_522(this$static){
}

function $dataAction_1(this$static, tag){
  this$static.isAttending = $equals_8(tag, 'attending');
  $resetAndLoadFirstPage(this$static.eventsPanel);
}

function MyEventsMenu_0(){
  $clinit_115();
  var previous, upcoming;
  SimpleFormPanel_0.call(this);
  $$init_522(this);
  $setBodyWidth(this, '100%');
  $addHandler_5(get_10(), ($clinit_111() , TYPE_29), this);
  ($clinit_117() , INSTANCE_4).css_1().ensureInjected();
  upcoming = new MyEventsMenu$1_0(this, true, 'attending', ($clinit_116() , c_0).myEventsMenu_action_upcoming());
  $addClickHandler_3(upcoming, new MyEventsMenu$2_0(this, upcoming));
  this.selected = upcoming;
  previous = new MyEventsMenu$3_0(this, false, 'attended', ($clinit_116() , c_0).myEventsMenu_action_previous());
  $addClickHandler_3(previous, new MyEventsMenu$4_0(this, previous));
  this.addWidget(getStyledLabel(($clinit_116() , c_0).myEventsMenu_text_show() + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [($clinit_117() , INSTANCE_4).css_1().show_0()])));
  this.addWidget(upcoming);
  this.addWidget(getStyledLabel('|', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [($clinit_117() , INSTANCE_4).css_1().showSeparator()])));
  this.addWidget(previous);
  this.addWidget(clearDiv_0());
  this.eventsPanel = new EventsPanel_0(this);
  this.addWidget(this.eventsPanel);
}

function MyEventsMenu(){
}

_ = MyEventsMenu_0.prototype = MyEventsMenu.prototype = new SimpleFormPanel;
_.getClass$ = function getClass_523(){
  return Lconsys_admin_event_gwt_client_module_event_MyEventsMenu_2_classLit;
}
;
_.getName = function getName_5(){
  return NAME_1;
}
;
_.getNote = function getNote_1(){
  return null;
}
;
_.loadNext = function loadNext_1(from, limit, panel){
  var prefix;
  prefix = corporationPrefix_0();
  $fireEvent_3(get_10(), new DispatchEvent_0(new ListMyEventsAction_0(this.isAttending, from, limit, prefix), new MyEventsMenu$5_0(this), this.eventsPanel));
}
;
_.onCreatedNewEvent = function onCreatedNewEvent_0(event_0){
  $clearMessageBox(this);
  $register_0(get_11(), 'new_event_created', $getUuid_0(event_0));
  newItem_0('Event?event=' + $getUuid_0(event_0));
  $fireEvent_3(get_10(), new ChangeContentEvent_0(new EventEntranceContent_0));
}
;
_.onShow = function onShow_1(panel){
  panel.setWidget(this);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 101:1, 147:1, 358:1};
_.eventsPanel = null;
_.isAttending = true;
_.selected = null;
var NAME_1;
function $$init_523(){
}

function MyEventsMenu$1_0(this$0, $anonymous0, $anonymous1, $anonymous2){
  $clinit_6();
  this.this$0 = this$0;
  EventFilterSelector_0.call(this, $anonymous0, $anonymous1, $anonymous2);
  $$init_523();
}

function MyEventsMenu$1(){
}

_ = MyEventsMenu$1_0.prototype = MyEventsMenu$1.prototype = new EventFilterSelector;
_.fireSelect = function fireSelect_3(selector){
  $dataAction_1(this.this$0, $getTag_1(this));
}
;
_.getClass$ = function getClass_524(){
  return Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$1_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1};
_.this$0 = null;
function $$init_524(){
}

function MyEventsMenu$2_0(this$0, val$upcoming){
  this.this$0 = this$0;
  this.val$upcoming = val$upcoming;
  Object_1.call(this);
  $$init_524();
}

function MyEventsMenu$2(){
}

_ = MyEventsMenu$2_0.prototype = MyEventsMenu$2.prototype = new Object_0;
_.getClass$ = function getClass_525(){
  return Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$2_2_classLit;
}
;
_.onClick = function onClick_17(event_0){
  $selected(this.val$upcoming);
  jsNotEquals(this.this$0.selected, this.val$upcoming) && $unselected(this.this$0.selected);
  this.this$0.selected = this.val$upcoming;
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$upcoming = null;
function $$init_525(){
}

function MyEventsMenu$3_0(this$0, $anonymous0, $anonymous1, $anonymous2){
  $clinit_6();
  this.this$0 = this$0;
  EventFilterSelector_0.call(this, $anonymous0, $anonymous1, $anonymous2);
  $$init_525();
}

function MyEventsMenu$3(){
}

_ = MyEventsMenu$3_0.prototype = MyEventsMenu$3.prototype = new EventFilterSelector;
_.fireSelect = function fireSelect_4(selector){
  $dataAction_1(this.this$0, $getTag_1(this));
}
;
_.getClass$ = function getClass_526(){
  return Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$3_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1};
_.this$0 = null;
function $$init_526(){
}

function MyEventsMenu$4_0(this$0, val$previous){
  this.this$0 = this$0;
  this.val$previous = val$previous;
  Object_1.call(this);
  $$init_526();
}

function MyEventsMenu$4(){
}

_ = MyEventsMenu$4_0.prototype = MyEventsMenu$4.prototype = new Object_0;
_.getClass$ = function getClass_527(){
  return Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$4_2_classLit;
}
;
_.onClick = function onClick_18(event_0){
  $selected(this.val$previous);
  jsNotEquals(this.this$0.selected, this.val$previous) && $unselected(this.this$0.selected);
  this.this$0.selected = this.val$previous;
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_.val$previous = null;
function $$init_527(){
}

function $onSuccess_10(this$static, result){
  $setItems(this$static.this$0.eventsPanel, $getList(result), $isHasMore(result));
}

function MyEventsMenu$5_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_527();
}

function MyEventsMenu$5(){
}

_ = MyEventsMenu$5_0.prototype = MyEventsMenu$5.prototype = new Object_0;
_.getClass$ = function getClass_528(){
  return Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$5_2_classLit;
}
;
_.onFailure = function onFailure_8(caught){
  var m_0;
  if (instanceOf(caught, 365)) {
    $clearMessageBox(this.this$0);
    if (this.this$0.isAttending) {
      m_0 = $getSuccessMessage_0(this.this$0, false);
      $setText_9(m_0, ($clinit_116() , c_0).myEventsMenu_text_myEventsEmpty(), true);
    }
  }
}
;
_.onSuccess = function onSuccess_9(result){
  $onSuccess_10(this, dynamicCast(result, 356));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $clinit_117(){
  $clinit_117 = nullMethod;
  INSTANCE_4 = new AdminEventResources_default_InlineClientBundleGenerator_0;
}

var INSTANCE_4;
function $clinit_118(){
  $clinit_118 = nullMethod;
  _instance0_0 = new AdminEventResources_default_InlineClientBundleGenerator_0;
}

function $$init_528(){
}

function $css_2(){
  return get_8();
}

function $cssInitializer_0(this$static){
  css_2 = new AdminEventResources_default_InlineClientBundleGenerator$1_0(this$static);
}

function AdminEventResources_default_InlineClientBundleGenerator_0(){
  $clinit_118();
  Object_1.call(this);
  $$init_528();
}

function AdminEventResources_default_InlineClientBundleGenerator(){
}

_ = AdminEventResources_default_InlineClientBundleGenerator_0.prototype = AdminEventResources_default_InlineClientBundleGenerator.prototype = new Object_0;
_.css_1 = function css_3(){
  return $css_2();
}
;
_.getClass$ = function getClass_529(){
  return Lconsys_admin_event_gwt_client_utils_AdminEventResources_1default_1InlineClientBundleGenerator_2_classLit;
}
;
_.castableTypeMap$ = {};
var _instance0_0, css_2 = null;
function $$init_529(){
}

function $getText_3(){
  return $isRTL_0(getCurrentLocale())?'.GCOOUJ4DKG{margin:3px 0 10px 10px;font-weight:bold;float:right;}.GCOOUJ4DLG{margin:3px 5px 0 5px;float:right;}':'.GCOOUJ4DKG{margin:3px 10px 10px 0;font-weight:bold;float:left;}.GCOOUJ4DLG{margin:3px 5px 0 5px;float:left;}';
}

function AdminEventResources_default_InlineClientBundleGenerator$1_0(this$0){
  this , this$0;
  Object_1.call(this);
  $$init_529();
}

function AdminEventResources_default_InlineClientBundleGenerator$1(){
}

_ = AdminEventResources_default_InlineClientBundleGenerator$1_0.prototype = AdminEventResources_default_InlineClientBundleGenerator$1.prototype = new Object_0;
_.ensureInjected = function ensureInjected_0(){
  if (!this.injected) {
    this.injected = true;
    inject($getText_3());
    return true;
  }
  return false;
}
;
_.getClass$ = function getClass_530(){
  return Lconsys_admin_event_gwt_client_utils_AdminEventResources_1default_1InlineClientBundleGenerator$1_2_classLit;
}
;
_.getText = function getText_11(){
  return $getText_3();
}
;
_.show_0 = function show_1(){
  return 'GCOOUJ4DKG';
}
;
_.showSeparator = function showSeparator_0(){
  return 'GCOOUJ4DLG';
}
;
_.castableTypeMap$ = {21:1};
_.injected = false;
function $clinit_119(){
  $clinit_119 = nullMethod;
  $cssInitializer_0(($clinit_118() , _instance0_0));
}

function get_8(){
  $clinit_119();
  return $clinit_118() , css_2;
}

function $clinit_120(){
  $clinit_120 = nullMethod;
  getFormat_0('d');
}

function eventDate(thumb){
  $clinit_120();
  var sb;
  sb = new StringBuilder_0;
  $append_8(sb, isNull($getFrom_2(thumb))?'n/a':getDate_2($getDate_1($getFrom_2(thumb))));
  if (!$isInSameDay(thumb) && isNotNull($getTo_2(thumb))) {
    $append_8(sb, ' - ');
    $append_8(sb, getDate_2($getDate_1($getTo_2(thumb))));
  }
  return $toString_13(sb);
}

function $$init_847(){
}

function $addClickHandler_5(this$static, handler){
  $add(this$static.clickHandlers, handler);
}

function $addMyHandler_0(this$static, handler){
  $addKeyUpHandler_0(this$static.focusPanel, new ActionImageBig$1_0(this$static, handler));
  $addHandler(this$static, handler, getType_10());
}

function $myClickkHandler_0(this$static){
  return new ActionImageBig$2_0(this$static);
}

function $setEnabled_4(this$static, enabled){
  if (enabled == this$static.enabled) {
    return;
  }
  if (enabled) {
    this$static.focusPanel.addStyleName(this$static.baseStyle);
    this$static.focusPanel.addStyleName('cursorHand');
    this$static.focusPanel.removeStyleName(system_0().css_7().bigDisabledB());
    this$static.leftWrapper.clear();
    this$static.leftWrapper.add_1(this$static.left_0);
    this$static.rightWrapper.clear();
    this$static.rightWrapper.add_1(this$static.right);
  }
   else {
    this$static.focusPanel.addStyleName(system_0().css_7().bigDisabledB());
    this$static.focusPanel.removeStyleName(this$static.baseStyle);
    this$static.focusPanel.removeStyleName('cursorHand');
    this$static.leftWrapper.clear();
    this$static.leftWrapper.add_1(new Image_2(system_0().bigDisabledBL()));
    this$static.rightWrapper.clear();
    this$static.rightWrapper.add_1(new Image_2(system_0().bigDisabledBR()));
  }
  this$static.enabled = enabled;
}

function ActionImageBig_0(text, type){
  var label, panel;
  Composite_0.call(this);
  $$init_847();
  this.clickHandlers = new ArrayList_0;
  this.enabled = true;
  this.baseStyle = '';
  this.left_0 = new Image_1;
  this.right = new Image_1;
  switch ($ordinal(type)) {
    case 0:
      this.baseStyle = system_0().css_7().bigGreenB();
      $setResource(this.left_0, system_0().bigGreenBL());
      $setResource(this.right, system_0().bigGreenBR());
      break;
    case 1:
      this.baseStyle = system_0().css_7().bigRedB();
      $setResource(this.left_0, system_0().bigRedBL());
      $setResource(this.right, system_0().bigRedBR());
      break;
    case 2:
      this.baseStyle = system_0().css_7().bigBlueB();
      $setResource(this.left_0, system_0().bigBlueBL());
      $setResource(this.right, system_0().bigBlueBR());
  }
  this.leftWrapper = new FlowPanel_0;
  this.leftWrapper.add_1(this.left_0);
  this.leftWrapper.setStyleName('floatL');
  this.focusPanel = new FocusPanel_0;
  this.focusPanel.setWidth('94px');
  this.focusPanel.setStyleName(this.baseStyle);
  this.focusPanel.addStyleName('floatL');
  this.focusPanel.addStyleName('cursorHand');
  this.rightWrapper = new FlowPanel_0;
  this.rightWrapper.add_1(this.right);
  this.rightWrapper.setStyleName('floatL');
  label = getStyledLabel(text, initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f16px', 'textWhite', 'fontBold', 'marT5']));
  $setHorizontalAlignment_0(label, ($clinit_89() , ALIGN_CENTER));
  this.focusPanel.add_1(label);
  panel = new FlowPanel_0;
  panel.add_1(this.leftWrapper);
  panel.add_1(this.focusPanel);
  panel.add_1(this.rightWrapper);
  this.sinkEvents(1);
  $initWidget(this, panel);
  $addMyHandler_0(this, $myClickkHandler_0(this));
}

function acceptButton(){
  $clinit_6();
  return new ActionImageBig_0(($clinit_515() , c_4).const_accept(), ($clinit_177() , GREEN));
}

function declineButton(){
  $clinit_6();
  return new ActionImageBig_0(($clinit_515() , c_4).const_decline(), ($clinit_177() , RED));
}

function ActionImageBig(){
}

_ = ActionImageBig_0.prototype = ActionImageBig.prototype = new Composite;
_.getClass$ = function getClass_848(){
  return Lconsys_common_gwt_client_ui_comp_ActionImageBig_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.baseStyle = null;
_.clickHandlers = null;
_.enableTimer = null;
_.enabled = false;
_.focusPanel = null;
_.left_0 = null;
_.leftWrapper = null;
_.right = null;
_.rightWrapper = null;
_.temporarilyDisabled = false;
function $$init_848(){
}

function ActionImageBig$1_0(this$0, val$handler){
  this , this$0;
  this.val$handler = val$handler;
  Object_1.call(this);
  $$init_848();
}

function ActionImageBig$1(){
}

_ = ActionImageBig$1_0.prototype = ActionImageBig$1.prototype = new Object_0;
_.getClass$ = function getClass_849(){
  return Lconsys_common_gwt_client_ui_comp_ActionImageBig$1_2_classLit;
}
;
_.onKeyUp = function onKeyUp_12(event_0){
  $getNativeKeyCode(event_0) == 13 && this.val$handler.onClick(null);
}
;
_.castableTypeMap$ = {77:1, 101:1};
_.val$handler = null;
function $$init_849(){
}

function ActionImageBig$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_849();
}

function ActionImageBig$2(){
}

_ = ActionImageBig$2_0.prototype = ActionImageBig$2.prototype = new Object_0;
_.getClass$ = function getClass_850(){
  return Lconsys_common_gwt_client_ui_comp_ActionImageBig$2_2_classLit;
}
;
_.onClick = function onClick_47(event_0){
  var handler, handler$iterator;
  if (this.this$0.enabled && !this.this$0.temporarilyDisabled) {
    this.this$0.temporarilyDisabled = true;
    for (handler$iterator = this.this$0.clickHandlers.iterator_0(); handler$iterator.hasNext_0();) {
      handler = dynamicCast(handler$iterator.next_0(), 67);
      handler.onClick(event_0);
    }
    $schedule(this.this$0.enableTimer, 1500);
  }
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $clinit_177(){
  $clinit_177 = nullMethod;
  GREEN = new ActionImageBig$ActionImageBigEnum_0('GREEN', 0);
  RED = new ActionImageBig$ActionImageBigEnum_0('RED', 1);
  BLUE = new ActionImageBig$ActionImageBigEnum_0('BLUE', 2);
  $VALUES_18 = initValues(_3Lconsys_common_gwt_client_ui_comp_ActionImageBig$ActionImageBigEnum_2_classLit, {57:1, 262:1}, 211, [GREEN, RED, BLUE]);
  P31ea0fae2aceb5dd_longLit;
}

function $$init_850(){
}

function ActionImageBig$ActionImageBigEnum_0(enum$name, enum$ordinal){
  Enum_0.call(this, enum$name, enum$ordinal);
  $$init_850();
}

function valueOf_19(name_0){
  $clinit_177();
  return valueOf(($clinit_178() , $MAP_18), name_0);
}

function values_19(){
  $clinit_177();
  return $VALUES_18;
}

function ActionImageBig$ActionImageBigEnum(){
}

_ = ActionImageBig$ActionImageBigEnum_0.prototype = ActionImageBig$ActionImageBigEnum.prototype = new Enum;
_.getClass$ = function getClass_851(){
  return Lconsys_common_gwt_client_ui_comp_ActionImageBig$ActionImageBigEnum_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 59:1, 211:1, 251:1};
var $VALUES_18, BLUE, GREEN, RED;
function $clinit_178(){
  $clinit_178 = nullMethod;
  $MAP_18 = createValueOfMap(($clinit_177() , $VALUES_18));
}

var $MAP_18;
function $$init_864(){
}

function ClientDate_0(date, styles){
  $clinit_6();
  var i, style;
  Composite_0.call(this);
  $$init_864();
  this.label = new Label_1(getDate_0(date));
  for (i = 0; i < styles.length; ++i) {
    style = styles[i];
    this.label.addStyleName(style);
  }
  $initWidget(this, this.label);
}

function ClientDate(){
}

_ = ClientDate_0.prototype = ClientDate.prototype = new Composite;
_.getClass$ = function getClass_865(){
  return Lconsys_common_gwt_client_ui_comp_ClientDate_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.label = null;
function $bigBlueBL(){
  return get_119();
}

function $bigBlueBLInitializer(){
  bigBlueBL = new ImageResourcePrototype_0('bigBlueBL', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAfCAYAAAAiPHdfAAAAnUlEQVR42kXIWQ7BYAAE4HmSOICzSRxA4jC0aG1VxA2sRS1Vuyeeucv4pel0km+SGcCkFH+J0YMoRh9ieE/kx89sYHCjYHClwDeVgn+hoH+mwDtR4MUU9I4UdCMKOgcKOnsK2jsKWlsKXFMpOCEFzoaC5pqCxoqCekBBfUmBvaDAmlNgzSiomUqhNqXkbPNUJ4ly+MrGP5XwzYIb8AeGBNW8R584ggAAAABJRU5ErkJggg==', 0, 0, 3, 31, false, false);
}

function $bigBlueBR(){
  return get_120();
}

function $bigBlueBRInitializer(){
  bigBlueBR = new ImageResourcePrototype_0('bigBlueBR', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAfCAYAAAAiPHdfAAAAoUlEQVR42kXRWw7BYBiE4bmSWIC1SSxAYjHaOpRSReyAoqhD1dkV1+xl/CQdX/JM8l5/wPDGUvomfje48quYvKjIj+4m+hcK+mcKAjMZBCcKekcK/AMFfkpBd09BJ6HA21HgbSlobyhorSlwzWTQjClorihoLCmoLyioRRTU5hQ4MwrsKQV2SIFlJgNrQkF1zK+cE/6jHD+IghuxEj9/X/gAoYzVvAaizg8AAAAASUVORK5CYII=', 0, 0, 3, 31, false, false);
}

function $bigDisabledBL(){
  return get_122();
}

function $bigDisabledBLInitializer(){
  bigDisabledBL = new ImageResourcePrototype_0('bigDisabledBL', 'data:image/gif;base64,R0lGODlhAwAfAIQdAJKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqr6+vr+/v8bGxsfHx////////////yH5BAEKAB8ALAAAAAADAB8AAAU04Mddm2VhVZpSbDu9UixHNGQ/uKM3POMvQIUwQUQYD8ikYVloEp7QgVRADVgJ1gwA8NEkQgA7', 0, 0, 3, 31, false, false);
}

function $bigDisabledBR(){
  return get_123();
}

function $bigDisabledBRInitializer(){
  bigDisabledBR = new ImageResourcePrototype_0('bigDisabledBR', 'data:image/gif;base64,R0lGODlhAwAfAIQdAJKSkpOTk5SUlJWVlZaWlpeXl5iYmJmZmZqampubm5ycnJ2dnZ6enp+fn6CgoKGhoaKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqr6+vr+/v8bGxsfHx////////////yH5BAEKAB8ALAAAAAADAB8AAAU14MV9lrVVFYZWVOtOsCTPUQ3dT+7sTc/8i6BimCgijoek0sAsOAnQ6GAqqAauVwIAkEloPiEAOw==', 0, 0, 3, 31, false, false);
}

function $bigGreenBL(){
  return get_125();
}

function $bigGreenBLInitializer(){
  bigGreenBL = new ImageResourcePrototype_0('bigGreenBL', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAfCAYAAAAiPHdfAAAAtklEQVR42jXRS05CYRCE0dpRTYhjVkUYwuUhIAKSsAEUBQXCSx7KXMaE/ZTNzV+dnM7X4wZijreyhmcKh2tJw1/mMDo/6OWHOTjyY3CiDINjRIL+gTL0Yhl6+4gEz9+UobujDN1tRIKnDWXorClDe0UZ2suIBK0FZXicU4bmF2VofkYkaMwoQ31KGeofEQlq75Qhi2XIJhEJqm+UofoakaA2KagScYftX6bKmLn7E7C7NNSZFfUPYNXcmeBjASoAAAAASUVORK5CYII=', 0, 0, 3, 31, false, false);
}

function $bigGreenBR(){
  return get_126();
}

function $bigGreenBRInitializer(){
  bigGreenBR = new ImageResourcePrototype_0('bigGreenBR', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAfCAYAAAAiPHdfAAAAtUlEQVR42kXRN46CQRCE0bpRJYiYUyFC+HGLNxIXwOwusIvwPocYcZ+iB02LkV7r67A16F+p4zMnhNe/UMHhkRV6ZyoYXNOfJUD3RDl0jxYROgfKoW3Dob23iNDaUQ7NLeXQ3FhEaKwph/qKcqgtKYfawiLC1z/lUP2jHCpzyqEys4hQnlIOpV/KofRjEaH4TTkkNhySiUWEwphyKIwsIuRtBMVJypahLWZzS+y4aUbbe/n9Cy9+HdyZmmQJhQAAAABJRU5ErkJggg==', 0, 0, 3, 31, false, false);
}

function $bigRedBL(){
  return get_128();
}

function $bigRedBLInitializer(){
  bigRedBL = new ImageResourcePrototype_0('bigRedBL', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAfCAYAAAAiPHdfAAAAsUlEQVR42lXRSQ6CAAwF0H8CUQTFARx33M2EMypqnOIU54m13uFbTP7CJu+n3XTRAlafJGHW7xPvwYBZr/eDLI75siaHZ7dLwcNC8Oh0KLi32xRcLQSXVouCcxRRcApDCo7NJgWHRoOCfb1Owc5CsK3VKNgEAQXrapWCVaVCwdL3KVh4HgXzcpmCmYVg6roUTEolCsbFIuV/SB2HgtT2jwqFH1zt1kNrcvkTcLMvTO2IX7P2uhg3lqasAAAAAElFTkSuQmCC', 0, 0, 3, 31, false, false);
}

function $bigRedBR(){
  return get_129();
}

function $bigRedBRInitializer(){
  bigRedBR = new ImageResourcePrototype_0('bigRedBR', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAfCAYAAAAiPHdfAAAAsElEQVR42lXRSQ6CUBAE0DoBCioijgg77mbCGR3jFKc44cRa71D+n1gLO3mV7lUn3SiShJ8sI2wVcUzrPRgQL9NYRZoSz36fgocJwSOKKLj3ehTkJgTXbpeCS6dDwbndpuDUalFwbDYpOIQhBXsTgl2jQcE2CCjY1OsUrH2fglWtRsGyWqVgUalQMDchmHkeBVPXpWBSLlP+h3GpRMHIcWiN7Z6haazc3npmjnf7feELyM66GIvz2PsAAAAASUVORK5CYII=', 0, 0, 3, 31, false, false);
}

_ = SystemBundle_gecko1_8_default_InlineClientBundleGenerator.prototype;
_.bigBlueBL = function bigBlueBL_0(){
  return $bigBlueBL();
}
;
_.bigBlueBR = function bigBlueBR_0(){
  return $bigBlueBR();
}
;
_.bigDisabledBL = function bigDisabledBL_0(){
  return $bigDisabledBL();
}
;
_.bigDisabledBR = function bigDisabledBR_0(){
  return $bigDisabledBR();
}
;
_.bigGreenBL = function bigGreenBL_0(){
  return $bigGreenBL();
}
;
_.bigGreenBR = function bigGreenBR_0(){
  return $bigGreenBR();
}
;
_.bigRedBL = function bigRedBL_0(){
  return $bigRedBL();
}
;
_.bigRedBR = function bigRedBR_0(){
  return $bigRedBR();
}
;
var bigBlueBL = null, bigBlueBR = null, bigDisabledBL = null, bigDisabledBR = null, bigGreenBL = null, bigGreenBR = null, bigRedBL = null, bigRedBR = null;
_ = SystemBundle_gecko1_8_default_InlineClientBundleGenerator$2.prototype;
_.bigBlueB = function bigBlueB_0(){
  return 'GCOOUJ4DCQ';
}
;
_.bigDisabledB = function bigDisabledB_0(){
  return 'GCOOUJ4DDQ';
}
;
_.bigGreenB = function bigGreenB_0(){
  return 'GCOOUJ4DEQ';
}
;
_.bigRedB = function bigRedB_0(){
  return 'GCOOUJ4DFQ';
}
;
function $clinit_342(){
  $clinit_342 = nullMethod;
  $bigBlueBLInitializer(($clinit_322() , _instance0_8));
}

function get_119(){
  $clinit_342();
  return $clinit_322() , bigBlueBL;
}

function $clinit_343(){
  $clinit_343 = nullMethod;
  $bigBlueBRInitializer(($clinit_322() , _instance0_8));
}

function get_120(){
  $clinit_343();
  return $clinit_322() , bigBlueBR;
}

function $clinit_345(){
  $clinit_345 = nullMethod;
  $bigDisabledBLInitializer(($clinit_322() , _instance0_8));
}

function get_122(){
  $clinit_345();
  return $clinit_322() , bigDisabledBL;
}

function $clinit_346(){
  $clinit_346 = nullMethod;
  $bigDisabledBRInitializer(($clinit_322() , _instance0_8));
}

function get_123(){
  $clinit_346();
  return $clinit_322() , bigDisabledBR;
}

function $clinit_348(){
  $clinit_348 = nullMethod;
  $bigGreenBLInitializer(($clinit_322() , _instance0_8));
}

function get_125(){
  $clinit_348();
  return $clinit_322() , bigGreenBL;
}

function $clinit_349(){
  $clinit_349 = nullMethod;
  $bigGreenBRInitializer(($clinit_322() , _instance0_8));
}

function get_126(){
  $clinit_349();
  return $clinit_322() , bigGreenBR;
}

function $clinit_351(){
  $clinit_351 = nullMethod;
  $bigRedBLInitializer(($clinit_322() , _instance0_8));
}

function get_128(){
  $clinit_351();
  return $clinit_322() , bigRedBL;
}

function $clinit_352(){
  $clinit_352 = nullMethod;
  $bigRedBRInitializer(($clinit_322() , _instance0_8));
}

function get_129(){
  $clinit_352();
  return $clinit_322() , bigRedBR;
}

function $getFrom_2(this$static){
  return this$static.from;
}

function $getTo_2(this$static){
  return this$static.to;
}

function $getWeb_0(this$static){
  return this$static.web;
}

function $getYear_2(this$static){
  return this$static.year;
}

function $isInSameDay(this$static){
  var f, t;
  if (isNull($getFrom_2(this$static)) || isNull($getTo_2(this$static))) {
    return false;
  }
  f = $getFrom_2(this$static);
  t = $getTo_2(this$static);
  return $getDay_0($getDate_1(f)) == $getDay_0($getDate_1(t)) && $getMonth_1($getDate_1(f)) == $getMonth_1($getDate_1(t)) && $getYear_0($getDate_1(f)) == $getYear_0($getDate_1(t));
}

var Lcom_google_gwt_lang_asyncloaders_AsyncLoader1_2_classLit = createForClass('com.google.gwt.lang.asyncloaders.', 'AsyncLoader1', 'AsyncLoader1', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_AbstractEventsPanel_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AbstractEventsPanel', 'AbstractEventsPanel', Lcom_google_gwt_user_client_ui_FlexTable_2_classLit), Lconsys_admin_event_gwt_client_module_event_AbstractEventsPanel$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AbstractEventsPanel$1', 'AbstractEventsPanel$1', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_AllEventsMenu_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AllEventsMenu', 'AllEventsMenu', Lconsys_common_gwt_client_ui_comp_panel_SimpleFormPanel_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventFilterSelector', 'EventFilterSelector', Lcom_google_gwt_user_client_ui_SimplePanel_2_classLit), Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AllEventsMenu$1', 'AllEventsMenu$1', Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit), Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$2_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AllEventsMenu$2', 'AllEventsMenu$2', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$3_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AllEventsMenu$3', 'AllEventsMenu$3', Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit), Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$4_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AllEventsMenu$4', 'AllEventsMenu$4', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_AllEventsMenu$5_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'AllEventsMenu$5', 'AllEventsMenu$5', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventItem_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventItem', 'EventItem', Lcom_google_gwt_user_client_ui_Composite_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventItem$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventItem$1', 'EventItem$1', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventItem$2_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventItem$2', 'EventItem$2', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventItem$3_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventItem$3', 'EventItem$3', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventMenuContent_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventMenuContent', 'EventMenuContent', Lconsys_common_gwt_client_ui_comp_panel_MenuFormPanel_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventMenuContent$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventMenuContent$1', 'EventMenuContent$1', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_EventsPanel_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'EventsPanel', 'EventsPanel', Lconsys_admin_event_gwt_client_module_event_AbstractEventsPanel_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationDetailDialog', 'InvitationDetailDialog', Lconsys_common_gwt_client_ui_comp_SmartDialog_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationDetailDialog$1', 'InvitationDetailDialog$1', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog$2_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationDetailDialog$2', 'InvitationDetailDialog$2', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationDetailDialog$2$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationDetailDialog$2$1', 'InvitationDetailDialog$2$1', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsItem_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsItem', 'InvitationsItem', Lcom_google_gwt_user_client_ui_Composite_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsItem$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsItem$1', 'InvitationsItem$1', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsItem$2_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsItem$2', 'InvitationsItem$2', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsItem$3_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsItem$3', 'InvitationsItem$3', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsMenu_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsMenu', 'InvitationsMenu', Lconsys_common_gwt_client_ui_comp_panel_SimpleFormPanel_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsMenu$1', 'InvitationsMenu$1', Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$2_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsMenu$2', 'InvitationsMenu$2', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$3_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsMenu$3', 'InvitationsMenu$3', Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$4_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsMenu$4', 'InvitationsMenu$4', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsMenu$5_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsMenu$5', 'InvitationsMenu$5', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_InvitationsPanel_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'InvitationsPanel', 'InvitationsPanel', Lconsys_admin_event_gwt_client_module_event_AbstractEventsPanel_2_classLit), Lconsys_admin_event_gwt_client_module_event_MyEventsMenu_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'MyEventsMenu', 'MyEventsMenu', Lconsys_common_gwt_client_ui_comp_panel_SimpleFormPanel_2_classLit), Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$1_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'MyEventsMenu$1', 'MyEventsMenu$1', Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit), Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$2_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'MyEventsMenu$2', 'MyEventsMenu$2', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$3_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'MyEventsMenu$3', 'MyEventsMenu$3', Lconsys_admin_event_gwt_client_module_event_EventFilterSelector_2_classLit), Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$4_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'MyEventsMenu$4', 'MyEventsMenu$4', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_module_event_MyEventsMenu$5_2_classLit = createForClass('consys.admin.event.gwt.client.module.event.', 'MyEventsMenu$5', 'MyEventsMenu$5', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_utils_AdminEventResources_1default_1InlineClientBundleGenerator_2_classLit = createForClass('consys.admin.event.gwt.client.utils.', 'AdminEventResources_default_InlineClientBundleGenerator', 'AdminEventResources_default_InlineClientBundleGenerator', Ljava_lang_Object_2_classLit), Lconsys_admin_event_gwt_client_utils_AdminEventResources_1default_1InlineClientBundleGenerator$1_2_classLit = createForClass('consys.admin.event.gwt.client.utils.', 'AdminEventResources_default_InlineClientBundleGenerator$1', 'AdminEventResources_default_InlineClientBundleGenerator$1', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_ActionImageBig_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'ActionImageBig', 'ActionImageBig', Lcom_google_gwt_user_client_ui_Composite_2_classLit), Lconsys_common_gwt_client_ui_comp_ActionImageBig$1_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'ActionImageBig$1', 'ActionImageBig$1', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_ActionImageBig$2_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'ActionImageBig$2', 'ActionImageBig$2', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_ActionImageBig$ActionImageBigEnum_2_classLit = createForEnum('consys.common.gwt.client.ui.comp.', 'ActionImageBig$ActionImageBigEnum', 'ActionImageBig$ActionImageBigEnum', Ljava_lang_Enum_2_classLit, values_19, valueOf_19), _3Lconsys_common_gwt_client_ui_comp_ActionImageBig$ActionImageBigEnum_2_classLit = createForArray('[Lconsys.common.gwt.client.ui.comp.', 'ActionImageBig$ActionImageBigEnum;', 'Array_0', Lconsys_common_gwt_client_ui_comp_ActionImageBig$ActionImageBigEnum_2_classLit), Lconsys_common_gwt_client_ui_comp_ClientDate_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'ClientDate', 'ClientDate', Lcom_google_gwt_user_client_ui_Composite_2_classLit);
$entry(onLoad_0)();
