function $fragmentHasLoaded(this$static, fragment){
  $logFragmentLoaded(this$static, fragment);
  fragment < this$static.pendingDownloadErrorHandlers.length && setCheck(this$static.pendingDownloadErrorHandlers, fragment, null);
  $isInitial(this$static, fragment) && $remove_1(this$static.remainingInitialFragments);
  this$static.fragmentLoading = -1;
  this$static.isLoaded[fragment] = true;
  $startLoadingNextFragment(this$static);
}

function $leftoversFragmentHasLoaded(this$static){
  $fragmentHasLoaded(this$static, $leftoversFragment(this$static));
}

function $logEventProgress(this$static, eventGroup, type){
  $logEventProgress_0(this$static, eventGroup, type, -1, -1);
}

function $logFragmentLoaded(this$static, fragment){
  var logGroup;
  logGroup = $downloadGroup(this$static, fragment);
  $logEventProgress_0(this$static, logGroup, 'end', fragment, -1);
}

function browserLoaderLeftoversFragmentHasLoaded(){
  $leftoversFragmentHasLoaded(BROWSER_LOADER);
}

function $getWidget_1(this$static, row, column){
  $checkCellBounds(this$static, row, column);
  return $getWidgetImpl(this$static, row, column);
}

function $getWidgetImpl(this$static, row, column){
  var child, e;
  e = $getRawElement(this$static.cellFormatter, row, column);
  child = getFirstChild(e);
  return isNull(child)?null:dynamicCast($get_5(this$static.widgetMap, child), 2);
}

_ = AdminEventConstants_.prototype;
_.invitationsMenu_title_invitations = function invitationsMenu_title_invitations(){
  return 'Invitations';
}
;
function $onSuccess_1(instance){
  var event_0;
  event_0 = new ChangeContentEvent_0(instance);
  $fireEvent_3(get_10(), event_0);
}

_ = EventModule$2$1.prototype;
_.onSuccess = function onSuccess_1(instance){
  $onSuccess_1(dynamicCast(instance, 359));
}
;
function $onSuccess_2(instance){
  var event_0;
  $showItem(instance, ($clinit_114() , NAME_0));
  event_0 = new ChangeContentEvent_0(instance);
  $fireEvent_3(get_10(), event_0);
}

_ = EventModule$5$1$1.prototype;
_.onSuccess = function onSuccess_2(instance){
  $onSuccess_2(dynamicCast(instance, 359));
}
;
function $setBodyWidth(this$static, width){
  this$static.bodyPart.setWidth(width);
}

function $showItem(this$static, name_0){
  var i, j, part, widget;
  for (i = 0; i < $getRowCount(this$static.topPanel); ++i) {
    for (j = 0; j < this$static.cols; ++j) {
      widget = $getWidget_1(this$static.topPanel, i, j);
      if (isNotNull(widget)) {
        part = dynamicCast(widget, 486);
        if ($equals_8($getItem_1(part).getName(), name_0)) {
          $showItem_0(this$static, part);
          return;
        }
      }
    }
  }
}

function $clinit_114(){
  $clinit_114 = nullMethod;
  $clinit_6();
  NAME_0 = ($clinit_116() , c_0).invitationsMenu_title_invitations();
}

var NAME_0;
function $onSuccess_28(instance){
  var event_0;
  event_0 = new ChangeContentEvent_0(instance);
  $fireEvent_3(get_10(), event_0);
}

_ = UserModule$1$1.prototype;
_.onSuccess = function onSuccess_30(instance){
  $onSuccess_28(dynamicCast(instance, 401));
}
;
function messagingURL(){
  $clinit_146();
  return SERVER_URL + 'mail/client';
}

function $setUrl_2(this$static, url){
  this$static , url;
}

function $setUrl_3(this$static, url){
  this$static.url = url;
}

function $setMessagingUrl(this$static, messagingUrl){
  $setUrl_2(this$static.apiMessage, messagingUrl);
  $setUrl_3(this$static.apiWall, messagingUrl);
}

function $onSuccess_56(instance){
  $setMessagingUrl(instance, messagingURL());
  $register_0(get_11(), 'messaging_api', instance);
}

_ = MessagingModule$1.prototype;
_.onSuccess = function onSuccess_60(instance){
  $onSuccess_56(dynamicCast(instance, 428));
}
;
$entry(browserLoaderLeftoversFragmentHasLoaded)();
