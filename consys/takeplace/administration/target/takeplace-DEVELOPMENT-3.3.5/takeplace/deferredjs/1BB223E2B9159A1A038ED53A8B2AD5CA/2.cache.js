function $delegateEvent(target, event_0){
  $fireEvent(target, event_0);
}

function $$init_136(){
}

function $addKeyHandlersTo(this$static, source){
  addHandlers(source, this$static);
}

function HandlesAllKeyEvents_0(){
  Object_1.call(this);
  $$init_136();
}

function addHandlers(eventSource, reciever){
  eventSource.addKeyDownHandler(reciever);
  eventSource.addKeyPressHandler(reciever);
  eventSource.addKeyUpHandler(reciever);
}

function HandlesAllKeyEvents(){
}

_ = HandlesAllKeyEvents.prototype = new Object_0;
_.getClass$ = function getClass_137(){
  return Lcom_google_gwt_event_dom_client_HandlesAllKeyEvents_2_classLit;
}
;
_.castableTypeMap$ = {75:1, 76:1, 77:1, 101:1};
function $getTarget(this$static){
  return this$static.target_0;
}

function $isAutoClosed(this$static){
  return this$static.autoClosed;
}

function $$init_160(){
}

function $dispatch_22(this$static, handler){
  handler.onSelection(this$static);
}

function $getSelectedItem(this$static){
  return this$static.selectedItem;
}

function SelectionEvent_0(selectedItem){
  GwtEvent_0.call(this);
  $$init_160();
  this.selectedItem = selectedItem;
}

function fire_5(source, selectedItem){
  var event_0;
  if (isNotNull(TYPE_22)) {
    event_0 = new SelectionEvent_0(selectedItem);
    source.fireEvent(event_0);
  }
}

function getType_29(){
  isNull(TYPE_22) && (TYPE_22 = new GwtEvent$Type_0);
  return TYPE_22;
}

function SelectionEvent(){
}

_ = SelectionEvent_0.prototype = SelectionEvent.prototype = new GwtEvent;
_.dispatch_0 = function dispatch_23(handler){
  $dispatch_22(this, dynamicCast(handler, 97));
}
;
_.getAssociatedType_0 = function getAssociatedType_24(){
  return TYPE_22;
}
;
_.getClass$ = function getClass_161(){
  return Lcom_google_gwt_event_logical_shared_SelectionEvent_2_classLit;
}
;
_.castableTypeMap$ = {455:1};
_.selectedItem = null;
var TYPE_22 = null;
function $$init_228(){
}

function $runCallbacks_0(){
  var $e0, e, handler, next;
  while (isNotNull(callbacksHead_0)) {
    handler = getUncaughtExceptionHandler();
    next = callbacksHead_0;
    callbacksHead_0 = callbacksHead_0.next;
    isNull(callbacksHead_0) && (callbacksTail_0 = null);
    if (isNull(handler)) {
      $onSuccess_29(next.callback);
    }
     else {
      try {
        $onSuccess_29(next.callback);
      }
       catch ($e0) {
        $e0 = caught_0($e0);
        if (instanceOf($e0, 25)) {
          e = $e0;
          handler.onUncaughtException(e);
        }
         else 
          throw $e0;
      }
    }
  }
}

function AsyncLoader2_0(){
  Object_1.call(this);
  $$init_228();
}

function onLoad_1(){
  instance_5 = new AsyncLoader2_0;
  $fragmentHasLoaded(($clinit_11() , BROWSER_LOADER), 2);
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks2', 'begin');
  instance_5.runCallbacks();
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks2', 'end');
}

function AsyncLoader2(){
}

_ = AsyncLoader2_0.prototype = AsyncLoader2.prototype = new Object_0;
_.getClass$ = function getClass_229(){
  return Lcom_google_gwt_lang_asyncloaders_AsyncLoader2_2_classLit;
}
;
_.runCallbacks = function runCallbacks_0(){
  $runCallbacks_0();
}
;
_.castableTypeMap$ = {};
function getChild(parent_0, index){
  $clinit_71();
  return impl_5.getChild(parent_0, index);
}

function getChildCount(parent_0){
  $clinit_71();
  return impl_5.getChildCount(parent_0);
}

_ = DOMImplStandard_1.prototype;
_.getChild = function getChild_0(elem, index){
  var count = 0, child = elem.firstChild;
  while (child) {
    if (child.nodeType == 1) {
      if (index == count)
        return child;
      ++count;
    }
    child = child.nextSibling;
  }
  return null;
}
;
_.getChildCount = function getChildCount_0(elem){
  var count = 0, child = elem.firstChild;
  while (child) {
    if (child.nodeType == 1)
      ++count;
    child = child.nextSibling;
  }
  return count;
}
;
_ = FocusWidget.prototype;
_.addKeyDownHandler = function addKeyDownHandler(handler){
  return $addKeyDownHandler(this, handler);
}
;
_.addKeyPressHandler = function addKeyPressHandler(handler){
  return $addKeyPressHandler(this, handler);
}
;
_ = FocusPanel.prototype;
_.addKeyDownHandler = function addKeyDownHandler_0(handler){
  return $addDomHandler(this, handler, getType_13());
}
;
_.addKeyPressHandler = function addKeyPressHandler_0(handler){
  return $addKeyPressHandler_0(this, handler);
}
;
function $addChangeHandler_0(this$static, handler){
  return $addDomHandler(this$static, handler, getType_9());
}

function $addValueChangeHandler_1(this$static, handler){
  if (!this$static.valueChangeHandlerInitialized) {
    this$static.valueChangeHandlerInitialized = true;
    $addChangeHandler_0(this$static, new ValueBoxBase$1_0(this$static));
  }
  return $addHandler(this$static, handler, getType_30());
}

function $isAnimationEnabled(this$static){
  return this$static.isAnimationEnabled;
}

function $setAnimationEnabled(this$static, enable){
  this$static.isAnimationEnabled = enable;
}

function $setPreviewingAllNativeEvents(this$static, previewAllNativeEvents){
  this$static.previewAllNativeEvents = previewAllNativeEvents;
}

function $$init_397(this$static){
  this$static.emptyResponse = new SuggestOracle$Response_0(new ArrayList_0);
}

function $requestDefaultSuggestions(this$static, request, callback){
  callback.onSuggestionsReady(request, this$static.emptyResponse);
}

function SuggestOracle_0(){
  Object_1.call(this);
  $$init_397(this);
}

function SuggestOracle(){
}

_ = SuggestOracle.prototype = new Object_0;
_.getClass$ = function getClass_398(){
  return Lcom_google_gwt_user_client_ui_SuggestOracle_2_classLit;
}
;
_.isDisplayStringHTML = function isDisplayStringHTML(){
  return false;
}
;
_.requestDefaultSuggestions = function requestDefaultSuggestions(request, callback){
  $requestDefaultSuggestions(this, request, callback);
}
;
_.castableTypeMap$ = {};
function $$init_398(this$static){
  this$static , 20;
}

function $getQuery(this$static){
  return this$static.query;
}

function $setLimit(this$static, limit){
  this$static , limit;
}

function $setQuery(this$static, query){
  this$static.query = query;
}

function SuggestOracle$Request_0(query, limit){
  Object_1.call(this);
  $$init_398(this);
  $setQuery(this, query);
  $setLimit(this, limit);
}

function SuggestOracle$Request(){
}

_ = SuggestOracle$Request_0.prototype = SuggestOracle$Request.prototype = new Object_0;
_.getClass$ = function getClass_399(){
  return Lcom_google_gwt_user_client_ui_SuggestOracle$Request_2_classLit;
}
;
_.castableTypeMap$ = {48:1};
_.query = null;
function $$init_399(this$static){
  this$static , false;
  this$static , 0;
}

function $getSuggestions(this$static){
  return this$static.suggestions;
}

function $setSuggestions(this$static, suggestions){
  this$static.suggestions = suggestions;
}

function SuggestOracle$Response_0(suggestions){
  Object_1.call(this);
  $$init_399(this);
  $setSuggestions(this, suggestions);
}

function SuggestOracle$Response(){
}

_ = SuggestOracle$Response_0.prototype = SuggestOracle$Response.prototype = new Object_0;
_.getClass$ = function getClass_400(){
  return Lcom_google_gwt_user_client_ui_SuggestOracle$Response_2_classLit;
}
;
_.castableTypeMap$ = {48:1};
_.suggestions = null;
function $$init_404(){
}

function ValueBoxBase$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_404();
}

function ValueBoxBase$1(){
}

_ = ValueBoxBase$1_0.prototype = ValueBoxBase$1.prototype = new Object_0;
_.getClass$ = function getClass_405(){
  return Lcom_google_gwt_user_client_ui_ValueBoxBase$1_2_classLit;
}
;
_.onChange = function onChange(event_0){
  fire_7(this.this$0, this.this$0.getValue());
}
;
_.castableTypeMap$ = {66:1, 101:1};
_.this$0 = null;
function $setDynamicWidth(this$static, set){
  this$static.panel_0.setWidth(set?'100%':'700px');
}

function $setHiddableLabel(this$static, hiddableLabel){
  this$static.hiddableLabel = hiddableLabel;
}

function $hideHelp(this$static){
  if (this$static.helpPanel.isVisible()) {
    this$static.contentPanel.setWidth('100%');
    this$static.helpPanel.setVisible(false);
    this$static.helpPanel.setWidth('0px');
  }
}

function $showHelp(this$static){
  if (!this$static.helpPanel.isVisible()) {
    this$static.helpPanel.setVisible(true);
    this$static.helpPanel.setWidth('200px');
    this$static.contentPanel.setWidth('490px');
  }
}

function $$init_534(){
}

function CreateUserOrgAction_0(uo){
  Object_1.call(this);
  $$init_534();
  this.uo = uo;
}

function CreateUserOrgAction(){
}

_ = CreateUserOrgAction_0.prototype = CreateUserOrgAction.prototype = new Object_0;
_.getClass$ = function getClass_535(){
  return Lconsys_admin_user_gwt_client_action_CreateUserOrgAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 372:1};
_.uo = null;
function $$init_535(){
}

function DeleteUserOrgAction_0(id){
  Object_1.call(this);
  $$init_535();
  this.uuid_0 = id;
}

function DeleteUserOrgAction(){
}

_ = DeleteUserOrgAction_0.prototype = DeleteUserOrgAction.prototype = new Object_0;
_.getClass$ = function getClass_536(){
  return Lconsys_admin_user_gwt_client_action_DeleteUserOrgAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 373:1};
_.uuid_0 = null;
function $$init_536(){
}

function ListOrganizationTypesAction_0(){
  Object_1.call(this);
  $$init_536();
}

function ListOrganizationTypesAction(){
}

_ = ListOrganizationTypesAction_0.prototype = ListOrganizationTypesAction.prototype = new Object_0;
_.getClass$ = function getClass_537(){
  return Lconsys_admin_user_gwt_client_action_ListOrganizationTypesAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 374:1};
function $$init_537(){
}

function ListOrganizationsByPrefixAction_0(prefix, limit){
  Object_1.call(this);
  $$init_537();
  this.prefix = prefix;
  this.limit = limit;
}

function ListOrganizationsByPrefixAction(){
}

_ = ListOrganizationsByPrefixAction_0.prototype = ListOrganizationsByPrefixAction.prototype = new Object_0;
_.getClass$ = function getClass_538(){
  return Lconsys_admin_user_gwt_client_action_ListOrganizationsByPrefixAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 375:1};
_.limit = 0;
_.prefix = null;
function $$init_538(){
}

function ListUserOrgsAction_0(){
  Object_1.call(this);
  $$init_538();
}

function ListUserOrgsAction(){
}

_ = ListUserOrgsAction_0.prototype = ListUserOrgsAction.prototype = new Object_0;
_.getClass$ = function getClass_539(){
  return Lconsys_admin_user_gwt_client_action_ListUserOrgsAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 376:1};
function $$init_541(){
}

function LoadOrganizationAction_0(uuid){
  Object_1.call(this);
  $$init_541();
  this.uuid_0 = uuid;
}

function LoadOrganizationAction(){
}

_ = LoadOrganizationAction_0.prototype = LoadOrganizationAction.prototype = new Object_0;
_.getClass$ = function getClass_542(){
  return Lconsys_admin_user_gwt_client_action_LoadOrganizationAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 379:1};
_.uuid_0 = null;
function $$init_542(){
}

function LoadPersonalInfoAction_0(){
  Object_1.call(this);
  $$init_542();
}

function LoadPersonalInfoAction(){
}

_ = LoadPersonalInfoAction_0.prototype = LoadPersonalInfoAction.prototype = new Object_0;
_.getClass$ = function getClass_543(){
  return Lconsys_admin_user_gwt_client_action_LoadPersonalInfoAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 380:1};
function $$init_543(){
}

function LoadPrivateUserOrgAction_0(){
  Object_1.call(this);
  $$init_543();
}

function LoadPrivateUserOrgAction(){
}

_ = LoadPrivateUserOrgAction_0.prototype = LoadPrivateUserOrgAction.prototype = new Object_0;
_.getClass$ = function getClass_544(){
  return Lconsys_admin_user_gwt_client_action_LoadPrivateUserOrgAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 381:1};
function $$init_544(){
}

function LoadUserOrgAction_0(userOrgUuid){
  Object_1.call(this);
  $$init_544();
  this.userOrgUuid = userOrgUuid;
}

function LoadUserOrgAction(){
}

_ = LoadUserOrgAction_0.prototype = LoadUserOrgAction.prototype = new Object_0;
_.getClass$ = function getClass_545(){
  return Lconsys_admin_user_gwt_client_action_LoadUserOrgAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 382:1};
_.userOrgUuid = null;
function $$init_548(){
}

function $isCeliac(this$static){
  return this$static.celiac;
}

function $isVegetarian(this$static){
  return this$static.vegetarian;
}

function UpdatePersonalInfoAction_0(info){
  Object_1.call(this);
  $$init_548();
  this.celiac = $isCeliac_0(info);
  this.vegetarian = $isVegetarian_0(info);
}

function UpdatePersonalInfoAction(){
}

_ = UpdatePersonalInfoAction_0.prototype = UpdatePersonalInfoAction.prototype = new Object_0;
_.getClass$ = function getClass_549(){
  return Lconsys_admin_user_gwt_client_action_UpdatePersonalInfoAction_2_classLit;
}
;
_.isCeliac = function isCeliac(){
  return $isCeliac(this);
}
;
_.isVegetarian = function isVegetarian(){
  return $isVegetarian(this);
}
;
_.castableTypeMap$ = {57:1, 386:1};
_.celiac = false;
_.vegetarian = false;
function $$init_549(){
}

function UpdatePrivateOrgAction_0(uo){
  Object_1.call(this);
  $$init_549();
  this.uo = uo;
}

function UpdatePrivateOrgAction(){
}

_ = UpdatePrivateOrgAction_0.prototype = UpdatePrivateOrgAction.prototype = new Object_0;
_.getClass$ = function getClass_550(){
  return Lconsys_admin_user_gwt_client_action_UpdatePrivateOrgAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 387:1};
_.uo = null;
function $$init_551(){
}

function UpdateUserOrgAction_0(uo){
  Object_1.call(this);
  $$init_551();
  this.uo = uo;
}

function UpdateUserOrgAction(){
}

_ = UpdateUserOrgAction_0.prototype = UpdateUserOrgAction.prototype = new Object_0;
_.getClass$ = function getClass_552(){
  return Lconsys_admin_user_gwt_client_action_UpdateUserOrgAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 389:1};
_.uo = null;
function $$init_593(){
}

function $getConfirmPassword(this$static){
  return this$static.confirmPassword;
}

function $getNewPassword(this$static){
  return this$static.newPassword;
}

function $setConfirmPassword(this$static, confirmPassword){
  this$static.confirmPassword = confirmPassword;
}

function $setNewPassword(this$static, newPassword){
  this$static.newPassword = newPassword;
}

function $setOldPassword(this$static, oldPassword){
  this$static , oldPassword;
}

function ClientPassword_0(){
  Object_1.call(this);
  $$init_593();
}

function ClientPassword(){
}

_ = ClientPassword_0.prototype = ClientPassword.prototype = new Object_0;
_.getClass$ = function getClass_594(){
  return Lconsys_admin_user_gwt_client_bo_ClientPassword_2_classLit;
}
;
_.castableTypeMap$ = {48:1};
_.confirmPassword = null;
_.newPassword = null;
function $getAddress(this$static){
  return this$static.address;
}

function $getFromDate(this$static){
  return this$static.fromDate;
}

function $getOrganization(this$static){
  return this$static.organization_0;
}

function $getPosition_0(this$static){
  return this$static.position_0;
}

function $getToDate(this$static){
  return this$static.toDate;
}

function $getUuid_1(this$static){
  return this$static.uuid_0;
}

function $isActualPosition(this$static){
  return this$static.actualPosition;
}

function $isPreferredPosition(this$static){
  return this$static.preferredPosition;
}

function $setActualPosition(this$static, actualPosition){
  this$static.actualPosition = actualPosition;
}

function $setAddress(this$static, address){
  this$static.address = address;
}

function $setFromDate(this$static, fromDate){
  this$static.fromDate = fromDate;
}

function $setOrganization(this$static, organization){
  this$static.organization_0 = organization;
}

function $setPosition_1(this$static, position){
  this$static.position_0 = position;
}

function $setPreferredPosition(this$static, preferredPosition){
  this$static.preferredPosition = preferredPosition;
}

function $setToDate(this$static, toDate){
  this$static.toDate = toDate;
}

function $setUuid(this$static, uuid){
  this$static.uuid_0 = uuid;
}

function $getFrom(this$static){
  return this$static.from;
}

function $getOrgName(this$static){
  return this$static.orgName;
}

function $getPosition_1(this$static){
  return this$static.position_0;
}

function $getTo(this$static){
  return this$static.to;
}

function $getUuid_2(this$static){
  return this$static.uuid_0;
}

function $isActualPosition_0(this$static){
  return this$static.actualPosition;
}

function $setActualPosition_0(this$static, actualPosition){
  this$static.actualPosition = actualPosition;
}

function $setFrom(this$static, from){
  this$static.from = from;
}

function $setOrgName(this$static, orgName){
  this$static.orgName = orgName;
}

function $setPosition_2(this$static, position){
  this$static.position_0 = position;
}

function $setTo(this$static, to){
  this$static.to = to;
}

function $setUuid_0(this$static, uuid){
  this$static.uuid_0 = uuid;
}

_ = AdminUserConstants_.prototype;
_.aboutYourself_helpText = function aboutYourself_helpText(){
  return 'Short resume helps others to get to know you better. It should highlight in a few sentences both your professional and personal experience, skills, abilities and interests.';
}
;
_.aboutYourself_helpTitle = function aboutYourself_helpTitle(){
  return 'Why should I fill my resume?';
}
;
_.aboutYourself_text_pleaseEnterBio = function aboutYourself_text_pleaseEnterBio(){
  return 'Here you can enter your resume.';
}
;
_.aboutYourself_text_successBioUpdate = function aboutYourself_text_successBioUpdate(){
  return 'Personal resume has been saved.';
}
;
_.aboutYourself_title_bio = function aboutYourself_title_bio(){
  return 'About yourself';
}
;
_.accountMenu_title_account = function accountMenu_title_account(){
  return 'Account details';
}
;
_.experience_title = function experience_title(){
  return 'Experience';
}
;
_.myResume_title = function myResume_title(){
  return 'My resume';
}
;
_.newUserOrganizationDialog_title = function newUserOrganizationDialog_title(){
  return 'New experience';
}
;
_.organizationDetailForm_form_industry = function organizationDetailForm_form_industry(){
  return 'Industry';
}
;
_.organizationDetailForm_form_internationalName = function organizationDetailForm_form_internationalName(){
  return 'International name';
}
;
_.organizationDetailForm_form_moreNameOptions = function organizationDetailForm_form_moreNameOptions(){
  return 'Show more name options';
}
;
_.organizationSuggestOracle_text_createNewOrganization = function organizationSuggestOracle_text_createNewOrganization(){
  return 'Create new organization';
}
;
_.passwordChangeContent_button_changePassword = function passwordChangeContent_button_changePassword(){
  return 'CHANGE PASSWORD';
}
;
_.passwordChangeContent_error_badLengthOfOldPassword = function passwordChangeContent_error_badLengthOfOldPassword(){
  return 'Bad length of your current password.';
}
;
_.passwordChangeContent_error_oldPasswordIsEmpty = function passwordChangeContent_error_oldPasswordIsEmpty(){
  return 'Your current password must be entered.';
}
;
_.passwordChangeContent_form_newPassword = function passwordChangeContent_form_newPassword(){
  return 'New password';
}
;
_.passwordChangeContent_form_oldPassword = function passwordChangeContent_form_oldPassword(){
  return 'Old password';
}
;
_.passwordChangeContent_text_successPasswordChange = function passwordChangeContent_text_successPasswordChange(){
  return 'The password has been changed.';
}
;
_.personInfoContent_form_contactEmail = function personInfoContent_form_contactEmail(){
  return 'Contact e-mail';
}
;
_.personInfoContent_form_frontDegree = function personInfoContent_form_frontDegree(){
  return 'Front degree';
}
;
_.personInfoContent_form_rearDegree = function personInfoContent_form_rearDegree(){
  return 'Rear degree';
}
;
_.personInfoContent_text_successPersonalInfoUpdate = function personInfoContent_text_successPersonalInfoUpdate(){
  return 'Your personal details have been saved.';
}
;
_.personalInfo_text_successPersonalInfoUpdate = function personalInfo_text_successPersonalInfoUpdate(){
  return 'Your personal informations have been updated.';
}
;
_.personalInfo_title = function personalInfo_title(){
  return 'Personal info';
}
;
_.privateOrgContent_helpText = function privateOrgContent_helpText(){
  return 'When you attend an event, organizers may need to know your afiliation. It can be pre-filled into the registration form which will save your time.';
}
;
_.privateOrgContent_helpTitle = function privateOrgContent_helpTitle(){
  return 'For what is organization used?';
}
;
_.privateOrgContent_text_successAddressUpdate = function privateOrgContent_text_successAddressUpdate(){
  return 'The address has been saved.';
}
;
_.privateOrgContent_title_personalAddress = function privateOrgContent_title_personalAddress(){
  return 'Personal address';
}
;
_.profileImageContent_button_picture = function profileImageContent_button_picture(){
  return 'PICTURE';
}
;
_.profileImageContent_error_profileUpdateFailed = function profileImageContent_error_profileUpdateFailed(){
  return 'The profile image update has failed.';
}
;
_.profileImageContent_text_profileUpdateSuccessfull = function profileImageContent_text_profileUpdateSuccessfull(){
  return 'The profile image has been updated.';
}
;
_.profileMenuContent_title = function profileMenuContent_title(){
  return 'Personal profile';
}
;
_.userOrg_error_invalidIdOfExperience = function userOrg_error_invalidIdOfExperience(){
  return 'This is not a proper experience';
}
;
_.userOrg_error_userOrganizationNotExists = function userOrg_error_userOrganizationNotExists(){
  return 'Cannot delete this experience.';
}
;
_.userOrg_form_at = function userOrg_form_at(){
  return 'At';
}
;
_.userOrg_form_currentlyWork = function userOrg_form_currentlyWork(){
  return 'I currently work here';
}
;
_.userOrg_form_position = function userOrg_form_position(){
  return 'Position';
}
;
_.userOrg_form_preferredPosition = function userOrg_form_preferredPosition(){
  return 'Preferred position';
}
;
_.userOrg_form_timePeriod = function userOrg_form_timePeriod(){
  return 'Time period';
}
;
_.userOrg_subtitle_currentlyWork = function userOrg_subtitle_currentlyWork(){
  return 'I currently work here';
}
;
_.userOrg_subtitle_pastPositions = function userOrg_subtitle_pastPositions(){
  return 'Past positions';
}
;
_.userOrg_text_at = function userOrg_text_at(){
  return 'at';
}
;
_.userOrg_text_presentLower = function userOrg_text_presentLower(){
  return 'present';
}
;
_.userOrganizationsContent_error_experienceListFailed = function userOrganizationsContent_error_experienceListFailed(){
  return 'Cannot load the experience list.';
}
;
_.userOrganizationsContent_text_experienceSucessfullyUpdated = function userOrganizationsContent_text_experienceSucessfullyUpdated(){
  return 'The experience has been updated.';
}
;
_.userOrganizationsContent_title = function userOrganizationsContent_title(){
  return 'Career & experience';
}
;
function $clinit_123(){
  $clinit_123 = nullMethod;
  $clinit_6();
  NAME_2 = ($clinit_131() , c_1).aboutYourself_title_bio();
}

function $$init_636(){
}

function AboutYourself_0(){
  $clinit_123();
  SimpleFormPanel_0.call(this);
  $$init_636();
  this.addWidget(new PersonInfoContent_0);
  this.addWidget(addedStyle('marB20'));
  this.addWidget(new PrivateOrgContent_0);
  this.addWidget(addedStyle('marB20'));
  this.addWidget(new PersonalInfo_0);
  this.addWidget(addedStyle('marB20'));
  this.addWidget(new MyResume_0);
}

function AboutYourself(){
}

_ = AboutYourself_0.prototype = AboutYourself.prototype = new SimpleFormPanel;
_.getClass$ = function getClass_637(){
  return Lconsys_admin_user_gwt_client_module_profile_AboutYourself_2_classLit;
}
;
_.getName = function getName_9(){
  return NAME_2;
}
;
_.getNote = function getNote_2(){
  return null;
}
;
_.onShow = function onShow_2(panel){
  panel.setWidget(this);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
var NAME_2;
function $clinit_124(){
  $clinit_124 = nullMethod;
  $clinit_6();
  NAME_3 = ($clinit_131() , c_1).accountMenu_title_account();
}

function $$init_637(){
}

function $init_3(this$static){
  this$static.addWidget(new PasswordChangeContent_0);
  this$static.initialized = true;
}

function AccountMenu_0(){
  $clinit_124();
  SimpleFormPanel_0.call(this);
  $$init_637();
  this.initialized = false;
}

function AccountMenu(){
}

_ = AccountMenu_0.prototype = AccountMenu.prototype = new SimpleFormPanel;
_.getClass$ = function getClass_638(){
  return Lconsys_admin_user_gwt_client_module_profile_AccountMenu_2_classLit;
}
;
_.getName = function getName_10(){
  return NAME_3;
}
;
_.getNote = function getNote_3(){
  return null;
}
;
_.onShow = function onShow_3(panel){
  this.initialized || $init_3(this);
  panel.setWidget(this);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.initialized = false;
var NAME_3;
function $clinit_125(){
  $clinit_125 = nullMethod;
  $clinit_6();
  NAME_4 = ($clinit_131() , c_1).experience_title();
}

function $$init_638(){
}

function $init_4(this$static){
  this$static.addWidget(new UserOrganizationsContent_0);
  this$static.initialized = true;
}

function Experience_0(){
  $clinit_125();
  SimpleFormPanel_0.call(this);
  $$init_638();
  this.initialized = false;
}

function Experience(){
}

_ = Experience_0.prototype = Experience.prototype = new SimpleFormPanel;
_.getClass$ = function getClass_639(){
  return Lconsys_admin_user_gwt_client_module_profile_Experience_2_classLit;
}
;
_.getName = function getName_11(){
  return NAME_4;
}
;
_.getNote = function getNote_4(){
  return null;
}
;
_.onShow = function onShow_4(panel){
  this.initialized || $init_4(this);
  panel.setWidget(this);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.initialized = false;
var NAME_4;
function $$init_639(){
}

function ProfileMenuContent_0(){
  $clinit_6();
  MenuFormPanel_0.call(this, ($clinit_131() , c_1).profileMenuContent_title());
  $$init_639();
  $addItem(this, new AboutYourself_0);
  $addItem(this, new Experience_0);
  $addItem(this, new AccountMenu_0);
  $initWidget_0(this);
}

function ProfileMenuContent(){
}

_ = ProfileMenuContent_0.prototype = ProfileMenuContent.prototype = new MenuFormPanel;
_.getClass$ = function getClass_640(){
  return Lconsys_admin_user_gwt_client_module_profile_ProfileMenuContent_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1, 401:1};
var instance_11 = null;
function $onSuccess_29(this$static){
  isNull(($clinit_6() , instance_11)) && ($clinit_6() , instance_11 = new ProfileMenuContent_0);
  this$static.val$asyncModule.onSuccess(($clinit_6() , instance_11));
}

function $clinit_126(){
  $clinit_126 = nullMethod;
  $clinit_6();
  NAME_5 = ($clinit_131() , c_1).myResume_title();
}

function $$init_642(){
}

function $confirmUpdateClickHandler(this$static){
  return new MyResume$3_0(this$static);
}

function $readForm(this$static){
  var panel;
  $setEnabled_6(this$static.bioBox, false);
  panel = new FlowPanel_0;
  panel.add_1(getOneLinedReadMode(this$static, ($clinit_131() , c_1).myResume_title(), ($clinit_515() , c_4).const_edit()));
  panel.add_1(this$static.bioBox);
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadLoggedUserAction_0, this$static.callback, this$static));
  return panel;
}

function MyResume_0(){
  $clinit_126();
  RUSimpleHelpFormPanel_1.call(this, true);
  $$init_642();
  this.bioBox = new ConsysTextArea_1(2048, '480px', '180px', NAME_5);
  $setEmptyText(this.bioBox, ($clinit_131() , c_1).aboutYourself_text_pleaseEnterBio());
  this.saveBio = getCheckButton(($clinit_515() , c_4).const_saveUpper());
  this.saveBio.addStyleName('floatL');
  this.saveBio.addStyleName('marR20');
  $addClickHandler_4(this.saveBio, $confirmUpdateClickHandler(this));
  this.callback = new MyResume$1_0(this);
  $addHelpTitle(this, ($clinit_131() , c_1).aboutYourself_helpTitle());
  $addHelpText(this, ($clinit_131() , c_1).aboutYourself_helpText());
}

function MyResume(){
}

_ = MyResume_0.prototype = MyResume.prototype = new RUSimpleHelpFormPanel;
_.confirmUpdateClickHandler = function confirmUpdateClickHandler(id){
  return $confirmUpdateClickHandler(this);
}
;
_.getClass$ = function getClass_643(){
  return Lconsys_admin_user_gwt_client_module_profile_account_MyResume_2_classLit;
}
;
_.readForm = function readForm(){
  return $readForm(this);
}
;
_.updateForm = function updateForm(id){
  var cancel, controlPanel, panel, titleLabel;
  $setEnabled_6(this.bioBox, true);
  titleLabel = getStyledLabel(($clinit_131() , c_1).myResume_title(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f16px']));
  titleLabel.addStyleName('fontBold');
  titleLabel.addStyleName('marB20');
  cancel = new ActionLabel_1(($clinit_515() , c_4).const_cancel(), 'floatL marT3');
  $setClickHandler(cancel, new MyResume$2_0(this));
  controlPanel = new FlowPanel_0;
  controlPanel.add_1(this.saveBio);
  controlPanel.add_1(cancel);
  controlPanel.add_1(clearDiv_0());
  panel = new FlowPanel_0;
  panel.add_1(titleLabel);
  panel.add_1(this.bioBox);
  panel.add_1(controlPanel);
  return panel;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.bioBox = null;
_.callback = null;
_.cu = null;
_.originalBio = null;
_.saveBio = null;
var NAME_5;
function $$init_643(){
}

function $onSuccess_30(this$static, result){
  var text;
  this$static.this$0.cu = result;
  text = $getBio(this$static.this$0.cu);
  this$static.this$0.originalBio = text;
  $setText_10(this$static.this$0.bioBox, text);
}

function MyResume$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_643();
}

function MyResume$1(){
}

_ = MyResume$1_0.prototype = MyResume$1.prototype = new Object_0;
_.getClass$ = function getClass_644(){
  return Lconsys_admin_user_gwt_client_module_profile_account_MyResume$1_2_classLit;
}
;
_.onFailure = function onFailure_25(caught){
}
;
_.onSuccess = function onSuccess_31(result){
  $onSuccess_30(this, dynamicCast(result, 354));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_644(){
}

function MyResume$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_644();
}

function MyResume$2(){
}

_ = MyResume$2_0.prototype = MyResume$2.prototype = new Object_0;
_.getClass$ = function getClass_645(){
  return Lconsys_admin_user_gwt_client_module_profile_account_MyResume$2_2_classLit;
}
;
_.onClick = function onClick_29(event_0){
  $clearMessageBox(this.this$0);
  $setText_10(this.this$0.bioBox, this.this$0.originalBio);
  $setWidget_2(this.this$0, $readForm(this.this$0));
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_645(){
}

function MyResume$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_645();
}

function MyResume$3(){
}

_ = MyResume$3_0.prototype = MyResume$3.prototype = new Object_0;
_.getClass$ = function getClass_646(){
  return Lconsys_admin_user_gwt_client_module_profile_account_MyResume$3_2_classLit;
}
;
_.onClick = function onClick_30(event_0){
  var text;
  text = $trim($getText_7(this.this$0.bioBox));
  $setBio(this.this$0.cu, text);
  $fireEvent_3(get_10(), new DispatchEvent_0(new UpdateUserAction_0(this.this$0.cu), new MyResume$3$1_0(this, text), this.this$0));
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_646(){
}

function $onSuccess_31(this$static){
  this$static.this$1.this$0.originalBio = this$static.val$text;
  $onSuccessUpdate(this$static.this$1.this$0, ($clinit_131() , c_1).aboutYourself_text_successBioUpdate());
}

function MyResume$3$1_0(this$1, val$text){
  this.this$1 = this$1;
  this.val$text = val$text;
  Object_1.call(this);
  $$init_646();
}

function MyResume$3$1(){
}

_ = MyResume$3$1_0.prototype = MyResume$3$1.prototype = new Object_0;
_.getClass$ = function getClass_647(){
  return Lconsys_admin_user_gwt_client_module_profile_account_MyResume$3$1_2_classLit;
}
;
_.onFailure = function onFailure_26(caught){
  $setText_10(this.this$1.this$0.bioBox, this.this$1.this$0.originalBio);
}
;
_.onSuccess = function onSuccess_32(result){
  $onSuccess_31(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
_.val$text = null;
function RUSimpleFormPanel_1(width, withHiddableLabel){
  SimpleFormPanel_4.call(this, null, width, withHiddableLabel);
  $$init_647();
}

function $$init_648(){
}

function $initHandlers_3(this$static){
  $addKeyUpHandler(this$static.oldPasswordBox, new PasswordChangeContent$5_0(this$static));
  $addKeyUpHandler(this$static.fakeOldPasswordBox, new PasswordChangeContent$6_0(this$static));
  $addKeyUpHandler(this$static.newPasswordBox, passwordStrengthHandler(this$static.newPasswordBox, this$static.passwordStrengthLabel));
  $addKeyUpHandler(this$static.newPasswordBox, new PasswordChangeContent$7_0(this$static));
  $addKeyUpHandler(this$static.fakeNewPasswordBox, passwordStrengthHandler(this$static.fakeNewPasswordBox, this$static.passwordStrengthLabel));
  $addKeyUpHandler(this$static.fakeNewPasswordBox, new PasswordChangeContent$8_0(this$static));
  $addChangeValueHandler(this$static.showPassword, new PasswordChangeContent$9_0(this$static));
}

function PasswordChangeContent_0(){
  $clinit_6();
  RUSimpleFormPanel_0.call(this, true);
  $$init_648();
}

function PasswordChangeContent(){
}

_ = PasswordChangeContent_0.prototype = PasswordChangeContent.prototype = new RUSimpleFormPanel;
_.confirmUpdateClickHandler = function confirmUpdateClickHandler_0(id){
  return new PasswordChangeContent$10_0(this);
}
;
_.getClass$ = function getClass_649(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent_2_classLit;
}
;
_.readForm = function readForm_0(){
  return getOneLinedReadMode(this, ($clinit_518() , c_5).const_password(), ($clinit_515() , c_4).const_change());
}
;
_.updateForm = function updateForm_0(id){
  var checkPanel, newPasswordPanel, oldPasswordPanel, passwordAgainPanel;
  this.form = new BaseForm_0;
  this.form.setStyleName('marB10');
  this.passwordStrengthLabel = getStyledLabel('', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['fontBold', 'floatR']));
  this.oldPasswordBox = new PasswordChangeContent$1_0(this, '100px', 6, 255, ($clinit_131() , c_1).passwordChangeContent_form_oldPassword());
  this.newPasswordBox = new ConsysPasswordTextBox_0('100px', 6, 255, ($clinit_131() , c_1).passwordChangeContent_form_newPassword());
  this.fakeOldPasswordBox = new ConsysStringTextBox_2('100px', 6, 255, ($clinit_131() , c_1).passwordChangeContent_form_oldPassword());
  this.fakeNewPasswordBox = new ConsysStringTextBox_2('100px', 6, 255, ($clinit_131() , c_1).passwordChangeContent_form_newPassword());
  this.confirmPasswordBox = new PasswordChangeContent$2_0(this, '100px', 6, 255, ($clinit_518() , c_5).const_confirmPassword());
  this.fakeOldPasswordBox.setVisible(false);
  this.fakeNewPasswordBox.setVisible(false);
  oldPasswordPanel = new PasswordChangeContent$3_0(this);
  $add_12(oldPasswordPanel, this.oldPasswordBox);
  $add_12(oldPasswordPanel, this.fakeOldPasswordBox);
  this.showPassword = new ConsysCheckBox_0;
  this.showPassword.addStyleName('floatL');
  checkPanel = new FlowPanel_0;
  checkPanel.addStyleName('floatR');
  checkPanel.addStyleName('marT3');
  checkPanel.add_1(this.showPassword);
  checkPanel.add_1(getStyledLabel(($clinit_518() , c_5).const_showPassword(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL5', 'floatL'])));
  checkPanel.add_1(clearDiv_0());
  newPasswordPanel = new PasswordChangeContent$4_0(this);
  $add_12(newPasswordPanel, checkPanel);
  $add_12(newPasswordPanel, this.newPasswordBox);
  $add_12(newPasswordPanel, this.fakeNewPasswordBox);
  passwordAgainPanel = new ConsysFlowPanel_0;
  $add_12(passwordAgainPanel, this.passwordStrengthLabel);
  $add_12(passwordAgainPanel, this.confirmPasswordBox);
  $add_12(passwordAgainPanel, clearDiv_0());
  $initHandlers_3(this);
  $addRequired(this.form, 0, ($clinit_131() , c_1).passwordChangeContent_form_oldPassword(), oldPasswordPanel);
  $addRequired(this.form, 1, ($clinit_131() , c_1).passwordChangeContent_form_newPassword(), newPasswordPanel);
  $addRequired(this.form, 2, ($clinit_518() , c_5).const_confirmPassword(), passwordAgainPanel);
  $addActionMembers(this.form, 3, getUpdateButton_0(($clinit_131() , c_1).passwordChangeContent_button_changePassword(), this, id), getCancelButton(this));
  return this.form;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.check = null;
_.confirmPasswordBox = null;
_.fakeNewPasswordBox = null;
_.fakeOldPasswordBox = null;
_.form = null;
_.newPasswordBox = null;
_.oldPasswordBox = null;
_.passwordStrengthLabel = null;
_.showPassword = null;
function $$init_649(){
}

function PasswordChangeContent$1_0(this$0, $anonymous0, $anonymous1, $anonymous2, $anonymous3){
  $clinit_94();
  this , this$0;
  ConsysPasswordTextBox_0.call(this, $anonymous0, $anonymous1, $anonymous2, $anonymous3);
  $$init_649();
}

function PasswordChangeContent$1(){
}

_ = PasswordChangeContent$1_0.prototype = PasswordChangeContent$1.prototype = new ConsysPasswordTextBox;
_.doValidate = function doValidate_8(fail){
  var result;
  result = isLenght($trim(this.getText()), this.min_0, this.max_0);
  if (result != 0) {
    isValidString($trim(this.getText()))?$addOrSetText(fail, ($clinit_131() , c_1).passwordChangeContent_error_badLengthOfOldPassword()):$addOrSetText(fail, ($clinit_131() , c_1).passwordChangeContent_error_oldPasswordIsEmpty());
    return false;
  }
  return true;
}
;
_.getClass$ = function getClass_650(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$1_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 13:1, 14:1, 27:1, 28:1, 29:1, 30:1, 31:1, 33:1, 34:1, 35:1, 37:1, 38:1, 39:1, 40:1, 41:1, 42:1, 43:1, 44:1, 45:1, 46:1, 47:1, 49:1, 50:1, 52:1, 147:1, 296:1, 335:1, 336:1, 456:1, 457:1, 466:1, 467:1, 499:1};
function $$init_650(){
}

function PasswordChangeContent$10_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_650();
}

function PasswordChangeContent$10(){
}

_ = PasswordChangeContent$10_0.prototype = PasswordChangeContent$10.prototype = new Object_0;
_.getClass$ = function getClass_651(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$10_2_classLit;
}
;
_.onClick = function onClick_31(event_0){
  var pass, updatePasswordEvent;
  if (!permitedCharacters($trim(this.this$0.newPasswordBox.getText()))) {
    $setText_8($getFailMessage(this.this$0), ($clinit_131() , c_1).changePasswordContent_error_invalidCharactersInPassword());
    return;
  }
  pass = new ClientPassword_0;
  $setOldPassword(pass, $trim(this.this$0.oldPasswordBox.getText()));
  $setNewPassword(pass, $trim(this.this$0.newPasswordBox.getText()));
  $setConfirmPassword(pass, $trim(this.this$0.confirmPasswordBox.getText()));
  this.this$0.confirmPasswordBox.isEnabled_0()?$setConfirmPassword(pass, $trim(this.this$0.confirmPasswordBox.getText())):$setConfirmPassword(pass, $trim($getText_4(this.this$0.fakeNewPasswordBox)));
  this.this$0.check = pass;
  if (!$validate(this.this$0.form, $getFailMessage(this.this$0))) {
    return;
  }
  updatePasswordEvent = new DispatchEvent_0(new UpdatePasswordAction_0($getNewPassword(pass)), new PasswordChangeContent$10$1_0(this), $getSelf(this.this$0));
  $fireEvent_3(get_10(), updatePasswordEvent);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_651(){
}

function $onSuccess_32(this$static){
  $onSuccessUpdate_0(this$static.this$1.this$0, ($clinit_131() , c_1).passwordChangeContent_text_successPasswordChange());
}

function PasswordChangeContent$10$1_0(this$1){
  this.this$1 = this$1;
  Object_1.call(this);
  $$init_651();
}

function PasswordChangeContent$10$1(){
}

_ = PasswordChangeContent$10$1_0.prototype = PasswordChangeContent$10$1.prototype = new Object_0;
_.getClass$ = function getClass_652(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$10$1_2_classLit;
}
;
_.onFailure = function onFailure_27(caught){
}
;
_.onSuccess = function onSuccess_33(result){
  $onSuccess_32(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
function $$init_652(){
}

function PasswordChangeContent$2_0(this$0, $anonymous0, $anonymous1, $anonymous2, $anonymous3){
  $clinit_94();
  this.this$0 = this$0;
  ConsysPasswordTextBox_0.call(this, $anonymous0, $anonymous1, $anonymous2, $anonymous3);
  $$init_652();
}

function PasswordChangeContent$2(){
}

_ = PasswordChangeContent$2_0.prototype = PasswordChangeContent$2.prototype = new ConsysPasswordTextBox;
_.doValidate = function doValidate_9(fail){
  var result;
  result = true;
  if (!changePassword($getNewPassword(this.this$0.check), $getConfirmPassword(this.this$0.check))) {
    $addOrSetText(fail, ($clinit_518() , c_5).const_passwordsNotMatch());
    result = false;
  }
  return result;
}
;
_.getClass$ = function getClass_653(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$2_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 13:1, 14:1, 27:1, 28:1, 29:1, 30:1, 31:1, 33:1, 34:1, 35:1, 37:1, 38:1, 39:1, 40:1, 41:1, 42:1, 43:1, 44:1, 45:1, 46:1, 47:1, 49:1, 50:1, 52:1, 147:1, 296:1, 335:1, 336:1, 456:1, 457:1, 466:1, 467:1, 499:1};
_.this$0 = null;
function $$init_653(){
}

function PasswordChangeContent$3_0(this$0){
  this.this$0 = this$0;
  ConsysFlowPanel_0.call(this);
  $$init_653();
}

function PasswordChangeContent$3(){
}

_ = PasswordChangeContent$3_0.prototype = PasswordChangeContent$3.prototype = new ConsysFlowPanel;
_.doValidate = function doValidate_10(fail){
  return this.this$0.oldPasswordBox.doValidate(fail);
}
;
_.getClass$ = function getClass_654(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$3_2_classLit;
}
;
_.onFail = function onFail_7(){
  $onFail_0(this.this$0.oldPasswordBox);
  $onFail(this.this$0.fakeOldPasswordBox);
}
;
_.onSuccess_0 = function onSuccess_34(){
  $onSuccess_13(this.this$0.oldPasswordBox);
  $onSuccess_11(this.this$0.fakeOldPasswordBox);
}
;
_.castableTypeMap$ = {2:1, 18:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1, 457:1, 463:1, 490:1};
_.this$0 = null;
function $$init_654(){
}

function PasswordChangeContent$4_0(this$0){
  this.this$0 = this$0;
  ConsysFlowPanel_0.call(this);
  $$init_654();
}

function PasswordChangeContent$4(){
}

_ = PasswordChangeContent$4_0.prototype = PasswordChangeContent$4.prototype = new ConsysFlowPanel;
_.doValidate = function doValidate_11(fail){
  if (!$equals_0(passwordStrength($getNewPassword(this.this$0.check)), ($clinit_519() , STRONG))) {
    $addOrSetText(fail, ($clinit_518() , c_5).const_passwordIsWeak());
    return false;
  }
  if ($contains_1($getNewPassword(this.this$0.check), ' ')) {
    $addOrSetText(fail, ($clinit_518() , c_5).const_passwordContainsSpace());
    return false;
  }
  return true;
}
;
_.getClass$ = function getClass_655(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$4_2_classLit;
}
;
_.onFail = function onFail_8(){
  $onFail_0(this.this$0.newPasswordBox);
  $onFail(this.this$0.fakeNewPasswordBox);
}
;
_.onSuccess_0 = function onSuccess_35(){
  $onSuccess_13(this.this$0.newPasswordBox);
  $onSuccess_11(this.this$0.fakeNewPasswordBox);
}
;
_.castableTypeMap$ = {2:1, 18:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1, 457:1, 463:1, 490:1};
_.this$0 = null;
function $$init_655(){
}

function PasswordChangeContent$5_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_655();
}

function PasswordChangeContent$5(){
}

_ = PasswordChangeContent$5_0.prototype = PasswordChangeContent$5.prototype = new Object_0;
_.getClass$ = function getClass_656(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$5_2_classLit;
}
;
_.onKeyUp = function onKeyUp_4(event_0){
  $setText_3(this.this$0.fakeOldPasswordBox, this.this$0.oldPasswordBox.getText());
}
;
_.castableTypeMap$ = {77:1, 101:1};
_.this$0 = null;
function $$init_656(){
}

function PasswordChangeContent$6_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_656();
}

function PasswordChangeContent$6(){
}

_ = PasswordChangeContent$6_0.prototype = PasswordChangeContent$6.prototype = new Object_0;
_.getClass$ = function getClass_657(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$6_2_classLit;
}
;
_.onKeyUp = function onKeyUp_5(event_0){
  this.this$0.oldPasswordBox.setText($getText_4(this.this$0.fakeOldPasswordBox));
}
;
_.castableTypeMap$ = {77:1, 101:1};
_.this$0 = null;
function $$init_657(){
}

function PasswordChangeContent$7_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_657();
}

function PasswordChangeContent$7(){
}

_ = PasswordChangeContent$7_0.prototype = PasswordChangeContent$7.prototype = new Object_0;
_.getClass$ = function getClass_658(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$7_2_classLit;
}
;
_.onKeyUp = function onKeyUp_6(event_0){
  $setText_3(this.this$0.fakeNewPasswordBox, this.this$0.newPasswordBox.getText());
}
;
_.castableTypeMap$ = {77:1, 101:1};
_.this$0 = null;
function $$init_658(){
}

function PasswordChangeContent$8_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_658();
}

function PasswordChangeContent$8(){
}

_ = PasswordChangeContent$8_0.prototype = PasswordChangeContent$8.prototype = new Object_0;
_.getClass$ = function getClass_659(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$8_2_classLit;
}
;
_.onKeyUp = function onKeyUp_7(event_0){
  this.this$0.newPasswordBox.setText($getText_4(this.this$0.fakeNewPasswordBox));
}
;
_.castableTypeMap$ = {77:1, 101:1};
_.this$0 = null;
function $$init_659(){
}

function PasswordChangeContent$9_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_659();
}

function PasswordChangeContent$9(){
}

_ = PasswordChangeContent$9_0.prototype = PasswordChangeContent$9.prototype = new Object_0;
_.getClass$ = function getClass_660(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$9_2_classLit;
}
;
_.onChangeValue = function onChangeValue_2(event_0){
  var value;
  $booleanValue(dynamicCast($getValue_12(event_0), 159)) && $onSuccess_13(this.this$0.confirmPasswordBox);
  value = $booleanValue(dynamicCast($getValue_12(event_0), 159));
  this.this$0.oldPasswordBox.setVisible(!value);
  this.this$0.fakeOldPasswordBox.setVisible(value);
  this.this$0.newPasswordBox.setVisible(!value);
  this.this$0.fakeNewPasswordBox.setVisible(value);
  this.this$0.confirmPasswordBox.setEnabled(!value);
  this.this$0.confirmPasswordBox.setText('');
}
;
_.castableTypeMap$ = {101:1, 458:1};
_.this$0 = null;
function $$init_660(){
}

function $generateClientUser(this$static){
  var gender, result;
  result = this$static.user;
  $setFirstName_1(result, $trim(this$static.firstNameBox.getText()));
  $setMiddleName(result, $trim(this$static.middleNameBox.getText()));
  $setLastName_1(result, $trim(this$static.lastNameBox.getText()));
  $setFrontDegree(result, $trim(this$static.frontDegreeBox.getText()));
  $setRearDegree(result, $trim(this$static.rearDegreeBox.getText()));
  this$static.enterEmail && $setLoginEmail_0(result, $trim(this$static.emailBox.getText()));
  gender = 0;
  $booleanValue($getValue_2(this$static.manBox))?(gender = 1):$booleanValue($getValue_2(this$static.womanBox)) && (gender = 2);
  $setGender(result, gender);
  return result;
}

function PersonInfoContent_0(){
  $clinit_6();
  RUSimpleFormPanel_0.call(this, true);
  $$init_660();
}

function PersonInfoContent(){
}

_ = PersonInfoContent_0.prototype = PersonInfoContent.prototype = new RUSimpleFormPanel;
_.confirmUpdateClickHandler = function confirmUpdateClickHandler_1(id){
  return new PersonInfoContent$4_0(this);
}
;
_.getClass$ = function getClass_661(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent_2_classLit;
}
;
_.onLoad = function onLoad_19(){
  $onLoad_3(this);
  $addHandler_5(get_10(), ($clinit_121() , TYPE_30), this);
}
;
_.onUnload = function onUnload_3(){
  $removeHandler(get_10(), ($clinit_121() , TYPE_30 , this));
}
;
_.onUpdatedUser = function onUpdatedUser_0(event_0){
  this.user = $getUser(event_0);
  this.frontDegreeLabel.setText($getFrontDegree(this.user));
  this.nameLabel.setText($name_0(this.user));
  this.rearDegreeLabel.setText($getRearDegree(this.user));
}
;
_.readForm = function readForm_1(){
  var edit, panel, portraitPanel, viewPanel, wrapper;
  viewPanel = new FlowPanel_0;
  viewPanel.setStyleName('marB10');
  viewPanel.addStyleName('floatL');
  portraitPanel = new ProfileImageContent_0($getHiddableLabel(this));
  portraitPanel.addStyleName('floatR');
  $addHandler_5(get_10(), ($clinit_121() , TYPE_30), portraitPanel);
  this.frontDegreeLabel = new PersonInfoContent$2_0(this);
  this.frontDegreeLabel.addStyleName('marR10');
  this.frontDegreeLabel.addStyleName('marT3');
  this.frontDegreeLabel.addStyleName('floatL');
  this.nameLabel = getStyledLabel('', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f16px', 'fontBold', 'floatL']));
  this.rearDegreeLabel = getStyledLabel('', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL10', 'floatL', 'marT3']));
  edit = getEditModeButton_1(this, null);
  $addStyleName_1(edit, 'floatL');
  $addStyleName_1(edit, 'marL10');
  $addStyleName_1(edit, 'marT3');
  panel = new FlowPanel_0;
  panel.setWidth('400px');
  panel.add_1(this.frontDegreeLabel);
  panel.add_1(this.nameLabel);
  panel.add_1(this.rearDegreeLabel);
  panel.add_1(edit);
  panel.add_1(clearDiv_0());
  viewPanel.add_1(panel);
  this.positionLabel = getStyledLabel('', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL20', 'marT10']));
  viewPanel.add_1(this.positionLabel);
  this.contactEmailLabel = getStyledLabel('', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL20', 'marT10']));
  viewPanel.add_1(this.contactEmailLabel);
  this.viewUserCallback = new PersonInfoContent$3_0(this);
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadLoggedUserAction_0, this.viewUserCallback, $getSelf(this)));
  wrapper = new FlowPanel_0;
  wrapper.setWidth('700px');
  wrapper.add_1(viewPanel);
  wrapper.add_1(portraitPanel);
  wrapper.add_1(clearDiv_0());
  wrapper.setVisible(true);
  return wrapper;
}
;
_.updateForm = function updateForm_1(id){
  var degreePanel, genderPanel, middleLabel;
  this.form = new BaseForm_0;
  this.form.setStyleName('marB10');
  this.firstNameBox = getFormInput();
  this.middleNameBox = getFormInput();
  this.lastNameBox = getFormInput();
  this.frontDegreeBox = getFormInputHalf();
  this.rearDegreeBox = getFormInputHalf();
  this.emailBox = getFormInput();
  this.notSpecBox = new RadioButton_0('gender');
  $setValue_0(this.notSpecBox, valueOf_48(true));
  this.manBox = new RadioButton_0('gender');
  this.womanBox = new RadioButton_0('gender');
  middleLabel = getStyledLabel(($clinit_131() , c_1).personInfoContent_form_rearDegree() + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marR10']));
  degreePanel = new HorizontalPanel_0;
  degreePanel.setWidth('100%');
  degreePanel.setStyleName('h21');
  $setVerticalAlignment_0(degreePanel, ($clinit_90() , ALIGN_MIDDLE));
  $add_9(degreePanel, this.frontDegreeBox);
  $add_9(degreePanel, middleLabel);
  $add_9(degreePanel, this.rearDegreeBox);
  $setCellHorizontalAlignment(degreePanel, middleLabel, ($clinit_89() , ALIGN_RIGHT));
  $setCellHorizontalAlignment(degreePanel, this.rearDegreeBox, ($clinit_89() , ALIGN_RIGHT));
  genderPanel = new HorizontalPanel_0;
  genderPanel.setWidth('100%');
  genderPanel.setStyleName('h21');
  $setVerticalAlignment_0(genderPanel, ($clinit_90() , ALIGN_MIDDLE));
  $add_9(genderPanel, this.notSpecBox);
  $setCellHorizontalAlignment(genderPanel, this.notSpecBox, ($clinit_89() , ALIGN_RIGHT));
  $add_9(genderPanel, getStyledLabel(($clinit_515() , c_4).const_genderNotSpec(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL5'])));
  $add_9(genderPanel, this.manBox);
  $setCellHorizontalAlignment(genderPanel, this.manBox, ($clinit_89() , ALIGN_RIGHT));
  $add_9(genderPanel, getStyledLabel(($clinit_515() , c_4).const_genderMan(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL5'])));
  $add_9(genderPanel, this.womanBox);
  $setCellHorizontalAlignment(genderPanel, this.womanBox, ($clinit_89() , ALIGN_RIGHT));
  $add_9(genderPanel, getStyledLabel(($clinit_515() , c_4).const_genderWoman(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL5'])));
  $addRequired(this.form, 0, ($clinit_518() , c_5).const_firstName(), this.firstNameBox);
  $addOptional(this.form, 1, ($clinit_518() , c_5).const_middleName(), this.middleNameBox);
  $addRequired(this.form, 2, ($clinit_518() , c_5).const_lastName(), this.lastNameBox);
  $addOptional(this.form, 3, ($clinit_131() , c_1).personInfoContent_form_frontDegree(), degreePanel);
  $addOptional(this.form, 4, ($clinit_515() , c_4).const_gender(), genderPanel);
  $addActionMembers(this.form, 6, getUpdateButton(this, id), getCancelButton(this));
  this.loadUserCallback = new PersonInfoContent$1_0(this);
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadLoggedUserAction_0, this.loadUserCallback, $getSelf(this)));
  return this.form;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 101:1, 147:1, 399:1};
_.contactEmailLabel = null;
_.emailBox = null;
_.enterEmail = false;
_.firstNameBox = null;
_.form = null;
_.frontDegreeBox = null;
_.frontDegreeLabel = null;
_.lastNameBox = null;
_.loadUserCallback = null;
_.manBox = null;
_.middleNameBox = null;
_.nameLabel = null;
_.notSpecBox = null;
_.positionLabel = null;
_.rearDegreeBox = null;
_.rearDegreeLabel = null;
_.user = null;
_.viewUserCallback = null;
_.womanBox = null;
function $$init_661(){
}

function $onSuccess_33(this$static, result){
  var email;
  this$static.this$0.user = result;
  this$static.this$0.firstNameBox.setText($getFirstName_0(result));
  this$static.this$0.middleNameBox.setText($getMiddleName(result));
  this$static.this$0.lastNameBox.setText($getLastName_0(result));
  this$static.this$0.frontDegreeBox.setText($getFrontDegree(result));
  this$static.this$0.rearDegreeBox.setText($getRearDegree(result));
  email = $getLoginEmail(result);
  if ($contains_1(email, '@')) {
    this$static.this$0.emailBox.setText(email);
    $setReadOnly(this$static.this$0.emailBox, true);
    $addOptional(this$static.this$0.form, 5, ($clinit_131() , c_1).personInfoContent_form_contactEmail(), this$static.this$0.emailBox);
    this$static.this$0.enterEmail = false;
  }
   else {
    $setReadOnly(this$static.this$0.emailBox, false);
    $addRequired(this$static.this$0.form, 5, ($clinit_131() , c_1).personInfoContent_form_contactEmail(), this$static.this$0.emailBox);
    this$static.this$0.enterEmail = true;
  }
  switch ($getGender(result)) {
    case 0:
      $setValue_0(this$static.this$0.notSpecBox, valueOf_48(true));
      break;
    case 1:
      $setValue_0(this$static.this$0.manBox, valueOf_48(true));
      break;
    case 2:
      $setValue_0(this$static.this$0.womanBox, valueOf_48(true));
  }
}

function PersonInfoContent$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_661();
}

function PersonInfoContent$1(){
}

_ = PersonInfoContent$1_0.prototype = PersonInfoContent$1.prototype = new Object_0;
_.getClass$ = function getClass_662(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$1_2_classLit;
}
;
_.onFailure = function onFailure_28(caught){
}
;
_.onSuccess = function onSuccess_36(result){
  $onSuccess_33(this, dynamicCast(result, 354));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_662(){
}

function PersonInfoContent$2_0(this$0){
  $clinit_87();
  this , this$0;
  Label_0.call(this);
  $$init_662();
}

function PersonInfoContent$2(){
}

_ = PersonInfoContent$2_0.prototype = PersonInfoContent$2.prototype = new Label;
_.getClass$ = function getClass_663(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$2_2_classLit;
}
;
_.setText = function setText_2(text){
  $setText_0(this, text);
  this.setVisible(!(jsEquals(text, null) || $isEmpty_1(text)));
}
;
_.castableTypeMap$ = {2:1, 13:1, 14:1, 27:1, 28:1, 29:1, 30:1, 31:1, 33:1, 34:1, 35:1, 37:1, 38:1, 39:1, 40:1, 41:1, 42:1, 43:1, 44:1, 45:1, 46:1, 47:1, 49:1, 50:1, 52:1, 147:1, 296:1, 333:1, 334:1};
function $$init_663(){
}

function $onSuccess_34(this$static, result){
  var rear;
  rear = jsEquals($getRearDegree(result), null)?'':$getRearDegree(result);
  this$static.this$0.frontDegreeLabel.setText($getFrontDegree(result));
  this$static.this$0.nameLabel.setText($isEmpty_1(rear)?$name_0(result):$name_0(result) + ',');
  this$static.this$0.rearDegreeLabel.setText(rear);
  this$static.this$0.positionLabel.setText($getUserOrgDefault(result));
  $contains_1($getLoginEmail(result), '@') && this$static.this$0.contactEmailLabel.setText($getLoginEmail(result));
}

function PersonInfoContent$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_663();
}

function PersonInfoContent$3(){
}

_ = PersonInfoContent$3_0.prototype = PersonInfoContent$3.prototype = new Object_0;
_.getClass$ = function getClass_664(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$3_2_classLit;
}
;
_.onFailure = function onFailure_29(caught){
}
;
_.onSuccess = function onSuccess_37(result){
  $onSuccess_34(this, dynamicCast(result, 354));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_664(){
}

function PersonInfoContent$4_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_664();
}

function PersonInfoContent$4(){
}

_ = PersonInfoContent$4_0.prototype = PersonInfoContent$4.prototype = new Object_0;
_.getClass$ = function getClass_665(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$4_2_classLit;
}
;
_.onClick = function onClick_32(event_0){
  var cu;
  cu = $generateClientUser(this.this$0);
  if (!updateUser(cu, $getFailMessage(this.this$0))) {
    return;
  }
  if (this.this$0.enterEmail && !isValidEmailAddress($getLoginEmail(cu))) {
    $addOrSetText($getFailMessage(this.this$0), ($clinit_515() , c_4).const_error_invalidEmailAddress());
    return;
  }
  $fireEvent_3(get_10(), new DispatchEvent_0(new UpdateUserAction_0(cu), new PersonInfoContent$4$1_0(this), $getSelf(this.this$0)));
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_665(){
}

function $onSuccess_35(this$static){
  $onSuccessUpdate_0(this$static.this$1.this$0, ($clinit_131() , c_1).personInfoContent_text_successPersonalInfoUpdate());
}

function PersonInfoContent$4$1_0(this$1){
  this.this$1 = this$1;
  Object_1.call(this);
  $$init_665();
}

function PersonInfoContent$4$1(){
}

_ = PersonInfoContent$4$1_0.prototype = PersonInfoContent$4$1.prototype = new Object_0;
_.getClass$ = function getClass_666(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$4$1_2_classLit;
}
;
_.onFailure = function onFailure_30(caught){
}
;
_.onSuccess = function onSuccess_38(result){
  $onSuccess_35(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
function $$init_666(){
}

function $yesNoLabel(yes){
  return new Label_1(yesNo(yes));
}

function PersonalInfo_0(){
  $clinit_6();
  RUSimpleHelpFormPanel_1.call(this, true);
  $$init_666();
}

function PersonalInfo(){
}

_ = PersonalInfo_0.prototype = PersonalInfo.prototype = new RUSimpleHelpFormPanel;
_.confirmUpdateClickHandler = function confirmUpdateClickHandler_2(id){
  return new PersonalInfo$3_0(this);
}
;
_.getClass$ = function getClass_667(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo_2_classLit;
}
;
_.readForm = function readForm_2(){
  var bf, panel;
  bf = new BaseForm_0;
  bf.addStyleName('marB10');
  panel = new FlowPanel_0;
  panel.add_1(getOneLinedReadMode(this, ($clinit_131() , c_1).personalInfo_title(), ($clinit_515() , c_4).const_edit()));
  panel.add_1(bf);
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadPersonalInfoAction_0, new PersonalInfo$1_0(this, bf), this));
  return panel;
}
;
_.updateForm = function updateForm_2(id){
  var bf, panel, titleLabel;
  titleLabel = getStyledLabel(($clinit_131() , c_1).personalInfo_title(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f16px']));
  titleLabel.addStyleName('fontBold');
  titleLabel.addStyleName('marB20');
  this.celiacBox = new ConsysCheckBox_0;
  this.vegetarianBox = new ConsysCheckBox_0;
  bf = new BaseForm_0;
  bf.setStyleName('marB10');
  $addOptional(bf, 0, ($clinit_515() , c_4).const_personalInfo_celiac(), this.celiacBox);
  $addOptional(bf, 1, ($clinit_515() , c_4).const_personalInfo_vegetarian(), this.vegetarianBox);
  $addActionMembers(bf, 2, getUpdateButton(this, id), getCancelButton(this));
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadPersonalInfoAction_0, new PersonalInfo$2_0(this), this));
  panel = new FlowPanel_0;
  panel.add_1(titleLabel);
  panel.add_1(bf);
  return panel;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.celiacBox = null;
_.vegetarianBox = null;
function $$init_667(){
}

function $onSuccess_36(this$static, result){
  $addOptional(this$static.val$bf, 0, ($clinit_515() , c_4).const_personalInfo_celiac(), $yesNoLabel($isCeliac_0(result)));
  $addOptional(this$static.val$bf, 1, ($clinit_515() , c_4).const_personalInfo_vegetarian(), $yesNoLabel($isVegetarian_0(result)));
}

function PersonalInfo$1_0(this$0, val$bf){
  this , this$0;
  this.val$bf = val$bf;
  Object_1.call(this);
  $$init_667();
}

function PersonalInfo$1(){
}

_ = PersonalInfo$1_0.prototype = PersonalInfo$1.prototype = new Object_0;
_.getClass$ = function getClass_668(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$1_2_classLit;
}
;
_.onFailure = function onFailure_31(caught){
}
;
_.onSuccess = function onSuccess_39(result){
  $onSuccess_36(this, dynamicCast(result, 398));
}
;
_.castableTypeMap$ = {};
_.val$bf = null;
function $$init_668(){
}

function $onSuccess_37(this$static, result){
  $setValue_7(this$static.this$0.celiacBox, valueOf_48($isCeliac_0(result)));
  $setValue_7(this$static.this$0.vegetarianBox, valueOf_48($isVegetarian_0(result)));
}

function PersonalInfo$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_668();
}

function PersonalInfo$2(){
}

_ = PersonalInfo$2_0.prototype = PersonalInfo$2.prototype = new Object_0;
_.getClass$ = function getClass_669(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$2_2_classLit;
}
;
_.onFailure = function onFailure_32(caught){
}
;
_.onSuccess = function onSuccess_40(result){
  $onSuccess_37(this, dynamicCast(result, 398));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_669(){
}

function PersonalInfo$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_669();
}

function PersonalInfo$3(){
}

_ = PersonalInfo$3_0.prototype = PersonalInfo$3.prototype = new Object_0;
_.getClass$ = function getClass_670(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$3_2_classLit;
}
;
_.onClick = function onClick_33(event_0){
  var data;
  data = new ClientPersonalInfo_0;
  $setCeliac(data, $booleanValue($getValue_10(this.this$0.celiacBox)));
  $setVegetarian(data, $booleanValue($getValue_10(this.this$0.vegetarianBox)));
  $fireEvent_3(get_10(), new DispatchEvent_0(new UpdatePersonalInfoAction_0(data), new PersonalInfo$3$1_0(this), this.this$0));
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_670(){
}

function $onSuccess_38(this$static){
  $onSuccessUpdate(this$static.this$1.this$0, ($clinit_131() , c_1).personalInfo_text_successPersonalInfoUpdate());
}

function PersonalInfo$3$1_0(this$1){
  this.this$1 = this$1;
  Object_1.call(this);
  $$init_670();
}

function PersonalInfo$3$1(){
}

_ = PersonalInfo$3$1_0.prototype = PersonalInfo$3$1.prototype = new Object_0;
_.getClass$ = function getClass_671(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$3$1_2_classLit;
}
;
_.onFailure = function onFailure_33(caught){
}
;
_.onSuccess = function onSuccess_41(result){
  $onSuccess_38(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
function $$init_671(){
}

function PrivateOrgContent_0(){
  $clinit_6();
  RUSimpleHelpFormPanel_1.call(this, true);
  $$init_671();
  $addHelpTitle(this, ($clinit_131() , c_1).privateOrgContent_helpTitle());
  $addHelpText(this, ($clinit_131() , c_1).privateOrgContent_helpText());
}

function PrivateOrgContent(){
}

_ = PrivateOrgContent_0.prototype = PrivateOrgContent.prototype = new RUSimpleHelpFormPanel;
_.confirmUpdateClickHandler = function confirmUpdateClickHandler_3(id){
  return new PrivateOrgContent$3_0(this);
}
;
_.getClass$ = function getClass_672(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent_2_classLit;
}
;
_.readForm = function readForm_3(){
  var bf, loadEvent, panel;
  bf = new BaseForm_0;
  bf.setStyleName('marB10');
  loadEvent = new DispatchEvent_0(new LoadPrivateUserOrgAction_0, new PrivateOrgContent$1_0(this, bf), $getSelf(this));
  $fireEvent_3(get_10(), loadEvent);
  panel = new FlowPanel_0;
  panel.add_1(getOneLinedReadMode(this, ($clinit_131() , c_1).privateOrgContent_title_personalAddress(), ($clinit_515() , c_4).const_edit()));
  panel.add_1(bf);
  return panel;
}
;
_.updateForm = function updateForm_3(id){
  var panel, titleLabel;
  titleLabel = getStyledLabel(($clinit_131() , c_1).privateOrgContent_title_personalAddress(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f16px']));
  titleLabel.addStyleName('fontBold');
  titleLabel.addStyleName('marB20');
  this.form = new AddressForm_1(getUpdateButton(this, id), getCancelButton(this));
  this.form.setStyleName('marB10');
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadPrivateUserOrgAction_0, new PrivateOrgContent$2_0(this), $getSelf(this)));
  panel = new FlowPanel_0;
  panel.add_1(titleLabel);
  panel.add_1(this.form);
  return panel;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.form = null;
_.uo = null;
function $$init_672(){
}

function $onSuccess_39(this$static, result){
  $addOptional(this$static.val$bf, 0, ($clinit_515() , c_4).const_address_location(), new Label_1(valueOrDash($getLocationName($getAddress(result)))));
  $getStateUs($getAddress(result)) != 0 && $addOptional(this$static.val$bf, 1, ($clinit_515() , c_4).const_address_state(), new Label_1(valueOrDash($getStateUsName($getAddress(result)))));
  $addOptional(this$static.val$bf, 2, ($clinit_515() , c_4).const_address_city(), new Label_1(valueOrDash($getCity($getAddress(result)))));
  $addOptional(this$static.val$bf, 3, ($clinit_515() , c_4).const_address_street(), new Label_1(valueOrDash($getStreet($getAddress(result)))));
  $addOptional(this$static.val$bf, 4, ($clinit_515() , c_4).const_address_zip(), new Label_1(valueOrDash($getZip($getAddress(result)))));
}

function PrivateOrgContent$1_0(this$0, val$bf){
  this , this$0;
  this.val$bf = val$bf;
  Object_1.call(this);
  $$init_672();
}

function PrivateOrgContent$1(){
}

_ = PrivateOrgContent$1_0.prototype = PrivateOrgContent$1.prototype = new Object_0;
_.getClass$ = function getClass_673(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$1_2_classLit;
}
;
_.onFailure = function onFailure_34(caught){
}
;
_.onSuccess = function onSuccess_42(result){
  $onSuccess_39(this, dynamicCast(result, 371));
}
;
_.castableTypeMap$ = {};
_.val$bf = null;
function $$init_673(){
}

function $onSuccess_40(this$static, result){
  this$static.this$0.uo = result;
  $setAddress_0(this$static.this$0.form, $getAddress(this$static.this$0.uo));
}

function PrivateOrgContent$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_673();
}

function PrivateOrgContent$2(){
}

_ = PrivateOrgContent$2_0.prototype = PrivateOrgContent$2.prototype = new Object_0;
_.getClass$ = function getClass_674(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$2_2_classLit;
}
;
_.onFailure = function onFailure_35(caught){
}
;
_.onSuccess = function onSuccess_43(result){
  $onSuccess_40(this, dynamicCast(result, 371));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_674(){
}

function PrivateOrgContent$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_674();
}

function PrivateOrgContent$3(){
}

_ = PrivateOrgContent$3_0.prototype = PrivateOrgContent$3.prototype = new Object_0;
_.getClass$ = function getClass_675(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$3_2_classLit;
}
;
_.onClick = function onClick_34(event_0){
  var editEvent;
  if (isNull(this.this$0.uo)) {
    return;
  }
  $setAddress(this.this$0.uo, $getAddress_0(this.this$0.form));
  editEvent = new DispatchEvent_0(new UpdatePrivateOrgAction_0(this.this$0.uo), new PrivateOrgContent$3$1_0(this), $getSelf(this.this$0));
  $fireEvent_3(get_10(), editEvent);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_675(){
}

function $onSuccess_41(this$static){
  $onSuccessUpdate(this$static.this$1.this$0, ($clinit_131() , c_1).privateOrgContent_text_successAddressUpdate());
}

function PrivateOrgContent$3$1_0(this$1){
  this.this$1 = this$1;
  Object_1.call(this);
  $$init_675();
}

function PrivateOrgContent$3$1(){
}

_ = PrivateOrgContent$3$1_0.prototype = PrivateOrgContent$3$1.prototype = new Object_0;
_.getClass$ = function getClass_676(){
  return Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$3$1_2_classLit;
}
;
_.onFailure = function onFailure_36(caught){
}
;
_.onSuccess = function onSuccess_44(result){
  $onSuccess_41(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
function $$init_676(){
}

function $confirmUpdateClickHandler_0(this$static){
  return new ProfileImageContent$4_0(this$static);
}

function $refreshImage(this$static, cu){
  var url;
  if (jsEquals($getUuidImg(cu), null)) {
    $setResource(this$static.portrait, system_0().portraitBig());
  }
   else {
    url = getModuleBaseURL() + 'user/profile/load?id=' + $getFullUuid_1(($clinit_141() , PROFILE), $getUuidImg(cu));
    $setUrl_0(this$static.portrait, url);
  }
}

function ProfileImageContent_0(hiddableLabel){
  RUSimpleFormPanel_1.call(this, '250px', false);
  $$init_676();
  $setHiddableLabel(this, hiddableLabel);
  $setBodyWidth(this, '250px');
  this.addStyleName('overHide');
}

function ProfileImageContent(){
}

_ = ProfileImageContent_0.prototype = ProfileImageContent.prototype = new RUSimpleFormPanel;
_.confirmUpdateClickHandler = function confirmUpdateClickHandler_4(id){
  return $confirmUpdateClickHandler_0(this);
}
;
_.getClass$ = function getClass_677(){
  return Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent_2_classLit;
}
;
_.onUpdatedUser = function onUpdatedUser_1(event_0){
  $refreshImage(this, $getUser(event_0));
}
;
_.readForm = function readForm_4(){
  var editButton, viewPanel;
  viewPanel = new FlowPanel_0;
  viewPanel.setStyleName('marB10');
  setStyleAttribute($getElement(viewPanel), 'marginRight', '50px');
  this.portrait = new Image_1;
  editButton = getPersonButton(($clinit_131() , c_1).profileImageContent_button_picture());
  editButton.addStyleName('marT10');
  $addClickHandler_4(editButton, $toEditModeClickHandler_0(this, null));
  viewPanel.add_1(this.portrait);
  viewPanel.add_1(editButton);
  $fireEvent_3(get_10(), new DispatchEvent_0(new LoadLoggedUserAction_0, new ProfileImageContent$1_0(this), this));
  return viewPanel;
}
;
_.updateForm = function updateForm_4(id){
  var panel;
  this.formPanel = new FormPanel_0;
  $setAction_0(this.formPanel, ($clinit_148() , BASE_URL) + 'user/profile/save');
  $setMethod_0(this.formPanel, 'post');
  this.formPanel.setStyleName('marB10');
  $setEncoding(this.formPanel, 'multipart/form-data');
  $addSubmitHandler(this.formPanel, new ProfileImageContent$2_0(this));
  $addSubmitCompleteHandler(this.formPanel, new ProfileImageContent$3_0(this));
  panel = sendFileWrapper('imageFile', 250, null, $confirmUpdateClickHandler_0(this), $toReadModeClickHandler_0(this));
  this.formPanel.setWidget(panel);
  return this.formPanel;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 101:1, 147:1, 399:1};
_.formPanel = null;
_.loggedUser = null;
_.portrait = null;
function $$init_677(){
}

function $onSuccess_42(this$static, result){
  this$static.this$0.loggedUser = result;
  $refreshImage(this$static.this$0, result);
}

function ProfileImageContent$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_677();
}

function ProfileImageContent$1(){
}

_ = ProfileImageContent$1_0.prototype = ProfileImageContent$1.prototype = new Object_0;
_.getClass$ = function getClass_678(){
  return Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$1_2_classLit;
}
;
_.onFailure = function onFailure_37(caught){
}
;
_.onSuccess = function onSuccess_45(result){
  $onSuccess_42(this, dynamicCast(result, 354));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_678(){
}

function ProfileImageContent$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_678();
}

function ProfileImageContent$2(){
}

_ = ProfileImageContent$2_0.prototype = ProfileImageContent$2.prototype = new Object_0;
_.getClass$ = function getClass_679(){
  return Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$2_2_classLit;
}
;
_.onSubmit = function onSubmit(event_0){
  $showWaiting(this.this$0, true);
}
;
_.castableTypeMap$ = {101:1, 299:1};
_.this$0 = null;
function $$init_679(){
}

function ProfileImageContent$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_679();
}

function ProfileImageContent$3(){
}

_ = ProfileImageContent$3_0.prototype = ProfileImageContent$3.prototype = new Object_0;
_.getClass$ = function getClass_680(){
  return Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$3_2_classLit;
}
;
_.onSubmitComplete = function onSubmitComplete(event_0){
  var result;
  result = $getResults(event_0);
  result = $trim($replaceAll(result, '\\<.*?\\>', ''));
  result = $trim($replaceAll(result, '&lt;.*?&gt;', ''));
  if (containHttpFail(result, $getFailMessage(this.this$0))) {
    $onFailedUpdate_0(this.this$0, ($clinit_131() , c_1).profileImageContent_error_profileUpdateFailed());
  }
   else {
    $setUuidImg(this.this$0.loggedUser, result);
    $fireEvent_3(get_10(), new UpdatedUserEvent_0(this.this$0.loggedUser));
    $onSuccessUpdate_0(this.this$0, ($clinit_131() , c_1).profileImageContent_text_profileUpdateSuccessfull());
  }
  $showWaiting(this.this$0, false);
}
;
_.castableTypeMap$ = {101:1, 298:1};
_.this$0 = null;
function $$init_680(){
}

function ProfileImageContent$4_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_680();
}

function ProfileImageContent$4(){
}

_ = ProfileImageContent$4_0.prototype = ProfileImageContent$4.prototype = new Object_0;
_.getClass$ = function getClass_681(){
  return Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$4_2_classLit;
}
;
_.onClick = function onClick_35(event_0){
  $submit_0(this.this$0.formPanel);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_682(){
}

function $getFailMessage_1(this$static){
  var failMessage;
  failMessage = getFail();
  failMessage.addStyleName('marB10');
  setStyleAttribute($getElement(failMessage), 'marginLeft', '90px');
  this$static.failPanel.add_1(failMessage);
  return failMessage;
}

function NewUserOrganizationDialog_0(delegate){
  $clinit_95();
  CreateDialog_0.call(this, ($clinit_131() , c_1).newUserOrganizationDialog_title());
  $$init_682();
  this.delegate = delegate;
}

function NewUserOrganizationDialog(){
}

_ = NewUserOrganizationDialog_0.prototype = NewUserOrganizationDialog.prototype = new CreateDialog;
_.createForm = function createForm(){
  var fp;
  fp = new FlowPanel_0;
  this.failPanel = new FlowPanel_0;
  this.form = new NewUserOrganizationDialog$1_0(this);
  fp.add_1(this.failPanel);
  fp.add_1(this.form);
  return fp;
}
;
_.getClass$ = function getClass_683(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_NewUserOrganizationDialog_2_classLit;
}
;
_.getFailMessage = function getFailMessage(){
  return $getFailMessage_1(this);
}
;
_.onCancel = function onCancel(){
}
;
_.onSave = function onSave(){
  var createEvent, uo;
  if ($validate(this.form, $getFailMessage_1(this))) {
    uo = $getClientUserOrg(this.form);
    createEvent = new DispatchEvent_0(new CreateUserOrgAction_0(uo), new NewUserOrganizationDialog$2_0(this, uo), $getFormPanel(this));
    $fireEvent_3(get_10(), createEvent);
  }
}
;
_.castableTypeMap$ = {2:1, 11:1, 19:1, 45:1, 46:1, 47:1, 49:1, 134:1, 147:1, 337:1};
_.delegate = null;
_.failPanel = null;
_.form = null;
function $addActionWidget(this$static, row, widget){
  $setWidget(this$static.ft, row, 2, widget);
}

function $insertRow_1(this$static, beforeRow){
  $insertRow_0(this$static.ft, beforeRow);
}

_ = BaseForm.prototype;
_.insertRow = function insertRow(beforeRow){
  $insertRow_1(this, beforeRow);
}
;
function $$init_684(){
}

function $addNowCheckBoxClickHandler(this$static){
  $addChangeValueHandler(this$static.nowCheckBox, new UserOrgForm$4_0(this$static));
}

function $getClientUserOrg(this$static){
  var address, organization;
  $setPosition_1(this$static.userOrg, $trim($getText_4(this$static.positionBox)));
  $setActualPosition(this$static.userOrg, $booleanValue($getValue_10(this$static.nowCheckBox)));
  $setFromDate(this$static.userOrg, $getDate_0(this$static.fromMonthYearBox));
  $isActualPosition(this$static.userOrg) || $setToDate(this$static.userOrg, $getDate_0(this$static.toMonthYearBox));
  $setPreferredPosition(this$static.userOrg, $booleanValue($getValue_10(this$static.preferredBox)));
  if (this$static.detailForm.isVisible()) {
    $setOrganization(this$static.userOrg, $getOrganization_0(this$static.detailForm));
  }
   else {
    organization = new ClientOrganization_0;
    $setWeb_0(organization, '');
    $setUniversalName_0(organization, '');
    $setOrganizationType(organization, 0);
    $setAcronym_0(organization, '');
    $setFullName_0(organization, $getText_9(this$static.atBox));
    address = new ClientAddress_0;
    $setLocation(address, 0);
    $setCity(address, '');
    $setStreet(address, '');
    $setZip(address, '');
    $setAddress_1(organization, address);
    $setOrganization(this$static.userOrg, organization);
  }
  return this$static.userOrg;
}

function $init_5(this$static, uuid){
  var dataEvent, nowPanel;
  this$static.atBox = new SuggestionBox_0(new OrganizationSuggestOracle_0);
  $addSelectionHandler(this$static.atBox, this$static);
  $setRequired_1(this$static.atBox, true);
  $addKeyUpHandler($getTextBox_0(this$static.atBox), new UserOrgForm$1_0(this$static));
  this$static.atBox.setWidth('300px');
  this$static.positionBox = new ConsysStringTextBox_2('300px', 1, 255, ($clinit_131() , c_1).userOrg_form_position());
  this$static.nowCheckBox = new ConsysCheckBox_0;
  this$static.nowCheckBox.addStyleName('floatL');
  this$static.fromMonthYearBox = new MonthYearDatePicker_0;
  $setRequired(this$static.fromMonthYearBox, true);
  this$static.fromMonthYearBox.addStyleName('floatL');
  this$static.toMonthYearBox = new MonthYearDatePicker_0;
  $setRequired(this$static.toMonthYearBox, true);
  this$static.toMonthYearBox.addStyleName('floatL');
  this$static.preferredBox = new ConsysCheckBox_0;
  $setEnabled_5(this$static.preferredBox, false);
  this$static.toLabel = getStyledLabel(($clinit_515() , c_4).const_toLower(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marT3', 'marH5', 'floatL']));
  this$static.presentLabel = getStyledLabel(($clinit_131() , c_1).userOrg_text_presentLower(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marT3', 'floatL']));
  nowPanel = new FlowPanel_0;
  nowPanel.add_1(this$static.nowCheckBox);
  nowPanel.add_1(getStyledLabel(($clinit_131() , c_1).userOrg_form_currentlyWork(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL5', 'floatL'])));
  nowPanel.add_1(clearDiv_0());
  this$static.timePanel = new ConsysFlowPanel_0;
  $add_12(this$static.timePanel, this$static.fromMonthYearBox);
  $add_12(this$static.timePanel, this$static.toLabel);
  $add_12(this$static.timePanel, this$static.toMonthYearBox);
  $add_12(this$static.timePanel, clearDiv_0());
  $addNowCheckBoxClickHandler(this$static);
  this$static.detailForm = new OrganizationDetailForm_0;
  this$static.detailForm.setVisible(false);
  $setSuggestInput(this$static.detailForm, this$static.atBox);
  this$static.infoPanel = new VerticalPanel_0;
  this$static.infoPanel.setVisible(false);
  this$static.setStyleName('marL20');
  $addRequired(this$static, 0, ($clinit_131() , c_1).userOrg_form_position(), this$static.positionBox);
  this$static.editOrganization = getEditModeButton(new UserOrgForm$2_0(this$static));
  this$static.editOrganization.setVisible(false);
  $addRequired(this$static, 1, ($clinit_131() , c_1).userOrg_form_at(), this$static.atBox);
  $addActionWidget(this$static, 1, this$static.editOrganization);
  $addContent(this$static, 2, this$static.infoPanel);
  $addContent(this$static, 3, this$static.detailForm);
  $addRequired(this$static, 4, ($clinit_131() , c_1).userOrg_form_timePeriod(), nowPanel);
  $addContent(this$static, 5, this$static.timePanel);
  $addOptional(this$static, 6, ($clinit_131() , c_1).userOrg_form_preferredPosition(), this$static.preferredBox);
  if (jsNotEquals(uuid, null)) {
    this$static.buttonEdit = getCheckButton(($clinit_515() , c_4).const_saveUpper());
    $addClickHandler_4(this$static.buttonEdit, this$static.confirmUpdateClickHandler_0());
    this$static.cancelEdit = new ActionLabel_0(($clinit_515() , c_4).const_cancel());
    $setClickHandler(this$static.cancelEdit, this$static.viewClickHandler());
    $addActionMembers(this$static, 7, this$static.buttonEdit, this$static.cancelEdit);
  }
  if (jsEquals(uuid, null)) {
    this$static.userOrg = new ClientUserOrg_0;
  }
   else {
    dataEvent = new DispatchEvent_0(new LoadUserOrgAction_0(uuid), new UserOrgForm$3_0(this$static), this$static.getFormPanel());
    $fireEvent_3(get_10(), dataEvent);
  }
}

function $setClientUserOrg(this$static, data){
  this$static.userOrg = data;
  $setText_3(this$static.positionBox, $getPosition_0(data));
  $setDate_0(this$static.fromMonthYearBox, $getFromDate(data));
  $setDate_0(this$static.toMonthYearBox, $getToDate(data));
  $setValue_7(this$static.preferredBox, valueOf_48($isPreferredPosition(data)));
  $setText_13(this$static.atBox, $getFullName_1($getOrganization(data)));
  this$static.editOrganization.setVisible(isNotNull($getOrganization(data)) && $isOwner($getOrganization(data)));
  $setOrganization_0(this$static.detailForm, $getOrganization(data));
  $setValue_7(this$static.nowCheckBox, valueOf_48($isActualPosition(data)));
  $setOrganizationInfo(this$static, $getOrganization(data));
  $showToPresent(this$static, $isActualPosition(data));
}

function $setOrganizationInfo(this$static, result){
  var address, sb;
  sb = new StringBuffer_0;
  $append_4(sb, $getAcronym_0(result));
  if (jsNotEquals($getUniversalName_0(result), null) && $length_2($getUniversalName_0(result)) > 0) {
    $length_3(sb) > 0 && $append_4(sb, ', ');
    $append_4(sb, $getUniversalName_0(result));
  }
  this$static.infoPanel.setVisible(true);
  this$static.infoPanel.clear();
  $length_3(sb) > 0 && $add_10(this$static.infoPanel, getStyledLabel($toString_12(sb), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f10px'])));
  address = isNull($getAddress_1(result))?null:$toString_6($getAddress_1(result));
  jsNotEquals(address, null) && $length_2(address) > 0 && $add_10(this$static.infoPanel, getStyledLabel(address, initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f10px'])));
  jsNotEquals($getWeb_1(result), null) && $length_2($getWeb_1(result)) > 0 && $add_10(this$static.infoPanel, getStyledLabel($getWeb_1(result), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f10px'])));
}

function $showOrganizationSettings(this$static){
  this$static.infoPanel.setVisible(false);
  this$static.infoPanel.clear();
  $getTextBox_0(this$static.atBox).setEnabled(false);
  this$static.detailForm.setVisible(true);
  this$static.editOrganization.setVisible(false);
  $setNameFocus(this$static.detailForm);
}

function $showToPresent(this$static, present){
  $setEnabled_5(this$static.preferredBox, present);
  if (present && isNull($getParent(this$static.presentLabel))) {
    $clear_5(this$static.timePanel);
    $add_12(this$static.timePanel, this$static.fromMonthYearBox);
    $add_12(this$static.timePanel, this$static.toLabel);
    $add_12(this$static.timePanel, this$static.presentLabel);
    $add_12(this$static.timePanel, clearDiv_0());
  }
   else if (!present && isNull($getParent(this$static.toMonthYearBox))) {
    $clear_5(this$static.timePanel);
    $add_12(this$static.timePanel, this$static.fromMonthYearBox);
    $add_12(this$static.timePanel, this$static.toLabel);
    $add_12(this$static.timePanel, this$static.toMonthYearBox);
    $add_12(this$static.timePanel, clearDiv_0());
  }
}

function UserOrgForm_0(){
  UserOrgForm_1.call(this, null);
}

function UserOrgForm_1(uuid){
  BaseForm_0.call(this);
  $$init_684();
  $init_5(this, uuid);
}

function UserOrgForm(){
}

_ = UserOrgForm.prototype = new BaseForm;
_.confirmUpdateClickHandler_0 = function confirmUpdateClickHandler_5(){
  throw new UnsupportedOperationException_1('Override to edit mode');
}
;
_.getClass$ = function getClass_685(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm_2_classLit;
}
;
_.onSelection = function onSelection(event_0){
  var consysSuggestion, loadEvent, organization, suggestion;
  suggestion = dynamicCast($getSelectedItem(event_0), 404);
  if (isNotNull(suggestion)) {
    consysSuggestion = suggestion;
    if ($equals_8(consysSuggestion.getSuggestionId(), 'CREATE_ORGANIZATION_SUGGESTION')) {
      organization = new ClientOrganization_0;
      $setOwner_0(organization, true);
      $setFullName_0(organization, this.atSave);
      $setOrganization_0(this.detailForm, organization);
      $showOrganizationSettings(this);
    }
     else {
      loadEvent = new DispatchEvent_0(new LoadOrganizationAction_0($getUuid_10($getThumb(consysSuggestion))), new UserOrgForm$5_0(this), null);
      $fireEvent_3(get_10(), loadEvent);
    }
  }
}
;
_.viewClickHandler = function viewClickHandler(){
  throw new UnsupportedOperationException_1('Override to edit mode');
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 97:1, 101:1, 147:1};
_.atBox = null;
_.atSave = null;
_.buttonEdit = null;
_.cancelEdit = null;
_.detailForm = null;
_.editOrganization = null;
_.fromMonthYearBox = null;
_.infoPanel = null;
_.nowCheckBox = null;
_.positionBox = null;
_.preferredBox = null;
_.presentLabel = null;
_.timePanel = null;
_.toLabel = null;
_.toMonthYearBox = null;
_.userOrg = null;
function $$init_685(){
}

function NewUserOrganizationDialog$1_0(this$0){
  $clinit_6();
  this.this$0 = this$0;
  UserOrgForm_0.call(this);
  $$init_685();
}

function NewUserOrganizationDialog$1(){
}

_ = NewUserOrganizationDialog$1_0.prototype = NewUserOrganizationDialog$1.prototype = new UserOrgForm;
_.getClass$ = function getClass_686(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_NewUserOrganizationDialog$1_2_classLit;
}
;
_.getFormPanel = function getFormPanel(){
  return $getFormPanel(this.this$0);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 97:1, 101:1, 147:1};
_.this$0 = null;
function $$init_686(){
}

function $onSuccess_43(this$static, result){
  $setUuid(this$static.val$uo, $getStringResult(result));
  this$static.this$0.hide_0();
  this$static.this$0.delegate.onSuccessCreate(this$static.val$uo);
}

function NewUserOrganizationDialog$2_0(this$0, val$uo){
  this.this$0 = this$0;
  this.val$uo = val$uo;
  Object_1.call(this);
  $$init_686();
}

function NewUserOrganizationDialog$2(){
}

_ = NewUserOrganizationDialog$2_0.prototype = NewUserOrganizationDialog$2.prototype = new Object_0;
_.getClass$ = function getClass_687(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_NewUserOrganizationDialog$2_2_classLit;
}
;
_.onFailure = function onFailure_38(caught){
}
;
_.onSuccess = function onSuccess_46(result){
  $onSuccess_43(this, dynamicCast(result, 363));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
_.val$uo = null;
function $$init_687(){
}

function $getOrganization_0(this$static){
  var industry;
  industry = $getSelectedItem_1(this$static.industryBox);
  $setWeb_0(this$static.organization_0, $trim(this$static.websiteBox.getText()));
  $setUniversalName_0(this$static.organization_0, $trim(this$static.universalBox.getText()));
  $setOrganizationType(this$static.organization_0, isNull(industry)?0:$intValue_0(dynamicCast($getItem_0(industry), 165)));
  $setAddress_1(this$static.organization_0, $getAddress_0(this$static.addressForm));
  $setAcronym_0(this$static.organization_0, $trim(this$static.acronymBox.getText()));
  $setFullName_0(this$static.organization_0, $trim($getText_4(this$static.fullName)));
  return this$static.organization_0;
}

function $setNameFocus(this$static){
  this$static.fullName.setFocus(true);
  isNotNull(this$static.suggestBox) && $setText_13(this$static.suggestBox, $getText_4(this$static.fullName));
}

function $setOrganization_0(this$static, organization){
  this$static.organization_0 = organization;
  this$static.websiteBox.setText($getWeb_1(organization));
  this$static.universalBox.setText($getUniversalName_0(organization));
  $setText_3(this$static.fullName, $getFullName_1(organization));
  $getOrganizationType(organization) > 0?$selectItem_0(this$static.industryBox, valueOf_53($getOrganizationType(organization))):$selectFirst(this$static.industryBox, false);
  $setAddress_0(this$static.addressForm, $getAddress_1(organization));
  this$static.acronymBox.setText($getAcronym_0(organization));
}

function $setSuggestInput(this$static, suggestBox){
  this$static.suggestBox = suggestBox;
}

function OrganizationDetailForm_0(){
  var showMoreNameLabel;
  SimplePanel_0.call(this);
  $$init_687();
  this.fullName = new ConsysStringTextBox_1(1, 255, ($clinit_515() , c_4).const_name());
  $addKeyUpHandler(this.fullName, new OrganizationDetailForm$1_0(this));
  this.industryBox = new SelectBox_0;
  $setRequired_0(this.industryBox, false);
  $selectFirst(this.industryBox, true);
  this.universalBox = getFormInput();
  this.acronymBox = getFormInput();
  this.addressForm = new AddressForm_0;
  this.websiteBox = getFormInput();
  this.formWeb = new BaseForm_0;
  $addRequired(this.formWeb, 0, ($clinit_515() , c_4).const_name(), this.fullName);
  showMoreNameLabel = new ActionLabel_1(($clinit_131() , c_1).organizationDetailForm_form_moreNameOptions(), Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f10px', 'fontBold', 'textConsysBlue'])));
  $setClickHandler(showMoreNameLabel, new OrganizationDetailForm$2_0(this));
  $addContent(this.formWeb, 1, showMoreNameLabel);
  $addOptional(this.formWeb, 2, ($clinit_131() , c_1).organizationDetailForm_form_industry(), this.industryBox);
  $addOptional(this.addressForm, 6, ($clinit_515() , c_4).const_website(), this.websiteBox);
  this.panel = new VerticalPanel_0;
  $add_10(this.panel, this.formWeb);
  $add_10(this.panel, this.addressForm);
  this.setWidget(new ConsysBaseWrapper_0(this.panel));
  $fireEvent_3(get_10(), new DispatchEvent_0(new ListOrganizationTypesAction_0, new OrganizationDetailForm$3_0(this), null));
}

function OrganizationDetailForm(){
}

_ = OrganizationDetailForm_0.prototype = OrganizationDetailForm.prototype = new SimplePanel;
_.doValidate = function doValidate_12(fail){
  return !this.isVisible() || $validate(this.formWeb, fail);
}
;
_.getClass$ = function getClass_688(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm_2_classLit;
}
;
_.onFail = function onFail_9(){
}
;
_.onSuccess_0 = function onSuccess_47(){
}
;
_.castableTypeMap$ = {2:1, 19:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1, 457:1};
_.acronymBox = null;
_.addressForm = null;
_.formWeb = null;
_.fullName = null;
_.industryBox = null;
_.organization_0 = null;
_.panel = null;
_.suggestBox = null;
_.universalBox = null;
_.websiteBox = null;
function $$init_688(){
}

function OrganizationDetailForm$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_688();
}

function OrganizationDetailForm$1(){
}

_ = OrganizationDetailForm$1_0.prototype = OrganizationDetailForm$1.prototype = new Object_0;
_.getClass$ = function getClass_689(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm$1_2_classLit;
}
;
_.onKeyUp = function onKeyUp_8(event_0){
  isNotNull(this.this$0.suggestBox) && $setText_13(this.this$0.suggestBox, $getText_4(this.this$0.fullName));
}
;
_.castableTypeMap$ = {77:1, 101:1};
_.this$0 = null;
function $$init_689(){
}

function OrganizationDetailForm$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_689();
}

function OrganizationDetailForm$2(){
}

_ = OrganizationDetailForm$2_0.prototype = OrganizationDetailForm$2.prototype = new Object_0;
_.getClass$ = function getClass_690(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm$2_2_classLit;
}
;
_.onClick = function onClick_36(event_0){
  this.this$0.formWeb.insertRow(2);
  $addOptional(this.this$0.formWeb, 1, ($clinit_131() , c_1).organizationDetailForm_form_internationalName(), this.this$0.universalBox);
  $addOptional(this.this$0.formWeb, 2, ($clinit_515() , c_4).const_acronym(), this.this$0.acronymBox);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_690(){
}

function $onSuccess_44(this$static, result){
  var i, i$iterator, out;
  out = new ArrayList_0;
  for (i$iterator = $getList_0(result).iterator_0(); i$iterator.hasNext_0();) {
    i = dynamicCast(i$iterator.next_0(), 360);
    $add(out, new SelectBoxItem_2($getId_1(i), $getName_5(i)));
  }
  $setItems_0(this$static.this$0.industryBox, out);
}

function OrganizationDetailForm$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_690();
}

function OrganizationDetailForm$3(){
}

_ = OrganizationDetailForm$3_0.prototype = OrganizationDetailForm$3.prototype = new Object_0;
_.getClass$ = function getClass_691(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm$3_2_classLit;
}
;
_.onFailure = function onFailure_39(caught){
}
;
_.onSuccess = function onSuccess_48(result){
  $onSuccess_44(this, dynamicCast(result, 361));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_691(this$static){
}

function $cancel_3(this$static){
  isNotNull(this$static.timer) && $cancel(this$static.timer);
}

function $setIsDisplayAsHTML(this$static, isDisplayAsHTML){
  this$static.isDisplayAsHTML = isDisplayAsHTML;
}

function ConsysSuggestOracle_0(minLength){
  ConsysSuggestOracle_1.call(this, 500, minLength);
}

function ConsysSuggestOracle_1(timeout, minLength){
  SuggestOracle_0.call(this);
  $$init_691(this);
  this.timeout = timeout;
  this.minLength = minLength;
}

function ConsysSuggestOracle(){
}

_ = ConsysSuggestOracle.prototype = new SuggestOracle;
_.getClass$ = function getClass_692(){
  return Lconsys_common_gwt_client_ui_comp_ConsysSuggestOracle_2_classLit;
}
;
_.isDisplayStringHTML = function isDisplayStringHTML_0(){
  return this.isDisplayAsHTML;
}
;
_.requestSuggestions = function requestSuggestions(request, callback){
  var query, response;
  query = $getQuery(request);
  $cancel_3(this);
  if ($length_2(query) > this.minLength) {
    this.timer = new ConsysSuggestOracle$1_0(this, query, callback, request);
    $schedule(this.timer, this.timeout);
  }
   else {
    response = new SuggestOracle$Response_0(emptyList_0());
    callback.onSuggestionsReady(request, response);
  }
}
;
_.castableTypeMap$ = {};
_.isDisplayAsHTML = false;
_.minLength = 0;
_.timeout = 0;
_.timer = null;
function $clinit_127(){
  $clinit_127 = nullMethod;
  logger_2 = getLogger_1(Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle_2_classLit);
}

function $$init_692(){
}

function OrganizationSuggestOracle_0(){
  $clinit_127();
  ConsysSuggestOracle_0.call(this, 1);
  $$init_692();
  $setIsDisplayAsHTML(this, true);
}

function OrganizationSuggestOracle(){
}

_ = OrganizationSuggestOracle_0.prototype = OrganizationSuggestOracle.prototype = new ConsysSuggestOracle;
_.getClass$ = function getClass_693(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle_2_classLit;
}
;
_.suggest = function suggest(query, limit, callback){
  $fireEvent_3(get_10(), new DispatchEvent_0(new ListOrganizationsByPrefixAction_0(query, limit), new OrganizationSuggestOracle$1_0(this, callback), null));
}
;
_.castableTypeMap$ = {};
var logger_2;
function $$init_693(){
}

function $onSuccess_45(this$static, result){
  var item, item$iterator, suggestions;
  $debug_0(($clinit_127() , logger_2), 'Successfully suggested ' + $size($getSelectBoxResult(result)) + ' organization');
  suggestions = new ArrayList_0;
  suggestions.add(new OrganizationSuggestOracle$CreateOrganizationSuggestion_0);
  for (item$iterator = $getSelectBoxResult(result).iterator_0(); item$iterator.hasNext_0();) {
    item = dynamicCast(item$iterator.next_0(), 362);
    suggestions.add(new OrganizationSuggestOracle$OrganizationSuggestion_0(dynamicCast($getItem_0(item), 402)));
  }
  $reponse(this$static.val$callback, suggestions);
}

function OrganizationSuggestOracle$1_0(this$0, val$callback){
  this , this$0;
  this.val$callback = val$callback;
  Object_1.call(this);
  $$init_693();
}

function OrganizationSuggestOracle$1(){
}

_ = OrganizationSuggestOracle$1_0.prototype = OrganizationSuggestOracle$1.prototype = new Object_0;
_.getClass$ = function getClass_694(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle$1_2_classLit;
}
;
_.onFailure = function onFailure_40(caught){
  $debug_1(($clinit_127() , logger_2), 'Failed download Organizations', caught);
}
;
_.onSuccess = function onSuccess_49(result){
  $onSuccess_45(this, dynamicCast(result, 403));
}
;
_.castableTypeMap$ = {};
_.val$callback = null;
function $$init_694(){
}

function $getThumb(this$static){
  return this$static.thumb;
}

function OrganizationSuggestOracle$OrganizationSuggestion_0(thumb){
  Object_1.call(this);
  $$init_694();
  this.thumb = thumb;
}

function OrganizationSuggestOracle$OrganizationSuggestion(){
}

_ = OrganizationSuggestOracle$OrganizationSuggestion_0.prototype = OrganizationSuggestOracle$OrganizationSuggestion.prototype = new Object_0;
_.getClass$ = function getClass_695(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle$OrganizationSuggestion_2_classLit;
}
;
_.getDisplayString = function getDisplayString(){
  var wrapper;
  wrapper = createDiv();
  $appendChild(wrapper, $getElement(getStyledLabel($getUniversalName_1($getThumb(this)), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f12px', 'fontBold', 'marB1']))));
  $appendChild(wrapper, $getElement(getStyledLabel($getAddress_2($getThumb(this)), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f11px']))));
  $setAttribute(wrapper, 'class', Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['padV5', 'marH5'])));
  $setProperty($getStyle(wrapper), 'textAlign', 'left');
  return $getString(wrapper);
}
;
_.getReplacementString = function getReplacementString(){
  return $getUniversalName_1($getThumb(this));
}
;
_.getSuggestionId = function getSuggestionId(){
  return $getUuid_10($getThumb(this));
}
;
_.castableTypeMap$ = {314:1, 404:1};
_.thumb = null;
function $$init_695(){
}

function OrganizationSuggestOracle$CreateOrganizationSuggestion_0(){
  OrganizationSuggestOracle$OrganizationSuggestion_0.call(this, null);
  $$init_695();
}

function OrganizationSuggestOracle$CreateOrganizationSuggestion(){
}

_ = OrganizationSuggestOracle$CreateOrganizationSuggestion_0.prototype = OrganizationSuggestOracle$CreateOrganizationSuggestion.prototype = new OrganizationSuggestOracle$OrganizationSuggestion;
_.getClass$ = function getClass_696(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle$CreateOrganizationSuggestion_2_classLit;
}
;
_.getDisplayString = function getDisplayString_0(){
  var l_0, wrapper;
  wrapper = createDiv();
  l_0 = getStyledLabel(($clinit_131() , c_1).organizationSuggestOracle_text_createNewOrganization(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['fontBold', 'textConsysBlue', 'f12px']))]));
  setStyleAttribute($getElement(l_0), 'fontStyle', 'italic');
  setStyleAttribute($getElement(l_0), 'textAlign', 'center');
  $appendChild(wrapper, $getElement(l_0));
  $setAttribute(wrapper, 'class', Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['padV5'])));
  return $getString(wrapper);
}
;
_.getReplacementString = function getReplacementString_0(){
  return '';
}
;
_.getSuggestionId = function getSuggestionId_0(){
  return 'CREATE_ORGANIZATION_SUGGESTION';
}
;
_.castableTypeMap$ = {314:1, 404:1};
function $$init_696(){
}

function UserOrgForm$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_696();
}

function UserOrgForm$1(){
}

_ = UserOrgForm$1_0.prototype = UserOrgForm$1.prototype = new Object_0;
_.getClass$ = function getClass_697(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$1_2_classLit;
}
;
_.onKeyUp = function onKeyUp_9(event_0){
  this.this$0.atSave = $getText_9(this.this$0.atBox);
}
;
_.castableTypeMap$ = {77:1, 101:1};
_.this$0 = null;
function $$init_697(){
}

function UserOrgForm$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_697();
}

function UserOrgForm$2(){
}

_ = UserOrgForm$2_0.prototype = UserOrgForm$2.prototype = new Object_0;
_.getClass$ = function getClass_698(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$2_2_classLit;
}
;
_.onClick = function onClick_37(event_0){
  $showOrganizationSettings(this.this$0);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_698(){
}

function $onSuccess_46(this$static, result){
  $setClientUserOrg(this$static.this$0, result);
}

function UserOrgForm$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_698();
}

function UserOrgForm$3(){
}

_ = UserOrgForm$3_0.prototype = UserOrgForm$3.prototype = new Object_0;
_.getClass$ = function getClass_699(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$3_2_classLit;
}
;
_.onFailure = function onFailure_41(caught){
  instanceOf(caught, 393) && $setText_8($getFailMessage(this.this$0.getFormPanel()), ($clinit_131() , c_1).userOrg_error_invalidIdOfExperience());
}
;
_.onSuccess = function onSuccess_50(result){
  $onSuccess_46(this, dynamicCast(result, 371));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_699(){
}

function UserOrgForm$4_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_699();
}

function UserOrgForm$4(){
}

_ = UserOrgForm$4_0.prototype = UserOrgForm$4.prototype = new Object_0;
_.getClass$ = function getClass_700(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$4_2_classLit;
}
;
_.onChangeValue = function onChangeValue_3(event_0){
  $booleanValue(dynamicCast($getValue_12(event_0), 159))?$showToPresent(this.this$0, true):$showToPresent(this.this$0, false);
}
;
_.castableTypeMap$ = {101:1, 458:1};
_.this$0 = null;
function $$init_700(){
}

function $onSuccess_47(this$static, result){
  this$static.this$0.detailForm.setVisible(false);
  $setOrganizationInfo(this$static.this$0, result);
  $setFocus_3(this$static.this$0.nowCheckBox, true);
  $setOrganization_0(this$static.this$0.detailForm, result);
  this$static.this$0.editOrganization.setVisible(isNotNull(result) && $isOwner(result));
}

function UserOrgForm$5_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_700();
}

function UserOrgForm$5(){
}

_ = UserOrgForm$5_0.prototype = UserOrgForm$5.prototype = new Object_0;
_.getClass$ = function getClass_701(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$5_2_classLit;
}
;
_.onFailure = function onFailure_42(caught){
}
;
_.onSuccess = function onSuccess_51(result){
  $onSuccess_47(this, dynamicCast(result, 397));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $clinit_128(){
  $clinit_128 = nullMethod;
  dateFormat = getFormat_0('y');
}

function $$init_701(){
}

function $getControl(this$static){
  return this$static.control;
}

function $getInfo(this$static){
  return $isActualPosition_0(this$static.uo)?$getPosition_1(this$static.uo) + ' ' + ($clinit_131() , c_1).userOrg_text_at() + ' ' + $getOrgName(this$static.uo) + ' (' + $format(dateFormat, $getFrom(this$static.uo)) + '\xA0' + '\u2014' + '\xA0' + ')':$getPosition_1(this$static.uo) + ' ' + ($clinit_131() , c_1).userOrg_text_at() + ' ' + $getOrgName(this$static.uo) + ' (' + $format(dateFormat, $getFrom(this$static.uo)) + '\xA0' + '\u2013' + '\xA0' + $format(dateFormat, $getTo(this$static.uo)) + ')';
}

function $getInstance(this$static){
  return this$static.instance;
}

function $getLabel(this$static){
  return this$static.label;
}

function $getUserOrg(this$static){
  return this$static.uo;
}

function $setDeleteAction(this$static, removeAction){
  var remove, remover;
  remove = new Image_2(system_0().removeCross());
  remove.setStyleName('cursorHand');
  remover = new UserOrgItem$1_0(this$static, remove, removeAction);
  this$static.removerPanel.setWidget(remover);
}

function $setInstance(this$static, instance){
  this$static.instance = instance;
}

function UserOrgItem_0(uo, editClickHandler){
  $clinit_128();
  var padd;
  Object_1.call(this);
  $$init_701();
  this.uo = uo;
  this.label = getStyledLabel($getInfo(this), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marR20', 'marV3']))]));
  this.edit = new ActionLabel_1(($clinit_515() , c_4).const_edit(), Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marR10', 'f10px', 'floatL'])));
  $setClickHandler(this.edit, editClickHandler);
  padd = new SimplePanel_0;
  padd.setSize('6px', '6px');
  padd.addStyleName('overHide');
  padd.addStyleName('floatL');
  this.removerPanel = new SimplePanel_0;
  this.removerPanel.addStyleName('floatL');
  this.control = new FlowPanel_0;
  this.control.addStyleName('floatR');
  this.control.add_1(padd);
  this.control.add_1(this.edit);
  this.control.add_1(this.removerPanel);
  this.control.add_1(clearDiv_0());
}

function UserOrgItem(){
}

_ = UserOrgItem_0.prototype = UserOrgItem.prototype = new Object_0;
_.getClass$ = function getClass_702(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgItem_2_classLit;
}
;
_.castableTypeMap$ = {};
_.control = null;
_.edit = null;
_.instance = null;
_.label = null;
_.removerPanel = null;
_.uo = null;
var dateFormat;
function $$init_703(){
}

function UserOrgItem$1_0(this$0, $anonymous0, $anonymous1){
  $clinit_6();
  this.this$0 = this$0;
  Remover_0.call(this, $anonymous0, $anonymous1);
  $$init_703();
}

function UserOrgItem$1(){
}

_ = UserOrgItem$1_0.prototype = UserOrgItem$1.prototype = new Remover;
_.getClass$ = function getClass_704(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgItem$1_2_classLit;
}
;
_.onDefaultSwap = function onDefaultSwap_0(){
  this.this$0.edit.setVisible(true);
}
;
_.onQuestionSwap = function onQuestionSwap_0(){
  this.this$0.edit.setVisible(false);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.this$0 = null;
function $$init_704(){
}

function $addItem_1(this$static, item){
  $isActualPosition_0($getUserOrg(item))?$addRecord(this$static.currentFT, item):$addRecord(this$static.pastFT, item);
}

function $addRecord(panel, item){
  var instance;
  instance = new FlowPanel_0;
  instance.add_1($getControl(item));
  instance.add_1($getLabel(item));
  instance.add_1(clearDiv_0());
  $setInstance(item, instance);
  panel.add_1(instance);
}

function $noRecordLabel(){
  return new Label_1(($clinit_515() , c_4).const_noRecordFound());
}

function UserOrgPanel_0(){
  $clinit_6();
  SimpleFormPanel_0.call(this);
  $$init_704();
  $setDynamicWidth(this, true);
  this.currentFT = new UserOrgPanel$OrgWrapper_0(this);
  this.pastFT = new UserOrgPanel$OrgWrapper_0(this);
  this.addWidget(getStyledLabel(($clinit_131() , c_1).userOrg_subtitle_currentlyWork(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['fontBold'])));
  this.addWidget(this.currentFT);
  this.addWidget(getStyledLabel(($clinit_131() , c_1).userOrg_subtitle_pastPositions(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['fontBold'])));
  this.addWidget(this.pastFT);
}

function UserOrgPanel(){
}

_ = UserOrgPanel_0.prototype = UserOrgPanel.prototype = new SimpleFormPanel;
_.getClass$ = function getClass_705(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgPanel_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.currentFT = null;
_.pastFT = null;
function $$init_705(){
}

function $add_13(this$static, w){
  $add_6(this$static, w);
  $getWidgetCount(this$static) != 0 && instanceOf($getWidget(this$static, 0), 333) && $remove_3(this$static, 0);
}

function UserOrgPanel$OrgWrapper_0(this$0){
  this , this$0;
  FlowPanel_0.call(this);
  $$init_705();
  this.addStyleName('marL20');
  this.addStyleName('marV10');
  $add_13(this, $noRecordLabel());
}

function UserOrgPanel$OrgWrapper(){
}

_ = UserOrgPanel$OrgWrapper_0.prototype = UserOrgPanel$OrgWrapper.prototype = new FlowPanel;
_.add_1 = function add_15(w){
  $add_13(this, w);
}
;
_.getClass$ = function getClass_706(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgPanel$OrgWrapper_2_classLit;
}
;
_.remove_1 = function remove_15(w){
  var result;
  result = $remove_4(this, w);
  $getWidgetCount(this) == 0 && $add_6(this, $noRecordLabel());
  return result;
}
;
_.castableTypeMap$ = {2:1, 18:1, 45:1, 46:1, 47:1, 49:1, 147:1, 337:1, 463:1, 490:1};
function $$init_706(){
}

function $createClickHandler(this$static){
  return new CRUDSimpleHelpFormPanel$1_0(this$static);
}

function CRUDSimpleHelpFormPanel_0(withHiddableLabel){
  $clinit_6();
  RUSimpleHelpFormPanel_1.call(this, withHiddableLabel);
  $$init_706();
}

function CRUDSimpleHelpFormPanel(){
}

_ = CRUDSimpleHelpFormPanel.prototype = new RUSimpleHelpFormPanel;
_.getClass$ = function getClass_707(){
  return Lconsys_common_gwt_client_ui_comp_panel_abst_CRUDSimpleHelpFormPanel_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
function $clinit_129(){
  $clinit_129 = nullMethod;
  $clinit_6();
  logger_3 = getLogger_1(Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent_2_classLit);
}

function $$init_707(){
}

function $confirmUpdateClickHandler_1(this$static){
  return new UserOrganizationsContent$3_0(this$static);
}

function $getDeleteAction(this$static, uuid, item){
  return new UserOrganizationsContent$5_0(this$static, uuid, item);
}

function $readForm_0(this$static){
  var add, listEvent, panel, title, vp;
  title = getStyledLabel(($clinit_131() , c_1).userOrganizationsContent_title(), initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['f16px', 'floatL', 'fontBold']));
  add = new ActionLabel_1(($clinit_515() , c_4).const_add(), Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL20', 'f10px', 'marT3', 'floatL'])));
  $setClickHandler(add, $createClickHandler(this$static));
  panel = new FlowPanel_0;
  panel.setStyleName('marB10');
  panel.add_1(title);
  panel.add_1(add);
  panel.add_1(clearDiv_0());
  $showHelp(this$static);
  this$static.viewPanel = new UserOrgPanel_0;
  listEvent = new DispatchEvent_0(new ListUserOrgsAction_0, new UserOrganizationsContent$1_0(this$static), $getSelf(this$static));
  $fireEvent_3(get_10(), listEvent);
  vp = new FlowPanel_0;
  vp.setWidth('95%');
  vp.add_1(panel);
  vp.add_1(this$static.viewPanel);
  return vp;
}

function UserOrganizationsContent_0(){
  $clinit_129();
  CRUDSimpleHelpFormPanel_0.call(this, true);
  $$init_707();
  $addHelpTitle(this, 'For what are organization used?');
  $addHelpText(this, 'When you will go to some event, the organizers want to know your afiliation. Takeplace provide automatic afiliton system which will save your time.');
}

function UserOrganizationsContent(){
}

_ = UserOrganizationsContent_0.prototype = UserOrganizationsContent.prototype = new CRUDSimpleHelpFormPanel;
_.confirmUpdateClickHandler = function confirmUpdateClickHandler_6(id){
  return $confirmUpdateClickHandler_1(this);
}
;
_.createForm = function createForm_0(){
  this.newOrganization = new NewUserOrganizationDialog_0(this);
  $showCentered(this.newOrganization);
  return null;
}
;
_.getClass$ = function getClass_708(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent_2_classLit;
}
;
_.onSuccessCreate = function onSuccessCreate(result){
  var item, thumb;
  thumb = new ClientUserOrgProfileThumb_0;
  $setActualPosition_0(thumb, $isActualPosition(result));
  $setFrom(thumb, $getFromDate(result));
  $setOrgName(thumb, $getFullName_1($getOrganization(result)));
  $setPosition_2(thumb, $getPosition_0(result));
  $setTo(thumb, $getToDate(result));
  $setUuid_0(thumb, $getUuid_1(result));
  item = new UserOrgItem_0(thumb, $toEditModeClickHandler(this, $getUuid_1(result)));
  $setDeleteAction(item, $getDeleteAction(this, $getUuid_1(result), item));
  $addItem_1(this.viewPanel, item);
}
;
_.readForm = function readForm_5(){
  return $readForm_0(this);
}
;
_.updateForm = function updateForm_5(id){
  $hideHelp(this);
  this.form = new UserOrganizationsContent$2_0(this, id, id);
  return this.form;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.form = null;
_.newOrganization = null;
_.viewPanel = null;
var logger_3;
function $$init_708(){
}

function $onSuccess_48(this$static, result){
  var item, uo, uo$iterator, uuid;
  $debug_0(($clinit_129() , logger_3), 'Downloaded ' + $size($getArrayListResult(result)) + ' experiences');
  for (uo$iterator = $getArrayListResult(result).iterator_0(); uo$iterator.hasNext_0();) {
    uo = dynamicCast(uo$iterator.next_0(), 395);
    uuid = $getUuid_2(uo);
    item = new UserOrgItem_0(uo, $toEditModeClickHandler(this$static.this$0, uuid));
    $setDeleteAction(item, $getDeleteAction(this$static.this$0, uuid, item));
    $addItem_1(this$static.this$0.viewPanel, item);
  }
}

function UserOrganizationsContent$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_708();
}

function UserOrganizationsContent$1(){
}

_ = UserOrganizationsContent$1_0.prototype = UserOrganizationsContent$1.prototype = new Object_0;
_.getClass$ = function getClass_709(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$1_2_classLit;
}
;
_.onFailure = function onFailure_43(caught){
  $debug_1(($clinit_129() , logger_3), 'Error', caught);
  $setText_8($getFailMessage(this.this$0), ($clinit_131() , c_1).userOrganizationsContent_error_experienceListFailed());
}
;
_.onSuccess = function onSuccess_52(result){
  $onSuccess_48(this, dynamicCast(result, 405));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_709(){
}

function UserOrganizationsContent$2_0(this$0, $anonymous0, val$id){
  $clinit_6();
  this.this$0 = this$0;
  this , val$id;
  UserOrgForm_1.call(this, $anonymous0);
  $$init_709();
}

function UserOrganizationsContent$2(){
}

_ = UserOrganizationsContent$2_0.prototype = UserOrganizationsContent$2.prototype = new UserOrgForm;
_.confirmUpdateClickHandler_0 = function confirmUpdateClickHandler_7(){
  return $confirmUpdateClickHandler_1(this.this$0);
}
;
_.getClass$ = function getClass_710(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$2_2_classLit;
}
;
_.getFormPanel = function getFormPanel_0(){
  return this.this$0;
}
;
_.viewClickHandler = function viewClickHandler_0(){
  return $toReadModeClickHandler(this.this$0);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 97:1, 101:1, 147:1};
_.this$0 = null;
function $$init_710(){
}

function UserOrganizationsContent$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_710();
}

function UserOrganizationsContent$3(){
}

_ = UserOrganizationsContent$3_0.prototype = UserOrganizationsContent$3.prototype = new Object_0;
_.getClass$ = function getClass_711(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$3_2_classLit;
}
;
_.onClick = function onClick_38(event_0){
  var editEvent, uo;
  if ($validate(this.this$0.form, $getFailMessage(this.this$0))) {
    uo = $getClientUserOrg(this.this$0.form);
    editEvent = new DispatchEvent_0(new UpdateUserOrgAction_0(uo), new UserOrganizationsContent$3$1_0(this), this.this$0);
    $fireEvent_3(get_10(), editEvent);
  }
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
function $$init_711(){
}

function $onSuccess_49(this$static){
  $onSuccessUpdate(this$static.this$1.this$0, ($clinit_131() , c_1).userOrganizationsContent_text_experienceSucessfullyUpdated());
}

function UserOrganizationsContent$3$1_0(this$1){
  this.this$1 = this$1;
  Object_1.call(this);
  $$init_711();
}

function UserOrganizationsContent$3$1(){
}

_ = UserOrganizationsContent$3$1_0.prototype = UserOrganizationsContent$3$1.prototype = new Object_0;
_.getClass$ = function getClass_712(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$3$1_2_classLit;
}
;
_.onFailure = function onFailure_44(caught){
}
;
_.onSuccess = function onSuccess_53(result){
  $onSuccess_49(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
function $$init_712(){
}

function UserOrganizationsContent$5_0(this$0, val$uuid, val$item){
  this.this$0 = this$0;
  this.val$uuid = val$uuid;
  this.val$item = val$item;
  Object_1.call(this);
  $$init_712();
}

function UserOrganizationsContent$5(){
}

_ = UserOrganizationsContent$5_0.prototype = UserOrganizationsContent$5.prototype = new Object_0;
_.getClass$ = function getClass_713(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$5_2_classLit;
}
;
_.run = function run_8(){
  $fireEvent_3(get_10(), new DispatchEvent_0(new DeleteUserOrgAction_0(this.val$uuid), new UserOrganizationsContent$5$1_0(this, this.val$item), this.this$0.viewPanel));
}
;
_.castableTypeMap$ = {485:1};
_.this$0 = null;
_.val$item = null;
_.val$uuid = null;
function $$init_713(){
}

function $onSuccess_50(this$static){
  $removeFromParent($getInstance(this$static.val$item));
}

function UserOrganizationsContent$5$1_0(this$1, val$item){
  this.this$1 = this$1;
  this.val$item = val$item;
  Object_1.call(this);
  $$init_713();
}

function UserOrganizationsContent$5$1(){
}

_ = UserOrganizationsContent$5$1_0.prototype = UserOrganizationsContent$5$1.prototype = new Object_0;
_.getClass$ = function getClass_714(){
  return Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$5$1_2_classLit;
}
;
_.onFailure = function onFailure_45(caught){
  instanceOf(caught, 393) && $setText_8($getFailMessage(this.this$1.this$0.viewPanel), ($clinit_131() , c_1).userOrg_error_userOrganizationNotExists());
}
;
_.onSuccess = function onSuccess_54(result){
  $onSuccess_50(this, dynamicCast(result, 364));
}
;
_.castableTypeMap$ = {};
_.this$1 = null;
_.val$item = null;
function $debug_1(this$static, message, t){
  $log_4(this$static.logger, ($clinit_68() , CONFIG_0), message, t);
}

function $$init_803(){
}

function ListUsStateAction_0(){
  Object_1.call(this);
  $$init_803();
}

function ListUsStateAction(){
}

_ = ListUsStateAction_0.prototype = ListUsStateAction.prototype = new Object_0;
_.getClass$ = function getClass_804(){
  return Lconsys_common_gwt_client_rpc_action_ListUsStateAction_2_classLit;
}
;
_.castableTypeMap$ = {57:1, 441:1};
function $getSelectBoxResult(this$static){
  return this$static.list;
}

function getPersonButton(text){
  return new ActionImage_0(new Image_2(system_0().actionImagePerson()), text);
}

function $$init_858(){
}

function $getAddress_0(this$static){
  var address;
  address = new ClientAddress_0;
  $setLocation(address, isNotNull($getSelectedItem_1(this$static.locationBox))?$intValue_0(dynamicCast($getItem_0($getSelectedItem_1(this$static.locationBox)), 165)):0);
  $setCity(address, $trim(this$static.cityBox.getText()));
  $setStreet(address, $trim(this$static.streetBox.getText()));
  $setZip(address, $trim(this$static.zipBox.getText()));
  $getLocation(address) == $intValue_0(USA_ID) && $setStateUs(address, isNotNull($getSelectedItem_1(this$static.stateBox))?$intValue_0(dynamicCast($getItem_0($getSelectedItem_1(this$static.stateBox)), 165)):0);
  return address;
}

function $setAddress_0(this$static, address){
  if (isNotNull(address)) {
    $getLocation(address) > 0?$selectItem_0(this$static.locationBox, valueOf_53($getLocation(address))):initCountryBox(this$static.locationBox);
    this$static.cityBox.setText($getCity(address));
    this$static.streetBox.setText($getStreet(address));
    this$static.zipBox.setText($getZip(address));
    $setEnabled_7(this$static.stateBox, $getLocation(address) == $intValue_0(USA_ID));
    $getStateUs(address) > 0 && $selectItem_0(this$static.stateBox, valueOf_53($getStateUs(address)));
  }
   else {
    $selectFirst(this$static.locationBox, false);
    this$static.cityBox.setText('');
    this$static.streetBox.setText('');
    this$static.zipBox.setText('');
    $setEnabled_7(this$static.stateBox, false);
    $selectFirst(this$static.stateBox, false);
    initCountryBox(this$static.locationBox);
  }
}

function AddressForm_0(){
  $clinit_179();
  AddressForm_1.call(this, null, null);
}

function AddressForm_1(button, cancel){
  $clinit_179();
  var statePanel, zipStatePanel;
  BaseForm_0.call(this);
  $$init_858();
  this.locationBox = new SelectBox_0;
  this.stateBox = new SelectBox_0;
  $setWidth_5(this.stateBox, 100);
  $setEnabled_7(this.stateBox, false);
  this.cityBox = getFormInput();
  this.streetBox = getFormInput();
  this.zipBox = getFormInputHalf();
  $setMaxLength_0(this.zipBox, 8);
  $addChangeValueHandler_0(this.locationBox, new AddressForm$1_0(this));
  $selectFirst(this.locationBox, false);
  statePanel = new HorizontalPanel_0;
  $setVerticalAlignment_0(statePanel, ($clinit_90() , ALIGN_MIDDLE));
  $add_9(statePanel, getStyledLabel(($clinit_515() , c_4).const_address_state() + ':', initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marR10'])));
  $add_9(statePanel, this.stateBox);
  zipStatePanel = new HorizontalPanel_0;
  zipStatePanel.setWidth('100%');
  $add_9(zipStatePanel, this.zipBox);
  $add_9(zipStatePanel, statePanel);
  $setCellHorizontalAlignment(zipStatePanel, statePanel, ($clinit_89() , ALIGN_RIGHT));
  $addOptional(this, 1, ($clinit_515() , c_4).const_address_location(), this.locationBox);
  $addOptional(this, 2, ($clinit_515() , c_4).const_address_city(), this.cityBox);
  $addOptional(this, 3, ($clinit_515() , c_4).const_address_street(), this.streetBox);
  $addOptional(this, 4, ($clinit_515() , c_4).const_address_zip(), zipStatePanel);
  isNotNull(button) && isNotNull(cancel) && $addActionMembers(this, 5, button, cancel);
  $fireEvent_3(get_10(), new DispatchEvent_0(new ListLocationAction_0, new AddressForm$2_0(this), null));
  $fireEvent_3(get_10(), new DispatchEvent_0(new ListUsStateAction_0, new AddressForm$3_0(this), null));
}

function AddressForm(){
}

_ = AddressForm_1.prototype = AddressForm_0.prototype = AddressForm.prototype = new BaseForm;
_.getClass$ = function getClass_859(){
  return Lconsys_common_gwt_client_ui_comp_AddressForm_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1};
_.cityBox = null;
_.locationBox = null;
_.stateBox = null;
_.streetBox = null;
_.zipBox = null;
function $$init_859(){
}

function AddressForm$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_859();
}

function AddressForm$1(){
}

_ = AddressForm$1_0.prototype = AddressForm$1.prototype = new Object_0;
_.getClass$ = function getClass_860(){
  return Lconsys_common_gwt_client_ui_comp_AddressForm$1_2_classLit;
}
;
_.onChangeValue = function onChangeValue_4(event_0){
  $setEnabled_7(this.this$0.stateBox, $equals_6(dynamicCast($getItem_0(dynamicCast($getValue_12(event_0), 362)), 165), ($clinit_179() , USA_ID)));
}
;
_.castableTypeMap$ = {101:1, 458:1};
_.this$0 = null;
function $$init_860(){
}

function $onSuccess_61(this$static, result){
  var e, e$iterator, out;
  out = new ArrayList_0;
  for (e$iterator = $getList_0(result).iterator_0(); e$iterator.hasNext_0();) {
    e = dynamicCast(e$iterator.next_0(), 360);
    $add(out, new SelectBoxItem_2($getId_1(e), $getName_5(e)));
  }
  $setItems_0(this$static.this$0.locationBox, out);
}

function AddressForm$2_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_860();
}

function AddressForm$2(){
}

_ = AddressForm$2_0.prototype = AddressForm$2.prototype = new Object_0;
_.getClass$ = function getClass_861(){
  return Lconsys_common_gwt_client_ui_comp_AddressForm$2_2_classLit;
}
;
_.onFailure = function onFailure_56(caught){
  $error_0(($clinit_179() , logger_14), 'Failed to load locations');
}
;
_.onSuccess = function onSuccess_64(result){
  $onSuccess_61(this, dynamicCast(result, 361));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_861(){
}

function $onSuccess_62(this$static, result){
  var e, e$iterator, out;
  out = new ArrayList_0;
  for (e$iterator = $getList_0(result).iterator_0(); e$iterator.hasNext_0();) {
    e = dynamicCast(e$iterator.next_0(), 360);
    $add(out, new SelectBoxItem_2($getId_1(e), $getName_5(e)));
  }
  $setItems_0(this$static.this$0.stateBox, out);
}

function AddressForm$3_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_861();
}

function AddressForm$3(){
}

_ = AddressForm$3_0.prototype = AddressForm$3.prototype = new Object_0;
_.getClass$ = function getClass_862(){
  return Lconsys_common_gwt_client_ui_comp_AddressForm$3_2_classLit;
}
;
_.onFailure = function onFailure_57(caught){
  $error_0(($clinit_179() , logger_14), 'Failed to load US states');
}
;
_.onSuccess = function onSuccess_65(result){
  $onSuccess_62(this, dynamicCast(result, 361));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $setFocus_3(this$static, value){
  $setFocus_0(this$static.content_0, value);
}

function $$init_892(){
}

function ConsysSuggestOracle$1_0(this$0, val$query, val$callback, val$request){
  $clinit_7();
  this.this$0 = this$0;
  this.val$query = val$query;
  this.val$callback = val$callback;
  this.val$request = val$request;
  Timer_0.call(this);
  $$init_892();
}

function ConsysSuggestOracle$1(){
}

_ = ConsysSuggestOracle$1_0.prototype = ConsysSuggestOracle$1.prototype = new Timer;
_.getClass$ = function getClass_893(){
  return Lconsys_common_gwt_client_ui_comp_ConsysSuggestOracle$1_2_classLit;
}
;
_.run = function run_16(){
  this.this$0.suggest(this.val$query, 5, new ConsysSuggestOracle$SuggestCallback_0(this.val$callback, this.val$request));
}
;
_.castableTypeMap$ = {142:1};
_.this$0 = null;
_.val$callback = null;
_.val$query = null;
_.val$request = null;
function $$init_893(){
}

function $reponse(this$static, response){
  this$static.callback.onSuggestionsReady(this$static.request, new SuggestOracle$Response_0(response));
}

function ConsysSuggestOracle$SuggestCallback_0(callback, request){
  Object_1.call(this);
  $$init_893();
  this.callback = callback;
  this.request = request;
}

function ConsysSuggestOracle$SuggestCallback(){
}

_ = ConsysSuggestOracle$SuggestCallback_0.prototype = ConsysSuggestOracle$SuggestCallback.prototype = new Object_0;
_.getClass$ = function getClass_894(){
  return Lconsys_common_gwt_client_ui_comp_ConsysSuggestOracle$SuggestCallback_2_classLit;
}
;
_.castableTypeMap$ = {};
_.callback = null;
_.request = null;
function $setEnabled_6(this$static, value){
  this$static.area.setEnabled(value);
}

_ = Form_0.prototype;
_.insertRow = function insertRow_0(beforeRow){
  $nextRow(this);
  $insertRow_1(this, beforeRow);
}
;
function $$init_921(this$static){
  this$static.items = new ArrayList_0;
}

function $addItem_2(this$static, item){
  return $insertItem(this$static, item, $size(this$static.items));
}

function $addItemElement(this$static, beforeIndex, elem){
  var div;
  div = createDiv();
  insertChild(this$static.body_0, div, beforeIndex);
  appendChild(div, elem);
}

function $clearItems(this$static){
  var container, item, item$iterator;
  $selectItem(this$static, null);
  container = $getItemContainerElement(this$static);
  while (getChildCount(container) > 0) {
    removeChild(container, getChild(container, 0));
  }
  for (item$iterator = this$static.items.iterator_0(); item$iterator.hasNext_0();) {
    item = dynamicCast(item$iterator.next_0(), 459);
    $setParentMenu(item, null);
  }
  $clear(this$static.items);
}

function $close_0(){
}

function $closeAllParents(){
  $close_0();
}

function $doItemAction(this$static, item, fireCommand){
  var cmd;
  $selectItem(this$static, item);
  if (isNotNull(item)) {
    if (fireCommand && isNotNull($getCommand(item))) {
      $closeAllParents();
      cmd = $getCommand(item);
      addCommand(cmd);
    }
  }
}

function $eatEvent(event_0){
  eventCancelBubble(event_0, true);
  eventPreventDefault_0(event_0);
}

function $findItem(this$static, hItem){
  var item, item$iterator;
  for (item$iterator = this$static.items.iterator_0(); item$iterator.hasNext_0();) {
    item = dynamicCast(item$iterator.next_0(), 459);
    if (isOrHasChild_1($getElement(item), hItem)) {
      return item;
    }
  }
  return null;
}

function $focus_1(this$static){
  $focus($getElement(this$static));
}

function $getItemContainerElement(this$static){
  return this$static.body_0;
}

function $getItems(this$static){
  return this$static.items;
}

function $getSelectedItem_0(this$static){
  return this$static.selectedItem;
}

function $init_7(this$static){
  var fp;
  this$static.body_0 = createDiv();
  fp = new FocusPanel_0;
  $appendChild($getElement(fp), this$static.body_0);
  $setElement_0(this$static, $getElement(fp));
  setRole($getElement(this$static), 'menubar');
  this$static.sinkEvents(2225);
  setStyleAttribute($getElement(this$static), 'outline', '0px');
  setElementAttribute($getElement(this$static), 'hideFocus', 'true');
}

function $insertItem(this$static, item, beforeIndex){
  if (beforeIndex < 0 || beforeIndex > $size(this$static.items)) {
    throw new IndexOutOfBoundsException_0;
  }
  $add_0(this$static.items, beforeIndex, item);
  $addItemElement(this$static, beforeIndex, $getElement(item));
  $setParentMenu(item, this$static);
  $setSelectionStyle(item, false);
  return item;
}

function $itemOver(this$static, item, focus_0){
  if (isNull(item)) {
    if (isNotNull(this$static.selectedItem)) {
      return;
    }
  }
  $selectItem(this$static, item);
  focus_0 && $focus_1(this$static);
}

function $moveSelectionDown(this$static){
  if ($selectFirstItemIfNoneSelected(this$static)) {
    return;
  }
  $selectNextItem(this$static);
}

function $moveSelectionUp(this$static){
  if ($selectFirstItemIfNoneSelected(this$static)) {
    return;
  }
  $selectPrevItem(this$static);
}

function $moveToNextItem(this$static){
  if ($selectFirstItemIfNoneSelected(this$static)) {
    return;
  }
  $selectNextItem(this$static);
}

function $moveToPrevItem(this$static){
  if ($selectFirstItemIfNoneSelected(this$static)) {
    return;
  }
}

function $onHide_0(this$static, focus_0){
  this$static.popup.hide_0();
  focus_0 && $focus_1(this$static);
}

function $selectFirstItemIfNoneSelected(this$static){
  var nextItem;
  if (isNull(this$static.selectedItem)) {
    if ($size(this$static.items) > 0) {
      nextItem = dynamicCast($get(this$static.items, 0), 459);
      $selectItem(this$static, nextItem);
    }
    return true;
  }
  return false;
}

function $selectItem(this$static, item){
  var td, tr;
  if (jsEquals(item, this$static.selectedItem)) {
    return;
  }
  if (isNotNull(this$static.selectedItem)) {
    $setSelectionStyle(this$static.selectedItem, false);
    tr = getParent($getElement(this$static.selectedItem));
    if (getChildCount(tr) == 2) {
      td = getChild(tr, 1);
      setStyleName_0(td, 'subMenuIcon-selected', false);
    }
  }
  if (isNotNull(item)) {
    $setSelectionStyle(item, true);
    setState($getElement(this$static), 'aria-activedescendant', getElementAttribute($getElement(item), 'id'));
  }
  this$static.selectedItem = item;
}

function $selectNextItem(this$static){
  var index, itemToBeSelected;
  if (isNull(this$static.selectedItem)) {
    return;
  }
  index = $indexOf(this$static.items, this$static.selectedItem);
  index < $size(this$static.items) - 1?(itemToBeSelected = dynamicCast($get(this$static.items, index + 1), 459)):(itemToBeSelected = dynamicCast($get(this$static.items, 0), 459));
  $selectItem(this$static, itemToBeSelected);
}

function $selectPrevItem(this$static){
  var index, itemToBeSelected;
  if (isNull(this$static.selectedItem)) {
    return;
  }
  index = $indexOf(this$static.items, this$static.selectedItem);
  index > 0?(itemToBeSelected = dynamicCast($get(this$static.items, index - 1), 459)):(itemToBeSelected = dynamicCast($get(this$static.items, $size(this$static.items) - 1), 459));
  $selectItem(this$static, itemToBeSelected);
}

function MenuBar_0(){
  Widget_0.call(this);
  $$init_921(this);
  $init_7(this);
}

function MenuBar(){
}

_ = MenuBar.prototype = new Widget;
_.getClass$ = function getClass_922(){
  return Lconsys_common_gwt_client_ui_comp_MenuBar_2_classLit;
}
;
_.onBrowserEvent = function onBrowserEvent_17(event_0){
  var item, keyCode;
  item = $findItem(this, eventGetTarget_0(event_0));
  switch (eventGetType(event_0)) {
    case 1:
      {
        $focus($getElement(this));
        isNotNull(item) && $doItemAction(this, item, true);
        break;
      }

    case 16:
      {
        isNotNull(item) && $itemOver(this, item, true);
        break;
      }

    case 32:
      {
        isNotNull(item) && $itemOver(this, null, true);
        break;
      }

    case 2048:
      {
        $selectFirstItemIfNoneSelected(this);
        break;
      }

    case 128:
      {
        keyCode = eventGetKeyCode(event_0);
        switch (keyCode) {
          case 37:
            $isRTL_0(getCurrentLocale())?$moveToNextItem(this):$moveToPrevItem(this);
            $eatEvent(event_0);
            break;
          case 39:
            $isRTL_0(getCurrentLocale())?$moveToPrevItem(this):$moveToNextItem(this);
            $eatEvent(event_0);
            break;
          case 38:
            $moveSelectionUp(this);
            $eatEvent(event_0);
            break;
          case 40:
            $moveSelectionDown(this);
            $eatEvent(event_0);
            break;
          case 27:
            $closeAllParents();
            isNotNull(this.popup) && this.popup.hide_0();
            $eatEvent(event_0);
            break;
          case 13:
            if (!$selectFirstItemIfNoneSelected(this)) {
              $doItemAction(this, this.selectedItem, true);
              $eatEvent(event_0);
            }

        }
        break;
      }

  }
  $onBrowserEvent(this, event_0);
}
;
_.onClose = function onClose_2(event_0){
  $onHide_0(this, !$isAutoClosed(event_0));
  fire_1(this, dynamicCast($getTarget(event_0), 11));
  this.popup = null;
}
;
_.onDetach = function onDetach_4(){
  isNotNull(this.popup) && this.popup.hide_0();
  $onDetach(this);
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 92:1, 101:1, 147:1};
_.body_0 = null;
_.popup = null;
_.selectedItem = null;
function $$init_922(){
}

function $getCommand(this$static){
  return this$static.command;
}

function $setCommand(this$static, cmd){
  this$static.command = cmd;
}

function $setHTML_2(this$static, html){
  setInnerHTML($getElement(this$static), html);
}

function $setParentMenu(this$static, parentMenu){
  this$static , parentMenu;
}

function $setSelectionStyle(this$static, selected){
  selected?$addStyleDependentName(this$static, 'selected'):$removeStyleDependentName(this$static, 'selected');
}

function $setText_12(this$static, text){
  setInnerText_1($getElement(this$static), text);
}

function MenuItem_0(text, asHTML){
  UIObject_0.call(this);
  $$init_922();
  $setElement_0(this, createDiv());
  $setSelectionStyle(this, false);
  asHTML?$setHTML_2(this, text):$setText_12(this, text);
  this.setStyleName('gwt-MenuItem');
  setElementAttribute($getElement(this), 'id', createUniqueId());
  setRole($getElement(this), 'menuitem');
}

function MenuItem(){
}

_ = MenuItem.prototype = new UIObject;
_.getClass$ = function getClass_923(){
  return Lconsys_common_gwt_client_ui_comp_MenuItem_2_classLit;
}
;
_.getText = function getText_14(){
  return getInnerText_1($getElement(this));
}
;
_.castableTypeMap$ = {147:1, 459:1};
_.command = null;
function $clinit_185(){
  $clinit_185 = nullMethod;
  $clinit_6();
  MONTHS = initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, [($clinit_515() , c_4).const_date_monthJanuary(), ($clinit_515() , c_4).const_date_monthFebruary(), ($clinit_515() , c_4).const_date_monthMarch(), ($clinit_515() , c_4).const_date_monthApril(), ($clinit_515() , c_4).const_date_monthMay(), ($clinit_515() , c_4).const_date_monthJune(), ($clinit_515() , c_4).const_date_monthJuly(), ($clinit_515() , c_4).const_date_monthAugust(), ($clinit_515() , c_4).const_date_monthSeptember(), ($clinit_515() , c_4).const_date_monthOctober(), ($clinit_515() , c_4).const_date_monthNovember(), ($clinit_515() , c_4).const_date_monthDecember()]);
  '40px';
}

function $$init_924(this$static){
}

function $getDate_0(this$static){
  var dateText, format, month, year;
  if ($getMonth_0(this$static) == -1 || $getYear(this$static) < 1950 || $getYear(this$static) > getYear_2(new Date_1)) {
    return null;
  }
  month = $getMonth_0(this$static) + 1;
  year = $getYear(this$static);
  dateText = '1.' + month + '.' + year;
  format = getFormat_0('d.M.y');
  return $parse(format, dateText);
}

function $getMonth_0(this$static){
  var item;
  item = $getSelectedItem_1(this$static.monthBox);
  return isNull(item)?-1:$intValue_0(dynamicCast($getItem_0(item), 165));
}

function $getYear(this$static){
  return $getTextInt(this$static.yearBox);
}

function $isRequired(this$static){
  return this$static.required;
}

function $setDate_0(this$static, date){
  if (isNotNull(date)) {
    $setMonth_1(this$static, date.getMonth_0());
    $setYear_0(this$static, getYear_2(date));
  }
}

function $setMonth_1(this$static, value){
  $selectItem_0(this$static.monthBox, valueOf_53(value));
}

function $setRequired(this$static, required){
  this$static.required = required;
}

function $setYear_0(this$static, value){
  $setText_7(this$static.yearBox, value);
}

function MonthYearDatePicker_0(){
  $clinit_185();
  var i, items, panel;
  Composite_0.call(this);
  $$init_924(this);
  this.yearBox = new ConsysIntTextBox_0(1950, getYear_2(new Date_1), ($clinit_515() , c_4).const_date_year());
  this.yearBox.setWidth('40px');
  this.yearBox.addStyleName('floatL');
  $setMaxLength_0(this.yearBox, 4);
  this.monthBox = new SelectBox_0;
  $setWidth_5(this.monthBox, 95);
  items = new ArrayList_0;
  for (i = 0; i < MONTHS.length; ++i) {
    $add(items, new SelectBoxItem_2(valueOf_53(i), MONTHS[i]));
  }
  $setItems_0(this.monthBox, items);
  $selectFirst(this.monthBox, true);
  this.monthBox.addStyleName('marR5');
  this.monthBox.addStyleName('floatL');
  panel = new FlowPanel_0;
  panel.add_1(this.monthBox);
  panel.add_1(this.yearBox);
  panel.add_1(clearDiv_0());
  $initWidget(this, panel);
}

function MonthYearDatePicker(){
}

_ = MonthYearDatePicker_0.prototype = MonthYearDatePicker.prototype = new Composite;
_.doValidate = function doValidate_17(fail){
  var out;
  out = true;
  if (this.isVisible() && isNotNull($getParent(this))) {
    if (!$doValidate_1(this.yearBox, fail)) {
      return false;
    }
    out = !$isRequired(this) || isNotNull($getDate_0(this));
  }
  return out;
}
;
_.getClass$ = function getClass_925(){
  return Lconsys_common_gwt_client_ui_comp_MonthYearDatePicker_2_classLit;
}
;
_.onFail = function onFail_15(){
  this.yearBox.addStyleName('consysGrayBg');
}
;
_.onSuccess_0 = function onSuccess_70(){
  this.yearBox.removeStyleName('consysGrayBg');
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 147:1, 457:1};
_.monthBox = null;
_.required = false;
_.yearBox = null;
var MONTHS;
function $setRequired_0(this$static, required){
  this$static.required = required;
}

function $$init_943(this$static){
  this$static.callback = new SuggestionBox$1_0(this$static);
}

function $addEventsToTextBox(this$static){
  var events;
  events = new SuggestionBox$1TextBoxEvents_0(this$static);
  $addKeyHandlersTo(events, this$static.box);
  $addValueChangeHandler_1(this$static.box, events);
}

function $addSelectionHandler(this$static, handler){
  return $addHandler(this$static, handler, getType_29());
}

function $createPopup(this$static){
  var p;
  p = new PopupPanel_2(true, false);
  p.setWidget(this$static.suggestionMenu);
  p.setStyleName('gwt-SuggestBoxPopup');
  $setPreviewingAllNativeEvents(p, true);
  $addAutoHidePartner(p, $getElement($getTextBox_0(this$static)));
  return p;
}

function $fireSuggestionEvent(this$static, selectedSuggestion){
  fire_5(this$static, selectedSuggestion);
}

function $getSuggestionPopup(this$static){
  return this$static.suggestionPopup;
}

function $getText_9(this$static){
  return this$static.box.getText();
}

function $getTextBox_0(this$static){
  return this$static.box;
}

function $isRequired_1(this$static){
  return this$static.required;
}

function $refreshSuggestions(this$static){
  var text;
  text = this$static.box.getText();
  if ($equals_8(text, this$static.currentText)) {
    return;
  }
   else {
    this$static.currentText = text;
  }
  $showSuggestions(this$static, text);
}

function $setAutoSelectEnabled(this$static, selectsFirstItem){
  this$static.selectsFirstItem = selectsFirstItem;
}

function $setNewSelection(this$static, menuItem){
  var curSuggestion;
  curSuggestion = $getSuggestion(menuItem);
  this$static.currentText = curSuggestion.getReplacementString();
  $setText_13(this$static, this$static.currentText);
  $getSuggestionPopup(this$static).hide_0();
  $fireSuggestionEvent(this$static, curSuggestion);
}

function $setOracle(this$static, oracle){
  this$static.oracle = oracle;
}

function $setPopupStyleName(this$static, style){
  $getSuggestionPopup(this$static).setStyleName(style);
}

function $setRequired_1(this$static, required){
  this$static.required = required;
}

function $setText_13(this$static, text){
  this$static.box.setText(text);
}

function $showSuggestions(this$static, query){
  $length_2(query) == 0?this$static.oracle.requestDefaultSuggestions(new SuggestOracle$Request_0(null, this$static.limit), this$static.callback):this$static.oracle.requestSuggestions(new SuggestOracle$Request_0(query, this$static.limit), this$static.callback);
}

function $showSuggestions_0(this$static, suggestions){
  var curSuggestion, curSuggestion$iterator, isAnimationEnabled, menuItem;
  if (suggestions.size_1() > 0) {
    isAnimationEnabled = $isAnimationEnabled($getSuggestionPopup(this$static));
    $getSuggestionPopup(this$static).isAttached() && $getSuggestionPopup(this$static).hide_0();
    $clearItems(this$static.suggestionMenu);
    for (curSuggestion$iterator = suggestions.iterator_0(); curSuggestion$iterator.hasNext_0();) {
      curSuggestion = dynamicCast(curSuggestion$iterator.next_0(), 314);
      menuItem = new SuggestionBox$SuggestionMenuItem_0(this$static, curSuggestion, this$static.oracle.isDisplayStringHTML());
      $setCommand(menuItem, new SuggestionBox$2_0(this$static, menuItem));
      $addItem_2(this$static.suggestionMenu, menuItem);
    }
    this$static.selectsFirstItem && $selectItem_1(this$static.suggestionMenu, 0);
    $showRelativeTo($getSuggestionPopup(this$static), $getTextBox_0(this$static));
    $setAnimationEnabled($getSuggestionPopup(this$static), isAnimationEnabled);
  }
   else {
    $getSuggestionPopup(this$static).hide_0();
  }
}

function SuggestionBox_0(oracle){
  SuggestionBox_1.call(this, oracle, new TextBox_0);
}

function SuggestionBox_1(oracle, box){
  Composite_0.call(this);
  $$init_943(this);
  this.box = box;
  $initWidget(this, box);
  this.suggestionMenu = new SuggestionBox$SuggestionMenu_0;
  this.suggestionPopup = $createPopup(this);
  $addEventsToTextBox(this);
  $setOracle(this, oracle);
  this.addStyleName('h21');
  this.addStyleName(system_0().css_7().inputPadd());
  $setPopupStyleName(this, 'sugBoxPopup');
  this.setStyleName('gwt-SuggestBox');
  this.addStyleName('inputBorder');
  this.addStyleName('w270');
  this.suggestionPopup.addStyleName('w270');
  this.suggestionMenu.addStyleName('w270');
  $setAutoSelectEnabled(this, true);
  this.addStyleName('h21');
  this.addStyleName(system_0().css_7().inputPadd());
  $setPopupStyleName(this, 'sugBoxPopup');
  $setAutoSelectEnabled(this, true);
}

function SuggestionBox(){
}

_ = SuggestionBox_0.prototype = SuggestionBox.prototype = new Composite;
_.addKeyDownHandler = function addKeyDownHandler_1(handler){
  return $addDomHandler(this, handler, getType_13());
}
;
_.addKeyPressHandler = function addKeyPressHandler_1(handler){
  return $addDomHandler(this, handler, getType_14());
}
;
_.addKeyUpHandler = function addKeyUpHandler_1(handler){
  return $addDomHandler(this, handler, getType_15());
}
;
_.doValidate = function doValidate_19(fail){
  if ($isRequired_1(this) && $getTextBox_0(this).isEnabled_0()) {
    return isValidString($getText_9(this));
  }
  return true;
}
;
_.getClass$ = function getClass_944(){
  return Lconsys_common_gwt_client_ui_comp_SuggestionBox_2_classLit;
}
;
_.getText = function getText_16(){
  return $getText_9(this);
}
;
_.onFail = function onFail_17(){
  $getTextBox_0(this).addStyleName('consysGrayBg');
}
;
_.onSuccess_0 = function onSuccess_72(){
  $getTextBox_0(this).removeStyleName('consysGrayBg');
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 50:1, 147:1, 296:1, 335:1, 336:1, 457:1};
_.box = null;
_.currentText = null;
_.limit = 20;
_.oracle = null;
_.required = false;
_.selectsFirstItem = true;
_.suggestionMenu = null;
_.suggestionPopup = null;
function $$init_944(){
}

function SuggestionBox$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_944();
}

function SuggestionBox$1(){
}

_ = SuggestionBox$1_0.prototype = SuggestionBox$1.prototype = new Object_0;
_.getClass$ = function getClass_945(){
  return Lconsys_common_gwt_client_ui_comp_SuggestionBox$1_2_classLit;
}
;
_.onSuggestionsReady = function onSuggestionsReady(request, response){
  $showSuggestions_0(this.this$0, $getSuggestions(response));
}
;
_.castableTypeMap$ = {};
_.this$0 = null;
function $$init_945(){
}

function SuggestionBox$1TextBoxEvents_0(this$0){
  this.this$0 = this$0;
  HandlesAllKeyEvents_0.call(this);
  $$init_945();
}

function SuggestionBox$1TextBoxEvents(){
}

_ = SuggestionBox$1TextBoxEvents_0.prototype = SuggestionBox$1TextBoxEvents.prototype = new HandlesAllKeyEvents;
_.getClass$ = function getClass_946(){
  return Lconsys_common_gwt_client_ui_comp_SuggestionBox$1TextBoxEvents_2_classLit;
}
;
_.onKeyDown = function onKeyDown_0(event_0){
  if ($getSuggestionPopup(this.this$0).isAttached()) {
    switch ($getNativeKeyCode(event_0)) {
      case 40:
        $selectItem_1(this.this$0.suggestionMenu, $getSelectedItemIndex(this.this$0.suggestionMenu) + 1);
        break;
      case 38:
        $selectItem_1(this.this$0.suggestionMenu, $getSelectedItemIndex(this.this$0.suggestionMenu) - 1);
        break;
      case 13:
      case 9:
        $getSelectedItemIndex(this.this$0.suggestionMenu) < 0?$getSuggestionPopup(this.this$0).hide_0():$doSelectedItemAction(this.this$0.suggestionMenu);
    }
  }
  $delegateEvent(this.this$0, event_0);
}
;
_.onKeyPress = function onKeyPress_0(event_0){
  $delegateEvent(this.this$0, event_0);
}
;
_.onKeyUp = function onKeyUp_18(event_0){
  $refreshSuggestions(this.this$0);
  $delegateEvent(this.this$0, event_0);
}
;
_.onValueChange = function onValueChange_2(event_0){
  $delegateEvent(this.this$0, event_0);
}
;
_.castableTypeMap$ = {75:1, 76:1, 77:1, 99:1, 101:1};
_.this$0 = null;
function $$init_946(){
}

function SuggestionBox$2_0(this$0, val$menuItem){
  this.this$0 = this$0;
  this.val$menuItem = val$menuItem;
  Object_1.call(this);
  $$init_946();
}

function SuggestionBox$2(){
}

_ = SuggestionBox$2_0.prototype = SuggestionBox$2.prototype = new Object_0;
_.execute = function execute_18(){
  $setNewSelection(this.this$0, this.val$menuItem);
}
;
_.getClass$ = function getClass_947(){
  return Lconsys_common_gwt_client_ui_comp_SuggestionBox$2_2_classLit;
}
;
_.castableTypeMap$ = {139:1};
_.this$0 = null;
_.val$menuItem = null;
function $$init_947(){
}

function $doSelectedItemAction(this$static){
  var selectedItem;
  selectedItem = $getSelectedItem_0(this$static);
  isNotNull(selectedItem) && $doItemAction(this$static, selectedItem, true);
}

function $getSelectedItemIndex(this$static){
  var selectedItem;
  selectedItem = $getSelectedItem_0(this$static);
  if (isNotNull(selectedItem)) {
    return $getItems(this$static).indexOf_0(selectedItem);
  }
  return -1;
}

function $selectItem_1(this$static, index){
  var items;
  items = $getItems(this$static);
  index > -1 && index < items.size_1() && $itemOver(this$static, dynamicCast(items.get(index), 459), false);
}

function SuggestionBox$SuggestionMenu_0(){
  MenuBar_0.call(this);
  $$init_947();
  this.setStyleName('');
}

function SuggestionBox$SuggestionMenu(){
}

_ = SuggestionBox$SuggestionMenu_0.prototype = SuggestionBox$SuggestionMenu.prototype = new MenuBar;
_.getClass$ = function getClass_948(){
  return Lconsys_common_gwt_client_ui_comp_SuggestionBox$SuggestionMenu_2_classLit;
}
;
_.castableTypeMap$ = {2:1, 45:1, 46:1, 47:1, 49:1, 92:1, 101:1, 147:1};
function $$init_948(){
}

function $getSuggestion(this$static){
  return this$static.suggestion;
}

function $setSuggestion(this$static, suggestion){
  this$static.suggestion = suggestion;
}

function SuggestionBox$SuggestionMenuItem_0(this$0, suggestion, asHTML){
  this , this$0;
  MenuItem_0.call(this, suggestion.getDisplayString(), asHTML);
  $$init_948();
  this.setStyleName('item');
  $setSuggestion(this, suggestion);
}

function SuggestionBox$SuggestionMenuItem(){
}

_ = SuggestionBox$SuggestionMenuItem_0.prototype = SuggestionBox$SuggestionMenuItem.prototype = new MenuItem;
_.getClass$ = function getClass_949(){
  return Lconsys_common_gwt_client_ui_comp_SuggestionBox$SuggestionMenuItem_2_classLit;
}
;
_.castableTypeMap$ = {147:1, 459:1, 461:1};
_.suggestion = null;
function $$init_1083(){
}

function CRUDSimpleHelpFormPanel$1_0(this$0){
  this.this$0 = this$0;
  Object_1.call(this);
  $$init_1083();
}

function CRUDSimpleHelpFormPanel$1(){
}

_ = CRUDSimpleHelpFormPanel$1_0.prototype = CRUDSimpleHelpFormPanel$1.prototype = new Object_0;
_.getClass$ = function getClass_1084(){
  return Lconsys_common_gwt_client_ui_comp_panel_abst_CRUDSimpleHelpFormPanel$1_2_classLit;
}
;
_.onClick = function onClick_82(event_0){
  var w;
  w = this.this$0.createForm();
  isNotNull(w) && $setWidget_2(this.this$0, w);
}
;
_.castableTypeMap$ = {67:1, 101:1};
_.this$0 = null;
_ = ConsysConstants_.prototype;
_.const_address_state = function const_address_state(){
  return 'State';
}
;
_.const_change = function const_change(){
  return 'Change';
}
;
_.const_date_monthApril = function const_date_monthApril(){
  return 'April';
}
;
_.const_date_monthAugust = function const_date_monthAugust(){
  return 'August';
}
;
_.const_date_monthDecember = function const_date_monthDecember(){
  return 'December';
}
;
_.const_date_monthFebruary = function const_date_monthFebruary(){
  return 'February';
}
;
_.const_date_monthJanuary = function const_date_monthJanuary(){
  return 'January';
}
;
_.const_date_monthJuly = function const_date_monthJuly(){
  return 'July';
}
;
_.const_date_monthJune = function const_date_monthJune(){
  return 'June';
}
;
_.const_date_monthMarch = function const_date_monthMarch(){
  return 'March';
}
;
_.const_date_monthMay = function const_date_monthMay(){
  return 'May';
}
;
_.const_date_monthNovember = function const_date_monthNovember(){
  return 'November';
}
;
_.const_date_monthOctober = function const_date_monthOctober(){
  return 'October';
}
;
_.const_date_monthSeptember = function const_date_monthSeptember(){
  return 'September';
}
;
_.const_gender = function const_gender(){
  return 'Gender';
}
;
_.const_genderMan = function const_genderMan(){
  return 'Male';
}
;
_.const_genderNotSpec = function const_genderNotSpec(){
  return 'Not specified';
}
;
_.const_genderWoman = function const_genderWoman(){
  return 'Female';
}
;
_.const_noRecordFound = function const_noRecordFound(){
  return 'No record has been found.';
}
;
_.const_personalInfo_celiac = function const_personalInfo_celiac(){
  return 'Celiac';
}
;
_.const_personalInfo_vegetarian = function const_personalInfo_vegetarian(){
  return 'Vegetarian';
}
;
function getEditModeButton(handler){
  var edit;
  edit = new ActionLabel_1(($clinit_515() , c_4).const_edit(), Assemble(initValues(_3Ljava_lang_String_2_classLit, {57:1, 106:1, 262:1}, 1, ['marL20', 'f10px'])));
  $setClickHandler(edit, handler);
  return edit;
}

function getFormInputHalf(){
  var input;
  input = new TextBox_0;
  input.setStyleName('inputBorder');
  input.addStyleName('w85');
  input.addStyleName('h21');
  input.addStyleName(system_0().css_7().inputPadd());
  return input;
}

_ = CommonWidgetsConstants_.prototype;
_.const_middleName = function const_middleName(){
  return 'Middle name';
}
;
function updateUser(user, fail){
  var isOk;
  isOk = true;
  if (!isValidString($getFirstName_0(user))) {
    $addOrSetText(fail, ($clinit_515() , m_3).const_fieldMustEntered(($clinit_518() , c_5).const_firstName()));
    isOk = false;
  }
  if (!isValidString($getLastName_0(user))) {
    $addOrSetText(fail, ($clinit_515() , m_3).const_fieldMustEntered(($clinit_518() , c_5).const_lastName()));
    isOk = false;
  }
  return isOk;
}

function $getCity(this$static){
  return this$static.city;
}

function $getLocation(this$static){
  return this$static.location_0;
}

function $getLocationName(this$static){
  return this$static.locationName;
}

function $getStateUs(this$static){
  return this$static.stateUs;
}

function $getStateUsName(this$static){
  return this$static.stateUsName;
}

function $getZip(this$static){
  return this$static.zip;
}

function $setCity(this$static, city){
  this$static.city = city;
}

function $setLocation(this$static, location_0){
  this$static.location_0 = location_0;
}

function $setStateUs(this$static, stateUs){
  this$static.stateUs = stateUs;
}

function $setStreet(this$static, street){
  this$static.street = street;
}

function $setZip(this$static, zip){
  this$static.zip = zip;
}

function $getAcronym_0(this$static){
  return this$static.acronym;
}

function $getAddress_1(this$static){
  return this$static.address;
}

function $getFullName_1(this$static){
  return this$static.fullName;
}

function $getOrganizationType(this$static){
  return this$static.organizationType;
}

function $getUniversalName_0(this$static){
  return this$static.universalName;
}

function $getWeb_1(this$static){
  return this$static.web;
}

function $isOwner(this$static){
  return this$static.owner;
}

function $setAcronym_0(this$static, acronym){
  this$static.acronym = acronym;
}

function $setAddress_1(this$static, address){
  this$static.address = address;
}

function $setFullName_0(this$static, fullName){
  this$static.fullName = fullName;
}

function $setOrganizationType(this$static, organizationType){
  this$static.organizationType = organizationType;
}

function $setOwner_0(this$static, owner){
  this$static.owner = owner;
}

function $setUniversalName_0(this$static, universalName){
  this$static.universalName = universalName;
}

function $setWeb_0(this$static, web){
  this$static.web = web;
}

function $getAddress_2(this$static){
  return this$static.address;
}

function $getUniversalName_1(this$static){
  return this$static.universalName;
}

function $getUuid_10(this$static){
  return this$static.uuid_0;
}

function $isCeliac_0(this$static){
  return this$static.celiac;
}

function $isVegetarian_0(this$static){
  return this$static.vegetarian;
}

function $getBio(this$static){
  return this$static.bio;
}

function $getFrontDegree(this$static){
  return this$static.frontDegree;
}

function $getGender(this$static){
  return this$static.gender;
}

function $getMiddleName(this$static){
  return this$static.middleName;
}

function $getRearDegree(this$static){
  return this$static.rearDegree;
}

function $getUserOrgDefault(this$static){
  return this$static.userOrgDefault;
}

function $getUuidImg(this$static){
  return this$static.uuidImg;
}

function $setBio(this$static, bio){
  this$static.bio = bio;
}

function $setFirstName_1(this$static, firstName){
  this$static.firstName = firstName;
}

function $setFrontDegree(this$static, frontDegree){
  this$static.frontDegree = frontDegree;
}

function $setGender(this$static, gender){
  this$static.gender = gender;
}

function $setLastName_1(this$static, lastName){
  this$static.lastName = lastName;
}

function $setMiddleName(this$static, middleName){
  this$static.middleName = middleName;
}

function $setRearDegree(this$static, rearDegree){
  this$static.rearDegree = rearDegree;
}

function $setUuidImg(this$static, uuidImg){
  this$static.uuidImg = uuidImg;
}

_ = Collections$UnmodifiableList.prototype;
_.indexOf_0 = function indexOf_1(o){
  return this.list.indexOf_0(o);
}
;
var Lcom_google_gwt_event_dom_client_HandlesAllKeyEvents_2_classLit = createForClass('com.google.gwt.event.dom.client.', 'HandlesAllKeyEvents', 'HandlesAllKeyEvents', Ljava_lang_Object_2_classLit), Lcom_google_gwt_event_logical_shared_SelectionEvent_2_classLit = createForClass('com.google.gwt.event.logical.shared.', 'SelectionEvent', 'SelectionEvent', Lcom_google_gwt_event_shared_GwtEvent_2_classLit), Lcom_google_gwt_lang_asyncloaders_AsyncLoader2_2_classLit = createForClass('com.google.gwt.lang.asyncloaders.', 'AsyncLoader2', 'AsyncLoader2', Ljava_lang_Object_2_classLit), Lcom_google_gwt_user_client_ui_SuggestOracle_2_classLit = createForClass('com.google.gwt.user.client.ui.', 'SuggestOracle', 'SuggestOracle', Ljava_lang_Object_2_classLit), Lcom_google_gwt_user_client_ui_SuggestOracle$Request_2_classLit = createForClass('com.google.gwt.user.client.ui.', 'SuggestOracle$Request', 'SuggestOracle$Request', Ljava_lang_Object_2_classLit), Lcom_google_gwt_user_client_ui_SuggestOracle$Response_2_classLit = createForClass('com.google.gwt.user.client.ui.', 'SuggestOracle$Response', 'SuggestOracle$Response', Ljava_lang_Object_2_classLit), Lcom_google_gwt_user_client_ui_ValueBoxBase$1_2_classLit = createForClass('com.google.gwt.user.client.ui.', 'ValueBoxBase$1', 'ValueBoxBase$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_bo_ClientPassword_2_classLit = createForClass('consys.admin.user.gwt.client.bo.', 'ClientPassword', 'ClientPassword', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_AboutYourself_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.', 'AboutYourself', 'AboutYourself', Lconsys_common_gwt_client_ui_comp_panel_SimpleFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_AccountMenu_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.', 'AccountMenu', 'AccountMenu', Lconsys_common_gwt_client_ui_comp_panel_SimpleFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_Experience_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.', 'Experience', 'Experience', Lconsys_common_gwt_client_ui_comp_panel_SimpleFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_ProfileMenuContent_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.', 'ProfileMenuContent', 'ProfileMenuContent', Lconsys_common_gwt_client_ui_comp_panel_MenuFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_MyResume_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'MyResume', 'MyResume', Lconsys_common_gwt_client_ui_comp_panel_abst_RUSimpleHelpFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_MyResume$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'MyResume$1', 'MyResume$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_MyResume$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'MyResume$2', 'MyResume$2', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_MyResume$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'MyResume$3', 'MyResume$3', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_MyResume$3$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'MyResume$3$1', 'MyResume$3$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent', 'PasswordChangeContent', Lconsys_common_gwt_client_ui_comp_panel_abst_RUSimpleFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$1', 'PasswordChangeContent$1', Lconsys_common_gwt_client_ui_comp_ConsysPasswordTextBox_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$10_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$10', 'PasswordChangeContent$10', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$10$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$10$1', 'PasswordChangeContent$10$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$2', 'PasswordChangeContent$2', Lconsys_common_gwt_client_ui_comp_ConsysPasswordTextBox_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$3', 'PasswordChangeContent$3', Lconsys_common_gwt_client_ui_comp_panel_ConsysFlowPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$4_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$4', 'PasswordChangeContent$4', Lconsys_common_gwt_client_ui_comp_panel_ConsysFlowPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$5_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$5', 'PasswordChangeContent$5', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$6_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$6', 'PasswordChangeContent$6', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$7_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$7', 'PasswordChangeContent$7', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$8_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$8', 'PasswordChangeContent$8', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PasswordChangeContent$9_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PasswordChangeContent$9', 'PasswordChangeContent$9', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonInfoContent', 'PersonInfoContent', Lconsys_common_gwt_client_ui_comp_panel_abst_RUSimpleFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonInfoContent$1', 'PersonInfoContent$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonInfoContent$2', 'PersonInfoContent$2', Lcom_google_gwt_user_client_ui_Label_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonInfoContent$3', 'PersonInfoContent$3', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$4_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonInfoContent$4', 'PersonInfoContent$4', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonInfoContent$4$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonInfoContent$4$1', 'PersonInfoContent$4$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonalInfo', 'PersonalInfo', Lconsys_common_gwt_client_ui_comp_panel_abst_RUSimpleHelpFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonalInfo$1', 'PersonalInfo$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonalInfo$2', 'PersonalInfo$2', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonalInfo$3', 'PersonalInfo$3', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PersonalInfo$3$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PersonalInfo$3$1', 'PersonalInfo$3$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PrivateOrgContent', 'PrivateOrgContent', Lconsys_common_gwt_client_ui_comp_panel_abst_RUSimpleHelpFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PrivateOrgContent$1', 'PrivateOrgContent$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PrivateOrgContent$2', 'PrivateOrgContent$2', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PrivateOrgContent$3', 'PrivateOrgContent$3', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_PrivateOrgContent$3$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'PrivateOrgContent$3$1', 'PrivateOrgContent$3$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'ProfileImageContent', 'ProfileImageContent', Lconsys_common_gwt_client_ui_comp_panel_abst_RUSimpleFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'ProfileImageContent$1', 'ProfileImageContent$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'ProfileImageContent$2', 'ProfileImageContent$2', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'ProfileImageContent$3', 'ProfileImageContent$3', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_account_ProfileImageContent$4_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.account.', 'ProfileImageContent$4', 'ProfileImageContent$4', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_NewUserOrganizationDialog_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'NewUserOrganizationDialog', 'NewUserOrganizationDialog', Lconsys_common_gwt_client_ui_comp_CreateDialog_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgForm', 'UserOrgForm', Lconsys_common_gwt_client_ui_comp_BaseForm_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_NewUserOrganizationDialog$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'NewUserOrganizationDialog$1', 'NewUserOrganizationDialog$1', Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_NewUserOrganizationDialog$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'NewUserOrganizationDialog$2', 'NewUserOrganizationDialog$2', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationDetailForm', 'OrganizationDetailForm', Lcom_google_gwt_user_client_ui_SimplePanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationDetailForm$1', 'OrganizationDetailForm$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationDetailForm$2', 'OrganizationDetailForm$2', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationDetailForm$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationDetailForm$3', 'OrganizationDetailForm$3', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_ConsysSuggestOracle_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'ConsysSuggestOracle', 'ConsysSuggestOracle', Lcom_google_gwt_user_client_ui_SuggestOracle_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationSuggestOracle', 'OrganizationSuggestOracle', Lconsys_common_gwt_client_ui_comp_ConsysSuggestOracle_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationSuggestOracle$1', 'OrganizationSuggestOracle$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle$OrganizationSuggestion_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationSuggestOracle$OrganizationSuggestion', 'OrganizationSuggestOracle$OrganizationSuggestion', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle$CreateOrganizationSuggestion_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'OrganizationSuggestOracle$CreateOrganizationSuggestion', 'OrganizationSuggestOracle$CreateOrganizationSuggestion', Lconsys_admin_user_gwt_client_module_profile_organization_OrganizationSuggestOracle$OrganizationSuggestion_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgForm$1', 'UserOrgForm$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgForm$2', 'UserOrgForm$2', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgForm$3', 'UserOrgForm$3', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$4_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgForm$4', 'UserOrgForm$4', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm$5_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgForm$5', 'UserOrgForm$5', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgItem_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgItem', 'UserOrgItem', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgItem$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgItem$1', 'UserOrgItem$1', Lconsys_common_gwt_client_ui_comp_Remover_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgPanel_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgPanel', 'UserOrgPanel', Lconsys_common_gwt_client_ui_comp_panel_SimpleFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgPanel$OrgWrapper_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrgPanel$OrgWrapper', 'UserOrgPanel$OrgWrapper', Lcom_google_gwt_user_client_ui_FlowPanel_2_classLit), Lconsys_common_gwt_client_ui_comp_panel_abst_CRUDSimpleHelpFormPanel_2_classLit = createForClass('consys.common.gwt.client.ui.comp.panel.abst.', 'CRUDSimpleHelpFormPanel', 'CRUDSimpleHelpFormPanel', Lconsys_common_gwt_client_ui_comp_panel_abst_RUSimpleHelpFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrganizationsContent', 'UserOrganizationsContent', Lconsys_common_gwt_client_ui_comp_panel_abst_CRUDSimpleHelpFormPanel_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrganizationsContent$1', 'UserOrganizationsContent$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$2_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrganizationsContent$2', 'UserOrganizationsContent$2', Lconsys_admin_user_gwt_client_module_profile_organization_UserOrgForm_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$3_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrganizationsContent$3', 'UserOrganizationsContent$3', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$3$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrganizationsContent$3$1', 'UserOrganizationsContent$3$1', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$5_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrganizationsContent$5', 'UserOrganizationsContent$5', Ljava_lang_Object_2_classLit), Lconsys_admin_user_gwt_client_module_profile_organization_UserOrganizationsContent$5$1_2_classLit = createForClass('consys.admin.user.gwt.client.module.profile.organization.', 'UserOrganizationsContent$5$1', 'UserOrganizationsContent$5$1', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_AddressForm$1_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'AddressForm$1', 'AddressForm$1', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_AddressForm$2_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'AddressForm$2', 'AddressForm$2', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_AddressForm$3_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'AddressForm$3', 'AddressForm$3', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_ConsysSuggestOracle$1_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'ConsysSuggestOracle$1', 'ConsysSuggestOracle$1', Lcom_google_gwt_user_client_Timer_2_classLit), Lconsys_common_gwt_client_ui_comp_ConsysSuggestOracle$SuggestCallback_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'ConsysSuggestOracle$SuggestCallback', 'ConsysSuggestOracle$SuggestCallback', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_MenuBar_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'MenuBar', 'MenuBar', Lcom_google_gwt_user_client_ui_Widget_2_classLit), Lconsys_common_gwt_client_ui_comp_MenuItem_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'MenuItem', 'MenuItem', Lcom_google_gwt_user_client_ui_UIObject_2_classLit), Lconsys_common_gwt_client_ui_comp_MonthYearDatePicker_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'MonthYearDatePicker', 'MonthYearDatePicker', Lcom_google_gwt_user_client_ui_Composite_2_classLit), Lconsys_common_gwt_client_ui_comp_SuggestionBox_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'SuggestionBox', 'SuggestionBox', Lcom_google_gwt_user_client_ui_Composite_2_classLit), Lconsys_common_gwt_client_ui_comp_SuggestionBox$1_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'SuggestionBox$1', 'SuggestionBox$1', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_SuggestionBox$1TextBoxEvents_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'SuggestionBox$1TextBoxEvents', 'SuggestionBox$1TextBoxEvents', Lcom_google_gwt_event_dom_client_HandlesAllKeyEvents_2_classLit), Lconsys_common_gwt_client_ui_comp_SuggestionBox$2_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'SuggestionBox$2', 'SuggestionBox$2', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_ui_comp_SuggestionBox$SuggestionMenu_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'SuggestionBox$SuggestionMenu', 'SuggestionBox$SuggestionMenu', Lconsys_common_gwt_client_ui_comp_MenuBar_2_classLit), Lconsys_common_gwt_client_ui_comp_SuggestionBox$SuggestionMenuItem_2_classLit = createForClass('consys.common.gwt.client.ui.comp.', 'SuggestionBox$SuggestionMenuItem', 'SuggestionBox$SuggestionMenuItem', Lconsys_common_gwt_client_ui_comp_MenuItem_2_classLit), Lconsys_common_gwt_client_ui_comp_panel_abst_CRUDSimpleHelpFormPanel$1_2_classLit = createForClass('consys.common.gwt.client.ui.comp.panel.abst.', 'CRUDSimpleHelpFormPanel$1', 'CRUDSimpleHelpFormPanel$1', Ljava_lang_Object_2_classLit);
$entry(onLoad_1)();
