function unwrap(value){
  return value.jsArray;
}

_ = JSONArray.prototype;
_.getUnwrapper = function getUnwrapper(){
  return unwrap;
}
;
function unwrap_0(value){
  return value.value_0;
}

_ = JSONBoolean.prototype;
_.getUnwrapper = function getUnwrapper_0(){
  return unwrap_0;
}
;
function unwrap_1(){
  return null;
}

_ = JSONNull.prototype;
_.getUnwrapper = function getUnwrapper_1(){
  return unwrap_1;
}
;
function unwrap_2(value){
  return value.value_0;
}

_ = JSONNumber.prototype;
_.getUnwrapper = function getUnwrapper_2(){
  return unwrap_2;
}
;
function $put(this$static, key, jsonValue){
  var previous;
  if (jsEquals(key, null)) {
    throw new NullPointerException_0;
  }
  previous = $get_4(this$static, key);
  $put0(this$static, key, jsonValue);
  return previous;
}

function $put0(this$static, key, value){
  if (value) {
    var func = value.getUnwrapper();
    this$static.jsObject[key] = func(value);
  }
   else {
    delete this$static.jsObject[key];
  }
}

function JSONObject_0(){
  JSONObject_1.call(this, createObject());
}

function unwrap_3(value){
  return value.jsObject;
}

_ = JSONObject_0.prototype = JSONObject.prototype;
_.getUnwrapper = function getUnwrapper_3(){
  return unwrap_3;
}
;
function parse_1(jsonString){
  $clinit_62();
  return parseLenient(jsonString);
}

function parseLenient(jsonString){
  return parse_2(jsonString, false);
}

function unwrap_4(value){
  return value.value_0;
}

_ = JSONString.prototype;
_.getUnwrapper = function getUnwrapper_4(){
  return unwrap_4;
}
;
function $$init_231(){
}

function $runCallbacks_1(){
  var $e0, e, handler, next;
  while (isNotNull(callbacksHead_1)) {
    handler = getUncaughtExceptionHandler();
    next = callbacksHead_1;
    callbacksHead_1 = callbacksHead_1.next;
    isNull(callbacksHead_1) && (callbacksTail_1 = null);
    if (isNull(handler)) {
      $onSuccess_55(next.callback);
    }
     else {
      try {
        $onSuccess_55(next.callback);
      }
       catch ($e0) {
        $e0 = caught_0($e0);
        if (instanceOf($e0, 25)) {
          e = $e0;
          handler.onUncaughtException(e);
        }
         else 
          throw $e0;
      }
    }
  }
}

function AsyncLoader3_0(){
  Object_1.call(this);
  $$init_231();
}

function onLoad_2(){
  instance_6 = new AsyncLoader3_0;
  $fragmentHasLoaded(($clinit_11() , BROWSER_LOADER), 3);
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks3', 'begin');
  instance_6.runCallbacks();
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks3', 'end');
}

function AsyncLoader3(){
}

_ = AsyncLoader3_0.prototype = AsyncLoader3.prototype = new Object_0;
_.getClass$ = function getClass_232(){
  return Lcom_google_gwt_lang_asyncloaders_AsyncLoader3_2_classLit;
}
;
_.runCallbacks = function runCallbacks_1(){
  $runCallbacks_1();
}
;
_.castableTypeMap$ = {};
function $getFrom_0(this$static){
  return this$static.from;
}

function $getMessage_2(this$static){
  return this$static.message_0;
}

function $getTo_0(this$static){
  return this$static.to;
}

function $$init_780(){
}

function BadResponseStatusCodeException_0(message){
  Exception_1.call(this, message);
  $$init_780();
}

function BadResponseStatusCodeException(){
}

_ = BadResponseStatusCodeException_0.prototype = BadResponseStatusCodeException.prototype = new Exception;
_.getClass$ = function getClass_781(){
  return Lconsys_common_gwt_client_messaging_exception_BadResponseStatusCodeException_2_classLit;
}
;
_.castableTypeMap$ = {12:1, 25:1, 57:1, 429:1};
function $$init_782(){
}

function ApiMessage_0(api){
  Object_1.call(this);
  $$init_782();
  this , api;
}

function ApiMessage(){
}

_ = ApiMessage_0.prototype = ApiMessage.prototype = new Object_0;
_.getClass$ = function getClass_783(){
  return Lconsys_common_gwt_client_messaging_module_ApiMessage_2_classLit;
}
;
_.castableTypeMap$ = {};
function $$init_783(){
}

function $deleteWallMessage(this$static, uuid, timestamp, callback){
  var request;
  request = new JSONObject_0;
  $put(request, 't', new JSONString_0('delWM'));
  $put(request, 'e', new JSONString_0(uuid));
  $put(request, 'time', new JSONNumber_0(toDouble(timestamp)));
  return $send_2(this$static.api, request, this$static.url, callback);
}

function $loadWall(this$static, uuid, number, offset, callback){
  var request;
  request = new JSONObject_0;
  $put(request, 't', new JSONString_0('lw'));
  $put(request, 'e', new JSONString_0(uuid));
  $put(request, 'num', new JSONNumber_0(number));
  $put(request, 'offs', new JSONNumber_0(toDouble(offset)));
  return $send_2(this$static.api, request, this$static.url, callback);
}

function $sendWallMessage(this$static, msg, callback){
  var request;
  request = new JSONObject_0;
  $put(request, 't', new JSONString_0('sendWM'));
  $put(request, 'e', new JSONString_0($getFrom_0(msg)));
  $put(request, 'to', new JSONString_0($getTo_0(msg)));
  $put(request, 'msg', new JSONString_0($getMessage_2(msg)));
  return $send_2(this$static.api, request, this$static.url, callback);
}

function ApiWall_0(api){
  Object_1.call(this);
  $$init_783();
  this.api = api;
}

function ApiWall(){
}

_ = ApiWall_0.prototype = ApiWall.prototype = new Object_0;
_.deleteWallMessage = function deleteWallMessage(uuid, timestamp, callback){
  return $deleteWallMessage(this, uuid, timestamp, callback);
}
;
_.getClass$ = function getClass_784(){
  return Lconsys_common_gwt_client_messaging_module_ApiWall_2_classLit;
}
;
_.loadWall = function loadWall(uuid, number, offset, callback){
  return $loadWall(this, uuid, number, offset, callback);
}
;
_.sendWallMessage = function sendWallMessage(msg, callback){
  return $sendWallMessage(this, msg, callback);
}
;
_.castableTypeMap$ = {};
_.api = null;
_.url = null;
function $$init_784(){
}

function $getApiWall(this$static){
  return this$static.apiWall;
}

function $send_2(this$static, jsonRequest, url, callback){
  var $e0, ex, rb;
  if (jsEquals(url, null)) {
    $error_0(logger_9, 'Messaging URL not set');
    return false;
  }
  try {
    rb = new RequestBuilder_0(($clinit_46() , POST), url);
    $sendRequest(rb, $toString_4(jsonRequest), new MessagingApi$2_0(this$static, callback));
  }
   catch ($e0) {
    $e0 = caught_0($e0);
    if (instanceOf($e0, 105)) {
      ex = $e0;
      $error_1(logger_9, 'request send failed', ex);
      return false;
    }
     else 
      throw $e0;
  }
  return true;
}

function MessagingApi_0(){
  $clinit_168();
  Object_1.call(this);
  $$init_784();
  this.apiMessage = new ApiMessage_0(this);
  this.apiWall = new ApiWall_0(this);
}

function MessagingApi(){
}

_ = MessagingApi_0.prototype = MessagingApi.prototype = new Object_0;
_.getApiWall = function getApiWall(){
  return $getApiWall(this);
}
;
_.getClass$ = function getClass_785(){
  return Lconsys_common_gwt_client_messaging_module_MessagingApi_2_classLit;
}
;
_.castableTypeMap$ = {428:1};
_.apiMessage = null;
_.apiWall = null;
var instance_13 = null;
function $onSuccess_55(this$static){
  isNull(($clinit_168() , instance_13)) && ($clinit_168() , instance_13 = new MessagingApi_0);
  this$static.val$asyncModule.onSuccess(($clinit_168() , instance_13));
}

function $$init_786(){
}

function MessagingApi$2_0(this$0, val$callback){
  this , this$0;
  this.val$callback = val$callback;
  Object_1.call(this);
  $$init_786();
}

function MessagingApi$2(){
}

_ = MessagingApi$2_0.prototype = MessagingApi$2.prototype = new Object_0;
_.getClass$ = function getClass_787(){
  return Lconsys_common_gwt_client_messaging_module_MessagingApi$2_2_classLit;
}
;
_.onError_0 = function onError_0(request, exception){
  $error_1(($clinit_168() , logger_9), 'request send failed', exception);
  this.val$callback.onFailure(exception);
}
;
_.onResponseReceived = function onResponseReceived_0(request, response){
  var object, returnCode;
  returnCode = response.getStatusCode();
  if (returnCode == 200) {
    object = $getJavaScriptObject(parse_1(response.getText()).isObject());
    this.val$callback.onSuccess(object);
  }
   else {
    this.val$callback.onFailure(new BadResponseStatusCodeException_0(returnCode + ':' + response.getStatusText()));
  }
}
;
_.castableTypeMap$ = {};
_.val$callback = null;
var Lcom_google_gwt_lang_asyncloaders_AsyncLoader3_2_classLit = createForClass('com.google.gwt.lang.asyncloaders.', 'AsyncLoader3', 'AsyncLoader3', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_messaging_exception_BadResponseStatusCodeException_2_classLit = createForClass('consys.common.gwt.client.messaging.exception.', 'BadResponseStatusCodeException', 'BadResponseStatusCodeException', Ljava_lang_Exception_2_classLit), Lconsys_common_gwt_client_messaging_module_ApiMessage_2_classLit = createForClass('consys.common.gwt.client.messaging.module.', 'ApiMessage', 'ApiMessage', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_messaging_module_ApiWall_2_classLit = createForClass('consys.common.gwt.client.messaging.module.', 'ApiWall', 'ApiWall', Ljava_lang_Object_2_classLit), Lconsys_common_gwt_client_messaging_module_MessagingApi$2_2_classLit = createForClass('consys.common.gwt.client.messaging.module.', 'MessagingApi$2', 'MessagingApi$2', Ljava_lang_Object_2_classLit);
$entry(onLoad_2)();
