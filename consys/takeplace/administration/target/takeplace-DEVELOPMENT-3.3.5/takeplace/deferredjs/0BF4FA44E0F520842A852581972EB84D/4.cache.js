function $$init_237(){
}

function $runCallbacks_2(){
  var $e0, e, handler;
  while (isNotNull(callbacksHead_2)) {
    handler = getUncaughtExceptionHandler();
    callbacksHead_2;
    callbacksHead_2 = null.nullField;
    isNull(callbacksHead_2) && null;
    if (isNull(handler)) {
      $onSuccess_219();
    }
     else {
      try {
        $onSuccess_219();
      }
       catch ($e0) {
        $e0 = caught_0($e0);
        if (instanceOf($e0, 25)) {
          e = $e0;
          handler.onUncaughtException(e);
        }
         else 
          throw $e0;
      }
    }
  }
}

function AsyncLoader4_0(){
  Object_1.call(this);
  $$init_237();
}

function onLoad_3(){
  instance_7 = new AsyncLoader4_0;
  $fragmentHasLoaded(($clinit_11() , BROWSER_LOADER), 4);
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks4', 'begin');
  instance_7.runCallbacks();
  $logEventProgress(($clinit_11() , BROWSER_LOADER), 'runCallbacks4', 'end');
}

function AsyncLoader4(){
}

_ = AsyncLoader4_0.prototype = AsyncLoader4.prototype = new Object_0;
_.getClass$ = function getClass_238(){
  return Lcom_google_gwt_lang_asyncloaders_AsyncLoader4_2_classLit;
}
;
_.runCallbacks = function runCallbacks_2(){
  $runCallbacks_2();
}
;
_.castableTypeMap$ = {};
var callbacksHead_2 = null, instance_7 = null;
var instance_24 = null;
function $onSuccess_219(){
  isNull(($clinit_6() , instance_24)) && ($clinit_6() , instance_24 = new RegistrationSettingsBundle_0(true));
  null.nullField.onSuccess(($clinit_6() , instance_24));
}

var Lcom_google_gwt_lang_asyncloaders_AsyncLoader4_2_classLit = createForClass('com.google.gwt.lang.asyncloaders.', 'AsyncLoader4', 'AsyncLoader4', Ljava_lang_Object_2_classLit);
$entry(onLoad_3)();
