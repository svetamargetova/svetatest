<%@page contentType="text/html;charset=UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Takeplace password reset</title>
    </head>
    <body>
        <div><img src="http://static.takeplace.eu/topTakeplace.png"/></div>
        <h1>Enter new password</h1>
        <form method="post" action="user-password-reset.do" enctype="multipart/form-data">
            <div><input type="password" name="password"></div>
            <div><input type="hidden" name="key" value="${key}"></div>
            <div><input type="submit" name="change" value="Change password"></div>
        </form>
    </body>
</html>
