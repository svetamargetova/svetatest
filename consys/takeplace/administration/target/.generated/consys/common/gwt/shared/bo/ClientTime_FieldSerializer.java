package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientTime_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getHours(consys.common.gwt.shared.bo.ClientTime instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientTime::hours;
  }-*/;
  
  private static native void setHours(consys.common.gwt.shared.bo.ClientTime instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientTime::hours = value;
  }-*/;
  
  private static native int getMinutes(consys.common.gwt.shared.bo.ClientTime instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientTime::minutes;
  }-*/;
  
  private static native void setMinutes(consys.common.gwt.shared.bo.ClientTime instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientTime::minutes = value;
  }-*/;
  
  private static native int getTime(consys.common.gwt.shared.bo.ClientTime instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientTime::time;
  }-*/;
  
  private static native void setTime(consys.common.gwt.shared.bo.ClientTime instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientTime::time = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientTime instance) throws SerializationException {
    setHours(instance, streamReader.readInt());
    setMinutes(instance, streamReader.readInt());
    setTime(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientTime instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientTime();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientTime instance) throws SerializationException {
    streamWriter.writeInt(getHours(instance));
    streamWriter.writeInt(getMinutes(instance));
    streamWriter.writeInt(getTime(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientTime_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientTime_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientTime)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientTime_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientTime)object);
  }
  
}
