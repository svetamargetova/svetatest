package consys.common.gwt.client.ui.comp.input;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class InputResources_gecko1_8_cs_InlineClientBundleGenerator implements consys.common.gwt.client.ui.comp.input.InputResources {
  private static InputResources_gecko1_8_cs_InlineClientBundleGenerator _instance0 = new InputResources_gecko1_8_cs_InlineClientBundleGenerator();
  private void consysSelectBoxArrowInitializer() {
    consysSelectBoxArrow = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "consysSelectBoxArrow",
      externalImage,
      0, 0, 9, 6, false, false
    );
  }
  private static class consysSelectBoxArrowInitializer {
    static {
      _instance0.consysSelectBoxArrowInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return consysSelectBoxArrow;
    }
  }
  public com.google.gwt.resources.client.ImageResource consysSelectBoxArrow() {
    return consysSelectBoxArrowInitializer.get();
  }
  private void cssInitializer() {
    css = new consys.common.gwt.client.ui.comp.input.InputCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GHAOYQ1MJ textarea,.GHAOYQ1OJ input{outline:" + ("none")  + ";}.GHAOYQ1MJ .GHAOYQ1NJ{margin-top:" + ("3px")  + ";margin-left:" + ("5px")  + ";float:" + ("left")  + ";}.GHAOYQ1MJ .GHAOYQ1NJ .gwt-Label{font-size:" + ("10px")  + ";float:" + ("right")  + ";}.GHAOYQ1HK{height:" + ("21px")  + ";padding-top:" + ("3px")  + ";font-style:" + ("italic")  + ";}.GHAOYQ1GK{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_GREEN())  + ";}.GHAOYQ1CK{color:") + ((consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_RED())  + ";font-size:" + ("11px")  + ";}.GHAOYQ1OJ{position:" + ("relative")  + ";right:" + ("-6px")  + ";top:" + ("-7px")  + ";}.GHAOYQ1BK{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_GRAY())  + ";}.GHAOYQ1HJ{overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}.GHAOYQ1HJ .GHAOYQ1FK{position:" + ("absolute")  + ";right:" + ("0")  + ";top:" + ("0") ) + (";width:" + ("0")  + ";height:" + ("0")  + ";z-index:" + ("1")  + ";}.GHAOYQ1HJ .GHAOYQ1EK{width:" + ("100%")  + ";position:" + ("absolute")  + ";top:" + ("0")  + ";right:" + ("0")  + ";}.GHAOYQ1HJ .GHAOYQ1EK .GHAOYQ1OJ{position:" + ("relative")  + ";right:" + ("-4px")  + ";top:" + ("-4px")  + ";float:") + (("right")  + ";}.GHAOYQ1HJ .GHAOYQ1EK .GHAOYQ1AH{float:" + ("right")  + ";}.GHAOYQ1IJ,.GHAOYQ1JJ{position:" + ("relative")  + ";right:" + ("-6px")  + ";top:" + ("-7px")  + ";}.GHAOYQ1IJ .GHAOYQ1IK input,.GHAOYQ1JJ input{outline:" + ("none")  + ";border:" + ("none")  + ";}.GHAOYQ1JJ .GHAOYQ1FJ{float:" + ("left")  + ";width:" + ("10px")  + ";margin-top:" + ("4px")  + ";}")) : ((".GHAOYQ1MJ textarea,.GHAOYQ1OJ input{outline:" + ("none")  + ";}.GHAOYQ1MJ .GHAOYQ1NJ{margin-top:" + ("3px")  + ";margin-right:" + ("5px")  + ";float:" + ("right")  + ";}.GHAOYQ1MJ .GHAOYQ1NJ .gwt-Label{font-size:" + ("10px")  + ";float:" + ("left")  + ";}.GHAOYQ1HK{height:" + ("21px")  + ";padding-top:" + ("3px")  + ";font-style:" + ("italic")  + ";}.GHAOYQ1GK{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_GREEN())  + ";}.GHAOYQ1CK{color:") + ((consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_RED())  + ";font-size:" + ("11px")  + ";}.GHAOYQ1OJ{position:" + ("relative")  + ";left:" + ("-6px")  + ";top:" + ("-7px")  + ";}.GHAOYQ1BK{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_GRAY())  + ";}.GHAOYQ1HJ{overflow:" + ("hidden")  + ";position:" + ("relative")  + ";}.GHAOYQ1HJ .GHAOYQ1FK{position:" + ("absolute")  + ";left:" + ("0")  + ";top:" + ("0") ) + (";width:" + ("0")  + ";height:" + ("0")  + ";z-index:" + ("1")  + ";}.GHAOYQ1HJ .GHAOYQ1EK{width:" + ("100%")  + ";position:" + ("absolute")  + ";top:" + ("0")  + ";left:" + ("0")  + ";}.GHAOYQ1HJ .GHAOYQ1EK .GHAOYQ1OJ{position:" + ("relative")  + ";left:" + ("-4px")  + ";top:" + ("-4px")  + ";float:") + (("left")  + ";}.GHAOYQ1HJ .GHAOYQ1EK .GHAOYQ1AH{float:" + ("left")  + ";}.GHAOYQ1IJ,.GHAOYQ1JJ{position:" + ("relative")  + ";left:" + ("-6px")  + ";top:" + ("-7px")  + ";}.GHAOYQ1IJ .GHAOYQ1IK input,.GHAOYQ1JJ input{outline:" + ("none")  + ";border:" + ("none")  + ";}.GHAOYQ1JJ .GHAOYQ1FJ{float:" + ("right")  + ";width:" + ("10px")  + ";margin-top:" + ("4px")  + ";}"));
      }
      public java.lang.String arrowPanel(){
        return "GHAOYQ1FJ";
      }
      public java.lang.String consysAutoFileUpload(){
        return "GHAOYQ1GJ";
      }
      public java.lang.String consysFileUpload(){
        return "GHAOYQ1HJ";
      }
      public java.lang.String consysPasswordBox(){
        return "GHAOYQ1IJ";
      }
      public java.lang.String consysSelectBox(){
        return "GHAOYQ1JJ";
      }
      public java.lang.String consysSelectBoxInput(){
        return "GHAOYQ1KJ";
      }
      public java.lang.String consysSelectBoxLabel(){
        return "GHAOYQ1LJ";
      }
      public java.lang.String consysTextArea(){
        return "GHAOYQ1MJ";
      }
      public java.lang.String consysTextAreaInfoPanel(){
        return "GHAOYQ1NJ";
      }
      public java.lang.String consysTextBox(){
        return "GHAOYQ1OJ";
      }
      public java.lang.String consysTextBoxCenterWrapper(){
        return "GHAOYQ1PJ";
      }
      public java.lang.String consysTextBoxInput(){
        return "GHAOYQ1AK";
      }
      public java.lang.String disabled(){
        return "GHAOYQ1BK";
      }
      public java.lang.String error(){
        return "GHAOYQ1CK";
      }
      public java.lang.String infoLabel(){
        return "GHAOYQ1DK";
      }
      public java.lang.String uploadFaceWrapper(){
        return "GHAOYQ1EK";
      }
      public java.lang.String uploadWrapper(){
        return "GHAOYQ1FK";
      }
      public java.lang.String uploaded(){
        return "GHAOYQ1GK";
      }
      public java.lang.String uploading(){
        return "GHAOYQ1HK";
      }
      public java.lang.String wrapper(){
        return "GHAOYQ1IK";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.common.gwt.client.ui.comp.input.InputCss get() {
      return css;
    }
  }
  public consys.common.gwt.client.ui.comp.input.InputCss css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAGCAYAAAARx7TFAAAATUlEQVR42oWK0QnAQAhDHdcdxH8ncRyH0QHS5uAKLaV9IJLkiari78TMPgXuMjPIzFeBPXfBSVUhIm4CM3uyJNLdcPcl8DNvLmlD6ckBNmmcZ+nuJCcAAAAASUVORK5CYII=";
  private static com.google.gwt.resources.client.ImageResource consysSelectBoxArrow;
  private static consys.common.gwt.client.ui.comp.input.InputCss css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      consysSelectBoxArrow(), 
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("consysSelectBoxArrow", consysSelectBoxArrow());
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'consysSelectBoxArrow': return this.@consys.common.gwt.client.ui.comp.input.InputResources::consysSelectBoxArrow()();
      case 'css': return this.@consys.common.gwt.client.ui.comp.input.InputResources::css()();
    }
    return null;
  }-*/;
}
