package consys.common.gwt.client.ui.comp.action;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ActionResources_gecko1_8_default_InlineClientBundleGenerator implements consys.common.gwt.client.ui.comp.action.ActionResources {
  private static ActionResources_gecko1_8_default_InlineClientBundleGenerator _instance0 = new ActionResources_gecko1_8_default_InlineClientBundleGenerator();
  private void actionImageBigCenterInitializer() {
    actionImageBigCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigCenter",
      externalImage,
      0, 0, 1, 40, false, false
    );
  }
  private static class actionImageBigCenterInitializer {
    static {
      _instance0.actionImageBigCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigCenter() {
    return actionImageBigCenterInitializer.get();
  }
  private void actionImageBigClearCenterInitializer() {
    actionImageBigClearCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigClearCenter",
      externalImage0,
      0, 0, 1, 38, false, false
    );
  }
  private static class actionImageBigClearCenterInitializer {
    static {
      _instance0.actionImageBigClearCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigClearCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigClearCenter() {
    return actionImageBigClearCenterInitializer.get();
  }
  private void actionImageBigClearLeftInitializer() {
    actionImageBigClearLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigClearLeft",
      externalImage1,
      0, 0, 5, 38, false, false
    );
  }
  private static class actionImageBigClearLeftInitializer {
    static {
      _instance0.actionImageBigClearLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigClearLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigClearLeft() {
    return actionImageBigClearLeftInitializer.get();
  }
  private void actionImageBigClearRightInitializer() {
    actionImageBigClearRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigClearRight",
      externalImage2,
      0, 0, 5, 38, false, false
    );
  }
  private static class actionImageBigClearRightInitializer {
    static {
      _instance0.actionImageBigClearRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigClearRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigClearRight() {
    return actionImageBigClearRightInitializer.get();
  }
  private void actionImageBigDisabledCenterInitializer() {
    actionImageBigDisabledCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigDisabledCenter",
      externalImage3,
      0, 0, 1, 40, false, false
    );
  }
  private static class actionImageBigDisabledCenterInitializer {
    static {
      _instance0.actionImageBigDisabledCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigDisabledCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigDisabledCenter() {
    return actionImageBigDisabledCenterInitializer.get();
  }
  private void actionImageBigDisabledLeftInitializer() {
    actionImageBigDisabledLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigDisabledLeft",
      externalImage4,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigDisabledLeftInitializer {
    static {
      _instance0.actionImageBigDisabledLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigDisabledLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigDisabledLeft() {
    return actionImageBigDisabledLeftInitializer.get();
  }
  private void actionImageBigDisabledRightInitializer() {
    actionImageBigDisabledRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigDisabledRight",
      externalImage5,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigDisabledRightInitializer {
    static {
      _instance0.actionImageBigDisabledRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigDisabledRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigDisabledRight() {
    return actionImageBigDisabledRightInitializer.get();
  }
  private void actionImageBigGreenCenterInitializer() {
    actionImageBigGreenCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigGreenCenter",
      externalImage6,
      0, 0, 1, 40, false, false
    );
  }
  private static class actionImageBigGreenCenterInitializer {
    static {
      _instance0.actionImageBigGreenCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigGreenCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigGreenCenter() {
    return actionImageBigGreenCenterInitializer.get();
  }
  private void actionImageBigGreenLeftInitializer() {
    actionImageBigGreenLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigGreenLeft",
      externalImage7,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigGreenLeftInitializer {
    static {
      _instance0.actionImageBigGreenLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigGreenLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigGreenLeft() {
    return actionImageBigGreenLeftInitializer.get();
  }
  private void actionImageBigGreenRightInitializer() {
    actionImageBigGreenRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigGreenRight",
      externalImage8,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigGreenRightInitializer {
    static {
      _instance0.actionImageBigGreenRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigGreenRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigGreenRight() {
    return actionImageBigGreenRightInitializer.get();
  }
  private void actionImageBigLeftInitializer() {
    actionImageBigLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigLeft",
      externalImage9,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigLeftInitializer {
    static {
      _instance0.actionImageBigLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigLeft() {
    return actionImageBigLeftInitializer.get();
  }
  private void actionImageBigRedCenterInitializer() {
    actionImageBigRedCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigRedCenter",
      externalImage10,
      0, 0, 1, 40, false, false
    );
  }
  private static class actionImageBigRedCenterInitializer {
    static {
      _instance0.actionImageBigRedCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigRedCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigRedCenter() {
    return actionImageBigRedCenterInitializer.get();
  }
  private void actionImageBigRedLeftInitializer() {
    actionImageBigRedLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigRedLeft",
      externalImage11,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigRedLeftInitializer {
    static {
      _instance0.actionImageBigRedLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigRedLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigRedLeft() {
    return actionImageBigRedLeftInitializer.get();
  }
  private void actionImageBigRedRightInitializer() {
    actionImageBigRedRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigRedRight",
      externalImage12,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigRedRightInitializer {
    static {
      _instance0.actionImageBigRedRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigRedRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigRedRight() {
    return actionImageBigRedRightInitializer.get();
  }
  private void actionImageBigRightInitializer() {
    actionImageBigRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageBigRight",
      externalImage13,
      0, 0, 5, 40, false, false
    );
  }
  private static class actionImageBigRightInitializer {
    static {
      _instance0.actionImageBigRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageBigRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageBigRight() {
    return actionImageBigRightInitializer.get();
  }
  private void actionImageCenterInitializer() {
    actionImageCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageCenter",
      externalImage14,
      0, 0, 1, 23, false, false
    );
  }
  private static class actionImageCenterInitializer {
    static {
      _instance0.actionImageCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageCenter() {
    return actionImageCenterInitializer.get();
  }
  private void actionImageDisabledCenterInitializer() {
    actionImageDisabledCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageDisabledCenter",
      externalImage15,
      0, 0, 1, 23, false, false
    );
  }
  private static class actionImageDisabledCenterInitializer {
    static {
      _instance0.actionImageDisabledCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageDisabledCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageDisabledCenter() {
    return actionImageDisabledCenterInitializer.get();
  }
  private void actionImageDisabledLeftInitializer() {
    actionImageDisabledLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageDisabledLeft",
      externalImage16,
      0, 0, 3, 23, false, false
    );
  }
  private static class actionImageDisabledLeftInitializer {
    static {
      _instance0.actionImageDisabledLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageDisabledLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageDisabledLeft() {
    return actionImageDisabledLeftInitializer.get();
  }
  private void actionImageDisabledRightInitializer() {
    actionImageDisabledRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageDisabledRight",
      externalImage17,
      0, 0, 3, 23, false, false
    );
  }
  private static class actionImageDisabledRightInitializer {
    static {
      _instance0.actionImageDisabledRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageDisabledRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageDisabledRight() {
    return actionImageDisabledRightInitializer.get();
  }
  private void actionImageLeftInitializer() {
    actionImageLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageLeft",
      externalImage18,
      0, 0, 3, 23, false, false
    );
  }
  private static class actionImageLeftInitializer {
    static {
      _instance0.actionImageLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageLeft() {
    return actionImageLeftInitializer.get();
  }
  private void actionImageRedCenterInitializer() {
    actionImageRedCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageRedCenter",
      externalImage19,
      0, 0, 1, 23, false, false
    );
  }
  private static class actionImageRedCenterInitializer {
    static {
      _instance0.actionImageRedCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageRedCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageRedCenter() {
    return actionImageRedCenterInitializer.get();
  }
  private void actionImageRedLeftInitializer() {
    actionImageRedLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageRedLeft",
      externalImage20,
      0, 0, 3, 23, false, false
    );
  }
  private static class actionImageRedLeftInitializer {
    static {
      _instance0.actionImageRedLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageRedLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageRedLeft() {
    return actionImageRedLeftInitializer.get();
  }
  private void actionImageRedRightInitializer() {
    actionImageRedRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageRedRight",
      externalImage21,
      0, 0, 3, 23, false, false
    );
  }
  private static class actionImageRedRightInitializer {
    static {
      _instance0.actionImageRedRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageRedRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageRedRight() {
    return actionImageRedRightInitializer.get();
  }
  private void actionImageRightInitializer() {
    actionImageRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "actionImageRight",
      externalImage22,
      0, 0, 3, 23, false, false
    );
  }
  private static class actionImageRightInitializer {
    static {
      _instance0.actionImageRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return actionImageRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource actionImageRight() {
    return actionImageRightInitializer.get();
  }
  private void iconBigConfirmInitializer() {
    iconBigConfirm = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "iconBigConfirm",
      externalImage23,
      0, 0, 22, 15, false, false
    );
  }
  private static class iconBigConfirmInitializer {
    static {
      _instance0.iconBigConfirmInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return iconBigConfirm;
    }
  }
  public com.google.gwt.resources.client.ImageResource iconBigConfirm() {
    return iconBigConfirmInitializer.get();
  }
  private void iconConfirmInitializer() {
    iconConfirm = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "iconConfirm",
      externalImage24,
      0, 0, 13, 9, false, false
    );
  }
  private static class iconConfirmInitializer {
    static {
      _instance0.iconConfirmInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return iconConfirm;
    }
  }
  public com.google.gwt.resources.client.ImageResource iconConfirm() {
    return iconConfirmInitializer.get();
  }
  private void cssInitializer() {
    css = new consys.common.gwt.client.ui.comp.action.ActionCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GHAOYQ1DH{cursor:" + ("pointer")  + ";float:" + ("right")  + ";}.GHAOYQ1EH{float:" + ("right")  + ";width:" + ("5px")  + ";height:" + ("40px")  + ";}.GHAOYQ1CH{float:" + ("right")  + ";height:" + ("35px")  + ";padding:" + ("5px"+ " " +"22px"+ " " +"0"+ " " +"22px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1LH .GHAOYQ1CH{padding:" + ("9px"+ " " +"22px"+ " " +"0"+ " " +"22px")  + ";}.GHAOYQ1CH .gwt-Label{color:") + (("#fff")  + ";font-size:" + ("22px")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1LH .GHAOYQ1CH .gwt-Label{color:" + ("#1a809a")  + ";font-size:" + ("16px")  + ";font-weight:" + ("normal")  + ";}.GHAOYQ1FH{float:" + ("right")  + ";width:" + ("5px")  + ";height:" + ("40px")  + ";}.GHAOYQ1MH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1MH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px")  + ";}.GHAOYQ1MH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1NH .GHAOYQ1EH{height:") + (((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1NH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px")  + ";}.GHAOYQ1NH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1KH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1KH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px")  + ";}.GHAOYQ1KH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getHeight() + "px")  + ";width:") + (((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1LH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1LH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px") ) + (";}.GHAOYQ1LH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1OH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getTop() + "px  no-repeat")  + ";cursor:" + ("default")  + ";}.GHAOYQ1OH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getTop() + "px  repeat-x")  + ";cursor:" + ("default")  + ";height:" + ("35px")  + ";}.GHAOYQ1OH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getTop() + "px  no-repeat")  + ";cursor:" + ("default")  + ";}.GHAOYQ1HH{cursor:" + ("pointer")  + ";float:" + ("right") ) + (";}.GHAOYQ1IH{float:" + ("right")  + ";width:" + ("3px")  + ";height:" + ("23px")  + ";}.GHAOYQ1GH{float:" + ("right")  + ";height:" + ("19px")  + ";padding:" + ("4px"+ " " +"9px"+ " " +"0"+ " " +"9px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1GI .GHAOYQ1GH{padding:" + ("4px"+ " " +"4px"+ " " +"0"+ " " +"9px")  + ";}.GHAOYQ1GH .gwt-Label{color:" + ("#fff")  + ";font-size:" + ("12px")  + ";font-weight:") + (("bold")  + ";}.GHAOYQ1DI .gwt-Label{float:" + ("right")  + ";}.GHAOYQ1JH{float:" + ("right")  + ";width:" + ("3px")  + ";height:" + ("23px")  + ";}.GHAOYQ1PH .GHAOYQ1IH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1PH .GHAOYQ1GH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getHeight() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getTop() + "px  repeat-x")  + ";height:" + ("19px")  + ";}.GHAOYQ1PH .GHAOYQ1JH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1OH .GHAOYQ1IH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getTop() + "px  no-repeat")  + ";cursor:") + (("default")  + ";}.GHAOYQ1OH .GHAOYQ1GH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getTop() + "px  repeat-x")  + ";cursor:" + ("default")  + ";height:" + ("19px")  + ";}.GHAOYQ1OH .GHAOYQ1JH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getTop() + "px  no-repeat")  + ";cursor:" + ("default") ) + (";}.GHAOYQ1FI .GHAOYQ1IH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1FI .GHAOYQ1GH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getTop() + "px  repeat-x")  + ";height:" + ("19px")  + ";}.GHAOYQ1FI .GHAOYQ1JH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1AI{float:" + ("right")  + ";margin-left:" + ("8px")  + ";}.GHAOYQ1DI .GHAOYQ1CI{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getTop() + "px  no-repeat")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1BI .GHAOYQ1CI{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1EI .gwt-Label{text-decoration:" + ("underline")  + ";}")) : ((".GHAOYQ1DH{cursor:" + ("pointer")  + ";float:" + ("left")  + ";}.GHAOYQ1EH{float:" + ("left")  + ";width:" + ("5px")  + ";height:" + ("40px")  + ";}.GHAOYQ1CH{float:" + ("left")  + ";height:" + ("35px")  + ";padding:" + ("5px"+ " " +"22px"+ " " +"0"+ " " +"22px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1LH .GHAOYQ1CH{padding:" + ("9px"+ " " +"22px"+ " " +"0"+ " " +"22px")  + ";}.GHAOYQ1CH .gwt-Label{color:") + (("#fff")  + ";font-size:" + ("22px")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1LH .GHAOYQ1CH .gwt-Label{color:" + ("#1a809a")  + ";font-size:" + ("16px")  + ";font-weight:" + ("normal")  + ";}.GHAOYQ1FH{float:" + ("left")  + ";width:" + ("5px")  + ";height:" + ("40px")  + ";}.GHAOYQ1MH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1MH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px")  + ";}.GHAOYQ1MH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigGreenRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1NH .GHAOYQ1EH{height:") + (((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1NH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px")  + ";}.GHAOYQ1NH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRedRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1KH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1KH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px")  + ";}.GHAOYQ1KH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getHeight() + "px")  + ";width:") + (((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1LH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1LH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearCenter()).getTop() + "px  repeat-x")  + ";height:" + ("35px") ) + (";}.GHAOYQ1LH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigClearRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1OH .GHAOYQ1EH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledLeft()).getTop() + "px  no-repeat")  + ";cursor:" + ("default")  + ";}.GHAOYQ1OH .GHAOYQ1CH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledCenter()).getTop() + "px  repeat-x")  + ";cursor:" + ("default")  + ";height:" + ("35px")  + ";}.GHAOYQ1OH .GHAOYQ1FH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageBigDisabledRight()).getTop() + "px  no-repeat")  + ";cursor:" + ("default")  + ";}.GHAOYQ1HH{cursor:" + ("pointer")  + ";float:" + ("left") ) + (";}.GHAOYQ1IH{float:" + ("left")  + ";width:" + ("3px")  + ";height:" + ("23px")  + ";}.GHAOYQ1GH{float:" + ("left")  + ";height:" + ("19px")  + ";padding:" + ("4px"+ " " +"9px"+ " " +"0"+ " " +"9px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1GI .GHAOYQ1GH{padding:" + ("4px"+ " " +"9px"+ " " +"0"+ " " +"4px")  + ";}.GHAOYQ1GH .gwt-Label{color:" + ("#fff")  + ";font-size:" + ("12px")  + ";font-weight:") + (("bold")  + ";}.GHAOYQ1DI .gwt-Label{float:" + ("left")  + ";}.GHAOYQ1JH{float:" + ("left")  + ";width:" + ("3px")  + ";height:" + ("23px")  + ";}.GHAOYQ1PH .GHAOYQ1IH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1PH .GHAOYQ1GH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getHeight() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageCenter()).getTop() + "px  repeat-x")  + ";height:" + ("19px")  + ";}.GHAOYQ1PH .GHAOYQ1JH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1OH .GHAOYQ1IH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledLeft()).getTop() + "px  no-repeat")  + ";cursor:") + (("default")  + ";}.GHAOYQ1OH .GHAOYQ1GH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledCenter()).getTop() + "px  repeat-x")  + ";cursor:" + ("default")  + ";height:" + ("19px")  + ";}.GHAOYQ1OH .GHAOYQ1JH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageDisabledRight()).getTop() + "px  no-repeat")  + ";cursor:" + ("default") ) + (";}.GHAOYQ1FI .GHAOYQ1IH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedLeft()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1FI .GHAOYQ1GH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedCenter()).getTop() + "px  repeat-x")  + ";height:" + ("19px")  + ";}.GHAOYQ1FI .GHAOYQ1JH{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getWidth() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.actionImageRedRight()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1AI{float:" + ("left")  + ";margin-right:" + ("8px")  + ";}.GHAOYQ1DI .GHAOYQ1CI{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconConfirm()).getTop() + "px  no-repeat")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1BI .GHAOYQ1CI{height:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getHeight() + "px")  + ";width:" + ((ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getURL() + "\") -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getLeft() + "px -" + (ActionResources_gecko1_8_default_InlineClientBundleGenerator.this.iconBigConfirm()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1EI .gwt-Label{text-decoration:" + ("underline")  + ";}"));
      }
      public java.lang.String actionImage(){
        return "GHAOYQ1AH";
      }
      public java.lang.String actionImageBig(){
        return "GHAOYQ1BH";
      }
      public java.lang.String actionImageBigCenter(){
        return "GHAOYQ1CH";
      }
      public java.lang.String actionImageBigContent(){
        return "GHAOYQ1DH";
      }
      public java.lang.String actionImageBigLeft(){
        return "GHAOYQ1EH";
      }
      public java.lang.String actionImageBigRight(){
        return "GHAOYQ1FH";
      }
      public java.lang.String actionImageCenter(){
        return "GHAOYQ1GH";
      }
      public java.lang.String actionImageContent(){
        return "GHAOYQ1HH";
      }
      public java.lang.String actionImageLeft(){
        return "GHAOYQ1IH";
      }
      public java.lang.String actionImageRight(){
        return "GHAOYQ1JH";
      }
      public java.lang.String bigBlue(){
        return "GHAOYQ1KH";
      }
      public java.lang.String bigClear(){
        return "GHAOYQ1LH";
      }
      public java.lang.String bigGreen(){
        return "GHAOYQ1MH";
      }
      public java.lang.String bigRed(){
        return "GHAOYQ1NH";
      }
      public java.lang.String disabled(){
        return "GHAOYQ1OH";
      }
      public java.lang.String enabled(){
        return "GHAOYQ1PH";
      }
      public java.lang.String icon(){
        return "GHAOYQ1AI";
      }
      public java.lang.String iconBigWrapper(){
        return "GHAOYQ1BI";
      }
      public java.lang.String iconConfirm(){
        return "GHAOYQ1CI";
      }
      public java.lang.String iconWrapper(){
        return "GHAOYQ1DI";
      }
      public java.lang.String over(){
        return "GHAOYQ1EI";
      }
      public java.lang.String red(){
        return "GHAOYQ1FI";
      }
      public java.lang.String withIcon(){
        return "GHAOYQ1GI";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.common.gwt.client.ui.comp.action.ActionCss get() {
      return css;
    }
  }
  public consys.common.gwt.client.ui.comp.action.ActionCss css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAoCAYAAAA/tpB3AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABlSURBVHjaPI47CsNQDASHDZiYFD6pj5l7BFK5dGX8ebsp4qdGCGZWWub3Z9MgPWUHhRoGOUEtQQH+im3UAkp80xsEmb6FoONqaN3PumfkyrqysetHB0m6Uh7VRa+Jx7l8B4DxNwB9rl+22ptz0AAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAmCAYAAAAFvPEHAAAAFUlEQVR42mMI3HDuO8N/IBhqBMjhAEtrlP1E+0FkAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAmCAYAAAAMV1F9AAAAc0lEQVR42mNgAAL12QcVDRccneix5vSDwPXnXjHIzDioar30xKX9j97+//7n338g+MygN//ootU3nv9HAp8ZPNecefn3/z9UQZ+1Zz//RwWjgqOCNBbEmhSxJlpY8t738O3/n3+hyRs5I3iuOf0ImBFeAwDh4afwmRQAGAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAmCAYAAAAMV1F9AAAAc0lEQVR42mMIXH/ulcea0w8MFxydqD77oCIDCPz////z9z///u9/9Pa/9dITl2RmHFQFC/6HgtU3nv/Xm390EYrg3////nuuOfMSRRAEfNae/TwqOCpIP0GsSREl0f78++//vodIyRuYEV57rjn9CDkjAABb7afwHas7RgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAoCAYAAAA/tpB3AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABLSURBVHjajI0xDoAwDANPQaLw/9/kU1WXMNVhbEQXltPZHoy7PybpsszEJG2Yc1Zb+Nv13rGIoHxkZoUkDNjiZ1jWWuMYY5wA9zsA35dlZzpfpq8AAAAASUVORK5CYII=";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAADzSURBVHjatJG9ToRQEIXPwF0gFOTGQGJi6NTazs74ArbQUcBrWPksPpeNFQ2NEUi8M2MhbGTvFruJTvllzk9mCMt0XfdYVdUzM98QEaFt26emaV7nec5U9dOUZXlV1/XLNE2ZqgIADIBLAHfMvDrBANipKkRkA0lEPAgPqiqYGYeeZ2x6UFXhnPtzz//o6UERwTAMcM5t4TiOWwgAqor1P3vPo5f3Ns+Dh54BjoxZpZ6cmU+Qnx70U3MbFKjqh4i8E9Eehsw8WmuzPM8fFsVXKCKu7/u3NE131trbKIrCVWOSJLkoiuI+juNr+hUaLm2C7wEAt7AAa49NjCMAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAEGSURBVHjatJAxTsNAEEXfDos2FQ0SQkLucoDcgZbU7rbJCbgC56BC4gA0OUM6XyBFDAcwEopdODtD40QONigpGGml1dP/f3f+xXK5LGOMDyGE96IoNgAiItfAfYzxbbFYzJ1ziJmhqjRNc5Xn+VOWZXdeVdmPiMyA2yNoZgCXR7C7uzGITykdYEoJM+MM5U84UO52uxMzRWSYKSJ/KP8js6qqA/Teo6r4uq6HsGu73/zwS2b2i/J0OLamMDID+6Cl/crj9r7SOXfGQ4BJ36qqH2b2JWaGcw7vPWVZvmy3240PIdC27ed6vX5drVbPbdvWTKfTxyzL5pPJ5AbwAA4IgHYnAXwPAFjH7VXWJHm7AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAoCAYAAAA/tpB3AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABpSURBVHjaVMoxDgJBDEPRL6+EoODkXIAbAqKCBtAmNgUzK9E954fz7fRS4r0co04zVajdqFLI9tDv5nU8Vxdqz9Bp1F2oPOv9fUXP9YHSQXFQsskgO4iA2ALhXyBgzOUols9l3QEcvgMAi6xjPQLTe6UAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAEySURBVHjafJG5SgRRFERPby6DTiCCgmYusZmZK4jCCH6ImJiaKPgn/pSJiYFg5MKI2vfeMhhHu5+ND25SnKpb772M7zO43ts5ON2+yIfVWkYOg8v945Pzoxt7ib7QsFxYn186PNu9qp+9HwQAZRb5Yl4XG7XqcRIlGZUyEYqGKGUuw2W/ogAPx8MbJMJkWEpGBBHtTOou8k+mEOY1FtZeNKrUJAXmhnlSycOSTKmrPLg8yUSYG97KFFh3z4Q0GY/vD8RH4+4h5+XziajVzpQLudqVFEKh9itJQkrtKTmyj6YlRohISfQ9zT/6mX8rddrVSaprESjtmSNew3RP9isWMt6ml6t+b2VySyYQdSGTDW8/7qq5sppaqtaLmawYm8pytpjrrU5slv18tZFEAeRA/jUAwCP55UC3POcAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage8 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAFUSURBVHjadJFNSlxBFIW/qndVmtAOVDSQecbJBgzBiYiCKxF6AxkkkBVkmIEEXIAbyCTTrCATzQ8hIDRqiC28d+/J4HX3q2e3NSoO3znn3qrqw+/33/ff7B5Ww/zj2+eLS4Bc5WpzdTLYOx4dnB++2zsiQw4FLqe+8fX9k9dvd55vPTOXA+A4VlcvUuSnFgpmRylDYsVczVzMAqRkHt6JkRBgTUEmAQiL6DIjYpGkzaSXmeaZUXekCyHmw/eKGm/6pMA8ipFCLdkvAmlpJpg/zERYU9gV7QK9ds3Iq/s/xdNlGjXYbX3TiSkRckyuwj7NVKhfBJhUiGpfb5Fs7SzaoyCZiRTa7G4URfM/WjrSUvtDUkvJR+ZU7v4CotFPxN+MWqEaZG6/Tj7VY7+0apjwO12Pv/w7+3U6/uiTuGPj1ZPR+svBkQ2rbcCmSaxNtw7AAf4PABNl5fz5OnRhAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage9 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAE4SURBVHjahJI9SgRBFIS/nln//wWRTVwUUUyFRVDxAN7BQIxM3NhIvIuhiXfwCKYaeQBRd8Wd6SqD2XVndNBuXlJUv6p6rwODs9+5Ptw9Ob/UR3cnkCQcXFwdH551buL765KhG5Y3tpunt/d3/ui1XTzqNoLVJM/awsNONCCMkwRcBQkWWIxAY+SiykyiTfwJelCj54YoI/1gKkYklYUguqgS09jCJU8NXKNuQLVCtSCmMhC7lunvWxHq55GYxxEom5fPDPWzas9f8wSQhSqJALmov7MbatZhUKyJaQlL/+0II6m6Dkz93gtL/s/S4Ie46tOWjEs+E9tvzrNnCGCDTeo86000W3OTre0jKYKdpc6zvPf48NRYXBkbX13bSqZn0zDsnc7OL0+t7+wlMwuboZQuBRIg+RoA83r4PBXXUrAAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage10 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAoCAYAAAA/tpB3AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABkSURBVHjaXI07CsNQEAMHBUJS5f638J3SmlSGwNtdbQo/59fNSAjxWJanqLqobdRVqDMPikCOOLJf3SkC+aOZU7tqZmNdUW4bso2wJ+2X3chV39rv9n/RjQB0kzjdxzgDXF8DAEg6a1A6v3tlAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage11 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAEuSURBVHjajJExSgNBGIW/2d2YTSAKIpJGBZGIeAA7Kw+Q5AI5gtjYCiJexCtZW1mm0WCEnZn/WWRjspuN+MM0j/e9efOPo5zn4fDmdjR6+M7zU5cAT6PR+H4yefmELmZf2aDfP7objx8/zLrECECmND0MeX6polgmkTnnWkgohJWI5BQCKtEFLiHvkfcVJ+Y9VhEBhbCRuYmrES/rVHHAigJrzFwXJWEhbDqj9zWxqedWXDFWF/JbqbKlECimU4qKaEaYzQhmNdwMq4gSmC3On04BMkN1pyQk1f4oxtpF/8YlbXE2ibHxRTFCpRLIzLA1MTGYeendlQ0kkXppftJu987z/NokDHzqpfA6n78dZFnreGdn0EuS1C2zd9N0/6LTudpLkjPHalIgAZKfAQBr7BZGfhyCYgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage12 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAE+SURBVHjadJK9TgJBFIW/+SESQpaO0Nja+AA21nYU8AS8Ao2tiYVPYGljYUFpbWx9BipjohYkW0hAEt2dey1YhkWGTSabnNzv3nPPjMsnk8+rfn/Qhvfn6fQVwNpGo/PT6ZxfjkaPN4PB0AJWQ0CLgrlIazwcXp/0esdeQwCAECibzVN1ruu1LImftRhjGttKQMsSVI3XotiKgKripSYKgCo7PdUYAPZwUrgm8b1K1URP1aqyhkvKZ9hU7gyq/ml8J5CkJZE1/pvnUTTOoWWJL5fLKDprURH8BonRAZ6aSEwpVak1UavjVXU3EMBLzbzE3f/hHMT3Bh3E6z5DcqN4R/WN1i9E43QLFPAhsLDrHtCylpfF4j4vijefGcO3yNfTfP5wO5vdrURWXGTZ+Kzd7mfOdSsIAxxVqcnG1d8AF+LxiJuZOnoAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage13 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAoCAYAAAA2XTANAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAFMSURBVHjadJExL0RBFIW/mTdhyZJQSHQapVJIRKuQ+Asq1SpQa/gNotPo1GqFRCLZ0h9AUCgUFtksb+cexex6z+7bYpKTkzP3nnNu1rh5uF/ZPdwIE/Wn5+b1E4CfCNlcLe9sru/sXa7tH2/iPd5MRBP5R2tmdbtxNLuwOB+EABCQdfNlJytIALwD3FiQFVwPu2AqlCYhRIgl0vVwKE38mx7MCjqakCCYFZtcjEkZB/4LCCp5SnhgO0rSf4ucpXxDZLJUyu7QsE/X91luKWERfrrxj8x8TIta33lRpzwmMdRnyl7RcrCK7qqz20D2oZjEXiEqldzHo25UsmSWTmxVJatEqjBfZemfckTJSPJpuNIxuvmLpE8vQD7D1yb5urs9776/PXo/OQXivdW8On29ODmzTrvN9OrGQX1pZSurT88BIV0VxgHrvQjwOwAJye5YUm2K2AAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage14 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAXCAYAAADKmiUPAAAAfklEQVR42gFzAIz/AGC412UAdMfi/wBwxOD/AG3D3v8AacHd/wBlv9v/AGG92f8AXrrX/wBauNb/AFa10/8AVLPS/wBCqsz/ADymyf8AOaTH/wA2osX/ADSfw/8AMZ3B/wAum7//ACyZvf8AKZi7/wArosr/AA86SYgAAAAACXbtOxQBAWepAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage15 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAXCAYAAADKmiUPAAAAZUlEQVR42i3IsQnEIACG0a+5bVwhS9wmFloYAmLjAFrkRwRBAhYOeSnulY/W2sHeW6y1xPM8Ys4pxhii9y704r5vUWsVpRSRcxYpJRFjFNd1ifM8RQhBeO+Fc05Ya/9njPny+vwA9eYv5MyGvPUAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage16 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAXCAYAAADOb/UyAAAA4klEQVR42kXQvWqFUAwA4NNypxYcOqlDHapQy/VF+g7OjgXRQUVFEUVwUUQbFBFEdFBEn7Dn3AwNZPjyMySE0NB1/dWyrDsRRfHlPM+ffd+ByLL8cV0XLMvSEkVRVNqBeZ6bB47jgGmaENu2wTiODZEkSV3XFfq+R9Bl6LoOQeehbVvEMAxQ1/U/qqpClGUJaZoiiqKAKIoaIgiCmuc5hGGIyLIMgiBAJEkCrusi4jgGx3EQdB5s224Iz/Nfvu8DvbRhl356ngemaf4SjuPeDMMwWYH945nuvWua9s3wxAo0b386/GkUe3OZhAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage17 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAXCAYAAADOb/UyAAAA4UlEQVR42kXQT4tGYBAA8Ge397RbDnvCYR2WWtvri+x3cHbcEgeESKRcSGOfSEriQOIT7qN5a+c0v+ZPzRDLsu66rr+SK7Zto8dx/Iii+ELmeW7O86SyLH+QaZqAVaiiKCoZxxH2fUcMwwDruiK6roNlWagkSSpp2xbYEkTTNMDmEHVdQ9/3iKqq/pGmKZRliYiiCIqiQIRhCHmeU0EQVBIEAWRZhnBdF5IkQTiOA3EcI2zbBjaHYJeC7/uU5/kvYprmr+d5lF36Sa7EMAyT47g3omnaN+t/Z+94vn5yeyRPf2WTaRTC+IWmAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage18 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAXCAYAAADOb/UyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAADESURBVHjaVIwxSkNREEXPm/8+BLGIdhaSQiFgIdiF1JLCdbgCN+Uy3IiQxlZEFOJ/b+aOxUfF8nDvOeXsZsNqe3t6crHe1Pb5sdzePzxWn3Y2juPVONiutSkMOFKKkGQJxZW4EkvAJVzCMpP+BxAhIoTB/HclJqB74BEYCVIiJdUlXg8T/dComdB60D3mtEuEhAHEDySJIlBorv1bfp1MiiQkYRQ8SiFSg/n72/PXy/7JjpdWgLpYra8X55d3BSjAANj3ADLRk4CjLi62AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage19 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAXCAYAAADKmiUPAAAAcklEQVR42mN4HBycyvAiP/8/w/OsrP8Mz1JT/zM8TUz8z/AkJgZIhIf/Z3gcFPSf4ZGf33+Gh+7uQMLN7T/DXUPD/wx3NDSAhKrqf4brysr/Ga6AiAtKSv8ZTisq/mc4ASKOKCj8Z7gEkvDl5e1gAAJOAGwaMclkVTZ2AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage20 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAXCAYAAADOb/UyAAAA+klEQVR42mNgAII5MjJCp5WUvBgMBAQEXlZU7HxRUvKfwY6X1+pVbe3/51lZfxgcuLldXlZW/n+WmvqLwZqb2/VlWdn/Z4mJEM6LwsL/T2JiIByg+v9PwsN/MZgDOc/S0v4/DgqCcJ4mJv5/5OcH5LCzuz6Jivr/0N0dyomM/P/Qze0XgwmQ88DJ6f9dQ8NfDAZAzn0rq/93NDR+MegAOfdMTP7fUVWFcIBK/l9XVoZydHT+X4FxgOr/X1BSgnBuq6j8P62oCOa4AdX/PwHiaHNwOF0Fco4oKPxlkGRgkNsoJ7cPaMB/UHiwaLKyGvnw8NSAOIwgASBmAwDjbW0wyAZZcwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage21 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAXCAYAAADOb/UyAAAA9UlEQVR42mM4raTkNUdGRogBBF6UlPx/WVGx00BAQIDheVbWn1e1tf/teHmtGJ6lpv56WVn534Gb24XhWWLir5dlZf+tubldGZ7ExPx6UVgI5YSH/wLqg3AeBwX9epaW9t8cxHnk5/fraWIihPPQ3f3Xk6io/+bs7ECOm9uvJ5GREM5dQ8NfD5yc/puAOHc0NH7dt7L6bwDmqKr+umdi8l8HxLmurPwLqBTCuQLi6OhAOBeUlH4B9UE4pxUVf91WUYFwTgA5QH0gjhvDEQWFv1eBHG0ODieQAf83ysntk2RgkGPw4eGp0WRlNQIGBwsoTNigDEYAGDNtMAeiC10AAAAASUVORK5CYII=";
  private static final java.lang.String externalImage22 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAXCAYAAADOb/UyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAADISURBVHjaTIwxSkNREEXPm/+eBLH4oL2NEjCdtbWg7sbCyjW4CVdhlQVYuAHBRrAyYhXzMnPHIjHY3cvhnOHq4fH66GT2Gd6X3D1/5O3L4unwdDZa76tog1221s4sJCkFsG+uxJUkFHMJl0jAXGItkZlYhIgQmVBdyaAEkuoRmAcCTEqkhIS6WHZaXeEStXuQ69gEQqJs07sDYAqhCJL/5M9B2hDtApQaqYFSoOBmB6P9vL/N/fvrlfHi5n5yPD0HKsDedpTfAQArp48rE76UgQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage23 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAYAAADgbT9oAAAAuUlEQVR42q2UQQrCMBBFUxERxVOIW72AN7Op1XsoulLvIR6g4MaNnsKVGl8gSjuIaDMfHjSLPIZPpsb8EOdcG5Zwh2sgh8TEBkkPdq6aqZa8C9uS+AHWaCTUUp7c15NqyRNYi1pspRYOfRj8ib8zgoOQp285HwWcanCEixD7WibQNE4/NxjHTnz+MLF9TVy34yHshTiDRuzLWKkuS3jHckmy2Ck7sBHSXHudfWZRnXK5BQvxd5t/kz4BR/90rqN04mkAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage24 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAJCAYAAADpeqZqAAAAkElEQVR42mNgwAL+///PBcS5QLwViDMYiAVAxXpAfOU/BBSRolEBiE8C8V8gLkSW0AJiPyD2wYIdQbYA8Tcg/gnE+UDMCNLUBsRfgfgLDvwSiH9AnQmy0QWkyQGIK4C4BAvOA+IGIH4D1bQIiPkJ+YkJiGciaeAlpEEQiJdBNSwGYj5ibAB5+h4QzwDFG7I8AION6Rpl1kVuAAAAAElFTkSuQmCC";
  private static com.google.gwt.resources.client.ImageResource actionImageBigCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageBigClearCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageBigClearLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageBigClearRight;
  private static com.google.gwt.resources.client.ImageResource actionImageBigDisabledCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageBigDisabledLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageBigDisabledRight;
  private static com.google.gwt.resources.client.ImageResource actionImageBigGreenCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageBigGreenLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageBigGreenRight;
  private static com.google.gwt.resources.client.ImageResource actionImageBigLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageBigRedCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageBigRedLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageBigRedRight;
  private static com.google.gwt.resources.client.ImageResource actionImageBigRight;
  private static com.google.gwt.resources.client.ImageResource actionImageCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageDisabledCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageDisabledLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageDisabledRight;
  private static com.google.gwt.resources.client.ImageResource actionImageLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageRedCenter;
  private static com.google.gwt.resources.client.ImageResource actionImageRedLeft;
  private static com.google.gwt.resources.client.ImageResource actionImageRedRight;
  private static com.google.gwt.resources.client.ImageResource actionImageRight;
  private static com.google.gwt.resources.client.ImageResource iconBigConfirm;
  private static com.google.gwt.resources.client.ImageResource iconConfirm;
  private static consys.common.gwt.client.ui.comp.action.ActionCss css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      actionImageBigCenter(), 
      actionImageBigClearCenter(), 
      actionImageBigClearLeft(), 
      actionImageBigClearRight(), 
      actionImageBigDisabledCenter(), 
      actionImageBigDisabledLeft(), 
      actionImageBigDisabledRight(), 
      actionImageBigGreenCenter(), 
      actionImageBigGreenLeft(), 
      actionImageBigGreenRight(), 
      actionImageBigLeft(), 
      actionImageBigRedCenter(), 
      actionImageBigRedLeft(), 
      actionImageBigRedRight(), 
      actionImageBigRight(), 
      actionImageCenter(), 
      actionImageDisabledCenter(), 
      actionImageDisabledLeft(), 
      actionImageDisabledRight(), 
      actionImageLeft(), 
      actionImageRedCenter(), 
      actionImageRedLeft(), 
      actionImageRedRight(), 
      actionImageRight(), 
      iconBigConfirm(), 
      iconConfirm(), 
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("actionImageBigCenter", actionImageBigCenter());
        resourceMap.put("actionImageBigClearCenter", actionImageBigClearCenter());
        resourceMap.put("actionImageBigClearLeft", actionImageBigClearLeft());
        resourceMap.put("actionImageBigClearRight", actionImageBigClearRight());
        resourceMap.put("actionImageBigDisabledCenter", actionImageBigDisabledCenter());
        resourceMap.put("actionImageBigDisabledLeft", actionImageBigDisabledLeft());
        resourceMap.put("actionImageBigDisabledRight", actionImageBigDisabledRight());
        resourceMap.put("actionImageBigGreenCenter", actionImageBigGreenCenter());
        resourceMap.put("actionImageBigGreenLeft", actionImageBigGreenLeft());
        resourceMap.put("actionImageBigGreenRight", actionImageBigGreenRight());
        resourceMap.put("actionImageBigLeft", actionImageBigLeft());
        resourceMap.put("actionImageBigRedCenter", actionImageBigRedCenter());
        resourceMap.put("actionImageBigRedLeft", actionImageBigRedLeft());
        resourceMap.put("actionImageBigRedRight", actionImageBigRedRight());
        resourceMap.put("actionImageBigRight", actionImageBigRight());
        resourceMap.put("actionImageCenter", actionImageCenter());
        resourceMap.put("actionImageDisabledCenter", actionImageDisabledCenter());
        resourceMap.put("actionImageDisabledLeft", actionImageDisabledLeft());
        resourceMap.put("actionImageDisabledRight", actionImageDisabledRight());
        resourceMap.put("actionImageLeft", actionImageLeft());
        resourceMap.put("actionImageRedCenter", actionImageRedCenter());
        resourceMap.put("actionImageRedLeft", actionImageRedLeft());
        resourceMap.put("actionImageRedRight", actionImageRedRight());
        resourceMap.put("actionImageRight", actionImageRight());
        resourceMap.put("iconBigConfirm", iconBigConfirm());
        resourceMap.put("iconConfirm", iconConfirm());
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'actionImageBigCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigCenter()();
      case 'actionImageBigClearCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigClearCenter()();
      case 'actionImageBigClearLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigClearLeft()();
      case 'actionImageBigClearRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigClearRight()();
      case 'actionImageBigDisabledCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigDisabledCenter()();
      case 'actionImageBigDisabledLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigDisabledLeft()();
      case 'actionImageBigDisabledRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigDisabledRight()();
      case 'actionImageBigGreenCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigGreenCenter()();
      case 'actionImageBigGreenLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigGreenLeft()();
      case 'actionImageBigGreenRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigGreenRight()();
      case 'actionImageBigLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigLeft()();
      case 'actionImageBigRedCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigRedCenter()();
      case 'actionImageBigRedLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigRedLeft()();
      case 'actionImageBigRedRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigRedRight()();
      case 'actionImageBigRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageBigRight()();
      case 'actionImageCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageCenter()();
      case 'actionImageDisabledCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageDisabledCenter()();
      case 'actionImageDisabledLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageDisabledLeft()();
      case 'actionImageDisabledRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageDisabledRight()();
      case 'actionImageLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageLeft()();
      case 'actionImageRedCenter': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageRedCenter()();
      case 'actionImageRedLeft': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageRedLeft()();
      case 'actionImageRedRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageRedRight()();
      case 'actionImageRight': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::actionImageRight()();
      case 'iconBigConfirm': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::iconBigConfirm()();
      case 'iconConfirm': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::iconConfirm()();
      case 'css': return this.@consys.common.gwt.client.ui.comp.action.ActionResources::css()();
    }
    return null;
  }-*/;
}
