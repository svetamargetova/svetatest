package consys.common.gwt.client.ui.comp.input.validator;

public class ValidatorConstants_ implements consys.common.gwt.client.ui.comp.input.validator.ValidatorConstants {
  
  public java.lang.String fieldMustBeNumberic() {
    return "The value must be numeric.";
  }
  
  public java.lang.String fieldMustEntered() {
    return "This field is required.";
  }
}
