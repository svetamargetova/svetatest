package consys.common.gwt.shared.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CheckCorporationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getPrefix(consys.common.gwt.shared.action.CheckCorporationAction instance) /*-{
    return instance.@consys.common.gwt.shared.action.CheckCorporationAction::prefix;
  }-*/;
  
  private static native void setPrefix(consys.common.gwt.shared.action.CheckCorporationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.action.CheckCorporationAction::prefix = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.action.CheckCorporationAction instance) throws SerializationException {
    setPrefix(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.action.CheckCorporationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.action.CheckCorporationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.action.CheckCorporationAction instance) throws SerializationException {
    streamWriter.writeString(getPrefix(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.action.CheckCorporationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.action.CheckCorporationAction_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.action.CheckCorporationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.action.CheckCorporationAction_FieldSerializer.serialize(writer, (consys.common.gwt.shared.action.CheckCorporationAction)object);
  }
  
}
