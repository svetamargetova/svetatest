package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEventPaymentProfile_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getBankOrderAsProfile(consys.common.gwt.shared.bo.ClientEventPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventPaymentProfile::bankOrderAsProfile;
  }-*/;
  
  private static native void setBankOrderAsProfile(consys.common.gwt.shared.bo.ClientEventPaymentProfile instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventPaymentProfile::bankOrderAsProfile = value;
  }-*/;
  
  private static native consys.common.constants.currency.CurrencyEnum getCurrency(consys.common.gwt.shared.bo.ClientEventPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventPaymentProfile::currency;
  }-*/;
  
  private static native void setCurrency(consys.common.gwt.shared.bo.ClientEventPaymentProfile instance, consys.common.constants.currency.CurrencyEnum value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventPaymentProfile::currency = value;
  }-*/;
  
  private static native java.lang.String getInvoiceNote(consys.common.gwt.shared.bo.ClientEventPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventPaymentProfile::invoiceNote;
  }-*/;
  
  private static native void setInvoiceNote(consys.common.gwt.shared.bo.ClientEventPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventPaymentProfile::invoiceNote = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientEventPaymentProfile instance) throws SerializationException {
    setBankOrderAsProfile(instance, streamReader.readBoolean());
    setCurrency(instance, (consys.common.constants.currency.CurrencyEnum) streamReader.readObject());
    setInvoiceNote(instance, streamReader.readString());
    
    consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.shared.bo.ClientEventPaymentProfile instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientEventPaymentProfile();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientEventPaymentProfile instance) throws SerializationException {
    streamWriter.writeBoolean(getBankOrderAsProfile(instance));
    streamWriter.writeObject(getCurrency(instance));
    streamWriter.writeString(getInvoiceNote(instance));
    
    consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientEventPaymentProfile_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEventPaymentProfile_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientEventPaymentProfile)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEventPaymentProfile_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientEventPaymentProfile)object);
  }
  
}
