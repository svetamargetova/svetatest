package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListDataSourceResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getAllItemsCount(consys.common.gwt.client.ui.comp.list.ListDataSourceResult instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceResult::allItemsCount;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setAllItemsCount(consys.common.gwt.client.ui.comp.list.ListDataSourceResult instance, long value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceResult::allItemsCount = value;
  }-*/;
  
  private static native java.util.ArrayList getItems(consys.common.gwt.client.ui.comp.list.ListDataSourceResult instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceResult::items;
  }-*/;
  
  private static native void setItems(consys.common.gwt.client.ui.comp.list.ListDataSourceResult instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceResult::items = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.ui.comp.list.ListDataSourceResult instance) throws SerializationException {
    setAllItemsCount(instance, streamReader.readLong());
    setItems(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.client.ui.comp.list.ListDataSourceResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.ui.comp.list.ListDataSourceResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.ui.comp.list.ListDataSourceResult instance) throws SerializationException {
    streamWriter.writeLong(getAllItemsCount(instance));
    streamWriter.writeObject(getItems(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.ui.comp.list.ListDataSourceResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.ListDataSourceResult_FieldSerializer.deserialize(reader, (consys.common.gwt.client.ui.comp.list.ListDataSourceResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.ListDataSourceResult_FieldSerializer.serialize(writer, (consys.common.gwt.client.ui.comp.list.ListDataSourceResult)object);
  }
  
}
