package consys.common.gwt.client.ui.comp.input.validator;

public class ValidatorMessages_cs implements consys.common.gwt.client.ui.comp.input.validator.ValidatorMessages {
  
  public java.lang.String fieldMustBeInRange(java.lang.Number arg0,java.lang.Number arg1) {
    return new java.lang.StringBuffer().append("Hodnota musí být mezi ").append(arg0).append(" a ").append(arg1).append(".").toString();
  }
}
