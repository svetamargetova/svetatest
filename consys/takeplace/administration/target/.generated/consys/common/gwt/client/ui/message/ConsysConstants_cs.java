package consys.common.gwt.client.ui.message;

public class ConsysConstants_cs implements consys.common.gwt.client.ui.message.ConsysConstants {
  
  public java.lang.String progress_text1() {
    return "REGISTRUJ SE";
  }
  
  public java.lang.String paymentProfilePanel_form_vatNumber() {
    return "DIČ";
  }
  
  public java.lang.String const_password() {
    return "Heslo";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_faq() {
    return "FAQ";
  }
  
  public java.lang.String help_text_noTitle() {
    return "Žádný titulek";
  }
  
  public java.lang.String consysRichTextToolbar_text_underline() {
    return "Podtržené";
  }
  
  public java.lang.String consysRichTextToolbar_text_strikethrough() {
    return "Přeškrtnuté";
  }
  
  public java.lang.String const_more() {
    return "Více";
  }
  
  public java.lang.String const_genderNotSpec() {
    return "Neuvedeno";
  }
  
  public java.lang.String const_add() {
    return "Přidat";
  }
  
  public java.lang.String const_address_street() {
    return "Ulice";
  }
  
  public java.lang.String dataListCommon_text_show() {
    return "Zobrazit";
  }
  
  public java.lang.String const_address_city() {
    return "Město";
  }
  
  public java.lang.String const_date_monthFebruary() {
    return "Únor";
  }
  
  public java.lang.String const_copy() {
    return "Kopírovat";
  }
  
  public java.lang.String consysRichTextToolbar_text_bold() {
    return "Tučné";
  }
  
  public java.lang.String const_yes() {
    return "Ano";
  }
  
  public java.lang.String consysRichTextToolbar_text_list() {
    return "Seznam";
  }
  
  public java.lang.String paymentProfilePanel_text_invoicedTo() {
    return "Fakturováno na Organizaci / Jméno";
  }
  
  public java.lang.String const_confirmPassword() {
    return "Potvrďte heslo";
  }
  
  public java.lang.String consysRichTextToolbar_text_italic() {
    return "Kurziva";
  }
  
  public java.lang.String consysPasswordBox_error_passwordNotEntered() {
    return "Heslo nebylo zadáno";
  }
  
  public java.lang.String servletUtils_error_http401() {
    return "Uživatel není přihlášený.";
  }
  
  public java.lang.String help_text_noDescription() {
    return "Žádný popis";
  }
  
  public java.lang.String const_description() {
    return "Popis";
  }
  
  public java.lang.String const_date_monthNovember() {
    return "Listopad";
  }
  
  public java.lang.String const_noneLower() {
    return "žádný";
  }
  
  public java.lang.String servletUtils_error_http403() {
    return "Akce nemá oprávnění nahrávat videa.";
  }
  
  public java.lang.String userInvitePanel_form_name() {
    return "Jméno";
  }
  
  public java.lang.String const_phoneNumber() {
    return "Telefonní číslo";
  }
  
  public java.lang.String const_edit() {
    return "Editovat";
  }
  
  public java.lang.String const_personalInfo_celiac() {
    return "Celiak";
  }
  
  public java.lang.String paymentPriceList_text_name() {
    return "Název";
  }
  
  public java.lang.String loginPanel_text_signUpWith() {
    return "Přihlašte se jedním z těchto";
  }
  
  public java.lang.String const_address_state() {
    return "Stát";
  }
  
  public java.lang.String const_showPassword() {
    return "Zobrazit heslo";
  }
  
  public java.lang.String const_personalInfo_vegetarian() {
    return "Vegetarián";
  }
  
  public java.lang.String const_addMore() {
    return "Přidat další";
  }
  
  public java.lang.String const_genderWoman() {
    return "Žena";
  }
  
  public java.lang.String paymentProfilePanel_text_supportedText() {
    return "<a href=\"http://www.ibancalculator.com/iban_and_bic.html\" target=\"_blank\">Vypočítejte</a> si svůj IBAN a SWIFT kód. Můžete také použít kalkulátor <a href=\"http://www.cnb.cz/cs/platebni_styk/iban/iban.html\" target=\"_blank\">ČNB</a>.";
  }
  
  public java.lang.String userSearchDialog_action_invite() {
    return "Nikdo nenalezen?\nPozvěte nového uživatele!";
  }
  
  public java.lang.String paymentProfilePanel_form_bankNumber() {
    return "SWIFT";
  }
  
  public java.lang.String paymentPriceList_text_total() {
    return "Celkem";
  }
  
  public java.lang.String unknownTokenContent_text() {
    return "Neznámý parametr v příznaku.";
  }
  
  public java.lang.String const_organization() {
    return "Organizace";
  }
  
  public java.lang.String const_select() {
    return "Vybrat";
  }
  
  public java.lang.String userSearchDialog_helpText() {
    return "Vyhledávání podle e-mailu nebo podle jména a příjmení.";
  }
  
  public java.lang.String paymentProfilePanel_form_accountNumber() {
    return "IBAN";
  }
  
  public java.lang.String const_close() {
    return "Zavřít";
  }
  
  public java.lang.String const_acronym() {
    return "Zkrácený název";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_customers() {
    return "Zákazníci";
  }
  
  public java.lang.String consysFileUpload_button_browsFile() {
    return "Procházet";
  }
  
  public java.lang.String dataList_text_empty() {
    return "Seznam je prázdný.";
  }
  
  public java.lang.String paymentProfilePanel_title() {
    return "Fakturační adresa";
  }
  
  public java.lang.String const_date_monthApril() {
    return "Duben";
  }
  
  public java.lang.String const_name() {
    return "Název";
  }
  
  public java.lang.String const_property() {
    return "Vlastnosti";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_overview() {
    return "Přehled";
  }
  
  public java.lang.String consysAutoFileUpload_text_uploaded() {
    return "Soubor byl nahrán na server.";
  }
  
  public java.lang.String const_discountCode() {
    return "Slevový kód";
  }
  
  public java.lang.String const_date_monthSeptember() {
    return "Září";
  }
  
  public java.lang.String const_currency() {
    return "Měna";
  }
  
  public java.lang.String remover_text_areYouSure() {
    return "Jste si jisti?";
  }
  
  public java.lang.String const_submit() {
    return "Potvrdit";
  }
  
  public java.lang.String const_item_requiredLower() {
    return "povinný";
  }
  
  public java.lang.String userInvitePanel_button_invite() {
    return "Pozvat";
  }
  
  public java.lang.String const_date_monthJuly() {
    return "Červenec";
  }
  
  public java.lang.String servletUtils_error_http500() {
    return "Vyskytla se chyba při zpracování souboru.";
  }
  
  public java.lang.String const_date_yesterday() {
    return "Včera";
  }
  
  public java.lang.String dataListCommon_text_sortBy() {
    return "Řadit podle";
  }
  
  public java.lang.String const_item_optionalLower() {
    return "volitelný";
  }
  
  public java.lang.String servletUtils_error_http406() {
    return "Požadavek nebyl přijat.";
  }
  
  public java.lang.String const_date_monthDecember() {
    return "Prosinec";
  }
  
  public java.lang.String const_decline() {
    return "Odmítnout";
  }
  
  public java.lang.String const_user_lastName() {
    return "Příjmení";
  }
  
  public java.lang.String consysRichTextToolbar_text_removeLink() {
    return "Zrušit odkaz";
  }
  
  public java.lang.String const_website() {
    return "Webová stránka";
  }
  
  public java.lang.String const_error_noPermissionForAction() {
    return "Nemáte oprávnění k provedení této akce";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_signUp() {
    return "REGISTRUJ SE";
  }
  
  public java.lang.String consysRichTextToolbar_text_removeFormat() {
    return "Zrušit formátování";
  }
  
  public java.lang.String const_date_monthJanuary() {
    return "Leden";
  }
  
  public java.lang.String const_toLower() {
    return "do";
  }
  
  public java.lang.String progress_text2() {
    return "POTVRĎ";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_aboutUs() {
    return "O nás";
  }
  
  public java.lang.String const_date_monthAugust() {
    return "Srpen";
  }
  
  public java.lang.String consysAutoFileUpload_text_uploading() {
    return "Soubor se nahrává na server ...";
  }
  
  public java.lang.String paymentPriceList_text_quantity() {
    return "Počet";
  }
  
  public java.lang.String const_no() {
    return "Ne";
  }
  
  public java.lang.String ssoLogin_text_orUseTakeplaceAccount() {
    return "... nebo použijte Takeplace účet";
  }
  
  public java.lang.String consysPasswordBox_error_passwordsNotSame() {
    return "Hesla nejsou shodná";
  }
  
  public java.lang.String const_clearForm() {
    return "Vyčistit formulář";
  }
  
  public java.lang.String const_gender() {
    return "Pohlaví";
  }
  
  public java.lang.String const_free() {
    return "zdarma";
  }
  
  public java.lang.String const_accept() {
    return "Přijmout";
  }
  
  public java.lang.String paymentPriceList_text_unitPrice() {
    return "Cena za kus";
  }
  
  public java.lang.String servletUtils_error_http400() {
    return "Špatný požadavek na server.";
  }
  
  public java.lang.String const_address_place() {
    return "Místo";
  }
  
  public int consysFileUpload_button_width() {
    return 98;
  }
  
  public java.lang.String const_delete() {
    return "Odstranit";
  }
  
  public java.lang.String const_email() {
    return "E-mail";
  }
  
  public java.lang.String const_update() {
    return "Aktualizovat";
  }
  
  public java.lang.String const_cancel() {
    return "Zrušit";
  }
  
  public java.lang.String const_exceptionBadIban() {
    return "Zadali jste špatný formát IBAN. IBAN musí začínat dvojicí písmen a dvojicí čísel, za kterými následují další čísla nebo písmena vzhledem k formátu, který byl přidelen Vaší bance.";
  }
  
  public java.lang.String consysRichTextToolbar_text_enterURL() {
    return "Vložte adresu odkazu";
  }
  
  public java.lang.String const_remove() {
    return "Odstranit";
  }
  
  public java.lang.String servletUtils_error_http404() {
    return "Požadované url nebylo nalezeno.";
  }
  
  public java.lang.String const_date_year() {
    return "Rok";
  }
  
  public java.lang.String const_timeZone() {
    return "Časová zóna";
  }
  
  public java.lang.String const_save() {
    return "Uložit";
  }
  
  public java.lang.String const_isLower() {
    return "je";
  }
  
  public java.lang.String const_warning() {
    return "Varování";
  }
  
  public java.lang.String dataList_error() {
    return "Data nemohla být načtena.";
  }
  
  public java.lang.String const_check() {
    return "Ověřit";
  }
  
  public java.lang.String system_locale() {
    return "cs";
  }
  
  public java.lang.String consysRichTextToolbar_text_createLink() {
    return "Vytvořit odkaz";
  }
  
  public java.lang.String const_date_monthMarch() {
    return "Březen";
  }
  
  public java.lang.String paymentProfilePanel_text_supportedSwift() {
    return "\"SWIFT kód\" je jedinečný kód, kterým se banka identifikuje (BIC). Jedná se o mezinárodní standard ISO 9362.";
  }
  
  public java.lang.String const_exceptionServiceFailed() {
    return "Vašemu požadavku nemůžeme v tuto chvíli vyhovět. Prosím zopakujte jej později nebo nás kontaktujte, pokud to nastane znovu.";
  }
  
  public java.lang.String consysWordArea_text_left() {
    return "zbývá slov";
  }
  
  public java.lang.String const_back() {
    return "Zpět";
  }
  
  public java.lang.String const_error_invalidEmailAddress() {
    return "Nesprávná e-mail adresa.";
  }
  
  public java.lang.String accessDeniedContent_error() {
    return "Přístup odepřen";
  }
  
  public java.lang.String paymentProfilePanel_text_supportedIban() {
    return "IBAN je mezinárodní číslo bankovního účtu definován standardem ISO 13616.";
  }
  
  public java.lang.String const_user_firstName() {
    return "Jméno";
  }
  
  public java.lang.String const_at() {
    return "v";
  }
  
  public java.lang.String const_address_zip() {
    return "PSČ";
  }
  
  public java.lang.String const_address_location() {
    return "Země";
  }
  
  public java.lang.String subbundlePanel_action_showDetails() {
    return "Více";
  }
  
  public java.lang.String consysFileUpload_text_chooseFileToUpload() {
    return "Vyberte soubor k nahrání";
  }
  
  public java.lang.String const_sendUpper() {
    return "ODEŠLI";
  }
  
  public java.lang.String const_exceptionNotPermittedAction() {
    return "Nepovolená akce.";
  }
  
  public java.lang.String const_apply() {
    return "Použít";
  }
  
  public java.lang.String const_unlimited() {
    return "Neomezeně";
  }
  
  public java.lang.String servletUtils_error_http415() {
    return "Špatný formát souboru.";
  }
  
  public java.lang.String const_position() {
    return "Pozice";
  }
  
  public java.lang.String const_genderMan() {
    return "Muž";
  }
  
  public java.lang.String userCard_error_userDataNotYetInEvent() {
    return "Uživatelovy data nejsou prozatím v akci k dispozici.";
  }
  
  public java.lang.String paymentProfilePanel_error_badIban() {
    return "Špatný formát IBAN.";
  }
  
  public java.lang.String const_freeUpper() {
    return "ZDARMA";
  }
  
  public java.lang.String const_date_monthOctober() {
    return "Říjen";
  }
  
  public java.lang.String const_saveUpper() {
    return "ULOŽIT";
  }
  
  public java.lang.String const_download() {
    return "Stáhnout";
  }
  
  public java.lang.String locale() {
    return "cs";
  }
  
  public java.lang.String const_date_agoLower() {
    return "nazpět";
  }
  
  public java.lang.String paymentProfilePanel_form_registrationNumber() {
    return "IČO";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_features() {
    return "Vlastnosti";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_pressAndMedia() {
    return "Tisk a média";
  }
  
  public java.lang.String servletUtils_error_http413() {
    return "Soubor je příliš velký.";
  }
  
  public java.lang.String const_date_monthMay() {
    return "Květen";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_home() {
    return "Titulka";
  }
  
  public java.lang.String const_exceptionBadInput() {
    return "Zadali jste neplatný vstup.";
  }
  
  public java.lang.String const_ssoNotWork() {
    return "SSO nefunguje";
  }
  
  public java.lang.String const_change() {
    return "Změnit";
  }
  
  public java.lang.String progress_text3() {
    return "PŘIHLAŠ SE";
  }
  
  public java.lang.String const_duplicate() {
    return "Duplikovat";
  }
  
  public java.lang.String const_date_monthJune() {
    return "Červen";
  }
  
  public java.lang.String const_settings() {
    return "Nastavení";
  }
  
  public java.lang.String const_loadingDots() {
    return "Načítá se...";
  }
  
  public java.lang.String const_noRecordFound() {
    return "Žádné záznamy nebyly nalezeny.";
  }
}
