package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUserWithAffiliationThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getPosition(consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb::position;
  }-*/;
  
  private static native void setPosition(consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb::position = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb instance) throws SerializationException {
    setPosition(instance, streamReader.readString());
    
    consys.common.gwt.shared.bo.ClientUserThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb instance) throws SerializationException {
    streamWriter.writeString(getPosition(instance));
    
    consys.common.gwt.shared.bo.ClientUserThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb)object);
  }
  
}
