package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPaymentOrderData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getProducts(consys.common.gwt.shared.bo.ClientPaymentOrderData instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientPaymentOrderData::products;
  }-*/;
  
  private static native void setProducts(consys.common.gwt.shared.bo.ClientPaymentOrderData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientPaymentOrderData::products = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getTotalPrice(consys.common.gwt.shared.bo.ClientPaymentOrderData instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientPaymentOrderData::totalPrice;
  }-*/;
  
  private static native void setTotalPrice(consys.common.gwt.shared.bo.ClientPaymentOrderData instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientPaymentOrderData::totalPrice = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientPaymentOrderData instance) throws SerializationException {
    setProducts(instance, (java.util.ArrayList) streamReader.readObject());
    setTotalPrice(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientPaymentOrderData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientPaymentOrderData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientPaymentOrderData instance) throws SerializationException {
    streamWriter.writeObject(getProducts(instance));
    streamWriter.writeObject(getTotalPrice(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientPaymentOrderData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientPaymentOrderData_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientPaymentOrderData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientPaymentOrderData_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientPaymentOrderData)object);
  }
  
}
