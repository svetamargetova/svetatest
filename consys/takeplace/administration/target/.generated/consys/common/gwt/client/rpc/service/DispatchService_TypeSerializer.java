package consys.common.gwt.client.rpc.service;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.user.client.rpc.impl.TypeHandler;
import java.util.HashMap;
import java.util.Map;
import com.google.gwt.core.client.GwtScriptOnly;

public class DispatchService_TypeSerializer extends com.google.gwt.user.client.rpc.impl.SerializerBase {
  private static final MethodMap methodMapNative;
  private static final JsArrayString signatureMapNative;
  
  static {
    methodMapNative = loadMethodsNative();
    signatureMapNative = loadSignaturesNative();
  }
  
  @SuppressWarnings("deprecation")
  @GwtScriptOnly
  private static native MethodMap loadMethodsNative() /*-{
    var result = {};
    result["com.allen_sauer.gwt.dnd.client.DragHandlerCollection/3996089253"] = [
        @com.allen_sauer.gwt.dnd.client.DragHandlerCollection_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.allen_sauer.gwt.dnd.client.DragHandlerCollection_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lcom/allen_sauer/gwt/dnd/client/DragHandlerCollection;),
      ];
    
    result["com.google.gwt.i18n.client.impl.DateRecord/3220471373"] = [
        @com.google.gwt.i18n.client.impl.DateRecord_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.i18n.client.impl.DateRecord_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lcom/google/gwt/i18n/client/impl/DateRecord;),
        @com.google.gwt.i18n.client.impl.DateRecord_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lcom/google/gwt/i18n/client/impl/DateRecord;)
      ];
    
    result["com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException/3936916533"] = [
        @com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lcom/google/gwt/user/client/rpc/IncompatibleRemoteServiceException;),
        @com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lcom/google/gwt/user/client/rpc/IncompatibleRemoteServiceException;)
      ];
    
    result["com.google.gwt.user.client.rpc.RpcTokenException/2345075298"] = [
        @com.google.gwt.user.client.rpc.RpcTokenException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.RpcTokenException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lcom/google/gwt/user/client/rpc/RpcTokenException;),
      ];
    
    result["com.google.gwt.user.client.rpc.XsrfToken/4254043109"] = [
        ,
        ,
        @com.google.gwt.user.client.rpc.XsrfToken_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lcom/google/gwt/user/client/rpc/XsrfToken;)
      ];
    
    result["consys.admin.event.gwt.client.action.CreateEventAction/1827866985"] = [
        ,
        ,
        @consys.admin.event.gwt.client.action.CreateEventAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/event/gwt/client/action/CreateEventAction;)
      ];
    
    result["consys.admin.event.gwt.client.action.EnterEventAction/2216433768"] = [
        ,
        ,
        @consys.admin.event.gwt.client.action.EnterEventAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/event/gwt/client/action/EnterEventAction;)
      ];
    
    result["consys.admin.event.gwt.client.action.ListAllEventsAction/978835033"] = [
        ,
        ,
        @consys.admin.event.gwt.client.action.ListAllEventsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/event/gwt/client/action/ListAllEventsAction;)
      ];
    
    result["consys.admin.event.gwt.client.action.ListMyEventInvitationsAction/2767998444"] = [
        ,
        ,
        @consys.admin.event.gwt.client.action.ListMyEventInvitationsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/event/gwt/client/action/ListMyEventInvitationsAction;)
      ];
    
    result["consys.admin.event.gwt.client.action.ListMyEventsAction/2492764888"] = [
        ,
        ,
        @consys.admin.event.gwt.client.action.ListMyEventsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/event/gwt/client/action/ListMyEventsAction;)
      ];
    
    result["consys.admin.event.gwt.client.action.LoadEventInvitationAction/408415773"] = [
        ,
        ,
        @consys.admin.event.gwt.client.action.LoadEventInvitationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/event/gwt/client/action/LoadEventInvitationAction;)
      ];
    
    result["consys.admin.event.gwt.client.action.ResolveInvitationAction/234308546"] = [
        ,
        ,
        @consys.admin.event.gwt.client.action.ResolveInvitationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/event/gwt/client/action/ResolveInvitationAction;)
      ];
    
    result["consys.admin.event.gwt.client.bo.EventInvitation/3368445619"] = [
        @consys.admin.event.gwt.client.bo.EventInvitation_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.event.gwt.client.bo.EventInvitation_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/event/gwt/client/bo/EventInvitation;),
      ];
    
    result["consys.admin.event.gwt.client.bo.EventInvitationListResult/414401816"] = [
        @consys.admin.event.gwt.client.bo.EventInvitationListResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.event.gwt.client.bo.EventInvitationListResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/event/gwt/client/bo/EventInvitationListResult;),
      ];
    
    result["consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb/1522199903"] = [
        @consys.admin.event.gwt.client.bo.EventInvitationListResult_InvitationThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.event.gwt.client.bo.EventInvitationListResult_InvitationThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/event/gwt/client/bo/EventInvitationListResult$InvitationThumb;),
      ];
    
    result["consys.admin.event.gwt.client.bo.EventListResult/2287712101"] = [
        @consys.admin.event.gwt.client.bo.EventListResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.event.gwt.client.bo.EventListResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/event/gwt/client/bo/EventListResult;),
      ];
    
    result["consys.admin.event.gwt.client.bo.InvitationState/2961317799"] = [
        @consys.admin.event.gwt.client.bo.InvitationState_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.event.gwt.client.bo.InvitationState_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/event/gwt/client/bo/InvitationState;),
      ];
    
    result["consys.admin.user.gwt.client.action.ActivateUserAction/2590092480"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.ActivateUserAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/ActivateUserAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.ActivateUserRequestAction/964788699"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.ActivateUserRequestAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/ActivateUserRequestAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.ConfirmInvitationAction/1171351160"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.ConfirmInvitationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/ConfirmInvitationAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.CreateUserAction/2711669843"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.CreateUserAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/CreateUserAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.CreateUserOrgAction/2468010342"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.CreateUserOrgAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/CreateUserOrgAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.DeleteUserOrgAction/488921357"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.DeleteUserOrgAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/DeleteUserOrgAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.ListOrganizationTypesAction/1279545806"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.ListOrganizationTypesAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/ListOrganizationTypesAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction/247338632"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/ListOrganizationsByPrefixAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.ListUserOrgsAction/2932916983"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.ListUserOrgsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/ListUserOrgsAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.LoadInvitationAction/3977156374"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.LoadInvitationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/LoadInvitationAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.LoadLoggedUserAction/1776801550"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.LoadLoggedUserAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/LoadLoggedUserAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.LoadOrganizationAction/968601743"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.LoadOrganizationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/LoadOrganizationAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.LoadPersonalInfoAction/3291455903"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.LoadPersonalInfoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/LoadPersonalInfoAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.LoadPrivateUserOrgAction/116241521"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.LoadPrivateUserOrgAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/LoadPrivateUserOrgAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.LoadUserOrgAction/342827780"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.LoadUserOrgAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/LoadUserOrgAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.LostPasswordAction/1906904301"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.LostPasswordAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/LostPasswordAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.UpdateLostPasswordAction/172872871"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.UpdateLostPasswordAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/UpdateLostPasswordAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.UpdatePasswordAction/1958868356"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.UpdatePasswordAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/UpdatePasswordAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.UpdatePersonalInfoAction/956786685"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.UpdatePersonalInfoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/UpdatePersonalInfoAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.UpdatePrivateOrgAction/3602099883"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.UpdatePrivateOrgAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/UpdatePrivateOrgAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.UpdateUserAction/3014084833"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.UpdateUserAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/UpdateUserAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.UpdateUserOrgAction/2926356051"] = [
        ,
        ,
        @consys.admin.user.gwt.client.action.UpdateUserOrgAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/action/UpdateUserOrgAction;)
      ];
    
    result["consys.admin.user.gwt.client.action.exception.ActivationKeyIsInvitationException/1369378291"] = [
        @consys.admin.user.gwt.client.action.exception.ActivationKeyIsInvitationException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.user.gwt.client.action.exception.ActivationKeyIsInvitationException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/user/gwt/client/action/exception/ActivationKeyIsInvitationException;),
      ];
    
    result["consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException/31914361"] = [
        @consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/user/gwt/client/action/exception/ActivationKeyNotFoundException;),
      ];
    
    result["consys.admin.user.gwt.client.action.exception.UserNotExistsException/296534185"] = [
        @consys.admin.user.gwt.client.action.exception.UserNotExistsException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.user.gwt.client.action.exception.UserNotExistsException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/user/gwt/client/action/exception/UserNotExistsException;),
      ];
    
    result["consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException/1662999906"] = [
        @consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/user/gwt/client/action/exception/UserOrganizationUuidException;),
      ];
    
    result["consys.admin.user.gwt.client.bo.ClientUserOrg/358888994"] = [
        @consys.admin.user.gwt.client.bo.ClientUserOrg_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.user.gwt.client.bo.ClientUserOrg_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/user/gwt/client/bo/ClientUserOrg;),
        @consys.admin.user.gwt.client.bo.ClientUserOrg_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/admin/user/gwt/client/bo/ClientUserOrg;)
      ];
    
    result["consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb/1778496328"] = [
        @consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/admin/user/gwt/client/bo/ClientUserOrgProfileThumb;),
      ];
    
    result["consys.common.constants.currency.CurrencyEnum/4158296137"] = [
        @consys.common.constants.currency.CurrencyEnum_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.constants.currency.CurrencyEnum_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/constants/currency/CurrencyEnum;),
        @consys.common.constants.currency.CurrencyEnum_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/constants/currency/CurrencyEnum;)
      ];
    
    result["consys.common.constants.sso.SSO/225063008"] = [
        @consys.common.constants.sso.SSO_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.constants.sso.SSO_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/constants/sso/SSO;),
        @consys.common.constants.sso.SSO_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/constants/sso/SSO;)
      ];
    
    result["consys.common.gwt.client.rpc.action.CloneEventAction/3465521815"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.CloneEventAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/CloneEventAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.CreateOuterRegisterAction/619093887"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.CreateOuterRegisterAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/CreateOuterRegisterAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.InitializeSsoAction/2485224978"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.InitializeSsoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/InitializeSsoAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.IsUserLoggedAction/2650918293"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.IsUserLoggedAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/IsUserLoggedAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.ListLocationAction/3937814474"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.ListLocationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/ListLocationAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.ListUsStateAction/2658509530"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.ListUsStateAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/ListUsStateAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction/256839086"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/ListUserAffiliationThumbsAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.ListUserThumbsAction/1573672824"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.ListUserThumbsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/ListUserThumbsAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction/2363772973"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/LoadSsoUserDetailsAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.LoadUserInfoAction/1248767124"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.LoadUserInfoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/LoadUserInfoAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.LoadUserThumbAction/2405829453"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.LoadUserThumbAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/LoadUserThumbAction;)
      ];
    
    result["consys.common.gwt.client.rpc.action.UpdateEventPropertyAction/565936556"] = [
        ,
        ,
        @consys.common.gwt.client.rpc.action.UpdateEventPropertyAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/rpc/action/UpdateEventPropertyAction;)
      ];
    
    result["consys.common.gwt.client.rpc.result.ArrayListResult/358078478"] = [
        @consys.common.gwt.client.rpc.result.ArrayListResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.ArrayListResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/ArrayListResult;),
      ];
    
    result["consys.common.gwt.client.rpc.result.BooleanResult/2020343530"] = [
        @consys.common.gwt.client.rpc.result.BooleanResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.BooleanResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/BooleanResult;),
      ];
    
    result["consys.common.gwt.client.rpc.result.BoxItem/367386518"] = [
        @consys.common.gwt.client.rpc.result.BoxItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.BoxItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/BoxItem;),
      ];
    
    result["consys.common.gwt.client.rpc.result.DateResult/2396398714"] = [
        @consys.common.gwt.client.rpc.result.DateResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.DateResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/DateResult;),
      ];
    
    result["consys.common.gwt.client.rpc.result.LongResult/4115447146"] = [
        @consys.common.gwt.client.rpc.result.LongResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.LongResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/LongResult;),
      ];
    
    result["consys.common.gwt.client.rpc.result.SelectBoxData/4130713844"] = [
        @consys.common.gwt.client.rpc.result.SelectBoxData_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.SelectBoxData_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/SelectBoxData;),
      ];
    
    result["consys.common.gwt.client.rpc.result.SelectBoxItem/1180891837"] = [
        @consys.common.gwt.client.rpc.result.SelectBoxItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.SelectBoxItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/SelectBoxItem;),
      ];
    
    result["consys.common.gwt.client.rpc.result.SelectBoxResult/2101284186"] = [
        @consys.common.gwt.client.rpc.result.SelectBoxResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.SelectBoxResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/SelectBoxResult;),
      ];
    
    result["consys.common.gwt.client.rpc.result.StringResult/4293830892"] = [
        @consys.common.gwt.client.rpc.result.StringResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.StringResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/StringResult;),
      ];
    
    result["consys.common.gwt.client.rpc.result.VoidResult/1277313421"] = [
        @consys.common.gwt.client.rpc.result.VoidResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.rpc.result.VoidResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/rpc/result/VoidResult;),
      ];
    
    result["consys.common.gwt.client.ui.comp.list.ListDataSourceRequest/3121066266"] = [
        ,
        ,
        @consys.common.gwt.client.ui.comp.list.ListDataSourceRequest_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/client/ui/comp/list/ListDataSourceRequest;)
      ];
    
    result["consys.common.gwt.client.ui.comp.list.ListDataSourceResult/1216131181"] = [
        @consys.common.gwt.client.ui.comp.list.ListDataSourceResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.ui.comp.list.ListDataSourceResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/ui/comp/list/ListDataSourceResult;),
      ];
    
    result["consys.common.gwt.client.ui.comp.list.item.DataListIdItem/4072070083"] = [
        @consys.common.gwt.client.ui.comp.list.item.DataListIdItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.ui.comp.list.item.DataListIdItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/ui/comp/list/item/DataListIdItem;),
      ];
    
    result["consys.common.gwt.client.ui.comp.list.item.DataListItem/1685635481"] = [
        @consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/ui/comp/list/item/DataListItem;),
      ];
    
    result["consys.common.gwt.client.ui.comp.list.item.DataListUuidItem/3604094547"] = [
        @consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/ui/comp/list/item/DataListUuidItem;),
      ];
    
    result["consys.common.gwt.client.widget.exception.UserExistsException/1727502461"] = [
        @consys.common.gwt.client.widget.exception.UserExistsException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.client.widget.exception.UserExistsException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/client/widget/exception/UserExistsException;),
      ];
    
    result["consys.common.gwt.shared.action.CheckCorporationAction/3219877223"] = [
        ,
        ,
        @consys.common.gwt.shared.action.CheckCorporationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/action/CheckCorporationAction;)
      ];
    
    result["consys.common.gwt.shared.action.exceptions.PermissionException/3958506368"] = [
        @consys.common.gwt.shared.action.exceptions.PermissionException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.action.exceptions.PermissionException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/action/exceptions/PermissionException;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientAddress/4006816371"] = [
        @consys.common.gwt.shared.bo.ClientAddress_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientAddress_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientAddress;),
        @consys.common.gwt.shared.bo.ClientAddress_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientAddress;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientCurrentEvent/2716808044"] = [
        @consys.common.gwt.shared.bo.ClientCurrentEvent_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientCurrentEvent_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientCurrentEvent;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientDate/4227272410"] = [
        @consys.common.gwt.shared.bo.ClientDate_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientDate_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientDate;),
        @consys.common.gwt.shared.bo.ClientDate_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientDate;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientDateTime/1023572743"] = [
        @consys.common.gwt.shared.bo.ClientDateTime_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientDateTime_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientDateTime;),
        @consys.common.gwt.shared.bo.ClientDateTime_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientDateTime;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientEvent/2767083701"] = [
        @consys.common.gwt.shared.bo.ClientEvent_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientEvent_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientEvent;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientEventPaymentProfile/745497340"] = [
        @consys.common.gwt.shared.bo.ClientEventPaymentProfile_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientEventPaymentProfile_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientEventPaymentProfile;),
        @consys.common.gwt.shared.bo.ClientEventPaymentProfile_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientEventPaymentProfile;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientEventThumb/1460043612"] = [
        @consys.common.gwt.shared.bo.ClientEventThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientEventThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientEventThumb;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientEventType/738359947"] = [
        @consys.common.gwt.shared.bo.ClientEventType_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientEventType_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientEventType;),
        @consys.common.gwt.shared.bo.ClientEventType_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientEventType;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientInvitedUser/479109913"] = [
        ,
        ,
        @consys.common.gwt.shared.bo.ClientInvitedUser_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientInvitedUser;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientOrganization/1399395921"] = [
        @consys.common.gwt.shared.bo.ClientOrganization_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientOrganization_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientOrganization;),
        @consys.common.gwt.shared.bo.ClientOrganization_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientOrganization;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientOrganizationThumb/1831128066"] = [
        @consys.common.gwt.shared.bo.ClientOrganizationThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientOrganizationThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientOrganizationThumb;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientPaymentOrderData/2203788992"] = [
        @consys.common.gwt.shared.bo.ClientPaymentOrderData_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientPaymentOrderData_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientPaymentOrderData;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientPaymentProduct/1115566039"] = [
        @consys.common.gwt.shared.bo.ClientPaymentProduct_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientPaymentProduct_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientPaymentProduct;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientPaymentProfileThumb/2893657349"] = [
        @consys.common.gwt.shared.bo.ClientPaymentProfileThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientPaymentProfileThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientPaymentProfileThumb;),
        @consys.common.gwt.shared.bo.ClientPaymentProfileThumb_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientPaymentProfileThumb;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientPersonalInfo/3166076476"] = [
        @consys.common.gwt.shared.bo.ClientPersonalInfo_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientPersonalInfo_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientPersonalInfo;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientRegistration/4240806994"] = [
        @consys.common.gwt.shared.bo.ClientRegistration_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientRegistration_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientRegistration;),
        @consys.common.gwt.shared.bo.ClientRegistration_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientRegistration;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientSsoUserDetails/3828159097"] = [
        @consys.common.gwt.shared.bo.ClientSsoUserDetails_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientSsoUserDetails_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientSsoUserDetails;),
        @consys.common.gwt.shared.bo.ClientSsoUserDetails_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientSsoUserDetails;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientTabItem/3927942398"] = [
        @consys.common.gwt.shared.bo.ClientTabItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientTabItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientTabItem;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientTime/258571924"] = [
        @consys.common.gwt.shared.bo.ClientTime_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientTime_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientTime;),
        @consys.common.gwt.shared.bo.ClientTime_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientTime;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientUser/336253265"] = [
        @consys.common.gwt.shared.bo.ClientUser_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientUser_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientUser;),
        @consys.common.gwt.shared.bo.ClientUser_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientUser;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientUserCommonInfo/1263823186"] = [
        @consys.common.gwt.shared.bo.ClientUserCommonInfo_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientUserCommonInfo_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientUserCommonInfo;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientUserPaymentProfile/678264809"] = [
        @consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientUserPaymentProfile;),
        @consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ClientUserPaymentProfile;)
      ];
    
    result["consys.common.gwt.shared.bo.ClientUserThumb/1899881068"] = [
        @consys.common.gwt.shared.bo.ClientUserThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientUserThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientUserThumb;),
      ];
    
    result["consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb/1176241876"] = [
        @consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ClientUserWithAffiliationThumb;),
      ];
    
    result["consys.common.gwt.shared.bo.EventSettings/1264626799"] = [
        @consys.common.gwt.shared.bo.EventSettings_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.EventSettings_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/EventSettings;),
      ];
    
    result["consys.common.gwt.shared.bo.EventUser/3547461302"] = [
        @consys.common.gwt.shared.bo.EventUser_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.EventUser_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/EventUser;),
      ];
    
    result["consys.common.gwt.shared.bo.InvitedUser/762237250"] = [
        ,
        ,
        @consys.common.gwt.shared.bo.InvitedUser_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/InvitedUser;)
      ];
    
    result["consys.common.gwt.shared.bo.Monetary/2484286945"] = [
        @consys.common.gwt.shared.bo.Monetary_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.Monetary_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/Monetary;),
        @consys.common.gwt.shared.bo.Monetary_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/Monetary;)
      ];
    
    result["consys.common.gwt.shared.bo.ProductThumb/1999943306"] = [
        @consys.common.gwt.shared.bo.ProductThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.ProductThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/ProductThumb;),
        @consys.common.gwt.shared.bo.ProductThumb_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/ProductThumb;)
      ];
    
    result["consys.common.gwt.shared.bo.list.Filter/927127043"] = [
        ,
        ,
        @consys.common.gwt.shared.bo.list.Filter_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/common/gwt/shared/bo/list/Filter;)
      ];
    
    result["consys.common.gwt.shared.bo.list.SelectFilterItem/1703740177"] = [
        @consys.common.gwt.shared.bo.list.SelectFilterItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.bo.list.SelectFilterItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/bo/list/SelectFilterItem;),
      ];
    
    result["consys.common.gwt.shared.exception.AccessDeniedException/1167204195"] = [
        @consys.common.gwt.shared.exception.AccessDeniedException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.AccessDeniedException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/AccessDeniedException;),
      ];
    
    result["consys.common.gwt.shared.exception.ActionNotPermittedException/3251007496"] = [
        @consys.common.gwt.shared.exception.ActionNotPermittedException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.ActionNotPermittedException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/ActionNotPermittedException;),
      ];
    
    result["consys.common.gwt.shared.exception.AlreadyUsedObjectException/969880330"] = [
        @consys.common.gwt.shared.exception.AlreadyUsedObjectException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.AlreadyUsedObjectException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/AlreadyUsedObjectException;),
      ];
    
    result["consys.common.gwt.shared.exception.ApplicationSecurityException/3049159960"] = [
        @consys.common.gwt.shared.exception.ApplicationSecurityException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.ApplicationSecurityException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/ApplicationSecurityException;),
      ];
    
    result["consys.common.gwt.shared.exception.BadInputException/1385664719"] = [
        @consys.common.gwt.shared.exception.BadInputException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.BadInputException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/BadInputException;),
      ];
    
    result["consys.common.gwt.shared.exception.BundleCapacityException/1078987923"] = [
        @consys.common.gwt.shared.exception.BundleCapacityException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.BundleCapacityException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/BundleCapacityException;),
      ];
    
    result["consys.common.gwt.shared.exception.CouponExpiredException/1500553063"] = [
        @consys.common.gwt.shared.exception.CouponExpiredException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.CouponExpiredException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/CouponExpiredException;),
      ];
    
    result["consys.common.gwt.shared.exception.CouponInvalidException/2121701892"] = [
        @consys.common.gwt.shared.exception.CouponInvalidException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.CouponInvalidException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/CouponInvalidException;),
      ];
    
    result["consys.common.gwt.shared.exception.CouponUsedException/3640358826"] = [
        @consys.common.gwt.shared.exception.CouponUsedException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.CouponUsedException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/CouponUsedException;),
      ];
    
    result["consys.common.gwt.shared.exception.IbanException/2509790356"] = [
        @consys.common.gwt.shared.exception.IbanException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.IbanException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/IbanException;),
      ];
    
    result["consys.common.gwt.shared.exception.NoRecordsForAction/4106925861"] = [
        @consys.common.gwt.shared.exception.NoRecordsForAction_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.NoRecordsForAction_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/NoRecordsForAction;),
      ];
    
    result["consys.common.gwt.shared.exception.ReadOnlyException/1027177219"] = [
        @consys.common.gwt.shared.exception.ReadOnlyException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.ReadOnlyException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/ReadOnlyException;),
      ];
    
    result["consys.common.gwt.shared.exception.ServiceFailedException/753470088"] = [
        @consys.common.gwt.shared.exception.ServiceFailedException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.ServiceFailedException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/ServiceFailedException;),
      ];
    
    result["consys.common.gwt.shared.exception.UncaughtException/2447453934"] = [
        @consys.common.gwt.shared.exception.UncaughtException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.common.gwt.shared.exception.UncaughtException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/common/gwt/shared/exception/UncaughtException;),
      ];
    
    result["consys.event.common.gwt.client.action.BuyLicencesAction/2457726221"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.BuyLicencesAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/BuyLicencesAction;)
      ];
    
    result["consys.event.common.gwt.client.action.BuyProductAction/1636464571"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.BuyProductAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/BuyProductAction;)
      ];
    
    result["consys.event.common.gwt.client.action.CreateCommitteeAction/1829066117"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.CreateCommitteeAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/CreateCommitteeAction;)
      ];
    
    result["consys.event.common.gwt.client.action.CreateVideoRecordAction/2487774951"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.CreateVideoRecordAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/CreateVideoRecordAction;)
      ];
    
    result["consys.event.common.gwt.client.action.CurrentEventUserRightAction/3680682125"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.CurrentEventUserRightAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/CurrentEventUserRightAction;)
      ];
    
    result["consys.event.common.gwt.client.action.DeleteEventVideoAction/1617014314"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.DeleteEventVideoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/DeleteEventVideoAction;)
      ];
    
    result["consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction/2458211792"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/ListEventPaymentProfileDialogAction;)
      ];
    
    result["consys.event.common.gwt.client.action.ListTransfersAction/1335629181"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.ListTransfersAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/ListTransfersAction;)
      ];
    
    result["consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction/3174485668"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/ListUserPaymentProfileDialogAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadCommitteeAction/754520937"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadCommitteeAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadCommitteeAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadCommitteesAction/410866642"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadCommitteesAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadCommitteesAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadEventDetailsAction/4066168443"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadEventDetailsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadEventDetailsAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadEventImportantDatesAction/2499048303"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadEventImportantDatesAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadEventImportantDatesAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadEventPaymentDataDialogAction/1410160106"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadEventPaymentDataDialogAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadEventPaymentDataDialogAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadEventPaymentProfileAction/3381844095"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadEventPaymentProfileAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadEventPaymentProfileAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadEventPaymentSettingsAction/3706235994"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadEventPaymentSettingsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadEventPaymentSettingsAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadEventPaymentTransferDetailAction/672493872"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadEventPaymentTransferDetailAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadEventPaymentTransferDetailAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadEventSettingsAction/403677910"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadEventSettingsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadEventSettingsAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadLicenseInfoAction/780091922"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadLicenseInfoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadLicenseInfoAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadLicensePaymentDataAction/738646837"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadLicensePaymentDataAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadLicensePaymentDataAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadOuterEventInfoAction/3948321739"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadOuterEventInfoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadOuterEventInfoAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadUserEventRolesAction/82095300"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadUserEventRolesAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadUserEventRolesAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadUserPaymentProfileAction/4050937601"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadUserPaymentProfileAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadUserPaymentProfileAction;)
      ];
    
    result["consys.event.common.gwt.client.action.LoadUuidAction/1116859610"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.LoadUuidAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/LoadUuidAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction/669732894"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateBaseEventSettingsAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateCommitteeAction/649616722"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateCommitteeAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateCommitteeAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction/3452096483"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateDetailedEventSettingsAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction/3207189877"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateEventCommunitySettingsAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateEventLogoAction/468913291"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateEventLogoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateEventLogoAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction/2844473614"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateEventPaymentProfileAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateEventVideoAction/3330762456"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateEventVideoAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateEventVideoAction;)
      ];
    
    result["consys.event.common.gwt.client.action.UpdateUserEventAction/283955364"] = [
        ,
        ,
        @consys.event.common.gwt.client.action.UpdateUserEventAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/common/gwt/client/action/UpdateUserEventAction;)
      ];
    
    result["consys.event.common.gwt.client.action.exception.DeleteRelationException/1312688157"] = [
        @consys.event.common.gwt.client.action.exception.DeleteRelationException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.action.exception.DeleteRelationException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/action/exception/DeleteRelationException;),
      ];
    
    result["consys.event.common.gwt.client.action.exception.EventNotActivatedException/2505993486"] = [
        @consys.event.common.gwt.client.action.exception.EventNotActivatedException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.action.exception.EventNotActivatedException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/action/exception/EventNotActivatedException;),
      ];
    
    result["consys.event.common.gwt.client.action.exception.LicensePurchasedException/909708134"] = [
        @consys.event.common.gwt.client.action.exception.LicensePurchasedException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.action.exception.LicensePurchasedException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/action/exception/LicensePurchasedException;),
      ];
    
    result["consys.event.common.gwt.client.action.exception.ListHandlerException/3560129992"] = [
        @consys.event.common.gwt.client.action.exception.ListHandlerException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.action.exception.ListHandlerException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/action/exception/ListHandlerException;),
      ];
    
    result["consys.event.common.gwt.client.action.exception.ProfileNotFilledException/1948682418"] = [
        @consys.event.common.gwt.client.action.exception.ProfileNotFilledException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.action.exception.ProfileNotFilledException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/action/exception/ProfileNotFilledException;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientCommittee/3388156035"] = [
        @consys.event.common.gwt.client.bo.ClientCommittee_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientCommittee_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientCommittee;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientCommitteeThumb/2163790318"] = [
        @consys.event.common.gwt.client.bo.ClientCommitteeThumb_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientCommitteeThumb_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientCommitteeThumb;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientEventConfirmed/2523910825"] = [
        @consys.event.common.gwt.client.bo.ClientEventConfirmed_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientEventConfirmed_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientEventConfirmed;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientEventImportantDates/1480780464"] = [
        @consys.event.common.gwt.client.bo.ClientEventImportantDates_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientEventImportantDates_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientEventImportantDates;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientEventLicense/381401445"] = [
        @consys.event.common.gwt.client.bo.ClientEventLicense_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientEventLicense_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientEventLicense;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientOuterEventData/530338901"] = [
        @consys.event.common.gwt.client.bo.ClientOuterEventData_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientOuterEventData_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientOuterEventData;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientPaymentActivation/3633691781"] = [
        @consys.event.common.gwt.client.bo.ClientPaymentActivation_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientPaymentActivation_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientPaymentActivation;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientPaymentProfileDialog/1307470758"] = [
        @consys.event.common.gwt.client.bo.ClientPaymentProfileDialog_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientPaymentProfileDialog_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientPaymentProfileDialog;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientPaymentSettings/1590248268"] = [
        @consys.event.common.gwt.client.bo.ClientPaymentSettings_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientPaymentSettings_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientPaymentSettings;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientPaymentTransferDetail/3229643049"] = [
        @consys.event.common.gwt.client.bo.ClientPaymentTransferDetail_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientPaymentTransferDetail_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientPaymentTransferDetail;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientPaymentTransferHead/2854530465"] = [
        @consys.event.common.gwt.client.bo.ClientPaymentTransferHead_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientPaymentTransferHead_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientPaymentTransferHead;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientPaymentTransferItem/925603630"] = [
        @consys.event.common.gwt.client.bo.ClientPaymentTransferItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientPaymentTransferItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientPaymentTransferItem;),
      ];
    
    result["consys.event.common.gwt.client.bo.ClientPaymentTransferState/945212591"] = [
        @consys.event.common.gwt.client.bo.ClientPaymentTransferState_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.ClientPaymentTransferState_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/ClientPaymentTransferState;),
      ];
    
    result["consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem/2357723715"] = [
        @consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/list/ClientVideoFilesListItem;),
      ];
    
    result["consys.event.common.gwt.client.bo.list.VideoRecordState/177293679"] = [
        @consys.event.common.gwt.client.bo.list.VideoRecordState_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.common.gwt.client.bo.list.VideoRecordState_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/common/gwt/client/bo/list/VideoRecordState;),
      ];
    
    result["consys.event.registration.gwt.client.action.CancelOrderAction/1829177448"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CancelOrderAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CancelOrderAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CancelRegistrationAction/2006902933"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CancelRegistrationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CancelRegistrationAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CancelWaitingAction/2372800003"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CancelWaitingAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CancelWaitingAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ConfirmOrderAction/3311009954"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ConfirmOrderAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ConfirmOrderAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CreateCustomQuestionAction/608774970"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CreateCustomQuestionAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CreateCustomQuestionAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CreateDiscountCouponAction/4029542739"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CreateDiscountCouponAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CreateDiscountCouponAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CreateInternalRegistrationAction/1381582474"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CreateInternalRegistrationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CreateInternalRegistrationAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CreateRegistrationBundleAction/3203837736"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CreateRegistrationBundleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CreateRegistrationBundleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CreateRegistrationCycleAction/3857859775"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CreateRegistrationCycleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CreateRegistrationCycleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.CreateWaitingRecordAction/1758231146"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.CreateWaitingRecordAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/CreateWaitingRecordAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.DeleteCustomQuestionAction/494886801"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.DeleteCustomQuestionAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/DeleteCustomQuestionAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.DeleteDiscountCouponAction/1962356698"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.DeleteDiscountCouponAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/DeleteDiscountCouponAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction/2844180808"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/DeleteRegistrationBundleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.DeleteRegistrationCycleAction/1157061531"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.DeleteRegistrationCycleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/DeleteRegistrationCycleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction/1982068061"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/DuplicateRegistrationBundleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.GenerateDiscountKeyAction/721783666"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.GenerateDiscountKeyAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/GenerateDiscountKeyAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ListBundleFilterAction/4219774654"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ListBundleFilterAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ListBundleFilterAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction/2129569909"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ListBundlePriceSummaryAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ListBundlesAndQuestionsForUserAction/1948134984"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ListBundlesAndQuestionsForUserAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ListBundlesAndQuestionsForUserAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ListCustomQuestionsAction/101868081"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ListCustomQuestionsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ListCustomQuestionsAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ListOpenMainRegistrationForWaitingAction/1697620414"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ListOpenMainRegistrationForWaitingAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ListOpenMainRegistrationForWaitingAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ListQuestionsForUserAction/498686122"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ListQuestionsForUserAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ListQuestionsForUserAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.ListRegistrationSettingsAction/4014922778"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.ListRegistrationSettingsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/ListRegistrationSettingsAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction/2937625051"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadAvailableSubbundlesAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadCustomQuestionAction/1689974707"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadCustomQuestionAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadCustomQuestionAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadEditRegistrationAction/3359023832"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadEditRegistrationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadEditRegistrationAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction/2048823618"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadEditRegistrationByUserUuidAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadIsUserRegisteredAction/471099326"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadIsUserRegisteredAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadIsUserRegisteredAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction/1980584353"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadOrderPaymentProfileAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadOuterRegisterAction/3590539745"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadOuterRegisterAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadOuterRegisterAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadProductDiscountAction/3152443104"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadProductDiscountAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadProductDiscountAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction/3894702678"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadRegisterUserPaymentDialogAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadRegistrationBundleAction/942858780"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadRegistrationBundleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadRegistrationBundleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadRegistrationCycleAction/2495125717"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadRegistrationCycleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadRegistrationCycleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadUserRegistrationAction/2775012220"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadUserRegistrationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadUserRegistrationAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction/4095147144"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadUserRegistrationDetailsAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.LoadWaitingRegistrationAction/388553139"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.LoadWaitingRegistrationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/LoadWaitingRegistrationAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.RegisterUserAction/1377229364"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.RegisterUserAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/RegisterUserAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.SendDiscountCodeAction/2423956257"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.SendDiscountCodeAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/SendDiscountCodeAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateCheckInAction/1515885232"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateCheckInAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateCheckInAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateCustomQuestionAction/3192230380"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateCustomQuestionAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateCustomQuestionAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction/3252194592"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateCustomQuestionOrderAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateDiscountCouponAction/2338856756"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateDiscountCouponAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateDiscountCouponAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateRegistrationAction/3337444314"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateRegistrationAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateRegistrationAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction/3295315318"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateRegistrationBundleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction/1208316149"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateRegistrationBundlePositionAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.UpdateRegistrationCycleAction/1853014790"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.UpdateRegistrationCycleAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/UpdateRegistrationCycleAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction/3289249263"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/managed/AddNewParticipantsAction;)
      ];
    
    result["consys.event.registration.gwt.client.action.managed.ListManagedRegistrationsAction/648164335"] = [
        ,
        ,
        @consys.event.registration.gwt.client.action.managed.ListManagedRegistrationsAction_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/action/managed/ListManagedRegistrationsAction;)
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientAnswer/2607646166"] = [
        @consys.event.registration.gwt.client.bo.ClientAnswer_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientAnswer_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientAnswer;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientBundle/1391364855"] = [
        @consys.event.registration.gwt.client.bo.ClientBundle_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientBundle_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientBundle;),
        @consys.event.registration.gwt.client.bo.ClientBundle_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/bo/ClientBundle;)
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientBundlePriceSummary/2374826208"] = [
        @consys.event.registration.gwt.client.bo.ClientBundlePriceSummary_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientBundlePriceSummary_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientBundlePriceSummary;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions/657623217"] = [
        @consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientBundlesAndQuestions;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientCheckInListItem/1521030261"] = [
        @consys.event.registration.gwt.client.bo.ClientCheckInListItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientCheckInListItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientCheckInListItem;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientCouponListItem/151138295"] = [
        @consys.event.registration.gwt.client.bo.ClientCouponListItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientCouponListItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientCouponListItem;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientCreateBundle/2789112226"] = [
        @consys.event.registration.gwt.client.bo.ClientCreateBundle_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientCreateBundle_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientCreateBundle;),
        @consys.event.registration.gwt.client.bo.ClientCreateBundle_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/bo/ClientCreateBundle;)
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientCreateQuestion/2394187268"] = [
        ,
        ,
        @consys.event.registration.gwt.client.bo.ClientCreateQuestion_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/bo/ClientCreateQuestion;)
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientCustomQuestion/543695670"] = [
        @consys.event.registration.gwt.client.bo.ClientCustomQuestion_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientCustomQuestion_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientCustomQuestion;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem/4033570353"] = [
        @consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientInternalRegistrationListItem;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientListParticipantItem/847210265"] = [
        @consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientListParticipantItem;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientListRegistrationItem/3480461047"] = [
        @consys.event.registration.gwt.client.bo.ClientListRegistrationItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientListRegistrationItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientListRegistrationItem;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientListRegistrationItem$ClientRegistrationState/1514155502"] = [
        @consys.event.registration.gwt.client.bo.ClientListRegistrationItem_ClientRegistrationState_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientListRegistrationItem_ClientRegistrationState_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientListRegistrationItem$ClientRegistrationState;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientOuterRegisterData/3702223120"] = [
        @consys.event.registration.gwt.client.bo.ClientOuterRegisterData_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientOuterRegisterData_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientOuterRegisterData;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientQuestions/1122454633"] = [
        @consys.event.registration.gwt.client.bo.ClientQuestions_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientQuestions_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientQuestions;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientRegistration/2747665748"] = [
        @consys.event.registration.gwt.client.bo.ClientRegistration_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientRegistration_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientRegistration;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientRegistration$State/887166233"] = [
        @consys.event.registration.gwt.client.bo.ClientRegistration_State_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientRegistration_State_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientRegistration$State;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientRegistrationCycle/2544946280"] = [
        @consys.event.registration.gwt.client.bo.ClientRegistrationCycle_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientRegistrationCycle_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientRegistrationCycle;),
        @consys.event.registration.gwt.client.bo.ClientRegistrationCycle_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/bo/ClientRegistrationCycle;)
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientRegistrationOrder/1604244302"] = [
        @consys.event.registration.gwt.client.bo.ClientRegistrationOrder_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientRegistrationOrder_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientRegistrationOrder;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem/3102277744"] = [
        @consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientRegistrationOrderItem;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientRegistrationPack/1615415885"] = [
        @consys.event.registration.gwt.client.bo.ClientRegistrationPack_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientRegistrationPack_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientRegistrationPack;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientRegistrationResult/4176758750"] = [
        @consys.event.registration.gwt.client.bo.ClientRegistrationResult_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientRegistrationResult_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientRegistrationResult;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails/3913187484"] = [
        @consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientUserRegistrationDetails;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo/3111642701"] = [
        @consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientWaitingBundleInfo;),
      ];
    
    result["consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo/3937546841"] = [
        @consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/ClientWaitingBundlesInfo;),
      ];
    
    result["consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations/960390909"] = [
        @consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/bo/managed/ClientManagedRegistrations;),
      ];
    
    result["consys.event.registration.gwt.client.bo.managed.NewParticipant/1845375661"] = [
        ,
        ,
        @consys.event.registration.gwt.client.bo.managed.NewParticipant_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Lconsys/event/registration/gwt/client/bo/managed/NewParticipant;)
      ];
    
    result["consys.event.registration.gwt.client.module.exception.BundleUniquityException/3984629269"] = [
        @consys.event.registration.gwt.client.module.exception.BundleUniquityException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.module.exception.BundleUniquityException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/module/exception/BundleUniquityException;),
      ];
    
    result["consys.event.registration.gwt.client.module.exception.CyclesIntersectionException/2320819921"] = [
        @consys.event.registration.gwt.client.module.exception.CyclesIntersectionException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.module.exception.CyclesIntersectionException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/module/exception/CyclesIntersectionException;),
      ];
    
    result["consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException/1928288592"] = [
        @consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/module/exception/RegistrationsInCycleException;),
      ];
    
    result["consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException/4287209176"] = [
        @consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException_FieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Lconsys/event/registration/gwt/client/module/exception/UserAlreadyRegistredException;),
      ];
    
    result["[I/2970817851"] = [
        ,
        ,
        @com.google.gwt.user.client.rpc.core.int_Array_Rank_1_FieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;[I)
      ];
    
    result["java.lang.Integer/3438268394"] = [
        @com.google.gwt.user.client.rpc.core.java.lang.Integer_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.lang.Integer_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/lang/Integer;),
        @com.google.gwt.user.client.rpc.core.java.lang.Integer_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/lang/Integer;)
      ];
    
    result["java.lang.Long/4227064769"] = [
        @com.google.gwt.user.client.rpc.core.java.lang.Long_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.lang.Long_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/lang/Long;),
        @com.google.gwt.user.client.rpc.core.java.lang.Long_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/lang/Long;)
      ];
    
    result["java.lang.String/2004016611"] = [
        @com.google.gwt.user.client.rpc.core.java.lang.String_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.lang.String_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/lang/String;),
        @com.google.gwt.user.client.rpc.core.java.lang.String_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/lang/String;)
      ];
    
    result["java.sql.Date/730999118"] = [
        @com.google.gwt.user.client.rpc.core.java.sql.Date_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.sql.Date_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/sql/Date;),
        @com.google.gwt.user.client.rpc.core.java.sql.Date_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/sql/Date;)
      ];
    
    result["java.sql.Time/1816797103"] = [
        @com.google.gwt.user.client.rpc.core.java.sql.Time_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.sql.Time_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/sql/Time;),
        @com.google.gwt.user.client.rpc.core.java.sql.Time_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/sql/Time;)
      ];
    
    result["java.sql.Timestamp/3040052672"] = [
        @com.google.gwt.user.client.rpc.core.java.sql.Timestamp_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.sql.Timestamp_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/sql/Timestamp;),
        @com.google.gwt.user.client.rpc.core.java.sql.Timestamp_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/sql/Timestamp;)
      ];
    
    result["java.util.ArrayList/4159755760"] = [
        @com.google.gwt.user.client.rpc.core.java.util.ArrayList_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.util.ArrayList_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/util/ArrayList;),
        @com.google.gwt.user.client.rpc.core.java.util.ArrayList_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/util/ArrayList;)
      ];
    
    result["java.util.Date/3385151746"] = [
        @com.google.gwt.user.client.rpc.core.java.util.Date_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.util.Date_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/util/Date;),
        @com.google.gwt.user.client.rpc.core.java.util.Date_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/util/Date;)
      ];
    
    result["java.util.HashMap/1797211028"] = [
        @com.google.gwt.user.client.rpc.core.java.util.HashMap_FieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.util.HashMap_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/util/HashMap;),
        @com.google.gwt.user.client.rpc.core.java.util.HashMap_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/util/HashMap;)
      ];
    
    result["java.util.LinkedHashMap/3008245022"] = [
        @com.google.gwt.user.client.rpc.core.java.util.LinkedHashMap_CustomFieldSerializer::instantiate(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;),
        @com.google.gwt.user.client.rpc.core.java.util.LinkedHashMap_CustomFieldSerializer::deserialize(Lcom/google/gwt/user/client/rpc/SerializationStreamReader;Ljava/util/LinkedHashMap;),
        @com.google.gwt.user.client.rpc.core.java.util.LinkedHashMap_CustomFieldSerializer::serialize(Lcom/google/gwt/user/client/rpc/SerializationStreamWriter;Ljava/util/LinkedHashMap;)
      ];
    
    return result;
  }-*/;
  
  @SuppressWarnings("deprecation")
  @GwtScriptOnly
  private static native JsArrayString loadSignaturesNative() /*-{
    var result = [];
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@com.allen_sauer.gwt.dnd.client.DragHandlerCollection::class)] = "com.allen_sauer.gwt.dnd.client.DragHandlerCollection/3996089253";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@com.google.gwt.i18n.client.impl.DateRecord::class)] = "com.google.gwt.i18n.client.impl.DateRecord/3220471373";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException::class)] = "com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException/3936916533";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@com.google.gwt.user.client.rpc.RpcTokenException::class)] = "com.google.gwt.user.client.rpc.RpcTokenException/2345075298";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@com.google.gwt.user.client.rpc.XsrfToken::class)] = "com.google.gwt.user.client.rpc.XsrfToken/4254043109";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.action.CreateEventAction::class)] = "consys.admin.event.gwt.client.action.CreateEventAction/1827866985";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.action.EnterEventAction::class)] = "consys.admin.event.gwt.client.action.EnterEventAction/2216433768";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.action.ListAllEventsAction::class)] = "consys.admin.event.gwt.client.action.ListAllEventsAction/978835033";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.action.ListMyEventInvitationsAction::class)] = "consys.admin.event.gwt.client.action.ListMyEventInvitationsAction/2767998444";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.action.ListMyEventsAction::class)] = "consys.admin.event.gwt.client.action.ListMyEventsAction/2492764888";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.action.LoadEventInvitationAction::class)] = "consys.admin.event.gwt.client.action.LoadEventInvitationAction/408415773";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.action.ResolveInvitationAction::class)] = "consys.admin.event.gwt.client.action.ResolveInvitationAction/234308546";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.bo.EventInvitation::class)] = "consys.admin.event.gwt.client.bo.EventInvitation/3368445619";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.bo.EventInvitationListResult::class)] = "consys.admin.event.gwt.client.bo.EventInvitationListResult/414401816";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb::class)] = "consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb/1522199903";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.bo.EventListResult::class)] = "consys.admin.event.gwt.client.bo.EventListResult/2287712101";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.event.gwt.client.bo.InvitationState::class)] = "consys.admin.event.gwt.client.bo.InvitationState/2961317799";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.ActivateUserAction::class)] = "consys.admin.user.gwt.client.action.ActivateUserAction/2590092480";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.ActivateUserRequestAction::class)] = "consys.admin.user.gwt.client.action.ActivateUserRequestAction/964788699";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.ConfirmInvitationAction::class)] = "consys.admin.user.gwt.client.action.ConfirmInvitationAction/1171351160";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.CreateUserAction::class)] = "consys.admin.user.gwt.client.action.CreateUserAction/2711669843";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.CreateUserOrgAction::class)] = "consys.admin.user.gwt.client.action.CreateUserOrgAction/2468010342";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.DeleteUserOrgAction::class)] = "consys.admin.user.gwt.client.action.DeleteUserOrgAction/488921357";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.ListOrganizationTypesAction::class)] = "consys.admin.user.gwt.client.action.ListOrganizationTypesAction/1279545806";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction::class)] = "consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction/247338632";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.ListUserOrgsAction::class)] = "consys.admin.user.gwt.client.action.ListUserOrgsAction/2932916983";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.LoadInvitationAction::class)] = "consys.admin.user.gwt.client.action.LoadInvitationAction/3977156374";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.LoadLoggedUserAction::class)] = "consys.admin.user.gwt.client.action.LoadLoggedUserAction/1776801550";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.LoadOrganizationAction::class)] = "consys.admin.user.gwt.client.action.LoadOrganizationAction/968601743";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.LoadPersonalInfoAction::class)] = "consys.admin.user.gwt.client.action.LoadPersonalInfoAction/3291455903";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.LoadPrivateUserOrgAction::class)] = "consys.admin.user.gwt.client.action.LoadPrivateUserOrgAction/116241521";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.LoadUserOrgAction::class)] = "consys.admin.user.gwt.client.action.LoadUserOrgAction/342827780";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.LostPasswordAction::class)] = "consys.admin.user.gwt.client.action.LostPasswordAction/1906904301";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.UpdateLostPasswordAction::class)] = "consys.admin.user.gwt.client.action.UpdateLostPasswordAction/172872871";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.UpdatePasswordAction::class)] = "consys.admin.user.gwt.client.action.UpdatePasswordAction/1958868356";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.UpdatePersonalInfoAction::class)] = "consys.admin.user.gwt.client.action.UpdatePersonalInfoAction/956786685";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.UpdatePrivateOrgAction::class)] = "consys.admin.user.gwt.client.action.UpdatePrivateOrgAction/3602099883";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.UpdateUserAction::class)] = "consys.admin.user.gwt.client.action.UpdateUserAction/3014084833";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.UpdateUserOrgAction::class)] = "consys.admin.user.gwt.client.action.UpdateUserOrgAction/2926356051";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.exception.ActivationKeyIsInvitationException::class)] = "consys.admin.user.gwt.client.action.exception.ActivationKeyIsInvitationException/1369378291";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException::class)] = "consys.admin.user.gwt.client.action.exception.ActivationKeyNotFoundException/31914361";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.exception.UserNotExistsException::class)] = "consys.admin.user.gwt.client.action.exception.UserNotExistsException/296534185";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException::class)] = "consys.admin.user.gwt.client.action.exception.UserOrganizationUuidException/1662999906";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.bo.ClientUserOrg::class)] = "consys.admin.user.gwt.client.bo.ClientUserOrg/358888994";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::class)] = "consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb/1778496328";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.constants.currency.CurrencyEnum::class)] = "consys.common.constants.currency.CurrencyEnum/4158296137";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.constants.sso.SSO::class)] = "consys.common.constants.sso.SSO/225063008";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.CloneEventAction::class)] = "consys.common.gwt.client.rpc.action.CloneEventAction/3465521815";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::class)] = "consys.common.gwt.client.rpc.action.CreateOuterRegisterAction/619093887";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.InitializeSsoAction::class)] = "consys.common.gwt.client.rpc.action.InitializeSsoAction/2485224978";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.IsUserLoggedAction::class)] = "consys.common.gwt.client.rpc.action.IsUserLoggedAction/2650918293";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.ListLocationAction::class)] = "consys.common.gwt.client.rpc.action.ListLocationAction/3937814474";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.ListUsStateAction::class)] = "consys.common.gwt.client.rpc.action.ListUsStateAction/2658509530";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction::class)] = "consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction/256839086";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.ListUserThumbsAction::class)] = "consys.common.gwt.client.rpc.action.ListUserThumbsAction/1573672824";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction::class)] = "consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction/2363772973";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.LoadUserInfoAction::class)] = "consys.common.gwt.client.rpc.action.LoadUserInfoAction/1248767124";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.LoadUserThumbAction::class)] = "consys.common.gwt.client.rpc.action.LoadUserThumbAction/2405829453";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.action.UpdateEventPropertyAction::class)] = "consys.common.gwt.client.rpc.action.UpdateEventPropertyAction/565936556";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.ArrayListResult::class)] = "consys.common.gwt.client.rpc.result.ArrayListResult/358078478";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.BooleanResult::class)] = "consys.common.gwt.client.rpc.result.BooleanResult/2020343530";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.BoxItem::class)] = "consys.common.gwt.client.rpc.result.BoxItem/367386518";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.DateResult::class)] = "consys.common.gwt.client.rpc.result.DateResult/2396398714";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.LongResult::class)] = "consys.common.gwt.client.rpc.result.LongResult/4115447146";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.SelectBoxData::class)] = "consys.common.gwt.client.rpc.result.SelectBoxData/4130713844";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.SelectBoxItem::class)] = "consys.common.gwt.client.rpc.result.SelectBoxItem/1180891837";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.SelectBoxResult::class)] = "consys.common.gwt.client.rpc.result.SelectBoxResult/2101284186";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.StringResult::class)] = "consys.common.gwt.client.rpc.result.StringResult/4293830892";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.rpc.result.VoidResult::class)] = "consys.common.gwt.client.rpc.result.VoidResult/1277313421";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::class)] = "consys.common.gwt.client.ui.comp.list.ListDataSourceRequest/3121066266";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.ui.comp.list.ListDataSourceResult::class)] = "consys.common.gwt.client.ui.comp.list.ListDataSourceResult/1216131181";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.ui.comp.list.item.DataListIdItem::class)] = "consys.common.gwt.client.ui.comp.list.item.DataListIdItem/4072070083";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.ui.comp.list.item.DataListItem::class)] = "consys.common.gwt.client.ui.comp.list.item.DataListItem/1685635481";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.ui.comp.list.item.DataListUuidItem::class)] = "consys.common.gwt.client.ui.comp.list.item.DataListUuidItem/3604094547";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.client.widget.exception.UserExistsException::class)] = "consys.common.gwt.client.widget.exception.UserExistsException/1727502461";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.action.CheckCorporationAction::class)] = "consys.common.gwt.shared.action.CheckCorporationAction/3219877223";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.action.exceptions.PermissionException::class)] = "consys.common.gwt.shared.action.exceptions.PermissionException/3958506368";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientAddress::class)] = "consys.common.gwt.shared.bo.ClientAddress/4006816371";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientCurrentEvent::class)] = "consys.common.gwt.shared.bo.ClientCurrentEvent/2716808044";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientDate::class)] = "consys.common.gwt.shared.bo.ClientDate/4227272410";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientDateTime::class)] = "consys.common.gwt.shared.bo.ClientDateTime/1023572743";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientEvent::class)] = "consys.common.gwt.shared.bo.ClientEvent/2767083701";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientEventPaymentProfile::class)] = "consys.common.gwt.shared.bo.ClientEventPaymentProfile/745497340";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientEventThumb::class)] = "consys.common.gwt.shared.bo.ClientEventThumb/1460043612";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientEventType::class)] = "consys.common.gwt.shared.bo.ClientEventType/738359947";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientInvitedUser::class)] = "consys.common.gwt.shared.bo.ClientInvitedUser/479109913";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientOrganization::class)] = "consys.common.gwt.shared.bo.ClientOrganization/1399395921";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientOrganizationThumb::class)] = "consys.common.gwt.shared.bo.ClientOrganizationThumb/1831128066";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientPaymentOrderData::class)] = "consys.common.gwt.shared.bo.ClientPaymentOrderData/2203788992";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientPaymentProduct::class)] = "consys.common.gwt.shared.bo.ClientPaymentProduct/1115566039";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientPaymentProfileThumb::class)] = "consys.common.gwt.shared.bo.ClientPaymentProfileThumb/2893657349";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientPersonalInfo::class)] = "consys.common.gwt.shared.bo.ClientPersonalInfo/3166076476";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientRegistration::class)] = "consys.common.gwt.shared.bo.ClientRegistration/4240806994";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientSsoUserDetails::class)] = "consys.common.gwt.shared.bo.ClientSsoUserDetails/3828159097";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientTabItem::class)] = "consys.common.gwt.shared.bo.ClientTabItem/3927942398";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientTime::class)] = "consys.common.gwt.shared.bo.ClientTime/258571924";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientUser::class)] = "consys.common.gwt.shared.bo.ClientUser/336253265";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientUserCommonInfo::class)] = "consys.common.gwt.shared.bo.ClientUserCommonInfo/1263823186";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientUserPaymentProfile::class)] = "consys.common.gwt.shared.bo.ClientUserPaymentProfile/678264809";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientUserThumb::class)] = "consys.common.gwt.shared.bo.ClientUserThumb/1899881068";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb::class)] = "consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb/1176241876";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.EventSettings::class)] = "consys.common.gwt.shared.bo.EventSettings/1264626799";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.EventUser::class)] = "consys.common.gwt.shared.bo.EventUser/3547461302";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.InvitedUser::class)] = "consys.common.gwt.shared.bo.InvitedUser/762237250";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.Monetary::class)] = "consys.common.gwt.shared.bo.Monetary/2484286945";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.ProductThumb::class)] = "consys.common.gwt.shared.bo.ProductThumb/1999943306";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.list.Filter::class)] = "consys.common.gwt.shared.bo.list.Filter/927127043";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.bo.list.SelectFilterItem::class)] = "consys.common.gwt.shared.bo.list.SelectFilterItem/1703740177";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.AccessDeniedException::class)] = "consys.common.gwt.shared.exception.AccessDeniedException/1167204195";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.ActionNotPermittedException::class)] = "consys.common.gwt.shared.exception.ActionNotPermittedException/3251007496";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.AlreadyUsedObjectException::class)] = "consys.common.gwt.shared.exception.AlreadyUsedObjectException/969880330";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.ApplicationSecurityException::class)] = "consys.common.gwt.shared.exception.ApplicationSecurityException/3049159960";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.BadInputException::class)] = "consys.common.gwt.shared.exception.BadInputException/1385664719";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.BundleCapacityException::class)] = "consys.common.gwt.shared.exception.BundleCapacityException/1078987923";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.CouponExpiredException::class)] = "consys.common.gwt.shared.exception.CouponExpiredException/1500553063";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.CouponInvalidException::class)] = "consys.common.gwt.shared.exception.CouponInvalidException/2121701892";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.CouponUsedException::class)] = "consys.common.gwt.shared.exception.CouponUsedException/3640358826";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.IbanException::class)] = "consys.common.gwt.shared.exception.IbanException/2509790356";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.NoRecordsForAction::class)] = "consys.common.gwt.shared.exception.NoRecordsForAction/4106925861";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.ReadOnlyException::class)] = "consys.common.gwt.shared.exception.ReadOnlyException/1027177219";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.ServiceFailedException::class)] = "consys.common.gwt.shared.exception.ServiceFailedException/753470088";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.common.gwt.shared.exception.UncaughtException::class)] = "consys.common.gwt.shared.exception.UncaughtException/2447453934";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.BuyLicencesAction::class)] = "consys.event.common.gwt.client.action.BuyLicencesAction/2457726221";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.BuyProductAction::class)] = "consys.event.common.gwt.client.action.BuyProductAction/1636464571";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.CreateCommitteeAction::class)] = "consys.event.common.gwt.client.action.CreateCommitteeAction/1829066117";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.CreateVideoRecordAction::class)] = "consys.event.common.gwt.client.action.CreateVideoRecordAction/2487774951";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.CurrentEventUserRightAction::class)] = "consys.event.common.gwt.client.action.CurrentEventUserRightAction/3680682125";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.DeleteEventVideoAction::class)] = "consys.event.common.gwt.client.action.DeleteEventVideoAction/1617014314";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction::class)] = "consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction/2458211792";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.ListTransfersAction::class)] = "consys.event.common.gwt.client.action.ListTransfersAction/1335629181";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction::class)] = "consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction/3174485668";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadCommitteeAction::class)] = "consys.event.common.gwt.client.action.LoadCommitteeAction/754520937";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadCommitteesAction::class)] = "consys.event.common.gwt.client.action.LoadCommitteesAction/410866642";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadEventDetailsAction::class)] = "consys.event.common.gwt.client.action.LoadEventDetailsAction/4066168443";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadEventImportantDatesAction::class)] = "consys.event.common.gwt.client.action.LoadEventImportantDatesAction/2499048303";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadEventPaymentDataDialogAction::class)] = "consys.event.common.gwt.client.action.LoadEventPaymentDataDialogAction/1410160106";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadEventPaymentProfileAction::class)] = "consys.event.common.gwt.client.action.LoadEventPaymentProfileAction/3381844095";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadEventPaymentSettingsAction::class)] = "consys.event.common.gwt.client.action.LoadEventPaymentSettingsAction/3706235994";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadEventPaymentTransferDetailAction::class)] = "consys.event.common.gwt.client.action.LoadEventPaymentTransferDetailAction/672493872";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadEventSettingsAction::class)] = "consys.event.common.gwt.client.action.LoadEventSettingsAction/403677910";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadLicenseInfoAction::class)] = "consys.event.common.gwt.client.action.LoadLicenseInfoAction/780091922";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadLicensePaymentDataAction::class)] = "consys.event.common.gwt.client.action.LoadLicensePaymentDataAction/738646837";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadOuterEventInfoAction::class)] = "consys.event.common.gwt.client.action.LoadOuterEventInfoAction/3948321739";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadUserEventRolesAction::class)] = "consys.event.common.gwt.client.action.LoadUserEventRolesAction/82095300";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadUserPaymentProfileAction::class)] = "consys.event.common.gwt.client.action.LoadUserPaymentProfileAction/4050937601";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.LoadUuidAction::class)] = "consys.event.common.gwt.client.action.LoadUuidAction/1116859610";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::class)] = "consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction/669732894";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateCommitteeAction::class)] = "consys.event.common.gwt.client.action.UpdateCommitteeAction/649616722";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::class)] = "consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction/3452096483";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::class)] = "consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction/3207189877";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateEventLogoAction::class)] = "consys.event.common.gwt.client.action.UpdateEventLogoAction/468913291";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction::class)] = "consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction/2844473614";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateEventVideoAction::class)] = "consys.event.common.gwt.client.action.UpdateEventVideoAction/3330762456";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.UpdateUserEventAction::class)] = "consys.event.common.gwt.client.action.UpdateUserEventAction/283955364";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.exception.DeleteRelationException::class)] = "consys.event.common.gwt.client.action.exception.DeleteRelationException/1312688157";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.exception.EventNotActivatedException::class)] = "consys.event.common.gwt.client.action.exception.EventNotActivatedException/2505993486";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.exception.LicensePurchasedException::class)] = "consys.event.common.gwt.client.action.exception.LicensePurchasedException/909708134";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.exception.ListHandlerException::class)] = "consys.event.common.gwt.client.action.exception.ListHandlerException/3560129992";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.action.exception.ProfileNotFilledException::class)] = "consys.event.common.gwt.client.action.exception.ProfileNotFilledException/1948682418";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientCommittee::class)] = "consys.event.common.gwt.client.bo.ClientCommittee/3388156035";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientCommitteeThumb::class)] = "consys.event.common.gwt.client.bo.ClientCommitteeThumb/2163790318";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientEventConfirmed::class)] = "consys.event.common.gwt.client.bo.ClientEventConfirmed/2523910825";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientEventImportantDates::class)] = "consys.event.common.gwt.client.bo.ClientEventImportantDates/1480780464";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientEventLicense::class)] = "consys.event.common.gwt.client.bo.ClientEventLicense/381401445";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientOuterEventData::class)] = "consys.event.common.gwt.client.bo.ClientOuterEventData/530338901";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientPaymentActivation::class)] = "consys.event.common.gwt.client.bo.ClientPaymentActivation/3633691781";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientPaymentProfileDialog::class)] = "consys.event.common.gwt.client.bo.ClientPaymentProfileDialog/1307470758";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientPaymentSettings::class)] = "consys.event.common.gwt.client.bo.ClientPaymentSettings/1590248268";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientPaymentTransferDetail::class)] = "consys.event.common.gwt.client.bo.ClientPaymentTransferDetail/3229643049";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::class)] = "consys.event.common.gwt.client.bo.ClientPaymentTransferHead/2854530465";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::class)] = "consys.event.common.gwt.client.bo.ClientPaymentTransferItem/925603630";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.ClientPaymentTransferState::class)] = "consys.event.common.gwt.client.bo.ClientPaymentTransferState/945212591";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::class)] = "consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem/2357723715";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.common.gwt.client.bo.list.VideoRecordState::class)] = "consys.event.common.gwt.client.bo.list.VideoRecordState/177293679";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CancelOrderAction::class)] = "consys.event.registration.gwt.client.action.CancelOrderAction/1829177448";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CancelRegistrationAction::class)] = "consys.event.registration.gwt.client.action.CancelRegistrationAction/2006902933";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CancelWaitingAction::class)] = "consys.event.registration.gwt.client.action.CancelWaitingAction/2372800003";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ConfirmOrderAction::class)] = "consys.event.registration.gwt.client.action.ConfirmOrderAction/3311009954";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CreateCustomQuestionAction::class)] = "consys.event.registration.gwt.client.action.CreateCustomQuestionAction/608774970";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::class)] = "consys.event.registration.gwt.client.action.CreateDiscountCouponAction/4029542739";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::class)] = "consys.event.registration.gwt.client.action.CreateInternalRegistrationAction/1381582474";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CreateRegistrationBundleAction::class)] = "consys.event.registration.gwt.client.action.CreateRegistrationBundleAction/3203837736";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CreateRegistrationCycleAction::class)] = "consys.event.registration.gwt.client.action.CreateRegistrationCycleAction/3857859775";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.CreateWaitingRecordAction::class)] = "consys.event.registration.gwt.client.action.CreateWaitingRecordAction/1758231146";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.DeleteCustomQuestionAction::class)] = "consys.event.registration.gwt.client.action.DeleteCustomQuestionAction/494886801";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.DeleteDiscountCouponAction::class)] = "consys.event.registration.gwt.client.action.DeleteDiscountCouponAction/1962356698";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction::class)] = "consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction/2844180808";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.DeleteRegistrationCycleAction::class)] = "consys.event.registration.gwt.client.action.DeleteRegistrationCycleAction/1157061531";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction::class)] = "consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction/1982068061";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.GenerateDiscountKeyAction::class)] = "consys.event.registration.gwt.client.action.GenerateDiscountKeyAction/721783666";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ListBundleFilterAction::class)] = "consys.event.registration.gwt.client.action.ListBundleFilterAction/4219774654";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction::class)] = "consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction/2129569909";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ListBundlesAndQuestionsForUserAction::class)] = "consys.event.registration.gwt.client.action.ListBundlesAndQuestionsForUserAction/1948134984";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ListCustomQuestionsAction::class)] = "consys.event.registration.gwt.client.action.ListCustomQuestionsAction/101868081";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ListOpenMainRegistrationForWaitingAction::class)] = "consys.event.registration.gwt.client.action.ListOpenMainRegistrationForWaitingAction/1697620414";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ListQuestionsForUserAction::class)] = "consys.event.registration.gwt.client.action.ListQuestionsForUserAction/498686122";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.ListRegistrationSettingsAction::class)] = "consys.event.registration.gwt.client.action.ListRegistrationSettingsAction/4014922778";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction::class)] = "consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction/2937625051";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadCustomQuestionAction::class)] = "consys.event.registration.gwt.client.action.LoadCustomQuestionAction/1689974707";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadEditRegistrationAction::class)] = "consys.event.registration.gwt.client.action.LoadEditRegistrationAction/3359023832";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction::class)] = "consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction/2048823618";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadIsUserRegisteredAction::class)] = "consys.event.registration.gwt.client.action.LoadIsUserRegisteredAction/471099326";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction::class)] = "consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction/1980584353";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadOuterRegisterAction::class)] = "consys.event.registration.gwt.client.action.LoadOuterRegisterAction/3590539745";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadProductDiscountAction::class)] = "consys.event.registration.gwt.client.action.LoadProductDiscountAction/3152443104";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction::class)] = "consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction/3894702678";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadRegistrationBundleAction::class)] = "consys.event.registration.gwt.client.action.LoadRegistrationBundleAction/942858780";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadRegistrationCycleAction::class)] = "consys.event.registration.gwt.client.action.LoadRegistrationCycleAction/2495125717";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadUserRegistrationAction::class)] = "consys.event.registration.gwt.client.action.LoadUserRegistrationAction/2775012220";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction::class)] = "consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction/4095147144";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.LoadWaitingRegistrationAction::class)] = "consys.event.registration.gwt.client.action.LoadWaitingRegistrationAction/388553139";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.RegisterUserAction::class)] = "consys.event.registration.gwt.client.action.RegisterUserAction/1377229364";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.SendDiscountCodeAction::class)] = "consys.event.registration.gwt.client.action.SendDiscountCodeAction/2423956257";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateCheckInAction::class)] = "consys.event.registration.gwt.client.action.UpdateCheckInAction/1515885232";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateCustomQuestionAction::class)] = "consys.event.registration.gwt.client.action.UpdateCustomQuestionAction/3192230380";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction::class)] = "consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction/3252194592";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateDiscountCouponAction::class)] = "consys.event.registration.gwt.client.action.UpdateDiscountCouponAction/2338856756";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateRegistrationAction::class)] = "consys.event.registration.gwt.client.action.UpdateRegistrationAction/3337444314";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction::class)] = "consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction/3295315318";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction::class)] = "consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction/1208316149";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.UpdateRegistrationCycleAction::class)] = "consys.event.registration.gwt.client.action.UpdateRegistrationCycleAction/1853014790";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction::class)] = "consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction/3289249263";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.action.managed.ListManagedRegistrationsAction::class)] = "consys.event.registration.gwt.client.action.managed.ListManagedRegistrationsAction/648164335";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientAnswer::class)] = "consys.event.registration.gwt.client.bo.ClientAnswer/2607646166";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientBundle::class)] = "consys.event.registration.gwt.client.bo.ClientBundle/1391364855";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientBundlePriceSummary::class)] = "consys.event.registration.gwt.client.bo.ClientBundlePriceSummary/2374826208";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions::class)] = "consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions/657623217";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientCheckInListItem::class)] = "consys.event.registration.gwt.client.bo.ClientCheckInListItem/1521030261";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientCouponListItem::class)] = "consys.event.registration.gwt.client.bo.ClientCouponListItem/151138295";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientCreateBundle::class)] = "consys.event.registration.gwt.client.bo.ClientCreateBundle/2789112226";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientCreateQuestion::class)] = "consys.event.registration.gwt.client.bo.ClientCreateQuestion/2394187268";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientCustomQuestion::class)] = "consys.event.registration.gwt.client.bo.ClientCustomQuestion/543695670";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem::class)] = "consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem/4033570353";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientListParticipantItem::class)] = "consys.event.registration.gwt.client.bo.ClientListParticipantItem/847210265";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::class)] = "consys.event.registration.gwt.client.bo.ClientListRegistrationItem/3480461047";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState::class)] = "consys.event.registration.gwt.client.bo.ClientListRegistrationItem$ClientRegistrationState/1514155502";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientOuterRegisterData::class)] = "consys.event.registration.gwt.client.bo.ClientOuterRegisterData/3702223120";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientQuestions::class)] = "consys.event.registration.gwt.client.bo.ClientQuestions/1122454633";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientRegistration::class)] = "consys.event.registration.gwt.client.bo.ClientRegistration/2747665748";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientRegistration.State::class)] = "consys.event.registration.gwt.client.bo.ClientRegistration$State/887166233";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::class)] = "consys.event.registration.gwt.client.bo.ClientRegistrationCycle/2544946280";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::class)] = "consys.event.registration.gwt.client.bo.ClientRegistrationOrder/1604244302";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::class)] = "consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem/3102277744";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientRegistrationPack::class)] = "consys.event.registration.gwt.client.bo.ClientRegistrationPack/1615415885";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientRegistrationResult::class)] = "consys.event.registration.gwt.client.bo.ClientRegistrationResult/4176758750";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails::class)] = "consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails/3913187484";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::class)] = "consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo/3111642701";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo::class)] = "consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo/3937546841";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations::class)] = "consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations/960390909";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.bo.managed.NewParticipant::class)] = "consys.event.registration.gwt.client.bo.managed.NewParticipant/1845375661";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.module.exception.BundleUniquityException::class)] = "consys.event.registration.gwt.client.module.exception.BundleUniquityException/3984629269";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.module.exception.CyclesIntersectionException::class)] = "consys.event.registration.gwt.client.module.exception.CyclesIntersectionException/2320819921";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException::class)] = "consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException/1928288592";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException::class)] = "consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException/4287209176";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@int[]::class)] = "[I/2970817851";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.lang.Integer::class)] = "java.lang.Integer/3438268394";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.lang.Long::class)] = "java.lang.Long/4227064769";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.lang.String::class)] = "java.lang.String/2004016611";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.sql.Date::class)] = "java.sql.Date/730999118";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.sql.Time::class)] = "java.sql.Time/1816797103";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.sql.Timestamp::class)] = "java.sql.Timestamp/3040052672";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.util.ArrayList::class)] = "java.util.ArrayList/4159755760";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.util.Date::class)] = "java.util.Date/3385151746";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.util.HashMap::class)] = "java.util.HashMap/1797211028";
    result[@com.google.gwt.core.client.impl.Impl::getHashCode(Ljava/lang/Object;)(@java.util.LinkedHashMap::class)] = "java.util.LinkedHashMap/3008245022";
    return result;
  }-*/;
  
  public DispatchService_TypeSerializer() {
    super(null, methodMapNative, null, signatureMapNative);
  }
  
}
