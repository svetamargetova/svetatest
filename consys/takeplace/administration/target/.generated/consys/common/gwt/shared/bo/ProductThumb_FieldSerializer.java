package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ProductThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDiscountCode(consys.common.gwt.shared.bo.ProductThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ProductThumb::discountCode;
  }-*/;
  
  private static native void setDiscountCode(consys.common.gwt.shared.bo.ProductThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ProductThumb::discountCode = value;
  }-*/;
  
  private static native java.lang.String getProductUuid(consys.common.gwt.shared.bo.ProductThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ProductThumb::productUuid;
  }-*/;
  
  private static native void setProductUuid(consys.common.gwt.shared.bo.ProductThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ProductThumb::productUuid = value;
  }-*/;
  
  private static native int getQuantity(consys.common.gwt.shared.bo.ProductThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ProductThumb::quantity;
  }-*/;
  
  private static native void setQuantity(consys.common.gwt.shared.bo.ProductThumb instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ProductThumb::quantity = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ProductThumb instance) throws SerializationException {
    setDiscountCode(instance, streamReader.readString());
    setProductUuid(instance, streamReader.readString());
    setQuantity(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.ProductThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ProductThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ProductThumb instance) throws SerializationException {
    streamWriter.writeString(getDiscountCode(instance));
    streamWriter.writeString(getProductUuid(instance));
    streamWriter.writeInt(getQuantity(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ProductThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ProductThumb_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ProductThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ProductThumb_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ProductThumb)object);
  }
  
}
