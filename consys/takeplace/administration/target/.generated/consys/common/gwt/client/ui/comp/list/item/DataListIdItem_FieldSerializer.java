package consys.common.gwt.client.ui.comp.list.item;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DataListIdItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(consys.common.gwt.client.ui.comp.list.item.DataListIdItem instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.item.DataListIdItem::id;
  }-*/;
  
  private static native void setId(consys.common.gwt.client.ui.comp.list.item.DataListIdItem instance, java.lang.Long value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.item.DataListIdItem::id = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.ui.comp.list.item.DataListIdItem instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    
    consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.client.ui.comp.list.item.DataListIdItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.ui.comp.list.item.DataListIdItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.ui.comp.list.item.DataListIdItem instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.ui.comp.list.item.DataListIdItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.item.DataListIdItem_FieldSerializer.deserialize(reader, (consys.common.gwt.client.ui.comp.list.item.DataListIdItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.item.DataListIdItem_FieldSerializer.serialize(writer, (consys.common.gwt.client.ui.comp.list.item.DataListIdItem)object);
  }
  
}
