package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class StringResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getStringResult(consys.common.gwt.client.rpc.result.StringResult instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.StringResult::stringResult;
  }-*/;
  
  private static native void setStringResult(consys.common.gwt.client.rpc.result.StringResult instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.StringResult::stringResult = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.StringResult instance) throws SerializationException {
    setStringResult(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.client.rpc.result.StringResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.StringResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.StringResult instance) throws SerializationException {
    streamWriter.writeString(getStringResult(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.StringResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.StringResult_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.StringResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.StringResult_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.StringResult)object);
  }
  
}
