package consys.common.gwt.client.ui.comp.form;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class FormResources_gecko1_8_cs_InlineClientBundleGenerator implements consys.common.gwt.client.ui.comp.form.FormResources {
  private static FormResources_gecko1_8_cs_InlineClientBundleGenerator _instance0 = new FormResources_gecko1_8_cs_InlineClientBundleGenerator();
  private void formCssInitializer() {
    formCss = new consys.common.gwt.client.ui.comp.form.FormCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "formCss";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GHAOYQ1II .input-tag-mode-write{margin-top:" + ("-6px")  + ";}.GHAOYQ1II .input-tag-mode-select-box{margin-top:" + ("-7px")  + ";}.GHAOYQ1KI{margin-bottom:" + ("7px")  + ";}.GHAOYQ1MI{margin-bottom:" + ("17px")  + ";}.GHAOYQ1LI{font-weight:" + ("bold")  + ";}.GHAOYQ1OI{float:" + ("right")  + ";text-align:" + ("left")  + ";width:" + ("125px")  + ";}.GHAOYQ1OI .GHAOYQ1AJ{float:" + ("left")  + ";width:" + ("112px")  + ";}.GHAOYQ1MI .GHAOYQ1BJ .gwt-HTML{font-size:") + (("20px")  + ";}.GHAOYQ1MI .GHAOYQ1OI{line-height:" + ("20px")  + ";}.GHAOYQ1OI .GHAOYQ1PI .GHAOYQ1NI{color:" + ("#1a809a")  + ";padding-right:" + ("2px")  + ";font-weight:" + ("bold")  + ";cursor:" + ("default")  + ";}.GHAOYQ1OI .GHAOYQ1PI{float:" + ("left")  + ";}.GHAOYQ1BJ{float:" + ("right")  + ";width:" + ("545px")  + ";margin-right:" + ("30px")  + ";}.GHAOYQ1CJ{color:" + ("#9a9a9a") ) + (";}.GHAOYQ1DJ{width:" + ("125px")  + ";height:" + ("10px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1II .GHAOYQ1EJ{position:" + ("relative")  + ";right:" + ("-6px")  + ";top:" + ("-7px")  + ";}.GHAOYQ1HI{color:" + ("red")  + ";position:" + ("relative")  + ";top:" + ("-6px")  + ";}")) : ((".GHAOYQ1II .input-tag-mode-write{margin-top:" + ("-6px")  + ";}.GHAOYQ1II .input-tag-mode-select-box{margin-top:" + ("-7px")  + ";}.GHAOYQ1KI{margin-bottom:" + ("7px")  + ";}.GHAOYQ1MI{margin-bottom:" + ("17px")  + ";}.GHAOYQ1LI{font-weight:" + ("bold")  + ";}.GHAOYQ1OI{float:" + ("left")  + ";text-align:" + ("right")  + ";width:" + ("125px")  + ";}.GHAOYQ1OI .GHAOYQ1AJ{float:" + ("right")  + ";width:" + ("112px")  + ";}.GHAOYQ1MI .GHAOYQ1BJ .gwt-HTML{font-size:") + (("20px")  + ";}.GHAOYQ1MI .GHAOYQ1OI{line-height:" + ("20px")  + ";}.GHAOYQ1OI .GHAOYQ1PI .GHAOYQ1NI{color:" + ("#1a809a")  + ";padding-left:" + ("2px")  + ";font-weight:" + ("bold")  + ";cursor:" + ("default")  + ";}.GHAOYQ1OI .GHAOYQ1PI{float:" + ("right")  + ";}.GHAOYQ1BJ{float:" + ("left")  + ";width:" + ("545px")  + ";margin-left:" + ("30px")  + ";}.GHAOYQ1CJ{color:" + ("#9a9a9a") ) + (";}.GHAOYQ1DJ{width:" + ("125px")  + ";height:" + ("10px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1II .GHAOYQ1EJ{position:" + ("relative")  + ";left:" + ("-6px")  + ";top:" + ("-7px")  + ";}.GHAOYQ1HI{color:" + ("red")  + ";position:" + ("relative")  + ";top:" + ("-6px")  + ";}"));
      }
      public java.lang.String errorLabel(){
        return "GHAOYQ1HI";
      }
      public java.lang.String form(){
        return "GHAOYQ1II";
      }
      public java.lang.String formElement(){
        return "GHAOYQ1JI";
      }
      public java.lang.String formElementNormal(){
        return "GHAOYQ1KI";
      }
      public java.lang.String formElementRequired(){
        return "GHAOYQ1LI";
      }
      public java.lang.String formElementTitle(){
        return "GHAOYQ1MI";
      }
      public java.lang.String formElementTitleHelp(){
        return "GHAOYQ1NI";
      }
      public java.lang.String formElementTitleWidget(){
        return "GHAOYQ1OI";
      }
      public java.lang.String formElementTitleWidgetEnd(){
        return "GHAOYQ1PI";
      }
      public java.lang.String formElementTitleWidgetTitle(){
        return "GHAOYQ1AJ";
      }
      public java.lang.String formElementValueWidget(){
        return "GHAOYQ1BJ";
      }
      public java.lang.String formElementValueWidgetDefault(){
        return "GHAOYQ1CJ";
      }
      public java.lang.String formPad(){
        return "GHAOYQ1DJ";
      }
      public java.lang.String inputWrapper(){
        return "GHAOYQ1EJ";
      }
    }
    ;
  }
  private static class formCssInitializer {
    static {
      _instance0.formCssInitializer();
    }
    static consys.common.gwt.client.ui.comp.form.FormCss get() {
      return formCss;
    }
  }
  public consys.common.gwt.client.ui.comp.form.FormCss formCss() {
    return formCssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.common.gwt.client.ui.comp.form.FormCss formCss;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      formCss(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("formCss", formCss());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'formCss': return this.@consys.common.gwt.client.ui.comp.form.FormResources::formCss()();
    }
    return null;
  }-*/;
}
