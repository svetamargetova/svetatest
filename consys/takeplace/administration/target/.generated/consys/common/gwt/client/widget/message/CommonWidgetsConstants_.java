package consys.common.gwt.client.widget.message;

public class CommonWidgetsConstants_ implements consys.common.gwt.client.widget.message.CommonWidgetsConstants {
  
  public java.lang.String const_password() {
    return "Password";
  }
  
  public java.lang.String const_weakLower() {
    return "weak";
  }
  
  public java.lang.String loginContent_action_createYourAccount() {
    return "Create your account";
  }
  
  public java.lang.String const_passwordIsWeak() {
    return "The password is weak. Try using more characacters.";
  }
  
  public java.lang.String registerUser_text_agreeing() {
    return "By clicking on 'REGISTER' below you are agreeing to the <a href=\"http://static.takeplace.eu/GBC_Takeplace_en.pdf\" target=\"_blank\">Terms of Service</a>.";
  }
  
  public java.lang.String loginContent_error_accountLocked() {
    return "Your account is not activated yet. Please activate the account first.";
  }
  
  public java.lang.String loginContent_error_loginDisabled() {
    return "Logging in is temporarily disabled.";
  }
  
  public java.lang.String loginContent_error_ssoUnsupported() {
    return "Logging in failed.";
  }
  
  public java.lang.String loginContent_error_authFail() {
    return "You are either not registered yet or you have mistyped the contact e-mail or password.";
  }
  
  public java.lang.String loginContent_title() {
    return "Log in";
  }
  
  public java.lang.String const_tooShortLower() {
    return "too short";
  }
  
  public java.lang.String loginContent_action_activateYourAccount() {
    return "Activate your account";
  }
  
  public java.lang.String const_confirmPassword() {
    return "Confirm password";
  }
  
  public java.lang.String loginContent_action_sendMeHint() {
    return "Send me a hint.";
  }
  
  public java.lang.String const_rememberMe() {
    return "Remember me";
  }
  
  public java.lang.String loginContent_button_logIn() {
    return "LOG IN";
  }
  
  public java.lang.String const_middleName() {
    return "Middle name";
  }
  
  public java.lang.String const_firstName() {
    return "First name";
  }
  
  public java.lang.String const_affiliation() {
    return "Organization";
  }
  
  public java.lang.String loginContent_text_lostPassword() {
    return "Lost password?";
  }
  
  public java.lang.String const_lastName() {
    return "Last name";
  }
  
  public java.lang.String const_passwordContainsSpace() {
    return "The password cannot contain the space character.";
  }
  
  public java.lang.String const_okLower() {
    return "ok";
  }
  
  public java.lang.String loginContent_text_notActivated() {
    return "Not activated?";
  }
  
  public java.lang.String registerContent_error_alreadyRegistered() {
    return "This contact email is already in use. Did you forget your password?";
  }
  
  public java.lang.String const_passwordsNotMatch() {
    return "The passwords do not match.";
  }
  
  public java.lang.String loginContent_text_notRegistered() {
    return "Not registered?";
  }
  
  public java.lang.String const_showPassword() {
    return "Show password";
  }
  
  public java.lang.String const_passwordStrength() {
    return "Password strength";
  }
}
