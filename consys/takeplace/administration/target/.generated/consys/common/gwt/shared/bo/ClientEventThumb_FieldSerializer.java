package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEventThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::description;
  }-*/;
  
  private static native void setDescription(consys.common.gwt.shared.bo.ClientEventThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::description = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientDateTime getFrom(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::from;
  }-*/;
  
  private static native void setFrom(consys.common.gwt.shared.bo.ClientEventThumb instance, consys.common.gwt.shared.bo.ClientDateTime value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::from = value;
  }-*/;
  
  private static native java.lang.String getFullName(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::fullName;
  }-*/;
  
  private static native void setFullName(consys.common.gwt.shared.bo.ClientEventThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::fullName = value;
  }-*/;
  
  private static native java.lang.String getLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::logoUuidPrefix;
  }-*/;
  
  private static native void setLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEventThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::logoUuidPrefix = value;
  }-*/;
  
  private static native java.lang.String getOverseer(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::overseer;
  }-*/;
  
  private static native void setOverseer(consys.common.gwt.shared.bo.ClientEventThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::overseer = value;
  }-*/;
  
  private static native java.lang.String getProfileLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::profileLogoUuidPrefix;
  }-*/;
  
  private static native void setProfileLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEventThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::profileLogoUuidPrefix = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientDateTime getTo(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::to;
  }-*/;
  
  private static native void setTo(consys.common.gwt.shared.bo.ClientEventThumb instance, consys.common.gwt.shared.bo.ClientDateTime value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::to = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientEventThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::uuid = value;
  }-*/;
  
  private static native java.lang.String getWeb(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::web;
  }-*/;
  
  private static native void setWeb(consys.common.gwt.shared.bo.ClientEventThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::web = value;
  }-*/;
  
  private static native int getYear(consys.common.gwt.shared.bo.ClientEventThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEventThumb::year;
  }-*/;
  
  private static native void setYear(consys.common.gwt.shared.bo.ClientEventThumb instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEventThumb::year = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientEventThumb instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setFrom(instance, (consys.common.gwt.shared.bo.ClientDateTime) streamReader.readObject());
    setFullName(instance, streamReader.readString());
    setLogoUuidPrefix(instance, streamReader.readString());
    setOverseer(instance, streamReader.readString());
    setProfileLogoUuidPrefix(instance, streamReader.readString());
    setTo(instance, (consys.common.gwt.shared.bo.ClientDateTime) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    setWeb(instance, streamReader.readString());
    setYear(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientEventThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientEventThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientEventThumb instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeString(getFullName(instance));
    streamWriter.writeString(getLogoUuidPrefix(instance));
    streamWriter.writeString(getOverseer(instance));
    streamWriter.writeString(getProfileLogoUuidPrefix(instance));
    streamWriter.writeObject(getTo(instance));
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeString(getWeb(instance));
    streamWriter.writeInt(getYear(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientEventThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEventThumb_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientEventThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEventThumb_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientEventThumb)object);
  }
  
}
