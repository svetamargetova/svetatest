package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Monetary_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getDecimal(consys.common.gwt.shared.bo.Monetary instance) /*-{
    return instance.@consys.common.gwt.shared.bo.Monetary::decimal;
  }-*/;
  
  private static native void setDecimal(consys.common.gwt.shared.bo.Monetary instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.Monetary::decimal = value;
  }-*/;
  
  private static native int getInteger(consys.common.gwt.shared.bo.Monetary instance) /*-{
    return instance.@consys.common.gwt.shared.bo.Monetary::integer;
  }-*/;
  
  private static native void setInteger(consys.common.gwt.shared.bo.Monetary instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.Monetary::integer = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.Monetary instance) throws SerializationException {
    setDecimal(instance, streamReader.readInt());
    setInteger(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.Monetary instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.Monetary();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.Monetary instance) throws SerializationException {
    streamWriter.writeInt(getDecimal(instance));
    streamWriter.writeInt(getInteger(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.Monetary_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.Monetary_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.Monetary)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.Monetary_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.Monetary)object);
  }
  
}
