package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEventType_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientEventType instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static consys.common.gwt.shared.bo.ClientEventType instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    consys.common.gwt.shared.bo.ClientEventType[] values = consys.common.gwt.shared.bo.ClientEventType.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientEventType instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientEventType_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEventType_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientEventType)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEventType_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientEventType)object);
  }
  
}
