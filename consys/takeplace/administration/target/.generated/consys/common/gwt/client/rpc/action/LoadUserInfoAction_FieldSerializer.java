package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadUserInfoAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUserUuid(consys.common.gwt.client.rpc.action.LoadUserInfoAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.LoadUserInfoAction::userUuid;
  }-*/;
  
  private static native void setUserUuid(consys.common.gwt.client.rpc.action.LoadUserInfoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.LoadUserInfoAction::userUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.LoadUserInfoAction instance) throws SerializationException {
    setUserUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.client.rpc.action.LoadUserInfoAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.LoadUserInfoAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.LoadUserInfoAction instance) throws SerializationException {
    streamWriter.writeString(getUserUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.LoadUserInfoAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.LoadUserInfoAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.LoadUserInfoAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.LoadUserInfoAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.LoadUserInfoAction)object);
  }
  
}
