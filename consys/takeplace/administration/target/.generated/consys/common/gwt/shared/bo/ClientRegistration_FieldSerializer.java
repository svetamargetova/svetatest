package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistration_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAffiliation(consys.common.gwt.shared.bo.ClientRegistration instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientRegistration::affiliation;
  }-*/;
  
  private static native void setAffiliation(consys.common.gwt.shared.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientRegistration::affiliation = value;
  }-*/;
  
  private static native java.lang.String getConfirmPassword(consys.common.gwt.shared.bo.ClientRegistration instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientRegistration::confirmPassword;
  }-*/;
  
  private static native void setConfirmPassword(consys.common.gwt.shared.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientRegistration::confirmPassword = value;
  }-*/;
  
  private static native java.lang.String getContactEmail(consys.common.gwt.shared.bo.ClientRegistration instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientRegistration::contactEmail;
  }-*/;
  
  private static native void setContactEmail(consys.common.gwt.shared.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientRegistration::contactEmail = value;
  }-*/;
  
  private static native java.lang.String getFirstName(consys.common.gwt.shared.bo.ClientRegistration instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientRegistration::firstName;
  }-*/;
  
  private static native void setFirstName(consys.common.gwt.shared.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientRegistration::firstName = value;
  }-*/;
  
  private static native java.lang.String getLastName(consys.common.gwt.shared.bo.ClientRegistration instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientRegistration::lastName;
  }-*/;
  
  private static native void setLastName(consys.common.gwt.shared.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientRegistration::lastName = value;
  }-*/;
  
  private static native java.lang.String getPassword(consys.common.gwt.shared.bo.ClientRegistration instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientRegistration::password;
  }-*/;
  
  private static native void setPassword(consys.common.gwt.shared.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientRegistration::password = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientRegistration instance) throws SerializationException {
    setAffiliation(instance, streamReader.readString());
    setConfirmPassword(instance, streamReader.readString());
    setContactEmail(instance, streamReader.readString());
    setFirstName(instance, streamReader.readString());
    setLastName(instance, streamReader.readString());
    setPassword(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientRegistration instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientRegistration();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientRegistration instance) throws SerializationException {
    streamWriter.writeString(getAffiliation(instance));
    streamWriter.writeString(getConfirmPassword(instance));
    streamWriter.writeString(getContactEmail(instance));
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getPassword(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientRegistration_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientRegistration_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientRegistration)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientRegistration_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientRegistration)object);
  }
  
}
