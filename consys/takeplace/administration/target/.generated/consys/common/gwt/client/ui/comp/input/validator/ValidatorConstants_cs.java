package consys.common.gwt.client.ui.comp.input.validator;

public class ValidatorConstants_cs implements consys.common.gwt.client.ui.comp.input.validator.ValidatorConstants {
  
  public java.lang.String fieldMustBeNumberic() {
    return "Hodnota musí být číslo.";
  }
  
  public java.lang.String fieldMustEntered() {
    return "Toto pole musí být zadáno.";
  }
}
