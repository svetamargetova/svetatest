package consys.common.gwt.client.messaging.message;

public class CommonMessagingConstants_ implements consys.common.gwt.client.messaging.message.CommonMessagingConstants {
  
  public java.lang.String messagingApi_text_unknownStatus() {
    return "The status is unknown.";
  }
  
  public java.lang.String const_newUpper() {
    return "NEW";
  }
  
  public java.lang.String messageBox_title() {
    return "Messages";
  }
  
  public java.lang.String messageInput_text_asLower() {
    return "as";
  }
  
  public java.lang.String messagingApi_text_entityNotFound() {
    return "This entity was not found.";
  }
  
  public java.lang.String const_notLoaded() {
    return "Not loaded";
  }
  
  public java.lang.String messagingApi_text_noPermission() {
    return "You do not have such permission.";
  }
  
  public java.lang.String messageBoxUI_action_show() {
    return "Show";
  }
  
  public java.lang.String toPanel_action_selectContact() {
    return "Select contact";
  }
  
  public java.lang.String const_to() {
    return "To";
  }
  
  public java.lang.String messageSentBoxUI_title() {
    return "Sent messages";
  }
  
  public java.lang.String messageBox_button_newMessage() {
    return "New message";
  }
  
  public java.lang.String messageSendUI_button_sendMessage() {
    return "Send message";
  }
  
  public java.lang.String bugPanel_text_bugGraphic() {
    return "Graphic bug";
  }
  
  public java.lang.String bugPanel_text_reportBug() {
    return "Report a bug";
  }
  
  public java.lang.String messagingApi_text_banned() {
    return "The user has been banned.";
  }
  
  public java.lang.String bugPanel_text_sendFailed() {
    return "Sending bug message has failed.";
  }
  
  public java.lang.String bugPanel_text_bugOther() {
    return "Other bug";
  }
  
  public java.lang.String bugPanel_text_bugFunction() {
    return "Function bug";
  }
  
  public java.lang.String bugPanel_button_sendBug() {
    return "SEND";
  }
  
  public java.lang.String messageSendUI_form_message() {
    return "Message";
  }
  
  public java.lang.String messageSendUI_form_subject() {
    return "Subject";
  }
  
  public java.lang.String messagingApi_text_requestFail() {
    return "Request fail";
  }
  
  public java.lang.String eventWallUI_text_emptyWall() {
    return "The wall is empty at the moment.";
  }
  
  public java.lang.String messageThreadUI_button_reply() {
    return "REPLY";
  }
  
  public java.lang.String userMessagingModule_text_messaging() {
    return "Messaging";
  }
  
  public java.lang.String messageBoxUI_title() {
    return "Threads";
  }
  
  public java.lang.String messagingApi_text_newMessage() {
    return "New message";
  }
  
  public java.lang.String const_communicationError() {
    return "There was an error in communication.";
  }
  
  public java.lang.String messagingApi_text_serverClosed() {
    return "The server is closed.";
  }
  
  public java.lang.String messagingApi_text_messageNotFound() {
    return "The message was not found.";
  }
  
  public java.lang.String messageInput_button_publishUpper() {
    return "PUBLISH";
  }
  
  public java.lang.String messageInput_text_messageAdded() {
    return "The message has been published.";
  }
  
  public java.lang.String messagingApi_text_threadNotFound() {
    return "The thread was not found.";
  }
}
