package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientInvitedUser_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getPosition(consys.common.gwt.shared.bo.ClientInvitedUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientInvitedUser::position;
  }-*/;
  
  private static native void setPosition(consys.common.gwt.shared.bo.ClientInvitedUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientInvitedUser::position = value;
  }-*/;
  
  private static native java.lang.String getUuidPortraitImagePrefix(consys.common.gwt.shared.bo.ClientInvitedUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientInvitedUser::uuidPortraitImagePrefix;
  }-*/;
  
  private static native void setUuidPortraitImagePrefix(consys.common.gwt.shared.bo.ClientInvitedUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientInvitedUser::uuidPortraitImagePrefix = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientInvitedUser instance) throws SerializationException {
    setPosition(instance, streamReader.readString());
    setUuidPortraitImagePrefix(instance, streamReader.readString());
    
    consys.common.gwt.shared.bo.InvitedUser_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.shared.bo.ClientInvitedUser instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientInvitedUser();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientInvitedUser instance) throws SerializationException {
    streamWriter.writeString(getPosition(instance));
    streamWriter.writeString(getUuidPortraitImagePrefix(instance));
    
    consys.common.gwt.shared.bo.InvitedUser_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientInvitedUser_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientInvitedUser_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientInvitedUser)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientInvitedUser_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientInvitedUser)object);
  }
  
}
