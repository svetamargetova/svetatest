package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CloneEventAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEventUuid(consys.common.gwt.client.rpc.action.CloneEventAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CloneEventAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.common.gwt.client.rpc.action.CloneEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CloneEventAction::eventUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.CloneEventAction instance) throws SerializationException {
    setEventUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.client.rpc.action.CloneEventAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.CloneEventAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.CloneEventAction instance) throws SerializationException {
    streamWriter.writeString(getEventUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.CloneEventAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.CloneEventAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.CloneEventAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.CloneEventAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.CloneEventAction)object);
  }
  
}
