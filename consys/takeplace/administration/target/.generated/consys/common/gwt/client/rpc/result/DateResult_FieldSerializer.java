package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DateResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getDate(consys.common.gwt.client.rpc.result.DateResult instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.DateResult::date;
  }-*/;
  
  private static native void setDate(consys.common.gwt.client.rpc.result.DateResult instance, java.util.Date value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.DateResult::date = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.DateResult instance) throws SerializationException {
    setDate(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.client.rpc.result.DateResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.DateResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.DateResult instance) throws SerializationException {
    streamWriter.writeObject(getDate(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.DateResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.DateResult_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.DateResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.DateResult_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.DateResult)object);
  }
  
}
