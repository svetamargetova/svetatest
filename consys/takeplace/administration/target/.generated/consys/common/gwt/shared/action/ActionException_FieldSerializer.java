package consys.common.gwt.shared.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ActionException_FieldSerializer {
  private static native java.lang.String getCauseClassname(consys.common.gwt.shared.action.ActionException instance) /*-{
    return instance.@consys.common.gwt.shared.action.ActionException::causeClassname;
  }-*/;
  
  private static native void setCauseClassname(consys.common.gwt.shared.action.ActionException instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.action.ActionException::causeClassname = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.action.ActionException instance) throws SerializationException {
    setCauseClassname(instance, streamReader.readString());
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.action.ActionException instance) throws SerializationException {
    streamWriter.writeString(getCauseClassname(instance));
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.serialize(streamWriter, instance);
  }
  
}
