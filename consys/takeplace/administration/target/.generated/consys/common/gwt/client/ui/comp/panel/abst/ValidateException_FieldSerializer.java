package consys.common.gwt.client.ui.comp.panel.abst;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ValidateException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.ui.comp.panel.abst.ValidateException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.client.ui.comp.panel.abst.ValidateException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.ui.comp.panel.abst.ValidateException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.ui.comp.panel.abst.ValidateException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.ui.comp.panel.abst.ValidateException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.panel.abst.ValidateException_FieldSerializer.deserialize(reader, (consys.common.gwt.client.ui.comp.panel.abst.ValidateException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.panel.abst.ValidateException_FieldSerializer.serialize(writer, (consys.common.gwt.client.ui.comp.panel.abst.ValidateException)object);
  }
  
}
