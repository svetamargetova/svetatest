package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EventSettings_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getRoles(consys.common.gwt.shared.bo.EventSettings instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventSettings::roles;
  }-*/;
  
  private static native void setRoles(consys.common.gwt.shared.bo.EventSettings instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventSettings::roles = value;
  }-*/;
  
  private static native java.util.HashMap getSystemProperties(consys.common.gwt.shared.bo.EventSettings instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventSettings::systemProperties;
  }-*/;
  
  private static native void setSystemProperties(consys.common.gwt.shared.bo.EventSettings instance, java.util.HashMap value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventSettings::systemProperties = value;
  }-*/;
  
  private static native java.lang.String getUserUuid(consys.common.gwt.shared.bo.EventSettings instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventSettings::userUuid;
  }-*/;
  
  private static native void setUserUuid(consys.common.gwt.shared.bo.EventSettings instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventSettings::userUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.EventSettings instance) throws SerializationException {
    setRoles(instance, (java.util.ArrayList) streamReader.readObject());
    setSystemProperties(instance, (java.util.HashMap) streamReader.readObject());
    setUserUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.EventSettings instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.EventSettings();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.EventSettings instance) throws SerializationException {
    streamWriter.writeObject(getRoles(instance));
    streamWriter.writeObject(getSystemProperties(instance));
    streamWriter.writeString(getUserUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.EventSettings_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.EventSettings_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.EventSettings)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.EventSettings_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.EventSettings)object);
  }
  
}
