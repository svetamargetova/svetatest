package consys.common.gwt.client.widget.message;

public class CommonsWidgetsMessages_ implements consys.common.gwt.client.widget.message.CommonsWidgetsMessages {
  
  public java.lang.String loginContent_error_ssoFailed(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Signing in using ").append(arg0).append(" failed. Please use different service or create your own Takeplace account.").toString();
  }
  
  public java.lang.String loginContent_error_ssoDenied(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("You have denied to use your ").append(arg0).append(" account. Please use different service or create your own Takeplace account.").toString();
  }
}
