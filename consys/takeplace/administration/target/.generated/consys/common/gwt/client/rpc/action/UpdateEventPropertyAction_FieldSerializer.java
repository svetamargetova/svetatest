package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateEventPropertyAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.HashMap getProperties(consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.UpdateEventPropertyAction::properties;
  }-*/;
  
  private static native void setProperties(consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance, java.util.HashMap value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.UpdateEventPropertyAction::properties = value;
  }-*/;
  
  private static native java.lang.String getPropertyKey(consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.UpdateEventPropertyAction::propertyKey;
  }-*/;
  
  private static native void setPropertyKey(consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.UpdateEventPropertyAction::propertyKey = value;
  }-*/;
  
  private static native java.lang.String getPropertyValue(consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.UpdateEventPropertyAction::propertyValue;
  }-*/;
  
  private static native void setPropertyValue(consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.UpdateEventPropertyAction::propertyValue = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance) throws SerializationException {
    setProperties(instance, (java.util.HashMap) streamReader.readObject());
    setPropertyKey(instance, streamReader.readString());
    setPropertyValue(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.UpdateEventPropertyAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.UpdateEventPropertyAction instance) throws SerializationException {
    streamWriter.writeObject(getProperties(instance));
    streamWriter.writeString(getPropertyKey(instance));
    streamWriter.writeString(getPropertyValue(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.UpdateEventPropertyAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.UpdateEventPropertyAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.UpdateEventPropertyAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.UpdateEventPropertyAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.UpdateEventPropertyAction)object);
  }
  
}
