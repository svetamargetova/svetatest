package consys.common.gwt.client.ui.comp.input.validator;

public class ValidatorMessages_ implements consys.common.gwt.client.ui.comp.input.validator.ValidatorMessages {
  
  public java.lang.String fieldMustBeInRange(java.lang.Number arg0,java.lang.Number arg1) {
    return new java.lang.StringBuffer().append("The value must be within ").append(arg0).append(" and ").append(arg1).append(".").toString();
  }
}
