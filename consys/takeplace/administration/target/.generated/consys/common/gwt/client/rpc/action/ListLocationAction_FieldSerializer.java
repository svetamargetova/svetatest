package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListLocationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.ListLocationAction instance) throws SerializationException {
    
  }
  
  public static consys.common.gwt.client.rpc.action.ListLocationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.ListLocationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.ListLocationAction instance) throws SerializationException {
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.ListLocationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.ListLocationAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.ListLocationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.ListLocationAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.ListLocationAction)object);
  }
  
}
