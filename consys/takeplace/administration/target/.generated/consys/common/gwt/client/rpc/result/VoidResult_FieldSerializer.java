package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class VoidResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.VoidResult instance) throws SerializationException {
    
  }
  
  public static consys.common.gwt.client.rpc.result.VoidResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.VoidResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.VoidResult instance) throws SerializationException {
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.VoidResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.VoidResult_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.VoidResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.VoidResult_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.VoidResult)object);
  }
  
}
