package consys.common.gwt.shared.bo.list;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Filter_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getIntValue(consys.common.gwt.shared.bo.list.Filter instance) /*-{
    return instance.@consys.common.gwt.shared.bo.list.Filter::intValue;
  }-*/;
  
  private static native void setIntValue(consys.common.gwt.shared.bo.list.Filter instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.list.Filter::intValue = value;
  }-*/;
  
  private static native java.lang.String getStringValue(consys.common.gwt.shared.bo.list.Filter instance) /*-{
    return instance.@consys.common.gwt.shared.bo.list.Filter::stringValue;
  }-*/;
  
  private static native void setStringValue(consys.common.gwt.shared.bo.list.Filter instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.list.Filter::stringValue = value;
  }-*/;
  
  private static native int getTag(consys.common.gwt.shared.bo.list.Filter instance) /*-{
    return instance.@consys.common.gwt.shared.bo.list.Filter::tag;
  }-*/;
  
  private static native void setTag(consys.common.gwt.shared.bo.list.Filter instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.list.Filter::tag = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.list.Filter instance) throws SerializationException {
    setIntValue(instance, streamReader.readInt());
    setStringValue(instance, streamReader.readString());
    setTag(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.list.Filter instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.list.Filter();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.list.Filter instance) throws SerializationException {
    streamWriter.writeInt(getIntValue(instance));
    streamWriter.writeString(getStringValue(instance));
    streamWriter.writeInt(getTag(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.list.Filter_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.list.Filter_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.list.Filter)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.list.Filter_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.list.Filter)object);
  }
  
}
