package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientOrganizationThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAddress(consys.common.gwt.shared.bo.ClientOrganizationThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::address;
  }-*/;
  
  private static native void setAddress(consys.common.gwt.shared.bo.ClientOrganizationThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::address = value;
  }-*/;
  
  private static native java.lang.String getType(consys.common.gwt.shared.bo.ClientOrganizationThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::type;
  }-*/;
  
  private static native void setType(consys.common.gwt.shared.bo.ClientOrganizationThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::type = value;
  }-*/;
  
  private static native java.lang.String getUniversalName(consys.common.gwt.shared.bo.ClientOrganizationThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::universalName;
  }-*/;
  
  private static native void setUniversalName(consys.common.gwt.shared.bo.ClientOrganizationThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::universalName = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientOrganizationThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientOrganizationThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::uuid = value;
  }-*/;
  
  private static native java.lang.String getWeb(consys.common.gwt.shared.bo.ClientOrganizationThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::web;
  }-*/;
  
  private static native void setWeb(consys.common.gwt.shared.bo.ClientOrganizationThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganizationThumb::web = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientOrganizationThumb instance) throws SerializationException {
    setAddress(instance, streamReader.readString());
    setType(instance, streamReader.readString());
    setUniversalName(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    setWeb(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientOrganizationThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientOrganizationThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientOrganizationThumb instance) throws SerializationException {
    streamWriter.writeString(getAddress(instance));
    streamWriter.writeString(getType(instance));
    streamWriter.writeString(getUniversalName(instance));
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeString(getWeb(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientOrganizationThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientOrganizationThumb_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientOrganizationThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientOrganizationThumb_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientOrganizationThumb)object);
  }
  
}
