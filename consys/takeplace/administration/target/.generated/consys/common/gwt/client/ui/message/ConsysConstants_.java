package consys.common.gwt.client.ui.message;

public class ConsysConstants_ implements consys.common.gwt.client.ui.message.ConsysConstants {
  
  public java.lang.String progress_text1() {
    return "REGISTER";
  }
  
  public java.lang.String paymentProfilePanel_form_vatNumber() {
    return "VAT ID";
  }
  
  public java.lang.String const_password() {
    return "Password";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_faq() {
    return "FAQ";
  }
  
  public java.lang.String help_text_noTitle() {
    return "No title";
  }
  
  public java.lang.String consysRichTextToolbar_text_underline() {
    return "Underline";
  }
  
  public java.lang.String consysRichTextToolbar_text_strikethrough() {
    return "Striketrough";
  }
  
  public java.lang.String const_more() {
    return "More";
  }
  
  public java.lang.String const_genderNotSpec() {
    return "Not specified";
  }
  
  public java.lang.String const_add() {
    return "Add";
  }
  
  public java.lang.String const_address_street() {
    return "Street";
  }
  
  public java.lang.String dataListCommon_text_show() {
    return "Show";
  }
  
  public java.lang.String const_address_city() {
    return "City";
  }
  
  public java.lang.String const_date_monthFebruary() {
    return "February";
  }
  
  public java.lang.String const_copy() {
    return "Copy";
  }
  
  public java.lang.String consysRichTextToolbar_text_bold() {
    return "Bold";
  }
  
  public java.lang.String const_yes() {
    return "Yes";
  }
  
  public java.lang.String consysRichTextToolbar_text_list() {
    return "List";
  }
  
  public java.lang.String paymentProfilePanel_text_invoicedTo() {
    return "Invoiced to Organisation / Name";
  }
  
  public java.lang.String const_confirmPassword() {
    return "Confirm password";
  }
  
  public java.lang.String consysRichTextToolbar_text_italic() {
    return "Italic";
  }
  
  public java.lang.String consysPasswordBox_error_passwordNotEntered() {
    return "Password not entered";
  }
  
  public java.lang.String servletUtils_error_http401() {
    return "The user is not logged in.";
  }
  
  public java.lang.String help_text_noDescription() {
    return "No description";
  }
  
  public java.lang.String const_description() {
    return "Description";
  }
  
  public java.lang.String const_date_monthNovember() {
    return "November";
  }
  
  public java.lang.String const_noneLower() {
    return "none";
  }
  
  public java.lang.String servletUtils_error_http403() {
    return "Event is not authorized to upload videos.";
  }
  
  public java.lang.String userInvitePanel_form_name() {
    return "Name";
  }
  
  public java.lang.String const_phoneNumber() {
    return "Phone number";
  }
  
  public java.lang.String const_edit() {
    return "Edit";
  }
  
  public java.lang.String const_personalInfo_celiac() {
    return "Celiac";
  }
  
  public java.lang.String paymentPriceList_text_name() {
    return "Name";
  }
  
  public java.lang.String loginPanel_text_signUpWith() {
    return "Sign-up with one of these";
  }
  
  public java.lang.String const_address_state() {
    return "State";
  }
  
  public java.lang.String const_showPassword() {
    return "Show password";
  }
  
  public java.lang.String const_personalInfo_vegetarian() {
    return "Vegetarian";
  }
  
  public java.lang.String const_addMore() {
    return "Add more";
  }
  
  public java.lang.String const_genderWoman() {
    return "Female";
  }
  
  public java.lang.String paymentProfilePanel_text_supportedText() {
    return "You may <a href=\"http://www.ibancalculator.com/iban_and_bic.html\" target=\"_blank\">calculate</a> your IBAN and SWIFT.";
  }
  
  public java.lang.String userSearchDialog_action_invite() {
    return "No matches?\nInvite new user!";
  }
  
  public java.lang.String paymentProfilePanel_form_bankNumber() {
    return "Bank SWIFT number";
  }
  
  public java.lang.String paymentPriceList_text_total() {
    return "Total";
  }
  
  public java.lang.String unknownTokenContent_text() {
    return "Unknown parameter in the token.";
  }
  
  public java.lang.String const_organization() {
    return "Organization";
  }
  
  public java.lang.String const_select() {
    return "Select";
  }
  
  public java.lang.String userSearchDialog_helpText() {
    return "Search by email, or by the first and last names.";
  }
  
  public java.lang.String paymentProfilePanel_form_accountNumber() {
    return "IBAN";
  }
  
  public java.lang.String const_close() {
    return "Close";
  }
  
  public java.lang.String const_acronym() {
    return "Acronym";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_customers() {
    return "Customers";
  }
  
  public java.lang.String consysFileUpload_button_browsFile() {
    return "Browse file";
  }
  
  public java.lang.String dataList_text_empty() {
    return "The list is empty.";
  }
  
  public java.lang.String paymentProfilePanel_title() {
    return "Billing address";
  }
  
  public java.lang.String const_date_monthApril() {
    return "April";
  }
  
  public java.lang.String const_name() {
    return "Name";
  }
  
  public java.lang.String const_property() {
    return "Properties";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_overview() {
    return "Overview";
  }
  
  public java.lang.String consysAutoFileUpload_text_uploaded() {
    return "File was uploaded to server.";
  }
  
  public java.lang.String const_discountCode() {
    return "Discount code";
  }
  
  public java.lang.String const_date_monthSeptember() {
    return "September";
  }
  
  public java.lang.String const_currency() {
    return "Currency";
  }
  
  public java.lang.String remover_text_areYouSure() {
    return "Are you sure?";
  }
  
  public java.lang.String const_submit() {
    return "Submit";
  }
  
  public java.lang.String const_item_requiredLower() {
    return "required";
  }
  
  public java.lang.String userInvitePanel_button_invite() {
    return "Invite";
  }
  
  public java.lang.String const_date_monthJuly() {
    return "July";
  }
  
  public java.lang.String servletUtils_error_http500() {
    return "There was an error while processing the file.";
  }
  
  public java.lang.String const_date_yesterday() {
    return "Yesterday";
  }
  
  public java.lang.String dataListCommon_text_sortBy() {
    return "Sort by";
  }
  
  public java.lang.String const_item_optionalLower() {
    return "optional";
  }
  
  public java.lang.String servletUtils_error_http406() {
    return "The request is not acceptable.";
  }
  
  public java.lang.String const_date_monthDecember() {
    return "December";
  }
  
  public java.lang.String const_decline() {
    return "Decline";
  }
  
  public java.lang.String const_user_lastName() {
    return "Last name";
  }
  
  public java.lang.String consysRichTextToolbar_text_removeLink() {
    return "Remove link";
  }
  
  public java.lang.String const_website() {
    return "Website";
  }
  
  public java.lang.String const_error_noPermissionForAction() {
    return "You do not have permission for this action";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_signUp() {
    return "SIGN UP";
  }
  
  public java.lang.String consysRichTextToolbar_text_removeFormat() {
    return "Remove format";
  }
  
  public java.lang.String const_date_monthJanuary() {
    return "January";
  }
  
  public java.lang.String const_toLower() {
    return "to";
  }
  
  public java.lang.String progress_text2() {
    return "CONFIRM";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_aboutUs() {
    return "About us";
  }
  
  public java.lang.String const_date_monthAugust() {
    return "August";
  }
  
  public java.lang.String consysAutoFileUpload_text_uploading() {
    return "File is uploading to server ...";
  }
  
  public java.lang.String paymentPriceList_text_quantity() {
    return "Quantity";
  }
  
  public java.lang.String const_no() {
    return "No";
  }
  
  public java.lang.String ssoLogin_text_orUseTakeplaceAccount() {
    return "... or use Takeplace account";
  }
  
  public java.lang.String consysPasswordBox_error_passwordsNotSame() {
    return "Passwords are not same";
  }
  
  public java.lang.String const_clearForm() {
    return "Clear form";
  }
  
  public java.lang.String const_gender() {
    return "Gender";
  }
  
  public java.lang.String const_free() {
    return "free";
  }
  
  public java.lang.String const_accept() {
    return "Accept";
  }
  
  public java.lang.String paymentPriceList_text_unitPrice() {
    return "Unit price";
  }
  
  public java.lang.String servletUtils_error_http400() {
    return "Bad request to server.";
  }
  
  public java.lang.String const_address_place() {
    return "Place";
  }
  
  public int consysFileUpload_button_width() {
    return 104;
  }
  
  public java.lang.String const_delete() {
    return "Delete";
  }
  
  public java.lang.String const_email() {
    return "E-mail";
  }
  
  public java.lang.String const_update() {
    return "Update";
  }
  
  public java.lang.String const_cancel() {
    return "Cancel";
  }
  
  public java.lang.String const_exceptionBadIban() {
    return "IBAN is in bad format.";
  }
  
  public java.lang.String consysRichTextToolbar_text_enterURL() {
    return "Enter a link URL";
  }
  
  public java.lang.String const_remove() {
    return "Remove";
  }
  
  public java.lang.String servletUtils_error_http404() {
    return "The requested url was not found.";
  }
  
  public java.lang.String const_date_year() {
    return "Year";
  }
  
  public java.lang.String const_timeZone() {
    return "Time Zone";
  }
  
  public java.lang.String const_save() {
    return "Save";
  }
  
  public java.lang.String const_isLower() {
    return "is";
  }
  
  public java.lang.String const_warning() {
    return "Warning";
  }
  
  public java.lang.String dataList_error() {
    return "The data cannot be loaded.";
  }
  
  public java.lang.String const_check() {
    return "Check";
  }
  
  public java.lang.String system_locale() {
    return "en";
  }
  
  public java.lang.String consysRichTextToolbar_text_createLink() {
    return "Create link";
  }
  
  public java.lang.String const_date_monthMarch() {
    return "March";
  }
  
  public java.lang.String paymentProfilePanel_text_supportedSwift() {
    return "\"SWIFT code\" is a bank identifier code (BIC) under international standard ISO 9362.";
  }
  
  public java.lang.String const_exceptionServiceFailed() {
    return "We're sorry, but we are unable to serve your request at this time. Please try back in a few minutes or do not hesitate to contact us if this happens often.";
  }
  
  public java.lang.String consysWordArea_text_left() {
    return "words left";
  }
  
  public java.lang.String const_back() {
    return "Back";
  }
  
  public java.lang.String const_error_invalidEmailAddress() {
    return "The e-mail address is invalid.";
  }
  
  public java.lang.String accessDeniedContent_error() {
    return "Access denied";
  }
  
  public java.lang.String paymentProfilePanel_text_supportedIban() {
    return "IBAN is an international standard under ISO 13616 for identifying bank accounts.";
  }
  
  public java.lang.String const_user_firstName() {
    return "First name";
  }
  
  public java.lang.String const_at() {
    return "at";
  }
  
  public java.lang.String const_address_zip() {
    return "Zip";
  }
  
  public java.lang.String const_address_location() {
    return "Country";
  }
  
  public java.lang.String subbundlePanel_action_showDetails() {
    return "More";
  }
  
  public java.lang.String consysFileUpload_text_chooseFileToUpload() {
    return "Choose a file to upload";
  }
  
  public java.lang.String const_sendUpper() {
    return "SEND";
  }
  
  public java.lang.String const_exceptionNotPermittedAction() {
    return "Action is not permitted.";
  }
  
  public java.lang.String const_apply() {
    return "Apply";
  }
  
  public java.lang.String const_unlimited() {
    return "Unlimited";
  }
  
  public java.lang.String servletUtils_error_http415() {
    return "Bad file format.";
  }
  
  public java.lang.String const_position() {
    return "Position";
  }
  
  public java.lang.String const_genderMan() {
    return "Male";
  }
  
  public java.lang.String userCard_error_userDataNotYetInEvent() {
    return "User's data are not available yet.";
  }
  
  public java.lang.String paymentProfilePanel_error_badIban() {
    return "Bad IBAN format.";
  }
  
  public java.lang.String const_freeUpper() {
    return "FREE";
  }
  
  public java.lang.String const_date_monthOctober() {
    return "October";
  }
  
  public java.lang.String const_saveUpper() {
    return "SAVE";
  }
  
  public java.lang.String const_download() {
    return "Download";
  }
  
  public java.lang.String locale() {
    return "en";
  }
  
  public java.lang.String const_date_agoLower() {
    return "ago";
  }
  
  public java.lang.String paymentProfilePanel_form_registrationNumber() {
    return "Reg. No.";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_features() {
    return "Features";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_pressAndMedia() {
    return "Press & media";
  }
  
  public java.lang.String servletUtils_error_http413() {
    return "The file is too large.";
  }
  
  public java.lang.String const_date_monthMay() {
    return "May";
  }
  
  public java.lang.String menuDispatcherImpl_mainMenu_home() {
    return "Home";
  }
  
  public java.lang.String const_exceptionBadInput() {
    return "You have entered an invalid input.";
  }
  
  public java.lang.String const_ssoNotWork() {
    return "SSO not work";
  }
  
  public java.lang.String const_change() {
    return "Change";
  }
  
  public java.lang.String progress_text3() {
    return "LOG IN";
  }
  
  public java.lang.String const_duplicate() {
    return "Duplicate";
  }
  
  public java.lang.String const_date_monthJune() {
    return "June";
  }
  
  public java.lang.String const_settings() {
    return "Settings";
  }
  
  public java.lang.String const_loadingDots() {
    return "Loading...";
  }
  
  public java.lang.String const_noRecordFound() {
    return "No record has been found.";
  }
}
