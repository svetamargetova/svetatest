package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class BoxItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Integer getId(consys.common.gwt.client.rpc.result.BoxItem instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.BoxItem::id;
  }-*/;
  
  private static native void setId(consys.common.gwt.client.rpc.result.BoxItem instance, java.lang.Integer value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.BoxItem::id = value;
  }-*/;
  
  private static native java.lang.String getName(consys.common.gwt.client.rpc.result.BoxItem instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.BoxItem::name;
  }-*/;
  
  private static native void setName(consys.common.gwt.client.rpc.result.BoxItem instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.BoxItem::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.BoxItem instance) throws SerializationException {
    setId(instance, (java.lang.Integer) streamReader.readObject());
    setName(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.client.rpc.result.BoxItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.BoxItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.BoxItem instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.BoxItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.BoxItem_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.BoxItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.BoxItem_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.BoxItem)object);
  }
  
}
