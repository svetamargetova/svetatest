package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPaymentProduct_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getName(consys.common.gwt.shared.bo.ClientPaymentProduct instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientPaymentProduct::name;
  }-*/;
  
  private static native void setName(consys.common.gwt.shared.bo.ClientPaymentProduct instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientPaymentProduct::name = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getPrice(consys.common.gwt.shared.bo.ClientPaymentProduct instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientPaymentProduct::price;
  }-*/;
  
  private static native void setPrice(consys.common.gwt.shared.bo.ClientPaymentProduct instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientPaymentProduct::price = value;
  }-*/;
  
  private static native int getQuantity(consys.common.gwt.shared.bo.ClientPaymentProduct instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientPaymentProduct::quantity;
  }-*/;
  
  private static native void setQuantity(consys.common.gwt.shared.bo.ClientPaymentProduct instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientPaymentProduct::quantity = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientPaymentProduct instance) throws SerializationException {
    setName(instance, streamReader.readString());
    setPrice(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setQuantity(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientPaymentProduct instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientPaymentProduct();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientPaymentProduct instance) throws SerializationException {
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getPrice(instance));
    streamWriter.writeInt(getQuantity(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientPaymentProduct_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientPaymentProduct_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientPaymentProduct)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientPaymentProduct_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientPaymentProduct)object);
  }
  
}
