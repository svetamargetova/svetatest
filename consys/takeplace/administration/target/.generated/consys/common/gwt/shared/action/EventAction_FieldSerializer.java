package consys.common.gwt.shared.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EventAction_FieldSerializer {
  private static native java.lang.String getEventUuid(consys.common.gwt.shared.action.EventAction instance) /*-{
    return instance.@consys.common.gwt.shared.action.EventAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.common.gwt.shared.action.EventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.action.EventAction::eventUuid = value;
  }-*/;
  
  private static native java.lang.String getUserEventUuid(consys.common.gwt.shared.action.EventAction instance) /*-{
    return instance.@consys.common.gwt.shared.action.EventAction::userEventUuid;
  }-*/;
  
  private static native void setUserEventUuid(consys.common.gwt.shared.action.EventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.action.EventAction::userEventUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.action.EventAction instance) throws SerializationException {
    setEventUuid(instance, streamReader.readString());
    setUserEventUuid(instance, streamReader.readString());
    
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.action.EventAction instance) throws SerializationException {
    streamWriter.writeString(getEventUuid(instance));
    streamWriter.writeString(getUserEventUuid(instance));
    
  }
  
}
