package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class SelectBoxItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getHtml(consys.common.gwt.client.rpc.result.SelectBoxItem instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.SelectBoxItem::html;
  }-*/;
  
  private static native void setHtml(consys.common.gwt.client.rpc.result.SelectBoxItem instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.SelectBoxItem::html = value;
  }-*/;
  
  private static native java.lang.Object getItem(consys.common.gwt.client.rpc.result.SelectBoxItem instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.SelectBoxItem::item;
  }-*/;
  
  private static native void setItem(consys.common.gwt.client.rpc.result.SelectBoxItem instance, java.lang.Object value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.SelectBoxItem::item = value;
  }-*/;
  
  private static native java.lang.String getName(consys.common.gwt.client.rpc.result.SelectBoxItem instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.SelectBoxItem::name;
  }-*/;
  
  private static native void setName(consys.common.gwt.client.rpc.result.SelectBoxItem instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.SelectBoxItem::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.SelectBoxItem instance) throws SerializationException {
    setHtml(instance, streamReader.readString());
    setItem(instance, streamReader.readObject());
    setName(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.client.rpc.result.SelectBoxItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.SelectBoxItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.SelectBoxItem instance) throws SerializationException {
    streamWriter.writeString(getHtml(instance));
    streamWriter.writeObject(getItem(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.SelectBoxItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.SelectBoxItem_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.SelectBoxItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.SelectBoxItem_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.SelectBoxItem)object);
  }
  
}
