package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class BooleanResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getBool(consys.common.gwt.client.rpc.result.BooleanResult instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.BooleanResult::bool;
  }-*/;
  
  private static native void setBool(consys.common.gwt.client.rpc.result.BooleanResult instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.BooleanResult::bool = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.BooleanResult instance) throws SerializationException {
    setBool(instance, streamReader.readBoolean());
    
  }
  
  public static consys.common.gwt.client.rpc.result.BooleanResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.BooleanResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.BooleanResult instance) throws SerializationException {
    streamWriter.writeBoolean(getBool(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.BooleanResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.BooleanResult_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.BooleanResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.BooleanResult_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.BooleanResult)object);
  }
  
}
