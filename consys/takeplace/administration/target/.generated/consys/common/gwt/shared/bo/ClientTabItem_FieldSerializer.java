package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientTabItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getTitle(consys.common.gwt.shared.bo.ClientTabItem instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientTabItem::title;
  }-*/;
  
  private static native void setTitle(consys.common.gwt.shared.bo.ClientTabItem instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientTabItem::title = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientTabItem instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientTabItem::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientTabItem instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientTabItem::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientTabItem instance) throws SerializationException {
    setTitle(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientTabItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientTabItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientTabItem instance) throws SerializationException {
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientTabItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientTabItem_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientTabItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientTabItem_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientTabItem)object);
  }
  
}
