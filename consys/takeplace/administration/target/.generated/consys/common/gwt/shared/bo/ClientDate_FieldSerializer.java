package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientDate_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getDay(consys.common.gwt.shared.bo.ClientDate instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientDate::day;
  }-*/;
  
  private static native void setDay(consys.common.gwt.shared.bo.ClientDate instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientDate::day = value;
  }-*/;
  
  private static native int getMonth(consys.common.gwt.shared.bo.ClientDate instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientDate::month;
  }-*/;
  
  private static native void setMonth(consys.common.gwt.shared.bo.ClientDate instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientDate::month = value;
  }-*/;
  
  private static native int getYear(consys.common.gwt.shared.bo.ClientDate instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientDate::year;
  }-*/;
  
  private static native void setYear(consys.common.gwt.shared.bo.ClientDate instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientDate::year = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientDate instance) throws SerializationException {
    setDay(instance, streamReader.readInt());
    setMonth(instance, streamReader.readInt());
    setYear(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientDate instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientDate();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientDate instance) throws SerializationException {
    streamWriter.writeInt(getDay(instance));
    streamWriter.writeInt(getMonth(instance));
    streamWriter.writeInt(getYear(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientDate_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientDate_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientDate)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientDate_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientDate)object);
  }
  
}
