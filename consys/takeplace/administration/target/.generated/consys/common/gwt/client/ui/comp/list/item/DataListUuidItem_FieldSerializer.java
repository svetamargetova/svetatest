package consys.common.gwt.client.ui.comp.list.item;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DataListUuidItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.common.gwt.client.ui.comp.list.item.DataListUuidItem instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.item.DataListUuidItem::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.client.ui.comp.list.item.DataListUuidItem instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.item.DataListUuidItem::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.ui.comp.list.item.DataListUuidItem instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.client.ui.comp.list.item.DataListUuidItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.ui.comp.list.item.DataListUuidItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.ui.comp.list.item.DataListUuidItem instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(reader, (consys.common.gwt.client.ui.comp.list.item.DataListUuidItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(writer, (consys.common.gwt.client.ui.comp.list.item.DataListUuidItem)object);
  }
  
}
