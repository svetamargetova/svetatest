package consys.common.gwt.client.messaging.message;

public class CommonMessagingConstants_cs implements consys.common.gwt.client.messaging.message.CommonMessagingConstants {
  
  public java.lang.String messagingApi_text_unknownStatus() {
    return "Neznámý stav odpovědi.";
  }
  
  public java.lang.String const_newUpper() {
    return "Nový";
  }
  
  public java.lang.String messageBox_title() {
    return "Zprávy";
  }
  
  public java.lang.String messageInput_text_asLower() {
    return "jako";
  }
  
  public java.lang.String messagingApi_text_entityNotFound() {
    return "Příjemce nebyl nalezen.";
  }
  
  public java.lang.String const_notLoaded() {
    return "Nebylo načteno";
  }
  
  public java.lang.String messagingApi_text_noPermission() {
    return "Nemáte adekvátní oprávnění.";
  }
  
  public java.lang.String messageBoxUI_action_show() {
    return "Zobrazit";
  }
  
  public java.lang.String toPanel_action_selectContact() {
    return "Vybrat kontakt";
  }
  
  public java.lang.String const_to() {
    return "Komu";
  }
  
  public java.lang.String messageSentBoxUI_title() {
    return "Odeslané zprávy";
  }
  
  public java.lang.String messageBox_button_newMessage() {
    return "Nová zpráva";
  }
  
  public java.lang.String messageSendUI_button_sendMessage() {
    return "Odeslat zprávu";
  }
  
  public java.lang.String bugPanel_text_bugGraphic() {
    return "Grafická chyba";
  }
  
  public java.lang.String bugPanel_text_reportBug() {
    return "Oznámení chyby";
  }
  
  public java.lang.String messagingApi_text_banned() {
    return "Uživatel byl vypovězen.";
  }
  
  public java.lang.String bugPanel_text_sendFailed() {
    return "Zaslání oznámení o chybě se nezdařilo.";
  }
  
  public java.lang.String bugPanel_text_bugOther() {
    return "Jiná chyba";
  }
  
  public java.lang.String bugPanel_text_bugFunction() {
    return "Funkční chyba";
  }
  
  public java.lang.String bugPanel_button_sendBug() {
    return "POŠLI";
  }
  
  public java.lang.String messageSendUI_form_message() {
    return "Zpráva";
  }
  
  public java.lang.String messageSendUI_form_subject() {
    return "Předmět";
  }
  
  public java.lang.String messagingApi_text_requestFail() {
    return "Požadavek se nezdařil.";
  }
  
  public java.lang.String eventWallUI_text_emptyWall() {
    return "Zeď je v tuto chvíli prázdná.";
  }
  
  public java.lang.String messageThreadUI_button_reply() {
    return "ODPOVĚDĚT";
  }
  
  public java.lang.String userMessagingModule_text_messaging() {
    return "Zprávy";
  }
  
  public java.lang.String messageBoxUI_title() {
    return "Vlákna";
  }
  
  public java.lang.String messagingApi_text_newMessage() {
    return "Nová zpráva";
  }
  
  public java.lang.String const_communicationError() {
    return "Vyskytla se chyba při komunikaci.";
  }
  
  public java.lang.String messagingApi_text_serverClosed() {
    return "Server je uzavřen.";
  }
  
  public java.lang.String messagingApi_text_messageNotFound() {
    return "Zpráva nebyla nalezena.";
  }
  
  public java.lang.String messageInput_button_publishUpper() {
    return "PUBLIKUJ";
  }
  
  public java.lang.String messageInput_text_messageAdded() {
    return "Zpráva byla publikována.";
  }
  
  public java.lang.String messagingApi_text_threadNotFound() {
    return "Vlákno nebylo nalezeno.";
  }
}
