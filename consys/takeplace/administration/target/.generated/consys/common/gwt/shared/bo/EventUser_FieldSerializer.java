package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EventUser_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFullName(consys.common.gwt.shared.bo.EventUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventUser::fullName;
  }-*/;
  
  private static native void setFullName(consys.common.gwt.shared.bo.EventUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventUser::fullName = value;
  }-*/;
  
  private static native java.lang.String getImageUuidPrefix(consys.common.gwt.shared.bo.EventUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventUser::imageUuidPrefix;
  }-*/;
  
  private static native void setImageUuidPrefix(consys.common.gwt.shared.bo.EventUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventUser::imageUuidPrefix = value;
  }-*/;
  
  private static native java.lang.String getLastName(consys.common.gwt.shared.bo.EventUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventUser::lastName;
  }-*/;
  
  private static native void setLastName(consys.common.gwt.shared.bo.EventUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventUser::lastName = value;
  }-*/;
  
  private static native java.lang.String getOrganization(consys.common.gwt.shared.bo.EventUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventUser::organization;
  }-*/;
  
  private static native void setOrganization(consys.common.gwt.shared.bo.EventUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventUser::organization = value;
  }-*/;
  
  private static native java.lang.String getPosition(consys.common.gwt.shared.bo.EventUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventUser::position;
  }-*/;
  
  private static native void setPosition(consys.common.gwt.shared.bo.EventUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventUser::position = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.EventUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.EventUser::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.EventUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.EventUser::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.EventUser instance) throws SerializationException {
    setFullName(instance, streamReader.readString());
    setImageUuidPrefix(instance, streamReader.readString());
    setLastName(instance, streamReader.readString());
    setOrganization(instance, streamReader.readString());
    setPosition(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.EventUser instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.EventUser();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.EventUser instance) throws SerializationException {
    streamWriter.writeString(getFullName(instance));
    streamWriter.writeString(getImageUuidPrefix(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getOrganization(instance));
    streamWriter.writeString(getPosition(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.EventUser_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.EventUser_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.EventUser)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.EventUser_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.EventUser)object);
  }
  
}
