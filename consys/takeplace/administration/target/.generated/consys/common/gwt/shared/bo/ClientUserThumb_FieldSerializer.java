package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUserThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getName(consys.common.gwt.shared.bo.ClientUserThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserThumb::name;
  }-*/;
  
  private static native void setName(consys.common.gwt.shared.bo.ClientUserThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserThumb::name = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientUserThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserThumb::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientUserThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserThumb::uuid = value;
  }-*/;
  
  private static native java.lang.String getUuidImg(consys.common.gwt.shared.bo.ClientUserThumb instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserThumb::uuidImg;
  }-*/;
  
  private static native void setUuidImg(consys.common.gwt.shared.bo.ClientUserThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserThumb::uuidImg = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientUserThumb instance) throws SerializationException {
    setName(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    setUuidImg(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientUserThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientUserThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientUserThumb instance) throws SerializationException {
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeString(getUuidImg(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientUserThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserThumb_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientUserThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserThumb_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientUserThumb)object);
  }
  
}
