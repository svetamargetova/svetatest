package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientAddress_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCity(consys.common.gwt.shared.bo.ClientAddress instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientAddress::city;
  }-*/;
  
  private static native void setCity(consys.common.gwt.shared.bo.ClientAddress instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientAddress::city = value;
  }-*/;
  
  private static native int getLocation(consys.common.gwt.shared.bo.ClientAddress instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientAddress::location;
  }-*/;
  
  private static native void setLocation(consys.common.gwt.shared.bo.ClientAddress instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientAddress::location = value;
  }-*/;
  
  private static native java.lang.String getLocationName(consys.common.gwt.shared.bo.ClientAddress instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientAddress::locationName;
  }-*/;
  
  private static native void setLocationName(consys.common.gwt.shared.bo.ClientAddress instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientAddress::locationName = value;
  }-*/;
  
  private static native int getStateUs(consys.common.gwt.shared.bo.ClientAddress instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientAddress::stateUs;
  }-*/;
  
  private static native void setStateUs(consys.common.gwt.shared.bo.ClientAddress instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientAddress::stateUs = value;
  }-*/;
  
  private static native java.lang.String getStateUsName(consys.common.gwt.shared.bo.ClientAddress instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientAddress::stateUsName;
  }-*/;
  
  private static native void setStateUsName(consys.common.gwt.shared.bo.ClientAddress instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientAddress::stateUsName = value;
  }-*/;
  
  private static native java.lang.String getStreet(consys.common.gwt.shared.bo.ClientAddress instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientAddress::street;
  }-*/;
  
  private static native void setStreet(consys.common.gwt.shared.bo.ClientAddress instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientAddress::street = value;
  }-*/;
  
  private static native java.lang.String getZip(consys.common.gwt.shared.bo.ClientAddress instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientAddress::zip;
  }-*/;
  
  private static native void setZip(consys.common.gwt.shared.bo.ClientAddress instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientAddress::zip = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientAddress instance) throws SerializationException {
    setCity(instance, streamReader.readString());
    setLocation(instance, streamReader.readInt());
    setLocationName(instance, streamReader.readString());
    setStateUs(instance, streamReader.readInt());
    setStateUsName(instance, streamReader.readString());
    setStreet(instance, streamReader.readString());
    setZip(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientAddress instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientAddress();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientAddress instance) throws SerializationException {
    streamWriter.writeString(getCity(instance));
    streamWriter.writeInt(getLocation(instance));
    streamWriter.writeString(getLocationName(instance));
    streamWriter.writeInt(getStateUs(instance));
    streamWriter.writeString(getStateUsName(instance));
    streamWriter.writeString(getStreet(instance));
    streamWriter.writeString(getZip(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientAddress_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientAddress_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientAddress)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientAddress_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientAddress)object);
  }
  
}
