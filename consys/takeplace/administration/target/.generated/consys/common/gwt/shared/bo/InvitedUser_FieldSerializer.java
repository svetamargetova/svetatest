package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class InvitedUser_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEmail(consys.common.gwt.shared.bo.InvitedUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.InvitedUser::email;
  }-*/;
  
  private static native void setEmail(consys.common.gwt.shared.bo.InvitedUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.InvitedUser::email = value;
  }-*/;
  
  private static native java.lang.String getMessage(consys.common.gwt.shared.bo.InvitedUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.InvitedUser::message;
  }-*/;
  
  private static native void setMessage(consys.common.gwt.shared.bo.InvitedUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.InvitedUser::message = value;
  }-*/;
  
  private static native java.lang.String getName(consys.common.gwt.shared.bo.InvitedUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.InvitedUser::name;
  }-*/;
  
  private static native void setName(consys.common.gwt.shared.bo.InvitedUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.InvitedUser::name = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.InvitedUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.InvitedUser::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.InvitedUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.InvitedUser::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.InvitedUser instance) throws SerializationException {
    setEmail(instance, streamReader.readString());
    setMessage(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.InvitedUser instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.InvitedUser();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.InvitedUser instance) throws SerializationException {
    streamWriter.writeString(getEmail(instance));
    streamWriter.writeString(getMessage(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.InvitedUser_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.InvitedUser_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.InvitedUser)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.InvitedUser_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.InvitedUser)object);
  }
  
}
