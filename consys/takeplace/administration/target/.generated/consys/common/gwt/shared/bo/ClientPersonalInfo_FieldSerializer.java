package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPersonalInfo_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getCeliac(consys.common.gwt.shared.bo.ClientPersonalInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientPersonalInfo::celiac;
  }-*/;
  
  private static native void setCeliac(consys.common.gwt.shared.bo.ClientPersonalInfo instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientPersonalInfo::celiac = value;
  }-*/;
  
  private static native boolean getVegetarian(consys.common.gwt.shared.bo.ClientPersonalInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientPersonalInfo::vegetarian;
  }-*/;
  
  private static native void setVegetarian(consys.common.gwt.shared.bo.ClientPersonalInfo instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientPersonalInfo::vegetarian = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientPersonalInfo instance) throws SerializationException {
    setCeliac(instance, streamReader.readBoolean());
    setVegetarian(instance, streamReader.readBoolean());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientPersonalInfo instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientPersonalInfo();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientPersonalInfo instance) throws SerializationException {
    streamWriter.writeBoolean(getCeliac(instance));
    streamWriter.writeBoolean(getVegetarian(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientPersonalInfo_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientPersonalInfo_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientPersonalInfo)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientPersonalInfo_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientPersonalInfo)object);
  }
  
}
