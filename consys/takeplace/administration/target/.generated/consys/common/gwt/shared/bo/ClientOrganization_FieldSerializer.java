package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientOrganization_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAcronym(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::acronym;
  }-*/;
  
  private static native void setAcronym(consys.common.gwt.shared.bo.ClientOrganization instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::acronym = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientAddress getAddress(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::address;
  }-*/;
  
  private static native void setAddress(consys.common.gwt.shared.bo.ClientOrganization instance, consys.common.gwt.shared.bo.ClientAddress value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::address = value;
  }-*/;
  
  private static native java.lang.String getFullName(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::fullName;
  }-*/;
  
  private static native void setFullName(consys.common.gwt.shared.bo.ClientOrganization instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::fullName = value;
  }-*/;
  
  private static native int getOrganizationType(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::organizationType;
  }-*/;
  
  private static native void setOrganizationType(consys.common.gwt.shared.bo.ClientOrganization instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::organizationType = value;
  }-*/;
  
  private static native boolean getOwner(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::owner;
  }-*/;
  
  private static native void setOwner(consys.common.gwt.shared.bo.ClientOrganization instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::owner = value;
  }-*/;
  
  private static native java.lang.String getUniversalName(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::universalName;
  }-*/;
  
  private static native void setUniversalName(consys.common.gwt.shared.bo.ClientOrganization instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::universalName = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientOrganization instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::uuid = value;
  }-*/;
  
  private static native java.lang.String getWeb(consys.common.gwt.shared.bo.ClientOrganization instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientOrganization::web;
  }-*/;
  
  private static native void setWeb(consys.common.gwt.shared.bo.ClientOrganization instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientOrganization::web = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientOrganization instance) throws SerializationException {
    setAcronym(instance, streamReader.readString());
    setAddress(instance, (consys.common.gwt.shared.bo.ClientAddress) streamReader.readObject());
    setFullName(instance, streamReader.readString());
    setOrganizationType(instance, streamReader.readInt());
    setOwner(instance, streamReader.readBoolean());
    setUniversalName(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    setWeb(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientOrganization instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientOrganization();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientOrganization instance) throws SerializationException {
    streamWriter.writeString(getAcronym(instance));
    streamWriter.writeObject(getAddress(instance));
    streamWriter.writeString(getFullName(instance));
    streamWriter.writeInt(getOrganizationType(instance));
    streamWriter.writeBoolean(getOwner(instance));
    streamWriter.writeString(getUniversalName(instance));
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeString(getWeb(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientOrganization_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientOrganization_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientOrganization)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientOrganization_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientOrganization)object);
  }
  
}
