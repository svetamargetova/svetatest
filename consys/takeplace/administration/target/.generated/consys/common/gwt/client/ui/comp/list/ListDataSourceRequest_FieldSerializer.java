package consys.common.gwt.client.ui.comp.list;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListDataSourceRequest_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDataListTag(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::dataListTag;
  }-*/;
  
  private static native void setDataListTag(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::dataListTag = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.list.Filter getFilter(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::filter;
  }-*/;
  
  private static native void setFilter(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance, consys.common.gwt.shared.bo.list.Filter value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::filter = value;
  }-*/;
  
  private static native int getFirstResult(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::firstResult;
  }-*/;
  
  private static native void setFirstResult(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance, int value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::firstResult = value;
  }-*/;
  
  private static native int getItemsPerPage(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::itemsPerPage;
  }-*/;
  
  private static native void setItemsPerPage(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance, int value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::itemsPerPage = value;
  }-*/;
  
  private static native int[] getOrderers(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance) /*-{
    return instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::orderers;
  }-*/;
  
  private static native void setOrderers(consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance, int[] value) 
  /*-{
    instance.@consys.common.gwt.client.ui.comp.list.ListDataSourceRequest::orderers = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance) throws SerializationException {
    setDataListTag(instance, streamReader.readString());
    setFilter(instance, (consys.common.gwt.shared.bo.list.Filter) streamReader.readObject());
    setFirstResult(instance, streamReader.readInt());
    setItemsPerPage(instance, streamReader.readInt());
    setOrderers(instance, (int[]) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.ui.comp.list.ListDataSourceRequest();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.ui.comp.list.ListDataSourceRequest instance) throws SerializationException {
    streamWriter.writeString(getDataListTag(instance));
    streamWriter.writeObject(getFilter(instance));
    streamWriter.writeInt(getFirstResult(instance));
    streamWriter.writeInt(getItemsPerPage(instance));
    streamWriter.writeObject(getOrderers(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.ui.comp.list.ListDataSourceRequest_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.ListDataSourceRequest_FieldSerializer.deserialize(reader, (consys.common.gwt.client.ui.comp.list.ListDataSourceRequest)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.ListDataSourceRequest_FieldSerializer.serialize(writer, (consys.common.gwt.client.ui.comp.list.ListDataSourceRequest)object);
  }
  
}
