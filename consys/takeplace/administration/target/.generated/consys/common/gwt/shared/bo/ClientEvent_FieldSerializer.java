package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEvent_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAcronym(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::acronym;
  }-*/;
  
  private static native void setAcronym(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::acronym = value;
  }-*/;
  
  private static native java.lang.String getCity(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::city;
  }-*/;
  
  private static native void setCity(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::city = value;
  }-*/;
  
  private static native java.lang.String getDescription(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::description;
  }-*/;
  
  private static native void setDescription(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::description = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientDateTime getFrom(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::from;
  }-*/;
  
  private static native void setFrom(consys.common.gwt.shared.bo.ClientEvent instance, consys.common.gwt.shared.bo.ClientDateTime value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::from = value;
  }-*/;
  
  private static native java.lang.String getFullName(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::fullName;
  }-*/;
  
  private static native void setFullName(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::fullName = value;
  }-*/;
  
  private static native java.lang.Integer getLocation(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::location;
  }-*/;
  
  private static native void setLocation(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.Integer value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::location = value;
  }-*/;
  
  private static native java.lang.String getLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::logoUuidPrefix;
  }-*/;
  
  private static native void setLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::logoUuidPrefix = value;
  }-*/;
  
  private static native java.lang.String getPlace(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::place;
  }-*/;
  
  private static native void setPlace(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::place = value;
  }-*/;
  
  private static native java.lang.String getProfileLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::profileLogoUuidPrefix;
  }-*/;
  
  private static native void setProfileLogoUuidPrefix(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::profileLogoUuidPrefix = value;
  }-*/;
  
  private static native java.lang.String getSeries(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::series;
  }-*/;
  
  private static native void setSeries(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::series = value;
  }-*/;
  
  private static native java.lang.String getShortName(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::shortName;
  }-*/;
  
  private static native void setShortName(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::shortName = value;
  }-*/;
  
  private static native java.lang.String getStreet(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::street;
  }-*/;
  
  private static native void setStreet(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::street = value;
  }-*/;
  
  private static native java.lang.String getTags(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::tags;
  }-*/;
  
  private static native void setTags(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::tags = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientDateTime getTo(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::to;
  }-*/;
  
  private static native void setTo(consys.common.gwt.shared.bo.ClientEvent instance, consys.common.gwt.shared.bo.ClientDateTime value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::to = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientEventType getType(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::type;
  }-*/;
  
  private static native void setType(consys.common.gwt.shared.bo.ClientEvent instance, consys.common.gwt.shared.bo.ClientEventType value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::type = value;
  }-*/;
  
  private static native java.lang.String getUniversalName(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::universalName;
  }-*/;
  
  private static native void setUniversalName(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::universalName = value;
  }-*/;
  
  private static native java.lang.Integer getUsState(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::usState;
  }-*/;
  
  private static native void setUsState(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.Integer value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::usState = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::uuid = value;
  }-*/;
  
  private static native boolean getVisible(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::visible;
  }-*/;
  
  private static native void setVisible(consys.common.gwt.shared.bo.ClientEvent instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::visible = value;
  }-*/;
  
  private static native java.lang.String getWeb(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::web;
  }-*/;
  
  private static native void setWeb(consys.common.gwt.shared.bo.ClientEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::web = value;
  }-*/;
  
  private static native int getYear(consys.common.gwt.shared.bo.ClientEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientEvent::year;
  }-*/;
  
  private static native void setYear(consys.common.gwt.shared.bo.ClientEvent instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientEvent::year = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientEvent instance) throws SerializationException {
    setAcronym(instance, streamReader.readString());
    setCity(instance, streamReader.readString());
    setDescription(instance, streamReader.readString());
    setFrom(instance, (consys.common.gwt.shared.bo.ClientDateTime) streamReader.readObject());
    setFullName(instance, streamReader.readString());
    setLocation(instance, (java.lang.Integer) streamReader.readObject());
    setLogoUuidPrefix(instance, streamReader.readString());
    setPlace(instance, streamReader.readString());
    setProfileLogoUuidPrefix(instance, streamReader.readString());
    setSeries(instance, streamReader.readString());
    setShortName(instance, streamReader.readString());
    setStreet(instance, streamReader.readString());
    setTags(instance, streamReader.readString());
    setTo(instance, (consys.common.gwt.shared.bo.ClientDateTime) streamReader.readObject());
    setType(instance, (consys.common.gwt.shared.bo.ClientEventType) streamReader.readObject());
    setUniversalName(instance, streamReader.readString());
    setUsState(instance, (java.lang.Integer) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    setVisible(instance, streamReader.readBoolean());
    setWeb(instance, streamReader.readString());
    setYear(instance, streamReader.readInt());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientEvent instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientEvent();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientEvent instance) throws SerializationException {
    streamWriter.writeString(getAcronym(instance));
    streamWriter.writeString(getCity(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeString(getFullName(instance));
    streamWriter.writeObject(getLocation(instance));
    streamWriter.writeString(getLogoUuidPrefix(instance));
    streamWriter.writeString(getPlace(instance));
    streamWriter.writeString(getProfileLogoUuidPrefix(instance));
    streamWriter.writeString(getSeries(instance));
    streamWriter.writeString(getShortName(instance));
    streamWriter.writeString(getStreet(instance));
    streamWriter.writeString(getTags(instance));
    streamWriter.writeObject(getTo(instance));
    streamWriter.writeObject(getType(instance));
    streamWriter.writeString(getUniversalName(instance));
    streamWriter.writeObject(getUsState(instance));
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeBoolean(getVisible(instance));
    streamWriter.writeString(getWeb(instance));
    streamWriter.writeInt(getYear(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientEvent_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEvent_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientEvent)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientEvent_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientEvent)object);
  }
  
}
