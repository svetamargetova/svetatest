package consys.common.gwt.client.widget.message;

public class CommonsWidgetsMessages_cs implements consys.common.gwt.client.widget.message.CommonsWidgetsMessages {
  
  public java.lang.String loginContent_error_ssoFailed(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Přihlásení se do Takeplace použitím Vašeho ").append(arg0).append(" účtu selhalo. Prosím použijte jinou službu nebo si vytvořte Váš Takeplace účet.").toString();
  }
  
  public java.lang.String loginContent_error_ssoDenied(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Odmítli jste použít Váš ").append(arg0).append(" účet pro přihlásení se do Takeplace. Prosím použijte jinou službu nebo si vytvořte Váš Takeplace účet.").toString();
  }
}
