package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class SelectBoxData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getList(consys.common.gwt.client.rpc.result.SelectBoxData instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.SelectBoxData::list;
  }-*/;
  
  private static native void setList(consys.common.gwt.client.rpc.result.SelectBoxData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.SelectBoxData::list = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.SelectBoxData instance) throws SerializationException {
    setList(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.client.rpc.result.SelectBoxData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.SelectBoxData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.SelectBoxData instance) throws SerializationException {
    streamWriter.writeObject(getList(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.SelectBoxData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.SelectBoxData_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.SelectBoxData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.SelectBoxData_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.SelectBoxData)object);
  }
  
}
