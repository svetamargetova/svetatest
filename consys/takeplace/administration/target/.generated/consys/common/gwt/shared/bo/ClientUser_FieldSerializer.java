package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUser_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getActivatedUser(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::activatedUser;
  }-*/;
  
  private static native void setActivatedUser(consys.common.gwt.shared.bo.ClientUser instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::activatedUser = value;
  }-*/;
  
  private static native java.lang.String getBio(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::bio;
  }-*/;
  
  private static native void setBio(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::bio = value;
  }-*/;
  
  private static native java.lang.String getFirstName(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::firstName;
  }-*/;
  
  private static native void setFirstName(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::firstName = value;
  }-*/;
  
  private static native java.lang.String getFrontDegree(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::frontDegree;
  }-*/;
  
  private static native void setFrontDegree(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::frontDegree = value;
  }-*/;
  
  private static native int getGender(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::gender;
  }-*/;
  
  private static native void setGender(consys.common.gwt.shared.bo.ClientUser instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::gender = value;
  }-*/;
  
  private static native java.lang.String getLastName(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::lastName;
  }-*/;
  
  private static native void setLastName(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::lastName = value;
  }-*/;
  
  private static native java.lang.String getLoginEmail(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::loginEmail;
  }-*/;
  
  private static native void setLoginEmail(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::loginEmail = value;
  }-*/;
  
  private static native java.lang.String getMiddleName(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::middleName;
  }-*/;
  
  private static native void setMiddleName(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::middleName = value;
  }-*/;
  
  private static native java.lang.String getRearDegree(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::rearDegree;
  }-*/;
  
  private static native void setRearDegree(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::rearDegree = value;
  }-*/;
  
  private static native java.lang.String getUserOrgDefault(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::userOrgDefault;
  }-*/;
  
  private static native void setUserOrgDefault(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::userOrgDefault = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::uuid = value;
  }-*/;
  
  private static native java.lang.String getUuidImg(consys.common.gwt.shared.bo.ClientUser instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUser::uuidImg;
  }-*/;
  
  private static native void setUuidImg(consys.common.gwt.shared.bo.ClientUser instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUser::uuidImg = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientUser instance) throws SerializationException {
    setActivatedUser(instance, streamReader.readBoolean());
    setBio(instance, streamReader.readString());
    setFirstName(instance, streamReader.readString());
    setFrontDegree(instance, streamReader.readString());
    setGender(instance, streamReader.readInt());
    setLastName(instance, streamReader.readString());
    setLoginEmail(instance, streamReader.readString());
    setMiddleName(instance, streamReader.readString());
    setRearDegree(instance, streamReader.readString());
    setUserOrgDefault(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    setUuidImg(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientUser instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientUser();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientUser instance) throws SerializationException {
    streamWriter.writeBoolean(getActivatedUser(instance));
    streamWriter.writeString(getBio(instance));
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getFrontDegree(instance));
    streamWriter.writeInt(getGender(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getLoginEmail(instance));
    streamWriter.writeString(getMiddleName(instance));
    streamWriter.writeString(getRearDegree(instance));
    streamWriter.writeString(getUserOrgDefault(instance));
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeString(getUuidImg(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientUser_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUser_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientUser)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUser_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientUser)object);
  }
  
}
