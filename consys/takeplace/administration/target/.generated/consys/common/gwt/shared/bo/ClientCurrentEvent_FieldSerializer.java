package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCurrentEvent_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientEvent getEvent(consys.common.gwt.shared.bo.ClientCurrentEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientCurrentEvent::event;
  }-*/;
  
  private static native void setEvent(consys.common.gwt.shared.bo.ClientCurrentEvent instance, consys.common.gwt.shared.bo.ClientEvent value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientCurrentEvent::event = value;
  }-*/;
  
  private static native boolean getJustVisitor(consys.common.gwt.shared.bo.ClientCurrentEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientCurrentEvent::justVisitor;
  }-*/;
  
  private static native void setJustVisitor(consys.common.gwt.shared.bo.ClientCurrentEvent instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientCurrentEvent::justVisitor = value;
  }-*/;
  
  private static native java.lang.String getOverseerUrl(consys.common.gwt.shared.bo.ClientCurrentEvent instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientCurrentEvent::overseerUrl;
  }-*/;
  
  private static native void setOverseerUrl(consys.common.gwt.shared.bo.ClientCurrentEvent instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientCurrentEvent::overseerUrl = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientCurrentEvent instance) throws SerializationException {
    setEvent(instance, (consys.common.gwt.shared.bo.ClientEvent) streamReader.readObject());
    setJustVisitor(instance, streamReader.readBoolean());
    setOverseerUrl(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientCurrentEvent instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientCurrentEvent();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientCurrentEvent instance) throws SerializationException {
    streamWriter.writeObject(getEvent(instance));
    streamWriter.writeBoolean(getJustVisitor(instance));
    streamWriter.writeString(getOverseerUrl(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientCurrentEvent_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientCurrentEvent_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientCurrentEvent)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientCurrentEvent_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientCurrentEvent)object);
  }
  
}
