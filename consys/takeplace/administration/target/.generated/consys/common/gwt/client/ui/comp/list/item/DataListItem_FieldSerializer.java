package consys.common.gwt.client.ui.comp.list.item;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DataListItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.ui.comp.list.item.DataListItem instance) throws SerializationException {
    
  }
  
  public static consys.common.gwt.client.ui.comp.list.item.DataListItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.ui.comp.list.item.DataListItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.ui.comp.list.item.DataListItem instance) throws SerializationException {
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer.deserialize(reader, (consys.common.gwt.client.ui.comp.list.item.DataListItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.ui.comp.list.item.DataListItem_FieldSerializer.serialize(writer, (consys.common.gwt.client.ui.comp.list.item.DataListItem)object);
  }
  
}
