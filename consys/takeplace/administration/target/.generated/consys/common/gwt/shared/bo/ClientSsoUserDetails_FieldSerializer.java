package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientSsoUserDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAffiliation(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::affiliation;
  }-*/;
  
  private static native void setAffiliation(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::affiliation = value;
  }-*/;
  
  private static native java.lang.String getBio(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::bio;
  }-*/;
  
  private static native void setBio(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::bio = value;
  }-*/;
  
  private static native java.lang.String getFirstName(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::firstName;
  }-*/;
  
  private static native void setFirstName(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::firstName = value;
  }-*/;
  
  private static native consys.common.constants.sso.SSO getFromSso(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::fromSso;
  }-*/;
  
  private static native void setFromSso(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, consys.common.constants.sso.SSO value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::fromSso = value;
  }-*/;
  
  private static native int getGender(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::gender;
  }-*/;
  
  private static native void setGender(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, int value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::gender = value;
  }-*/;
  
  private static native java.lang.String getLastName(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::lastName;
  }-*/;
  
  private static native void setLastName(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::lastName = value;
  }-*/;
  
  private static native java.lang.String getLoginEmail(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::loginEmail;
  }-*/;
  
  private static native void setLoginEmail(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::loginEmail = value;
  }-*/;
  
  private static native java.lang.String getMiddleName(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::middleName;
  }-*/;
  
  private static native void setMiddleName(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::middleName = value;
  }-*/;
  
  private static native java.lang.String getPassword(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::password;
  }-*/;
  
  private static native void setPassword(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::password = value;
  }-*/;
  
  private static native java.lang.String getProfilePictureUrl(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::profilePictureUrl;
  }-*/;
  
  private static native void setProfilePictureUrl(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::profilePictureUrl = value;
  }-*/;
  
  private static native java.lang.Long getSsoId(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::ssoId;
  }-*/;
  
  private static native void setSsoId(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.Long value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::ssoId = value;
  }-*/;
  
  private static native java.lang.String getSsoStringId(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::ssoStringId;
  }-*/;
  
  private static native void setSsoStringId(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::ssoStringId = value;
  }-*/;
  
  private static native java.lang.String getUserUuid(consys.common.gwt.shared.bo.ClientSsoUserDetails instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::userUuid;
  }-*/;
  
  private static native void setUserUuid(consys.common.gwt.shared.bo.ClientSsoUserDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientSsoUserDetails::userUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientSsoUserDetails instance) throws SerializationException {
    setAffiliation(instance, streamReader.readString());
    setBio(instance, streamReader.readString());
    setFirstName(instance, streamReader.readString());
    setFromSso(instance, (consys.common.constants.sso.SSO) streamReader.readObject());
    setGender(instance, streamReader.readInt());
    setLastName(instance, streamReader.readString());
    setLoginEmail(instance, streamReader.readString());
    setMiddleName(instance, streamReader.readString());
    setPassword(instance, streamReader.readString());
    setProfilePictureUrl(instance, streamReader.readString());
    setSsoId(instance, (java.lang.Long) streamReader.readObject());
    setSsoStringId(instance, streamReader.readString());
    setUserUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientSsoUserDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientSsoUserDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientSsoUserDetails instance) throws SerializationException {
    streamWriter.writeString(getAffiliation(instance));
    streamWriter.writeString(getBio(instance));
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeObject(getFromSso(instance));
    streamWriter.writeInt(getGender(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getLoginEmail(instance));
    streamWriter.writeString(getMiddleName(instance));
    streamWriter.writeString(getPassword(instance));
    streamWriter.writeString(getProfilePictureUrl(instance));
    streamWriter.writeObject(getSsoId(instance));
    streamWriter.writeString(getSsoStringId(instance));
    streamWriter.writeString(getUserUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientSsoUserDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientSsoUserDetails_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientSsoUserDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientSsoUserDetails_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientSsoUserDetails)object);
  }
  
}
