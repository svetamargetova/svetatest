package consys.common.gwt.client.widget.exception;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UserExistsException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.widget.exception.UserExistsException instance) throws SerializationException {
    
    consys.common.gwt.shared.action.ActionException_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.client.widget.exception.UserExistsException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.widget.exception.UserExistsException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.widget.exception.UserExistsException instance) throws SerializationException {
    
    consys.common.gwt.shared.action.ActionException_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.widget.exception.UserExistsException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.widget.exception.UserExistsException_FieldSerializer.deserialize(reader, (consys.common.gwt.client.widget.exception.UserExistsException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.widget.exception.UserExistsException_FieldSerializer.serialize(writer, (consys.common.gwt.client.widget.exception.UserExistsException)object);
  }
  
}
