package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientDateTime_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientDate getDate(consys.common.gwt.shared.bo.ClientDateTime instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientDateTime::date;
  }-*/;
  
  private static native void setDate(consys.common.gwt.shared.bo.ClientDateTime instance, consys.common.gwt.shared.bo.ClientDate value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientDateTime::date = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientTime getTime(consys.common.gwt.shared.bo.ClientDateTime instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientDateTime::time;
  }-*/;
  
  private static native void setTime(consys.common.gwt.shared.bo.ClientDateTime instance, consys.common.gwt.shared.bo.ClientTime value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientDateTime::time = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientDateTime instance) throws SerializationException {
    setDate(instance, (consys.common.gwt.shared.bo.ClientDate) streamReader.readObject());
    setTime(instance, (consys.common.gwt.shared.bo.ClientTime) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientDateTime instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientDateTime();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientDateTime instance) throws SerializationException {
    streamWriter.writeObject(getDate(instance));
    streamWriter.writeObject(getTime(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientDateTime_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientDateTime_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientDateTime)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientDateTime_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientDateTime)object);
  }
  
}
