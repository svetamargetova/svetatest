package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateOuterRegisterAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.HashMap getAnswers(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::answers;
  }-*/;
  
  private static native void setAnswers(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, java.util.HashMap value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::answers = value;
  }-*/;
  
  private static native java.lang.String getApiKey(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::apiKey;
  }-*/;
  
  private static native void setApiKey(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::apiKey = value;
  }-*/;
  
  private static native java.lang.String getDiscountTicket(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::discountTicket;
  }-*/;
  
  private static native void setDiscountTicket(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::discountTicket = value;
  }-*/;
  
  private static native java.lang.String getEventUuid(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::eventUuid = value;
  }-*/;
  
  private static native java.lang.String getPackageUuid(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::packageUuid;
  }-*/;
  
  private static native void setPackageUuid(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::packageUuid = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientUserPaymentProfile getPaymentProfile(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::paymentProfile;
  }-*/;
  
  private static native void setPaymentProfile(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, consys.common.gwt.shared.bo.ClientUserPaymentProfile value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::paymentProfile = value;
  }-*/;
  
  private static native int getQuantity(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::quantity;
  }-*/;
  
  private static native void setQuantity(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, int value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::quantity = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientSsoUserDetails getRegistration(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::registration;
  }-*/;
  
  private static native void setRegistration(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, consys.common.gwt.shared.bo.ClientSsoUserDetails value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::registration = value;
  }-*/;
  
  private static native java.util.ArrayList getSubbundles(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::subbundles;
  }-*/;
  
  private static native void setSubbundles(consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.CreateOuterRegisterAction::subbundles = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) throws SerializationException {
    setAnswers(instance, (java.util.HashMap) streamReader.readObject());
    setApiKey(instance, streamReader.readString());
    setDiscountTicket(instance, streamReader.readString());
    setEventUuid(instance, streamReader.readString());
    setPackageUuid(instance, streamReader.readString());
    setPaymentProfile(instance, (consys.common.gwt.shared.bo.ClientUserPaymentProfile) streamReader.readObject());
    setQuantity(instance, streamReader.readInt());
    setRegistration(instance, (consys.common.gwt.shared.bo.ClientSsoUserDetails) streamReader.readObject());
    setSubbundles(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.CreateOuterRegisterAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.CreateOuterRegisterAction instance) throws SerializationException {
    streamWriter.writeObject(getAnswers(instance));
    streamWriter.writeString(getApiKey(instance));
    streamWriter.writeString(getDiscountTicket(instance));
    streamWriter.writeString(getEventUuid(instance));
    streamWriter.writeString(getPackageUuid(instance));
    streamWriter.writeObject(getPaymentProfile(instance));
    streamWriter.writeInt(getQuantity(instance));
    streamWriter.writeObject(getRegistration(instance));
    streamWriter.writeObject(getSubbundles(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.CreateOuterRegisterAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.CreateOuterRegisterAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.CreateOuterRegisterAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.CreateOuterRegisterAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.CreateOuterRegisterAction)object);
  }
  
}
