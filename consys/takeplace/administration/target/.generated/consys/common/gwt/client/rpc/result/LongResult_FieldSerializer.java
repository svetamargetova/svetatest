package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LongResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getValue(consys.common.gwt.client.rpc.result.LongResult instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.LongResult::value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setValue(consys.common.gwt.client.rpc.result.LongResult instance, long value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.LongResult::value = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.LongResult instance) throws SerializationException {
    setValue(instance, streamReader.readLong());
    
  }
  
  public static consys.common.gwt.client.rpc.result.LongResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.LongResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.LongResult instance) throws SerializationException {
    streamWriter.writeLong(getValue(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.LongResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.LongResult_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.LongResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.LongResult_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.LongResult)object);
  }
  
}
