package consys.common.gwt.client.ui.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class CommonResources_cs_InlineClientBundleGenerator implements consys.common.gwt.client.ui.resource.CommonResources {
  private static CommonResources_cs_InlineClientBundleGenerator _instance0 = new CommonResources_cs_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new consys.common.gwt.client.ui.css.CommonCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return (".GHAOYQ1KO{color:" + ("#c20d0d")  + ";}.GHAOYQ1OO{color:" + ("#7a9e24")  + ";}");
      }
      public java.lang.String action(){
        return "GHAOYQ1IO";
      }
      public java.lang.String active(){
        return "GHAOYQ1JO";
      }
      public java.lang.String error(){
        return "GHAOYQ1KO";
      }
      public java.lang.String leftColumn(){
        return "GHAOYQ1LO";
      }
      public java.lang.String notActive(){
        return "GHAOYQ1MO";
      }
      public java.lang.String rightColumn(){
        return "GHAOYQ1NO";
      }
      public java.lang.String success(){
        return "GHAOYQ1OO";
      }
      public java.lang.String title(){
        return "GHAOYQ1PO";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.common.gwt.client.ui.css.CommonCss get() {
      return css;
    }
  }
  public consys.common.gwt.client.ui.css.CommonCss css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.common.gwt.client.ui.css.CommonCss css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@consys.common.gwt.client.ui.resource.CommonResources::css()();
    }
    return null;
  }-*/;
}
