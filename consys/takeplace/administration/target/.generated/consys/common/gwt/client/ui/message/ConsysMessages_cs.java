package consys.common.gwt.client.ui.message;

public class ConsysMessages_cs implements consys.common.gwt.client.ui.message.ConsysMessages {
  
  public java.lang.String const_invalidValueInField(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Neplatná hodnota v poli ").append(arg0).append(".").toString();
  }
  
  public java.lang.String const_mustChoose(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Musíte vybrat hodnotu v poli ").append(arg0).append(".").toString();
  }
  
  public java.lang.String const_fieldMustInteger(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Hodnota ").append(arg0).append(" musí být celé číslo.").toString();
  }
  
  public java.lang.String const_hoursLower(int arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" hodin").toString();
  }
  
  public java.lang.String const_fieldMustEntered(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Pole ").append(arg0).append(" musí být zadáno.").toString();
  }
  
  public java.lang.String const_minutesLower(int arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" minut").toString();
  }
  
  public java.lang.String const_notInCache(java.lang.String arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" není v paměti.").toString();
  }
  
  public java.lang.String const_more(int arg0) {
    return new java.lang.StringBuffer().append("Více (").append(arg0).append(")").toString();
  }
  
  public java.lang.String const_notInRange(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Hodnota ").append(arg0).append(" není povolena.").toString();
  }
  
  public java.lang.String subbundlePanel_text_freeCapacity(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule_cs().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("Kapacita je již vyčerpaná.").toString();
        break;
      default: // non-exact matches
        switch (arg0_form) {
          case 2:  // few
            returnVal = new java.lang.StringBuffer().append("Zbýva už jen ").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" registrací.").toString();
            break;
          case 1:  // one
            returnVal = new java.lang.StringBuffer().append("Poslední registrace k dispozici").toString();
            break;
        }
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("Zbýva už jen ").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" registrací.").toString();
  }
  
  public java.lang.String const_notAfter(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append(arg1).append(" musí být za ").append(arg0).append(".").toString();
  }
  
  public java.lang.String consysAutoFileUpload_error_invalidExtensions(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Neplatná přípona souboru! Je vyžadována jedna z přípon: ").append(arg0).toString();
  }
  
  public java.lang.String consysAutoFileUpload_error_invalidExtension(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Neplatná přípona souboru! Je vyžadována přípona: ").append(arg0).toString();
  }
  
  public java.lang.String const_fieldMustNumeric(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Hodnota ").append(arg0).append(" musí být číslo.").toString();
  }
  
  public java.lang.String const_fieldUpdateSuccess(java.lang.String arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" byla úspěšne aktualizována.").toString();
  }
  
  public java.lang.String const_fieldUpdateFailed(java.lang.String arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" NEBYLA úspěšne aktualizována. Nastala chyba.").toString();
  }
  
  public java.lang.String const_missingParameter(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Chybí parametr ").append(arg0).toString();
  }
  
  public java.lang.String const_fieldMustBeInRange(java.lang.String arg0,int arg1,int arg2) {
    return new java.lang.StringBuffer().append("Hodnota ").append(arg0).append(" musí být mezi ").append(arg1).append(" a ").append(arg2).append(".").toString();
  }
}
