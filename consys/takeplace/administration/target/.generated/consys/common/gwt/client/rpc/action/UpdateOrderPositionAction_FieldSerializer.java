package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateOrderPositionAction_FieldSerializer {
  private static native int getNewPosition(consys.common.gwt.client.rpc.action.UpdateOrderPositionAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.UpdateOrderPositionAction::newPosition;
  }-*/;
  
  private static native void setNewPosition(consys.common.gwt.client.rpc.action.UpdateOrderPositionAction instance, int value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.UpdateOrderPositionAction::newPosition = value;
  }-*/;
  
  private static native int getOldPosition(consys.common.gwt.client.rpc.action.UpdateOrderPositionAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.UpdateOrderPositionAction::oldPosition;
  }-*/;
  
  private static native void setOldPosition(consys.common.gwt.client.rpc.action.UpdateOrderPositionAction instance, int value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.UpdateOrderPositionAction::oldPosition = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.UpdateOrderPositionAction instance) throws SerializationException {
    setNewPosition(instance, streamReader.readInt());
    setOldPosition(instance, streamReader.readInt());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.UpdateOrderPositionAction instance) throws SerializationException {
    streamWriter.writeInt(getNewPosition(instance));
    streamWriter.writeInt(getOldPosition(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
}
