package consys.common.gwt.client.rpc.result;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class SelectBoxResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getList(consys.common.gwt.client.rpc.result.SelectBoxResult instance) /*-{
    return instance.@consys.common.gwt.client.rpc.result.SelectBoxResult::list;
  }-*/;
  
  private static native void setList(consys.common.gwt.client.rpc.result.SelectBoxResult instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.result.SelectBoxResult::list = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.result.SelectBoxResult instance) throws SerializationException {
    setList(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.client.rpc.result.SelectBoxResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.result.SelectBoxResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.result.SelectBoxResult instance) throws SerializationException {
    streamWriter.writeObject(getList(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.result.SelectBoxResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.SelectBoxResult_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.result.SelectBoxResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.result.SelectBoxResult_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.result.SelectBoxResult)object);
  }
  
}
