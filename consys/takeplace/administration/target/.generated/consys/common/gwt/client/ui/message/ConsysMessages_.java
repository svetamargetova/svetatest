package consys.common.gwt.client.ui.message;

public class ConsysMessages_ implements consys.common.gwt.client.ui.message.ConsysMessages {
  
  public java.lang.String const_invalidValueInField(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Value in field ").append(arg0).append(" is invalid.").toString();
  }
  
  public java.lang.String const_mustChoose(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("You must choose a value from the ").append(arg0).append(" field.").toString();
  }
  
  public java.lang.String const_fieldMustInteger(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("The value of ").append(arg0).append(" must be integer.").toString();
  }
  
  public java.lang.String const_hoursLower(int arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" hours").toString();
  }
  
  public java.lang.String const_fieldMustEntered(java.lang.String arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" field must be entered.").toString();
  }
  
  public java.lang.String const_minutesLower(int arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" minutes").toString();
  }
  
  public java.lang.String const_notInCache(java.lang.String arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" is not in cache.").toString();
  }
  
  public java.lang.String const_more(int arg0) {
    return new java.lang.StringBuffer().append("More (").append(arg0).append(")").toString();
  }
  
  public java.lang.String const_notInRange(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("The value of ").append(arg0).append(" is not allowed.").toString();
  }
  
  public java.lang.String subbundlePanel_text_freeCapacity(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("The capacity is full.").toString();
        break;
      case 1:  // =1
        returnVal = new java.lang.StringBuffer().append("The very last registration is available.").toString();
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("Only ").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" registrations left.").toString();
  }
  
  public java.lang.String const_notAfter(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append(arg1).append(" must be after ").append(arg0).append(".").toString();
  }
  
  public java.lang.String consysAutoFileUpload_error_invalidExtensions(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Invalid file extension! Required extensions: ").append(arg0).toString();
  }
  
  public java.lang.String consysAutoFileUpload_error_invalidExtension(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Invalid file extension! Required extension: ").append(arg0).toString();
  }
  
  public java.lang.String const_fieldMustNumeric(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("The value of ").append(arg0).append(" must be numeric.").toString();
  }
  
  public java.lang.String const_fieldUpdateSuccess(java.lang.String arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" was updated successfully.").toString();
  }
  
  public java.lang.String const_fieldUpdateFailed(java.lang.String arg0) {
    return new java.lang.StringBuffer().append(arg0).append(" was NOT updated successfully. An error occured.").toString();
  }
  
  public java.lang.String const_missingParameter(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Missing parameter ").append(arg0).toString();
  }
  
  public java.lang.String const_fieldMustBeInRange(java.lang.String arg0,int arg1,int arg2) {
    return new java.lang.StringBuffer().append("The value of ").append(arg0).append(" must be within ").append(arg1).append(" and ").append(arg2).append(".").toString();
  }
}
