package consys.common.gwt.client.ui.comp.wrapper;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class WrapperResources_gecko1_8_default_InlineClientBundleGenerator implements consys.common.gwt.client.ui.comp.wrapper.WrapperResources {
  private static WrapperResources_gecko1_8_default_InlineClientBundleGenerator _instance0 = new WrapperResources_gecko1_8_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new consys.common.gwt.client.ui.comp.wrapper.WrapperCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GHAOYQ1AN{clear:" + ("both")  + ";}.GHAOYQ1DN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1CN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getTop() + "px  repeat-y")  + ";overflow:" + ("visible")  + ";}.GHAOYQ1BN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getHeight() + "px")  + ";width:") + (((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1EN .GHAOYQ1EO{position:" + ("relative")  + ";}.GHAOYQ1EN .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1EN .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getHeight() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("right")  + ";}.GHAOYQ1EN .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1JN{position:" + ("relative")  + ";top:" + ("0")  + ";}.GHAOYQ1LN{height:" + ("100%")  + ";position:") + (("absolute")  + ";top:" + ("0")  + ";}.GHAOYQ1EN .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1KN{position:" + ("relative")  + ";}.GHAOYQ1MN{height:" + ("100%")  + ";position:" + ("absolute")  + ";top:" + ("0")  + ";}.GHAOYQ1EN .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1EN .GHAOYQ1FN{position:" + ("relative")  + ";}.GHAOYQ1EN .GHAOYQ1HN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1EN .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getTop() + "px  repeat-x")  + ";float:") + (("right")  + ";}.GHAOYQ1EN .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1EN .GHAOYQ1KN{background-color:" + ("#fff")  + ";}.GHAOYQ1DO{cursor:" + ("text")  + ";}.GHAOYQ1DO .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1DO .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("right")  + ";}.GHAOYQ1DO .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getTop() + "px  no-repeat")  + ";float:") + (("right")  + ";}.GHAOYQ1DO .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1DO .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1DO .GHAOYQ1HN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getTop() + "px  no-repeat") ) + (";float:" + ("right")  + ";}.GHAOYQ1DO .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getTop() + "px  repeat-x")  + ";float:" + ("right")  + ";}.GHAOYQ1DO .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1DO .GHAOYQ1KN{background-color:") + (("#eaf8fc")  + ";}.GHAOYQ1CO .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1CO .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("right")  + ";}.GHAOYQ1CO .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getHeight() + "px") ) + (";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1CO .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1CO .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1CO .GHAOYQ1HN{height:") + (((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1CO .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getTop() + "px  repeat-x")  + ";float:" + ("right")  + ";}.GHAOYQ1CO .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1CO .GHAOYQ1KN{background-color:" + ("#fff")  + ";}.GHAOYQ1BO .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1BO .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("right")  + ";}.GHAOYQ1BO .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1BO .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getTop() + "px  repeat-y") ) + (";}.GHAOYQ1BO .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1BO .GHAOYQ1HN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1BO .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getTop() + "px  repeat-x")  + ";float:" + ("right")  + ";}.GHAOYQ1BO .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("right")  + ";}.GHAOYQ1BO .GHAOYQ1KN{background-color:" + ("#d4d4d4")  + ";}.GHAOYQ1PN,.GHAOYQ1ON{border:" + ("none")  + ";background-color:" + ("#fff")  + ";}.GHAOYQ1AO{border:" + ("none") ) + (";background-color:" + ("#eaf8fc")  + ";}.GHAOYQ1NN{border:" + ("none")  + ";background-color:" + ("#d4d4d4")  + ";}")) : ((".GHAOYQ1AN{clear:" + ("both")  + ";}.GHAOYQ1DN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperTop()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1CN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperContent()).getTop() + "px  repeat-y")  + ";overflow:" + ("visible")  + ";}.GHAOYQ1BN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getHeight() + "px")  + ";width:") + (((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.panelWrapperBottom()).getTop() + "px  no-repeat")  + ";}.GHAOYQ1EN .GHAOYQ1EO{position:" + ("relative")  + ";}.GHAOYQ1EN .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1EN .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getHeight() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("left")  + ";}.GHAOYQ1EN .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperTopRight()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1JN{position:" + ("relative")  + ";top:" + ("0")  + ";}.GHAOYQ1LN{height:" + ("100%")  + ";position:") + (("absolute")  + ";top:" + ("0")  + ";}.GHAOYQ1EN .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterLeft()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1KN{position:" + ("relative")  + ";}.GHAOYQ1MN{height:" + ("100%")  + ";position:" + ("absolute")  + ";top:" + ("0")  + ";}.GHAOYQ1EN .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1EN .GHAOYQ1FN{position:" + ("relative")  + ";}.GHAOYQ1EN .GHAOYQ1HN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomLeft()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1EN .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomCenter()).getTop() + "px  repeat-x")  + ";float:") + (("left")  + ";}.GHAOYQ1EN .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1EN .GHAOYQ1KN{background-color:" + ("#fff")  + ";}.GHAOYQ1DO{cursor:" + ("text")  + ";}.GHAOYQ1DO .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1DO .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("left")  + ";}.GHAOYQ1DO .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverTopRight()).getTop() + "px  no-repeat")  + ";float:") + (("left")  + ";}.GHAOYQ1DO .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterLeft()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1DO .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1DO .GHAOYQ1HN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomLeft()).getTop() + "px  no-repeat") ) + (";float:" + ("left")  + ";}.GHAOYQ1DO .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomCenter()).getTop() + "px  repeat-x")  + ";float:" + ("left")  + ";}.GHAOYQ1DO .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperOverBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1DO .GHAOYQ1KN{background-color:") + (("#eaf8fc")  + ";}.GHAOYQ1CO .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1CO .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("left")  + ";}.GHAOYQ1CO .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getHeight() + "px") ) + (";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalTopRight()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1CO .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterLeft()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1CO .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1CO .GHAOYQ1HN{height:") + (((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomLeft()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1CO .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomCenter()).getTop() + "px  repeat-x")  + ";float:" + ("left")  + ";}.GHAOYQ1CO .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getWidth() + "px") ) + (";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperNormalBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1CO .GHAOYQ1KN{background-color:" + ("#fff")  + ";}.GHAOYQ1BO .GHAOYQ1GO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopLeft()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1BO .GHAOYQ1FO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopCenter()).getTop() + "px  repeat-x")  + ";float:" + ("left")  + ";}.GHAOYQ1BO .GHAOYQ1HO{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledTopRight()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1BO .GHAOYQ1JN .GHAOYQ1LN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterLeft()).getTop() + "px  repeat-y") ) + (";}.GHAOYQ1BO .GHAOYQ1JN .GHAOYQ1MN{width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledCenterRight()).getTop() + "px  repeat-y")  + ";}.GHAOYQ1BO .GHAOYQ1HN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomLeft()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1BO .GHAOYQ1GN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:") + (("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomCenter()).getTop() + "px  repeat-x")  + ";float:" + ("left")  + ";}.GHAOYQ1BO .GHAOYQ1IN{height:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getHeight() + "px")  + ";width:" + ((WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getURL() + "\") -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getLeft() + "px -" + (WrapperResources_gecko1_8_default_InlineClientBundleGenerator.this.textAreaWrapperDisabledBottomRight()).getTop() + "px  no-repeat")  + ";float:" + ("left")  + ";}.GHAOYQ1BO .GHAOYQ1KN{background-color:" + ("#d4d4d4")  + ";}.GHAOYQ1PN,.GHAOYQ1ON{border:" + ("none")  + ";background-color:" + ("#fff")  + ";}.GHAOYQ1AO{border:" + ("none") ) + (";background-color:" + ("#eaf8fc")  + ";}.GHAOYQ1NN{border:" + ("none")  + ";background-color:" + ("#d4d4d4")  + ";}"));
      }
      public java.lang.String contentPanelWrapper(){
        return "GHAOYQ1AN";
      }
      public java.lang.String contentPanelWrapperBottom(){
        return "GHAOYQ1BN";
      }
      public java.lang.String contentPanelWrapperContent(){
        return "GHAOYQ1CN";
      }
      public java.lang.String contentPanelWrapperTop(){
        return "GHAOYQ1DN";
      }
      public java.lang.String textAreaWrapper(){
        return "GHAOYQ1EN";
      }
      public java.lang.String textAreaWrapperBottom(){
        return "GHAOYQ1FN";
      }
      public java.lang.String textAreaWrapperBottomCenter(){
        return "GHAOYQ1GN";
      }
      public java.lang.String textAreaWrapperBottomLeft(){
        return "GHAOYQ1HN";
      }
      public java.lang.String textAreaWrapperBottomRight(){
        return "GHAOYQ1IN";
      }
      public java.lang.String textAreaWrapperCenter(){
        return "GHAOYQ1JN";
      }
      public java.lang.String textAreaWrapperCenterCenter(){
        return "GHAOYQ1KN";
      }
      public java.lang.String textAreaWrapperCenterLeft(){
        return "GHAOYQ1LN";
      }
      public java.lang.String textAreaWrapperCenterRight(){
        return "GHAOYQ1MN";
      }
      public java.lang.String textAreaWrapperContentDisabled(){
        return "GHAOYQ1NN";
      }
      public java.lang.String textAreaWrapperContentEdit(){
        return "GHAOYQ1ON";
      }
      public java.lang.String textAreaWrapperContentNormal(){
        return "GHAOYQ1PN";
      }
      public java.lang.String textAreaWrapperContentOver(){
        return "GHAOYQ1AO";
      }
      public java.lang.String textAreaWrapperDisabled(){
        return "GHAOYQ1BO";
      }
      public java.lang.String textAreaWrapperNormal(){
        return "GHAOYQ1CO";
      }
      public java.lang.String textAreaWrapperOver(){
        return "GHAOYQ1DO";
      }
      public java.lang.String textAreaWrapperTop(){
        return "GHAOYQ1EO";
      }
      public java.lang.String textAreaWrapperTopCenter(){
        return "GHAOYQ1FO";
      }
      public java.lang.String textAreaWrapperTopLeft(){
        return "GHAOYQ1GO";
      }
      public java.lang.String textAreaWrapperTopRight(){
        return "GHAOYQ1HO";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.common.gwt.client.ui.comp.wrapper.WrapperCss get() {
      return css;
    }
  }
  public consys.common.gwt.client.ui.comp.wrapper.WrapperCss css() {
    return cssInitializer.get();
  }
  private void panelWrapperBottomInitializer() {
    panelWrapperBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "panelWrapperBottom",
      externalImage,
      0, 0, 700, 9, false, false
    );
  }
  private static class panelWrapperBottomInitializer {
    static {
      _instance0.panelWrapperBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return panelWrapperBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource panelWrapperBottom() {
    return panelWrapperBottomInitializer.get();
  }
  private void panelWrapperContentInitializer() {
    panelWrapperContent = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "panelWrapperContent",
      externalImage0,
      0, 0, 700, 1, false, false
    );
  }
  private static class panelWrapperContentInitializer {
    static {
      _instance0.panelWrapperContentInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return panelWrapperContent;
    }
  }
  public com.google.gwt.resources.client.ImageResource panelWrapperContent() {
    return panelWrapperContentInitializer.get();
  }
  private void panelWrapperTopInitializer() {
    panelWrapperTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "panelWrapperTop",
      externalImage1,
      0, 0, 700, 7, false, false
    );
  }
  private static class panelWrapperTopInitializer {
    static {
      _instance0.panelWrapperTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return panelWrapperTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource panelWrapperTop() {
    return panelWrapperTopInitializer.get();
  }
  private void textAreaWrapperBottomCenterInitializer() {
    textAreaWrapperBottomCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperBottomCenter",
      externalImage2,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperBottomCenterInitializer {
    static {
      _instance0.textAreaWrapperBottomCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperBottomCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperBottomCenter() {
    return textAreaWrapperBottomCenterInitializer.get();
  }
  private void textAreaWrapperBottomLeftInitializer() {
    textAreaWrapperBottomLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperBottomLeft",
      externalImage3,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperBottomLeftInitializer {
    static {
      _instance0.textAreaWrapperBottomLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperBottomLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperBottomLeft() {
    return textAreaWrapperBottomLeftInitializer.get();
  }
  private void textAreaWrapperBottomRightInitializer() {
    textAreaWrapperBottomRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperBottomRight",
      externalImage4,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperBottomRightInitializer {
    static {
      _instance0.textAreaWrapperBottomRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperBottomRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperBottomRight() {
    return textAreaWrapperBottomRightInitializer.get();
  }
  private void textAreaWrapperCenterLeftInitializer() {
    textAreaWrapperCenterLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperCenterLeft",
      externalImage5,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperCenterLeftInitializer {
    static {
      _instance0.textAreaWrapperCenterLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperCenterLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperCenterLeft() {
    return textAreaWrapperCenterLeftInitializer.get();
  }
  private void textAreaWrapperCenterRightInitializer() {
    textAreaWrapperCenterRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperCenterRight",
      externalImage6,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperCenterRightInitializer {
    static {
      _instance0.textAreaWrapperCenterRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperCenterRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperCenterRight() {
    return textAreaWrapperCenterRightInitializer.get();
  }
  private void textAreaWrapperDisabledBottomCenterInitializer() {
    textAreaWrapperDisabledBottomCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledBottomCenter",
      externalImage7,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperDisabledBottomCenterInitializer {
    static {
      _instance0.textAreaWrapperDisabledBottomCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledBottomCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledBottomCenter() {
    return textAreaWrapperDisabledBottomCenterInitializer.get();
  }
  private void textAreaWrapperDisabledBottomLeftInitializer() {
    textAreaWrapperDisabledBottomLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledBottomLeft",
      externalImage8,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperDisabledBottomLeftInitializer {
    static {
      _instance0.textAreaWrapperDisabledBottomLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledBottomLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledBottomLeft() {
    return textAreaWrapperDisabledBottomLeftInitializer.get();
  }
  private void textAreaWrapperDisabledBottomRightInitializer() {
    textAreaWrapperDisabledBottomRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledBottomRight",
      externalImage9,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperDisabledBottomRightInitializer {
    static {
      _instance0.textAreaWrapperDisabledBottomRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledBottomRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledBottomRight() {
    return textAreaWrapperDisabledBottomRightInitializer.get();
  }
  private void textAreaWrapperDisabledCenterLeftInitializer() {
    textAreaWrapperDisabledCenterLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledCenterLeft",
      externalImage10,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperDisabledCenterLeftInitializer {
    static {
      _instance0.textAreaWrapperDisabledCenterLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledCenterLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledCenterLeft() {
    return textAreaWrapperDisabledCenterLeftInitializer.get();
  }
  private void textAreaWrapperDisabledCenterRightInitializer() {
    textAreaWrapperDisabledCenterRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledCenterRight",
      externalImage11,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperDisabledCenterRightInitializer {
    static {
      _instance0.textAreaWrapperDisabledCenterRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledCenterRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledCenterRight() {
    return textAreaWrapperDisabledCenterRightInitializer.get();
  }
  private void textAreaWrapperDisabledTopCenterInitializer() {
    textAreaWrapperDisabledTopCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledTopCenter",
      externalImage12,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperDisabledTopCenterInitializer {
    static {
      _instance0.textAreaWrapperDisabledTopCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledTopCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledTopCenter() {
    return textAreaWrapperDisabledTopCenterInitializer.get();
  }
  private void textAreaWrapperDisabledTopLeftInitializer() {
    textAreaWrapperDisabledTopLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledTopLeft",
      externalImage13,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperDisabledTopLeftInitializer {
    static {
      _instance0.textAreaWrapperDisabledTopLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledTopLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledTopLeft() {
    return textAreaWrapperDisabledTopLeftInitializer.get();
  }
  private void textAreaWrapperDisabledTopRightInitializer() {
    textAreaWrapperDisabledTopRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperDisabledTopRight",
      externalImage14,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperDisabledTopRightInitializer {
    static {
      _instance0.textAreaWrapperDisabledTopRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperDisabledTopRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledTopRight() {
    return textAreaWrapperDisabledTopRightInitializer.get();
  }
  private void textAreaWrapperNormalBottomCenterInitializer() {
    textAreaWrapperNormalBottomCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalBottomCenter",
      externalImage15,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperNormalBottomCenterInitializer {
    static {
      _instance0.textAreaWrapperNormalBottomCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalBottomCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalBottomCenter() {
    return textAreaWrapperNormalBottomCenterInitializer.get();
  }
  private void textAreaWrapperNormalBottomLeftInitializer() {
    textAreaWrapperNormalBottomLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalBottomLeft",
      externalImage16,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperNormalBottomLeftInitializer {
    static {
      _instance0.textAreaWrapperNormalBottomLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalBottomLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalBottomLeft() {
    return textAreaWrapperNormalBottomLeftInitializer.get();
  }
  private void textAreaWrapperNormalBottomRightInitializer() {
    textAreaWrapperNormalBottomRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalBottomRight",
      externalImage17,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperNormalBottomRightInitializer {
    static {
      _instance0.textAreaWrapperNormalBottomRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalBottomRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalBottomRight() {
    return textAreaWrapperNormalBottomRightInitializer.get();
  }
  private void textAreaWrapperNormalCenterLeftInitializer() {
    textAreaWrapperNormalCenterLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalCenterLeft",
      externalImage18,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperNormalCenterLeftInitializer {
    static {
      _instance0.textAreaWrapperNormalCenterLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalCenterLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalCenterLeft() {
    return textAreaWrapperNormalCenterLeftInitializer.get();
  }
  private void textAreaWrapperNormalCenterRightInitializer() {
    textAreaWrapperNormalCenterRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalCenterRight",
      externalImage19,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperNormalCenterRightInitializer {
    static {
      _instance0.textAreaWrapperNormalCenterRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalCenterRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalCenterRight() {
    return textAreaWrapperNormalCenterRightInitializer.get();
  }
  private void textAreaWrapperNormalTopCenterInitializer() {
    textAreaWrapperNormalTopCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalTopCenter",
      externalImage20,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperNormalTopCenterInitializer {
    static {
      _instance0.textAreaWrapperNormalTopCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalTopCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalTopCenter() {
    return textAreaWrapperNormalTopCenterInitializer.get();
  }
  private void textAreaWrapperNormalTopLeftInitializer() {
    textAreaWrapperNormalTopLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalTopLeft",
      externalImage21,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperNormalTopLeftInitializer {
    static {
      _instance0.textAreaWrapperNormalTopLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalTopLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalTopLeft() {
    return textAreaWrapperNormalTopLeftInitializer.get();
  }
  private void textAreaWrapperNormalTopRightInitializer() {
    textAreaWrapperNormalTopRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperNormalTopRight",
      externalImage22,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperNormalTopRightInitializer {
    static {
      _instance0.textAreaWrapperNormalTopRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperNormalTopRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperNormalTopRight() {
    return textAreaWrapperNormalTopRightInitializer.get();
  }
  private void textAreaWrapperOverBottomCenterInitializer() {
    textAreaWrapperOverBottomCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverBottomCenter",
      externalImage23,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperOverBottomCenterInitializer {
    static {
      _instance0.textAreaWrapperOverBottomCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverBottomCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverBottomCenter() {
    return textAreaWrapperOverBottomCenterInitializer.get();
  }
  private void textAreaWrapperOverBottomLeftInitializer() {
    textAreaWrapperOverBottomLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverBottomLeft",
      externalImage24,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperOverBottomLeftInitializer {
    static {
      _instance0.textAreaWrapperOverBottomLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverBottomLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverBottomLeft() {
    return textAreaWrapperOverBottomLeftInitializer.get();
  }
  private void textAreaWrapperOverBottomRightInitializer() {
    textAreaWrapperOverBottomRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverBottomRight",
      externalImage25,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperOverBottomRightInitializer {
    static {
      _instance0.textAreaWrapperOverBottomRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverBottomRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverBottomRight() {
    return textAreaWrapperOverBottomRightInitializer.get();
  }
  private void textAreaWrapperOverCenterLeftInitializer() {
    textAreaWrapperOverCenterLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverCenterLeft",
      externalImage26,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperOverCenterLeftInitializer {
    static {
      _instance0.textAreaWrapperOverCenterLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverCenterLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverCenterLeft() {
    return textAreaWrapperOverCenterLeftInitializer.get();
  }
  private void textAreaWrapperOverCenterRightInitializer() {
    textAreaWrapperOverCenterRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverCenterRight",
      externalImage27,
      0, 0, 8, 1, false, false
    );
  }
  private static class textAreaWrapperOverCenterRightInitializer {
    static {
      _instance0.textAreaWrapperOverCenterRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverCenterRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverCenterRight() {
    return textAreaWrapperOverCenterRightInitializer.get();
  }
  private void textAreaWrapperOverTopCenterInitializer() {
    textAreaWrapperOverTopCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverTopCenter",
      externalImage28,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperOverTopCenterInitializer {
    static {
      _instance0.textAreaWrapperOverTopCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverTopCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverTopCenter() {
    return textAreaWrapperOverTopCenterInitializer.get();
  }
  private void textAreaWrapperOverTopLeftInitializer() {
    textAreaWrapperOverTopLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverTopLeft",
      externalImage29,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperOverTopLeftInitializer {
    static {
      _instance0.textAreaWrapperOverTopLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverTopLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverTopLeft() {
    return textAreaWrapperOverTopLeftInitializer.get();
  }
  private void textAreaWrapperOverTopRightInitializer() {
    textAreaWrapperOverTopRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperOverTopRight",
      externalImage30,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperOverTopRightInitializer {
    static {
      _instance0.textAreaWrapperOverTopRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperOverTopRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperOverTopRight() {
    return textAreaWrapperOverTopRightInitializer.get();
  }
  private void textAreaWrapperTopCenterInitializer() {
    textAreaWrapperTopCenter = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperTopCenter",
      externalImage31,
      0, 0, 1, 8, false, false
    );
  }
  private static class textAreaWrapperTopCenterInitializer {
    static {
      _instance0.textAreaWrapperTopCenterInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperTopCenter;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperTopCenter() {
    return textAreaWrapperTopCenterInitializer.get();
  }
  private void textAreaWrapperTopLeftInitializer() {
    textAreaWrapperTopLeft = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperTopLeft",
      externalImage32,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperTopLeftInitializer {
    static {
      _instance0.textAreaWrapperTopLeftInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperTopLeft;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperTopLeft() {
    return textAreaWrapperTopLeftInitializer.get();
  }
  private void textAreaWrapperTopRightInitializer() {
    textAreaWrapperTopRight = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "textAreaWrapperTopRight",
      externalImage33,
      0, 0, 8, 8, false, false
    );
  }
  private static class textAreaWrapperTopRightInitializer {
    static {
      _instance0.textAreaWrapperTopRightInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return textAreaWrapperTopRight;
    }
  }
  public com.google.gwt.resources.client.ImageResource textAreaWrapperTopRight() {
    return textAreaWrapperTopRightInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.common.gwt.client.ui.comp.wrapper.WrapperCss css;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAAJCAYAAADQIv+sAAABMUlEQVR42u3bQUrDUBCH8VebVppSXCQgVcFFN0K9j7jxSN4im2xyha7FRQ6RVcgqkmQlOiP/QKEeIIHvwe8Cs/oY5oUQQmR2Zt913dcwDD8AAADAnFnXdt636ty/4N2a26ZpPhkQAAAA5q6u6w/vW3VuWJqNSYuieO37/pshAQAAYK68Z/M8f/G+VeeGK3NtbsxDWZbvDAoAAABz5T3rXau+XXvwLnTWEJvEPGZZ9lZV1alt24ahAQAAYOq8W71fvWO9Z9W1sTo3jFvelW4cEhXxwTyZo3kGAAAAJuqobj2oYxN17UqdG8Yt7xi9sda/qQ59/XfbndwDAAAAEzE26l7dmqpj47PYXYSzN0ZvpFuHjcp4BwAAAEzcVv26Vs9exO5/4buUCAAAAJi4sV0vQvcXEX6gxq0XujkAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAABCAYAAAA8cX3BAAAALElEQVR42u3WMREAIAwEsB9YUYB/jR2htcFliIckyWq7naq6AADwg/ntPPcBvo+Q523gsDsAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArwAAAAHCAYAAADqKJ7cAAAA30lEQVR42u3bP0rEQBTA4XWNyv4RG0+leCkv5QmCkDJnCJbZICRpFnTeMlOIe4AEvoGvS5pX/XjMbDb/z02yTW6zCgAAFq606zb37NVTQjd+uE92ySE5Jo8AALBQx9ytu9yx1bXwLbF7l+zbtn0ZhuFjHMeveZ5/AABgyaJbo1+jY6Nnc9f+id4Su4eu694NDQCAtYqezRvfEr2X6o21775pmrf00dmgAABYsXNd169501uV7e5D8nQ6nRoDAgBg7fq+/4y+zXd6L6/Z4oLv8zRN3wYEAMDaRddG30bn/gLYReJKdgtPgAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAJklEQVR42mP4DwQMCOLA1Tv/GWSXPDdlUJj/XAFIvBdgMJ75nxUAGa4WlmrB8UkAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAxUlEQVR42mNg2P+fRX3ua17Z5a+lZJa80N175e6X33///ocBBoZV/5mlZj7jUl70Qkx64Wu1DRfu7nvy9gOSgvr/TCqTbrMrzH8voLTwqVze7kf+e6/c+fTw9bv/P37/Bir4/5/ReOZ/VvFFL7illj0TkV/0QjFm60PX9Rfu7tl9+c5rsAKQNVqrrrCJrnrFA1IkNe+pLMg62UUvdBjAAEkRyCSQdRLznouCHI5QAFUEsg7kJpDDQSYyoACYQqDDQYpBQQAAP6WBcSAcIWYAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAxUlEQVR42mP4DwW///79v/fK3S8yS17oyi5/LaU+9zUvw/7/LAwwBU/efvi/4cLdfdILX6spL3ohJjXzGRfDqv/MDD9+//7/8PU7oO47n/J2P/JXWvhUTmH+ewGVSbfZGer/MzHsvnzn9foLd/fEbH3oKr/ohaLUsmci4otecBvP/M8KNJ6RQXbRCx2QsVLznsqCJEVXveLRWnWFDWw8WAHQQRLznouCjAXpRJEEAZAOkINAdoKNhUnCFIC9AhIEOQhFAgIA1nOBcd7c2QkAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAIElEQVR42mMwnvmfVWnVO37Fpc/kZRc9Mzlw9c5/ZAAAMB8WouPV1xUAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAIUlEQVR42mP4jwQOXL3zX3bRMxPFpc/klVa94zee+Z8VALmTFqJKw6xwAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAEklEQVR42mO4cuXKfwY0AgsAAKDdDe1+N+7fAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage8 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAM0lEQVR42mNgQANXrlz5AMT/YZgBi4LVeBVcvHhRHSjxFqcCELhw4YI0UHIFED9hGAQAAJkCMX46l0sFAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage9 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAK0lEQVR42mO4cuXKfyT8gQEdoClYjU/B24sXL6pjU/AEiFdcuHBBmmGQAgDrWzF+DXoCswAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage10 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAEUlEQVR42mNgQANXrlz5j4wBdXUN7VteETkAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage11 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAEUlEQVR42mO4cuXKf2TMgAYAVEQN7b9Cm/AAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage12 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAEklEQVR42mNgwAKuXLnyH40AAIpeDe3LD6VdAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage13 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAMUlEQVR42mNgGKTgwoUL0leuXFkBxE8wJC9evKgOlHgLxP9BGEMBUHA1TBKXgg/ICgAi0DF+WAgnsAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage14 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAALElEQVR42mNgGATgypUrT4B4xYULF6RxKfgPxW8vXryojk8BCK8mpOADujwAdSkxfhUH4+UAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage15 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAFUlEQVR42mP4DwQMCOLKlSv/GbAAANLAD3CmYVcNAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage16 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAQElEQVR42mNgQANXrlz58OfPn/8wwIBFweo3b97gVnDx4kV1oKK3r169+v/r1y9MBSBw4cIFaaCiFUD8hGEQAAB2/TT28pQhDwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage17 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAQElEQVR42mP4DwV//vz5f+XKlQ8M6ACm4M2bNyAFqzEU/Pr16/+rV69Akm8vXryojqEAKPEEiFdcuHBBmmGQAgAUZTT2uDmGvAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage18 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAEUlEQVR42mNgQANXrlz5jwwAgAoPcCoTaNIAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage19 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAE0lEQVR42mP4jwSuXLnynwENAAB9FQ9wDNQ5cQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage20 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAFUlEQVR42mNgwAKuXLnyn+E/ECAIAJZ2D3AToCO5AAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage21 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAPklEQVR42mNgGKTgwoUL0leuXFkBxE8wJC9evKgOlHj76tWr/79+/fqPoQAoufrNmzf/YQCbgg9//vyBKwAAkTo09h0jp2sAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage22 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAQElEQVR42mNgGATgypUrT4B4xYULF6SxKvj169f/V69e/Qcqenvx4kV1DAX/oeDNmzcgRatxKvjz5w9IwQd0eQAuojT2Wz7SIAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage23 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAF0lEQVR42mN49uXnfwYEcfrOp/8MWAAAxL0PDy6s6e0AAAAASUVORK5CYII=";
  private static final java.lang.String externalImage24 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAASUlEQVR42mNgQAOn73z68OrH3/9vfv0DYwYMBXc/rb739iceBfc+qZ++/entnTc//r/8/hdTAQgcu/lN+vSdjyvO3Pn0hGEQAACZvDVM1c2oSAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage25 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAATElEQVR42mN48+vffxB+9ePv/9N3Pn1gQAcwBffe/vx/+u6n1RgKXn7/+//Omx//T9/+9Pb0vU/qGArO3Pn05PSdjyuO3fwmzTBIAQA1JDVM6ZMPOwAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage26 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAFUlEQVR42mNgQAOn73z6/+zLTzgGAH3rDw+bNM3pAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage27 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAYAAADjAO9DAAAAFklEQVR42mN49uXnfxg+fefTfwY0AABxkg8PbFLkigAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage28 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAFklEQVR42mNgwAJO3/n0n+HZl59IBACUMA8PZi30yQAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage29 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAR0lEQVR42mNgGKTg2M1v0qfvfFxx5s6nJxiSp+99Uj99+9PbO29+/H/5/e9/TAV3P62+9/bn/ze//oExpoI7nz68+vEXrgAAxgU1TEVOwWQAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage30 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAATElEQVR42mNgGATgzJ1PT07f+bji2M1v0lgVvPz+9/+dNz/+n7796e3pe5/UMRS8+fXvPwjfe/vz/+m7n1bjVPDqx9//p+98+oAuDwBhbTVMXCJGRAAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage31 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAICAYAAAA4GpVBAAAAJklEQVR42mMwnvmflUFh/nsBIPFcgUF2yXNThgNX7/xn+A8ECAIAbIQWlsXtkHgAAAAASUVORK5CYII=";
  private static final java.lang.String externalImage32 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAxElEQVR42mNgQAb//zOCcf1/JoZV/5kZ9v9nQZUAChrP/M+qMuk2u9TMZ1yiq17xIBQAJbVWXWETX/SCW2H+ewGJec9FZZe/lkKRBOmQWvZMRGreU1npha/VZBe90AErABkL0gmSlF/0QjFm60PX9Rfu7tl9+c5rBpCDQHaCjFVa+FQub/cj/71X7nx6+Prd/x+/f/9nABkPcpDyohdiIGM3XLi778nbD/9hgAHkFfW5r3lBDpJZ8kJ375W7X37//QtXAACkiYFxvnDlzgAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage33 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAwUlEQVR42mNg2P+fhWHVf2aG+v9MDP//M4IxMhBd9YpHauYzLpVJt9mNZ/5nBStGVii7/LWUxLznogrz3wuIL3rBrbXqChtcEVjBohc60gtfq0nNeyorteyZCMhEFEW7L995vf7C3T0xWx+6yi96oQhSBDIJbB1IwY/fv/8/fP3u/94rdz7l7X7kr7TwqRzIOpCboA6HgCdvP/zfcOHuPpB1yoteiIEcDrUGAn7//Qs05e4XmSUvdEEOV5/7mhcUBAA7ZoFxF/ZvUgAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource panelWrapperBottom;
  private static com.google.gwt.resources.client.ImageResource panelWrapperContent;
  private static com.google.gwt.resources.client.ImageResource panelWrapperTop;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperBottomCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperBottomLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperBottomRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperCenterLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperCenterRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledBottomCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledBottomLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledBottomRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledCenterLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledCenterRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledTopCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledTopLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperDisabledTopRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalBottomCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalBottomLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalBottomRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalCenterLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalCenterRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalTopCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalTopLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperNormalTopRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverBottomCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverBottomLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverBottomRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverCenterLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverCenterRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverTopCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverTopLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperOverTopRight;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperTopCenter;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperTopLeft;
  private static com.google.gwt.resources.client.ImageResource textAreaWrapperTopRight;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
      panelWrapperBottom(), 
      panelWrapperContent(), 
      panelWrapperTop(), 
      textAreaWrapperBottomCenter(), 
      textAreaWrapperBottomLeft(), 
      textAreaWrapperBottomRight(), 
      textAreaWrapperCenterLeft(), 
      textAreaWrapperCenterRight(), 
      textAreaWrapperDisabledBottomCenter(), 
      textAreaWrapperDisabledBottomLeft(), 
      textAreaWrapperDisabledBottomRight(), 
      textAreaWrapperDisabledCenterLeft(), 
      textAreaWrapperDisabledCenterRight(), 
      textAreaWrapperDisabledTopCenter(), 
      textAreaWrapperDisabledTopLeft(), 
      textAreaWrapperDisabledTopRight(), 
      textAreaWrapperNormalBottomCenter(), 
      textAreaWrapperNormalBottomLeft(), 
      textAreaWrapperNormalBottomRight(), 
      textAreaWrapperNormalCenterLeft(), 
      textAreaWrapperNormalCenterRight(), 
      textAreaWrapperNormalTopCenter(), 
      textAreaWrapperNormalTopLeft(), 
      textAreaWrapperNormalTopRight(), 
      textAreaWrapperOverBottomCenter(), 
      textAreaWrapperOverBottomLeft(), 
      textAreaWrapperOverBottomRight(), 
      textAreaWrapperOverCenterLeft(), 
      textAreaWrapperOverCenterRight(), 
      textAreaWrapperOverTopCenter(), 
      textAreaWrapperOverTopLeft(), 
      textAreaWrapperOverTopRight(), 
      textAreaWrapperTopCenter(), 
      textAreaWrapperTopLeft(), 
      textAreaWrapperTopRight(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
        resourceMap.put("panelWrapperBottom", panelWrapperBottom());
        resourceMap.put("panelWrapperContent", panelWrapperContent());
        resourceMap.put("panelWrapperTop", panelWrapperTop());
        resourceMap.put("textAreaWrapperBottomCenter", textAreaWrapperBottomCenter());
        resourceMap.put("textAreaWrapperBottomLeft", textAreaWrapperBottomLeft());
        resourceMap.put("textAreaWrapperBottomRight", textAreaWrapperBottomRight());
        resourceMap.put("textAreaWrapperCenterLeft", textAreaWrapperCenterLeft());
        resourceMap.put("textAreaWrapperCenterRight", textAreaWrapperCenterRight());
        resourceMap.put("textAreaWrapperDisabledBottomCenter", textAreaWrapperDisabledBottomCenter());
        resourceMap.put("textAreaWrapperDisabledBottomLeft", textAreaWrapperDisabledBottomLeft());
        resourceMap.put("textAreaWrapperDisabledBottomRight", textAreaWrapperDisabledBottomRight());
        resourceMap.put("textAreaWrapperDisabledCenterLeft", textAreaWrapperDisabledCenterLeft());
        resourceMap.put("textAreaWrapperDisabledCenterRight", textAreaWrapperDisabledCenterRight());
        resourceMap.put("textAreaWrapperDisabledTopCenter", textAreaWrapperDisabledTopCenter());
        resourceMap.put("textAreaWrapperDisabledTopLeft", textAreaWrapperDisabledTopLeft());
        resourceMap.put("textAreaWrapperDisabledTopRight", textAreaWrapperDisabledTopRight());
        resourceMap.put("textAreaWrapperNormalBottomCenter", textAreaWrapperNormalBottomCenter());
        resourceMap.put("textAreaWrapperNormalBottomLeft", textAreaWrapperNormalBottomLeft());
        resourceMap.put("textAreaWrapperNormalBottomRight", textAreaWrapperNormalBottomRight());
        resourceMap.put("textAreaWrapperNormalCenterLeft", textAreaWrapperNormalCenterLeft());
        resourceMap.put("textAreaWrapperNormalCenterRight", textAreaWrapperNormalCenterRight());
        resourceMap.put("textAreaWrapperNormalTopCenter", textAreaWrapperNormalTopCenter());
        resourceMap.put("textAreaWrapperNormalTopLeft", textAreaWrapperNormalTopLeft());
        resourceMap.put("textAreaWrapperNormalTopRight", textAreaWrapperNormalTopRight());
        resourceMap.put("textAreaWrapperOverBottomCenter", textAreaWrapperOverBottomCenter());
        resourceMap.put("textAreaWrapperOverBottomLeft", textAreaWrapperOverBottomLeft());
        resourceMap.put("textAreaWrapperOverBottomRight", textAreaWrapperOverBottomRight());
        resourceMap.put("textAreaWrapperOverCenterLeft", textAreaWrapperOverCenterLeft());
        resourceMap.put("textAreaWrapperOverCenterRight", textAreaWrapperOverCenterRight());
        resourceMap.put("textAreaWrapperOverTopCenter", textAreaWrapperOverTopCenter());
        resourceMap.put("textAreaWrapperOverTopLeft", textAreaWrapperOverTopLeft());
        resourceMap.put("textAreaWrapperOverTopRight", textAreaWrapperOverTopRight());
        resourceMap.put("textAreaWrapperTopCenter", textAreaWrapperTopCenter());
        resourceMap.put("textAreaWrapperTopLeft", textAreaWrapperTopLeft());
        resourceMap.put("textAreaWrapperTopRight", textAreaWrapperTopRight());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::css()();
      case 'panelWrapperBottom': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::panelWrapperBottom()();
      case 'panelWrapperContent': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::panelWrapperContent()();
      case 'panelWrapperTop': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::panelWrapperTop()();
      case 'textAreaWrapperBottomCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperBottomCenter()();
      case 'textAreaWrapperBottomLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperBottomLeft()();
      case 'textAreaWrapperBottomRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperBottomRight()();
      case 'textAreaWrapperCenterLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperCenterLeft()();
      case 'textAreaWrapperCenterRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperCenterRight()();
      case 'textAreaWrapperDisabledBottomCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledBottomCenter()();
      case 'textAreaWrapperDisabledBottomLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledBottomLeft()();
      case 'textAreaWrapperDisabledBottomRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledBottomRight()();
      case 'textAreaWrapperDisabledCenterLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledCenterLeft()();
      case 'textAreaWrapperDisabledCenterRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledCenterRight()();
      case 'textAreaWrapperDisabledTopCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledTopCenter()();
      case 'textAreaWrapperDisabledTopLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledTopLeft()();
      case 'textAreaWrapperDisabledTopRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperDisabledTopRight()();
      case 'textAreaWrapperNormalBottomCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalBottomCenter()();
      case 'textAreaWrapperNormalBottomLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalBottomLeft()();
      case 'textAreaWrapperNormalBottomRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalBottomRight()();
      case 'textAreaWrapperNormalCenterLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalCenterLeft()();
      case 'textAreaWrapperNormalCenterRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalCenterRight()();
      case 'textAreaWrapperNormalTopCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalTopCenter()();
      case 'textAreaWrapperNormalTopLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalTopLeft()();
      case 'textAreaWrapperNormalTopRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperNormalTopRight()();
      case 'textAreaWrapperOverBottomCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverBottomCenter()();
      case 'textAreaWrapperOverBottomLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverBottomLeft()();
      case 'textAreaWrapperOverBottomRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverBottomRight()();
      case 'textAreaWrapperOverCenterLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverCenterLeft()();
      case 'textAreaWrapperOverCenterRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverCenterRight()();
      case 'textAreaWrapperOverTopCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverTopCenter()();
      case 'textAreaWrapperOverTopLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverTopLeft()();
      case 'textAreaWrapperOverTopRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperOverTopRight()();
      case 'textAreaWrapperTopCenter': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperTopCenter()();
      case 'textAreaWrapperTopLeft': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperTopLeft()();
      case 'textAreaWrapperTopRight': return this.@consys.common.gwt.client.ui.comp.wrapper.WrapperResources::textAreaWrapperTopRight()();
    }
    return null;
  }-*/;
}
