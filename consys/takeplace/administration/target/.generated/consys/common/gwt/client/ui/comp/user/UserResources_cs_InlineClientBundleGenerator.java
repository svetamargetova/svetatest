package consys.common.gwt.client.ui.comp.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class UserResources_cs_InlineClientBundleGenerator implements consys.common.gwt.client.ui.comp.user.UserResources {
  private static UserResources_cs_InlineClientBundleGenerator _instance0 = new UserResources_cs_InlineClientBundleGenerator();
  private void userCssInitializer() {
    userCss = new consys.common.gwt.client.ui.comp.user.UserCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "userCss";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCOOUJ4DHM{margin:" + ("5px")  + ";width:" + ("100%")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DGM{float:" + ("right")  + ";margin:" + ("5px"+ " " +"5px"+ " " +"5px"+ " " +"0")  + ";width:" + ("250px")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DFM{float:" + ("left")  + ";margin:" + ("5px"+ " " +"0"+ " " +"5px"+ " " +"5px")  + ";text-align:" + ("left")  + ";width:") + (("69px")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DFM div{font-size:" + ("11px")  + ";}.GCOOUJ4DLM{float:" + ("right")  + ";margin:" + ("5px")  + ";}.GCOOUJ4DKM{float:" + ("left")  + ";margin:" + ("5px"+ " " +"0"+ " " +"5px"+ " " +"5px")  + ";width:" + ("280px")  + ";}.GCOOUJ4DJM{float:" + ("right")  + ";margin:" + ("5px"+ " " +"5px"+ " " +"5px"+ " " +"0")  + ";width:" + ("260px") ) + (";overflow:" + ("hidden")  + ";}.GCOOUJ4DIM{height:" + ("100%")  + ";float:" + ("left")  + ";font-size:" + ("11px")  + ";margin:" + ("5px"+ " " +"0"+ " " +"5px"+ " " +"5px")  + ";text-align:" + ("left")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DIM div{position:" + ("relative")  + ";top:" + ("28%")  + ";font-size:" + ("11px")  + ";}.GCOOUJ4DMM .GCOOUJ4DNM{font-size:") + (("11px")  + ";color:" + ("#999")  + ";margin-bottom:" + ("5px")  + ";margin-right:" + ("10px")  + ";}.GCOOUJ4DMM .GCOOUJ4DPM{margin:" + ("10px"+ " " +"10px"+ " " +"5px"+ " " +"40px")  + ";display:" + ("inline")  + ";}.GCOOUJ4DEM{display:" + ("inline")  + ";cursor:" + ("pointer")  + ";font-weight:" + ("bold")  + ";color:" + ("#1a809a")  + ";text-decoration:" + ("underline") ) + (";}.GCOOUJ4DDM .GCOOUJ4DEM{margin-left:" + ("5px")  + ";}.GCOOUJ4DPL{width:" + ("305px")  + ";}.GCOOUJ4DAM{float:" + ("left")  + ";margin-top:" + ("5px")  + ";margin-left:" + ("5px")  + ";}.GCOOUJ4DBM{margin-top:" + ("5px")  + ";margin-bottom:" + ("5px")  + ";margin-right:" + ("10px")  + ";margin-left:" + ("10px")  + ";float:" + ("right")  + ";width:") + (("56px")  + ";height:" + ("56px")  + ";}.GCOOUJ4DCM{margin-top:" + ("5px")  + ";width:" + ("200px")  + ";float:" + ("right")  + ";}")) : ((".GCOOUJ4DHM{margin:" + ("5px")  + ";width:" + ("100%")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DGM{float:" + ("left")  + ";margin:" + ("5px"+ " " +"0"+ " " +"5px"+ " " +"5px")  + ";width:" + ("250px")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DFM{float:" + ("right")  + ";margin:" + ("5px"+ " " +"5px"+ " " +"5px"+ " " +"0")  + ";text-align:" + ("right")  + ";width:") + (("69px")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DFM div{font-size:" + ("11px")  + ";}.GCOOUJ4DLM{float:" + ("left")  + ";margin:" + ("5px")  + ";}.GCOOUJ4DKM{float:" + ("right")  + ";margin:" + ("5px"+ " " +"5px"+ " " +"5px"+ " " +"0")  + ";width:" + ("280px")  + ";}.GCOOUJ4DJM{float:" + ("left")  + ";margin:" + ("5px"+ " " +"0"+ " " +"5px"+ " " +"5px")  + ";width:" + ("260px") ) + (";overflow:" + ("hidden")  + ";}.GCOOUJ4DIM{height:" + ("100%")  + ";float:" + ("right")  + ";font-size:" + ("11px")  + ";margin:" + ("5px"+ " " +"5px"+ " " +"5px"+ " " +"0")  + ";text-align:" + ("right")  + ";overflow:" + ("hidden")  + ";}.GCOOUJ4DIM div{position:" + ("relative")  + ";top:" + ("28%")  + ";font-size:" + ("11px")  + ";}.GCOOUJ4DMM .GCOOUJ4DNM{font-size:") + (("11px")  + ";color:" + ("#999")  + ";margin-bottom:" + ("5px")  + ";margin-left:" + ("10px")  + ";}.GCOOUJ4DMM .GCOOUJ4DPM{margin:" + ("10px"+ " " +"40px"+ " " +"5px"+ " " +"10px")  + ";display:" + ("inline")  + ";}.GCOOUJ4DEM{display:" + ("inline")  + ";cursor:" + ("pointer")  + ";font-weight:" + ("bold")  + ";color:" + ("#1a809a")  + ";text-decoration:" + ("underline") ) + (";}.GCOOUJ4DDM .GCOOUJ4DEM{margin-right:" + ("5px")  + ";}.GCOOUJ4DPL{width:" + ("305px")  + ";}.GCOOUJ4DAM{float:" + ("right")  + ";margin-top:" + ("5px")  + ";margin-right:" + ("5px")  + ";}.GCOOUJ4DBM{margin-top:" + ("5px")  + ";margin-bottom:" + ("5px")  + ";margin-left:" + ("10px")  + ";margin-right:" + ("10px")  + ";float:" + ("left")  + ";width:") + (("56px")  + ";height:" + ("56px")  + ";}.GCOOUJ4DCM{margin-top:" + ("5px")  + ";width:" + ("200px")  + ";float:" + ("left")  + ";}"));
      }
      public java.lang.String userCard(){
        return "GCOOUJ4DOL";
      }
      public java.lang.String userCardContent(){
        return "GCOOUJ4DPL";
      }
      public java.lang.String userCardContentControlls(){
        return "GCOOUJ4DAM";
      }
      public java.lang.String userCardContentPortrait(){
        return "GCOOUJ4DBM";
      }
      public java.lang.String userCardContentUserInfo(){
        return "GCOOUJ4DCM";
      }
      public java.lang.String userInlineListPanel(){
        return "GCOOUJ4DDM";
      }
      public java.lang.String userLabel(){
        return "GCOOUJ4DEM";
      }
      public java.lang.String userLabelListEditItemAction(){
        return "GCOOUJ4DFM";
      }
      public java.lang.String userLabelListEditItemName(){
        return "GCOOUJ4DGM";
      }
      public java.lang.String userLabelListItem(){
        return "GCOOUJ4DHM";
      }
      public java.lang.String userListEditItemAction(){
        return "GCOOUJ4DIM";
      }
      public java.lang.String userListEditItemName(){
        return "GCOOUJ4DJM";
      }
      public java.lang.String userListItemName(){
        return "GCOOUJ4DKM";
      }
      public java.lang.String userListItemPortrait(){
        return "GCOOUJ4DLM";
      }
      public java.lang.String userSearchDialogBody(){
        return "GCOOUJ4DMM";
      }
      public java.lang.String userSearchDialogHelp(){
        return "GCOOUJ4DNM";
      }
      public java.lang.String userSearchDialogResultScrollPanel(){
        return "GCOOUJ4DOM";
      }
      public java.lang.String userSearchDialogTextBox(){
        return "GCOOUJ4DPM";
      }
    }
    ;
  }
  private static class userCssInitializer {
    static {
      _instance0.userCssInitializer();
    }
    static consys.common.gwt.client.ui.comp.user.UserCss get() {
      return userCss;
    }
  }
  public consys.common.gwt.client.ui.comp.user.UserCss userCss() {
    return userCssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.common.gwt.client.ui.comp.user.UserCss userCss;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      userCss(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("userCss", userCss());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'userCss': return this.@consys.common.gwt.client.ui.comp.user.UserResources::userCss()();
    }
    return null;
  }-*/;
}
