package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadUserThumbAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.common.gwt.client.rpc.action.LoadUserThumbAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.LoadUserThumbAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.client.rpc.action.LoadUserThumbAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.LoadUserThumbAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.LoadUserThumbAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.client.rpc.action.LoadUserThumbAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.LoadUserThumbAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.LoadUserThumbAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.LoadUserThumbAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.LoadUserThumbAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.LoadUserThumbAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.LoadUserThumbAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.LoadUserThumbAction)object);
  }
  
}
