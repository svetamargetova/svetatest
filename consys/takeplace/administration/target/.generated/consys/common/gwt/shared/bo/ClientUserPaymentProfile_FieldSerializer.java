package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUserPaymentProfile_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAccountNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::accountNo;
  }-*/;
  
  private static native void setAccountNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::accountNo = value;
  }-*/;
  
  private static native java.lang.String getBankNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::bankNo;
  }-*/;
  
  private static native void setBankNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::bankNo = value;
  }-*/;
  
  private static native java.lang.String getCity(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::city;
  }-*/;
  
  private static native void setCity(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::city = value;
  }-*/;
  
  private static native java.lang.Integer getCountry(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::country;
  }-*/;
  
  private static native void setCountry(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.Integer value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::country = value;
  }-*/;
  
  private static native java.lang.String getCountry2ch(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::country2ch;
  }-*/;
  
  private static native void setCountry2ch(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::country2ch = value;
  }-*/;
  
  private static native boolean getNoVatPayer(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::noVatPayer;
  }-*/;
  
  private static native void setNoVatPayer(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, boolean value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::noVatPayer = value;
  }-*/;
  
  private static native java.lang.String getRegNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::regNo;
  }-*/;
  
  private static native void setRegNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::regNo = value;
  }-*/;
  
  private static native java.lang.String getStreet(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::street;
  }-*/;
  
  private static native void setStreet(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::street = value;
  }-*/;
  
  private static native java.lang.String getVatNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::vatNo;
  }-*/;
  
  private static native void setVatNo(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::vatNo = value;
  }-*/;
  
  private static native java.lang.String getZip(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::zip;
  }-*/;
  
  private static native void setZip(consys.common.gwt.shared.bo.ClientUserPaymentProfile instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserPaymentProfile::zip = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) throws SerializationException {
    setAccountNo(instance, streamReader.readString());
    setBankNo(instance, streamReader.readString());
    setCity(instance, streamReader.readString());
    setCountry(instance, (java.lang.Integer) streamReader.readObject());
    setCountry2ch(instance, streamReader.readString());
    setNoVatPayer(instance, streamReader.readBoolean());
    setRegNo(instance, streamReader.readString());
    setStreet(instance, streamReader.readString());
    setVatNo(instance, streamReader.readString());
    setZip(instance, streamReader.readString());
    
    consys.common.gwt.shared.bo.ClientPaymentProfileThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.shared.bo.ClientUserPaymentProfile instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientUserPaymentProfile();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientUserPaymentProfile instance) throws SerializationException {
    streamWriter.writeString(getAccountNo(instance));
    streamWriter.writeString(getBankNo(instance));
    streamWriter.writeString(getCity(instance));
    streamWriter.writeObject(getCountry(instance));
    streamWriter.writeString(getCountry2ch(instance));
    streamWriter.writeBoolean(getNoVatPayer(instance));
    streamWriter.writeString(getRegNo(instance));
    streamWriter.writeString(getStreet(instance));
    streamWriter.writeString(getVatNo(instance));
    streamWriter.writeString(getZip(instance));
    
    consys.common.gwt.shared.bo.ClientPaymentProfileThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientUserPaymentProfile)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserPaymentProfile_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientUserPaymentProfile)object);
  }
  
}
