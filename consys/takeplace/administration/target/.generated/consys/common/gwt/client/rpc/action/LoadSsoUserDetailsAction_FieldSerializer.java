package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadSsoUserDetailsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getToken(consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction::token;
  }-*/;
  
  private static native void setToken(consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction::token = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction instance) throws SerializationException {
    setToken(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction instance) throws SerializationException {
    streamWriter.writeString(getToken(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction)object);
  }
  
}
