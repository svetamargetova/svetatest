package consys.common.gwt.shared.exception;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CouponInvalidException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.exception.CouponInvalidException instance) throws SerializationException {
    
    consys.common.gwt.shared.action.ActionException_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.common.gwt.shared.exception.CouponInvalidException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.exception.CouponInvalidException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.exception.CouponInvalidException instance) throws SerializationException {
    
    consys.common.gwt.shared.action.ActionException_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.exception.CouponInvalidException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.exception.CouponInvalidException_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.exception.CouponInvalidException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.exception.CouponInvalidException_FieldSerializer.serialize(writer, (consys.common.gwt.shared.exception.CouponInvalidException)object);
  }
  
}
