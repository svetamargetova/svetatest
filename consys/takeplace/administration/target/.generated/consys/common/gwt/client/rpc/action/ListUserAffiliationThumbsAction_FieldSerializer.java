package consys.common.gwt.client.rpc.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListUserAffiliationThumbsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEventUuid(consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction::eventUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getUuids(consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction instance) /*-{
    return instance.@consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction::uuids;
  }-*/;
  
  private static native void setUuids(consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction::uuids = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction instance) throws SerializationException {
    setEventUuid(instance, streamReader.readString());
    setUuids(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction instance) throws SerializationException {
    streamWriter.writeString(getEventUuid(instance));
    streamWriter.writeObject(getUuids(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction_FieldSerializer.deserialize(reader, (consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction_FieldSerializer.serialize(writer, (consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction)object);
  }
  
}
