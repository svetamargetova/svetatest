package consys.common.gwt.shared.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUserCommonInfo_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEmail(consys.common.gwt.shared.bo.ClientUserCommonInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::email;
  }-*/;
  
  private static native void setEmail(consys.common.gwt.shared.bo.ClientUserCommonInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::email = value;
  }-*/;
  
  private static native java.lang.String getFullName(consys.common.gwt.shared.bo.ClientUserCommonInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::fullName;
  }-*/;
  
  private static native void setFullName(consys.common.gwt.shared.bo.ClientUserCommonInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::fullName = value;
  }-*/;
  
  private static native java.lang.String getImageUuid(consys.common.gwt.shared.bo.ClientUserCommonInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::imageUuid;
  }-*/;
  
  private static native void setImageUuid(consys.common.gwt.shared.bo.ClientUserCommonInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::imageUuid = value;
  }-*/;
  
  private static native java.lang.String getOrganization(consys.common.gwt.shared.bo.ClientUserCommonInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::organization;
  }-*/;
  
  private static native void setOrganization(consys.common.gwt.shared.bo.ClientUserCommonInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::organization = value;
  }-*/;
  
  private static native java.lang.String getPosition(consys.common.gwt.shared.bo.ClientUserCommonInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::position;
  }-*/;
  
  private static native void setPosition(consys.common.gwt.shared.bo.ClientUserCommonInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::position = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.common.gwt.shared.bo.ClientUserCommonInfo instance) /*-{
    return instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::uuid;
  }-*/;
  
  private static native void setUuid(consys.common.gwt.shared.bo.ClientUserCommonInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.common.gwt.shared.bo.ClientUserCommonInfo::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.common.gwt.shared.bo.ClientUserCommonInfo instance) throws SerializationException {
    setEmail(instance, streamReader.readString());
    setFullName(instance, streamReader.readString());
    setImageUuid(instance, streamReader.readString());
    setOrganization(instance, streamReader.readString());
    setPosition(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.common.gwt.shared.bo.ClientUserCommonInfo instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.common.gwt.shared.bo.ClientUserCommonInfo();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.common.gwt.shared.bo.ClientUserCommonInfo instance) throws SerializationException {
    streamWriter.writeString(getEmail(instance));
    streamWriter.writeString(getFullName(instance));
    streamWriter.writeString(getImageUuid(instance));
    streamWriter.writeString(getOrganization(instance));
    streamWriter.writeString(getPosition(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.common.gwt.shared.bo.ClientUserCommonInfo_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserCommonInfo_FieldSerializer.deserialize(reader, (consys.common.gwt.shared.bo.ClientUserCommonInfo)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.common.gwt.shared.bo.ClientUserCommonInfo_FieldSerializer.serialize(writer, (consys.common.gwt.shared.bo.ClientUserCommonInfo)object);
  }
  
}
