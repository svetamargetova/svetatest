package consys.common.gwt.client.widget.message;

public class CommonWidgetsConstants_cs implements consys.common.gwt.client.widget.message.CommonWidgetsConstants {
  
  public java.lang.String const_password() {
    return "Heslo";
  }
  
  public java.lang.String const_weakLower() {
    return "slabé";
  }
  
  public java.lang.String loginContent_action_createYourAccount() {
    return "Vytvořte si účet";
  }
  
  public java.lang.String const_passwordIsWeak() {
    return "Heslo je krátké. Zkuste přidat více znaků.";
  }
  
  public java.lang.String registerUser_text_agreeing() {
    return "Kliknutím na 'REGISTRUJ' souhlasíte s <a href=\"http://static.takeplace.eu/GBC_Takeplace_en.pdf\" target=\"_blank\">Podmínkami používání</a>.";
  }
  
  public java.lang.String loginContent_error_accountLocked() {
    return "Váš účet není aktivován. Prosím aktivujte si nejprve účet.";
  }
  
  public java.lang.String loginContent_error_loginDisabled() {
    return "Přihlašování je momentálně nedostupné.";
  }
  
  public java.lang.String loginContent_error_ssoUnsupported() {
    return "Přihlašování se nezdařilo.";
  }
  
  public java.lang.String loginContent_error_authFail() {
    return "Nejste zřejmě registrován, nebo jste špatně zadal kontaktní e-mail nebo heslo.";
  }
  
  public java.lang.String loginContent_title() {
    return "Přihlášení";
  }
  
  public java.lang.String const_tooShortLower() {
    return "příliš krátké";
  }
  
  public java.lang.String loginContent_action_activateYourAccount() {
    return "Aktivujte si účet";
  }
  
  public java.lang.String const_confirmPassword() {
    return "Potvrdit heslo";
  }
  
  public java.lang.String loginContent_action_sendMeHint() {
    return "Pošli mi nápovědu.";
  }
  
  public java.lang.String const_rememberMe() {
    return "Zapamatovat";
  }
  
  public java.lang.String loginContent_button_logIn() {
    return "PŘIHLÁŠENÍ";
  }
  
  public java.lang.String const_middleName() {
    return "Prostřední jméno";
  }
  
  public java.lang.String const_firstName() {
    return "Jméno";
  }
  
  public java.lang.String const_affiliation() {
    return "Organizace";
  }
  
  public java.lang.String loginContent_text_lostPassword() {
    return "Ztracené heslo?";
  }
  
  public java.lang.String const_lastName() {
    return "Příjmení";
  }
  
  public java.lang.String const_passwordContainsSpace() {
    return "Heslo nemůže obsahovat znak mezery.";
  }
  
  public java.lang.String const_okLower() {
    return "ok";
  }
  
  public java.lang.String loginContent_text_notActivated() {
    return "Nemáte účet aktivní?";
  }
  
  public java.lang.String registerContent_error_alreadyRegistered() {
    return "Tento kontaktní e-mail se již používá. Možná jste zapomněli heslo?";
  }
  
  public java.lang.String const_passwordsNotMatch() {
    return "Hesla nejsou totožná.";
  }
  
  public java.lang.String loginContent_text_notRegistered() {
    return "Nejste registrován?";
  }
  
  public java.lang.String const_showPassword() {
    return "Zobrazit heslo";
  }
  
  public java.lang.String const_passwordStrength() {
    return "Délka hesla";
  }
}
