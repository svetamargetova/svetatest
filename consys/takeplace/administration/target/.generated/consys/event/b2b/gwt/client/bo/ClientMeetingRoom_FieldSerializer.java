package consys.event.b2b.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientMeetingRoom_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getTablesCount(consys.event.b2b.gwt.client.bo.ClientMeetingRoom instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientMeetingRoom::tablesCount;
  }-*/;
  
  private static native void setTablesCount(consys.event.b2b.gwt.client.bo.ClientMeetingRoom instance, int value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientMeetingRoom::tablesCount = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.b2b.gwt.client.bo.ClientMeetingRoom instance) throws SerializationException {
    setTablesCount(instance, streamReader.readInt());
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.b2b.gwt.client.bo.ClientMeetingRoom instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.b2b.gwt.client.bo.ClientMeetingRoom();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.b2b.gwt.client.bo.ClientMeetingRoom instance) throws SerializationException {
    streamWriter.writeInt(getTablesCount(instance));
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.b2b.gwt.client.bo.ClientMeetingRoom_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientMeetingRoom_FieldSerializer.deserialize(reader, (consys.event.b2b.gwt.client.bo.ClientMeetingRoom)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientMeetingRoom_FieldSerializer.serialize(writer, (consys.event.b2b.gwt.client.bo.ClientMeetingRoom)object);
  }
  
}
