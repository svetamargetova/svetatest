package consys.event.b2b.gwt.client.message;

public class B2BMessages_cs implements consys.event.b2b.gwt.client.message.B2BMessages {
  
  public java.lang.String b2bModule_text_navigationModelAnonymousRegisterInfo(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("K vstupu na B2B Brokerage <b>").append(arg0).append("</b> si musíte nejdřív zakoupit vstupenku.").toString();
  }
}
