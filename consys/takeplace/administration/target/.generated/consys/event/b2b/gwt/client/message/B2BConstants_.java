package consys.event.b2b.gwt.client.message;

public class B2BConstants_ implements consys.event.b2b.gwt.client.message.B2BConstants {
  
  public java.lang.String b2bModule_text_navigationModelAgentProfile() {
    return "Agent profile";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentFeedback() {
    return "Feedback";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentParticipants() {
    return "Participants";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorParticipants() {
    return "Participants";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentProfileVisitors() {
    return "Visitors";
  }
  
  public java.lang.String b2bModule_text_moduleName() {
    return "B2B";
  }
  
  public java.lang.String b2bModule_text_moduleRightAgentDescription() {
    return "User has active profile in B2B module.";
  }
  
  public java.lang.String b2bModule_text_moduleRightAdministratorDescription() {
    return "B2B administrator can manage whole B2B module.";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorSchedule() {
    return "Schedule";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentOrganizationProfile() {
    return "Organization profile";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentDashboard() {
    return "Dashboard";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorFeedback() {
    return "Feedback";
  }
  
  public java.lang.String b2bModule_text_navigationModelAnonymousRegister() {
    return "Registration";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentMeetings() {
    return "Meetings";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorStatistics() {
    return "Statistics";
  }
  
  public java.lang.String b2bModule_text_moduleRightAgent() {
    return "B2B Agent";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentSchedule() {
    return "Schedule";
  }
  
  public java.lang.String b2bModule_text_navigationModelAnonymousDasboard() {
    return "Dashboard";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentStatistics() {
    return "Statistics";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentCooperationProfile() {
    return "Cooperation profiles";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorMeetings() {
    return "Meetings";
  }
  
  public java.lang.String aboutEvent_text_name() {
    return "About event";
  }
  
  public java.lang.String b2bModule_text_moduleRightAdministrator() {
    return "B2B Administrator";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorDashboard() {
    return "Dashboard";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorManageEvent() {
    return "Settings";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorB2B() {
    return "B2B Administration";
  }
  
  public java.lang.String b2bModule_text_navigationModelB2B() {
    return "B2B Brokerage";
  }
}
