package consys.event.b2b.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientImportantDates_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getMeetFrom(consys.event.b2b.gwt.client.bo.ClientImportantDates instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::meetFrom;
  }-*/;
  
  private static native void setMeetFrom(consys.event.b2b.gwt.client.bo.ClientImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::meetFrom = value;
  }-*/;
  
  private static native java.util.Date getMeetTo(consys.event.b2b.gwt.client.bo.ClientImportantDates instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::meetTo;
  }-*/;
  
  private static native void setMeetTo(consys.event.b2b.gwt.client.bo.ClientImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::meetTo = value;
  }-*/;
  
  private static native java.util.Date getRegFrom(consys.event.b2b.gwt.client.bo.ClientImportantDates instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::regFrom;
  }-*/;
  
  private static native void setRegFrom(consys.event.b2b.gwt.client.bo.ClientImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::regFrom = value;
  }-*/;
  
  private static native java.util.Date getRegTo(consys.event.b2b.gwt.client.bo.ClientImportantDates instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::regTo;
  }-*/;
  
  private static native void setRegTo(consys.event.b2b.gwt.client.bo.ClientImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::regTo = value;
  }-*/;
  
  private static native java.util.Date getSelectFrom(consys.event.b2b.gwt.client.bo.ClientImportantDates instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::selectFrom;
  }-*/;
  
  private static native void setSelectFrom(consys.event.b2b.gwt.client.bo.ClientImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::selectFrom = value;
  }-*/;
  
  private static native java.util.Date getSelectTo(consys.event.b2b.gwt.client.bo.ClientImportantDates instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::selectTo;
  }-*/;
  
  private static native void setSelectTo(consys.event.b2b.gwt.client.bo.ClientImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientImportantDates::selectTo = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.b2b.gwt.client.bo.ClientImportantDates instance) throws SerializationException {
    setMeetFrom(instance, (java.util.Date) streamReader.readObject());
    setMeetTo(instance, (java.util.Date) streamReader.readObject());
    setRegFrom(instance, (java.util.Date) streamReader.readObject());
    setRegTo(instance, (java.util.Date) streamReader.readObject());
    setSelectFrom(instance, (java.util.Date) streamReader.readObject());
    setSelectTo(instance, (java.util.Date) streamReader.readObject());
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.b2b.gwt.client.bo.ClientImportantDates instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.b2b.gwt.client.bo.ClientImportantDates();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.b2b.gwt.client.bo.ClientImportantDates instance) throws SerializationException {
    streamWriter.writeObject(getMeetFrom(instance));
    streamWriter.writeObject(getMeetTo(instance));
    streamWriter.writeObject(getRegFrom(instance));
    streamWriter.writeObject(getRegTo(instance));
    streamWriter.writeObject(getSelectFrom(instance));
    streamWriter.writeObject(getSelectTo(instance));
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.b2b.gwt.client.bo.ClientImportantDates_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientImportantDates_FieldSerializer.deserialize(reader, (consys.event.b2b.gwt.client.bo.ClientImportantDates)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientImportantDates_FieldSerializer.serialize(writer, (consys.event.b2b.gwt.client.bo.ClientImportantDates)object);
  }
  
}
