package consys.event.b2b.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRoomsAndTablesAndTimes_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getDoubleTables(consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes::doubleTables;
  }-*/;
  
  private static native void setDoubleTables(consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes instance, int value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes::doubleTables = value;
  }-*/;
  
  private static native int getQuatreTables(consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes::quatreTables;
  }-*/;
  
  private static native void setQuatreTables(consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes instance, int value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes::quatreTables = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes instance) throws SerializationException {
    setDoubleTables(instance, streamReader.readInt());
    setQuatreTables(instance, streamReader.readInt());
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes instance) throws SerializationException {
    streamWriter.writeInt(getDoubleTables(instance));
    streamWriter.writeInt(getQuatreTables(instance));
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes_FieldSerializer.deserialize(reader, (consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes_FieldSerializer.serialize(writer, (consys.event.b2b.gwt.client.bo.ClientRoomsAndTablesAndTimes)object);
  }
  
}
