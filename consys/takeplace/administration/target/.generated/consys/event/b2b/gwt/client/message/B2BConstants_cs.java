package consys.event.b2b.gwt.client.message;

public class B2BConstants_cs implements consys.event.b2b.gwt.client.message.B2BConstants {
  
  public java.lang.String b2bModule_text_navigationModelAgentProfile() {
    return "Profil agenta";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentFeedback() {
    return "Zpětná vazba";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentParticipants() {
    return "Účastnící";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorParticipants() {
    return "Účastnící";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentProfileVisitors() {
    return "Návštevníci profilu";
  }
  
  public java.lang.String b2bModule_text_moduleName() {
    return "B2B";
  }
  
  public java.lang.String b2bModule_text_moduleRightAgentDescription() {
    return "User has active profile in B2B module.";
  }
  
  public java.lang.String b2bModule_text_moduleRightAdministratorDescription() {
    return "B2B administrator can manage whole B2B module.";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorSchedule() {
    return "Rozvrh";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentOrganizationProfile() {
    return "Organizační profil";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentDashboard() {
    return "Přehled";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorFeedback() {
    return "Zpětná vazba";
  }
  
  public java.lang.String b2bModule_text_navigationModelAnonymousRegister() {
    return "Registrace";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentMeetings() {
    return "Schůzky";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorStatistics() {
    return "Statistiky";
  }
  
  public java.lang.String b2bModule_text_moduleRightAgent() {
    return "B2B Agent";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentSchedule() {
    return "Rozvrh";
  }
  
  public java.lang.String b2bModule_text_navigationModelAnonymousDasboard() {
    return "Dashboard";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentStatistics() {
    return "Statistiky";
  }
  
  public java.lang.String b2bModule_text_navigationModelAgentCooperationProfile() {
    return "Kooperační profily";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorMeetings() {
    return "Schůzky";
  }
  
  public java.lang.String aboutEvent_text_name() {
    return "About event";
  }
  
  public java.lang.String b2bModule_text_moduleRightAdministrator() {
    return "B2B Administrator";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorDashboard() {
    return "Organizátorský přehled";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorManageEvent() {
    return "Settings";
  }
  
  public java.lang.String b2bModule_text_navigationModelOrganizatorB2B() {
    return "Správa B2B";
  }
  
  public java.lang.String b2bModule_text_navigationModelB2B() {
    return "B2B Brokerage";
  }
}
