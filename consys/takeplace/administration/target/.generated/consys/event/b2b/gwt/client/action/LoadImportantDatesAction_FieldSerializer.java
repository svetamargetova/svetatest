package consys.event.b2b.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadImportantDatesAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.b2b.gwt.client.action.LoadImportantDatesAction instance) throws SerializationException {
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.b2b.gwt.client.action.LoadImportantDatesAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.b2b.gwt.client.action.LoadImportantDatesAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.b2b.gwt.client.action.LoadImportantDatesAction instance) throws SerializationException {
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.b2b.gwt.client.action.LoadImportantDatesAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.action.LoadImportantDatesAction_FieldSerializer.deserialize(reader, (consys.event.b2b.gwt.client.action.LoadImportantDatesAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.action.LoadImportantDatesAction_FieldSerializer.serialize(writer, (consys.event.b2b.gwt.client.action.LoadImportantDatesAction)object);
  }
  
}
