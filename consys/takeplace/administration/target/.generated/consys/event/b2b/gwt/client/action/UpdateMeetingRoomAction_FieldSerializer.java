package consys.event.b2b.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateMeetingRoomAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.b2b.gwt.client.bo.ClientMeetingRoom getCmr(consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction instance) /*-{
    return instance.@consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction::cmr;
  }-*/;
  
  private static native void setCmr(consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction instance, consys.event.b2b.gwt.client.bo.ClientMeetingRoom value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction::cmr = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction instance) throws SerializationException {
    setCmr(instance, (consys.event.b2b.gwt.client.bo.ClientMeetingRoom) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction instance) throws SerializationException {
    streamWriter.writeObject(getCmr(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction_FieldSerializer.deserialize(reader, (consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction_FieldSerializer.serialize(writer, (consys.event.b2b.gwt.client.action.UpdateMeetingRoomAction)object);
  }
  
}
