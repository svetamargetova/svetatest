package consys.event.b2b.gwt.client.comp;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class B2BResources_default_InlineClientBundleGenerator implements consys.event.b2b.gwt.client.comp.B2BResources {
  private static B2BResources_default_InlineClientBundleGenerator _instance0 = new B2BResources_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new consys.event.b2b.gwt.client.comp.B2BCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return ("iframe{border:" + ("none")  + ";}.GCOOUJ4DH0{overflow:" + ("hidden")  + ";}");
      }
      public java.lang.String hideScrollbars(){
        return "GCOOUJ4DH0";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.event.b2b.gwt.client.comp.B2BCss get() {
      return css;
    }
  }
  public consys.event.b2b.gwt.client.comp.B2BCss css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.event.b2b.gwt.client.comp.B2BCss css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@consys.event.b2b.gwt.client.comp.B2BResources::css()();
    }
    return null;
  }-*/;
}
