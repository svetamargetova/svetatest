package consys.event.b2b.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class AbstractClientBO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getId(consys.event.b2b.gwt.client.bo.AbstractClientBO instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.AbstractClientBO::id;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setId(consys.event.b2b.gwt.client.bo.AbstractClientBO instance, long value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.AbstractClientBO::id = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.b2b.gwt.client.bo.AbstractClientBO instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.AbstractClientBO::name;
  }-*/;
  
  private static native void setName(consys.event.b2b.gwt.client.bo.AbstractClientBO instance, java.lang.String value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.AbstractClientBO::name = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.b2b.gwt.client.bo.AbstractClientBO instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.AbstractClientBO::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.b2b.gwt.client.bo.AbstractClientBO instance, java.lang.String value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.AbstractClientBO::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.b2b.gwt.client.bo.AbstractClientBO instance) throws SerializationException {
    setId(instance, streamReader.readLong());
    setName(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.b2b.gwt.client.bo.AbstractClientBO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.b2b.gwt.client.bo.AbstractClientBO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.b2b.gwt.client.bo.AbstractClientBO instance) throws SerializationException {
    streamWriter.writeLong(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.deserialize(reader, (consys.event.b2b.gwt.client.bo.AbstractClientBO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.serialize(writer, (consys.event.b2b.gwt.client.bo.AbstractClientBO)object);
  }
  
}
