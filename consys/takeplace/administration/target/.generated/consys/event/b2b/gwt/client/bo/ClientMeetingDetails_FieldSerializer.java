package consys.event.b2b.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientMeetingDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.b2b.gwt.client.bo.ClientMeetingDetails instance) /*-{
    return instance.@consys.event.b2b.gwt.client.bo.ClientMeetingDetails::description;
  }-*/;
  
  private static native void setDescription(consys.event.b2b.gwt.client.bo.ClientMeetingDetails instance, java.lang.String value) 
  /*-{
    instance.@consys.event.b2b.gwt.client.bo.ClientMeetingDetails::description = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.b2b.gwt.client.bo.ClientMeetingDetails instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.b2b.gwt.client.bo.ClientMeetingDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.b2b.gwt.client.bo.ClientMeetingDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.b2b.gwt.client.bo.ClientMeetingDetails instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    
    consys.event.b2b.gwt.client.bo.AbstractClientBO_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.b2b.gwt.client.bo.ClientMeetingDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientMeetingDetails_FieldSerializer.deserialize(reader, (consys.event.b2b.gwt.client.bo.ClientMeetingDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.b2b.gwt.client.bo.ClientMeetingDetails_FieldSerializer.serialize(writer, (consys.event.b2b.gwt.client.bo.ClientMeetingDetails)object);
  }
  
}
