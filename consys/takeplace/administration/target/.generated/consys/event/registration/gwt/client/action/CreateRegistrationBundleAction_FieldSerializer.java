package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateRegistrationBundleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getBundles(consys.event.registration.gwt.client.action.CreateRegistrationBundleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateRegistrationBundleAction::bundles;
  }-*/;
  
  private static native void setBundles(consys.event.registration.gwt.client.action.CreateRegistrationBundleAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateRegistrationBundleAction::bundles = value;
  }-*/;
  
  private static native boolean getSubbundle(consys.event.registration.gwt.client.action.CreateRegistrationBundleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateRegistrationBundleAction::subbundle;
  }-*/;
  
  private static native void setSubbundle(consys.event.registration.gwt.client.action.CreateRegistrationBundleAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateRegistrationBundleAction::subbundle = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.CreateRegistrationBundleAction instance) throws SerializationException {
    setBundles(instance, (java.util.ArrayList) streamReader.readObject());
    setSubbundle(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.CreateRegistrationBundleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.CreateRegistrationBundleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.CreateRegistrationBundleAction instance) throws SerializationException {
    streamWriter.writeObject(getBundles(instance));
    streamWriter.writeBoolean(getSubbundle(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.CreateRegistrationBundleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateRegistrationBundleAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.CreateRegistrationBundleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateRegistrationBundleAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.CreateRegistrationBundleAction)object);
  }
  
}
