package consys.event.registration.gwt.client.message;

public class EventRegistrationMessages_cs implements consys.event.registration.gwt.client.message.EventRegistrationMessages {
  
  public java.lang.String registrationCycleList_cycle_exception_endSameAsStart(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("The beginning of registration cycle (").append(arg0).append(") can not be same as the end of it (").append(arg1).append("). If you want to have duration of registration cycle 1 minute, set the end of cycle to ").append(arg0).append(" +1 minute.").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_registrationDate(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Datum registrace:</b> ").append(arg0).toString();
  }
  
  public java.lang.String registrationDate(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Datum registrace ").append(arg0).toString();
  }
  
  public java.lang.String registrationFormItem_text_registrationExpiresDays(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule_cs().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("Registrace bude dnes ukončena.</b>").toString();
        break;
      default: // non-exact matches
        switch (arg0_form) {
          case 2:  // few
            returnVal = new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" dny.</b>").toString();
            break;
          case 1:  // one
            returnVal = new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(arg0).append(" den.</b>").toString();
            break;
        }
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(arg0).append(" dní.</b>").toString();
  }
  
  public java.lang.String registrationForm_text_before(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("před ").append(arg0).append(" ").append(arg1).toString();
  }
  
  public java.lang.String registrationFormRegistered_text_successfulyRegisteredInfo(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("Platba za Vaši registraci nebyla stále potvrzena. Pokud nebude platba obdržena do <span class=\"").append(arg0).append("\">").append(arg1).append("</span> bude registrace zrušena.").toString();
  }
  
  public java.lang.String outerRegistrationForm_text_alreadyTakeplaceMember(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Jste již uživatelem Takeplace? <a href=\"").append(arg0).append("\">Klikněte zde pro přihlášení.</a>").toString();
  }
  
  public java.lang.String discountListItem_text_showAll(int arg0) {
    return new java.lang.StringBuffer().append("Zobrazit všechny (").append(arg0).append(")").toString();
  }
  
  public java.lang.String outerRegisterPanel_text1(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("You are registering at <strong>").append(arg0).append("</strong> with Takeplace - your event cloud in online paradise. Takeplace makes your events run! ").toString();
  }
  
  public java.lang.String registrationCycleList_cycle_exception_endBeforeStart(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("Konec registračního cyklu (").append(arg1).append(") nemůže předcházet jeho začátku (").append(arg0).append(").").toString();
  }
  
  public java.lang.String registrationFormItem_exception_noRecordsForAction(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Slevový kupón ").append(arg0).append(" neexistuje.").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_statusPaid(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Stav: <span class=\"").append(arg0).append("\">potvrzena</span></b>").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_statusApprove(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Stav: <span class=\"").append(arg0).append("\">čeká na potvrzení</span></b>").toString();
  }
  
  public java.lang.String registrationFormItem_text_registrationExpiresMinutes(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule_cs().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("Registrace bude ukončena během minuty.</b>").toString();
        break;
      default: // non-exact matches
        switch (arg0_form) {
          case 2:  // few
            returnVal = new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" minuty.</b>").toString();
            break;
          case 1:  // one
            returnVal = new java.lang.StringBuffer().append("Registrace bude ukončena za <b>minutu.</b>").toString();
            break;
        }
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(arg0).append(" minut.</b>").toString();
  }
  
  public java.lang.String registrationFormItem_text_lastFreeCapacity(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule_cs().select(arg0);
    switch (arg0_form) {
      case 2:  // few
        returnVal = new java.lang.StringBuffer().append("Zbývají <b>").append(arg0).append(" volné</b> registrace.").toString();
        break;
      case 1:  // one
        returnVal = new java.lang.StringBuffer().append("Zbývá <b>").append(arg0).append(" volná</b> registrace.").toString();
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("Zbývá <b>").append(arg0).append(" volných</b> registrací.").toString();
  }
  
  public java.lang.String registrationFormItem_text_registrationExpiresHours(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule_cs().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("Registrace bude během hodiny ukončena.</b>").toString();
        break;
      default: // non-exact matches
        switch (arg0_form) {
          case 2:  // few
            returnVal = new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" hodiny.</b>").toString();
            break;
          case 1:  // one
            returnVal = new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(arg0).append(" hodinu.</b>").toString();
            break;
        }
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("Registrace bude ukončena za <b>").append(arg0).append(" hodin.</b>").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_discountCode(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Slevový kód</b> (").append(arg0).append(")").toString();
  }
  
  public java.lang.String registrationFormItem_text_limitedCapacity(int arg0) {
    return new java.lang.StringBuffer().append("Kapacita je omezena na <b>").append(arg0).append("</b>.").toString();
  }
}
