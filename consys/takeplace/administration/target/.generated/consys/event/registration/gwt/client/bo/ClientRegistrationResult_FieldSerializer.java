package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistrationResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getBundles(consys.event.registration.gwt.client.bo.ClientRegistrationResult instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationResult::bundles;
  }-*/;
  
  private static native void setBundles(consys.event.registration.gwt.client.bo.ClientRegistrationResult instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationResult::bundles = value;
  }-*/;
  
  private static native consys.event.registration.gwt.client.bo.ClientRegistration getRegistration(consys.event.registration.gwt.client.bo.ClientRegistrationResult instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationResult::registration;
  }-*/;
  
  private static native void setRegistration(consys.event.registration.gwt.client.bo.ClientRegistrationResult instance, consys.event.registration.gwt.client.bo.ClientRegistration value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationResult::registration = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientRegistrationResult instance) throws SerializationException {
    setBundles(instance, (java.util.ArrayList) streamReader.readObject());
    setRegistration(instance, (consys.event.registration.gwt.client.bo.ClientRegistration) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientRegistrationResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientRegistrationResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientRegistrationResult instance) throws SerializationException {
    streamWriter.writeObject(getBundles(instance));
    streamWriter.writeObject(getRegistration(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientRegistrationResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationResult_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientRegistrationResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationResult_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientRegistrationResult)object);
  }
  
}
