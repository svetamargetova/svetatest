package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCreateBundle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.registration.gwt.client.bo.ClientCreateBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCreateBundle::description;
  }-*/;
  
  private static native void setDescription(consys.event.registration.gwt.client.bo.ClientCreateBundle instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCreateBundle::description = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.registration.gwt.client.bo.ClientCreateBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCreateBundle::title;
  }-*/;
  
  private static native void setTitle(consys.event.registration.gwt.client.bo.ClientCreateBundle instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCreateBundle::title = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientCreateBundle instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientCreateBundle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientCreateBundle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientCreateBundle instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getTitle(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientCreateBundle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCreateBundle_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientCreateBundle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCreateBundle_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientCreateBundle)object);
  }
  
}
