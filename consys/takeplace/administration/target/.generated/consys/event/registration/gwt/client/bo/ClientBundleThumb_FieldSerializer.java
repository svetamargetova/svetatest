package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientBundleThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getSubbundle(consys.event.registration.gwt.client.bo.ClientBundleThumb instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundleThumb::subbundle;
  }-*/;
  
  private static native void setSubbundle(consys.event.registration.gwt.client.bo.ClientBundleThumb instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundleThumb::subbundle = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.registration.gwt.client.bo.ClientBundleThumb instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundleThumb::title;
  }-*/;
  
  private static native void setTitle(consys.event.registration.gwt.client.bo.ClientBundleThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundleThumb::title = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.bo.ClientBundleThumb instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundleThumb::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.bo.ClientBundleThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundleThumb::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientBundleThumb instance) throws SerializationException {
    setSubbundle(instance, streamReader.readBoolean());
    setTitle(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientBundleThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientBundleThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientBundleThumb instance) throws SerializationException {
    streamWriter.writeBoolean(getSubbundle(instance));
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientBundleThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundleThumb_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientBundleThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundleThumb_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientBundleThumb)object);
  }
  
}
