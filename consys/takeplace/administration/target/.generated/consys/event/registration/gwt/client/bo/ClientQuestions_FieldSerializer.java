package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientQuestions_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getQuestions(consys.event.registration.gwt.client.bo.ClientQuestions instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientQuestions::questions;
  }-*/;
  
  private static native void setQuestions(consys.event.registration.gwt.client.bo.ClientQuestions instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientQuestions::questions = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientQuestions instance) throws SerializationException {
    setQuestions(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientQuestions instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientQuestions();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientQuestions instance) throws SerializationException {
    streamWriter.writeObject(getQuestions(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientQuestions_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientQuestions_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientQuestions)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientQuestions_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientQuestions)object);
  }
  
}
