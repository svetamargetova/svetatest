package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListBundlePriceSummaryAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction::bundleUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction instance) throws SerializationException {
    setBundleUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction instance) throws SerializationException {
    streamWriter.writeString(getBundleUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction)object);
  }
  
}
