package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadUserRegistrationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getManagedUserUuid(consys.event.registration.gwt.client.action.LoadUserRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadUserRegistrationAction::managedUserUuid;
  }-*/;
  
  private static native void setManagedUserUuid(consys.event.registration.gwt.client.action.LoadUserRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadUserRegistrationAction::managedUserUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.LoadUserRegistrationAction instance) throws SerializationException {
    setManagedUserUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.LoadUserRegistrationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.LoadUserRegistrationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.LoadUserRegistrationAction instance) throws SerializationException {
    streamWriter.writeString(getManagedUserUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.LoadUserRegistrationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadUserRegistrationAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.LoadUserRegistrationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadUserRegistrationAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.LoadUserRegistrationAction)object);
  }
  
}
