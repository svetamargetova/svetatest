package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCheckInListItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getCheckDate(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::checkDate;
  }-*/;
  
  private static native void setCheckDate(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::checkDate = value;
  }-*/;
  
  private static native int getMaxQuantity(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::maxQuantity;
  }-*/;
  
  private static native void setMaxQuantity(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::maxQuantity = value;
  }-*/;
  
  private static native int getQuantityChecked(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::quantityChecked;
  }-*/;
  
  private static native void setQuantityChecked(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::quantityChecked = value;
  }-*/;
  
  private static native java.lang.String getUuidRegistration(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::uuidRegistration;
  }-*/;
  
  private static native void setUuidRegistration(consys.event.registration.gwt.client.bo.ClientCheckInListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCheckInListItem::uuidRegistration = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientCheckInListItem instance) throws SerializationException {
    setCheckDate(instance, (java.util.Date) streamReader.readObject());
    setMaxQuantity(instance, streamReader.readInt());
    setQuantityChecked(instance, streamReader.readInt());
    setUuidRegistration(instance, streamReader.readString());
    
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.bo.ClientCheckInListItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientCheckInListItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientCheckInListItem instance) throws SerializationException {
    streamWriter.writeObject(getCheckDate(instance));
    streamWriter.writeInt(getMaxQuantity(instance));
    streamWriter.writeInt(getQuantityChecked(instance));
    streamWriter.writeString(getUuidRegistration(instance));
    
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientCheckInListItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCheckInListItem_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientCheckInListItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCheckInListItem_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientCheckInListItem)object);
  }
  
}
