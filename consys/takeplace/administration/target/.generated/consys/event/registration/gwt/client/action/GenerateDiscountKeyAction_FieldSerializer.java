package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class GenerateDiscountKeyAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.action.GenerateDiscountKeyAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.GenerateDiscountKeyAction::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.action.GenerateDiscountKeyAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.GenerateDiscountKeyAction::bundleUuid = value;
  }-*/;
  
  private static native java.lang.String getKey(consys.event.registration.gwt.client.action.GenerateDiscountKeyAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.GenerateDiscountKeyAction::key;
  }-*/;
  
  private static native void setKey(consys.event.registration.gwt.client.action.GenerateDiscountKeyAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.GenerateDiscountKeyAction::key = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.GenerateDiscountKeyAction instance) throws SerializationException {
    setBundleUuid(instance, streamReader.readString());
    setKey(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.GenerateDiscountKeyAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.GenerateDiscountKeyAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.GenerateDiscountKeyAction instance) throws SerializationException {
    streamWriter.writeString(getBundleUuid(instance));
    streamWriter.writeString(getKey(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.GenerateDiscountKeyAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.GenerateDiscountKeyAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.GenerateDiscountKeyAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.GenerateDiscountKeyAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.GenerateDiscountKeyAction)object);
  }
  
}
