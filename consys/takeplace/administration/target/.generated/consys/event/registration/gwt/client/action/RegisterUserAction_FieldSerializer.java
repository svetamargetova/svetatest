package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class RegisterUserAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getAdditionalProducts(consys.event.registration.gwt.client.action.RegisterUserAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.RegisterUserAction::additionalProducts;
  }-*/;
  
  private static native void setAdditionalProducts(consys.event.registration.gwt.client.action.RegisterUserAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.RegisterUserAction::additionalProducts = value;
  }-*/;
  
  private static native java.util.HashMap getAnswers(consys.event.registration.gwt.client.action.RegisterUserAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.RegisterUserAction::answers;
  }-*/;
  
  private static native void setAnswers(consys.event.registration.gwt.client.action.RegisterUserAction instance, java.util.HashMap value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.RegisterUserAction::answers = value;
  }-*/;
  
  private static native java.lang.String getManagedUserUuid(consys.event.registration.gwt.client.action.RegisterUserAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.RegisterUserAction::managedUserUuid;
  }-*/;
  
  private static native void setManagedUserUuid(consys.event.registration.gwt.client.action.RegisterUserAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.RegisterUserAction::managedUserUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.RegisterUserAction instance) throws SerializationException {
    setAdditionalProducts(instance, streamReader.readBoolean());
    setAnswers(instance, (java.util.HashMap) streamReader.readObject());
    setManagedUserUuid(instance, streamReader.readString());
    
    consys.event.common.gwt.client.action.BuyProductAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.RegisterUserAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.RegisterUserAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.RegisterUserAction instance) throws SerializationException {
    streamWriter.writeBoolean(getAdditionalProducts(instance));
    streamWriter.writeObject(getAnswers(instance));
    streamWriter.writeString(getManagedUserUuid(instance));
    
    consys.event.common.gwt.client.action.BuyProductAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.RegisterUserAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.RegisterUserAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.RegisterUserAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.RegisterUserAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.RegisterUserAction)object);
  }
  
}
