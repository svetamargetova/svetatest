package consys.event.registration.gwt.client.module.register.outer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class OuterResources_cs_InlineClientBundleGenerator implements consys.event.registration.gwt.client.module.register.outer.OuterResources {
  private static OuterResources_cs_InlineClientBundleGenerator _instance0 = new OuterResources_cs_InlineClientBundleGenerator();
  private void bottomBackgroundInitializer() {
    bottomBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomBackground",
      externalImage,
      0, 0, 1, 67, false, false
    );
  }
  private static class bottomBackgroundInitializer {
    static {
      _instance0.bottomBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomBackground() {
    return bottomBackgroundInitializer.get();
  }
  private void bottomSeparatorInitializer() {
    bottomSeparator = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomSeparator",
      externalImage0,
      0, 0, 750, 1, false, false
    );
  }
  private static class bottomSeparatorInitializer {
    static {
      _instance0.bottomSeparatorInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomSeparator;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomSeparator() {
    return bottomSeparatorInitializer.get();
  }
  private void bottomTakeplaceInitializer() {
    bottomTakeplace = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "bottomTakeplace",
      externalImage1,
      0, 0, 143, 32, false, false
    );
  }
  private static class bottomTakeplaceInitializer {
    static {
      _instance0.bottomTakeplaceInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return bottomTakeplace;
    }
  }
  public com.google.gwt.resources.client.ImageResource bottomTakeplace() {
    return bottomTakeplaceInitializer.get();
  }
  private void middlePartBottomInitializer() {
    middlePartBottom = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "middlePartBottom",
      externalImage2,
      0, 0, 1, 5, false, false
    );
  }
  private static class middlePartBottomInitializer {
    static {
      _instance0.middlePartBottomInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return middlePartBottom;
    }
  }
  public com.google.gwt.resources.client.ImageResource middlePartBottom() {
    return middlePartBottomInitializer.get();
  }
  private void middlePartSeparatorInitializer() {
    middlePartSeparator = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "middlePartSeparator",
      externalImage3,
      0, 0, 548, 5, false, false
    );
  }
  private static class middlePartSeparatorInitializer {
    static {
      _instance0.middlePartSeparatorInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return middlePartSeparator;
    }
  }
  public com.google.gwt.resources.client.ImageResource middlePartSeparator() {
    return middlePartSeparatorInitializer.get();
  }
  private void middlePartTopInitializer() {
    middlePartTop = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "middlePartTop",
      externalImage4,
      0, 0, 1, 73, false, false
    );
  }
  private static class middlePartTopInitializer {
    static {
      _instance0.middlePartTopInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return middlePartTop;
    }
  }
  public com.google.gwt.resources.client.ImageResource middlePartTop() {
    return middlePartTopInitializer.get();
  }
  private void topCenterBackgroundInitializer() {
    topCenterBackground = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topCenterBackground",
      externalImage5,
      0, 0, 750, 1, false, false
    );
  }
  private static class topCenterBackgroundInitializer {
    static {
      _instance0.topCenterBackgroundInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topCenterBackground;
    }
  }
  public com.google.gwt.resources.client.ImageResource topCenterBackground() {
    return topCenterBackgroundInitializer.get();
  }
  private void topSeparatorInitializer() {
    topSeparator = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topSeparator",
      externalImage6,
      0, 0, 1, 12, false, false
    );
  }
  private static class topSeparatorInitializer {
    static {
      _instance0.topSeparatorInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topSeparator;
    }
  }
  public com.google.gwt.resources.client.ImageResource topSeparator() {
    return topSeparatorInitializer.get();
  }
  private void topTakeplaceInitializer() {
    topTakeplace = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "topTakeplace",
      externalImage7,
      0, 0, 181, 40, false, false
    );
  }
  private static class topTakeplaceInitializer {
    static {
      _instance0.topTakeplaceInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return topTakeplace;
    }
  }
  public com.google.gwt.resources.client.ImageResource topTakeplace() {
    return topTakeplaceInitializer.get();
  }
  private void cssInitializer() {
    css = new consys.event.registration.gwt.client.module.register.outer.OuterCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? (("html,body{height:" + ("100%")  + ";margin:" + ("0")  + ";padding:" + ("0")  + ";}#wrapper{min-height:" + ("100%")  + ";height:" + ("auto")  + " !important;height:" + ("100%")  + ";margin:" + ("0"+ " " +"auto"+ " " +"-105px")  + ";color:" + ("#555")  + ";}#wrapperNoFooter{min-height:" + ("100%")  + ";height:" + ("auto")  + " !important;height:") + (("100%")  + ";margin:" + ("0"+ " " +"auto")  + ";color:" + ("#555")  + ";}#footer{height:" + ("105px")  + ";}.GHAOYQ1E3{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";text-align:" + ("right")  + ";}.GHAOYQ1A4{background-color:" + ("#1b1b1b")  + ";text-align:" + ("center")  + ";}.GHAOYQ1A4 .GHAOYQ1B4{width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getTop() + "px  repeat-y")  + ";background-color:" + ("#1b1b1b")  + ";height:" + ("75px")  + ";margin:" + ("0"+ " " +"auto")  + ";text-align:" + ("right")  + ";width:" + ("750px")  + ";}.GHAOYQ1A4 .GHAOYQ1E4{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getTop() + "px  no-repeat")  + ";position:") + (("relative")  + ";top:" + ("21px")  + ";}.GHAOYQ1A4 .GHAOYQ1C4{float:" + ("left")  + ";margin-top:" + ("32px")  + ";width:" + ("200px")  + ";}.GHAOYQ1C4 .gwt-Label{float:" + ("left")  + ";margin:" + ("2px"+ " " +"0"+ " " +"0"+ " " +"5px")  + ";font-weight:" + ("bold")  + ";color:" + ("white")  + ";font-size:" + ("14px")  + ";}.GHAOYQ1C4 .GHAOYQ1NY{background-color:" + ("black") ) + (";float:" + ("left")  + ";}.GHAOYQ1C4 .GHAOYQ1NY input{background-color:" + ("black")  + ";}.GHAOYQ1D4{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getTop() + "px  repeat-x")  + ";height:" + ("12px")  + ";width:" + ("100%")  + ";}.GHAOYQ1E2{background-color:" + ("#e5e5e5")  + ";height:" + ("6px")  + ";}.GHAOYQ1A2{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getTop() + "px  repeat-x")  + ";background-color:" + ("#1b1b1b")  + ";height:" + ("67px")  + ";}.GHAOYQ1A2 .GHAOYQ1C2{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";}.GHAOYQ1A2 .GHAOYQ1F2{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getTop() + "px  no-repeat")  + ";position:" + ("relative") ) + (";top:" + ("19px")  + ";}.GHAOYQ1B2{background-color:" + ("#0c0c0c")  + ";height:" + ("32px")  + ";text-align:" + ("center")  + ";}.GHAOYQ1B2 .GHAOYQ1C2{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";}.GHAOYQ1B2 .GHAOYQ1C2 .gwt-HTML{color:" + ("#c0c0c0")  + ";font-size:" + ("11px")  + ";font-weight:" + ("bold")  + ";padding-top:" + ("9px")  + ";}.GHAOYQ1D2{float:") + (("left")  + ";width:" + ("586px")  + ";position:" + ("relative")  + ";top:" + ("15px")  + ";}.GHAOYQ1D2 ul{display:" + ("block")  + ";list-style:" + ("none"+ " " +"outside"+ " " +"none")  + ";margin:" + ("0")  + ";padding:" + ("0"+ " " +"15px"+ " " +"0"+ " " +"0")  + ";}.GHAOYQ1D2 ul li{cursor:" + ("pointer")  + ";display:" + ("block")  + ";float:" + ("right") ) + (";list-style:" + ("none"+ " " +"outside"+ " " +"none")  + ";padding:" + ("0"+ " " +"0"+ " " +"0"+ " " +"8px")  + ";text-align:" + ("center")  + ";font-size:" + ("12px")  + ";}.GHAOYQ1D2 ul li a{color:" + ("white")  + ";text-decoration:" + ("none")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1D2 ul li a:hover{color:" + ("white")  + ";text-decoration:" + ("underline")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1G2{color:") + (("#979797")  + ";font-size:" + ("12px")  + ";}.GHAOYQ1J2{clear:" + ("both")  + ";padding:" + ("0"+ " " +"0"+ " " +"105px"+ " " +"0")  + ";}.GHAOYQ1K2{clear:" + ("both")  + ";padding:" + ("0")  + ";}.GHAOYQ1H3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getTop() + "px  repeat-x")  + ";}.GHAOYQ1H3 .GHAOYQ1E3{margin-top:" + ("25px")  + ";}.GHAOYQ1H3 .GHAOYQ1E3 .GHAOYQ1B3{font-size:" + ("16px") ) + (";}.GHAOYQ1F3{background-color:" + ("#f9f9f9")  + ";}.GHAOYQ1F3 .GHAOYQ1G3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getTop() + "px  no-repeat")  + ";margin:" + ("15px"+ " " +"0")  + ";}.GHAOYQ1F3 .GHAOYQ1II{padding:" + ("15px"+ " " +"0")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1OI{width:" + ("180px")  + ";margin-left:" + ("10px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1AJ{width:" + ("160px")  + ";font-size:") + (("15px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1PI span{font-size:" + ("15px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1NI{font-size:" + ("12px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1BJ{width:" + ("364px")  + ";margin-right:" + ("0")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1A3 .GHAOYQ1AJ,.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1A3 .GHAOYQ1PI span{font-size:" + ("17px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1A3 .GHAOYQ1NI{font-size:" + ("14px")  + ";}.GHAOYQ1F3 .GHAOYQ1M2{font-weight:" + ("normal")  + ";}.GHAOYQ1D3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getTop() + "px  repeat-x") ) + (";}.GHAOYQ1B3 a{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_CONSYS_BLUE())  + ";text-decoration:" + ("none")  + ";}.GHAOYQ1B3 a:hover{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_CONSYS_BLUE())  + ";text-decoration:" + ("underline")  + ";}.GHAOYQ1I2{margin-bottom:" + ("10px")  + ";font-size:" + ("16px")  + ";}.GHAOYQ1K3 .GHAOYQ1O3 div{float:" + ("left")  + ";}.GHAOYQ1K3 .GHAOYQ1O3 .gwt-Label{margin-left:" + ("8px")  + ";font-size:" + ("15px")  + ";position:" + ("relative")  + ";top:") + (("-1px")  + ";}.GHAOYQ1K3 .GHAOYQ1IJ{float:" + ("right")  + ";}.GHAOYQ1H2{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";text-align:" + ("right")  + ";}.GHAOYQ1P3{font-size:" + ("30px")  + ";font-weight:" + ("bold")  + ";margin-bottom:" + ("20px")  + ";}.GHAOYQ1M3{font-size:" + ("12px")  + ";margin:" + ("10px"+ " " +"0"+ " " +"30px"+ " " +"0")  + ";}.GHAOYQ1J3{margin-top:" + ("30px") ) + (";}.GHAOYQ1N3{float:" + ("left")  + ";margin-top:" + ("20px")  + ";}.GHAOYQ1P2{margin-top:" + ("20px")  + ";}.GHAOYQ1O2{float:" + ("left")  + ";text-align:" + ("left")  + ";width:" + ("200px")  + ";}.GHAOYQ1L2{float:" + ("right")  + ";width:" + ("550px")  + ";}.GHAOYQ1L2 div,.GHAOYQ1L2 p,.GHAOYQ1L2 span,.GHAOYQ1L2 ul,.GHAOYQ1L2 li,.GHAOYQ1L2 td{font-size:" + ("14px")  + ";}.GHAOYQ1N2{overflow:" + ("hidden")  + ";}.GHAOYQ1L3{width:") + (("180px")  + ";height:" + ("130px")  + ";margin-top:" + ("35px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1P1{margin-top:" + ("35px")  + ";float:" + ("right")  + ";}.GHAOYQ1P1 a{font-weight:" + ("bold")  + ";}.GHAOYQ1C3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getTop() + "px  no-repeat") ) + (";margin:" + ("30px"+ " " +"0")  + ";}")) : (("html,body{height:" + ("100%")  + ";margin:" + ("0")  + ";padding:" + ("0")  + ";}#wrapper{min-height:" + ("100%")  + ";height:" + ("auto")  + " !important;height:" + ("100%")  + ";margin:" + ("0"+ " " +"auto"+ " " +"-105px")  + ";color:" + ("#555")  + ";}#wrapperNoFooter{min-height:" + ("100%")  + ";height:" + ("auto")  + " !important;height:") + (("100%")  + ";margin:" + ("0"+ " " +"auto")  + ";color:" + ("#555")  + ";}#footer{height:" + ("105px")  + ";}.GHAOYQ1E3{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";text-align:" + ("left")  + ";}.GHAOYQ1A4{background-color:" + ("#1b1b1b")  + ";text-align:" + ("center")  + ";}.GHAOYQ1A4 .GHAOYQ1B4{width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getWidth() + "px")  + ";overflow:" + ("hidden") ) + (";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.topCenterBackground()).getTop() + "px  repeat-y")  + ";background-color:" + ("#1b1b1b")  + ";height:" + ("75px")  + ";margin:" + ("0"+ " " +"auto")  + ";text-align:" + ("left")  + ";width:" + ("750px")  + ";}.GHAOYQ1A4 .GHAOYQ1E4{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.topTakeplace()).getTop() + "px  no-repeat")  + ";position:") + (("relative")  + ";top:" + ("21px")  + ";}.GHAOYQ1A4 .GHAOYQ1C4{float:" + ("right")  + ";margin-top:" + ("32px")  + ";width:" + ("200px")  + ";}.GHAOYQ1C4 .gwt-Label{float:" + ("right")  + ";margin:" + ("2px"+ " " +"5px"+ " " +"0"+ " " +"0")  + ";font-weight:" + ("bold")  + ";color:" + ("white")  + ";font-size:" + ("14px")  + ";}.GHAOYQ1C4 .GHAOYQ1NY{background-color:" + ("black") ) + (";float:" + ("right")  + ";}.GHAOYQ1C4 .GHAOYQ1NY input{background-color:" + ("black")  + ";}.GHAOYQ1D4{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.topSeparator()).getTop() + "px  repeat-x")  + ";height:" + ("12px")  + ";width:" + ("100%")  + ";}.GHAOYQ1E2{background-color:" + ("#e5e5e5")  + ";height:" + ("6px")  + ";}.GHAOYQ1A2{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getHeight() + "px")  + ";overflow:") + (("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomBackground()).getTop() + "px  repeat-x")  + ";background-color:" + ("#1b1b1b")  + ";height:" + ("67px")  + ";}.GHAOYQ1A2 .GHAOYQ1C2{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";}.GHAOYQ1A2 .GHAOYQ1F2{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomTakeplace()).getTop() + "px  no-repeat")  + ";position:" + ("relative") ) + (";top:" + ("19px")  + ";}.GHAOYQ1B2{background-color:" + ("#0c0c0c")  + ";height:" + ("32px")  + ";text-align:" + ("center")  + ";}.GHAOYQ1B2 .GHAOYQ1C2{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";}.GHAOYQ1B2 .GHAOYQ1C2 .gwt-HTML{color:" + ("#c0c0c0")  + ";font-size:" + ("11px")  + ";font-weight:" + ("bold")  + ";padding-top:" + ("9px")  + ";}.GHAOYQ1D2{float:") + (("right")  + ";width:" + ("586px")  + ";position:" + ("relative")  + ";top:" + ("15px")  + ";}.GHAOYQ1D2 ul{display:" + ("block")  + ";list-style:" + ("none"+ " " +"outside"+ " " +"none")  + ";margin:" + ("0")  + ";padding:" + ("0"+ " " +"0"+ " " +"0"+ " " +"15px")  + ";}.GHAOYQ1D2 ul li{cursor:" + ("pointer")  + ";display:" + ("block")  + ";float:" + ("left") ) + (";list-style:" + ("none"+ " " +"outside"+ " " +"none")  + ";padding:" + ("0"+ " " +"8px"+ " " +"0"+ " " +"0")  + ";text-align:" + ("center")  + ";font-size:" + ("12px")  + ";}.GHAOYQ1D2 ul li a{color:" + ("white")  + ";text-decoration:" + ("none")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1D2 ul li a:hover{color:" + ("white")  + ";text-decoration:" + ("underline")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1G2{color:") + (("#979797")  + ";font-size:" + ("12px")  + ";}.GHAOYQ1J2{clear:" + ("both")  + ";padding:" + ("0"+ " " +"0"+ " " +"105px"+ " " +"0")  + ";}.GHAOYQ1K2{clear:" + ("both")  + ";padding:" + ("0")  + ";}.GHAOYQ1H3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartTop()).getTop() + "px  repeat-x")  + ";}.GHAOYQ1H3 .GHAOYQ1E3{margin-top:" + ("25px")  + ";}.GHAOYQ1H3 .GHAOYQ1E3 .GHAOYQ1B3{font-size:" + ("16px") ) + (";}.GHAOYQ1F3{background-color:" + ("#f9f9f9")  + ";}.GHAOYQ1F3 .GHAOYQ1G3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartSeparator()).getTop() + "px  no-repeat")  + ";margin:" + ("15px"+ " " +"0")  + ";}.GHAOYQ1F3 .GHAOYQ1II{padding:" + ("15px"+ " " +"0")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1OI{width:" + ("180px")  + ";margin-right:" + ("10px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1AJ{width:" + ("160px")  + ";font-size:") + (("15px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1PI span{font-size:" + ("15px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1NI{font-size:" + ("12px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1BJ{width:" + ("364px")  + ";margin-left:" + ("0")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1A3 .GHAOYQ1AJ,.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1A3 .GHAOYQ1PI span{font-size:" + ("17px")  + ";}.GHAOYQ1F3 .GHAOYQ1II .GHAOYQ1A3 .GHAOYQ1NI{font-size:" + ("14px")  + ";}.GHAOYQ1F3 .GHAOYQ1M2{font-weight:" + ("normal")  + ";}.GHAOYQ1D3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getHeight() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.middlePartBottom()).getTop() + "px  repeat-x") ) + (";}.GHAOYQ1B3 a{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_CONSYS_BLUE())  + ";text-decoration:" + ("none")  + ";}.GHAOYQ1B3 a:hover{color:" + (consys.common.gwt.client.ui.img.SystemBundle.INSTANCE.constants().COLOR_CONSYS_BLUE())  + ";text-decoration:" + ("underline")  + ";}.GHAOYQ1I2{margin-bottom:" + ("10px")  + ";font-size:" + ("16px")  + ";}.GHAOYQ1K3 .GHAOYQ1O3 div{float:" + ("right")  + ";}.GHAOYQ1K3 .GHAOYQ1O3 .gwt-Label{margin-right:" + ("8px")  + ";font-size:" + ("15px")  + ";position:" + ("relative")  + ";top:") + (("-1px")  + ";}.GHAOYQ1K3 .GHAOYQ1IJ{float:" + ("left")  + ";}.GHAOYQ1H2{margin:" + ("0"+ " " +"auto")  + ";width:" + ("750px")  + ";text-align:" + ("left")  + ";}.GHAOYQ1P3{font-size:" + ("30px")  + ";font-weight:" + ("bold")  + ";margin-bottom:" + ("20px")  + ";}.GHAOYQ1M3{font-size:" + ("12px")  + ";margin:" + ("10px"+ " " +"0"+ " " +"30px"+ " " +"0")  + ";}.GHAOYQ1J3{margin-top:" + ("30px") ) + (";}.GHAOYQ1N3{float:" + ("right")  + ";margin-top:" + ("20px")  + ";}.GHAOYQ1P2{margin-top:" + ("20px")  + ";}.GHAOYQ1O2{float:" + ("right")  + ";text-align:" + ("right")  + ";width:" + ("200px")  + ";}.GHAOYQ1L2{float:" + ("left")  + ";width:" + ("550px")  + ";}.GHAOYQ1L2 div,.GHAOYQ1L2 p,.GHAOYQ1L2 span,.GHAOYQ1L2 ul,.GHAOYQ1L2 li,.GHAOYQ1L2 td{font-size:" + ("14px")  + ";}.GHAOYQ1N2{overflow:" + ("hidden")  + ";}.GHAOYQ1L3{width:") + (("180px")  + ";height:" + ("130px")  + ";margin-top:" + ("35px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1P1{margin-top:" + ("35px")  + ";float:" + ("left")  + ";}.GHAOYQ1P1 a{font-weight:" + ("bold")  + ";}.GHAOYQ1C3{height:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getHeight() + "px")  + ";width:" + ((OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getWidth() + "px")  + ";overflow:" + ("hidden")  + ";background:" + ("url(\"" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getURL() + "\") -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getLeft() + "px -" + (OuterResources_cs_InlineClientBundleGenerator.this.bottomSeparator()).getTop() + "px  no-repeat") ) + (";margin:" + ("30px"+ " " +"0")  + ";}"));
      }
      public java.lang.String agreeing(){
        return "GHAOYQ1P1";
      }
      public java.lang.String bottom1(){
        return "GHAOYQ1A2";
      }
      public java.lang.String bottom2(){
        return "GHAOYQ1B2";
      }
      public java.lang.String bottomCenter(){
        return "GHAOYQ1C2";
      }
      public java.lang.String bottomMenu(){
        return "GHAOYQ1D2";
      }
      public java.lang.String bottomSeparator(){
        return "GHAOYQ1E2";
      }
      public java.lang.String bottomTakeplace(){
        return "GHAOYQ1F2";
      }
      public java.lang.String bottomTextSeparator(){
        return "GHAOYQ1G2";
      }
      public java.lang.String centerPanel(){
        return "GHAOYQ1H2";
      }
      public java.lang.String completeYourProfile(){
        return "GHAOYQ1I2";
      }
      public java.lang.String contentPanel(){
        return "GHAOYQ1J2";
      }
      public java.lang.String contentPanelNoFooter(){
        return "GHAOYQ1K2";
      }
      public java.lang.String descriptionPanel(){
        return "GHAOYQ1L2";
      }
      public java.lang.String dontHaveAny(){
        return "GHAOYQ1M2";
      }
      public java.lang.String eventDescriptionOverflow(){
        return "GHAOYQ1N2";
      }
      public java.lang.String eventLogo(){
        return "GHAOYQ1O2";
      }
      public java.lang.String fail(){
        return "GHAOYQ1P2";
      }
      public java.lang.String formTitleBig(){
        return "GHAOYQ1A3";
      }
      public java.lang.String htmlStyle(){
        return "GHAOYQ1B3";
      }
      public java.lang.String middleBottomSeparator(){
        return "GHAOYQ1C3";
      }
      public java.lang.String middlePartBottom(){
        return "GHAOYQ1D3";
      }
      public java.lang.String middlePartCenter(){
        return "GHAOYQ1E3";
      }
      public java.lang.String middlePartCenterWrapper(){
        return "GHAOYQ1F3";
      }
      public java.lang.String middlePartSeparator(){
        return "GHAOYQ1G3";
      }
      public java.lang.String middlePartTop(){
        return "GHAOYQ1H3";
      }
      public java.lang.String outerBundle(){
        return "GHAOYQ1I3";
      }
      public java.lang.String partBottom(){
        return "GHAOYQ1J3";
      }
      public java.lang.String passwordPanel(){
        return "GHAOYQ1K3";
      }
      public java.lang.String portraitPanel(){
        return "GHAOYQ1L3";
      }
      public java.lang.String providedByTakeplace(){
        return "GHAOYQ1M3";
      }
      public java.lang.String registerButton(){
        return "GHAOYQ1N3";
      }
      public java.lang.String showPasswordPanel(){
        return "GHAOYQ1O3";
      }
      public java.lang.String title(){
        return "GHAOYQ1P3";
      }
      public java.lang.String top(){
        return "GHAOYQ1A4";
      }
      public java.lang.String topCenter(){
        return "GHAOYQ1B4";
      }
      public java.lang.String topLanguage(){
        return "GHAOYQ1C4";
      }
      public java.lang.String topSeparator(){
        return "GHAOYQ1D4";
      }
      public java.lang.String topTakeplace(){
        return "GHAOYQ1E4";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.event.registration.gwt.client.module.register.outer.OuterCss get() {
      return css;
    }
  }
  public consys.event.registration.gwt.client.module.register.outer.OuterCss css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String externalImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABDCAYAAABdhW/uAAAAG0lEQVR42mOQlpb+zzBKUEZISUn9Z7C2tv4PADW0WE6mWPrqAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage0 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAu4AAAABCAYAAACBkCu0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABkSURBVHja7FQ5DgAgCJP/PxpXFw5riQmhGz0galBUVZcBR4K9kS/Tx/O89Lc0ZuYH38Hb+bxMnp2p3DfGviN/FeIfDAYDBkQE4s46q2UyN1lEZ3CVc9G7QN8kU0fcBgAA//8DAHnIk/5s0aWTAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage1 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAI8AAAAgCAYAAAArKSuoAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAqTSURBVHja7Jt5cFVXHcc/92XfQ3YSmoQdaxFordgltArFadWKdJuWgqJSWrC20qnguCEWR9qqiFOgEWYUW0trR2kZOkq10DLaYKXVKRW7UEAQCE1ClheSvJf3rn+87w0nt+/lXbLYCclv5k7uu8vvnPM73/P9LefGytnwEkAKcB9wG5ANhPEmPqAV2AH8CKjDq9gMCgnZiaQknGHFhTeSm9xIpw0WwwKQCKQB24GZfdAzCZilo27YrENDfGKbmf2gawrwwLBJhxZ4buxHfVVis2EZIuBJ7c8QAUg+X+KdYYkPnvZ+Bk94GDhDJ2BO72d9CcNAGTrgqe1HfUGg7f8AnGKVFjKADiAT+A2wexDY/H5gtBg/GdgL/HqwgmczcFM/6XsS23ONqC+Sr0kw5cggAc8dwDjj9wWDFTw+YCfw/X7Q9TjwkNzWfGANMA9IGoB+H1B5YZtxrXWQ2Hw+sMH4fWIwuy0bWAn8A7gOKFHQ6zgeS7/zgRmu908DzwB/BJ7UO5nAg9JzXPf6u3BoAy8I/HOMfg4GqQEOA3eqz77BDB5HtulIiBKx2GKQR4CvGNf/Bix0xTh+xVEOeAay4lw8SO0+kvNglyMxRrodTQLAUmAM8Mluz0aAkwbkaCU5OsJAmYLadKAZaHTpTQPyiOypZQi8YT17Emjqof8JHsc5Ss+eAd6Lcr8QKFL7lgL/pghDWCGfFSInqZGwDTZkWVAAdGqctcZ4x2jRhNT/t/VctHDBi+TJptlE9h8t2bIB+I9HHRXSkyK7tmgOGqQrmhTpSFebgbP2OBvTnms1OADMBfYAk/XbkeXAF2WYUl27WDQdEEieB76ge7NF3ZVEJiNHLs+nDp4GjordfgK82csFshxYLEMcBj7RlRHCx4GvAZcI5Bm63injvuWzQn9uDuav3HbsXhaMXktjkJWKW1oEkluA14AfqJ1C6ahTnx93xTjxJBP4FvAx9SlXtnGKue0C7DvAVmBTDD3zgAVE9h2LXeA5rf49q347UgUsAy4SO5r2qNNi+B2wtjfgQQj8vBRNdKG1IgqzjXJlFo5MkR5TwpqQBMVY+cBUYDq923S9n8huvyMmE1wtN53jCroDMloRUGQRurKjM6Oo5tSCJfdM3ESn7fc3BihM9HWBZKO4d7rBxglaEAXAFZq4tR77nAbcy/sr/yGxQKrsXKGkIRd42PXs17XgojFejo5KMbsj1wC/NwDjhCBBXSvRUaVscWVvg7WDwBJRp5NNPQBMkxH/rWv7tbov0t9Fho4dwHrgS3KDU4GxqoFM0vWjBtBuOMc+LlPgjjLKCQJMUNdWy4htwAq5nAoZ9QKVL2rBIju57q4G/5hLth75MuOy+F4gzHjgmPRMBl4So31Y7XwU+LkRCa4QkLxInSb/h0oGrpA9RmvSLlV5xXGVi4ERxvtjxFwO4B4RMC6VJ7gc+Axwt2yA5nC1QNIO3KNxOCAtA243XP4iYEFfNjE3EvmUI1ETclwHokaHpfbGeP9fiqFiyZtyHb/V4Co8lB0cuRP4sc53y7WYsdZ4TQgawxqXrmbgaVH3OgublLR3P/1K/fR90/LGkpN88EgwjF/Pvgh8I0p/9gmIn5XbqNLK9pJJbuzh/iElLROBK7UASuWKUMacr/NNwFc9tOmAEuCXwDrX/Ra53wnAd1XcvKavO+D/jXIt1aC+TA2uKY6PLxEIndQ1Qe5jhJGVxGNJZ1UsNGKMnWIsv+vZiQoiMViSGAAAICOpsWT/qdnsyXmW2yoPpta2dzFutsbgj/L+boEHoLwX9i0FsjR2S3ZxPsCLFeyONVhnm8d2LjTOX+zhudeM8+yB+HzCNiLyMLE3Skvlq6u0MuMVE0Nx7t8u13iXfm+Waw1Eedak+UVyM68LAD6NIQh8pKtxOzGVpCayklrptLsVJ8LE3oxpdi2ScykkLhPbjojzrLt9p53gORRORxrna7QA98mlW9IfMtgJIOOD+vYmTW7hMiOLO2yk+WF1PM/I3OLJNMUFjrwbAzi4gtETCqKXx9Gf0otxmmm617rO54Atxu9azn75ENLRrtgmI075xWub5tje0N9vxnkn+4MCz2UGcF7R6j+kwToGagOuVTrppZ/fJrI5+ryC9tViknUxSg6OVAOPyr3MkptNMgDsB+p9VudOQum0h1LxeS/vJboYwovMcwX9W8UglsDYKZe1XYGv1Q+ANV3gQ8AuIh8JXiW3mSS97bJHE/D3voPn/YRtue5Go/Qy4/xXwD89DCqaIdxttShL2iPK/5kMv9n1XoNxXm4EzttjDbMlUMzFRU8xo2gXjQHP1slxpb3EGYNP8Z+TbGwg9vdWncb7VpR2EvH+oZ+Zsk8WeJ7W4SlD6RkgsY7oT1vG5PvjGKynKnGL8WxLlPv1Ri+cmOiosi2MjGOO670DRr9m4uHrx85QBiPT36YsvY6O7tFXWw+xxXRXlhQtFvL3YJsUDzFgh+zgyDEDPNd5BM9+43yu18r9QLitgGGACiKbqa+KbRqUFZk7yQtVRXYGnSyqHKfA0RlISZS2zKKjuc/1BwXL643guVMurUPV2Rq5qekqB/xUfQiozTQFq9OAN7BCuzpCGbSHcLutYtG7E5uExTjXcvZTl1rgZVctxllkhRpzQO/WGax1H/CYwJkgMBWpblZl2KvCYI8dqhElqwZ0SBnUael37Fuu+OYdHbtUq7pKMdejKr049khXCWAKkU30Gkv/t3UubsmLVBsFwTZVdss1SXcoI9hrpIi2Js4W1eYJ2C+oTJ+plbZXk2IT+QbmUwY1B5QazwdO6dp6I/tCIL4b+KtqPX9ypdDvuYyVres1TR1lM2aVVgcXj19DfQdZ0jXOxSZn1M9cVzC7xCgfVIsJC10lgaUa381EvlAwXWwLZ/9NKk8L4HVliQ4412urwQa+A6xyuf8GgSfFiOueIPLfMwAfUrGzIIY9Mo1M7lVgjm8AgINWzBOi5DSlvLkG1fqB64HndG6JRcq1st5S1XmmANGq3lwufUlaISmatGYZd7Yx4c6kbdP7IVVYRxpbFVcDvzCKm4ViyBLpaQNqLMLPEE4OtYUyowXLtSp4Zuu9MqNSW6MAeIMrWSg0+h0gsrfmMOtT2p44rHt5RpU3TwlAJZE9uTqNrVhptG1U+5cKlO2y00jpKJD9GpSRmq58phZlbRR7OLWsl0UCrT0zT98/Ja1UB8Jig5PG9oDj38uVjqcZK3i/ESjmGYFnWDGNrXdSjCzGydSOuzKOZCL7a079qTZKEJon8OYYFfOWiHu1G4Lh1OCZjjJuHbuC60c9RlOwG/M4hchcTXKqJv0Eka8bg662ijQRZr9tPW8mCNmyzQhNdlBgOeBy24mG3eqjpOAVajNF9jkjF3bSYEq35Et3ttoOSP9xVeqDAAMNnvNCmgKl3FCxipvKt9DQAT6rG3j+IpYMDDW7DP+DXsz82abTTsIfLODmilXcUrGF+kBXsOw32K11KALHe6o+BCVkJxIKZzC3/GFurdxCUxBCESaeQGTX26lVTVJyUDoMnmGJFJWChUzNe475o6up74COcFf94UEiO/ZZRpGxWkHusNsaFsBOIC2xmUQfBMLdajsHFTSeMFLfAqJ/YXBey/8GADlU9zQfDE8WAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage2 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAFCAYAAACEhIafAAAAGUlEQVR42mP4+fPnf4Zfv379Z/jw4QMSAQDyOxNFBQwg+gAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage3 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAiQAAAAFCAYAAAB1nPfKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAGBSURBVHja7Jm/SsNQFIe/3ASKY/9Q6NBnOK6CL+EqCE6Ck+AgBWehD9DJVRB8ACfBVXALgqtzYshcLPfGQdKhJG1Dk7bg+eByzz33d3LJuQR+EC/LMhRFUVYRhmEHuAXOgWdgIiLvFeoPgSvgFHgB7kTkTTurKAqAN51OASgyJk3ndqmpo2ZX59bZl23l6sxvsldFU0VXV93Sj9Xz5nPRMMZgjCmNy8aiJl8XnbFgTC6AT2ACPInIT4kROQGugWPgERiLyEeR1jlHlmU45+axtXa+zuOyeXHkz8rvo8l7abJulXaT/bK9JvO7yO27Zls1+1qXG5JMDYkaEjUkakjWNSQ51lriOCZJEowxdLtder0eQRBgrSVNU5IkYTab0W636ff7tFqtpe+ohkQNiRqS/2tIAsBDURSlIr7vMxgMiOO445wbRVF0GUXRAfAKHAE+8ACMh8Phl3ZMUZRlBNoCRVE2QURS4CYMwzEwAs6Ae/5+zXxrhxRFWYdfAAAA//8DAITH1hBa1mpzAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage4 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAABJCAYAAAD8HkyIAAAAVElEQVR42u3QOwrAIBRE0QshBAIp0rj/xVlYCIKKn2riS7aRgXsWMADbit04jNO4jNtwaI1Syo/xvuG9FyEEEWMUKSWRcxa1VtFaE713McYQc86PBxPN/h/w6vNnAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage5 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAu4AAAABCAIAAAAO8rzjAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAABrSURBVHjavFPZFQAwBKs57D+nTtASCT5z1UPN3c+3zOwgleorgeijRa+WkuBoyFAyCaLiubkJ95X2z7DCLzChnEtYq4hYsGtlDe+HRamXvo6jCUws2RUJLg9Ksq82Rd52+4Ah8QUAAP//AwBXIWZTTdTU8wAAAABJRU5ErkJggg==";
  private static final java.lang.String externalImage6 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAMCAYAAACji9dXAAAAOUlEQVR42hXEoQEAIAgEwC82q9kJ3H8EIk0SBEZggVcuHCKCEBFCVYlrRrg7kZlEVRHf6Xa3utmNByF+GV9vOllwAAAAAElFTkSuQmCC";
  private static final java.lang.String externalImage7 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALUAAAAoCAYAAAC4q7s0AAAMZ0lEQVR42u1cC5QT1Rn+8VEtsEl2gUIBX7UePdVaxSqtT1DwbX2gVuxDq5Za8VEUC5VSVHa1gGdZl8xMsrs8RKWwh6JQoXDAk52ZzSS7pUesUCxgPaW2CEiXLoLlmf7/ZIa9c3cymckm2YWde85/cjJz55+byXf/+/3f/RMAowVFtTQYUaYHJeUvaNuCkvppbqZsRVsdkOQfgN/81lmtRGw8F4G4HS2VTwuI8mv+0/Vb8Vsq1SMoyZ/kG9CmhSLq0/5D9ltRG4Lux4UCtGH7B1dqX/aftN+K1jBKLykwqFOBqDrUf9J+KyKolZWFBjUmoSP8J+23orWApCwtNKhDkjrMf9J+Kx6oRXlZwUEtNAz3n7Tfikc/Isoqn3747XhLFGcXGtQl0dh5na7yiModIUF+FelWZdrUKhzbtH5CrPfx9p1GEvDriAZV0QRUkkkJeFVKwo+6D/2IqkMLDOpNXWLyikoDWoq3vrMaBx6HoE7xhsCOdzcFZEHBonSk4btd4jMK8sMI4g85UO/rLcW/ctyBOgkLEMi7WFBjxP5dd1RB5uUZ0P9BG2X6LxWVCwOisp4MqcCGgKDe2EkRe8bxDmpqQgx6I5g/7tagpkZRNRRRp2JiV4cJ5BzeCPg6+DMnlwf1ayXl54E6rczCacPy1WyUDEnqQ500eR/vDqA2aMi6bg9qjxFvYTtQR5SWjGAKI3dnl35B/n4njfu57gJqBPJffVB75+IaB+xdMDd2qu0qEF7ThwV1Z3FtH9R+c24IYK7CbwcL6hJJuQL58y/IkEvPZEFNZanmOdMsSsSU1An9Z6zqFRAaLkX+PTokyc/gddN1ekOrhG7qmyFBmYWvE0Ki/L2B0bU98w3qoKD8FPs9iyvLeLzXOBpTu06pVA/KEfQ+olJN4zo6RkmpwWMvUH15WXVsMH/plBicFE3C2KgGz2KCN15/1WA020dKwn1SAmoRnAsNE7DvU5IGF3cU1FNScILQDKfhGK5Hn2N0OTAB1WivM/ebg9dPw9dHxSRc4hYeUhwuwDH+DP1OR3sNrR59LNI/SxJeQp9P4vk70eeFc2NwaobPMBT7PI7XvKL74MZE/mvicFFecV1WGxuMX9xeA9RbIbr25KM8WlTm28lomQz57g1Hr5XUYXhst5fr0bajPZovUIdENczfozSqnm6hVTjh8PjHLsd3kLRy9vr6dELXToJLzxXoYXyBqUyGX/abHQG1Ad7DTvewsTU4EQZkum91EgJ4v8VefOLnOJP1QUDH440efCyfo0C//CVfbVr3QQso9CjqHpRsjUggIp/jEdBtkyMi39xRUHMqCdkBtHVsAozjvTPDGPajtaJ9bj95ZcH0sWIznCLEQcUv5SD3Ja92+4VidJ/fAVAv8gho0zbV18OJtj41WO/VH+uLVg7+eRh2CK3VMLuJuIlWnrwBm+gDAbusOhloUzziZ5QK6pXEn41lnI3Kk+l4iSBfrhtSlb6zG0vYJR19voP95mJ0e4rkQfJVFm48n/wGZ8lnBcPKEEo40d+7lskhKvGOgBqPV1ipkjKzlxAbwFMOPLeH6beLFJ3SWfI3e9Wu6U+fJTQzFgpG42cj1bofz7e0jU9OQdVHloiPy+gg/FK2Wb5ADWbg610I8CtqmuF8Mjx2Hb6v4L9Qimw5gvo2irx4vhzH8BAu8TdHmuDb+HpOOA5n4IQ5LxKHq7HPBLQDlvsm4W4bjfwxbmyfo01FP/dITXBtjQaXk79oE9yCfX+In2US2i+5MVlWJ6RGk4mGzGqEgbQKkJmUCa9t5vT4B/KdOI4iwNmd6yNpgzjgXePoDEHjabUQlfWM/8+gvv7EXECNE+h5bvJV2k5ioWG4JfGV1FuzSIn3WVelhnFZQFjpyFcTMI4D2AuFThSNCcACdqpNnw3M+T21TXCWG99EtfQxr4WeeN3+oz40CDtdRwDnJtuy4unDBZb0kP8+zfhvtUT9zKDeS1G1LSlUJ7kBtAHSyUzf/0H1ilOcxkc1JgYtMaP1PBtAbGG+nLkuQLaXAe3iYqgfxtJvAm4Ge642Cf050Fd79Y+rxWUWH3HIqpJRCQBzzQfHBKgDQuzrAbHxO8S59QgZVq4LRhpG4rGbMELfgrTkNgTkIsb/bpYGOYD6izZAy+OtK4kadk445SjTf6tLOfEfzOdf0mFQJ+E9pn9TR0Bdp0EZ0o5v4ZJ+Jdpwogo61dHgBqIk+HqrlIRRTqCuicOl3OrxE8+TRoPRrA+KxC5AXctc8/cuDeqApE7BvltySBZbXEZqsgpObjyACe6LLnYm32YjNfF4fJ1D8h1RGNaCklxu3KOV4elLOwxqDVYx/TfkxKmRO6Oft4kqeE4WkzDdAi6aCNY+nldjnFDPcD42oi3ROX8CnreYBi/qUl8C/sb0/2eXBbWN+lAoUPO2B+rXf8lF1F3FXHM4h8m3Og+gfocB2IdeQU0SmJHIpXIyDtR6kskqOBjZcwD1ZO4+H3gc13+7JKhJaeAAQNLYcxj17iKqQUkZ0Q+MgNdSTYlOTayauFtQHwmFldspwbWOTd2YDdjYbyUTddenJb7YmehLzgJmkgZ3BsWG6Xng1MvZiOYV1DgpfsOBVDYUkNv1pBBBWpOAEURHTFpioR88qDW4MZs64oJKTOL1euOz1lkSyPZ2GO/Xgq9/7JKgpk0YC78VlHHZlRd5Yg6g3td2XB3Dga/ZeaexjcNT1aElaSXJMawMIQmScgB6Rf+X0J8G6dJghlICr6BGADQw/dd5BTVev4JVKlwmirsygjot/bH6+f05ROqxFh8qnM7o1wNIuiRZEMc+jO5HPJ6kTkxSB89/H3oVdUedoqkFNJJ8rwNA77Xs4okNV2WnA/JvcwC1Rf1ov4Mor3Dg+1VM37wseTnQj4/YzZocIrXCXN+c7X4EGiMa2nPqOFzA6ctjOyob2unvXabxoCYa4RbUtDmTFdRWOa4l08+08NyvLDp1tdKPq/XQuBVltv399B8duNapcwB1nVNfUYVS7HOEAaXoqBknYYETqO0ifbZITbUXlkmU1pgPMPdc6TlSY2TmaIXQZUFNPxLglvdHMkdddYRVL1arnJ/E2pOpcIjd3XPgwo/Y0Q+z0b9J4fFtHMd+me/HVx2i7XBTeUjyJP0ZZ1ZQaxB1nSSml/p7bCL1+07+OB8HonE42+metJHCJZYTso4rAU9meya0tW1uvhjPYR1XMvCgC317EFlRQU10gAPKpyTZBQT1MXrVC30MrknR0ybBeovApctketWbWoVc+w083mSbjElyOSWSVO1nJnEhUX7AkN7aVAtJmUZJJ1uI1TuifKO9T70i8EG2kpCvPDSMtuxFgw5V4Jhf0SsLBWW5+XMyvO4JF5F6p5EIUjVapVHVNs1ImDZx0Ww7WztBGxZU8UdKAAOuzVRlR9veDH+dyPnZSRGfdicNyexl4/1b3Ba+aX9GED8sNsPXLPfma1NwcqGfeSS/0Ra/8TkE2g7Xt7k1eJdTUUbaKC3v4XhnkzaulwlQ5V8CJLTf6+NIwCH0u7To0ZrbsGhnlEgxFKTcgzwmUt2F3TmThiCYX3cspuK29xGEd2fo+xK3Pb/Uc8GVKC9zAWq3doRqNTg/Tv0bzX5zYxDC97tdF06lk7hlNucWcitEudfPEW6CPlmkPVfGRvziNIyaxFFtSkqPoG3m/0qB6qRJXnMAyGb2P0WMEtBDhpF23GLWP6frnPVrDnFG/VJlgnyaDVWp4PpSgjvRhi6NMX6t/oVT2Wl6vMriTP/dzYF6C0alPxmVaXalmjuovpiNvIyfrUYV2yHOiIMvaUcp0nXOux3KQmsrNdD/5BPvdwfv047Pk0ZtVBruc5woCfgXRWpSL2yUkOuNSPyZI5g1+Dfxd1pdig9qk19HVwf1H+JKymUhSb7YDlA8D6Vk0dwap21yutakFkf71WllVDhlWs+o8lWzMIruyZ5jTacUGQqg+L5O/xNCtCkkxC+i7Xz60QAZFW7RMTd/xcCC2ixo0qv30lVzI0kHrtHgGlIZTJDZct8k9Dc5Jm+0HW6flkCQtsnJP92HtsfFJriK5DJLXIrBSbxPujZzugN96ccMpHHXJOAm8kv3wM83hKoAyZ8b1YXGQeMhoOt+aPcSnwvVYbvx4bdOal4lPb/5zQe13/zWCaDeyGT7M/0n4rdjsum/Q0zLYE9wlXLr6Jchbovs/ea3LtOMXTgnmeoP/lPy2zHVSMVA4H5i7NS1crY3286i39q3/wNXJoSLLLJKGAAAAABJRU5ErkJggg==";
  private static com.google.gwt.resources.client.ImageResource bottomBackground;
  private static com.google.gwt.resources.client.ImageResource bottomSeparator;
  private static com.google.gwt.resources.client.ImageResource bottomTakeplace;
  private static com.google.gwt.resources.client.ImageResource middlePartBottom;
  private static com.google.gwt.resources.client.ImageResource middlePartSeparator;
  private static com.google.gwt.resources.client.ImageResource middlePartTop;
  private static com.google.gwt.resources.client.ImageResource topCenterBackground;
  private static com.google.gwt.resources.client.ImageResource topSeparator;
  private static com.google.gwt.resources.client.ImageResource topTakeplace;
  private static consys.event.registration.gwt.client.module.register.outer.OuterCss css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      bottomBackground(), 
      bottomSeparator(), 
      bottomTakeplace(), 
      middlePartBottom(), 
      middlePartSeparator(), 
      middlePartTop(), 
      topCenterBackground(), 
      topSeparator(), 
      topTakeplace(), 
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("bottomBackground", bottomBackground());
        resourceMap.put("bottomSeparator", bottomSeparator());
        resourceMap.put("bottomTakeplace", bottomTakeplace());
        resourceMap.put("middlePartBottom", middlePartBottom());
        resourceMap.put("middlePartSeparator", middlePartSeparator());
        resourceMap.put("middlePartTop", middlePartTop());
        resourceMap.put("topCenterBackground", topCenterBackground());
        resourceMap.put("topSeparator", topSeparator());
        resourceMap.put("topTakeplace", topTakeplace());
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'bottomBackground': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::bottomBackground()();
      case 'bottomSeparator': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::bottomSeparator()();
      case 'bottomTakeplace': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::bottomTakeplace()();
      case 'middlePartBottom': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::middlePartBottom()();
      case 'middlePartSeparator': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::middlePartSeparator()();
      case 'middlePartTop': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::middlePartTop()();
      case 'topCenterBackground': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::topCenterBackground()();
      case 'topSeparator': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::topSeparator()();
      case 'topTakeplace': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::topTakeplace()();
      case 'css': return this.@consys.event.registration.gwt.client.module.register.outer.OuterResources::css()();
    }
    return null;
  }-*/;
}
