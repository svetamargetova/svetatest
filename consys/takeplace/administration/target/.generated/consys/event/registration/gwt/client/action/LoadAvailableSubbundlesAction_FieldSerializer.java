package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadAvailableSubbundlesAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction::bundleUuid = value;
  }-*/;
  
  private static native java.lang.String getManagedUserUuid(consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction::managedUserUuid;
  }-*/;
  
  private static native void setManagedUserUuid(consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction::managedUserUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction instance) throws SerializationException {
    setBundleUuid(instance, streamReader.readString());
    setManagedUserUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction instance) throws SerializationException {
    streamWriter.writeString(getBundleUuid(instance));
    streamWriter.writeString(getManagedUserUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction)object);
  }
  
}
