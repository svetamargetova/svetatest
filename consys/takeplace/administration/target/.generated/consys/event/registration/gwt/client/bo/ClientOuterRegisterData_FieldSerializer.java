package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientOuterRegisterData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCurrency(consys.event.registration.gwt.client.bo.ClientOuterRegisterData instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientOuterRegisterData::currency;
  }-*/;
  
  private static native void setCurrency(consys.event.registration.gwt.client.bo.ClientOuterRegisterData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientOuterRegisterData::currency = value;
  }-*/;
  
  private static native java.util.ArrayList getItems(consys.event.registration.gwt.client.bo.ClientOuterRegisterData instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientOuterRegisterData::items;
  }-*/;
  
  private static native void setItems(consys.event.registration.gwt.client.bo.ClientOuterRegisterData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientOuterRegisterData::items = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientOuterRegisterData instance) throws SerializationException {
    setCurrency(instance, streamReader.readString());
    setItems(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientOuterRegisterData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientOuterRegisterData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientOuterRegisterData instance) throws SerializationException {
    streamWriter.writeString(getCurrency(instance));
    streamWriter.writeObject(getItems(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientOuterRegisterData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientOuterRegisterData_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientOuterRegisterData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientOuterRegisterData_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientOuterRegisterData)object);
  }
  
}
