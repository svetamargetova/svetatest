package consys.event.registration.gwt.client.message;

public class EventRegistrationConstants_cs implements consys.event.registration.gwt.client.message.EventRegistrationConstants {
  
  public java.lang.String registrationModule_text_navigationModelParticipantList() {
    return "Seznam účastníků";
  }
  
  public java.lang.String registrationCycleList_action_addRC() {
    return "Přidat registrační cyklus";
  }
  
  public java.lang.String customQuestionItem_text_required() {
    return "Povinné";
  }
  
  public java.lang.String registrationList_action_all() {
    return "Vše";
  }
  
  public java.lang.String registrationParticipantInfoPanel_action_details() {
    return "Detaily";
  }
  
  public java.lang.String subbundleItem_text_vat() {
    return "DPH";
  }
  
  public java.lang.String registrationSettingsBundle_title() {
    return "Vstupenky";
  }
  
  public java.lang.String registrationOrderPanel_text_additionalProducts() {
    return "Další produkty";
  }
  
  public java.lang.String outerRegistrationForm_error_eventNotActivated() {
    return "Zaregistrovat se můžete až po aktivaci události. Obraťte se na organizátory akce.";
  }
  
  public java.lang.String const_exceptionTicketUniquity() {
    return "Vstupenka se stejným jménem již existuje.";
  }
  
  public java.lang.String outerRegistrationForm_error_userInfoNotEntered() {
    return "Nebyl zadán uživatelův účet";
  }
  
  public java.lang.String discountFilterList_action_all() {
    return "Vše";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_action_addField() {
    return "Nové volitelné políčko";
  }
  
  public java.lang.String const_exceptionCancelRegistrationNoRecords() {
    return "Není žádný záznam pro zrušení registrace. Prosím kontaktujte nás.";
  }
  
  public java.lang.String outerRegistrationForm_text_invoiceAddress() {
    return "Faktrurační adresa";
  }
  
  public java.lang.String buyAdditionProducts_button_order() {
    return "OBJEDNAT";
  }
  
  public java.lang.String registrationSettingsSubbundle_text_newTicketAccessory() {
    return "Nový doplněk vstupenky";
  }
  
  public java.lang.String outerRegisterContent_error_noRecorsForParameterValues() {
    return "Žádné záznamy pro zadané parametry";
  }
  
  public java.lang.String outerRegisterContent_text_alreadyRegistered() {
    return "Jste již zaregistrovaní v Takeplace?";
  }
  
  public java.lang.String registrationFormItem_exception_noRecordsForAction() {
    return "Není žádný záznam pro zvolenou možnost registrace. Prosím kontaktujte nás.";
  }
  
  public java.lang.String outerRegistrationForm_error_registrationIsClosed() {
    return "Registrace je uzavřena pro tento typ vstupenek.";
  }
  
  public java.lang.String registrationFormItem_text_registrationClosed() {
    return "Registrace uzavřena";
  }
  
  public java.lang.String discountListItem_text_code() {
    return "Kód";
  }
  
  public java.lang.String registrationFormRegistered_button_sendCoupon() {
    return "Poslat slevový kupón";
  }
  
  public java.lang.String discountFilterList_action_notUsed() {
    return "Nepoužité";
  }
  
  public java.lang.String confirmOrderDialog_text_notifyUser() {
    return "Upozornit uživatele";
  }
  
  public java.lang.String questionWidget_action_next() {
    return "Pokračovat";
  }
  
  public java.lang.String registrationList_title() {
    return "Seznam účastníků";
  }
  
  public java.lang.String orderControlPanel_action_downloadInvoice() {
    return "STÁHNOUT FAKTURU";
  }
  
  public java.lang.String registrationFormItem_discount_exception_CouponUsedException() {
    return "Slevový kupón má omezenou kapacitu, která již byla vyčerpána.";
  }
  
  public java.lang.String waitingOverview_cancel_registration_question_title() {
    return "Opravdu si přejete zrušit čekání?";
  }
  
  public java.lang.String const_exceptionTicketCapacity() {
    return "Registrace překročila volnou kapacitu vstupenek.";
  }
  
  public java.lang.String registrationPropertyPanel_property_allowRegisterAnotherParticipant() {
    return "Allow register another participant";
  }
  
  public java.lang.String const_exceptionRegistrationsInCycle() {
    return "Pro registrční cyklus existuje platná registrace.";
  }
  
  public java.lang.String discountListItem_text_discount() {
    return "Sleva";
  }
  
  public java.lang.String registrationAdditionalProducts_text_ifDisabled() {
    return "Další položky si můžete objednat až po zaplacení vaší vstupenky.";
  }
  
  public java.lang.String registrationList_text_paid() {
    return "Cena";
  }
  
  public java.lang.String registrationList_text_preregistered() {
    return "Předregistrovaný";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadProForma() {
    return "Stáhnout proforma fakturu";
  }
  
  public java.lang.String editRegistrationForm_text_note_placeholder() {
    return "Máte nejakou poznámku k tomuto účastníkovi? Napište si ji, poznámku účastník nevidí.";
  }
  
  public java.lang.String checkInList_action_name() {
    return "Jméno";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadInvoice() {
    return "Stáhnout vaši fakturu";
  }
  
  public java.lang.String registrationOrderPanel_text_invoice() {
    return "Faktura";
  }
  
  public java.lang.String discountListItem_text_validThrough() {
    return "Platný do";
  }
  
  public java.lang.String const_price() {
    return "Cena";
  }
  
  public java.lang.String registrationFormRegistered_button_cancelRegistration() {
    return "Zrušit registraci";
  }
  
  public java.lang.String registrationForm_progress_enjoyUpper() {
    return "UŽÍVEJ";
  }
  
  public java.lang.String editFormBundle_title() {
    return "Upravit vstupenku";
  }
  
  public java.lang.String outerRegisterContent_button_register() {
    return "REGISTRUJ";
  }
  
  public java.lang.String registrationAdditionalProducts_title() {
    return "Můžete si objednat další produkty";
  }
  
  public java.lang.String checkInList_action_filterChecked() {
    return "Checked";
  }
  
  public java.lang.String registrationSettingsSubbundle_text_pleaseAddTicketAccessory() {
    return "Začnete přidaním doplňků vstupenky.";
  }
  
  public java.lang.String checkInList_action_filterAll() {
    return "Vše";
  }
  
  public java.lang.String editFormDiscountCoupon_title() {
    return "Upravit slevový kupón";
  }
  
  public java.lang.String waitingOverview_button_cancelWaiting() {
    return "Zruš čekání";
  }
  
  public java.lang.String waitingOverview_title() {
    return "Registrace / Čekárna";
  }
  
  public java.lang.String outerRegistrationForm_text_completeYourProfile() {
    return "Doplňte váš profil";
  }
  
  public java.lang.String registrationModule_text_moduleRoleAdministrator() {
    return "Administrátor";
  }
  
  public java.lang.String registrationFormItem_discount_exception_CouponExpiredException() {
    return "Slevový kupón je prošlý.";
  }
  
  public java.lang.String outerRegisterContent_help1() {
    return "To complete your registration we need to know some more information about you. If you are <strong>already<strong> registred in Takeplace, click on <strong>\"Click here to log in ...\"</strong>.";
  }
  
  public java.lang.String registrationList_action_state() {
    return "Stav";
  }
  
  public java.lang.String discountListItem_text_group() {
    return "Skupinový";
  }
  
  public java.lang.String editFormBundle_form_b2bTicket() {
    return "Jedná se o B2B vstupenku";
  }
  
  public java.lang.String outerRegistrationForm_text_register() {
    return "Registrace";
  }
  
  public java.lang.String internalRegistrationDialog_action_register() {
    return "Zaregistruj účastníka";
  }
  
  public java.lang.String bundleItem_text_notUsed() {
    return "Nepoužito";
  }
  
  public java.lang.String editFormSubbundle_form_vatPercent() {
    return "Daň (procenta)";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_text_permittedMax5Questions() {
    return "Dohromady je povoleno maximálně pět políček";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipant() {
    return "Participant";
  }
  
  public java.lang.String bundleItem_text_used() {
    return "Použito";
  }
  
  public java.lang.String const_exceptionUserAlreadyRegistered() {
    return "Uživatel je již registrován.";
  }
  
  public java.lang.String editFormQuestion_title() {
    return "Upravit volitelné políčko";
  }
  
  public java.lang.String registerAnother_text_pleaseAddParticipant() {
    return "Začněte přidáním účastníka.";
  }
  
  public java.lang.String badgesSizeDialog_title() {
    return "Badges";
  }
  
  public java.lang.String discountListItem_text_capacity() {
    return "Kapacita";
  }
  
  public java.lang.String registrationFormRegistered_action_confirmOrder() {
    return "Potvrdit objednávku";
  }
  
  public java.lang.String userParticipantList_action_name() {
    return "Jméno";
  }
  
  public java.lang.String checkDialog_titleIn() {
    return "Check In";
  }
  
  public java.lang.String checkDialog_titleOut() {
    return "Uncheck";
  }
  
  public java.lang.String orderControlPanel_action_downloadTicket() {
    return "STÁHNOUT VSTUPENKU";
  }
  
  public java.lang.String registrationSettingsBundle_text_newTicket() {
    return "Nová vstupenka";
  }
  
  public java.lang.String registrationCycleList_text_pleaseAddRC() {
    return "Začnete přidaním registračních cyklů.";
  }
  
  public java.lang.String registrationFormItem_text_capacityFilled() {
    return "Kapacita je vyčerpana.";
  }
  
  public java.lang.String registrationForm_action_toWaiting() {
    return "Do čekárny";
  }
  
  public java.lang.String discountListItem_text_ticket() {
    return "Vstupenka / Doplněk";
  }
  
  public java.lang.String registrationList_text_state() {
    return "Stav";
  }
  
  public java.lang.String registrationSettingsBundle_action_addTicket() {
    return "Vstupenka";
  }
  
  public java.lang.String orderControlPanel_action_downloadProformaInvoice() {
    return "STÁHNOUT PROFORMA FAKTURU";
  }
  
  public java.lang.String registrationModule_text_navigationModelCheckInList() {
    return "Check In seznam";
  }
  
  public java.lang.String discountListItem_text_restriction() {
    return "Omezení";
  }
  
  public java.lang.String outerQuestions_error_loadQuestions() {
    return "Chyba při načítání otázek organizátora";
  }
  
  public java.lang.String addManagedUserUI_text_firstName() {
    return "Jméno";
  }
  
  public java.lang.String registrationModule_text_moduleName() {
    return "Registrace";
  }
  
  public java.lang.String createDiscountDialog_action_addRestriction() {
    return "Přidat konec platnosti kuponu";
  }
  
  public java.lang.String outerRegisterPanel_title() {
    return "Vítejte na palubě!";
  }
  
  public java.lang.String registrationModule_text_moduleCheckInDescription() {
    return "";
  }
  
  public java.lang.String bundleItem_text_discountTitle() {
    return "Slevové kupóny";
  }
  
  public java.lang.String outerRegisterContent_text_agreeing() {
    return "Kliknutím na 'REGISTRUJ' souhlasíte s <a href=\"http://static.takeplace.eu/GBC_Takeplace_en.pdf\" target=\"_blank\">Podmínkami používání</a>.";
  }
  
  public java.lang.String questionWidget_title() {
    return "Otázky organizátora";
  }
  
  public java.lang.String editFormRegistrationCycle_title() {
    return "Upravit registrační cyklus";
  }
  
  public java.lang.String waitingOverview_text_capacity() {
    return "Volná kapacita";
  }
  
  public java.lang.String outerRegisterContent_help4() {
    return "The password shall be at least eight characters long; please avoid common names and words. In the Payment profile section please specity payment details you wish to appear in your invoice.";
  }
  
  public java.lang.String editFormBundle_form_maxQuantity() {
    return "Max. kvantita";
  }
  
  public java.lang.String registrationForm_progress_payUpper() {
    return "ZAPLAŤ";
  }
  
  public java.lang.String registrationModule_text_moduleRoleAdministratorDescription() {
    return "Administrátor ma právo spravovat registrace.";
  }
  
  public java.lang.String checkInList_action_buttonChecked() {
    return "PŘIŠEL";
  }
  
  public java.lang.String registrationForm_text_insertDiscountCoupon() {
    return "Vložit slevový kupón";
  }
  
  public java.lang.String internalRegistration_title() {
    return "Interní registrace";
  }
  
  public java.lang.String registrationFormRegistered_cancel_order_question_title() {
    return "Jste si jisti, že chcete zrušit objednavku?";
  }
  
  public java.lang.String outerRegisterContent_help5() {
    return "The invoice name and address can be used for your TAX office for depreciation. Please insert the organization address in case of asking your organization for refunding.";
  }
  
  public java.lang.String registrationFormItem_text_attention() {
    return "Pozor";
  }
  
  public java.lang.String outerRegisterContent_action_clickToLogin() {
    return "Klikněte zde pro přihlášení ...";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadProFormaAdmin() {
    return "Stáhnout uživatelovu proforma fakturu";
  }
  
  public java.lang.String registrationPropertyPanel_property_visibleParticipants() {
    return "Viditelní účastníci";
  }
  
  public java.lang.String editRegistrationForm_text_special_wish() {
    return "Speciální přání:";
  }
  
  public java.lang.String internalRegistrationDialog_text_userInformation() {
    return "Informace o účastníkovi";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegistrationList() {
    return "Registrovaní účastníci";
  }
  
  public java.lang.String outerRegisterContent_action_clickToRegister() {
    return "Klikněte zde pro vytvoření účtu ...";
  }
  
  public java.lang.String registrationFormRegistered_text_registrationPaid() {
    return "Vaše registrace byla potvrzena.";
  }
  
  public java.lang.String registrationFormItem_action_showDetails() {
    return "Zobrazit detaily";
  }
  
  public java.lang.String const_registrationCycleNotOverlap() {
    return "Registrační cykly se nemohou překrývat.";
  }
  
  public java.lang.String registerAnother_text_canceled() {
    return "Zrušeno";
  }
  
  public java.lang.String const_exceptionCyclesIntersection() {
    return "Registrační cykly se překrývají.";
  }
  
  public java.lang.String editRegistrationForm_text_note() {
    return "Poznámka:";
  }
  
  public java.lang.String checkInList_title() {
    return "Check In seznam";
  }
  
  public java.lang.String registrationList_text_waiting() {
    return "Čekající";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegister() {
    return "Registrace";
  }
  
  public java.lang.String registrationFormItem_discount_exception_Common() {
    return "Nastala chyba při spracování slevového kódu.";
  }
  
  public java.lang.String discountListItem_text_notUsed() {
    return "Nepoužit.";
  }
  
  public java.lang.String checkDialog_text_all() {
    return "Vše";
  }
  
  public java.lang.String outerRegistrationForm_text_providedByTakeplace() {
    return "Registrace do této akce je poskytovana pomocí Takeplace.";
  }
  
  public java.lang.String discountFilterList_action_used() {
    return "Použité";
  }
  
  public java.lang.String registrationForm_progress_registerUpper() {
    return "ZAREGISTRUJ";
  }
  
  public java.lang.String registrationModule_text_moduleCheckIn() {
    return "";
  }
  
  public java.lang.String checkInList_action_filterNotChecked() {
    return "Not checked";
  }
  
  public java.lang.String registrationOrderPanel_text_total() {
    return "Celkem";
  }
  
  public java.lang.String addManagedUserUI_text_lastName() {
    return "Příjmení";
  }
  
  public java.lang.String outerRegisterContent_error_ticketFull() {
    return "Je nám líto, vybraná registrace má omezenou kapacitu a je již plná. Zkuste počkat, anebo si vyberte jinou variantu.";
  }
  
  public java.lang.String internalRegistrationDialog_text_mustSelectTicket() {
    return "Musíte vybrat tiket";
  }
  
  public java.lang.String registrationFormRegistered_cancel_registration_question_title() {
    return "Jste si jisti, že chcete zrušit registraci?";
  }
  
  public java.lang.String outerRegistrationForm_error_registrationIsClosedDue() {
    return "Registrace je uzavřena z důvodu omezené kapacity pro tento typ vstupenek.";
  }
  
  public java.lang.String outerLogin_action_iDontHaveAny() {
    return "... nemám ani jeden z těchto účtů.";
  }
  
  public java.lang.String registrationSettingsSubbundle_title() {
    return "Doplňky vstupenky";
  }
  
  public java.lang.String outerRegistrationFormV2_action_showDescription() {
    return "Zobrazit popis akce";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipantAdeptDescription() {
    return "Participant aspirants do not have their registration confirmed.";
  }
  
  public java.lang.String registrationFormRegistered_error_registrationCanceled() {
    return "Registrace zrušena.";
  }
  
  public java.lang.String couponListItem_error_noRecords() {
    return "Požadovaný slevový kupón nebyl nalezen.";
  }
  
  public java.lang.String discountInfoPanel_action_groupCoupon() {
    return "Skupinový kupón";
  }
  
  public java.lang.String addManagedUserUI_text_organization() {
    return "Organizace";
  }
  
  public java.lang.String registrationCycleList_title() {
    return "Registrační cykly";
  }
  
  public java.lang.String registrationFormItem_button_register() {
    return "Registrovat";
  }
  
  public java.lang.String outerRegistrationForm_text_invoiceTo() {
    return "Fakturováno na Organizaci / Jméno";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipantDescription() {
    return "Participants with confirmed redistration.";
  }
  
  public java.lang.String outerRegistrationForm_error_eventNotFound() {
    return "Akce nebyla nalezena";
  }
  
  public java.lang.String discountFilterList_action_individual() {
    return "Individuální";
  }
  
  public java.lang.String registrationSettingsBundle_text_pleaseAddTicket() {
    return "Začnete přidaním vstupenky.";
  }
  
  public java.lang.String customQuestionItem_text_question() {
    return "Název políčka";
  }
  
  public java.lang.String registrationForm_text_validated() {
    return "Validován";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_title() {
    return "Volitelná políčka registrace";
  }
  
  public java.lang.String couponListItem_error_alreadyUsed() {
    return "Slevový kupón byl již použit.";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipantAdept() {
    return "Participant aspirant";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegistrationSettings() {
    return "Nastavení Registrace";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadInvoiceAdmin() {
    return "Stáhnout uživatelovu fakturu";
  }
  
  public java.lang.String registrationFormRegistered_error_payTimeouted() {
    return "Čas na zaplacení vypršel.";
  }
  
  public java.lang.String confirmOrderDialog_title() {
    return "Potvrdit objednávku";
  }
  
  public java.lang.String outerRegisterContent_help2() {
    return "If you are new to Takeplace, please fill in the registration form. Requested fields are marked with red asterisks.";
  }
  
  public java.lang.String discountList_title() {
    return "Slevové kupóny";
  }
  
  public java.lang.String outerRegistrationForm_text_registerHelp() {
    return "Zaregistrujte se jedním účtem z těchto služeb nebo si vytvořte nový učet v Takeplace";
  }
  
  public java.lang.String outerRegisterContent_help3() {
    return "The contact e-mail shall be permanent as it will identify you throughout the events you attend or organize.";
  }
  
  public java.lang.String waitingOverview_text_noOpenRegistration() {
    return "Nejsou otevřené žádné registrace";
  }
  
  public java.lang.String userParticipantList_title() {
    return "Seznam účastníků";
  }
  
  public java.lang.String const_exceptionEventNotActivated() {
    return "Registrace se nezdařila, protože akce zatím nebyla aktivována.";
  }
  
  public java.lang.String discountInfoPanel_action_individualCoupon() {
    return "Individuální kupón";
  }
  
  public java.lang.String registrationSettingsSubbundle_action_addTicketAccessory() {
    return "Doplněk vstupenky";
  }
  
  public java.lang.String registerAnother_action_addManagedParticipant() {
    return "Přidat spravovaného účastníka";
  }
  
  public java.lang.String createDiscountDialog_title() {
    return "Vytvořit slevový kupón";
  }
  
  public java.lang.String registerAnother_action_registerParticipant() {
    return "Registruj účastníka";
  }
  
  public java.lang.String editFormBundle_form_b2bTicketAccessory() {
    return "Jedná se o B2B doplněk vstupenky";
  }
  
  public java.lang.String editFormBundle_form_quantity() {
    return "Kvantita";
  }
  
  public java.lang.String registerAnother_title() {
    return "Další registrace";
  }
  
  public java.lang.String registrationForm_title() {
    return "Registrace";
  }
  
  public java.lang.String discountListItem_text_individual() {
    return "Individuální";
  }
  
  public java.lang.String registrationFormItem_discount_exception_CouponInvalidException() {
    return "Vložili jste neplatný slevový kód.";
  }
  
  public java.lang.String outerRegisterPanel_text_beta() {
    return "We are running Takeplace in Beta, leniency towards occassional bugs is welcome";
  }
  
  public java.lang.String registrationList_action_confirm() {
    return "Confirm";
  }
  
  public java.lang.String bundleItem_text_coupons() {
    return "Kupónů";
  }
  
  public java.lang.String registrationModule_text_moduleRoleChairDescription() {
    return "Předseda registrace má povoleno nastavovat a spravovat registrace.";
  }
  
  public java.lang.String confirmOrderDialog_text_note() {
    return "Poznámka";
  }
  
  public java.lang.String editFormSubbundle_title() {
    return "Upravit doplněk vstupenky";
  }
  
  public java.lang.String registrationForm_text_noneTickets() {
    return "Nejsou k dispozici žádné vstupenky";
  }
  
  public java.lang.String checkInList_action_buttonUncheck() {
    return "Uncheck";
  }
  
  public java.lang.String registrationFormItem_text_notActive() {
    return "Neaktivní";
  }
  
  public java.lang.String registrationOrderPanel_text_dueDate() {
    return "Datum splatnosti:";
  }
  
  public java.lang.String registrationSettingsBundle_action_ticketAccessories() {
    return "Doplňky vstupenky";
  }
  
  public java.lang.String outerRegisterContent_text_notYetRegistered() {
    return "Ještě nejste zaregistrovaní v Takeplace?";
  }
  
  public java.lang.String registrationForm_text_pricing() {
    return "Ceny";
  }
  
  public java.lang.String registrationForm_text_enteredCode() {
    return "Vložit kód";
  }
  
  public java.lang.String discountListItem_text_unlimited() {
    return "Neomezena";
  }
  
  public java.lang.String registrationList_text_registered() {
    return "Registrovaný";
  }
  
  public java.lang.String outerRegistrationForm_text_invoiceAddressHelp() {
    return "Adresa, která bude uvedena na faktuře";
  }
  
  public java.lang.String registrationCycleList_text_newRC() {
    return "Nová registrační cyklus";
  }
  
  public java.lang.String registrationFormRegistered_button_cancelOrder() {
    return "Zrušit objednávku";
  }
  
  public java.lang.String registrationModule_text_moduleRoleChair() {
    return "Předseda Registrace";
  }
  
  public java.lang.String outerRegisterContent_help6() {
    return "If you have any question, please check the <a href=\"#FAQ\">faq</a> or contact our support at <a href=\"mailto:support@takeplace.eu\">support@takeplace.eu</a>.";
  }
  
  public java.lang.String checkInList_action_buttonCheckIn() {
    return "CHECK IN";
  }
  
  public java.lang.String const_registrationDate() {
    return "Datum registrace";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_text_pleaseAddField() {
    return "Začnete přidáním volitelného políčka";
  }
  
  public java.lang.String editRegistrationForm_text_special_wish_placeholder() {
    return "Máte nejaké speciální přání? Napište ji organizátorům.";
  }
  
  public java.lang.String discountFilterList_action_group() {
    return "Skupinové";
  }
  
  public java.lang.String internalRegistration_action_addNewRegistration() {
    return "Přidat novou registraci";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegistration() {
    return "Účastníci";
  }
  
  public java.lang.String registrationModule_text_navigationModelMyRegistration() {
    return "Moje Registrace";
  }
  
  public java.lang.String editFormBundle_form_capacity() {
    return "Kapacita";
  }
}
