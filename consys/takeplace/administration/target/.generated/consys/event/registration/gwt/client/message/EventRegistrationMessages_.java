package consys.event.registration.gwt.client.message;

public class EventRegistrationMessages_ implements consys.event.registration.gwt.client.message.EventRegistrationMessages {
  
  public java.lang.String registrationCycleList_cycle_exception_endSameAsStart(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("The beginning of registration cycle (").append(arg0).append(") can not be same as the end of it (").append(arg1).append("). If you want to have duration of registration cycle 1 minute, set the end of cycle to ").append(arg0).append(" +1 minute.").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_registrationDate(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Registration date:</b> ").append(arg0).toString();
  }
  
  public java.lang.String registrationDate(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Registration date: ").append(arg0).toString();
  }
  
  public java.lang.String registrationFormItem_text_registrationExpiresDays(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("The registration expires today.</b>").toString();
        break;
      case 1:  // =1
        returnVal = new java.lang.StringBuffer().append("The registration expires in <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" day.</b>").toString();
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("The registration expires in <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" days.</b>").toString();
  }
  
  public java.lang.String registrationForm_text_before(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("before ").append(arg0).append(" ").append(arg1).toString();
  }
  
  public java.lang.String registrationFormRegistered_text_successfulyRegisteredInfo(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("Payment for your ticket registration was not confirmed yet. Confirmation has to be done <span class=\"").append(arg0).append("\"> until ").append(arg1).append("</span> otherwise the order will be canceled.").toString();
  }
  
  public java.lang.String outerRegistrationForm_text_alreadyTakeplaceMember(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Already a Takeplace member? <a href=\"").append(arg0).append("\">Click here to log in.</a>").toString();
  }
  
  public java.lang.String discountListItem_text_showAll(int arg0) {
    return new java.lang.StringBuffer().append("Show all (").append(arg0).append(")").toString();
  }
  
  public java.lang.String outerRegisterPanel_text1(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("You are registering at <strong>").append(arg0).append("</strong> with Takeplace - your event cloud in online paradise. Takeplace makes your events run! ").toString();
  }
  
  public java.lang.String registrationCycleList_cycle_exception_endBeforeStart(java.lang.String arg0,java.lang.String arg1) {
    return new java.lang.StringBuffer().append("The end (").append(arg1).append(") of registration cycle have to be after the start (").append(arg0).append(").").toString();
  }
  
  public java.lang.String registrationFormItem_exception_noRecordsForAction(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("There is no discount code ").append(arg0).append(".").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_statusPaid(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Status: <span class=\"").append(arg0).append("\">confirmed</span></b>").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_statusApprove(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Status: <span class=\"").append(arg0).append("\">waiting to approve</span></b>").toString();
  }
  
  public java.lang.String registrationFormItem_text_registrationExpiresMinutes(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("The registration expires in less than a minute.</b>").toString();
        break;
      case 1:  // =1
        returnVal = new java.lang.StringBuffer().append("The registration expires in <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" minute.</b>").toString();
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("The registration expires in <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" minutes.</b>").toString();
  }
  
  public java.lang.String registrationFormItem_text_lastFreeCapacity(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule().select(arg0);
    switch (arg0) {
      case 1:  // =1
        returnVal = new java.lang.StringBuffer().append("Remaining <b>").append(arg0).append(" registration</b>.").toString();
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("Remaining <b>").append(arg0).append(" registrations</b>.").toString();
  }
  
  public java.lang.String registrationFormItem_text_registrationExpiresHours(int arg0) {
    java.lang.String returnVal = null;
    int arg0_form = new com.google.gwt.i18n.client.impl.plurals.DefaultRule().select(arg0);
    switch (arg0) {
      case 0:  // =0
        returnVal = new java.lang.StringBuffer().append("The registration expires in less than a hour.</b>").toString();
        break;
      case 1:  // =1
        returnVal = new java.lang.StringBuffer().append("The registration expires in <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" hour.</b>").toString();
        break;
    }
    if (returnVal != null) {
      return returnVal;
    }
    return new java.lang.StringBuffer().append("The registration expires in <b>").append(com.google.gwt.i18n.client.NumberFormat.getDecimalFormat().format(arg0)).append(" hours.</b>").toString();
  }
  
  public java.lang.String registrationOrderPanel_text_discountCode(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("<b>Discount code</b> (").append(arg0).append(")").toString();
  }
  
  public java.lang.String registrationFormItem_text_limitedCapacity(int arg0) {
    return new java.lang.StringBuffer().append("The capacity is limited to <b>").append(arg0).append("</b>.").toString();
  }
}
