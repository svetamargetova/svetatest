package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateRegistrationBundleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.registration.gwt.client.bo.ClientBundle getBundle(consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction::bundle;
  }-*/;
  
  private static native void setBundle(consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction instance, consys.event.registration.gwt.client.bo.ClientBundle value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction::bundle = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction instance) throws SerializationException {
    setBundle(instance, (consys.event.registration.gwt.client.bo.ClientBundle) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction instance) throws SerializationException {
    streamWriter.writeObject(getBundle(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction)object);
  }
  
}
