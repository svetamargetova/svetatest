package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadEditRegistrationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getRegistrationUuid(consys.event.registration.gwt.client.action.LoadEditRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadEditRegistrationAction::registrationUuid;
  }-*/;
  
  private static native void setRegistrationUuid(consys.event.registration.gwt.client.action.LoadEditRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadEditRegistrationAction::registrationUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.LoadEditRegistrationAction instance) throws SerializationException {
    setRegistrationUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.LoadEditRegistrationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.LoadEditRegistrationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.LoadEditRegistrationAction instance) throws SerializationException {
    streamWriter.writeString(getRegistrationUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.LoadEditRegistrationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadEditRegistrationAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.LoadEditRegistrationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadEditRegistrationAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.LoadEditRegistrationAction)object);
  }
  
}
