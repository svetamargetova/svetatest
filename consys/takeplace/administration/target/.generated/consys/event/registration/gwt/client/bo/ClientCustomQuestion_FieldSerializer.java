package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCustomQuestion_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getRequired(consys.event.registration.gwt.client.bo.ClientCustomQuestion instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCustomQuestion::required;
  }-*/;
  
  private static native void setRequired(consys.event.registration.gwt.client.bo.ClientCustomQuestion instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCustomQuestion::required = value;
  }-*/;
  
  private static native java.lang.String getText(consys.event.registration.gwt.client.bo.ClientCustomQuestion instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCustomQuestion::text;
  }-*/;
  
  private static native void setText(consys.event.registration.gwt.client.bo.ClientCustomQuestion instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCustomQuestion::text = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.bo.ClientCustomQuestion instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCustomQuestion::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.bo.ClientCustomQuestion instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCustomQuestion::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientCustomQuestion instance) throws SerializationException {
    setRequired(instance, streamReader.readBoolean());
    setText(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientCustomQuestion instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientCustomQuestion();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientCustomQuestion instance) throws SerializationException {
    streamWriter.writeBoolean(getRequired(instance));
    streamWriter.writeString(getText(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientCustomQuestion_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCustomQuestion_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientCustomQuestion)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCustomQuestion_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientCustomQuestion)object);
  }
  
}
