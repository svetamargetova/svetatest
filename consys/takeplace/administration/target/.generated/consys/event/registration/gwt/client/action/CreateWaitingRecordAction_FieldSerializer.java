package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateWaitingRecordAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getManagedUserUuid(consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateWaitingRecordAction::managedUserUuid;
  }-*/;
  
  private static native void setManagedUserUuid(consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateWaitingRecordAction::managedUserUuid = value;
  }-*/;
  
  private static native java.lang.String getProductUuid(consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateWaitingRecordAction::productUuid;
  }-*/;
  
  private static native void setProductUuid(consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateWaitingRecordAction::productUuid = value;
  }-*/;
  
  private static native int getQuantity(consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateWaitingRecordAction::quantity;
  }-*/;
  
  private static native void setQuantity(consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateWaitingRecordAction::quantity = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance) throws SerializationException {
    setManagedUserUuid(instance, streamReader.readString());
    setProductUuid(instance, streamReader.readString());
    setQuantity(instance, streamReader.readInt());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.CreateWaitingRecordAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.CreateWaitingRecordAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.CreateWaitingRecordAction instance) throws SerializationException {
    streamWriter.writeString(getManagedUserUuid(instance));
    streamWriter.writeString(getProductUuid(instance));
    streamWriter.writeInt(getQuantity(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.CreateWaitingRecordAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateWaitingRecordAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.CreateWaitingRecordAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateWaitingRecordAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.CreateWaitingRecordAction)object);
  }
  
}
