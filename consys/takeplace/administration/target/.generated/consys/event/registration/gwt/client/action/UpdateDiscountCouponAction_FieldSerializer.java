package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateDiscountCouponAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getCapacity(consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateDiscountCouponAction::capacity;
  }-*/;
  
  private static native void setCapacity(consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateDiscountCouponAction::capacity = value;
  }-*/;
  
  private static native java.util.Date getEndDate(consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateDiscountCouponAction::endDate;
  }-*/;
  
  private static native void setEndDate(consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateDiscountCouponAction::endDate = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateDiscountCouponAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateDiscountCouponAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance) throws SerializationException {
    setCapacity(instance, streamReader.readInt());
    setEndDate(instance, (java.util.Date) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.UpdateDiscountCouponAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.UpdateDiscountCouponAction instance) throws SerializationException {
    streamWriter.writeInt(getCapacity(instance));
    streamWriter.writeObject(getEndDate(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.UpdateDiscountCouponAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateDiscountCouponAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.UpdateDiscountCouponAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateDiscountCouponAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.UpdateDiscountCouponAction)object);
  }
  
}
