package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateInternalRegistrationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.HashMap getAnswers(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::answers;
  }-*/;
  
  private static native void setAnswers(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.util.HashMap value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::answers = value;
  }-*/;
  
  private static native java.lang.String getCustomerProfileUuid(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::customerProfileUuid;
  }-*/;
  
  private static native void setCustomerProfileUuid(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::customerProfileUuid = value;
  }-*/;
  
  private static native java.lang.String getCutomerUuid(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::cutomerUuid;
  }-*/;
  
  private static native void setCutomerUuid(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::cutomerUuid = value;
  }-*/;
  
  private static native java.lang.String getFirstName(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::firstName;
  }-*/;
  
  private static native void setFirstName(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::firstName = value;
  }-*/;
  
  private static native java.lang.String getLastName(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::lastName;
  }-*/;
  
  private static native void setLastName(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::lastName = value;
  }-*/;
  
  private static native java.lang.String getOrganization(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::organization;
  }-*/;
  
  private static native void setOrganization(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::organization = value;
  }-*/;
  
  private static native java.util.ArrayList getProducts(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::products;
  }-*/;
  
  private static native void setProducts(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::products = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientUserPaymentProfile getProfile(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::profile;
  }-*/;
  
  private static native void setProfile(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, consys.common.gwt.shared.bo.ClientUserPaymentProfile value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::profile = value;
  }-*/;
  
  private static native java.lang.String getUserEmail(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::userEmail;
  }-*/;
  
  private static native void setUserEmail(consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateInternalRegistrationAction::userEmail = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) throws SerializationException {
    setAnswers(instance, (java.util.HashMap) streamReader.readObject());
    setCustomerProfileUuid(instance, streamReader.readString());
    setCutomerUuid(instance, streamReader.readString());
    setFirstName(instance, streamReader.readString());
    setLastName(instance, streamReader.readString());
    setOrganization(instance, streamReader.readString());
    setProducts(instance, (java.util.ArrayList) streamReader.readObject());
    setProfile(instance, (consys.common.gwt.shared.bo.ClientUserPaymentProfile) streamReader.readObject());
    setUserEmail(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.CreateInternalRegistrationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.CreateInternalRegistrationAction instance) throws SerializationException {
    streamWriter.writeObject(getAnswers(instance));
    streamWriter.writeString(getCustomerProfileUuid(instance));
    streamWriter.writeString(getCutomerUuid(instance));
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getOrganization(instance));
    streamWriter.writeObject(getProducts(instance));
    streamWriter.writeObject(getProfile(instance));
    streamWriter.writeString(getUserEmail(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.CreateInternalRegistrationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateInternalRegistrationAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.CreateInternalRegistrationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateInternalRegistrationAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.CreateInternalRegistrationAction)object);
  }
  
}
