package consys.event.registration.gwt.client.utils.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ERBundle_cs_InlineClientBundleGenerator implements consys.event.registration.gwt.client.utils.css.ERBundle {
  private static ERBundle_cs_InlineClientBundleGenerator _instance0 = new ERBundle_cs_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new consys.event.registration.gwt.client.utils.css.ERCssResources() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GHAOYQ1EAB{margin-right:" + ("15px")  + ";margin-left:" + ("15px")  + ";width:" + ("420px")  + ";float:" + ("right")  + ";}.GHAOYQ1FAB{margin-left:" + ("15px")  + ";width:" + ("420px")  + ";float:" + ("right")  + ";}.GHAOYQ1GAB{width:" + ("170px")  + ";float:" + ("right")  + ";}.GHAOYQ1DAB{color:" + ("#b02424")  + ";}.GHAOYQ1IAB{margin:") + (("12px")  + ";}.GHAOYQ1HAB{margin:" + ("9px")  + ";}.GHAOYQ1OBB{margin-top:" + ("5px")  + ";margin-right:" + ("17px")  + ";}.GHAOYQ1K5{margin-top:" + ("5px")  + ";}.GHAOYQ1K5 .GHAOYQ1M5{float:" + ("right")  + ";width:" + ("70px")  + ";}.GHAOYQ1K5 .GHAOYQ1LO{width:" + ("330px")  + ";float:" + ("right")  + ";}.GHAOYQ1K5 .GHAOYQ1NO{width:" + ("350px")  + ";float:" + ("left") ) + (";}.GHAOYQ1K5 .GHAOYQ1L5{float:" + ("right")  + ";margin-bottom:" + ("20px")  + ";width:" + ("260px")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 .GHAOYQ1P4{color:" + ("#bababa")  + ";font-style:" + ("italic")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pName{font-size:" + ("17px")  + ";font-weight:" + ("bold")  + ";margin-left:" + ("20px")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pPosition{margin-bottom:" + ("10px")  + ";float:" + ("right")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pAt{margin:") + (("0"+ " " +"5px"+ " " +"10px"+ " " +"5px")  + ";float:" + ("right")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pOrganization{margin-bottom:" + ("10px")  + ";float:" + ("right")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 input{width:" + ("100%")  + ";margin-bottom:" + ("10px")  + ";}.GHAOYQ1K5 .GHAOYQ1DCB{margin-bottom:" + ("10px")  + ";width:" + ("350px")  + ";}.GHAOYQ1K5 .GHAOYQ1DCB textarea{margin-bottom:" + ("5px")  + ";}.GHAOYQ1K5 .GHAOYQ1DCB .gwt-Label{font-weight:" + ("bold")  + ";margin-bottom:" + ("5px") ) + (";}.GHAOYQ1PBB .GHAOYQ1IBB div{font-weight:" + ("bold")  + ";}.GHAOYQ1CBB .GHAOYQ1EBB{margin-top:" + ("3px")  + ";}.GHAOYQ1CBB .GHAOYQ1JBB{float:" + ("right")  + ";margin-left:" + ("10px")  + ";}.GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("530px")  + ";float:" + ("right")  + ";}.GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1NO{width:" + ("150px")  + ";float:" + ("left")  + ";text-align:" + ("left")  + ";}.GHAOYQ1EBB .GHAOYQ1PO{float:" + ("right")  + ";font-weight:") + (("bold")  + ";margin-top:" + ("3px")  + ";margin-left:" + ("10px")  + ";}.GHAOYQ1EBB .GHAOYQ1IO{margin-top:" + ("3px")  + ";}.GHAOYQ1EBB .GHAOYQ1FBB{text-align:" + ("left")  + ";float:" + ("left")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1EBB .GHAOYQ1GBB{float:" + ("right")  + ";margin-left:" + ("20px")  + ";}.GHAOYQ1EBB .GHAOYQ1OJ{top:" + ("-5px")  + ";}.GHAOYQ1KAB .GHAOYQ1IBB div{font-size:" + ("17px") ) + (";}.GHAOYQ1KAB .GHAOYQ1CBB .GHAOYQ1DBB,.GHAOYQ1KAB .GHAOYQ1CBB .GHAOYQ1BBB{margin-top:" + ("10px")  + ";}.GHAOYQ1PAB .GHAOYQ1CBB{margin-top:" + ("20px")  + ";}.GHAOYQ1PAB .GHAOYQ1ABB{float:" + ("right")  + ";margin-left:" + ("10px")  + ";}.GHAOYQ1PAB .GHAOYQ1IBB div{font-size:" + ("12px")  + ";}.GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1MO{color:" + ("#bababa")  + ";}.GHAOYQ1PAB .GHAOYQ1HBB,.GHAOYQ1PAB .GHAOYQ1CBB .GHAOYQ1EBB,.GHAOYQ1PAB .GHAOYQ1CBB .GHAOYQ1P5{margin-right:" + ("23px")  + ";}.GHAOYQ1PAB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("490px")  + ";}.GHAOYQ1JAB .GHAOYQ1KBB{cursor:" + ("pointer")  + ";}.GHAOYQ1JAB .GHAOYQ1NBB{padding:" + ("10px")  + ";}.GHAOYQ1LAB{margin-top:") + (("10px")  + ";margin-right:" + ("3px")  + ";width:" + ("530px")  + ";}.GHAOYQ1MAB{color:" + ("#888")  + ";white-space:" + ("nowrap")  + ";}.GHAOYQ1NAB{float:" + ("right")  + ";width:" + ("450px")  + ";}.GHAOYQ1OAB{float:" + ("right")  + ";margin-left:" + ("5px")  + ";color:" + ("#888")  + ";font-weight:" + ("bold") ) + (";}.GHAOYQ1JAB .GHAOYQ1JO,.GHAOYQ1M4 .GHAOYQ1KAB.GHAOYQ1JO{background-color:" + ("#f8f8f8")  + ";}.GHAOYQ1CCB{margin-right:" + ("23px")  + ";margin-top:" + ("5px")  + ";}.GHAOYQ1BCB{margin-right:" + ("23px")  + ";}.GHAOYQ1HCB{font-weight:" + ("bold")  + ";margin-bottom:" + ("15px")  + ";}.GHAOYQ1JCB{color:" + ("red")  + ";}.GHAOYQ1ICB{color:" + ("green")  + ";}.GHAOYQ1MBB .GHAOYQ1E5{float:" + ("right")  + ";font-weight:" + ("bold")  + ";margin-left:") + (("10px")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1MBB .GHAOYQ1D5{float:" + ("right")  + ";}.GHAOYQ1P5{margin-top:" + ("10px")  + ";}.GHAOYQ1P5 .GHAOYQ1BAB{float:" + ("right")  + ";font-weight:" + ("bold")  + ";margin-left:" + ("10px")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1P5 .GHAOYQ1AAB{float:" + ("right")  + ";}.GHAOYQ1P5 .GHAOYQ1CAB{float:" + ("left")  + ";font-weight:" + ("bold") ) + (";text-align:" + ("left")  + ";}.GHAOYQ1H4{font-size:" + ("16px")  + ";font-weight:" + ("bold")  + ";margin-right:" + ("20px")  + ";}.GHAOYQ1G4{margin-right:" + ("20px")  + ";}.GHAOYQ1F4,.GHAOYQ1B5 .GHAOYQ1K4{float:" + ("right")  + ";margin-left:" + ("20px")  + ";}.GHAOYQ1B5{margin-right:" + ("190px")  + ";}.GHAOYQ1B5 .GHAOYQ1L4{float:" + ("right")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1M4 .GHAOYQ1J5{margin:") + (("0"+ " " +"10px")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB{font-weight:" + ("bold")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1P5{padding-left:" + ("5px")  + ";margin-right:" + ("35px")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1IBB{padding-right:" + ("5px")  + ";padding-left:" + ("5px")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1IBB .GHAOYQ1JBB{width:" + ("20px")  + ";float:" + ("right")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1IBB .GHAOYQ1KBB{width:" + ("390px")  + ";float:" + ("right")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("420px") ) + (";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1P5{margin-right:" + ("55px")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1JBB{width:" + ("15px")  + ";float:" + ("right")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1KBB{width:" + ("360px")  + ";float:" + ("right")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1LBB.GHAOYQ1MO{color:" + ("#bababa")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("390px")  + ";}.GHAOYQ1M4 .GHAOYQ1K0{margin-right:" + ("190px")  + ";}.GHAOYQ1A5{float:" + ("left")  + ";margin-right:" + ("30px")  + ";font-size:") + (("16px")  + ";width:" + ("300px")  + ";text-align:" + ("left")  + ";}.GHAOYQ1C5{float:" + ("right")  + ";width:" + ("360px")  + ";}.GHAOYQ1GCB .GHAOYQ1LO{width:" + ("350px")  + ";float:" + ("right")  + ";}.GHAOYQ1GCB .GHAOYQ1LO.GHAOYQ1F5{width:" + ("330px")  + ";}.GHAOYQ1GCB .GHAOYQ1NO{width:" + ("330px")  + ";float:" + ("left")  + ";}.GHAOYQ1GCB .GHAOYQ1NO.GHAOYQ1ACB{width:" + ("350px") ) + (";}.GHAOYQ1GCB .GHAOYQ1LO .GHAOYQ1DCB{margin-top:" + ("10px")  + ";}.GHAOYQ1GCB .GHAOYQ1LO .GHAOYQ1DCB textarea{margin-bottom:" + ("5px")  + ";}.GHAOYQ1GCB .GHAOYQ1LO .GHAOYQ1DCB .gwt-Label{font-weight:" + ("bold")  + ";margin-bottom:" + ("5px")  + ";}.GHAOYQ1H5 .GHAOYQ1ECB{font-weight:" + ("bold")  + ";font-size:" + ("20px")  + ";margin:" + ("0"+ " " +"0"+ " " +"10px"+ " " +"10px")  + ";float:" + ("right")  + ";}.GHAOYQ1H5 .GHAOYQ1N4{display:" + ("block")  + ";margin:" + ("4px"+ " " +"0"+ " " +"10px"+ " " +"0")  + ";float:") + (("right")  + ";}.GHAOYQ1H5 .GHAOYQ1J4{margin-top:" + ("10px")  + ";}.GHAOYQ1H5 .GHAOYQ1J4 .GHAOYQ1I4{float:" + ("right")  + ";margin:" + ("3px"+ " " +"0"+ " " +"0"+ " " +"10px")  + ";}.GHAOYQ1H5 .GHAOYQ1J4 .gwt-Label{float:" + ("right")  + ";width:" + ("320px")  + ";}.GHAOYQ1H5 .GHAOYQ1J4 .GHAOYQ1O4{margin:" + ("0"+ " " +"15px"+ " " +"10px"+ " " +"0")  + ";font-size:" + ("10px")  + ";}.GHAOYQ1H5 .GHAOYQ1FCB{margin-bottom:" + ("10px")  + ";}.GHAOYQ1H5 .GHAOYQ1FCB .gwt-Label{font-size:" + ("26px")  + ";font-weight:" + ("bold") ) + (";}.GHAOYQ1G5 .GHAOYQ1GH .gwt-Label{width:" + ("240px")  + ";text-align:" + ("center")  + ";}.GHAOYQ1O5{margin-bottom:" + ("3px")  + ";}.GHAOYQ1N5{margin-bottom:" + ("4px")  + ";}")) : ((".GHAOYQ1EAB{margin-left:" + ("15px")  + ";margin-right:" + ("15px")  + ";width:" + ("420px")  + ";float:" + ("left")  + ";}.GHAOYQ1FAB{margin-right:" + ("15px")  + ";width:" + ("420px")  + ";float:" + ("left")  + ";}.GHAOYQ1GAB{width:" + ("170px")  + ";float:" + ("left")  + ";}.GHAOYQ1DAB{color:" + ("#b02424")  + ";}.GHAOYQ1IAB{margin:") + (("12px")  + ";}.GHAOYQ1HAB{margin:" + ("9px")  + ";}.GHAOYQ1OBB{margin-top:" + ("5px")  + ";margin-left:" + ("17px")  + ";}.GHAOYQ1K5{margin-top:" + ("5px")  + ";}.GHAOYQ1K5 .GHAOYQ1M5{float:" + ("left")  + ";width:" + ("70px")  + ";}.GHAOYQ1K5 .GHAOYQ1LO{width:" + ("330px")  + ";float:" + ("left")  + ";}.GHAOYQ1K5 .GHAOYQ1NO{width:" + ("350px")  + ";float:" + ("right") ) + (";}.GHAOYQ1K5 .GHAOYQ1L5{float:" + ("left")  + ";margin-bottom:" + ("20px")  + ";width:" + ("260px")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 .GHAOYQ1P4{color:" + ("#bababa")  + ";font-style:" + ("italic")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pName{font-size:" + ("17px")  + ";font-weight:" + ("bold")  + ";margin-right:" + ("20px")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pPosition{margin-bottom:" + ("10px")  + ";float:" + ("left")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pAt{margin:") + (("0"+ " " +"5px"+ " " +"10px"+ " " +"5px")  + ";float:" + ("left")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 #pOrganization{margin-bottom:" + ("10px")  + ";float:" + ("left")  + ";}.GHAOYQ1K5 .GHAOYQ1L5 input{width:" + ("100%")  + ";margin-bottom:" + ("10px")  + ";}.GHAOYQ1K5 .GHAOYQ1DCB{margin-bottom:" + ("10px")  + ";width:" + ("350px")  + ";}.GHAOYQ1K5 .GHAOYQ1DCB textarea{margin-bottom:" + ("5px")  + ";}.GHAOYQ1K5 .GHAOYQ1DCB .gwt-Label{font-weight:" + ("bold")  + ";margin-bottom:" + ("5px") ) + (";}.GHAOYQ1PBB .GHAOYQ1IBB div{font-weight:" + ("bold")  + ";}.GHAOYQ1CBB .GHAOYQ1EBB{margin-top:" + ("3px")  + ";}.GHAOYQ1CBB .GHAOYQ1JBB{float:" + ("left")  + ";margin-right:" + ("10px")  + ";}.GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("530px")  + ";float:" + ("left")  + ";}.GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1NO{width:" + ("150px")  + ";float:" + ("right")  + ";text-align:" + ("right")  + ";}.GHAOYQ1EBB .GHAOYQ1PO{float:" + ("left")  + ";font-weight:") + (("bold")  + ";margin-top:" + ("3px")  + ";margin-right:" + ("10px")  + ";}.GHAOYQ1EBB .GHAOYQ1IO{margin-top:" + ("3px")  + ";}.GHAOYQ1EBB .GHAOYQ1FBB{text-align:" + ("right")  + ";float:" + ("right")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1EBB .GHAOYQ1GBB{float:" + ("left")  + ";margin-right:" + ("20px")  + ";}.GHAOYQ1EBB .GHAOYQ1OJ{top:" + ("-5px")  + ";}.GHAOYQ1KAB .GHAOYQ1IBB div{font-size:" + ("17px") ) + (";}.GHAOYQ1KAB .GHAOYQ1CBB .GHAOYQ1DBB,.GHAOYQ1KAB .GHAOYQ1CBB .GHAOYQ1BBB{margin-top:" + ("10px")  + ";}.GHAOYQ1PAB .GHAOYQ1CBB{margin-top:" + ("20px")  + ";}.GHAOYQ1PAB .GHAOYQ1ABB{float:" + ("left")  + ";margin-right:" + ("10px")  + ";}.GHAOYQ1PAB .GHAOYQ1IBB div{font-size:" + ("12px")  + ";}.GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1MO{color:" + ("#bababa")  + ";}.GHAOYQ1PAB .GHAOYQ1HBB,.GHAOYQ1PAB .GHAOYQ1CBB .GHAOYQ1EBB,.GHAOYQ1PAB .GHAOYQ1CBB .GHAOYQ1P5{margin-left:" + ("23px")  + ";}.GHAOYQ1PAB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("490px")  + ";}.GHAOYQ1JAB .GHAOYQ1KBB{cursor:" + ("pointer")  + ";}.GHAOYQ1JAB .GHAOYQ1NBB{padding:" + ("10px")  + ";}.GHAOYQ1LAB{margin-top:") + (("10px")  + ";margin-left:" + ("3px")  + ";width:" + ("530px")  + ";}.GHAOYQ1MAB{color:" + ("#888")  + ";white-space:" + ("nowrap")  + ";}.GHAOYQ1NAB{float:" + ("left")  + ";width:" + ("450px")  + ";}.GHAOYQ1OAB{float:" + ("left")  + ";margin-right:" + ("5px")  + ";color:" + ("#888")  + ";font-weight:" + ("bold") ) + (";}.GHAOYQ1JAB .GHAOYQ1JO,.GHAOYQ1M4 .GHAOYQ1KAB.GHAOYQ1JO{background-color:" + ("#f8f8f8")  + ";}.GHAOYQ1CCB{margin-left:" + ("23px")  + ";margin-top:" + ("5px")  + ";}.GHAOYQ1BCB{margin-left:" + ("23px")  + ";}.GHAOYQ1HCB{font-weight:" + ("bold")  + ";margin-bottom:" + ("15px")  + ";}.GHAOYQ1JCB{color:" + ("red")  + ";}.GHAOYQ1ICB{color:" + ("green")  + ";}.GHAOYQ1MBB .GHAOYQ1E5{float:" + ("left")  + ";font-weight:" + ("bold")  + ";margin-right:") + (("10px")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1MBB .GHAOYQ1D5{float:" + ("left")  + ";}.GHAOYQ1P5{margin-top:" + ("10px")  + ";}.GHAOYQ1P5 .GHAOYQ1BAB{float:" + ("left")  + ";font-weight:" + ("bold")  + ";margin-right:" + ("10px")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1P5 .GHAOYQ1AAB{float:" + ("left")  + ";}.GHAOYQ1P5 .GHAOYQ1CAB{float:" + ("right")  + ";font-weight:" + ("bold") ) + (";text-align:" + ("right")  + ";}.GHAOYQ1H4{font-size:" + ("16px")  + ";font-weight:" + ("bold")  + ";margin-left:" + ("20px")  + ";}.GHAOYQ1G4{margin-left:" + ("20px")  + ";}.GHAOYQ1F4,.GHAOYQ1B5 .GHAOYQ1K4{float:" + ("left")  + ";margin-right:" + ("20px")  + ";}.GHAOYQ1B5{margin-left:" + ("190px")  + ";}.GHAOYQ1B5 .GHAOYQ1L4{float:" + ("left")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1M4 .GHAOYQ1J5{margin:") + (("0"+ " " +"10px")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB{font-weight:" + ("bold")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1P5{padding-right:" + ("5px")  + ";margin-left:" + ("35px")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1IBB{padding-left:" + ("5px")  + ";padding-right:" + ("5px")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1IBB .GHAOYQ1JBB{width:" + ("20px")  + ";float:" + ("left")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1IBB .GHAOYQ1KBB{width:" + ("390px")  + ";float:" + ("left")  + ";}.GHAOYQ1M4 .GHAOYQ1KAB .GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("420px") ) + (";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1P5{margin-left:" + ("55px")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1JBB{width:" + ("15px")  + ";float:" + ("left")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1KBB{width:" + ("360px")  + ";float:" + ("left")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1IBB .GHAOYQ1LBB.GHAOYQ1MO{color:" + ("#bababa")  + ";}.GHAOYQ1M4 .GHAOYQ1PAB .GHAOYQ1CBB .GHAOYQ1HBB .GHAOYQ1LO{width:" + ("390px")  + ";}.GHAOYQ1M4 .GHAOYQ1K0{margin-left:" + ("190px")  + ";}.GHAOYQ1A5{float:" + ("right")  + ";margin-left:" + ("30px")  + ";font-size:") + (("16px")  + ";width:" + ("300px")  + ";text-align:" + ("right")  + ";}.GHAOYQ1C5{float:" + ("left")  + ";width:" + ("360px")  + ";}.GHAOYQ1GCB .GHAOYQ1LO{width:" + ("350px")  + ";float:" + ("left")  + ";}.GHAOYQ1GCB .GHAOYQ1LO.GHAOYQ1F5{width:" + ("330px")  + ";}.GHAOYQ1GCB .GHAOYQ1NO{width:" + ("330px")  + ";float:" + ("right")  + ";}.GHAOYQ1GCB .GHAOYQ1NO.GHAOYQ1ACB{width:" + ("350px") ) + (";}.GHAOYQ1GCB .GHAOYQ1LO .GHAOYQ1DCB{margin-top:" + ("10px")  + ";}.GHAOYQ1GCB .GHAOYQ1LO .GHAOYQ1DCB textarea{margin-bottom:" + ("5px")  + ";}.GHAOYQ1GCB .GHAOYQ1LO .GHAOYQ1DCB .gwt-Label{font-weight:" + ("bold")  + ";margin-bottom:" + ("5px")  + ";}.GHAOYQ1H5 .GHAOYQ1ECB{font-weight:" + ("bold")  + ";font-size:" + ("20px")  + ";margin:" + ("0"+ " " +"10px"+ " " +"10px"+ " " +"0")  + ";float:" + ("left")  + ";}.GHAOYQ1H5 .GHAOYQ1N4{display:" + ("block")  + ";margin:" + ("4px"+ " " +"0"+ " " +"10px"+ " " +"0")  + ";float:") + (("left")  + ";}.GHAOYQ1H5 .GHAOYQ1J4{margin-top:" + ("10px")  + ";}.GHAOYQ1H5 .GHAOYQ1J4 .GHAOYQ1I4{float:" + ("left")  + ";margin:" + ("3px"+ " " +"10px"+ " " +"0"+ " " +"0")  + ";}.GHAOYQ1H5 .GHAOYQ1J4 .gwt-Label{float:" + ("left")  + ";width:" + ("320px")  + ";}.GHAOYQ1H5 .GHAOYQ1J4 .GHAOYQ1O4{margin:" + ("0"+ " " +"0"+ " " +"10px"+ " " +"15px")  + ";font-size:" + ("10px")  + ";}.GHAOYQ1H5 .GHAOYQ1FCB{margin-bottom:" + ("10px")  + ";}.GHAOYQ1H5 .GHAOYQ1FCB .gwt-Label{font-size:" + ("26px")  + ";font-weight:" + ("bold") ) + (";}.GHAOYQ1G5 .GHAOYQ1GH .gwt-Label{width:" + ("240px")  + ";text-align:" + ("center")  + ";}.GHAOYQ1O5{margin-bottom:" + ("3px")  + ";}.GHAOYQ1N5{margin-bottom:" + ("4px")  + ";}"));
      }
      public java.lang.String badgesSizeDialogSizeButton(){
        return "GHAOYQ1F4";
      }
      public java.lang.String badgesSizeDialogSizeButtonWrapper(){
        return "GHAOYQ1G4";
      }
      public java.lang.String badgesSizeDialogTitle(){
        return "GHAOYQ1H4";
      }
      public java.lang.String bulletItem(){
        return "GHAOYQ1I4";
      }
      public java.lang.String bulletItemPanel(){
        return "GHAOYQ1J4";
      }
      public java.lang.String button(){
        return "GHAOYQ1K4";
      }
      public java.lang.String cancel(){
        return "GHAOYQ1L4";
      }
      public java.lang.String contentPanel(){
        return "GHAOYQ1M4";
      }
      public java.lang.String detils(){
        return "GHAOYQ1N4";
      }
      public java.lang.String discount(){
        return "GHAOYQ1O4";
      }
      public java.lang.String incomplete(){
        return "GHAOYQ1P4";
      }
      public java.lang.String internalRegistrationBundleLabel(){
        return "GHAOYQ1A5";
      }
      public java.lang.String internalRegistrationDialog(){
        return "GHAOYQ1B5";
      }
      public java.lang.String internalRegistrationUserName(){
        return "GHAOYQ1C5";
      }
      public java.lang.String itemPanelBox(){
        return "GHAOYQ1D5";
      }
      public java.lang.String itemPanelLabel(){
        return "GHAOYQ1E5";
      }
      public java.lang.String leftColumnAdmin(){
        return "GHAOYQ1F5";
      }
      public java.lang.String orderControlPanel(){
        return "GHAOYQ1G5";
      }
      public java.lang.String orderDetailPanel(){
        return "GHAOYQ1H5";
      }
      public java.lang.String outerRegistrationForm(){
        return "GHAOYQ1I5";
      }
      public java.lang.String panel(){
        return "GHAOYQ1J5";
      }
      public java.lang.String participantInfoPanel(){
        return "GHAOYQ1K5";
      }
      public java.lang.String participantInfoPanelPersonal(){
        return "GHAOYQ1L5";
      }
      public java.lang.String participantInfoPanelPortrait(){
        return "GHAOYQ1M5";
      }
      public java.lang.String payButtonCard(){
        return "GHAOYQ1N5";
      }
      public java.lang.String payButtonPal(){
        return "GHAOYQ1O5";
      }
      public java.lang.String quantityPanel(){
        return "GHAOYQ1P5";
      }
      public java.lang.String quantityPanelBox(){
        return "GHAOYQ1AAB";
      }
      public java.lang.String quantityPanelLabel(){
        return "GHAOYQ1BAB";
      }
      public java.lang.String quantityPanelMultipleLabel(){
        return "GHAOYQ1CAB";
      }
      public java.lang.String registerAttention(){
        return "GHAOYQ1DAB";
      }
      public java.lang.String registerItemLeftPart(){
        return "GHAOYQ1EAB";
      }
      public java.lang.String registerItemLeftPartRegistered(){
        return "GHAOYQ1FAB";
      }
      public java.lang.String registerItemRightPart(){
        return "GHAOYQ1GAB";
      }
      public java.lang.String registerItemSelected(){
        return "GHAOYQ1HAB";
      }
      public java.lang.String registerItemUnselected(){
        return "GHAOYQ1IAB";
      }
      public java.lang.String registrationForm(){
        return "GHAOYQ1JAB";
      }
      public java.lang.String registrationFormItem(){
        return "GHAOYQ1KAB";
      }
      public java.lang.String registrationFormPricingInfo(){
        return "GHAOYQ1LAB";
      }
      public java.lang.String registrationFormPricingLabel(){
        return "GHAOYQ1MAB";
      }
      public java.lang.String registrationFormPricingPanel(){
        return "GHAOYQ1NAB";
      }
      public java.lang.String registrationFormPricingTitle(){
        return "GHAOYQ1OAB";
      }
      public java.lang.String registrationFormSubItem(){
        return "GHAOYQ1PAB";
      }
      public java.lang.String registrationFormSubItemAction(){
        return "GHAOYQ1ABB";
      }
      public java.lang.String registrationItemAlerts(){
        return "GHAOYQ1BBB";
      }
      public java.lang.String registrationItemContent(){
        return "GHAOYQ1CBB";
      }
      public java.lang.String registrationItemDescription(){
        return "GHAOYQ1DBB";
      }
      public java.lang.String registrationItemDiscountCoupon(){
        return "GHAOYQ1EBB";
      }
      public java.lang.String registrationItemDiscountCouponAmount(){
        return "GHAOYQ1FBB";
      }
      public java.lang.String registrationItemDiscountCouponInput(){
        return "GHAOYQ1GBB";
      }
      public java.lang.String registrationItemPricePanel(){
        return "GHAOYQ1HBB";
      }
      public java.lang.String registrationItemTitle(){
        return "GHAOYQ1IBB";
      }
      public java.lang.String registrationItemTitleAction(){
        return "GHAOYQ1JBB";
      }
      public java.lang.String registrationItemTitleLabel(){
        return "GHAOYQ1KBB";
      }
      public java.lang.String registrationItemTitlePrice(){
        return "GHAOYQ1LBB";
      }
      public java.lang.String registrationOptionItemPanel(){
        return "GHAOYQ1MBB";
      }
      public java.lang.String registrationOrderPanel(){
        return "GHAOYQ1NBB";
      }
      public java.lang.String registrationOrderPanelDiscountText(){
        return "GHAOYQ1OBB";
      }
      public java.lang.String registrationOrderPanelWrapper(){
        return "GHAOYQ1PBB";
      }
      public java.lang.String rightColumnAdmin(){
        return "GHAOYQ1ACB";
      }
      public java.lang.String subbundlePanelCapacity(){
        return "GHAOYQ1BCB";
      }
      public java.lang.String subbundlePanelDiscount(){
        return "GHAOYQ1CCB";
      }
      public java.lang.String textAreaUpdateBox(){
        return "GHAOYQ1DCB";
      }
      public java.lang.String title(){
        return "GHAOYQ1ECB";
      }
      public java.lang.String totalPrice(){
        return "GHAOYQ1FCB";
      }
      public java.lang.String userAnswersAndWish(){
        return "GHAOYQ1GCB";
      }
      public java.lang.String waitingCapacity(){
        return "GHAOYQ1HCB";
      }
      public java.lang.String waitingCapacityFree(){
        return "GHAOYQ1ICB";
      }
      public java.lang.String waitingCapacityFull(){
        return "GHAOYQ1JCB";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.event.registration.gwt.client.utils.css.ERCssResources get() {
      return css;
    }
  }
  public consys.event.registration.gwt.client.utils.css.ERCssResources css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.event.registration.gwt.client.utils.css.ERCssResources css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@consys.event.registration.gwt.client.utils.css.ERBundle::css()();
    }
    return null;
  }-*/;
}
