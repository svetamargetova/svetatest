package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistrationOrder_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getDueDate(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::dueDate;
  }-*/;
  
  private static native void setDueDate(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::dueDate = value;
  }-*/;
  
  private static native java.util.Date getOrderDate(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::orderDate;
  }-*/;
  
  private static native void setOrderDate(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::orderDate = value;
  }-*/;
  
  private static native java.util.ArrayList getOrderItems(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::orderItems;
  }-*/;
  
  private static native void setOrderItems(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::orderItems = value;
  }-*/;
  
  private static native java.util.Date getPaidDate(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::paidDate;
  }-*/;
  
  private static native void setPaidDate(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::paidDate = value;
  }-*/;
  
  private static native consys.event.registration.gwt.client.bo.ClientRegistration.State getState(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::state;
  }-*/;
  
  private static native void setState(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance, consys.event.registration.gwt.client.bo.ClientRegistration.State value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::state = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getTotalPrice(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::totalPrice;
  }-*/;
  
  private static native void setTotalPrice(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::totalPrice = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrder::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) throws SerializationException {
    setDueDate(instance, (java.util.Date) streamReader.readObject());
    setOrderDate(instance, (java.util.Date) streamReader.readObject());
    setOrderItems(instance, (java.util.ArrayList) streamReader.readObject());
    setPaidDate(instance, (java.util.Date) streamReader.readObject());
    setState(instance, (consys.event.registration.gwt.client.bo.ClientRegistration.State) streamReader.readObject());
    setTotalPrice(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientRegistrationOrder instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientRegistrationOrder();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientRegistrationOrder instance) throws SerializationException {
    streamWriter.writeObject(getDueDate(instance));
    streamWriter.writeObject(getOrderDate(instance));
    streamWriter.writeObject(getOrderItems(instance));
    streamWriter.writeObject(getPaidDate(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeObject(getTotalPrice(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientRegistrationOrder_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationOrder_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientRegistrationOrder)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationOrder_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientRegistrationOrder)object);
  }
  
}
