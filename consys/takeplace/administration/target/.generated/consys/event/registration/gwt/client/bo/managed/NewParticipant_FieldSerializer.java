package consys.event.registration.gwt.client.bo.managed;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class NewParticipant_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFirstName(consys.event.registration.gwt.client.bo.managed.NewParticipant instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.managed.NewParticipant::firstName;
  }-*/;
  
  private static native void setFirstName(consys.event.registration.gwt.client.bo.managed.NewParticipant instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.managed.NewParticipant::firstName = value;
  }-*/;
  
  private static native java.lang.String getLastName(consys.event.registration.gwt.client.bo.managed.NewParticipant instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.managed.NewParticipant::lastName;
  }-*/;
  
  private static native void setLastName(consys.event.registration.gwt.client.bo.managed.NewParticipant instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.managed.NewParticipant::lastName = value;
  }-*/;
  
  private static native java.lang.String getOrganization(consys.event.registration.gwt.client.bo.managed.NewParticipant instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.managed.NewParticipant::organization;
  }-*/;
  
  private static native void setOrganization(consys.event.registration.gwt.client.bo.managed.NewParticipant instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.managed.NewParticipant::organization = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.managed.NewParticipant instance) throws SerializationException {
    setFirstName(instance, streamReader.readString());
    setLastName(instance, streamReader.readString());
    setOrganization(instance, streamReader.readString());
    
  }
  
  public static consys.event.registration.gwt.client.bo.managed.NewParticipant instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.managed.NewParticipant();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.managed.NewParticipant instance) throws SerializationException {
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getOrganization(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.managed.NewParticipant_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.managed.NewParticipant_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.managed.NewParticipant)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.managed.NewParticipant_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.managed.NewParticipant)object);
  }
  
}
