package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class SendDiscountCodeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCode(consys.event.registration.gwt.client.action.SendDiscountCodeAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.SendDiscountCodeAction::code;
  }-*/;
  
  private static native void setCode(consys.event.registration.gwt.client.action.SendDiscountCodeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.SendDiscountCodeAction::code = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.SendDiscountCodeAction instance) throws SerializationException {
    setCode(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.SendDiscountCodeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.SendDiscountCodeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.SendDiscountCodeAction instance) throws SerializationException {
    streamWriter.writeString(getCode(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.SendDiscountCodeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.SendDiscountCodeAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.SendDiscountCodeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.SendDiscountCodeAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.SendDiscountCodeAction)object);
  }
  
}
