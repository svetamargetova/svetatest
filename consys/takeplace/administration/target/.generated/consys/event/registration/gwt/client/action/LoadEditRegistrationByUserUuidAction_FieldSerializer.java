package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadEditRegistrationByUserUuidAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUserUuid(consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction::userUuid;
  }-*/;
  
  private static native void setUserUuid(consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction::userUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction instance) throws SerializationException {
    setUserUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction instance) throws SerializationException {
    streamWriter.writeString(getUserUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction)object);
  }
  
}
