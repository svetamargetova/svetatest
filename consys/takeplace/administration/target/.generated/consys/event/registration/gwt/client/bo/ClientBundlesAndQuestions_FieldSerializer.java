package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientBundlesAndQuestions_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getBundles(consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions::bundles;
  }-*/;
  
  private static native void setBundles(consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions::bundles = value;
  }-*/;
  
  private static native java.util.ArrayList getQuestions(consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions::questions;
  }-*/;
  
  private static native void setQuestions(consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions::questions = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions instance) throws SerializationException {
    setBundles(instance, (java.util.ArrayList) streamReader.readObject());
    setQuestions(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions instance) throws SerializationException {
    streamWriter.writeObject(getBundles(instance));
    streamWriter.writeObject(getQuestions(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions)object);
  }
  
}
