package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteRegistrationBundleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction)object);
  }
  
}
