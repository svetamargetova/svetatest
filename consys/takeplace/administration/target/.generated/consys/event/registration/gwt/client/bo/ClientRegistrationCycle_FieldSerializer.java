package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistrationCycle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getFrom(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::from;
  }-*/;
  
  private static native void setFrom(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::from = value;
  }-*/;
  
  private static native java.util.Date getTo(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::to;
  }-*/;
  
  private static native void setTo(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::to = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::uuid = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getValue(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::value;
  }-*/;
  
  private static native void setValue(consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationCycle::value = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance) throws SerializationException {
    setFrom(instance, (java.util.Date) streamReader.readObject());
    setTo(instance, (java.util.Date) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    setValue(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientRegistrationCycle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientRegistrationCycle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientRegistrationCycle instance) throws SerializationException {
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeObject(getTo(instance));
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeObject(getValue(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientRegistrationCycle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationCycle_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientRegistrationCycle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationCycle_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientRegistrationCycle)object);
  }
  
}
