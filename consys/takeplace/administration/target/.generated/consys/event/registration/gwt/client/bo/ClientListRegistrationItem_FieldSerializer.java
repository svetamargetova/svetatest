package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientListRegistrationItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleName(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::bundleName;
  }-*/;
  
  private static native void setBundleName(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::bundleName = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getPaid(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::paid;
  }-*/;
  
  private static native void setPaid(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::paid = value;
  }-*/;
  
  private static native int getQuantity(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::quantity;
  }-*/;
  
  private static native void setQuantity(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::quantity = value;
  }-*/;
  
  private static native java.util.Date getRegistrationDate(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::registrationDate;
  }-*/;
  
  private static native void setRegistrationDate(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::registrationDate = value;
  }-*/;
  
  private static native consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState getState(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::state;
  }-*/;
  
  private static native void setState(consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance, consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListRegistrationItem::state = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance) throws SerializationException {
    setBundleName(instance, streamReader.readString());
    setPaid(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setQuantity(instance, streamReader.readInt());
    setRegistrationDate(instance, (java.util.Date) streamReader.readObject());
    setState(instance, (consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState) streamReader.readObject());
    
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.bo.ClientListRegistrationItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientListRegistrationItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientListRegistrationItem instance) throws SerializationException {
    streamWriter.writeString(getBundleName(instance));
    streamWriter.writeObject(getPaid(instance));
    streamWriter.writeInt(getQuantity(instance));
    streamWriter.writeObject(getRegistrationDate(instance));
    streamWriter.writeObject(getState(instance));
    
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientListRegistrationItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientListRegistrationItem_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientListRegistrationItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientListRegistrationItem_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientListRegistrationItem)object);
  }
  
}
