package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadProductDiscountAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDiscountCode(consys.event.registration.gwt.client.action.LoadProductDiscountAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadProductDiscountAction::discountCode;
  }-*/;
  
  private static native void setDiscountCode(consys.event.registration.gwt.client.action.LoadProductDiscountAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadProductDiscountAction::discountCode = value;
  }-*/;
  
  private static native java.lang.String getProductUuid(consys.event.registration.gwt.client.action.LoadProductDiscountAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadProductDiscountAction::productUuid;
  }-*/;
  
  private static native void setProductUuid(consys.event.registration.gwt.client.action.LoadProductDiscountAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadProductDiscountAction::productUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.LoadProductDiscountAction instance) throws SerializationException {
    setDiscountCode(instance, streamReader.readString());
    setProductUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.LoadProductDiscountAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.LoadProductDiscountAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.LoadProductDiscountAction instance) throws SerializationException {
    streamWriter.writeString(getDiscountCode(instance));
    streamWriter.writeString(getProductUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.LoadProductDiscountAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadProductDiscountAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.LoadProductDiscountAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadProductDiscountAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.LoadProductDiscountAction)object);
  }
  
}
