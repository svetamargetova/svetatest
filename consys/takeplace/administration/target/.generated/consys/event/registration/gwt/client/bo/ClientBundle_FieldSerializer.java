package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientBundle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getB2BTicket(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::b2BTicket;
  }-*/;
  
  private static native void setB2BTicket(consys.event.registration.gwt.client.bo.ClientBundle instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::b2BTicket = value;
  }-*/;
  
  private static native int getCapacity(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::capacity;
  }-*/;
  
  private static native void setCapacity(consys.event.registration.gwt.client.bo.ClientBundle instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::capacity = value;
  }-*/;
  
  private static native boolean getCodeEnter(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::codeEnter;
  }-*/;
  
  private static native void setCodeEnter(consys.event.registration.gwt.client.bo.ClientBundle instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::codeEnter = value;
  }-*/;
  
  private static native java.util.ArrayList getCycles(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::cycles;
  }-*/;
  
  private static native void setCycles(consys.event.registration.gwt.client.bo.ClientBundle instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::cycles = value;
  }-*/;
  
  private static native java.lang.String getDescription(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::description;
  }-*/;
  
  private static native void setDescription(consys.event.registration.gwt.client.bo.ClientBundle instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::description = value;
  }-*/;
  
  private static native int getDiscountCodes(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::discountCodes;
  }-*/;
  
  private static native void setDiscountCodes(consys.event.registration.gwt.client.bo.ClientBundle instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::discountCodes = value;
  }-*/;
  
  private static native int getDiscountUsed(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::discountUsed;
  }-*/;
  
  private static native void setDiscountUsed(consys.event.registration.gwt.client.bo.ClientBundle instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::discountUsed = value;
  }-*/;
  
  private static native int getQuantity(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::quantity;
  }-*/;
  
  private static native void setQuantity(consys.event.registration.gwt.client.bo.ClientBundle instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::quantity = value;
  }-*/;
  
  private static native double getVat(consys.event.registration.gwt.client.bo.ClientBundle instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundle::vat;
  }-*/;
  
  private static native void setVat(consys.event.registration.gwt.client.bo.ClientBundle instance, double value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundle::vat = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientBundle instance) throws SerializationException {
    setB2BTicket(instance, streamReader.readBoolean());
    setCapacity(instance, streamReader.readInt());
    setCodeEnter(instance, streamReader.readBoolean());
    setCycles(instance, (java.util.ArrayList) streamReader.readObject());
    setDescription(instance, streamReader.readString());
    setDiscountCodes(instance, streamReader.readInt());
    setDiscountUsed(instance, streamReader.readInt());
    setQuantity(instance, streamReader.readInt());
    setVat(instance, streamReader.readDouble());
    
    consys.event.registration.gwt.client.bo.ClientBundleThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.bo.ClientBundle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientBundle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientBundle instance) throws SerializationException {
    streamWriter.writeBoolean(getB2BTicket(instance));
    streamWriter.writeInt(getCapacity(instance));
    streamWriter.writeBoolean(getCodeEnter(instance));
    streamWriter.writeObject(getCycles(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeInt(getDiscountCodes(instance));
    streamWriter.writeInt(getDiscountUsed(instance));
    streamWriter.writeInt(getQuantity(instance));
    streamWriter.writeDouble(getVat(instance));
    
    consys.event.registration.gwt.client.bo.ClientBundleThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientBundle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundle_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientBundle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundle_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientBundle)object);
  }
  
}
