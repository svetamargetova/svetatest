package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateCustomQuestionOrderAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getNewPosition(consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction::newPosition;
  }-*/;
  
  private static native void setNewPosition(consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction::newPosition = value;
  }-*/;
  
  private static native int getOldPosition(consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction::oldPosition;
  }-*/;
  
  private static native void setOldPosition(consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction::oldPosition = value;
  }-*/;
  
  private static native java.lang.String getQuestionUuid(consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction::questionUuid;
  }-*/;
  
  private static native void setQuestionUuid(consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction::questionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance) throws SerializationException {
    setNewPosition(instance, streamReader.readInt());
    setOldPosition(instance, streamReader.readInt());
    setQuestionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction instance) throws SerializationException {
    streamWriter.writeInt(getNewPosition(instance));
    streamWriter.writeInt(getOldPosition(instance));
    streamWriter.writeString(getQuestionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction)object);
  }
  
}
