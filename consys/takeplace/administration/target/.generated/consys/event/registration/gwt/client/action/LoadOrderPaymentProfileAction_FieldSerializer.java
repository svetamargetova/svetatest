package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadOrderPaymentProfileAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getOrderUuid(consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction::orderUuid;
  }-*/;
  
  private static native void setOrderUuid(consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction::orderUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction instance) throws SerializationException {
    setOrderUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction instance) throws SerializationException {
    streamWriter.writeString(getOrderUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction)object);
  }
  
}
