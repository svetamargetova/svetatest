package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientBundlePriceSummary_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.Monetary getPrice(consys.event.registration.gwt.client.bo.ClientBundlePriceSummary instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundlePriceSummary::price;
  }-*/;
  
  private static native void setPrice(consys.event.registration.gwt.client.bo.ClientBundlePriceSummary instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundlePriceSummary::price = value;
  }-*/;
  
  private static native java.util.Date getTo(consys.event.registration.gwt.client.bo.ClientBundlePriceSummary instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientBundlePriceSummary::to;
  }-*/;
  
  private static native void setTo(consys.event.registration.gwt.client.bo.ClientBundlePriceSummary instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientBundlePriceSummary::to = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientBundlePriceSummary instance) throws SerializationException {
    setPrice(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setTo(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientBundlePriceSummary instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientBundlePriceSummary();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientBundlePriceSummary instance) throws SerializationException {
    streamWriter.writeObject(getPrice(instance));
    streamWriter.writeObject(getTo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientBundlePriceSummary_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundlePriceSummary_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientBundlePriceSummary)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientBundlePriceSummary_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientBundlePriceSummary)object);
  }
  
}
