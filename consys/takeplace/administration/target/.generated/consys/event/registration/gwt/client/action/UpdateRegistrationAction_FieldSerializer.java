package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateRegistrationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getAdmin(consys.event.registration.gwt.client.action.UpdateRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateRegistrationAction::admin;
  }-*/;
  
  private static native void setAdmin(consys.event.registration.gwt.client.action.UpdateRegistrationAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateRegistrationAction::admin = value;
  }-*/;
  
  private static native java.lang.String getNote(consys.event.registration.gwt.client.action.UpdateRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateRegistrationAction::note;
  }-*/;
  
  private static native void setNote(consys.event.registration.gwt.client.action.UpdateRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateRegistrationAction::note = value;
  }-*/;
  
  private static native java.lang.String getRegistrationUuid(consys.event.registration.gwt.client.action.UpdateRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateRegistrationAction::registrationUuid;
  }-*/;
  
  private static native void setRegistrationUuid(consys.event.registration.gwt.client.action.UpdateRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateRegistrationAction::registrationUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.UpdateRegistrationAction instance) throws SerializationException {
    setAdmin(instance, streamReader.readBoolean());
    setNote(instance, streamReader.readString());
    setRegistrationUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.UpdateRegistrationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.UpdateRegistrationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.UpdateRegistrationAction instance) throws SerializationException {
    streamWriter.writeBoolean(getAdmin(instance));
    streamWriter.writeString(getNote(instance));
    streamWriter.writeString(getRegistrationUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.UpdateRegistrationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateRegistrationAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.UpdateRegistrationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateRegistrationAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.UpdateRegistrationAction)object);
  }
  
}
