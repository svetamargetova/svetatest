package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUserRegistrationDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getPacks(consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails::packs;
  }-*/;
  
  private static native void setPacks(consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails::packs = value;
  }-*/;
  
  private static native java.util.HashMap getPricing(consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails::pricing;
  }-*/;
  
  private static native void setPricing(consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails instance, java.util.HashMap value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails::pricing = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails instance) throws SerializationException {
    setPacks(instance, (java.util.ArrayList) streamReader.readObject());
    setPricing(instance, (java.util.HashMap) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails instance) throws SerializationException {
    streamWriter.writeObject(getPacks(instance));
    streamWriter.writeObject(getPricing(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientUserRegistrationDetails)object);
  }
  
}
