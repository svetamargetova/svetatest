package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCouponListItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.bo.ClientCouponListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.bo.ClientCouponListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::bundleUuid = value;
  }-*/;
  
  private static native int getCapacity(consys.event.registration.gwt.client.bo.ClientCouponListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::capacity;
  }-*/;
  
  private static native void setCapacity(consys.event.registration.gwt.client.bo.ClientCouponListItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::capacity = value;
  }-*/;
  
  private static native int getDiscount(consys.event.registration.gwt.client.bo.ClientCouponListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::discount;
  }-*/;
  
  private static native void setDiscount(consys.event.registration.gwt.client.bo.ClientCouponListItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::discount = value;
  }-*/;
  
  private static native java.util.Date getEndDate(consys.event.registration.gwt.client.bo.ClientCouponListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::endDate;
  }-*/;
  
  private static native void setEndDate(consys.event.registration.gwt.client.bo.ClientCouponListItem instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::endDate = value;
  }-*/;
  
  private static native java.lang.String getKey(consys.event.registration.gwt.client.bo.ClientCouponListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::key;
  }-*/;
  
  private static native void setKey(consys.event.registration.gwt.client.bo.ClientCouponListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::key = value;
  }-*/;
  
  private static native int getUsedCount(consys.event.registration.gwt.client.bo.ClientCouponListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::usedCount;
  }-*/;
  
  private static native void setUsedCount(consys.event.registration.gwt.client.bo.ClientCouponListItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::usedCount = value;
  }-*/;
  
  private static native java.util.ArrayList getUsers(consys.event.registration.gwt.client.bo.ClientCouponListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::users;
  }-*/;
  
  private static native void setUsers(consys.event.registration.gwt.client.bo.ClientCouponListItem instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCouponListItem::users = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientCouponListItem instance) throws SerializationException {
    setBundleUuid(instance, streamReader.readString());
    setCapacity(instance, streamReader.readInt());
    setDiscount(instance, streamReader.readInt());
    setEndDate(instance, (java.util.Date) streamReader.readObject());
    setKey(instance, streamReader.readString());
    setUsedCount(instance, streamReader.readInt());
    setUsers(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.bo.ClientCouponListItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientCouponListItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientCouponListItem instance) throws SerializationException {
    streamWriter.writeString(getBundleUuid(instance));
    streamWriter.writeInt(getCapacity(instance));
    streamWriter.writeInt(getDiscount(instance));
    streamWriter.writeObject(getEndDate(instance));
    streamWriter.writeString(getKey(instance));
    streamWriter.writeInt(getUsedCount(instance));
    streamWriter.writeObject(getUsers(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientCouponListItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCouponListItem_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientCouponListItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCouponListItem_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientCouponListItem)object);
  }
  
}
