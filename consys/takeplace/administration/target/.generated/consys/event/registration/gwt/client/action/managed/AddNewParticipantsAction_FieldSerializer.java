package consys.event.registration.gwt.client.action.managed;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class AddNewParticipantsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getParticipants(consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction::participants;
  }-*/;
  
  private static native void setParticipants(consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction::participants = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction instance) throws SerializationException {
    setParticipants(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction instance) throws SerializationException {
    streamWriter.writeObject(getParticipants(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction)object);
  }
  
}
