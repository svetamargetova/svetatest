package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadRegisterUserPaymentDialogAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getManagedUserUuid(consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction::managedUserUuid;
  }-*/;
  
  private static native void setManagedUserUuid(consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction::managedUserUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction instance) throws SerializationException {
    setManagedUserUuid(instance, streamReader.readString());
    
    consys.event.common.gwt.client.action.ListPaymentDataDialogAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction instance) throws SerializationException {
    streamWriter.writeString(getManagedUserUuid(instance));
    
    consys.event.common.gwt.client.action.ListPaymentDataDialogAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction)object);
  }
  
}
