package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientListRegistrationItem_ClientRegistrationState_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState[] values = consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientListRegistrationItem_ClientRegistrationState_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientListRegistrationItem_ClientRegistrationState_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientListRegistrationItem_ClientRegistrationState_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState)object);
  }
  
}
