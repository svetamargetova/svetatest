package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCreateQuestion_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getName(consys.event.registration.gwt.client.bo.ClientCreateQuestion instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCreateQuestion::name;
  }-*/;
  
  private static native void setName(consys.event.registration.gwt.client.bo.ClientCreateQuestion instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCreateQuestion::name = value;
  }-*/;
  
  private static native boolean getRequired(consys.event.registration.gwt.client.bo.ClientCreateQuestion instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientCreateQuestion::required;
  }-*/;
  
  private static native void setRequired(consys.event.registration.gwt.client.bo.ClientCreateQuestion instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientCreateQuestion::required = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientCreateQuestion instance) throws SerializationException {
    setName(instance, streamReader.readString());
    setRequired(instance, streamReader.readBoolean());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientCreateQuestion instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientCreateQuestion();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientCreateQuestion instance) throws SerializationException {
    streamWriter.writeString(getName(instance));
    streamWriter.writeBoolean(getRequired(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientCreateQuestion_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCreateQuestion_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientCreateQuestion)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientCreateQuestion_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientCreateQuestion)object);
  }
  
}
