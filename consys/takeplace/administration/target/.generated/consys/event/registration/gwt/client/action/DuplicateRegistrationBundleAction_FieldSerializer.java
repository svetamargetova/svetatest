package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DuplicateRegistrationBundleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getTitle(consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction::title;
  }-*/;
  
  private static native void setTitle(consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction::title = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction instance) throws SerializationException {
    setTitle(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction instance) throws SerializationException {
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction)object);
  }
  
}
