package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateRegistrationCycleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.action.CreateRegistrationCycleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateRegistrationCycleAction::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.action.CreateRegistrationCycleAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateRegistrationCycleAction::bundleUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getCycles(consys.event.registration.gwt.client.action.CreateRegistrationCycleAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateRegistrationCycleAction::cycles;
  }-*/;
  
  private static native void setCycles(consys.event.registration.gwt.client.action.CreateRegistrationCycleAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateRegistrationCycleAction::cycles = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.CreateRegistrationCycleAction instance) throws SerializationException {
    setBundleUuid(instance, streamReader.readString());
    setCycles(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.CreateRegistrationCycleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.CreateRegistrationCycleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.CreateRegistrationCycleAction instance) throws SerializationException {
    streamWriter.writeString(getBundleUuid(instance));
    streamWriter.writeObject(getCycles(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.CreateRegistrationCycleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateRegistrationCycleAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.CreateRegistrationCycleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateRegistrationCycleAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.CreateRegistrationCycleAction)object);
  }
  
}
