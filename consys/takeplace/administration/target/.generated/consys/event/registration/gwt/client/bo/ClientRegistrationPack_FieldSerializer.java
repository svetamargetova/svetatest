package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistrationPack_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleDescription(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::bundleDescription;
  }-*/;
  
  private static native void setBundleDescription(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::bundleDescription = value;
  }-*/;
  
  private static native java.lang.String getBundleTitle(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::bundleTitle;
  }-*/;
  
  private static native void setBundleTitle(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::bundleTitle = value;
  }-*/;
  
  private static native int getCapacity(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::capacity;
  }-*/;
  
  private static native void setCapacity(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::capacity = value;
  }-*/;
  
  private static native int getMaxQuantity(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::maxQuantity;
  }-*/;
  
  private static native void setMaxQuantity(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::maxQuantity = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getPrice(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::price;
  }-*/;
  
  private static native void setPrice(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::price = value;
  }-*/;
  
  private static native java.util.HashMap getPricing(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::pricing;
  }-*/;
  
  private static native void setPricing(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, java.util.HashMap value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::pricing = value;
  }-*/;
  
  private static native int getRegistred(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::registred;
  }-*/;
  
  private static native void setRegistred(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::registred = value;
  }-*/;
  
  private static native java.util.Date getTo(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::to;
  }-*/;
  
  private static native void setTo(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::to = value;
  }-*/;
  
  private static native java.lang.String getUuidBundle(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::uuidBundle;
  }-*/;
  
  private static native void setUuidBundle(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::uuidBundle = value;
  }-*/;
  
  private static native java.lang.String getUuidCycle(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::uuidCycle;
  }-*/;
  
  private static native void setUuidCycle(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::uuidCycle = value;
  }-*/;
  
  private static native boolean getWithCode(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::withCode;
  }-*/;
  
  private static native void setWithCode(consys.event.registration.gwt.client.bo.ClientRegistrationPack instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationPack::withCode = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) throws SerializationException {
    setBundleDescription(instance, streamReader.readString());
    setBundleTitle(instance, streamReader.readString());
    setCapacity(instance, streamReader.readInt());
    setMaxQuantity(instance, streamReader.readInt());
    setPrice(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setPricing(instance, (java.util.HashMap) streamReader.readObject());
    setRegistred(instance, streamReader.readInt());
    setTo(instance, (java.util.Date) streamReader.readObject());
    setUuidBundle(instance, streamReader.readString());
    setUuidCycle(instance, streamReader.readString());
    setWithCode(instance, streamReader.readBoolean());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientRegistrationPack instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientRegistrationPack();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientRegistrationPack instance) throws SerializationException {
    streamWriter.writeString(getBundleDescription(instance));
    streamWriter.writeString(getBundleTitle(instance));
    streamWriter.writeInt(getCapacity(instance));
    streamWriter.writeInt(getMaxQuantity(instance));
    streamWriter.writeObject(getPrice(instance));
    streamWriter.writeObject(getPricing(instance));
    streamWriter.writeInt(getRegistred(instance));
    streamWriter.writeObject(getTo(instance));
    streamWriter.writeString(getUuidBundle(instance));
    streamWriter.writeString(getUuidCycle(instance));
    streamWriter.writeBoolean(getWithCode(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientRegistrationPack_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationPack_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientRegistrationPack)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationPack_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientRegistrationPack)object);
  }
  
}
