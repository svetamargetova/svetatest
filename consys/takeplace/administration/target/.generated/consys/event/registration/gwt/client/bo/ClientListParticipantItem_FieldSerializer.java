package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientListParticipantItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUserName(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userName;
  }-*/;
  
  private static native void setUserName(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userName = value;
  }-*/;
  
  private static native java.lang.String getUserOrganization(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userOrganization;
  }-*/;
  
  private static native void setUserOrganization(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userOrganization = value;
  }-*/;
  
  private static native java.lang.String getUserPosition(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userPosition;
  }-*/;
  
  private static native void setUserPosition(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userPosition = value;
  }-*/;
  
  private static native java.lang.String getUserUuid(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userUuid;
  }-*/;
  
  private static native void setUserUuid(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::userUuid = value;
  }-*/;
  
  private static native java.lang.String getUuidPortraitImagePrefix(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::uuidPortraitImagePrefix;
  }-*/;
  
  private static native void setUuidPortraitImagePrefix(consys.event.registration.gwt.client.bo.ClientListParticipantItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientListParticipantItem::uuidPortraitImagePrefix = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientListParticipantItem instance) throws SerializationException {
    setUserName(instance, streamReader.readString());
    setUserOrganization(instance, streamReader.readString());
    setUserPosition(instance, streamReader.readString());
    setUserUuid(instance, streamReader.readString());
    setUuidPortraitImagePrefix(instance, streamReader.readString());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.bo.ClientListParticipantItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientListParticipantItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientListParticipantItem instance) throws SerializationException {
    streamWriter.writeString(getUserName(instance));
    streamWriter.writeString(getUserOrganization(instance));
    streamWriter.writeString(getUserPosition(instance));
    streamWriter.writeString(getUserUuid(instance));
    streamWriter.writeString(getUuidPortraitImagePrefix(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientListParticipantItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientListParticipantItem)object);
  }
  
}
