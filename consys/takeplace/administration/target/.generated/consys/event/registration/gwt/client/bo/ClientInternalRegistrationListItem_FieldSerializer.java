package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientInternalRegistrationListItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleName(consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem::bundleName;
  }-*/;
  
  private static native void setBundleName(consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem::bundleName = value;
  }-*/;
  
  private static native int getQuantity(consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem::quantity;
  }-*/;
  
  private static native void setQuantity(consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem::quantity = value;
  }-*/;
  
  private static native java.lang.String getRegistrationUuid(consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem::registrationUuid;
  }-*/;
  
  private static native void setRegistrationUuid(consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem::registrationUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance) throws SerializationException {
    setBundleName(instance, streamReader.readString());
    setQuantity(instance, streamReader.readInt());
    setRegistrationUuid(instance, streamReader.readString());
    
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem instance) throws SerializationException {
    streamWriter.writeString(getBundleName(instance));
    streamWriter.writeInt(getQuantity(instance));
    streamWriter.writeString(getRegistrationUuid(instance));
    
    consys.event.registration.gwt.client.bo.ClientListParticipantItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem)object);
  }
  
}
