package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistration_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getAnswers(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::answers;
  }-*/;
  
  private static native void setAnswers(consys.event.registration.gwt.client.bo.ClientRegistration instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::answers = value;
  }-*/;
  
  private static native boolean getCeliac(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::celiac;
  }-*/;
  
  private static native void setCeliac(consys.event.registration.gwt.client.bo.ClientRegistration instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::celiac = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.EventUser getEventUser(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::eventUser;
  }-*/;
  
  private static native void setEventUser(consys.event.registration.gwt.client.bo.ClientRegistration instance, consys.common.gwt.shared.bo.EventUser value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::eventUser = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.list.SelectFilterItem getItem(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::item;
  }-*/;
  
  private static native void setItem(consys.event.registration.gwt.client.bo.ClientRegistration instance, consys.common.gwt.shared.bo.list.SelectFilterItem value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::item = value;
  }-*/;
  
  private static native java.lang.String getNote(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::note;
  }-*/;
  
  private static native void setNote(consys.event.registration.gwt.client.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::note = value;
  }-*/;
  
  private static native int getQuantity(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::quantity;
  }-*/;
  
  private static native void setQuantity(consys.event.registration.gwt.client.bo.ClientRegistration instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::quantity = value;
  }-*/;
  
  private static native java.util.ArrayList getRegistrationOrders(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::registrationOrders;
  }-*/;
  
  private static native void setRegistrationOrders(consys.event.registration.gwt.client.bo.ClientRegistration instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::registrationOrders = value;
  }-*/;
  
  private static native java.lang.String getRegistrationUuid(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::registrationUuid;
  }-*/;
  
  private static native void setRegistrationUuid(consys.event.registration.gwt.client.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::registrationUuid = value;
  }-*/;
  
  private static native java.lang.String getSpecialWish(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::specialWish;
  }-*/;
  
  private static native void setSpecialWish(consys.event.registration.gwt.client.bo.ClientRegistration instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::specialWish = value;
  }-*/;
  
  private static native consys.event.registration.gwt.client.bo.ClientRegistration.State getState(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::state;
  }-*/;
  
  private static native void setState(consys.event.registration.gwt.client.bo.ClientRegistration instance, consys.event.registration.gwt.client.bo.ClientRegistration.State value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::state = value;
  }-*/;
  
  private static native boolean getVegetarian(consys.event.registration.gwt.client.bo.ClientRegistration instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration::vegetarian;
  }-*/;
  
  private static native void setVegetarian(consys.event.registration.gwt.client.bo.ClientRegistration instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration::vegetarian = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientRegistration instance) throws SerializationException {
    setAnswers(instance, (java.util.ArrayList) streamReader.readObject());
    setCeliac(instance, streamReader.readBoolean());
    setEventUser(instance, (consys.common.gwt.shared.bo.EventUser) streamReader.readObject());
    setItem(instance, (consys.common.gwt.shared.bo.list.SelectFilterItem) streamReader.readObject());
    setNote(instance, streamReader.readString());
    setQuantity(instance, streamReader.readInt());
    setRegistrationOrders(instance, (java.util.ArrayList) streamReader.readObject());
    setRegistrationUuid(instance, streamReader.readString());
    setSpecialWish(instance, streamReader.readString());
    setState(instance, (consys.event.registration.gwt.client.bo.ClientRegistration.State) streamReader.readObject());
    setVegetarian(instance, streamReader.readBoolean());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientRegistration instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientRegistration();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientRegistration instance) throws SerializationException {
    streamWriter.writeObject(getAnswers(instance));
    streamWriter.writeBoolean(getCeliac(instance));
    streamWriter.writeObject(getEventUser(instance));
    streamWriter.writeObject(getItem(instance));
    streamWriter.writeString(getNote(instance));
    streamWriter.writeInt(getQuantity(instance));
    streamWriter.writeObject(getRegistrationOrders(instance));
    streamWriter.writeString(getRegistrationUuid(instance));
    streamWriter.writeString(getSpecialWish(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeBoolean(getVegetarian(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientRegistration_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistration_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientRegistration)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistration_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientRegistration)object);
  }
  
}
