package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateDiscountCouponAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::bundleUuid = value;
  }-*/;
  
  private static native int getCapacity(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::capacity;
  }-*/;
  
  private static native void setCapacity(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::capacity = value;
  }-*/;
  
  private static native int getDiscount(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::discount;
  }-*/;
  
  private static native void setDiscount(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::discount = value;
  }-*/;
  
  private static native java.util.Date getEndDate(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::endDate;
  }-*/;
  
  private static native void setEndDate(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance, java.util.Date value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::endDate = value;
  }-*/;
  
  private static native java.lang.String getKey(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::key;
  }-*/;
  
  private static native void setKey(consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateDiscountCouponAction::key = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance) throws SerializationException {
    setBundleUuid(instance, streamReader.readString());
    setCapacity(instance, streamReader.readInt());
    setDiscount(instance, streamReader.readInt());
    setEndDate(instance, (java.util.Date) streamReader.readObject());
    setKey(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.CreateDiscountCouponAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.CreateDiscountCouponAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.CreateDiscountCouponAction instance) throws SerializationException {
    streamWriter.writeString(getBundleUuid(instance));
    streamWriter.writeInt(getCapacity(instance));
    streamWriter.writeInt(getDiscount(instance));
    streamWriter.writeObject(getEndDate(instance));
    streamWriter.writeString(getKey(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.CreateDiscountCouponAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateDiscountCouponAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.CreateDiscountCouponAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateDiscountCouponAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.CreateDiscountCouponAction)object);
  }
  
}
