package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CancelRegistrationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getRegistrationOrderUuid(consys.event.registration.gwt.client.action.CancelRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CancelRegistrationAction::registrationOrderUuid;
  }-*/;
  
  private static native void setRegistrationOrderUuid(consys.event.registration.gwt.client.action.CancelRegistrationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CancelRegistrationAction::registrationOrderUuid = value;
  }-*/;
  
  private static native boolean getSourceOwner(consys.event.registration.gwt.client.action.CancelRegistrationAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CancelRegistrationAction::sourceOwner;
  }-*/;
  
  private static native void setSourceOwner(consys.event.registration.gwt.client.action.CancelRegistrationAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CancelRegistrationAction::sourceOwner = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.CancelRegistrationAction instance) throws SerializationException {
    setRegistrationOrderUuid(instance, streamReader.readString());
    setSourceOwner(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.CancelRegistrationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.CancelRegistrationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.CancelRegistrationAction instance) throws SerializationException {
    streamWriter.writeString(getRegistrationOrderUuid(instance));
    streamWriter.writeBoolean(getSourceOwner(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.CancelRegistrationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CancelRegistrationAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.CancelRegistrationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CancelRegistrationAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.CancelRegistrationAction)object);
  }
  
}
