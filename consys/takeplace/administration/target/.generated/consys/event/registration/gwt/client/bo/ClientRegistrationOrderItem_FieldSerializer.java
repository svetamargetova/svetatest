package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistrationOrderItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::bundleUuid = value;
  }-*/;
  
  private static native java.lang.String getDescription(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::description;
  }-*/;
  
  private static native void setDescription(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::description = value;
  }-*/;
  
  private static native java.lang.String getDiscountCode(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::discountCode;
  }-*/;
  
  private static native void setDiscountCode(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::discountCode = value;
  }-*/;
  
  private static native int getQuantity(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::quantity;
  }-*/;
  
  private static native void setQuantity(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::quantity = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::title;
  }-*/;
  
  private static native void setTitle(consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem::title = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance) throws SerializationException {
    setBundleUuid(instance, streamReader.readString());
    setDescription(instance, streamReader.readString());
    setDiscountCode(instance, streamReader.readString());
    setQuantity(instance, streamReader.readInt());
    setTitle(instance, streamReader.readString());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem instance) throws SerializationException {
    streamWriter.writeString(getBundleUuid(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getDiscountCode(instance));
    streamWriter.writeInt(getQuantity(instance));
    streamWriter.writeString(getTitle(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem)object);
  }
  
}
