package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ConfirmOrderAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getNote(consys.event.registration.gwt.client.action.ConfirmOrderAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.ConfirmOrderAction::note;
  }-*/;
  
  private static native void setNote(consys.event.registration.gwt.client.action.ConfirmOrderAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.ConfirmOrderAction::note = value;
  }-*/;
  
  private static native boolean getNotifyUser(consys.event.registration.gwt.client.action.ConfirmOrderAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.ConfirmOrderAction::notifyUser;
  }-*/;
  
  private static native void setNotifyUser(consys.event.registration.gwt.client.action.ConfirmOrderAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.ConfirmOrderAction::notifyUser = value;
  }-*/;
  
  private static native java.lang.String getOrderUuid(consys.event.registration.gwt.client.action.ConfirmOrderAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.ConfirmOrderAction::orderUuid;
  }-*/;
  
  private static native void setOrderUuid(consys.event.registration.gwt.client.action.ConfirmOrderAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.ConfirmOrderAction::orderUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.ConfirmOrderAction instance) throws SerializationException {
    setNote(instance, streamReader.readString());
    setNotifyUser(instance, streamReader.readBoolean());
    setOrderUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.ConfirmOrderAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.ConfirmOrderAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.ConfirmOrderAction instance) throws SerializationException {
    streamWriter.writeString(getNote(instance));
    streamWriter.writeBoolean(getNotifyUser(instance));
    streamWriter.writeString(getOrderUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.ConfirmOrderAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.ConfirmOrderAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.ConfirmOrderAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.ConfirmOrderAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.ConfirmOrderAction)object);
  }
  
}
