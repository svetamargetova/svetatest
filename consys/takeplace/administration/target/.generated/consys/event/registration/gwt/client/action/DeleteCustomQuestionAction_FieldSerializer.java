package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteCustomQuestionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getQuestionUuid(consys.event.registration.gwt.client.action.DeleteCustomQuestionAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.DeleteCustomQuestionAction::questionUuid;
  }-*/;
  
  private static native void setQuestionUuid(consys.event.registration.gwt.client.action.DeleteCustomQuestionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.DeleteCustomQuestionAction::questionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.DeleteCustomQuestionAction instance) throws SerializationException {
    setQuestionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.DeleteCustomQuestionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.DeleteCustomQuestionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.DeleteCustomQuestionAction instance) throws SerializationException {
    streamWriter.writeString(getQuestionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.DeleteCustomQuestionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DeleteCustomQuestionAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.DeleteCustomQuestionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DeleteCustomQuestionAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.DeleteCustomQuestionAction)object);
  }
  
}
