package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientAnswer_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAnswer(consys.event.registration.gwt.client.bo.ClientAnswer instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientAnswer::answer;
  }-*/;
  
  private static native void setAnswer(consys.event.registration.gwt.client.bo.ClientAnswer instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientAnswer::answer = value;
  }-*/;
  
  private static native int getOrder(consys.event.registration.gwt.client.bo.ClientAnswer instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientAnswer::order;
  }-*/;
  
  private static native void setOrder(consys.event.registration.gwt.client.bo.ClientAnswer instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientAnswer::order = value;
  }-*/;
  
  private static native java.lang.String getQuestion(consys.event.registration.gwt.client.bo.ClientAnswer instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientAnswer::question;
  }-*/;
  
  private static native void setQuestion(consys.event.registration.gwt.client.bo.ClientAnswer instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientAnswer::question = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientAnswer instance) throws SerializationException {
    setAnswer(instance, streamReader.readString());
    setOrder(instance, streamReader.readInt());
    setQuestion(instance, streamReader.readString());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientAnswer instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientAnswer();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientAnswer instance) throws SerializationException {
    streamWriter.writeString(getAnswer(instance));
    streamWriter.writeInt(getOrder(instance));
    streamWriter.writeString(getQuestion(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientAnswer_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientAnswer_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientAnswer)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientAnswer_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientAnswer)object);
  }
  
}
