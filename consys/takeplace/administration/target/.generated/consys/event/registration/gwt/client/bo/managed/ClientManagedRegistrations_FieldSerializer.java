package consys.event.registration.gwt.client.bo.managed;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientManagedRegistrations_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getParticipants(consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations::participants;
  }-*/;
  
  private static native void setParticipants(consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations::participants = value;
  }-*/;
  
  private static native java.util.ArrayList getRegistrations(consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations::registrations;
  }-*/;
  
  private static native void setRegistrations(consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations::registrations = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations instance) throws SerializationException {
    setParticipants(instance, (java.util.ArrayList) streamReader.readObject());
    setRegistrations(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations instance) throws SerializationException {
    streamWriter.writeObject(getParticipants(instance));
    streamWriter.writeObject(getRegistrations(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations)object);
  }
  
}
