package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateCustomQuestionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getQuestionUuid(consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionAction::questionUuid;
  }-*/;
  
  private static native void setQuestionUuid(consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionAction::questionUuid = value;
  }-*/;
  
  private static native boolean getRequired(consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionAction::required;
  }-*/;
  
  private static native void setRequired(consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionAction::required = value;
  }-*/;
  
  private static native java.lang.String getText(consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionAction::text;
  }-*/;
  
  private static native void setText(consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCustomQuestionAction::text = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance) throws SerializationException {
    setQuestionUuid(instance, streamReader.readString());
    setRequired(instance, streamReader.readBoolean());
    setText(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.UpdateCustomQuestionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.UpdateCustomQuestionAction instance) throws SerializationException {
    streamWriter.writeString(getQuestionUuid(instance));
    streamWriter.writeBoolean(getRequired(instance));
    streamWriter.writeString(getText(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.UpdateCustomQuestionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateCustomQuestionAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.UpdateCustomQuestionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateCustomQuestionAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.UpdateCustomQuestionAction)object);
  }
  
}
