package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientWaitingBundlesInfo_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getInfos(consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo::infos;
  }-*/;
  
  private static native void setInfos(consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo::infos = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo instance) throws SerializationException {
    setInfos(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo instance) throws SerializationException {
    streamWriter.writeObject(getInfos(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo)object);
  }
  
}
