package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateCustomQuestionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getQuestions(consys.event.registration.gwt.client.action.CreateCustomQuestionAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.CreateCustomQuestionAction::questions;
  }-*/;
  
  private static native void setQuestions(consys.event.registration.gwt.client.action.CreateCustomQuestionAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.CreateCustomQuestionAction::questions = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.CreateCustomQuestionAction instance) throws SerializationException {
    setQuestions(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.CreateCustomQuestionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.CreateCustomQuestionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.CreateCustomQuestionAction instance) throws SerializationException {
    streamWriter.writeObject(getQuestions(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.CreateCustomQuestionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateCustomQuestionAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.CreateCustomQuestionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.CreateCustomQuestionAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.CreateCustomQuestionAction)object);
  }
  
}
