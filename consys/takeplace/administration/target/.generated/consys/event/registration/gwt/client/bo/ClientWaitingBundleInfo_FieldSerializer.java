package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientWaitingBundleInfo_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getBundleName(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::bundleName;
  }-*/;
  
  private static native void setBundleName(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::bundleName = value;
  }-*/;
  
  private static native java.lang.String getBundleUuid(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::bundleUuid;
  }-*/;
  
  private static native void setBundleUuid(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::bundleUuid = value;
  }-*/;
  
  private static native int getFilledCapacity(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::filledCapacity;
  }-*/;
  
  private static native void setFilledCapacity(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::filledCapacity = value;
  }-*/;
  
  private static native int getFullCapacity(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::fullCapacity;
  }-*/;
  
  private static native void setFullCapacity(consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo::fullCapacity = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance) throws SerializationException {
    setBundleName(instance, streamReader.readString());
    setBundleUuid(instance, streamReader.readString());
    setFilledCapacity(instance, streamReader.readInt());
    setFullCapacity(instance, streamReader.readInt());
    
  }
  
  public static consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo instance) throws SerializationException {
    streamWriter.writeString(getBundleName(instance));
    streamWriter.writeString(getBundleUuid(instance));
    streamWriter.writeInt(getFilledCapacity(instance));
    streamWriter.writeInt(getFullCapacity(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo)object);
  }
  
}
