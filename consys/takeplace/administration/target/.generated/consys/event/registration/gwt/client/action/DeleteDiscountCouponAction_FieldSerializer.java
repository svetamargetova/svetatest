package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteDiscountCouponAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.action.DeleteDiscountCouponAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.DeleteDiscountCouponAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.action.DeleteDiscountCouponAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.DeleteDiscountCouponAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.DeleteDiscountCouponAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.DeleteDiscountCouponAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.DeleteDiscountCouponAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.DeleteDiscountCouponAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.DeleteDiscountCouponAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DeleteDiscountCouponAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.DeleteDiscountCouponAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.DeleteDiscountCouponAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.DeleteDiscountCouponAction)object);
  }
  
}
