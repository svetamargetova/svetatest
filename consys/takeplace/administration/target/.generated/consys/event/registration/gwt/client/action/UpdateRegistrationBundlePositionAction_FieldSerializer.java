package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateRegistrationBundlePositionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getSubbundle(consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction::subbundle;
  }-*/;
  
  private static native void setSubbundle(consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction::subbundle = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction instance) throws SerializationException {
    setSubbundle(instance, streamReader.readBoolean());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.client.rpc.action.UpdateOrderPositionAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction instance) throws SerializationException {
    streamWriter.writeBoolean(getSubbundle(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.client.rpc.action.UpdateOrderPositionAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction)object);
  }
  
}
