package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientRegistration_State_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Integer getId(consys.event.registration.gwt.client.bo.ClientRegistration.State instance) /*-{
    return instance.@consys.event.registration.gwt.client.bo.ClientRegistration$State::id;
  }-*/;
  
  private static native void setId(consys.event.registration.gwt.client.bo.ClientRegistration.State instance, java.lang.Integer value) 
  /*-{
    instance.@consys.event.registration.gwt.client.bo.ClientRegistration$State::id = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.bo.ClientRegistration.State instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static consys.event.registration.gwt.client.bo.ClientRegistration.State instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    consys.event.registration.gwt.client.bo.ClientRegistration.State[] values = consys.event.registration.gwt.client.bo.ClientRegistration.State.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.bo.ClientRegistration.State instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.bo.ClientRegistration_State_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistration_State_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.bo.ClientRegistration.State)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.bo.ClientRegistration_State_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.bo.ClientRegistration.State)object);
  }
  
}
