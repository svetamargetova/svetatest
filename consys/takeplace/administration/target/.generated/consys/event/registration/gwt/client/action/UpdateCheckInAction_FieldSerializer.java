package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateCheckInAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getChecked(consys.event.registration.gwt.client.action.UpdateCheckInAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCheckInAction::checked;
  }-*/;
  
  private static native void setChecked(consys.event.registration.gwt.client.action.UpdateCheckInAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCheckInAction::checked = value;
  }-*/;
  
  private static native int getCount(consys.event.registration.gwt.client.action.UpdateCheckInAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCheckInAction::count;
  }-*/;
  
  private static native void setCount(consys.event.registration.gwt.client.action.UpdateCheckInAction instance, int value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCheckInAction::count = value;
  }-*/;
  
  private static native java.lang.String getRegistrationUuid(consys.event.registration.gwt.client.action.UpdateCheckInAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.UpdateCheckInAction::registrationUuid;
  }-*/;
  
  private static native void setRegistrationUuid(consys.event.registration.gwt.client.action.UpdateCheckInAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.UpdateCheckInAction::registrationUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.UpdateCheckInAction instance) throws SerializationException {
    setChecked(instance, streamReader.readBoolean());
    setCount(instance, streamReader.readInt());
    setRegistrationUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.UpdateCheckInAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.UpdateCheckInAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.UpdateCheckInAction instance) throws SerializationException {
    streamWriter.writeBoolean(getChecked(instance));
    streamWriter.writeInt(getCount(instance));
    streamWriter.writeString(getRegistrationUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.UpdateCheckInAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateCheckInAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.UpdateCheckInAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.UpdateCheckInAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.UpdateCheckInAction)object);
  }
  
}
