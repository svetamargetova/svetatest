package consys.event.registration.gwt.client.message;

public class EventRegistrationConstants_ implements consys.event.registration.gwt.client.message.EventRegistrationConstants {
  
  public java.lang.String registrationModule_text_navigationModelParticipantList() {
    return "Participant List";
  }
  
  public java.lang.String registrationCycleList_action_addRC() {
    return "Add registration cycle";
  }
  
  public java.lang.String customQuestionItem_text_required() {
    return "Required";
  }
  
  public java.lang.String registrationList_action_all() {
    return "All";
  }
  
  public java.lang.String registrationParticipantInfoPanel_action_details() {
    return "Details";
  }
  
  public java.lang.String subbundleItem_text_vat() {
    return "VAT";
  }
  
  public java.lang.String registrationSettingsBundle_title() {
    return "Tickets";
  }
  
  public java.lang.String registrationOrderPanel_text_additionalProducts() {
    return "Additional products";
  }
  
  public java.lang.String outerRegistrationForm_error_eventNotActivated() {
    return "You can register only after the event is activated. Contact the organizators of the event.";
  }
  
  public java.lang.String const_exceptionTicketUniquity() {
    return "This ticket already exists.";
  }
  
  public java.lang.String outerRegistrationForm_error_userInfoNotEntered() {
    return "You have not entered user details. Please choose one from  the third party services (Facebook, Twitter, LinkedIn or Google) or fill Takeplace registration form.";
  }
  
  public java.lang.String discountFilterList_action_all() {
    return "All";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_action_addField() {
    return "New custom field";
  }
  
  public java.lang.String const_exceptionCancelRegistrationNoRecords() {
    return "There is no record for cancel registration. Please contact us.";
  }
  
  public java.lang.String outerRegistrationForm_text_invoiceAddress() {
    return "Billing address";
  }
  
  public java.lang.String buyAdditionProducts_button_order() {
    return "ORDER";
  }
  
  public java.lang.String registrationSettingsSubbundle_text_newTicketAccessory() {
    return "New ticket accessory";
  }
  
  public java.lang.String outerRegisterContent_error_noRecorsForParameterValues() {
    return "No records for parameter values";
  }
  
  public java.lang.String outerRegisterContent_text_alreadyRegistered() {
    return "Already registered in Takeplace?";
  }
  
  public java.lang.String registrationFormItem_exception_noRecordsForAction() {
    return "There is no record for given registration option. Please contact us.";
  }
  
  public java.lang.String outerRegistrationForm_error_registrationIsClosed() {
    return "Registration is closed for this ticket option.";
  }
  
  public java.lang.String registrationFormItem_text_registrationClosed() {
    return "Registration closed";
  }
  
  public java.lang.String discountListItem_text_code() {
    return "Code";
  }
  
  public java.lang.String registrationFormRegistered_button_sendCoupon() {
    return "Send discount code";
  }
  
  public java.lang.String discountFilterList_action_notUsed() {
    return "Not used";
  }
  
  public java.lang.String confirmOrderDialog_text_notifyUser() {
    return "Notify user";
  }
  
  public java.lang.String questionWidget_action_next() {
    return "Next";
  }
  
  public java.lang.String registrationList_title() {
    return "Registration list";
  }
  
  public java.lang.String orderControlPanel_action_downloadInvoice() {
    return "DOWNLOAD INVOICE";
  }
  
  public java.lang.String registrationFormItem_discount_exception_CouponUsedException() {
    return "Discount code has limited capacity that has been already used.";
  }
  
  public java.lang.String waitingOverview_cancel_registration_question_title() {
    return "Are you sure you want to cancel waiting?";
  }
  
  public java.lang.String const_exceptionTicketCapacity() {
    return "Registration exceeds the ticket capacity.";
  }
  
  public java.lang.String registrationPropertyPanel_property_allowRegisterAnotherParticipant() {
    return "Allow register another participant";
  }
  
  public java.lang.String const_exceptionRegistrationsInCycle() {
    return "Delete registration cycle failed! There are registred users in this cycle.";
  }
  
  public java.lang.String discountListItem_text_discount() {
    return "Discount";
  }
  
  public java.lang.String registrationAdditionalProducts_text_ifDisabled() {
    return "Ordering additional products is available after paying your ticket.";
  }
  
  public java.lang.String registrationList_text_paid() {
    return "Price";
  }
  
  public java.lang.String registrationList_text_preregistered() {
    return "Preregistered";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadProForma() {
    return "Download pro forma invoice";
  }
  
  public java.lang.String editRegistrationForm_text_note_placeholder() {
    return "You can write a note about this participant here. This note is not visible to participant.";
  }
  
  public java.lang.String checkInList_action_name() {
    return "Name";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadInvoice() {
    return "Download your invoice";
  }
  
  public java.lang.String registrationOrderPanel_text_invoice() {
    return "Invoice";
  }
  
  public java.lang.String discountListItem_text_validThrough() {
    return "Can be used till";
  }
  
  public java.lang.String const_price() {
    return "Price";
  }
  
  public java.lang.String registrationFormRegistered_button_cancelRegistration() {
    return "Cancel registration";
  }
  
  public java.lang.String registrationForm_progress_enjoyUpper() {
    return "ENJOY";
  }
  
  public java.lang.String editFormBundle_title() {
    return "Edit ticket";
  }
  
  public java.lang.String outerRegisterContent_button_register() {
    return "REGISTER";
  }
  
  public java.lang.String registrationAdditionalProducts_title() {
    return "You may order additional products";
  }
  
  public java.lang.String checkInList_action_filterChecked() {
    return "Checked";
  }
  
  public java.lang.String registrationSettingsSubbundle_text_pleaseAddTicketAccessory() {
    return "Start by adding a ticket accessory.";
  }
  
  public java.lang.String checkInList_action_filterAll() {
    return "All";
  }
  
  public java.lang.String editFormDiscountCoupon_title() {
    return "Edit discount coupon";
  }
  
  public java.lang.String waitingOverview_button_cancelWaiting() {
    return "Cancel wait";
  }
  
  public java.lang.String waitingOverview_title() {
    return "Registration / Waiting room";
  }
  
  public java.lang.String outerRegistrationForm_text_completeYourProfile() {
    return "Complete your profile";
  }
  
  public java.lang.String registrationModule_text_moduleRoleAdministrator() {
    return "Administrator";
  }
  
  public java.lang.String registrationFormItem_discount_exception_CouponExpiredException() {
    return "Discount code expired.";
  }
  
  public java.lang.String outerRegisterContent_help1() {
    return "To complete your registration we need to know some more information about you. If you are <strong>already</strong> registred in Takeplace, click on <strong>\"Click here to log in ...\"</strong>.";
  }
  
  public java.lang.String registrationList_action_state() {
    return "State";
  }
  
  public java.lang.String discountListItem_text_group() {
    return "Group";
  }
  
  public java.lang.String editFormBundle_form_b2bTicket() {
    return "Is B2B ticket";
  }
  
  public java.lang.String outerRegistrationForm_text_register() {
    return "Register";
  }
  
  public java.lang.String internalRegistrationDialog_action_register() {
    return "Register participant";
  }
  
  public java.lang.String bundleItem_text_notUsed() {
    return "Not used";
  }
  
  public java.lang.String editFormSubbundle_form_vatPercent() {
    return "Vat (percent)";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_text_permittedMax5Questions() {
    return "Allowed a total maximum of five fields";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipant() {
    return "Participant";
  }
  
  public java.lang.String bundleItem_text_used() {
    return "Used";
  }
  
  public java.lang.String const_exceptionUserAlreadyRegistered() {
    return "This user is already registered.";
  }
  
  public java.lang.String editFormQuestion_title() {
    return "Edit custom field";
  }
  
  public java.lang.String registerAnother_text_pleaseAddParticipant() {
    return "Start by adding participant.";
  }
  
  public java.lang.String badgesSizeDialog_title() {
    return "Badges";
  }
  
  public java.lang.String discountListItem_text_capacity() {
    return "Capacity";
  }
  
  public java.lang.String registrationFormRegistered_action_confirmOrder() {
    return "Confirm order";
  }
  
  public java.lang.String userParticipantList_action_name() {
    return "Name";
  }
  
  public java.lang.String checkDialog_titleIn() {
    return "Check In";
  }
  
  public java.lang.String checkDialog_titleOut() {
    return "Uncheck";
  }
  
  public java.lang.String orderControlPanel_action_downloadTicket() {
    return "DOWNLOAD TICKET";
  }
  
  public java.lang.String registrationSettingsBundle_text_newTicket() {
    return "New ticket";
  }
  
  public java.lang.String registrationCycleList_text_pleaseAddRC() {
    return "Start by assigning a registration cycle.";
  }
  
  public java.lang.String registrationFormItem_text_capacityFilled() {
    return "The capacity is filled.";
  }
  
  public java.lang.String registrationForm_action_toWaiting() {
    return "To waiting room";
  }
  
  public java.lang.String discountListItem_text_ticket() {
    return "Ticket / Accessory";
  }
  
  public java.lang.String registrationList_text_state() {
    return "State";
  }
  
  public java.lang.String registrationSettingsBundle_action_addTicket() {
    return "Ticket";
  }
  
  public java.lang.String orderControlPanel_action_downloadProformaInvoice() {
    return "DOWNLOAD PROFORMA INVOICE";
  }
  
  public java.lang.String registrationModule_text_navigationModelCheckInList() {
    return "Check In List";
  }
  
  public java.lang.String discountListItem_text_restriction() {
    return "Restriction";
  }
  
  public java.lang.String outerQuestions_error_loadQuestions() {
    return "Error while loading organizer questions";
  }
  
  public java.lang.String addManagedUserUI_text_firstName() {
    return "First name";
  }
  
  public java.lang.String registrationModule_text_moduleName() {
    return "Registration";
  }
  
  public java.lang.String createDiscountDialog_action_addRestriction() {
    return "Add coupon end date";
  }
  
  public java.lang.String outerRegisterPanel_title() {
    return "Welcome aboard!";
  }
  
  public java.lang.String registrationModule_text_moduleCheckInDescription() {
    return "Administrators can do participants check in on event site.";
  }
  
  public java.lang.String bundleItem_text_discountTitle() {
    return "Discount coupons";
  }
  
  public java.lang.String outerRegisterContent_text_agreeing() {
    return "By clicking on 'REGISTER' below you are agreeing to the <a href=\"http://static.takeplace.eu/GBC_Takeplace_en.pdf\" target=\"_blank\">Terms of Service</a>.";
  }
  
  public java.lang.String questionWidget_title() {
    return "Organizer questions";
  }
  
  public java.lang.String editFormRegistrationCycle_title() {
    return "Edit registration cycle";
  }
  
  public java.lang.String waitingOverview_text_capacity() {
    return "Free capacity";
  }
  
  public java.lang.String outerRegisterContent_help4() {
    return "The password shall be at least eight characters long; please avoid common names and words. In the Payment profile section please specity payment details you wish to appear in your invoice.";
  }
  
  public java.lang.String editFormBundle_form_maxQuantity() {
    return "Max. quantity";
  }
  
  public java.lang.String registrationForm_progress_payUpper() {
    return "PAY";
  }
  
  public java.lang.String registrationModule_text_moduleRoleAdministratorDescription() {
    return "Administrators have permission to manage participants... (description)";
  }
  
  public java.lang.String checkInList_action_buttonChecked() {
    return "CHECKED";
  }
  
  public java.lang.String registrationForm_text_insertDiscountCoupon() {
    return "Insert discount code";
  }
  
  public java.lang.String internalRegistration_title() {
    return "Internal registration";
  }
  
  public java.lang.String registrationFormRegistered_cancel_order_question_title() {
    return "Are you sure you want to cancel order?";
  }
  
  public java.lang.String outerRegisterContent_help5() {
    return "The invoice name and address can be used for your TAX office for depreciation. Please insert the organization address in case of asking your organization for refunding.";
  }
  
  public java.lang.String registrationFormItem_text_attention() {
    return "Attention";
  }
  
  public java.lang.String outerRegisterContent_action_clickToLogin() {
    return "Click here to log in ...";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadProFormaAdmin() {
    return "Download user pro forma invoice";
  }
  
  public java.lang.String registrationPropertyPanel_property_visibleParticipants() {
    return "Visible participants";
  }
  
  public java.lang.String editRegistrationForm_text_special_wish() {
    return "Special wish:";
  }
  
  public java.lang.String internalRegistrationDialog_text_userInformation() {
    return "Participant informations";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegistrationList() {
    return "Registred Participants";
  }
  
  public java.lang.String outerRegisterContent_action_clickToRegister() {
    return "Click here to create account ...";
  }
  
  public java.lang.String registrationFormRegistered_text_registrationPaid() {
    return "Your ticket registration has been confirmed.";
  }
  
  public java.lang.String registrationFormItem_action_showDetails() {
    return "Show details";
  }
  
  public java.lang.String const_registrationCycleNotOverlap() {
    return "Registration cycles must not overlap";
  }
  
  public java.lang.String registerAnother_text_canceled() {
    return "Canceled";
  }
  
  public java.lang.String const_exceptionCyclesIntersection() {
    return "Registration cycles cannot intersect.";
  }
  
  public java.lang.String editRegistrationForm_text_note() {
    return "Note:";
  }
  
  public java.lang.String checkInList_title() {
    return "Check In List";
  }
  
  public java.lang.String registrationList_text_waiting() {
    return "Waiting";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegister() {
    return "Register";
  }
  
  public java.lang.String registrationFormItem_discount_exception_Common() {
    return "An error occured when processing discount code.";
  }
  
  public java.lang.String discountListItem_text_notUsed() {
    return "Not used.";
  }
  
  public java.lang.String checkDialog_text_all() {
    return "All";
  }
  
  public java.lang.String outerRegistrationForm_text_providedByTakeplace() {
    return "Registration to this event is provided by Takeplace.";
  }
  
  public java.lang.String discountFilterList_action_used() {
    return "Used";
  }
  
  public java.lang.String registrationForm_progress_registerUpper() {
    return "REGISTER";
  }
  
  public java.lang.String registrationModule_text_moduleCheckIn() {
    return "Check In Administrator";
  }
  
  public java.lang.String checkInList_action_filterNotChecked() {
    return "Not checked";
  }
  
  public java.lang.String registrationOrderPanel_text_total() {
    return "Total";
  }
  
  public java.lang.String addManagedUserUI_text_lastName() {
    return "Last name";
  }
  
  public java.lang.String outerRegisterContent_error_ticketFull() {
    return "We're really sorry but selected ticket option has limited capacity and is full now. Try to wait or select another ticket option.";
  }
  
  public java.lang.String internalRegistrationDialog_text_mustSelectTicket() {
    return "You must select one ticket";
  }
  
  public java.lang.String registrationFormRegistered_cancel_registration_question_title() {
    return "Are you sure you want to cancel registration?";
  }
  
  public java.lang.String outerRegistrationForm_error_registrationIsClosedDue() {
    return "Registration is closed due to limited capacity for this ticket option.";
  }
  
  public java.lang.String outerLogin_action_iDontHaveAny() {
    return "... I don't have any of those accounts.";
  }
  
  public java.lang.String registrationSettingsSubbundle_title() {
    return "Ticket accessories";
  }
  
  public java.lang.String outerRegistrationFormV2_action_showDescription() {
    return "Show event description";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipantAdeptDescription() {
    return "Participant aspirants do not have their registration confirmed.";
  }
  
  public java.lang.String registrationFormRegistered_error_registrationCanceled() {
    return "Registraion canceled.";
  }
  
  public java.lang.String couponListItem_error_noRecords() {
    return "Required discount coupont not found.";
  }
  
  public java.lang.String discountInfoPanel_action_groupCoupon() {
    return "Group coupon";
  }
  
  public java.lang.String addManagedUserUI_text_organization() {
    return "Organization";
  }
  
  public java.lang.String registrationCycleList_title() {
    return "Registration cycles";
  }
  
  public java.lang.String registrationFormItem_button_register() {
    return "Register";
  }
  
  public java.lang.String outerRegistrationForm_text_invoiceTo() {
    return "Invoiced to Organisation / Name";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipantDescription() {
    return "Participants with confirmed redistration.";
  }
  
  public java.lang.String outerRegistrationForm_error_eventNotFound() {
    return "Event not found";
  }
  
  public java.lang.String discountFilterList_action_individual() {
    return "Individual";
  }
  
  public java.lang.String registrationSettingsBundle_text_pleaseAddTicket() {
    return "Start by adding a ticket.";
  }
  
  public java.lang.String customQuestionItem_text_question() {
    return "Field name";
  }
  
  public java.lang.String registrationForm_text_validated() {
    return "Validated";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_title() {
    return "Custom registration fields";
  }
  
  public java.lang.String couponListItem_error_alreadyUsed() {
    return "Discount coupon already used.";
  }
  
  public java.lang.String registrationModule_text_moduleRoleParticipantAdept() {
    return "Participant aspirant";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegistrationSettings() {
    return "Registration Settings";
  }
  
  public java.lang.String registrationOrderPanel_action_downloadInvoiceAdmin() {
    return "Download user invoice";
  }
  
  public java.lang.String registrationFormRegistered_error_payTimeouted() {
    return "Payment period timeouted.";
  }
  
  public java.lang.String confirmOrderDialog_title() {
    return "Confirm order";
  }
  
  public java.lang.String outerRegisterContent_help2() {
    return "If you are new to Takeplace, please fill in the registration form. Requested fields are marked with red asterisks.";
  }
  
  public java.lang.String discountList_title() {
    return "Discount coupons";
  }
  
  public java.lang.String outerRegistrationForm_text_registerHelp() {
    return "Register by your existing account in one of this services or create new account in Takeplace";
  }
  
  public java.lang.String outerRegisterContent_help3() {
    return "The contact e-mail shall be permanent as it will identify you throughout the events you attend or organize.";
  }
  
  public java.lang.String waitingOverview_text_noOpenRegistration() {
    return "No open registrations";
  }
  
  public java.lang.String userParticipantList_title() {
    return "Participant list";
  }
  
  public java.lang.String const_exceptionEventNotActivated() {
    return "Registration failed because event is not active yet.";
  }
  
  public java.lang.String discountInfoPanel_action_individualCoupon() {
    return "Individual coupon";
  }
  
  public java.lang.String registrationSettingsSubbundle_action_addTicketAccessory() {
    return "Ticket accessory";
  }
  
  public java.lang.String registerAnother_action_addManagedParticipant() {
    return "Add managed participant";
  }
  
  public java.lang.String createDiscountDialog_title() {
    return "Create discount coupon";
  }
  
  public java.lang.String registerAnother_action_registerParticipant() {
    return "Register participant";
  }
  
  public java.lang.String editFormBundle_form_b2bTicketAccessory() {
    return "Is B2B ticket accessory";
  }
  
  public java.lang.String editFormBundle_form_quantity() {
    return "Quantity";
  }
  
  public java.lang.String registerAnother_title() {
    return "Other registration";
  }
  
  public java.lang.String registrationForm_title() {
    return "Registration";
  }
  
  public java.lang.String discountListItem_text_individual() {
    return "Individual";
  }
  
  public java.lang.String registrationFormItem_discount_exception_CouponInvalidException() {
    return "Discount code is invalid. Are you sure that you have the right code?";
  }
  
  public java.lang.String outerRegisterPanel_text_beta() {
    return "We are running Takeplace in Beta, leniency towards occassional bugs is welcome";
  }
  
  public java.lang.String registrationList_action_confirm() {
    return "Confirm";
  }
  
  public java.lang.String bundleItem_text_coupons() {
    return "Coupons";
  }
  
  public java.lang.String registrationModule_text_moduleRoleChairDescription() {
    return "Registration chair has access to all registration module screens and action except of module settings.";
  }
  
  public java.lang.String confirmOrderDialog_text_note() {
    return "Note";
  }
  
  public java.lang.String editFormSubbundle_title() {
    return "Edit ticket accessory";
  }
  
  public java.lang.String registrationForm_text_noneTickets() {
    return "None tickets are currently available";
  }
  
  public java.lang.String checkInList_action_buttonUncheck() {
    return "Uncheck";
  }
  
  public java.lang.String registrationFormItem_text_notActive() {
    return "Not active";
  }
  
  public java.lang.String registrationOrderPanel_text_dueDate() {
    return "Due date:";
  }
  
  public java.lang.String registrationSettingsBundle_action_ticketAccessories() {
    return "Ticket Accessories";
  }
  
  public java.lang.String outerRegisterContent_text_notYetRegistered() {
    return "Not yet registered in Takeplace?";
  }
  
  public java.lang.String registrationForm_text_pricing() {
    return "Pricing";
  }
  
  public java.lang.String registrationForm_text_enteredCode() {
    return "Entered discount code";
  }
  
  public java.lang.String discountListItem_text_unlimited() {
    return "Unlimited";
  }
  
  public java.lang.String registrationList_text_registered() {
    return "Registered";
  }
  
  public java.lang.String outerRegistrationForm_text_invoiceAddressHelp() {
    return "Address listed on invoice";
  }
  
  public java.lang.String registrationCycleList_text_newRC() {
    return "New registration cycle";
  }
  
  public java.lang.String registrationFormRegistered_button_cancelOrder() {
    return "Cancel order";
  }
  
  public java.lang.String registrationModule_text_moduleRoleChair() {
    return "Registration Chair";
  }
  
  public java.lang.String outerRegisterContent_help6() {
    return "If you have any question, please check the <a href=\"#FAQ\">faq</a> or contact our support at <a href=\"mailto:support@takeplace.eu\">support@takeplace.eu</a>.";
  }
  
  public java.lang.String checkInList_action_buttonCheckIn() {
    return "CHECK IN";
  }
  
  public java.lang.String const_registrationDate() {
    return "Registration date";
  }
  
  public java.lang.String registrationSettingsCustomQuestion_text_pleaseAddField() {
    return "Start by adding a custom field.";
  }
  
  public java.lang.String editRegistrationForm_text_special_wish_placeholder() {
    return "Do you have any special wish? Write it to organizators.";
  }
  
  public java.lang.String discountFilterList_action_group() {
    return "Group";
  }
  
  public java.lang.String internalRegistration_action_addNewRegistration() {
    return "Add new registration";
  }
  
  public java.lang.String registrationModule_text_navigationModelRegistration() {
    return "Participants";
  }
  
  public java.lang.String registrationModule_text_navigationModelMyRegistration() {
    return "My Registration";
  }
  
  public java.lang.String editFormBundle_form_capacity() {
    return "Capacity";
  }
}
