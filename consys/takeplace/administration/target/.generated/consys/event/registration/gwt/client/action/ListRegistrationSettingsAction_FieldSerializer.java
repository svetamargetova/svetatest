package consys.event.registration.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListRegistrationSettingsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getAll(consys.event.registration.gwt.client.action.ListRegistrationSettingsAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.ListRegistrationSettingsAction::all;
  }-*/;
  
  private static native void setAll(consys.event.registration.gwt.client.action.ListRegistrationSettingsAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.ListRegistrationSettingsAction::all = value;
  }-*/;
  
  private static native boolean getSubbundle(consys.event.registration.gwt.client.action.ListRegistrationSettingsAction instance) /*-{
    return instance.@consys.event.registration.gwt.client.action.ListRegistrationSettingsAction::subbundle;
  }-*/;
  
  private static native void setSubbundle(consys.event.registration.gwt.client.action.ListRegistrationSettingsAction instance, boolean value) 
  /*-{
    instance.@consys.event.registration.gwt.client.action.ListRegistrationSettingsAction::subbundle = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.registration.gwt.client.action.ListRegistrationSettingsAction instance) throws SerializationException {
    setAll(instance, streamReader.readBoolean());
    setSubbundle(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.registration.gwt.client.action.ListRegistrationSettingsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.registration.gwt.client.action.ListRegistrationSettingsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.registration.gwt.client.action.ListRegistrationSettingsAction instance) throws SerializationException {
    streamWriter.writeBoolean(getAll(instance));
    streamWriter.writeBoolean(getSubbundle(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.registration.gwt.client.action.ListRegistrationSettingsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.ListRegistrationSettingsAction_FieldSerializer.deserialize(reader, (consys.event.registration.gwt.client.action.ListRegistrationSettingsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.registration.gwt.client.action.ListRegistrationSettingsAction_FieldSerializer.serialize(writer, (consys.event.registration.gwt.client.action.ListRegistrationSettingsAction)object);
  }
  
}
