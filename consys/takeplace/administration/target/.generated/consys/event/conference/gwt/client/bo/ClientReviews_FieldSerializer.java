package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviews_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getArtefactNotes(consys.event.conference.gwt.client.bo.ClientReviews instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviews::artefactNotes;
  }-*/;
  
  private static native void setArtefactNotes(consys.event.conference.gwt.client.bo.ClientReviews instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviews::artefactNotes = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData getHead(consys.event.conference.gwt.client.bo.ClientReviews instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviews::head;
  }-*/;
  
  private static native void setHead(consys.event.conference.gwt.client.bo.ClientReviews instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviews::head = value;
  }-*/;
  
  private static native java.util.ArrayList getMainNotes(consys.event.conference.gwt.client.bo.ClientReviews instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviews::mainNotes;
  }-*/;
  
  private static native void setMainNotes(consys.event.conference.gwt.client.bo.ClientReviews instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviews::mainNotes = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientReviews instance) throws SerializationException {
    setArtefactNotes(instance, (java.util.ArrayList) streamReader.readObject());
    setHead(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData) streamReader.readObject());
    setMainNotes(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.ClientReviews instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientReviews();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientReviews instance) throws SerializationException {
    streamWriter.writeObject(getArtefactNotes(instance));
    streamWriter.writeObject(getHead(instance));
    streamWriter.writeObject(getMainNotes(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientReviews_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviews_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientReviews)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviews_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientReviews)object);
  }
  
}
