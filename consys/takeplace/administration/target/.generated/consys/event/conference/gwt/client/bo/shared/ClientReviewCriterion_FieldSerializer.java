package consys.event.conference.gwt.client.bo.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewCriterion_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::description;
  }-*/;
  
  private static native void setDescription(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::description = value;
  }-*/;
  
  private static native java.lang.Long getId(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::id;
  }-*/;
  
  private static native void setId(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::id = value;
  }-*/;
  
  private static native java.util.ArrayList getItems(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::items;
  }-*/;
  
  private static native void setItems(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::items = value;
  }-*/;
  
  private static native int getMaxPoints(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::maxPoints;
  }-*/;
  
  private static native void setMaxPoints(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::maxPoints = value;
  }-*/;
  
  private static native double getMinAverageAccept(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::minAverageAccept;
  }-*/;
  
  private static native void setMinAverageAccept(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, double value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::minAverageAccept = value;
  }-*/;
  
  private static native double getMinPointsAccept(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::minPointsAccept;
  }-*/;
  
  private static native void setMinPointsAccept(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, double value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::minPointsAccept = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::name = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.evaluation.EvaluationValue getValue(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::value;
  }-*/;
  
  private static native void setValue(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance, consys.event.conference.gwt.client.bo.evaluation.EvaluationValue value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion::value = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setItems(instance, (java.util.ArrayList) streamReader.readObject());
    setMaxPoints(instance, streamReader.readInt());
    setMinAverageAccept(instance, streamReader.readDouble());
    setMinPointsAccept(instance, streamReader.readDouble());
    setName(instance, streamReader.readString());
    setValue(instance, (consys.event.conference.gwt.client.bo.evaluation.EvaluationValue) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getItems(instance));
    streamWriter.writeInt(getMaxPoints(instance));
    streamWriter.writeDouble(getMinAverageAccept(instance));
    streamWriter.writeDouble(getMinPointsAccept(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getValue(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion)object);
  }
  
}
