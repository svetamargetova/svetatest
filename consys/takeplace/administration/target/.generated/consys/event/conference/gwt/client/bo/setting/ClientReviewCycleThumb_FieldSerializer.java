package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewCycleThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getFrom(consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb::from;
  }-*/;
  
  private static native void setFrom(consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb::from = value;
  }-*/;
  
  private static native java.lang.Long getId(consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb::id;
  }-*/;
  
  private static native void setId(consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb::id = value;
  }-*/;
  
  private static native java.util.Date getTo(consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb::to;
  }-*/;
  
  private static native void setTo(consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb::to = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance) throws SerializationException {
    setFrom(instance, (java.util.Date) streamReader.readObject());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setTo(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb instance) throws SerializationException {
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getTo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb)object);
  }
  
}
