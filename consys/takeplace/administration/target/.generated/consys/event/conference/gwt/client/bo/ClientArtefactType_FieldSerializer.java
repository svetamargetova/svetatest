package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientArtefactType_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getAutoAccepted(consys.event.conference.gwt.client.bo.ClientArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::autoAccepted;
  }-*/;
  
  private static native void setAutoAccepted(consys.event.conference.gwt.client.bo.ClientArtefactType instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::autoAccepted = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType getDataType(consys.event.conference.gwt.client.bo.ClientArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::dataType;
  }-*/;
  
  private static native void setDataType(consys.event.conference.gwt.client.bo.ClientArtefactType instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::dataType = value;
  }-*/;
  
  private static native java.lang.String getFileFormats(consys.event.conference.gwt.client.bo.ClientArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::fileFormats;
  }-*/;
  
  private static native void setFileFormats(consys.event.conference.gwt.client.bo.ClientArtefactType instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::fileFormats = value;
  }-*/;
  
  private static native int getMaxInputLength(consys.event.conference.gwt.client.bo.ClientArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::maxInputLength;
  }-*/;
  
  private static native void setMaxInputLength(consys.event.conference.gwt.client.bo.ClientArtefactType instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::maxInputLength = value;
  }-*/;
  
  private static native boolean getRequired(consys.event.conference.gwt.client.bo.ClientArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::required;
  }-*/;
  
  private static native void setRequired(consys.event.conference.gwt.client.bo.ClientArtefactType instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::required = value;
  }-*/;
  
  private static native java.util.ArrayList getUploadedExamples(consys.event.conference.gwt.client.bo.ClientArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::uploadedExamples;
  }-*/;
  
  private static native void setUploadedExamples(consys.event.conference.gwt.client.bo.ClientArtefactType instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::uploadedExamples = value;
  }-*/;
  
  private static native java.util.ArrayList getUploadedTemplates(consys.event.conference.gwt.client.bo.ClientArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::uploadedTemplates;
  }-*/;
  
  private static native void setUploadedTemplates(consys.event.conference.gwt.client.bo.ClientArtefactType instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactType::uploadedTemplates = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientArtefactType instance) throws SerializationException {
    setAutoAccepted(instance, streamReader.readBoolean());
    setDataType(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType) streamReader.readObject());
    setFileFormats(instance, streamReader.readString());
    setMaxInputLength(instance, streamReader.readInt());
    setRequired(instance, streamReader.readBoolean());
    setUploadedExamples(instance, (java.util.ArrayList) streamReader.readObject());
    setUploadedTemplates(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.ClientArtefactType instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientArtefactType();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientArtefactType instance) throws SerializationException {
    streamWriter.writeBoolean(getAutoAccepted(instance));
    streamWriter.writeObject(getDataType(instance));
    streamWriter.writeString(getFileFormats(instance));
    streamWriter.writeInt(getMaxInputLength(instance));
    streamWriter.writeBoolean(getRequired(instance));
    streamWriter.writeObject(getUploadedExamples(instance));
    streamWriter.writeObject(getUploadedTemplates(instance));
    
    consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientArtefactType_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientArtefactType_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientArtefactType)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientArtefactType_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientArtefactType)object);
  }
  
}
