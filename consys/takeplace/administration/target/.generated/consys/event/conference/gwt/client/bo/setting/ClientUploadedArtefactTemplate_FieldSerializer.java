package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUploadedArtefactTemplate_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContentType(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::contentType;
  }-*/;
  
  private static native void setContentType(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::contentType = value;
  }-*/;
  
  private static native java.util.Date getDateInserted(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::dateInserted;
  }-*/;
  
  private static native void setDateInserted(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::dateInserted = value;
  }-*/;
  
  private static native java.lang.String getDescription(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::description;
  }-*/;
  
  private static native void setDescription(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::description = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::name = value;
  }-*/;
  
  private static native java.lang.Long getSize(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::size;
  }-*/;
  
  private static native void setSize(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::size = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) throws SerializationException {
    setContentType(instance, streamReader.readString());
    setDateInserted(instance, (java.util.Date) streamReader.readObject());
    setDescription(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    setSize(instance, (java.lang.Long) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate instance) throws SerializationException {
    streamWriter.writeString(getContentType(instance));
    streamWriter.writeObject(getDateInserted(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getSize(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate)object);
  }
  
}
