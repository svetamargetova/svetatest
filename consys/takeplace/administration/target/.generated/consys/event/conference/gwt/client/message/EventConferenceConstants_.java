package consys.event.conference.gwt.client.message;

public class EventConferenceConstants_ implements consys.event.conference.gwt.client.message.EventConferenceConstants {
  
  public java.lang.String reviewCycleList_title() {
    return "Review cycles";
  }
  
  public java.lang.String const_publicCommentHelpText() {
    return "";
  }
  
  public java.lang.String editFormArtefact_text_zeroIsUnlimited() {
    return "0 is unlimited";
  }
  
  public java.lang.String reviewScoreAndComments_overallEvaluationHelp_text() {
    return "Reviewer overall contribution evaluation that is an average of all evaluated artifacts. Click to see the review.";
  }
  
  public java.lang.String newContributionForm_text_submitPanel_title() {
    return "Submit this contribution";
  }
  
  public java.lang.String editFormReviewCycle_error_submissionPeriodDatesMustBeSet() {
    return "Dates for the submission period must be specified.";
  }
  
  public java.lang.String reviewCycleList_text_beginByAddingReviewCycle() {
    return "Please begin by adding review cycle";
  }
  
  public java.lang.String const_artefacts() {
    return "Artefacts";
  }
  
  public java.lang.String contributionHeadPanel_state_helpTitle() {
    return "Contribution state";
  }
  
  public java.lang.String contributionHeadPanel_overallScore_helpText() {
    return "Final score that is average of all evaluations of reviewers.";
  }
  
  public java.lang.String editFormArtefact_action_addTemplate() {
    return "Add template";
  }
  
  public java.lang.String evaluationChairForm_text_minLower() {
    return "min";
  }
  
  public java.lang.String const_file() {
    return "File";
  }
  
  public java.lang.String criteriaList_text_excellent() {
    return "Excellent";
  }
  
  public java.lang.String conferenceModule_text_moduleRightReviewerDescription() {
    return "Right to do reviews of submissions";
  }
  
  public java.lang.String const_actionUrlFail() {
    return "Action url not created";
  }
  
  public java.lang.String reviewCycleList_text_newReviewCycle() {
    return "New review cycle";
  }
  
  public java.lang.String conferencePropertyPanel_property_visibleAuthors() {
    return "Anonymous reviews";
  }
  
  public java.lang.String evaluationReviewerForm_text_notes() {
    return "Notes";
  }
  
  public java.lang.String reviewerEvaluation_helpText_totalScore() {
    return "";
  }
  
  public java.lang.String editFormArtefact_form_necessity() {
    return "Necessity";
  }
  
  public java.lang.String newContributionForm_text_termClosed() {
    return "The term for submitting this contribution type is closed.";
  }
  
  public java.lang.String conferenceModule_text_moduleRightSubmissionSettingsDescription() {
    return "Permission to view and set settings.";
  }
  
  public java.lang.String criteriaList_text_newCriterion() {
    return "New criterion";
  }
  
  public java.lang.String preferredTopicSelectForm_title() {
    return "Preferred topics";
  }
  
  public java.lang.String const_state() {
    return "State";
  }
  
  public java.lang.String const_submitDate() {
    return "Submit date";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignNotWantTitle() {
    return "These people do not want review this contribution";
  }
  
  public java.lang.String evaluationReviewerForm_text_cyclePeriodOutOfDate() {
    return "Review of artefact is not open at the moment.";
  }
  
  public java.lang.String artefactFileUpload_error_artefactUploadFailed() {
    return "Artefact upload failed";
  }
  
  public java.lang.String editFormReviewCycle_form_submissionPeriod() {
    return "Submission period";
  }
  
  public java.lang.String artefactUpload_action_remove() {
    return "Remove";
  }
  
  public java.lang.String contributionHeadPanel_partialScore_helpText() {
    return "Reviewer contribution evaluation. Click to see the review.";
  }
  
  public java.lang.String evaluationReviewerForm_action_updateNotes() {
    return "Update notes";
  }
  
  public java.lang.String reviewCycles_text_wasAutoAccept() {
    return "Artefact is automatically accepted.";
  }
  
  public java.lang.String evaluationReviewerForm_text_whitoutEvaluationTerm() {
    return "Without evaluation term";
  }
  
  public java.lang.String contributionHeadPanel_error_noAuthorsEntered() {
    return "You must enter at least one author of this contribution.";
  }
  
  public java.lang.String bidForReviewForm_text_notMind() {
    return "I do not mind to review";
  }
  
  public java.lang.String addPublicAndPrivateComment_text_updateComments() {
    return "Update comments";
  }
  
  public java.lang.String contributionTypeContent_text_newContributionType() {
    return "New contribution type";
  }
  
  public java.lang.String criteriaList_text_adequate() {
    return "Adequate";
  }
  
  public java.lang.String artefactItem_text_uploadedExamples() {
    return "Uploaded examples";
  }
  
  public java.lang.String contributionTypeContent_text_contributionTypes() {
    return "Contribution types";
  }
  
  public java.lang.String conferenceModule_text_navigationModelMyReviews() {
    return "My reviews";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAssignsSubmissionReviewers() {
    return "Assign contribution reviewers";
  }
  
  public java.lang.String editFormArtefact_title_editArtefact() {
    return "Edit Artefact";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAddInternSubmissions() {
    return "Submit internal submissions";
  }
  
  public int changeArtefactUpload_button_width() {
    return 69;
  }
  
  public java.lang.String artefactList_text_newArtefact() {
    return "New artefact";
  }
  
  public java.lang.String contributionReviewerForm_error_reviewNotFound() {
    return "Review not found";
  }
  
  public java.lang.String contributionFormArtefactsPanelBuilder_text_noArtefactInCycle() {
    return "No artefact found in cycle";
  }
  
  public java.lang.String const_automaticallyAccepted() {
    return "Automatically accepted";
  }
  
  public java.lang.String submission_action_addContributor() {
    return "Add submitter";
  }
  
  public java.lang.String evaluationReviewerForm_button_sendEvaluation() {
    return "Send evaluation";
  }
  
  public java.lang.String contributionTypeContent_title() {
    return "Contribution settings";
  }
  
  public java.lang.String myContributionReviews_text_evaluateThisArtefact() {
    return "Evaluate this artefact";
  }
  
  public java.lang.String criteriaList_title() {
    return "Criteria";
  }
  
  public java.lang.String const_exceptionNotInCache() {
    return "NotInCacheException";
  }
  
  public java.lang.String reviewerEvaluation_text_youMustEnterAllEvaluationOfArtefact() {
    return "";
  }
  
  public java.lang.String editFormReviewCycle_text_noArtefactAdded() {
    return "Please add artefact first";
  }
  
  public java.lang.String evaluationReviewerForm_text_artefactNotUpladedYet() {
    return "Artefact has not been uploaded yet";
  }
  
  public java.lang.String evaluationReviewerForm_text_showReview() {
    return "Show review";
  }
  
  public java.lang.String submission_action_addSubtitle() {
    return "Add subtitle";
  }
  
  public java.lang.String newContributionForm_title() {
    return "New contribution";
  }
  
  public java.lang.String reviewerEvaluation_helpTitle_totalScore() {
    return "";
  }
  
  public java.lang.String evaluationReviewerForm_text_evaluationTerm() {
    return "Evaluation term";
  }
  
  public java.lang.String const_comment() {
    return "Comment";
  }
  
  public java.lang.String const_privateComment() {
    return "Private comment";
  }
  
  public java.lang.String reviewCycles_title_view() {
    return "View reviews of artifacts";
  }
  
  public java.lang.String contributionHeadPanel_partialScore_helpTitle() {
    return "Reviewer contribution evaluation";
  }
  
  public java.lang.String bidForReviewForm_action_interest() {
    return "Interest";
  }
  
  public java.lang.String artefactUpload_action_change() {
    return "Change";
  }
  
  public java.lang.String const_privateNote() {
    return "Private note";
  }
  
  public java.lang.String uploadArtefactDialog_text_existingFiles() {
    return "Existing files";
  }
  
  public java.lang.String contributionChairForm_text_noReviewers() {
    return "There are no assigned reviewers for this contribution.";
  }
  
  public java.lang.String bidForReviewForm_text_conflict() {
    return "Conflict";
  }
  
  public java.lang.String const_contributors() {
    return "Contributors";
  }
  
  public java.lang.String editFormCriteria_text_minimumPoints() {
    return "Minimum points";
  }
  
  public java.lang.String artefact_text_fileTypes() {
    return "File types";
  }
  
  public java.lang.String editFormReviewCycle_error_wrongReviewPeriod() {
    return "Review period is not valid (“from” date is after “to” date).";
  }
  
  public java.lang.String const_state_pending() {
    return "Pending";
  }
  
  public java.lang.String myReviewsFormFilter_action_all() {
    return "All";
  }
  
  public java.lang.String const_showDetails() {
    return "Show details";
  }
  
  public java.lang.String submission_text_contributionType() {
    return "Contribution type";
  }
  
  public java.lang.String reviewScoreAndComments_title() {
    return "Evaluation of reviewers";
  }
  
  public java.lang.String criteriaList_text_beginByAddingCriterion() {
    return "Please begin by adding criterion";
  }
  
  public java.lang.String conferencePropertyPanel_property_maxReviewsPerReviewer() {
    return "Max. reviews per reviewer";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignConflictTitle() {
    return "These people are in conflict with this contribution";
  }
  
  public java.lang.String editFormArtefact_form_uploadExample() {
    return "Upload example";
  }
  
  public java.lang.String const_publicComment() {
    return "Public comment";
  }
  
  public java.lang.String contributionHeadPanel_error_noTitleEntered() {
    return "You must enter the title of this contribution.";
  }
  
  public java.lang.String contributionHeadPanel_error_noTopicsSelected() {
    return "You must choose at least one topic.";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignNotCareTitle() {
    return "These people do not care";
  }
  
  public java.lang.String myContributionReviews_text_evaluationOfThisArtefact() {
    return "Evaluation of this artefact";
  }
  
  public java.lang.String contributionReviewerForm_text_artifactsOfContribution() {
    return "Artifacts of this contribution";
  }
  
  public java.lang.String editFormArtefact_text_required() {
    return "Required";
  }
  
  public java.lang.String criteriaList_text_poor() {
    return "Poor";
  }
  
  public java.lang.String const_state_accepted() {
    return "Accepted";
  }
  
  public java.lang.String uploadArtefactDialog_text_uploadNewFile() {
    return "Upload new file";
  }
  
  public java.lang.String const_authorsNotVisible() {
    return "The authors are not visible due to the anonymous reviews.";
  }
  
  public java.lang.String editFormContributionType_title() {
    return "Edit Contribution type";
  }
  
  public java.lang.String editFormArtefact_text_direct() {
    return "Direct";
  }
  
  public java.lang.String const_submitContribution() {
    return "Submit contribution";
  }
  
  public java.lang.String reviewCycles_text_noCycles() {
    return "No reviews of artifacts found";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAcceptSubmissionsDescription() {
    return "Permission to accept or decline contributions.";
  }
  
  public java.lang.String editFormReviewCycle_text_noItem() {
    return "No item";
  }
  
  public java.lang.String assignmentReviewersForm_text_nobody() {
    return "Nobody";
  }
  
  public java.lang.String contributionFormArtefactsPanelBuilderUtils_action_setCriterias() {
    return "Set criterias";
  }
  
  public java.lang.String const_type() {
    return "Type";
  }
  
  public java.lang.String submissionReviewsOverview_text_noneNotes() {
    return "None notes";
  }
  
  public java.lang.String editFormCriteria_text_minimumPerAllReviewers() {
    return "Minimal mean per all reviews";
  }
  
  public java.lang.String updateContributionForm_text_submitPanel_description() {
    return "Updated contribution with artefacts of chosen type which will accessible to reviewers and organizers. You may edit only if there is at least one open upload artefacts term.";
  }
  
  public java.lang.String contributionReviewerForm_text_reviewArtifacts() {
    return "Review artifacts of this contribution";
  }
  
  public java.lang.String conferenceModule_text_navigationModelPrefferedTopics() {
    return "Preffered topics";
  }
  
  public java.lang.String fileRecordUI_text_examples() {
    return "Examples";
  }
  
  public java.lang.String updateContributionForm_text_submitPanel_title() {
    return "Update this contribution";
  }
  
  public java.lang.String editFormArtefact_error_problemWithTemplateOrPattern() {
    return "Problem with template or example";
  }
  
  public java.lang.String criteriaList_text_tolerable() {
    return "Tolerable";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAddInternSubmissionsDescription() {
    return "Permission to submit internal contributions.";
  }
  
  public java.lang.String reviewCycles_text_willAutoAccept() {
    return "Artefact will be automatically accepted after the contributor will upload it.";
  }
  
  public java.lang.String createPage_button_upload() {
    return "Upload";
  }
  
  public java.lang.String reviewCycles_text_artefactScoreTitle() {
    return "Artefact score";
  }
  
  public java.lang.String const_state_declined() {
    return "Declined";
  }
  
  public java.lang.String reviewCycles_text_artefactScoreText() {
    return "Artefact score that is average of all evaluations of this artefact";
  }
  
  public java.lang.String artefactList_text_beginByAddingArtefact() {
    return "Please begin by adding artefact";
  }
  
  public java.lang.String createPage_text_uploadItem() {
    return "Upload item";
  }
  
  public java.lang.String reviewCycles_text_artefactIsNotUploaded() {
    return "Artefact has not been uploaded yet.";
  }
  
  public java.lang.String const_exceptionDeleteReviewCriteria() {
    return "Review criteria is already used in submission review.";
  }
  
  public java.lang.String conferenceModule_text_navigationModelMySubmissions() {
    return "My contributions";
  }
  
  public java.lang.String submissionReviewsOverview_title() {
    return "Reviews overview";
  }
  
  public java.lang.String const_commentHelpText() {
    return "Comment from reviewer";
  }
  
  public java.lang.String selectPage_button_select() {
    return "Select";
  }
  
  public java.lang.String criteriaList_text_good() {
    return "Good";
  }
  
  public java.lang.String const_publicNote() {
    return "Public note";
  }
  
  public java.lang.String editFormCriteria_text_maximumValue() {
    return "Maximal value";
  }
  
  public java.lang.String conferenceModule_text_navigationModelSettings() {
    return "Contribution settings";
  }
  
  public java.lang.String criteriaList_action_addCriterion() {
    return "Add criterion";
  }
  
  public java.lang.String editFormCriteria_error_labelsNotUnique() {
    return "Labels are not unique";
  }
  
  public java.lang.String contributionList_title() {
    return "My contributions";
  }
  
  public java.lang.String const_exceptionDeleteRelation() {
    return "DeleteRelationException";
  }
  
  public java.lang.String contributionEvaluationReviewer_title() {
    return "Contribution evaluation - Reviewer";
  }
  
  public java.lang.String const_authors() {
    return "Authors";
  }
  
  public java.lang.String editFormArtefact_form_uploadTemplate() {
    return "Upload template";
  }
  
  public java.lang.String evaluator_text_empty() {
    return "Empty";
  }
  
  public java.lang.String contributionTypeContent_text_beginByAddingContributionType() {
    return "Please begin by adding contribution type";
  }
  
  public java.lang.String reviewsForm_action_evaluate() {
    return "Evaluate";
  }
  
  public java.lang.String newContributionForm_text_submitPanel_description() {
    return "Creates new contribution with artefacts of chosen type which will accessible to reviewers and organizers. You may edit and add further artefacts but only if there is at least one open upload artefacts term.";
  }
  
  public java.lang.String reviewCycle_form_reviewPeriod() {
    return "Review period";
  }
  
  public java.lang.String bidForReviewForm_text_dislike() {
    return "I would dislike to review";
  }
  
  public java.lang.String submissionReviewsOverview_text_mainNotes() {
    return "Main notes";
  }
  
  public java.lang.String const_addTopic() {
    return "Add topic";
  }
  
  public java.lang.String editFormCriteria_title() {
    return "Edit Criterion";
  }
  
  public java.lang.String contributionChairForm_text_assignReviewers() {
    return "Assign reviewers";
  }
  
  public java.lang.String const_showReview() {
    return "Show review";
  }
  
  public java.lang.String evaluationChairForm_title() {
    return "Contribution evalutation - Chair";
  }
  
  public int artefactUpload_int_textBoxWidth() {
    return 265;
  }
  
  public java.lang.String editFormArtefact_form_numReviewers() {
    return "Number of reviewers";
  }
  
  public java.lang.String editFormCriteria_form_points() {
    return "Points";
  }
  
  public java.lang.String const_exceptionSubmissionTypeUnique() {
    return "Name of submission type has to be unique.";
  }
  
  public java.lang.String artefact_text_automaticAccept() {
    return "Automatic accept";
  }
  
  public java.lang.String editFormArtefact_form_documentType() {
    return "Input format";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignLikeTitle() {
    return "These people would like to review this contribution";
  }
  
  public java.lang.String conferenceModule_text_navigationModelBidForReview() {
    return "Bid for review";
  }
  
  public java.lang.String editFormArtefact_action_addExample() {
    return "Add example";
  }
  
  public java.lang.String fileRecordUI_text_templates() {
    return "Templates";
  }
  
  public java.lang.String bidForReviewForm_text_like() {
    return "I would like to review";
  }
  
  public java.lang.String reviewScoreAndComments_partialEvaluationHelp_text() {
    return "Evaluation of specific artifact. Click to see the review.";
  }
  
  public java.lang.String editFormReviewCycle_error_whenFillOneReviewDate() {
    return "Both dates ot the time period must be entered.";
  }
  
  public java.lang.String const_topics() {
    return "Topics";
  }
  
  public java.lang.String reviewsForm_title() {
    return "Reviews";
  }
  
  public java.lang.String editFormCriteria_text_minimumValue() {
    return "Minimal value";
  }
  
  public java.lang.String chairContributionsListItem_action_assignReviewers() {
    return "Assign reviewers";
  }
  
  public java.lang.String conferenceModule_text_moduleRightSubmissionSettings() {
    return "Settings";
  }
  
  public java.lang.String contributionHeadPanel_error_noContributorsEntered() {
    return "You must enter at least one contributor.";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAcceptSubmissions() {
    return "Accept submission";
  }
  
  public java.lang.String newContributionForm_text_artefactsPanel_description() {
    return "Upload the artefacts that represents concrete content that will be accepted or rejected by reviewers. Artefacts are represented by a data file of an allowed file format or by a plain text.  Sometimes there are available templates and examples – please use them and avoid the problems.";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAssignsSubmissionReviewersDescription() {
    return "Permission to assign reviewers to contributions.";
  }
  
  public java.lang.String editFormReviewCycle_title() {
    return "Edit Review cycle";
  }
  
  public java.lang.String conferenceModule_text_moduleName() {
    return "Submission";
  }
  
  public java.lang.String editFormArtefact_text_fileUpload() {
    return "File upload";
  }
  
  public java.lang.String conferenceModule_text_moduleRightReviewer() {
    return "Reviewer";
  }
  
  public java.lang.String myContributionsListItem_action_reviews() {
    return "Reviews";
  }
  
  public java.lang.String artefactItem_text_uploadedTemplates() {
    return "Uploaded templates";
  }
  
  public java.lang.String reviewerEvaluation_text_noEvaluationCriterias() {
    return "The administrator have not enter evaluation criterias.";
  }
  
  public int artefactItem_title_width() {
    return 570;
  }
  
  public java.lang.String contributionTypeContent_action_addContributionType() {
    return "Add contribution type";
  }
  
  public java.lang.String submissionReviewsOverview_text_artefactsNotes() {
    return "Notes of artefacts";
  }
  
  public java.lang.String contributionChairForm_text_evaluateThisReview() {
    return "Evaluate this review";
  }
  
  public java.lang.String conferenceModule_text_navigationModelContributions() {
    return "Contributions";
  }
  
  public java.lang.String myReviewsForm_text_submitted() {
    return "Submitted";
  }
  
  public java.lang.String editFormContributionType_form_internal() {
    return "Internal";
  }
  
  public java.lang.String const_title() {
    return "Title";
  }
  
  public java.lang.String const_exceptionTopicUnique() {
    return "Name of topic has to be unique.";
  }
  
  public java.lang.String reviewerEvaluation_action_sendEvaluation() {
    return "Send evaluation";
  }
  
  public java.lang.String bidForReviewForm_title() {
    return "Bid for review";
  }
  
  public java.lang.String const_exceptionDeleteReviewCycle() {
    return "DeleteReviewCycleException";
  }
  
  public java.lang.String const_notSet() {
    return "Not set";
  }
  
  public java.lang.String artefactUpload_text_confirmTitle() {
    return "Do you really want to remove uploaded artefact?";
  }
  
  public java.lang.String editFormArtefact_form_maxInputWords() {
    return "Maximum input words";
  }
  
  public java.lang.String editFormReviewCycle_error_wrongSubmissionPeriod() {
    return "Submission period is not valid (“from” date is after “to” date).";
  }
  
  public java.lang.String const_privateCommentHelpText() {
    return "";
  }
  
  public java.lang.String contributionHeadPanel_overallScore_helpTitle() {
    return "Final score";
  }
  
  public java.lang.String contributionReviewerHeadPanel_text_reviewer() {
    return "Reviewer";
  }
  
  public java.lang.String evaluationReviewerForm_action_review() {
    return "Review";
  }
  
  public java.lang.String const_exceptionNoRecordsForAction() {
    return "No data could be found for this action.";
  }
  
  public java.lang.String submission_text_warnCantUpdate() {
    return "You can't update your submission at this time.";
  }
  
  public java.lang.String const_evaluationTerm() {
    return "Evaluation term";
  }
  
  public java.lang.String reviewScoreAndComments_partialEvaluationHelp_title() {
    return "Reviewer artefact evaluation";
  }
  
  public java.lang.String editFormReviewCycle_error_reviewEndsBeforeSubmit() {
    return "Review date ends before submission date.";
  }
  
  public java.lang.String unknownNameForm_text_contributionAbstract() {
    return "Abstract";
  }
  
  public java.lang.String const_noSubtitle() {
    return "No subtitle.";
  }
  
  public java.lang.String editFormTopics_text_enteredEmptyValue() {
    return "Empty value cannot be entered.";
  }
  
  public java.lang.String contributionArtefactsChairReviewPanel_text_noArtefacts() {
    return "There are no posted artefacts.";
  }
  
  public java.lang.String reviewsForm_action_assignmentReviewers() {
    return "Assignment of reviewers";
  }
  
  public java.lang.String conferenceModule_text_navigationModelAddSubmission() {
    return "Add Internal Submission";
  }
  
  public java.lang.String contributionTypePanel_text_noArtefactPleaseInformEventAdministrator() {
    return "There are no artefacts assigned. Please notify the event administrator.";
  }
  
  public java.lang.String editFormReviewCycle_error_reviewStartsBeforeSubmit() {
    return "Review date begin before submission date.";
  }
  
  public java.lang.String editFormArtefact_text_fileFormatDelimiterHelp() {
    return "Separate acceptable file formats by a delimiter, for example: pdf,doc,png";
  }
  
  public java.lang.String const_review() {
    return "Review";
  }
  
  public java.lang.String conferenceModule_text_navigationModelSubmission() {
    return "Contributions";
  }
  
  public java.lang.String addPublicAndPrivateComment_text_addPublicComment() {
    return "Add public comment";
  }
  
  public java.lang.String artefactList_action_addArtefact() {
    return "Add artefact";
  }
  
  public java.lang.String reviewScoreAndComments_overallEvaluationHelp_title() {
    return "Reviewer evaluation";
  }
  
  public java.lang.String contributionHeadPanel_state_helpText() {
    return "Describes state of contribution processing. There are these states: accepted, rejected and pending. ";
  }
  
  public java.lang.String addPublicAndPrivateComment_text_addPrivateComment() {
    return "Add private comment";
  }
  
  public java.lang.String editFormArtefact_text_optional() {
    return "Optional";
  }
  
  public java.lang.String myReviewsForm_title() {
    return "My reviews";
  }
  
  public java.lang.String newContributionForm_text_artefactsPanel_title() {
    return "Artefacts upload";
  }
  
  public java.lang.String reviewCycleList_action_addReviewCycle() {
    return "Add review cycle";
  }
  
  public java.lang.String reviewCycles_title() {
    return "Reviews of artifacts";
  }
  
  public java.lang.String const_subtitle() {
    return "Subtitle";
  }
  
  public java.lang.String const_score() {
    return "Score";
  }
}
