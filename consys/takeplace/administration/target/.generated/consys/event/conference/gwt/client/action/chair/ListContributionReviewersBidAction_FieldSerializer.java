package consys.event.conference.gwt.client.action.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListContributionReviewersBidAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction)object);
  }
  
}
