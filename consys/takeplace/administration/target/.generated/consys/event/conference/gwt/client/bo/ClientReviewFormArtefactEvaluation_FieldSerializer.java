package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewFormArtefactEvaluation_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getCriteria(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::criteria;
  }-*/;
  
  private static native void setCriteria(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::criteria = value;
  }-*/;
  
  private static native java.util.HashMap getEvaluation(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::evaluation;
  }-*/;
  
  private static native void setEvaluation(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance, java.util.HashMap value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::evaluation = value;
  }-*/;
  
  private static native java.lang.String getPrivateNote(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::privateNote;
  }-*/;
  
  private static native void setPrivateNote(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::privateNote = value;
  }-*/;
  
  private static native java.lang.String getPublicNote(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::publicNote;
  }-*/;
  
  private static native void setPublicNote(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::publicNote = value;
  }-*/;
  
  private static native java.util.Date getReviewed(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::reviewed;
  }-*/;
  
  private static native void setReviewed(consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation::reviewed = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance) throws SerializationException {
    setCriteria(instance, (java.util.ArrayList) streamReader.readObject());
    setEvaluation(instance, (java.util.HashMap) streamReader.readObject());
    setPrivateNote(instance, streamReader.readString());
    setPublicNote(instance, streamReader.readString());
    setReviewed(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation instance) throws SerializationException {
    streamWriter.writeObject(getCriteria(instance));
    streamWriter.writeObject(getEvaluation(instance));
    streamWriter.writeString(getPrivateNote(instance));
    streamWriter.writeString(getPublicNote(instance));
    streamWriter.writeObject(getReviewed(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientReviewFormArtefactEvaluation)object);
  }
  
}
