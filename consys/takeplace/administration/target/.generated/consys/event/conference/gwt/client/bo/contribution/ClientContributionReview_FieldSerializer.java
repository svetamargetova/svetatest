package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionReview_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData getContribution(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::contribution;
  }-*/;
  
  private static native void setContribution(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::contribution = value;
  }-*/;
  
  private static native java.util.ArrayList getCycles(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::cycles;
  }-*/;
  
  private static native void setCycles(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::cycles = value;
  }-*/;
  
  private static native java.lang.String getPrivateNote(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::privateNote;
  }-*/;
  
  private static native void setPrivateNote(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::privateNote = value;
  }-*/;
  
  private static native java.lang.String getPublicNote(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::publicNote;
  }-*/;
  
  private static native void setPublicNote(consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionReview::publicNote = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance) throws SerializationException {
    setContribution(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData) streamReader.readObject());
    setCycles(instance, (java.util.ArrayList) streamReader.readObject());
    setPrivateNote(instance, streamReader.readString());
    setPublicNote(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.ClientContributionReview();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.ClientContributionReview instance) throws SerializationException {
    streamWriter.writeObject(getContribution(instance));
    streamWriter.writeObject(getCycles(instance));
    streamWriter.writeString(getPrivateNote(instance));
    streamWriter.writeString(getPublicNote(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.ClientContributionReview_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionReview_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.ClientContributionReview)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionReview_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.ClientContributionReview)object);
  }
  
}
