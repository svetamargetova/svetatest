package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionActionData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData getHead(consys.event.conference.gwt.client.bo.ClientContributionActionData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientContributionActionData::head;
  }-*/;
  
  private static native void setHead(consys.event.conference.gwt.client.bo.ClientContributionActionData instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientContributionActionData::head = value;
  }-*/;
  
  private static native java.util.List getReviews(consys.event.conference.gwt.client.bo.ClientContributionActionData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientContributionActionData::reviews;
  }-*/;
  
  private static native void setReviews(consys.event.conference.gwt.client.bo.ClientContributionActionData instance, java.util.List value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientContributionActionData::reviews = value;
  }-*/;
  
  private static native java.util.ArrayList getSelectedCycles(consys.event.conference.gwt.client.bo.ClientContributionActionData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientContributionActionData::selectedCycles;
  }-*/;
  
  private static native void setSelectedCycles(consys.event.conference.gwt.client.bo.ClientContributionActionData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientContributionActionData::selectedCycles = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientContributionActionData instance) throws SerializationException {
    setHead(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData) streamReader.readObject());
    setReviews(instance, (java.util.List) streamReader.readObject());
    setSelectedCycles(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.ClientContributionActionData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientContributionActionData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientContributionActionData instance) throws SerializationException {
    streamWriter.writeObject(getHead(instance));
    streamWriter.writeObject(getReviews(instance));
    streamWriter.writeObject(getSelectedCycles(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientContributionActionData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientContributionActionData_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientContributionActionData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientContributionActionData_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientContributionActionData)object);
  }
  
}
