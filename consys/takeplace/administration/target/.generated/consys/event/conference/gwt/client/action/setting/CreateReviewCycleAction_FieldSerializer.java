package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateReviewCycleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionTypeUuid(consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction::contributionTypeUuid;
  }-*/;
  
  private static native void setContributionTypeUuid(consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction::contributionTypeUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getCycles(consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction::cycles;
  }-*/;
  
  private static native void setCycles(consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction::cycles = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction instance) throws SerializationException {
    setContributionTypeUuid(instance, streamReader.readString());
    setCycles(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction instance) throws SerializationException {
    streamWriter.writeString(getContributionTypeUuid(instance));
    streamWriter.writeObject(getCycles(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction)object);
  }
  
}
