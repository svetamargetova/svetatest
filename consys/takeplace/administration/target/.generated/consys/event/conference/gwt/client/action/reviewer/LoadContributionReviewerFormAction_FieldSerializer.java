package consys.event.conference.gwt.client.action.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadContributionReviewerFormAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction::contributionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction instance) throws SerializationException {
    setContributionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction instance) throws SerializationException {
    streamWriter.writeString(getContributionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction)object);
  }
  
}
