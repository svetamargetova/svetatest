package consys.event.conference.gwt.client.bo.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionChairFormData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getEvaluated(consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData::evaluated;
  }-*/;
  
  private static native void setEvaluated(consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData::evaluated = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead getHead(consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData::head;
  }-*/;
  
  private static native void setHead(consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData instance, consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData::head = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData instance) throws SerializationException {
    setEvaluated(instance, (java.util.ArrayList) streamReader.readObject());
    setHead(instance, (consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData instance) throws SerializationException {
    streamWriter.writeObject(getEvaluated(instance));
    streamWriter.writeObject(getHead(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData)object);
  }
  
}
