package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteTopicAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(consys.event.conference.gwt.client.action.setting.DeleteTopicAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.DeleteTopicAction::id;
  }-*/;
  
  private static native void setId(consys.event.conference.gwt.client.action.setting.DeleteTopicAction instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.DeleteTopicAction::id = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.DeleteTopicAction instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.DeleteTopicAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.DeleteTopicAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.DeleteTopicAction instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.DeleteTopicAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.DeleteTopicAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.DeleteTopicAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.DeleteTopicAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.DeleteTopicAction)object);
  }
  
}
