package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCriteria_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.conference.gwt.client.bo.setting.ClientCriteria instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteria::description;
  }-*/;
  
  private static native void setDescription(consys.event.conference.gwt.client.bo.setting.ClientCriteria instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteria::description = value;
  }-*/;
  
  private static native java.util.ArrayList getPoints(consys.event.conference.gwt.client.bo.setting.ClientCriteria instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteria::points;
  }-*/;
  
  private static native void setPoints(consys.event.conference.gwt.client.bo.setting.ClientCriteria instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteria::points = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientCriteria instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setPoints(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientCriteria instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientCriteria();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientCriteria instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeObject(getPoints(instance));
    
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientCriteria_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteria_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientCriteria)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteria_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientCriteria)object);
  }
  
}
