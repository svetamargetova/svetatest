package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UserTopicsResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getSelectedTopics(consys.event.conference.gwt.client.bo.setting.UserTopicsResult instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.UserTopicsResult::selectedTopics;
  }-*/;
  
  private static native void setSelectedTopics(consys.event.conference.gwt.client.bo.setting.UserTopicsResult instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.UserTopicsResult::selectedTopics = value;
  }-*/;
  
  private static native java.util.ArrayList getTopics(consys.event.conference.gwt.client.bo.setting.UserTopicsResult instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.UserTopicsResult::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.bo.setting.UserTopicsResult instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.UserTopicsResult::topics = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.UserTopicsResult instance) throws SerializationException {
    setSelectedTopics(instance, (java.util.ArrayList) streamReader.readObject());
    setTopics(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.UserTopicsResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.UserTopicsResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.UserTopicsResult instance) throws SerializationException {
    streamWriter.writeObject(getSelectedTopics(instance));
    streamWriter.writeObject(getTopics(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.UserTopicsResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.UserTopicsResult_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.UserTopicsResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.UserTopicsResult_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.UserTopicsResult)object);
  }
  
}
