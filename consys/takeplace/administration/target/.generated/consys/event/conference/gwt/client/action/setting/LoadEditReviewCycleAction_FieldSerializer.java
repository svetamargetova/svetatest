package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadEditReviewCycleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getReviewCycleId(consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction::reviewCycleId;
  }-*/;
  
  private static native void setReviewCycleId(consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction::reviewCycleId = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction instance) throws SerializationException {
    setReviewCycleId(instance, (java.lang.Long) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction instance) throws SerializationException {
    streamWriter.writeObject(getReviewCycleId(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction)object);
  }
  
}
