package consys.event.conference.gwt.client.action.exception;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class TopicUniqueException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.exception.TopicUniqueException instance) throws SerializationException {
    
    consys.common.gwt.shared.action.ActionException_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.exception.TopicUniqueException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.exception.TopicUniqueException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.exception.TopicUniqueException instance) throws SerializationException {
    
    consys.common.gwt.shared.action.ActionException_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.exception.TopicUniqueException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.exception.TopicUniqueException_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.exception.TopicUniqueException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.exception.TopicUniqueException_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.exception.TopicUniqueException)object);
  }
  
}
