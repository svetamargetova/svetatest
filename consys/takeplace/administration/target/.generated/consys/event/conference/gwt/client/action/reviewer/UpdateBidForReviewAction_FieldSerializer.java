package consys.event.conference.gwt.client.action.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateBidForReviewAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum getCh(consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction::ch;
  }-*/;
  
  private static native void setCh(consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction instance, consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction::ch = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction instance) throws SerializationException {
    setCh(instance, (consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction instance) throws SerializationException {
    streamWriter.writeObject(getCh(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction)object);
  }
  
}
