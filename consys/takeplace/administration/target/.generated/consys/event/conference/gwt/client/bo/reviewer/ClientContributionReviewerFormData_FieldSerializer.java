package consys.event.conference.gwt.client.bo.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionReviewerFormData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead getHead(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::head;
  }-*/;
  
  private static native void setHead(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance, consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::head = value;
  }-*/;
  
  private static native boolean getIsAuthor(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::isAuthor;
  }-*/;
  
  private static native void setIsAuthor(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::isAuthor = value;
  }-*/;
  
  private static native java.lang.String getPrivateComment(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::privateComment;
  }-*/;
  
  private static native void setPrivateComment(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::privateComment = value;
  }-*/;
  
  private static native java.lang.String getPublicComment(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::publicComment;
  }-*/;
  
  private static native void setPublicComment(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::publicComment = value;
  }-*/;
  
  private static native java.util.ArrayList getReviewCycles(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::reviewCycles;
  }-*/;
  
  private static native void setReviewCycles(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData::reviewCycles = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance) throws SerializationException {
    setHead(instance, (consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead) streamReader.readObject());
    setIsAuthor(instance, streamReader.readBoolean());
    setPrivateComment(instance, streamReader.readString());
    setPublicComment(instance, streamReader.readString());
    setReviewCycles(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData instance) throws SerializationException {
    streamWriter.writeObject(getHead(instance));
    streamWriter.writeBoolean(getIsAuthor(instance));
    streamWriter.writeString(getPrivateComment(instance));
    streamWriter.writeString(getPublicComment(instance));
    streamWriter.writeObject(getReviewCycles(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData)object);
  }
  
}
