package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCriteriaItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem::id;
  }-*/;
  
  private static native void setId(consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem::id = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    setName(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem)object);
  }
  
}
