package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateContributionTypePositionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.client.rpc.action.UpdateOrderPositionAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.client.rpc.action.UpdateOrderPositionAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction)object);
  }
  
}
