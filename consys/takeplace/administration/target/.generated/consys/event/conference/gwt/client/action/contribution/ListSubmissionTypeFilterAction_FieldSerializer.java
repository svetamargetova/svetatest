package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListSubmissionTypeFilterAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction instance) throws SerializationException {
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction instance) throws SerializationException {
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction)object);
  }
  
}
