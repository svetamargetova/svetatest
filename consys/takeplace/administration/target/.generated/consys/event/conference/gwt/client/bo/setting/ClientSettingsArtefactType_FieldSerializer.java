package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientSettingsArtefactType_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getCriteriaList(consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType::criteriaList;
  }-*/;
  
  private static native void setCriteriaList(consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType::criteriaList = value;
  }-*/;
  
  private static native int getNumReviewers(consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType::numReviewers;
  }-*/;
  
  private static native void setNumReviewers(consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType::numReviewers = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType instance) throws SerializationException {
    setCriteriaList(instance, (java.util.ArrayList) streamReader.readObject());
    setNumReviewers(instance, streamReader.readInt());
    
    consys.event.conference.gwt.client.bo.ClientArtefactType_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType instance) throws SerializationException {
    streamWriter.writeObject(getCriteriaList(instance));
    streamWriter.writeInt(getNumReviewers(instance));
    
    consys.event.conference.gwt.client.bo.ClientArtefactType_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType)object);
  }
  
}
