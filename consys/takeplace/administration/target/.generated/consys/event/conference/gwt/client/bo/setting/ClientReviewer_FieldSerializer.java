package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewer_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum getInterest(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::interest;
  }-*/;
  
  private static native void setInterest(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance, consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::interest = value;
  }-*/;
  
  private static native int getLoad(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::load;
  }-*/;
  
  private static native void setLoad(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::load = value;
  }-*/;
  
  private static native int getMaxLoad(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::maxLoad;
  }-*/;
  
  private static native void setMaxLoad(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::maxLoad = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::name = value;
  }-*/;
  
  private static native boolean getReviewing(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::reviewing;
  }-*/;
  
  private static native void setReviewing(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::reviewing = value;
  }-*/;
  
  private static native java.util.ArrayList getTopics(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::topics = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.bo.setting.ClientReviewer instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientReviewer::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) throws SerializationException {
    setInterest(instance, (consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum) streamReader.readObject());
    setLoad(instance, streamReader.readInt());
    setMaxLoad(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    setReviewing(instance, streamReader.readBoolean());
    setTopics(instance, (java.util.ArrayList) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientReviewer instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientReviewer();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientReviewer instance) throws SerializationException {
    streamWriter.writeObject(getInterest(instance));
    streamWriter.writeInt(getLoad(instance));
    streamWriter.writeInt(getMaxLoad(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeBoolean(getReviewing(instance));
    streamWriter.writeObject(getTopics(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientReviewer_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientReviewer_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientReviewer)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientReviewer_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientReviewer)object);
  }
  
}
