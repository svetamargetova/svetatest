package consys.event.conference.gwt.client.bo.evaluation;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ReviewScoreAndCommentsData_Array_Rank_1_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData[] instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int size = streamReader.readInt();
    return new consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData[size];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData_Array_Rank_1_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData_Array_Rank_1_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData[])object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData_Array_Rank_1_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData[])object);
  }
  
}
