package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateContributionTypeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb getThumb(consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction::thumb;
  }-*/;
  
  private static native void setThumb(consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction::thumb = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction instance) throws SerializationException {
    setThumb(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction instance) throws SerializationException {
    streamWriter.writeObject(getThumb(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction)object);
  }
  
}
