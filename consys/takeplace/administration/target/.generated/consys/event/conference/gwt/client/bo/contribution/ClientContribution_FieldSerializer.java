package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContribution_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAuthors(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::authors = value;
  }-*/;
  
  private static native java.util.ArrayList getSubmitters(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::submitters;
  }-*/;
  
  private static native void setSubmitters(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::submitters = value;
  }-*/;
  
  private static native java.lang.String getSubtitle(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::subtitle;
  }-*/;
  
  private static native void setSubtitle(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::subtitle = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::title;
  }-*/;
  
  private static native void setTitle(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::title = value;
  }-*/;
  
  private static native java.util.ArrayList getTopics(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::topics = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.bo.contribution.ClientContribution instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContribution::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) throws SerializationException {
    setAuthors(instance, streamReader.readString());
    setSubmitters(instance, (java.util.ArrayList) streamReader.readObject());
    setSubtitle(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    setTopics(instance, (java.util.ArrayList) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.ClientContribution instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.ClientContribution();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.ClientContribution instance) throws SerializationException {
    streamWriter.writeString(getAuthors(instance));
    streamWriter.writeObject(getSubmitters(instance));
    streamWriter.writeString(getSubtitle(instance));
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeObject(getTopics(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.ClientContribution_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContribution_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.ClientContribution)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContribution_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.ClientContribution)object);
  }
  
}
