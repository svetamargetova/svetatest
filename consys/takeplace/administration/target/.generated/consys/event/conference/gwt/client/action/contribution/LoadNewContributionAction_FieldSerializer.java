package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadNewContributionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getInternal(consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction::internal;
  }-*/;
  
  private static native void setInternal(consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction::internal = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction instance) throws SerializationException {
    setInternal(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction instance) throws SerializationException {
    streamWriter.writeBoolean(getInternal(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction)object);
  }
  
}
