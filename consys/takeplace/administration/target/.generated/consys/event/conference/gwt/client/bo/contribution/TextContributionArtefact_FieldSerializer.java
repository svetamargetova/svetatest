package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class TextContributionArtefact_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactText(consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact::artefactText;
  }-*/;
  
  private static native void setArtefactText(consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact::artefactText = value;
  }-*/;
  
  private static native java.lang.String getArtefactTypeUuid(consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact::artefactTypeUuid;
  }-*/;
  
  private static native void setArtefactTypeUuid(consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact::artefactTypeUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact instance) throws SerializationException {
    setArtefactText(instance, streamReader.readString());
    setArtefactTypeUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact instance) throws SerializationException {
    streamWriter.writeString(getArtefactText(instance));
    streamWriter.writeString(getArtefactTypeUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact)object);
  }
  
}
