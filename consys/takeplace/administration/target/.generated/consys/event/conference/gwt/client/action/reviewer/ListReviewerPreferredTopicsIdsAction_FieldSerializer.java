package consys.event.conference.gwt.client.action.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListReviewerPreferredTopicsIdsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction instance) throws SerializationException {
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction instance) throws SerializationException {
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction)object);
  }
  
}
