package consys.event.conference.gwt.client.message;

public class EventConferenceConstants_cs implements consys.event.conference.gwt.client.message.EventConferenceConstants {
  
  public java.lang.String reviewCycleList_title() {
    return "Cykly recenzování";
  }
  
  public java.lang.String const_publicCommentHelpText() {
    return "";
  }
  
  public java.lang.String editFormArtefact_text_zeroIsUnlimited() {
    return "0 je neomezeno";
  }
  
  public java.lang.String reviewScoreAndComments_overallEvaluationHelp_text() {
    return "Celkové hodnocení recenzenta, které je průměrem všech hodnocených artefaktů. Kliknutím na hodnocení zobrazíte celou recenzi.";
  }
  
  public java.lang.String newContributionForm_text_submitPanel_title() {
    return "Pošlete tento příspěvek";
  }
  
  public java.lang.String editFormReviewCycle_error_submissionPeriodDatesMustBeSet() {
    return "Datumy pro termín zasílání musí být zadány.";
  }
  
  public java.lang.String reviewCycleList_text_beginByAddingReviewCycle() {
    return "Prosím začněte přidáním cyklu recenzování";
  }
  
  public java.lang.String const_artefacts() {
    return "Artefakty";
  }
  
  public java.lang.String contributionHeadPanel_state_helpTitle() {
    return "Stav příspěvku";
  }
  
  public java.lang.String contributionHeadPanel_overallScore_helpText() {
    return "Výsledné hodnocení příspěvku, které je průměrem všech hodnocení jednolitvých recenzentů.";
  }
  
  public java.lang.String editFormArtefact_action_addTemplate() {
    return "Přidat šablonu";
  }
  
  public java.lang.String evaluationChairForm_text_minLower() {
    return "min";
  }
  
  public java.lang.String const_file() {
    return "Soubor";
  }
  
  public java.lang.String criteriaList_text_excellent() {
    return "Skvělý";
  }
  
  public java.lang.String conferenceModule_text_moduleRightReviewerDescription() {
    return "Právo hodnotit příspěvky";
  }
  
  public java.lang.String const_actionUrlFail() {
    return "Nevytvořilo se url pro vykonání akce";
  }
  
  public java.lang.String reviewCycleList_text_newReviewCycle() {
    return "Nový cyklus recenzování";
  }
  
  public java.lang.String conferencePropertyPanel_property_visibleAuthors() {
    return "Anonymní recenzování";
  }
  
  public java.lang.String evaluationReviewerForm_text_notes() {
    return "Poznámky";
  }
  
  public java.lang.String reviewerEvaluation_helpText_totalScore() {
    return "Hodnota je určena jako průměr ze všech kriterií";
  }
  
  public java.lang.String editFormArtefact_form_necessity() {
    return "Vyžadován";
  }
  
  public java.lang.String newContributionForm_text_termClosed() {
    return "Termín pro zaslání tohoto typu příspěvku je již uzavřen.";
  }
  
  public java.lang.String conferenceModule_text_moduleRightSubmissionSettingsDescription() {
    return "Právo prohlížet a zadávat nastavení.";
  }
  
  public java.lang.String criteriaList_text_newCriterion() {
    return "Nové kritérium";
  }
  
  public java.lang.String preferredTopicSelectForm_title() {
    return "Preferovaná témata";
  }
  
  public java.lang.String const_state() {
    return "Stav";
  }
  
  public java.lang.String const_submitDate() {
    return "Datum odeslání";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignNotWantTitle() {
    return "Tito lidé nechtějí příspěvek hodnotit";
  }
  
  public java.lang.String evaluationReviewerForm_text_cyclePeriodOutOfDate() {
    return "cyklus recenzování je mimo přednastavený termín";
  }
  
  public java.lang.String artefactFileUpload_error_artefactUploadFailed() {
    return "Nezdařil se upload artefaktu";
  }
  
  public java.lang.String editFormReviewCycle_form_submissionPeriod() {
    return "Termín zasílání";
  }
  
  public java.lang.String artefactUpload_action_remove() {
    return "Odstranit";
  }
  
  public java.lang.String contributionHeadPanel_partialScore_helpText() {
    return "Hodnoceni příspěvku konkrétním recenzentem. Kliknutím na hodnocení zobrazíte detail hodnocení recenzentem.";
  }
  
  public java.lang.String evaluationReviewerForm_action_updateNotes() {
    return "Aktualizovat poznámky";
  }
  
  public java.lang.String reviewCycles_text_wasAutoAccept() {
    return "Artefakt je automaticky přijat.";
  }
  
  public java.lang.String evaluationReviewerForm_text_whitoutEvaluationTerm() {
    return "Bez recenzenčního cyklu";
  }
  
  public java.lang.String contributionHeadPanel_error_noAuthorsEntered() {
    return "Musíte zadat alespoň jednoho autora příspěvku.";
  }
  
  public java.lang.String bidForReviewForm_text_notMind() {
    return "Můžu hodnotit";
  }
  
  public java.lang.String addPublicAndPrivateComment_text_updateComments() {
    return "Aktualizovat komentáře";
  }
  
  public java.lang.String contributionTypeContent_text_newContributionType() {
    return "Nový typ příspěvku";
  }
  
  public java.lang.String criteriaList_text_adequate() {
    return "Dostatečný";
  }
  
  public java.lang.String artefactItem_text_uploadedExamples() {
    return "Nahrané příklady";
  }
  
  public java.lang.String contributionTypeContent_text_contributionTypes() {
    return "Typy příspěvku";
  }
  
  public java.lang.String conferenceModule_text_navigationModelMyReviews() {
    return "Má hodnocení";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAssignsSubmissionReviewers() {
    return "Přidat hodnotitele příspěvku";
  }
  
  public java.lang.String editFormArtefact_title_editArtefact() {
    return "Upravit artefakt";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAddInternSubmissions() {
    return "Zaslat interní příspěvek";
  }
  
  public int changeArtefactUpload_button_width() {
    return 64;
  }
  
  public java.lang.String artefactList_text_newArtefact() {
    return "Nový artefakt";
  }
  
  public java.lang.String contributionReviewerForm_error_reviewNotFound() {
    return "Recenze nenalezena";
  }
  
  public java.lang.String contributionFormArtefactsPanelBuilder_text_noArtefactInCycle() {
    return "Do cyklu nebyl nastaven artefakt";
  }
  
  public java.lang.String const_automaticallyAccepted() {
    return "Automaticky přijato";
  }
  
  public java.lang.String submission_action_addContributor() {
    return "Přidat předkladatele";
  }
  
  public java.lang.String evaluationReviewerForm_button_sendEvaluation() {
    return "Zaslat hodnocení";
  }
  
  public java.lang.String contributionTypeContent_title() {
    return "Nastavení příspěvků";
  }
  
  public java.lang.String myContributionReviews_text_evaluateThisArtefact() {
    return "Ohodnoťte tento artefakt";
  }
  
  public java.lang.String criteriaList_title() {
    return "Kritéria";
  }
  
  public java.lang.String const_exceptionNotInCache() {
    return "NotInCacheException";
  }
  
  public java.lang.String reviewerEvaluation_text_youMustEnterAllEvaluationOfArtefact() {
    return "Je potřeba zdat všechna hodnocení kriterií artefaktu";
  }
  
  public java.lang.String editFormReviewCycle_text_noArtefactAdded() {
    return "Začněte přidáním artefaktu";
  }
  
  public java.lang.String evaluationReviewerForm_text_artefactNotUpladedYet() {
    return "Artefakt zatím nebyl nahrán";
  }
  
  public java.lang.String evaluationReviewerForm_text_showReview() {
    return "Zobrazit recenzi";
  }
  
  public java.lang.String submission_action_addSubtitle() {
    return "Přidat podtitulek";
  }
  
  public java.lang.String newContributionForm_title() {
    return "Nový příspěvek";
  }
  
  public java.lang.String reviewerEvaluation_helpTitle_totalScore() {
    return "Celkové hodnocení artefaktu";
  }
  
  public java.lang.String evaluationReviewerForm_text_evaluationTerm() {
    return "Termín hodnocení";
  }
  
  public java.lang.String const_comment() {
    return "Komentář";
  }
  
  public java.lang.String const_privateComment() {
    return "Soukromý komentář";
  }
  
  public java.lang.String reviewCycles_title_view() {
    return "Zobrazit přehled recenzí artefaktů";
  }
  
  public java.lang.String contributionHeadPanel_partialScore_helpTitle() {
    return "Hodnoceni příspěvku recenzentem";
  }
  
  public java.lang.String bidForReviewForm_action_interest() {
    return "Zájem";
  }
  
  public java.lang.String artefactUpload_action_change() {
    return "Změnit";
  }
  
  public java.lang.String const_privateNote() {
    return "Poznámka pro recenzenty";
  }
  
  public java.lang.String uploadArtefactDialog_text_existingFiles() {
    return "Existující soubory";
  }
  
  public java.lang.String contributionChairForm_text_noReviewers() {
    return "K tomuto příspěvku nejsou přiřazeni žádní recenzenti.";
  }
  
  public java.lang.String bidForReviewForm_text_conflict() {
    return "Konflikt";
  }
  
  public java.lang.String const_contributors() {
    return "Přispěvatelé";
  }
  
  public java.lang.String editFormCriteria_text_minimumPoints() {
    return "Minimum bodů";
  }
  
  public java.lang.String artefact_text_fileTypes() {
    return "Typy souborů";
  }
  
  public java.lang.String editFormReviewCycle_error_wrongReviewPeriod() {
    return "Termín recenzování je neplatný (datum “od” je za datumem “do”).";
  }
  
  public java.lang.String const_state_pending() {
    return "Zpracováván";
  }
  
  public java.lang.String myReviewsFormFilter_action_all() {
    return "Vše";
  }
  
  public java.lang.String const_showDetails() {
    return "Zobraz detaily";
  }
  
  public java.lang.String submission_text_contributionType() {
    return "Typ příspěvku";
  }
  
  public java.lang.String reviewScoreAndComments_title() {
    return "Hodnocení recenzentů";
  }
  
  public java.lang.String criteriaList_text_beginByAddingCriterion() {
    return "Prosím začněte přidáním kritéria";
  }
  
  public java.lang.String conferencePropertyPanel_property_maxReviewsPerReviewer() {
    return "Max. recenzí na recenzenta";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignConflictTitle() {
    return "Tito lidé jsou v konfliktu zájmů";
  }
  
  public java.lang.String editFormArtefact_form_uploadExample() {
    return "Nahrát příklad";
  }
  
  public java.lang.String const_publicComment() {
    return "Veřejný komentář";
  }
  
  public java.lang.String contributionHeadPanel_error_noTitleEntered() {
    return "Musíte zadat název příspěvku.";
  }
  
  public java.lang.String contributionHeadPanel_error_noTopicsSelected() {
    return "Musíte vybrat alespoň jednu tému příspevku.";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignNotCareTitle() {
    return "Tito lidé nejsou rozhodnuti";
  }
  
  public java.lang.String myContributionReviews_text_evaluationOfThisArtefact() {
    return "Hodnocení tohoto artefaktu";
  }
  
  public java.lang.String contributionReviewerForm_text_artifactsOfContribution() {
    return "Artefakty tohoto příspěvku";
  }
  
  public java.lang.String editFormArtefact_text_required() {
    return "Povinne";
  }
  
  public java.lang.String criteriaList_text_poor() {
    return "Nedostatečný";
  }
  
  public java.lang.String const_state_accepted() {
    return "Přijat";
  }
  
  public java.lang.String uploadArtefactDialog_text_uploadNewFile() {
    return "Nahrát nový soubor";
  }
  
  public java.lang.String const_authorsNotVisible() {
    return "Autoři nejsou viditelní z důvodu anonymního recenzování";
  }
  
  public java.lang.String editFormContributionType_title() {
    return "Upravit typ příspěvku";
  }
  
  public java.lang.String editFormArtefact_text_direct() {
    return "Přímý";
  }
  
  public java.lang.String const_submitContribution() {
    return "Vložit příspěvek";
  }
  
  public java.lang.String reviewCycles_text_noCycles() {
    return "Nebyly nalezeny žádné recenze artefaktů";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAcceptSubmissionsDescription() {
    return "Právo přijímat nebo odmítat příspěvky.";
  }
  
  public java.lang.String editFormReviewCycle_text_noItem() {
    return "Žádná položka";
  }
  
  public java.lang.String assignmentReviewersForm_text_nobody() {
    return "Nikdo";
  }
  
  public java.lang.String contributionFormArtefactsPanelBuilderUtils_action_setCriterias() {
    return "Nastavit kritéria";
  }
  
  public java.lang.String const_type() {
    return "Typ";
  }
  
  public java.lang.String submissionReviewsOverview_text_noneNotes() {
    return "Žádné poznámky";
  }
  
  public java.lang.String editFormCriteria_text_minimumPerAllReviewers() {
    return "Minimální průměr za všechny recenzenty";
  }
  
  public java.lang.String updateContributionForm_text_submitPanel_description() {
    return "Aktualizuje příspěvek, obsahující Vámi vložené artefakty, který bude poskytnut recenzentům a organizátorům. Můžete upravovat a přidávat další artefakty pokud je otevřen alespoň jeden termín zasílání artefaktů.";
  }
  
  public java.lang.String contributionReviewerForm_text_reviewArtifacts() {
    return "Recenzujte artefakty tohoto příspěvku";
  }
  
  public java.lang.String conferenceModule_text_navigationModelPrefferedTopics() {
    return "Preferovaná témata";
  }
  
  public java.lang.String fileRecordUI_text_examples() {
    return "Příklady";
  }
  
  public java.lang.String updateContributionForm_text_submitPanel_title() {
    return "Aktualizuje tento příspěvek";
  }
  
  public java.lang.String editFormArtefact_error_problemWithTemplateOrPattern() {
    return "Problém se šablonou nebo příkladem";
  }
  
  public java.lang.String criteriaList_text_tolerable() {
    return "Slabý";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAddInternSubmissionsDescription() {
    return "Právo zaslat interní příspěvek.";
  }
  
  public java.lang.String reviewCycles_text_willAutoAccept() {
    return "Artefakt bude automaticky přijmutý, po nahrátí předkladatelem.";
  }
  
  public java.lang.String createPage_button_upload() {
    return "Nahrát";
  }
  
  public java.lang.String reviewCycles_text_artefactScoreTitle() {
    return "Hodnocení artefaktu";
  }
  
  public java.lang.String const_state_declined() {
    return "Odmítnut";
  }
  
  public java.lang.String reviewCycles_text_artefactScoreText() {
    return "Jedná se o průměrnou hodnotu z hodnocení tohoto artefaktu";
  }
  
  public java.lang.String artefactList_text_beginByAddingArtefact() {
    return "Prosím začněte přidáním artefaktu";
  }
  
  public java.lang.String createPage_text_uploadItem() {
    return "Nahrát položku";
  }
  
  public java.lang.String reviewCycles_text_artefactIsNotUploaded() {
    return "Artefakt zatím nebyl nahrán na server.";
  }
  
  public java.lang.String const_exceptionDeleteReviewCriteria() {
    return "DeleteReviewCriteriaException";
  }
  
  public java.lang.String conferenceModule_text_navigationModelMySubmissions() {
    return "Mé příspěvky";
  }
  
  public java.lang.String submissionReviewsOverview_title() {
    return "Přehled hodnocení";
  }
  
  public java.lang.String const_commentHelpText() {
    return "Komentář od recenzenta";
  }
  
  public java.lang.String selectPage_button_select() {
    return "Vybrat";
  }
  
  public java.lang.String criteriaList_text_good() {
    return "Dobrý";
  }
  
  public java.lang.String const_publicNote() {
    return "Poznámka pro přispěvatele";
  }
  
  public java.lang.String editFormCriteria_text_maximumValue() {
    return "Maximální hodnota";
  }
  
  public java.lang.String conferenceModule_text_navigationModelSettings() {
    return "Nastavení příspěvků";
  }
  
  public java.lang.String criteriaList_action_addCriterion() {
    return "Přidat kritérium";
  }
  
  public java.lang.String editFormCriteria_error_labelsNotUnique() {
    return "Popisky nejsou unikátní";
  }
  
  public java.lang.String contributionList_title() {
    return "Mé příspěvky";
  }
  
  public java.lang.String const_exceptionDeleteRelation() {
    return "DeleteRelationException";
  }
  
  public java.lang.String contributionEvaluationReviewer_title() {
    return "Hodnocení příspěvku - Recenzent";
  }
  
  public java.lang.String const_authors() {
    return "Autoři";
  }
  
  public java.lang.String editFormArtefact_form_uploadTemplate() {
    return "Nahrát šablonu";
  }
  
  public java.lang.String evaluator_text_empty() {
    return "Prázdný";
  }
  
  public java.lang.String contributionTypeContent_text_beginByAddingContributionType() {
    return "Prosím začněte přidáním typu příspěvku";
  }
  
  public java.lang.String reviewsForm_action_evaluate() {
    return "Hodnotit";
  }
  
  public java.lang.String newContributionForm_text_submitPanel_description() {
    return "Vytvoří nový příspěvek, obsahující Vámi vložené artefakty, který bude poskytnut recenzentům a organizátorům. Po odeslání můžete upravovat a přidávat další artefakty pokud je otevřen alespoň jeden termín zasílání artefaktů.";
  }
  
  public java.lang.String reviewCycle_form_reviewPeriod() {
    return "Termín recenzování";
  }
  
  public java.lang.String bidForReviewForm_text_dislike() {
    return "Nechci hodnotit";
  }
  
  public java.lang.String submissionReviewsOverview_text_mainNotes() {
    return "Hlavní poznámky";
  }
  
  public java.lang.String const_addTopic() {
    return "Přidat téma";
  }
  
  public java.lang.String editFormCriteria_title() {
    return "Upravit kritérium";
  }
  
  public java.lang.String contributionChairForm_text_assignReviewers() {
    return "Přiřadit recenzenty";
  }
  
  public java.lang.String const_showReview() {
    return "Zobraz recenzi";
  }
  
  public java.lang.String evaluationChairForm_title() {
    return "Hodnocení příspěvku - Předseda";
  }
  
  public int artefactUpload_int_textBoxWidth() {
    return 268;
  }
  
  public java.lang.String editFormArtefact_form_numReviewers() {
    return "Počet recenzentů";
  }
  
  public java.lang.String editFormCriteria_form_points() {
    return "Body";
  }
  
  public java.lang.String const_exceptionSubmissionTypeUnique() {
    return "SubmissionTypeUniqueException";
  }
  
  public java.lang.String artefact_text_automaticAccept() {
    return "Automaticky přijmout";
  }
  
  public java.lang.String editFormArtefact_form_documentType() {
    return "Typ vstupu";
  }
  
  public java.lang.String assignmentReviewersForm_text_assignLikeTitle() {
    return "Tito lidé chtějí příspěvek hodnotit";
  }
  
  public java.lang.String conferenceModule_text_navigationModelBidForReview() {
    return "Zájem o hodnocení";
  }
  
  public java.lang.String editFormArtefact_action_addExample() {
    return "Přidat příklad";
  }
  
  public java.lang.String fileRecordUI_text_templates() {
    return "Šablony";
  }
  
  public java.lang.String bidForReviewForm_text_like() {
    return "Chci hodnotit";
  }
  
  public java.lang.String reviewScoreAndComments_partialEvaluationHelp_text() {
    return "Hodnocení konkrétního artefaktu recenzentem. Kliknutím na hodnocení zobrazíte celou recenzi.";
  }
  
  public java.lang.String editFormReviewCycle_error_whenFillOneReviewDate() {
    return "Musí být zadány oba datumy termínu.";
  }
  
  public java.lang.String const_topics() {
    return "Témata";
  }
  
  public java.lang.String reviewsForm_title() {
    return "Recenze";
  }
  
  public java.lang.String editFormCriteria_text_minimumValue() {
    return "Minimální hodnota";
  }
  
  public java.lang.String chairContributionsListItem_action_assignReviewers() {
    return "Přiřadit recenzenty";
  }
  
  public java.lang.String conferenceModule_text_moduleRightSubmissionSettings() {
    return "Nastavení";
  }
  
  public java.lang.String contributionHeadPanel_error_noContributorsEntered() {
    return "Musíte zadat alespoň jednoho přispěvovatele.";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAcceptSubmissions() {
    return "Přijmout příspěvek";
  }
  
  public java.lang.String newContributionForm_text_artefactsPanel_description() {
    return "Nahrajte artefakty, které představují konkrétní recenzovaný obsah. Artefakty jsou reprezentovány datovým souborem povoleného formátu, anebo jednoduchým textem. Artefakty můžou obsahovat šablony a ukázková použití šablon – využijte je prosím a předcházejte tím problémům.";
  }
  
  public java.lang.String conferenceModule_text_moduleRightAssignsSubmissionReviewersDescription() {
    return "Právo přidávat hodnotitele k příspěvkům.";
  }
  
  public java.lang.String editFormReviewCycle_title() {
    return "Upravit cyklus recenzování";
  }
  
  public java.lang.String conferenceModule_text_moduleName() {
    return "Příspěvek";
  }
  
  public java.lang.String editFormArtefact_text_fileUpload() {
    return "Soubor";
  }
  
  public java.lang.String conferenceModule_text_moduleRightReviewer() {
    return "Hodnotitel";
  }
  
  public java.lang.String myContributionsListItem_action_reviews() {
    return "Recenze";
  }
  
  public java.lang.String artefactItem_text_uploadedTemplates() {
    return "Nahrané šablony";
  }
  
  public java.lang.String reviewerEvaluation_text_noEvaluationCriterias() {
    return "Administrátor nezadal kritéria hodnocení.";
  }
  
  public int artefactItem_title_width() {
    return 540;
  }
  
  public java.lang.String contributionTypeContent_action_addContributionType() {
    return "Přidat typ příspěvku";
  }
  
  public java.lang.String submissionReviewsOverview_text_artefactsNotes() {
    return "Poznámky artefaktů";
  }
  
  public java.lang.String contributionChairForm_text_evaluateThisReview() {
    return "Zhodnoťte tuto recenzi";
  }
  
  public java.lang.String conferenceModule_text_navigationModelContributions() {
    return "Příspěvky";
  }
  
  public java.lang.String myReviewsForm_text_submitted() {
    return "Submitted";
  }
  
  public java.lang.String editFormContributionType_form_internal() {
    return "Interní";
  }
  
  public java.lang.String const_title() {
    return "Název";
  }
  
  public java.lang.String const_exceptionTopicUnique() {
    return "Téma musí být unikátní.";
  }
  
  public java.lang.String reviewerEvaluation_action_sendEvaluation() {
    return "Odeslat hodnocení";
  }
  
  public java.lang.String bidForReviewForm_title() {
    return "Zájem o hodnocení";
  }
  
  public java.lang.String const_exceptionDeleteReviewCycle() {
    return "DeleteReviewCycleException";
  }
  
  public java.lang.String const_notSet() {
    return "Nenastaveno";
  }
  
  public java.lang.String artefactUpload_text_confirmTitle() {
    return "Opravdu si přejete odstranit nahraný artefakt?";
  }
  
  public java.lang.String editFormArtefact_form_maxInputWords() {
    return "Maximum slov";
  }
  
  public java.lang.String editFormReviewCycle_error_wrongSubmissionPeriod() {
    return "Termín zasílání je neplatný (datum “od” je za datumem “do”).";
  }
  
  public java.lang.String const_privateCommentHelpText() {
    return "";
  }
  
  public java.lang.String contributionHeadPanel_overallScore_helpTitle() {
    return "Výsledné hodnocení příspěvku";
  }
  
  public java.lang.String contributionReviewerHeadPanel_text_reviewer() {
    return "Recenzent";
  }
  
  public java.lang.String evaluationReviewerForm_action_review() {
    return "Recenzovat";
  }
  
  public java.lang.String const_exceptionNoRecordsForAction() {
    return "Nebyla nalezena žádná data pro tuto akci.";
  }
  
  public java.lang.String submission_text_warnCantUpdate() {
    return "V tomto čase nemůžete aktualizovat příspěvek.";
  }
  
  public java.lang.String const_evaluationTerm() {
    return "Termín hodnocení";
  }
  
  public java.lang.String reviewScoreAndComments_partialEvaluationHelp_title() {
    return "Hodnocení artefaktu recenzentem";
  }
  
  public java.lang.String editFormReviewCycle_error_reviewEndsBeforeSubmit() {
    return "Datum recenzování končí dřív než datum konce podání.";
  }
  
  public java.lang.String unknownNameForm_text_contributionAbstract() {
    return "Abstrakt";
  }
  
  public java.lang.String const_noSubtitle() {
    return "Žádný podtitulek.";
  }
  
  public java.lang.String editFormTopics_text_enteredEmptyValue() {
    return "Nemůže být vložena prázdná hodnota.";
  }
  
  public java.lang.String contributionArtefactsChairReviewPanel_text_noArtefacts() {
    return "Příspěvek neobsahuje žádné artefakty.";
  }
  
  public java.lang.String reviewsForm_action_assignmentReviewers() {
    return "Přiřazení recenzentů";
  }
  
  public java.lang.String conferenceModule_text_navigationModelAddSubmission() {
    return "Přidat interní příspěvek";
  }
  
  public java.lang.String contributionTypePanel_text_noArtefactPleaseInformEventAdministrator() {
    return "Nejsou přiřazeny žádné artefakty. Prosím informujte administrátora akce.";
  }
  
  public java.lang.String editFormReviewCycle_error_reviewStartsBeforeSubmit() {
    return "Datum recenzování začíná dřív než datum začátku podání.";
  }
  
  public java.lang.String editFormArtefact_text_fileFormatDelimiterHelp() {
    return "Oddělujte formáty souborů čárkou, např.: pdf,doc,png";
  }
  
  public java.lang.String const_review() {
    return "Recenzuj";
  }
  
  public java.lang.String conferenceModule_text_navigationModelSubmission() {
    return "Příspěvky";
  }
  
  public java.lang.String addPublicAndPrivateComment_text_addPublicComment() {
    return "Přidat veřejný komentář";
  }
  
  public java.lang.String artefactList_action_addArtefact() {
    return "Přidat artefakt";
  }
  
  public java.lang.String reviewScoreAndComments_overallEvaluationHelp_title() {
    return "Hodnocení recenzenta";
  }
  
  public java.lang.String contributionHeadPanel_state_helpText() {
    return "Informace v jaké fázi spracování se příspěvek nachází. Stavy příspěvku jsou: přijatý, odmitnutý a spracovávan. ";
  }
  
  public java.lang.String addPublicAndPrivateComment_text_addPrivateComment() {
    return "Přidat soukromý komentář";
  }
  
  public java.lang.String editFormArtefact_text_optional() {
    return "Volitelne";
  }
  
  public java.lang.String myReviewsForm_title() {
    return "Mé recenze";
  }
  
  public java.lang.String newContributionForm_text_artefactsPanel_title() {
    return "Nahrání artefaktů";
  }
  
  public java.lang.String reviewCycleList_action_addReviewCycle() {
    return "Přidat cyklus recenzování";
  }
  
  public java.lang.String reviewCycles_title() {
    return "Přehled recenzí artefaktů";
  }
  
  public java.lang.String const_subtitle() {
    return "Podtitulek";
  }
  
  public java.lang.String const_score() {
    return "Hodnocení";
  }
}
