package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewFormCycle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getArtefacts(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::artefacts;
  }-*/;
  
  private static native void setArtefacts(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::artefacts = value;
  }-*/;
  
  private static native boolean getCanReview(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::canReview;
  }-*/;
  
  private static native void setCanReview(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::canReview = value;
  }-*/;
  
  private static native java.util.Date getReviewFrom(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::reviewFrom;
  }-*/;
  
  private static native void setReviewFrom(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::reviewFrom = value;
  }-*/;
  
  private static native java.util.Date getReviewTo(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::reviewTo;
  }-*/;
  
  private static native void setReviewTo(consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycle::reviewTo = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance) throws SerializationException {
    setArtefacts(instance, (java.util.ArrayList) streamReader.readObject());
    setCanReview(instance, streamReader.readBoolean());
    setReviewFrom(instance, (java.util.Date) streamReader.readObject());
    setReviewTo(instance, (java.util.Date) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.ClientReviewFormCycle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientReviewFormCycle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientReviewFormCycle instance) throws SerializationException {
    streamWriter.writeObject(getArtefacts(instance));
    streamWriter.writeBoolean(getCanReview(instance));
    streamWriter.writeObject(getReviewFrom(instance));
    streamWriter.writeObject(getReviewTo(instance));
    
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientReviewFormCycle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviewFormCycle_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientReviewFormCycle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviewFormCycle_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientReviewFormCycle)object);
  }
  
}
