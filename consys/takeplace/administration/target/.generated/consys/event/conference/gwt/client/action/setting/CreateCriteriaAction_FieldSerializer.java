package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateCriteriaAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactUuid(consys.event.conference.gwt.client.action.setting.CreateCriteriaAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateCriteriaAction::artefactUuid;
  }-*/;
  
  private static native void setArtefactUuid(consys.event.conference.gwt.client.action.setting.CreateCriteriaAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateCriteriaAction::artefactUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getCriteria(consys.event.conference.gwt.client.action.setting.CreateCriteriaAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateCriteriaAction::criteria;
  }-*/;
  
  private static native void setCriteria(consys.event.conference.gwt.client.action.setting.CreateCriteriaAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateCriteriaAction::criteria = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.CreateCriteriaAction instance) throws SerializationException {
    setArtefactUuid(instance, streamReader.readString());
    setCriteria(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.CreateCriteriaAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.CreateCriteriaAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.CreateCriteriaAction instance) throws SerializationException {
    streamWriter.writeString(getArtefactUuid(instance));
    streamWriter.writeObject(getCriteria(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.CreateCriteriaAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateCriteriaAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.CreateCriteriaAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateCriteriaAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.CreateCriteriaAction)object);
  }
  
}
