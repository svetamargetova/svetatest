package consys.event.conference.gwt.client.action.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateContributionCommentAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction::contributionUuid = value;
  }-*/;
  
  private static native java.lang.String getPrivateComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction::privateComment;
  }-*/;
  
  private static native void setPrivateComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction::privateComment = value;
  }-*/;
  
  private static native java.lang.String getPublicComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction::publicComment;
  }-*/;
  
  private static native void setPublicComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction::publicComment = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance) throws SerializationException {
    setContributionUuid(instance, streamReader.readString());
    setPrivateComment(instance, streamReader.readString());
    setPublicComment(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction instance) throws SerializationException {
    streamWriter.writeString(getContributionUuid(instance));
    streamWriter.writeString(getPrivateComment(instance));
    streamWriter.writeString(getPublicComment(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction)object);
  }
  
}
