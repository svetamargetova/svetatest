package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteAllUploadedContributionArtefactsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getSubmissionUuid(consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction::submissionUuid;
  }-*/;
  
  private static native void setSubmissionUuid(consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction::submissionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction instance) throws SerializationException {
    setSubmissionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction instance) throws SerializationException {
    streamWriter.writeString(getSubmissionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.DeleteAllUploadedContributionArtefactsAction)object);
  }
  
}
