package consys.event.conference.gwt.client.bo.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewCycle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getArtefacts(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::artefacts;
  }-*/;
  
  private static native void setArtefacts(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::artefacts = value;
  }-*/;
  
  private static native boolean getEditable(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::editable;
  }-*/;
  
  private static native void setEditable(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::editable = value;
  }-*/;
  
  private static native java.util.Date getFrom(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::from;
  }-*/;
  
  private static native void setFrom(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::from = value;
  }-*/;
  
  private static native java.util.Date getTo(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::to;
  }-*/;
  
  private static native void setTo(consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCycle::to = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance) throws SerializationException {
    setArtefacts(instance, (java.util.ArrayList) streamReader.readObject());
    setEditable(instance, streamReader.readBoolean());
    setFrom(instance, (java.util.Date) streamReader.readObject());
    setTo(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.shared.ClientReviewCycle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.shared.ClientReviewCycle instance) throws SerializationException {
    streamWriter.writeObject(getArtefacts(instance));
    streamWriter.writeBoolean(getEditable(instance));
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeObject(getTo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.shared.ClientReviewCycle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewCycle_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.shared.ClientReviewCycle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewCycle_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.shared.ClientReviewCycle)object);
  }
  
}
