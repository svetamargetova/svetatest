package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateContributionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactTypeUuid(consys.event.conference.gwt.client.action.setting.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateContributionAction::artefactTypeUuid;
  }-*/;
  
  private static native void setArtefactTypeUuid(consys.event.conference.gwt.client.action.setting.CreateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateContributionAction::artefactTypeUuid = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContribution getContribution(consys.event.conference.gwt.client.action.setting.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateContributionAction::contribution;
  }-*/;
  
  private static native void setContribution(consys.event.conference.gwt.client.action.setting.CreateContributionAction instance, consys.event.conference.gwt.client.bo.contribution.ClientContribution value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateContributionAction::contribution = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.CreateContributionAction instance) throws SerializationException {
    setArtefactTypeUuid(instance, streamReader.readString());
    setContribution(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContribution) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.CreateContributionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.CreateContributionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.CreateContributionAction instance) throws SerializationException {
    streamWriter.writeString(getArtefactTypeUuid(instance));
    streamWriter.writeObject(getContribution(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.CreateContributionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateContributionAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.CreateContributionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateContributionAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.CreateContributionAction)object);
  }
  
}
