package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteUploadedContributionArtefactAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactTypeUuid(consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction::artefactTypeUuid;
  }-*/;
  
  private static native void setArtefactTypeUuid(consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction::artefactTypeUuid = value;
  }-*/;
  
  private static native java.lang.String getArtefactUuid(consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction::artefactUuid;
  }-*/;
  
  private static native void setArtefactUuid(consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction::artefactUuid = value;
  }-*/;
  
  private static native java.lang.String getSubmissionUuid(consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction::submissionUuid;
  }-*/;
  
  private static native void setSubmissionUuid(consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction::submissionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance) throws SerializationException {
    setArtefactTypeUuid(instance, streamReader.readString());
    setArtefactUuid(instance, streamReader.readString());
    setSubmissionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction instance) throws SerializationException {
    streamWriter.writeString(getArtefactTypeUuid(instance));
    streamWriter.writeString(getArtefactUuid(instance));
    streamWriter.writeString(getSubmissionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction)object);
  }
  
}
