package consys.event.conference.gwt.client.bo.evaluation;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Evaluation_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.evaluation.EvaluationValue getOverallScore(consys.event.conference.gwt.client.bo.evaluation.Evaluation instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.Evaluation::overallScore;
  }-*/;
  
  private static native void setOverallScore(consys.event.conference.gwt.client.bo.evaluation.Evaluation instance, consys.event.conference.gwt.client.bo.evaluation.EvaluationValue value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.Evaluation::overallScore = value;
  }-*/;
  
  private static native java.util.ArrayList getPartialScores(consys.event.conference.gwt.client.bo.evaluation.Evaluation instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.Evaluation::partialScores;
  }-*/;
  
  private static native void setPartialScores(consys.event.conference.gwt.client.bo.evaluation.Evaluation instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.Evaluation::partialScores = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.evaluation.Evaluation instance) throws SerializationException {
    setOverallScore(instance, (consys.event.conference.gwt.client.bo.evaluation.EvaluationValue) streamReader.readObject());
    setPartialScores(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.evaluation.Evaluation instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.evaluation.Evaluation();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.evaluation.Evaluation instance) throws SerializationException {
    streamWriter.writeObject(getOverallScore(instance));
    streamWriter.writeObject(getPartialScores(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.evaluation.Evaluation_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.Evaluation_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.evaluation.Evaluation)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.Evaluation_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.evaluation.Evaluation)object);
  }
  
}
