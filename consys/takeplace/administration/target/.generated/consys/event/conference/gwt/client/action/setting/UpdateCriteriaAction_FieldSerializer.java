package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateCriteriaAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.setting.ClientCriteria getCriteria(consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction::criteria;
  }-*/;
  
  private static native void setCriteria(consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction instance, consys.event.conference.gwt.client.bo.setting.ClientCriteria value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction::criteria = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction instance) throws SerializationException {
    setCriteria(instance, (consys.event.conference.gwt.client.bo.setting.ClientCriteria) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction instance) throws SerializationException {
    streamWriter.writeObject(getCriteria(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction)object);
  }
  
}
