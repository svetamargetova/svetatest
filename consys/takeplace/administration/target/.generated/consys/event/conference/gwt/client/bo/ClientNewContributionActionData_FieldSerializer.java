package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientNewContributionActionData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionTypeUuid(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::contributionTypeUuid;
  }-*/;
  
  private static native void setContributionTypeUuid(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::contributionTypeUuid = value;
  }-*/;
  
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::contributionUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getSelectedContributionTypeCycles(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::selectedContributionTypeCycles;
  }-*/;
  
  private static native void setSelectedContributionTypeCycles(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::selectedContributionTypeCycles = value;
  }-*/;
  
  private static native java.util.ArrayList getTypes(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::types;
  }-*/;
  
  private static native void setTypes(consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientNewContributionActionData::types = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance) throws SerializationException {
    setContributionTypeUuid(instance, streamReader.readString());
    setContributionUuid(instance, streamReader.readString());
    setSelectedContributionTypeCycles(instance, (java.util.ArrayList) streamReader.readObject());
    setTypes(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.ClientNewContributionActionData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientNewContributionActionData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientNewContributionActionData instance) throws SerializationException {
    streamWriter.writeString(getContributionTypeUuid(instance));
    streamWriter.writeString(getContributionUuid(instance));
    streamWriter.writeObject(getSelectedContributionTypeCycles(instance));
    streamWriter.writeObject(getTypes(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientNewContributionActionData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientNewContributionActionData_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientNewContributionActionData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientNewContributionActionData_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientNewContributionActionData)object);
  }
  
}
