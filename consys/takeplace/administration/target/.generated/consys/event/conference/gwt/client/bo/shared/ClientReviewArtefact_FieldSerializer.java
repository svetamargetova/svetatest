package consys.event.conference.gwt.client.bo.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewArtefact_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactUuid(consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact::artefactUuid;
  }-*/;
  
  private static native void setArtefactUuid(consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact::artefactUuid = value;
  }-*/;
  
  private static native java.lang.String getFileName(consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact::fileName;
  }-*/;
  
  private static native void setFileName(consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact::fileName = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.evaluation.EvaluationValue getTotalScore(consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact::totalScore;
  }-*/;
  
  private static native void setTotalScore(consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance, consys.event.conference.gwt.client.bo.evaluation.EvaluationValue value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact::totalScore = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance) throws SerializationException {
    setArtefactUuid(instance, streamReader.readString());
    setFileName(instance, streamReader.readString());
    setTotalScore(instance, (consys.event.conference.gwt.client.bo.evaluation.EvaluationValue) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.ClientArtefactType_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact instance) throws SerializationException {
    streamWriter.writeString(getArtefactUuid(instance));
    streamWriter.writeString(getFileName(instance));
    streamWriter.writeObject(getTotalScore(instance));
    
    consys.event.conference.gwt.client.bo.ClientArtefactType_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact)object);
  }
  
}
