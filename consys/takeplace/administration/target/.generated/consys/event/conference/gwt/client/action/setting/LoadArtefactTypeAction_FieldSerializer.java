package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadArtefactTypeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction)object);
  }
  
}
