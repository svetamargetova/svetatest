package consys.event.conference.gwt.client.action.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateReviewerTopicsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getUserTopics(consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction::userTopics;
  }-*/;
  
  private static native void setUserTopics(consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction::userTopics = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction instance) throws SerializationException {
    setUserTopics(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction instance) throws SerializationException {
    streamWriter.writeObject(getUserTopics(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction)object);
  }
  
}
