package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionTypeThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb::description;
  }-*/;
  
  private static native void setDescription(consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb::description = value;
  }-*/;
  
  private static native boolean getInternal(consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb::internal;
  }-*/;
  
  private static native void setInternal(consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb::internal = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setInternal(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.bo.CommonThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeBoolean(getInternal(instance));
    
    consys.common.gwt.shared.bo.CommonThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb)object);
  }
  
}
