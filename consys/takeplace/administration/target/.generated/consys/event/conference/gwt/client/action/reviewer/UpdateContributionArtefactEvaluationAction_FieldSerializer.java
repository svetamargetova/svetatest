package consys.event.conference.gwt.client.action.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateContributionArtefactEvaluationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::artefactUuid;
  }-*/;
  
  private static native void setArtefactUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::artefactUuid = value;
  }-*/;
  
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::contributionUuid = value;
  }-*/;
  
  private static native java.util.HashMap getEvaluations(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::evaluations;
  }-*/;
  
  private static native void setEvaluations(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance, java.util.HashMap value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::evaluations = value;
  }-*/;
  
  private static native java.lang.String getPrivateComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::privateComment;
  }-*/;
  
  private static native void setPrivateComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::privateComment = value;
  }-*/;
  
  private static native java.lang.String getPublicComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::publicComment;
  }-*/;
  
  private static native void setPublicComment(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::publicComment = value;
  }-*/;
  
  private static native java.lang.String getReviewUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::reviewUuid;
  }-*/;
  
  private static native void setReviewUuid(consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction::reviewUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) throws SerializationException {
    setArtefactUuid(instance, streamReader.readString());
    setContributionUuid(instance, streamReader.readString());
    setEvaluations(instance, (java.util.HashMap) streamReader.readObject());
    setPrivateComment(instance, streamReader.readString());
    setPublicComment(instance, streamReader.readString());
    setReviewUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction instance) throws SerializationException {
    streamWriter.writeString(getArtefactUuid(instance));
    streamWriter.writeString(getContributionUuid(instance));
    streamWriter.writeObject(getEvaluations(instance));
    streamWriter.writeString(getPrivateComment(instance));
    streamWriter.writeString(getPublicComment(instance));
    streamWriter.writeString(getReviewUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction)object);
  }
  
}
