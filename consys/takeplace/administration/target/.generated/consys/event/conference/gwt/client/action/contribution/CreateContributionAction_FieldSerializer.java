package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateContributionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String[] getAuthors(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.lang.String[] value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::authors = value;
  }-*/;
  
  private static native java.util.Set getContributionFileArtefacts(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionFileArtefacts;
  }-*/;
  
  private static native void setContributionFileArtefacts(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.util.Set value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionFileArtefacts = value;
  }-*/;
  
  private static native java.util.Set getContributionTextArtefacts(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionTextArtefacts;
  }-*/;
  
  private static native void setContributionTextArtefacts(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.util.Set value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionTextArtefacts = value;
  }-*/;
  
  private static native java.lang.String getContributionTypeUuid(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionTypeUuid;
  }-*/;
  
  private static native void setContributionTypeUuid(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionTypeUuid = value;
  }-*/;
  
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributionUuid = value;
  }-*/;
  
  private static native java.util.Set getContributors(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributors;
  }-*/;
  
  private static native void setContributors(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.util.Set value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::contributors = value;
  }-*/;
  
  private static native java.lang.String getSubtitle(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::subtitle;
  }-*/;
  
  private static native void setSubtitle(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::subtitle = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::title;
  }-*/;
  
  private static native void setTitle(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::title = value;
  }-*/;
  
  private static native java.util.Set getTopics(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.util.Set value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::topics = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.CreateContributionAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) throws SerializationException {
    setAuthors(instance, (java.lang.String[]) streamReader.readObject());
    setContributionFileArtefacts(instance, (java.util.Set) streamReader.readObject());
    setContributionTextArtefacts(instance, (java.util.Set) streamReader.readObject());
    setContributionTypeUuid(instance, streamReader.readString());
    setContributionUuid(instance, streamReader.readString());
    setContributors(instance, (java.util.Set) streamReader.readObject());
    setSubtitle(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    setTopics(instance, (java.util.Set) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.CreateContributionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.CreateContributionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.CreateContributionAction instance) throws SerializationException {
    streamWriter.writeObject(getAuthors(instance));
    streamWriter.writeObject(getContributionFileArtefacts(instance));
    streamWriter.writeObject(getContributionTextArtefacts(instance));
    streamWriter.writeString(getContributionTypeUuid(instance));
    streamWriter.writeString(getContributionUuid(instance));
    streamWriter.writeObject(getContributors(instance));
    streamWriter.writeString(getSubtitle(instance));
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeObject(getTopics(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.CreateContributionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.CreateContributionAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.CreateContributionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.CreateContributionAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.CreateContributionAction)object);
  }
  
}
