package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateArtefactTypeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::description;
  }-*/;
  
  private static native void setDescription(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::description = value;
  }-*/;
  
  private static native boolean getFile(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::file;
  }-*/;
  
  private static native void setFile(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::file = value;
  }-*/;
  
  private static native java.lang.String getFileFormats(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::fileFormats;
  }-*/;
  
  private static native void setFileFormats(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::fileFormats = value;
  }-*/;
  
  private static native int getMaxInputLength(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::maxInputLength;
  }-*/;
  
  private static native void setMaxInputLength(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::maxInputLength = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::name = value;
  }-*/;
  
  private static native int getNumReviewers(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::numReviewers;
  }-*/;
  
  private static native void setNumReviewers(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::numReviewers = value;
  }-*/;
  
  private static native boolean getRequired(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::required;
  }-*/;
  
  private static native void setRequired(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::required = value;
  }-*/;
  
  private static native java.util.ArrayList getUploadedExamples(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::uploadedExamples;
  }-*/;
  
  private static native void setUploadedExamples(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::uploadedExamples = value;
  }-*/;
  
  private static native java.util.ArrayList getUploadedTemplates(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::uploadedTemplates;
  }-*/;
  
  private static native void setUploadedTemplates(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::uploadedTemplates = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setFile(instance, streamReader.readBoolean());
    setFileFormats(instance, streamReader.readString());
    setMaxInputLength(instance, streamReader.readInt());
    setName(instance, streamReader.readString());
    setNumReviewers(instance, streamReader.readInt());
    setRequired(instance, streamReader.readBoolean());
    setUploadedExamples(instance, (java.util.ArrayList) streamReader.readObject());
    setUploadedTemplates(instance, (java.util.ArrayList) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeBoolean(getFile(instance));
    streamWriter.writeString(getFileFormats(instance));
    streamWriter.writeInt(getMaxInputLength(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeInt(getNumReviewers(instance));
    streamWriter.writeBoolean(getRequired(instance));
    streamWriter.writeObject(getUploadedExamples(instance));
    streamWriter.writeObject(getUploadedTemplates(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction)object);
  }
  
}
