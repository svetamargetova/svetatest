package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateContributionTextArtefactAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction::contributionUuid = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact getTextContributionArtefact(consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction::textContributionArtefact;
  }-*/;
  
  private static native void setTextContributionArtefact(consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction instance, consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction::textContributionArtefact = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction instance) throws SerializationException {
    setContributionUuid(instance, streamReader.readString());
    setTextContributionArtefact(instance, (consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction instance) throws SerializationException {
    streamWriter.writeString(getContributionUuid(instance));
    streamWriter.writeObject(getTextContributionArtefact(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction)object);
  }
  
}
