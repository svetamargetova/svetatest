package consys.event.conference.gwt.client.bo.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewArtefactReviewer_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getPrivateComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer::privateComment;
  }-*/;
  
  private static native void setPrivateComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer::privateComment = value;
  }-*/;
  
  private static native java.lang.String getPublicComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer::publicComment;
  }-*/;
  
  private static native void setPublicComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer::publicComment = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer instance) throws SerializationException {
    setPrivateComment(instance, streamReader.readString());
    setPublicComment(instance, streamReader.readString());
    
    consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer instance) throws SerializationException {
    streamWriter.writeString(getPrivateComment(instance));
    streamWriter.writeString(getPublicComment(instance));
    
    consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer)object);
  }
  
}
