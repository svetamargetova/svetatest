package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewFormCycleArtefact_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getAlreadyReviewed(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::alreadyReviewed;
  }-*/;
  
  private static native void setAlreadyReviewed(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::alreadyReviewed = value;
  }-*/;
  
  private static native java.lang.String getArtefactTypeName(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::artefactTypeName;
  }-*/;
  
  private static native void setArtefactTypeName(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::artefactTypeName = value;
  }-*/;
  
  private static native java.lang.String getArtefactUuid(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::artefactUuid;
  }-*/;
  
  private static native void setArtefactUuid(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::artefactUuid = value;
  }-*/;
  
  private static native boolean getAutomaticallyAccepted(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::automaticallyAccepted;
  }-*/;
  
  private static native void setAutomaticallyAccepted(consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact::automaticallyAccepted = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance) throws SerializationException {
    setAlreadyReviewed(instance, streamReader.readBoolean());
    setArtefactTypeName(instance, streamReader.readString());
    setArtefactUuid(instance, streamReader.readString());
    setAutomaticallyAccepted(instance, streamReader.readBoolean());
    
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact instance) throws SerializationException {
    streamWriter.writeBoolean(getAlreadyReviewed(instance));
    streamWriter.writeString(getArtefactTypeName(instance));
    streamWriter.writeString(getArtefactUuid(instance));
    streamWriter.writeBoolean(getAutomaticallyAccepted(instance));
    
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientReviewFormCycleArtefact)object);
  }
  
}
