package consys.event.conference.gwt.client.bo.evaluation;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ReviewScoreAndCommentsData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.evaluation.Evaluation getEvaluation(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::evaluation;
  }-*/;
  
  private static native void setEvaluation(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, consys.event.conference.gwt.client.bo.evaluation.Evaluation value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::evaluation = value;
  }-*/;
  
  private static native java.lang.String getPrivateComment(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::privateComment;
  }-*/;
  
  private static native void setPrivateComment(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::privateComment = value;
  }-*/;
  
  private static native java.lang.String getPublicComment(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::publicComment;
  }-*/;
  
  private static native void setPublicComment(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::publicComment = value;
  }-*/;
  
  private static native java.lang.String getReviewUuid(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewUuid;
  }-*/;
  
  private static native void setReviewUuid(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewUuid = value;
  }-*/;
  
  private static native java.lang.String getReviewerImageUuid(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerImageUuid;
  }-*/;
  
  private static native void setReviewerImageUuid(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerImageUuid = value;
  }-*/;
  
  private static native java.lang.String getReviewerName(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerName;
  }-*/;
  
  private static native void setReviewerName(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerName = value;
  }-*/;
  
  private static native java.lang.String getReviewerOrganization(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerOrganization;
  }-*/;
  
  private static native void setReviewerOrganization(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerOrganization = value;
  }-*/;
  
  private static native java.lang.String getReviewerPosition(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerPosition;
  }-*/;
  
  private static native void setReviewerPosition(consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData::reviewerPosition = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) throws SerializationException {
    setEvaluation(instance, (consys.event.conference.gwt.client.bo.evaluation.Evaluation) streamReader.readObject());
    setPrivateComment(instance, streamReader.readString());
    setPublicComment(instance, streamReader.readString());
    setReviewUuid(instance, streamReader.readString());
    setReviewerImageUuid(instance, streamReader.readString());
    setReviewerName(instance, streamReader.readString());
    setReviewerOrganization(instance, streamReader.readString());
    setReviewerPosition(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData instance) throws SerializationException {
    streamWriter.writeObject(getEvaluation(instance));
    streamWriter.writeString(getPrivateComment(instance));
    streamWriter.writeString(getPublicComment(instance));
    streamWriter.writeString(getReviewUuid(instance));
    streamWriter.writeString(getReviewerImageUuid(instance));
    streamWriter.writeString(getReviewerName(instance));
    streamWriter.writeString(getReviewerOrganization(instance));
    streamWriter.writeString(getReviewerPosition(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData)object);
  }
  
}
