package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientArtefactTypeThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactTypeDescription(consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb::artefactTypeDescription;
  }-*/;
  
  private static native void setArtefactTypeDescription(consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb::artefactTypeDescription = value;
  }-*/;
  
  private static native java.lang.String getArtefactTypeName(consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb::artefactTypeName;
  }-*/;
  
  private static native void setArtefactTypeName(consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb::artefactTypeName = value;
  }-*/;
  
  private static native java.lang.String getArtefactTypeUuid(consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb::artefactTypeUuid;
  }-*/;
  
  private static native void setArtefactTypeUuid(consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb::artefactTypeUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance) throws SerializationException {
    setArtefactTypeDescription(instance, streamReader.readString());
    setArtefactTypeName(instance, streamReader.readString());
    setArtefactTypeUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb instance) throws SerializationException {
    streamWriter.writeString(getArtefactTypeDescription(instance));
    streamWriter.writeString(getArtefactTypeName(instance));
    streamWriter.writeString(getArtefactTypeUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb)object);
  }
  
}
