package consys.event.conference.gwt.client.message;

public class EventConferenceMessages_ implements consys.event.conference.gwt.client.message.EventConferenceMessages {
  
  public java.lang.String editFormArtefact_error_invalidNotation(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Invalid notation in the ").append(arg0).append(" field.").toString();
  }
  
  public java.lang.String fileRecordUI_error_illegalFileFormat(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Illegal file format in the ").append(arg0).append(" field.").toString();
  }
  
  public java.lang.String contributionHeadPanel_text_deleteContribution(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Are you sure you want to delete contribution ").append(arg0).append("?").toString();
  }
  
  public java.lang.String fileRecordUI_text_allowedFileFormats(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Allowed file formats: ").append(arg0).toString();
  }
  
  public java.lang.String content_error_missing(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Missing value in the ").append(arg0).append(" field.").toString();
  }
}
