package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateTopicAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.contribution.ClientTopic getTopic(consys.event.conference.gwt.client.action.setting.UpdateTopicAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateTopicAction::topic;
  }-*/;
  
  private static native void setTopic(consys.event.conference.gwt.client.action.setting.UpdateTopicAction instance, consys.event.conference.gwt.client.bo.contribution.ClientTopic value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateTopicAction::topic = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.UpdateTopicAction instance) throws SerializationException {
    setTopic(instance, (consys.event.conference.gwt.client.bo.contribution.ClientTopic) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.UpdateTopicAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.UpdateTopicAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.UpdateTopicAction instance) throws SerializationException {
    streamWriter.writeObject(getTopic(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.UpdateTopicAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateTopicAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.UpdateTopicAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateTopicAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.UpdateTopicAction)object);
  }
  
}
