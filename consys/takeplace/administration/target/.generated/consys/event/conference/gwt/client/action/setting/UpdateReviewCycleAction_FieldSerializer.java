package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateReviewCycleAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle getReviewCycle(consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction::reviewCycle;
  }-*/;
  
  private static native void setReviewCycle(consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction instance, consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction::reviewCycle = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction instance) throws SerializationException {
    setReviewCycle(instance, (consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction instance) throws SerializationException {
    streamWriter.writeObject(getReviewCycle(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction)object);
  }
  
}
