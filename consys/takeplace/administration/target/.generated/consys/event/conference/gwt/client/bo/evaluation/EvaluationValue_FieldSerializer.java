package consys.event.conference.gwt.client.bo.evaluation;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EvaluationValue_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.bo.evaluation.EvaluationValue instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.EvaluationValue::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.bo.evaluation.EvaluationValue instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.EvaluationValue::uuid = value;
  }-*/;
  
  private static native int getValue(consys.event.conference.gwt.client.bo.evaluation.EvaluationValue instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.evaluation.EvaluationValue::value;
  }-*/;
  
  private static native void setValue(consys.event.conference.gwt.client.bo.evaluation.EvaluationValue instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.evaluation.EvaluationValue::value = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.evaluation.EvaluationValue instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    setValue(instance, streamReader.readInt());
    
  }
  
  public static consys.event.conference.gwt.client.bo.evaluation.EvaluationValue instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.evaluation.EvaluationValue();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.evaluation.EvaluationValue instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    streamWriter.writeInt(getValue(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.evaluation.EvaluationValue_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.EvaluationValue_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.evaluation.EvaluationValue)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.evaluation.EvaluationValue_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.evaluation.EvaluationValue)object);
  }
  
}
