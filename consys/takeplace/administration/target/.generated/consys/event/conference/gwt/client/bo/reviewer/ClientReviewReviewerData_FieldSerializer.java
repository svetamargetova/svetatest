package consys.event.conference.gwt.client.bo.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewReviewerData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getCriterias(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::criterias;
  }-*/;
  
  private static native void setCriterias(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::criterias = value;
  }-*/;
  
  private static native java.util.ArrayList getEnteredEvaluations(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::enteredEvaluations;
  }-*/;
  
  private static native void setEnteredEvaluations(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::enteredEvaluations = value;
  }-*/;
  
  private static native java.lang.String getPrivateComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::privateComment;
  }-*/;
  
  private static native void setPrivateComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::privateComment = value;
  }-*/;
  
  private static native java.lang.String getPublicComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::publicComment;
  }-*/;
  
  private static native void setPublicComment(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::publicComment = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.evaluation.EvaluationValue getScore(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::score;
  }-*/;
  
  private static native void setScore(consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance, consys.event.conference.gwt.client.bo.evaluation.EvaluationValue value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData::score = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance) throws SerializationException {
    setCriterias(instance, (java.util.ArrayList) streamReader.readObject());
    setEnteredEvaluations(instance, (java.util.ArrayList) streamReader.readObject());
    setPrivateComment(instance, streamReader.readString());
    setPublicComment(instance, streamReader.readString());
    setScore(instance, (consys.event.conference.gwt.client.bo.evaluation.EvaluationValue) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData instance) throws SerializationException {
    streamWriter.writeObject(getCriterias(instance));
    streamWriter.writeObject(getEnteredEvaluations(instance));
    streamWriter.writeString(getPrivateComment(instance));
    streamWriter.writeString(getPublicComment(instance));
    streamWriter.writeObject(getScore(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData)object);
  }
  
}
