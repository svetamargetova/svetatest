package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FileContributionArtefact_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactTypeUuid(consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact::artefactTypeUuid;
  }-*/;
  
  private static native void setArtefactTypeUuid(consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact::artefactTypeUuid = value;
  }-*/;
  
  private static native java.lang.String getArtefactUuid(consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact::artefactUuid;
  }-*/;
  
  private static native void setArtefactUuid(consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact::artefactUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact instance) throws SerializationException {
    setArtefactTypeUuid(instance, streamReader.readString());
    setArtefactUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact instance) throws SerializationException {
    streamWriter.writeString(getArtefactTypeUuid(instance));
    streamWriter.writeString(getArtefactUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact)object);
  }
  
}
