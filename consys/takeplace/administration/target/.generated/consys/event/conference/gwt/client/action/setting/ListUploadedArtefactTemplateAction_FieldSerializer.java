package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListUploadedArtefactTemplateAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getExamples(consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction::examples;
  }-*/;
  
  private static native void setExamples(consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction::examples = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction instance) throws SerializationException {
    setExamples(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction instance) throws SerializationException {
    streamWriter.writeBoolean(getExamples(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction)object);
  }
  
}
