package consys.event.conference.gwt.client.bo.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ReviewerInterestEnum_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum[] values = consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum)object);
  }
  
}
