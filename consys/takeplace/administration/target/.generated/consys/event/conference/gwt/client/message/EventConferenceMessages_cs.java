package consys.event.conference.gwt.client.message;

public class EventConferenceMessages_cs implements consys.event.conference.gwt.client.message.EventConferenceMessages {
  
  public java.lang.String editFormArtefact_error_invalidNotation(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Neplatný zápis v poli ").append(arg0).append(".").toString();
  }
  
  public java.lang.String fileRecordUI_error_illegalFileFormat(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Nepovolený formát souboru v poli ").append(arg0).append(".").toString();
  }
  
  public java.lang.String contributionHeadPanel_text_deleteContribution(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Jste si jisti, že chcete odstranit příspěvek ").append(arg0).append("?").toString();
  }
  
  public java.lang.String fileRecordUI_text_allowedFileFormats(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Povolené formáty souboru: ").append(arg0).toString();
  }
  
  public java.lang.String content_error_missing(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Chybějící hodnota v poli ").append(arg0).append(".").toString();
  }
}
