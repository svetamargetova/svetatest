package consys.event.conference.gwt.client.action.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ContributionReviewerAssignmentAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getAssigned(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::assigned;
  }-*/;
  
  private static native void setAssigned(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::assigned = value;
  }-*/;
  
  private static native java.lang.String getEventUserUuid(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::eventUserUuid;
  }-*/;
  
  private static native void setEventUserUuid(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::eventUserUuid = value;
  }-*/;
  
  private static native boolean getForce(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::force;
  }-*/;
  
  private static native void setForce(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::force = value;
  }-*/;
  
  private static native java.lang.String getSubmissionUuid(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::submissionUuid;
  }-*/;
  
  private static native void setSubmissionUuid(consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction::submissionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance) throws SerializationException {
    setAssigned(instance, streamReader.readBoolean());
    setEventUserUuid(instance, streamReader.readString());
    setForce(instance, streamReader.readBoolean());
    setSubmissionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction instance) throws SerializationException {
    streamWriter.writeBoolean(getAssigned(instance));
    streamWriter.writeString(getEventUserUuid(instance));
    streamWriter.writeBoolean(getForce(instance));
    streamWriter.writeString(getSubmissionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction)object);
  }
  
}
