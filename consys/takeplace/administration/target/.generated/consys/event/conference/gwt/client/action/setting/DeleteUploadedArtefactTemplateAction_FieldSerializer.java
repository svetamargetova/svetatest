package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteUploadedArtefactTemplateAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getPattern(consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction::pattern;
  }-*/;
  
  private static native void setPattern(consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction::pattern = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction instance) throws SerializationException {
    setPattern(instance, streamReader.readBoolean());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction instance) throws SerializationException {
    streamWriter.writeBoolean(getPattern(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction)object);
  }
  
}
