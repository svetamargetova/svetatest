package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionType_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getClientArtefactTypeList(consys.event.conference.gwt.client.bo.contribution.ClientContributionType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionType::clientArtefactTypeList;
  }-*/;
  
  private static native void setClientArtefactTypeList(consys.event.conference.gwt.client.bo.contribution.ClientContributionType instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionType::clientArtefactTypeList = value;
  }-*/;
  
  private static native java.util.ArrayList getClientReviewCycleList(consys.event.conference.gwt.client.bo.contribution.ClientContributionType instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionType::clientReviewCycleList;
  }-*/;
  
  private static native void setClientReviewCycleList(consys.event.conference.gwt.client.bo.contribution.ClientContributionType instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionType::clientReviewCycleList = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.ClientContributionType instance) throws SerializationException {
    setClientArtefactTypeList(instance, (java.util.ArrayList) streamReader.readObject());
    setClientReviewCycleList(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.ClientContributionType instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.ClientContributionType();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.ClientContributionType instance) throws SerializationException {
    streamWriter.writeObject(getClientArtefactTypeList(instance));
    streamWriter.writeObject(getClientReviewCycleList(instance));
    
    consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.ClientContributionType_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionType_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.ClientContributionType)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionType_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.ClientContributionType)object);
  }
  
}
