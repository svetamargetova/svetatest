package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadUploadedArtefactTemplateAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getPattern(consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction::pattern;
  }-*/;
  
  private static native void setPattern(consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction::pattern = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction instance) throws SerializationException {
    setPattern(instance, streamReader.readBoolean());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction instance) throws SerializationException {
    streamWriter.writeBoolean(getPattern(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction)object);
  }
  
}
