package consys.event.conference.gwt.client.bo.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewArtefactChair_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionTypeUuid(consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair::contributionTypeUuid;
  }-*/;
  
  private static native void setContributionTypeUuid(consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair::contributionTypeUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getCriterias(consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair::criterias;
  }-*/;
  
  private static native void setCriterias(consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair::criterias = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair instance) throws SerializationException {
    setContributionTypeUuid(instance, streamReader.readString());
    setCriterias(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair instance) throws SerializationException {
    streamWriter.writeString(getContributionTypeUuid(instance));
    streamWriter.writeObject(getCriterias(instance));
    
    consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair)object);
  }
  
}
