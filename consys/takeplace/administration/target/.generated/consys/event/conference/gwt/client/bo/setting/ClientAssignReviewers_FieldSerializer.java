package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientAssignReviewers_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData getHead(consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers::head;
  }-*/;
  
  private static native void setHead(consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers::head = value;
  }-*/;
  
  private static native java.util.ArrayList getReviewers(consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers::reviewers;
  }-*/;
  
  private static native void setReviewers(consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers::reviewers = value;
  }-*/;
  
  private static native java.util.ArrayList getTopics(consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers::topics = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance) throws SerializationException {
    setHead(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData) streamReader.readObject());
    setReviewers(instance, (java.util.ArrayList) streamReader.readObject());
    setTopics(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers instance) throws SerializationException {
    streamWriter.writeObject(getHead(instance));
    streamWriter.writeObject(getReviewers(instance));
    streamWriter.writeObject(getTopics(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers)object);
  }
  
}
