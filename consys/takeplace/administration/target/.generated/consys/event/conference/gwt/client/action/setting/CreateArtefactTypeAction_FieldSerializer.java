package consys.event.conference.gwt.client.action.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateArtefactTypeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getSubmissionTypeUuid(consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction::submissionTypeUuid;
  }-*/;
  
  private static native void setSubmissionTypeUuid(consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction::submissionTypeUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getType(consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction::type;
  }-*/;
  
  private static native void setType(consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction::type = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction instance) throws SerializationException {
    setSubmissionTypeUuid(instance, streamReader.readString());
    setType(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction instance) throws SerializationException {
    streamWriter.writeString(getSubmissionTypeUuid(instance));
    streamWriter.writeObject(getType(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction)object);
  }
  
}
