package consys.event.conference.gwt.client.action.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadContributionReviewFormAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction::contributionUuid = value;
  }-*/;
  
  private static native java.lang.String getReviewUuid(consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction::reviewUuid;
  }-*/;
  
  private static native void setReviewUuid(consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction::reviewUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction instance) throws SerializationException {
    setContributionUuid(instance, streamReader.readString());
    setReviewUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction instance) throws SerializationException {
    streamWriter.writeString(getContributionUuid(instance));
    streamWriter.writeString(getReviewUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction)object);
  }
  
}
