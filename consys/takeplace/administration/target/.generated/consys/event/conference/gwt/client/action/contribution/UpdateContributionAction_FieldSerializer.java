package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateContributionAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String[] getAuthors(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance, java.lang.String[] value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::authors = value;
  }-*/;
  
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::contributionUuid = value;
  }-*/;
  
  private static native java.util.Set getContributors(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::contributors;
  }-*/;
  
  private static native void setContributors(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance, java.util.Set value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::contributors = value;
  }-*/;
  
  private static native java.lang.String getSubtitle(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::subtitle;
  }-*/;
  
  private static native void setSubtitle(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::subtitle = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::title;
  }-*/;
  
  private static native void setTitle(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::title = value;
  }-*/;
  
  private static native java.util.Set getTopics(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance, java.util.Set value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.UpdateContributionAction::topics = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) throws SerializationException {
    setAuthors(instance, (java.lang.String[]) streamReader.readObject());
    setContributionUuid(instance, streamReader.readString());
    setContributors(instance, (java.util.Set) streamReader.readObject());
    setSubtitle(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    setTopics(instance, (java.util.Set) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.UpdateContributionAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.UpdateContributionAction instance) throws SerializationException {
    streamWriter.writeObject(getAuthors(instance));
    streamWriter.writeString(getContributionUuid(instance));
    streamWriter.writeObject(getContributors(instance));
    streamWriter.writeString(getSubtitle(instance));
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeObject(getTopics(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.UpdateContributionAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.UpdateContributionAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.UpdateContributionAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.UpdateContributionAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.UpdateContributionAction)object);
  }
  
}
