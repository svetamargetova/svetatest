package consys.event.conference.gwt.client.bo.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionChairHead_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getChangedDate(consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead::changedDate;
  }-*/;
  
  private static native void setChangedDate(consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead::changedDate = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead instance) throws SerializationException {
    setChangedDate(instance, (java.util.Date) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead instance) throws SerializationException {
    streamWriter.writeObject(getChangedDate(instance));
    
    consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead)object);
  }
  
}
