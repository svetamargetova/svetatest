package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCriteriaThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::id;
  }-*/;
  
  private static native void setId(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::id = value;
  }-*/;
  
  private static native int getMaxPoints(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::maxPoints;
  }-*/;
  
  private static native void setMaxPoints(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::maxPoints = value;
  }-*/;
  
  private static native double getMinimumPoints(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::minimumPoints;
  }-*/;
  
  private static native void setMinimumPoints(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance, double value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::minimumPoints = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    setMaxPoints(instance, streamReader.readInt());
    setMinimumPoints(instance, streamReader.readDouble());
    setName(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    streamWriter.writeInt(getMaxPoints(instance));
    streamWriter.writeDouble(getMinimumPoints(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb)object);
  }
  
}
