package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionEvaluationChair_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getEvaluations(consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair::evaluations;
  }-*/;
  
  private static native void setEvaluations(consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair::evaluations = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData getHead(consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair::head;
  }-*/;
  
  private static native void setHead(consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair::head = value;
  }-*/;
  
  private static native java.util.ArrayList getReviewers(consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair::reviewers;
  }-*/;
  
  private static native void setReviewers(consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair::reviewers = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance) throws SerializationException {
    setEvaluations(instance, (java.util.ArrayList) streamReader.readObject());
    setHead(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData) streamReader.readObject());
    setReviewers(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair instance) throws SerializationException {
    streamWriter.writeObject(getEvaluations(instance));
    streamWriter.writeObject(getHead(instance));
    streamWriter.writeObject(getReviewers(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.ClientContributionEvaluationChair)object);
  }
  
}
