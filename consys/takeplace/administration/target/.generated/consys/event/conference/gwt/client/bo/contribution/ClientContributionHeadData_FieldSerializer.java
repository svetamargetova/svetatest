package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionHeadData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAuthors(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::authors = value;
  }-*/;
  
  private static native java.util.ArrayList getContributors(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::contributors;
  }-*/;
  
  private static native void setContributors(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::contributors = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.evaluation.Evaluation getEvaluations(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::evaluations;
  }-*/;
  
  private static native void setEvaluations(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, consys.event.conference.gwt.client.bo.evaluation.Evaluation value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::evaluations = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum getState(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::state;
  }-*/;
  
  private static native void setState(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::state = value;
  }-*/;
  
  private static native java.util.Date getSubmittedDate(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::submittedDate;
  }-*/;
  
  private static native void setSubmittedDate(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::submittedDate = value;
  }-*/;
  
  private static native java.lang.String getSubtitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::subtitle;
  }-*/;
  
  private static native void setSubtitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::subtitle = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::title;
  }-*/;
  
  private static native void setTitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::title = value;
  }-*/;
  
  private static native java.lang.String getTopics(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::topics = value;
  }-*/;
  
  private static native java.lang.String getType(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::type;
  }-*/;
  
  private static native void setType(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::type = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) throws SerializationException {
    setAuthors(instance, streamReader.readString());
    setContributors(instance, (java.util.ArrayList) streamReader.readObject());
    setEvaluations(instance, (consys.event.conference.gwt.client.bo.evaluation.Evaluation) streamReader.readObject());
    setState(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum) streamReader.readObject());
    setSubmittedDate(instance, (java.util.Date) streamReader.readObject());
    setSubtitle(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    setTopics(instance, streamReader.readString());
    setType(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData instance) throws SerializationException {
    streamWriter.writeString(getAuthors(instance));
    streamWriter.writeObject(getContributors(instance));
    streamWriter.writeObject(getEvaluations(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeObject(getSubmittedDate(instance));
    streamWriter.writeString(getSubtitle(instance));
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeString(getTopics(instance));
    streamWriter.writeString(getType(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData)object);
  }
  
}
