package consys.event.conference.gwt.client.action.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadContributionTypeCyclesAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionTypeUuid(consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction::contributionTypeUuid;
  }-*/;
  
  private static native void setContributionTypeUuid(consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction::contributionTypeUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction instance) throws SerializationException {
    setContributionTypeUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction instance) throws SerializationException {
    streamWriter.writeString(getContributionTypeUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction)object);
  }
  
}
