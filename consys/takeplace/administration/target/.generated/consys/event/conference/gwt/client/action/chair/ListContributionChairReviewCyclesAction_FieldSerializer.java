package consys.event.conference.gwt.client.action.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListContributionChairReviewCyclesAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction::contributionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction instance) throws SerializationException {
    setContributionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction instance) throws SerializationException {
    streamWriter.writeString(getContributionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction)object);
  }
  
}
