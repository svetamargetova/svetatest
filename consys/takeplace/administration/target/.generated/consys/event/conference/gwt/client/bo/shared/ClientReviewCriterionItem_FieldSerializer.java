package consys.event.conference.gwt.client.bo.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientReviewCriterionItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem::name = value;
  }-*/;
  
  private static native java.lang.String getReviewUuid(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem::reviewUuid;
  }-*/;
  
  private static native void setReviewUuid(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem::reviewUuid = value;
  }-*/;
  
  private static native java.lang.Object getValue(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem::value;
  }-*/;
  
  private static native void setValue(consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance, java.lang.Object value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem::value = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance) throws SerializationException {
    setName(instance, streamReader.readString());
    setReviewUuid(instance, streamReader.readString());
    setValue(instance, streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem instance) throws SerializationException {
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getReviewUuid(instance));
    streamWriter.writeObject(getValue(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem)object);
  }
  
}
