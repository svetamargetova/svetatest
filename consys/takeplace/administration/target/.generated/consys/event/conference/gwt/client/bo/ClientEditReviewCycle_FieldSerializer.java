package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEditReviewCycle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getNotAssignedArtefacts(consys.event.conference.gwt.client.bo.ClientEditReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientEditReviewCycle::notAssignedArtefacts;
  }-*/;
  
  private static native void setNotAssignedArtefacts(consys.event.conference.gwt.client.bo.ClientEditReviewCycle instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientEditReviewCycle::notAssignedArtefacts = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientEditReviewCycle instance) throws SerializationException {
    setNotAssignedArtefacts(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.ClientEditReviewCycle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientEditReviewCycle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientEditReviewCycle instance) throws SerializationException {
    streamWriter.writeObject(getNotAssignedArtefacts(instance));
    
    consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientEditReviewCycle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientEditReviewCycle_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientEditReviewCycle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientEditReviewCycle_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientEditReviewCycle)object);
  }
  
}
