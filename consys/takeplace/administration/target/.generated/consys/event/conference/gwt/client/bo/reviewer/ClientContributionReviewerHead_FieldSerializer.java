package consys.event.conference.gwt.client.bo.reviewer;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionReviewerHead_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getReviewUuid(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead::reviewUuid;
  }-*/;
  
  private static native void setReviewUuid(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead::reviewUuid = value;
  }-*/;
  
  private static native java.lang.String getReviewerName(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead::reviewerName;
  }-*/;
  
  private static native void setReviewerName(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead::reviewerName = value;
  }-*/;
  
  private static native java.lang.String getReviewerUuid(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead::reviewerUuid;
  }-*/;
  
  private static native void setReviewerUuid(consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead::reviewerUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance) throws SerializationException {
    setReviewUuid(instance, streamReader.readString());
    setReviewerName(instance, streamReader.readString());
    setReviewerUuid(instance, streamReader.readString());
    
    consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead instance) throws SerializationException {
    streamWriter.writeString(getReviewUuid(instance));
    streamWriter.writeString(getReviewerName(instance));
    streamWriter.writeString(getReviewerUuid(instance));
    
    consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead)object);
  }
  
}
