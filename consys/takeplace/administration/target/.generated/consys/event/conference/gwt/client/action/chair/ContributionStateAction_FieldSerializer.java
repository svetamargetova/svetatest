package consys.event.conference.gwt.client.action.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ContributionStateAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum getToState(consys.event.conference.gwt.client.action.chair.ContributionStateAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ContributionStateAction::toState;
  }-*/;
  
  private static native void setToState(consys.event.conference.gwt.client.action.chair.ContributionStateAction instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ContributionStateAction::toState = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.conference.gwt.client.action.chair.ContributionStateAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.ContributionStateAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.conference.gwt.client.action.chair.ContributionStateAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.ContributionStateAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.chair.ContributionStateAction instance) throws SerializationException {
    setToState(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.chair.ContributionStateAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.chair.ContributionStateAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.chair.ContributionStateAction instance) throws SerializationException {
    streamWriter.writeObject(getToState(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.chair.ContributionStateAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ContributionStateAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.chair.ContributionStateAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.ContributionStateAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.chair.ContributionStateAction)object);
  }
  
}
