package consys.event.conference.gwt.client.bo.list;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientListContributionItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getDecidedDate(consys.event.conference.gwt.client.bo.list.ClientListContributionItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListContributionItem::decidedDate;
  }-*/;
  
  private static native void setDecidedDate(consys.event.conference.gwt.client.bo.list.ClientListContributionItem instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListContributionItem::decidedDate = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData getOverview(consys.event.conference.gwt.client.bo.list.ClientListContributionItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListContributionItem::overview;
  }-*/;
  
  private static native void setOverview(consys.event.conference.gwt.client.bo.list.ClientListContributionItem instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListContributionItem::overview = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.list.ClientListContributionItem instance) throws SerializationException {
    setDecidedDate(instance, (java.util.Date) streamReader.readObject());
    setOverview(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData) streamReader.readObject());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.list.ClientListContributionItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.list.ClientListContributionItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.list.ClientListContributionItem instance) throws SerializationException {
    streamWriter.writeObject(getDecidedDate(instance));
    streamWriter.writeObject(getOverview(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.list.ClientListContributionItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.list.ClientListContributionItem_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.list.ClientListContributionItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.list.ClientListContributionItem_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.list.ClientListContributionItem)object);
  }
  
}
