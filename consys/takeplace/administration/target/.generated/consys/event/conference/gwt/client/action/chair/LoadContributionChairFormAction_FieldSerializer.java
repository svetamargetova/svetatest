package consys.event.conference.gwt.client.action.chair;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadContributionChairFormAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContributionUuid(consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction instance) /*-{
    return instance.@consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction::contributionUuid;
  }-*/;
  
  private static native void setContributionUuid(consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction::contributionUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction instance) throws SerializationException {
    setContributionUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction instance) throws SerializationException {
    streamWriter.writeString(getContributionUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction)object);
  }
  
}
