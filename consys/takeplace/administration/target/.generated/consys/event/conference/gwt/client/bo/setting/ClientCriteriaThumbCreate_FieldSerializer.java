package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCriteriaThumbCreate_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate::description;
  }-*/;
  
  private static native void setDescription(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate::description = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate)object);
  }
  
}
