package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientSettingsReviewCycle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getAssignedArtefacts(consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle::assignedArtefacts;
  }-*/;
  
  private static native void setAssignedArtefacts(consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle::assignedArtefacts = value;
  }-*/;
  
  private static native java.util.Date getReviewFrom(consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle::reviewFrom;
  }-*/;
  
  private static native void setReviewFrom(consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle::reviewFrom = value;
  }-*/;
  
  private static native java.util.Date getReviewTo(consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle::reviewTo;
  }-*/;
  
  private static native void setReviewTo(consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance, java.util.Date value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle::reviewTo = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance) throws SerializationException {
    setAssignedArtefacts(instance, (java.util.ArrayList) streamReader.readObject());
    setReviewFrom(instance, (java.util.Date) streamReader.readObject());
    setReviewTo(instance, (java.util.Date) streamReader.readObject());
    
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle instance) throws SerializationException {
    streamWriter.writeObject(getAssignedArtefacts(instance));
    streamWriter.writeObject(getReviewFrom(instance));
    streamWriter.writeObject(getReviewTo(instance));
    
    consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle)object);
  }
  
}
