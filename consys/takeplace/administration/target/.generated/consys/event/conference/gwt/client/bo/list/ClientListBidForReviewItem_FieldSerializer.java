package consys.event.conference.gwt.client.bo.list;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientListBidForReviewItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getArtefacts(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::artefacts;
  }-*/;
  
  private static native void setArtefacts(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::artefacts = value;
  }-*/;
  
  private static native java.lang.String getAuthors(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::authors = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum getIntereset(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::intereset;
  }-*/;
  
  private static native void setIntereset(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance, consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::intereset = value;
  }-*/;
  
  private static native java.lang.String getSubtitle(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::subtitle;
  }-*/;
  
  private static native void setSubtitle(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::subtitle = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::title;
  }-*/;
  
  private static native void setTitle(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::title = value;
  }-*/;
  
  private static native java.util.ArrayList getTopics(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::topics;
  }-*/;
  
  private static native void setTopics(consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem::topics = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) throws SerializationException {
    setArtefacts(instance, (java.util.ArrayList) streamReader.readObject());
    setAuthors(instance, streamReader.readString());
    setIntereset(instance, (consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum) streamReader.readObject());
    setSubtitle(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    setTopics(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem instance) throws SerializationException {
    streamWriter.writeObject(getArtefacts(instance));
    streamWriter.writeString(getAuthors(instance));
    streamWriter.writeObject(getIntereset(instance));
    streamWriter.writeString(getSubtitle(instance));
    streamWriter.writeString(getTitle(instance));
    streamWriter.writeObject(getTopics(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem)object);
  }
  
}
