package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientArtefactReviews_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactName(consys.event.conference.gwt.client.bo.ClientArtefactReviews instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactReviews::artefactName;
  }-*/;
  
  private static native void setArtefactName(consys.event.conference.gwt.client.bo.ClientArtefactReviews instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactReviews::artefactName = value;
  }-*/;
  
  private static native java.util.ArrayList getNotes(consys.event.conference.gwt.client.bo.ClientArtefactReviews instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactReviews::notes;
  }-*/;
  
  private static native void setNotes(consys.event.conference.gwt.client.bo.ClientArtefactReviews instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactReviews::notes = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientArtefactReviews instance) throws SerializationException {
    setArtefactName(instance, streamReader.readString());
    setNotes(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.conference.gwt.client.bo.ClientArtefactReviews instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientArtefactReviews();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientArtefactReviews instance) throws SerializationException {
    streamWriter.writeString(getArtefactName(instance));
    streamWriter.writeObject(getNotes(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientArtefactReviews_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientArtefactReviews_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientArtefactReviews)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientArtefactReviews_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientArtefactReviews)object);
  }
  
}
