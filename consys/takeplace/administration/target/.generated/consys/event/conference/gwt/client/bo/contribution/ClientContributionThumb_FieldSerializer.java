package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientContributionThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.List getArtefacts(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::artefacts;
  }-*/;
  
  private static native void setArtefacts(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance, java.util.List value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::artefacts = value;
  }-*/;
  
  private static native java.lang.String getAuthors(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::authors = value;
  }-*/;
  
  private static native java.util.List getCoauthors(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::coauthors;
  }-*/;
  
  private static native void setCoauthors(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance, java.util.List value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::coauthors = value;
  }-*/;
  
  private static native consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum getState(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::state;
  }-*/;
  
  private static native void setState(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance, consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::state = value;
  }-*/;
  
  private static native java.lang.String getSubtitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::subtitle;
  }-*/;
  
  private static native void setSubtitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::subtitle = value;
  }-*/;
  
  private static native java.lang.String getTitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::title;
  }-*/;
  
  private static native void setTitle(consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb::title = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) throws SerializationException {
    setArtefacts(instance, (java.util.List) streamReader.readObject());
    setAuthors(instance, streamReader.readString());
    setCoauthors(instance, (java.util.List) streamReader.readObject());
    setState(instance, (consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum) streamReader.readObject());
    setSubtitle(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb instance) throws SerializationException {
    streamWriter.writeObject(getArtefacts(instance));
    streamWriter.writeString(getAuthors(instance));
    streamWriter.writeObject(getCoauthors(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeString(getSubtitle(instance));
    streamWriter.writeString(getTitle(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.contribution.ClientContributionThumb)object);
  }
  
}
