package consys.event.conference.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientArtefactEvaluationChair_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getArtefactTypeUuid(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::artefactTypeUuid;
  }-*/;
  
  private static native void setArtefactTypeUuid(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::artefactTypeUuid = value;
  }-*/;
  
  private static native java.lang.String getArtefactUuid(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::artefactUuid;
  }-*/;
  
  private static native void setArtefactUuid(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::artefactUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getCriteria(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::criteria;
  }-*/;
  
  private static native void setCriteria(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::criteria = value;
  }-*/;
  
  private static native java.lang.String getDescription(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::description;
  }-*/;
  
  private static native void setDescription(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::description = value;
  }-*/;
  
  private static native java.util.HashMap getEvaluations(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::evaluations;
  }-*/;
  
  private static native void setEvaluations(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, java.util.HashMap value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::evaluations = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::name;
  }-*/;
  
  private static native void setName(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, java.lang.String value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::name = value;
  }-*/;
  
  private static native int getNumOfReviewers(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::numOfReviewers;
  }-*/;
  
  private static native void setNumOfReviewers(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, int value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::numOfReviewers = value;
  }-*/;
  
  private static native boolean getRequired(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) /*-{
    return instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::required;
  }-*/;
  
  private static native void setRequired(consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance, boolean value) 
  /*-{
    instance.@consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair::required = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) throws SerializationException {
    setArtefactTypeUuid(instance, streamReader.readString());
    setArtefactUuid(instance, streamReader.readString());
    setCriteria(instance, (java.util.ArrayList) streamReader.readObject());
    setDescription(instance, streamReader.readString());
    setEvaluations(instance, (java.util.HashMap) streamReader.readObject());
    setName(instance, streamReader.readString());
    setNumOfReviewers(instance, streamReader.readInt());
    setRequired(instance, streamReader.readBoolean());
    
  }
  
  public static consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair instance) throws SerializationException {
    streamWriter.writeString(getArtefactTypeUuid(instance));
    streamWriter.writeString(getArtefactUuid(instance));
    streamWriter.writeObject(getCriteria(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeObject(getEvaluations(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeInt(getNumOfReviewers(instance));
    streamWriter.writeBoolean(getRequired(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair_FieldSerializer.deserialize(reader, (consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair_FieldSerializer.serialize(writer, (consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair)object);
  }
  
}
