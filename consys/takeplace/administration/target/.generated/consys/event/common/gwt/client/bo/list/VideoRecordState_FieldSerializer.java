package consys.event.common.gwt.client.bo.list;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class VideoRecordState_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.list.VideoRecordState instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static consys.event.common.gwt.client.bo.list.VideoRecordState instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    consys.event.common.gwt.client.bo.list.VideoRecordState[] values = consys.event.common.gwt.client.bo.list.VideoRecordState.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.list.VideoRecordState instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.list.VideoRecordState_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.list.VideoRecordState_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.list.VideoRecordState)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.list.VideoRecordState_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.list.VideoRecordState)object);
  }
  
}
