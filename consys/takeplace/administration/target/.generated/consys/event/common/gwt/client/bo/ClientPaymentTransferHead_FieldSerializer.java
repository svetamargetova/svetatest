package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPaymentTransferHead_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.Monetary getFees(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::fees;
  }-*/;
  
  private static native void setFees(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::fees = value;
  }-*/;
  
  private static native java.lang.String getInfo(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::info;
  }-*/;
  
  private static native void setInfo(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::info = value;
  }-*/;
  
  private static native consys.event.common.gwt.client.bo.ClientPaymentTransferState getState(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::state;
  }-*/;
  
  private static native void setState(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance, consys.event.common.gwt.client.bo.ClientPaymentTransferState value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::state = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getThirdPartyFees(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::thirdPartyFees;
  }-*/;
  
  private static native void setThirdPartyFees(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::thirdPartyFees = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getTotal(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::total;
  }-*/;
  
  private static native void setTotal(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::total = value;
  }-*/;
  
  private static native java.util.Date getTransferDate(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::transferDate;
  }-*/;
  
  private static native void setTransferDate(consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferHead::transferDate = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) throws SerializationException {
    setFees(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setInfo(instance, streamReader.readString());
    setState(instance, (consys.event.common.gwt.client.bo.ClientPaymentTransferState) streamReader.readObject());
    setThirdPartyFees(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setTotal(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setTransferDate(instance, (java.util.Date) streamReader.readObject());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.bo.ClientPaymentTransferHead instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientPaymentTransferHead();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientPaymentTransferHead instance) throws SerializationException {
    streamWriter.writeObject(getFees(instance));
    streamWriter.writeString(getInfo(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeObject(getThirdPartyFees(instance));
    streamWriter.writeObject(getTotal(instance));
    streamWriter.writeObject(getTransferDate(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientPaymentTransferHead_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentTransferHead_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientPaymentTransferHead)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentTransferHead_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientPaymentTransferHead)object);
  }
  
}
