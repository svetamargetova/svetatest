package consys.event.common.gwt.client.utils.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class ECoBundle_default_InlineClientBundleGenerator implements consys.event.common.gwt.client.utils.css.ECoBundle {
  private static ECoBundle_default_InlineClientBundleGenerator _instance0 = new ECoBundle_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new consys.event.common.gwt.client.utils.css.ECoCssResources() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GHAOYQ1J0{float:" + ("right")  + ";margin-top:" + ("7px")  + ";}.GHAOYQ1K0{float:" + ("right")  + ";margin-right:" + ("100px")  + ";margin-left:" + ("30px")  + ";}.GHAOYQ1M0{margin-right:" + ("5px")  + ";}.GHAOYQ1N0{clear:" + ("both")  + ";width:" + ("100%")  + ";}.GHAOYQ1N0 .GHAOYQ1B1{width:" + ("300px")  + ";float:" + ("right")  + ";}.GHAOYQ1N0 .GHAOYQ1O0{margin-right:") + (("10px")  + ";margin-left:" + ("5px")  + ";width:" + ("190px")  + ";float:" + ("left")  + ";}.GHAOYQ1O0 .GHAOYQ1A1{font-size:" + ("11px")  + ";margin:" + ("5px"+ " " +"0")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1O0 .GHAOYQ1P0{font-size:" + ("11px")  + ";}.GHAOYQ1H0 a{color:" + ("#1a809a")  + ";text-decoration:" + ("underline")  + ";}.GHAOYQ1H0 a:hover{color:" + ("#1a809a") ) + (";text-decoration:" + ("none")  + ";}.GHAOYQ1H0 ul{margin:" + ("10px"+ " " +"20px"+ " " +"10px"+ " " +"0")  + ";}.GHAOYQ1H0 p{margin:" + ("7px"+ " " +"0")  + ";}.GHAOYQ1I0 h1{font-size:" + ("24px")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1I0 h2{font-size:" + ("18px")  + ";font-weight:" + ("bold")  + ";margin-top:" + ("30px")  + ";}.GHAOYQ1I0 .GHAOYQ1H0{margin-top:" + ("10px")  + ";width:" + ("700px")  + ";overflow:") + (("hidden")  + ";}.GHAOYQ1I0 .date1{color:" + ("#1a809a")  + ";float:" + ("right")  + ";font-weight:" + ("bold")  + ";width:" + ("145px")  + ";}.GHAOYQ1I0 .date2{float:" + ("right")  + ";margin-right:" + ("15px")  + ";width:" + ("530px")  + ";}.GHAOYQ1C1{float:" + ("right")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1L0{float:" + ("right") ) + (";margin-right:" + ("20px")  + ";}.GHAOYQ1I1{margin-bottom:" + ("10px")  + ";display:" + ("block")  + ";}.GHAOYQ1J1{margin-bottom:" + ("3px")  + ";display:" + ("block")  + ";}.GHAOYQ1K1{font-size:" + ("18px")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1L1{float:" + ("right")  + ";}.GHAOYQ1O1{float:" + ("right")  + ";margin-left:" + ("20px")  + ";font-weight:") + (("bold")  + ";}.GHAOYQ1D1{width:" + ("364px")  + ";height:" + ("364px")  + ";border:" + ("1px"+ " " +"solid"+ " " +"#929292")  + ";}.GHAOYQ1E1{float:" + ("right")  + ";width:" + ("270px")  + ";margin-right:" + ("15px")  + ";margin-top:" + ("10px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1F1{float:" + ("right")  + ";margin:" + ("10px"+ " " +"20px"+ " " +"10px"+ " " +"0") ) + (";}.GHAOYQ1G1{position:" + ("relative")  + ";width:" + ("128px")  + ";top:" + ("121px")  + ";right:" + ("118px")  + ";}.GHAOYQ1N1{position:" + ("relative")  + ";right:" + ("48px")  + ";}.GHAOYQ1M1{margin-top:" + ("10px")  + ";font-weight:" + ("bold")  + ";width:" + ("128px")  + ";text-align:" + ("center")  + ";}.GHAOYQ1H1{float:") + (("left")  + ";margin:" + ("10px"+ " " +"0")  + ";}")) : ((".GHAOYQ1J0{float:" + ("left")  + ";margin-top:" + ("7px")  + ";}.GHAOYQ1K0{float:" + ("left")  + ";margin-left:" + ("100px")  + ";margin-right:" + ("30px")  + ";}.GHAOYQ1M0{margin-left:" + ("5px")  + ";}.GHAOYQ1N0{clear:" + ("both")  + ";width:" + ("100%")  + ";}.GHAOYQ1N0 .GHAOYQ1B1{width:" + ("300px")  + ";float:" + ("left")  + ";}.GHAOYQ1N0 .GHAOYQ1O0{margin-left:") + (("10px")  + ";margin-right:" + ("5px")  + ";width:" + ("190px")  + ";float:" + ("right")  + ";}.GHAOYQ1O0 .GHAOYQ1A1{font-size:" + ("11px")  + ";margin:" + ("5px"+ " " +"0")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1O0 .GHAOYQ1P0{font-size:" + ("11px")  + ";}.GHAOYQ1H0 a{color:" + ("#1a809a")  + ";text-decoration:" + ("underline")  + ";}.GHAOYQ1H0 a:hover{color:" + ("#1a809a") ) + (";text-decoration:" + ("none")  + ";}.GHAOYQ1H0 ul{margin:" + ("10px"+ " " +"0"+ " " +"10px"+ " " +"20px")  + ";}.GHAOYQ1H0 p{margin:" + ("7px"+ " " +"0")  + ";}.GHAOYQ1I0 h1{font-size:" + ("24px")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1I0 h2{font-size:" + ("18px")  + ";font-weight:" + ("bold")  + ";margin-top:" + ("30px")  + ";}.GHAOYQ1I0 .GHAOYQ1H0{margin-top:" + ("10px")  + ";width:" + ("700px")  + ";overflow:") + (("hidden")  + ";}.GHAOYQ1I0 .date1{color:" + ("#1a809a")  + ";float:" + ("left")  + ";font-weight:" + ("bold")  + ";width:" + ("145px")  + ";}.GHAOYQ1I0 .date2{float:" + ("left")  + ";margin-left:" + ("15px")  + ";width:" + ("530px")  + ";}.GHAOYQ1C1{float:" + ("left")  + ";margin-top:" + ("3px")  + ";}.GHAOYQ1L0{float:" + ("left") ) + (";margin-left:" + ("20px")  + ";}.GHAOYQ1I1{margin-bottom:" + ("10px")  + ";display:" + ("block")  + ";}.GHAOYQ1J1{margin-bottom:" + ("3px")  + ";display:" + ("block")  + ";}.GHAOYQ1K1{font-size:" + ("18px")  + ";font-weight:" + ("bold")  + ";}.GHAOYQ1L1{float:" + ("left")  + ";}.GHAOYQ1O1{float:" + ("left")  + ";margin-right:" + ("20px")  + ";font-weight:") + (("bold")  + ";}.GHAOYQ1D1{width:" + ("364px")  + ";height:" + ("364px")  + ";border:" + ("1px"+ " " +"solid"+ " " +"#929292")  + ";}.GHAOYQ1E1{float:" + ("left")  + ";width:" + ("270px")  + ";margin-left:" + ("15px")  + ";margin-top:" + ("10px")  + ";overflow:" + ("hidden")  + ";}.GHAOYQ1F1{float:" + ("left")  + ";margin:" + ("10px"+ " " +"0"+ " " +"10px"+ " " +"20px") ) + (";}.GHAOYQ1G1{position:" + ("relative")  + ";width:" + ("128px")  + ";top:" + ("121px")  + ";left:" + ("118px")  + ";}.GHAOYQ1N1{position:" + ("relative")  + ";left:" + ("48px")  + ";}.GHAOYQ1M1{margin-top:" + ("10px")  + ";font-weight:" + ("bold")  + ";width:" + ("128px")  + ";text-align:" + ("center")  + ";}.GHAOYQ1H1{float:") + (("right")  + ";margin:" + ("10px"+ " " +"0")  + ";}"));
      }
      public java.lang.String eventDescription(){
        return "GHAOYQ1H0";
      }
      public java.lang.String eventProfile(){
        return "GHAOYQ1I0";
      }
      public java.lang.String eventWallRegister(){
        return "GHAOYQ1J0";
      }
      public java.lang.String paymentDialogOk(){
        return "GHAOYQ1K0";
      }
      public java.lang.String redClone(){
        return "GHAOYQ1L0";
      }
      public java.lang.String rightListItem(){
        return "GHAOYQ1M0";
      }
      public java.lang.String rightsDialogBody(){
        return "GHAOYQ1N0";
      }
      public java.lang.String rightsDialogHelp(){
        return "GHAOYQ1O0";
      }
      public java.lang.String rightsDialogHelpDesc(){
        return "GHAOYQ1P0";
      }
      public java.lang.String rightsDialogHelpTitle(){
        return "GHAOYQ1A1";
      }
      public java.lang.String rightsDialogList(){
        return "GHAOYQ1B1";
      }
      public java.lang.String seriesLabel(){
        return "GHAOYQ1C1";
      }
      public java.lang.String videoDisplay(){
        return "GHAOYQ1D1";
      }
      public java.lang.String videoDisplayL(){
        return "GHAOYQ1E1";
      }
      public java.lang.String videoDisplayR(){
        return "GHAOYQ1F1";
      }
      public java.lang.String videoDisplayUpload(){
        return "GHAOYQ1G1";
      }
      public java.lang.String videoEdit(){
        return "GHAOYQ1H1";
      }
      public java.lang.String videoItem(){
        return "GHAOYQ1I1";
      }
      public java.lang.String videoItemPeople(){
        return "GHAOYQ1J1";
      }
      public java.lang.String videoLabel(){
        return "GHAOYQ1K1";
      }
      public java.lang.String videoLink(){
        return "GHAOYQ1L1";
      }
      public java.lang.String videoPercentLabel(){
        return "GHAOYQ1M1";
      }
      public java.lang.String videoPercentWaiting(){
        return "GHAOYQ1N1";
      }
      public java.lang.String videoState(){
        return "GHAOYQ1O1";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.event.common.gwt.client.utils.css.ECoCssResources get() {
      return css;
    }
  }
  public consys.event.common.gwt.client.utils.css.ECoCssResources css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.event.common.gwt.client.utils.css.ECoCssResources css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@consys.event.common.gwt.client.utils.css.ECoBundle::css()();
    }
    return null;
  }-*/;
}
