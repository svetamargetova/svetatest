package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateCommitteeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.InvitedUser getChair(consys.event.common.gwt.client.action.CreateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::chair;
  }-*/;
  
  private static native void setChair(consys.event.common.gwt.client.action.CreateCommitteeAction instance, consys.common.gwt.shared.bo.InvitedUser value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::chair = value;
  }-*/;
  
  private static native java.util.ArrayList getChairRights(consys.event.common.gwt.client.action.CreateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::chairRights;
  }-*/;
  
  private static native void setChairRights(consys.event.common.gwt.client.action.CreateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::chairRights = value;
  }-*/;
  
  private static native java.util.ArrayList getMembers(consys.event.common.gwt.client.action.CreateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::members;
  }-*/;
  
  private static native void setMembers(consys.event.common.gwt.client.action.CreateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::members = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.common.gwt.client.action.CreateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::name;
  }-*/;
  
  private static native void setName(consys.event.common.gwt.client.action.CreateCommitteeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::name = value;
  }-*/;
  
  private static native java.util.ArrayList getRights(consys.event.common.gwt.client.action.CreateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::rights;
  }-*/;
  
  private static native void setRights(consys.event.common.gwt.client.action.CreateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CreateCommitteeAction::rights = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.CreateCommitteeAction instance) throws SerializationException {
    setChair(instance, (consys.common.gwt.shared.bo.InvitedUser) streamReader.readObject());
    setChairRights(instance, (java.util.ArrayList) streamReader.readObject());
    setMembers(instance, (java.util.ArrayList) streamReader.readObject());
    setName(instance, streamReader.readString());
    setRights(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.CreateCommitteeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.CreateCommitteeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.CreateCommitteeAction instance) throws SerializationException {
    streamWriter.writeObject(getChair(instance));
    streamWriter.writeObject(getChairRights(instance));
    streamWriter.writeObject(getMembers(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getRights(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.CreateCommitteeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.CreateCommitteeAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.CreateCommitteeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.CreateCommitteeAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.CreateCommitteeAction)object);
  }
  
}
