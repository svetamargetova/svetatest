package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateEventLogoAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getLogoPregix(consys.event.common.gwt.client.action.UpdateEventLogoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventLogoAction::logoPregix;
  }-*/;
  
  private static native void setLogoPregix(consys.event.common.gwt.client.action.UpdateEventLogoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventLogoAction::logoPregix = value;
  }-*/;
  
  private static native boolean getProfile(consys.event.common.gwt.client.action.UpdateEventLogoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventLogoAction::profile;
  }-*/;
  
  private static native void setProfile(consys.event.common.gwt.client.action.UpdateEventLogoAction instance, boolean value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventLogoAction::profile = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateEventLogoAction instance) throws SerializationException {
    setLogoPregix(instance, streamReader.readString());
    setProfile(instance, streamReader.readBoolean());
    
  }
  
  public static consys.event.common.gwt.client.action.UpdateEventLogoAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateEventLogoAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateEventLogoAction instance) throws SerializationException {
    streamWriter.writeString(getLogoPregix(instance));
    streamWriter.writeBoolean(getProfile(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateEventLogoAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventLogoAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateEventLogoAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventLogoAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateEventLogoAction)object);
  }
  
}
