package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateEventPaymentProfileAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientEventPaymentProfile getProfile(consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction::profile;
  }-*/;
  
  private static native void setProfile(consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction instance, consys.common.gwt.shared.bo.ClientEventPaymentProfile value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction::profile = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction instance) throws SerializationException {
    setProfile(instance, (consys.common.gwt.shared.bo.ClientEventPaymentProfile) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction instance) throws SerializationException {
    streamWriter.writeObject(getProfile(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction)object);
  }
  
}
