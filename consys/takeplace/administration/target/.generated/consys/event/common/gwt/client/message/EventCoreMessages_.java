package consys.event.common.gwt.client.message;

public class EventCoreMessages_ implements consys.event.common.gwt.client.message.EventCoreMessages {
  
  public java.lang.String baseSettingsContent_text_infoCloned(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("New <a href=\"").append(arg0).append("\">event</a> series has been created successfully.").toString();
  }
  
  public java.lang.String paymentsLicencesContent_text_confirmed(java.lang.String arg0,java.lang.String arg1,java.lang.String arg2) {
    return new java.lang.StringBuffer().append("Congratulation you have successfully activate your event. ").append(arg0).append(" is now active from ").append(arg1).append(" to ").append(arg2).append(". If there are any problems, contact us immediately on support@takeplace.eu.").toString();
  }
  
  public java.lang.String partnerBannerSettings_TypeNote(int arg0) {
    return new java.lang.StringBuffer().append("Please provide picture that is ").append(arg0).append(" pixels wide. Otherwise when width will be over ").append(arg0).append(" pixels we will adapt it.").toString();
  }
}
