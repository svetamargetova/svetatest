package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPaymentTransferItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.Monetary getFees(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::fees;
  }-*/;
  
  private static native void setFees(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::fees = value;
  }-*/;
  
  private static native java.lang.String getInfo(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::info;
  }-*/;
  
  private static native void setInfo(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::info = value;
  }-*/;
  
  private static native java.util.Date getInvoicePaidDate(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::invoicePaidDate;
  }-*/;
  
  private static native void setInvoicePaidDate(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::invoicePaidDate = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getThirdPartyFees(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::thirdPartyFees;
  }-*/;
  
  private static native void setThirdPartyFees(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::thirdPartyFees = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.Monetary getTotal(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::total;
  }-*/;
  
  private static native void setTotal(consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance, consys.common.gwt.shared.bo.Monetary value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferItem::total = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance) throws SerializationException {
    setFees(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setInfo(instance, streamReader.readString());
    setInvoicePaidDate(instance, (java.util.Date) streamReader.readObject());
    setThirdPartyFees(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    setTotal(instance, (consys.common.gwt.shared.bo.Monetary) streamReader.readObject());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.bo.ClientPaymentTransferItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientPaymentTransferItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientPaymentTransferItem instance) throws SerializationException {
    streamWriter.writeObject(getFees(instance));
    streamWriter.writeString(getInfo(instance));
    streamWriter.writeObject(getInvoicePaidDate(instance));
    streamWriter.writeObject(getThirdPartyFees(instance));
    streamWriter.writeObject(getTotal(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientPaymentTransferItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentTransferItem_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientPaymentTransferItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentTransferItem_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientPaymentTransferItem)object);
  }
  
}
