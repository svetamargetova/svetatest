package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadEventDetailsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEventUuid(consys.event.common.gwt.client.action.LoadEventDetailsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.LoadEventDetailsAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.event.common.gwt.client.action.LoadEventDetailsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.LoadEventDetailsAction::eventUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.LoadEventDetailsAction instance) throws SerializationException {
    setEventUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.common.gwt.client.action.LoadEventDetailsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.LoadEventDetailsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.LoadEventDetailsAction instance) throws SerializationException {
    streamWriter.writeString(getEventUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.LoadEventDetailsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadEventDetailsAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.LoadEventDetailsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadEventDetailsAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.LoadEventDetailsAction)object);
  }
  
}
