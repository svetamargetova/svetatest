package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEventLicense_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getActivatedFrom(consys.event.common.gwt.client.bo.ClientEventLicense instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventLicense::activatedFrom;
  }-*/;
  
  private static native void setActivatedFrom(consys.event.common.gwt.client.bo.ClientEventLicense instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventLicense::activatedFrom = value;
  }-*/;
  
  private static native java.util.Date getActivatedTo(consys.event.common.gwt.client.bo.ClientEventLicense instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventLicense::activatedTo;
  }-*/;
  
  private static native void setActivatedTo(consys.event.common.gwt.client.bo.ClientEventLicense instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventLicense::activatedTo = value;
  }-*/;
  
  private static native java.util.Date getLicenseConfirmed(consys.event.common.gwt.client.bo.ClientEventLicense instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventLicense::licenseConfirmed;
  }-*/;
  
  private static native void setLicenseConfirmed(consys.event.common.gwt.client.bo.ClientEventLicense instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventLicense::licenseConfirmed = value;
  }-*/;
  
  private static native java.util.Date getLicenseDue(consys.event.common.gwt.client.bo.ClientEventLicense instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventLicense::licenseDue;
  }-*/;
  
  private static native void setLicenseDue(consys.event.common.gwt.client.bo.ClientEventLicense instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventLicense::licenseDue = value;
  }-*/;
  
  private static native java.util.Date getLicensePurchased(consys.event.common.gwt.client.bo.ClientEventLicense instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventLicense::licensePurchased;
  }-*/;
  
  private static native void setLicensePurchased(consys.event.common.gwt.client.bo.ClientEventLicense instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventLicense::licensePurchased = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientEventLicense instance) throws SerializationException {
    setActivatedFrom(instance, (java.util.Date) streamReader.readObject());
    setActivatedTo(instance, (java.util.Date) streamReader.readObject());
    setLicenseConfirmed(instance, (java.util.Date) streamReader.readObject());
    setLicenseDue(instance, (java.util.Date) streamReader.readObject());
    setLicensePurchased(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientEventLicense instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientEventLicense();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientEventLicense instance) throws SerializationException {
    streamWriter.writeObject(getActivatedFrom(instance));
    streamWriter.writeObject(getActivatedTo(instance));
    streamWriter.writeObject(getLicenseConfirmed(instance));
    streamWriter.writeObject(getLicenseDue(instance));
    streamWriter.writeObject(getLicensePurchased(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientEventLicense_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientEventLicense_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientEventLicense)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientEventLicense_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientEventLicense)object);
  }
  
}
