package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadEventPaymentProfileAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.event.common.gwt.client.action.LoadEventPaymentProfileAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.LoadEventPaymentProfileAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.common.gwt.client.action.LoadEventPaymentProfileAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.LoadEventPaymentProfileAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.LoadEventPaymentProfileAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.event.common.gwt.client.action.LoadEventPaymentProfileAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.LoadEventPaymentProfileAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.LoadEventPaymentProfileAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.LoadEventPaymentProfileAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadEventPaymentProfileAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.LoadEventPaymentProfileAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadEventPaymentProfileAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.LoadEventPaymentProfileAction)object);
  }
  
}
