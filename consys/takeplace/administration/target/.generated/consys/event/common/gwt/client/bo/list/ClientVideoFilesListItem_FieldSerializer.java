package consys.event.common.gwt.client.bo.list;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientVideoFilesListItem_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAuthors(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::authors = value;
  }-*/;
  
  private static native java.lang.String getCustomName(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::customName;
  }-*/;
  
  private static native void setCustomName(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::customName = value;
  }-*/;
  
  private static native java.lang.String getDescription(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::description;
  }-*/;
  
  private static native void setDescription(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::description = value;
  }-*/;
  
  private static native boolean getProcessing(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::processing;
  }-*/;
  
  private static native void setProcessing(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, boolean value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::processing = value;
  }-*/;
  
  private static native java.lang.String getSpeakers(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::speakers;
  }-*/;
  
  private static native void setSpeakers(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::speakers = value;
  }-*/;
  
  private static native consys.event.common.gwt.client.bo.list.VideoRecordState getState(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::state;
  }-*/;
  
  private static native void setState(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, consys.event.common.gwt.client.bo.list.VideoRecordState value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::state = value;
  }-*/;
  
  private static native java.lang.String getSystemName(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::systemName;
  }-*/;
  
  private static native void setSystemName(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::systemName = value;
  }-*/;
  
  private static native java.lang.String getUrl(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::url;
  }-*/;
  
  private static native void setUrl(consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem::url = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) throws SerializationException {
    setAuthors(instance, streamReader.readString());
    setCustomName(instance, streamReader.readString());
    setDescription(instance, streamReader.readString());
    setProcessing(instance, streamReader.readBoolean());
    setSpeakers(instance, streamReader.readString());
    setState(instance, (consys.event.common.gwt.client.bo.list.VideoRecordState) streamReader.readObject());
    setSystemName(instance, streamReader.readString());
    setUrl(instance, streamReader.readString());
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem instance) throws SerializationException {
    streamWriter.writeString(getAuthors(instance));
    streamWriter.writeString(getCustomName(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeBoolean(getProcessing(instance));
    streamWriter.writeString(getSpeakers(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeString(getSystemName(instance));
    streamWriter.writeString(getUrl(instance));
    
    consys.common.gwt.client.ui.comp.list.item.DataListUuidItem_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem)object);
  }
  
}
