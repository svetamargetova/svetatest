package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateVideoRecordAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFileName(consys.event.common.gwt.client.action.CreateVideoRecordAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CreateVideoRecordAction::fileName;
  }-*/;
  
  private static native void setFileName(consys.event.common.gwt.client.action.CreateVideoRecordAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CreateVideoRecordAction::fileName = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.event.common.gwt.client.action.CreateVideoRecordAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CreateVideoRecordAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.event.common.gwt.client.action.CreateVideoRecordAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CreateVideoRecordAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.CreateVideoRecordAction instance) throws SerializationException {
    setFileName(instance, streamReader.readString());
    setUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.CreateVideoRecordAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.CreateVideoRecordAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.CreateVideoRecordAction instance) throws SerializationException {
    streamWriter.writeString(getFileName(instance));
    streamWriter.writeString(getUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.CreateVideoRecordAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.CreateVideoRecordAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.CreateVideoRecordAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.CreateVideoRecordAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.CreateVideoRecordAction)object);
  }
  
}
