package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPaymentProfileDialog_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientUserPaymentProfile getFirstProfile(consys.event.common.gwt.client.bo.ClientPaymentProfileDialog instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentProfileDialog::firstProfile;
  }-*/;
  
  private static native void setFirstProfile(consys.event.common.gwt.client.bo.ClientPaymentProfileDialog instance, consys.common.gwt.shared.bo.ClientUserPaymentProfile value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentProfileDialog::firstProfile = value;
  }-*/;
  
  private static native java.util.ArrayList getProfileThumbs(consys.event.common.gwt.client.bo.ClientPaymentProfileDialog instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentProfileDialog::profileThumbs;
  }-*/;
  
  private static native void setProfileThumbs(consys.event.common.gwt.client.bo.ClientPaymentProfileDialog instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentProfileDialog::profileThumbs = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientPaymentProfileDialog instance) throws SerializationException {
    setFirstProfile(instance, (consys.common.gwt.shared.bo.ClientUserPaymentProfile) streamReader.readObject());
    setProfileThumbs(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientPaymentProfileDialog instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientPaymentProfileDialog();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientPaymentProfileDialog instance) throws SerializationException {
    streamWriter.writeObject(getFirstProfile(instance));
    streamWriter.writeObject(getProfileThumbs(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientPaymentProfileDialog_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentProfileDialog_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientPaymentProfileDialog)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentProfileDialog_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientPaymentProfileDialog)object);
  }
  
}
