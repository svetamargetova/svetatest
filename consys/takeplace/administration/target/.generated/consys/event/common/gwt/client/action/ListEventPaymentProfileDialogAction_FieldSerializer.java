package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListEventPaymentProfileDialogAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction instance) throws SerializationException {
    
    consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction instance) throws SerializationException {
    
    consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction)object);
  }
  
}
