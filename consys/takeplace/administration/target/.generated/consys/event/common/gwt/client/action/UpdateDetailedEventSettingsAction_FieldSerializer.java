package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateDetailedEventSettingsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCity(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::city;
  }-*/;
  
  private static native void setCity(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::city = value;
  }-*/;
  
  private static native java.lang.String getEventUuid(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::eventUuid = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientDateTime getFrom(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::from;
  }-*/;
  
  private static native void setFrom(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, consys.common.gwt.shared.bo.ClientDateTime value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::from = value;
  }-*/;
  
  private static native java.lang.Integer getLocation(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::location;
  }-*/;
  
  private static native void setLocation(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, java.lang.Integer value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::location = value;
  }-*/;
  
  private static native java.lang.String getPlace(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::place;
  }-*/;
  
  private static native void setPlace(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::place = value;
  }-*/;
  
  private static native java.lang.String getStreet(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::street;
  }-*/;
  
  private static native void setStreet(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::street = value;
  }-*/;
  
  private static native java.lang.String getTimeZone(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::timeZone;
  }-*/;
  
  private static native void setTimeZone(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::timeZone = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientDateTime getTo(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::to;
  }-*/;
  
  private static native void setTo(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, consys.common.gwt.shared.bo.ClientDateTime value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::to = value;
  }-*/;
  
  private static native java.lang.String getWeb(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::web;
  }-*/;
  
  private static native void setWeb(consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction::web = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) throws SerializationException {
    setCity(instance, streamReader.readString());
    setEventUuid(instance, streamReader.readString());
    setFrom(instance, (consys.common.gwt.shared.bo.ClientDateTime) streamReader.readObject());
    setLocation(instance, (java.lang.Integer) streamReader.readObject());
    setPlace(instance, streamReader.readString());
    setStreet(instance, streamReader.readString());
    setTimeZone(instance, streamReader.readString());
    setTo(instance, (consys.common.gwt.shared.bo.ClientDateTime) streamReader.readObject());
    setWeb(instance, streamReader.readString());
    
  }
  
  public static consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction instance) throws SerializationException {
    streamWriter.writeString(getCity(instance));
    streamWriter.writeString(getEventUuid(instance));
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeObject(getLocation(instance));
    streamWriter.writeString(getPlace(instance));
    streamWriter.writeString(getStreet(instance));
    streamWriter.writeString(getTimeZone(instance));
    streamWriter.writeObject(getTo(instance));
    streamWriter.writeString(getWeb(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction)object);
  }
  
}
