package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteEventVideoAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getSystemName(consys.event.common.gwt.client.action.DeleteEventVideoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.DeleteEventVideoAction::systemName;
  }-*/;
  
  private static native void setSystemName(consys.event.common.gwt.client.action.DeleteEventVideoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.DeleteEventVideoAction::systemName = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.DeleteEventVideoAction instance) throws SerializationException {
    setSystemName(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.DeleteEventVideoAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.DeleteEventVideoAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.DeleteEventVideoAction instance) throws SerializationException {
    streamWriter.writeString(getSystemName(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.DeleteEventVideoAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.DeleteEventVideoAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.DeleteEventVideoAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.DeleteEventVideoAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.DeleteEventVideoAction)object);
  }
  
}
