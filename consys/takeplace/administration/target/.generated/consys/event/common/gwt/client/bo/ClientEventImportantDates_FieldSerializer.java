package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEventImportantDates_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getContributionFrom(consys.event.common.gwt.client.bo.ClientEventImportantDates instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::contributionFrom;
  }-*/;
  
  private static native void setContributionFrom(consys.event.common.gwt.client.bo.ClientEventImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::contributionFrom = value;
  }-*/;
  
  private static native java.util.Date getContributionTo(consys.event.common.gwt.client.bo.ClientEventImportantDates instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::contributionTo;
  }-*/;
  
  private static native void setContributionTo(consys.event.common.gwt.client.bo.ClientEventImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::contributionTo = value;
  }-*/;
  
  private static native java.util.Date getRegistrationFrom(consys.event.common.gwt.client.bo.ClientEventImportantDates instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::registrationFrom;
  }-*/;
  
  private static native void setRegistrationFrom(consys.event.common.gwt.client.bo.ClientEventImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::registrationFrom = value;
  }-*/;
  
  private static native java.util.Date getRegistrationTo(consys.event.common.gwt.client.bo.ClientEventImportantDates instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::registrationTo;
  }-*/;
  
  private static native void setRegistrationTo(consys.event.common.gwt.client.bo.ClientEventImportantDates instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventImportantDates::registrationTo = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientEventImportantDates instance) throws SerializationException {
    setContributionFrom(instance, (java.util.Date) streamReader.readObject());
    setContributionTo(instance, (java.util.Date) streamReader.readObject());
    setRegistrationFrom(instance, (java.util.Date) streamReader.readObject());
    setRegistrationTo(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientEventImportantDates instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientEventImportantDates();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientEventImportantDates instance) throws SerializationException {
    streamWriter.writeObject(getContributionFrom(instance));
    streamWriter.writeObject(getContributionTo(instance));
    streamWriter.writeObject(getRegistrationFrom(instance));
    streamWriter.writeObject(getRegistrationTo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientEventImportantDates_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientEventImportantDates_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientEventImportantDates)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientEventImportantDates_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientEventImportantDates)object);
  }
  
}
