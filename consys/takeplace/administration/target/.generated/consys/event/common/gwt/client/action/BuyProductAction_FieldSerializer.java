package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class BuyProductAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCustomerProfileUuid(consys.event.common.gwt.client.action.BuyProductAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.BuyProductAction::customerProfileUuid;
  }-*/;
  
  private static native void setCustomerProfileUuid(consys.event.common.gwt.client.action.BuyProductAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.BuyProductAction::customerProfileUuid = value;
  }-*/;
  
  private static native java.lang.String getCutomerUuid(consys.event.common.gwt.client.action.BuyProductAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.BuyProductAction::cutomerUuid;
  }-*/;
  
  private static native void setCutomerUuid(consys.event.common.gwt.client.action.BuyProductAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.BuyProductAction::cutomerUuid = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientUserPaymentProfile getEditedProfile(consys.event.common.gwt.client.action.BuyProductAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.BuyProductAction::editedProfile;
  }-*/;
  
  private static native void setEditedProfile(consys.event.common.gwt.client.action.BuyProductAction instance, consys.common.gwt.shared.bo.ClientUserPaymentProfile value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.BuyProductAction::editedProfile = value;
  }-*/;
  
  private static native java.util.ArrayList getProducts(consys.event.common.gwt.client.action.BuyProductAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.BuyProductAction::products;
  }-*/;
  
  private static native void setProducts(consys.event.common.gwt.client.action.BuyProductAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.BuyProductAction::products = value;
  }-*/;
  
  private static native boolean getProfileEdited(consys.event.common.gwt.client.action.BuyProductAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.BuyProductAction::profileEdited;
  }-*/;
  
  private static native void setProfileEdited(consys.event.common.gwt.client.action.BuyProductAction instance, boolean value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.BuyProductAction::profileEdited = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.BuyProductAction instance) throws SerializationException {
    setCustomerProfileUuid(instance, streamReader.readString());
    setCutomerUuid(instance, streamReader.readString());
    setEditedProfile(instance, (consys.common.gwt.shared.bo.ClientUserPaymentProfile) streamReader.readObject());
    setProducts(instance, (java.util.ArrayList) streamReader.readObject());
    setProfileEdited(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.BuyProductAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.BuyProductAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.BuyProductAction instance) throws SerializationException {
    streamWriter.writeString(getCustomerProfileUuid(instance));
    streamWriter.writeString(getCutomerUuid(instance));
    streamWriter.writeObject(getEditedProfile(instance));
    streamWriter.writeObject(getProducts(instance));
    streamWriter.writeBoolean(getProfileEdited(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.BuyProductAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.BuyProductAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.BuyProductAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.BuyProductAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.BuyProductAction)object);
  }
  
}
