package consys.event.common.gwt.client.message;

public class EventCoreConstants_ implements consys.event.common.gwt.client.message.EventCoreConstants {
  
  public java.lang.String paymentTransfers_help_preparedTransfer() {
    return "Prepared transfer";
  }
  
  public java.lang.String const_error_currencyNotFoundInCache() {
    return "This currency cannot be used";
  }
  
  public java.lang.String paymentTransfersDetail_form_sum() {
    return "Sum";
  }
  
  public java.lang.String videoFilesSettings_state_processed() {
    return "Processed";
  }
  
  public java.lang.String logoSettingsContent_text_sizeInfo() {
    return "Maximal event logo size is 300x40";
  }
  
  public java.lang.String const_downloadProFormaInvoice() {
    return "Download pro forma invoice";
  }
  
  public java.lang.String paymentsLicencesContent_form_agreements() {
    return "Agreements";
  }
  
  public java.lang.String videoFilesSettings_state_error() {
    return "Error";
  }
  
  public java.lang.String videoFilesSettings_action_addVideo() {
    return "Add video";
  }
  
  public java.lang.String partnerBannerSettings_changeImage() {
    return "Change image";
  }
  
  public java.lang.String partnerBannerSettings_footerTypeTitle() {
    return "Footer image";
  }
  
  public java.lang.String const_invoice() {
    return "Invoice";
  }
  
  public java.lang.String paymentsLicencesContent_text_agree1() {
    return "I agree with the <a href=\"http://static.takeplace.eu/Contract_on_Provision_of_Services_9_clear_en.pdf\" target=\"_blank\">Contract on Provision of Services</a>";
  }
  
  public java.lang.String paymentsProfileContent_helpText2() {
    return "Be aware that without license you can't activate your event and can't be found on public events list.";
  }
  
  public java.lang.String committee_form_committeeChair() {
    return "Chair";
  }
  
  public java.lang.String eventSettings_text_updateSuccess() {
    return "Event settings have been updated.";
  }
  
  public java.lang.String paymentProfileContent_form_noVatPayer() {
    return "Non-payer of VAT";
  }
  
  public java.lang.String paymentTransfers_text_date() {
    return "Date";
  }
  
  public java.lang.String committee_form_committeeMembers() {
    return "Members";
  }
  
  public java.lang.String detailSettingsContent_error_eventMustStartInSameYear() {
    return "Inserted year of begin date of your event is different from the one you have entered when creating the event. If you want to keep your begin date please change year of your event.";
  }
  
  public java.lang.String profileLogoSettingsContent_text_sizeInfo() {
    return "Maximal profile logo size is 90x90";
  }
  
  public java.lang.String videoFilesSettings_text_authors() {
    return "Authors";
  }
  
  public java.lang.String paymentsProfileContent_helpText3() {
    return "DOPLNIT NÁPOVĚDU PRO Invoice note - je to text, který se zobrazuje na faktuře. Pokud na ní nejsou potřeba dodatečné informace tak nechat prázdné.";
  }
  
  public java.lang.String paymentTransfers_title() {
    return "Payment transfers";
  }
  
  public java.lang.String communitySettings_title() {
    return "Event details / web";
  }
  
  public java.lang.String eventWall_button_sendContribution() {
    return "SEND CONTRIBUTION";
  }
  
  public java.lang.String videoFilesSettings_error_unsupportedBrowser() {
    return "No file selected or your browser not support check upload file size.";
  }
  
  public java.lang.String const_date() {
    return "Date";
  }
  
  public java.lang.String eventWall_button_register() {
    return "REGISTER";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessageAsEvent() {
    return "Post official messages";
  }
  
  public java.lang.String paymentsLicencesContent_title() {
    return "Agreements";
  }
  
  public java.lang.String eventCommittees_title() {
    return "Committees";
  }
  
  public java.lang.String videoFilesListItem_text_processing() {
    return "PROCESSING";
  }
  
  public java.lang.String paymentsLicencesContent_button_activateEvent() {
    return "ACTIVATE EVENT";
  }
  
  public java.lang.String paymentTransfers_help_errorTransfer() {
    return "Error transfer";
  }
  
  public java.lang.String logoSettingsContent_text_logoMissing() {
    return "No logo is set for the event yet. Assigning a logo makes the event more distinguishable.";
  }
  
  public java.lang.String partnerBannerSettings_title() {
    return "Logos";
  }
  
  public java.lang.String eventWall_text_collectingContributions() {
    return "Collecting contributions";
  }
  
  public java.lang.String paymentDialog_text_name() {
    return "Name";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessageDescription() {
    return "Users can publish messages on the wall by their name.";
  }
  
  public java.lang.String videoFilesSettings_title() {
    return "Video setting";
  }
  
  public java.lang.String commonPropertyPanel_property_wallImgSrc() {
    return "Wall image url";
  }
  
  public java.lang.String communitySettings_action_showLinkSource() {
    return "Show link source";
  }
  
  public java.lang.String paymentTransfers_text_fittage() {
    return "Fittage";
  }
  
  public java.lang.String partnerBannerSettings_panelTypeTitle() {
    return "Image under right menu";
  }
  
  public java.lang.String paymentsContent_title() {
    return "Billing information";
  }
  
  public java.lang.String commonPropertyPanel_property_visibleVideos() {
    return "Visible videos";
  }
  
  public java.lang.String partnerBannerSettings_changeFailed() {
    return "Change image failed!";
  }
  
  public java.lang.String communitySettings_action_toHTML() {
    return "Show as HTML";
  }
  
  public java.lang.String baseSettingsContent_form_fullName() {
    return "Full Name";
  }
  
  public java.lang.String videoFilesListItem_text_readyToProcessing() {
    return "READY TO PROCESSING";
  }
  
  public java.lang.String const_currency() {
    return "Currency";
  }
  
  public java.lang.String paymentTransfers_action_showDetail() {
    return "Show detail";
  }
  
  public java.lang.String baseSettingsContent_action_createSeries() {
    return "Create new series";
  }
  
  public java.lang.String videoFilesSettings_text_startProcessing() {
    return "Start processing";
  }
  
  public java.lang.String paymentDialog_text_total() {
    return "Total";
  }
  
  public java.lang.String overseerModule_text_moduleRightRemoveWallMessage() {
    return "Remove messages";
  }
  
  public java.lang.String paymentsProfileContent_form_invoiceNote() {
    return "Invoice note";
  }
  
  public java.lang.String eventSettings_text_eventSuccessfullyCreated() {
    return "The event has been created. Now it is visible only to you as its owner, and to administrators you assign. After setting everything up, you can publish the event and start collecting attendees.";
  }
  
  public java.lang.String const_preparingEventData() {
    return "Preparing event data";
  }
  
  public java.lang.String eventWall_text_participantsRegistration() {
    return "Participants registration";
  }
  
  public java.lang.String committee_action_addMember() {
    return "Add member";
  }
  
  public java.lang.String videoFilesSettings_text_videoName() {
    return "Video name";
  }
  
  public java.lang.String communitySettings_helpTitle() {
    return "Describe and tag the event attentively.";
  }
  
  public java.lang.String committee_action_chairSelect() {
    return "Select chair";
  }
  
  public java.lang.String committee_text_committeeName() {
    return "Name";
  }
  
  public java.lang.String videoFilesSettings_text_uploading() {
    return "Uploading ...";
  }
  
  public java.lang.String paymentsProfileContent_form_organizationName() {
    return "Organization name";
  }
  
  public java.lang.String const_downloadInvoice() {
    return "Download invoice";
  }
  
  public java.lang.String overseerModule_text_roleAdministratorName() {
    return "Event Administrator";
  }
  
  public java.lang.String rightsDialog_button_select() {
    return "Select";
  }
  
  public java.lang.String paymentTransfersDetail_form_sumOfFees() {
    return "Sum of fees";
  }
  
  public java.lang.String videoFilesSettings_state_uploading() {
    return "Uploading";
  }
  
  public java.lang.String communitySettings_text_insertDescription() {
    return "Here you can enter description of the event.";
  }
  
  public java.lang.String overseerModule_text_roleAdministratorDescription() {
    return "Event administrators have full access to the event settings.";
  }
  
  public java.lang.String paymentTransfers_text_total() {
    return "Total";
  }
  
  public java.lang.String partnerBannerSettings_changeSuccess() {
    return "Change image success.";
  }
  
  public java.lang.String baseSettingsContent_form_eventType() {
    return "Event type";
  }
  
  public java.lang.String generalSettings_title() {
    return "General Settings";
  }
  
  public java.lang.String eventCommonPropertyPanel_property_defaultVatRate() {
    return "Default VAT rate";
  }
  
  public java.lang.String committee_button_addCommittee() {
    return "ADD COMMITTEE";
  }
  
  public java.lang.String paymentTransfers_text_transfer() {
    return "Transfer";
  }
  
  public java.lang.String logoSettingsContent_form_logoPicture() {
    return "Event logo";
  }
  
  public java.lang.String paymentTransfersDetail_form_date() {
    return "Date";
  }
  
  public java.lang.String paymentsLicencesContent_text_agree2() {
    return "I agree with the <a href=\"http://static.takeplace.eu/Annex_to_the_Contract_on_Provision_of_Services.pdf\" target=\"_blank\">Annex to the Contract <br/>on Provision of Services</a>";
  }
  
  public java.lang.String videoFilesSettings_action_startProcessing() {
    return "Start processing";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessage() {
    return "Post personal messages";
  }
  
  public java.lang.String baseSettingsContent_form_series() {
    return "Series";
  }
  
  public java.lang.String overseerModule_text_moduleRightTransfers() {
    return "Payment transfer management";
  }
  
  public java.lang.String paymentDialog_text_quantity() {
    return "Quantity";
  }
  
  public java.lang.String paymentTransfers_text_fees() {
    return "Fees";
  }
  
  public java.lang.String overseerModule_text_moduleRightRemoveWallMessageDescription() {
    return "Users can remove messages from the event’s wall.";
  }
  
  public java.lang.String communitySettings_form_registerLink() {
    return "Register link";
  }
  
  public java.lang.String logoSettingsContent_error_updateLogoFailed() {
    return "The logo cannot be updated.";
  }
  
  public java.lang.String committee_action_chairChange() {
    return "Assign chair";
  }
  
  public java.lang.String const_date_from() {
    return "From";
  }
  
  public java.lang.String baseSettingsContent_title() {
    return "Names";
  }
  
  public java.lang.String detailSettingsContent_text_currentEvent() {
    return "Current event";
  }
  
  public java.lang.String paymentTransfers_help_showDetails() {
    return "Show transfer detail";
  }
  
  public java.lang.String communitySettings_text_updateSuccess() {
    return "The description and tags have been updated.";
  }
  
  public java.lang.String paymentTransfers_text_information() {
    return "Information";
  }
  
  public java.lang.String communitySettings_form_registerWidget() {
    return "Register widget";
  }
  
  public java.lang.String baseSettingsContent_form_internationalName() {
    return "International Name";
  }
  
  public java.lang.String commonPropertyPanel_property_footerImgSrc() {
    return "Footer image url";
  }
  
  public java.lang.String paymentTransfersDetail_form_total() {
    return "Total";
  }
  
  public java.lang.String logoSettingsContent_form_logoProfilePicture() {
    return "Profile logo";
  }
  
  public java.lang.String paymentsLicencesContent_helpText() {
    return "You can activate event only after agreeing all terms and conditions.";
  }
  
  public java.lang.String communitySettings_action_showWidgetSource() {
    return "Show widget source";
  }
  
  public java.lang.String paymentTransfers_help_activeTransfer() {
    return "Active transfer";
  }
  
  public java.lang.String paymentsPropertyPanel_error_invalidVat() {
    return "Invalid VAT value, is required decimal number from 0 to 100.";
  }
  
  public java.lang.String videoFilesSettings_text_separateNamesByCommas() {
    return "Separate names by commas, eg: James Smith, Mary Johnson";
  }
  
  public java.lang.String profileLogoSettingsContent_form_logoPicture() {
    return "Profile logo";
  }
  
  public java.lang.String paymentsProfileContent_title() {
    return "Payment profile";
  }
  
  public java.lang.String communitySettings_action_toEditor() {
    return "Show in editor";
  }
  
  public java.lang.String videoFilesSettings_text_maxSize() {
    return "Max. video file size is 500MB";
  }
  
  public java.lang.String communitySettings_helpText2() {
    return "Tags should sum up in a few words the event’s focus, aim or key topics. Proper tagging is important if you wish to make you event searchable within Takeplace.";
  }
  
  public java.lang.String communitySettings_form_tags() {
    return "Tags";
  }
  
  public java.lang.String paymentDialog_button_ok() {
    return "OK";
  }
  
  public java.lang.String rightsDialog_helpText() {
    return "Select a permission from the list; here you will find out what the permission grants users to do.";
  }
  
  public java.lang.String overseerModule_text_payments() {
    return "Payments";
  }
  
  public java.lang.String paymentDialog_text_unitPrice() {
    return "Unit price";
  }
  
  public java.lang.String videoFilesListItem_text_error() {
    return "ERROR";
  }
  
  public java.lang.String eventWall_text_importantDates() {
    return "Important dates";
  }
  
  public java.lang.String committee_text_chairMissing() {
    return "You should start with assigning the committee chair.";
  }
  
  public java.lang.String videoFilesSettings_state_processing() {
    return "Processing";
  }
  
  public java.lang.String videoFilesSettings_text_all() {
    return "All";
  }
  
  public java.lang.String rightsDialog_helpTitle() {
    return "Add permissions";
  }
  
  public java.lang.String const_date_to() {
    return "To";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessageAsEventDescription() {
    return "Users can publish messages on the wall by the name of the event.";
  }
  
  public java.lang.String overseerModule_text_overseerModuleName() {
    return "Common";
  }
  
  public java.lang.String committee_action_addRight() {
    return "Add permissions";
  }
  
  public java.lang.String paymentsProfileContent_helpText1() {
    return "Payment profile is needed when buying license for event. Also payment profile is used on all product invoices in your event and to this profile are transfered money.";
  }
  
  public java.lang.String logoSettingsContent_text_updateLogoSuccess() {
    return "The logo has been updated.";
  }
  
  public java.lang.String videoFilesSettings_text_speakers() {
    return "Speakers";
  }
  
  public java.lang.String paymentTransfers_help_transferedTransfer() {
    return "Transfered transfer";
  }
  
  public java.lang.String overseerModule_text_administration() {
    return "Administration";
  }
  
  public java.lang.String detailSettingsContent_title() {
    return "When and where";
  }
  
  public java.lang.String baseSettingsContent_form_shortName() {
    return "Short Name";
  }
  
  public java.lang.String const_error_readOnlyProperty() {
    return "Some items can not be changed.";
  }
  
  public java.lang.String paymentTransfersDetail_form_account() {
    return "Account number";
  }
  
  public java.lang.String videos_title() {
    return "Videos";
  }
  
  public java.lang.String overseerModule_text_video() {
    return "Video";
  }
  
  public java.lang.String committee_form_committeeRights() {
    return "Rights";
  }
  
  public java.lang.String communitySettings_helpText1() {
    return "The description helps people to identify what exactly is the event about. You should provide a brief yet accurate summary of what can prospective attendees and participants await from the event.";
  }
  
  public java.lang.String communitySettings_form_profileWebPage() {
    return "Profile web page";
  }
  
  public java.lang.String overseerModule_text_moduleRightTransfersDescription() {
    return "Users with this role are authorized to make transfer requests, they can download participants invoices and see cash flow.";
  }
  
  public java.lang.String eventSettings_title() {
    return "Event Settings";
  }
  
  public java.lang.String logoSettingsContent_action_changeLogo() {
    return "Change logo";
  }
  
  public java.lang.String paymentsLicencesContent_text_agree3() {
    return "I agree with the <a href=\"http://static.takeplace.eu/Agreement_of_Confidentiality_v5_clear_en.pdf\" target=\"_blank\">Agreement of Confidentiality</a>";
  }
}
