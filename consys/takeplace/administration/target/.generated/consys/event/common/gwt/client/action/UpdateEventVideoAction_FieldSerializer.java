package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateEventVideoAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAuthors(consys.event.common.gwt.client.action.UpdateEventVideoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::authors;
  }-*/;
  
  private static native void setAuthors(consys.event.common.gwt.client.action.UpdateEventVideoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::authors = value;
  }-*/;
  
  private static native java.lang.String getDescription(consys.event.common.gwt.client.action.UpdateEventVideoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::description;
  }-*/;
  
  private static native void setDescription(consys.event.common.gwt.client.action.UpdateEventVideoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::description = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.common.gwt.client.action.UpdateEventVideoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::name;
  }-*/;
  
  private static native void setName(consys.event.common.gwt.client.action.UpdateEventVideoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::name = value;
  }-*/;
  
  private static native java.lang.String getSpeakers(consys.event.common.gwt.client.action.UpdateEventVideoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::speakers;
  }-*/;
  
  private static native void setSpeakers(consys.event.common.gwt.client.action.UpdateEventVideoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::speakers = value;
  }-*/;
  
  private static native java.lang.String getSystemName(consys.event.common.gwt.client.action.UpdateEventVideoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::systemName;
  }-*/;
  
  private static native void setSystemName(consys.event.common.gwt.client.action.UpdateEventVideoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventVideoAction::systemName = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateEventVideoAction instance) throws SerializationException {
    setAuthors(instance, streamReader.readString());
    setDescription(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    setSpeakers(instance, streamReader.readString());
    setSystemName(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.UpdateEventVideoAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateEventVideoAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateEventVideoAction instance) throws SerializationException {
    streamWriter.writeString(getAuthors(instance));
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getSpeakers(instance));
    streamWriter.writeString(getSystemName(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateEventVideoAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventVideoAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateEventVideoAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventVideoAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateEventVideoAction)object);
  }
  
}
