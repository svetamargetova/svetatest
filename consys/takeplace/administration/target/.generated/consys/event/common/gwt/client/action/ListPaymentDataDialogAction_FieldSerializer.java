package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListPaymentDataDialogAction_FieldSerializer {
  private static native java.lang.String getCustomerProfileUuid(consys.event.common.gwt.client.action.ListPaymentDataDialogAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.ListPaymentDataDialogAction::customerProfileUuid;
  }-*/;
  
  private static native void setCustomerProfileUuid(consys.event.common.gwt.client.action.ListPaymentDataDialogAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.ListPaymentDataDialogAction::customerProfileUuid = value;
  }-*/;
  
  private static native java.util.ArrayList getProducts(consys.event.common.gwt.client.action.ListPaymentDataDialogAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.ListPaymentDataDialogAction::products;
  }-*/;
  
  private static native void setProducts(consys.event.common.gwt.client.action.ListPaymentDataDialogAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.ListPaymentDataDialogAction::products = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.ListPaymentDataDialogAction instance) throws SerializationException {
    setCustomerProfileUuid(instance, streamReader.readString());
    setProducts(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.ListPaymentDataDialogAction instance) throws SerializationException {
    streamWriter.writeString(getCustomerProfileUuid(instance));
    streamWriter.writeObject(getProducts(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
}
