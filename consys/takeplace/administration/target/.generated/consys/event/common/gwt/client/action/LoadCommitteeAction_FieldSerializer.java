package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadCommitteeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(consys.event.common.gwt.client.action.LoadCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.LoadCommitteeAction::id;
  }-*/;
  
  private static native void setId(consys.event.common.gwt.client.action.LoadCommitteeAction instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.LoadCommitteeAction::id = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.LoadCommitteeAction instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.LoadCommitteeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.LoadCommitteeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.LoadCommitteeAction instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.LoadCommitteeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadCommitteeAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.LoadCommitteeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadCommitteeAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.LoadCommitteeAction)object);
  }
  
}
