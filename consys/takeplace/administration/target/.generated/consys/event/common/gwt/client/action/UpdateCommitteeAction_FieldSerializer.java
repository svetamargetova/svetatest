package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateCommitteeAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.InvitedUser getChair(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::chair;
  }-*/;
  
  private static native void setChair(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, consys.common.gwt.shared.bo.InvitedUser value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::chair = value;
  }-*/;
  
  private static native java.lang.String getCommitteeName(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::committeeName;
  }-*/;
  
  private static native void setCommitteeName(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::committeeName = value;
  }-*/;
  
  private static native java.lang.Long getId(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::id;
  }-*/;
  
  private static native void setId(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::id = value;
  }-*/;
  
  private static native java.util.ArrayList getNewChairRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::newChairRights;
  }-*/;
  
  private static native void setNewChairRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::newChairRights = value;
  }-*/;
  
  private static native java.util.ArrayList getNewMembers(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::newMembers;
  }-*/;
  
  private static native void setNewMembers(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::newMembers = value;
  }-*/;
  
  private static native java.util.ArrayList getNewMembersRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::newMembersRights;
  }-*/;
  
  private static native void setNewMembersRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::newMembersRights = value;
  }-*/;
  
  private static native java.util.ArrayList getRemoveChairRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::removeChairRights;
  }-*/;
  
  private static native void setRemoveChairRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::removeChairRights = value;
  }-*/;
  
  private static native java.util.ArrayList getRemoveMembers(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::removeMembers;
  }-*/;
  
  private static native void setRemoveMembers(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::removeMembers = value;
  }-*/;
  
  private static native java.util.ArrayList getRemoveMembersRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::removeMembersRights;
  }-*/;
  
  private static native void setRemoveMembersRights(consys.event.common.gwt.client.action.UpdateCommitteeAction instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateCommitteeAction::removeMembersRights = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateCommitteeAction instance) throws SerializationException {
    setChair(instance, (consys.common.gwt.shared.bo.InvitedUser) streamReader.readObject());
    setCommitteeName(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setNewChairRights(instance, (java.util.ArrayList) streamReader.readObject());
    setNewMembers(instance, (java.util.ArrayList) streamReader.readObject());
    setNewMembersRights(instance, (java.util.ArrayList) streamReader.readObject());
    setRemoveChairRights(instance, (java.util.ArrayList) streamReader.readObject());
    setRemoveMembers(instance, (java.util.ArrayList) streamReader.readObject());
    setRemoveMembersRights(instance, (java.util.ArrayList) streamReader.readObject());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.UpdateCommitteeAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateCommitteeAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateCommitteeAction instance) throws SerializationException {
    streamWriter.writeObject(getChair(instance));
    streamWriter.writeString(getCommitteeName(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getNewChairRights(instance));
    streamWriter.writeObject(getNewMembers(instance));
    streamWriter.writeObject(getNewMembersRights(instance));
    streamWriter.writeObject(getRemoveChairRights(instance));
    streamWriter.writeObject(getRemoveMembers(instance));
    streamWriter.writeObject(getRemoveMembersRights(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateCommitteeAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateCommitteeAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateCommitteeAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateCommitteeAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateCommitteeAction)object);
  }
  
}
