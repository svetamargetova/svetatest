package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPaymentTransferDetail_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.event.common.gwt.client.bo.ClientPaymentTransferHead getHeader(consys.event.common.gwt.client.bo.ClientPaymentTransferDetail instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferDetail::header;
  }-*/;
  
  private static native void setHeader(consys.event.common.gwt.client.bo.ClientPaymentTransferDetail instance, consys.event.common.gwt.client.bo.ClientPaymentTransferHead value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferDetail::header = value;
  }-*/;
  
  private static native java.util.ArrayList getItems(consys.event.common.gwt.client.bo.ClientPaymentTransferDetail instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferDetail::items;
  }-*/;
  
  private static native void setItems(consys.event.common.gwt.client.bo.ClientPaymentTransferDetail instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentTransferDetail::items = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientPaymentTransferDetail instance) throws SerializationException {
    setHeader(instance, (consys.event.common.gwt.client.bo.ClientPaymentTransferHead) streamReader.readObject());
    setItems(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientPaymentTransferDetail instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientPaymentTransferDetail();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientPaymentTransferDetail instance) throws SerializationException {
    streamWriter.writeObject(getHeader(instance));
    streamWriter.writeObject(getItems(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientPaymentTransferDetail_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentTransferDetail_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientPaymentTransferDetail)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentTransferDetail_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientPaymentTransferDetail)object);
  }
  
}
