package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientEventConfirmed_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getFrom(consys.event.common.gwt.client.bo.ClientEventConfirmed instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventConfirmed::from;
  }-*/;
  
  private static native void setFrom(consys.event.common.gwt.client.bo.ClientEventConfirmed instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventConfirmed::from = value;
  }-*/;
  
  private static native java.util.Date getTo(consys.event.common.gwt.client.bo.ClientEventConfirmed instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientEventConfirmed::to;
  }-*/;
  
  private static native void setTo(consys.event.common.gwt.client.bo.ClientEventConfirmed instance, java.util.Date value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientEventConfirmed::to = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientEventConfirmed instance) throws SerializationException {
    setFrom(instance, (java.util.Date) streamReader.readObject());
    setTo(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientEventConfirmed instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientEventConfirmed();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientEventConfirmed instance) throws SerializationException {
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeObject(getTo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientEventConfirmed_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientEventConfirmed_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientEventConfirmed)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientEventConfirmed_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientEventConfirmed)object);
  }
  
}
