package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCommitteeThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.Long getId(consys.event.common.gwt.client.bo.ClientCommitteeThumb instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommitteeThumb::id;
  }-*/;
  
  private static native void setId(consys.event.common.gwt.client.bo.ClientCommitteeThumb instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommitteeThumb::id = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.common.gwt.client.bo.ClientCommitteeThumb instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommitteeThumb::name;
  }-*/;
  
  private static native void setName(consys.event.common.gwt.client.bo.ClientCommitteeThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommitteeThumb::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientCommitteeThumb instance) throws SerializationException {
    setId(instance, (java.lang.Long) streamReader.readObject());
    setName(instance, streamReader.readString());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientCommitteeThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientCommitteeThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientCommitteeThumb instance) throws SerializationException {
    streamWriter.writeObject(getId(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientCommitteeThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientCommitteeThumb_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientCommitteeThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientCommitteeThumb_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientCommitteeThumb)object);
  }
  
}
