package consys.event.common.gwt.client.message;

public class EventCoreMessages_cs implements consys.event.common.gwt.client.message.EventCoreMessages {
  
  public java.lang.String baseSettingsContent_text_infoCloned(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Nová série <a href=\"").append(arg0).append("\">akce</a> byla úspěšně vytvořena.").toString();
  }
  
  public java.lang.String paymentsLicencesContent_text_confirmed(java.lang.String arg0,java.lang.String arg1,java.lang.String arg2) {
    return new java.lang.StringBuffer().append("Blahopřejeme Vám k úspěšnému aktivování akce. ").append(arg0).append(" je nyní aktivní od ").append(arg1).append(" do ").append(arg2).append(". Pokud se vyskytnou problémy, kontaktujte nás na adrese support@takeplace.eu.").toString();
  }
  
  public java.lang.String partnerBannerSettings_TypeNote(int arg0) {
    return new java.lang.StringBuffer().append("K dosáhnutí nejlepší kvality, dodejte prosím obrázek se šířkou ").append(arg0).append(" pixelů. Pokud bude dodaný obrázek širší než ").append(arg0).append(" pixelů tak ho automaticky přizpůsobíme.").toString();
  }
}
