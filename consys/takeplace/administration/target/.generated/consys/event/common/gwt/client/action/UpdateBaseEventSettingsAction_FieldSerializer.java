package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateBaseEventSettingsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAcronym(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::acronym;
  }-*/;
  
  private static native void setAcronym(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::acronym = value;
  }-*/;
  
  private static native java.lang.String getEventUuid(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::eventUuid = value;
  }-*/;
  
  private static native java.lang.String getFullName(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::fullName;
  }-*/;
  
  private static native void setFullName(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::fullName = value;
  }-*/;
  
  private static native java.lang.String getSeries(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::series;
  }-*/;
  
  private static native void setSeries(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::series = value;
  }-*/;
  
  private static native java.lang.String getShortName(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::shortName;
  }-*/;
  
  private static native void setShortName(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::shortName = value;
  }-*/;
  
  private static native java.lang.String getUniversalName(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::universalName;
  }-*/;
  
  private static native void setUniversalName(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::universalName = value;
  }-*/;
  
  private static native int getYear(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::year;
  }-*/;
  
  private static native void setYear(consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance, int value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction::year = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) throws SerializationException {
    setAcronym(instance, streamReader.readString());
    setEventUuid(instance, streamReader.readString());
    setFullName(instance, streamReader.readString());
    setSeries(instance, streamReader.readString());
    setShortName(instance, streamReader.readString());
    setUniversalName(instance, streamReader.readString());
    setYear(instance, streamReader.readInt());
    
  }
  
  public static consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction instance) throws SerializationException {
    streamWriter.writeString(getAcronym(instance));
    streamWriter.writeString(getEventUuid(instance));
    streamWriter.writeString(getFullName(instance));
    streamWriter.writeString(getSeries(instance));
    streamWriter.writeString(getShortName(instance));
    streamWriter.writeString(getUniversalName(instance));
    streamWriter.writeInt(getYear(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction)object);
  }
  
}
