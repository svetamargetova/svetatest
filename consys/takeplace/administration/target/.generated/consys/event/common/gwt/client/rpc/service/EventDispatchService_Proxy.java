package consys.event.common.gwt.client.rpc.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class EventDispatchService_Proxy extends RemoteServiceProxy implements consys.event.common.gwt.client.rpc.service.EventDispatchServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "consys.event.common.gwt.client.rpc.service.EventDispatchService";
  private static final String SERIALIZATION_POLICY ="CC4B45EDDB170B997E3266A7E1501F43";
  private static final consys.event.common.gwt.client.rpc.service.EventDispatchService_TypeSerializer SERIALIZER = new consys.event.common.gwt.client.rpc.service.EventDispatchService_TypeSerializer();
  
  public EventDispatchService_Proxy() {
    super(GWT.getModuleBaseURL(),
      null, 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void execute(consys.common.gwt.shared.action.EventAction action, com.google.gwt.user.client.rpc.AsyncCallback callback) {
    RpcStatsContext statsContext = new RpcStatsContext();
    boolean toss = statsContext.isStatsAvailable() && statsContext.stats(statsContext.timeStat("EventDispatchService_Proxy.execute", "begin"));
    SerializationStreamWriter streamWriter = createStreamWriter();
    // createStreamWriter() prepared the stream
    try {
      if (getRpcToken() != null) {
        streamWriter.writeObject(getRpcToken());
      }
      streamWriter.writeString(REMOTE_SERVICE_INTERFACE_NAME);
      streamWriter.writeString("execute");
      streamWriter.writeInt(1);
      streamWriter.writeString("consys.common.gwt.shared.action.EventAction/2838931917");
      streamWriter.writeObject(action);
      String payload = streamWriter.toString();
      toss = statsContext.isStatsAvailable() && statsContext.stats(statsContext.timeStat("EventDispatchService_Proxy.execute",  "requestSerialized"));
      doInvoke(ResponseReader.OBJECT, "EventDispatchService_Proxy.execute", statsContext, payload, callback);
    } catch (SerializationException ex) {
      callback.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
