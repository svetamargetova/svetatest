package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListUserPaymentProfileDialogAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getManagedUserUuid(consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction::managedUserUuid;
  }-*/;
  
  private static native void setManagedUserUuid(consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction::managedUserUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction instance) throws SerializationException {
    setManagedUserUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction instance) throws SerializationException {
    streamWriter.writeString(getManagedUserUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction)object);
  }
  
}
