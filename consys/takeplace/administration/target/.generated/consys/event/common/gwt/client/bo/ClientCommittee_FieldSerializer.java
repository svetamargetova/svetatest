package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientCommittee_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.EventUser getChair(consys.event.common.gwt.client.bo.ClientCommittee instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommittee::chair;
  }-*/;
  
  private static native void setChair(consys.event.common.gwt.client.bo.ClientCommittee instance, consys.common.gwt.shared.bo.EventUser value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommittee::chair = value;
  }-*/;
  
  private static native java.util.ArrayList getChairRights(consys.event.common.gwt.client.bo.ClientCommittee instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommittee::chairRights;
  }-*/;
  
  private static native void setChairRights(consys.event.common.gwt.client.bo.ClientCommittee instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommittee::chairRights = value;
  }-*/;
  
  private static native java.lang.Long getId(consys.event.common.gwt.client.bo.ClientCommittee instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommittee::id;
  }-*/;
  
  private static native void setId(consys.event.common.gwt.client.bo.ClientCommittee instance, java.lang.Long value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommittee::id = value;
  }-*/;
  
  private static native java.util.ArrayList getMemebers(consys.event.common.gwt.client.bo.ClientCommittee instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommittee::memebers;
  }-*/;
  
  private static native void setMemebers(consys.event.common.gwt.client.bo.ClientCommittee instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommittee::memebers = value;
  }-*/;
  
  private static native java.lang.String getName(consys.event.common.gwt.client.bo.ClientCommittee instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommittee::name;
  }-*/;
  
  private static native void setName(consys.event.common.gwt.client.bo.ClientCommittee instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommittee::name = value;
  }-*/;
  
  private static native java.util.ArrayList getRights(consys.event.common.gwt.client.bo.ClientCommittee instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientCommittee::rights;
  }-*/;
  
  private static native void setRights(consys.event.common.gwt.client.bo.ClientCommittee instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientCommittee::rights = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientCommittee instance) throws SerializationException {
    setChair(instance, (consys.common.gwt.shared.bo.EventUser) streamReader.readObject());
    setChairRights(instance, (java.util.ArrayList) streamReader.readObject());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setMemebers(instance, (java.util.ArrayList) streamReader.readObject());
    setName(instance, streamReader.readString());
    setRights(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientCommittee instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientCommittee();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientCommittee instance) throws SerializationException {
    streamWriter.writeObject(getChair(instance));
    streamWriter.writeObject(getChairRights(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeObject(getMemebers(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeObject(getRights(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientCommittee_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientCommittee_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientCommittee)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientCommittee_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientCommittee)object);
  }
  
}
