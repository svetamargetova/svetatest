package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadOuterEventInfoAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEventUuid(consys.event.common.gwt.client.action.LoadOuterEventInfoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.LoadOuterEventInfoAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.event.common.gwt.client.action.LoadOuterEventInfoAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.LoadOuterEventInfoAction::eventUuid = value;
  }-*/;
  
  private static native boolean getFillDescription(consys.event.common.gwt.client.action.LoadOuterEventInfoAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.LoadOuterEventInfoAction::fillDescription;
  }-*/;
  
  private static native void setFillDescription(consys.event.common.gwt.client.action.LoadOuterEventInfoAction instance, boolean value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.LoadOuterEventInfoAction::fillDescription = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.LoadOuterEventInfoAction instance) throws SerializationException {
    setEventUuid(instance, streamReader.readString());
    setFillDescription(instance, streamReader.readBoolean());
    
  }
  
  public static consys.event.common.gwt.client.action.LoadOuterEventInfoAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.LoadOuterEventInfoAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.LoadOuterEventInfoAction instance) throws SerializationException {
    streamWriter.writeString(getEventUuid(instance));
    streamWriter.writeBoolean(getFillDescription(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.LoadOuterEventInfoAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadOuterEventInfoAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.LoadOuterEventInfoAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.LoadOuterEventInfoAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.LoadOuterEventInfoAction)object);
  }
  
}
