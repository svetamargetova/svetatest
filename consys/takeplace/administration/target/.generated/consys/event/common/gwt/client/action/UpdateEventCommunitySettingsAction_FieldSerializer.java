package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateEventCommunitySettingsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getDescription(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::description;
  }-*/;
  
  private static native void setDescription(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::description = value;
  }-*/;
  
  private static native java.lang.String getEventUuid(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::eventUuid = value;
  }-*/;
  
  private static native java.lang.String getTags(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::tags;
  }-*/;
  
  private static native void setTags(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::tags = value;
  }-*/;
  
  private static native java.lang.String getWeb(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::web;
  }-*/;
  
  private static native void setWeb(consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction::web = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance) throws SerializationException {
    setDescription(instance, streamReader.readString());
    setEventUuid(instance, streamReader.readString());
    setTags(instance, streamReader.readString());
    setWeb(instance, streamReader.readString());
    
  }
  
  public static consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction instance) throws SerializationException {
    streamWriter.writeString(getDescription(instance));
    streamWriter.writeString(getEventUuid(instance));
    streamWriter.writeString(getTags(instance));
    streamWriter.writeString(getWeb(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction)object);
  }
  
}
