package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CurrentEventUserRightAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getRight(consys.event.common.gwt.client.action.CurrentEventUserRightAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.CurrentEventUserRightAction::right;
  }-*/;
  
  private static native void setRight(consys.event.common.gwt.client.action.CurrentEventUserRightAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.CurrentEventUserRightAction::right = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.CurrentEventUserRightAction instance) throws SerializationException {
    setRight(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.CurrentEventUserRightAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.CurrentEventUserRightAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.CurrentEventUserRightAction instance) throws SerializationException {
    streamWriter.writeString(getRight(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.CurrentEventUserRightAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.CurrentEventUserRightAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.CurrentEventUserRightAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.CurrentEventUserRightAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.CurrentEventUserRightAction)object);
  }
  
}
