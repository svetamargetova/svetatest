package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientPaymentSettings_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientEventPaymentProfile getFirstProfile(consys.event.common.gwt.client.bo.ClientPaymentSettings instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentSettings::firstProfile;
  }-*/;
  
  private static native void setFirstProfile(consys.event.common.gwt.client.bo.ClientPaymentSettings instance, consys.common.gwt.shared.bo.ClientEventPaymentProfile value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentSettings::firstProfile = value;
  }-*/;
  
  private static native java.util.ArrayList getProfileThumbs(consys.event.common.gwt.client.bo.ClientPaymentSettings instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientPaymentSettings::profileThumbs;
  }-*/;
  
  private static native void setProfileThumbs(consys.event.common.gwt.client.bo.ClientPaymentSettings instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientPaymentSettings::profileThumbs = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientPaymentSettings instance) throws SerializationException {
    setFirstProfile(instance, (consys.common.gwt.shared.bo.ClientEventPaymentProfile) streamReader.readObject());
    setProfileThumbs(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.event.common.gwt.client.bo.ClientPaymentSettings instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientPaymentSettings();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientPaymentSettings instance) throws SerializationException {
    streamWriter.writeObject(getFirstProfile(instance));
    streamWriter.writeObject(getProfileThumbs(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientPaymentSettings_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentSettings_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientPaymentSettings)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientPaymentSettings_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientPaymentSettings)object);
  }
  
}
