package consys.event.common.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateUserEventAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getName(consys.event.common.gwt.client.action.UpdateUserEventAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::name;
  }-*/;
  
  private static native void setName(consys.event.common.gwt.client.action.UpdateUserEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::name = value;
  }-*/;
  
  private static native java.lang.String getOrganization(consys.event.common.gwt.client.action.UpdateUserEventAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::organization;
  }-*/;
  
  private static native void setOrganization(consys.event.common.gwt.client.action.UpdateUserEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::organization = value;
  }-*/;
  
  private static native java.lang.String getPosition(consys.event.common.gwt.client.action.UpdateUserEventAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::position;
  }-*/;
  
  private static native void setPosition(consys.event.common.gwt.client.action.UpdateUserEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::position = value;
  }-*/;
  
  private static native java.lang.String getUserUuid(consys.event.common.gwt.client.action.UpdateUserEventAction instance) /*-{
    return instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::userUuid;
  }-*/;
  
  private static native void setUserUuid(consys.event.common.gwt.client.action.UpdateUserEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.event.common.gwt.client.action.UpdateUserEventAction::userUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.action.UpdateUserEventAction instance) throws SerializationException {
    setName(instance, streamReader.readString());
    setOrganization(instance, streamReader.readString());
    setPosition(instance, streamReader.readString());
    setUserUuid(instance, streamReader.readString());
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.action.UpdateUserEventAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.action.UpdateUserEventAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.action.UpdateUserEventAction instance) throws SerializationException {
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getOrganization(instance));
    streamWriter.writeString(getPosition(instance));
    streamWriter.writeString(getUserUuid(instance));
    
    consys.common.gwt.shared.action.EventAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.action.UpdateUserEventAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateUserEventAction_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.action.UpdateUserEventAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.action.UpdateUserEventAction_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.action.UpdateUserEventAction)object);
  }
  
}
