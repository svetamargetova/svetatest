package consys.event.common.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientOuterEventData_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getActive(consys.event.common.gwt.client.bo.ClientOuterEventData instance) /*-{
    return instance.@consys.event.common.gwt.client.bo.ClientOuterEventData::active;
  }-*/;
  
  private static native void setActive(consys.event.common.gwt.client.bo.ClientOuterEventData instance, boolean value) 
  /*-{
    instance.@consys.event.common.gwt.client.bo.ClientOuterEventData::active = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.event.common.gwt.client.bo.ClientOuterEventData instance) throws SerializationException {
    setActive(instance, streamReader.readBoolean());
    
    consys.common.gwt.shared.bo.ClientEventThumb_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.event.common.gwt.client.bo.ClientOuterEventData instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.event.common.gwt.client.bo.ClientOuterEventData();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.event.common.gwt.client.bo.ClientOuterEventData instance) throws SerializationException {
    streamWriter.writeBoolean(getActive(instance));
    
    consys.common.gwt.shared.bo.ClientEventThumb_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.event.common.gwt.client.bo.ClientOuterEventData_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientOuterEventData_FieldSerializer.deserialize(reader, (consys.event.common.gwt.client.bo.ClientOuterEventData)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.event.common.gwt.client.bo.ClientOuterEventData_FieldSerializer.serialize(writer, (consys.event.common.gwt.client.bo.ClientOuterEventData)object);
  }
  
}
