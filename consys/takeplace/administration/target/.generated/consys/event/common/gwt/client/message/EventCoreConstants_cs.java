package consys.event.common.gwt.client.message;

public class EventCoreConstants_cs implements consys.event.common.gwt.client.message.EventCoreConstants {
  
  public java.lang.String paymentTransfers_help_preparedTransfer() {
    return "Transfer připraven";
  }
  
  public java.lang.String const_error_currencyNotFoundInCache() {
    return "Nepodařilo se načíst měnu";
  }
  
  public java.lang.String paymentTransfersDetail_form_sum() {
    return "Suma";
  }
  
  public java.lang.String videoFilesSettings_state_processed() {
    return "Zpracované";
  }
  
  public java.lang.String logoSettingsContent_text_sizeInfo() {
    return "Maximální velikost loga akce je 300x40";
  }
  
  public java.lang.String const_downloadProFormaInvoice() {
    return "Stáhnout proforma fakturu";
  }
  
  public java.lang.String paymentsLicencesContent_form_agreements() {
    return "Smlouvy";
  }
  
  public java.lang.String videoFilesSettings_state_error() {
    return "Chyba";
  }
  
  public java.lang.String videoFilesSettings_action_addVideo() {
    return "Přidat video";
  }
  
  public java.lang.String partnerBannerSettings_changeImage() {
    return "Změnit obrázek";
  }
  
  public java.lang.String partnerBannerSettings_footerTypeTitle() {
    return "Obrázek v patičce";
  }
  
  public java.lang.String const_invoice() {
    return "Faktura";
  }
  
  public java.lang.String paymentsLicencesContent_text_agree1() {
    return "Souhlasím s <a href=\"http://static.takeplace.eu/Contract_on_Provision_of_Services_9_clear_en.pdf\" target=\"_blank\">Smlouva o poskytování služeb</a>";
  }
  
  public java.lang.String paymentsProfileContent_helpText2() {
    return "Buďte si vědomi, že bez licence nemůžete akci aktivovat, ani nemůžete být zobrazeni ve veřejném přehledu akcí.";
  }
  
  public java.lang.String committee_form_committeeChair() {
    return "Předseda";
  }
  
  public java.lang.String eventSettings_text_updateSuccess() {
    return "Nastavení akce bylo aktualizováno.";
  }
  
  public java.lang.String paymentProfileContent_form_noVatPayer() {
    return "Neplátce DPH";
  }
  
  public java.lang.String paymentTransfers_text_date() {
    return "Datum";
  }
  
  public java.lang.String committee_form_committeeMembers() {
    return "Členové";
  }
  
  public java.lang.String detailSettingsContent_error_eventMustStartInSameYear() {
    return "Vložený počáteční rok v datumu konání vašeho eventu je odlišný od toho, který jste zadali při založení eventu. Pokud chcete ponechat zvolené datum, změňte prosím rok akce.";
  }
  
  public java.lang.String profileLogoSettingsContent_text_sizeInfo() {
    return "Maximální velikost loga profilu je 90x90";
  }
  
  public java.lang.String videoFilesSettings_text_authors() {
    return "Autoři";
  }
  
  public java.lang.String paymentsProfileContent_helpText3() {
    return "DOPLNIT NÁPOVĚDU PRO Invoice note - je to text, který se zobrazuje na faktuře. Pokud na ní nejsou potřeba dodatečné informace tak nechat prázdné.";
  }
  
  public java.lang.String paymentTransfers_title() {
    return "Platební převody";
  }
  
  public java.lang.String communitySettings_title() {
    return "Detaily akce / web";
  }
  
  public java.lang.String eventWall_button_sendContribution() {
    return "ZASLAT PŘÍSPĚVEK";
  }
  
  public java.lang.String videoFilesSettings_error_unsupportedBrowser() {
    return "Nebyl vybrán soubor nebo Váš prohlížeč nepodporuje zjišťování velikosti nahrávaného souboru.";
  }
  
  public java.lang.String const_date() {
    return "Datum";
  }
  
  public java.lang.String eventWall_button_register() {
    return "ZAREGISTROVAT SE";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessageAsEvent() {
    return "Oficiální zprávy na zeď";
  }
  
  public java.lang.String paymentsLicencesContent_title() {
    return "Smlouvy";
  }
  
  public java.lang.String eventCommittees_title() {
    return "Výbory";
  }
  
  public java.lang.String videoFilesListItem_text_processing() {
    return "ZPRACOVÁVÁ SE";
  }
  
  public java.lang.String paymentsLicencesContent_button_activateEvent() {
    return "AKTIVOVAT AKCI";
  }
  
  public java.lang.String paymentTransfers_help_errorTransfer() {
    return "Chyba transferu";
  }
  
  public java.lang.String logoSettingsContent_text_logoMissing() {
    return "Zatím nebylo nastaveno logo akce. Přiřazením loga uděláte akci lépe rozeznatelnou.";
  }
  
  public java.lang.String partnerBannerSettings_title() {
    return "Loga";
  }
  
  public java.lang.String eventWall_text_collectingContributions() {
    return "Sběr příspěvků";
  }
  
  public java.lang.String paymentDialog_text_name() {
    return "Název";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessageDescription() {
    return "Uživatelé mohou vkládat zprávy na zeď akce vlastním jménem.";
  }
  
  public java.lang.String videoFilesSettings_title() {
    return "Nastavení videí";
  }
  
  public java.lang.String commonPropertyPanel_property_wallImgSrc() {
    return "Url obrázku na zdi";
  }
  
  public java.lang.String communitySettings_action_showLinkSource() {
    return "Zobrazit kód odkazu";
  }
  
  public java.lang.String paymentTransfers_text_fittage() {
    return "Provize";
  }
  
  public java.lang.String partnerBannerSettings_panelTypeTitle() {
    return "Obrázek pod pravým menu";
  }
  
  public java.lang.String paymentsContent_title() {
    return "Fakturační údaje";
  }
  
  public java.lang.String commonPropertyPanel_property_visibleVideos() {
    return "Viditelná videa";
  }
  
  public java.lang.String partnerBannerSettings_changeFailed() {
    return "Změna obrázku selhala1";
  }
  
  public java.lang.String communitySettings_action_toHTML() {
    return "Zobrazit jako HTML";
  }
  
  public java.lang.String baseSettingsContent_form_fullName() {
    return "Plný název";
  }
  
  public java.lang.String videoFilesListItem_text_readyToProcessing() {
    return "PŘIPRAVENO KE ZPRACOVÁNÍ";
  }
  
  public java.lang.String const_currency() {
    return "Měna";
  }
  
  public java.lang.String paymentTransfers_action_showDetail() {
    return "Show detail";
  }
  
  public java.lang.String baseSettingsContent_action_createSeries() {
    return "Vytvořit novou sérii";
  }
  
  public java.lang.String videoFilesSettings_text_startProcessing() {
    return "Spustit zpracování";
  }
  
  public java.lang.String paymentDialog_text_total() {
    return "Celkem";
  }
  
  public java.lang.String overseerModule_text_moduleRightRemoveWallMessage() {
    return "Mazání ze zdi";
  }
  
  public java.lang.String paymentsProfileContent_form_invoiceNote() {
    return "Fakturační poznámka";
  }
  
  public java.lang.String eventSettings_text_eventSuccessfullyCreated() {
    return "Akce byla vytvořena. Nyní je viditelná pouze pro vás jako vlastníka a administrátory, které přiřadíte. Jakmile vše nastavíte, můžete zveřejnit akci a začít nabírat účastníky.";
  }
  
  public java.lang.String const_preparingEventData() {
    return "Připravují se data akce";
  }
  
  public java.lang.String eventWall_text_participantsRegistration() {
    return "Registrace účastníků";
  }
  
  public java.lang.String committee_action_addMember() {
    return "Přidat člena";
  }
  
  public java.lang.String videoFilesSettings_text_videoName() {
    return "Název videa";
  }
  
  public java.lang.String communitySettings_helpTitle() {
    return "Pečlivě popište a označkujte akci.";
  }
  
  public java.lang.String committee_action_chairSelect() {
    return "Vybrat předsedu";
  }
  
  public java.lang.String committee_text_committeeName() {
    return "Název";
  }
  
  public java.lang.String videoFilesSettings_text_uploading() {
    return "Nahrává se ...";
  }
  
  public java.lang.String paymentsProfileContent_form_organizationName() {
    return "Název organizace";
  }
  
  public java.lang.String const_downloadInvoice() {
    return "Stáhnout fakturu";
  }
  
  public java.lang.String overseerModule_text_roleAdministratorName() {
    return "Administrátor akce";
  }
  
  public java.lang.String rightsDialog_button_select() {
    return "Vybrat";
  }
  
  public java.lang.String paymentTransfersDetail_form_sumOfFees() {
    return "Suma poplatků";
  }
  
  public java.lang.String videoFilesSettings_state_uploading() {
    return "Uploading";
  }
  
  public java.lang.String communitySettings_text_insertDescription() {
    return "Zde můžete vložit popis akce.";
  }
  
  public java.lang.String overseerModule_text_roleAdministratorDescription() {
    return "Administrátor má plný přístup k nastavením akce.";
  }
  
  public java.lang.String paymentTransfers_text_total() {
    return "Celkem";
  }
  
  public java.lang.String partnerBannerSettings_changeSuccess() {
    return "Změna obrázku proběhla úspěšne.";
  }
  
  public java.lang.String baseSettingsContent_form_eventType() {
    return "Typ akce";
  }
  
  public java.lang.String generalSettings_title() {
    return "Hlavní nastavení";
  }
  
  public java.lang.String eventCommonPropertyPanel_property_defaultVatRate() {
    return "Výchozí hodnota daně";
  }
  
  public java.lang.String committee_button_addCommittee() {
    return "PŘIDAT VÝBOR";
  }
  
  public java.lang.String paymentTransfers_text_transfer() {
    return "Převod";
  }
  
  public java.lang.String logoSettingsContent_form_logoPicture() {
    return "Logo akce";
  }
  
  public java.lang.String paymentTransfersDetail_form_date() {
    return "Datum";
  }
  
  public java.lang.String paymentsLicencesContent_text_agree2() {
    return "Souhlasím s <a href=\"http://static.takeplace.eu/Annex_to_the_Contract_on_Provision_of_Services.pdf\" target=\"_blank\">Příloha ke smlouvě <br/>o poskytování služeb</a>";
  }
  
  public java.lang.String videoFilesSettings_action_startProcessing() {
    return "Spustit zpracování";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessage() {
    return "Osobní zprávy na zeď";
  }
  
  public java.lang.String baseSettingsContent_form_series() {
    return "Série";
  }
  
  public java.lang.String overseerModule_text_moduleRightTransfers() {
    return "Správce transferů";
  }
  
  public java.lang.String paymentDialog_text_quantity() {
    return "Počet";
  }
  
  public java.lang.String paymentTransfers_text_fees() {
    return "Poplatky";
  }
  
  public java.lang.String overseerModule_text_moduleRightRemoveWallMessageDescription() {
    return "Uživatelé mohou mazat zprávy ze zdi akci.";
  }
  
  public java.lang.String communitySettings_form_registerLink() {
    return "Registrační odkaz";
  }
  
  public java.lang.String logoSettingsContent_error_updateLogoFailed() {
    return "Logo se nezdařilo aktualizovat.";
  }
  
  public java.lang.String committee_action_chairChange() {
    return "Změnit předsedu";
  }
  
  public java.lang.String const_date_from() {
    return "Od";
  }
  
  public java.lang.String baseSettingsContent_title() {
    return "Názvy";
  }
  
  public java.lang.String detailSettingsContent_text_currentEvent() {
    return "Aktuální akce";
  }
  
  public java.lang.String paymentTransfers_help_showDetails() {
    return "Zobraz detail převodu";
  }
  
  public java.lang.String communitySettings_text_updateSuccess() {
    return "Popis a tagy byly aktualizovány.";
  }
  
  public java.lang.String paymentTransfers_text_information() {
    return "Informace";
  }
  
  public java.lang.String communitySettings_form_registerWidget() {
    return "Registrační widget";
  }
  
  public java.lang.String baseSettingsContent_form_internationalName() {
    return "Mezinárodní název";
  }
  
  public java.lang.String commonPropertyPanel_property_footerImgSrc() {
    return "Url obrázku v zápatí";
  }
  
  public java.lang.String paymentTransfersDetail_form_total() {
    return "Celkem";
  }
  
  public java.lang.String logoSettingsContent_form_logoProfilePicture() {
    return "Profilové logo akce";
  }
  
  public java.lang.String paymentsLicencesContent_helpText() {
    return "Akci můžete aktivovat po odsouhlasení všech podmínek.";
  }
  
  public java.lang.String communitySettings_action_showWidgetSource() {
    return "Zobrazit zdrojový kód widgetu";
  }
  
  public java.lang.String paymentTransfers_help_activeTransfer() {
    return "Aktivní transfer";
  }
  
  public java.lang.String paymentsPropertyPanel_error_invalidVat() {
    return "Neplatná hodnota daně, je vyžadované číslo od 0 do 100.";
  }
  
  public java.lang.String videoFilesSettings_text_separateNamesByCommas() {
    return "Oddělujte jména čárkou, např.: Jiří Novák, Marie Svobodová";
  }
  
  public java.lang.String profileLogoSettingsContent_form_logoPicture() {
    return "Logo profilu";
  }
  
  public java.lang.String paymentsProfileContent_title() {
    return "Platební profil";
  }
  
  public java.lang.String communitySettings_action_toEditor() {
    return "Zobrazit v editoru";
  }
  
  public java.lang.String videoFilesSettings_text_maxSize() {
    return "Max. velikost souboru je 500MB";
  }
  
  public java.lang.String communitySettings_helpText2() {
    return "Tagy by měly shrnout v několika slovech zaměření, cíl nebo klíčová témata. Správné označkování je důležité, pokud chcete, aby se akce správně vyhledávala v rámci Takeplace.";
  }
  
  public java.lang.String communitySettings_form_tags() {
    return "Tagy";
  }
  
  public java.lang.String paymentDialog_button_ok() {
    return "OK";
  }
  
  public java.lang.String rightsDialog_helpText() {
    return "Vyberte oprávnění ze seznamu; zde najdete o jaké oprávnění uživatele se jedná.";
  }
  
  public java.lang.String overseerModule_text_payments() {
    return "Platby";
  }
  
  public java.lang.String paymentDialog_text_unitPrice() {
    return "Cena za kus";
  }
  
  public java.lang.String videoFilesListItem_text_error() {
    return "CHYBA";
  }
  
  public java.lang.String eventWall_text_importantDates() {
    return "Důležitá data";
  }
  
  public java.lang.String committee_text_chairMissing() {
    return "Měli by jste začít přiřazením předsedy výboru.";
  }
  
  public java.lang.String videoFilesSettings_state_processing() {
    return "Zpracovává se";
  }
  
  public java.lang.String videoFilesSettings_text_all() {
    return "Vše";
  }
  
  public java.lang.String rightsDialog_helpTitle() {
    return "Přidat oprávnění";
  }
  
  public java.lang.String const_date_to() {
    return "Do";
  }
  
  public java.lang.String overseerModule_text_moduleRightInsertWallMessageAsEventDescription() {
    return "Uživatelé mohou vkládat zprávy na zeď jménem akce.";
  }
  
  public java.lang.String overseerModule_text_overseerModuleName() {
    return "Overseer modul";
  }
  
  public java.lang.String committee_action_addRight() {
    return "Přidat oprávnění";
  }
  
  public java.lang.String paymentsProfileContent_helpText1() {
    return "Platební profil je potřeba když kupujete licenci pro akci. Platební profil je také použit pro vystavování všech faktur v akci a jsou na něj převáděny peníze.";
  }
  
  public java.lang.String logoSettingsContent_text_updateLogoSuccess() {
    return "Logo bylo aktualizováno.";
  }
  
  public java.lang.String videoFilesSettings_text_speakers() {
    return "Přednášející";
  }
  
  public java.lang.String paymentTransfers_help_transferedTransfer() {
    return "Transfer převeden";
  }
  
  public java.lang.String overseerModule_text_administration() {
    return "Administrace";
  }
  
  public java.lang.String detailSettingsContent_title() {
    return "Kdy a kde";
  }
  
  public java.lang.String baseSettingsContent_form_shortName() {
    return "Krátký název";
  }
  
  public java.lang.String const_error_readOnlyProperty() {
    return "Některé položky již nelze měnit.";
  }
  
  public java.lang.String paymentTransfersDetail_form_account() {
    return "Číslo účtu";
  }
  
  public java.lang.String videos_title() {
    return "Videa";
  }
  
  public java.lang.String overseerModule_text_video() {
    return "Video";
  }
  
  public java.lang.String committee_form_committeeRights() {
    return "Práva";
  }
  
  public java.lang.String communitySettings_helpText1() {
    return "Popis pomůže lidem rozpoznat o jakou akci se přesně jedná. Měli by jste poskytnout stručný přehled o tom, co čeká potencionální návštěvníky a účastníky akce.";
  }
  
  public java.lang.String communitySettings_form_profileWebPage() {
    return "Profilová stránka akce";
  }
  
  public java.lang.String overseerModule_text_moduleRightTransfersDescription() {
    return "Uživatelé s touto rolí mají autorizaci k vytvoření požadavku na transfer a mají prístup k tokům peněz. Můžou také stahovat účastnické faktury.";
  }
  
  public java.lang.String eventSettings_title() {
    return "Nastavení akce";
  }
  
  public java.lang.String logoSettingsContent_action_changeLogo() {
    return "Změnit logo";
  }
  
  public java.lang.String paymentsLicencesContent_text_agree3() {
    return "Souhlasím s <a href=\"http://static.takeplace.eu/Agreement_of_Confidentiality_v5_clear_en.pdf\" target=\"_blank\">Dohoda o důvěrnosti</a>";
  }
}
