package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdatePasswordAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getPass(consys.admin.user.gwt.client.action.UpdatePasswordAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.UpdatePasswordAction::pass;
  }-*/;
  
  private static native void setPass(consys.admin.user.gwt.client.action.UpdatePasswordAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.UpdatePasswordAction::pass = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.UpdatePasswordAction instance) throws SerializationException {
    setPass(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.UpdatePasswordAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.UpdatePasswordAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.UpdatePasswordAction instance) throws SerializationException {
    streamWriter.writeString(getPass(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.UpdatePasswordAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdatePasswordAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.UpdatePasswordAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdatePasswordAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.UpdatePasswordAction)object);
  }
  
}
