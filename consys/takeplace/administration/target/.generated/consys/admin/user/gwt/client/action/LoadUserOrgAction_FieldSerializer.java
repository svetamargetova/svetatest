package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadUserOrgAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUserOrgUuid(consys.admin.user.gwt.client.action.LoadUserOrgAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.LoadUserOrgAction::userOrgUuid;
  }-*/;
  
  private static native void setUserOrgUuid(consys.admin.user.gwt.client.action.LoadUserOrgAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.LoadUserOrgAction::userOrgUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.LoadUserOrgAction instance) throws SerializationException {
    setUserOrgUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.LoadUserOrgAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.LoadUserOrgAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.LoadUserOrgAction instance) throws SerializationException {
    streamWriter.writeString(getUserOrgUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.LoadUserOrgAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LoadUserOrgAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.LoadUserOrgAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LoadUserOrgAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.LoadUserOrgAction)object);
  }
  
}
