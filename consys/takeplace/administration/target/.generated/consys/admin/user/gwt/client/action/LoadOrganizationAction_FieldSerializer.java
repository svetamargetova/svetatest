package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadOrganizationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.admin.user.gwt.client.action.LoadOrganizationAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.LoadOrganizationAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.user.gwt.client.action.LoadOrganizationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.LoadOrganizationAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.LoadOrganizationAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.LoadOrganizationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.LoadOrganizationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.LoadOrganizationAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.LoadOrganizationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LoadOrganizationAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.LoadOrganizationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LoadOrganizationAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.LoadOrganizationAction)object);
  }
  
}
