package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LostPasswordAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEmail(consys.admin.user.gwt.client.action.LostPasswordAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.LostPasswordAction::email;
  }-*/;
  
  private static native void setEmail(consys.admin.user.gwt.client.action.LostPasswordAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.LostPasswordAction::email = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.LostPasswordAction instance) throws SerializationException {
    setEmail(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.LostPasswordAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.LostPasswordAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.LostPasswordAction instance) throws SerializationException {
    streamWriter.writeString(getEmail(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.LostPasswordAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LostPasswordAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.LostPasswordAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LostPasswordAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.LostPasswordAction)object);
  }
  
}
