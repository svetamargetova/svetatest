package consys.admin.user.gwt.client.utils.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class AUBundle_default_InlineClientBundleGenerator implements consys.admin.user.gwt.client.utils.css.AUBundle {
  private static AUBundle_default_InlineClientBundleGenerator _instance0 = new AUBundle_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new consys.admin.user.gwt.client.utils.css.AUCssResources() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCOOUJ4DOG{font-size:" + ("16px")  + ";font-weight:" + ("bold")  + ";float:" + ("right")  + ";margin:" + ("10px"+ " " +"10px"+ " " +"10px"+ " " +"10px")  + ";}.GCOOUJ4DNG{font-size:" + ("16px")  + ";font-weight:" + ("bold")  + ";float:" + ("right")  + ";margin:" + ("10px"+ " " +"0"+ " " +"10px"+ " " +"10px")  + ";}.GCOOUJ4DMG{margin:" + ("0"+ " " +"30px"+ " " +"10px"+ " " +"20px")  + ";}.GCOOUJ4DMG a{color:" + ("#1a809a")  + ";font-weight:") + (("bold")  + ";text-decoration:" + ("underline")  + ";}.GCOOUJ4DMG a:hover{color:" + ("#1a809a")  + ";font-weight:" + ("bold")  + ";text-decoration:" + ("none")  + ";}.GCOOUJ4DPG{font-weight:" + ("bold")  + ";margin:" + ("0"+ " " +"20px"+ " " +"10px"+ " " +"20px")  + ";}")) : ((".GCOOUJ4DOG{font-size:" + ("16px")  + ";font-weight:" + ("bold")  + ";float:" + ("left")  + ";margin:" + ("10px"+ " " +"10px"+ " " +"10px"+ " " +"10px")  + ";}.GCOOUJ4DNG{font-size:" + ("16px")  + ";font-weight:" + ("bold")  + ";float:" + ("left")  + ";margin:" + ("10px"+ " " +"10px"+ " " +"10px"+ " " +"0")  + ";}.GCOOUJ4DMG{margin:" + ("0"+ " " +"20px"+ " " +"10px"+ " " +"30px")  + ";}.GCOOUJ4DMG a{color:" + ("#1a809a")  + ";font-weight:") + (("bold")  + ";text-decoration:" + ("underline")  + ";}.GCOOUJ4DMG a:hover{color:" + ("#1a809a")  + ";font-weight:" + ("bold")  + ";text-decoration:" + ("none")  + ";}.GCOOUJ4DPG{font-weight:" + ("bold")  + ";margin:" + ("0"+ " " +"20px"+ " " +"10px"+ " " +"20px")  + ";}"));
      }
      public java.lang.String faqAnswer(){
        return "GCOOUJ4DMG";
      }
      public java.lang.String faqQuestion(){
        return "GCOOUJ4DNG";
      }
      public java.lang.String faqState(){
        return "GCOOUJ4DOG";
      }
      public java.lang.String fillEmailDialogTitle(){
        return "GCOOUJ4DPG";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.admin.user.gwt.client.utils.css.AUCssResources get() {
      return css;
    }
  }
  public consys.admin.user.gwt.client.utils.css.AUCssResources css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.admin.user.gwt.client.utils.css.AUCssResources css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@consys.admin.user.gwt.client.utils.css.AUBundle::css()();
    }
    return null;
  }-*/;
}
