package consys.admin.user.gwt.client.message;

public class AdminUserConstants_ implements consys.admin.user.gwt.client.message.AdminUserConstants {
  
  public java.lang.String headProfileRow_action_logOut() {
    return "Log out";
  }
  
  public java.lang.String aboutYourself_text_successBioUpdate() {
    return "Personal resume has been saved.";
  }
  
  public java.lang.String userOrg_form_currentlyWork() {
    return "I currently work here";
  }
  
  public java.lang.String faqContent_title() {
    return "Frequently asked questions";
  }
  
  public java.lang.String privateOrgContent_text_successAddressUpdate() {
    return "The address has been saved.";
  }
  
  public java.lang.String headProfileRow_action_notFillEmail() {
    return "We need your email address to confirm your registration. We will send event notifications, tickets or invoices to the email address you provide. ";
  }
  
  public java.lang.String activateContent_title() {
    return "Activate your account";
  }
  
  public java.lang.String changePasswordContent_helpText1() {
    return "You can set a new Takeplace password if you remember the current one. If you forgot your password, we can create a new one for you.";
  }
  
  public java.lang.String activateContent_error_activateFieldBadInput() {
    return "Wrong activation key entered. Please use the key that was sent to your contact e-mail.";
  }
  
  public java.lang.String faqContent_question5() {
    return "I have registered, how can I pay for it?";
  }
  
  public java.lang.String lostPasswordContent_title() {
    return "Lost password";
  }
  
  public java.lang.String faqContent_question2() {
    return "I have registered but have not received the email with activation link.";
  }
  
  public java.lang.String faqContent_question3() {
    return "I want to log into my conference, where can I find the link?";
  }
  
  public java.lang.String organizationDetailForm_form_moreNameOptions() {
    return "Show more name options";
  }
  
  public java.lang.String userOrg_form_preferredPosition() {
    return "Preferred position";
  }
  
  public java.lang.String faqContent_question4() {
    return "My event is registered in Takeplace but the link to the event is not visible.";
  }
  
  public java.lang.String faqContent_question6() {
    return "How to make an online payment?";
  }
  
  public java.lang.String myResume_title() {
    return "My resume";
  }
  
  public java.lang.String organizationSuggestOracle_text_createNewOrganization() {
    return "Create new organization";
  }
  
  public java.lang.String passwordChangeContent_form_oldPassword() {
    return "Old password";
  }
  
  public java.lang.String userModule_mainMenu_profile() {
    return "Profile";
  }
  
  public java.lang.String faqContent_question7() {
    return "If it is not possible to finish the online payment, what should I do?";
  }
  
  public java.lang.String headProfileRow_action_accountNotActivated() {
    return "Account not activated";
  }
  
  public java.lang.String registerContent_title() {
    return "New Takeplace account";
  }
  
  public java.lang.String fillEmailDialog_text_email() {
    return "Email";
  }
  
  public java.lang.String accountMenu_title_account() {
    return "Account details";
  }
  
  public java.lang.String registerContent_helpTitle() {
    return "You are now creating your Takeplace personal account.";
  }
  
  public java.lang.String passwordChangeContent_error_badLengthOfOldPassword() {
    return "Bad length of your current password.";
  }
  
  public java.lang.String faqContent_answer7() {
    return "You are probably not an owner of a credit card that supports online payments. Please check with your bank if you have the right type of credit card. Alternatively, you can use the bank transfer to pay for your event.";
  }
  
  public java.lang.String joinPanel_title() {
    return "Join Takeplace now!";
  }
  
  public java.lang.String organizationDetailForm_form_internationalName() {
    return "International name";
  }
  
  public java.lang.String profileImageContent_button_picture() {
    return "PICTURE";
  }
  
  public java.lang.String profileImageContent_error_profileUpdateFailed() {
    return "The profile image update has failed.";
  }
  
  public java.lang.String privateOrgContent_helpText() {
    return "When you attend an event, organizers may need to know your afiliation. It can be pre-filled into the registration form which will save your time.";
  }
  
  public java.lang.String faqContent_answer6() {
    return "After the registration, you will receive a detailed information about how to pay through PayPal, please follow the instructions.";
  }
  
  public java.lang.String faqContent_question9() {
    return "I need the invoice before the payment.";
  }
  
  public java.lang.String activateContent_helpText3() {
    return "If you did not receive the activation e-mail, please contact Takeplace support at support@takeplece.eu.";
  }
  
  public java.lang.String activateContent_text2() {
    return "Please check your inbox for the e-mail with further instruction on account activation. If you do not receive the activation e-mail within 15 minutes, you should check your spam-box as well.";
  }
  
  public java.lang.String changePasswordContent_error_invalidCharactersInPassword() {
    return "Password contains invalid characters. You can use only letters (both upper-case and lower-case, without diacritical marks), numbers, and some special characters , . _ - = + @ # $ % ^ & *";
  }
  
  public java.lang.String invitationContent_error_missingToken() {
    return "Bad or missing token";
  }
  
  public java.lang.String userOrg_subtitle_currentlyWork() {
    return "I currently work here";
  }
  
  public java.lang.String profileImageContent_text_profileUpdateSuccessfull() {
    return "The profile image has been updated.";
  }
  
  public java.lang.String fillEmailDialog_action_updateEmail() {
    return "Update email";
  }
  
  public java.lang.String privateOrgContent_title_personalAddress() {
    return "Personal address";
  }
  
  public java.lang.String activateContent_button_confirmAccount() {
    return "CONFIRM ACCOUNT";
  }
  
  public java.lang.String passwordChangeContent_text_successPasswordChange() {
    return "The password has been changed.";
  }
  
  public java.lang.String lostPasswordContent_helpText3() {
    return "Always keep your password safe and secret!";
  }
  
  public java.lang.String personalInfo_title() {
    return "Personal info";
  }
  
  public java.lang.String faqContent_answer4() {
    return "Your event will be visible and accessible to participants after the registration fee is paid and activated. If you have any futher questions, please contact our support team at <a href=\"mailto:support@takeplace.eu\">support@takeplace.eu</a>";
  }
  
  public java.lang.String passwordChangeContent_button_changePassword() {
    return "CHANGE PASSWORD";
  }
  
  public java.lang.String faqContent_question1() {
    return "How can I register for Takeplace?";
  }
  
  public java.lang.String loginContent_helpText2() {
    return "Are you a new user? Please register into Takeplace first.";
  }
  
  public java.lang.String joinPanel_text() {
    return "Takeplace is the leading event management service for easy and efficient organization of professional events. Takeplace makes events run!";
  }
  
  public java.lang.String loginContent_helpText1() {
    return "Did you lost your password? We will send you a new one.";
  }
  
  public java.lang.String loginContent_helpTitle() {
    return "Log into Takeplace with your contact e-mail and account password.";
  }
  
  public java.lang.String aboutYourself_helpText() {
    return "Short resume helps others to get to know you better. It should highlight in a few sentences both your professional and personal experience, skills, abilities and interests.";
  }
  
  public java.lang.String activateContent_helpText1() {
    return "You should now activate your account with the key we have sent you.";
  }
  
  public java.lang.String faqContent_answer9() {
    return "We can only send the invoice after receiving your payment. For more information please contact our support team at <a href=\"mailto:support@takeplace.eu\">support@takeplace.eu</a>";
  }
  
  public java.lang.String personInfoContent_form_frontDegree() {
    return "Front degree";
  }
  
  public java.lang.String loginContent_helpText3() {
    return "Is you account activated? If not, check your inbox for an activation e-mail.";
  }
  
  public java.lang.String userOrg_form_timePeriod() {
    return "Time period";
  }
  
  public java.lang.String personInfoContent_text_successPersonalInfoUpdate() {
    return "Your personal details have been saved.";
  }
  
  public java.lang.String faqContent_answer2() {
    return "Please check your email SPAM box. If you do not receive the email within 24 hours after you registered, please contact our support centre at <a href=\"mailto:support@takeplace.com\">support@takeplace.eu</a>";
  }
  
  public java.lang.String experience_title() {
    return "Experience";
  }
  
  public java.lang.String aboutYourself_helpTitle() {
    return "Why should I fill my resume?";
  }
  
  public java.lang.String loginContent_text_activationSuccessfull() {
    return "Activation was successfull. You can log in now.";
  }
  
  public java.lang.String lostPasswordContent_text2() {
    return "Check your inbox for the e-mail with instruction on setting a new password. If you do not receive the instructional e-mail within 15 minutes, you should check your spam-box as well.";
  }
  
  public java.lang.String personalInfo_text_successPersonalInfoUpdate() {
    return "Your personal informations have been updated.";
  }
  
  public java.lang.String loginPanel_action_notActivated() {
    return "Not activated?";
  }
  
  public java.lang.String loginContent_text_lostPasswordChanged() {
    return "The password has been reset. Please log in now.";
  }
  
  public java.lang.String headProfileRow_text_accountActivated() {
    return "Account was successfully activated";
  }
  
  public java.lang.String loginPanel_action_notRegistered() {
    return "Not registered?";
  }
  
  public java.lang.String profileMenuContent_title() {
    return "Personal profile";
  }
  
  public java.lang.String personInfoContent_form_rearDegree() {
    return "Rear degree";
  }
  
  public java.lang.String passwordChangeContent_error_oldPasswordIsEmpty() {
    return "Your current password must be entered.";
  }
  
  public java.lang.String faqContent_answer8() {
    return "After the registration you will receive the pro forma invoice where you will find all the informations needed for the bank transfer. Please make sure you fill out the variable symbol correctly.";
  }
  
  public java.lang.String loginPanel_action_lostPassword() {
    return "Reset your password?";
  }
  
  public java.lang.String userOrganizationsContent_error_experienceListFailed() {
    return "Cannot load the experience list.";
  }
  
  public java.lang.String userOrg_error_userOrganizationNotExists() {
    return "Cannot delete this experience.";
  }
  
  public java.lang.String userOrg_subtitle_pastPositions() {
    return "Past positions";
  }
  
  public java.lang.String faqContent_answer10() {
    return "On the login page, go to „Send me a hint.“ It will send you an email with information on how to generate a new password. Please follow the instructions.";
  }
  
  public java.lang.String aboutYourself_text_pleaseEnterBio() {
    return "Here you can enter your resume.";
  }
  
  public java.lang.String userOrg_error_invalidIdOfExperience() {
    return "This is not a proper experience";
  }
  
  public java.lang.String lostPasswordContent_error_userNotExists() {
    return "The user does not exist.";
  }
  
  public java.lang.String changePasswordContent_title() {
    return "Change your password";
  }
  
  public java.lang.String userOrganizationsContent_text_experienceSucessfullyUpdated() {
    return "The experience has been updated.";
  }
  
  public java.lang.String newUserOrganizationDialog_title() {
    return "New experience";
  }
  
  public java.lang.String userOrg_text_areYouSure() {
    return "Are you sure?";
  }
  
  public java.lang.String changePasswordContent_helpTitle() {
    return "Changing your password is easy.";
  }
  
  public java.lang.String changePasswordContent_button_changePassword() {
    return "CHANGE PASSWORD";
  }
  
  public java.lang.String personInfoContent_form_contactEmail() {
    return "Contact e-mail";
  }
  
  public java.lang.String passwordChangeContent_form_newPassword() {
    return "New password";
  }
  
  public java.lang.String activateContent_text1() {
    return "Before you get started with Takeplace you need to activate your account. A confirmative e-mail has been sent to your contact e-mail address.";
  }
  
  public java.lang.String lostPasswordContent_helpText1() {
    return "To reset the password just enter your contact e-mail — the same as you are registered with — and we will send you more instructions.";
  }
  
  public java.lang.String registerContent_button_registerAccount() {
    return "REGISTER ACCOUNT";
  }
  
  public java.lang.String faqContent_answer5() {
    return "After completing the registration process, it is possible to pay in two ways: through online service PayPal which accepts all major credit cards, or via bank transfer.";
  }
  
  public java.lang.String userOrganizationsContent_title() {
    return "Career & experience";
  }
  
  public java.lang.String userOrg_text_at() {
    return "at";
  }
  
  public java.lang.String registerContent_helpText1() {
    return "Please fill in the form to create your Takeplace account.";
  }
  
  public java.lang.String faqContent_answer3() {
    return "After logging in, please choose the EVENTS section and then follow the ALL EVENTS link – there you find your event.";
  }
  
  public java.lang.String changePasswordContent_error_lostPasswordTokenMissing() {
    return "Please follow the link you have received in your e-mail inbox.";
  }
  
  public java.lang.String privateOrgContent_helpTitle() {
    return "For what is organization used?";
  }
  
  public java.lang.String lostPasswordContent_helpText2() {
    return "If you do not receive the instructional e-mail, please contact Takeplace support at support@takeplece.eu.";
  }
  
  public java.lang.String activateContent_helpText2() {
    return "Please note that the activation key is valid for 30 days. If you do not activate your account within this period, you will need to register again.";
  }
  
  public java.lang.String userOrg_form_at() {
    return "At";
  }
  
  public java.lang.String registerContent_helpText2() {
    return "The contact e-mail shall be permanent as it will identify you throughout Takeplace and will serve as the main communication channel.";
  }
  
  public java.lang.String faqContent_question8() {
    return "How can I pay via bank transfer?";
  }
  
  public java.lang.String loginPanel_text() {
    return "Already registred? Log in here:";
  }
  
  public java.lang.String organizationDetailForm_form_industry() {
    return "Industry";
  }
  
  public java.lang.String activateContent_error_activationFailed() {
    return "Activation failed. Please use the activation key that was sent to your contact e-mail.";
  }
  
  public java.lang.String userOrg_text_presentLower() {
    return "present";
  }
  
  public java.lang.String registerContent_helpText3() {
    return "The password shall be at least eight characters long. Please avoid using common names and words.";
  }
  
  public java.lang.String faqContent_question10() {
    return "I am registered but I have forgotten my password. What should I do?";
  }
  
  public java.lang.String activateContent_text3() {
    return "After receiving the activation e-mail please click the link in your e-mail client, or enter the activation key we have sent you in the box below:";
  }
  
  public java.lang.String userOrg_form_position() {
    return "Position";
  }
  
  public java.lang.String invitationContent_button_registerAccount() {
    return "REGISTER ACCOUNT";
  }
  
  public java.lang.String aboutYourself_title_bio() {
    return "About yourself";
  }
  
  public java.lang.String changePasswordContent_error_lostPasswordBadToken() {
    return "The link you are following is not valid. Please try resetting your password again.";
  }
  
  public java.lang.String loginPanel_button_logIn() {
    return "LOG IN";
  }
  
  public java.lang.String lostPasswordContent_text1() {
    return "Please enter the contact e-mail address you are registered with Takeplace and we will send you further instructions.";
  }
  
  public java.lang.String invitationContent_title() {
    return "Invitation";
  }
  
  public java.lang.String lostPasswordContent_text_lostPasswordSendSuccess() {
    return "The instructions have been sent to your contact e-mail. Please check your inbox now.";
  }
  
  public java.lang.String faqContent_answer1() {
    return "If you have not registered yet, please go to the REGISTRATION link and fill up the information about you. Your login is the email address you fill up in the registration section. The email address will be used for sending informations to you. After completing the registration, you will receive an email with activation link. Please click on the link and finish your registration. After that you will be able to log on to Takeplace.";
  }
  
  public java.lang.String changePasswordContent_helpText2() {
    return "Always keep your password safe and secret!";
  }
  
  public java.lang.String activateContent_helpTitle() {
    return "Thank you for registering with Takeplace!";
  }
  
  public java.lang.String lostPasswordContent_helpTitle() {
    return "If you forgot your Takeplace password, we can create a new one for you.";
  }
}
