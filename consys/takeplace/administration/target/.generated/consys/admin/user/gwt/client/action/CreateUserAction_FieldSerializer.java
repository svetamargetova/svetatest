package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateUserAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientRegistration getCr(consys.admin.user.gwt.client.action.CreateUserAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.CreateUserAction::cr;
  }-*/;
  
  private static native void setCr(consys.admin.user.gwt.client.action.CreateUserAction instance, consys.common.gwt.shared.bo.ClientRegistration value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.CreateUserAction::cr = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.CreateUserAction instance) throws SerializationException {
    setCr(instance, (consys.common.gwt.shared.bo.ClientRegistration) streamReader.readObject());
    
  }
  
  public static consys.admin.user.gwt.client.action.CreateUserAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.CreateUserAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.CreateUserAction instance) throws SerializationException {
    streamWriter.writeObject(getCr(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.CreateUserAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.CreateUserAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.CreateUserAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.CreateUserAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.CreateUserAction)object);
  }
  
}
