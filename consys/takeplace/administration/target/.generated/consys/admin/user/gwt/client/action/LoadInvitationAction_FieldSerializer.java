package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadInvitationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getToken(consys.admin.user.gwt.client.action.LoadInvitationAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.LoadInvitationAction::token;
  }-*/;
  
  private static native void setToken(consys.admin.user.gwt.client.action.LoadInvitationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.LoadInvitationAction::token = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.LoadInvitationAction instance) throws SerializationException {
    setToken(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.LoadInvitationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.LoadInvitationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.LoadInvitationAction instance) throws SerializationException {
    streamWriter.writeString(getToken(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.LoadInvitationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LoadInvitationAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.LoadInvitationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.LoadInvitationAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.LoadInvitationAction)object);
  }
  
}
