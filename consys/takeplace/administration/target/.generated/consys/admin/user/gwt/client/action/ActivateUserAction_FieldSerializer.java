package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ActivateUserAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getKey(consys.admin.user.gwt.client.action.ActivateUserAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.ActivateUserAction::key;
  }-*/;
  
  private static native void setKey(consys.admin.user.gwt.client.action.ActivateUserAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.ActivateUserAction::key = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.ActivateUserAction instance) throws SerializationException {
    setKey(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.ActivateUserAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.ActivateUserAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.ActivateUserAction instance) throws SerializationException {
    streamWriter.writeString(getKey(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.ActivateUserAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ActivateUserAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.ActivateUserAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ActivateUserAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.ActivateUserAction)object);
  }
  
}
