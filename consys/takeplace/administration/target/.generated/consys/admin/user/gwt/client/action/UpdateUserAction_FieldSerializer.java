package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateUserAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientUser getCu(consys.admin.user.gwt.client.action.UpdateUserAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.UpdateUserAction::cu;
  }-*/;
  
  private static native void setCu(consys.admin.user.gwt.client.action.UpdateUserAction instance, consys.common.gwt.shared.bo.ClientUser value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.UpdateUserAction::cu = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.UpdateUserAction instance) throws SerializationException {
    setCu(instance, (consys.common.gwt.shared.bo.ClientUser) streamReader.readObject());
    
  }
  
  public static consys.admin.user.gwt.client.action.UpdateUserAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.UpdateUserAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.UpdateUserAction instance) throws SerializationException {
    streamWriter.writeObject(getCu(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.UpdateUserAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdateUserAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.UpdateUserAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdateUserAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.UpdateUserAction)object);
  }
  
}
