package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListOrganizationsByPrefixAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getLimit(consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction::limit;
  }-*/;
  
  private static native void setLimit(consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction instance, int value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction::limit = value;
  }-*/;
  
  private static native java.lang.String getPrefix(consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction::prefix;
  }-*/;
  
  private static native void setPrefix(consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction::prefix = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction instance) throws SerializationException {
    setLimit(instance, streamReader.readInt());
    setPrefix(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction instance) throws SerializationException {
    streamWriter.writeInt(getLimit(instance));
    streamWriter.writeString(getPrefix(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.ListOrganizationsByPrefixAction)object);
  }
  
}
