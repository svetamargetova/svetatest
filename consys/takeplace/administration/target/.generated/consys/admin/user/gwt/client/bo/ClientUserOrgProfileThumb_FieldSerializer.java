package consys.admin.user.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUserOrgProfileThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getActualPosition(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::actualPosition;
  }-*/;
  
  private static native void setActualPosition(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance, boolean value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::actualPosition = value;
  }-*/;
  
  private static native java.util.Date getFrom(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::from;
  }-*/;
  
  private static native void setFrom(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance, java.util.Date value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::from = value;
  }-*/;
  
  private static native java.lang.String getOrgName(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::orgName;
  }-*/;
  
  private static native void setOrgName(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::orgName = value;
  }-*/;
  
  private static native java.lang.String getPosition(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::position;
  }-*/;
  
  private static native void setPosition(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::position = value;
  }-*/;
  
  private static native boolean getPrefferedPosition(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::prefferedPosition;
  }-*/;
  
  private static native void setPrefferedPosition(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance, boolean value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::prefferedPosition = value;
  }-*/;
  
  private static native java.util.Date getTo(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::to;
  }-*/;
  
  private static native void setTo(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance, java.util.Date value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::to = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) throws SerializationException {
    setActualPosition(instance, streamReader.readBoolean());
    setFrom(instance, (java.util.Date) streamReader.readObject());
    setOrgName(instance, streamReader.readString());
    setPosition(instance, streamReader.readString());
    setPrefferedPosition(instance, streamReader.readBoolean());
    setTo(instance, (java.util.Date) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb instance) throws SerializationException {
    streamWriter.writeBoolean(getActualPosition(instance));
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeString(getOrgName(instance));
    streamWriter.writeString(getPosition(instance));
    streamWriter.writeBoolean(getPrefferedPosition(instance));
    streamWriter.writeObject(getTo(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.bo.ClientUserOrgProfileThumb)object);
  }
  
}
