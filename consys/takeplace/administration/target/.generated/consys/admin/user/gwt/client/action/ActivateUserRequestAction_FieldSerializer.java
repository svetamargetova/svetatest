package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ActivateUserRequestAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUserUuid(consys.admin.user.gwt.client.action.ActivateUserRequestAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.ActivateUserRequestAction::userUuid;
  }-*/;
  
  private static native void setUserUuid(consys.admin.user.gwt.client.action.ActivateUserRequestAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.ActivateUserRequestAction::userUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.ActivateUserRequestAction instance) throws SerializationException {
    setUserUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.ActivateUserRequestAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.ActivateUserRequestAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.ActivateUserRequestAction instance) throws SerializationException {
    streamWriter.writeString(getUserUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.ActivateUserRequestAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ActivateUserRequestAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.ActivateUserRequestAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ActivateUserRequestAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.ActivateUserRequestAction)object);
  }
  
}
