package consys.admin.user.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ClientUserOrg_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getActualPosition(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::actualPosition;
  }-*/;
  
  private static native void setActualPosition(consys.admin.user.gwt.client.bo.ClientUserOrg instance, boolean value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::actualPosition = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientAddress getAddress(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::address;
  }-*/;
  
  private static native void setAddress(consys.admin.user.gwt.client.bo.ClientUserOrg instance, consys.common.gwt.shared.bo.ClientAddress value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::address = value;
  }-*/;
  
  private static native java.util.Date getFromDate(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::fromDate;
  }-*/;
  
  private static native void setFromDate(consys.admin.user.gwt.client.bo.ClientUserOrg instance, java.util.Date value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::fromDate = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientOrganization getOrganization(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::organization;
  }-*/;
  
  private static native void setOrganization(consys.admin.user.gwt.client.bo.ClientUserOrg instance, consys.common.gwt.shared.bo.ClientOrganization value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::organization = value;
  }-*/;
  
  private static native java.lang.String getPosition(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::position;
  }-*/;
  
  private static native void setPosition(consys.admin.user.gwt.client.bo.ClientUserOrg instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::position = value;
  }-*/;
  
  private static native boolean getPreferredPosition(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::preferredPosition;
  }-*/;
  
  private static native void setPreferredPosition(consys.admin.user.gwt.client.bo.ClientUserOrg instance, boolean value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::preferredPosition = value;
  }-*/;
  
  private static native java.util.Date getToDate(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::toDate;
  }-*/;
  
  private static native void setToDate(consys.admin.user.gwt.client.bo.ClientUserOrg instance, java.util.Date value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::toDate = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.admin.user.gwt.client.bo.ClientUserOrg instance) /*-{
    return instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.user.gwt.client.bo.ClientUserOrg instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.bo.ClientUserOrg::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.bo.ClientUserOrg instance) throws SerializationException {
    setActualPosition(instance, streamReader.readBoolean());
    setAddress(instance, (consys.common.gwt.shared.bo.ClientAddress) streamReader.readObject());
    setFromDate(instance, (java.util.Date) streamReader.readObject());
    setOrganization(instance, (consys.common.gwt.shared.bo.ClientOrganization) streamReader.readObject());
    setPosition(instance, streamReader.readString());
    setPreferredPosition(instance, streamReader.readBoolean());
    setToDate(instance, (java.util.Date) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.bo.ClientUserOrg instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.bo.ClientUserOrg();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.bo.ClientUserOrg instance) throws SerializationException {
    streamWriter.writeBoolean(getActualPosition(instance));
    streamWriter.writeObject(getAddress(instance));
    streamWriter.writeObject(getFromDate(instance));
    streamWriter.writeObject(getOrganization(instance));
    streamWriter.writeString(getPosition(instance));
    streamWriter.writeBoolean(getPreferredPosition(instance));
    streamWriter.writeObject(getToDate(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.bo.ClientUserOrg_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.bo.ClientUserOrg_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.bo.ClientUserOrg)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.bo.ClientUserOrg_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.bo.ClientUserOrg)object);
  }
  
}
