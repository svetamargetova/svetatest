package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateUserOrgAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.admin.user.gwt.client.bo.ClientUserOrg getUo(consys.admin.user.gwt.client.action.CreateUserOrgAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.CreateUserOrgAction::uo;
  }-*/;
  
  private static native void setUo(consys.admin.user.gwt.client.action.CreateUserOrgAction instance, consys.admin.user.gwt.client.bo.ClientUserOrg value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.CreateUserOrgAction::uo = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.CreateUserOrgAction instance) throws SerializationException {
    setUo(instance, (consys.admin.user.gwt.client.bo.ClientUserOrg) streamReader.readObject());
    
  }
  
  public static consys.admin.user.gwt.client.action.CreateUserOrgAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.CreateUserOrgAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.CreateUserOrgAction instance) throws SerializationException {
    streamWriter.writeObject(getUo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.CreateUserOrgAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.CreateUserOrgAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.CreateUserOrgAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.CreateUserOrgAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.CreateUserOrgAction)object);
  }
  
}
