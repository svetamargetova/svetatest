package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdateLostPasswordAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getToken(consys.admin.user.gwt.client.action.UpdateLostPasswordAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.UpdateLostPasswordAction::token;
  }-*/;
  
  private static native void setToken(consys.admin.user.gwt.client.action.UpdateLostPasswordAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.UpdateLostPasswordAction::token = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.UpdateLostPasswordAction instance) throws SerializationException {
    setToken(instance, streamReader.readString());
    
    consys.admin.user.gwt.client.action.UpdatePasswordAction_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static consys.admin.user.gwt.client.action.UpdateLostPasswordAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.UpdateLostPasswordAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.UpdateLostPasswordAction instance) throws SerializationException {
    streamWriter.writeString(getToken(instance));
    
    consys.admin.user.gwt.client.action.UpdatePasswordAction_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.UpdateLostPasswordAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdateLostPasswordAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.UpdateLostPasswordAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdateLostPasswordAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.UpdateLostPasswordAction)object);
  }
  
}
