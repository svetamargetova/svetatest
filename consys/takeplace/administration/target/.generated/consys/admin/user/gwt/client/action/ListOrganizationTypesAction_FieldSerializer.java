package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListOrganizationTypesAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.ListOrganizationTypesAction instance) throws SerializationException {
    
  }
  
  public static consys.admin.user.gwt.client.action.ListOrganizationTypesAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.ListOrganizationTypesAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.ListOrganizationTypesAction instance) throws SerializationException {
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.ListOrganizationTypesAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ListOrganizationTypesAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.ListOrganizationTypesAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ListOrganizationTypesAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.ListOrganizationTypesAction)object);
  }
  
}
