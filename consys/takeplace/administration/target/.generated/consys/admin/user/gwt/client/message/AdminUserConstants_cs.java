package consys.admin.user.gwt.client.message;

public class AdminUserConstants_cs implements consys.admin.user.gwt.client.message.AdminUserConstants {
  
  public java.lang.String headProfileRow_action_logOut() {
    return "Odhlásit se";
  }
  
  public java.lang.String aboutYourself_text_successBioUpdate() {
    return "Váš životopis byl uložen.";
  }
  
  public java.lang.String userOrg_form_currentlyWork() {
    return "Zde právě pracuji";
  }
  
  public java.lang.String faqContent_title() {
    return "Často kladené dotazy";
  }
  
  public java.lang.String privateOrgContent_text_successAddressUpdate() {
    return "Adresa byla uložena.";
  }
  
  public java.lang.String headProfileRow_action_notFillEmail() {
    return "Potřebujeme abyste vyplnili Váš email pro potvrzení registrace do Takeplace. Na tento email Vám budou zasílány notifikace, vstupenky nebo faktury.";
  }
  
  public java.lang.String activateContent_title() {
    return "Aktivujte si účet";
  }
  
  public java.lang.String changePasswordContent_helpText1() {
    return "Své heslo do Takeplace si můžete změnit, pokud znáte heslo stávající. Pokud jste heslo zapomněli, můžete si vygenerovat nové.";
  }
  
  public java.lang.String activateContent_error_activateFieldBadInput() {
    return "Zadán špatný aktivační klíč. Prosím vložte klíč, který vám byl zaslán na kontaktní e-mail.";
  }
  
  public java.lang.String faqContent_question5() {
    return "Zaregistroval/a jsem se, jak mám zaplatit?";
  }
  
  public java.lang.String lostPasswordContent_title() {
    return "Ztracené heslo";
  }
  
  public java.lang.String faqContent_question2() {
    return "Zaregistroval/a jsem se, ale nepřišel mi aktivační email.";
  }
  
  public java.lang.String faqContent_question3() {
    return "Chci se přihlásit na sekce své konference, kde ji najdu?";
  }
  
  public java.lang.String organizationDetailForm_form_moreNameOptions() {
    return "Zobrazit více nastavení názvu";
  }
  
  public java.lang.String userOrg_form_preferredPosition() {
    return "Preferovaná funkce";
  }
  
  public java.lang.String faqContent_question4() {
    return "Konference je v Takeplace vedená, ale není na ni vidět odkaz.";
  }
  
  public java.lang.String faqContent_question6() {
    return "Jak mám postupovat při platbě online?";
  }
  
  public java.lang.String myResume_title() {
    return "Životopis";
  }
  
  public java.lang.String organizationSuggestOracle_text_createNewOrganization() {
    return "Vytvořit novou organizaci";
  }
  
  public java.lang.String passwordChangeContent_form_oldPassword() {
    return "Staré heslo";
  }
  
  public java.lang.String userModule_mainMenu_profile() {
    return "Profil";
  }
  
  public java.lang.String faqContent_question7() {
    return "Platbu online se mi nedaří provést, jak mám postupovat?";
  }
  
  public java.lang.String headProfileRow_action_accountNotActivated() {
    return "Neaktivovaný účet";
  }
  
  public java.lang.String registerContent_title() {
    return "Nový Takeplace účet";
  }
  
  public java.lang.String fillEmailDialog_text_email() {
    return "Email";
  }
  
  public java.lang.String accountMenu_title_account() {
    return "Detaily účtu";
  }
  
  public java.lang.String registerContent_helpTitle() {
    return "Právě vytváříte svůj osobní účet v Takeplace.";
  }
  
  public java.lang.String passwordChangeContent_error_badLengthOfOldPassword() {
    return "Špatná délka vašeho současného hesla.";
  }
  
  public java.lang.String faqContent_answer7() {
    return "Pravděpodobně nevlastníte typ platební karty, která umožňuje platbu online. Prosím zkontrolujte ve své bance, zda vlastníte správný typ platební karty pro platby online. Zaplatit můžete také převodem na bankovní účet.";
  }
  
  public java.lang.String joinPanel_title() {
    return "Zaregistruj se do Takeplace!";
  }
  
  public java.lang.String organizationDetailForm_form_internationalName() {
    return "Mezinárodní název";
  }
  
  public java.lang.String profileImageContent_button_picture() {
    return "OBRÁZEK";
  }
  
  public java.lang.String profileImageContent_error_profileUpdateFailed() {
    return "Nahrání profilového obrázku se nezdařilo.";
  }
  
  public java.lang.String privateOrgContent_helpText() {
    return "Pokud se registrujete na akci, organizátoři mohou vyžadovat vaši afiliaci. Tato data vám můžeme předvyplnit do registračního formuláře, což vám ušetří čas.";
  }
  
  public java.lang.String faqContent_answer6() {
    return "V detailu Vaší registrace je nabídka na zaplacení přes službu Paypal, postupujte podle instrukcí.";
  }
  
  public java.lang.String faqContent_question9() {
    return "Fakturu potřebuji ještě před zaplacením.";
  }
  
  public java.lang.String activateContent_helpText3() {
    return "Pokud jste neobdrželi aktivační e-mail, kontaktujte prosím podporu Takeplace na adrese suport@takeplace.eu.";
  }
  
  public java.lang.String activateContent_text2() {
    return "Ve schránce s příchozí poštou byste měli najít e-mail s dalšími instrukcemi jak aktivovat účet. Pokud aktivační instrukce neobrdříte během 15 minut, zkontrolujte prosím i svoji spamovou schránku.";
  }
  
  public java.lang.String changePasswordContent_error_invalidCharactersInPassword() {
    return "Heslo obsahuje nepovolené znaky. Můžete použít písmena (malá i velká, bez diakritických znamének), číslice, a některé speciální znaky ' , . _ - = + @ # $ % ^ & * ' ).";
  }
  
  public java.lang.String invitationContent_error_missingToken() {
    return "Špatný nebo chybějící příznak.";
  }
  
  public java.lang.String userOrg_subtitle_currentlyWork() {
    return "Zde právě pracuji";
  }
  
  public java.lang.String profileImageContent_text_profileUpdateSuccessfull() {
    return "Profilový obrázek byl změněn.";
  }
  
  public java.lang.String fillEmailDialog_action_updateEmail() {
    return "Aktualizuj email";
  }
  
  public java.lang.String privateOrgContent_title_personalAddress() {
    return "Osobní adresa";
  }
  
  public java.lang.String activateContent_button_confirmAccount() {
    return "POTVRĎIT ÚČET";
  }
  
  public java.lang.String passwordChangeContent_text_successPasswordChange() {
    return "Heslo bylo změněno.";
  }
  
  public java.lang.String lostPasswordContent_helpText3() {
    return "Mějte své heslo bezpečné a utajené!";
  }
  
  public java.lang.String personalInfo_title() {
    return "Osobní informace";
  }
  
  public java.lang.String faqContent_answer4() {
    return "Proto, aby Vaše konference byla viditelná a přístupná účastníkům, je potřeba nejdříve akci Takeplace zaplatit a aktivovat ji. V případě dotazů se prosím obracejte na <a href=\"mailto:support@takeplace.eu\">support@takeplace.eu</a>";
  }
  
  public java.lang.String passwordChangeContent_button_changePassword() {
    return "ZMĚNIT HESLO";
  }
  
  public java.lang.String faqContent_question1() {
    return "Rád/a bych se zaregistroval/a, jak mám postupovat?";
  }
  
  public java.lang.String loginContent_helpText2() {
    return "Jste nový uživatel? Nejprve se zaregistruje do Takeplace.";
  }
  
  public java.lang.String joinPanel_text() {
    return "Takeplace je služba pro snadné a efektivní organizování profesionálních akcí. Rozjeďte své akce s Takeplace!";
  }
  
  public java.lang.String loginContent_helpText1() {
    return "Zapomněli jste heslo? Zašleme vám nové.";
  }
  
  public java.lang.String loginContent_helpTitle() {
    return "Přihlašte se do Takeplace pomocí kontaktního e-mailu a hesla.";
  }
  
  public java.lang.String aboutYourself_helpText() {
    return "Krátký životopis umožní ostatním uživatelům vás lépe poznat. Mělo by obsahovat v několika větách vaše profesionální i osobní dovednosti, zkušenosti, schopnosti a zájmy.";
  }
  
  public java.lang.String activateContent_helpText1() {
    return "Nyní můžete aktivovat svůj účat pomocí zaslaného klíče.";
  }
  
  public java.lang.String faqContent_answer9() {
    return "Omlouváme se, ale faktura je vydávána až na základě obrdžení platby za Vaši akci. Pro bližší informace prosím kontaktujte oddělení podpory na <a href=\"mailto:support@takeplace.eu\">support@takeplace.eu</a>";
  }
  
  public java.lang.String personInfoContent_form_frontDegree() {
    return "Tituly před";
  }
  
  public java.lang.String loginContent_helpText3() {
    return "Máte aktivovaný účet? Pokud ne, podívejte se do pošty, zda vám došel aktivační e-mail.";
  }
  
  public java.lang.String userOrg_form_timePeriod() {
    return "Období";
  }
  
  public java.lang.String personInfoContent_text_successPersonalInfoUpdate() {
    return "Vaše osobní údaje byly změněny.";
  }
  
  public java.lang.String faqContent_answer2() {
    return "Zkontrolujte prosím ve své emailové schránce spam. Pokud email nedojde do následujícího dne od registrace, kontaktujte nás na <a href=\"mailto:support@takeplace.com\">support@takeplace.eu</a>";
  }
  
  public java.lang.String experience_title() {
    return "Praxe";
  }
  
  public java.lang.String aboutYourself_helpTitle() {
    return "Proč bych měl zveřejnit životopis?";
  }
  
  public java.lang.String loginContent_text_activationSuccessfull() {
    return "Aktivace proběhla úspěšně. Nyní se můžete přihlásit.";
  }
  
  public java.lang.String lostPasswordContent_text2() {
    return "Ve vaší příchozí poště byste měli mít e-mail s instrukcemi jak vytvořit nové heslo: Pokud takový e-mail neobdržíte do 15 minut, podívejte se prosím i do spamové schránky.";
  }
  
  public java.lang.String personalInfo_text_successPersonalInfoUpdate() {
    return "Vaše osobní údaje byly aktualizovány.";
  }
  
  public java.lang.String loginPanel_action_notActivated() {
    return "Nemáte účet aktivní?";
  }
  
  public java.lang.String loginContent_text_lostPasswordChanged() {
    return "Heslo bylo vygenerováno. Prosím přihlašte se.";
  }
  
  public java.lang.String headProfileRow_text_accountActivated() {
    return "Účet byl úspěšně aktivován";
  }
  
  public java.lang.String loginPanel_action_notRegistered() {
    return "Nejste registrován?";
  }
  
  public java.lang.String profileMenuContent_title() {
    return "Osobní profil";
  }
  
  public java.lang.String personInfoContent_form_rearDegree() {
    return "Tituly za";
  }
  
  public java.lang.String passwordChangeContent_error_oldPasswordIsEmpty() {
    return "Musí být zadáno vaše současné heslo.";
  }
  
  public java.lang.String faqContent_answer8() {
    return "Po registraci Vám bude vytvořena proforma faktura, na které najdete všechny potřebné informace k provedení platby převodním příkazem. Před odesláním prosím zkontrolujte správnost variabilního symbolu.";
  }
  
  public java.lang.String loginPanel_action_lostPassword() {
    return "Vygenerovat nové heslo?";
  }
  
  public java.lang.String userOrganizationsContent_error_experienceListFailed() {
    return "Nelze zobrazit seznam zaměstnání.";
  }
  
  public java.lang.String userOrg_error_userOrganizationNotExists() {
    return "Toto zaměstnání nelze vymazat.";
  }
  
  public java.lang.String userOrg_subtitle_pastPositions() {
    return "Předešlá zaměstnání";
  }
  
  public java.lang.String faqContent_answer10() {
    return "Na přihlašovací stránce Takeplace klikněte na odkaz Pošli mi nápovědu, na email Vám budou zaslány další instrukce k vygenerování nového hesla.";
  }
  
  public java.lang.String aboutYourself_text_pleaseEnterBio() {
    return "Zde můžete napsat svůj životopis.";
  }
  
  public java.lang.String userOrg_error_invalidIdOfExperience() {
    return "Toto není správné zaměstnání";
  }
  
  public java.lang.String lostPasswordContent_error_userNotExists() {
    return "Tento uživatel neexistuje.";
  }
  
  public java.lang.String changePasswordContent_title() {
    return "Změnit heslo";
  }
  
  public java.lang.String userOrganizationsContent_text_experienceSucessfullyUpdated() {
    return "Zaměstnání bylo aktualizováno.";
  }
  
  public java.lang.String newUserOrganizationDialog_title() {
    return "Nové zaměstnání";
  }
  
  public java.lang.String userOrg_text_areYouSure() {
    return "Jste si jisti?";
  }
  
  public java.lang.String changePasswordContent_helpTitle() {
    return "Heslo lze změnit jednoduše.";
  }
  
  public java.lang.String changePasswordContent_button_changePassword() {
    return "ZMĚNIT HESLO";
  }
  
  public java.lang.String personInfoContent_form_contactEmail() {
    return "Kontaktní e-mail";
  }
  
  public java.lang.String passwordChangeContent_form_newPassword() {
    return "Nové heslo";
  }
  
  public java.lang.String activateContent_text1() {
    return "Než začnete používat Takeplace, potřebujete svůj účet aktivovat. E-mail s potvrzením byl právě zaslán na vaši e-mailovou adresu.";
  }
  
  public java.lang.String lostPasswordContent_helpText1() {
    return "Pro vygenererování nového hesla zadejte svůj kontaktní e-mail — stejný jakým jste se registrovali — kam vám budou zaslány další instrukce.";
  }
  
  public java.lang.String registerContent_button_registerAccount() {
    return "REGISTRUJ ÚČET";
  }
  
  public java.lang.String faqContent_answer5() {
    return "Za registraci je možné zaplatit online přes služby PayPal, která přijímá platební karty, a nebo zaplatit bankovním převodem na účet Takeplace.";
  }
  
  public java.lang.String userOrganizationsContent_title() {
    return "Kariéra a praxe";
  }
  
  public java.lang.String userOrg_text_at() {
    return "v";
  }
  
  public java.lang.String registerContent_helpText1() {
    return "Pro založení účtu v Takeplace nejprve prosím vyplňte registrační formulář.";
  }
  
  public java.lang.String faqContent_answer3() {
    return "Po přihlášení do systému vyberte v menu sekci Akce a tam záložku všechny akce – zde najdete svou konferenci.";
  }
  
  public java.lang.String changePasswordContent_error_lostPasswordTokenMissing() {
    return "Prosím klikněte na odkaz, který jste obdrželi e-mailem do své příchozí pošty.";
  }
  
  public java.lang.String privateOrgContent_helpTitle() {
    return "K čemu jsou organizace?";
  }
  
  public java.lang.String lostPasswordContent_helpText2() {
    return "Pokud neobdržíte e-mail s instrukcemi, kontaktujte prosím podporu Takeplace na adrese support@takeplece.eu.";
  }
  
  public java.lang.String activateContent_helpText2() {
    return "Váš aktivační klíč je platný po dobu 30 dní. Pokud během této doby svůj účet neaktivujete, budete se muset znovu zaregistrovat.";
  }
  
  public java.lang.String userOrg_form_at() {
    return "V";
  }
  
  public java.lang.String registerContent_helpText2() {
    return "Kontaktní e-mail by měl být trvalý, protože se jím budete v Takeplace identifikovat a bude sloužit jako hlavní komunikační kanál.";
  }
  
  public java.lang.String faqContent_question8() {
    return "Jak postupovat při platbě bankovním převodem?";
  }
  
  public java.lang.String loginPanel_text() {
    return "Jste registrován? Přihlašte se:";
  }
  
  public java.lang.String organizationDetailForm_form_industry() {
    return "Obor";
  }
  
  public java.lang.String activateContent_error_activationFailed() {
    return "Aktivace se nezdařila. Prosím vložte aktivační klíč, který vám byl zaslán na kontaktní e-mail.";
  }
  
  public java.lang.String userOrg_text_presentLower() {
    return "současnost";
  }
  
  public java.lang.String registerContent_helpText3() {
    return "Heslo by mělo být alespoň osm znaků dlouhé. Měli byste se vyhnout vlastním jménům a obvyklým slovům.";
  }
  
  public java.lang.String faqContent_question10() {
    return "Jsem zaregistrován/a a zapomněl/a jsem heslo, jak mám postupovat?";
  }
  
  public java.lang.String activateContent_text3() {
    return "Po obdržení aktivačního e-mailu prosím klikněte na odkaz přímo z vašeho poštovního programu nebo vložte kód, jenž vám byl zaslán, do následujícího políčka:";
  }
  
  public java.lang.String userOrg_form_position() {
    return "Funkce";
  }
  
  public java.lang.String invitationContent_button_registerAccount() {
    return "REGISTRACE";
  }
  
  public java.lang.String aboutYourself_title_bio() {
    return "O tobě";
  }
  
  public java.lang.String changePasswordContent_error_lostPasswordBadToken() {
    return "Tento odkaz není platný. Prosím zkuste své heslo vygenerovat ještě jednou.";
  }
  
  public java.lang.String loginPanel_button_logIn() {
    return "PŘIHLÁŠENÍ";
  }
  
  public java.lang.String lostPasswordContent_text1() {
    return "Prosím zadejte kontaktní e-mail, kterým jste se registroval do Takeplace, a budou vám zaslány další instrukce.";
  }
  
  public java.lang.String invitationContent_title() {
    return "Pozvánka";
  }
  
  public java.lang.String lostPasswordContent_text_lostPasswordSendSuccess() {
    return "Instrukce byly zaslány na váš kontaktní e-mail. Prosím zkontrolujte si doručenou poštu.";
  }
  
  public java.lang.String faqContent_answer1() {
    return "Pokud ještě nejste zaregistrovaní, navštivte odkaz REGISTRACE a vyplňte základní údaje o sobě. Přihlašovacím jménem bude Vaše emailová adresa, na kterou Vám budeme zasílat informační zprávy. Po dokončení registrace na uvedený email obdržíte aktivační odkaz a poté se budete moci přihlásit do konferenčního prostoru Takeplace.";
  }
  
  public java.lang.String changePasswordContent_helpText2() {
    return "Mějte své heslo bezpečné a utajené!";
  }
  
  public java.lang.String activateContent_helpTitle() {
    return "Blahopřejeme k registraci do Takeplace!";
  }
  
  public java.lang.String lostPasswordContent_helpTitle() {
    return "Pokud jste zapomněli heslo do Takeplace, můžeme vám vygenerovat nové.";
  }
}
