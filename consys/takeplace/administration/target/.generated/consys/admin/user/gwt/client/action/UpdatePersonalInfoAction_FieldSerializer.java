package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdatePersonalInfoAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getCeliac(consys.admin.user.gwt.client.action.UpdatePersonalInfoAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.UpdatePersonalInfoAction::celiac;
  }-*/;
  
  private static native void setCeliac(consys.admin.user.gwt.client.action.UpdatePersonalInfoAction instance, boolean value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.UpdatePersonalInfoAction::celiac = value;
  }-*/;
  
  private static native boolean getVegetarian(consys.admin.user.gwt.client.action.UpdatePersonalInfoAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.UpdatePersonalInfoAction::vegetarian;
  }-*/;
  
  private static native void setVegetarian(consys.admin.user.gwt.client.action.UpdatePersonalInfoAction instance, boolean value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.UpdatePersonalInfoAction::vegetarian = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.UpdatePersonalInfoAction instance) throws SerializationException {
    setCeliac(instance, streamReader.readBoolean());
    setVegetarian(instance, streamReader.readBoolean());
    
  }
  
  public static consys.admin.user.gwt.client.action.UpdatePersonalInfoAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.UpdatePersonalInfoAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.UpdatePersonalInfoAction instance) throws SerializationException {
    streamWriter.writeBoolean(getCeliac(instance));
    streamWriter.writeBoolean(getVegetarian(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.UpdatePersonalInfoAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdatePersonalInfoAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.UpdatePersonalInfoAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdatePersonalInfoAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.UpdatePersonalInfoAction)object);
  }
  
}
