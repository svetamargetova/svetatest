package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ConfirmInvitationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientRegistration getRegistraion(consys.admin.user.gwt.client.action.ConfirmInvitationAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.ConfirmInvitationAction::registraion;
  }-*/;
  
  private static native void setRegistraion(consys.admin.user.gwt.client.action.ConfirmInvitationAction instance, consys.common.gwt.shared.bo.ClientRegistration value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.ConfirmInvitationAction::registraion = value;
  }-*/;
  
  private static native java.lang.String getToken(consys.admin.user.gwt.client.action.ConfirmInvitationAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.ConfirmInvitationAction::token;
  }-*/;
  
  private static native void setToken(consys.admin.user.gwt.client.action.ConfirmInvitationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.ConfirmInvitationAction::token = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.ConfirmInvitationAction instance) throws SerializationException {
    setRegistraion(instance, (consys.common.gwt.shared.bo.ClientRegistration) streamReader.readObject());
    setToken(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.ConfirmInvitationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.ConfirmInvitationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.ConfirmInvitationAction instance) throws SerializationException {
    streamWriter.writeObject(getRegistraion(instance));
    streamWriter.writeString(getToken(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.ConfirmInvitationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ConfirmInvitationAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.ConfirmInvitationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.ConfirmInvitationAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.ConfirmInvitationAction)object);
  }
  
}
