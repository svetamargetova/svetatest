package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DeleteUserOrgAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.admin.user.gwt.client.action.DeleteUserOrgAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.DeleteUserOrgAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.user.gwt.client.action.DeleteUserOrgAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.DeleteUserOrgAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.DeleteUserOrgAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.user.gwt.client.action.DeleteUserOrgAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.DeleteUserOrgAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.DeleteUserOrgAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.DeleteUserOrgAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.DeleteUserOrgAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.DeleteUserOrgAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.DeleteUserOrgAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.DeleteUserOrgAction)object);
  }
  
}
