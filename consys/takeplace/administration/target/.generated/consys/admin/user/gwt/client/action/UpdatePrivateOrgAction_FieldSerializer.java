package consys.admin.user.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UpdatePrivateOrgAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.admin.user.gwt.client.bo.ClientUserOrg getUo(consys.admin.user.gwt.client.action.UpdatePrivateOrgAction instance) /*-{
    return instance.@consys.admin.user.gwt.client.action.UpdatePrivateOrgAction::uo;
  }-*/;
  
  private static native void setUo(consys.admin.user.gwt.client.action.UpdatePrivateOrgAction instance, consys.admin.user.gwt.client.bo.ClientUserOrg value) 
  /*-{
    instance.@consys.admin.user.gwt.client.action.UpdatePrivateOrgAction::uo = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.user.gwt.client.action.UpdatePrivateOrgAction instance) throws SerializationException {
    setUo(instance, (consys.admin.user.gwt.client.bo.ClientUserOrg) streamReader.readObject());
    
  }
  
  public static consys.admin.user.gwt.client.action.UpdatePrivateOrgAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.user.gwt.client.action.UpdatePrivateOrgAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.user.gwt.client.action.UpdatePrivateOrgAction instance) throws SerializationException {
    streamWriter.writeObject(getUo(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.user.gwt.client.action.UpdatePrivateOrgAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdatePrivateOrgAction_FieldSerializer.deserialize(reader, (consys.admin.user.gwt.client.action.UpdatePrivateOrgAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.user.gwt.client.action.UpdatePrivateOrgAction_FieldSerializer.serialize(writer, (consys.admin.user.gwt.client.action.UpdatePrivateOrgAction)object);
  }
  
}
