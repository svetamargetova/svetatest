package consys.admin.event.gwt.client.message;

public class AdminEventConstants_cs implements consys.admin.event.gwt.client.message.AdminEventConstants {
  
  public java.lang.String invitationsItem_text_declined() {
    return "Odmítnuto";
  }
  
  public java.lang.String allEventsMenu_title_allEvents() {
    return "Všechny akce";
  }
  
  public java.lang.String myEventsMenu_text_show() {
    return "Zobraz";
  }
  
  public java.lang.String const_addEvent() {
    return "Přidat akci";
  }
  
  public java.lang.String createEventForm_title_createEvent() {
    return "Vytvořit akci";
  }
  
  public java.lang.String myEventsMenu_action_previous() {
    return "Předchozí";
  }
  
  public java.lang.String invitationsMenu_action_inactive() {
    return "Neaktivní";
  }
  
  public java.lang.String invitationDetailDialog_text_invited() {
    return "Vytvořeno";
  }
  
  public java.lang.String invitationDetailDialog_text_state() {
    return "Současný stav";
  }
  
  public java.lang.String myEventsMenu_title_myEvents() {
    return "Mé akce";
  }
  
  public java.lang.String invitationsItem_text_accepted() {
    return "Přijato";
  }
  
  public java.lang.String invitationsItem_text_notResponse() {
    return "Bez odpovědi";
  }
  
  public java.lang.String invitationsMenu_title_invitations() {
    return "Pozvánky";
  }
  
  public java.lang.String createEventPanel_button_addEvent() {
    return "PŘIDEJ AKCI";
  }
  
  public java.lang.String invitationDetailDialog_text_responsed() {
    return "Odpovězeno";
  }
  
  public java.lang.String invitationsItem_text_timeouted() {
    return "Čas vypršel";
  }
  
  public java.lang.String invitationDetailDialog_text_message() {
    return "Zpráva";
  }
  
  public java.lang.String invitationsItem_text_unknownState() {
    return "Neznámý stav";
  }
  
  public java.lang.String myEventsMenu_text_myEventsEmpty() {
    return "Seznam akcí je momentálně prázdný. Registrujte se na existující akci ze seznamu \"Všechny akce\", najděte akci přes vyhledávací políčko nebo založte novou akci kliknutím na tlačítko \"Přidej akci\".";
  }
  
  public java.lang.String invitationsMenu_action_active() {
    return "Aktivní";
  }
  
  public java.lang.String myEventsMenu_action_upcoming() {
    return "Nadcházející";
  }
  
  public java.lang.String invitationsItem_text_state() {
    return "Stav";
  }
  
  public java.lang.String const_events() {
    return "Akce";
  }
  
  public java.lang.String createEventPanel_form_fullName() {
    return "Celý název";
  }
}
