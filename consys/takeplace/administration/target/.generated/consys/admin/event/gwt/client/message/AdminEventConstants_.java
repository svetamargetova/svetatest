package consys.admin.event.gwt.client.message;

public class AdminEventConstants_ implements consys.admin.event.gwt.client.message.AdminEventConstants {
  
  public java.lang.String invitationsItem_text_declined() {
    return "Declined";
  }
  
  public java.lang.String allEventsMenu_title_allEvents() {
    return "All events";
  }
  
  public java.lang.String myEventsMenu_text_show() {
    return "Show";
  }
  
  public java.lang.String const_addEvent() {
    return "Add event";
  }
  
  public java.lang.String createEventForm_title_createEvent() {
    return "Create event";
  }
  
  public java.lang.String myEventsMenu_action_previous() {
    return "Previous";
  }
  
  public java.lang.String invitationsMenu_action_inactive() {
    return "Inactive";
  }
  
  public java.lang.String invitationDetailDialog_text_invited() {
    return "Invited";
  }
  
  public java.lang.String invitationDetailDialog_text_state() {
    return "Current state";
  }
  
  public java.lang.String myEventsMenu_title_myEvents() {
    return "My events";
  }
  
  public java.lang.String invitationsItem_text_accepted() {
    return "Accepted";
  }
  
  public java.lang.String invitationsItem_text_notResponse() {
    return "No response";
  }
  
  public java.lang.String invitationsMenu_title_invitations() {
    return "Invitations";
  }
  
  public java.lang.String createEventPanel_button_addEvent() {
    return "ADD EVENT";
  }
  
  public java.lang.String invitationDetailDialog_text_responsed() {
    return "Responsed";
  }
  
  public java.lang.String invitationsItem_text_timeouted() {
    return "Timeouted";
  }
  
  public java.lang.String invitationDetailDialog_text_message() {
    return "Message";
  }
  
  public java.lang.String invitationsItem_text_unknownState() {
    return "Unknown state";
  }
  
  public java.lang.String myEventsMenu_text_myEventsEmpty() {
    return "This list of events is empty at the moment. You can register into an existing event in the “All events” list, search for an event via the search box, or start a new event by clicking the “Add event” button.";
  }
  
  public java.lang.String invitationsMenu_action_active() {
    return "Active";
  }
  
  public java.lang.String myEventsMenu_action_upcoming() {
    return "Upcoming";
  }
  
  public java.lang.String invitationsItem_text_state() {
    return "State";
  }
  
  public java.lang.String const_events() {
    return "Events";
  }
  
  public java.lang.String createEventPanel_form_fullName() {
    return "Full name";
  }
}
