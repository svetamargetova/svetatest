package consys.admin.event.gwt.client.message;

public class AdminEventMessages_ implements consys.admin.event.gwt.client.message.AdminEventMessages {
  
  public java.lang.String eventsPanel_action_loadNextEvents(int arg0) {
    return new java.lang.StringBuffer().append("Load next ").append(arg0).append(" items").toString();
  }
  
  public java.lang.String createEventPanel_info_manyTimeZones(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("We have found many time zones for country ").append(arg0).append(". Please choose the right one. Time zone may be changed during the action.").toString();
  }
  
  public java.lang.String createEventPanel_info_noTimeZone(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("We're really sorry but we have not found any time zone for selected country ").append(arg0).append(". Please choose time zone for your event. Time zone may be changed during the action.").toString();
  }
  
  public java.lang.String createEventPanel_error_yearBoxError(int arg0,int arg1) {
    return new java.lang.StringBuffer().append("The year of event has to be between ").append(arg0).append(" and ").append(arg1).toString();
  }
}
