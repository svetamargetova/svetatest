package consys.admin.event.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EventListResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getHasMore(consys.admin.event.gwt.client.bo.EventListResult instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventListResult::hasMore;
  }-*/;
  
  private static native void setHasMore(consys.admin.event.gwt.client.bo.EventListResult instance, boolean value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventListResult::hasMore = value;
  }-*/;
  
  private static native java.util.ArrayList getList(consys.admin.event.gwt.client.bo.EventListResult instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventListResult::list;
  }-*/;
  
  private static native void setList(consys.admin.event.gwt.client.bo.EventListResult instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventListResult::list = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.bo.EventListResult instance) throws SerializationException {
    setHasMore(instance, streamReader.readBoolean());
    setList(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.admin.event.gwt.client.bo.EventListResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.bo.EventListResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.bo.EventListResult instance) throws SerializationException {
    streamWriter.writeBoolean(getHasMore(instance));
    streamWriter.writeObject(getList(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.bo.EventListResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventListResult_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.bo.EventListResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventListResult_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.bo.EventListResult)object);
  }
  
}
