package consys.admin.event.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListMyEventInvitationsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getActive(consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ListMyEventInvitationsAction::active;
  }-*/;
  
  private static native void setActive(consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance, boolean value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ListMyEventInvitationsAction::active = value;
  }-*/;
  
  private static native int getFrom(consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ListMyEventInvitationsAction::from;
  }-*/;
  
  private static native void setFrom(consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance, int value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ListMyEventInvitationsAction::from = value;
  }-*/;
  
  private static native int getLimit(consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ListMyEventInvitationsAction::limit;
  }-*/;
  
  private static native void setLimit(consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance, int value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ListMyEventInvitationsAction::limit = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance) throws SerializationException {
    setActive(instance, streamReader.readBoolean());
    setFrom(instance, streamReader.readInt());
    setLimit(instance, streamReader.readInt());
    
  }
  
  public static consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.action.ListMyEventInvitationsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.action.ListMyEventInvitationsAction instance) throws SerializationException {
    streamWriter.writeBoolean(getActive(instance));
    streamWriter.writeInt(getFrom(instance));
    streamWriter.writeInt(getLimit(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.action.ListMyEventInvitationsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.ListMyEventInvitationsAction_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.action.ListMyEventInvitationsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.ListMyEventInvitationsAction_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.action.ListMyEventInvitationsAction)object);
  }
  
}
