package consys.admin.event.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class LoadEventInvitationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getUuid(consys.admin.event.gwt.client.action.LoadEventInvitationAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.LoadEventInvitationAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.event.gwt.client.action.LoadEventInvitationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.LoadEventInvitationAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.action.LoadEventInvitationAction instance) throws SerializationException {
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.event.gwt.client.action.LoadEventInvitationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.action.LoadEventInvitationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.action.LoadEventInvitationAction instance) throws SerializationException {
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.action.LoadEventInvitationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.LoadEventInvitationAction_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.action.LoadEventInvitationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.LoadEventInvitationAction_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.action.LoadEventInvitationAction)object);
  }
  
}
