package consys.admin.event.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EnterEventAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEventUuid(consys.admin.event.gwt.client.action.EnterEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.EnterEventAction::eventUuid;
  }-*/;
  
  private static native void setEventUuid(consys.admin.event.gwt.client.action.EnterEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.EnterEventAction::eventUuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.action.EnterEventAction instance) throws SerializationException {
    setEventUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.event.gwt.client.action.EnterEventAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.action.EnterEventAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.action.EnterEventAction instance) throws SerializationException {
    streamWriter.writeString(getEventUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.action.EnterEventAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.EnterEventAction_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.action.EnterEventAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.EnterEventAction_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.action.EnterEventAction)object);
  }
  
}
