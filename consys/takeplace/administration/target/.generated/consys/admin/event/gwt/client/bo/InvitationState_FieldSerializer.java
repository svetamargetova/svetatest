package consys.admin.event.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class InvitationState_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.bo.InvitationState instance) throws SerializationException {
    // Enum deserialization is handled via the instantiate method
  }
  
  public static consys.admin.event.gwt.client.bo.InvitationState instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int ordinal = streamReader.readInt();
    consys.admin.event.gwt.client.bo.InvitationState[] values = consys.admin.event.gwt.client.bo.InvitationState.values();
    assert (ordinal >= 0 && ordinal < values.length);
    return values[ordinal];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.bo.InvitationState instance) throws SerializationException {
    assert (instance != null);
    streamWriter.writeInt(instance.ordinal());
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.bo.InvitationState_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.InvitationState_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.bo.InvitationState)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.InvitationState_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.bo.InvitationState)object);
  }
  
}
