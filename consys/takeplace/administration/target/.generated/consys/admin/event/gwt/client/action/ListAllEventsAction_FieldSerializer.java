package consys.admin.event.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ListAllEventsAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native int getFrom(consys.admin.event.gwt.client.action.ListAllEventsAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::from;
  }-*/;
  
  private static native void setFrom(consys.admin.event.gwt.client.action.ListAllEventsAction instance, int value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::from = value;
  }-*/;
  
  private static native int getLimit(consys.admin.event.gwt.client.action.ListAllEventsAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::limit;
  }-*/;
  
  private static native void setLimit(consys.admin.event.gwt.client.action.ListAllEventsAction instance, int value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::limit = value;
  }-*/;
  
  private static native java.lang.String getPrefix(consys.admin.event.gwt.client.action.ListAllEventsAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::prefix;
  }-*/;
  
  private static native void setPrefix(consys.admin.event.gwt.client.action.ListAllEventsAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::prefix = value;
  }-*/;
  
  private static native boolean getUpcoming(consys.admin.event.gwt.client.action.ListAllEventsAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::upcoming;
  }-*/;
  
  private static native void setUpcoming(consys.admin.event.gwt.client.action.ListAllEventsAction instance, boolean value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ListAllEventsAction::upcoming = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.action.ListAllEventsAction instance) throws SerializationException {
    setFrom(instance, streamReader.readInt());
    setLimit(instance, streamReader.readInt());
    setPrefix(instance, streamReader.readString());
    setUpcoming(instance, streamReader.readBoolean());
    
  }
  
  public static consys.admin.event.gwt.client.action.ListAllEventsAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.action.ListAllEventsAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.action.ListAllEventsAction instance) throws SerializationException {
    streamWriter.writeInt(getFrom(instance));
    streamWriter.writeInt(getLimit(instance));
    streamWriter.writeString(getPrefix(instance));
    streamWriter.writeBoolean(getUpcoming(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.action.ListAllEventsAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.ListAllEventsAction_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.action.ListAllEventsAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.ListAllEventsAction_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.action.ListAllEventsAction)object);
  }
  
}
