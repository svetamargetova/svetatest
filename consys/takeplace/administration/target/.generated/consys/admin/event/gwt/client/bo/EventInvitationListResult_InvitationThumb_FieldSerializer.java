package consys.admin.event.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EventInvitationListResult_InvitationThumb_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientEventThumb getEvent(consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb::event;
  }-*/;
  
  private static native void setEvent(consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance, consys.common.gwt.shared.bo.ClientEventThumb value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb::event = value;
  }-*/;
  
  private static native consys.admin.event.gwt.client.bo.InvitationState getState(consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb::state;
  }-*/;
  
  private static native void setState(consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance, consys.admin.event.gwt.client.bo.InvitationState value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb::state = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult$InvitationThumb::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance) throws SerializationException {
    setEvent(instance, (consys.common.gwt.shared.bo.ClientEventThumb) streamReader.readObject());
    setState(instance, (consys.admin.event.gwt.client.bo.InvitationState) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb instance) throws SerializationException {
    streamWriter.writeObject(getEvent(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.bo.EventInvitationListResult_InvitationThumb_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventInvitationListResult_InvitationThumb_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventInvitationListResult_InvitationThumb_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.bo.EventInvitationListResult.InvitationThumb)object);
  }
  
}
