package consys.admin.event.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EventInvitationListResult_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.ArrayList getInvitations(consys.admin.event.gwt.client.bo.EventInvitationListResult instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult::invitations;
  }-*/;
  
  private static native void setInvitations(consys.admin.event.gwt.client.bo.EventInvitationListResult instance, java.util.ArrayList value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitationListResult::invitations = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.bo.EventInvitationListResult instance) throws SerializationException {
    setInvitations(instance, (java.util.ArrayList) streamReader.readObject());
    
  }
  
  public static consys.admin.event.gwt.client.bo.EventInvitationListResult instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.bo.EventInvitationListResult();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.bo.EventInvitationListResult instance) throws SerializationException {
    streamWriter.writeObject(getInvitations(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.bo.EventInvitationListResult_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventInvitationListResult_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.bo.EventInvitationListResult)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventInvitationListResult_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.bo.EventInvitationListResult)object);
  }
  
}
