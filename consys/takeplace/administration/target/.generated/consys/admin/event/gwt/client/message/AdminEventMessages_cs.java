package consys.admin.event.gwt.client.message;

public class AdminEventMessages_cs implements consys.admin.event.gwt.client.message.AdminEventMessages {
  
  public java.lang.String eventsPanel_action_loadNextEvents(int arg0) {
    return new java.lang.StringBuffer().append("Načíst dalších ").append(arg0).append(" položek").toString();
  }
  
  public java.lang.String createEventPanel_info_manyTimeZones(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Bohužel jsme pro území ").append(arg0).append(" našli několik časových zón. Prosím vyberte časovou zónu pro Vaši akci. Časovou zónu můžete změnit kdykoliv během akce.").toString();
  }
  
  public java.lang.String createEventPanel_info_noTimeZone(java.lang.String arg0) {
    return new java.lang.StringBuffer().append("Bohužel jsme nenašli žádnou časovou zónu pro území ").append(arg0).append(". Prosím vyberte časovou zónu pro Vaši akci. Časovou zónu můžete změnit kdykoliv během akce.").toString();
  }
  
  public java.lang.String createEventPanel_error_yearBoxError(int arg0,int arg1) {
    return new java.lang.StringBuffer().append("Rok konání akce musí být mezi ").append(arg0).append(" a ").append(arg1).toString();
  }
}
