package consys.admin.event.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class AdminEventResources_cs_InlineClientBundleGenerator implements consys.admin.event.gwt.client.utils.AdminEventResources {
  private static AdminEventResources_cs_InlineClientBundleGenerator _instance0 = new AdminEventResources_cs_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new consys.admin.event.gwt.client.utils.AdminEventCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "css";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GCOOUJ4DKG{margin:" + ("3px"+ " " +"0"+ " " +"10px"+ " " +"10px")  + ";font-weight:" + ("bold")  + ";float:" + ("right")  + ";}.GCOOUJ4DLG{margin:" + ("3px"+ " " +"5px"+ " " +"0"+ " " +"5px")  + ";float:" + ("right")  + ";}")) : ((".GCOOUJ4DKG{margin:" + ("3px"+ " " +"10px"+ " " +"10px"+ " " +"0")  + ";font-weight:" + ("bold")  + ";float:" + ("left")  + ";}.GCOOUJ4DLG{margin:" + ("3px"+ " " +"5px"+ " " +"0"+ " " +"5px")  + ";float:" + ("left")  + ";}"));
      }
      public java.lang.String show(){
        return "GCOOUJ4DKG";
      }
      public java.lang.String showSeparator(){
        return "GCOOUJ4DLG";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static consys.admin.event.gwt.client.utils.AdminEventCss get() {
      return css;
    }
  }
  public consys.admin.event.gwt.client.utils.AdminEventCss css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static consys.admin.event.gwt.client.utils.AdminEventCss css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@consys.admin.event.gwt.client.utils.AdminEventResources::css()();
    }
    return null;
  }-*/;
}
