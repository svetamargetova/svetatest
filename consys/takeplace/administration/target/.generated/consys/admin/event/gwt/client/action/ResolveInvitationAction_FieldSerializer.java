package consys.admin.event.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ResolveInvitationAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getAccept(consys.admin.event.gwt.client.action.ResolveInvitationAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ResolveInvitationAction::accept;
  }-*/;
  
  private static native void setAccept(consys.admin.event.gwt.client.action.ResolveInvitationAction instance, boolean value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ResolveInvitationAction::accept = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.admin.event.gwt.client.action.ResolveInvitationAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.ResolveInvitationAction::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.event.gwt.client.action.ResolveInvitationAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.ResolveInvitationAction::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.action.ResolveInvitationAction instance) throws SerializationException {
    setAccept(instance, streamReader.readBoolean());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.event.gwt.client.action.ResolveInvitationAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.action.ResolveInvitationAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.action.ResolveInvitationAction instance) throws SerializationException {
    streamWriter.writeBoolean(getAccept(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.action.ResolveInvitationAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.ResolveInvitationAction_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.action.ResolveInvitationAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.ResolveInvitationAction_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.action.ResolveInvitationAction)object);
  }
  
}
