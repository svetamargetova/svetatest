package consys.admin.event.gwt.client.action;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CreateEventAction_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getAcronym(consys.admin.event.gwt.client.action.CreateEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.CreateEventAction::acronym;
  }-*/;
  
  private static native void setAcronym(consys.admin.event.gwt.client.action.CreateEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.CreateEventAction::acronym = value;
  }-*/;
  
  private static native java.lang.Integer getCountryId(consys.admin.event.gwt.client.action.CreateEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.CreateEventAction::countryId;
  }-*/;
  
  private static native void setCountryId(consys.admin.event.gwt.client.action.CreateEventAction instance, java.lang.Integer value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.CreateEventAction::countryId = value;
  }-*/;
  
  private static native java.lang.String getFullname(consys.admin.event.gwt.client.action.CreateEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.CreateEventAction::fullname;
  }-*/;
  
  private static native void setFullname(consys.admin.event.gwt.client.action.CreateEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.CreateEventAction::fullname = value;
  }-*/;
  
  private static native java.lang.String getPrefix(consys.admin.event.gwt.client.action.CreateEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.CreateEventAction::prefix;
  }-*/;
  
  private static native void setPrefix(consys.admin.event.gwt.client.action.CreateEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.CreateEventAction::prefix = value;
  }-*/;
  
  private static native java.lang.String getTimeZone(consys.admin.event.gwt.client.action.CreateEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.CreateEventAction::timeZone;
  }-*/;
  
  private static native void setTimeZone(consys.admin.event.gwt.client.action.CreateEventAction instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.CreateEventAction::timeZone = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientEventType getType(consys.admin.event.gwt.client.action.CreateEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.CreateEventAction::type;
  }-*/;
  
  private static native void setType(consys.admin.event.gwt.client.action.CreateEventAction instance, consys.common.gwt.shared.bo.ClientEventType value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.CreateEventAction::type = value;
  }-*/;
  
  private static native int getYear(consys.admin.event.gwt.client.action.CreateEventAction instance) /*-{
    return instance.@consys.admin.event.gwt.client.action.CreateEventAction::year;
  }-*/;
  
  private static native void setYear(consys.admin.event.gwt.client.action.CreateEventAction instance, int value) 
  /*-{
    instance.@consys.admin.event.gwt.client.action.CreateEventAction::year = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.action.CreateEventAction instance) throws SerializationException {
    setAcronym(instance, streamReader.readString());
    setCountryId(instance, (java.lang.Integer) streamReader.readObject());
    setFullname(instance, streamReader.readString());
    setPrefix(instance, streamReader.readString());
    setTimeZone(instance, streamReader.readString());
    setType(instance, (consys.common.gwt.shared.bo.ClientEventType) streamReader.readObject());
    setYear(instance, streamReader.readInt());
    
  }
  
  public static consys.admin.event.gwt.client.action.CreateEventAction instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.action.CreateEventAction();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.action.CreateEventAction instance) throws SerializationException {
    streamWriter.writeString(getAcronym(instance));
    streamWriter.writeObject(getCountryId(instance));
    streamWriter.writeString(getFullname(instance));
    streamWriter.writeString(getPrefix(instance));
    streamWriter.writeString(getTimeZone(instance));
    streamWriter.writeObject(getType(instance));
    streamWriter.writeInt(getYear(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.action.CreateEventAction_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.CreateEventAction_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.action.CreateEventAction)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.action.CreateEventAction_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.action.CreateEventAction)object);
  }
  
}
