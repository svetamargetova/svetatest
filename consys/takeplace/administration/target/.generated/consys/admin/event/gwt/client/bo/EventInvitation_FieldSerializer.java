package consys.admin.event.gwt.client.bo;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class EventInvitation_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native consys.common.gwt.shared.bo.ClientEvent getEvent(consys.admin.event.gwt.client.bo.EventInvitation instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitation::event;
  }-*/;
  
  private static native void setEvent(consys.admin.event.gwt.client.bo.EventInvitation instance, consys.common.gwt.shared.bo.ClientEvent value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitation::event = value;
  }-*/;
  
  private static native consys.common.gwt.shared.bo.ClientUser getFrom(consys.admin.event.gwt.client.bo.EventInvitation instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitation::from;
  }-*/;
  
  private static native void setFrom(consys.admin.event.gwt.client.bo.EventInvitation instance, consys.common.gwt.shared.bo.ClientUser value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitation::from = value;
  }-*/;
  
  private static native java.util.Date getInvitationDate(consys.admin.event.gwt.client.bo.EventInvitation instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitation::invitationDate;
  }-*/;
  
  private static native void setInvitationDate(consys.admin.event.gwt.client.bo.EventInvitation instance, java.util.Date value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitation::invitationDate = value;
  }-*/;
  
  private static native java.lang.String getMessage(consys.admin.event.gwt.client.bo.EventInvitation instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitation::message;
  }-*/;
  
  private static native void setMessage(consys.admin.event.gwt.client.bo.EventInvitation instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitation::message = value;
  }-*/;
  
  private static native java.util.Date getReponseDate(consys.admin.event.gwt.client.bo.EventInvitation instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitation::reponseDate;
  }-*/;
  
  private static native void setReponseDate(consys.admin.event.gwt.client.bo.EventInvitation instance, java.util.Date value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitation::reponseDate = value;
  }-*/;
  
  private static native consys.admin.event.gwt.client.bo.InvitationState getState(consys.admin.event.gwt.client.bo.EventInvitation instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitation::state;
  }-*/;
  
  private static native void setState(consys.admin.event.gwt.client.bo.EventInvitation instance, consys.admin.event.gwt.client.bo.InvitationState value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitation::state = value;
  }-*/;
  
  private static native java.lang.String getUuid(consys.admin.event.gwt.client.bo.EventInvitation instance) /*-{
    return instance.@consys.admin.event.gwt.client.bo.EventInvitation::uuid;
  }-*/;
  
  private static native void setUuid(consys.admin.event.gwt.client.bo.EventInvitation instance, java.lang.String value) 
  /*-{
    instance.@consys.admin.event.gwt.client.bo.EventInvitation::uuid = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, consys.admin.event.gwt.client.bo.EventInvitation instance) throws SerializationException {
    setEvent(instance, (consys.common.gwt.shared.bo.ClientEvent) streamReader.readObject());
    setFrom(instance, (consys.common.gwt.shared.bo.ClientUser) streamReader.readObject());
    setInvitationDate(instance, (java.util.Date) streamReader.readObject());
    setMessage(instance, streamReader.readString());
    setReponseDate(instance, (java.util.Date) streamReader.readObject());
    setState(instance, (consys.admin.event.gwt.client.bo.InvitationState) streamReader.readObject());
    setUuid(instance, streamReader.readString());
    
  }
  
  public static consys.admin.event.gwt.client.bo.EventInvitation instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new consys.admin.event.gwt.client.bo.EventInvitation();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, consys.admin.event.gwt.client.bo.EventInvitation instance) throws SerializationException {
    streamWriter.writeObject(getEvent(instance));
    streamWriter.writeObject(getFrom(instance));
    streamWriter.writeObject(getInvitationDate(instance));
    streamWriter.writeString(getMessage(instance));
    streamWriter.writeObject(getReponseDate(instance));
    streamWriter.writeObject(getState(instance));
    streamWriter.writeString(getUuid(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return consys.admin.event.gwt.client.bo.EventInvitation_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventInvitation_FieldSerializer.deserialize(reader, (consys.admin.event.gwt.client.bo.EventInvitation)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    consys.admin.event.gwt.client.bo.EventInvitation_FieldSerializer.serialize(writer, (consys.admin.event.gwt.client.bo.EventInvitation)object);
  }
  
}
