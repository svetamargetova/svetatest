package com.google.gwt.i18n.client;

import com.google.gwt.i18n.client.impl.CurrencyDataImpl;
import com.google.gwt.core.client.JavaScriptObject;
import java.util.HashMap;

public class CurrencyList_cs extends com.google.gwt.i18n.client.CurrencyList_ {
  
  @Override
  protected CurrencyData getDefaultJava() {
    return new CurrencyDataImpl("CZK", "Kč", 2, "Kč");
  }
  
  @Override
  protected native CurrencyData getDefaultNative() /*-{
    return [ "CZK", "Kč", 2, "Kč"];
  }-*/;
  
  @Override
  protected HashMap<String, CurrencyData> loadCurrencyMapJava() {
    HashMap<String, CurrencyData> result = super.loadCurrencyMapJava();
    // Peseta andorrská
    result.put("ADP", new CurrencyDataImpl("ADP", "ADP", 128));
    // Dirham SAE
    result.put("AED", new CurrencyDataImpl("AED", "DH", 2, "DH"));
    // Afghán (1927-2002)
    result.put("AFA", new CurrencyDataImpl("AFA", "AFA", 130));
    // Afghán
    result.put("AFN", new CurrencyDataImpl("AFN", "Af", 0));
    // Lek
    result.put("ALL", new CurrencyDataImpl("ALL", "ALL", 0));
    // Dram arménský
    result.put("AMD", new CurrencyDataImpl("AMD", "AMD", 0));
    // Zlatý Nizozemských Antil
    result.put("ANG", new CurrencyDataImpl("ANG", "NAf.", 2));
    // Kwanza
    result.put("AOA", new CurrencyDataImpl("AOA", "Kz", 2));
    // Kwanza (1977-1990)
    result.put("AOK", new CurrencyDataImpl("AOK", "AOK", 130));
    // Kwanza nová (1990-2000)
    result.put("AON", new CurrencyDataImpl("AON", "AON", 130));
    // Kwanza reajustado (1995-1999)
    result.put("AOR", new CurrencyDataImpl("AOR", "AOR", 130));
    // Austral
    result.put("ARA", new CurrencyDataImpl("ARA", "₳", 130));
    // ARL
    result.put("ARL", new CurrencyDataImpl("ARL", "$L", 130));
    // ARM
    result.put("ARM", new CurrencyDataImpl("ARM", "m$n", 130));
    // Peso argentinské (1983-1985)
    result.put("ARP", new CurrencyDataImpl("ARP", "ARP", 130));
    // Peso argentinské
    result.put("ARS", new CurrencyDataImpl("ARS", "AR$", 2, "AR$"));
    // Šilink
    result.put("ATS", new CurrencyDataImpl("ATS", "ATS", 130));
    // Dolar australský
    result.put("AUD", new CurrencyDataImpl("AUD", "AU$", 2, "AU$"));
    // Zlatý arubský
    result.put("AWG", new CurrencyDataImpl("AWG", "Afl.", 2));
    // Manat ázerbajdžánský
    result.put("AZM", new CurrencyDataImpl("AZM", "AZM", 130));
    // AZN
    result.put("AZN", new CurrencyDataImpl("AZN", "man.", 2));
    // Dinár Bosny a Hercegoviny
    result.put("BAD", new CurrencyDataImpl("BAD", "BAD", 130));
    // Marka konvertibilní
    result.put("BAM", new CurrencyDataImpl("BAM", "KM", 2));
    // Dolar barbadoský
    result.put("BBD", new CurrencyDataImpl("BBD", "Bds$", 2));
    // Taka
    result.put("BDT", new CurrencyDataImpl("BDT", "Tk", 2, "Tk"));
    // Frank konvertibilní belgický
    result.put("BEC", new CurrencyDataImpl("BEC", "BEC", 2));
    // Frank belgický
    result.put("BEF", new CurrencyDataImpl("BEF", "BF", 130));
    // Frank finanční belgický
    result.put("BEL", new CurrencyDataImpl("BEL", "BEL", 2));
    // Lev
    result.put("BGL", new CurrencyDataImpl("BGL", "BGL", 130));
    // Lev Bulharský
    result.put("BGN", new CurrencyDataImpl("BGN", "BGN", 2));
    // Dinár bahrajnský
    result.put("BHD", new CurrencyDataImpl("BHD", "BD", 3));
    // Frank burundský
    result.put("BIF", new CurrencyDataImpl("BIF", "FBu", 0));
    // Dolar bermudský
    result.put("BMD", new CurrencyDataImpl("BMD", "BD$", 2));
    // Dolar brunejský
    result.put("BND", new CurrencyDataImpl("BND", "BN$", 2));
    // Boliviano
    result.put("BOB", new CurrencyDataImpl("BOB", "Bs", 2));
    // Peso
    result.put("BOP", new CurrencyDataImpl("BOP", "$b.", 130));
    // Mvdol
    result.put("BOV", new CurrencyDataImpl("BOV", "BOV", 2));
    // Cruzeiro (1967-1986)
    result.put("BRB", new CurrencyDataImpl("BRB", "BRB", 130));
    // Cruzado
    result.put("BRC", new CurrencyDataImpl("BRC", "BRC", 130));
    // Cruzeiro (1990-1993)
    result.put("BRE", new CurrencyDataImpl("BRE", "BRE", 130));
    // Real brazilský
    result.put("BRL", new CurrencyDataImpl("BRL", "R$", 2, "R$"));
    // Cruzado nové
    result.put("BRN", new CurrencyDataImpl("BRN", "BRN", 130));
    // Cruzeiro real
    result.put("BRR", new CurrencyDataImpl("BRR", "BRR", 130));
    // Dolar bahamský
    result.put("BSD", new CurrencyDataImpl("BSD", "BS$", 2));
    // Ngultrum
    result.put("BTN", new CurrencyDataImpl("BTN", "Nu.", 2));
    // Kyat barmský
    result.put("BUK", new CurrencyDataImpl("BUK", "BUK", 130));
    // Pula
    result.put("BWP", new CurrencyDataImpl("BWP", "BWP", 2));
    // Rubl nový běloruský (1994-1999)
    result.put("BYB", new CurrencyDataImpl("BYB", "BYB", 130));
    // Rubl běloruský
    result.put("BYR", new CurrencyDataImpl("BYR", "BYR", 0));
    // Dolar belizský
    result.put("BZD", new CurrencyDataImpl("BZD", "BZ$", 2));
    // Dolar kanadský
    result.put("CAD", new CurrencyDataImpl("CAD", "CA$", 2, "C$"));
    // Frank konžský
    result.put("CDF", new CurrencyDataImpl("CDF", "CDF", 2));
    // CHE
    result.put("CHE", new CurrencyDataImpl("CHE", "CHE", 2));
    // Frank švýcarský
    result.put("CHF", new CurrencyDataImpl("CHF", "Fr.", 2, "CHF"));
    // CHW
    result.put("CHW", new CurrencyDataImpl("CHW", "CHW", 2));
    // CLE
    result.put("CLE", new CurrencyDataImpl("CLE", "Eº", 130));
    // Unidades de fomento
    result.put("CLF", new CurrencyDataImpl("CLF", "CLF", 0));
    // Peso chilské
    result.put("CLP", new CurrencyDataImpl("CLP", "CL$", 0, "CL$"));
    // Juan renminbi
    result.put("CNY", new CurrencyDataImpl("CNY", "CN¥", 2, "RMB¥"));
    // Peso kolumbijské
    result.put("COP", new CurrencyDataImpl("COP", "CO$", 0, "COL$"));
    // COU
    result.put("COU", new CurrencyDataImpl("COU", "COU", 2));
    // Colón kostarický
    result.put("CRC", new CurrencyDataImpl("CRC", "₡", 0, "CR₡"));
    // CSD
    result.put("CSD", new CurrencyDataImpl("CSD", "CSD", 130));
    // Koruna československá
    result.put("CSK", new CurrencyDataImpl("CSK", "CSK", 130));
    // CUC
    result.put("CUC", new CurrencyDataImpl("CUC", "CUC$", 2));
    // Peso kubánské
    result.put("CUP", new CurrencyDataImpl("CUP", "CU$", 2, "$MN"));
    // Escudo kapverdské
    result.put("CVE", new CurrencyDataImpl("CVE", "CV$", 2));
    // Libra kyperská
    result.put("CYP", new CurrencyDataImpl("CYP", "CY£", 130));
    // Koruna česká
    result.put("CZK", new CurrencyDataImpl("CZK", "Kč", 2, "Kč"));
    // Marka NDR
    result.put("DDM", new CurrencyDataImpl("DDM", "DDM", 130));
    // Marka německá
    result.put("DEM", new CurrencyDataImpl("DEM", "DM", 130));
    // Frank džibutský
    result.put("DJF", new CurrencyDataImpl("DJF", "Fdj", 0));
    // Koruna dánská
    result.put("DKK", new CurrencyDataImpl("DKK", "Dkr", 2, "kr"));
    // Peso dominikánské
    result.put("DOP", new CurrencyDataImpl("DOP", "RD$", 2, "RD$"));
    // Dinár alžírský
    result.put("DZD", new CurrencyDataImpl("DZD", "DA", 2));
    // Sucre ekvádorský
    result.put("ECS", new CurrencyDataImpl("ECS", "ECS", 130));
    // Ecuador Unidad de Valor Constante (UVC)
    result.put("ECV", new CurrencyDataImpl("ECV", "ECV", 2));
    // Kroon
    result.put("EEK", new CurrencyDataImpl("EEK", "Ekr", 2));
    // Libra egyptská
    result.put("EGP", new CurrencyDataImpl("EGP", "EG£", 2, "LE"));
    // EQE
    result.put("EQE", new CurrencyDataImpl("EQE", "EQE", 130));
    // Nakfa
    result.put("ERN", new CurrencyDataImpl("ERN", "Nfk", 2));
    // Peseta španělská („A“ účet)
    result.put("ESA", new CurrencyDataImpl("ESA", "ESA", 2));
    // Konvertibilní pesetové účty
    result.put("ESB", new CurrencyDataImpl("ESB", "ESB", 2));
    // Peseta španělská
    result.put("ESP", new CurrencyDataImpl("ESP", "Pts", 128));
    // Birr etiopský
    result.put("ETB", new CurrencyDataImpl("ETB", "Br", 2));
    // Euro
    result.put("EUR", new CurrencyDataImpl("EUR", "€", 2, "€"));
    // Markka
    result.put("FIM", new CurrencyDataImpl("FIM", "mk", 130));
    // Dolar fidžijský
    result.put("FJD", new CurrencyDataImpl("FJD", "FJ$", 2));
    // Libra falklandská
    result.put("FKP", new CurrencyDataImpl("FKP", "FK£", 2));
    // Frank francouzský
    result.put("FRF", new CurrencyDataImpl("FRF", "₣", 130));
    // Libra šterlinků
    result.put("GBP", new CurrencyDataImpl("GBP", "£", 2, "GB£"));
    // Georgian Kupon Larit
    result.put("GEK", new CurrencyDataImpl("GEK", "GEK", 130));
    // Lari
    result.put("GEL", new CurrencyDataImpl("GEL", "GEL", 2));
    // Cedi
    result.put("GHC", new CurrencyDataImpl("GHC", "₵", 130));
    // GHS
    result.put("GHS", new CurrencyDataImpl("GHS", "GH₵", 2));
    // Libra gibraltarská
    result.put("GIP", new CurrencyDataImpl("GIP", "GI£", 2));
    // Dalasi
    result.put("GMD", new CurrencyDataImpl("GMD", "GMD", 2));
    // Frank guinejský
    result.put("GNF", new CurrencyDataImpl("GNF", "FG", 0));
    // Guinea Syli
    result.put("GNS", new CurrencyDataImpl("GNS", "GNS", 130));
    // Equatorial Guinea Ekwele Guineana
    result.put("GQE", new CurrencyDataImpl("GQE", "GQE", 130));
    // Drachma
    result.put("GRD", new CurrencyDataImpl("GRD", "₯", 130));
    // Quetzal
    result.put("GTQ", new CurrencyDataImpl("GTQ", "GTQ", 2));
    // Escudo guinejské
    result.put("GWE", new CurrencyDataImpl("GWE", "GWE", 130));
    // Peso Guinnea-Bissau
    result.put("GWP", new CurrencyDataImpl("GWP", "GWP", 2));
    // Dolar guyanský
    result.put("GYD", new CurrencyDataImpl("GYD", "GY$", 0));
    // Dolar hongkongský
    result.put("HKD", new CurrencyDataImpl("HKD", "HK$", 2, "HK$"));
    // Lempira
    result.put("HNL", new CurrencyDataImpl("HNL", "HNL", 2));
    // Dinar chorvatský
    result.put("HRD", new CurrencyDataImpl("HRD", "HRD", 130));
    // Kuna chorvatská
    result.put("HRK", new CurrencyDataImpl("HRK", "kn", 2));
    // Gourde
    result.put("HTG", new CurrencyDataImpl("HTG", "HTG", 2));
    // Forint
    result.put("HUF", new CurrencyDataImpl("HUF", "Ft", 0));
    // Rupie indonézská
    result.put("IDR", new CurrencyDataImpl("IDR", "Rp", 0));
    // Libra irská
    result.put("IEP", new CurrencyDataImpl("IEP", "IR£", 130));
    // Libra izraelská
    result.put("ILP", new CurrencyDataImpl("ILP", "I£", 130));
    // Šekel nový izraelský
    result.put("ILS", new CurrencyDataImpl("ILS", "₪", 2, "IL₪"));
    // Rupie indická
    result.put("INR", new CurrencyDataImpl("INR", "Rs", 2, "Rs"));
    // Dinár irácký
    result.put("IQD", new CurrencyDataImpl("IQD", "IQD", 0));
    // Rijál íránský
    result.put("IRR", new CurrencyDataImpl("IRR", "IRR", 0));
    // Koruna islandská
    result.put("ISK", new CurrencyDataImpl("ISK", "Ikr", 0, "kr"));
    // Lira italská
    result.put("ITL", new CurrencyDataImpl("ITL", "IT₤", 128));
    // Dolar jamajský
    result.put("JMD", new CurrencyDataImpl("JMD", "J$", 2, "JA$"));
    // Dinár jordánský
    result.put("JOD", new CurrencyDataImpl("JOD", "JD", 3));
    // Jen
    result.put("JPY", new CurrencyDataImpl("JPY", "JP¥", 0, "JP¥"));
    // Šilink keňský
    result.put("KES", new CurrencyDataImpl("KES", "Ksh", 2));
    // Som
    result.put("KGS", new CurrencyDataImpl("KGS", "KGS", 2));
    // Riel
    result.put("KHR", new CurrencyDataImpl("KHR", "KHR", 2));
    // Frank komorský
    result.put("KMF", new CurrencyDataImpl("KMF", "CF", 0));
    // Won severokorejský
    result.put("KPW", new CurrencyDataImpl("KPW", "KPW", 0));
    // Won jihokorejský
    result.put("KRW", new CurrencyDataImpl("KRW", "₩", 0, "KR₩"));
    // Dinár kuvajtský
    result.put("KWD", new CurrencyDataImpl("KWD", "KD", 3));
    // Dolar Kajmanských ostrovů
    result.put("KYD", new CurrencyDataImpl("KYD", "KY$", 2));
    // Tenge
    result.put("KZT", new CurrencyDataImpl("KZT", "KZT", 2));
    // Kip
    result.put("LAK", new CurrencyDataImpl("LAK", "₭", 0));
    // Libra libanonská
    result.put("LBP", new CurrencyDataImpl("LBP", "LB£", 0));
    // Rupie srílanská
    result.put("LKR", new CurrencyDataImpl("LKR", "SLRs", 2, "SLRs"));
    // Dolar liberijský
    result.put("LRD", new CurrencyDataImpl("LRD", "L$", 2));
    // Loti
    result.put("LSL", new CurrencyDataImpl("LSL", "LSL", 2));
    // LSM
    result.put("LSM", new CurrencyDataImpl("LSM", "LSM", 130));
    // Litus litevský
    result.put("LTL", new CurrencyDataImpl("LTL", "Lt", 2));
    // Talon
    result.put("LTT", new CurrencyDataImpl("LTT", "LTT", 130));
    // Frank konvertibilní lucemburský
    result.put("LUC", new CurrencyDataImpl("LUC", "LUC", 2));
    // Frank lucemburský
    result.put("LUF", new CurrencyDataImpl("LUF", "LUF", 128));
    // Frank finanční lucemburský
    result.put("LUL", new CurrencyDataImpl("LUL", "LUL", 2));
    // Lat lotyšský
    result.put("LVL", new CurrencyDataImpl("LVL", "Ls", 2));
    // Rubl lotyšský
    result.put("LVR", new CurrencyDataImpl("LVR", "LVR", 130));
    // Dinár lybijský
    result.put("LYD", new CurrencyDataImpl("LYD", "LD", 3));
    // Dirham marocký
    result.put("MAD", new CurrencyDataImpl("MAD", "MAD", 2));
    // Frank marocký
    result.put("MAF", new CurrencyDataImpl("MAF", "MAF", 130));
    // Leu moldavský
    result.put("MDL", new CurrencyDataImpl("MDL", "MDL", 2));
    // Ariary madagaskarský
    result.put("MGA", new CurrencyDataImpl("MGA", "MGA", 0));
    // Frank madagaskarský
    result.put("MGF", new CurrencyDataImpl("MGF", "MGF", 128));
    // Denár
    result.put("MKD", new CurrencyDataImpl("MKD", "MKD", 2));
    // Frank malijský
    result.put("MLF", new CurrencyDataImpl("MLF", "MLF", 130));
    // Kyat
    result.put("MMK", new CurrencyDataImpl("MMK", "MMK", 0));
    // Tugrik
    result.put("MNT", new CurrencyDataImpl("MNT", "₮", 0, "MN₮"));
    // Pataca
    result.put("MOP", new CurrencyDataImpl("MOP", "MOP$", 2));
    // Ouguiya
    result.put("MRO", new CurrencyDataImpl("MRO", "UM", 0));
    // Lira maltská
    result.put("MTL", new CurrencyDataImpl("MTL", "Lm", 130));
    // Libra maltská
    result.put("MTP", new CurrencyDataImpl("MTP", "MT£", 130));
    // Rupie mauricijská
    result.put("MUR", new CurrencyDataImpl("MUR", "MURs", 0));
    // Rufiyaa
    result.put("MVR", new CurrencyDataImpl("MVR", "MVR", 2));
    // Kwacha malawská
    result.put("MWK", new CurrencyDataImpl("MWK", "MWK", 2));
    // Peso mexické
    result.put("MXN", new CurrencyDataImpl("MXN", "Mex$", 2, "Mex$"));
    // Peso stříbrné mexické (1861-1992)
    result.put("MXP", new CurrencyDataImpl("MXP", "MX$", 130));
    // Mexican Unidad de Inversion (UDI)
    result.put("MXV", new CurrencyDataImpl("MXV", "MXV", 2));
    // Ringgit malajskijský
    result.put("MYR", new CurrencyDataImpl("MYR", "RM", 2, "RM"));
    // Escudo Mosambiku
    result.put("MZE", new CurrencyDataImpl("MZE", "MZE", 130));
    // Metical
    result.put("MZM", new CurrencyDataImpl("MZM", "Mt", 130));
    // MZN
    result.put("MZN", new CurrencyDataImpl("MZN", "MTn", 2));
    // Dolar namibijský
    result.put("NAD", new CurrencyDataImpl("NAD", "N$", 2));
    // Naira
    result.put("NGN", new CurrencyDataImpl("NGN", "₦", 2));
    // Cordoba
    result.put("NIC", new CurrencyDataImpl("NIC", "NIC", 130));
    // Cordoba oro
    result.put("NIO", new CurrencyDataImpl("NIO", "C$", 2));
    // Zlatý holandský
    result.put("NLG", new CurrencyDataImpl("NLG", "fl", 130));
    // Koruna norská
    result.put("NOK", new CurrencyDataImpl("NOK", "Nkr", 2, "NOkr"));
    // Rupie nepálská
    result.put("NPR", new CurrencyDataImpl("NPR", "NPRs", 2));
    // Dolar novozélandský
    result.put("NZD", new CurrencyDataImpl("NZD", "NZ$", 2));
    // Rijál ománský
    result.put("OMR", new CurrencyDataImpl("OMR", "OMR", 3));
    // Balboa
    result.put("PAB", new CurrencyDataImpl("PAB", "B/.", 2, "B/."));
    // Inti
    result.put("PEI", new CurrencyDataImpl("PEI", "I/.", 130));
    // Nuevo sol
    result.put("PEN", new CurrencyDataImpl("PEN", "S/.", 2, "S/."));
    // Sol
    result.put("PES", new CurrencyDataImpl("PES", "PES", 130));
    // Kina
    result.put("PGK", new CurrencyDataImpl("PGK", "PGK", 2));
    // Peso filipínské
    result.put("PHP", new CurrencyDataImpl("PHP", "₱", 2, "PHP"));
    // Rupie pákistánská
    result.put("PKR", new CurrencyDataImpl("PKR", "PKRs", 0, "PKRs."));
    // Zlotý
    result.put("PLN", new CurrencyDataImpl("PLN", "zł", 2));
    // Zlotý (1950-1995)
    result.put("PLZ", new CurrencyDataImpl("PLZ", "PLZ", 130));
    // Escudo portugalské
    result.put("PTE", new CurrencyDataImpl("PTE", "Esc", 130));
    // Guarani
    result.put("PYG", new CurrencyDataImpl("PYG", "₲", 0));
    // Rijál katarský
    result.put("QAR", new CurrencyDataImpl("QAR", "QR", 2));
    // RHD
    result.put("RHD", new CurrencyDataImpl("RHD", "RH$", 130));
    // Lei
    result.put("ROL", new CurrencyDataImpl("ROL", "ROL", 130));
    // Leu rumunské
    result.put("RON", new CurrencyDataImpl("RON", "RON", 2));
    // Dinár srbský
    result.put("RSD", new CurrencyDataImpl("RSD", "din.", 0));
    // Rubl ruský
    result.put("RUB", new CurrencyDataImpl("RUB", "руб", 2, "руб"));
    // Rubl ruský (1991-1998)
    result.put("RUR", new CurrencyDataImpl("RUR", "RUR", 130));
    // Frank rwandský
    result.put("RWF", new CurrencyDataImpl("RWF", "RWF", 0));
    // Rijál saudský
    result.put("SAR", new CurrencyDataImpl("SAR", "SR", 2, "SR"));
    // Dolar Šalamounových ostrovů
    result.put("SBD", new CurrencyDataImpl("SBD", "SI$", 2));
    // Rupie seychelská
    result.put("SCR", new CurrencyDataImpl("SCR", "SRe", 2));
    // Dinár súdánský
    result.put("SDD", new CurrencyDataImpl("SDD", "LSd", 130));
    // SDG
    result.put("SDG", new CurrencyDataImpl("SDG", "SDG", 2));
    // Libra súdánská
    result.put("SDP", new CurrencyDataImpl("SDP", "SDP", 130));
    // Koruna švédská
    result.put("SEK", new CurrencyDataImpl("SEK", "Skr", 2, "kr"));
    // Dolar singapurský
    result.put("SGD", new CurrencyDataImpl("SGD", "S$", 2, "S$"));
    // Libra Svaté Heleny
    result.put("SHP", new CurrencyDataImpl("SHP", "SH£", 2));
    // Tolar
    result.put("SIT", new CurrencyDataImpl("SIT", "SIT", 130));
    // Koruna slovenská
    result.put("SKK", new CurrencyDataImpl("SKK", "Sk", 130));
    // Leone
    result.put("SLL", new CurrencyDataImpl("SLL", "Le", 0));
    // Šilink somálský
    result.put("SOS", new CurrencyDataImpl("SOS", "Ssh", 0));
    // SRD
    result.put("SRD", new CurrencyDataImpl("SRD", "SR$", 2));
    // Zlatý surinamský
    result.put("SRG", new CurrencyDataImpl("SRG", "Sf", 130));
    // Dobra
    result.put("STD", new CurrencyDataImpl("STD", "Db", 0));
    // Rubl
    result.put("SUR", new CurrencyDataImpl("SUR", "SUR", 130));
    // Colon salvadorský
    result.put("SVC", new CurrencyDataImpl("SVC", "SV₡", 130));
    // Libra syrská
    result.put("SYP", new CurrencyDataImpl("SYP", "SY£", 0));
    // Lilangeni
    result.put("SZL", new CurrencyDataImpl("SZL", "SZL", 2));
    // Baht
    result.put("THB", new CurrencyDataImpl("THB", "฿", 2, "THB"));
    // Tajikistan Ruble
    result.put("TJR", new CurrencyDataImpl("TJR", "TJR", 130));
    // Somoni
    result.put("TJS", new CurrencyDataImpl("TJS", "TJS", 2));
    // Manat
    result.put("TMM", new CurrencyDataImpl("TMM", "TMM", 128));
    // Dinár tuniský
    result.put("TND", new CurrencyDataImpl("TND", "DT", 3));
    // Paʻanga
    result.put("TOP", new CurrencyDataImpl("TOP", "T$", 2));
    // Escudo timorské
    result.put("TPE", new CurrencyDataImpl("TPE", "TPE", 130));
    // Lira turecká
    result.put("TRL", new CurrencyDataImpl("TRL", "TRL", 128));
    // Lira nová turecká
    result.put("TRY", new CurrencyDataImpl("TRY", "TL", 2, "YTL"));
    // Dolar Trinidad a Tobago
    result.put("TTD", new CurrencyDataImpl("TTD", "TT$", 2));
    // Dolar tchajvanský nový
    result.put("TWD", new CurrencyDataImpl("TWD", "NT$", 2, "NT$"));
    // Šilink tanzanský
    result.put("TZS", new CurrencyDataImpl("TZS", "TSh", 0));
    // Hřivna
    result.put("UAH", new CurrencyDataImpl("UAH", "₴", 2));
    // Karbovanec
    result.put("UAK", new CurrencyDataImpl("UAK", "UAK", 130));
    // Šilink ugandský (1966-1987)
    result.put("UGS", new CurrencyDataImpl("UGS", "UGS", 130));
    // Šilink ugandský
    result.put("UGX", new CurrencyDataImpl("UGX", "USh", 0));
    // Dolar americký
    result.put("USD", new CurrencyDataImpl("USD", "US$", 2, "US$"));
    // Dolar americký (příští den)
    result.put("USN", new CurrencyDataImpl("USN", "USN", 2));
    // Dolar americký (týž den)
    result.put("USS", new CurrencyDataImpl("USS", "USS", 2));
    // UYI
    result.put("UYI", new CurrencyDataImpl("UYI", "UYI", 2));
    // Peso uruguayské (1975-1993)
    result.put("UYP", new CurrencyDataImpl("UYP", "UYP", 130));
    // Peso uruguayské
    result.put("UYU", new CurrencyDataImpl("UYU", "$U", 2, "UY$"));
    // Sum uzbecký
    result.put("UZS", new CurrencyDataImpl("UZS", "UZS", 0));
    // Bolivar
    result.put("VEB", new CurrencyDataImpl("VEB", "VEB", 130));
    // VEF
    result.put("VEF", new CurrencyDataImpl("VEF", "Bs.F.", 2));
    // Dong vietnamský
    result.put("VND", new CurrencyDataImpl("VND", "₫", 24, "₫"));
    // Vatu
    result.put("VUV", new CurrencyDataImpl("VUV", "VT", 0));
    // Tala
    result.put("WST", new CurrencyDataImpl("WST", "WS$", 2));
    // Frank BEAC/CFA
    result.put("XAF", new CurrencyDataImpl("XAF", "FCFA", 0));
    // Stříbro
    result.put("XAG", new CurrencyDataImpl("XAG", "XAG", 2));
    // Zlato
    result.put("XAU", new CurrencyDataImpl("XAU", "XAU", 2));
    // Evropská smíšená jednotka
    result.put("XBA", new CurrencyDataImpl("XBA", "XBA", 2));
    // Evropská peněžní jednotka
    result.put("XBB", new CurrencyDataImpl("XBB", "XBB", 2));
    // Evropská jednotka účtu 9 (XBC)
    result.put("XBC", new CurrencyDataImpl("XBC", "XBC", 2));
    // Evropská jednotka účtu 17 (XBD)
    result.put("XBD", new CurrencyDataImpl("XBD", "XBD", 2));
    // Dolar východokaribský
    result.put("XCD", new CurrencyDataImpl("XCD", "EC$", 2));
    // SDR
    result.put("XDR", new CurrencyDataImpl("XDR", "XDR", 2));
    // Evropská měnová jednotka
    result.put("XEU", new CurrencyDataImpl("XEU", "XEU", 2));
    // Frank zlatý
    result.put("XFO", new CurrencyDataImpl("XFO", "XFO", 2));
    // Frank UIC
    result.put("XFU", new CurrencyDataImpl("XFU", "XFU", 2));
    // Frank BCEAO/CFA
    result.put("XOF", new CurrencyDataImpl("XOF", "CFA", 0));
    // Paladium
    result.put("XPD", new CurrencyDataImpl("XPD", "XPD", 2));
    // Frank CFP
    result.put("XPF", new CurrencyDataImpl("XPF", "CFPF", 0));
    // Platina
    result.put("XPT", new CurrencyDataImpl("XPT", "XPT", 2));
    // Kód fondů RINET
    result.put("XRE", new CurrencyDataImpl("XRE", "XRE", 2));
    // Kódy zvlášť vyhrazené pro testovací účely
    result.put("XTS", new CurrencyDataImpl("XTS", "XTS", 2));
    // Neznámá nebo neplatná měna
    result.put("XXX", new CurrencyDataImpl("XXX", "XXX", 2));
    // Dinár jemenský
    result.put("YDD", new CurrencyDataImpl("YDD", "YDD", 130));
    // Rijál jemenský
    result.put("YER", new CurrencyDataImpl("YER", "YR", 0, "YER"));
    // Dinár jugoslávský nový [YUD]
    result.put("YUD", new CurrencyDataImpl("YUD", "YUD", 130));
    // Dinár jugoslávský nový [YUM]
    result.put("YUM", new CurrencyDataImpl("YUM", "YUM", 130));
    // Dinár jugoslávský konvertibilní
    result.put("YUN", new CurrencyDataImpl("YUN", "YUN", 130));
    // Rand finanční
    result.put("ZAL", new CurrencyDataImpl("ZAL", "ZAL", 2));
    // Rand
    result.put("ZAR", new CurrencyDataImpl("ZAR", "R", 2, "ZAR"));
    // Kwacha zambijská
    result.put("ZMK", new CurrencyDataImpl("ZMK", "ZK", 0));
    // Zaire nový
    result.put("ZRN", new CurrencyDataImpl("ZRN", "NZ", 130));
    // Zaire
    result.put("ZRZ", new CurrencyDataImpl("ZRZ", "ZRZ", 130));
    // Dolar zimbabwský
    result.put("ZWD", new CurrencyDataImpl("ZWD", "Z$", 128));
    return result;
  }
  
  @Override
  protected JavaScriptObject loadCurrencyMapNative() {
    return overrideMap(super.loadCurrencyMapNative(), loadMyCurrencyMapOverridesNative());
  }
  
  private native JavaScriptObject loadMyCurrencyMapOverridesNative() /*-{
    return {
      // Peseta andorrská
      "ADP": [ "ADP", "ADP", 128],
      // Dirham SAE
      "AED": [ "AED", "DH", 2, "DH"],
      // Afghán (1927-2002)
      "AFA": [ "AFA", "AFA", 130],
      // Afghán
      "AFN": [ "AFN", "Af", 0],
      // Lek
      "ALL": [ "ALL", "ALL", 0],
      // Dram arménský
      "AMD": [ "AMD", "AMD", 0],
      // Zlatý Nizozemských Antil
      "ANG": [ "ANG", "NAf.", 2],
      // Kwanza
      "AOA": [ "AOA", "Kz", 2],
      // Kwanza (1977-1990)
      "AOK": [ "AOK", "AOK", 130],
      // Kwanza nová (1990-2000)
      "AON": [ "AON", "AON", 130],
      // Kwanza reajustado (1995-1999)
      "AOR": [ "AOR", "AOR", 130],
      // Austral
      "ARA": [ "ARA", "₳", 130],
      // ARL
      "ARL": [ "ARL", "$L", 130],
      // ARM
      "ARM": [ "ARM", "m$n", 130],
      // Peso argentinské (1983-1985)
      "ARP": [ "ARP", "ARP", 130],
      // Peso argentinské
      "ARS": [ "ARS", "AR$", 2, "AR$"],
      // Šilink
      "ATS": [ "ATS", "ATS", 130],
      // Dolar australský
      "AUD": [ "AUD", "AU$", 2, "AU$"],
      // Zlatý arubský
      "AWG": [ "AWG", "Afl.", 2],
      // Manat ázerbajdžánský
      "AZM": [ "AZM", "AZM", 130],
      // AZN
      "AZN": [ "AZN", "man.", 2],
      // Dinár Bosny a Hercegoviny
      "BAD": [ "BAD", "BAD", 130],
      // Marka konvertibilní
      "BAM": [ "BAM", "KM", 2],
      // Dolar barbadoský
      "BBD": [ "BBD", "Bds$", 2],
      // Taka
      "BDT": [ "BDT", "Tk", 2, "Tk"],
      // Frank konvertibilní belgický
      "BEC": [ "BEC", "BEC", 2],
      // Frank belgický
      "BEF": [ "BEF", "BF", 130],
      // Frank finanční belgický
      "BEL": [ "BEL", "BEL", 2],
      // Lev
      "BGL": [ "BGL", "BGL", 130],
      // Lev Bulharský
      "BGN": [ "BGN", "BGN", 2],
      // Dinár bahrajnský
      "BHD": [ "BHD", "BD", 3],
      // Frank burundský
      "BIF": [ "BIF", "FBu", 0],
      // Dolar bermudský
      "BMD": [ "BMD", "BD$", 2],
      // Dolar brunejský
      "BND": [ "BND", "BN$", 2],
      // Boliviano
      "BOB": [ "BOB", "Bs", 2],
      // Peso
      "BOP": [ "BOP", "$b.", 130],
      // Mvdol
      "BOV": [ "BOV", "BOV", 2],
      // Cruzeiro (1967-1986)
      "BRB": [ "BRB", "BRB", 130],
      // Cruzado
      "BRC": [ "BRC", "BRC", 130],
      // Cruzeiro (1990-1993)
      "BRE": [ "BRE", "BRE", 130],
      // Real brazilský
      "BRL": [ "BRL", "R$", 2, "R$"],
      // Cruzado nové
      "BRN": [ "BRN", "BRN", 130],
      // Cruzeiro real
      "BRR": [ "BRR", "BRR", 130],
      // Dolar bahamský
      "BSD": [ "BSD", "BS$", 2],
      // Ngultrum
      "BTN": [ "BTN", "Nu.", 2],
      // Kyat barmský
      "BUK": [ "BUK", "BUK", 130],
      // Pula
      "BWP": [ "BWP", "BWP", 2],
      // Rubl nový běloruský (1994-1999)
      "BYB": [ "BYB", "BYB", 130],
      // Rubl běloruský
      "BYR": [ "BYR", "BYR", 0],
      // Dolar belizský
      "BZD": [ "BZD", "BZ$", 2],
      // Dolar kanadský
      "CAD": [ "CAD", "CA$", 2, "C$"],
      // Frank konžský
      "CDF": [ "CDF", "CDF", 2],
      // CHE
      "CHE": [ "CHE", "CHE", 2],
      // Frank švýcarský
      "CHF": [ "CHF", "Fr.", 2, "CHF"],
      // CHW
      "CHW": [ "CHW", "CHW", 2],
      // CLE
      "CLE": [ "CLE", "Eº", 130],
      // Unidades de fomento
      "CLF": [ "CLF", "CLF", 0],
      // Peso chilské
      "CLP": [ "CLP", "CL$", 0, "CL$"],
      // Juan renminbi
      "CNY": [ "CNY", "CN¥", 2, "RMB¥"],
      // Peso kolumbijské
      "COP": [ "COP", "CO$", 0, "COL$"],
      // COU
      "COU": [ "COU", "COU", 2],
      // Colón kostarický
      "CRC": [ "CRC", "₡", 0, "CR₡"],
      // CSD
      "CSD": [ "CSD", "CSD", 130],
      // Koruna československá
      "CSK": [ "CSK", "CSK", 130],
      // CUC
      "CUC": [ "CUC", "CUC$", 2],
      // Peso kubánské
      "CUP": [ "CUP", "CU$", 2, "$MN"],
      // Escudo kapverdské
      "CVE": [ "CVE", "CV$", 2],
      // Libra kyperská
      "CYP": [ "CYP", "CY£", 130],
      // Koruna česká
      "CZK": [ "CZK", "Kč", 2, "Kč"],
      // Marka NDR
      "DDM": [ "DDM", "DDM", 130],
      // Marka německá
      "DEM": [ "DEM", "DM", 130],
      // Frank džibutský
      "DJF": [ "DJF", "Fdj", 0],
      // Koruna dánská
      "DKK": [ "DKK", "Dkr", 2, "kr"],
      // Peso dominikánské
      "DOP": [ "DOP", "RD$", 2, "RD$"],
      // Dinár alžírský
      "DZD": [ "DZD", "DA", 2],
      // Sucre ekvádorský
      "ECS": [ "ECS", "ECS", 130],
      // Ecuador Unidad de Valor Constante (UVC)
      "ECV": [ "ECV", "ECV", 2],
      // Kroon
      "EEK": [ "EEK", "Ekr", 2],
      // Libra egyptská
      "EGP": [ "EGP", "EG£", 2, "LE"],
      // EQE
      "EQE": [ "EQE", "EQE", 130],
      // Nakfa
      "ERN": [ "ERN", "Nfk", 2],
      // Peseta španělská („A“ účet)
      "ESA": [ "ESA", "ESA", 2],
      // Konvertibilní pesetové účty
      "ESB": [ "ESB", "ESB", 2],
      // Peseta španělská
      "ESP": [ "ESP", "Pts", 128],
      // Birr etiopský
      "ETB": [ "ETB", "Br", 2],
      // Euro
      "EUR": [ "EUR", "€", 2, "€"],
      // Markka
      "FIM": [ "FIM", "mk", 130],
      // Dolar fidžijský
      "FJD": [ "FJD", "FJ$", 2],
      // Libra falklandská
      "FKP": [ "FKP", "FK£", 2],
      // Frank francouzský
      "FRF": [ "FRF", "₣", 130],
      // Libra šterlinků
      "GBP": [ "GBP", "£", 2, "GB£"],
      // Georgian Kupon Larit
      "GEK": [ "GEK", "GEK", 130],
      // Lari
      "GEL": [ "GEL", "GEL", 2],
      // Cedi
      "GHC": [ "GHC", "₵", 130],
      // GHS
      "GHS": [ "GHS", "GH₵", 2],
      // Libra gibraltarská
      "GIP": [ "GIP", "GI£", 2],
      // Dalasi
      "GMD": [ "GMD", "GMD", 2],
      // Frank guinejský
      "GNF": [ "GNF", "FG", 0],
      // Guinea Syli
      "GNS": [ "GNS", "GNS", 130],
      // Equatorial Guinea Ekwele Guineana
      "GQE": [ "GQE", "GQE", 130],
      // Drachma
      "GRD": [ "GRD", "₯", 130],
      // Quetzal
      "GTQ": [ "GTQ", "GTQ", 2],
      // Escudo guinejské
      "GWE": [ "GWE", "GWE", 130],
      // Peso Guinnea-Bissau
      "GWP": [ "GWP", "GWP", 2],
      // Dolar guyanský
      "GYD": [ "GYD", "GY$", 0],
      // Dolar hongkongský
      "HKD": [ "HKD", "HK$", 2, "HK$"],
      // Lempira
      "HNL": [ "HNL", "HNL", 2],
      // Dinar chorvatský
      "HRD": [ "HRD", "HRD", 130],
      // Kuna chorvatská
      "HRK": [ "HRK", "kn", 2],
      // Gourde
      "HTG": [ "HTG", "HTG", 2],
      // Forint
      "HUF": [ "HUF", "Ft", 0],
      // Rupie indonézská
      "IDR": [ "IDR", "Rp", 0],
      // Libra irská
      "IEP": [ "IEP", "IR£", 130],
      // Libra izraelská
      "ILP": [ "ILP", "I£", 130],
      // Šekel nový izraelský
      "ILS": [ "ILS", "₪", 2, "IL₪"],
      // Rupie indická
      "INR": [ "INR", "Rs", 2, "Rs"],
      // Dinár irácký
      "IQD": [ "IQD", "IQD", 0],
      // Rijál íránský
      "IRR": [ "IRR", "IRR", 0],
      // Koruna islandská
      "ISK": [ "ISK", "Ikr", 0, "kr"],
      // Lira italská
      "ITL": [ "ITL", "IT₤", 128],
      // Dolar jamajský
      "JMD": [ "JMD", "J$", 2, "JA$"],
      // Dinár jordánský
      "JOD": [ "JOD", "JD", 3],
      // Jen
      "JPY": [ "JPY", "JP¥", 0, "JP¥"],
      // Šilink keňský
      "KES": [ "KES", "Ksh", 2],
      // Som
      "KGS": [ "KGS", "KGS", 2],
      // Riel
      "KHR": [ "KHR", "KHR", 2],
      // Frank komorský
      "KMF": [ "KMF", "CF", 0],
      // Won severokorejský
      "KPW": [ "KPW", "KPW", 0],
      // Won jihokorejský
      "KRW": [ "KRW", "₩", 0, "KR₩"],
      // Dinár kuvajtský
      "KWD": [ "KWD", "KD", 3],
      // Dolar Kajmanských ostrovů
      "KYD": [ "KYD", "KY$", 2],
      // Tenge
      "KZT": [ "KZT", "KZT", 2],
      // Kip
      "LAK": [ "LAK", "₭", 0],
      // Libra libanonská
      "LBP": [ "LBP", "LB£", 0],
      // Rupie srílanská
      "LKR": [ "LKR", "SLRs", 2, "SLRs"],
      // Dolar liberijský
      "LRD": [ "LRD", "L$", 2],
      // Loti
      "LSL": [ "LSL", "LSL", 2],
      // LSM
      "LSM": [ "LSM", "LSM", 130],
      // Litus litevský
      "LTL": [ "LTL", "Lt", 2],
      // Talon
      "LTT": [ "LTT", "LTT", 130],
      // Frank konvertibilní lucemburský
      "LUC": [ "LUC", "LUC", 2],
      // Frank lucemburský
      "LUF": [ "LUF", "LUF", 128],
      // Frank finanční lucemburský
      "LUL": [ "LUL", "LUL", 2],
      // Lat lotyšský
      "LVL": [ "LVL", "Ls", 2],
      // Rubl lotyšský
      "LVR": [ "LVR", "LVR", 130],
      // Dinár lybijský
      "LYD": [ "LYD", "LD", 3],
      // Dirham marocký
      "MAD": [ "MAD", "MAD", 2],
      // Frank marocký
      "MAF": [ "MAF", "MAF", 130],
      // Leu moldavský
      "MDL": [ "MDL", "MDL", 2],
      // Ariary madagaskarský
      "MGA": [ "MGA", "MGA", 0],
      // Frank madagaskarský
      "MGF": [ "MGF", "MGF", 128],
      // Denár
      "MKD": [ "MKD", "MKD", 2],
      // Frank malijský
      "MLF": [ "MLF", "MLF", 130],
      // Kyat
      "MMK": [ "MMK", "MMK", 0],
      // Tugrik
      "MNT": [ "MNT", "₮", 0, "MN₮"],
      // Pataca
      "MOP": [ "MOP", "MOP$", 2],
      // Ouguiya
      "MRO": [ "MRO", "UM", 0],
      // Lira maltská
      "MTL": [ "MTL", "Lm", 130],
      // Libra maltská
      "MTP": [ "MTP", "MT£", 130],
      // Rupie mauricijská
      "MUR": [ "MUR", "MURs", 0],
      // Rufiyaa
      "MVR": [ "MVR", "MVR", 2],
      // Kwacha malawská
      "MWK": [ "MWK", "MWK", 2],
      // Peso mexické
      "MXN": [ "MXN", "Mex$", 2, "Mex$"],
      // Peso stříbrné mexické (1861-1992)
      "MXP": [ "MXP", "MX$", 130],
      // Mexican Unidad de Inversion (UDI)
      "MXV": [ "MXV", "MXV", 2],
      // Ringgit malajskijský
      "MYR": [ "MYR", "RM", 2, "RM"],
      // Escudo Mosambiku
      "MZE": [ "MZE", "MZE", 130],
      // Metical
      "MZM": [ "MZM", "Mt", 130],
      // MZN
      "MZN": [ "MZN", "MTn", 2],
      // Dolar namibijský
      "NAD": [ "NAD", "N$", 2],
      // Naira
      "NGN": [ "NGN", "₦", 2],
      // Cordoba
      "NIC": [ "NIC", "NIC", 130],
      // Cordoba oro
      "NIO": [ "NIO", "C$", 2],
      // Zlatý holandský
      "NLG": [ "NLG", "fl", 130],
      // Koruna norská
      "NOK": [ "NOK", "Nkr", 2, "NOkr"],
      // Rupie nepálská
      "NPR": [ "NPR", "NPRs", 2],
      // Dolar novozélandský
      "NZD": [ "NZD", "NZ$", 2],
      // Rijál ománský
      "OMR": [ "OMR", "OMR", 3],
      // Balboa
      "PAB": [ "PAB", "B/.", 2, "B/."],
      // Inti
      "PEI": [ "PEI", "I/.", 130],
      // Nuevo sol
      "PEN": [ "PEN", "S/.", 2, "S/."],
      // Sol
      "PES": [ "PES", "PES", 130],
      // Kina
      "PGK": [ "PGK", "PGK", 2],
      // Peso filipínské
      "PHP": [ "PHP", "₱", 2, "PHP"],
      // Rupie pákistánská
      "PKR": [ "PKR", "PKRs", 0, "PKRs."],
      // Zlotý
      "PLN": [ "PLN", "zł", 2],
      // Zlotý (1950-1995)
      "PLZ": [ "PLZ", "PLZ", 130],
      // Escudo portugalské
      "PTE": [ "PTE", "Esc", 130],
      // Guarani
      "PYG": [ "PYG", "₲", 0],
      // Rijál katarský
      "QAR": [ "QAR", "QR", 2],
      // RHD
      "RHD": [ "RHD", "RH$", 130],
      // Lei
      "ROL": [ "ROL", "ROL", 130],
      // Leu rumunské
      "RON": [ "RON", "RON", 2],
      // Dinár srbský
      "RSD": [ "RSD", "din.", 0],
      // Rubl ruský
      "RUB": [ "RUB", "руб", 2, "руб"],
      // Rubl ruský (1991-1998)
      "RUR": [ "RUR", "RUR", 130],
      // Frank rwandský
      "RWF": [ "RWF", "RWF", 0],
      // Rijál saudský
      "SAR": [ "SAR", "SR", 2, "SR"],
      // Dolar Šalamounových ostrovů
      "SBD": [ "SBD", "SI$", 2],
      // Rupie seychelská
      "SCR": [ "SCR", "SRe", 2],
      // Dinár súdánský
      "SDD": [ "SDD", "LSd", 130],
      // SDG
      "SDG": [ "SDG", "SDG", 2],
      // Libra súdánská
      "SDP": [ "SDP", "SDP", 130],
      // Koruna švédská
      "SEK": [ "SEK", "Skr", 2, "kr"],
      // Dolar singapurský
      "SGD": [ "SGD", "S$", 2, "S$"],
      // Libra Svaté Heleny
      "SHP": [ "SHP", "SH£", 2],
      // Tolar
      "SIT": [ "SIT", "SIT", 130],
      // Koruna slovenská
      "SKK": [ "SKK", "Sk", 130],
      // Leone
      "SLL": [ "SLL", "Le", 0],
      // Šilink somálský
      "SOS": [ "SOS", "Ssh", 0],
      // SRD
      "SRD": [ "SRD", "SR$", 2],
      // Zlatý surinamský
      "SRG": [ "SRG", "Sf", 130],
      // Dobra
      "STD": [ "STD", "Db", 0],
      // Rubl
      "SUR": [ "SUR", "SUR", 130],
      // Colon salvadorský
      "SVC": [ "SVC", "SV₡", 130],
      // Libra syrská
      "SYP": [ "SYP", "SY£", 0],
      // Lilangeni
      "SZL": [ "SZL", "SZL", 2],
      // Baht
      "THB": [ "THB", "฿", 2, "THB"],
      // Tajikistan Ruble
      "TJR": [ "TJR", "TJR", 130],
      // Somoni
      "TJS": [ "TJS", "TJS", 2],
      // Manat
      "TMM": [ "TMM", "TMM", 128],
      // Dinár tuniský
      "TND": [ "TND", "DT", 3],
      // Paʻanga
      "TOP": [ "TOP", "T$", 2],
      // Escudo timorské
      "TPE": [ "TPE", "TPE", 130],
      // Lira turecká
      "TRL": [ "TRL", "TRL", 128],
      // Lira nová turecká
      "TRY": [ "TRY", "TL", 2, "YTL"],
      // Dolar Trinidad a Tobago
      "TTD": [ "TTD", "TT$", 2],
      // Dolar tchajvanský nový
      "TWD": [ "TWD", "NT$", 2, "NT$"],
      // Šilink tanzanský
      "TZS": [ "TZS", "TSh", 0],
      // Hřivna
      "UAH": [ "UAH", "₴", 2],
      // Karbovanec
      "UAK": [ "UAK", "UAK", 130],
      // Šilink ugandský (1966-1987)
      "UGS": [ "UGS", "UGS", 130],
      // Šilink ugandský
      "UGX": [ "UGX", "USh", 0],
      // Dolar americký
      "USD": [ "USD", "US$", 2, "US$"],
      // Dolar americký (příští den)
      "USN": [ "USN", "USN", 2],
      // Dolar americký (týž den)
      "USS": [ "USS", "USS", 2],
      // UYI
      "UYI": [ "UYI", "UYI", 2],
      // Peso uruguayské (1975-1993)
      "UYP": [ "UYP", "UYP", 130],
      // Peso uruguayské
      "UYU": [ "UYU", "$U", 2, "UY$"],
      // Sum uzbecký
      "UZS": [ "UZS", "UZS", 0],
      // Bolivar
      "VEB": [ "VEB", "VEB", 130],
      // VEF
      "VEF": [ "VEF", "Bs.F.", 2],
      // Dong vietnamský
      "VND": [ "VND", "₫", 24, "₫"],
      // Vatu
      "VUV": [ "VUV", "VT", 0],
      // Tala
      "WST": [ "WST", "WS$", 2],
      // Frank BEAC/CFA
      "XAF": [ "XAF", "FCFA", 0],
      // Stříbro
      "XAG": [ "XAG", "XAG", 2],
      // Zlato
      "XAU": [ "XAU", "XAU", 2],
      // Evropská smíšená jednotka
      "XBA": [ "XBA", "XBA", 2],
      // Evropská peněžní jednotka
      "XBB": [ "XBB", "XBB", 2],
      // Evropská jednotka účtu 9 (XBC)
      "XBC": [ "XBC", "XBC", 2],
      // Evropská jednotka účtu 17 (XBD)
      "XBD": [ "XBD", "XBD", 2],
      // Dolar východokaribský
      "XCD": [ "XCD", "EC$", 2],
      // SDR
      "XDR": [ "XDR", "XDR", 2],
      // Evropská měnová jednotka
      "XEU": [ "XEU", "XEU", 2],
      // Frank zlatý
      "XFO": [ "XFO", "XFO", 2],
      // Frank UIC
      "XFU": [ "XFU", "XFU", 2],
      // Frank BCEAO/CFA
      "XOF": [ "XOF", "CFA", 0],
      // Paladium
      "XPD": [ "XPD", "XPD", 2],
      // Frank CFP
      "XPF": [ "XPF", "CFPF", 0],
      // Platina
      "XPT": [ "XPT", "XPT", 2],
      // Kód fondů RINET
      "XRE": [ "XRE", "XRE", 2],
      // Kódy zvlášť vyhrazené pro testovací účely
      "XTS": [ "XTS", "XTS", 2],
      // Neznámá nebo neplatná měna
      "XXX": [ "XXX", "XXX", 2],
      // Dinár jemenský
      "YDD": [ "YDD", "YDD", 130],
      // Rijál jemenský
      "YER": [ "YER", "YR", 0, "YER"],
      // Dinár jugoslávský nový [YUD]
      "YUD": [ "YUD", "YUD", 130],
      // Dinár jugoslávský nový [YUM]
      "YUM": [ "YUM", "YUM", 130],
      // Dinár jugoslávský konvertibilní
      "YUN": [ "YUN", "YUN", 130],
      // Rand finanční
      "ZAL": [ "ZAL", "ZAL", 2],
      // Rand
      "ZAR": [ "ZAR", "R", 2, "ZAR"],
      // Kwacha zambijská
      "ZMK": [ "ZMK", "ZK", 0],
      // Zaire nový
      "ZRN": [ "ZRN", "NZ", 130],
      // Zaire
      "ZRZ": [ "ZRZ", "ZRZ", 130],
      // Dolar zimbabwský
      "ZWD": [ "ZWD", "Z$", 128],
    };
  }-*/;
  
  @Override
  protected HashMap<String, String> loadNamesMapJava() {
    HashMap<String, String> result = super.loadNamesMapJava();
    result.put("ADP", "Peseta andorrská");
    result.put("AED", "Dirham SAE");
    result.put("AFA", "Afghán (1927-2002)");
    result.put("AFN", "Afghán");
    result.put("ALL", "Lek");
    result.put("AMD", "Dram arménský");
    result.put("ANG", "Zlatý Nizozemských Antil");
    result.put("AOA", "Kwanza");
    result.put("AOK", "Kwanza (1977-1990)");
    result.put("AON", "Kwanza nová (1990-2000)");
    result.put("AOR", "Kwanza reajustado (1995-1999)");
    result.put("ARA", "Austral");
    result.put("ARP", "Peso argentinské (1983-1985)");
    result.put("ARS", "Peso argentinské");
    result.put("ATS", "Šilink");
    result.put("AUD", "Dolar australský");
    result.put("AWG", "Zlatý arubský");
    result.put("AZM", "Manat ázerbajdžánský");
    result.put("BAD", "Dinár Bosny a Hercegoviny");
    result.put("BAM", "Marka konvertibilní");
    result.put("BBD", "Dolar barbadoský");
    result.put("BDT", "Taka");
    result.put("BEC", "Frank konvertibilní belgický");
    result.put("BEF", "Frank belgický");
    result.put("BEL", "Frank finanční belgický");
    result.put("BGL", "Lev");
    result.put("BGN", "Lev Bulharský");
    result.put("BHD", "Dinár bahrajnský");
    result.put("BIF", "Frank burundský");
    result.put("BMD", "Dolar bermudský");
    result.put("BND", "Dolar brunejský");
    result.put("BOB", "Boliviano");
    result.put("BOP", "Peso");
    result.put("BOV", "Mvdol");
    result.put("BRB", "Cruzeiro (1967-1986)");
    result.put("BRC", "Cruzado");
    result.put("BRE", "Cruzeiro (1990-1993)");
    result.put("BRL", "Real brazilský");
    result.put("BRN", "Cruzado nové");
    result.put("BRR", "Cruzeiro real");
    result.put("BSD", "Dolar bahamský");
    result.put("BTN", "Ngultrum");
    result.put("BUK", "Kyat barmský");
    result.put("BWP", "Pula");
    result.put("BYB", "Rubl nový běloruský (1994-1999)");
    result.put("BYR", "Rubl běloruský");
    result.put("BZD", "Dolar belizský");
    result.put("CAD", "Dolar kanadský");
    result.put("CDF", "Frank konžský");
    result.put("CHF", "Frank švýcarský");
    result.put("CLF", "Unidades de fomento");
    result.put("CLP", "Peso chilské");
    result.put("CNY", "Juan renminbi");
    result.put("COP", "Peso kolumbijské");
    result.put("CRC", "Colón kostarický");
    result.put("CSK", "Koruna československá");
    result.put("CUP", "Peso kubánské");
    result.put("CVE", "Escudo kapverdské");
    result.put("CYP", "Libra kyperská");
    result.put("CZK", "Koruna česká");
    result.put("DDM", "Marka NDR");
    result.put("DEM", "Marka německá");
    result.put("DJF", "Frank džibutský");
    result.put("DKK", "Koruna dánská");
    result.put("DOP", "Peso dominikánské");
    result.put("DZD", "Dinár alžírský");
    result.put("ECS", "Sucre ekvádorský");
    result.put("ECV", "Ecuador Unidad de Valor Constante (UVC)");
    result.put("EEK", "Kroon");
    result.put("EGP", "Libra egyptská");
    result.put("ERN", "Nakfa");
    result.put("ESA", "Peseta španělská („A“ účet)");
    result.put("ESB", "Konvertibilní pesetové účty");
    result.put("ESP", "Peseta španělská");
    result.put("ETB", "Birr etiopský");
    result.put("EUR", "Euro");
    result.put("FIM", "Markka");
    result.put("FJD", "Dolar fidžijský");
    result.put("FKP", "Libra falklandská");
    result.put("FRF", "Frank francouzský");
    result.put("GBP", "Libra šterlinků");
    result.put("GEK", "Georgian Kupon Larit");
    result.put("GEL", "Lari");
    result.put("GHC", "Cedi");
    result.put("GIP", "Libra gibraltarská");
    result.put("GMD", "Dalasi");
    result.put("GNF", "Frank guinejský");
    result.put("GNS", "Guinea Syli");
    result.put("GQE", "Equatorial Guinea Ekwele Guineana");
    result.put("GRD", "Drachma");
    result.put("GTQ", "Quetzal");
    result.put("GWE", "Escudo guinejské");
    result.put("GWP", "Peso Guinnea-Bissau");
    result.put("GYD", "Dolar guyanský");
    result.put("HKD", "Dolar hongkongský");
    result.put("HNL", "Lempira");
    result.put("HRD", "Dinar chorvatský");
    result.put("HRK", "Kuna chorvatská");
    result.put("HTG", "Gourde");
    result.put("HUF", "Forint");
    result.put("IDR", "Rupie indonézská");
    result.put("IEP", "Libra irská");
    result.put("ILP", "Libra izraelská");
    result.put("ILS", "Šekel nový izraelský");
    result.put("INR", "Rupie indická");
    result.put("IQD", "Dinár irácký");
    result.put("IRR", "Rijál íránský");
    result.put("ISK", "Koruna islandská");
    result.put("ITL", "Lira italská");
    result.put("JMD", "Dolar jamajský");
    result.put("JOD", "Dinár jordánský");
    result.put("JPY", "Jen");
    result.put("KES", "Šilink keňský");
    result.put("KGS", "Som");
    result.put("KHR", "Riel");
    result.put("KMF", "Frank komorský");
    result.put("KPW", "Won severokorejský");
    result.put("KRW", "Won jihokorejský");
    result.put("KWD", "Dinár kuvajtský");
    result.put("KYD", "Dolar Kajmanských ostrovů");
    result.put("KZT", "Tenge");
    result.put("LAK", "Kip");
    result.put("LBP", "Libra libanonská");
    result.put("LKR", "Rupie srílanská");
    result.put("LRD", "Dolar liberijský");
    result.put("LSL", "Loti");
    result.put("LTL", "Litus litevský");
    result.put("LTT", "Talon");
    result.put("LUC", "Frank konvertibilní lucemburský");
    result.put("LUF", "Frank lucemburský");
    result.put("LUL", "Frank finanční lucemburský");
    result.put("LVL", "Lat lotyšský");
    result.put("LVR", "Rubl lotyšský");
    result.put("LYD", "Dinár lybijský");
    result.put("MAD", "Dirham marocký");
    result.put("MAF", "Frank marocký");
    result.put("MDL", "Leu moldavský");
    result.put("MGA", "Ariary madagaskarský");
    result.put("MGF", "Frank madagaskarský");
    result.put("MKD", "Denár");
    result.put("MLF", "Frank malijský");
    result.put("MMK", "Kyat");
    result.put("MNT", "Tugrik");
    result.put("MOP", "Pataca");
    result.put("MRO", "Ouguiya");
    result.put("MTL", "Lira maltská");
    result.put("MTP", "Libra maltská");
    result.put("MUR", "Rupie mauricijská");
    result.put("MVR", "Rufiyaa");
    result.put("MWK", "Kwacha malawská");
    result.put("MXN", "Peso mexické");
    result.put("MXP", "Peso stříbrné mexické (1861-1992)");
    result.put("MXV", "Mexican Unidad de Inversion (UDI)");
    result.put("MYR", "Ringgit malajskijský");
    result.put("MZE", "Escudo Mosambiku");
    result.put("MZM", "Metical");
    result.put("NAD", "Dolar namibijský");
    result.put("NGN", "Naira");
    result.put("NIC", "Cordoba");
    result.put("NIO", "Cordoba oro");
    result.put("NLG", "Zlatý holandský");
    result.put("NOK", "Koruna norská");
    result.put("NPR", "Rupie nepálská");
    result.put("NZD", "Dolar novozélandský");
    result.put("OMR", "Rijál ománský");
    result.put("PAB", "Balboa");
    result.put("PEI", "Inti");
    result.put("PEN", "Nuevo sol");
    result.put("PES", "Sol");
    result.put("PGK", "Kina");
    result.put("PHP", "Peso filipínské");
    result.put("PKR", "Rupie pákistánská");
    result.put("PLN", "Zlotý");
    result.put("PLZ", "Zlotý (1950-1995)");
    result.put("PTE", "Escudo portugalské");
    result.put("PYG", "Guarani");
    result.put("QAR", "Rijál katarský");
    result.put("ROL", "Lei");
    result.put("RON", "Leu rumunské");
    result.put("RSD", "Dinár srbský");
    result.put("RUB", "Rubl ruský");
    result.put("RUR", "Rubl ruský (1991-1998)");
    result.put("RWF", "Frank rwandský");
    result.put("SAR", "Rijál saudský");
    result.put("SBD", "Dolar Šalamounových ostrovů");
    result.put("SCR", "Rupie seychelská");
    result.put("SDD", "Dinár súdánský");
    result.put("SDP", "Libra súdánská");
    result.put("SEK", "Koruna švédská");
    result.put("SGD", "Dolar singapurský");
    result.put("SHP", "Libra Svaté Heleny");
    result.put("SIT", "Tolar");
    result.put("SKK", "Koruna slovenská");
    result.put("SLL", "Leone");
    result.put("SOS", "Šilink somálský");
    result.put("SRG", "Zlatý surinamský");
    result.put("STD", "Dobra");
    result.put("SUR", "Rubl");
    result.put("SVC", "Colon salvadorský");
    result.put("SYP", "Libra syrská");
    result.put("SZL", "Lilangeni");
    result.put("THB", "Baht");
    result.put("TJR", "Tajikistan Ruble");
    result.put("TJS", "Somoni");
    result.put("TMM", "Manat");
    result.put("TND", "Dinár tuniský");
    result.put("TOP", "Paʻanga");
    result.put("TPE", "Escudo timorské");
    result.put("TRL", "Lira turecká");
    result.put("TRY", "Lira nová turecká");
    result.put("TTD", "Dolar Trinidad a Tobago");
    result.put("TWD", "Dolar tchajvanský nový");
    result.put("TZS", "Šilink tanzanský");
    result.put("UAH", "Hřivna");
    result.put("UAK", "Karbovanec");
    result.put("UGS", "Šilink ugandský (1966-1987)");
    result.put("UGX", "Šilink ugandský");
    result.put("USD", "Dolar americký");
    result.put("USN", "Dolar americký (příští den)");
    result.put("USS", "Dolar americký (týž den)");
    result.put("UYP", "Peso uruguayské (1975-1993)");
    result.put("UYU", "Peso uruguayské");
    result.put("UZS", "Sum uzbecký");
    result.put("VEB", "Bolivar");
    result.put("VND", "Dong vietnamský");
    result.put("VUV", "Vatu");
    result.put("WST", "Tala");
    result.put("XAF", "Frank BEAC/CFA");
    result.put("XAG", "Stříbro");
    result.put("XAU", "Zlato");
    result.put("XBA", "Evropská smíšená jednotka");
    result.put("XBB", "Evropská peněžní jednotka");
    result.put("XBC", "Evropská jednotka účtu 9 (XBC)");
    result.put("XBD", "Evropská jednotka účtu 17 (XBD)");
    result.put("XCD", "Dolar východokaribský");
    result.put("XDR", "SDR");
    result.put("XEU", "Evropská měnová jednotka");
    result.put("XFO", "Frank zlatý");
    result.put("XFU", "Frank UIC");
    result.put("XOF", "Frank BCEAO/CFA");
    result.put("XPD", "Paladium");
    result.put("XPF", "Frank CFP");
    result.put("XPT", "Platina");
    result.put("XRE", "Kód fondů RINET");
    result.put("XTS", "Kódy zvlášť vyhrazené pro testovací účely");
    result.put("XXX", "Neznámá nebo neplatná měna");
    result.put("YDD", "Dinár jemenský");
    result.put("YER", "Rijál jemenský");
    result.put("YUD", "Dinár jugoslávský nový [YUD]");
    result.put("YUM", "Dinár jugoslávský nový [YUM]");
    result.put("YUN", "Dinár jugoslávský konvertibilní");
    result.put("ZAL", "Rand finanční");
    result.put("ZAR", "Rand");
    result.put("ZMK", "Kwacha zambijská");
    result.put("ZRN", "Zaire nový");
    result.put("ZRZ", "Zaire");
    result.put("ZWD", "Dolar zimbabwský");
    return result;
  }
  
  @Override
  protected JavaScriptObject loadNamesMapNative() {
    return overrideMap(super.loadNamesMapNative(), loadMyNamesMapOverridesNative());
  }
  
  private native JavaScriptObject loadMyNamesMapOverridesNative() /*-{
    return {
      "ADP": "Peseta andorrská",
      "AED": "Dirham SAE",
      "AFA": "Afghán (1927-2002)",
      "AFN": "Afghán",
      "ALL": "Lek",
      "AMD": "Dram arménský",
      "ANG": "Zlatý Nizozemských Antil",
      "AOA": "Kwanza",
      "AOK": "Kwanza (1977-1990)",
      "AON": "Kwanza nová (1990-2000)",
      "AOR": "Kwanza reajustado (1995-1999)",
      "ARA": "Austral",
      "ARP": "Peso argentinské (1983-1985)",
      "ARS": "Peso argentinské",
      "ATS": "Šilink",
      "AUD": "Dolar australský",
      "AWG": "Zlatý arubský",
      "AZM": "Manat ázerbajdžánský",
      "BAD": "Dinár Bosny a Hercegoviny",
      "BAM": "Marka konvertibilní",
      "BBD": "Dolar barbadoský",
      "BDT": "Taka",
      "BEC": "Frank konvertibilní belgický",
      "BEF": "Frank belgický",
      "BEL": "Frank finanční belgický",
      "BGL": "Lev",
      "BGN": "Lev Bulharský",
      "BHD": "Dinár bahrajnský",
      "BIF": "Frank burundský",
      "BMD": "Dolar bermudský",
      "BND": "Dolar brunejský",
      "BOB": "Boliviano",
      "BOP": "Peso",
      "BOV": "Mvdol",
      "BRB": "Cruzeiro (1967-1986)",
      "BRC": "Cruzado",
      "BRE": "Cruzeiro (1990-1993)",
      "BRL": "Real brazilský",
      "BRN": "Cruzado nové",
      "BRR": "Cruzeiro real",
      "BSD": "Dolar bahamský",
      "BTN": "Ngultrum",
      "BUK": "Kyat barmský",
      "BWP": "Pula",
      "BYB": "Rubl nový běloruský (1994-1999)",
      "BYR": "Rubl běloruský",
      "BZD": "Dolar belizský",
      "CAD": "Dolar kanadský",
      "CDF": "Frank konžský",
      "CHF": "Frank švýcarský",
      "CLF": "Unidades de fomento",
      "CLP": "Peso chilské",
      "CNY": "Juan renminbi",
      "COP": "Peso kolumbijské",
      "CRC": "Colón kostarický",
      "CSK": "Koruna československá",
      "CUP": "Peso kubánské",
      "CVE": "Escudo kapverdské",
      "CYP": "Libra kyperská",
      "CZK": "Koruna česká",
      "DDM": "Marka NDR",
      "DEM": "Marka německá",
      "DJF": "Frank džibutský",
      "DKK": "Koruna dánská",
      "DOP": "Peso dominikánské",
      "DZD": "Dinár alžírský",
      "ECS": "Sucre ekvádorský",
      "ECV": "Ecuador Unidad de Valor Constante (UVC)",
      "EEK": "Kroon",
      "EGP": "Libra egyptská",
      "ERN": "Nakfa",
      "ESA": "Peseta španělská („A“ účet)",
      "ESB": "Konvertibilní pesetové účty",
      "ESP": "Peseta španělská",
      "ETB": "Birr etiopský",
      "EUR": "Euro",
      "FIM": "Markka",
      "FJD": "Dolar fidžijský",
      "FKP": "Libra falklandská",
      "FRF": "Frank francouzský",
      "GBP": "Libra šterlinků",
      "GEK": "Georgian Kupon Larit",
      "GEL": "Lari",
      "GHC": "Cedi",
      "GIP": "Libra gibraltarská",
      "GMD": "Dalasi",
      "GNF": "Frank guinejský",
      "GNS": "Guinea Syli",
      "GQE": "Equatorial Guinea Ekwele Guineana",
      "GRD": "Drachma",
      "GTQ": "Quetzal",
      "GWE": "Escudo guinejské",
      "GWP": "Peso Guinnea-Bissau",
      "GYD": "Dolar guyanský",
      "HKD": "Dolar hongkongský",
      "HNL": "Lempira",
      "HRD": "Dinar chorvatský",
      "HRK": "Kuna chorvatská",
      "HTG": "Gourde",
      "HUF": "Forint",
      "IDR": "Rupie indonézská",
      "IEP": "Libra irská",
      "ILP": "Libra izraelská",
      "ILS": "Šekel nový izraelský",
      "INR": "Rupie indická",
      "IQD": "Dinár irácký",
      "IRR": "Rijál íránský",
      "ISK": "Koruna islandská",
      "ITL": "Lira italská",
      "JMD": "Dolar jamajský",
      "JOD": "Dinár jordánský",
      "JPY": "Jen",
      "KES": "Šilink keňský",
      "KGS": "Som",
      "KHR": "Riel",
      "KMF": "Frank komorský",
      "KPW": "Won severokorejský",
      "KRW": "Won jihokorejský",
      "KWD": "Dinár kuvajtský",
      "KYD": "Dolar Kajmanských ostrovů",
      "KZT": "Tenge",
      "LAK": "Kip",
      "LBP": "Libra libanonská",
      "LKR": "Rupie srílanská",
      "LRD": "Dolar liberijský",
      "LSL": "Loti",
      "LTL": "Litus litevský",
      "LTT": "Talon",
      "LUC": "Frank konvertibilní lucemburský",
      "LUF": "Frank lucemburský",
      "LUL": "Frank finanční lucemburský",
      "LVL": "Lat lotyšský",
      "LVR": "Rubl lotyšský",
      "LYD": "Dinár lybijský",
      "MAD": "Dirham marocký",
      "MAF": "Frank marocký",
      "MDL": "Leu moldavský",
      "MGA": "Ariary madagaskarský",
      "MGF": "Frank madagaskarský",
      "MKD": "Denár",
      "MLF": "Frank malijský",
      "MMK": "Kyat",
      "MNT": "Tugrik",
      "MOP": "Pataca",
      "MRO": "Ouguiya",
      "MTL": "Lira maltská",
      "MTP": "Libra maltská",
      "MUR": "Rupie mauricijská",
      "MVR": "Rufiyaa",
      "MWK": "Kwacha malawská",
      "MXN": "Peso mexické",
      "MXP": "Peso stříbrné mexické (1861-1992)",
      "MXV": "Mexican Unidad de Inversion (UDI)",
      "MYR": "Ringgit malajskijský",
      "MZE": "Escudo Mosambiku",
      "MZM": "Metical",
      "NAD": "Dolar namibijský",
      "NGN": "Naira",
      "NIC": "Cordoba",
      "NIO": "Cordoba oro",
      "NLG": "Zlatý holandský",
      "NOK": "Koruna norská",
      "NPR": "Rupie nepálská",
      "NZD": "Dolar novozélandský",
      "OMR": "Rijál ománský",
      "PAB": "Balboa",
      "PEI": "Inti",
      "PEN": "Nuevo sol",
      "PES": "Sol",
      "PGK": "Kina",
      "PHP": "Peso filipínské",
      "PKR": "Rupie pákistánská",
      "PLN": "Zlotý",
      "PLZ": "Zlotý (1950-1995)",
      "PTE": "Escudo portugalské",
      "PYG": "Guarani",
      "QAR": "Rijál katarský",
      "ROL": "Lei",
      "RON": "Leu rumunské",
      "RSD": "Dinár srbský",
      "RUB": "Rubl ruský",
      "RUR": "Rubl ruský (1991-1998)",
      "RWF": "Frank rwandský",
      "SAR": "Rijál saudský",
      "SBD": "Dolar Šalamounových ostrovů",
      "SCR": "Rupie seychelská",
      "SDD": "Dinár súdánský",
      "SDP": "Libra súdánská",
      "SEK": "Koruna švédská",
      "SGD": "Dolar singapurský",
      "SHP": "Libra Svaté Heleny",
      "SIT": "Tolar",
      "SKK": "Koruna slovenská",
      "SLL": "Leone",
      "SOS": "Šilink somálský",
      "SRG": "Zlatý surinamský",
      "STD": "Dobra",
      "SUR": "Rubl",
      "SVC": "Colon salvadorský",
      "SYP": "Libra syrská",
      "SZL": "Lilangeni",
      "THB": "Baht",
      "TJR": "Tajikistan Ruble",
      "TJS": "Somoni",
      "TMM": "Manat",
      "TND": "Dinár tuniský",
      "TOP": "Paʻanga",
      "TPE": "Escudo timorské",
      "TRL": "Lira turecká",
      "TRY": "Lira nová turecká",
      "TTD": "Dolar Trinidad a Tobago",
      "TWD": "Dolar tchajvanský nový",
      "TZS": "Šilink tanzanský",
      "UAH": "Hřivna",
      "UAK": "Karbovanec",
      "UGS": "Šilink ugandský (1966-1987)",
      "UGX": "Šilink ugandský",
      "USD": "Dolar americký",
      "USN": "Dolar americký (příští den)",
      "USS": "Dolar americký (týž den)",
      "UYP": "Peso uruguayské (1975-1993)",
      "UYU": "Peso uruguayské",
      "UZS": "Sum uzbecký",
      "VEB": "Bolivar",
      "VND": "Dong vietnamský",
      "VUV": "Vatu",
      "WST": "Tala",
      "XAF": "Frank BEAC/CFA",
      "XAG": "Stříbro",
      "XAU": "Zlato",
      "XBA": "Evropská smíšená jednotka",
      "XBB": "Evropská peněžní jednotka",
      "XBC": "Evropská jednotka účtu 9 (XBC)",
      "XBD": "Evropská jednotka účtu 17 (XBD)",
      "XCD": "Dolar východokaribský",
      "XDR": "SDR",
      "XEU": "Evropská měnová jednotka",
      "XFO": "Frank zlatý",
      "XFU": "Frank UIC",
      "XOF": "Frank BCEAO/CFA",
      "XPD": "Paladium",
      "XPF": "Frank CFP",
      "XPT": "Platina",
      "XRE": "Kód fondů RINET",
      "XTS": "Kódy zvlášť vyhrazené pro testovací účely",
      "XXX": "Neznámá nebo neplatná měna",
      "YDD": "Dinár jemenský",
      "YER": "Rijál jemenský",
      "YUD": "Dinár jugoslávský nový [YUD]",
      "YUM": "Dinár jugoslávský nový [YUM]",
      "YUN": "Dinár jugoslávský konvertibilní",
      "ZAL": "Rand finanční",
      "ZAR": "Rand",
      "ZMK": "Kwacha zambijská",
      "ZRN": "Zaire nový",
      "ZRZ": "Zaire",
      "ZWD": "Dolar zimbabwský",
    };
  }-*/;
}
