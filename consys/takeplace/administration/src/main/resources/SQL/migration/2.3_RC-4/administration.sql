-- 08.03.2011
-- drop constraint - state
ALTER TABLE user_organization DROP CONSTRAINT fkcee31d27626012ac;
-- drop constraint - us_state
ALTER TABLE user_organization DROP CONSTRAINT fkcee31d27ace70535;

-- drop constraint - state
ALTER TABLE organization DROP CONSTRAINT fkd063d533626012ac;
-- drop constraint - us_state
ALTER TABLE organization DROP CONSTRAINT fkd063d533e4729c70;

-- drop event - id_state
ALTER TABLE event DROP CONSTRAINT fk3f47a7a626012ac;

