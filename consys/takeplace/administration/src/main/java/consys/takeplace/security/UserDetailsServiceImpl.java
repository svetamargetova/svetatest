package consys.takeplace.security;

import consys.admin.common.core.CommonSystemProperties;
import consys.admin.common.gwt.server.ContextProvider;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class UserDetailsServiceImpl implements UserDetailsService {

    private final static Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    private static final long serialVersionUID = 6361507293039466723L;
    private UserLoginInfoService userLoginService;
    private SystemPropertyService propertyService;
    private ContextProvider contextProvider;

    @Override
    public synchronized UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {

        GrantedAuthority[] authorities;
        String password;        
        Boolean allowLogin = false;

        try {
            allowLogin = propertyService.getBoolean(CommonSystemProperties.ALLOW_LOGIN_TO_TAKEPLACE);

            UserLoginInfo loginInfo = userLoginService.loadUserLoginInfo(username);

            logger.info("Login:" + username);
            authorities = new GrantedAuthority[]{new GrantedAuthorityImpl("ROLE_USER")};

            password = loginInfo.getPassword();
            // Odstranene - aj neaktivovany uzivatelia sa mozu prihlasit
            //accountNonLocked = loginInfo.isActive();

            logger.info("Intializing context: id: "+loginInfo.getUser().getId()+"uuid:" + loginInfo.getUser().getUuid());
            contextProvider.getUserContext().initContext(loginInfo.getUser().getUuid(),loginInfo.getUser().getId());
        } catch (NoRecordException e) {
            throw new UsernameNotFoundException(username);
        } catch (Exception e) {
            logger.error("Error!", e);
            throw new UsernameNotFoundException(username, e);
        }

        org.springframework.security.core.userdetails.User userDetails = new org.springframework.security.core.userdetails.User(
                username,
                password,
                allowLogin,
                true, //boolean accountNonExpired,
                true, //boolean credentialsNonExpired,
                true, //accountNonLocked
                authorities);
        return userDetails;
    }

    public UserLoginInfoService getUserLoginService() {
        return userLoginService;
    }

    public void setUserLoginService(UserLoginInfoService userLoginService) {
        this.userLoginService = userLoginService;
    }
    
    /**
     * @return the propertyService
     */
    public SystemPropertyService getPropertyService() {
        return propertyService;
    }

    /**
     * @param propertyService the propertyService to set
     */
    public void setPropertyService(SystemPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    /**
     * @return the contextProvider
     */
    public ContextProvider getContextProvider() {
        return contextProvider;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    public void setContextProvider(ContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }

   
}
