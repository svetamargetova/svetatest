/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.servlet;

import consys.common.utils.enums.CountryEnum;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author palo
 */
public class EventProfilePageLocalizationResolverServlet extends AbstractCountryServlet {

    private static final String XFORWARDEDHOST = "x-forwarded-host";

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    private void process(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
                        
            final String page = req.getParameter("page");
            if (page == null) {
                logger.error("Missing event profile web page.");
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            logger.debug("Profile page {} localization resolution", page);

            /**
             * Redirekt len na takeplace.eu lebo v tomto momente nemame ine prostredie
             */
            CountryEnum country = processRequest(req, resp);
            if (country != null) {
                switch (country) {
                    case CZECH_REPUBLIC:
                    case SLOVAKIA:
                        // redirect /cs
                        resp.sendRedirect(String.format("http://%s.takeplace.eu/cs", page));
                        return;
                }
            }
            // redirect /en                    
            resp.sendRedirect(String.format("http://%s.takeplace.eu/en", page));
        } catch (Exception ex) {
            logger.error("Error while resolving country code by IP: ",ex);
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
