package consys.takeplace.servlet;

import consys.common.utils.enums.CountryEnum;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author pepa
 */
public class GetCountryServlet extends AbstractCountryServlet {

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    private void process(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            CountryEnum country = processRequest(req, resp);
            if (country != null) {
                // vracime id country
                resp.getOutputStream().print(country.getId());
                resp.setStatus(HttpServletResponse.SC_OK);

            }
        } catch (Exception ex) {
            logger.error("Error while resolving country code by IP");
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
