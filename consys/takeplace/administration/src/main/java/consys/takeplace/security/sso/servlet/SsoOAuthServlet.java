/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.sso.servlet;

import consys.admin.common.server.security.SsoAuthenticationToken;
import consys.admin.event.gwt.server.sso.SsoLockCache;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserService;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.UserUtils;
import consys.common.constants.sso.SSO;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.utils.UuidProvider;
import consys.takeplace.security.sso.facebook.FacebookOAuthServletHandler;
import consys.takeplace.security.sso.gmail.GoogleOAuthServletHandler;
import consys.takeplace.security.sso.linkedin.LinkedInOAuthServletHandler;
import consys.takeplace.security.sso.twitter.TwitterOAuthServletHandler;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Servlet ktory redirectuje uzivatela vzhladom na podporvany typ OAuth
 * providera a nasledne spracuje poziadavku.
 *
 * @author palo
 */
public class SsoOAuthServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(SsoOAuthServlet.class);
    private static final String TOKEN = "state";
    private static final String UNSUPPORTED_REDIRECT = "/takeplace/takeplace.html#LogIn?sso_unsupported";
    private static final String FAILED__LOGIN_REDIRECT = "/takeplace/takeplace.html#LogIn?sso_failed";
    private static final String DENIED_LOGIN_REDIRECT = "/takeplace/takeplace.html#LogIn?sso_denied";
    private static final String SUCCESS_LOGIN_REDIRECT = "/takeplace/takeplace.html#LoginSuccess";
    private static final String SUCCESS_SESSION_REDIRECT = "/sso/success.html";
    private static final String FAILED_SESSION_REDIRECT = "/sso/failed.html";
    private static final String PARAM_SSO = "service";
    private static final String LOGIN_SUFFIX = "login";
    private static final String CALLBACK_SUFFIX = "callback";
    @Autowired
    private SsoLockCache ssoLockCache;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;
    private TwitterOAuthServletHandler twitterOAuthServletHandler = new TwitterOAuthServletHandler();
    private LinkedInOAuthServletHandler linkedInOAuthServletHandler = new LinkedInOAuthServletHandler();
    private FacebookOAuthServletHandler facebookOAuthServletHandler = new FacebookOAuthServletHandler();
    private GoogleOAuthServletHandler gmailOAuthServletHandler;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // autowirujeme handlere
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();

        beanFactory.autowireBean(this);

        twitterOAuthServletHandler = new TwitterOAuthServletHandler();
        linkedInOAuthServletHandler = new LinkedInOAuthServletHandler();
        facebookOAuthServletHandler = new FacebookOAuthServletHandler();
        gmailOAuthServletHandler = new GoogleOAuthServletHandler(config);

        beanFactory.autowireBean(twitterOAuthServletHandler);
        beanFactory.autowireBean(linkedInOAuthServletHandler);
        beanFactory.autowireBean(facebookOAuthServletHandler);
        beanFactory.autowireBean(gmailOAuthServletHandler);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            final SSO sso = translateToSso(req);
            if (sso == null) {
                logger.debug("Unsupported SSO: {}", req.getQueryString());
                resp.sendRedirect(UNSUPPORTED_REDIRECT);
                return;
            }
            if (req.getPathInfo().endsWith(CALLBACK_SUFFIX)) {


                logger.debug("SSO: callback processing: {}", req.getRequestURL().toString());


                SsoOAuthServletHandler handler = resolveHandler(sso);
                String token = getSynchronizationToken(req);

                try {



                    ClientSsoUserDetails userDetails = handler.handleCallback(req, resp);
                    userDetails.setFromSso(sso);

                    if (token.length() > 0) {
                        logger.debug("SSO: [{} {}]->session", userDetails.getFirstName(), userDetails.getLastName());
                        processUserToSession(userDetails, token);
                        resp.sendRedirect(SUCCESS_SESSION_REDIRECT);

                    } else {
                        logger.debug("SSO: [{} {}]->login", userDetails.getFirstName(), userDetails.getLastName());
                        SsoAuthenticationToken authtoken = processUserLogin(userDetails, handler);
                        try {
                            Authentication authResult = authenticationManager.authenticate(authtoken);
                            SecurityContextHolder.getContext().setAuthentication(authResult);
                            resp.sendRedirect(SUCCESS_LOGIN_REDIRECT);
                        } catch (AuthenticationException ex) {
                            resp.sendRedirect(FAILED__LOGIN_REDIRECT);
                        }
                    }
                } catch (SsoUserDeniedException e) {
                    resp.sendRedirect(redirectWithService(token.length() > 0 ? FAILED_SESSION_REDIRECT : DENIED_LOGIN_REDIRECT, sso));
                } catch (SsoException ex) {
                    resp.sendRedirect(redirectWithService(token.length() > 0 ? FAILED_SESSION_REDIRECT : FAILED__LOGIN_REDIRECT, sso));
                }




            } else if (req.getPathInfo().endsWith(LOGIN_SUFFIX)) {
                /**
                 * Ak sa jedna o Login tak sa resolvuje handler a vytvori
                 * callback URL
                 */
                try {
                    SsoOAuthServletHandler handler = resolveHandler(sso);
                    final String callbackBase = getCallbackHostName(req);
                    final String token = getSynchronizationToken(req);

                    String callback;

                    if (handler.isStateTokenPartOfCallback()) {
                        callback = String.format("%s/%s?%s=%s&state=%s", callbackBase, CALLBACK_SUFFIX, PARAM_SSO, sso.name(), token);
                    } else {
                        callback = String.format("%s/%s?%s=%s", callbackBase, CALLBACK_SUFFIX, PARAM_SSO, sso.name());
                    }

                    logger.debug("SSO: callback:{} token:{}", callback, token);

                    handler.handleLoginRedirect(req, resp, callback, token);
                } catch (SsoException ex) {
                    resp.sendRedirect(redirectWithService(FAILED__LOGIN_REDIRECT, sso));
                }
            } else {
                logger.error("Unsupported: {}", req.getPathInfo());
                resp.sendRedirect(UNSUPPORTED_REDIRECT);
            }
        } catch (Exception e) {
            logger.error("Exception: ", e);
            resp.sendRedirect(UNSUPPORTED_REDIRECT);
        }
    }

    /**
     * Vrati zaklad pre callback.
     *
     * TODO spravit to ako placeholder na: tp.eu hekuba.. app
     *
     * @param req
     * @return
     */
    private String getCallbackHostName(HttpServletRequest req) {
        // osetrenie pri apache proxy
        String callbackBase;
        String xforwarded = req.getHeader("x-forwarded-host");
        if (xforwarded != null) {
            callbackBase = String.format(("%s://%s%s"), req.getScheme(), xforwarded, req.getRequestURI());
        } else {
            callbackBase = req.getRequestURL().toString();
        }
        int idx = callbackBase.lastIndexOf("/");
        callbackBase = callbackBase.substring(0, idx);
        return callbackBase;
    }

    private String redirectWithService(String base, SSO sso) {
        return String.format("%s&%s=%s", base, PARAM_SSO, sso.name());
    }

    private SsoOAuthServletHandler resolveHandler(SSO sso) throws SsoException {
        SsoOAuthServletHandler handler = null;
        switch (sso) {
            case FACEBOOK:
                handler = facebookOAuthServletHandler;
                break;
            case LINKEDIN:
                handler = linkedInOAuthServletHandler;
                break;
            case TWITTER:
                handler = twitterOAuthServletHandler;
                break;
            case GOOGLE_PLUS:
                handler = gmailOAuthServletHandler;
                break;
        }

        if (handler != null) {
            return handler;
        } else {
            throw new SsoException();
        }
    }

    private void processUserToSession(ClientSsoUserDetails ssoUser, String token) {
        SsoLockCache.SsoLockItem item = ssoLockCache.getLockForToken(token);
        if (item == null) {
            logger.error("Lock for notify abou new user not found");
        } else {
            synchronized (item.lock) {
                item.setUserDetails(ssoUser);
                logger.debug("Notify abou new user: {} {} {} {}", new Object[]{ssoUser.getFirstName(), ssoUser.getLastName(), ssoUser.getLoginEmail(), ssoUser.getProfilePictureUrl()});
                item.lock.notifyAll();
            }
        }

    }

    private SsoAuthenticationToken processUserLogin(ClientSsoUserDetails ssoUser, SsoOAuthServletHandler handler) throws SsoException {
        // ak sa prihalsuje novy uzivatel alebo sa opakovane prihlasuje cez facebook
        SsoAuthenticationToken authToken = null;
        try {
            // najskor skusime nacitat podle email                                                            
            UserLoginInfo uli = handler.loadUserLoginInfo(ssoUser);
            authToken = new SsoAuthenticationToken(uli.getUser().getUuid(), uli.getUser().getId());
        } catch (RequiredPropertyNullException ex) {
            throw new SsoException();
        } catch (NoRecordException ex) {


            // setujeme secko co vime resp.                         
            try {

                String email = StringUtils.isBlank(ssoUser.getLoginEmail()) ? UuidProvider.getUuid() : ssoUser.getLoginEmail();

                UserLoginInfo info = UserUtils.prepareNewUser(email, UuidProvider.getUuid());
                info.setActive(true);
                info.getUser().setBio(ssoUser.getBio());
                info.getUser().setFirstName(ssoUser.getFirstName());
                info.getUser().setMiddleName(ssoUser.getMiddleName());
                info.getUser().setLastName(ssoUser.getLastName());
                info.getUser().setGender(ssoUser.getGender());

                switch (ssoUser.getFromSso()) {
                    case FACEBOOK:
                        info.setFacebookId(ssoUser.getSsoId());
                        break;
                    case GOOGLE_PLUS:
                        info.setGoogleId(ssoUser.getSsoStringId());
                        break;
                    case LINKEDIN:
                        info.setLinkedInId(ssoUser.getSsoStringId());
                        break;
                    case TWITTER:
                        info.setTwitterId(ssoUser.getSsoId());
                        break;
                }

                userService.createRegistredUser(info, false, true);
                User user = userService.loadUserByUuid(info.getUser().getUuid());
                authToken = new SsoAuthenticationToken(user.getUuid(), user.getId());
            } catch (NoRecordException ex1) {
                logger.error("Facebook failed! NoRecords", ex1);
                throw new SsoException();
            } catch (RequiredPropertyNullException ex1) {
                logger.error("Create Facebook user failed! Required property null.", ex1);
                throw new SsoException();
            } catch (UsernameExistsException ex1) {
                logger.error("Create Facebook user failed! User already exists? ", ex1);
                throw new SsoException();
            }
        }
        return authToken;
    }

    /**
     * Vrati synchronizacny token, ktory definuje ze user sa ma ulozit do
     * session a nie prihlasovat.
     *
     * <p/>
     * @return
     */
    public String getSynchronizationToken(HttpServletRequest r) {
        String param = r.getParameter(TOKEN);
        return param == null ? "" : param;
    }

    private SSO translateToSso(HttpServletRequest req) {
        if (req.getPathInfo() != null) {
            return SSO.valueOf(req.getParameter(PARAM_SSO).toUpperCase());
        } else {
            return null;
        }
    }
}
