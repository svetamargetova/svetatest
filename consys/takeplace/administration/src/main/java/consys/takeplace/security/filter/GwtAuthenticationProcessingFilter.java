/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.filter;

import consys.admin.common.gwt.server.ContextProvider;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


/**
 * Zabezpecei spravnu redirect na prihalsovaciu stranku. V pripade ze je prihlasenie
 * z GWT aplikacie tak sa redirect vlozi ako obsah redirect page
 * @author Palo
 */
public class GwtAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter {

    
    private ContextProvider contextProvider;

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, authResult);

        // Ulozime do cookie uuid uzivatela
        String userUuid = contextProvider.getUserContext().getUserUuid();
        Cookie cookie = new Cookie("userid", userUuid);        
        cookie.setMaxAge(-1); // not stored
        cookie.setPath("/b2b/");
        response.addCookie(cookie);
    } 


   
    /**
     * @return the contextProvider
     */
    public ContextProvider getContextProvider() {
        return contextProvider;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    public void setContextProvider(ContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }
}
