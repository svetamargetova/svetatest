package consys.takeplace.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import consys.event.registration.gwt.client.module.register.OuterRegistrationFormV2;

/**
 *
 * @author pepa
 */
public class RegisterEntryPoint implements EntryPoint {

    @Override
    public void onModuleLoad() {
        OuterRegistrationFormV2 form = new OuterRegistrationFormV2();
        form.prepare();
        form.start();
    }
}
