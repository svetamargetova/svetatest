/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.sso.linkedin;

import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.enumeration.ProfileField;
import com.google.code.linkedinapi.client.oauth.*;
import com.google.code.linkedinapi.schema.Person;
import com.google.common.collect.ImmutableSet;
import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.exception.UsernameExistsException;
import consys.admin.user.core.util.UserUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.utils.UuidProvider;
import consys.admin.common.server.security.SsoAuthenticationToken;
import consys.takeplace.security.sso.servlet.SsoException;
import consys.takeplace.security.sso.servlet.SsoOAuthServletHandler;
import consys.takeplace.security.sso.servlet.SsoUserDeniedException;
import java.io.IOException;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public class LinkedInOAuthServletHandler extends SsoOAuthServletHandler {

    private static final Logger logger = LoggerFactory.getLogger(LinkedInOAuthServletHandler.class);
    private static final String CONSUMER_KEY = "696dvusqh7i7";
    private static final String CONSUMER_SECRET = "vfEOkdGdeK826K99";
    private final LinkedInApiClientFactory factory = LinkedInApiClientFactory.newInstance(CONSUMER_KEY, CONSUMER_SECRET);
    private final Set<ProfileField> fields;

    public LinkedInOAuthServletHandler() {
        fields = ImmutableSet.<ProfileField>builder().
                add(ProfileField.ID).
                add(ProfileField.FIRST_NAME).
                add(ProfileField.LAST_NAME).
                add(ProfileField.PICTURE_URL).
                add(ProfileField.HEADLINE).
                add(ProfileField.SUMMARY).build();
    }

    @Override
    public void handleLoginRedirect(HttpServletRequest request, HttpServletResponse response, String callback, String token) throws SsoException {
        try {
            LinkedInOAuthService linkedIn = LinkedInOAuthServiceFactory.getInstance().createLinkedInOAuthService(CONSUMER_KEY, CONSUMER_SECRET);

            LinkedInRequestToken requestToken = linkedIn.getOAuthRequestToken(callback);

            
            
            request.getSession().setAttribute("linkedInRequestToken", requestToken);
            request.getSession().setAttribute("linkedInInstance", linkedIn);

            response.sendRedirect(requestToken.getAuthorizationUrl());

        } catch (LinkedInOAuthServiceException e) {
            logger.error("Failed to login by LinkedIn", e);
            throw new SsoException();
        } catch (IOException e) {
            logger.error("Failed to redirect ", e);
            throw new SsoException();
        }

    }

    @Override
    public ClientSsoUserDetails handleCallback(HttpServletRequest request, HttpServletResponse response) throws SsoException {
        final LinkedInRequestToken requestToken = (LinkedInRequestToken) request.getSession().getAttribute("linkedInRequestToken");
        final LinkedInOAuthService linkedIn = (LinkedInOAuthService) request.getSession().getAttribute("linkedInInstance");
        try {
            if (StringUtils.isNotBlank(request.getParameter("oauth_problem"))) {
                if ("user_refused".equalsIgnoreCase(request.getParameter("oauth_problem"))) {
                    throw new SsoUserDeniedException();
                } else {
                    throw new SsoException();
                }
            }

            LinkedInAccessToken accessToken = linkedIn.getOAuthAccessToken(requestToken, request.getParameter("oauth_verifier"));
            LinkedInApiClient c = factory.createLinkedInApiClient(accessToken);
            Person profile = c.getProfileForCurrentUser(fields);

            ClientSsoUserDetails sso = new ClientSsoUserDetails();
            sso.setSsoStringId(profile.getId());
            sso.setBio(profile.getSummary());
            sso.setFirstName(profile.getFirstName());
            sso.setLastName(profile.getLastName());
            sso.setProfilePictureUrl(profile.getPictureUrl());
            sso.setAffiliation(profile.getHeadline());
            return sso;
            
        } catch (LinkedInOAuthServiceException e) {
            logger.error("Failed to login by LinkedIn", e);
            throw new SsoUserDeniedException();
        } finally {
            request.getSession().removeAttribute("linkedInRequestToken");
            request.getSession().removeAttribute("linkedInInstance");
        }
    }

    @Override
    public UserLoginInfo loadUserLoginInfo(ClientSsoUserDetails ssoUserDetails) throws RequiredPropertyNullException, NoRecordException {
        return getLoginInfoService().loadUserLoginInfoByLinkedInId(ssoUserDetails.getSsoStringId());
    }

    @Override
    public boolean isStateTokenPartOfCallback() {
        return true;
    }
}
