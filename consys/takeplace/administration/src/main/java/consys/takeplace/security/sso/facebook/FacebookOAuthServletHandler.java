/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.sso.facebook;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.constants.sso.SSO;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.takeplace.security.sso.servlet.SsoException;
import consys.takeplace.security.sso.servlet.SsoOAuthServletHandler;
import consys.takeplace.security.sso.servlet.SsoUserDeniedException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.MappingJsonFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public class FacebookOAuthServletHandler extends SsoOAuthServletHandler {

    private static final Logger logger = LoggerFactory.getLogger(FacebookOAuthServletHandler.class);
    private static final String APP_ID = "317785891597058";
    private static final String APP_SECRET = "8e79133e7bd5f57eee353d4a70edff9e";
    private final JsonFactory jsonFactory = new MappingJsonFactory();

    @Override
    public void handleLoginRedirect(HttpServletRequest request, HttpServletResponse response, String callback, String token) throws SsoException {
        try {
            String redirect = String.format("https://www.facebook.com/dialog/oauth?client_id=%s&redirect_uri=%s&state=%s&scope=email,user_about_me", APP_ID, callback,token);
            request.getSession().setAttribute("facebookCallback", callback);
            response.sendRedirect(redirect);
        } catch (IOException e) {
            logger.error("Failed to redirect ", e);
            throw new SsoException();
        }

    }
    
    

    @Override
    public ClientSsoUserDetails handleCallback(HttpServletRequest request, HttpServletResponse response) throws SsoException {

        try {
            String code = request.getParameter("code");
            if (StringUtils.isBlank(code)) {
                // error
                String reason = request.getParameter("error_reason");
                String description = request.getParameter("error_description");
                logger.error("Error occured when authorizing user, reason '{}' and description '{}'", reason, description);
                if (reason.equals("user_denied")) {
                    throw new SsoUserDeniedException();
                } else {
                    throw new SsoException();
                }
            }

            String callback = (String) request.getSession().getAttribute("facebookCallback");
            String accessTokenUrl = String.format("https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s", APP_ID, callback, APP_SECRET, code);
            
            try {
                URL url = new URL(accessTokenUrl);
                InputStream is = url.openStream();
                String accessTokenResponse = IOUtils.toString(is);

                String[] separated = StringUtils.splitByWholeSeparator(accessTokenResponse, "&");
                if (separated.length == 0 || !separated[0].startsWith("access_token")) {
                    logger.info("Error retrieve access token {}", accessTokenResponse);
                    throw new SsoException();
                }

                String accessToken = separated[0].substring("access_token".length() + 1);
                URL userInfo = new URL(String.format("https://graph.facebook.com/me?fields=id,bio,first_name,last_name,middle_name,gender,email&access_token=%s", accessToken));
                is = userInfo.openStream();

                JsonParser jp = jsonFactory.createJsonParser(is);
                JsonNode root = jp.readValueAsTree();


                ClientSsoUserDetails ssoUser = new ClientSsoUserDetails();
                ssoUser.setFromSso(SSO.FACEBOOK);
                ssoUser.setSsoId(root.get("id").getValueAsLong());
                ssoUser.setBio(getString("bio", root));
                ssoUser.setFirstName(root.get("first_name").getTextValue());
                ssoUser.setMiddleName(getString("middle_name", root));
                ssoUser.setLastName(root.get("last_name").getTextValue());
                ssoUser.setLoginEmail(root.get("email").getTextValue());
                
                String gender = root.get("gender").getValueAsText();

                if ("female".equalsIgnoreCase(gender)) {
                    ssoUser.setGender(2);
                } else if ("male".equalsIgnoreCase(gender)) {
                    ssoUser.setGender(1);
                } else {
                    ssoUser.setGender(0);
                }                
                return ssoUser;
            } catch (IOException ex) {
                logger.error("Failed to open stream", ex);
                throw new RuntimeException("There was an error processing Facebook Authentication",ex);
            }            
        } finally {
            request.getSession().removeAttribute("facebookCallback");
        }
    }

    

    

    private String getString(String key, JsonNode node) {
        JsonNode t = node.get(key);
        if (t != null) {
            return t.getTextValue();
        } else {
            return null;
        }
    }

    @Override
    public UserLoginInfo loadUserLoginInfo(ClientSsoUserDetails ssoUserDetails) 
            throws RequiredPropertyNullException, NoRecordException {
        return getLoginInfoService().loadUserLoginInfoByFacebookIdOrEmail(ssoUserDetails.getSsoId(), ssoUserDetails.getLoginEmail());
    }

    @Override
    public boolean isStateTokenPartOfCallback() {
        return false;
    }
}
