package consys.takeplace.servlet;

import consys.common.utils.enums.CountryEnum;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author palo
 */
public class AbstractCountryServlet extends HttpServlet {

    private static final long serialVersionUID = 6474208649281709484L;
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private final SAXParserFactory factory = SAXParserFactory.newInstance();
    /** url serveru na ktery se vola pro zjisteni zeme podle ip */
    private static final String REQUEST_SERVER_URL = "http://api.ipinfodb.com/v2/ip_query_country.php?"
            + "key=d9c7cf0ded4ab6a285fd45af5dc1d6d85bb0daa0952dcec7ab4bc3f7affca353&ip=";
    /** maximalni cas cekani na odpoved z ciziho serveru (v milisekundach) */
    private static final int TIMEOUT = 1000;

    /** Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected CountryEnum processRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String xforwarded = request.getHeader("x-forwarded-for");
        xforwarded = xforwarded == null ? request.getRemoteAddr() : xforwarded;

        URL countryRequestURL = new URL(REQUEST_SERVER_URL + xforwarded);
        URLConnection connection = countryRequestURL.openConnection();
        connection.setConnectTimeout(TIMEOUT);

        SAXParser saxParser = factory.newSAXParser();

        CountryHandler handler = new CountryHandler();

        saxParser.parse(connection.getInputStream(), handler);

        Integer id = CountryEnum.fromCode(handler.getResult());
        if (id == null) {
            throw new RuntimeException("Country id not found for ip (" + request.getRemoteAddr() + ") with result: " + handler.getResult());
        }

        return CountryEnum.fromId(id);
    }

    /** parser odpovedi */
    private class CountryHandler extends DefaultHandler {

        private boolean status = false;
        private boolean countryCode = false;
        private String elementContent;
        private String result;

        public String getResult() {
            return result;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

            if (qName.equalsIgnoreCase("Status")) {
                status = true;
            }

            if (qName.equalsIgnoreCase("CountryCode")) {
                countryCode = true;
            }

            elementContent = "";
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            elementContent = new String(ch, start, length);
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (status) {
                if (!elementContent.equalsIgnoreCase("OK")) {
                    // dosla odpoved, ale neni OK -> chyba
                    throw new RuntimeException("Wrong response state");
                }
                status = false;
            }
            if (countryCode) {
                result = elementContent;
                countryCode = false;
            }
            elementContent = "";
        }
    }
}