/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.sso;

import consys.admin.common.server.security.SsoAuthenticationToken;
import consys.admin.common.gwt.server.ContextProvider;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class SsoAuthentictionProvider implements AuthenticationProvider {

    private ContextProvider contextProvider;

    @Override
    public boolean supports(Class authentication) {
        return (SsoAuthenticationToken.class.isAssignableFrom(authentication));
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        if (!supports(authentication.getClass())) {
            return null;
        }

        SsoAuthenticationToken token = (SsoAuthenticationToken) authentication;        
        getContextProvider().getUserContext().initContext(token.getConsysUserUuid(), token.getConsysUserId());
        return authentication;
    }

    /**
     * @return the contextProvider
     */
    public ContextProvider getContextProvider() {
        return contextProvider;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    public void setContextProvider(ContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }
}
