package consys.takeplace.servlet;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.common.constants.img.EventLogoImageEnum;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ReadOnlyPropertyException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.servlet.AbstractUploadImageServlet;
import consys.common.utils.UuidProvider;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.MissingResourceException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Servlet zainstujuci upload loga eventu
 * @author Palo
 */
public class UploadEventLogoImageServlet extends AbstractUploadImageServlet {

    private static final long serialVersionUID = 7563959595557407232L;
    private static final String EVENT_UUID = "eid";
    
    @Autowired
    private EventService eventService;        

    @Override
    protected String saveImage(HttpServletRequest request, String name, BufferedImage bi, String contentType, String subContentType) throws NoRecordException, ServiceExecutionFailed {
        try {
            String eventUuid = (String) request.getParameter(EVENT_UUID);
            if (eventUuid == null) {
                throw new MissingResourceException("Missing", this.getClass().getName(), EVENT_UUID);
            }
            Event e = getEventService().loadEventDetails(eventUuid);            
            String uuidPrefix = UuidProvider.getUuid();
            
            logger.debug("Uploading Event Logo: {}", e);
            
            try {      
                // Wall
                int width = EventLogoImageEnum.WALL.getWidth();
                int height = EventLogoImageEnum.WALL.getHeight();
                String iuuid = EventLogoImageEnum.WALL.getFullUuid(uuidPrefix);
                createImage(width, height, bi, name, contentType, subContentType, iuuid);
                       
                // Ticket
                width = EventLogoImageEnum.TICKET.getWidth();
                height = EventLogoImageEnum.TICKET.getHeight();
                iuuid = EventLogoImageEnum.TICKET.getFullUuid(uuidPrefix);
                createImage(width, height, bi, name, contentType, subContentType, iuuid);
                
                // List
                width = EventLogoImageEnum.LIST.getWidth();
                height = EventLogoImageEnum.LIST.getHeight();
                iuuid = EventLogoImageEnum.LIST.getFullUuid(uuidPrefix);
                createImage(width, height, bi, name, contentType, subContentType, iuuid);
            
                // Original
                iuuid = EventLogoImageEnum.ORIGINAL.getFullUuid(uuidPrefix);
                createImage(bi, name, contentType, subContentType, iuuid);                                
            } catch (IOException ex) {
                throw new ServiceExecutionFailed();
            }
            
            String toDelete = e.getImageLogoPrefix();
            e.setImageLogoPrefix(uuidPrefix);
            getEventService().updateEventDetails(e);
            // teraz mozeme zmazat
            deleteImage(EventLogoImageEnum.TICKET.getFullUuid(toDelete));
            deleteImage(EventLogoImageEnum.ORIGINAL.getFullUuid(toDelete));
            deleteImage(EventLogoImageEnum.LIST.getFullUuid(toDelete));
            deleteImage(EventLogoImageEnum.WALL.getFullUuid(toDelete));
            
            
            return String.format("<logo>%s</logo>",uuidPrefix);
        } catch (ReadOnlyPropertyException ex) {
            throw new ServiceExecutionFailed();
        } catch (RequiredPropertyNullException ex) {
            throw new ServiceExecutionFailed();
        }
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {        
        return eventService;
    }
}
