package consys.takeplace.servlet;

import consys.admin.common.gwt.server.ContextProvider;
import consys.admin.event.core.service.image.UserImageService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.servlet.AbstractUploadImageServlet;
import java.awt.image.BufferedImage;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class UploadUserProfileImageServlet extends AbstractUploadImageServlet {

    private static final long serialVersionUID = 7563959595557407232L;
    @Autowired
    private ContextProvider contextProvider;
    @Autowired
    private UserImageService userImageService;

    @Override
    protected boolean loadBucketFromInitParam() {
        return false;
    }
            
    @Override
    protected String saveImage(HttpServletRequest request, String name, BufferedImage bi, String contentType, String subContentType) throws NoRecordException, ServiceExecutionFailed {

        try {
            String uuid = getContextProvider().getUserContext().getUserUuid();
            String imgUuid = userImageService.createOrUpdateUserImage(uuid, name, bi, contentType, subContentType);
            return String.format("<portrait>%s</portrait>", imgUuid);
        } catch (RequiredPropertyNullException ex) {
            throw new ServiceExecutionFailed();
        }
    }
    
    public ContextProvider getContextProvider() {
        return contextProvider;
    }  
}
