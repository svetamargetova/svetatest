package consys.takeplace.support.api.controller;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import java.io.IOException;
import java.io.StringWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author pepa
 */
public class ApiUserResetPasswordController extends AbstractController {

    // logger
    private Logger log = LoggerFactory.getLogger(ApiUserResetPasswordController.class);
    // konstanty
    public static final String TOKEN = "token";
    public static final String ERROR_JSP = "error-page";
    public static final String USER_RESET_PASSWORD_JSP = "user-password-reset";
    public static final String PASSWORD_CHANGED_JSP = "password-changed";
    public static final String ERROR_MESSAGE = "err_msg";
    public static final String KEY = "key";
    public static final String PASSWORD = "password";
    public static final String CHANGE = "change";
    @Autowired
    private UserLoginInfoService userLoginInfoService;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.debug("API - Changing user password");

        ServletFileUpload upload = new ServletFileUpload();

        try {
            FileItemIterator iter = upload.getItemIterator(request);
            // je odeslan pozadavek s novym heslem k prislusnemu tokenu
            return changePasswordForm(iter);
        } catch (Exception exc) {
            // jedna se o pozadavek na formular pro vlozeni noveho hesla, neobsahuje post data
            return newPasswordForm(request.getParameter(TOKEN));
        }
    }

    private ModelAndView newPasswordForm(String token) {
        log.debug("API - New password request form");

        if (StringUtils.isBlank(token)) {
            return showMissingTokenForm();
        } else {
            try {
                return showNewPasswordForm(token);
            } catch (NoRecordException ex) {
                return showInvalidTokenForm();
            } catch (RequiredPropertyNullException ex) {
                return showMissingTokenForm();
            }
        }
    }

    private ModelAndView changePasswordForm(FileItemIterator iter) {
        log.debug("API - New password sent");

        String newPassword = null;
        String key = null;

        try {
            while (iter.hasNext()) {
                FileItemStream item = iter.next();
                if (item.isFormField()) {
                    if (PASSWORD.equals(item.getFieldName())) {
                        newPassword = getFromFormItem(item);
                    } else if (KEY.equals(item.getFieldName())) {
                        key = getFromFormItem(item);
                    } else if (CHANGE.equals(item.getFieldName())) {
                        // odesilaci tlacitko formu, nic s nim nepotrebujem
                    } else {
                        return showBadRequestForm();
                    }
                } else {
                    return showBadRequestForm();
                }
            }
        } catch (Exception ex) {
            return showBadRequestForm();
        }

        if (StringUtils.isBlank(newPassword) || StringUtils.isBlank(key)) {
            return showBadRequestForm();
        }

        try {
            UserLoginInfo loginInfo = userLoginInfoService.loadUserLoginInfoByLostToken(key);
            userLoginInfoService.updateLostPasswordWithoutSendMail(newPassword, loginInfo);
            
            log.debug("API - Password changed");
            
            return showPasswordChangedForm();
        } catch (NoRecordException ex) {
            return showInvalidTokenForm();
        } catch (RequiredPropertyNullException ex) {
            return showMissingTokenForm();
        }
    }

    private ModelAndView showMissingTokenForm() {
        return showErrorForm("Missing reset token");
    }

    private ModelAndView showInvalidTokenForm() {
        return showErrorForm("Invalid reset token");
    }

    /** neocekavany nebo chybejici vstup */
    private ModelAndView showBadRequestForm() {
        return showErrorForm("Bad request");
    }

    private ModelAndView showErrorForm(String errorMessage) {
        ModelAndView model = new ModelAndView(ERROR_JSP);
        model.addObject(ERROR_MESSAGE, errorMessage);
        return model;
    }

    private ModelAndView showNewPasswordForm(String token) throws RequiredPropertyNullException, NoRecordException {
        UserLoginInfo loginInfo = userLoginInfoService.loadUserLoginInfoByLostToken(token);

        ModelAndView model = new ModelAndView(USER_RESET_PASSWORD_JSP);
        model.addObject(KEY, loginInfo.getLostPasswordKey());
        return model;
    }

    private ModelAndView showPasswordChangedForm() {
        return new ModelAndView(PASSWORD_CHANGED_JSP);
    }

    /** vraci hodnotu ve formularovem policku */
    private String getFromFormItem(FileItemStream item) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(item.openStream(), writer);
        return writer.toString();
    }
}
