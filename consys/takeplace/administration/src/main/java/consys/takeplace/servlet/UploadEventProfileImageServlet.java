package consys.takeplace.servlet;

import consys.admin.event.core.bo.Event;
import consys.admin.event.core.service.EventService;
import consys.common.constants.img.EventProfileImageEnum;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ReadOnlyPropertyException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.servlet.AbstractUploadImageServlet;
import consys.common.utils.UuidProvider;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.MissingResourceException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Servlet zainstujuci upload profilovej fotky eventu
 * @author Palo
 */
public class UploadEventProfileImageServlet extends AbstractUploadImageServlet {

    private static final String EVENT_UUID = "eid";
    private static final long serialVersionUID = -7034685343729110365L;
    @Autowired
    private EventService eventService;

    @Override
    protected String saveImage(HttpServletRequest request,String name, BufferedImage bi, String contentType, String subContentType) throws NoRecordException, ServiceExecutionFailed {
        try {
            String eventUuid = (String) request.getParameter(EVENT_UUID);
            if (eventUuid == null) {
                throw new MissingResourceException("Missing", this.getClass().getName(), EVENT_UUID);
            }
            Event e = getEventService().loadEventDetails(eventUuid);
            
            String uuidPrefix = UuidProvider.getUuid();
            logger.debug("Uploading Event Profile Logo: {}", e);
            
            try {                
                // Event Web 
                int width = EventProfileImageEnum.WEB.getWidth();
                int height = EventProfileImageEnum.WEB.getHeight();                                                
                String iuuid = EventProfileImageEnum.WEB.getFullUuid(uuidPrefix);                
                createImage(width, height, bi, name, contentType, subContentType, iuuid);                
                
                // Profile
                width = EventProfileImageEnum.LIST.getWidth();
                height = EventProfileImageEnum.LIST.getHeight();                                                
                iuuid = EventProfileImageEnum.LIST.getFullUuid(uuidPrefix);                
                createImage(width, height, bi, name, contentType, subContentType, iuuid);                
                
                // Original                
                iuuid = EventProfileImageEnum.ORIGINAL.getFullUuid(uuidPrefix);
                createImage(bi, name, contentType, subContentType, iuuid);                               
            } catch (IOException ex) {
                throw new ServiceExecutionFailed();
            }
            
            String toDelete = e.getPortraitLogoPrefix();            
            e.setPortraitLogoPrefix(uuidPrefix);
            getEventService().updateEventDetails(e);
            // teraz zmazeme
            deleteImage(EventProfileImageEnum.ORIGINAL.getFullUuid(toDelete));
            deleteImage(EventProfileImageEnum.WEB.getFullUuid(toDelete));
            deleteImage(EventProfileImageEnum.LIST.getFullUuid(toDelete));
            
            return String.format("<logo>%s</logo>",uuidPrefix);                        
        } catch (ReadOnlyPropertyException ex) {
            throw new ServiceExecutionFailed();
        } catch (RequiredPropertyNullException ex) {
            throw new ServiceExecutionFailed();
        }
    }

    /**
     * @return the eventService
     */
    public EventService getEventService() {        
        return eventService;
    }
}
