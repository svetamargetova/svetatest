/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.sso.servlet;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.admin.user.core.service.UserLoginInfoService;
import consys.admin.user.core.service.UserService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.admin.common.server.security.SsoAuthenticationToken;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;

/**
 *
 * @author palo
 */
public abstract class SsoOAuthServletHandler {

    
    @Autowired
    private UserLoginInfoService loginInfoService;

    public UserLoginInfoService getLoginInfoService() {
        return loginInfoService;
    }
    
    
    
    public abstract boolean isStateTokenPartOfCallback();
    
    public abstract void handleLoginRedirect(HttpServletRequest req, HttpServletResponse resp, String callback, String token) throws SsoException;

    public abstract ClientSsoUserDetails handleCallback(HttpServletRequest req, HttpServletResponse resp) throws SsoException;

    public abstract UserLoginInfo loadUserLoginInfo(ClientSsoUserDetails ssoUserDetails) throws RequiredPropertyNullException, NoRecordException;
        
            
}
