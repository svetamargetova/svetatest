package consys.takeplace.json.handler;

import consys.admin.common.gwt.server.ContextProvider;
import consys.admin.user.core.bo.User;
import consys.admin.user.core.service.UserService;
import consys.admin.user.json.action.result.UserResultUtils;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.common.json.error.ErrorCode;
import consys.common.utils.UuidProvider;
import java.io.IOException;
import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionIdentifierAware;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoginActionHandler implements ActionHandler {

    private static final String FIELD_SESSION = "session";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_USERNAME = "username";
    private static final Logger logger = LoggerFactory.getLogger(LoginActionHandler.class);
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private ContextProvider contextProvider;

    @Override
    public String getActionName() {
        return "login";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        String username = action.getStringParam(PARAM_USERNAME);
        String password = action.getStringParam(PARAM_PASSWORD);
        try {
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
            authRequest.setDetails(new JsonAuthenticationDetails());
            Authentication authentication = authenticationManager.authenticate(authRequest);
            try {
                User user = userService.loadDetailedUserByUuid(contextProvider.getUserContext().getUserUuid());
                UserResultUtils.writeUser(result, user);
                result.writeStringField(FIELD_SESSION, UuidProvider.getUuid());
            } catch (RequiredPropertyNullException ex) {
                ErrorCode.error(result, ex, PARAM_USERNAME, PARAM_USERNAME);
            } catch (ServiceExecutionFailed ex) {
                ErrorCode.error(result, ex);
            } catch (NoRecordException ex) {
                ErrorCode.error(result, ex);
            }
        } catch (AuthenticationException e){
            logger.error("Error while authenticating user by JSON:", e);
            result.writeError(255, e.getMessage());
            
        } catch (Exception e) {
            logger.error("Error while login user by JSON:", e);
            result.writeError(255, "Internal server error");
        }
    }

    public class JsonAuthenticationDetails implements SessionIdentifierAware, Serializable {

        private static final long serialVersionUID = -7965148597515746142L;
        //~ Instance fields ================================================================================================
        private String sessionUuid;

        public JsonAuthenticationDetails() {
        }

        //~ Methods ========================================================================================================
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof JsonAuthenticationDetails) {
                JsonAuthenticationDetails rhs = (JsonAuthenticationDetails) obj;


                if ((sessionUuid == null) && (rhs.getSessionId() != null)) {
                    return false;
                }

                if ((sessionUuid != null) && (rhs.getSessionId() == null)) {
                    return false;
                }

                if (sessionUuid != null) {
                    if (!sessionUuid.equals(rhs.getSessionId())) {
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        /**
         * Indicates the
         * <code>HttpSession</code> id the authentication request was received
         * from.
         *
         * @return the session ID
         */
        @Override
        public String getSessionId() {
            return sessionUuid;
        }

        @Override
        public int hashCode() {
            int code = 7654;

            if (this.sessionUuid != null) {
                code = code * (this.sessionUuid.hashCode() % 7);
            }

            return code;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(super.toString()).append(": ");
            sb.append("SessionId: ").append(this.getSessionId());

            return sb.toString();
        }
    }
}
