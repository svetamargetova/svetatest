/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.sso.twitter;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.constants.sso.SSO;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.admin.common.server.security.SsoAuthenticationToken;
import consys.takeplace.security.sso.servlet.SsoException;
import consys.takeplace.security.sso.servlet.SsoOAuthServletHandler;
import consys.takeplace.security.sso.servlet.SsoUserDeniedException;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author palo
 */
public class TwitterOAuthServletHandler extends SsoOAuthServletHandler {

    private static final Logger logger = LoggerFactory.getLogger(TwitterOAuthServletHandler.class);
    private static final String CONSUMER_KEY = "UQ7o6XkY83qex6rfikO1w";
    private static final String CONSUMER_SECRET = "OXpXGpNnpPNUjGGPeOpHt7cuE3kVUGF5ZfZUmYHSE";
    private final TwitterFactory twitterFactory;

    public TwitterOAuthServletHandler() {
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setOAuthConsumerKey(CONSUMER_KEY);
        configurationBuilder.setOAuthConsumerSecret(CONSUMER_SECRET);
        twitterFactory = new TwitterFactory(configurationBuilder.build());
    }

    @Override
    public void handleLoginRedirect(HttpServletRequest request, HttpServletResponse response, String callback, String token) throws SsoException {
        try {
            Twitter twitter = twitterFactory.getInstance();

            RequestToken requestToken = twitter.getOAuthRequestToken(callback);

            request.getSession().setAttribute("twitterRequestToken", requestToken);
            request.getSession().setAttribute("twitterInstance", twitter);
            
            
            response.sendRedirect(requestToken.getAuthenticationURL());

        } catch (TwitterException e) {
            logger.error("Failed to login by Twitter", e);
            throw new SsoException();
        } catch (IOException e) {
            logger.error("Failed to redirect ", e);
            throw new SsoException();
        }

    }

    @Override
    public ClientSsoUserDetails handleCallback(HttpServletRequest request, HttpServletResponse response) throws SsoException {
        final RequestToken requestToken = (RequestToken) request.getSession().getAttribute("twitterRequestToken");
        final Twitter twitter = (Twitter) request.getSession().getAttribute("twitterInstance");
        try {
            if (StringUtils.isNotBlank(request.getParameter("denied"))) {
                throw new SsoUserDeniedException();
            }
            AccessToken token = twitter.getOAuthAccessToken(requestToken, request.getParameter("oauth_verifier"));            
            ClientSsoUserDetails sso = new ClientSsoUserDetails();            
            User user = twitter.verifyCredentials();
            sso.setSsoId(token.getUserId());

            if (user.getName().contains(" ")) {
                String[] names = user.getName().split(" ");
                sso.setFirstName(names[0]);
                sso.setLastName(user.getName().substring(names[0].length() + 1));
            } else {
                sso.setFirstName(" ");
                sso.setLastName(user.getName());
            }
            sso.setProfilePictureUrl(user.getProfileImageUrlHttps().toString());
            return sso;
        } catch (TwitterException e) {
            logger.error("Failed to login by Twitter", e);
            throw new SsoException();
        } finally {
            request.getSession().removeAttribute("twitterRequestToken");
            request.getSession().removeAttribute("twitterInstance");
        }

    }

    @Override
    public UserLoginInfo loadUserLoginInfo(ClientSsoUserDetails ssoUserDetails) throws RequiredPropertyNullException, NoRecordException {
        return getLoginInfoService().loadUserLoginInfoByTwitterId(ssoUserDetails.getSsoId());
    }

    @Override
    public boolean isStateTokenPartOfCallback() {
        return true;
    }

   
}
