/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.sso.gmail;

import consys.admin.user.core.bo.UserLoginInfo;
import consys.common.constants.sso.SSO;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.takeplace.security.sso.servlet.SsoException;
import consys.takeplace.security.sso.servlet.SsoOAuthServletHandler;
import consys.takeplace.security.sso.servlet.SsoUserDeniedException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author palo
 */
public class GoogleOAuthServletHandler extends SsoOAuthServletHandler {

    private static final Logger logger = LoggerFactory.getLogger(GoogleOAuthServletHandler.class);
    private static final String APP_ID_PARAM = "sso.oauth.google.id";
    private static final String APP_SECRET_PARAM = "sso.oauth.google.secret";
    private final String APP_ID;
    private final String APP_SECRET;
    private final JsonFactory jsonFactory = new MappingJsonFactory();
    private final URLCodec codec = new URLCodec("UTF-8");

    public GoogleOAuthServletHandler(ServletConfig config) {

        APP_ID = config.getInitParameter(APP_ID_PARAM);
        APP_SECRET = config.getInitParameter(APP_SECRET_PARAM);
        if (APP_ID == null || APP_ID.startsWith("$") || APP_SECRET == null || APP_SECRET.startsWith("$")) {
            logger.error("ALERT! Missing GOOGLE OAUTH PROPERTIES");
        }

    }

    @Override
    public void handleLoginRedirect(HttpServletRequest request, HttpServletResponse response, String callback, String token) throws SsoException {
        try {

            String redirect = null;
            try {
                callback = codec.encode(callback);
                redirect = String.format("https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=%s&redirect_uri=%s&state=%s&scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email", APP_ID, callback, token);
            } catch (EncoderException ex) {
                logger.error("Error while encoding callback", ex);
            }

            request.getSession().setAttribute("googleCallback", callback);
            response.sendRedirect(redirect);
        } catch (IOException e) {
            logger.error("Failed to redirect ", e);
            throw new SsoException();
        }

    }

    @Override
    public ClientSsoUserDetails handleCallback(HttpServletRequest request, HttpServletResponse response) throws SsoException {

        try {
            String code = request.getParameter("code");
            if (StringUtils.isBlank(code)) {
                // error
                String error = request.getParameter("error");
                logger.error("Error occured when authorizing user'{}'", error);
                throw new SsoUserDeniedException();

            }

            String callback = (String) request.getSession().getAttribute("googleCallback");
            String accessTokenUrl = String.format("https://accounts.google.com/o/oauth2/token");

            try {
                String params = String.format("client_id=%s&redirect_uri=%s&client_secret=%s&code=%s&grant_type=authorization_code", APP_ID, callback, APP_SECRET, code);

                URL url = new URL(accessTokenUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setInstanceFollowRedirects(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("charset", "utf-8");
                connection.setRequestProperty("Content-Length", Integer.toString(params.getBytes().length));
                connection.setUseCaches(false);

                OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
                osw.write(params);
                osw.flush();

                InputStream is = connection.getInputStream();
                String accessTokenResponse = IOUtils.toString(is);

                JsonParser accessTokenParser = jsonFactory.createJsonParser(accessTokenResponse);
                JsonNode tokenNode = accessTokenParser.readValueAsTree().get("access_token");
                if (tokenNode == null) {
                    logger.info("Error retrieve access token {}", accessTokenResponse);
                    throw new SsoException();
                }

                final String accessToken = tokenNode.getTextValue();


                URL userInfo = new URL(String.format("https://www.googleapis.com/oauth2/v1/userinfo?access_token=%s", accessToken));
                is = userInfo.openStream();

                JsonParser jp = jsonFactory.createJsonParser(is);
                JsonNode root = jp.readValueAsTree();


                ClientSsoUserDetails ssoUser = new ClientSsoUserDetails();
                ssoUser.setFromSso(SSO.GOOGLE_PLUS);

                ssoUser.setSsoStringId(root.get("id").getTextValue());
                ssoUser.setFirstName(root.get("given_name").getTextValue());
                ssoUser.setLastName(root.get("family_name").getTextValue());
                ssoUser.setLoginEmail(root.get("email").getTextValue());

                String gender = getString("gender", root);

                if ("female".equalsIgnoreCase(gender)) {
                    ssoUser.setGender(2);
                } else if ("male".equalsIgnoreCase(gender)) {
                    ssoUser.setGender(1);
                } else {
                    ssoUser.setGender(0);
                }
                return ssoUser;
            } catch (IOException ex) {
                logger.error("Failed to open stream", ex);
                throw new RuntimeException("There was an error processing Facebook Authentication", ex);
            }
        } finally {
            request.getSession().removeAttribute("googleCallback");
        }
    }

    private String getString(String key, JsonNode node) {
        JsonNode t = node.get(key);
        if (t != null) {
            return t.getTextValue();
        } else {
            return null;
        }
    }

    @Override
    public UserLoginInfo loadUserLoginInfo(ClientSsoUserDetails ssoUserDetails)
            throws RequiredPropertyNullException, NoRecordException {
        return getLoginInfoService().loadUserLoginInfoByGoogleId(ssoUserDetails.getSsoStringId(),ssoUserDetails.getLoginEmail());
    }

    @Override
    public boolean isStateTokenPartOfCallback() {
        return false;
    }
}
