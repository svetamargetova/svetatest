package consys.takeplace.gwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import consys.admin.event.gwt.client.module.EventModule;
import consys.common.gwt.client.rpc.action.IsUserLoggedAction;
import consys.admin.user.gwt.client.base.FAQContent;
import consys.admin.user.gwt.client.base.RegisterContent;
import consys.admin.user.gwt.client.module.UserModule;
import consys.admin.user.gwt.client.panel.JoinPanel;
import consys.admin.user.gwt.client.panel.LoginPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventEnterRequest;
import consys.common.gwt.client.event.UserLoginEvent;
import consys.common.gwt.client.event.UserLogoutEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.messaging.module.MessagingModule;
import consys.common.gwt.client.module.ModuleRegistrator;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.event.FireAfterLoginEvent;
import consys.common.gwt.client.ui.layout.menu.MenuDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;
import consys.common.gwt.client.widget.CommonWidgetsModule;
import consys.event.b2b.gwt.client.module.B2BModule;
import consys.event.common.gwt.client.module.EventCoreModule;
import consys.event.common.gwt.client.module.OverseerModule;
import consys.event.conference.gwt.client.module.ConferenceModule;
import consys.event.registration.gwt.client.module.RegistrationModule;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author pepa
 */
public class TakeplaceEntryPoint implements EntryPoint {

    // konstanty
    private static final String COMMAND = "cmd";

    /**
     * Hlavny entry point Aplikacie. Najskor sa musia zaregistrovat moduly a
     * potom sa inicializuje obsah
     */
    @Override
    public void onModuleLoad() {
        LoggerFactory.log(TakeplaceEntryPoint.class, "TakeplaceEntryPoint: start");

        fireGETParametersToHistoryToken();

        // Inicializuje Layout
        LayoutManager layoutManager = LayoutManager.get();
        RootPanel.get().add(layoutManager);
        MenuDispatcher.get();

        // >>>> MODULE REGISTRACE  <<<<
        // Administracne moduly
        ModuleRegistrator.get().registerInModule(
                new UserModule(),
                new EventModule(),
                new MessagingModule()/*,
                 new UserMessagingModule()*/);

        // Moduly obecne spustene
        ModuleRegistrator.get().registerOutModule(
                new EventCoreModule());

        // Event Moduly
        ModuleRegistrator.get().registerInEventModule(
                new OverseerModule(),
                new ConferenceModule(),
                new RegistrationModule(),
                //new SchedulingModule(),
                new B2BModule());

        // zinicializovani PanelDispatcheru a vlozeni panelu
        final PanelDispatcher pd = PanelDispatcher.get();

        pd.addLogoutPanelItem(new JoinPanel());
        pd.addLogoutPanelItem(new LoginPanel());

        MenuDispatcher.get().setSignUpWidget(new RegisterContent());
        MenuDispatcher.get().setFAQWidget(new FAQContent());

        // Zisti ci je user prihlaseny
        ActionExecutor.execute(new IsUserLoggedAction(), new AsyncCallback<BooleanResult>() {
            @Override
            public void onFailure(Throwable caught) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void onSuccess(BooleanResult result) {
                if (result.isBool()) {
                    String token = History.getToken();
                    if (token != null && !token.isEmpty()
                            && (token.startsWith(EventEnterRequest.EVENT) || token.startsWith(CommonWidgetsModule.REGISTER_ACCOUNT))) {
                        EventBus.get().fireEvent(new FireAfterLoginEvent(token));
                    }

                    // presmerovani pro sso uzivatele
                    String cookie = Cookies.getCookie(UserModule.SSO_LOGIN_COOKIE);
                    if (cookie != null) {
                        EventBus.get().fireEvent(new FireAfterLoginEvent(cookie));
                    }

                    Cache.get().register(Cache.LOGGED, Boolean.TRUE);
                    EventBus.get().fireEvent(new UserLoginEvent());
                } else {
                    Cache.get().register(Cache.LOGGED, Boolean.FALSE);
                    EventBus.get().fireEvent(new UserLogoutEvent());
                    Cache.get().register(Cache.LOGGED, Boolean.FALSE);
                }
            }
        }, null);
    }

    /** specialne kvuli IE predelavka zpracovani vstupnich parametru, co maji byt v history tokenu */
    private void fireGETParametersToHistoryToken() {
        Map<String, List<String>> parameters = Window.Location.getParameterMap();
        List<String> command = parameters.get(COMMAND);
        if (command != null) {
            String historyToken = command.get(0) + "?";
            for (Entry<String, List<String>> e : parameters.entrySet()) {
                if (!e.getKey().equals(COMMAND)) {
                    historyToken += e.getKey() + "=" + e.getValue().get(0) + "&";
                }
            }
            if (historyToken.endsWith("&")) {
                historyToken = historyToken.substring(0, historyToken.length() - 1);
            }
            History.newItem(historyToken);
        }
    }
}
