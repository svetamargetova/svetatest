/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.takeplace.security.filter;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.web.DefaultRedirectStrategy;


/**
 *
 * @author palo
 */
public class GwtRedirectStrategy extends DefaultRedirectStrategy{

    private String gwtFieldName;
    private String eventUUID;
    
    @Override
    public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
        String field = request.getParameter(getGwtRequestTag());
        String event = request.getParameter(getEventRedirectTag());
        if (event != null && event.length() > 0) {
            url = url+"?"+getEventRedirectTag()+"="+event;
        }

        if (field != null && Boolean.parseBoolean(field)) {
            logger.debug("Logging in from GWT " + url);
            // V pripade ze je call od z GWT tak nepresmerujeme len posleme kam sa ma presmerovat
            response.getOutputStream().print(url);
        } else {
            logger.debug("Logging in from outside " + url);
            super.sendRedirect(request, response, url);
        }
    }

    
    
    
    /**
     * @return the gwtFieldName
     */
    public String getGwtRequestTag() {
        return gwtFieldName;
    }

    /**
     * @param gwtFieldName the gwtFieldName to set
     */
    public void setGwtRequestTag(String gwtFieldName) {
        this.gwtFieldName = gwtFieldName;
    }

    /**
     * @return the eventUUID
     */
    public String getEventRedirectTag() {
        return eventUUID;
    }

    /**
     * @param eventUUID the eventUUID to set
     */
    public void setEventRedirectTag(String eventUUID) {
        this.eventUUID = eventUUID;
    }

    
    
}
