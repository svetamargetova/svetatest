--------------------------------------------------------------------------------
-- Vytvoreni indexu pro administraci
--------------------------------------------------------------------------------

-- EVENT
CREATE INDEX from_date_idx
  ON event
  USING btree
  (from_date);

CREATE INDEX to_date_idx
  ON event
  USING btree
  (to_date);

-- USER
CREATE INDEX username_idx
  ON users
  USING btree
  (email);

CREATE INDEX user_private_uo_idx
  ON users
  USING btree
  (id_private_user_organization);

CREATE INDEX user_default_uo_idx
  ON users
  USING btree
  (id_default_user_organization);


-- USER_ORGANIZATION
CREATE INDEX user_idx
  ON user_organization
  USING btree
  (id_user);

-- USER_ACTIVATION
CREATE INDEX user_info_idx
  ON user_activation
  USING btree
  (id_login_info);

-- EVENT USER_ORGANIZATION
CREATE INDEX user_organization_idx
  ON event_user_organization
  USING btree
  (id_uo);

CREATE INDEX event_idx
  ON event_user_organization
  USING btree
  (id_event);