SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;




--
-- Pending activation
--
UPDATE system_property SET value='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Pending account activation</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>thank you for registering with Takeplace! Before you get started you need to activate your&nbsp;account.</p>
                <p>Please click the following link:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount&code=$activation_key$">https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount&code=$activation_key$</a></div>
                <p>Alternativelly, you can access the page</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount">https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount</a></div>
                <p>and enter the activation key: <b>$activation_key$</b></p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='user_activation_request_email_text';
UPDATE system_property SET value='Takeplace - Account activation request' where key='user_activation_request_email_title';

--
-- Lost Password - Changed
--
UPDATE system_property SET value='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Your new password</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>per your request we have changed your password.</p>
                <p>Please click the following link to log in:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=LogIn">https://app.takeplace.eu/takeplace/takeplace.html?cmd=LogIn</a></div>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='user_lost_pass_email_changed_text';
UPDATE system_property SET value='Takeplace - Your new password' where key='user_lost_pass_email_changed_title';

--
-- Lost Password - Request
--
UPDATE system_property SET value='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Reset password</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>We received a request to reset the password associated with this e-mail address. If you made this request, please click the link below to reset your password using our secure server:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=ChangePassword&token=$token$">https://app.takeplace.eu/takeplace/takeplace.html?cmd=ChangePassword&token=$token$</a></div>
                <p>If you did not request to have your password reset you can safely ignore this email.</p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there. Once you have returned to takeplace.eu, we will give instructions for resetting your password.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>z' where key='user_lost_pass_email_text';
UPDATE system_property SET value='Takeplace - Reset password' where key='user_lost_pass_email_title';

--
-- Aktivace Accountu
--
UPDATE system_property SET value='<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Account activated</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>your Takeplace account has been activated and you can start using it right now.</p>
                <p>Please click the following link to log in:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=LogIn">https://app.takeplace.eu/takeplace/takeplace.html?cmd=LogIn</a></div>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='user_activation_email_text';
UPDATE system_property SET value='Takeplace - Account activated' where key='user_activation_email_title';

--
-- Registrace Accountu
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Account registration</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>thank you for registering with Takeplace! Before you get started you need to activate your&nbsp;account.</p>
                <p>Please click the following link:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount&code=$activation_key$">https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount&code=$activation_key$</a></div>
                <p>Alternativelly, you can access the page</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount">https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount</a></div>
                <p>and enter the activation key: <b>$activation_key$</b></p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='user_registration_email_text';
UPDATE system_property SET value='Takeplace - Account registration' where key='user_registration_email_title';

--
-- Invitation - Accepted - Invited
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Invitation to $event_name$ accepted</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>you have joined $event_name$ at Takeplace.</p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='event_invitation_accepted_invited_text';
UPDATE system_property SET value='Takeplace - Invitation to $event_name$ accepted' where key='event_invitation_accepted_invited_title';

--
-- Invitation - Accepted - Invitor
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Invitation to $event_name$ accepted by $invited_name$</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>$invited_name$ ($invited_email$) has accepted your invitation to $event_name$ at Takeplace.</p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='event_invitation_accepted_invitor_text';
UPDATE system_property SET value='Takeplace - Invitation to $event_name$ accepted by $invited_name$' where key='event_invitation_accepted_invitor_title';

--
-- Invitation - Declined
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - Invitation to $event_name$ not accepted by $invited_name$</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>$invited_name$ ($invited_email$) did not accept your invitation to $event_name$ at Takeplace.</p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='event_invitation_declined_text';
UPDATE system_property SET value='Takeplace - Invitation to $event_name$ not accepted by $invited_name$' where key='event_invitation_declined_title';


--
-- Invitation - Existing User - Confirm
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - You have been invited to $event_name$</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>you have been invited by $from$ ($from_email$) to join $event_name$ at Takeplace.</p>
                <p>To accept the invitation please click the following link to log in:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=Invitations">https://app.takeplace.eu/takeplace/takeplace.html?cmd=Invitations</a></div>
                <p>Alternativelly, you can access the page</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=LogIn">https://app.takeplace.eu/takeplace/takeplace.html?cmd=LogIn</a></div>
                <p>and proceed into Events - Invitations.</p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>$message$</p>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='event_user_confirm_invitation_text';
UPDATE system_property SET value='Takeplace - You have been invited to $event_name$' where key='event_user_confirm_invitation_title';

--
-- Invitation - Existing User - Without Confirm
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - You have been invited to $event_name$</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>you have been invited by $from$ ($from_email$) to join $event_name$ at Takeplace.</p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>$message$</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='event_user_without_confirm_invitation_text';
UPDATE system_property SET value='Takeplace - You have been invited to $event_name$' where key='event_user_without_confirm_invitation_title';


--
-- Invitation - New User - Confirm
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - You have been invited to $event_name$</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>you have been invited by $from$ ($from_email$) to join $event_name$ at Takeplace.</p>
                <p>Takeplace (<a href="http://www.takeplace.eu">www.takeplace.eu</a>) is the leading web service for online event management and maintenance of professional communities. </p>
                <p>Before accepting the invitation please click the following link. You will be asked to complete your personal profile to activate your account:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=Invitation&token=$token$">https://app.takeplace.eu/takeplace/takeplace.html?cmd=Invitation&token=$token$</a></div>
                <p>Alternativelly, you can access the page:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount">https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount</a></div>
                <p>and enter the invitation key: <b>$token$</b></p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>$message$</p>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='event_new_user_confirm_invitation_text';
UPDATE system_property SET value='Takeplace - You have been invited to $event_name$' where key='event_new_user_confirm_invitation_title';

--
-- Invitation - New User - Without Confirm
--
UPDATE system_property SET value= '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Takeplace - You have been invited to $event_name$</title>
        <style type="text/css">
            body{background-color:#e6e7e8;color:#000000;font-family:Arial,Verdana;font-size:13px;}
            p{margin:15px 24px 10px 24px;width:570px;line-height:1.3;}
            a{color:#0a445c;text-decoration:underline;}
            a:hover{color:#0a445c;text-decoration:none;}
            .algn{text-align:center;}
            .m{background-color:#ffffff;border:1px solid #0a435c;width:618px;text-align:left;margin:0 auto 0 auto;}
            .he{background-color:#328ab3;color:#ffffff;font-weight:bold;padding:4px 24px 4px 24px;font-size:18px;}
            .hi{background-color:#d9efb9;border:1px solid #7bab00;margin:0 24px 0 24px;font-size:12px;padding:12px;}
            .hi a{color:#000000;text-decoration:none;}
            .hi a:hover{color:#000000;text-decoration:underline;}
            .de{margin:0 0 0 52px;}
            a.ta{text-decoration:none;}
            a:hover.ta{text-decoration:underline;}
            .fo{color:#666666;}
            .bo{width:570px;margin:5px auto 10px auto;font-size:12px;color:#444444;text-align:left;}
        </style>
    </head>
    <body>
        <div class="algn">
            <div></div>
            <div class="m">
                <div class="he">www.takeplace.eu</div>
                <p>Dear $salutation$,</p>
                <p>you have been invited by $from$ ($from_email$) to join $event_name$ at Takeplace.</p>
                <p>Takeplace (<a href="http://www.takeplace.eu">www.takeplace.eu</a>) is the leading web service for online event management and maintenance of professional communities. </p>
                <p>To complete your Takeplace account please click the following link to complete your personal profile and log in:</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=Invitation&token=$token$">https://app.takeplace.eu/takeplace/takeplace.html?cmd=Invitation&token=$token$</a></div>
                <p>Alternativelly, you can access the page</p>
                <div class="hi"><a href="https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount">https://app.takeplace.eu/takeplace/takeplace.html?cmd=ActivateAccount</a></div>
                <p>and enter the invitation key: <b>$token$</b></p>
                <p>Your account details:</p>
                <div class="de">Name: <b>$salutation$</b></div>
                <div class="de">E-mail: <b>$email$</b></div>
                <p>$message$</p>
                <p>If clicking the link doesn&#145;t seem to work, you can copy and paste the link into your browser&#145;s address window, or retype it there.</p>
                <p>Best regards,</p>
                <p>Takeplace Team<br><a href="http://www.takeplace.eu" class="ta">www.takeplace.eu</a></p>
                <p class="fo">Please do not reply to this message. The email address is used for email notifications only so you will receive no response. If you have any question, please contact <a href="mailto:support@takeplace.eu">Takeplace support</a>.</p>
            </div>
            <div class="bo">This message was addressed to $email$. Please read our <a href="http://www.takeplace.eu/terms_of_use_en.pdf">Terms&nbsp;of&nbsp;use</a>.</div>
        </div>
    </body>
</html>' where key='event_new_user_without_confirm_invitation_text';
UPDATE system_property SET value='Takeplace - You have been invited to $event_name$' where key='event_new_user_without_confirm_invitation_title';





