package consys.event.conference.json.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import consys.event.conference.core.service.SubmissionService;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionTypesOptionsActionHandler implements ActionHandler {

    @Autowired
    private SubmissionService submissionService;

    @Override
    public String getActionName() {
        return "submission-options";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        result.writeArrayFieldStart("options");                        
        try {
            List<SubmissionTypeThumb> options = submissionService.listSubmissionTypesThumbs(false);
            for (SubmissionTypeThumb item : options) {
                result.writeStartObject();
                result.writeStringField("uuid", item.getUuid());
                result.writeStringField("title", item.getName());
                result.writeStringField("description", item.getDescription());                
                result.writeEndObject();
            }
        } catch (NoRecordException ex) {
        }
        result.writeEndArray();
    }
}
