/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.api.list;

/**
 *
 * @author palo
 */
public interface CommonFilters {
    
    /** Zobrazi vsetko */
    public static final int Filter_ALL = 0;
    
    /** Filter podla typu prispevku - UUID typu je vlozene ako string hodnota filtru*/
    public static final int Filter_TYPE = 1;
    
    /** Filter stavu */
    public static final int Filter_STATE = 2;
    
    public enum States{    
        PENDING,ACCEPTED,REJECTED;        
    }        
}
