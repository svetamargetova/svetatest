package consys.event.conference.api.right;

import consys.event.common.api.right.ModuleRoleModel;



/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventConferenceModuleRoleModel extends ModuleRoleModel {

    // Pravo recenzovat
    public static final String RIGHT_SUBMISSION_REVIEWER = "RIGHT_SUBMISSION_REVIEWER";
    // Pravo vlozit prispevky s internym typom
    public static final String RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS = "RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS";
    // Pravo nastavovat submission module
    public static final String RIGHT_SUBMISSION_SETTINGS = "RIGHT_SUBMISSION_SETTINGS";
    // Pravo priradovat recenzentov k prispevkom
    public static final String RIGHT_SUBMISSION_ASSING_REVIEWERS = "RIGHT_SUBMISSION_ASSIGN_REVIEWERS";
    // Pravo priradovat recenzentov k prispevkom
    public static final String RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS = "RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS";

    public EventConferenceModuleRoleModel() {
        super(2);
    }
 
    @Override
    public void registerRights() {
        addRole(RIGHT_SUBMISSION_REVIEWER,true);
        addRole(RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS,true);
        addRole(RIGHT_SUBMISSION_SETTINGS,true);
        addRole(RIGHT_SUBMISSION_ASSING_REVIEWERS,true);
        addRole(RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS,true);
    }
}
