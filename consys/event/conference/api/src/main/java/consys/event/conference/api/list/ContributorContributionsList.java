package consys.event.conference.api.list;

/**
 * Zoznam poslanych prispevkov.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ContributorContributionsList extends CommonOrderers, CommonFilters{
    public static final String TAG = "7ac9896251d9b50c3535a79d6fcc3418";

    
}
