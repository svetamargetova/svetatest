package consys.event.conference.api.servlet;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ConferenceSubmissionArtefactUploadServlet {

    public static final String NEW_SUBMISSION_ARTEFACT_URL = "/upload/artefact/new";
    public static final String UPDATE_SUBMISSION_ARTEFACT_URL = "/upload/artefact/update";
    
    
    public static final String FIELD_ARTEFACT_TYPE = "atype";
    public static final String FIELD_SUBMISSION_UUID = "suuid";
    public static final String FIELD_FILE = "file";
}
