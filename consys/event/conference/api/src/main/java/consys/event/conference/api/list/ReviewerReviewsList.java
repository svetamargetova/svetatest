package consys.event.conference.api.list;

/**
 * Zoznam recenzovanych prispevkov
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewerReviewsList extends CommonOrderers, CommonFilters {

    public static final String TAG = "a8b2cc0474fddea5d2627c4b6aa452bb";
        
}
