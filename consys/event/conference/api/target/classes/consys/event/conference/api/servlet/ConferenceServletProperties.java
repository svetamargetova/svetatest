package consys.event.conference.api.servlet;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ConferenceServletProperties {

    /**
     * Konstanta pre upload templatov(sablon)
     *
     * <i> "http://www.takeplace.eu/o/o1/upload/template"[event_filter_tags]</i>
     */
    public static final String UPLOAD_TEMPLATE = "/upload/template";
    /**
     * Konstanta pre upload patternov(vzorov)
     *
     * <i> "http://www.takeplace.eu/o/o1/upload/pattern"[event_filter_tags]</i>
     */
    
}
