package consys.event.conference.api.properties;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ConferenceProperties {
    // SYSTEMOVE PROPERTY
    public static final String SYSTEM_PROPERTY_VISIBLE_AUTHORS = "conference_author_review_visible";    
    public static final String SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER = "conference_submission_max_reviews";
    
}
