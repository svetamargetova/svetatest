package consys.event.conference.api.list;

/**
 * Zoznam vsetkych vsetkych prispevkov a vyjadrenie zaujmu recenzenta o recenzo-
 * vanie.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface BidForReviewList {

    public static final String TAG = "7eedf5c795f7c5e50d51d8863d7e03cb";

    /** Serazeni podla titulky */
    public static final int Order_TITLE = 1;

    /** Serazeni podla datumu poslania prispevku */
    public static final int Order_SUBMIT_DATE = 2;

    /** Zoradenie podla zaujmu - Like To > Don't Like > Conflict > Not Care */
    public static final int Order_INTEREST = 3;


    /**
     * Filter . na zaklade zaujmu. Defaultne je ALL:
     * ALL(4),LIKE(3),DONT_LIKE(2),CONFLICT(1),NOT_CARE(0);
     */
    public static final int Filter_INTEREST = 1;

}
