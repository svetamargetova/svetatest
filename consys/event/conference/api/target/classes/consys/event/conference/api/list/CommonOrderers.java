/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.api.list;

/**
 * Konstanty pre obecne radenia
 * @author palo
 */
public interface CommonOrderers {
     /** Serazeni podla titulky */
    public static final int Order_TITLE = 1;

    /** Serazeni podla datumu poslania prispevku */
    public static final int Order_SUBMIT_DATE = 2;
    
    /** Serazeni podla skore prispevku */
    public static final int Order_SCORE = 3;
}
