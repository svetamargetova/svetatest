package consys.event.conference.gwt.client.comp.evaluator;

import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;

/**
 * Datovy objekt pro EvaluatorItem, ktery take urcuje jak se ma komponenta chovat
 * @author pepa
 */
public class EvaluatorItemData<T> {

    private boolean isValue;
    private String name;
    private EvaluationItemValue score;
    private ConsysActionWithValue<EvaluatorItemData<T>> action;
    private T value;

    public ConsysActionWithValue<EvaluatorItemData<T>> getAction() {
        return action;
    }

    public void setAction(ConsysActionWithValue<EvaluatorItemData<T>> action) {
        this.action = action;
    }

    public boolean isIsValue() {
        return isValue;
    }

    public void setIsValue(boolean isValue) {
        this.isValue = isValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EvaluationItemValue getScore() {
        return score;
    }

    public void setScore(EvaluationItemValue score) {
        this.score = score;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
