package consys.event.conference.gwt.client.bo.evaluation;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * 
 * @author pepa
 */
public class ReviewScoreAndCommentsData implements IsSerializable {

    // data
    private String reviewUuid;
    private String reviewerName;
    private String reviewerOrganization;
    private String reviewerPosition;
    private String reviewerImageUuid;
    private String publicComment;
    private String privateComment;
    // overall -> len hodnota, ostatne -> uuid recenze
    private Evaluation<EvaluationValue, EvaluationValue> evaluation;

    public Evaluation<EvaluationValue, EvaluationValue> getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Evaluation<EvaluationValue, EvaluationValue> evaluation) {
        this.evaluation = evaluation;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    public String getPublicComment() {
        return publicComment;
    }

    public void setPublicComment(String publicComment) {
        this.publicComment = publicComment;
    }

    public String getReviewerOrganization() {
        return reviewerOrganization;
    }

    public void setReviewerOrganization(String reviewerAffiliation) {
        this.reviewerOrganization = reviewerAffiliation;
    }

    public String getReviewerImageUuid() {
        return reviewerImageUuid;
    }

    public void setReviewerImageUuid(String reviewerImageUuid) {
        this.reviewerImageUuid = reviewerImageUuid;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    /**
     * @return the reviewUuid
     */
    public String getReviewUuid() {
        return reviewUuid;
    }

    /**
     * @param reviewUuid the reviewUuid to set
     */
    public void setReviewUuid(String reviewUuid) {
        this.reviewUuid = reviewUuid;
    }

    /**
     * @return the reviewerPosition
     */
    public String getReviewerPosition() {
        return reviewerPosition;
    }

    /**
     * @param reviewerPosition the reviewerPosition to set
     */
    public void setReviewerPosition(String reviewerPosition) {
        this.reviewerPosition = reviewerPosition;
    }
}
