package consys.event.conference.gwt.client.utils.css;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
public interface ECCssResources extends CssResource {

    String artefactSettingsUploadedFileItem();

    String artefactSettingsUploadFileHead();

    String assignHead();

    String assignHeadTitle();

    String assignLikeItem();

    String assignNotCareItem();

    String assignNotWantItem();

    String assignConflictItem();

    String assignNobody();

    String assignItemContent();

    String assignItemContentFirst();

    String assignCheckItem();

    String assignCheckItemContent();

    String assignReviewerName();

    String assignTopicIntersection();

    String assignLoad();

    String bfrCheckBox();

    String bfrSelectPanel();

    String noMargin();

    String uaLeft();

    String uaRight();

    String uaSelectPanel();

    String dowloadExTemFile();

    String bidForReviewTable();

    String controlCell();

    String evaluationItem();

    String evaluationItemClickable();

    String smallEvaluation1();

    String smallEvaluation2();

    String smallEvaluation3();

    String smallEvaluation4();

    String smallEvaluation5();

    String smallEvaluation6();

    String smallEvaluation7();

    String smallEvaluation8();

    String smallEvaluation9();

    String smallEvaluation10();

    String smallEvaluationNone();

    String mediumEvaluation1();

    String mediumEvaluation2();

    String mediumEvaluation3();

    String mediumEvaluation4();

    String mediumEvaluation5();

    String mediumEvaluation6();

    String mediumEvaluation7();

    String mediumEvaluation8();

    String mediumEvaluation9();

    String mediumEvaluation10();

    String mediumEvaluationNone();

    String bigEvaluation1();

    String bigEvaluation2();

    String bigEvaluation3();

    String bigEvaluation4();

    String bigEvaluation5();

    String bigEvaluation6();

    String bigEvaluation7();

    String bigEvaluation8();

    String bigEvaluation9();

    String bigEvaluation10();

    String bigEvaluationNone();

    String middleEvaluation1();

    String middleEvaluation2();

    String middleEvaluation3();

    String middleEvaluation4();

    String middleEvaluation5();

    String middleEvaluation6();

    String middleEvaluation7();

    String middleEvaluation8();

    String middleEvaluation9();

    String middleEvaluation10();

    String middleEvaluationNone();

    String bottomEvaluation1();

    String bottomEvaluation2();

    String bottomEvaluation3();

    String bottomEvaluation4();

    String bottomEvaluation5();

    String bottomEvaluation6();

    String bottomEvaluation7();

    String bottomEvaluation8();

    String bottomEvaluation9();

    String bottomEvaluation10();

    String bottomEvaluationNone();

    String smallEvaluationItem();

    String mediumEvaluationItem();

    String bigEvaluationItem();

    String middleOrBottomEvaluationItem();

    String smallScorePanelInteger09();

    String smallScorePanelInteger10();

    String smallScorePanelIntegerDecimal();

    String smallScorePanelNA();

    String mediumScorePanelInteger09();

    String mediumScorePanelInteger10();

    String mediumScorePanelIntegerDecimal();

    String mediumScorePanelNA();

    String bigScorePanelInteger09();

    String bigScorePanelInteger10();

    String bigScorePanelIntegerDecimal();

    String bigScorePanelNA();

    String middleOrBottomScorePanelInteger09();

    String middleOrBottomScorePanelInteger10();

    String smallEvaluationItemInteger();

    String smallEvaluationItemDecimal();

    String mediumEvaluationItemInteger();

    String mediumEvaluationItemDecimal();

    String bigEvaluationItemInteger();

    String bigEvaluationItemDecimal();

    String evaluationDisplay();

    String evaluationDisplayItems();

    String evaluationDisplayItemsMargin();

    String evaluationDisplayList();

    String evaluator();

    String evaluatorWrapperItem();

    String evaluatorResult();

    String evaluatorSelector();

    String evaluatorHead();

    String evaluatorPointsPanelWrapper();

    String evaluatorPointsPanel();

    String evaluatorEmpty();

    String evaluatorTitleText();

    String evaluatorTitle();

    String evaluatorTitleOver();

    String evaluatorTitleEnd();

    String evaluatorItemText();

    String evaluatorItemMiddle();

    String evaluatorItemMiddleOver();

    String evaluatorItemBottom();

    String evaluatorItemBottomOver();

    String evaluatorItemEnd();

    String newContributionForm();

    String newContributionFormHeadPanel();

    String newContributionFormHeadPanelContributionTypes();

    String newContributionFormArtefactsUploadPanel();

    String newContributionFormSubmitPanel();

    String reviewScoreAndCommentsProfile();

    String reviewScoreAndCommentsProfileText();

    String name();

    String affiliation();

    String separator();

    String separatorWrapper();

    String reviewerEvaluationTotalWrapper();

    String reviewerEvaluationCommentWrapper();

    String reviewerEvaluationCommentWrapperTitle();

    String reviewerEvaluationError();

    String reviewCycles();

    String reviewCyclesEmpty();

    String reviewTermOpen();

    String reviewTermClose();

    String displayWrapper();

    String displayInWrapper();

    String contributionState();

    String contributionStateAccepted();

    String contributionStateDeclined();

    String contributionStatePending();

    String contributionChairFormDecision();

    String contributionListItemWrapper();

    String contributionListItem();

    String contributionListItemDisplay();

    String contributorEditPanel();

    String contributorEditPanelRemove();

    String chairContributionsList();

    String myReviewsList();

    /* Contribution Head Panel */
    String contributionHeadPanel();

    String contributionHeadPanelVisibleEvaluation();

    String contributionHeadPanelNoEvaluation();

    String contributionHeadPanelStateAndControlPanel();

    String contributionHeadPanelStateForm();

    String contributionHeadPanelControlsPanel();

    String contributionHeadPanelControl();

    String reviewScoreAndComments();

    String addPublicAndPrivateComment();

    String myContributionReviews();

    String myContributionReviewsReview();

    String disabled();

    String artefactUpload();

    String artefactUploadFileUploaded();

    String artefactUploadUpdateFileNameBoxEnabled();

    String description();

    String uploadedTitle();

    String uploadedFiles();

    String wrapper();

    String changeUpload();

    String deleteUpload();

    String reviewer();

    String tags();

    String hand();

    String infoPanel();
}
