package consys.event.conference.gwt.client.module.chair;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.LoadIndicator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction;
import consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers;
import consys.event.conference.gwt.client.bo.setting.ClientReviewer;
import consys.event.conference.gwt.client.module.contribution.item.ContributionHeadPanel;
import consys.event.conference.gwt.client.module.contribution.item.HeadPanelReadInputFactory;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Formular pro prirazovani recenzentu k prispevku
 * @author pepa
 */
public class AssignmentReviewersForm extends SimpleFormPanel {

    // komponenty
    private SimplePanel headPanel;
    private FlowPanel likePanel;
    private FlowPanel notCarePanel;
    private FlowPanel notWantPanel;
    private FlowPanel conflictPanel;
    // data
    private String uuid;
    private ClientAssignReviewers assign;
    private int maxLoad;

    public AssignmentReviewersForm(String uuid) {
        super();
        this.uuid = uuid;
        try {
            maxLoad = Integer.parseInt((String) Cache.get().getSafe(ConferenceProperties.SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER));
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(ConferenceProperties.SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER));
            return;
        }
        init();
    }

    @Override
    protected void onLoad() {
        EventDispatchEvent loadAssign = new EventDispatchEvent(new ListContributionReviewersBidAction(uuid),
                new AsyncCallback<ClientAssignReviewers>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava EventActionExecutor
                    }

                    @Override
                    public void onSuccess(ClientAssignReviewers result) {
                        assign = result;
                        headPanel.setWidget(new ContributionHeadPanel(assign.getContributionHead(), new HeadPanelReadInputFactory(true), false));
                        clearPanels();
                        initItems();
                    }
                }, this);
        EventBus.get().fireEvent(loadAssign);
    }

    /** zinicializuje zakladni casti formulare */
    private void init() {
        likePanel = new FlowPanel();
        likePanel.setStyleName(ECResourceUtils.bundle().css().assignLikeItem());

        notCarePanel = new FlowPanel();
        notCarePanel.setStyleName(ECResourceUtils.bundle().css().assignNotCareItem());

        notWantPanel = new FlowPanel();
        notWantPanel.setStyleName(ECResourceUtils.bundle().css().assignNotWantItem());

        conflictPanel = new FlowPanel();
        conflictPanel.setStyleName(ECResourceUtils.bundle().css().assignConflictItem());

        FlowPanel titleLikePanel = new FlowPanel();
        titleLikePanel.setStyleName(ECResourceUtils.bundle().css().assignHead());
        titleLikePanel.add(StyleUtils.getStyledLabel(ECMessageUtils.c.assignmentReviewersForm_text_assignLikeTitle(),
                ECResourceUtils.bundle().css().assignHeadTitle()));

        FlowPanel titleNotCarePanel = new FlowPanel();
        titleNotCarePanel.setStyleName(ECResourceUtils.bundle().css().assignHead());
        titleNotCarePanel.add(StyleUtils.getStyledLabel(ECMessageUtils.c.assignmentReviewersForm_text_assignNotCareTitle(),
                ECResourceUtils.bundle().css().assignHeadTitle()));

        FlowPanel titleNotWantPanel = new FlowPanel();
        titleNotWantPanel.setStyleName(ECResourceUtils.bundle().css().assignHead());
        titleNotWantPanel.add(StyleUtils.getStyledLabel(ECMessageUtils.c.assignmentReviewersForm_text_assignNotWantTitle(),
                ECResourceUtils.bundle().css().assignHeadTitle()));

        FlowPanel titleConflictPanel = new FlowPanel();
        titleConflictPanel.setStyleName(ECResourceUtils.bundle().css().assignHead());
        titleConflictPanel.add(StyleUtils.getStyledLabel(ECMessageUtils.c.assignmentReviewersForm_text_assignConflictTitle(),
                ECResourceUtils.bundle().css().assignHeadTitle()));

        headPanel = new SimplePanel();

        addWidget(headPanel);
        addWidget(titleLikePanel);
        addWidget(likePanel);
        addWidget(titleNotCarePanel);
        addWidget(notCarePanel);
        addWidget(titleNotWantPanel);
        addWidget(notWantPanel);
        addWidget(titleConflictPanel);
        addWidget(conflictPanel);
    }

    /** vynuluje obsah panelu */
    private void clearPanels() {
        likePanel.clear();
        likePanel.add(nobody());

        notCarePanel.clear();
        notCarePanel.add(nobody());

        notWantPanel.clear();
        notWantPanel.add(nobody());

        conflictPanel.clear();
        conflictPanel.add(nobody());
    }

    /** zinicializuje jednotlive polozky formulare */
    private void initItems() {
        for (ClientReviewer cr : assign.getReviewers()) {
            FlowPanel panel = null;
            if (cr.getInterest() == null) {
                LoggerFactory.log(AssignmentReviewersForm.class, "null reviewer interest:" + cr.getUuid());
                continue;
            }
            switch (cr.getInterest()) {
                case LIKE:
                    panel = likePanel;
                    break;
                case NOT_CARE:
                    panel = notCarePanel;
                    break;
                case NOT_WANT:
                    panel = notWantPanel;
                    break;
                case CONFLICT:
                    panel = conflictPanel;
                    break;
                default:
                    LoggerFactory.log(AssignmentReviewersForm.class, "unknown reviewer interest");
                    continue;
            }
            if (panel != null) {
                boolean first = false;
                if (panel.getWidgetCount() == 1) {
                    Widget w = panel.getWidget(0);
                    if (w instanceof Label) {
                        w.removeFromParent();
                        first = true;
                    }
                }
                panel.add(new AssignItem(cr, first));
            }
        }
    }

    /** vraci label pro prazdny panel */
    private Label nobody() {
        return StyleUtils.getStyledLabel(ECMessageUtils.c.assignmentReviewersForm_text_nobody(), ECResourceUtils.bundle().css().assignNobody());
    }

    /** trida jedne polozky pro prirazeni */
    private class AssignItem extends FlowPanel {

        // komponenty
        private ConsysCheckBox checkBox;

        public AssignItem(final ClientReviewer reviewer, boolean firstItem) {
            super();

            if (firstItem) {
                FlowPanel separator = new FlowPanel();
                separator.setHeight("10px");
                separator.addStyleName(OVERFLOW_HIDDEN);
                FlowPanel sepContent = new FlowPanel();
                sepContent.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 34) + "px");
                sepContent.addStyleName(ECResourceUtils.bundle().css().assignItemContentFirst());
                separator.add(sepContent);
                add(separator);
            }

            final LoadIndicator load = new LoadIndicator(reviewer.getLoad(), maxLoad);
            load.addStyleName(ECResourceUtils.bundle().css().assignLoad());

            checkBox = new ConsysCheckBox();
            checkBox.addStyleName(ECResourceUtils.bundle().css().assignCheckItem());
            checkBox.setValue(reviewer.isReviewing());
            checkBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

                @Override
                public void onChangeValue(final ChangeValueEvent<Boolean> event) {
                    LoggerFactory.log(AssignmentReviewersForm.class, "user: " + reviewer.getName() + " selected -> " + event.getValue());
                    EventDispatchEvent changeEvent = new EventDispatchEvent(
                            new ContributionReviewerAssignmentAction(uuid, reviewer.getUuid(), event.getValue()),
                            new AsyncCallback<VoidResult>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    // obecne chyby zpracovava eventactionexecutor
                                }

                                @Override
                                public void onSuccess(VoidResult result) {
                                    if (event.getValue()) {
                                        load.increase(1);
                                    } else {
                                        load.decrease(1);
                                    }
                                }
                            },
                            AssignmentReviewersForm.this);
                    EventBus.get().fireEvent(changeEvent);
                }
            });

            switch (reviewer.getInterest()) {
                case LIKE:
                    addStyleName(ECResourceUtils.bundle().css().assignLikeItem());
                    break;
                case NOT_CARE:
                    addStyleName(ECResourceUtils.bundle().css().assignNotCareItem());
                    break;
                case NOT_WANT:
                    addStyleName(ECResourceUtils.bundle().css().assignNotWantItem());
                    break;
                case CONFLICT:
                    addStyleName(ECResourceUtils.bundle().css().assignConflictItem());
                    break;
            }

            FlowPanel check = new FlowPanel();
            check.addStyleName(ECResourceUtils.bundle().css().assignCheckItemContent());
            check.add(checkBox);
            add(check);

            FlowPanel content = new FlowPanel();
            content.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 34) + "px");
            content.addStyleName(ECResourceUtils.bundle().css().assignItemContent());
            add(content);

            String html = "";
            int size = reviewer.getTopics().size();
            for (int i = 0; i < size; i++) {
                ClientTopic ct = reviewer.getTopics().get(i);

                boolean contains = false;
                for (ClientTopic t : assign.getTopics()) {
                    if (t.getId().equals(ct.getId())) {
                        contains = true;
                        break;
                    }
                }

                String name = "";
                if (i < size - 1) {
                    name = ct.getName() + ", ";
                } else {
                    name = ct.getName();
                }

                html += contains ? "<span>" + name + "</span>" : name;
            }
            HTML topics = new HTML(html);
            topics.addStyleName(ECResourceUtils.bundle().css().assignTopicIntersection());

            content.add(StyleUtils.getStyledLabel(reviewer.getName(), ECResourceUtils.bundle().css().assignReviewerName()));
            content.add(load);
            content.add(topics);

            add(StyleUtils.clearDiv());
        }
    }
}
