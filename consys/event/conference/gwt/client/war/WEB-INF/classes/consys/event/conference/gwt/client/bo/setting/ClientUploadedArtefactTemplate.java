package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class ClientUploadedArtefactTemplate implements IsSerializable, Result {

    private static final long serialVersionUID = 8860076736309222127L;
    // data
    private String uuid;
    private String name;
    private String description;
    private Date dateInserted;
    private String contentType;
    private Long size;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    /**
     * @return the dateInserted
     */
    public Date getDateInserted() {
        return dateInserted;
    }

    /**
     * @param dateInserted the dateInserted to set
     */
    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the size
     */
    public Long getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(Long size) {
        this.size = size;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
