package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;

/**
 * Akce pro update topicu
 * @author pepa
 */
public class UpdateTopicAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 3632755512476994876L;
    // data
    private ClientTopic topic;

    public UpdateTopicAction() {
    }

    public UpdateTopicAction(ClientTopic topic) {
        this.topic = topic;
    }

    public ClientTopic getTopic() {
        return topic;
    }
}
