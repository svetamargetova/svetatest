package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysDoubleTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.LoadCriteriaAction;
import consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 * Editacni formular pro nastaveni Criteria
 * @author pepa
 */
public class EditFormCriteria extends RootPanel {

    // konstanty
    private static final String WIDTH = "320px";
    private static final int ITEMS = 10;
    private final ArrayList<SelectBoxItem<Integer>> pointsArray;
    // komponenty
    private SimpleFormWithHelpPanel mainPanel;
    private BaseForm form;
    private ConsysStringTextBox nameBox;
    private ConsysTextArea descBox;
    private SelectBox<Integer> pointsBox;
    private ConsysFlowPanel pointsListPanel;
    private ConsysDoubleTextBox minimumPointsBox;
    // data
    private ClientCriteria cc;
    private CriteriaItem source;
    private ArrayList<TextBox> inputList;

    public EditFormCriteria(Long id, final CriteriaItem source) {
        super(ECMessageUtils.c.editFormCriteria_title());

        mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        this.source = source;

        pointsArray = new ArrayList<SelectBoxItem<Integer>>();
        for (int i = 1; i < ITEMS; i++) {
            SelectBoxItem<Integer> item = new SelectBoxItem<Integer>(i + 1, String.valueOf(i + 1));
            pointsArray.add(item);
        }

        EventDispatchEvent loadEvent = new EventDispatchEvent(new LoadCriteriaAction(id),
                new AsyncCallback<ClientCriteria>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava ActionExecutor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientCriteria result) {
                        setTitleText(ECMessageUtils.c.editFormCriteria_title() + " " + result.getName());
                        cc = result;
                        source.setCriteria(result);
                        init();
                    }
                }, mainPanel);
        EventBus.get().fireEvent(loadEvent);

        //mainPanel.addHelpTitle("Help title");
        //mainPanel.addHelpText("Help text");
    }

    private void init() {
        form = new BaseForm("200px");
        form.setStyleName(StyleUtils.MARGIN_TOP_10);

        nameBox = new ConsysStringTextBox(WIDTH, 1, 255, UIMessageUtils.c.const_name());
        nameBox.setText(cc.getName());

        descBox = new ConsysTextArea(255, WIDTH, ConsysTextArea.HEIGHT, UIMessageUtils.c.const_description());
        descBox.setText(cc.getDescription());

        pointsBox = new SelectBox<Integer>();
        pointsBox.setWidth(50);
        pointsBox.setItems(pointsArray);
        pointsBox.selectItem(cc.getPoints().size());
        pointsBox.addChangeValueHandler(new ChangeValueEvent.Handler() {

            @Override
            public void onChangeValue(ChangeValueEvent event) {
                generatePointsList((Integer) ((SelectBoxItem) event.getValue()).getItem(), false);
            }
        });

        inputList = new ArrayList<TextBox>();
        pointsListPanel = new ConsysFlowPanel();
        generatePointsList(cc.getPoints().size(), true);

        minimumPointsBox = new ConsysDoubleTextBox("50px", 0, ITEMS, ECMessageUtils.c.editFormCriteria_text_minimumPoints()) {

            @Override
            public boolean doValidate(ConsysMessage fail) {
                if (!getText().trim().isEmpty()) {
                    return super.doValidate(fail);
                }
                return true;
            }
        };
        minimumPointsBox.addStyleName(FLOAT_LEFT);
        if (cc.getMinimumPoints() > 0) {
            minimumPointsBox.setText(cc.getMinimumPoints());
        }

        ConsysFlowPanel minimumPointsPanel = new ConsysFlowPanel();
        minimumPointsPanel.add(minimumPointsBox);
        minimumPointsPanel.add(StyleUtils.getStyledLabel("(" + ECMessageUtils.c.editFormCriteria_text_minimumPerAllReviewers() + ")",
                FLOAT_LEFT, MARGIN_LEFT_10, MARGIN_TOP_1));

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        form.addRequired(0, UIMessageUtils.c.const_name(), nameBox);
        form.addOptional(1, UIMessageUtils.c.const_description(), descBox);
        form.addRequired(2, ECMessageUtils.c.editFormCriteria_form_points(), pointsBox);
        form.addContent(3, pointsListPanel);
        form.addOptional(4, ECMessageUtils.c.editFormCriteria_text_minimumPoints(), minimumPointsPanel);
        form.addActionMembers(6, button, ActionLabel.breadcrumbBackCancel(), WIDTH);
        mainPanel.addWidget(form);
    }

    /** vygeneruje seznam bodu */
    private void generatePointsList(int points, boolean init) {
        ArrayList<String> previousValues = new ArrayList<String>();
        for (TextBox tb : inputList) {
            previousValues.add(tb.getText().trim());
        }

        pointsListPanel.clear();
        inputList.clear();

        for (int i = 0; i < points; i++) {
            ConsysFlowPanel panel = new ConsysFlowPanel();
            panel.setHeight("30px");

            String labelText = String.valueOf(i + 1);

            FlowPanel p = new FlowPanel();
            p.setWidth("30px");
            p.addStyleName(FLOAT_LEFT);
            p.addStyleName(CLEAR_BOTH);
            p.addStyleName(MARGIN_TOP_3);
            p.add(StyleUtils.getStyledLabel(labelText + ":", FLOAT_RIGHT));
            panel.add(p);

            ConsysStringTextBox input = new ConsysStringTextBox("150px", 1, 255, labelText);
            input.addStyleName(MARGIN_LEFT_10);
            input.addStyleName(FLOAT_LEFT);
            panel.add(input);
            inputList.add(input);

            if (init) {
                input.setText(cc.getPoints().get(i).getName());
            } else if (i < previousValues.size()) {
                input.setText(previousValues.get(i));
            }

            if (i == 0) {
                panel.addStyleName(MARGIN_TOP_5);
                Label label = StyleUtils.getStyledLabel(ECMessageUtils.c.editFormCriteria_text_minimumValue(), FLOAT_LEFT, MARGIN_LEFT_10, MARGIN_TOP_3);
                panel.add(label);
            } else if (i == points - 1) {
                Label label = StyleUtils.getStyledLabel(ECMessageUtils.c.editFormCriteria_text_maximumValue(), FLOAT_LEFT, MARGIN_LEFT_10, MARGIN_TOP_3);
                panel.add(label);
            }

            pointsListPanel.add(panel);
        }
    }

    /** potvrzovaci clickhandler */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                ArrayList<String> points = new ArrayList<String>();
                for (TextBox box : inputList) {
                    String label = box.getText().trim();

                    if (points.contains(label)) {
                        // validace na unikatnost nazvu
                        getFailMessage().setText(ECMessageUtils.c.editFormCriteria_error_labelsNotUnique());
                        return;
                    }

                    points.add(label);
                }

                double minPoints = 0d;
                if (!minimumPointsBox.getText().trim().isEmpty()) {
                    if (!minimumPointsBox.doValidate(getFailMessage())) {
                        return;
                    }
                    minPoints = minimumPointsBox.getTextDouble();
                    // validace na hodnotu minimum bodu (vetsi nebo rovno a mensi nebo rovno krajnim hodnotam)
                    if (minPoints < 1 || minPoints > points.size()) {
                        getFailMessage().setText(
                                UIMessageUtils.m.const_fieldMustBeInRange(ECMessageUtils.c.editFormCriteria_text_minimumPoints(), 1, points.size()));
                        return;
                    }
                }

                final ClientCriteria data = new ClientCriteria();
                data.setId(cc.getId());
                data.setName(nameBox.getText().trim());
                data.setDescription(descBox.getText().trim());
                for (String point : points) {
                    data.getPoints().add(new ClientCriteriaItem(0L, point));
                }

                data.setMinimumPoints(minPoints);

                EventDispatchEvent updateEvent = new EventDispatchEvent(new UpdateCriteriaAction(data),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava ActionExecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // aktualizace dat
                                cc.setName(data.getName());
                                cc.setDescription(data.getDescription());
                                cc.setPoints(data.getPoints());
                                cc.setPoints(data.getPoints());
                                cc.setMinimumPoints(data.getMinimumPoints());
                                // aktualizace zobrazovaciho formulare
                                source.setCriteria(cc);
                                // navrat na predchozi formular
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, mainPanel);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }
}
