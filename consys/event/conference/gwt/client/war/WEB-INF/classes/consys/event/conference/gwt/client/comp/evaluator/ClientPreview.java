package consys.event.conference.gwt.client.comp.evaluator;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;

/**
 *
 * @author pepa
 */
public class ClientPreview implements IsSerializable {

    private String reviewUuid;
    private String reviewerName;
    private EvaluationItemValue score;

    public ClientPreview() {
    }

    public ClientPreview(String reviewUuid, EvaluationItemValue score, String reviewerName) {
        this.reviewUuid = reviewUuid;
        this.score = score;
        this.reviewerName = reviewerName;
    }

    public String getReviewUuid() {
        return reviewUuid;
    }

    public void setReviewUuid(String reviewUuid) {
        this.reviewUuid = reviewUuid;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public EvaluationItemValue getScore() {
        return score;
    }

    public void setScore(EvaluationItemValue score) {
        this.score = score;
    }
}
