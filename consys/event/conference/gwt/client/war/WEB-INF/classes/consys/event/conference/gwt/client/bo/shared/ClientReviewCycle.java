package consys.event.conference.gwt.client.bo.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import java.util.ArrayList;
import java.util.Date;

/**
 * Recenzencni cyklus, ktery obsahuje od kdy do kdy a seznam artefaktu. 
 * <p/>
 * Data sa posielaju v ramci potomkov objektu {@link ClientArtefactType}
 * 
 * @author pepa
 */
public class ClientReviewCycle implements IsSerializable {

    // data    
    private Date from;
    private Date to;
    // cyklus je editovatelny - vzhladom na to co predstavuje
    private boolean editable;
    private ArrayList<ClientArtefactType> artefacts;

    public ClientReviewCycle() {
        artefacts = new ArrayList<ClientArtefactType>();
    }

    public ArrayList<ClientArtefactType> getArtefacts() {
        return artefacts;
    }

    public void setArtefacts(ArrayList<ClientArtefactType> artefacts) {
        this.artefacts = artefacts;
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the editable
     */
    public boolean isEditable() {
        return editable;
    }

    /**
     * @param editable the editable to set
     */
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

  
  
}
