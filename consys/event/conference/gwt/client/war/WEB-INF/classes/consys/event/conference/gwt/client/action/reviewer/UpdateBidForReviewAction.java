package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum;

/**
 * Akce pro aktualizaci recenzentova bid for review
 * @author pepa
 */
public class UpdateBidForReviewAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -777431646814990751L;
    // data
    private String uuid;
    private ReviewerInterestEnum ch;

    public UpdateBidForReviewAction() {
    }

    public UpdateBidForReviewAction(String uuid, ReviewerInterestEnum ch) {
        this.uuid = uuid;
        this.ch = ch;
    }

    public ReviewerInterestEnum getCh() {
        return ch;
    }

    public String getUuid() {
        return uuid;
    }
}
