package consys.event.conference.gwt.client.bo.contribution;

import java.io.Serializable;

/**
 * Trieda ktora predstavuje uz nahraty datovy subor na serveru
 * @author palo
 */
public class FileContributionArtefact implements Serializable {

    private static final long serialVersionUID = 6228426094255884555L;
    // data
    private String artefactTypeUuid;
    private String artefactUuid;

    public FileContributionArtefact() {
    }

    public FileContributionArtefact(String artefactTypeUuid, String artefactUuid) {
        this.artefactTypeUuid = artefactTypeUuid;
        this.artefactUuid = artefactUuid;
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    public void setArtefactTypeUuid(String artefactTypeUuid) {
        this.artefactTypeUuid = artefactTypeUuid;
    }

    public void setArtefactUuid(String artefactUuid) {
        this.artefactUuid = artefactUuid;
    }

    @Override
    public int hashCode() {
        return artefactUuid.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileContributionArtefact other = (FileContributionArtefact) obj;
        if ((this.artefactUuid == null) ? (other.artefactUuid != null) : !this.artefactUuid.equals(other.artefactUuid)) {
            return false;
        }
        return true;
    }
}
