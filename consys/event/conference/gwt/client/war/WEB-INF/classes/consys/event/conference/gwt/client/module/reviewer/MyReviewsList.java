package consys.event.conference.gwt.client.module.reviewer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.list.*;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.module.reviewer.item.MyReviewsFormFilter;
import consys.event.conference.gwt.client.module.reviewer.item.MyReviewsListItem;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 *
 * @author pepa
 */
public class MyReviewsList extends DataListPanel implements consys.event.conference.api.list.ReviewerReviewsList {

    // konstanty
    public MyReviewsList() {
        super(ECMessageUtils.c.myReviewsForm_title(), TAG, false);
        addStyleName(ECResourceUtils.css().myReviewsList());

        setListDelegate(new MyReviewListDelegate());
        setDefaultFilter(new ListFilter(Filter_TYPE) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
        DataListCommonOrderer orderers = new DataListCommonOrderer();
        orderers.addOrderer(ECMessageUtils.c.const_title(), Order_TITLE, true);
        orderers.addOrderer(ECMessageUtils.c.const_submitDate(), Order_SUBMIT_DATE);
        orderers.addOrderer(ECMessageUtils.c.const_score(), Order_SCORE);
        addHeadPanel(orderers);
        addHeadPanel(new MyReviewsFormFilter(false));

        ActionImage bid = ActionImage.getPersonButton(ECMessageUtils.c.bidForReviewForm_title(), true);
        bid.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                FormUtils.fireNextBreadcrumb(ECMessageUtils.c.bidForReviewForm_title(), new BidForReviewForm());
            }
        });
        addLeftControll(bid);

        ActionImage topics = ActionImage.getPersonButton(ECMessageUtils.c.preferredTopicSelectForm_title(), true);
        topics.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                PreferredTopicSelectForm w = new PreferredTopicSelectForm();
                FormUtils.fireNextBreadcrumb(ECMessageUtils.c.preferredTopicSelectForm_title(), w);
            }
        });
        addLeftControll(topics);
    }

    /** delegat seznamu */
    private class MyReviewListDelegate implements ListDelegate<ClientListContributionItem, ReviewListItem> {

        @Override
        public ReviewListItem createCell(ClientListContributionItem item) {
            return new ReviewListItem(item);
        }
    }

    private class ReviewListItem extends ListItem<ClientListContributionItem> {

        public ReviewListItem(final ClientListContributionItem thumb) {
            super(thumb, true);
        }

        @Override
        protected void createCell(final ClientListContributionItem item, FlowPanel panel) {
            panel.clear();
            panel.setStyleName(ECResourceUtils.css().contributionListItemWrapper());
            panel.add(new MyReviewsListItem(item));
        }
    }
}
