package consys.event.conference.gwt.client.comp;

import consys.event.conference.gwt.client.comp.evaluator.EvaluatorWrapper;
import consys.event.conference.gwt.client.comp.evaluator.PreviewEvaluator;
import consys.event.conference.gwt.client.comp.evaluator.ClientPreviews;
import java.util.ArrayList;

/**
 * Seznam vysledku hodnoceni
 * @author pepa
 */
public class PreviewEvaluatorWrapper extends EvaluatorWrapper<ClientPreviews, PreviewEvaluator> {

    // data
    private String contributionUuid;
    private String contributionTitle;

    public PreviewEvaluatorWrapper(String contributionUuid, String contributionTitle) {
        this.contributionUuid = contributionUuid;
        this.contributionTitle = contributionTitle;
    }

    public void setPreviews(ArrayList<ClientPreviews> previews) {
        super.setData(previews);
    }

    @Override
    protected PreviewEvaluator createEvaluator(ClientPreviews data) {
        return new PreviewEvaluator(data, contributionUuid, contributionTitle);
    }
}
