package consys.event.conference.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientNewContributionActionData implements Result {

    private static final long serialVersionUID = 1625055358961033479L;
    // data
    /** UUID ktore sa ma pridadit novemu prispevku */
    private String contributionUuid;
    /* Uuid typu pre ktory je naplnene pole selectedArtefacts */
    private String contributionTypeUuid;
    /* Typu prispevkov  */
    private ArrayList<ClientContributionTypeThumb> types;
    /* Pre jeden(prvy) vybrany typ per kolo recenzie vypis artefaktu - ak nema ziadne kola tak vypis vsetky a moznost ich update */
    private ArrayList<ClientReviewCycle> selectedContributionTypeCycles;

    public ClientNewContributionActionData() {
        this(null);
    }

    public ClientNewContributionActionData(String selectedUuid) {
        this.contributionTypeUuid = selectedUuid;
        types = new ArrayList<ClientContributionTypeThumb>();
        selectedContributionTypeCycles = new ArrayList<ClientReviewCycle>();
    }

    public ArrayList<ClientReviewCycle> getSelectedCycles() {
        return selectedContributionTypeCycles;
    }
   
    public String getContributionTypeUuid() {
        return contributionTypeUuid;
    }

    public void setContributionTypeUuid(String selectedUuid) {
        this.contributionTypeUuid = selectedUuid;
    }

    public ArrayList<ClientContributionTypeThumb> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<ClientContributionTypeThumb> types) {
        this.types = types;
    }

    /**
     * UUID ktore sa ma pridadit novemu prispevku
     * @return the submissionUuid
     */
    public String getContributionUuid() {
        return contributionUuid;
    }

    /**
     * UUID ktore sa ma pridadit novemu prispevku
     * @param submissionUuid the submissionUuid to set
     */
    public void setContributionUuid(String submissionUuid) {
        this.contributionUuid = submissionUuid;
    }
}
