package consys.event.conference.gwt.client.module.chair.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.comp.ContributionsListItem;
import consys.event.conference.gwt.client.module.chair.AssignmentReviewersForm;
import consys.event.conference.gwt.client.module.chair.ContributionChairForm;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Jedna polozka v seznamu prispevku, ktere se zobrazuji chairovi
 * @author pepa
 */
public class ChairContributionsListItem extends ContributionsListItem {

    public ChairContributionsListItem(final ClientListContributionItem data) {
        super(data, true, false, new ConsysAction() {

            @Override
            public void run() {
                evaluateAction(data);
            }
        });
    }

    @Override
    protected void createActionButtons() {
        addControl(ECMessageUtils.c.const_showDetails(), evaluateClickHandler());
        addControl(ECMessageUtils.c.chairContributionsListItem_action_assignReviewers(), assignClickHandler());
    }

    private ClickHandler assignClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                AssignmentReviewersForm w = new AssignmentReviewersForm(getData().getUuid());
                FormUtils.fireNextBreadcrumb(getData().getOverview().getTitle(), w);
            }
        };
    }

    private ClickHandler evaluateClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                evaluateAction(getData());
            }
        };
    }

    private static void evaluateAction(ClientListContributionItem data) {
        ContributionChairForm w = new ContributionChairForm(data.getOverview().getTitle(), data.getUuid());
        FormUtils.fireNextBreadcrumb(data.getOverview().getTitle(), w);
    }
}
