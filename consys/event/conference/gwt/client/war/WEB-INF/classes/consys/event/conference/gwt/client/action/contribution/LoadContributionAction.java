package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.ClientContributionActionData;

/**
 * Akce pro nacteni prispevku pro prehled a aktualizaci od uzivatele
 * @author pepa
 */
public class LoadContributionAction extends EventAction<ClientContributionActionData> {

    private static final long serialVersionUID = -2096115919465664441L;
    // data
    private String contributionUuid;

    public LoadContributionAction() {
    }

    public LoadContributionAction(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }
}
