package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate;

/**
 * Akce pro nacteni konkretniho uploadovaneho vzoru
 * @author pepa
 */
public class LoadUploadedArtefactTemplateAction extends EventAction<ClientUploadedArtefactTemplate> {

    private static final long serialVersionUID = 7653144388410691692L;
    // data
    private String uuid;
    private boolean pattern;

    public LoadUploadedArtefactTemplateAction() {
    }

    public LoadUploadedArtefactTemplateAction(String uuid) {
        this.uuid = uuid;
    }

    public LoadUploadedArtefactTemplateAction(String uuid, boolean pattern) {
        this.uuid = uuid;
        this.pattern = pattern;
    }

    

    public String getUuid() {
        return uuid;
    }

    public boolean isPattern() {
        return pattern;
    }


}
