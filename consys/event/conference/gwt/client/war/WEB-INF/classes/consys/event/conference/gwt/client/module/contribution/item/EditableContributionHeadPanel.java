package consys.event.conference.gwt.client.module.contribution.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConfirmDialog;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.contribution.DeleteContributionAction;
import consys.event.conference.gwt.client.action.contribution.LoadContributionHeadAction;
import consys.event.conference.gwt.client.action.contribution.UpdateContributionAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.HashSet;

/**
 *
 * @author palo
 */
public class EditableContributionHeadPanel extends ConsysFlowPanel {

    private ClientContributionHeadData data;
    private boolean chairView;

    /**
     * Priznakom
     * <code>chairView</code> sa urcuje ci moze vykonavat nad tymto prispevkom
     * vsetko alebo je to zavisle od stavu prispevku.
     *
     * @param data
     * @param chairView
     */
    public EditableContributionHeadPanel(ClientContributionHeadData data, boolean chairView) {
        this.data = data;
        this.chairView = chairView;
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        setWidget(readHead());
    }

    private void setWidget(Widget w) {
        clear();
        add(w);
    }

    private ContributionHeadPanel updateHead() {
        HeadPanelUpdateInputFactory hpu = new HeadPanelUpdateInputFactory();
        ContributionHeadPanel headPanel = new ContributionHeadPanel(data, hpu, !chairView);
        headPanel.addStyleName(ECResourceUtils.css().newContributionFormHeadPanel());
        headPanel.addControl(UIMessageUtils.c.const_cancel(), cancelUpdateModeHandler());
        headPanel.addControl(UIMessageUtils.c.const_update(), executeUpdateHandler(headPanel,hpu));
        return headPanel;
    }

    private ContributionHeadPanel readHead() {
        // Vytvorime read mode
        HeadPanelReadInputFactory inputFactory = new HeadPanelReadInputFactory(true);
        ContributionHeadPanel headPanel = new ContributionHeadPanel(data, inputFactory, !chairView);
        headPanel.addStyleName(ECResourceUtils.css().newContributionFormHeadPanel());

        if (chairView || !ClientContributionStateEnum.DECLINED.equals(data.getState())) {
            // Nastavime akciu na delete        
            ActionImage delete = ActionImage.delete(UIMessageUtils.c.const_delete());
            delete.setClickHandler(deleteHandler());
            headPanel.addControl(delete);

            // nastavime akcie na update
            headPanel.addControl(UIMessageUtils.c.const_edit(), toEditModeHandler());
        }

        return headPanel;
    }

    private ClickHandler cancelUpdateModeHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setWidget(readHead());
            }
        };
    }

    private ClickHandler toEditModeHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                setWidget(updateHead());
            }
        };
    }

    private ClickHandler deleteHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final ConfirmDialog dialog = new ConfirmDialog(ECMessageUtils.m.contributionHeadPanel_text_deleteContribution(data.getTitle()));
                ConsysAction action = new ConsysAction() {

                    @Override
                    public void run() {
                        EventDispatchEvent cancelEvent = new EventDispatchEvent(
                                new DeleteContributionAction(data.getUuid()),
                                new AsyncCallback<VoidResult>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        dialog.hide();
                                    }

                                    @Override
                                    public void onSuccess(VoidResult result) {
                                        FormUtils.fireBreadcrumbBack();
                                        dialog.hide();
                                    }
                                }, EditableContributionHeadPanel.this);
                        EventBus.get().fireEvent(cancelEvent);
                    }
                };
                dialog.setAction(action);
                dialog.showCentered();
            }
        };
    }

    private ClickHandler executeUpdateHandler(final ContributionHeadPanel headpanel, final HeadPanelUpdateInputFactory f) {
        
        
        
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                
                if(!headpanel.validate()){
                    return;
                }
                
                
                UpdateContributionAction action = new UpdateContributionAction();
                action.setAuthors(f.getAuthors());
                action.setContributionUuid(data.getUuid());
                action.setContributors(new HashSet(f.getContributors()));
                action.setSubtitle(f.getSubTitle());
                action.setTitle(f.getTitle());
                action.setTopics(new HashSet(f.getTopics()));

                EventBus.fire(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        loadNewHeadData();
                    }
                }, EditableContributionHeadPanel.this));
            }
        };
    }

    /**
     * nacte aktuali hlavickova data a necha je zobrazovat ve ctecim rezimu
     * hlavicky
     */
    private void loadNewHeadData() {
        EventBus.fire(new EventDispatchEvent(new LoadContributionHeadAction(data.getUuid()),
                new AsyncCallback<ClientContributionHeadData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }

                    @Override
                    public void onSuccess(ClientContributionHeadData result) {
                        data = result;
                        setWidget(readHead());
                    }
                }, this));
    }
}
