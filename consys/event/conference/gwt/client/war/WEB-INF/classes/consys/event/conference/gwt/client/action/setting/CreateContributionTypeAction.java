package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import java.util.ArrayList;

/**
 * Akce pro vytvoreni typu prispevku
 * @author pepa
 */
public class CreateContributionTypeAction extends EventAction<ArrayListResult<String>> {

    // konstanty
    private static final long serialVersionUID = -4909994201900524162L;
    // data
    private ArrayList<ClientContributionTypeThumb> types;

    public CreateContributionTypeAction() {
    }

    public CreateContributionTypeAction(ArrayList<ClientContributionTypeThumb> types) {
        this.types = types;
    }

    public ArrayList<ClientContributionTypeThumb> getTypes() {
        return types;
    }
}
