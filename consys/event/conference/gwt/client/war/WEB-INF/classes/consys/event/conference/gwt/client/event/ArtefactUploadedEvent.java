/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 * Event ktory je volany akonahle dojde k uspesnemu uploadu suboroveho artefaktu
 * na server.
 * 
 * @author palo
 */
public class ArtefactUploadedEvent extends GwtEvent<ArtefactUploadedEvent.Handler>{
   
    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();        
    
    private String contributionUuid;
    private String artefactTypeUuid;
    private String artefactUuid;

    public ArtefactUploadedEvent(String contributionUuid, String artefactTypeUuid, String artefactUuid) {
        this.contributionUuid = contributionUuid;
        this.artefactTypeUuid = artefactTypeUuid;
        this.artefactUuid = artefactUuid;
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onArtefactUpload(this);
    }            
    
     /** interface eventu */
    public interface Handler extends EventHandler {

        public void onArtefactUpload(ArtefactUploadedEvent event);
    }
}
