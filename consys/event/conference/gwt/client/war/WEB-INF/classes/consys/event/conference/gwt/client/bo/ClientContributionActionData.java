package consys.event.conference.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class ClientContributionActionData implements Result {

    private static final long serialVersionUID = -6730493231238585046L;
    // data
    private ClientContributionHeadData head;
    private List<ReviewScoreAndCommentsData> reviews;
    /* Pre jeden(prvy) vybrany typ per kolo recenzie vypis artefaktu - ak nema ziadne kola tak vypis vsetky a moznost ich update */
    private ArrayList<ClientReviewCycle> selectedCycles;    
    

    public ClientContributionActionData() {        
        selectedCycles = new ArrayList<ClientReviewCycle>();
    }   

    public ClientContributionHeadData getHead() {
        return head;
    }

    public void setHead(ClientContributionHeadData contributionHead) {
        this.head = contributionHead;
    }

    public List<ReviewScoreAndCommentsData> getReviews() {
        if(reviews == null){
            reviews = new ArrayList<ReviewScoreAndCommentsData>();
        }
        
        return reviews;
    }

    public void setReviews(List<ReviewScoreAndCommentsData> reviews) {
        this.reviews = reviews;
    }

    public ArrayList<ClientReviewCycle> getSelectedCycles() {
        return selectedCycles;
    }

    public void setSelectedCycles(ArrayList<ClientReviewCycle> selectedCycles) {
        this.selectedCycles = selectedCycles;
    }
}
