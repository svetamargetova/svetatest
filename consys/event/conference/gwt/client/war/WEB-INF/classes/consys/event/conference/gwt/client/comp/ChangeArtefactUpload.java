package consys.event.conference.gwt.client.comp;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.comp.input.AbstractConsysFileUpload;
import consys.common.gwt.client.ui.comp.input.ConsysFileUpload;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Komponentka na aktualizaci uploadovaneho souboru
 * @author pepa
 */
public class ChangeArtefactUpload extends AbstractConsysFileUpload {

    // komponenty
    private ActionImage button;
    // data
    private String fileName;

    public ChangeArtefactUpload() {
        super(ECMessageUtils.c.changeArtefactUpload_button_width(), ConsysFileUpload.HEIGHT);
    }

    @Override
    protected void initFace(FlowPanel face) {
        button = ActionImage.withoutIcon(ECMessageUtils.c.artefactUpload_action_change());
        face.add(button);

        initHandlers();
    }

    private void initHandlers() {
        addUploadChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                String[] text = getFilename().split("\\\\");
                fileName = text[text.length - 1];
            }
        });
        addUploadOverHandler(ConsysFileUpload.mouseOverHandler(button));
        addUploadOutHandler(ConsysFileUpload.mouseOutHandler(button));
    }

    @Override
    protected void disable(boolean disable) {
        button.setEnabled(!disable);
    }

    /** vraci nazev vybraneho souboru */
    public String getSelectedFilename() {
        return fileName;
    }
}
