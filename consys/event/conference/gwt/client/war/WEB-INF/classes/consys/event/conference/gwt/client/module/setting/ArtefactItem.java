package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysAsyncAction;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.RollOutPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.DeleteArtefactTypeAction;
import consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCriteriaException;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCycleException;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;

/**
 * Jedna polozka artefaktu
 * @author pepa
 */
public class ArtefactItem extends RollOutPanel implements ActionExecutionDelegate {

    // komponenty
    private Label titleLabel;
    private Label descriptionText;
    private HTML reviewersPanel;
    private HTML requiredPanel;
    private HTML fileTypesPanel;
    private FlowPanel failPanel;
    private FlowPanel uploadTemplatesPanel;
    private FlowPanel uploadExamplesPanel;
    private Separator criteriaSeparator;
    // data
    private ClientSettingsArtefactType cat;
    private CriteriaList criteriaList;

    public ArtefactItem(final ClientSettingsArtefactType typet) {
    }

    public ArtefactItem(final ClientArtefactTypeThumb typet) {
        super();
        criteriaList = new CriteriaList(typet.getArtefactTypeUuid());
        setStyleName(StyleUtils.MARGIN_TOP_10);
        setWidth("100%");
        enableWaiting();

        cat = new ClientSettingsArtefactType();
        cat.setArtefactTypeUuid(typet.getArtefactTypeUuid());
        cat.setArtefactTypeName(typet.getArtefactTypeName());
        if (typet instanceof ClientSettingsArtefactType) {
            ClientSettingsArtefactType type = (ClientSettingsArtefactType) typet;
            cat.setDataType(type.getDataType());
            cat.setRequired(type.isRequired());
            cat.setArtefactTypeDescription(type.getArtefactTypeDescription());
            cat.setFileFormats(type.getFileFormats());
            cat.setCriteriaList(type.getCriteriaList());
            cat.setUploadedExamples(type.getUploadedExamples());
            cat.setUploadedTemplates(type.getUploadedTemplates());
            criteriaList.setClientCriteriaList(cat.getCriteriaList());
        }

        // hlavicka
        titleLabel = StyleUtils.getStyledLabel(cat.getArtefactTypeName(), RollOutPanel.TITLE_TEXT_STYLE);

        ConsysAction editAction = new ConsysAction() {

            @Override
            public void run() {
                EditFormArtefact w = new EditFormArtefact(cat.getArtefactTypeUuid(), ArtefactItem.this);
                String title = ECMessageUtils.c.editFormArtefact_title_editArtefact() + " " + cat.getArtefactTypeName();
                FormUtils.fireNextBreadcrumb(title, w);
            }
        };

        ConsysAction removeAction = new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent removeEvent = new EventDispatchEvent(new DeleteArtefactTypeAction(typet.getArtefactTypeUuid()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby obstarava ActionExecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                } else if (caught instanceof DeleteReviewCriteriaException) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionDeleteReviewCriteria());
                                } else if (caught instanceof DeleteReviewCycleException) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionDeleteReviewCycle());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                ArtefactItem.this.removeFromParent();
                            }
                        }, ArtefactItem.this);
                EventBus.get().fireEvent(removeEvent);
            }
        };

        EditWithRemover ewr = new EditWithRemover(editAction, removeAction);
        ewr.atRight();

        failPanel = new FlowPanel();

        FlowPanel titlePanel = new FlowPanel();
        titlePanel.addStyleName(StyleUtils.FLOAT_LEFT);
        titlePanel.setWidth(ECMessageUtils.c.artefactItem_title_width() + "px");
        titlePanel.add(titleLabel);
        titlePanel.add(failPanel);

        String headWidth = (ContributionTypeItem.WIDTH - RollOutPanel.LEFT_PART_WIDTH) + "px";

        FlowPanel head = new FlowPanel();
        head.setWidth(headWidth);
        head.add(ewr);
        head.add(titlePanel);
        head.add(StyleUtils.clearDiv());

        setTitle(head);
        setMainPanelWidth(headWidth);

        // obsah
        Label descriptionLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.const_description(), StyleUtils.FONT_BOLD);
        descriptionText = StyleUtils.getStyledLabel(FormUtils.valueOrDash(cat.getArtefactTypeDescription()), StyleUtils.MARGIN_BOT_20);

        reviewersPanel = new HTML(reviewersText());
        requiredPanel = new HTML(requiredText());
        fileTypesPanel = new HTML(fileTypesText());

        uploadTemplatesPanel = new FlowPanel();
        uploadTemplatesPanel.setStyleName(StyleUtils.FLOAT_LEFT);
        uploadTemplatesPanel.addStyleName(StyleUtils.MARGIN_LEFT_5);
        FlowPanel uploadTemplates = new FlowPanel();
        uploadTemplates.add(StyleUtils.getStyledLabel(ECMessageUtils.c.artefactItem_text_uploadedTemplates() + ":", StyleUtils.FLOAT_LEFT));
        uploadTemplates.add(uploadTemplatesPanel);
        uploadTemplates.add(StyleUtils.clearDiv());

        uploadExamplesPanel = new FlowPanel();
        uploadExamplesPanel.setStyleName(StyleUtils.FLOAT_LEFT);
        uploadExamplesPanel.addStyleName(StyleUtils.MARGIN_LEFT_5);
        FlowPanel uploadExamples = new FlowPanel();
        uploadExamples.add(StyleUtils.getStyledLabel(ECMessageUtils.c.artefactItem_text_uploadedExamples() + ":", StyleUtils.FLOAT_LEFT));
        uploadExamples.add(uploadExamplesPanel);
        uploadExamples.add(StyleUtils.clearDiv());

        criteriaSeparator = Separator.addedStyle(StyleUtils.MARGIN_TOP_10);

        FlowPanel content = new FlowPanel();
        content.setWidth((ContributionTypeItem.WIDTH - RollOutPanel.LEFT_PART_WIDTH) + "px");
        content.setStyleName(StyleUtils.MARGIN_TOP_10);
        content.add(descriptionLabel);
        content.add(descriptionText);
        content.add(reviewersPanel);
        content.add(requiredPanel);
        content.add(fileTypesPanel);
        content.add(uploadTemplates);
        content.add(uploadExamples);
        content.add(criteriaSeparator);
        content.add(criteriaList);

        setContent(content);
    }

    /** text u polozky required */
    private String requiredText() {
        String text = "<div>" + cat.getArtefactTypeName() + " ";
        text += UIMessageUtils.c.const_isLower();
        text += " <span class=\"" + (cat.isRequired() ? StyleUtils.TEXT_RED : StyleUtils.TEXT_GREEN) + " " + StyleUtils.FONT_BOLD + "\"";
        text += ">" + (cat.isRequired() ? UIMessageUtils.c.const_item_requiredLower() : UIMessageUtils.c.const_item_optionalLower()) + "</span></div>";
        return text;
    }

    /** text u polozky fileTypes */
    private String fileTypesText() {
        if (cat.isFile()) {
            String textFile = cat.getFileFormats() == null || cat.getFileFormats().isEmpty()
                    ? UIMessageUtils.c.const_noneLower() : cat.getFileFormats();
            return ECMessageUtils.c.artefact_text_fileTypes() + ": " + textFile;
        } else {
            String textLimit = cat.getMaxInputLength() == 0 ? UIMessageUtils.c.const_unlimited() : String.valueOf(cat.getMaxInputLength());
            return ECMessageUtils.c.editFormArtefact_form_maxInputWords() + ": " + textLimit;
        }
    }

    /** vraci text s poctem hodnotitelu */
    private String reviewersText() {
        String reviewers = cat.getNumReviewers() == 0 ? ECMessageUtils.c.artefact_text_automaticAccept() : String.valueOf(cat.getNumReviewers());
        return "<div>" + ECMessageUtils.c.editFormArtefact_form_numReviewers() + ": <b>" + reviewers + "</b></div>";
    }

    /** vrati ClientSettingsArtefactType */
    public ClientSettingsArtefactType getArtefactType() {
        cat.setCriteriaList(criteriaList.getClientCriteriaList());
        return cat;
    }

    /** nastavi ClientSettingsArtefactType */
    public void setArtefactType(ClientSettingsArtefactType cat) {
        this.cat = cat;
        titleLabel.setText(cat.getArtefactTypeName());
        descriptionText.setText(FormUtils.valueOrDash(cat.getArtefactTypeDescription()));
        reviewersPanel.setHTML(reviewersText());
        requiredPanel.setHTML(requiredText());
        fileTypesPanel.setHTML(fileTypesText());

        boolean autoAccept = cat.getNumReviewers() == 0;
        criteriaList.setVisible(!autoAccept);
        criteriaSeparator.setVisible(!autoAccept);
        if (autoAccept) {
            cat.setCriteriaList(new ArrayList<ClientCriteriaThumb>());
        }

        uploadTemplatesPanel.clear();
        for (CommonThumb ct : cat.getUploadedTemplates()) {
            Anchor anchor = new Anchor(ct.getName(), ECResourceUtils.downloadArtefactUrl(ct.getUuid()));
            anchor.addStyleName(StyleUtils.PADDING_RIGHT_5);
            uploadTemplatesPanel.add(anchor);
        }
        if (cat.getUploadedTemplates().isEmpty()) {
            uploadTemplatesPanel.add(new Label(UIMessageUtils.c.const_noneLower()));
        }

        uploadExamplesPanel.clear();
        for (CommonThumb ct : cat.getUploadedExamples()) {
            Anchor anchor = new Anchor(ct.getName(), ECResourceUtils.downloadArtefactUrl(ct.getUuid()));
            anchor.addStyleName(StyleUtils.PADDING_RIGHT_5);
            uploadExamplesPanel.add(anchor);
        }
        if (cat.getUploadedExamples().isEmpty()) {
            uploadExamplesPanel.add(new Label(UIMessageUtils.c.const_noneLower()));
        }

        criteriaList.setClientCriteriaList(cat.getCriteriaList());
    }

    /** vraci chybovou zpravu */
    private ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setStyleName(StyleUtils.MARGIN_TOP_10);
        failMessage.setVisible(false);
        failPanel.add(failMessage);
        return failMessage;
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    @Override
    protected void onOpenRoll(final ConsysAsyncAction onAction) {
        EventDispatchEvent loadEvent = new EventDispatchEvent(new LoadArtefactTypeAction(cat.getArtefactTypeUuid()),
                new AsyncCallback<ClientSettingsArtefactType>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava ActionExecutor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientSettingsArtefactType result) {
                        setArtefactType(result);
                        onAction.onSuccess();
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);
    }
}
