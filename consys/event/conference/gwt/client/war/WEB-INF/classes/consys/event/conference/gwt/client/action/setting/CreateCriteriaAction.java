package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate;
import java.util.ArrayList;

/**
 * Akce pro vytvoreni kriterii
 * @author pepa
 */
public class CreateCriteriaAction extends EventAction<ArrayListResult<Long>> {

    // konstanty
    private static final long serialVersionUID = -330367779459458173L;
    // data
    private String artefactUuid;
    private ArrayList<ClientCriteriaThumbCreate> criteria;

    public CreateCriteriaAction() {
    }

    public CreateCriteriaAction(String artefactUuid, ArrayList<ClientCriteriaThumbCreate> criteria) {
        this.artefactUuid = artefactUuid;
        this.criteria = criteria;
    }

    public ArrayList<ClientCriteriaThumbCreate> getCriteria() {
        return criteria;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }
}
