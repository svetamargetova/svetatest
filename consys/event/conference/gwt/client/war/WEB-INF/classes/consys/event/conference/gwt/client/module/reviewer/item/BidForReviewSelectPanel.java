package consys.event.conference.gwt.client.module.reviewer.item;

import consys.event.conference.gwt.client.module.reviewer.BidForReviewForm;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction;
import consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum;
import consys.event.conference.gwt.client.module.reviewer.BidForReviewForm;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 *
 * @author pepa
 */
public class BidForReviewSelectPanel extends FlowPanel implements CssStyles {

    // komponenty
    private RadioButton like;
    private RadioButton dislike;
    private RadioButton notMind;
    private RadioButton conflict;
    // data
    private ReviewerInterestEnum choice;
    private String uuid;
    private BidForReviewForm parent;
    private Element cellElement;

    public BidForReviewSelectPanel(String uuid, ReviewerInterestEnum intereset, BidForReviewForm parent) {
        super();
        this.uuid = uuid;
        this.choice = intereset;
        this.parent = parent;

        setStyleName(ECResourceUtils.bundle().css().bfrSelectPanel());

        like = rb(uuid);
        like.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                changeChoice(ReviewerInterestEnum.LIKE);
            }
        });
        dislike = rb(uuid);
        dislike.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                changeChoice(ReviewerInterestEnum.NOT_WANT);
            }
        });
        notMind = rb(uuid);
        notMind.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                changeChoice(ReviewerInterestEnum.NOT_CARE);
            }
        });
        conflict = rb(uuid);
        conflict.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                changeChoice(ReviewerInterestEnum.CONFLICT);
            }
        });

        switch (choice) {
            case LIKE:
                like.setValue(Boolean.TRUE, false);
                break;
            case NOT_WANT:
                dislike.setValue(Boolean.TRUE, false);
                break;
            case NOT_CARE:
                notMind.setValue(Boolean.TRUE, false);
                break;
            case CONFLICT:
                conflict.setValue(Boolean.TRUE, false);
                break;
        }

        add(like);
        add(label(ECMessageUtils.c.bidForReviewForm_text_like(), false));
        add(StyleUtils.clearDiv("10px"));

        add(dislike);
        add(label(ECMessageUtils.c.bidForReviewForm_text_dislike(), false));
        add(StyleUtils.clearDiv("10px"));

        add(notMind);
        add(label(ECMessageUtils.c.bidForReviewForm_text_notMind(), false));
        add(StyleUtils.clearDiv("10px"));

        add(conflict);
        add(label(ECMessageUtils.c.bidForReviewForm_text_conflict(), true));
    }

    public void init(Element cellElement) {
        this.cellElement = cellElement;
        colorBy(choice);
    }

    /** vraci nastylovany label */
    private Label label(String text, boolean conflict) {
        Label label = StyleUtils.getStyledLabel(text, FONT_11PX, FLOAT_LEFT);
        if (conflict) {
            label.addStyleName(TEXT_RED);
        }
        label.setWidth("90px");
        return label;
    }

    /** vraci nastylovany radiobutton */
    private RadioButton rb(String group) {
        RadioButton rb = new RadioButton(group);
        rb.setStyleName(FLOAT_LEFT);
        rb.addStyleName(MARGIN_HOR_10);
        return rb;
    }

    /** vraci vybranou hodnotu */
    private void changeChoice(final ReviewerInterestEnum ch) {
        EventBus.get().fireEvent(new EventDispatchEvent(
                new UpdateBidForReviewAction(uuid, ch),
                new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action excecutor
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        choice = ch;
                        colorBy(ch);
                    }
                }, parent));
    }

    /** obarvi pozadi */
    private void colorBy(ReviewerInterestEnum ch) {
        if (cellElement == null) {
            throw new IllegalArgumentException("cellElement is null");
        }
        switch (ch) {
            case LIKE:
                DOM.setStyleAttribute(cellElement, "backgroundColor", "#d6f884");
                DOM.setStyleAttribute(cellElement, "borderLeft", "1px solid #b1c77e");
                break;
            case NOT_WANT:
                DOM.setStyleAttribute(cellElement, "backgroundColor", "#f9fa80");
                DOM.setStyleAttribute(cellElement, "borderLeft", "1px solid #bdb985");
                break;
            case NOT_CARE:
                DOM.setStyleAttribute(cellElement, "backgroundColor", "#f0f0f0");
                DOM.setStyleAttribute(cellElement, "borderLeft", "1px solid #c7c7c7");
                break;
            case CONFLICT:
                DOM.setStyleAttribute(cellElement, "backgroundColor", "#ffcec6");
                DOM.setStyleAttribute(cellElement, "borderLeft", "1px solid #d17c7c");
                break;
        }
    }
}
