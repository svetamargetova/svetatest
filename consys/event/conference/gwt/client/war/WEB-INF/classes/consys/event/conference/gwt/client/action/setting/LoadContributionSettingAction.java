package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.CommonThumb;

/**
 * Akce pro nacteni nastaveni prispevku
 * @author pepa
 */
public class LoadContributionSettingAction extends EventAction<ArrayListResult<CommonThumb>> {

    private static final long serialVersionUID = -7672588960910681824L;
}
