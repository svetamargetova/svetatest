package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContribution;

/**
 * Akce pro vytvoreni odeslaneho prispevku
 * @author pepa
 */
public class CreateContributionAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = 1927612275489907811L;
    // data
    private ClientContribution contribution;
    private String artefactTypeUuid;

    public CreateContributionAction() {
    }

    public CreateContributionAction(ClientContribution contribution, String artefactTypeUuid) {
        this.contribution = contribution;
        this.artefactTypeUuid = artefactTypeUuid;
    }

    public ClientContribution getContribution() {
        return contribution;
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }
}
