package consys.event.conference.gwt.client.module.contributor.item;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.client.module.contribution.item.ContributionFormArtefactsPanelBuilder;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.List;

/**
 *
 * @author palo
 */
public class NewContributionArtefactPanel extends ContributionFormArtefactsPanelBuilder {

    public NewContributionArtefactPanel(ClientContributionHeadData head, List<ClientReviewCycle> cycles) {
        super(cycles, head);
    }

    @Override
    protected HeadlinePanel createHeadlinePanel() {
        return new HeadlinePanel(ECMessageUtils.c.newContributionForm_text_artefactsPanel_title(), ECMessageUtils.c.newContributionForm_text_artefactsPanel_title(), ECMessageUtils.c.newContributionForm_text_artefactsPanel_description());
    }

    @Override
    public void addArtefact(ClientArtefactType artefactType, FormV2 artefactFormPanel, FlowPanel panel, boolean editable) {
        // nic                        
    }

    @Override
    public boolean canEditArtefact(boolean nowInReviewCycleTerm) {
        return true && nowInReviewCycleTerm;
    }

    @Override
    public boolean useUpdateArtefactUrl() {
        return false;
    }
}
