package consys.event.conference.gwt.client.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 *
 * @author pepa
 */
public class Separator extends Composite {

    private Separator() {
        SimplePanel panel = new SimplePanel();
        panel.setStyleName(ECResourceUtils.css().separator());
        initWidget(panel);
    }

    /** separator zarovnany doprava o sirce 570px */
    public static Widget formWidth() {
        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ECResourceUtils.css().separatorWrapper());
        panel.add(new Separator());
        panel.add(StyleUtils.clearDiv());
        return panel;
    }

    /** separator pres celou sirku nadrazene komponenty */
    public static Widget fullWidth() {
        return new Separator();
    }
}
