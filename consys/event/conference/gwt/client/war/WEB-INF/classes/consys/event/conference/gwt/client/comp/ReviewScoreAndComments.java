package consys.event.conference.gwt.client.comp;

import consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.common.gwt.client.ui.event.HelpEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.module.reviewer.ContributionReviewerForm;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Komponenta zobrazuje hodnoceni recenzenta a jeho komentare.
 *
 * @author pepa
 */
public class ReviewScoreAndComments extends Composite {

    // konstanty
    private static final int COMMENT_THRESHOLD = 160;
    // komponenty
    private FlowPanel panel;
    // data
    private List<ReviewScoreAndCommentsData> data;
    private boolean onlyPublicComments;
    private ClientContributionHeadData head;

    public ReviewScoreAndComments(List<ReviewScoreAndCommentsData> data, ClientContributionHeadData head) {
        this(data, false, head);
    }

    public ReviewScoreAndComments(List<ReviewScoreAndCommentsData> data, boolean showOnlyPublicComments, ClientContributionHeadData head) {
        this.data = data;
        this.onlyPublicComments = showOnlyPublicComments;
        this.head = head;

        panel = new FlowPanel();
        initWidget(panel);

        panel.setStyleName(ECResourceUtils.css().reviewScoreAndComments());
    }

    @Override
    protected void onLoad() {
        panel.clear();
        generate();
    }

    private void generate() {
        // panel
        if (data == null || data.isEmpty()) {            
            return;
        }
        HeadlinePanel headlinePanel = new HeadlinePanel(ECMessageUtils.c.reviewScoreAndComments_title());
        panel.add(headlinePanel);

        // data
        FormV2 form = new FormV2();
        for (int i = 0; i < data.size(); i++) {
            ReviewScoreAndCommentsData evaluation = data.get(i);

            EvaluationDisplay display = EvaluationDisplay.forList();
            display.addStyleName(ECResourceUtils.css().displayInWrapper());
            EvaluationItemValue overall = new EvaluationItemValue(evaluation.getEvaluation().getOverallScore());
            overall.setHelpTitle(ECMessageUtils.c.reviewScoreAndComments_overallEvaluationHelp_title());
            overall.setHelpText(ECMessageUtils.c.reviewScoreAndComments_overallEvaluationHelp_text());
            if (StringUtils.isNotEmpty(evaluation.getReviewUuid())) {
                overall.setOnClickAction(reviewAction(evaluation.getReviewUuid(), null));
            }

            display.setOverallScore(overall);

            List<EvaluationItemValue> items = new ArrayList<EvaluationItemValue>(evaluation.getEvaluation().getPartialScores().size());
            for (EvaluationValue value : evaluation.getEvaluation().getPartialScores()) {
                EvaluationItemValue partial = new EvaluationItemValue(value);
                partial.setHelpTitle(ECMessageUtils.c.reviewScoreAndComments_partialEvaluationHelp_title());
                partial.setHelpText(ECMessageUtils.c.reviewScoreAndComments_partialEvaluationHelp_text());
                if (StringUtils.isNotEmpty(value.getUuid())) {
                    partial.setOnClickAction(reviewAction(evaluation.getReviewUuid(), value.getUuid()));
                }
                items.add(partial);
            }

            display.setPartialScores(items);

            FlowPanel displayWrapper = new FlowPanel();
            displayWrapper.add(display);
            displayWrapper.add(StyleUtils.clearDiv());

            form.add(displayWrapper, getReviewerInfo(evaluation), false);
            if (StringUtils.isNotEmpty(evaluation.getPublicComment())) {
                String title = onlyPublicComments ? ECMessageUtils.c.const_comment() : ECMessageUtils.c.const_publicComment();
                form.add(title, comment(evaluation.getPublicComment()), true);
            }
            if (StringUtils.isNotEmpty(evaluation.getPrivateComment()) && !onlyPublicComments) {
                form.add(ECMessageUtils.c.const_privateComment(), comment(evaluation.getPrivateComment()), true);
            }

            if (i + 1 < data.size()) {
                form.addValueWidget(Separator.formWidth());
            }
        }
        panel.add(form);

    }

    private FlowPanel getReviewerInfo(ReviewScoreAndCommentsData data) {
        SimplePanel wrapperImage = new SimplePanel();
        wrapperImage.setStyleName(ECResourceUtils.css().reviewScoreAndCommentsProfile());
        wrapperImage.setWidget(FormUtils.userPortrait(data.getReviewerImageUuid(), UserProfileImageEnum.LIST));

        Label name = StyleUtils.getStyledLabel(data.getReviewerName(), ECResourceUtils.css().name());
        Label affiliation = StyleUtils.getStyledLabel(data.getReviewerOrganization(), ECResourceUtils.css().affiliation());

        FlowPanel wrapperText = new FlowPanel();
        wrapperText.setStyleName(ECResourceUtils.css().reviewScoreAndCommentsProfileText());
        wrapperText.add(name);
        wrapperText.add(affiliation);

        FlowPanel p = new FlowPanel();
        p.add(wrapperImage);
        p.add(wrapperText);
        p.add(StyleUtils.clearDiv());
        return p;
    }

    private FlowPanel comment(String comment) {
        boolean trunclated = comment.length() > COMMENT_THRESHOLD;
        final InlineLabel label = new InlineLabel(FormUtils.shortTextOnWord(COMMENT_THRESHOLD, comment));

        ActionLabel more = new ActionLabel(UIMessageUtils.c.const_more(), ResourceUtils.css().more());
        more.setClickHandler(moreClickHandler(more, label, comment));
        more.setVisible(trunclated);

        FlowPanel p = new FlowPanel();
        p.add(label);
        p.add(more);
        return p;
    }

    private ClickHandler moreClickHandler(final ActionLabel more, final InlineLabel label, final String comment) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                more.setVisible(false);
                label.setText(comment);
            }
        };
    }

    private ConsysAction reviewAction(final String reviewUuid, final String artefactUuid) {
        return new ConsysAction() {

            @Override
            public void run() {
                // schovame napovedu
                EventBus.fire(new HelpEvent(null));

                ContributionReviewerForm form;

                if (artefactUuid != null) {
                    // proklik na recenzi s prescrollovanim na vybrany artefat
                    form = new ContributionReviewerForm(head.getTitle(), head.getUuid(), reviewUuid, onlyPublicComments);
                    form.scrollToArtefactReview(artefactUuid);
                } else {
                    // proklik na celou recenzi
                    form = new ContributionReviewerForm(head.getTitle(), head.getUuid(), reviewUuid, onlyPublicComments);
                }
                FormUtils.fireNextBreadcrumb(head.getTitle(), form);
            }
        };
    }
}
