package consys.event.conference.gwt.client.module.contributor.item;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.client.module.contribution.item.ContributionFormArtefactsPanelBuilder;
import consys.event.conference.gwt.client.module.contribution.item.ContributionFormArtefactsPanelBuilderUtils;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.List;

/**
 *
 * @author palo
 */
public class UpdateContributionArtefactPanel extends ContributionFormArtefactsPanelBuilder {

    public UpdateContributionArtefactPanel(List<ClientReviewCycle> cycles, ClientContributionHeadData head) {
        super(cycles, head);
    }

    @Override
    public void addArtefact(ClientArtefactType artefactType, FormV2 artefactFormPanel, FlowPanel panel, boolean editable) {
        if (artefactType instanceof ClientReviewArtefactChair) {
            // zrecenzovane
            ContributionFormArtefactsPanelBuilderUtils.addArtefactOverallReview((ClientReviewArtefactChair) artefactType,
                    artefactFormPanel, getContributionUuid(), getContributionTitle(), false);
        }
    }

    @Override
    public boolean canEditArtefact(boolean nowInReviewCycleTerm) {
        return true && nowInReviewCycleTerm;
    }

    @Override
    public boolean useUpdateArtefactUrl() {
        return true;
    }

    @Override
    protected HeadlinePanel createHeadlinePanel() {
        return new HeadlinePanel(ECMessageUtils.c.contributionReviewerForm_text_artifactsOfContribution());
    }
}
