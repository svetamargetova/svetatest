package consys.event.conference.gwt.client.bo;

import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
@Deprecated
public class ClientArtefactEvaluationChair implements IsSerializable {

    // identifikacia artefaktu
    private String artefactUuid;
    // popis typu artefaktu
    private String artefactTypeUuid;
    private String name;
    private String description;
    private boolean required;    
    private int numOfReviewers;    
    // Kriteria artefaktu   
    private ArrayList<ClientCriteriaThumb> criteria;
      
    // hodnoteni
    private HashMap<String, HashMap<Long, Integer>> evaluations;

    public ClientArtefactEvaluationChair() {
        criteria = new ArrayList<ClientCriteriaThumb>();
        evaluations = new HashMap<String, HashMap<Long, Integer>>();
        
    }


    public ArrayList<ClientCriteriaThumb> getCriteria() {
        return criteria;
    }

    public void setCriteria(ArrayList<ClientCriteriaThumb> criteria) {
        this.criteria = criteria;
    }

    public HashMap<String, HashMap<Long, Integer>> getEvaluations() {
        return evaluations;
    }

    public void setEvaluation(HashMap<String, HashMap<Long, Integer>> evaluations) {
        this.evaluations = evaluations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    public void setArtefactUuid(String uuid) {
        this.artefactUuid = uuid;
    }

    /**
     * @return the artefactTypeUuid
     */
    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }

    /**
     * @param artefactTypeUuid the artefactTypeUuid to set
     */
    public void setArtefactTypeUuid(String artefactTypeUuid) {
        this.artefactTypeUuid = artefactTypeUuid;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * @return the numOfReviewers
     */
    public int getNumOfReviewers() {
        return numOfReviewers;
    }

    /**
     * @param numOfReviewers the numOfReviewers to set
     */
    public void setNumOfReviewers(int numOfReviewers) {
        this.numOfReviewers = numOfReviewers;
    }    
}
