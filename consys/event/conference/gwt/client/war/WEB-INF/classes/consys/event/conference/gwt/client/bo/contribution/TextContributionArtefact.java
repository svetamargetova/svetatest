package consys.event.conference.gwt.client.bo.contribution;

import java.io.Serializable;

/**
 *
 * @author palo
 */
public class TextContributionArtefact implements Serializable {

    private static final long serialVersionUID = -5922183233281381906L;
    // data
    private String artefactTypeUuid;
    private String artefactText;

    public TextContributionArtefact() {
    }

    public TextContributionArtefact(String artefactTypeUuid, String artefactText) {
        this.artefactTypeUuid = artefactTypeUuid;
        this.artefactText = artefactText;
    }

    public String getArtefactText() {
        return artefactText;
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }
}
