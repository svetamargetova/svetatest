package consys.event.conference.gwt.client.bo.setting;

import consys.common.gwt.shared.action.Result;

/**
 * Jeden bod kriteria
 * @author palo
 */
public class ClientCriteriaItem implements Result {

    private static final long serialVersionUID = -3364069770814656326L;
    // data
    private Long id;
    private String name;

    public ClientCriteriaItem(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public ClientCriteriaItem() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
