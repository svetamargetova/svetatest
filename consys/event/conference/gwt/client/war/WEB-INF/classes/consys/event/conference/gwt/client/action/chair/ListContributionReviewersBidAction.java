package consys.event.conference.gwt.client.action.chair;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers;

/**
 * Akce pro nacteni dat prirazeni recenzentu k prispevku
 * @author pepa
 */
public class ListContributionReviewersBidAction extends EventAction<ClientAssignReviewers> {

    private static final long serialVersionUID = 1723200803068578418L;
    // data
    private String uuid;

    public ListContributionReviewersBidAction() {
    }

    public ListContributionReviewersBidAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
