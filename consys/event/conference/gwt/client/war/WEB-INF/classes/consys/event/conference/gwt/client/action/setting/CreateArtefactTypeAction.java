package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import java.util.ArrayList;

/**
 * Akce pro vytvoreni artefact type
 * @author pepa
 */
public class CreateArtefactTypeAction extends EventAction<ArrayListResult<String>> {

    // konstanty
    private static final long serialVersionUID = -1630877819303916435L;
    // data
    private ArrayList<ClientArtefactTypeThumb> type;
    private String submissionTypeUuid;

    public CreateArtefactTypeAction() {
    }

    public CreateArtefactTypeAction(String submissionTypeUuid, ArrayList<ClientArtefactTypeThumb> type) {
        this.submissionTypeUuid = submissionTypeUuid;
        this.type = type;
    }

    public ArrayList<ClientArtefactTypeThumb> getType() {
        return type;
    }

    public String getSubmissionTypeUuid() {
        return submissionTypeUuid;
    }
}
