package consys.event.conference.gwt.client.module.reviewer.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.reviewer.LoadContributionArtefactReviewAction;
import consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import java.util.ArrayList;

/**
 * ClickHandler pro tlacitko an rozkliknuti recenze
 * @author pepa
 */
public class ReviewButtonClickHandler implements ClickHandler {

    // data
    private boolean open = false;
    private ConsysActionWithValue<EvaluationItemValue> callback;
    private ClientReviewArtefact clientReviewArtefact;
    private EvaluationItem evaluationItem;
    private ReviewerEvaluation reviewerEvaluation;
    private String contributionUuid;
    private String reviewUuid;
    private ActionImage button;

    public void setCallback(ConsysActionWithValue<EvaluationItemValue> callback) {
        this.callback = callback;
    }

    public void setClientReviewArtefact(ClientReviewArtefact clientReviewArtefact) {
        this.clientReviewArtefact = clientReviewArtefact;
    }

    public void setContributionUuid(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    public void setEvaluationItem(EvaluationItem evaluationItem) {
        this.evaluationItem = evaluationItem;
    }

    public void setReviewerEvaluation(ReviewerEvaluation reviewerEvaluation) {
        this.reviewerEvaluation = reviewerEvaluation;
    }

    public void setReviewUuid(String reviewUuid) {
        this.reviewUuid = reviewUuid;
    }

    public void setButton(ActionImage button) {
        this.button = button;
    }

    public boolean isOpen() {
        return open;
    }

    @Override
    public void onClick(ClickEvent event) {
        if (open) {
            open = false;
            reset(open);
            return;
        }

        reset(true);

        LoadContributionArtefactReviewAction action = new LoadContributionArtefactReviewAction(
                clientReviewArtefact.getArtefactUuid(),
                clientReviewArtefact.getArtefactTypeUuid(),
                contributionUuid,
                reviewUuid);

        EventBus.fire(new EventDispatchEvent(action,
                new AsyncCallback<ClientReviewReviewerData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        if (caught instanceof NoRecordsForAction) {
                            showNoCriterias();
                        }
                    }

                    @Override
                    public void onSuccess(ClientReviewReviewerData result) {
                        ArrayList<ClientReviewCriterion<Long>> criterias = result.getCriterias();
                        open = true;
                        if (criterias != null && !criterias.isEmpty()) {
                            reviewerEvaluation.setCallback(callback);
                            reviewerEvaluation.setData(result, clientReviewArtefact.getArtefactUuid(), contributionUuid, reviewUuid);
                        } else {
                            showNoCriterias();
                        }
                    }
                }, reviewerEvaluation));
    }

    private void showNoCriterias() {
        button.setVisible(false);
        reviewerEvaluation.showNoCriterias();
    }

    private void reset(boolean open) {
        reviewerEvaluation.setVisible(open);
        evaluationItem.setVisible(!open);
        reviewerEvaluation.setData(null, null, null, null);
    }
}
