package consys.event.conference.gwt.client.module.reviewer.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.comp.input.ConsysTextArea;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Komponenta pro pridani verejneho a soukromeho komentare
 * @author pepa
 */
public class EditReviewCommentsPanel extends Composite implements ActionExecutionDelegate {

    // konstanty
    private static final int INPUT_WIDTH = LayoutManager.LAYOUT_CONTENT_WIDTH_INT;
    private static final int INPUT_HEIGHT = 80;
    // komponenty
    private FlowPanel panel;
    private ConsysTextArea publicComment;
    private ConsysTextArea privateComment;
    private ActionImage update;
    private Waiting waiting;
    private FlowPanel errors;
    // data
    private String contributionUuid;
    private boolean enabled;
    private boolean onlyPublicComment;

    public EditReviewCommentsPanel(String contributionUuid, boolean enabled, boolean onlyPublicComment) {
        this.contributionUuid = contributionUuid;
        this.enabled = enabled;
        this.onlyPublicComment = onlyPublicComment;

        panel = new FlowPanel();
        panel.addStyleName(ECResourceUtils.css().addPublicAndPrivateComment());
        initWidget(panel);

        generate();
    }

    private void generate() {
        errors = new FlowPanel();

        update = ActionImage.confirm(ECMessageUtils.c.addPublicAndPrivateComment_text_updateComments());
        update.setVisible(false);
        update.setClickHandler(updateHandler());

        publicComment = textAreaComponent();
        privateComment = textAreaComponent();

        waiting = new Waiting(panel);

        String publicTitle;
        if (onlyPublicComment) {
            publicTitle = ECMessageUtils.c.const_comment();
        } else if (enabled) {
            publicTitle = ECMessageUtils.c.addPublicAndPrivateComment_text_addPublicComment();
        } else {
            publicTitle = ECMessageUtils.c.const_publicComment();
        }

        panel.add(new HeadlinePanel(publicTitle));
        panel.add(publicComment);

        if (!onlyPublicComment) {
            String privateTitle;
            if (enabled) {
                privateTitle = ECMessageUtils.c.addPublicAndPrivateComment_text_addPrivateComment();
            } else {
                privateTitle = ECMessageUtils.c.const_privateComment();
            }

            panel.add(new HeadlinePanel(privateTitle));
            panel.add(privateComment);
            panel.add(errors);
            panel.add(update);
            panel.add(StyleUtils.clearDiv());
        }
        panel.add(waiting);
    }

    private ConsysTextArea textAreaComponent() {
        ConsysTextArea comment = new ConsysTextArea(0, 0, INPUT_WIDTH, INPUT_HEIGHT, false, "#");
        comment.setOnEnterToEdit(editConsysAction());
        comment.setEnabled(enabled);
        return comment;
    }

    private ConsysAction editConsysAction() {
        return new ConsysAction() {

            @Override
            public void run() {
                update.setVisible(true);
            }
        };
    }

    public void setPublicComment(String comment) {
        publicComment.setText(comment);
    }

    public void setPrivateComment(String comment) {
        privateComment.setText(comment);
    }

    private ClickHandler updateHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EventBus.fire(new EventDispatchEvent(
                        new UpdateContributionCommentAction(contributionUuid, publicComment.getText(), privateComment.getText()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                update.setVisible(false);
                            }
                        }, EditReviewCommentsPanel.this));
            }
        };
    }

    @Override
    public void actionStarted() {
        waiting.show();
    }

    @Override
    public void actionEnds() {
        waiting.hide();
    }

    private ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setStyleName(StyleUtils.MARGIN_BOT_10);
        errors.add(failMessage);
        return failMessage;
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }
}
