/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.ClientEditReviewCycle;

/**
 *
 * @author palo
 */
public class LoadEditReviewCycleAction extends EventAction<ClientEditReviewCycle> {

    // konstanty
    private static final long serialVersionUID = -3134184178713848193L;
    // data
    private Long reviewCycleId;

    public LoadEditReviewCycleAction() {
    }

    public LoadEditReviewCycleAction(Long reviewCycleId) {
        this.reviewCycleId = reviewCycleId;
    }

    public Long getReviewCycleId() {
        return reviewCycleId;
    }
    
}
