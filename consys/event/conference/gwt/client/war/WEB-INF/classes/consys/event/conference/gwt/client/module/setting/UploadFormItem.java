package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Jedna uploadovana polozka
 * @author pepa
 */
public class UploadFormItem extends Composite {

    // data
    private CommonThumb ct;

    public UploadFormItem(CommonThumb ct) {
        super();
        this.ct = ct;

        generateLayout();               
    }

    /** vraci data polozky */
    public CommonThumb getItem() {
        return ct;
    }

    private void generateLayout() {
        // zakladny wrapper div
        final FlowPanel content = new FlowPanel();
        content.setStyleName(ECResourceUtils.css().artefactSettingsUploadedFileItem());
        
        // pridame odkaz        
        Anchor anchor = new Anchor(ct.getName(), ECResourceUtils.downloadArtefactUrl(ct.getUuid()));        
        content.add(anchor);

        Image remove = new Image(ResourceUtils.system().removeCross());
        remove.setStyleName(StyleUtils.HAND);
        Remover remover = new Remover(remove, new ConsysAction() {

            @Override
            public void run() {
                removeFromParent();
            }
        });
        

        content.add(remover);        
        initWidget(content);
        
    }
}
