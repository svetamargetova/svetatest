package consys.event.conference.gwt.client.bo.contribution;

import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import java.util.ArrayList;
import java.util.List;

/**
 * Nahled prispevku
 * @author pepa
 */
public class ClientContributionThumb extends DataListUuidItem {
    private static final long serialVersionUID = 5404216640532346758L;
    
    // data    
    private String title;
    private String subtitle;
    private String authors;
    private List<String> coauthors;
    private List<CommonThumb> artefacts;
    private ClientContributionStateEnum state = ClientContributionStateEnum.PENDING;

    public ClientContributionThumb() {
        artefacts = new ArrayList<CommonThumb>();
    }



    /**
     * @return the artefacts
     */
    public List<CommonThumb> getArtefacts() {
        return artefacts;
    }

    /**
     * @param artefacts the artefacts to set
     */
    public void setArtefacts(List<CommonThumb> artefacts) {
        this.artefacts = artefacts;
    }

    /**
     * @return the state
     */
    public ClientContributionStateEnum getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(ClientContributionStateEnum state) {
        this.state = state;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the coauthors
     */
    public List<String> getCoauthors() {
        return coauthors;
    }

    /**
     * @param coauthors the coauthors to set
     */
    public void setCoauthors(List<String> coauthors) {
        this.coauthors = coauthors;
    }
}
