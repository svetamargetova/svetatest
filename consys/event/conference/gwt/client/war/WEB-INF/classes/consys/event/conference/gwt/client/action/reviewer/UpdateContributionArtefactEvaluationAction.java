package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.HashMap;

/**
 * Akce pro zaslani hodnoceni od recenzenta pro jeden artefact
 * @author pepa
 */
public class UpdateContributionArtefactEvaluationAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -277400466784174087L;
    // data    
    private String reviewUuid;
    private String contributionUuid;
    private String artefactUuid;
    /* Hodnotenie. Kluc je ID kriteria, a hodnota je ID hodnoty kriteria */
    private HashMap<Long, Long> evaluations;
    private String publicComment;
    private String privateComment;

    public UpdateContributionArtefactEvaluationAction() {
    }

    public UpdateContributionArtefactEvaluationAction(String artefactUuid, HashMap<Long, Long> evaluations) {
        this.artefactUuid = artefactUuid;
        this.evaluations = evaluations;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }

    public void setContributionUuid(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public void setPrivateComment(String privateNote) {
        this.privateComment = privateNote;
    }

    public String getPublicComment() {
        return publicComment;
    }

    public void setPublicComment(String publicNote) {
        this.publicComment = publicNote;
    }

    public String getReviewUuid() {
        return reviewUuid;
    }

    public void setReviewUuid(String reviewUuid) {
        this.reviewUuid = reviewUuid;
    }

    public HashMap<Long, Long> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(HashMap<Long, Long> evaluations) {
        this.evaluations = evaluations;
    }
}
