package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.Date;

/**
 * Thumb a predek ClientReviewCycle
 * @author pepa
 */
public class ClientReviewCycleThumb implements IsSerializable {

    private Long id;
    private Date from;
    private Date to;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getSubmitFrom() {
        return from;
    }

    public void setSubmitFrom(Date from) {
        this.from = from;
    }

    public Date getSubmitTo() {
        return to;
    }

    public void setSubmitTo(Date to) {
        this.to = to;
    }
}
