package consys.event.conference.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Prehled hodnoceni od vsech recenzentu pro jednotlivý artefakt (zatim jen poznamky)
 * @author pepa
 */
@Deprecated
public class ClientArtefactReviews implements Result {

    private static final long serialVersionUID = -7002587520923460771L;
    // data
    private String artefactName;
    private ArrayList<String> notes;

    public ClientArtefactReviews() {
        notes = new ArrayList<String>();
    }

    public ClientArtefactReviews(String artefactName) {
        this.artefactName = artefactName;
        notes = new ArrayList<String>();
    }

    public String getArtefactName() {
        return artefactName;
    }

    public void setArtefactName(String artefactName) {
        this.artefactName = artefactName;
    }

    public ArrayList<String> getNotes() {
        return notes;
    }

    public void setNotes(ArrayList<String> notes) {
        this.notes = notes;
    }
}
