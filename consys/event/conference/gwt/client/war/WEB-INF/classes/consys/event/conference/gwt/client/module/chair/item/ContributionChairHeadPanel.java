package consys.event.conference.gwt.client.module.chair.item;

import consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead;
import consys.event.conference.gwt.client.module.contribution.item.EditableContributionHeadPanel;

/**
 * 
 * @author palo
 */
public class ContributionChairHeadPanel extends EditableContributionHeadPanel{

    public ContributionChairHeadPanel(ClientContributionChairHead cc) {
        super(cc, true);
    }

}
