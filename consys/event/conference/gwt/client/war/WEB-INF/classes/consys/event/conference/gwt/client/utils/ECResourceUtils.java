package consys.event.conference.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.comp.HiddenTextBox;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.servlet.CommonServletProperties;
import consys.common.gwt.client.EventURLFactory;
import consys.event.conference.api.servlet.ConferenceDownloadSubmissionArtefactServlet;
import consys.event.conference.api.servlet.ConferenceSubmissionArtefactUploadServlet;
import consys.event.conference.gwt.client.utils.css.ECCssResources;

/**
 *
 * @author pepa
 */
public class ECResourceUtils {

    private static ECBundle bundle = (ECBundle) GWT.create(ECBundle.class);

    public static ECBundle bundle() {
        return bundle;
    }

    public static ECCssResources css() {
        return bundle.css();
    }

    /** vraci panel s vyplnenym skrytym polem uid a eid */
    public static FlowPanel requiredFormItems() throws NotInCacheException {
        FlowPanel panel = new FlowPanel();

        ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
        ClientUser cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);

        panel.add(new HiddenTextBox(cce.getEvent().getUuid(), CommonServletProperties.FILTER_EVENT_TAG));
        panel.add(new HiddenTextBox(cu.getUuid(), CommonServletProperties.FILTER_USER_TAG));

        return panel;
    }

    /**
     * vraci string url pro upload prikladu a sablon<br>
     * pokud nastane chyba, vraci null
     */
    public static String artefactAdminUploadUrl(ArtefactUrlEnum en) throws NotInCacheException {
        return EventURLFactory.overseerServlet(ArtefactUrlEnum.value(en));
    }

    public static String downloadArtefactUrl(String aUuid) {
        try {
            ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
            ClientUser cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
            String url = EventURLFactory.overseerURL(ConferenceDownloadSubmissionArtefactServlet.URL + "?");
            url += CommonServletProperties.FILTER_EVENT_TAG + "=" + cce.getEvent().getUuid() + "&";
            url += CommonServletProperties.FILTER_USER_TAG + "=" + cu.getUuid() + "&";
            url += ConferenceDownloadSubmissionArtefactServlet.ARTEFACT_TAG + "=" + aUuid;
            return url;
        } catch (NotInCacheException ex) {
            LoggerFactory.log(ECResourceUtils.class, "NotInCacheException: downloadUserArtefactUrl");
            return null;
        }
    }

    public enum ArtefactUrlEnum {

        TEMPLATE, EXAMPLE;
        private static final long serialVersionUID = -7534254789684574802L;

        public static String value(ArtefactUrlEnum en) {
            switch (en) {
                case TEMPLATE:
                    return "/upload/template";
                case EXAMPLE:
                    return "/upload/pattern";
                default:
                    return "";
            }
        }
    }

    public static String uploadArtefactUrl(boolean updateContributionArtefact) {
        try {
            final String urlPart;
            if (updateContributionArtefact) {
                urlPart = ConferenceSubmissionArtefactUploadServlet.UPDATE_SUBMISSION_ARTEFACT_URL;
            } else {
                urlPart = ConferenceSubmissionArtefactUploadServlet.NEW_SUBMISSION_ARTEFACT_URL;
            }

            ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
            ClientUser cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
            String url = EventURLFactory.overseerURL(urlPart + "?");
            url += CommonServletProperties.FILTER_EVENT_TAG + "=" + cce.getEvent().getUuid() + "&";
            url += CommonServletProperties.FILTER_USER_TAG + "=" + cu.getUuid();
            return url;
        } catch (NotInCacheException ex) {
            LoggerFactory.log(ECResourceUtils.class, "NotInCacheException: uploadUserArtefactUrl");
            return null;
        }
    }
}
