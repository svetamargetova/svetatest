package consys.event.conference.gwt.client.module;

import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.module.event.EventNavigationItem;
import consys.common.gwt.client.module.event.EventNavigationModel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.ArrayUtils;
import consys.event.common.api.right.Role;
import consys.event.common.gwt.client.event.EventSendSubmissionRequest;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.gwt.client.module.reviewer.MyReviewsList;
import consys.event.conference.gwt.client.module.contributor.MyContributionList;
import consys.event.conference.gwt.client.module.chair.ChairContributionsList;
import consys.event.conference.gwt.client.module.contributor.NewContributionForm;
import consys.event.conference.gwt.client.module.setting.ContributionTypeContent;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConferenceModule implements EventModule, EventSendSubmissionRequest.Handler {

    @Override
    public String getLocalizedModuleName() {
        return ECMessageUtils.c.conferenceModule_text_moduleName();
    }

    @Override
    public void registerModuleRoles(List<ModuleRole> moduleRoles) {
        EventConferenceModuleRoleModel roles = new EventConferenceModuleRoleModel();
        Map<String, Role> rightMap = roles.getModelInMap();

        // RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS
        ModuleRole acceptSubmission = new ModuleRole();
        acceptSubmission.setId(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS).getIdentifier());
        acceptSubmission.setSystem(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS).isSystem());
        acceptSubmission.setShortcut(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS);
        acceptSubmission.setLocalizedName(ECMessageUtils.c.conferenceModule_text_moduleRightAcceptSubmissions());
        acceptSubmission.setLocalizedDescription(ECMessageUtils.c.conferenceModule_text_moduleRightAcceptSubmissionsDescription());
        moduleRoles.add(acceptSubmission);

        // RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS
        ModuleRole submitInternSubmission = new ModuleRole();
        submitInternSubmission.setId(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS).getIdentifier());
        submitInternSubmission.setSystem(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS).isSystem());
        submitInternSubmission.setShortcut(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS);
        submitInternSubmission.setLocalizedName(ECMessageUtils.c.conferenceModule_text_moduleRightAddInternSubmissions());
        submitInternSubmission.setLocalizedDescription(ECMessageUtils.c.conferenceModule_text_moduleRightAddInternSubmissionsDescription());
        moduleRoles.add(submitInternSubmission);

        // RIGHT_SUBMISSION_ASSING_REVIEWERS
        ModuleRole assignReviewers = new ModuleRole();
        assignReviewers.setId(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ASSING_REVIEWERS).getIdentifier());
        assignReviewers.setSystem(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ASSING_REVIEWERS).isSystem());
        assignReviewers.setShortcut(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ASSING_REVIEWERS);
        assignReviewers.setLocalizedName(ECMessageUtils.c.conferenceModule_text_moduleRightAssignsSubmissionReviewers());
        assignReviewers.setLocalizedDescription(ECMessageUtils.c.conferenceModule_text_moduleRightAssignsSubmissionReviewersDescription());
        moduleRoles.add(assignReviewers);

        // RIGHT_SUBMISSION_REVIEWER
        ModuleRole doReview = new ModuleRole();
        doReview.setId(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER).getIdentifier());
        doReview.setSystem(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER).isSystem());
        doReview.setShortcut(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER);
        doReview.setLocalizedName(ECMessageUtils.c.conferenceModule_text_moduleRightReviewer());
        doReview.setLocalizedDescription(ECMessageUtils.c.conferenceModule_text_moduleRightReviewerDescription());
        moduleRoles.add(doReview);

        // RIGHT_SUBMISSION_SETTINGS
        ModuleRole submissionSettings = new ModuleRole();
        submissionSettings.setId(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_SETTINGS).getIdentifier());
        submissionSettings.setSystem(rightMap.get(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_SETTINGS).isSystem());
        submissionSettings.setShortcut(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_SETTINGS);
        submissionSettings.setLocalizedName(ECMessageUtils.c.conferenceModule_text_moduleRightSubmissionSettings());
        submissionSettings.setLocalizedDescription(ECMessageUtils.c.conferenceModule_text_moduleRightSubmissionSettingsDescription());
        moduleRoles.add(submissionSettings);

    }
    public static final String NAVIGATION_PARENT_SUBMISSION = ECMessageUtils.c.conferenceModule_text_navigationModelSubmission();

    @Override
    public void registerEventNavigation(EventNavigationModel model) {
        // Parent pre submuission module 30
        EventNavigationItem submission = new EventNavigationItem(NAVIGATION_PARENT_SUBMISSION, 50, new ConsysAction() {
            @Override
            public void run() {
                String title = ECMessageUtils.c.conferenceModule_text_navigationModelSettings();
                FormUtils.fireNextRootBreadcrumb(title, new ContributionTypeContent());
            }
        }, true, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_SETTINGS);

        model.insertFirstLevelAddon(submission, UIMessageUtils.c.const_settings(), 5, new ConsysAction() {
            @Override
            public void run() {
                String title = ECMessageUtils.c.conferenceModule_text_navigationModelSettings();
                FormUtils.fireNextRootBreadcrumb(title, new ContributionTypeContent());
            }
        }, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_SETTINGS);

        // My Submissions [10] - Everyone
        model.insertFirstLevelAddon(submission, ECMessageUtils.c.conferenceModule_text_navigationModelMySubmissions(), 10, new ConsysAction() {
            @Override
            public void run() {
                String title = ECMessageUtils.c.conferenceModule_text_navigationModelMySubmissions();
                FormUtils.fireNextRootBreadcrumb(title, new MyContributionList());
            }
        }, ArrayUtils.EMPTY_STRING_ARRAY);

        // Vlozit prispevok interny
        model.insertFirstLevelAddon(submission, ECMessageUtils.c.conferenceModule_text_navigationModelAddSubmission(), 20, new ConsysAction() {
            @Override
            public void run() {
                String title = ECMessageUtils.c.conferenceModule_text_navigationModelAddSubmission();
                FormUtils.fireNextRootBreadcrumb(title, new NewContributionForm(true));
            }
        }, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ADD_INTERN_SUBMISSIONS);

        // My Reviews  - Reviewer
        model.insertFirstLevelAddon(submission, ECMessageUtils.c.conferenceModule_text_navigationModelMyReviews(), 30, new ConsysAction() {
            @Override
            public void run() {
                String title = ECMessageUtils.c.conferenceModule_text_navigationModelMyReviews();
                FormUtils.fireNextRootBreadcrumb(title, new MyReviewsList());
            }
        }, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER);


        // Reviews 
        model.insertFirstLevelAddon(submission, ECMessageUtils.c.conferenceModule_text_navigationModelContributions(), 60, new ConsysAction() {
            @Override
            public void run() {
                String title = ECMessageUtils.c.conferenceModule_text_navigationModelContributions();
                FormUtils.fireNextRootBreadcrumb(title, new ChairContributionsList());
            }
        }, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ASSING_REVIEWERS);
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
        EventBus.get().addHandler(EventSendSubmissionRequest.TYPE, this);
    }

    @Override
    public void registerModule() {
        // nainjectovani stylu do DOM
        ECResourceUtils.bundle().css().ensureInjected();
    }

    @Override
    public void unregisterModule() {
    }

    @Override
    public void onSendSubmission(EventSendSubmissionRequest event) {
        NewContributionForm nc = new NewContributionForm();
        FormUtils.fireNextRootBreadcrumb(ECMessageUtils.c.newContributionForm_title(), nc);
    }
}
