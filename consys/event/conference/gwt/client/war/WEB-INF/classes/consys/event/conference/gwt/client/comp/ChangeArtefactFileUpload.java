package consys.event.conference.gwt.client.comp;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.ui.comp.HiddenTextBox;
import consys.common.gwt.client.ui.comp.input.ConsysAutoFileUpload;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.event.conference.api.servlet.ConferenceSubmissionArtefactUploadServlet;
import consys.event.conference.gwt.client.event.ArtefactUploadedEvent;
import consys.event.conference.gwt.client.event.UploadedArtefactCanceledEvent;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Uploadovaci komponenta, kdy uz na serveru nejaky artefakt existuje
 * @author pepa
 */
public class ChangeArtefactFileUpload extends ConsysAutoFileUpload<ChangeArtefactUpload> {

    // komponenty
    private ChangeArtefactUpload upload;
    private HiddenTextBox contribution;
    private HiddenTextBox artefactType;
    // data
    private String contributionUuid;
    private String artefactTypeUuid;
    private String artefactUuid;
    private String[] validSuffixs;
    private ConsysTextBox textBox;
    private ConsysActionWithValue<Boolean> hideAction;
    private ConsysAction beginAction;
    private ConsysActionWithValue<Boolean> endAction;

    public ChangeArtefactFileUpload(String servletUrl, String contributionUuid, String artefactTypeUuid, String artefactUuid) {
        super(servletUrl);
        this.contributionUuid = contributionUuid;
        this.artefactTypeUuid = artefactTypeUuid;
        this.artefactUuid = artefactUuid;

        addStyleName(ECResourceUtils.css().changeUpload());

        contribution.setValue(contributionUuid);
        artefactType.setValue(artefactTypeUuid);
    }

    @Override
    protected ChangeArtefactUpload uploadComponent() {
        upload = new ChangeArtefactUpload();
        upload.setName(ConferenceSubmissionArtefactUploadServlet.FIELD_FILE);
        return upload;
    }

    @Override
    protected void additionalFormItems(FlowPanel panel) {
        contribution = new HiddenTextBox("", ConferenceSubmissionArtefactUploadServlet.FIELD_SUBMISSION_UUID);
        panel.add(contribution);

        artefactType = new HiddenTextBox("", ConferenceSubmissionArtefactUploadServlet.FIELD_ARTEFACT_TYPE);
        panel.add(artefactType);
    }

    @Override
    protected void onSubmit() {
        if (beginAction != null) {
            beginAction.run();
        }
        hideAction.run(Boolean.TRUE);
    }

    @Override
    protected boolean processResult(String result) {
        String oldArtefactUuid = artefactUuid;

        UploadResponse response = ArtefactFileUpload.processResponse(result);
        if (response.isSuccess()) {
            artefactUuid = response.getResponse();
        }

        hideAction.run(Boolean.FALSE);
        if (endAction != null) {
            endAction.run(response.isSuccess());
        }

        if (response.isSuccess()) {
            if (textBox != null) {
                textBox.setText(upload.getSelectedFilename());
            }
            // zruseni puvodniho artefaktu
            EventBus.fire(new UploadedArtefactCanceledEvent(oldArtefactUuid,artefactTypeUuid));
            // potvrzeni noveho
            EventBus.fire(new ArtefactUploadedEvent(contributionUuid, artefactTypeUuid, artefactUuid));
            return true;
        } else {
            ArtefactFileUpload.errorUpload(getInfoLabel());
            return false;
        }
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }        

    @Override
    protected String[] validSuffixs() {
        return validSuffixs;
    }

    public void setValidSuffixs(String suffixList) {
        validSuffixs = ArtefactFileUpload.parseValidSuffixs(suffixList);
    }

    public void setTextBoxWithFilename(ConsysTextBox textBox) {
        this.textBox = textBox;
    }

    public void setHideAction(ConsysActionWithValue<Boolean> hideAction) {
        this.hideAction = hideAction;
    }

    public void uploadBegin(ConsysAction beginAction) {
        this.beginAction = beginAction;
    }

    public void uploadEnd(ConsysActionWithValue<Boolean> endAction) {
        this.endAction = endAction;
    }

    /** vrací nazev uploadovaneho souboru */
    public String getFileName() {
        return upload.getFilename();
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }
}
