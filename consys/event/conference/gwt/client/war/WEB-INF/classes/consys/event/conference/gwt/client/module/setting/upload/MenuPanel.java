package consys.event.conference.gwt.client.module.setting.upload;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.utils.StyleUtils;

/**
 * Prepinatelna zalozky
 * @author pepa
 */
public class MenuPanel extends FlowPanel {

    // komponenty
    private Label label;
    // data
    private boolean selected;

    public MenuPanel(String name) {
        super();
        selected = false;
        label = StyleUtils.getStyledLabel(name, StyleUtils.PADDING_VER_5, StyleUtils.FONT_BOLD);
        label.addStyleName(StyleUtils.PADDING_RIGHT_5);
        label.addStyleName(StyleUtils.FLOAT_RIGHT);

        addStyleName(StyleUtils.HAND);
        add(label);
        add(StyleUtils.clearDiv());

        sinkEvents(Event.ONCLICK | Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        initHandlers();
    }

    private void initHandlers() {
        addHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                addStyleName(StyleUtils.BACKGROUND_GRAY);
                if (selected) {
                    removeStyleName(StyleUtils.BACKGROUND_GREEN);
                } else {
                    removeStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
                }
            }
        }, MouseOverEvent.getType());
        addHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                removeStyleName(StyleUtils.BACKGROUND_GRAY);
                if (selected) {
                    addStyleName(StyleUtils.BACKGROUND_GREEN);
                } else {
                    addStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
                }
            }
        }, MouseOutEvent.getType());
    }

    public void addClickHandler(ClickHandler handler) {
        addHandler(handler, ClickEvent.getType());
    }

    public void select() {
        addStyleName(StyleUtils.BACKGROUND_GREEN);
        removeStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
        selected = true;
    }

    public void unselect() {
        addStyleName(StyleUtils.BACKGROUND_TRANSPARENT);
        removeStyleName(StyleUtils.BACKGROUND_GREEN);
        selected = false;
    }
}
