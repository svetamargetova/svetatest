package consys.event.conference.gwt.client.bo.setting;

import consys.common.gwt.shared.action.Result;

/**
 * Thumb artefaktu
 *
 * @author pepa
 */
public class ClientArtefactTypeThumb implements Result {

    private static final long serialVersionUID = 5167853893846300920L;
    // data
    private String artefactTypeUuid;
    private String artefactTypeName;
    private String artefactTypeDescription;

    /**
     * @return the artefactTypeUuid
     */
    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }

    /**
     * @param artefactTypeUuid the artefactTypeUuid to set
     */
    public void setArtefactTypeUuid(String artefactTypeUuid) {
        this.artefactTypeUuid = artefactTypeUuid;
    }

    /**
     * @return the artefactTypeName
     */
    public String getArtefactTypeName() {
        return artefactTypeName;
    }

    /**
     * @param artefactTypeName the artefactTypeName to set
     */
    public void setArtefactTypeName(String artefactTypeName) {
        this.artefactTypeName = artefactTypeName;
    }

    /**
     * @return the artefactTypeDescription
     */
    public String getArtefactTypeDescription() {
        return artefactTypeDescription;
    }

    /**
     * @param artefactTypeDescription the artefactTypeDescription to set
     */
    public void setArtefactTypeDescription(String artefactTypeDescription) {
        this.artefactTypeDescription = artefactTypeDescription;
    }
}
