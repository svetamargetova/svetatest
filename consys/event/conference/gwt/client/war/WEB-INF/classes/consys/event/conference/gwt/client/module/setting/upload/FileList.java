package consys.event.conference.gwt.client.module.setting.upload;

import consys.common.gwt.client.ui.comp.panel.ListPanel;
import consys.common.gwt.shared.bo.CommonThumb;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class FileList extends ListPanel<UploadItem> {

    // data
    private UploadArtefactDialog dialog;
    private SelectPage parent;

    public FileList(int visible, UploadArtefactDialog dialog, SelectPage parent) {
        super(UploadItem.HEIGHT, visible);
        this.dialog = dialog;
        this.parent = parent;
    }

    public void setUploadedFiles(ArrayList<CommonThumb> files) {
        for (CommonThumb file : files) {
            addItem(new UploadItem(file, dialog, parent));
        }
    }
}
