package consys.event.conference.gwt.client.bo.reviewer;

import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import java.util.ArrayList;

/**
 * Data pro vyplneni formulare recenzenta pro hodnoceni
 * @author pepa
 */
public class ClientContributionReviewerFormData implements Result {

    private static final long serialVersionUID = -3558503857371438150L;
    // data
    private String publicComment;
    private String privateComment;    
    private boolean isAuthor;
    private ClientContributionReviewerHead head;
    private ArrayList<ClientReviewCycle> reviewCycles;    

    public ClientContributionReviewerFormData() {
        reviewCycles = new ArrayList<ClientReviewCycle>();
    }

    public ClientContributionReviewerHead getHead() {
        return head;
    }

    public void setHead(ClientContributionReviewerHead head) {
        this.head = head;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    public String getPublicComment() {
        return publicComment;
    }

    public void setPublicComment(String publicComment) {
        this.publicComment = publicComment;
    }

    public ArrayList<ClientReviewCycle> getReviewCycles() {
        return reviewCycles;
    }

    /**
     * @return the isAuthor
     */
    public boolean isIsAuthor() {
        return isAuthor;
    }

    /**
     * @param isAuthor the isAuthor to set
     */
    public void setIsAuthor(boolean isAuthor) {
        this.isAuthor = isAuthor;
    }
    
    
}
