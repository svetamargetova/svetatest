package consys.event.conference.gwt.client.bo.setting;

import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Prirazeni recenzentu k prispevku
 * @author pepa
 */
public class ClientAssignReviewers implements IsSerializable, Result {

    private static final long serialVersionUID = -8045940969610152860L;
    // data
    private ClientContributionHeadData head;
    private ArrayList<ClientTopic> topics;
    private ArrayList<ClientReviewer> reviewers;

    public ClientAssignReviewers() {
        reviewers = new ArrayList<ClientReviewer>();        
    }   

    public ClientContributionHeadData getContributionHead() {
        return head;
    }

    public void setContributionHead(ClientContributionHeadData contribution) {
        this.head = contribution;
    }

    public ArrayList<ClientReviewer> getReviewers() {
        return reviewers;
    }

    public void setReviewers(ArrayList<ClientReviewer> reviewers) {
        this.reviewers = reviewers;
    }

    /**
     * @return the topics
     */
    public ArrayList<ClientTopic> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(ArrayList<ClientTopic> topics) {
        this.topics = topics;
    }
}
