package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysAsyncAction;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.RollOutPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.DeleteCriteriaAction;
import consys.event.conference.gwt.client.action.setting.LoadCriteriaAction;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCriteriaException;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Jedna polozka kriteria
 * @author pepa
 */
public class CriteriaItem extends RollOutPanel {

    // komponenty
    private Label titleLabel;
    private Label descriptionText;
    private HTML pointsHTML;
    private HTML pointsListHTML;
    private HTML minimumPointsHTML;
    private FlowPanel failPanel;
    // data
    private ClientCriteria cc;

    public CriteriaItem(ClientCriteriaThumb ccInt) {
        super();
        setStyleName(StyleUtils.MARGIN_TOP_10);
        setWidth("100%");
        enableWaiting();

        cc = new ClientCriteria();
        cc.setId(ccInt.getId());
        cc.setName(ccInt.getName());
        if (ccInt instanceof ClientCriteria) {
            ClientCriteria ccIn = (ClientCriteria) ccInt;
            cc.setDescription(ccIn.getDescription());
            cc.setPoints(ccIn.getPoints());
            cc.setMinimumPoints(ccIn.getMinimumPoints());
        }

        // hlavicka
        titleLabel = StyleUtils.getStyledLabel(cc.getName(), RollOutPanel.TITLE_TEXT_STYLE);

        ConsysAction editAction = new ConsysAction() {

            @Override
            public void run() {
                EditFormCriteria w = new EditFormCriteria(cc.getId(), CriteriaItem.this);
                String title = ECMessageUtils.c.editFormCriteria_title() + " " + cc.getName();
                FormUtils.fireNextBreadcrumb(title, w);
            }
        };

        ConsysAction removeAction = new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent removeEvent = new EventDispatchEvent(new DeleteCriteriaAction(),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby obstarava ActionExecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                } else if (caught instanceof DeleteReviewCriteriaException) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionDeleteReviewCriteria());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                CriteriaItem.this.removeFromParent();
                            }
                        }, CriteriaItem.this);
                EventBus.get().fireEvent(removeEvent);
            }
        };

        EditWithRemover ewr = new EditWithRemover(editAction, removeAction);
        ewr.atRight();

        failPanel = new FlowPanel();

        FlowPanel titlePanel = new FlowPanel();
        titlePanel.addStyleName(StyleUtils.FLOAT_LEFT);
        titlePanel.add(titleLabel);
        titlePanel.add(failPanel);

        String headWidth = (ContributionTypeItem.WIDTH - (2 * RollOutPanel.LEFT_PART_WIDTH)) + "px";

        FlowPanel head = new FlowPanel();
        head.setWidth(headWidth);
        head.add(ewr);
        head.add(titlePanel);
        head.add(StyleUtils.clearDiv());

        setTitle(head);
        setMainPanelWidth(headWidth);

        // obsah
        descriptionText = StyleUtils.getStyledLabel("", StyleUtils.MARGIN_BOT_10);
        pointsHTML = new HTML();
        pointsListHTML = new HTML();
        pointsListHTML.addStyleName(StyleUtils.MARGIN_TOP_5);
        minimumPointsHTML = new HTML();
        minimumPointsHTML.setStyleName(StyleUtils.MARGIN_TOP_10);

        FlowPanel content = new FlowPanel();
        content.setStyleName(StyleUtils.MARGIN_TOP_10);
        content.setWidth((ContributionTypeItem.WIDTH - (2 * RollOutPanel.LEFT_PART_WIDTH)) + "px");
        content.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_description(), StyleUtils.FONT_BOLD));
        content.add(descriptionText);
        content.add(pointsHTML);
        content.add(pointsListHTML);
        content.add(minimumPointsHTML);

        setContent(content);
    }

    /** zobrazi info */
    private void showInfo() {
        titleLabel.setText(cc.getName());
        descriptionText.setText(FormUtils.valueOrDash(cc.getDescription()));
        pointsHTML.setHTML("<b>" + ECMessageUtils.c.editFormCriteria_form_points() + "</b>: " + cc.getPoints().size());

        String points = "";
        int i = 1;
        for (ClientCriteriaItem p : cc.getPoints()) {
            points += "<div class=\"" + StyleUtils.FLOAT_LEFT + " " + StyleUtils.CLEAR_BOTH
                    + "\" style=\"width:50px;\"><div class=\""
                    + StyleUtils.FLOAT_RIGHT + " "
                    + StyleUtils.MARGIN_RIGHT_10 + "\">" + i + ":</div></div>";
            points += "<div class=\"" + StyleUtils.FLOAT_LEFT + "\" style=\"width:150px;\">" + p.getName() + "</div>";
            if (i == 1) {
                points += "<div class=\"" + StyleUtils.FLOAT_LEFT + "\">"
                        + ECMessageUtils.c.editFormCriteria_text_minimumValue() + "</div>";
            }
            if (i != cc.getPoints().size()) {
                points += StyleUtils.clearDiv();
            }
            i++;
        }
        points += "<div class=\"" + StyleUtils.FLOAT_LEFT + "\">"
                + ECMessageUtils.c.editFormCriteria_text_maximumValue() + "</div>" + StyleUtils.clearDiv();
        pointsListHTML.setHTML(points);

        minimumPointsHTML.setHTML("<b>" + ECMessageUtils.c.editFormCriteria_text_minimumPoints() + "</b>: "
                + (cc.getMinimumPoints() > 0 ? FormUtils.doubleFormat.format(cc.getMinimumPoints()) : ECMessageUtils.c.const_notSet())
                + " (" + ECMessageUtils.c.editFormCriteria_text_minimumPerAllReviewers() + ")");
    }

    /** vraci kriterium */
    public ClientCriteria getCriteria() {
        return cc;
    }

    /** nastavi kriterium */
    public void setCriteria(ClientCriteria cc) {
        this.cc = cc;
        showInfo();
    }

    /** vraci chybovou zpravu */
    private ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setStyleName(StyleUtils.MARGIN_TOP_10);
        failMessage.setVisible(false);
        failPanel.add(failMessage);
        return failMessage;
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    @Override
    protected void onOpenRoll(final ConsysAsyncAction onAction) {
        EventDispatchEvent loadEvent = new EventDispatchEvent(new LoadCriteriaAction(cc.getId()),
                new AsyncCallback<ClientCriteria>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava ActionExecutor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientCriteria result) {
                        setCriteria(result);
                        onAction.onSuccess();
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);
    }
}
