package consys.event.conference.gwt.client.action.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class DeleteReviewCriteriaException extends ActionException {

    private static final long serialVersionUID = -6331817380354178617L;

    public DeleteReviewCriteriaException() {
        super();
    }
}
