package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani kolecka hodnoceni
 * @author pepa
 */
public class DeleteReviewCycleAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = 631350422073440455L;
    // data    
    private Long cycleId;

    public DeleteReviewCycleAction() {
    }

    public DeleteReviewCycleAction(Long cycleId) {
        this.cycleId = cycleId;
    }

    
    

    /**
     * @return the cycleId
     */
    public Long getCycleId() {
        return cycleId;
    }
}
