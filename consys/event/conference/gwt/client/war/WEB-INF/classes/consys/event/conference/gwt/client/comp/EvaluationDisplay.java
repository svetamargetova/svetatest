package consys.event.conference.gwt.client.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItem.EvaluationItemType;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.List;

/**
 * Zobrazovátko hodnocení
 * <p/>
 * @author pepa
 */
public class EvaluationDisplay<PARTIAL extends EvaluationItemValue, OVERALL extends EvaluationItemValue> extends Composite {

    // komponenty
    private SimplePanel mainEvaluation;
    private FlowPanel partialEvaluations;
    // data
    private EvaluationDisplaySize size;
    private EvaluationItemType mainItemType;
    private OVERALL overallScore;
    private List<PARTIAL> partialScores;

    /** enum velikosti zobrazovatka */
    private enum EvaluationDisplaySize {

        LIST, HEAD;
        private static final long serialVersionUID = -5900216932551222751L;
    }

    private EvaluationDisplay(EvaluationDisplaySize size) {
        this.size = size;
        initComponents();
    }

    private void initComponents() {
        mainEvaluation = new SimplePanel();
        mainEvaluation.setStyleName(StyleUtils.FLOAT_RIGHT);

        partialEvaluations = new FlowPanel();

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ECResourceUtils.css().evaluationDisplay());
        panel.add(mainEvaluation);
        panel.add(StyleUtils.clearDiv());
        panel.add(partialEvaluations);

        initWidget(panel);

        switch (size) {
            case LIST:
                mainItemType = EvaluationItemType.MEDIUM;
                partialEvaluations.setStyleName(ECResourceUtils.css().evaluationDisplayList());
                break;
            case HEAD:
                mainItemType = EvaluationItemType.BIG;
                break;
        }
    }

    public void setOverallScore(OVERALL overallScore) {
        this.overallScore = overallScore;
        refresh();
    }

    public void setPartialScores(List<PARTIAL> partialScores) {
        this.partialScores = partialScores;
        refresh();
    }

    public OVERALL getOverallScore() {
        return overallScore;
    }

    public List<PARTIAL> getPartialScores() {
        return partialScores;
    }

    private void refresh() {
        partialEvaluations.clear();
        if (getOverallScore() != null) {
            mainEvaluation.setWidget(new EvaluationItem(getOverallScore(), mainItemType));
            if (getPartialScores() != null && !getPartialScores().isEmpty()) {
                int counter = 1;
                for (EvaluationItemValue value : getPartialScores()) {
                    EvaluationItem item = EvaluationItem.small(value);
                    item.addStyleName(ECResourceUtils.css().evaluationDisplayItems());
                    if (counter % 3 != 0) {
                        item.addStyleName(ECResourceUtils.css().evaluationDisplayItemsMargin());
                    }
                    partialEvaluations.add(item);
                    counter++;
                }
                partialEvaluations.add(StyleUtils.clearDiv());
            }
        } else {
            mainEvaluation.setWidget(null);
        }
    }

    /** zaktualizuje polozku se zadanym uuid na novou hodnotu */
    public void update(PARTIAL value) {
        boolean valueChanged = false;
        int validItemCounter = 0;
        int sum = 0;
        for (int i = 0; i < partialEvaluations.getWidgetCount(); i++) {
            Widget w = partialEvaluations.getWidget(i);
            if (w instanceof EvaluationItem) {
                EvaluationItem item = (EvaluationItem) w;
                if (value.getValue().getUuid().equals(item.getValue().getValue().getUuid())) {
                    // zmena spravneho policka
                    item.setValue(value);
                    valueChanged = true;
                }
                // predpocitavani noveho celkoveho skore
                if (!item.getValue().isNotEntered()) {
                    validItemCounter++;
                    sum += item.getValue().getValue().getValue();
                }
            }
        }
        if (valueChanged) {
            // nejakemu policku byla nastavena nova hodnota, prepocitame take celkove skore
            EvaluationItem overall = (EvaluationItem) mainEvaluation.getWidget();
            OVERALL oldOverallValue = getOverallScore();

            EvaluationValue ev = new EvaluationValue(sum / validItemCounter);
            EvaluationItemValue newOverallValue = new EvaluationItemValue(ev);
            newOverallValue.setHelpTitle(oldOverallValue.getHelpTitle());
            newOverallValue.setHelpText(oldOverallValue.getHelpTitle());

            overall.setValue(newOverallValue);
        }
    }

    public static EvaluationDisplay forList() {
        return new EvaluationDisplay(EvaluationDisplaySize.LIST);
    }

    public static EvaluationDisplay forHead() {
        return new EvaluationDisplay(EvaluationDisplaySize.HEAD);
    }
}
