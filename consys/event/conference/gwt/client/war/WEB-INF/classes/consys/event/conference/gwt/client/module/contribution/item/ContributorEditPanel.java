package consys.event.conference.gwt.client.module.contribution.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.ui.comp.user.UserCard;
import consys.common.gwt.client.ui.comp.user.UserLabel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.event.common.gwt.client.action.CurrentEventUserRightAction;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * UI pro zobrazeni contributora
 * @author pepa
 */
public class ContributorEditPanel extends FlowPanel {

    
    
    public ContributorEditPanel(final ClientInvitedUser item, boolean removable) {
        super();

        setStyleName(ECResourceUtils.css().contributorEditPanel());
        
        UserLabel user = new UserLabel(item.getUuid(), item.getName());                
        add(user);
        
        if (removable) {            
            Image remove = new Image(ResourceUtils.system().removeCross());
            remove.setStyleName(ECResourceUtils.css().contributorEditPanelRemove());
            remove.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    removeFromParent();
                }
            });
            add(remove);
        }
                          
        add(StyleUtils.clearDiv());        
    }

    /**
     * TODO: Vzhladom na redesign ak to bude?
     */
    /** pokud ma uzivatel opraveni admina submissionu, bude se mu zobrazovat vizitka */
    private void checkPermission(final Label nameLabel, final String uuid) {
        EventBus.get().fireEvent(new EventDispatchEvent(
                new CurrentEventUserRightAction(EventConferenceModuleRoleModel.RIGHT_SUBMISSION_SETTINGS),
                new AsyncCallback<BooleanResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // neco se pokazilo, nebude se zobrazovat vizitka
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        if (result.isBool()) {
                            nameLabel.addStyleName(StyleUtils.HAND);
                            nameLabel.addClickHandler(new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    UserCard card = new UserCard(uuid);
                                    card.setPopupPosition(nameLabel.getAbsoluteLeft(), nameLabel.getAbsoluteTop() + 15);
                                    card.show();
                                }
                            });
                        }
                    }
                }, null));
    }
}
