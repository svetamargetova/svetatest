package consys.event.conference.gwt.client.module.chair;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.action.ActionImageBig;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.common.gwt.client.ui.comp.panel.InfoPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.conference.gwt.client.action.chair.ContributionStateAction;
import consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum;
import consys.event.conference.gwt.client.module.chair.item.ContributionArtefactsChairReviewPanel;
import consys.event.conference.gwt.client.comp.ReviewScoreAndComments;
import consys.event.conference.gwt.client.module.chair.item.ContributionChairHeadPanel;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 *
 * @author palo
 */
public class ContributionChairForm extends RootPanel {

    private final String contributionUuid;

    public ContributionChairForm(String title, String contributionUuid) {
        super(title);
        this.contributionUuid = contributionUuid;
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        // nacitani ze serveru
        EventBus.fire(new EventDispatchEvent(new LoadContributionChairFormAction(contributionUuid),
                new AsyncCallback<ClientContributionChairFormData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava EventActionExecutor
                    }

                    @Override
                    public void onSuccess(ClientContributionChairFormData result) {
                        clear();
                        generateContent(result);
                    }
                }, this));
    }

    private void generateContent(ClientContributionChairFormData data) {
        // hlavicka
        addWidget(new ContributionChairHeadPanel(data.getHead()));

        // recenzie
        if (data.getEvaluated() != null && !data.getEvaluated().isEmpty()) {
            addWidget(new ReviewScoreAndComments(data.getEvaluated(), data.getHead()));
        } else {
            InfoPanel infoPanel = new InfoPanel(ECMessageUtils.c.contributionChairForm_text_noReviewers(),
                    ECMessageUtils.c.contributionChairForm_text_assignReviewers(),
                    assignReviewersAction(data.getHead().getUuid(), data.getHead().getTitle()));
            infoPanel.addStyleName(ECResourceUtils.css().infoPanel());
            addWidget(infoPanel);
        }

        // moznost rozkliknut detaile hodnotena
        addWidget(new ContributionArtefactsChairReviewPanel(data.getHead()));


        // rozhodnuti
        addWidget(decision(data.getHead().getUuid()));
    }

    private ConsysAction assignReviewersAction(final String uuid, final String title) {
        return new ConsysAction() {

            @Override
            public void run() {
                AssignmentReviewersForm w = new AssignmentReviewersForm(uuid);
                FormUtils.fireNextBreadcrumb(title, w);
            }
        };
    }

    private FlowPanel decision(String contributionUuid) {
        ActionImageBig accept = ActionImageBig.green(UIMessageUtils.c.const_accept());
        accept.setClickHandler(decisionClickHandler(contributionUuid, true));

        ActionImageBig decline = ActionImageBig.red(UIMessageUtils.c.const_decline());
        decline.setClickHandler(decisionClickHandler(contributionUuid, false));

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ECResourceUtils.css().contributionChairFormDecision());
        panel.add(new HeadlinePanel(ECMessageUtils.c.contributionChairForm_text_evaluateThisReview()));
        panel.add(accept);
        panel.add(decline);
        panel.add(StyleUtils.clearDiv());
        return panel;
    }

    private ClickHandler decisionClickHandler(final String uuid, final boolean accepted) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EventBus.fire(new EventDispatchEvent(
                        new ContributionStateAction(uuid, accepted ? ClientContributionStateEnum.ACCEPTED : ClientContributionStateEnum.DECLINED),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExecutor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, ContributionChairForm.this));
            }
        };
    }
}
