package consys.event.conference.gwt.client.bo.contribution;

import consys.common.gwt.shared.bo.CommonThumb;
import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Objekt prispevku
 * @author pepa
 */
public class ClientContribution implements IsSerializable, Result {

    // konstanty
    private static final long serialVersionUID = 879735881745401251L;
    // data
    private String uuid;
    private String title;
    private String subtitle;
    private ArrayList<CommonThumb> submitters;
    private String authors;
    private ArrayList<ClientTopic> topics;

    public ClientContribution() {
        submitters = new ArrayList<CommonThumb>();
        topics = new ArrayList<ClientTopic>();
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public ArrayList<CommonThumb> getSubmitters() {
        return submitters;
    }

    public void setSubmitters(ArrayList<CommonThumb> submitters) {
        this.submitters = submitters;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ClientTopic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<ClientTopic> topics) {
        this.topics = topics;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
