package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.OrderPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.comp.panel.element.OrderPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.OrderPanelItemUI;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction;
import consys.event.conference.gwt.client.bo.ClientEditReviewCycle;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 * Editacni formular pro nastaveni ReviewCycle
 * @author pepa
 */
public class EditFormReviewCycle extends RootPanel {

    // konstanty
    private static final String WIDTH = "320px";
    // komponenty
    private SimpleFormWithHelpPanel mainPanel;
    private BaseForm form;
    private ConsysDateBox submissionFromBox;
    private ConsysDateBox submissionToBox;
    private ConsysDateBox acceptanceFromBox;
    private ConsysDateBox acceptanceToBox;
    private SelectBox<CommonThumb> selectBox;
    private SelectBoxItem<CommonThumb> noItem;
    private OrderPanel<CommonThumbOrderItem> orderPanel;
    private FlowPanel artefactPanelWrapper;
    // data
    private ClientEditReviewCycle rc;
    private ReviewCycleItem source;
    private ArrayList<CommonThumb> artefactThumbs;
    private ArrayList<CommonThumb> selectedArtefactThumbs;
    private Label noArtefact;

    public EditFormReviewCycle(Long id, final ReviewCycleItem source, ArrayList<CommonThumb> artefactThumbs) {
        super(ECMessageUtils.c.editFormReviewCycle_title());

        mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        this.source = source;
        this.artefactThumbs = artefactThumbs;

        selectedArtefactThumbs = new ArrayList<CommonThumb>();
        noItem = new SelectBoxItem<CommonThumb>(new CommonThumb("", ""), ECMessageUtils.c.editFormReviewCycle_text_noItem());

        noArtefact = StyleUtils.getStyledLabel(ECMessageUtils.c.editFormReviewCycle_text_noArtefactAdded(), MARGIN_TOP_3);

        // dotazeni aktualnich dat
        EventDispatchEvent loadEvent = new EventDispatchEvent(new LoadEditReviewCycleAction(id),
                new AsyncCallback<ClientEditReviewCycle>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava ActionExecutor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientEditReviewCycle result) {
                        rc = result;
                        source.setReviewCycle(result);
                        init();
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);

        //mainPanel.addHelpTitle("Help title");
        //mainPanel.addHelpText("Help text");
    }

    private void init() {
        submissionFromBox = ConsysDateBox.getEventHalfBox(true, rc.getSubmitFrom());
        submissionToBox = ConsysDateBox.getEventHalfBox(true, rc.getSubmitTo());
        acceptanceFromBox = ConsysDateBox.getEventHalfBox(true, rc.getReviewFrom());
        acceptanceToBox = ConsysDateBox.getEventHalfBox(true, rc.getReviewTo());

        submissionFromBox.addValueChangeHandler(DateTimeUtils.dateValueChangeHandler(submissionFromBox, submissionToBox));
        acceptanceFromBox.addValueChangeHandler(DateTimeUtils.dateValueChangeHandler(acceptanceFromBox, acceptanceToBox));

        Widget submissionPanel = datePanel(submissionFromBox, submissionToBox);
        Widget acceptancePanel = datePanel(acceptanceFromBox, acceptanceToBox);

        orderPanel = new OrderPanel<CommonThumbOrderItem>();
        orderPanel.addStyleName(MARGIN_TOP_3);

        for (String uuid : rc.getAssignedArtefacts()) {
            LoggerFactory.log(getClass(), "-> " + uuid);
            for (CommonThumb t : artefactThumbs) {
                if (uuid.equalsIgnoreCase(t.getUuid())) {
                    LoggerFactory.log(getClass(), "   " + t.getUuid() + " " + t.getName());
                    // pridani vybranych polozek do panelu
                    orderPanel.addItemUI(new ArtefactThumbItem(new CommonThumbOrderItem(t)));
                    selectedArtefactThumbs.add(t);
                }
            }
        }

        selectBox = new SelectBox<CommonThumb>();
        selectBox.addStyleName(FLOAT_LEFT);
        selectBox.addStyleName(MARGIN_RIGHT_10);
        selectBox.setWidth(250);

        refreshSelectBox();

        ActionLabel add = new ActionLabel(UIMessageUtils.c.const_add(), FLOAT_LEFT + " " + MARGIN_TOP_3);
        add.setClickHandler(addClickHandler());

        FlowPanel artefactPanelControl = new FlowPanel();
        artefactPanelControl.addStyleName(MARGIN_TOP_10);
        artefactPanelControl.add(selectBox);
        artefactPanelControl.add(add);
        artefactPanelControl.add(StyleUtils.clearDiv());

        artefactPanelWrapper = new FlowPanel();
        artefactPanelWrapper.add(orderPanel);
        artefactPanelWrapper.add(artefactPanelControl);

        if (orderPanel.getItemUICount() == 0) {
            artefactPanelWrapper.insert(noArtefact, 0);
        }

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        form = new BaseForm("200px");
        form.setStyleName(StyleUtils.MARGIN_TOP_10);
        form.addRequired(0, ECMessageUtils.c.editFormReviewCycle_form_submissionPeriod(), submissionPanel);
        form.addOptional(1, ECMessageUtils.c.reviewCycle_form_reviewPeriod(), acceptancePanel);
        form.addOptional(2, ECMessageUtils.c.const_artefacts(), artefactPanelWrapper);
        form.addActionMembers(3, button, ActionLabel.breadcrumbBackCancel(), WIDTH);
        mainPanel.addWidget(form);
    }

    /** vytvori panel se vstupem od a vstupem do */
    public static Widget datePanel(ConsysDateBox from, ConsysDateBox to) {
        HorizontalPanel p = new HorizontalPanel();
        p.setWidth("100%");
        p.setVerticalAlignment(HasAlignment.ALIGN_MIDDLE);
        p.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
        p.add(from);
        p.setCellHorizontalAlignment(from, HasAlignment.ALIGN_LEFT);
        p.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_toLower(), StyleUtils.MARGIN_HOR_10));
        p.add(to);
        p.setCellHorizontalAlignment(to, HasAlignment.ALIGN_RIGHT);

        return p;
    }

    /** click handler pro pridani artefaktu */
    private ClickHandler addClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (orderPanel.getItemUICount() == 0 && artefactPanelWrapper.getWidget(0) instanceof Label) {
                    artefactPanelWrapper.getWidget(0).removeFromParent();
                }
                CommonThumb value = selectBox.getSelectedItem().getItem();
                if (value != null && !value.getUuid().isEmpty()) {
                    orderPanel.addItemUI(new ArtefactThumbItem(new CommonThumbOrderItem(value)));
                    selectedArtefactThumbs.add(value);
                }
                refreshSelectBox();
            }
        };
    }

    /** aktualizuje polozky v select boxu */
    private void refreshSelectBox() {
        ArrayList<SelectBoxItem<CommonThumb>> sbItems = new ArrayList<SelectBoxItem<CommonThumb>>();

        for (CommonThumb t : artefactThumbs) {
            if (!selectedArtefactThumbs.contains(t) && rc.getNotAssignedArtefacts().contains(t.getUuid())) {
                // pridani nevybranych polozek do selectboxu
                sbItems.add(new SelectBoxItem<CommonThumb>(t, t.getName()));
            }
        }
        selectBox.selectNone();
        if (sbItems.isEmpty()) {
            sbItems.add(noItem);
            selectBox.setItems(sbItems);
            selectBox.setSelectedItem(noItem);
        } else {
            selectBox.setItems(sbItems);
        }
    }

    /** potvrzovaci clickhandler */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                final ClientSettingsReviewCycle data = new ClientSettingsReviewCycle();
                data.setId(rc.getId());
                data.setSubmitFrom(submissionFromBox.getValue());
                data.setSubmitTo(submissionToBox.getValue());
                data.setReviewFrom(acceptanceFromBox.getValue());
                data.setReviewTo(acceptanceToBox.getValue());

                if (!validateData(data)) {
                    return;
                }

                for (int i = 0; i < orderPanel.getItemUICount(); i++) {
                    Widget w = orderPanel.getItemUI(i);
                    if (w instanceof ArtefactThumbItem) {
                        ArtefactThumbItem item = (ArtefactThumbItem) w;
                        data.getAssignedArtefacts().add(item.getArtefactUuid());
                    }
                }

                EventDispatchEvent updateEvent = new EventDispatchEvent(new UpdateReviewCycleAction(data),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby obstarava ActionExecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // aktualizace dat
                                rc.setSubmitFrom(data.getSubmitFrom());
                                rc.setSubmitTo(data.getSubmitTo());
                                rc.setReviewFrom(data.getReviewFrom());
                                rc.setReviewTo(data.getReviewTo());
                                rc.setAssignedArtefacts(data.getAssignedArtefacts());
                                // aktualizace zobrazovaciho formulare
                                source.setReviewCycle(rc);
                                // navrat na predchozi formular
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, mainPanel);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }

    /** kontrola dat aktualizacni akce */
    private boolean validateData(ClientSettingsReviewCycle data) {
        if (data.getSubmitFrom() == null || data.getSubmitTo() == null) {
            getFailMessage().setText(ECMessageUtils.c.editFormReviewCycle_error_submissionPeriodDatesMustBeSet());
            return false;
        }
        if (data.getSubmitFrom().after(data.getSubmitTo())) {
            getFailMessage().setText(ECMessageUtils.c.editFormReviewCycle_error_wrongSubmissionPeriod());
            return false;
        }
        if ((data.getReviewFrom() == null && data.getReviewTo() != null)
                || (data.getReviewFrom() != null && data.getReviewTo() == null)) {
            getFailMessage().setText(ECMessageUtils.c.editFormReviewCycle_error_whenFillOneReviewDate());
            return false;
        }
        if (data.getReviewFrom() != null && data.getReviewTo() != null) {
            if (data.getReviewFrom().after(data.getReviewTo())) {
                getFailMessage().setText(ECMessageUtils.c.editFormReviewCycle_error_wrongReviewPeriod());
                return false;
            }
            if (data.getReviewFrom().before(data.getSubmitFrom())) {
                getFailMessage().setText(ECMessageUtils.c.editFormReviewCycle_error_reviewStartsBeforeSubmit());
                return false;
            }
            if (data.getReviewTo().before(data.getSubmitTo())) {
                getFailMessage().setText(ECMessageUtils.c.editFormReviewCycle_error_reviewEndsBeforeSubmit());
                return false;
            }
        }
        return true;
    }

    private class CommonThumbOrderItem extends OrderPanelItem {

        // data
        private CommonThumb item;

        public CommonThumbOrderItem(CommonThumb item) {
            super(item.getName());
            this.item = item;
        }

        public CommonThumb getItem() {
            return item;
        }
    }

    /** jeden vybrany artefakt */
    private class ArtefactThumbItem extends OrderPanelItemUI<CommonThumbOrderItem> {

        // komponenty
        private Label label;
        // data
        private CommonThumbOrderItem t;

        public ArtefactThumbItem(final CommonThumbOrderItem t) {
            super(t, orderPanel);
            this.t = t;
        }

        @Override
        protected Widget generateContent() {
            Image remove = createRemove();

            label = StyleUtils.getStyledLabel(getObject().getName(), FLOAT_LEFT);
            label.setWidth("230px");

            FlowPanel p = new FlowPanel();
            p.addStyleName(FLOAT_LEFT);
            p.add(label);
            p.add(remove);
            p.add(StyleUtils.clearDiv());
            return p;
        }

        private Image createRemove() {
            Image i = new Image(ResourceUtils.system().removeCross());
            i.addStyleName(HAND);
            i.addStyleName(FLOAT_RIGHT);
            i.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    orderPanel.removeItemUI(ArtefactThumbItem.this);
                    selectedArtefactThumbs.remove(t.getItem());
                    rc.getNotAssignedArtefacts().add(t.getItem().getUuid());
                    if (orderPanel.getItemUICount() == 0) {
                        artefactPanelWrapper.insert(noArtefact, 0);
                    }
                    refreshSelectBox();
                }
            });
            return i;
        }

        @Override
        public Widget getHandler() {
            return label;
        }

        public String getArtefactUuid() {
            return t.getItem().getUuid();
        }

        public String getName() {
            return t.getItem().getName();
        }
    }
}
