package consys.event.conference.gwt.client.bo.evaluation;

import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class Evaluation<PARTIAL extends EvaluationValue, OVERALL extends EvaluationValue> implements IsSerializable {

    // data
    private OVERALL overallScore;
    private ArrayList<PARTIAL> partialScores;

    public Evaluation() {
        overallScore = (OVERALL) new EvaluationValue();
        partialScores = new ArrayList<PARTIAL>();
    }

    public OVERALL getOverallScore() {
        return overallScore;
    }

    public void setOverallScore(OVERALL overallScore) {
        this.overallScore = overallScore;
    }

    public ArrayList<PARTIAL> getPartialScores() {
        return partialScores;
    }
}
