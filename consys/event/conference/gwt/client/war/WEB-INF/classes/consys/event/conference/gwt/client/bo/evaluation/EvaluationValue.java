package consys.event.conference.gwt.client.bo.evaluation;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Konrektne hodnotenie.
 * 
 * @author palo
 */
public class EvaluationValue implements IsSerializable {

    public static final int NOT_ENTERED = -1;
    public static final int NA = -2;
    public static final int AUTOACCEPTED = -3;
    /** Ak zadadane potom sa jedna o UUID recenzie */
    private String uuid;
    private int value;

    public EvaluationValue() {
        this(NOT_ENTERED);
    }

    public EvaluationValue(int value) {
        this.value = value;
    }

    public EvaluationValue(String uuid, int value) {
        this.uuid = uuid;
        this.value = value;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isEntered() {
        return !isNotEntered() && !isNA() && !isAutoaccepted();
    }

    private boolean isNotEntered() {
        return value == NOT_ENTERED;
    }

    public boolean isNA() {
        return value == NA;
    }

    public boolean isAutoaccepted() {
        return value == AUTOACCEPTED;
    }
}
