package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionType;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.client.action.exception.DeleteRelationException;
import consys.event.conference.gwt.client.action.setting.DeleteContributionTypeAction;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Podcast SubmissionTypeContent, tvori jednu zalozku
 * @author pepa
 */
public class ContributionTypeItem extends VerticalPanel implements ConsysTabPanelItem, ActionExecutionDelegate {

    // konstanty
    public static final int WIDTH = LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 2 - 20; // -ramecek -pravy a levy margin
    // komponenty
    private ConsysTabPanelLabel tabLabel;
    private Label descriptionText;
    private HTML internalHTML;
    private FlowPanel failPanel;
    private Waiting waiting;
    // data
    private ConsysTabPanel tabPanel;
    private ClientContributionType cst;
    private ContributionTypeItem self;
    private ArtefactList artefactList;
    private ReviewCycleList reviewCycleList;

    public ContributionTypeItem(ConsysTabPanelLabel tabLabel, ClientTabItem item, final ConsysMessage helpMessage) {
        super();
        this.tabLabel = tabLabel;
        self = this;
        artefactList = new ArtefactList(item.getUuid());
        reviewCycleList = new ReviewCycleList(item.getUuid(), artefactList);

        waiting = new Waiting(this);
        add(waiting);

        cst = new ClientContributionType();
        cst.setUuid(item.getUuid());
        cst.setName(item.getTitle());
        artefactList.setClientArtefactTypeList(cst.getClientArtefactTypeList());
        reviewCycleList.setClientReviewCycleList(cst.getClientReviewCycleList());

        setWidth(WIDTH + "px");
        setStyleName(StyleUtils.MARGIN_VER_10);
        addStyleName(StyleUtils.MARGIN_HOR_10);

        ConsysAction editAction = new ConsysAction() {

            @Override
            public void run() {
                EditFormContributionType w = new EditFormContributionType(getSubmissionType(), ContributionTypeItem.this);
                String title = ECMessageUtils.c.editFormContributionType_title() + " " + cst.getName();
                FormUtils.fireNextBreadcrumb(title, w);
            }
        };

        ConsysAction removeAction = new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent removeEvent = new EventDispatchEvent(new DeleteContributionTypeAction(cst.getUuid()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby obstarava ActionExecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                } else if (caught instanceof DeleteRelationException) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionDeleteRelation());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // odstraneni v ui
                                tabPanel.removeTab(self);
                                if (tabPanel.getTabCount() == 0) {
                                    tabPanel.setVisible(false);
                                    helpMessage.setVisible(true);
                                }
                            }
                        }, ContributionTypeItem.this);
                EventBus.get().fireEvent(removeEvent);
            }
        };

        EditWithRemover ewr = new EditWithRemover(editAction, removeAction);
        ewr.addStyleName(StyleUtils.FLOAT_RIGHT);

        failPanel = new FlowPanel();
        failPanel.addStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel showControlPanel = new FlowPanel();
        showControlPanel.add(ewr);
        showControlPanel.add(failPanel);
        showControlPanel.add(StyleUtils.clearDiv());
        add(showControlPanel);

        Label descriptionLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.const_description(), StyleUtils.FONT_16PX);
        descriptionLabel.addStyleName(StyleUtils.FONT_BOLD);
        descriptionLabel.addStyleName(StyleUtils.MARGIN_BOT_10);
        add(descriptionLabel);

        descriptionText = StyleUtils.getStyledLabel(FormUtils.MDASH, StyleUtils.MARGIN_BOT_10);
        add(descriptionText);

        internalHTML = new HTML();
        internalHTML.addStyleName(StyleUtils.MARGIN_BOT_10);
        add(internalHTML);

        add(new Separator());
        add(artefactList);
        add(Separator.addedStyle(StyleUtils.MARGIN_TOP_10));
        add(reviewCycleList);
    }

    @Override
    public Widget getTitleNote() {
        return null;
    }

    @Override
    public Widget getContent() {
        return this;
    }

    @Override
    public void setConsysTabPanel(ConsysTabPanel tabPanel) {
        this.tabPanel = tabPanel;
    }

    private String internalText() {
        return "<b>" + ECMessageUtils.c.editFormContributionType_form_internal() + ":</b> " + (cst.isInternal() ? UIMessageUtils.c.const_yes() : UIMessageUtils.c.const_no());
    }

    /** vraci ClientSubmissionType */
    public ClientContributionType getSubmissionType() {
        cst.setClientArtefactTypeList(artefactList.getClientArtefactTypeList());
        cst.setClientReviewCycleList(reviewCycleList.getClientReviewCycleList());
        return cst;
    }

    /** nastavuje ClientSubmissionType */
    public void setSubmissionType(ClientContributionType cst) {
        this.cst = cst;
        tabLabel.setText(cst.getName());
        descriptionText.setText(cst.getDescription().isEmpty() ? FormUtils.MDASH : cst.getDescription());
        internalHTML.setHTML(internalText());
        artefactList.setClientArtefactTypeList(cst.getClientArtefactTypeList());
        reviewCycleList.setClientReviewCycleList(cst.getClientReviewCycleList());
    }

    @Override
    public void actionStarted() {
        waiting.show();
    }

    @Override
    public void actionEnds() {
        waiting.hide();
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    /** vraci chybovou zpravu */
    private ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setStyleName(StyleUtils.MARGIN_BOT_10);
        failMessage.setVisible(false);
        failPanel.add(failMessage);
        return failMessage;
    }
}
