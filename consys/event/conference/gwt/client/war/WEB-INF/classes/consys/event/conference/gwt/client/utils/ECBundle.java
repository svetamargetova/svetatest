package consys.event.conference.gwt.client.utils;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource.Import;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;
import consys.common.gwt.client.ui.comp.action.ActionCss;
import consys.common.gwt.client.ui.comp.form.FormCss;
import consys.common.gwt.client.ui.comp.input.InputCss;
import consys.common.gwt.client.ui.comp.panel.PanelCss;
import consys.common.gwt.client.ui.comp.user.UserCss;
import consys.common.gwt.client.ui.comp.wrapper.WrapperCss;
import consys.common.gwt.client.ui.img.CssConstants;
import consys.common.gwt.client.ui.img.SystemCssResource;
import consys.event.conference.gwt.client.utils.css.ECCssResources;

/**
 *
 * @author pepa
 */
public interface ECBundle extends ClientBundle {

    @Source("img/s/s1.png")
    public ImageResource small1();

    @Source("img/s/s2.png")
    public ImageResource small2();

    @Source("img/s/s3.png")
    public ImageResource small3();

    @Source("img/s/s4.png")
    public ImageResource small4();

    @Source("img/s/s5.png")
    public ImageResource small5();

    @Source("img/s/s6.png")
    public ImageResource small6();

    @Source("img/s/s7.png")
    public ImageResource small7();

    @Source("img/s/s8.png")
    public ImageResource small8();

    @Source("img/s/s9.png")
    public ImageResource small9();

    @Source("img/s/s10.png")
    public ImageResource small10();

    @Source("img/s/s11.png")
    public ImageResource small11();

    @Source("img/m/m1.png")
    public ImageResource medium1();

    @Source("img/m/m2.png")
    public ImageResource medium2();

    @Source("img/m/m3.png")
    public ImageResource medium3();

    @Source("img/m/m4.png")
    public ImageResource medium4();

    @Source("img/m/m5.png")
    public ImageResource medium5();

    @Source("img/m/m6.png")
    public ImageResource medium6();

    @Source("img/m/m7.png")
    public ImageResource medium7();

    @Source("img/m/m8.png")
    public ImageResource medium8();

    @Source("img/m/m9.png")
    public ImageResource medium9();

    @Source("img/m/m10.png")
    public ImageResource medium10();

    @Source("img/m/m11.png")
    public ImageResource medium11();

    @Source("img/b/b1.png")
    public ImageResource big1();

    @Source("img/b/b2.png")
    public ImageResource big2();

    @Source("img/b/b3.png")
    public ImageResource big3();

    @Source("img/b/b4.png")
    public ImageResource big4();

    @Source("img/b/b5.png")
    public ImageResource big5();

    @Source("img/b/b6.png")
    public ImageResource big6();

    @Source("img/b/b7.png")
    public ImageResource big7();

    @Source("img/b/b8.png")
    public ImageResource big8();

    @Source("img/b/b9.png")
    public ImageResource big9();

    @Source("img/b/b10.png")
    public ImageResource big10();

    @Source("img/b/b11.png")
    public ImageResource big11();

    @Source("img/mid/mid1.png")
    public ImageResource middle1();

    @Source("img/mid/mid2.png")
    public ImageResource middle2();

    @Source("img/mid/mid3.png")
    public ImageResource middle3();

    @Source("img/mid/mid4.png")
    public ImageResource middle4();

    @Source("img/mid/mid5.png")
    public ImageResource middle5();

    @Source("img/mid/mid6.png")
    public ImageResource middle6();

    @Source("img/mid/mid7.png")
    public ImageResource middle7();

    @Source("img/mid/mid8.png")
    public ImageResource middle8();

    @Source("img/mid/mid9.png")
    public ImageResource middle9();

    @Source("img/mid/mid10.png")
    public ImageResource middle10();

    @Source("img/mid/mid11.png")
    public ImageResource middle11();

    @Source("img/bot/bot1.png")
    public ImageResource bottom1();

    @Source("img/bot/bot2.png")
    public ImageResource bottom2();

    @Source("img/bot/bot3.png")
    public ImageResource bottom3();

    @Source("img/bot/bot4.png")
    public ImageResource bottom4();

    @Source("img/bot/bot5.png")
    public ImageResource bottom5();

    @Source("img/bot/bot6.png")
    public ImageResource bottom6();

    @Source("img/bot/bot7.png")
    public ImageResource bottom7();

    @Source("img/bot/bot8.png")
    public ImageResource bottom8();

    @Source("img/bot/bot9.png")
    public ImageResource bottom9();

    @Source("img/bot/bot10.png")
    public ImageResource bottom10();

    @Source("img/bot/bot11.png")
    public ImageResource bottom11();

    @Source("img/rollDownTitleMiddle.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource evaluatorItemTextMiddle();

    @Source("img/rollDownTitleMiddleOver.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource evaluatorItemTextMiddleOver();

    @Source("img/rollDownTitleBottom.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource evaluatorItemTextBottom();

    @Source("img/rollDownTitleBottomOver.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource evaluatorItemTextBottomOver();

    @Source("img/rollDownTitleMiddleEnd.png")
    public ImageResource evaluatorItemTextMiddleEnd();

    @Source("img/rollDownTitleMiddleEndOver.png")
    public ImageResource evaluatorItemTextMiddleEndOver();

    @Source("img/rollDownTitleBottomEnd.png")
    public ImageResource evaluatorItemTextBottomEnd();

    @Source("img/rollDownTitleBottomEndOver.png")
    public ImageResource evaluatorItemTextBottomEndOver();

    @Source("img/rollDownTitle.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource evaluatorTextTitle();

    @Source("img/rollDownTitleOver.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource evaluatorTextTitleOver();

    @Source("img/rollDownTitleEnd.png")
    public ImageResource evaluatorTextTitleEnd();

    @Source("img/rollDownTitleEndOver.png")
    public ImageResource evaluatorTextTitleEndOver();

    @Source("img/rollDownClosed.png")
    public ImageResource evaluatorIconClosed();

    @Source("img/rollDownOpened.png")
    public ImageResource evaluatorIconOpened();

    @Import(value = {CssConstants.class, FormCss.class, ActionCss.class, InputCss.class, PanelCss.class, SystemCssResource.class, WrapperCss.class, UserCss.class})
    @Source("css/ec.css")
    public ECCssResources css();
}
    