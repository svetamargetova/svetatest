package consys.event.conference.gwt.client.module.chair;

import consys.event.conference.gwt.client.module.chair.item.ChairContributionsListFilter;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.DataListCommonOrderer;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import consys.event.conference.api.list.ContributionsList;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.module.chair.item.ChairContributionsListItem;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Seznam prispevku pro chaira
 * @author pepa
 */
public class ChairContributionsList extends DataListPanel implements ContributionsList {

    public ChairContributionsList() {
        super(ECMessageUtils.c.reviewsForm_title(), TAG, false);
        addStyleName(ECResourceUtils.css().chairContributionsList());

        enableExport(ListExportTypeEnum.EXCEL);
        
        setListDelegate(new ReviewsListDelegate());
        setDefaultFilter(new ListFilter(Filter_TYPE) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
        DataListCommonOrderer orderer = new DataListCommonOrderer();
        orderer.addOrderer(ECMessageUtils.c.const_title(), Order_TITLE, true);
        orderer.addOrderer(ECMessageUtils.c.const_submitDate(), Order_SUBMIT_DATE);
        orderer.addOrderer(ECMessageUtils.c.const_score(), Order_SCORE);
        addHeadPanel(orderer);
        addHeadPanel(new ChairContributionsListFilter(false));
    }

    /** delegat seznamu */
    private class ReviewsListDelegate implements ListDelegate<ClientListContributionItem, ReviewsListItem> {

        @Override
        public ReviewsListItem createCell(ClientListContributionItem item) {
            return new ReviewsListItem(item);
        }
    }

    /** jedna polozka seznamu */
    private class ReviewsListItem extends ListItem<ClientListContributionItem> {

        public ReviewsListItem(ClientListContributionItem item) {
            super(item, true);
        }

        @Override
        protected void createCell(final ClientListContributionItem item, FlowPanel panel) {
            panel.clear();
            panel.setStyleName(ECResourceUtils.css().contributionListItemWrapper());
            panel.add(new ChairContributionsListItem(item));
        }
    }
}
