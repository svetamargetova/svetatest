package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.base.TitlePanel;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.PropertyPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 * Nastaveni property
 * @author pepa
 */
public class ConferencePropertyPanel extends PropertyPanel implements TitlePanel {

    // komponenty
    private HorizontalPanel controllPart;
    private Label titleLabel;

    public ConferencePropertyPanel() {
        titleLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.const_property(), FONT_19PX, FONT_BOLD, MARGIN_RIGHT_10);

        ActionImage backButton = ActionImage.getBackButton(ECMessageUtils.c.contributionTypeContent_title(), true);
        backButton.addStyleName(MARGIN_LEFT_20);
        backButton.addClickHandler(FormUtils.breadcrumbBackClickHandler());

        controllPart = new HorizontalPanel();
        controllPart.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
        controllPart.add(titleLabel);
        controllPart.add(backButton);
    }

    @Override
    protected void onLoad() {
        clear();
        addWidget(controllPart);
        addWidget(Separator.addedStyle(MARGIN_VER_10));

        try {
            String value = (String) Cache.get().getSafe(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS);

            ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
            items.add(new SelectBoxItem<String>("true", UIMessageUtils.c.const_yes()));
            items.add(new SelectBoxItem<String>("false", UIMessageUtils.c.const_no()));

            addProperty(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS,
                    ECMessageUtils.c.conferencePropertyPanel_property_visibleAuthors(), items, value, "");
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS));
        }

        try {
            String value = (String) Cache.get().getSafe(ConferenceProperties.SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER);

            ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
            items.add(new SelectBoxItem<String>("0", UIMessageUtils.c.const_unlimited()));
            for (int i = 1; i <= 40; i++) {
                items.add(new SelectBoxItem<String>(String.valueOf(i)));
            }

            addProperty(ConferenceProperties.SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER,
                    ECMessageUtils.c.conferencePropertyPanel_property_maxReviewsPerReviewer(), items, value, "");
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(ConferenceProperties.SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER));
        }
    }

    @Override
    public String getPanelTitle() {
        return UIMessageUtils.c.const_property();
    }
}
