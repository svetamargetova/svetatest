package consys.event.conference.gwt.client.action.chair;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionStateEnum;

/**
 * Akce pro rozhodnuti jestli je prispevek prijat nebo odmitnut
 * @author pepa
 */
public class ContributionStateAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -3148470915563691423L;
    // data
    private String uuid;
    private ClientContributionStateEnum toState;

    public ContributionStateAction() {
    }

    public ContributionStateAction(String uuid, ClientContributionStateEnum state) {
        this.uuid = uuid;
        this.toState = state;
    }

    public ClientContributionStateEnum getToStateEnum() {
        return toState;
    }

   
    /** uuid prispevku */
    public String getUuid() {
        return uuid;
    }
}
