/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Enumerator ktory definuje sposob zadavanie/reprezentacie artefaktov. Teraz rozlisujeme
 * dve typy: text, file. Do buducna sa moze rozsirit a preto exsituje tento enumerator.
 * @author palo
 */
public enum ClientContributionArtefactDataType implements IsSerializable{
    
    TEXT, FILE;
    
    private static final long serialVersionUID = 879735881745401251L;
}
