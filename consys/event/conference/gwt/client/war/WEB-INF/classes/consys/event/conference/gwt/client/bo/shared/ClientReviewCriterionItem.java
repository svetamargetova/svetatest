package consys.event.conference.gwt.client.bo.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Jedna polozka kriteria
 * @author pepa
 */
public class ClientReviewCriterionItem<T> implements IsSerializable {

    // data
    private String reviewUuid;
    private String name;
    private T value;

    public ClientReviewCriterionItem() {
    }

    public ClientReviewCriterionItem(T value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReviewUuid() {
        return reviewUuid;
    }

    public void setReviewUuid(String reviewUuid) {
        this.reviewUuid = reviewUuid;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
