package consys.event.conference.gwt.client.bo.contribution;

import consys.common.gwt.shared.bo.CommonThumb;
import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.ClientArtefactEvaluationChair;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientContributionEvaluationChair implements IsSerializable, Result {

    private static final long serialVersionUID = -5949691338166965965L;
    // data
    private ClientContributionHeadData head;
    private ArrayList<CommonThumb> reviewers;
    private ArrayList<ClientArtefactEvaluationChair> evaluations;

    public ClientContributionEvaluationChair() {
        evaluations = new ArrayList<ClientArtefactEvaluationChair>();
        reviewers = new ArrayList<CommonThumb>();
    }

    public ArrayList<ClientArtefactEvaluationChair> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(ArrayList<ClientArtefactEvaluationChair> evaluations) {
        this.evaluations = evaluations;
    }

    /**
     * @return the head
     */
    public ClientContributionHeadData getHead() {
        return head;
    }

    /**
     * @param head the head to set
     */
    public void setContributionHead(ClientContributionHeadData head) {
        this.head = head;
    }

    /**
     * @return the reviewers
     */
    public ArrayList<CommonThumb> getReviewers() {
        return reviewers;
    }

    /**
     * @param reviewers the reviewers to set
     */
    public void setReviewers(ArrayList<CommonThumb> reviewers) {
        this.reviewers = reviewers;
    }
}
