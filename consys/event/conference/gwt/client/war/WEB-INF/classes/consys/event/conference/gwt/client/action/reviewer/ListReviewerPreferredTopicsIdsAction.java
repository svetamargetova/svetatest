package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Nacte id topiku, ktere si recenzent vybral
 * @author pepa
 */
public class ListReviewerPreferredTopicsIdsAction extends EventAction<ArrayListResult<Long>> {

    private static final long serialVersionUID = 8541111036934094159L;
}
