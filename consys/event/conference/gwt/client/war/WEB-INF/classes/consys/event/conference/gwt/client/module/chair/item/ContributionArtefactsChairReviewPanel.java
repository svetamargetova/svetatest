package consys.event.conference.gwt.client.module.chair.item;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.common.gwt.client.ui.comp.panel.InfoPanel;
import consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.client.module.contribution.item.ContributionFormArtefactsPanelBuilder;
import consys.event.conference.gwt.client.module.contribution.item.ContributionFormArtefactsPanelBuilderUtils;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.List;

/**
 * Komponenta pro zobrazeni recenzencnich cyklu
 *
 * @author pepa
 */
public class ContributionArtefactsChairReviewPanel extends ContributionFormArtefactsPanelBuilder {

    // data    
    private boolean detailVisible = false;
    private boolean isSetAtLeastOneArtefact = true;

    public ContributionArtefactsChairReviewPanel(ClientContributionHeadData head) {
        super(head);
    }

    @Override
    protected HeadlinePanel createHeadlinePanel() {
        if (detailVisible) {
            return new HeadlinePanel(ECMessageUtils.c.reviewCycles_title());
        } else {
            return new HeadlinePanel(ECMessageUtils.c.reviewCycles_title_view(), showAction());
        }
    }

    @Override
    protected Panel createNoCyclesPanel() {
        return null;
    }

    private ConsysAction showAction() {
        return new ConsysAction() {

            @Override
            public void run() {
                EventBus.fire(new EventDispatchEvent(
                        new ListContributionChairReviewCyclesAction(getContributionUuid()),
                        new AsyncCallback<ArrayListResult<ClientReviewCycle>>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExecutor
                            }

                            @Override
                            public void onSuccess(ArrayListResult<ClientReviewCycle> result) {
                                showReviewCycles(result.getArrayListResult());
                            }
                        }, ContributionArtefactsChairReviewPanel.this));
            }
        };
    }

    @Override
    protected Widget createUnderHeadlinePanelContent() {
        if (isSetAtLeastOneArtefact) {
            return null;
        } else {
            InfoPanel infoPanel = new InfoPanel(ECMessageUtils.c.contributionArtefactsChairReviewPanel_text_noArtefacts());
            infoPanel.addStyleName(ECResourceUtils.css().infoPanel());
            return infoPanel;
        }
    }

    private void showReviewCycles(List<ClientReviewCycle> cycles) {
        detailVisible = true;

        isSetAtLeastOneArtefact = false;
        if (cycles != null && !cycles.isEmpty()) {
            // overime jestli je aspon jeden artefakt nastaven
            for (ClientReviewCycle c : cycles) {
                for (ClientArtefactType at : c.getArtefacts()) {
                    if (at instanceof ClientReviewArtefact) {
                        isSetAtLeastOneArtefact = true;
                        break;
                    }
                }
                if (isSetAtLeastOneArtefact) {
                    break;
                }
            }
        }

        setReviewCycles(cycles);
    }

    /** label popisujici automaticke prijeti artefaktu */
    public static Label autoAcceptedArtefact(String artefactUuid) {
        return new Label(artefactUuid == null
                ? ECMessageUtils.c.reviewCycles_text_willAutoAccept()
                : ECMessageUtils.c.reviewCycles_text_wasAutoAccept());
    }

    @Override
    public boolean canEditArtefact(boolean nowInReviewCycleTerm) {
        return true;
    }

    @Override
    public void addArtefact(ClientArtefactType artefactType, FormV2 form, FlowPanel panel, boolean editable) {
        // prida hodnoceni artafaktu
        if (artefactType.isAutoAccepted()) {
            form.addValueWidget(autoAcceptedArtefact(artefactType.getArtefactTypeUuid()));
        } else {
            // ak nebol prispevok stale nahraty tak nemame k tomu nic?
            if (artefactType instanceof ClientReviewArtefactChair) {
                ContributionFormArtefactsPanelBuilderUtils.addArtefactOverallReview((ClientReviewArtefactChair) artefactType,
                        form, getContributionUuid(), getContributionTitle(), true);
            }
        }
    }

    @Override
    public boolean useUpdateArtefactUrl() {
        return true;
    }
}
