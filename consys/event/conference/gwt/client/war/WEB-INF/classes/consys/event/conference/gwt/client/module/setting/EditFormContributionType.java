package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction;
import consys.event.conference.gwt.client.action.exception.SubmissionTypeUniqueException;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Editacni formular pro nastaveni ContributionType
 * @author pepa
 */
public class EditFormContributionType extends RootPanel {

    // konstanty
    private static final String WIDTH = "320px";
    // komponenty
    private SimpleFormWithHelpPanel mainPanel;
    private BaseForm form;
    private ConsysStringTextBox nameBox;
    private ConsysTextArea descBox;
    private ConsysCheckBox internalBox;
    // data
    private ClientContributionType cst;
    private ContributionTypeItem source;

    public EditFormContributionType(ClientContributionType cst, ContributionTypeItem source) {
        super(ECMessageUtils.c.editFormContributionType_title() + " " + cst.getName());

        mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        this.cst = cst;
        this.source = source;
        init();

        //addHelpTitle("Help title");
        //addHelpText("Help text");
    }

    private void init() {
        form = new BaseForm("200px");
        form.setStyleName(StyleUtils.MARGIN_TOP_10);

        nameBox = new ConsysStringTextBox(1, 255, UIMessageUtils.c.const_name());
        nameBox.setText(cst.getName());
        nameBox.setWidth(WIDTH);

        descBox = new ConsysTextArea(0, 255, UIMessageUtils.c.const_description());
        descBox.setText(cst.getDescription());
        descBox.setWidth(WIDTH);

        internalBox = new ConsysCheckBox();
        internalBox.setValue(cst.isInternal());

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        form.addRequired(0, UIMessageUtils.c.const_name(), nameBox);
        form.addRequired(1, UIMessageUtils.c.const_description(), descBox);
        form.addOptional(2, ECMessageUtils.c.editFormContributionType_form_internal(), internalBox);
        form.addActionMembers(3, button, ActionLabel.breadcrumbBackCancel(), WIDTH);
        mainPanel.addWidget(form);
    }

    /** potvrzovaci clickhandler */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // validace
                if (!form.validate(getFailMessage())) {
                    return;
                }

                final ClientContributionTypeThumb thumb = new ClientContributionTypeThumb();
                thumb.setUuid(cst.getUuid());
                thumb.setName(nameBox.getText().trim());
                thumb.setDescription(descBox.getText().trim());
                thumb.setInternal(internalBox.getValue());

                // odeslani na server
                EventDispatchEvent updateEvent = new EventDispatchEvent(new UpdateContributionTypeAction(thumb),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby resi ActionExecutor
                                if (caught instanceof SubmissionTypeUniqueException) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionSubmissionTypeUnique());
                                } else if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // aktualizace dat
                                cst.setName(thumb.getName());
                                cst.setDescription(thumb.getDescription());
                                cst.setInternal(thumb.isInternal());
                                // aktualizace zobrazovaciho formulare
                                source.setSubmissionType(cst);
                                // navrat na predchozi formular
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, mainPanel);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }
}
