package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani typu artefaktu
 * @author pepa
 */
public class DeleteArtefactTypeAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = 631350422073440455L;
    // data
    private String uuid;

    public DeleteArtefactTypeAction() {
    }

    public DeleteArtefactTypeAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
