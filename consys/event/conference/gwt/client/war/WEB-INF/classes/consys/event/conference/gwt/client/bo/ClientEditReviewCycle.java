package consys.event.conference.gwt.client.bo;

import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import java.util.ArrayList;

/**
 *
 * @author palo
 */
public class ClientEditReviewCycle extends ClientSettingsReviewCycle {

    private static final long serialVersionUID = -4671845962078644998L;
    // data
    private ArrayList<String> notAssignedArtefacts;

    public ClientEditReviewCycle() {
        notAssignedArtefacts = new ArrayList<String>();
    }

    public ArrayList<String> getNotAssignedArtefacts() {
        return notAssignedArtefacts;
    }

    public void setNotAssignedArtefacts(ArrayList<String> notAssignedArtefacts) {
        this.notAssignedArtefacts = notAssignedArtefacts;
    }
}
