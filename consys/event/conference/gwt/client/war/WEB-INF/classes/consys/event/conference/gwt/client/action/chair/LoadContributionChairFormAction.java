/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.client.action.chair;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData;

/**
 *
 * @author palo
 */
public class LoadContributionChairFormAction extends EventAction<ClientContributionChairFormData>  {
    
    private String contributionUuid;

    public LoadContributionChairFormAction() {
    }

    public LoadContributionChairFormAction(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }        
    
}
