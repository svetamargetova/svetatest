package consys.event.conference.gwt.client.module.reviewer.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.input.ConsysTextArea;
import consys.common.gwt.client.ui.comp.panel.InfoPanel;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem;
import consys.event.conference.gwt.client.comp.CriteriaEvaluatorWrapper;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.event.ArtefactEvaluationChanged;
import consys.event.conference.gwt.client.utils.CalculationUtils;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Komponenta pro zadani a editaci hodnoceni recenzenta
 * @author pepa
 */
public class ReviewerEvaluation extends Composite implements CriteriaEvaluatorWrapper.CriteriaEvaluatorWrapperListener, ActionExecutionDelegate {

    // komponenty
    private SimplePanel panel;
    private FormV2 form;
    private EvaluationItem evaluation;
    private CriteriaEvaluatorWrapper criterias;
    private ConsysTextArea publicComment;
    private ConsysTextArea privateComment;
    private Waiting waiting;
    private FlowPanel errorPanel;
    // data
    private ClientReviewReviewerData data;
    private boolean editable;
    private String artefactUuid;
    private String contributionUuid;
    private String reviewUuid;
    private ConsysActionWithValue<EvaluationItemValue> callback;
    private boolean onlyPublicComment;

    public ReviewerEvaluation() {
        panel = new SimplePanel();
        waiting = new Waiting(this);
        errorPanel = new FlowPanel();

        FlowPanel p = new FlowPanel();
        p.add(errorPanel);
        p.add(panel);
        p.add(waiting);
        initWidget(p);
        setVisible(false);
    }

    /** nastavi callback po uspesnem odeslani hodnoceni na server */
    public void setCallback(ConsysActionWithValue<EvaluationItemValue> callback) {
        this.callback = callback;
    }

    /** po nasetovani dat vygeneruje komponenta svuj obsah */
    public void setData(ClientReviewReviewerData data, String artefactUuid, String contributionUuid, String reviewUuid) {
        if (data == null) {
            newForm();
            return;
        }

        this.data = data;
        this.artefactUuid = artefactUuid;
        this.contributionUuid = contributionUuid;
        this.reviewUuid = reviewUuid;
        generate();
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public void setOnlyPublicComment(boolean onlyPublicComment) {
        this.onlyPublicComment = onlyPublicComment;
    }

    /** obrazi text, ze nebyly definovany kriteria hodnoceni, at se obrati na administratora akce */
    public void showNoCriterias() {
        newForm();
        form.addValueWidget(new InfoPanel(ECMessageUtils.c.reviewerEvaluation_text_noEvaluationCriterias()));
    }

    private void generate() {
        newForm();
        beginLabel();

        evaluation = EvaluationItem.medium(overall());

        publicComment = comment(data.getPublicComment(), onlyPublicComment
                ? ECMessageUtils.c.const_comment()
                : ECMessageUtils.c.const_publicComment());
        FlowPanel wrapperPublic = commentWrapper(publicComment,
                onlyPublicComment ? ECMessageUtils.c.const_comment() : ECMessageUtils.c.const_publicComment(),
                onlyPublicComment ? ECMessageUtils.c.const_commentHelpText() : ECMessageUtils.c.const_publicCommentHelpText());

        FlowPanel wrapperPrivate = null;
        if (!onlyPublicComment) {
            privateComment = comment(data.getPrivateComment(), ECMessageUtils.c.const_privateComment());
            wrapperPrivate = commentWrapper(privateComment,
                    ECMessageUtils.c.const_privateComment(),
                    ECMessageUtils.c.const_privateCommentHelpText());
        }

        FlowPanel confirmWrapper = new FlowPanel();
        confirmWrapper.add(sendButton());
        confirmWrapper.add(StyleUtils.clearDiv());

        form.add(totalEvaluation(evaluation), criterias(), false);
        form.addValueWidget(wrapperPublic);
        if (wrapperPrivate != null) {
            form.addValueWidget(wrapperPrivate);
        }
        form.addValueWidget(confirmWrapper);
    }

    /** vytvori novy prazdny formu pro vyskladani obsahu */
    private void newForm() {
        form = new FormV2();
        panel.setWidget(form);
    }

    /** vlozi popisek pred na zacatek formulare */
    private void beginLabel() {
        String formBegin = editable
                ? ECMessageUtils.c.myContributionReviews_text_evaluateThisArtefact()
                : ECMessageUtils.c.myContributionReviews_text_evaluationOfThisArtefact();

        form.addValueWidget(new Label(formBegin + ":"));
    }

    /** vytvori hodnotu do celkoveho hodnoceni */
    private EvaluationItemValue overall() {
        EvaluationItemValue overall = new EvaluationItemValue(data.getScore());
        overall.setHelpTitle(ECMessageUtils.c.reviewerEvaluation_helpTitle_totalScore());
        overall.setHelpText(ECMessageUtils.c.reviewerEvaluation_helpText_totalScore());
        return overall;
    }

    /** vytvori odesilaci tlacitko */
    private ActionImage sendButton() {
        ActionImage send = ActionImage.confirm(ECMessageUtils.c.reviewerEvaluation_action_sendEvaluation());
        send.setClickHandler(sendClickHandler());
        send.setVisible(editable);
        return send;
    }

    /** vytvari celkove hodnoceni */
    private FlowPanel totalEvaluation(EvaluationItem evaluation) {
        SimplePanel wrapper = new SimplePanel();
        wrapper.setStyleName(ECResourceUtils.css().reviewerEvaluationTotalWrapper());
        wrapper.setWidget(evaluation);

        FlowPanel p = new FlowPanel();
        p.add(wrapper);
        p.add(StyleUtils.clearDiv());
        return p;
    }

    /** vraci vytvorena kriteria v panelu */
    private SimplePanel criterias() {
        criterias = new CriteriaEvaluatorWrapper();
        criterias.setCriterias(data.getCriterias());
        criterias.setValues(data.getEnteredEvaluations());
        criterias.setListener(this);
        criterias.setEnabled(editable);

        SimplePanel wrapper = new SimplePanel();
        wrapper.setWidget(criterias);
        return wrapper;
    }

    /** vytvari vstup pro komentar */
    private ConsysTextArea comment(String comment, String title) {
        ConsysTextArea area = new ConsysTextArea(0, 0, 515, ConsysTextArea.HEIGHT, false, title);
        area.setText(comment);
        area.setEnabled(editable);
        return area;
    }

    /** vytvari wrapper pro komentar */
    private FlowPanel commentWrapper(ConsysTextArea comment, String title, String help) {
        FlowPanel titlePanel = new FlowPanel();
        titlePanel.setStyleName(ECResourceUtils.css().reviewerEvaluationCommentWrapperTitle());
        titlePanel.add(FormV2.getTitleWidget(title, help, false));
        titlePanel.add(StyleUtils.clearDiv());

        FlowPanel wrapper = new FlowPanel();
        wrapper.setStyleName(ECResourceUtils.css().reviewerEvaluationCommentWrapper());
        wrapper.add(titlePanel);
        wrapper.add(StyleUtils.clearDiv());
        wrapper.add(comment);
        return wrapper;
    }

    public String getPublicComment() {
        return publicComment.getText();
    }

    public String getPrivateComment() {
        return privateComment.getText();
    }

    public ArrayList<Long> getSelectedCriteriaIds() {
        return criterias.getValues();
    }

    @Override
    public void allCriteriaSet() {
        ArrayList<Long> values = getSelectedCriteriaIds();
        ArrayList<ClientReviewCriterion<Long>> ccs = data.getCriterias();

        Set<Integer> denominators = new HashSet<Integer>();
        for (ClientReviewCriterion<Long> cc : ccs) {
            denominators.add(cc.getItems().size());
        }
        final int mutualDenominator = CalculationUtils.mutualDenominator(denominators);

        int v = 0;
        for (ClientReviewCriterion<Long> cc : ccs) {
            int counter = 1;
            for (ClientReviewCriterionItem<Long> cci : cc.getItems()) {
                if (values.contains(cci.getValue())) {
                    break;
                }
                counter++;
            }
            v += CalculationUtils.numeratorFromMutualDenominator(counter, cc.getItems().size(), mutualDenominator);
        }
        final int percent = CalculationUtils.percent(v / ccs.size(), mutualDenominator);

        EvaluationItemValue percentResult = new EvaluationItemValue(new EvaluationValue(artefactUuid, percent));
        percentResult.setHelpTitle(ECMessageUtils.c.reviewerEvaluation_helpTitle_totalScore());
        percentResult.setHelpText(ECMessageUtils.c.reviewerEvaluation_helpText_totalScore());
        evaluation.setValue(percentResult);
    }

    /** click handler pro odeslani hodnoceni */
    private ClickHandler sendClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                HashMap<Long, Long> eval = new HashMap<Long, Long>();
                ArrayList<Long> evalc = criterias.getValues();
                for (ClientReviewCriterion<Long> c : data.getCriterias()) {
                    boolean criterionFound = false;
                    for (Long l : evalc) {
                        boolean found = false;
                        for (ClientReviewCriterionItem<Long> i : c.getItems()) {
                            if (l.equals(i.getValue())) {
                                eval.put(c.getId(), l);
                                found = true;
                            }
                        }
                        if (found) {
                            criterionFound = true;
                            break;
                        }
                    }
                    if (!criterionFound) {
                        getFailMessage().setText(ECMessageUtils.c.reviewerEvaluation_text_youMustEnterAllEvaluationOfArtefact());
                        return;
                    }
                }

                UpdateContributionArtefactEvaluationAction update =
                        new UpdateContributionArtefactEvaluationAction(artefactUuid, eval);
                update.setContributionUuid(contributionUuid);
                update.setReviewUuid(reviewUuid);
                update.setPublicComment(publicComment.getText());
                update.setPrivateComment(privateComment.getText());

                EventBus.fire(new EventDispatchEvent(update,
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                setVisible(false);
                                if (callback != null) {
                                    EvaluationItemValue item = evaluation.getValue();
                                    ReviewsContributionArtefactsPanel.addHelpToItemValue(item);
                                    callback.run(item);
                                }

                                EventBus.fire(new ArtefactEvaluationChanged(evaluation.getValue().getValue()));
                            }
                        }, ReviewerEvaluation.this));
            }
        };
    }

    public ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setStyleName(ECResourceUtils.css().reviewerEvaluationError());
        errorPanel.add(failMessage);
        return failMessage;
    }

    @Override
    public void actionStarted() {
        waiting.show();
    }

    @Override
    public void actionEnds() {
        waiting.hide();
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }
}
