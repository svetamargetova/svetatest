package consys.event.conference.gwt.client.comp.evaluator;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientPreviews implements IsSerializable {

    // data
    private EvaluationItemValue overallScore;
    private String name;
    private String description;
    private ArrayList<ClientPreview> reviewers;
    private int maxPoints;

    public ClientPreviews() {
        reviewers = new ArrayList<ClientPreview>();
    }

    public ClientPreviews(EvaluationItemValue overallScore, String name, String description, int maxPoints) {
        this();
        this.overallScore = overallScore;
        this.name = name;
        this.description = description;
        this.maxPoints = maxPoints;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EvaluationItemValue getOverallScore() {
        return overallScore;
    }

    public void setOverallScore(EvaluationItemValue overallScore) {
        this.overallScore = overallScore;
    }

    public ArrayList<ClientPreview> getReviewers() {
        return reviewers;
    }
}
