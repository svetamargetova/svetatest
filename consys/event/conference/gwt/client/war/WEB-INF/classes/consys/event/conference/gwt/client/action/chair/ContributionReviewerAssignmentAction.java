package consys.event.conference.gwt.client.action.chair;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 *
 * @author pepa
 */
public class ContributionReviewerAssignmentAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 1981133113909414833L;
    // data
    private String submissionUuid;
    private String eventUserUuid;
    private boolean assigned;
    private boolean force;

    public ContributionReviewerAssignmentAction() {
    }

    public ContributionReviewerAssignmentAction(String submissionUuid, String eventUserUuid, boolean assigned) {
        this.submissionUuid = submissionUuid;
        this.eventUserUuid = eventUserUuid;
        this.assigned = assigned;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public String getEventUserUuid() {
        return eventUserUuid;
    }

    public String getSubmissionUuid() {
        return submissionUuid;
    }

    /**
     * Priznak ze ak sa ma odstranit recenzent a recenzent uz ma nejake 
     * zrecenzovane artefakty tak ak je <code>true</code> odstrani aj recenzie
     * inak neodstrani a vyhodi chybu.
     * 
     * @return the force
     */
    public boolean isForce() {
        return force;
    }

    /**
     * Priznak ze ak sa ma odstranit recenzent a recenzent uz ma nejake 
     * zrecenzovane artefakty tak ak je <code>true</code> odstrani aj recenzie
     * inak neodstrani a vyhodi chybu.
     * 
     * @param force the force to set
     */
    public void setForce(boolean force) {
        this.force = force;
    }
}
