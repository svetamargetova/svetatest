package consys.event.conference.gwt.client.comp;

import consys.common.gwt.client.action.ConsysAction;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.module.contribution.item.ContributionHeadPanel;
import consys.event.conference.gwt.client.module.contribution.item.HeadPanelContributionListInputFactory;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Abstraktni predek vsech polozek seznamu s prispevky
 * @author pepa
 */
public abstract class ContributionsListItem extends ContributionHeadPanel {

    // data
    private ClientListContributionItem data;

    public ContributionsListItem(ClientListContributionItem data, boolean authorsVisible, boolean onlyPublicComment, ConsysAction titleClickAction) {
        super(data.getOverview(), new HeadPanelContributionListInputFactory(authorsVisible), onlyPublicComment, titleClickAction);
        this.data = data;
        addStyleName(ECResourceUtils.css().contributionListItem());
        createActionButtons();
    }

    public ClientListContributionItem getData() {
        return data;
    }

    /** nastetovani dat do promennych objektu */
    protected abstract void createActionButtons();
}
