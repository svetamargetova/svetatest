package consys.event.conference.gwt.client.module.contribution.item;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.comp.form.Form;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.wrapper.ContentPanelWrapper;
import consys.common.gwt.client.ui.event.HelpEvent;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.comp.EvaluationDisplay;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.module.reviewer.ContributionReviewerForm;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel zobrazuje zakladni informace prispevku.
 *
 * @author pepa
 */
public class ContributionHeadPanel<DATA extends ClientContributionHeadData> extends Composite {

    // komponenty
    private final FlowPanel headPanelDiv;
    private FlowPanel controlsPanel;
    private EvaluationDisplay evaluationDisplay;
    private FormV2 formV2;
    // data
    private final DATA headData;
    private final InputFactory inputFactory;
    private final boolean onlyPublicComment;
    private ConsysAction titleClickAction;

    public ContributionHeadPanel(DATA cc, InputFactory inputFactory, boolean onlyPublicComment) {
        this(cc, inputFactory, onlyPublicComment, null);
    }

    public ContributionHeadPanel(DATA cc, InputFactory inputFactory, boolean onlyPublicComment, ConsysAction titleClickAction) {
        this.inputFactory = inputFactory;
        this.onlyPublicComment = onlyPublicComment;
        this.titleClickAction = titleClickAction;

        headData = cc;
        headPanelDiv = new FlowPanel();
        headPanelDiv.setStyleName(ECResourceUtils.css().contributionHeadPanel());

        // inicializujeme
        ContentPanelWrapper wrapper = new ContentPanelWrapper(headPanelDiv);
        initWidget(wrapper);

        writeBasicInfo();
    }

    private void writeBasicInfo() {
        // Vytvorime zakladne informacie o prispevku
        createBasicInfoPanel();

        // Pridame skore
        createScoreInfoPanel();

        // Pridame stav a mozne ovladacie buttony
        createStateAndControlsPanel();
    }

    private void createScoreInfoPanel() {
        // Zobrazenie skore
        if (inputFactory.showEvaluationDisplay()) {
            headPanelDiv.addStyleName(ECResourceUtils.css().contributionHeadPanelVisibleEvaluation());
            evaluationDisplay = getEvaluationDisplay(headData);
            headPanelDiv.add(evaluationDisplay);
        } else {
            headPanelDiv.addStyleName(ECResourceUtils.css().contributionHeadPanelNoEvaluation());
        }

        headPanelDiv.add(StyleUtils.clearDiv());

    }

    private void createBasicInfoPanel() {
        // Zakladne informacie hlavicky
        formV2 = new FormV2();
        formV2.add(ECMessageUtils.c.const_title(), inputFactory.getTitleWidget(headData, titleClickAction), null, true).setTitle(true);
        if (inputFactory.showSubTitle()) {
            formV2.add(ECMessageUtils.c.const_subtitle(), inputFactory.getSubtitleWidget(headData), null, false);
        }
        formV2.add(ECMessageUtils.c.const_authors(), inputFactory.getAuthorsWidget(headData), null, true);
        formV2.add(ECMessageUtils.c.const_contributors(), inputFactory.getContributorsWidget(headData), null, true);
        if (inputFactory.showType()) {
            formV2.add(ECMessageUtils.c.const_type(), inputFactory.getContributionTypeWidget(headData), null, true);
        }
        formV2.add(ECMessageUtils.c.const_topics(), inputFactory.getTopicsWidget(headData), null, false);
        // Dalsie informacie vzhladom na typ implementacie
        writeAdditionalInfo(formV2, headData);
        headPanelDiv.add(formV2);
    }

    private void createStateAndControlsPanel() {
        // Zobrazenie stavu pirjaty/neprijaty/cakajuci
        FlowPanel panel = new FlowPanel();
        panel.addStyleName(ECResourceUtils.css().contributionHeadPanelStateAndControlPanel());
        // Vytvorime stavovy panel
        FormV2 stateForm = new FormV2();
        if (inputFactory.showState()) {
            stateForm.addStyleName(ECResourceUtils.css().contributionHeadPanelStateForm());

            addContributionState(stateForm);
        }
        panel.add(stateForm);

        // Vytvorime panel na buttony
        controlsPanel = new FlowPanel();
        controlsPanel.addStyleName(ECResourceUtils.css().contributionHeadPanelControlsPanel());
        panel.add(controlsPanel);

        // Clear div
        panel.add(StyleUtils.clearDiv());

        headPanelDiv.add(panel);
    }
    
    public boolean validate(){
        return formV2.validate();
    }

    private void addContributionState(FormV2 formV2) {
        Label stateLabel = new Label();
        stateLabel.addStyleName(ECResourceUtils.css().contributionState());

        switch (headData.getState()) {
            case ACCEPTED:
                stateLabel.setText(ECMessageUtils.c.const_state_accepted());
                stateLabel.addStyleName(ECResourceUtils.css().contributionStateAccepted());
                break;
            case DECLINED:
                stateLabel.setText(ECMessageUtils.c.const_state_declined());
                stateLabel.addStyleName(ECResourceUtils.css().contributionStateDeclined());
                break;
            case PENDING:
                stateLabel.setText(ECMessageUtils.c.const_state_pending());
                stateLabel.addStyleName(ECResourceUtils.css().contributionStatePending());
                break;
        }

        formV2.add(ECMessageUtils.c.const_state(), stateLabel, ECMessageUtils.c.contributionHeadPanel_state_helpText(), true);
        formV2.add(ECMessageUtils.c.const_submitDate(), DateTimeUtils.getDateAndTime(headData.getSubmittedDate()), null, true);
    }

    protected EvaluationDisplay getEvaluationDisplay(DATA data) {
        EvaluationDisplay display = EvaluationDisplay.forHead();
        EvaluationItemValue overall = new EvaluationItemValue(headData.getEvaluations().getOverallScore().getValue());
        overall.setHelpTitle(ECMessageUtils.c.contributionHeadPanel_overallScore_helpTitle());
        overall.setHelpText(ECMessageUtils.c.contributionHeadPanel_overallScore_helpText());

        List<EvaluationItemValue> values = new ArrayList<EvaluationItemValue>(headData.getEvaluations().getPartialScores().size());
        for (EvaluationValue item : headData.getEvaluations().getPartialScores()) {
            values.add(partialItem(item));
        }

        display.setOverallScore(overall);
        display.setPartialScores(values);
        return display;
    }

    /** vytvori hodnotu castecneho hodnoceni s nastavenym helpem */
    protected EvaluationItemValue partialItem(EvaluationValue value) {
        EvaluationItemValue partial = new EvaluationItemValue(value);
        partial.setHelpTitle(ECMessageUtils.c.contributionHeadPanel_partialScore_helpTitle());
        partial.setHelpText(ECMessageUtils.c.contributionHeadPanel_partialScore_helpText());
        if (value.isEntered()) {
            // TODO: kontrola jestli je ve value.getUuid uuid recenzenta pro seznamy a pro konkretni recenzi uuid artefaktu
            partial.setOnClickAction(partialAction(value.getUuid()));
        }
        return partial;
    }

    private ConsysAction partialAction(final String reviewUuid) {
        return new ConsysAction() {

            @Override
            public void run() {
                // schovame napovedu
                EventBus.fire(new HelpEvent(null));
                ContributionReviewerForm review = new ContributionReviewerForm(headData.getTitle(), headData.getUuid(),
                        reviewUuid, onlyPublicComment);
                FormUtils.fireNextBreadcrumb(headData.getTitle(), review);
            }
        };
    }

    /**
     * Pridavanie buttonov musi ist v reverznom poradi.
     * <p/>
     * @param control
     */
    public void addControl(ActionImage control) {
        control.addStyleName(ECResourceUtils.css().contributionHeadPanelControl());
        controlsPanel.add(control);
    }

    /**
     * Pridavanie buttonov musi ist v reverznom poradi.
     * <p/>
     * @param controlName
     * @param clickHandler
     */
    public void addControl(String controlName, ClickHandler clickHandler) {
        ActionImage control = ActionImage.withoutIcon(controlName);
        control.setClickHandler(clickHandler);
        addControl(control);
    }

    public EvaluationDisplay getEvaluationDisplay() {
        return evaluationDisplay;
    }

    /**
     * Mozne pouzitie pre potomkov ako rozsirenie
     */
    protected void writeAdditionalInfo(Form form, DATA headData) {
    }

    public static interface InputFactory<DATA extends ClientContributionHeadData> {

        Widget getTitleWidget(DATA data, ConsysAction clickAction);

        Widget getSubtitleWidget(DATA data);

        Widget getAuthorsWidget(DATA data);

        Widget getContributorsWidget(DATA data);

        Widget getContributionTypeWidget(DATA data);

        Widget getTopicsWidget(DATA data);

        boolean showSubTitle();

        boolean showType();

        boolean showEvaluationDisplay();

        boolean showState();
    }
}
