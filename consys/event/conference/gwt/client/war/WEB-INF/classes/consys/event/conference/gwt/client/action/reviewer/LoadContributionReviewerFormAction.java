package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData;

/**
 * Akce pro nacteni dat do formulare hodnoceni recenzenta
 * @author pepa
 */
public class LoadContributionReviewerFormAction extends EventAction<ClientContributionReviewerFormData> {

    private static final long serialVersionUID = -2067905143406851094L;
    // data
    private String contributionUuid;    

    public LoadContributionReviewerFormAction() {
    }

    public LoadContributionReviewerFormAction(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    
    public String getContributionUuid() {
        return contributionUuid;
    }

}
