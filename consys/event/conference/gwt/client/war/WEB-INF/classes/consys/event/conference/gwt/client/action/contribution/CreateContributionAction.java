package consys.event.conference.gwt.client.action.contribution;

import consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact;
import consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.InvitedUser;
import java.util.HashSet;
import java.util.Set;

/**
 * Vytvori novy prispevok. Obsah artefackot uz musi byt nahraty na serveru.
 * @author palo
 */
public class CreateContributionAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -7922406791070886776L;
    
    private String uuid;
    private String contributionUuid;
    private String[] authors;
    private String title;
    private String subtitle;
    private String contributionTypeUuid;
    private Set<Long> topics;
    private Set<InvitedUser> contributors;
    private Set<FileContributionArtefact> contributionFileArtefacts;
    private Set<TextContributionArtefact> contributionTextArtefacts;
    

    public CreateContributionAction() {
        contributionFileArtefacts = new HashSet<FileContributionArtefact>();
        contributionTextArtefacts = new HashSet<TextContributionArtefact>();
        contributors = new HashSet<InvitedUser>();
        topics = new HashSet<Long>();
    }

    /**
     * @return the contributionUuid
     */
    public String getContributionUuid() {
        return contributionUuid;
    }

    /**
     * @param contributionUuid the contributionUuid to set
     */
    public void setContributionUuid(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the contributionTypeUuid
     */
    public String getContributionTypeUuid() {
        return contributionTypeUuid;
    }

    /**
     * @param contributionTypeUuid the contributionTypeUuid to set
     */
    public void setContributionTypeUuid(String contributionTypeUuid) {
        this.contributionTypeUuid = contributionTypeUuid;
    }

    /**
     * @return the topics
     */
    public Set<Long> getTopics() {
        return topics;
    }

    /**
     * @return the contributors
     */
    public Set<InvitedUser> getContributors() {
        return contributors;
    }
   

    /**
     * @return the contributionArtefacts
     */
    public Set<FileContributionArtefact> getFileArtefacts() {
        return contributionFileArtefacts;
    }
    
    
    public Set<TextContributionArtefact> getTextArtefacts() {
        return contributionTextArtefacts;
    }
    

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the authors
     */
    public String[] getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public void addFileArtefact(String artefactTypeUuid, String artefactUuid){
        getFileArtefacts().add(new FileContributionArtefact(artefactTypeUuid, artefactUuid));
    }
    
    public void addTextArtefact(String artefactTypeUuid, String content){
        getTextArtefacts().add(new TextContributionArtefact(artefactTypeUuid, content));
    }
    
    public void removeFileArtefact(String artefactUuid){
        FileContributionArtefact u = null;
        for (FileContributionArtefact a : getFileArtefacts()) {
            if(a.getArtefactUuid().equalsIgnoreCase(artefactUuid)){
                u = a;
                break;
            }
        }
        if(u == null){
            throw new NullPointerException("There is no artefact with uuid "+artefactUuid);
        }
        getFileArtefacts().remove(u);        
    }
    
    
}
