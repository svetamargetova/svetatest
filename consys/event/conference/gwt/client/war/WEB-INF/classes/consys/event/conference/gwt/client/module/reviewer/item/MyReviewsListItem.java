package consys.event.conference.gwt.client.module.reviewer.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.comp.ContributionsListItem;
import consys.event.conference.gwt.client.module.reviewer.ContributionReviewerForm;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Prehled recenzentovych prispevku k ohodnoceni
 * @author pepa
 */
public class MyReviewsListItem extends ContributionsListItem {

    public MyReviewsListItem(final ClientListContributionItem data) {
        super(data, false, false, new ConsysAction() {

            @Override
            public void run() {
                reviewAction(data);
            }
        });
    }

    private ClickHandler reviewClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                reviewAction(getData());
            }
        };
    }

    private static void reviewAction(ClientListContributionItem data) {
        ContributionReviewerForm w = new ContributionReviewerForm(data.getOverview().getTitle(), data.getOverview().getUuid());
        FormUtils.fireNextBreadcrumb(data.getOverview().getTitle(), w);

    }

    @Override
    protected void createActionButtons() {
        addControl(ECMessageUtils.c.const_showDetails(), reviewClickHandler());
    }
}
