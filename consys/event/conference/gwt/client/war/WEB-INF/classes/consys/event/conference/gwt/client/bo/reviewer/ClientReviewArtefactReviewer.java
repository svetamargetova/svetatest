package consys.event.conference.gwt.client.bo.reviewer;

import consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact;

/**
 *
 * @author palo
 */
public class ClientReviewArtefactReviewer extends ClientReviewArtefact {

    private String privateComment;
    private String publicComment;

    public ClientReviewArtefactReviewer() {
    }

    public ClientReviewArtefactReviewer(String privetComment, String publicComment) {
        this.privateComment = privetComment;
        this.publicComment = publicComment;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public String getPublicComment() {
        return publicComment;
    }

    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    public void setPublicComment(String publicComment) {
        this.publicComment = publicComment;
    }
}
