package consys.event.conference.gwt.client.module.reviewer.item;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.event.ScrollToReviewEvent;
import consys.event.conference.gwt.client.module.chair.item.ContributionArtefactsChairReviewPanel;
import consys.event.conference.gwt.client.module.contribution.item.ContributionFormArtefactsPanelBuilder;
import consys.event.conference.gwt.client.module.contribution.item.ContributionFormArtefactsPanelBuilderUtils;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.HashMap;
import java.util.List;

/**
 * Jednotliva hodnoceni recenzenta
 * <p/>
 * @author pepa
 */
public class ReviewsContributionArtefactsPanel extends ContributionFormArtefactsPanelBuilder implements ScrollToReviewEvent.Handler {

    // komponenty
    private HeadlinePanel headlinePanel;
    // data    
    private String reviewUuid;
    /** pokud je hodnota true, znamena, ze se jedna o autora recenze */
    private boolean isAuthor;
    private boolean onlyPublicComment;
    /** klic - uuid artefaktu, hodnota - tlacitko na rozkliknuti recenze */
    private HashMap<String, ActionImage> reviewButtons;

    public ReviewsContributionArtefactsPanel(ClientContributionHeadData head, boolean isAuthor, String reviewUuid,
            List<ClientReviewCycle> reviewCycles, boolean onlyPublicComment) {
        super(reviewCycles, head);
        addStyleName(ECResourceUtils.css().myContributionReviews());

        this.reviewUuid = reviewUuid;
        this.isAuthor = isAuthor;
        this.onlyPublicComment = onlyPublicComment;

        if (onlyPublicComment || !isAuthor) {
            headlinePanel = new HeadlinePanel(ECMessageUtils.c.contributionReviewerForm_text_artifactsOfContribution());
        } else {
            headlinePanel = new HeadlinePanel(ECMessageUtils.c.contributionReviewerForm_text_reviewArtifacts());
        }

        reviewButtons = new HashMap<String, ActionImage>();
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        EventBus.get().addHandler(ScrollToReviewEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        super.onUnload();
        EventBus.get().removeHandler(ScrollToReviewEvent.TYPE, this);
    }

    @Override
    protected HeadlinePanel createHeadlinePanel() {
        return headlinePanel;
    }

    /** vraci hodnoceni artefaktu se zadanou hodnotou a v pozadovane velikosti */
    private EvaluationItem totalItem(EvaluationValue v, boolean big) {
        EvaluationItemValue value = new EvaluationItemValue(v);
        addHelpToItemValue(value);
        return big ? EvaluationItem.medium(value) : EvaluationItem.small(value);
    }

    /** nastavi do EvaluationItemValue napovedou pro artefakt */
    public static void addHelpToItemValue(EvaluationItemValue value) {
        value.setHelpTitle(ECMessageUtils.c.reviewCycles_text_artefactScoreTitle());
        value.setHelpText(ECMessageUtils.c.reviewCycles_text_artefactScoreText());
    }

    // vytvori callback s akcema, ktere se maji provest po uspesnem odeslani hodnoceni
    private ConsysActionWithValue<EvaluationItemValue> callback(final EvaluationItem totalItem) {
        return new ConsysActionWithValue<EvaluationItemValue>() {

            @Override
            public void run(EvaluationItemValue data) {
                totalItem.setVisible(true);
                totalItem.setValue(data);
            }
        };
    }

    @Override
    public boolean canEditArtefact(boolean b) {
        return false;
    }

    @Override
    public void addArtefact(ClientArtefactType artefactType, FormV2 form, FlowPanel panel, boolean editable) {
        if (artefactType instanceof ClientReviewArtefactReviewer) {
            ClientReviewArtefactReviewer cra = (ClientReviewArtefactReviewer) artefactType;

            // prida hodnoceni artafaktu
            if (cra.isAutoAccepted()) {
                form.addValueWidget(ContributionArtefactsChairReviewPanel.autoAcceptedArtefact(cra.getArtefactTypeUuid()));
            } else {
                // nastaveni ReviewerEvaluation
                ReviewerEvaluation re = new ReviewerEvaluation();
                re.setVisible(false);
                re.setStyleName(ECResourceUtils.css().myContributionReviewsReview());
                re.setEditable(editable && isAuthor);
                re.setOnlyPublicComment(onlyPublicComment);

                // male celkove hodnoceni
                EvaluationItem totalItem = totalItem(cra.getTotalScore(), false);
                FlowPanel totalItemWrapper = ContributionFormArtefactsPanelBuilderUtils.createTotalItemWrapper(totalItem, true);

                ReviewButtonClickHandler rb = new ReviewButtonClickHandler();
                rb.setCallback(callback(totalItem));
                rb.setClientReviewArtefact(cra);
                rb.setContributionUuid(getContributionUuid());
                rb.setEvaluationItem(totalItem);
                rb.setReviewUuid(reviewUuid);
                rb.setReviewerEvaluation(re);

                // rozklikavaci tlacitko hodnoceni
                String buttonTitle = editable && isAuthor ? ECMessageUtils.c.const_review() : ECMessageUtils.c.const_showReview();
                ActionImage button = ActionImage.withoutIcon(buttonTitle);
                
                rb.setButton(button);
                button.setClickHandler(rb);
                
                reviewButtons.put(cra.getArtefactUuid(), button);
                LoggerFactory.getLogger(ReviewsContributionArtefactsPanel.class).debug("Review button uuid: " + cra.getArtefactUuid());

                form.add(totalItemWrapper, button, false);
                panel.add(re);
            }
        }
    }

    @Override
    public boolean useUpdateArtefactUrl() {
        return true;
    }

    @Override
    public void scrollToReview(ScrollToReviewEvent event) {
        GWT.log("Scroll to artefact: " + event.getArtefactUuid());
        ActionImage button = reviewButtons.get(event.getArtefactUuid());
        if (button != null) {
            // tlacitko nalezeno, preskrolujeme a kliknem si na nej
            Window.scrollTo(button.getAbsoluteLeft(), button.getAbsoluteTop());
            if (button.getClickHandler() instanceof ReviewButtonClickHandler) {
                ReviewButtonClickHandler rb = (ReviewButtonClickHandler) button.getClickHandler();
                if (!rb.isOpen()) {
                    button.click();
                }
            }
        }
    }
}
