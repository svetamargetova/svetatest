package consys.event.conference.gwt.client.module.reviewer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopics;
import consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.client.bo.setting.UserTopicsResult;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 * Formular pro vyber topiku
 * @author pepa
 */
public class PreferredTopicSelectForm extends RootPanel {

    // komponenty
    private SimpleFormWithHelpPanel mainPanel;
    private FlowPanel content;

    public PreferredTopicSelectForm() {
        super(ECMessageUtils.c.preferredTopicSelectForm_title());

        mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        init();
    }

    @Override
    protected void onLoad() {
        EventDispatchEvent loadEvent = new EventDispatchEvent(new ListReviewerPreferredTopics(),
                new AsyncCallback<UserTopicsResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava EventActionExcecutor
                    }

                    @Override
                    public void onSuccess(UserTopicsResult result) {
                        content.clear();
                        ArrayList<Long> selected = result.getSelectedTopics();
                        TopicItem last = null;
                        for (ClientTopic ct : result.getTopics()) {
                            last = new TopicItem(ct, selected.contains(ct.getId()));
                            content.add(last);
                        }
                        if (last != null) {
                            last.setHeight("40px");
                        }
                    }
                }, getSelf());
        EventBus.get().fireEvent(loadEvent);
    }

    private void init() {
        content = new FlowPanel();
        mainPanel.addWidget(content);

        ActionImage save = ActionImage.getConfirmButton(UIMessageUtils.c.const_save());
        save.addStyleName(FLOAT_LEFT);
        save.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UpdateReviewerTopicsAction update = new UpdateReviewerTopicsAction();

                for (int i = 0; i < content.getWidgetCount(); i++) {
                    TopicItem ti = (TopicItem) content.getWidget(i);
                    if (ti.isChecked()) {
                        update.getUserTopics().add(ti.getId());
                    }
                }

                EventDispatchEvent updateEvent = new EventDispatchEvent(update,
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, getSelf());
                EventBus.get().fireEvent(updateEvent);
            }
        });

        ActionLabel back = new ActionLabel(UIMessageUtils.c.const_back(), MARGIN_LEFT_20 + " " + FLOAT_LEFT + " " + MARGIN_TOP_3);
        back.setClickHandler(FormUtils.breadcrumbBackClickHandler());

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(save);
        controlPanel.add(back);
        controlPanel.add(StyleUtils.clearDiv());
        mainPanel.addWidget(controlPanel);

        //mainPanel.addHelpTitle("Consectetur adipiscing");
        //mainPanel.addHelpText("Sed at leo nunc, eget iaculis ligula. Donec accumsan tempus posuere. Duis fermentum, mi tincidunt cursus vestibulum, magna leo vestibulum eros, at feugiat ligula felis adipiscing diam. Nam ac venenatis risus. Suspendisse vestibulum, lacus ut consectetur congue, nibh augue sagittis sapien, ac lobortis dolor metus et elit. Maecenas in tellus eget turpis elementum luctus vel eget diam. Integer eros justo, eleifend in varius id, tincidunt tempus tortor. Nam fringilla scelerisque nibh, eu fermentum sem iaculis a.");
    }

    private class TopicItem extends FlowPanel {

        // komponenty
        private ConsysCheckBox checkBox;
        // data
        private ClientTopic ct;

        public TopicItem(ClientTopic ct, boolean checked) {
            super();
            this.ct = ct;
            setStyleName(CLEAR_BOTH);
            setHeight("20px");

            checkBox = new ConsysCheckBox();
            checkBox.setValue(checked);
            checkBox.addStyleName(FLOAT_LEFT);
            add(checkBox);

            Label label = StyleUtils.getStyledLabel(ct.getName(), FLOAT_LEFT, FONT_BOLD, MARGIN_LEFT_10);
            add(label);
        }

        /** id topiku */
        public Long getId() {
            return ct.getId();
        }

        /** true pokud je zaskrtnuty */
        public boolean isChecked() {
            return checkBox.getValue();
        }
    }
}
