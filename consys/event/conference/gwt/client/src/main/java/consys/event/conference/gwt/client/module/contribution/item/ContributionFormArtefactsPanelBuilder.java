package consys.event.conference.gwt.client.module.contribution.item;

import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.common.gwt.client.ui.comp.panel.InfoPanel;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.client.comp.ArtefactUpload;
import consys.event.conference.gwt.client.comp.Separator;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public abstract class ContributionFormArtefactsPanelBuilder extends ConsysFlowPanel {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private List<ClientReviewCycle> reviewCycles;
    private List<ArtefactUpload> activeArtefactsUpload;
    private final String contributionUuid;
    private final String contributionTitle;

    public ContributionFormArtefactsPanelBuilder(ClientContributionHeadData head) {
        this(null, head);
    }

    public ContributionFormArtefactsPanelBuilder(List<ClientReviewCycle> cycles, ClientContributionHeadData head) {
        this.reviewCycles = cycles;
        this.contributionUuid = head.getUuid();
        this.contributionTitle = head.getTitle();
        addStyleName(ECResourceUtils.css().reviewCycles());
    }

    public String getContributionUuid() {
        return contributionUuid;
    }

    public String getContributionTitle() {
        return contributionTitle;
    }

    public void setReviewCycles(List<ClientReviewCycle> cycles) {
        this.reviewCycles = cycles;
        generate();
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        generate();
    }

    public void regenerate() {
        generate();
    }

    private void generate() {
        clear();

        activeArtefactsUpload = new ArrayList<ArtefactUpload>();

        // vytvorime headline - potomkovia mozu menit
        add(createHeadlinePanel());

        Widget w = createUnderHeadlinePanelContent();
        if (w != null) {
            add(w);
        }

        final Date today = new Date();

        if (reviewCycles != null && !reviewCycles.isEmpty()) {
            for (int i = 0; i < reviewCycles.size(); i++) {
                if (i != 0) {
                    add(Separator.fullWidth());
                }

                // Vytvorime zahlavie recenzencneho cyklu
                ClientReviewCycle c = reviewCycles.get(i);

                // Vytvorime a vlozime info o artefakte                                                    
                boolean editable = canEditArtefact(c.isEditable());

                FormV2 form = new FormV2();
                createEvaluationTerm(form, editable, today, c.getFrom(), c.getTo());
                add(form);

                logger.info("Cycle <" + c.getFrom() + " - " + c.getTo() + "> editable:" + editable);

                // Generujeme obsah pre kazdy jeden artefact
                for (int j = 0; j < c.getArtefacts().size(); j++) {
                    ClientArtefactType artefactType = c.getArtefacts().get(j);

                    // Panel ktory obsahuje vsetko k tomuto artefaktu
                    FlowPanel artefactPanel = new FlowPanel();
                    add(artefactPanel);

                    // formular pre zaklad
                    FormV2 artefactForm = new FormV2();
                    artefactPanel.add(artefactForm);

                    if (j != 0) {
                        artefactForm.addValueWidget(Separator.formWidth());
                    }

                    logger.info("Artefact: title=" + artefactType.getArtefactTypeName() + " enabled=" + editable);

                    ArtefactUpload artefactUpload = new ArtefactUpload(artefactType, contributionUuid);
                    artefactUpload.setEnabled(editable);
                    artefactUpload.setUpdateUploadUrl(useUpdateArtefactUrl());
                    if (editable) {
                        activeArtefactsUpload.add(artefactUpload);
                    }

                    artefactForm.add(artefactType.getArtefactTypeName(), artefactUpload, artefactType.isRequired());

                    // Potomkovia mozu pridat detail
                    addArtefact(artefactType, artefactForm, artefactPanel, c.isEditable());

                }
                if (c.getArtefacts() == null || c.getArtefacts().isEmpty()) {
                    form.addValueWidget(new InfoPanel(ECMessageUtils.c.contributionFormArtefactsPanelBuilder_text_noArtefactInCycle()));
                }
            }
        } else {
            // Ak nemame co generovat tak vlozime - potomkovia mozu menit
            Panel panel = createNoCyclesPanel();
            if (panel != null) {
                add(panel);
            }
        }
    }

    public List<ArtefactUpload> getActiveArtefactsUpload() {
        return activeArtefactsUpload;
    }

    public List<ArtefactUpload> getActiveTextArtefactsUpload() {
        List<ArtefactUpload> textArtefacts = new ArrayList<ArtefactUpload>();
        for (ArtefactUpload au : getActiveArtefactsUpload()) {
            if (!au.getClientArtefactType().isFile()) {
                textArtefacts.add(au);
            }
        }
        return textArtefacts;
    }

    /** prida terminy cyklu */
    private void createEvaluationTerm(FormV2 form, boolean editable, Date now, Date from, Date to) {
        if (from == null || to == null) {
            // Ak nemame datumy
            return;
        }
        boolean open = now.after(from) && now.before(to) && editable;

        Widget titleWidget = FormV2.getTitleWidget(ECMessageUtils.c.const_evaluationTerm(), null, true);
        if (open) {
            titleWidget.addStyleName(ECResourceUtils.css().reviewTermOpen());
        } else {
            titleWidget.addStyleName(ECResourceUtils.css().reviewTermClose());
        }

        String sFrom = DateTimeUtils.monthDayYearFormat.format(from);
        String sTo = DateTimeUtils.monthDayYearFormat.format(to);
        Label term = new Label(sFrom + " " + FormUtils.NDASH + " " + sTo);
        term.setStyleName(open ? ECResourceUtils.css().reviewTermOpen() : ECResourceUtils.css().reviewTermClose());

        form.add(titleWidget, term, true);
    }

    protected HeadlinePanel createHeadlinePanel() {
        return new HeadlinePanel(ECMessageUtils.c.contributionReviewerForm_text_reviewArtifacts());
    }

    protected Panel createNoCyclesPanel() {
        Label label = new Label(ECMessageUtils.c.reviewCycles_text_noCycles());
        label.setStyleName(ECResourceUtils.css().reviewCyclesEmpty());
        return new SimplePanel(label);
    }

    /** vytvori dodatecny obsah hned pod titulkem */
    protected Widget createUnderHeadlinePanelContent() {
        return null;
    }

    public abstract void addArtefact(ClientArtefactType artefactType, FormV2 artefactFormPanel, FlowPanel panel, boolean editable);

    /**
     * Vrati priznak ci vzhladom na pouzitie buildera je mozne menit artefakty
     * <p/>
     * @param nowInReviewCycleTerm priznak ze dnesok je v ramci daneho cyklu
     * <p/>
     * @return
     */
    public abstract boolean canEditArtefact(boolean nowInReviewCycleTerm);

    /**
     * Priznak ktory rozhoduje na aku adresu sa budu uploadovat artefacty.
     * <p/>
     * @return
     */
    public abstract boolean useUpdateArtefactUrl();
}
