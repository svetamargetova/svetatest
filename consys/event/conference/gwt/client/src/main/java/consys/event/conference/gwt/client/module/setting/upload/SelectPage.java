package consys.event.conference.gwt.client.module.setting.upload;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ListPanel.ListPanelDelegate;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction;
import consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction;
import consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.module.setting.UploadFormItem;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Seznam uploadovanych souboru
 * @author pepa
 */
public class SelectPage extends FlowPanel implements ListPanelDelegate<UploadItem> {

    // komponenty
    private FileList fileList;
    private HTML html;
    private UploadItem selectedItem;
    private ActionImage selectButton;
    // data
    private boolean loaded;
    private boolean isTemplate;
    private UploadArtefactDialog parent;

    public SelectPage(final boolean isTemplate, final UploadArtefactDialog parent) {
        loaded = false;
        this.isTemplate = isTemplate;
        this.parent = parent;
        

        fileList = new FileList(12, parent, this);
        fileList.setStaticHeight();
        fileList.setWidth("290px");
        fileList.addStyleName(ECoResourceUtils.bundle().css().rightsDialogList());
        fileList.addStyleName(StyleUtils.FLOAT_LEFT);
        fileList.setSelectable(true);
        fileList.setDelegate(this);

        html = new HTML();
        html.setWidth("180px");
        html.setStyleName(StyleUtils.FLOAT_LEFT);

        FlowPanel content = new FlowPanel();
        content.add(fileList);
        content.add(html);
        content.add(StyleUtils.clearDiv());
        add(content);

        add(Separator.separator());
        add(StyleUtils.clearDiv("20px"));

        selectButton = ActionImage.getConfirmButton(ECMessageUtils.c.selectPage_button_select());
        selectButton.setEnabled(false);
        selectButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                parent.getTarget().add(new UploadFormItem(selectedItem.getObject()));
                parent.hide();
            }
        });
        selectButton.addStyleName(StyleUtils.FLOAT_LEFT);

        ActionLabel close = new ActionLabel(UIMessageUtils.c.const_close(), StyleUtils.FLOAT_RIGHT + " " + StyleUtils.MARGIN_RIGHT_10);
        close.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                parent.hide();
            }
        });

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.addStyleName(StyleUtils.MARGIN_HOR_10);
        controlPanel.add(selectButton);
        controlPanel.add(close);
        controlPanel.add(StyleUtils.clearDiv());
        add(controlPanel);
    }

    @Override
    protected void onLoad() {
        if (!loaded) {
            EventDispatchEvent loadData = new EventDispatchEvent(new ListUploadedArtefactTemplateAction(!isTemplate),
                    new AsyncCallback<ArrayListResult<CommonThumb>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<CommonThumb> result) {
                            loaded = true;
                            fileList.setUploadedFiles(result.getArrayListResult());
                        }
                    }, parent);
            EventBus.get().fireEvent(loadData);
        }
    }

    private void updateHTML(ClientUploadedArtefactTemplate result) {
        String text = "<b>" + UIMessageUtils.c.const_name() + "</b><br/>" + result.getName() + "<br/><br/>";
        text += "<b>" + UIMessageUtils.c.const_description() + "</b><br/>" + result.getDescription();
        html.setHTML(text);
    }

    public void clearHTML() {
        html.setHTML("");
    }

    /** prida polozku do seznamu */
    public void addItem(CommonThumb file) {
        fileList.addItem(new UploadItem(file, parent, this));
        // TODO: vybrat
    }

    @Override
    public void clear() {
    }

    public void disableSelectButton() {
        selectButton.setEnabled(false);
    }

    @Override
    public void didSelect(UploadItem item) {
        selectedItem = item;
        selectButton.setEnabled(true);
        EventDispatchEvent loadEvent = new EventDispatchEvent(
                new LoadUploadedArtefactTemplateAction(item.getObject().getUuid()),
                new AsyncCallback<ClientUploadedArtefactTemplate>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientUploadedArtefactTemplate result) {
                        updateHTML(result);
                    }
                }, parent);
        EventBus.get().fireEvent(loadEvent);
    }

    public void removeItem(int index) {
        fileList.removeItem(index);
    }
}
