package consys.event.conference.gwt.client.module.setting.upload;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysFileUpload;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.utils.ServletUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils.ArtefactUrlEnum;

/**
 * Upload souboru
 * @author pepa
 */
public class CreatePage extends FlowPanel {

    // komponenty
    private ActionImage upload;

    public CreatePage(final boolean isTemplate, final SelectPage pageSelect, final UploadArtefactDialog parent) {
        addStyleName(StyleUtils.MARGIN_HOR_10);
        addStyleName(StyleUtils.MARGIN_TOP_10);
        add(StyleUtils.getStyledLabel(ECMessageUtils.c.createPage_text_uploadItem(), StyleUtils.FONT_BOLD,
                StyleUtils.FONT_16PX, StyleUtils.MARGIN_BOT_20));

        final TextBox nameBox = new ConsysStringTextBox("320px", 1, 255, UIMessageUtils.c.const_name());
        nameBox.setName("t");
        ConsysTextArea descriptionBox = new ConsysTextArea(0, 512, "320px", "50px", true, UIMessageUtils.c.const_description());
        descriptionBox.setName("d");
        final ConsysFileUpload up = new ConsysFileUpload(320);
        up.setName("file");

        final BaseForm baseForm = new BaseForm("100px");
        baseForm.addRequired(0, UIMessageUtils.c.const_name(), nameBox);
        baseForm.addOptional(1, UIMessageUtils.c.const_description(), descriptionBox);
        baseForm.addRequired(2, ECMessageUtils.c.const_file(), up);

        try {
            baseForm.addContent(3, ECResourceUtils.requiredFormItems());
        } catch (NotInCacheException ex) {
            parent.getFailMessage().setText(ECMessageUtils.c.const_actionUrlFail());
            return;
        }

        String actionUrl = null;
        try {
            actionUrl = ECResourceUtils.artefactAdminUploadUrl(isTemplate ? ArtefactUrlEnum.TEMPLATE : ArtefactUrlEnum.EXAMPLE);
        } catch (NotInCacheException ex) {
        }
        if (actionUrl == null) {
            parent.getFailMessage().setText(ECMessageUtils.c.const_actionUrlFail());
            return;
        }


        final FormPanel form = new FormPanel();
        form.setMethod(FormPanel.METHOD_POST);
        form.setEncoding(FormPanel.ENCODING_MULTIPART);
        form.setAction(actionUrl);
        form.setHeight("202px");
        form.setWidget(baseForm);
        form.addSubmitHandler(new SubmitHandler() {

            @Override
            public void onSubmit(SubmitEvent event) {
                parent.actionStarted();
            }
        });
        form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {

            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {
                String result = event.getResults();
                // odstraneni pripadnych tagu
                result = result.replaceAll(ServletUtils.TAG_MATCH, "").trim();
                result = result.replaceAll(ServletUtils.TAG_MATCH_CODE, "").trim();

                parent.actionEnds();

                if (!ServletUtils.containHttpFail(result, parent.getFailMessage())) {
                    pageSelect.addItem(new CommonThumb(result, nameBox.getText().trim()));
                    parent.showSelect();
                } else {
                    parent.getFailMessage().setText("Upload " + (isTemplate ? "Template" : "Pattern") + "failed");
                }
            }
        });
        add(form);
        add(StyleUtils.clearDiv("10px"));

        upload = ActionImage.getConfirmButton(ECMessageUtils.c.createPage_button_upload());
        upload.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!baseForm.validate(parent.getFailMessage())) {
                    return;
                }
                if (up.getFilename().isEmpty()) {
                    parent.getFailMessage().setText(UIMessageUtils.m.const_fieldMustEntered(ECMessageUtils.c.const_file()));
                    return;
                }
                form.submit();
            }
        });
        upload.addStyleName(StyleUtils.FLOAT_LEFT);

        ActionLabel close = new ActionLabel(UIMessageUtils.c.const_close(), StyleUtils.FLOAT_RIGHT + " " + StyleUtils.MARGIN_RIGHT_10);
        close.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                parent.hide();
            }
        });

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(upload);
        controlPanel.add(close);
        controlPanel.add(StyleUtils.clearDiv());
        add(controlPanel);
    }
}
