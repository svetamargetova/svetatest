package consys.event.conference.gwt.client.comp.evaluator;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.ui.comp.Help;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class CriteriaEvaluator extends Evaluator {

    // data
    private ClientReviewCriterion<Long> criterion;
    private CriteriaEvaluatorListener listener;
    private Long selectedCriteriaId;
    private ArrayList<EvaluatorItem<Long>> evaluatorItems;

    /** interface, ktery implementuje listener co sleduje zmeny  */
    public interface CriteriaEvaluatorListener {

        /** volano pri zmene vybrane hodnoty v evaluatoru */
        void criteriaEvaluatorChanged(EvaluatorItemData<Long> data);

        /** volano po vytvoreni evaluatoru */
        void criteriaEvaluatorCreated(CriteriaEvaluator evaluator);
    }

    public CriteriaEvaluator(ClientReviewCriterion<Long> criterion) {
        super();
        this.criterion = criterion;
        evaluatorItems = new ArrayList<EvaluatorItem<Long>>();
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        if (listener != null) {
            listener.criteriaEvaluatorCreated(this);
        }
    }

    @Override
    protected EvaluationItem generateChoosedValue() {
        return EvaluationItem.small(new EvaluationItemValue(), true);
    }

    @Override
    protected String evaluatorTitleText() {
        return criterion.getName();
    }

    @Override
    protected void itemsContent(FlowPanel panel) {
        evaluatorItems.clear();
        if (!criterion.getItems().isEmpty()) {
            final int max = criterion.getItems().size();
            for (int i = 0; i < max; i++) {
                ClientReviewCriterionItem<Long> item = criterion.getItems().get(i);
                EvaluatorItem<Long> ei = createEvaluatorItem(item, i, max);
                evaluatorItems.add(ei);
                panel.add(ei);

                if (selectedCriteriaId != null) {
                    getChoosedValue().setValue(ei.getData().getScore());
                }
            }
        } else {
            empty();
        }
    }

    private EvaluatorItem<Long> createEvaluatorItem(ClientReviewCriterionItem<Long> item, int i, int max) {
        EvaluationItemValue evaluationValue = new EvaluationItemValue(i + 1, 0, max);
        evaluationValue.setStartFromOne(true);

        EvaluatorItemData<Long> data = new EvaluatorItemData<Long>();
        data.setName(item.getName());
        data.setValue(item.getValue());
        data.setScore(evaluationValue);
        data.setAction(criteriaItemAction());
        data.setIsValue(true);

        return new EvaluatorItem<Long>(data, i + 1 == max);
    }

    private ConsysActionWithValue<EvaluatorItemData<Long>> criteriaItemAction() {
        return new ConsysActionWithValue<EvaluatorItemData<Long>>() {

            @Override
            public void run(EvaluatorItemData<Long> data) {
                getChoosedValue().setValue(data.getScore());
                selectedCriteriaId = data.getValue();
                rollPanel();
                if (listener != null) {
                    listener.criteriaEvaluatorChanged(data);
                }
            }
        };
    }

    public void setListener(CriteriaEvaluatorListener listener) {
        this.listener = listener;
    }

    public Long getSelectedCriteriaId() {
        return selectedCriteriaId;
    }

    public void setSelectedCriteriaId(ArrayList<Long> selectedIds) {
        if (selectedIds != null && criterion.getItems() != null) {
            ArrayList<ClientReviewCriterionItem<Long>> points = criterion.getItems();
            for (ClientReviewCriterionItem<Long> item : points) {
                if (selectedIds.contains(item.getValue())) {
                    selectedCriteriaId = item.getValue();
                    break;
                }
            }
            for (EvaluatorItem<Long> ei : evaluatorItems) {
                if (ei.getData().getValue().equals(selectedCriteriaId)) {
                    getChoosedValue().setValue(ei.getData().getScore());
                    break;
                }
            }
        }
    }

    @Override
    protected Help createHelp() {
        return Help.left(criterion.getName(), criterion.getDescription());
    }

    public void setEnabled(boolean enabled) {
        for (EvaluatorItem<Long> ei : evaluatorItems) {
            ei.setEnabled(enabled);
        }
    }
}
