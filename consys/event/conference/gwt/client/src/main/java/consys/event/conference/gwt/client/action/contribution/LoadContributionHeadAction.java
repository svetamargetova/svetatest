package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;

/**
 * Akce pro nacteni hlavicky prispevku podle uuid
 * @author pepa
 */
public class LoadContributionHeadAction extends EventAction<ClientContributionHeadData> {

    private static final long serialVersionUID = -1380957209619594987L;
    // data
    private String contributionUuid;

    public LoadContributionHeadAction() {
    }

    public LoadContributionHeadAction(String contributionHead) {
        this.contributionUuid = contributionHead;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }
}
