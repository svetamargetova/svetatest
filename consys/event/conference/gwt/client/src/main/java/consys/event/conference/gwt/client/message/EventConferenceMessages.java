package consys.event.conference.gwt.client.message;

import com.google.gwt.i18n.client.Messages;

/**
 * abecedne serazene zpravy
 * @author pepa
 */
public interface EventConferenceMessages extends Messages {

    String content_error_missing(String field);

    String editFormArtefact_error_invalidNotation(String field);

    String fileRecordUI_error_illegalFileFormat(String field);

    String fileRecordUI_text_allowedFileFormats(String formats);
    
    String contributionHeadPanel_text_deleteContribution(String name);
}
