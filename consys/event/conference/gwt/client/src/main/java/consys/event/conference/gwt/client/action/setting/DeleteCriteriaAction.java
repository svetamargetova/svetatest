package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani kriteria
 * @author pepa
 */
public class DeleteCriteriaAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = 631350422073440455L;
    // data
    private String uuid;

    private Long criteriaId;

    public DeleteCriteriaAction() {
    }

    public DeleteCriteriaAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    /**
     * @return the criteriaId
     */
    public Long getCriteriaId() {
        return criteriaId;
    }
}
