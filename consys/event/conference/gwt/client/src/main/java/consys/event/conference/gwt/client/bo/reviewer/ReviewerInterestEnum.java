package consys.event.conference.gwt.client.bo.reviewer;

/**
 *
 * @author pepa
 */
public enum ReviewerInterestEnum {

    LIKE, NOT_CARE, NOT_WANT, CONFLICT;
    private static final long serialVersionUID = -3300458130046705136L;
}
