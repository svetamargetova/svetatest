package consys.event.conference.gwt.client.panel;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.layout.panel.PanelItem;

/**
 * Panel pro konferenci
 * @author pepa
 */
public class ConferencePanel extends PanelItem {

    // konstanty
    public static final String PANEL_NAME = "ConferencePanel";

    public ConferencePanel() {
        super(true);
    }

    @Override
    public Widget getContent() {
        return new Label("Conference Panel Item");
    }

    @Override
    public String getName() {
        return PANEL_NAME;
    }

    @Override
    public Widget getTitle() {
        return new Label("Header - cela polozka bude v eventovem panelu");
    }
}
