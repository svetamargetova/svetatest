package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Odstrani vsetky nahrate artefakty prispevku.
 * @author palo
 */
public class DeleteAllUploadedContributionArtefactsAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -7922406791070886776L;
    
    private String submissionUuid;
    
    public DeleteAllUploadedContributionArtefactsAction() {
    }

    public DeleteAllUploadedContributionArtefactsAction(String submissionUuid) {
        this.submissionUuid = submissionUuid;        
    }    

    public String getSubmissionUuid() {
        return submissionUuid;
    }
}
