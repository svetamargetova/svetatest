package consys.event.conference.gwt.client.bo.contribution;

/**
 *
 * @author palo
 */
public enum ClientContributionStateEnum {

    ACCEPTED, DECLINED, PENDING;
    private static final long serialVersionUID = -4198346195024038380L;
}
