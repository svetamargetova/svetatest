package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro vytvoreni topiku
 * @author pepa
 */
public class CreateTopicAction extends EventAction<LongResult> {

    private static final long serialVersionUID = 5527044370468920744L;
    // data
    private String name;

    public CreateTopicAction() {
    }

    public CreateTopicAction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
