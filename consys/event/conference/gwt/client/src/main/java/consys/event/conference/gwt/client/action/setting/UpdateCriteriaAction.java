package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;

/**
 * Akce pro aktualizaci kriteria
 * @author pepa
 */
public class UpdateCriteriaAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = 5360019188593599880L;
    // data
    private ClientCriteria criteria;

    public UpdateCriteriaAction() {
    }

    public UpdateCriteriaAction(ClientCriteria criteria) {
        this.criteria = criteria;
    }

    public ClientCriteria getCriteria() {
        return criteria;
    }
}
