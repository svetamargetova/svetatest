package consys.event.conference.gwt.client.module.contribution.item;

import com.google.code.gwt.component.tag.InputTag;
import com.google.code.gwt.component.tag.StringTag;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.user.list.UserInlineListPanel;
import consys.common.gwt.client.utils.StringUtils;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributor;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author palo
 */
public class HeadPanelReadInputFactory implements ContributionHeadPanel.InputFactory {

    // data    
    private boolean authorsVisible;

    public HeadPanelReadInputFactory(boolean authorsVisible) {
        this.authorsVisible = authorsVisible;
    }

    @Override
    public Widget getTitleWidget(ClientContributionHeadData data, final ConsysAction clickAction) {
        Widget widget = FormV2.getValueWidget(data.getTitle(), null);
        if (clickAction != null) {
            DOM.sinkEvents(widget.getElement(), Event.ONCLICK);
            widget.addDomHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    clickAction.run();
                }
            }, ClickEvent.getType());
            widget.addStyleName(ECResourceUtils.css().hand());
        }
        return widget;
    }

    @Override
    public Widget getSubtitleWidget(ClientContributionHeadData data) {
        return FormV2.getValueWidget(data.getSubtitle(), ECMessageUtils.c.const_noSubtitle());
    }

    @Override
    public Widget getAuthorsWidget(ClientContributionHeadData data) {
        if (!authorsVisible && "false".equals(Cache.get().get(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS))) {
            return FormV2.getValueWidget("", ECMessageUtils.c.const_authorsNotVisible());
        } else {
            List<StringTag> list = new ArrayList<StringTag>();
            if (StringUtils.isNotBlank(data.getAuthors())) {
                String[] authors = data.getAuthors().split(",");
                for (int i = 0; i < authors.length; i++) {
                    list.add(new StringTag(authors[i], authors[i]));
                }
            }
            InputTag<StringTag> inputTag = new InputTag<StringTag>(list);
            inputTag.setMode(InputTag.Mode.READ);

            return inputTag;
        }
    }

    @Override
    public Widget getContributorsWidget(ClientContributionHeadData data) {
        if (!authorsVisible && "false".equals(Cache.get().get(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS))) {
            return FormV2.getValueWidget("", ECMessageUtils.c.const_authorsNotVisible());
        } else {
            UserInlineListPanel uilp = new UserInlineListPanel();
            for (ClientContributor ct : data.getContributors()) {
                uilp.addUser(ct.getUuid(), ct.getName());
            }
            return uilp;
        }
    }

    @Override
    public Widget getTopicsWidget(ClientContributionHeadData data) {
        List<StringTag> list = new ArrayList<StringTag>();
        if (StringUtils.isNotBlank(data.getTopics())) {
            String[] topicsa = data.getTopics().split(",");
            for (int i = 0; i < topicsa.length; i++) {
                list.add(new StringTag(topicsa[i], topicsa[i]));
            }
        }
        InputTag<StringTag> inputTag = new InputTag<StringTag>(list);
        inputTag.setMode(InputTag.Mode.READ);


        return inputTag;
    }

    @Override
    public Widget getContributionTypeWidget(ClientContributionHeadData data) {
        return FormV2.getValueWidget(data.getType(), null);
    }

    @Override
    public boolean showEvaluationDisplay() {
        return true;
    }

    @Override
    public boolean showState() {
        return true;
    }

    @Override
    public boolean showSubTitle() {
        return true;
    }

    @Override
    public boolean showType() {
        return true;
    }
}
