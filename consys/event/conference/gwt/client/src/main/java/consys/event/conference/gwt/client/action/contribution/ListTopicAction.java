package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;

/**
 * Akce pro nacteni seznamu topiku
 * @author pepa
 */
public class ListTopicAction extends EventAction<ArrayListResult<ClientTopic>> {

    private static final long serialVersionUID = -8944243979325385550L;
}
