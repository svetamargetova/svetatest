package consys.event.conference.gwt.client.comp;

/**
 * Objekt ve kterem se predava zpracovana odpoved ze serveru
 * @author pepa
 */
public class UploadResponse {

    // data
    private boolean success;
    private String response;

    public UploadResponse(boolean success, String response) {
        this.success = success;
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public boolean isSuccess() {
        return success;
    }
}
