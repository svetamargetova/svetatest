package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.action.UpdateOrderPositionAction;

/**
 * Akce pro aktualizaci pozice typu submissionu
 * @author pepa
 */
public class UpdateContributionTypePositionAction extends UpdateOrderPositionAction {

    private static final long serialVersionUID = -1636119162442357090L;
    // data
    private String uuid;

    public UpdateContributionTypePositionAction() {
    }

    public UpdateContributionTypePositionAction(String uuid, int oldPosition, int newPosition) {
        super(oldPosition, newPosition);
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
