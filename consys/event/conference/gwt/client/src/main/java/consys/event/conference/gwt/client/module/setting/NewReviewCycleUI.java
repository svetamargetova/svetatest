package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Vnitrek dialogu pro vytvoreni noveho cyklu
 * @author pepa
 */
public class NewReviewCycleUI extends NewItemUI {

    // komponenty
    private ConsysDateBox submitFromBox;
    private ConsysDateBox submitToBox;
    private ConsysDateBox reviewFromBox;
    private ConsysDateBox reviewToBox;

    public NewReviewCycleUI() {
        super();
    }

    @Override
    public void createForm(BaseForm form) {
        submitFromBox = ConsysDateBox.getEventHalfBox(true);
        submitToBox = ConsysDateBox.getEventHalfBox(true);

        reviewFromBox = ConsysDateBox.getEventHalfBox(true);
        reviewToBox = ConsysDateBox.getEventHalfBox(true);

        submitFromBox.addValueChangeHandler(DateTimeUtils.dateValueChangeHandler(submitFromBox, submitToBox));
        reviewFromBox.addValueChangeHandler(DateTimeUtils.dateValueChangeHandler(reviewFromBox, reviewToBox));

        Widget submissionPanel = EditFormReviewCycle.datePanel(submitFromBox, submitToBox);
        Widget acceptancePanel = EditFormReviewCycle.datePanel(reviewFromBox, reviewToBox);

        form.addRequired(0, ECMessageUtils.c.editFormReviewCycle_form_submissionPeriod(), submissionPanel);
        form.addOptional(1, ECMessageUtils.c.reviewCycle_form_reviewPeriod(), acceptancePanel);
    }

    @Override
    public void focus() {
        submitFromBox.setFocus(true);
    }

    public ConsysDateBox getSubmitFromBox() {
        return submitFromBox;
    }

    public ConsysDateBox getSubmitToBox() {
        return submitToBox;
    }

    public ConsysDateBox getReviewFromBox() {
        return reviewFromBox;
    }

    public ConsysDateBox getReviewToBox() {
        return reviewToBox;
    }
}
