package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Odstrani nahraty artefaktu prispevku.
 * @author palo
 */
public class DeleteUploadedContributionArtefactAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -7922406791070886776L;
    
    private String submissionUuid;
    private String artefactUuid;
    private String artefactTypeUuid;

    public DeleteUploadedContributionArtefactAction() {
    }

    public DeleteUploadedContributionArtefactAction(String submissionUuid, String artefactUuid, String artefactTypeUuid) {
        this.submissionUuid = submissionUuid;
        this.artefactUuid = artefactUuid;
        this.artefactTypeUuid = artefactTypeUuid;
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    public String getSubmissionUuid() {
        return submissionUuid;
    }
}
