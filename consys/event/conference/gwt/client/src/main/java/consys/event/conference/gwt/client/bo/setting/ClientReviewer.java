package consys.event.conference.gwt.client.bo.setting;

import consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;

/**
 * Objekt hodnotitele prispevku
 * @author pepa
 */
public class ClientReviewer implements IsSerializable {

    private String uuid;
    private String name;
    private ArrayList<ClientTopic> topics;
    private int load;
    private int maxLoad;
    private boolean reviewing;
    private ReviewerInterestEnum interest;

    public ClientReviewer() {
        topics = new ArrayList<ClientTopic>();
    }

    public ReviewerInterestEnum getInterest() {
        return interest;
    }

    public void setInterest(ReviewerInterestEnum interest) {
        this.interest = interest;
    }

    public int getLoad() {
        return load;
    }

    public void setLoad(int load) {
        this.load = load;
    }

    public int getMaxLoad() {
        return maxLoad;
    }

    public void setMaxLoad(int maxLoad) {
        this.maxLoad = maxLoad;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ClientTopic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<ClientTopic> topics) {
        this.topics = topics;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the reviewing
     */
    public boolean isReviewing() {
        return reviewing;
    }

    /**
     * @param reviewing the reviewing to set
     */
    public void setReviewing(boolean reviewing) {
        this.reviewing = reviewing;
    }
}
