package consys.event.conference.gwt.client.bo.contribution;

import java.io.Serializable;

/**
 * Topik
 * @author pepa
 */
public class ClientTopic implements Serializable {

    private static final long serialVersionUID = 5735077638146295536L;
    private Long id;
    private String name;

    public ClientTopic() {
    }

    public ClientTopic(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
