package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Thumb a predek ClientCriteria
 * @author pepa
 */
public class ClientCriteriaThumb implements IsSerializable {

    private Long id;
    private String name;
    /** minimalni pocet bodu */
    private double minimumPoints;
    /** maximalni pocet bodu */
    private int maxPoints;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * minimalni pocet bodu
     * @return the minimumPoints
     */
    public double getMinimumPoints() {
        return minimumPoints;
    }

    /**
     * minimalni pocet bodu
     * @param minimumPoints the minimumPoints to set
     */
    public void setMinimumPoints(double minimumPoints) {
        this.minimumPoints = minimumPoints;
    }

    /**
     * @return the maxPoints
     */
    public int getMaxPoints() {
        return maxPoints;
    }

    /**
     * @param maxPoints the maxPoints to set
     */
    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }
}
