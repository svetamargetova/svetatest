package consys.event.conference.gwt.client.comp;

import com.google.gwt.user.client.ui.Widget;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.comp.evaluator.CriteriaEvaluator;
import consys.event.conference.gwt.client.comp.evaluator.EvaluatorItemData;
import consys.event.conference.gwt.client.comp.evaluator.EvaluatorWrapper;
import java.util.ArrayList;

/**
 * Seznam kriterii k hodnoceni
 * @author pepa
 */
public class CriteriaEvaluatorWrapper extends EvaluatorWrapper<ClientReviewCriterion<Long>, CriteriaEvaluator> implements CriteriaEvaluator.CriteriaEvaluatorListener {

    // data
    private ArrayList<Long> selectedIds;
    private CriteriaEvaluatorWrapperListener listener;
    private boolean enabled;

    public interface CriteriaEvaluatorWrapperListener {

        void allCriteriaSet();
    }

    public CriteriaEvaluatorWrapper() {
        setValues(null);
        enabled = true;
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        setEnabled(enabled);
    }

    /** nastavi kriteria */
    public void setCriterias(ArrayList<ClientReviewCriterion<Long>> criterias) {
        super.setData(criterias);
    }

    /** enabluje/disabluje komponentu */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        for (int i = 0; i < getWidgetCount(); i++) {
            Widget w = getWidget(i);
            if (w instanceof CriteriaEvaluator) {
                ((CriteriaEvaluator) w).setEnabled(enabled);
            }
        }
    }

    @Override
    protected CriteriaEvaluator createEvaluator(ClientReviewCriterion<Long> data) {
        return new CriteriaEvaluator(data);
    }

    @Override
    protected void postProcessEvaluator(CriteriaEvaluator evaluator) {
        evaluator.setListener(this);
    }

    /** vraci hodnoceni ze zobrazeneho! wrapperu */
    public ArrayList<Long> getValues() {
        ArrayList<Long> result = new ArrayList<Long>();
        for (int i = 0; i < getWidgetCount(); i++) {
            Widget w = getWidget(i);
            if (w instanceof CriteriaEvaluator) {
                CriteriaEvaluator e = (CriteriaEvaluator) w;
                if (e.getSelectedCriteriaId() != null) {
                    result.add(e.getSelectedCriteriaId());
                }
            }
        }
        return result;
    }

    /** potreba nastavit pred zobrazenim komponenty! */
    public void setValues(ArrayList<Long> selectedIds) {
        if (selectedIds != null) {
            this.selectedIds = selectedIds;
        } else {
            this.selectedIds = new ArrayList<Long>();
        }
    }

    public void setListener(CriteriaEvaluatorWrapperListener listener) {
        this.listener = listener;
    }

    @Override
    public void criteriaEvaluatorChanged(EvaluatorItemData<Long> evaluator) {
        for (int i = 0; i < getWidgetCount(); i++) {
            Widget w = getWidget(i);
            if (w instanceof CriteriaEvaluator) {
                CriteriaEvaluator e = (CriteriaEvaluator) w;
                Long value = e.getSelectedCriteriaId();
                if (value == null) {
                    return;
                }
            }
        }
        if (listener != null) {
            listener.allCriteriaSet();
        }
    }

    @Override
    public void criteriaEvaluatorCreated(CriteriaEvaluator evaluator) {
        if (!selectedIds.isEmpty()) {
            evaluator.setSelectedCriteriaId(selectedIds);
        }
    }
}
