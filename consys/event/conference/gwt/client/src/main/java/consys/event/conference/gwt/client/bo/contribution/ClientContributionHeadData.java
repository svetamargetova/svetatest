package consys.event.conference.gwt.client.bo.contribution;

import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.evaluation.Evaluation;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author palo
 */
public class ClientContributionHeadData implements Result {

    private static final long serialVersionUID = -5841256340218479801L;
    /** Uuid prispevku */
    private String uuid;
    /** Nazov prispevku  */
    private String title;
    /** Podnazov prispevku  */
    private String subtitle;
    /** Kto posiela */
    private ArrayList<ClientContributor> contributors;
    /** Typ priespevku */
    private String type;
    /** Autori */
    private String authors;
    /** Topici */
    private String topics;
    /** Evaluations */
    private Evaluation<EvaluationValue, EvaluationValue> evaluations;
    /** Stav */
    private ClientContributionStateEnum state;
    /** Datum vytvorenie prispevku */
    private Date submittedDate;

    public ClientContributionHeadData() {
        contributors = new ArrayList<ClientContributor>();
        state = ClientContributionStateEnum.PENDING;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the submittedDate
     */
    public Date getSubmittedDate() {
        return submittedDate;
    }

    /**
     * @param submittedDate the submittedDate to set
     */
    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    /**
     * @return the state
     */
    public ClientContributionStateEnum getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(ClientContributionStateEnum state) {
        this.state = state;
    }

    /**
     * @return the submitters
     */
    public ArrayList<ClientContributor> getContributors() {
        return contributors;
    }

    /**
     * @param submitters the submitters to set
     */
    public void setContributors(ArrayList<ClientContributor> submitters) {
        this.contributors = submitters;
    }

    /**
     * @return the authors
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    /**
     * @return the topics
     */
    public String getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(String topics) {
        this.topics = topics;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the evaluations
     */
    public Evaluation<EvaluationValue, EvaluationValue> getEvaluations() {
        return evaluations;
    }

    /**
     * @param evaluations the evaluations to set
     */
    public void setEvaluations(Evaluation<EvaluationValue, EvaluationValue> evaluations) {
        this.evaluations = evaluations;
    }
}
