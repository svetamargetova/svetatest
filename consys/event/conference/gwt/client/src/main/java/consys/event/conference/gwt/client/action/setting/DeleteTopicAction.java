package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani topicu
 * @author pepa
 */
public class DeleteTopicAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 5527044370468920744L;
    // data
    private Long id;

    public DeleteTopicAction() {
    }

    public DeleteTopicAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
