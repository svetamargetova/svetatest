package consys.event.conference.gwt.client.module.contributor;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.list.*;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.api.list.ContributorContributionsList;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Přehled všech zaslanych příspěvků
 * @author pepa
 */
public class MyContributionList extends DataListPanel implements ContributorContributionsList {

    public MyContributionList() {
        super(ECMessageUtils.c.contributionList_title(), TAG, false);

        ActionImage sub = ActionImage.getPersonButton(ECMessageUtils.c.newContributionForm_title(), true);
        sub.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                NewContributionForm form = new NewContributionForm();
                FormUtils.fireNextBreadcrumb(ECMessageUtils.c.const_submitContribution(), form);
            }
        });
        addLeftControll(sub);

        setListDelegate(new ContributionListDelegate());
        setDefaultFilter(new ListFilter(Filter_ALL) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
        DataListCommonOrderer orderer = new DataListCommonOrderer();
        orderer.addOrderer(ECMessageUtils.c.const_title(), Order_TITLE, true);
        orderer.addOrderer(ECMessageUtils.c.const_submitDate(), Order_SUBMIT_DATE);
        orderer.addOrderer(ECMessageUtils.c.const_score(), Order_SCORE);
        addHeadPanel(orderer);
    }

    /** delegat seznamu */
    private class ContributionListDelegate implements ListDelegate<ClientListContributionItem, ContributionListItem> {

        @Override
        public ContributionListItem createCell(ClientListContributionItem item) {
            return new ContributionListItem(item);
        }
    }

    /** jedna polozka seznamu */
    private class ContributionListItem extends ListItem<ClientListContributionItem> {

        public ContributionListItem(ClientListContributionItem value) {
            super(value, true);
        }

        @Override
        protected void createCell(final ClientListContributionItem item, FlowPanel panel) {
            panel.clear();
            panel.setStyleName(ECResourceUtils.css().contributionListItemWrapper());
            panel.add(new MyContributionListItem(item));
        }
    }
}
