package consys.event.conference.gwt.client.module.reviewer;

import consys.event.conference.gwt.client.module.reviewer.item.ContributionReviewerHeadPanel;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction;
import consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData;
import consys.event.conference.gwt.client.event.ScrollToReviewEvent;
import consys.event.conference.gwt.client.module.reviewer.item.EditReviewCommentsPanel;
import consys.event.conference.gwt.client.module.reviewer.item.ReviewsContributionArtefactsPanel;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Hodnoceni prispevku recenzentem
 * @author pepa
 */
public class ContributionReviewerForm extends RootPanel {

    // komponenty
    private ReviewsContributionArtefactsPanel review;
    // data
    private final String contributionUuid;
    private final String reviewUuid;
    private final boolean onlyPublicComment;
    private String artefactUuidReview;

    public ContributionReviewerForm(String title, String contributionUuid) {
        this(title, contributionUuid, null, false);
    }

    public ContributionReviewerForm(String title, String contributionUuid, String reviewUuid, boolean onlyPublicComment) {
        super(title);
        this.contributionUuid = contributionUuid;
        this.reviewUuid = reviewUuid;
        this.onlyPublicComment = onlyPublicComment;
    }

    @Override
    protected void onLoad() {
        LoggerFactory.getLogger(ContributionReviewerForm.class).debug("loading contribution (" + contributionUuid + ") for review (" + reviewUuid + ")");

        EventAction<ClientContributionReviewerFormData> action;
        if (reviewUuid == null) {
            action = new LoadContributionReviewerFormAction(contributionUuid);
        } else {
            action = new LoadContributionReviewFormAction(contributionUuid, reviewUuid);
        }

        EventBus.fire(new EventDispatchEvent(action,
                new AsyncCallback<ClientContributionReviewerFormData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava EventActionExecutor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().addOrSetText(ECMessageUtils.c.contributionReviewerForm_error_reviewNotFound());
                        }
                    }

                    @Override
                    public void onSuccess(ClientContributionReviewerFormData result) {
                        clear();
                        generateContent(result);
                        if (artefactUuidReview != null) {
                            review.scrollToReview(new ScrollToReviewEvent(artefactUuidReview));
                        }
                    }
                }, this));
    }

    private void generateContent(ClientContributionReviewerFormData data) {
        // hlavicka
        addWidget(new ContributionReviewerHeadPanel(data.getHead(), onlyPublicComment));

        // recenze
        review = new ReviewsContributionArtefactsPanel(data.getHead(), data.isIsAuthor(), data.getHead().getReviewUuid(),
                data.getReviewCycles(), onlyPublicComment);
        addWidget(review);

        // celkovy verejny a soukromy komentar
        EditReviewCommentsPanel comments = new EditReviewCommentsPanel(contributionUuid, data.isIsAuthor(), onlyPublicComment);
        comments.setPublicComment(data.getPublicComment());
        comments.setPrivateComment(data.getPrivateComment());
        addWidget(comments);
    }

    public void scrollToArtefactReview(String artefactUuidReview) {
        this.artefactUuidReview = artefactUuidReview;
    }
}
