package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;

/**
 * Akce pro nacteni jednoho kriteria
 * @author pepa
 */
public class LoadCriteriaAction extends EventAction<ClientCriteria> {

    // data
    private static final long serialVersionUID = -7847084517374895181L;
    // konstanty
    private Long criteriaId;

    public LoadCriteriaAction() {
    }

    public LoadCriteriaAction(Long criteriaId) {
        this.criteriaId = criteriaId;
    }

    public Long getCriteriaId() {
        return criteriaId;
    }
}
