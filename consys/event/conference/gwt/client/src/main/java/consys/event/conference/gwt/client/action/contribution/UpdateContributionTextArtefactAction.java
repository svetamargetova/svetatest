package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact;

/**
 * Aktualizuje artefakty prispevku
 * @author pepa
 */
public class UpdateContributionTextArtefactAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 7887443505168487569L;
    // data
    private String contributionUuid;
    private TextContributionArtefact textContributionArtefact;

    public String getUuid() {
        return contributionUuid;
    }

    public void setUuid(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    /**
     * @return the textContributionArtefact
     */
    public TextContributionArtefact getTextContributionArtefact() {
        return textContributionArtefact;
    }

    /**
     * @param textContributionArtefact the textContributionArtefact to set
     */
    public void setTextContributionArtefact(TextContributionArtefact textContributionArtefact) {
        this.textContributionArtefact = textContributionArtefact;
    }
}
