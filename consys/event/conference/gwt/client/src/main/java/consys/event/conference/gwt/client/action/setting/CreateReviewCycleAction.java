package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import java.util.ArrayList;

/**
 * Akce pro vytvoreni review cycle
 * @author pepa
 */
public class CreateReviewCycleAction extends EventAction<ArrayListResult<Long>> {

    // konstanty
    private static final long serialVersionUID = -3685746193755173519L;
    // data
    private String contributionTypeUuid;
    private ArrayList<ClientSettingsReviewCycle> cycles;

    public CreateReviewCycleAction() {
    }

    public CreateReviewCycleAction(String contributionTypeUuid, ArrayList<ClientSettingsReviewCycle> cycles) {
        this.contributionTypeUuid = contributionTypeUuid;
        this.cycles = cycles;
    }

    public ArrayList<ClientSettingsReviewCycle> getCycles() {
        return cycles;
    }

    public String getContributionTypeUuid() {
        return contributionTypeUuid;
    }
}
