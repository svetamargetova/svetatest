package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;

/**
 * Akce pro nacteni seznamu cyklu typu prispevku
 * @author pepa
 */
public class LoadContributionTypeCyclesAction extends EventAction<ArrayListResult<ClientReviewCycle>> {

    private static final long serialVersionUID = -4281838714367681425L;
    // data
    private String contributionTypeUuid;

    public LoadContributionTypeCyclesAction() {
    }

    public LoadContributionTypeCyclesAction(String contributionTypeUuid) {
        this.contributionTypeUuid = contributionTypeUuid;
    }

    public String getContributionTypeUuid() {
        return contributionTypeUuid;
    }
}
