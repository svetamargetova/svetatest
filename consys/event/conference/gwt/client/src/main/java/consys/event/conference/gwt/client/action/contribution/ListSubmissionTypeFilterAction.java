package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.CommonThumb;

/**
 * Akce pro nacteni seznamu typu pro filtrovani
 * @author pepa
 */
public class ListSubmissionTypeFilterAction extends EventAction<ArrayListResult<CommonThumb>> {

    private static final long serialVersionUID = -3893595133818483601L;
}
