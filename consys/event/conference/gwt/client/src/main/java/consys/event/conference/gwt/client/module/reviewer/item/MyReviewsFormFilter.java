package consys.event.conference.gwt.client.module.reviewer.item;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.DataListCommonFiltersItem;
import consys.common.gwt.client.ui.comp.list.DataListCommonHeadPanel;
import consys.common.gwt.client.ui.comp.list.HeadPanel;
import consys.common.gwt.client.ui.comp.list.ListPagerWidget;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.bo.list.Filter;
import consys.event.conference.api.list.ReviewerReviewsList;
import consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class MyReviewsFormFilter extends HeadPanel implements DataListCommonHeadPanel, ReviewerReviewsList {

    // komponenty
    private FlowPanel filters;
    private ListPagerWidget listPagerWidget;
    private SelectBox<String> selectBox;
    // data
    private ArrayList<DataListCommonFiltersItem> allFilters;
    private DataListCommonFiltersItem activeFilter;

    public MyReviewsFormFilter() {
        this(true);
    }

    public MyReviewsFormFilter(boolean showListPager) {
        allFilters = new ArrayList<DataListCommonFiltersItem>();

        selectBox = new SelectBox<String>();
        selectBox.addStyleName(StyleUtils.FLOAT_LEFT);
        selectBox.setWidth(240);
        selectBox.selectFirst(true);
        final SelectBoxItem<String> initItem = new SelectBoxItem<String>("0", ECMessageUtils.c.myReviewsFormFilter_action_all());
        ArrayList<SelectBoxItem<String>> initItems = new ArrayList<SelectBoxItem<String>>();
        initItems.add(initItem);
        selectBox.setItems(initItems);

        Label title = StyleUtils.getStyledLabel(UIMessageUtils.c.dataListCommon_text_show() + ":",
                ResourceUtils.system().css().dataListCommonOrdererTitle(), StyleUtils.FLOAT_LEFT);

        filters = new FlowPanel();
        filters.add(title);
        filters.addStyleName(StyleUtils.FLOAT_LEFT);

        listPagerWidget = new ListPagerWidget();

        SimplePanel sp = new SimplePanel();
        sp.setWidget(listPagerWidget);
        sp.addStyleName(StyleUtils.FLOAT_RIGHT);
        sp.setVisible(showListPager);

        FlowPanel flowPanel = new FlowPanel();
        flowPanel.setHeight("25px");
        flowPanel.add(filters);
        flowPanel.add(sp);
        initWidget(flowPanel);

        initFilters();

        EventBus.get().fireEvent(new EventDispatchEvent(new ListSubmissionTypeFilterAction(),
                new AsyncCallback<ArrayListResult<CommonThumb>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ArrayListResult<CommonThumb> result) {
                        ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
                        items.add(initItem);
                        for (CommonThumb i : result.getArrayListResult()) {
                            items.add(new SelectBoxItem<String>(i.getUuid(), i.getName()));
                        }
                        selectBox.setItems(items);
                    }
                }, null));
    }

    private void initFilters() {
        final DataListCommonFiltersItem all = new DataListCommonFiltersItem(Filter_TYPE, "all", this);
        allFilters.add(all);
        
        activeFilter = all;

        selectBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                String value = event.getValue().getItem().equals("0") ? null : event.getValue().getItem();
                activeFilter.setFilter(new Filter(Filter_TYPE, value));
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        filters.add(selectBox);
    }

    @Override
    public void setParentList(DataListPanel parentList) {
        for (DataListCommonFiltersItem cf : allFilters) {
            cf.setParentList(parentList);
        }
    }

    @Override
    public ListPagerWidget getListPagerWidget() {
        return listPagerWidget;
    }
}
