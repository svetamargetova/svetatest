package consys.event.conference.gwt.client.module.reviewer.item;

import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.form.Form;
import consys.common.gwt.client.utils.StringUtils;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead;
import consys.event.conference.gwt.client.comp.EvaluationDisplay;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.event.ArtefactEvaluationChanged;
import consys.event.conference.gwt.client.event.ScrollToReviewEvent;
import consys.event.conference.gwt.client.module.contribution.item.ContributionHeadPanel;
import consys.event.conference.gwt.client.module.contribution.item.HeadPanelReadInputFactory;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Panel zobrazujici data pri recenzovani prispevku
 * @author pepa
 */
public class ContributionReviewerHeadPanel extends ContributionHeadPanel<ClientContributionReviewerHead> implements ArtefactEvaluationChanged.Handler {

    // logger
    private static Logger logger = LoggerFactory.getLogger(ContributionReviewerHeadPanel.class);

    public ContributionReviewerHeadPanel(ClientContributionReviewerHead cc, boolean onlyPublicComment) {
        super(cc, new HeadPanelReadInputFactory(false), onlyPublicComment);
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(ArtefactEvaluationChanged.TYPE, this);
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(ArtefactEvaluationChanged.TYPE, this);
    }

    @Override
    protected EvaluationItemValue partialItem(EvaluationValue value) {
        EvaluationItemValue eiv = super.partialItem(value);
        if (value.getUuid() != null) {
            eiv.setOnClickAction(scrollToReviewAction(value.getUuid()));
        }
        return eiv;
    }

    @Override
    protected void writeAdditionalInfo(Form form, ClientContributionReviewerHead headData) {
        if (StringUtils.isNotEmpty(headData.getReviewerName())) {
            Label reviewer = new Label(headData.getReviewerName());
            reviewer.setStyleName(ECResourceUtils.css().reviewer());
            form.add(ECMessageUtils.c.contributionReviewerHeadPanel_text_reviewer(), reviewer, false);
        }
    }

    @Override
    public void onArtefactEvaluationChanged(ArtefactEvaluationChanged event) {
        EvaluationDisplay display = getEvaluationDisplay();
        if (display != null) {
            // nastavit spravnemu ctverecku spravnou hodnotu
            logger.debug("artefact evaluation changed (" + event.getValue().getUuid() + ", " + event.getValue().getValue() + ")");
            display.update(partialItem(event.getValue()));
        }
    }

    private ConsysAction scrollToReviewAction(final String artefactUuid) {
        return new ConsysAction() {

            @Override
            public void run() {
                logger.debug("Scroll to review button: " + artefactUuid);
                EventBus.fire(new ScrollToReviewEvent(artefactUuid));
            }
        };
    }
}
