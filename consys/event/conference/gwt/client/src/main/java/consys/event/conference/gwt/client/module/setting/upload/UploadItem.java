package consys.event.conference.gwt.client.module.setting.upload;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.ListPanelItem;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction;
import consys.common.gwt.shared.bo.CommonThumb;

/**
 * Jedna uploadovana polozka
 * @author pepa
 */
public class UploadItem extends ListPanelItem<CommonThumb> {

    // konstanty
    public static final int HEIGHT = 20;
    // data
    private CommonThumb ct;
    private UploadArtefactDialog dialog;
    private SelectPage parent;

    public UploadItem(CommonThumb ct, UploadArtefactDialog dialog, SelectPage parent) {
        super();
        this.ct = ct;
        this.dialog = dialog;
        this.parent = parent;
    }

    @Override
    protected void onLoad() {
        create(getBody());
        if (!isSelected()) {
            showOddEven();
        }
    }

    @Override
    public void create(FlowPanel panel) {
        panel.clear();

        ActionLabel remove = new ActionLabel(UIMessageUtils.c.const_remove(), StyleUtils.FLOAT_RIGHT);
        remove.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // TODO pepa - rozlisovat medzi template a pattern
                throw new NullPointerException("Chyba - neni impleemtovane");
//                EventDispatchEvent removeEvent = new EventDispatchEvent(new DeleteUploadedArtefactTemplateAction(ct.getUuid()),
//                        new AsyncCallback<VoidResult>() {
//
//                            @Override
//                            public void onFailure(Throwable caught) {
//                                // obecne chyby obstarava EventActionExecutor
//                            }
//
//                            @Override
//                            public void onSuccess(VoidResult result) {
//                                parent.disableSelectButton();
//                                parent.clearHTML();
//                                parent.removeItem(getIndex());
//                            }
//                        }, dialog);
//                EventBus.get().fireEvent(removeEvent);
            }
        });
        
        panel.add(new Label(ct.getName()));
    }

    @Override
    public CommonThumb getObject() {
        return ct;
    }
}
