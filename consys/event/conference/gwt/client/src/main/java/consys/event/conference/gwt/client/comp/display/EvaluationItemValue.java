package consys.event.conference.gwt.client.comp.display;

import consys.common.gwt.client.action.ConsysAction;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;

/**
 * 
 * Hodnotenie. Ak je value -1 hodnotenie este nebolo zadane.
 *
 * @author pepa
 */
public class EvaluationItemValue {

    // konstanty
    private static final int TEN = 10;
    private static final int ZERO = 0;
    public static final int MAX_VALUE = TEN;
    // data        
    private String helpText;
    private EvaluationValue value;
    private int maxValue;
    private int integer;
    private int decimal;
    private boolean startFromOne;
    private String helpTitle;
    private ConsysAction onClickAction;

    public EvaluationItemValue() {
        this.value = new EvaluationValue();
    }

    public EvaluationItemValue(int value) {
        this(new EvaluationValue(value));
    }

    public EvaluationItemValue(EvaluationValue value) {
        this.value = value;
        if (value.isEntered()) {
            this.integer = value.getValue() / TEN;
            this.decimal = value.getValue() % TEN;
            this.maxValue = TEN;
        }
    }

    /**
     * @param helpTitle titulek helpu
     * @param helpText text helpu
     */
    public EvaluationItemValue(String helpTitle, String helpText) {
        this.value = new EvaluationValue();
        this.helpTitle = helpTitle;
        this.helpText = helpText;
    }

    /**
     * @param integer hodnota v rozsahu 0-10 (10 pouze v kombinaci s decimal == 0), je to cela cast hodnoceni
     * @param decimal procenta (hodnota v rozsahu 0-9), kriteria (0), je to desetinna cast hodnoceni
     * @param maxValue procenta (hodnota 10), kriteria (rozsah 2-10), pouziva se pro urceni barvicek podkladu
     * @param helpTitle titulek helpu
     * @param helpText text helpu
     */
    public EvaluationItemValue(int integer, int decimal, int maxValue, String helpTitle, String helpText) {
        this(integer, decimal, maxValue);
        this.helpTitle = helpTitle;
        this.helpText = helpText;
    }

    /**
     * @param integer hodnota v rozsahu 0-10 (10 pouze v kombinaci s decimal == 0), je to cela cast hodnoceni
     * @param decimal procenta (hodnota v rozsahu 0-9), kriteria (0), je to desetinna cast hodnoceni
     * @param maxValue procenta (hodnota 10), kriteria (rozsah 2-10), pouziva se pro urceni barvicek podkladu
     */
    public EvaluationItemValue(int integer, int decimal, int maxValue) {
        this.value = new EvaluationValue(integer * 10 + decimal);
        this.integer = integer;
        this.decimal = decimal;
        this.maxValue = maxValue;
    }

    public boolean isStartFromOne() {
        return startFromOne;
    }

    public void setStartFromOne(boolean startFromOne) {
        this.startFromOne = startFromOne;
    }

    public int getDecimal() {
        return decimal;
    }

    public int getInteger() {
        return integer;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public EvaluationValue getValue() {
        return value;
    }

    public boolean isNotEntered() {
        return !value.isEntered();
    }

    public boolean isNA() {
        return value.isNA();
    }

    public boolean isAutoaccepted() {
        return value.isAutoaccepted();
    }

    public boolean isInteger() {
        return decimal == ZERO;
    }

    public boolean isTen() {
        return integer == TEN;
    }

    public boolean isMaxValue() {
        return integer == maxValue;
    }

    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    public String getHelpTitle() {
        return helpTitle;
    }

    public void setHelpTitle(String helpTitle) {
        this.helpTitle = helpTitle;
    }

    public ConsysAction getOnClickAction() {
        return onClickAction;
    }

    public void setOnClickAction(ConsysAction onClickAction) {
        this.onClickAction = onClickAction;
    }
}
