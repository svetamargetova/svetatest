package consys.event.conference.gwt.client.utils;

import java.util.Set;

/**
 *
 * @author pepa
 */
public class CalculationUtils {

    /** vypocita procentualni cast ze zadane hodnoty a maximalni hodnoty */
    public static int percent(int value, int maxValue) {
        float result = (value / (float) maxValue) * 100;
        result = Math.min(result, 100f);
        return Math.round(result);
    }

    /** najde spolecneho jmenovatele */
    public static int mutualDenominator(Set<Integer> denominators) {
        int denominator = 1;
        for (Integer d : denominators) {
            if (d > denominator) {
                denominator *= d;
            } else if (d < denominator && denominator % d != 0) {
                denominator *= d;
            }
        }
        return denominator;
    }

    /** vrati prepocitaneho citatele ze spolecnoho jmenovatele */
    public static int numeratorFromMutualDenominator(int numerator, int denominator, int mutualDenominator) {
        if (denominator != mutualDenominator) {
            return numerator * (mutualDenominator / denominator);
        } else {
            return numerator;
        }
    }
}
