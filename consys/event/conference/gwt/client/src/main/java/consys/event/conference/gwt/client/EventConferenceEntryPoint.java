package consys.event.conference.gwt.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.rpc.action.LoadUserInfoAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.list.ListDataSourceRequest;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.common.gwt.client.utils.TimeZoneProvider;
import consys.common.gwt.shared.bo.*;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.client.module.EventCoreModule;
import consys.event.common.gwt.client.module.OverseerModule;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.gwt.client.action.chair.*;
import consys.event.conference.gwt.client.action.contribution.*;
import consys.event.conference.gwt.client.action.reviewer.*;
import consys.event.conference.gwt.client.action.setting.CreateContributionAction;
import consys.event.conference.gwt.client.action.setting.*;
import consys.event.conference.gwt.client.bo.*;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead;
import consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair;
import consys.event.conference.gwt.client.bo.contribution.*;
import consys.event.conference.gwt.client.bo.evaluation.Evaluation;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData;
import consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.bo.reviewer.*;
import consys.event.conference.gwt.client.bo.setting.*;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.client.comp.*;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.comp.evaluator.ClientPreview;
import consys.event.conference.gwt.client.comp.evaluator.ClientPreviews;
import consys.event.conference.gwt.client.module.ConferenceModule;
import consys.event.conference.gwt.client.module.chair.AssignmentReviewersForm;
import consys.event.conference.gwt.client.module.chair.ChairContributionsList;
import consys.event.conference.gwt.client.module.chair.ContributionChairForm;
import consys.event.conference.gwt.client.module.contribution.item.ContributionHeadPanel;
import consys.event.conference.gwt.client.module.contribution.item.HeadPanelReadInputFactory;
import consys.event.conference.gwt.client.module.contribution.item.HeadPanelUpdateInputFactory;
import consys.event.conference.gwt.client.module.contributor.MyContributionList;
import consys.event.conference.gwt.client.module.contributor.NewContributionForm;
import consys.event.conference.gwt.client.module.reviewer.BidForReviewForm;
import consys.event.conference.gwt.client.module.reviewer.ContributionReviewerForm;
import consys.event.conference.gwt.client.module.reviewer.MyReviewsList;
import consys.event.conference.gwt.client.module.reviewer.item.ReviewerEvaluation;
import consys.event.conference.gwt.client.module.setting.ContributionTypeContent;
import java.util.*;

/**
 *
 * @author pepa
 */
public class EventConferenceEntryPoint extends DebugModuleEntryPoint {

    // data
    private ClientContributionHeadData contri = new ClientContributionHeadData();

    @Override
    public void initMocks() {
        ClientCurrentEvent cce = new ClientCurrentEvent();
        cce.setOverseerUrl("http://overseer");
        ClientEvent event = new ClientEvent();
        event.setUuid("234234");
        cce.setEvent(event);
        Cache.get().register(CurrentEventCacheAction.CURRENT_EVENT, cce);

        ClientUser cu = new ClientUser();
        cu.setUuid("23324AAA");
        Cache.get().register(UserCacheAction.USER, cu);
        // --------------
        ArrayListResult<ClientSettingsArtefactType> cartefactTypeResult = new ArrayListResult<ClientSettingsArtefactType>();
        ClientSettingsArtefactType artefactType1c = new ClientSettingsArtefactType();
        artefactType1c.setArtefactTypeUuid("123");
        artefactType1c.setArtefactTypeName("Talk");
        artefactType1c.setNumReviewers(2);
        artefactType1c.setDataType(ClientContributionArtefactDataType.FILE);
        artefactType1c.setRequired(true);

        ClientSettingsArtefactType artefactType2c = new ClientSettingsArtefactType();
        artefactType2c.setArtefactTypeUuid("12333");
        artefactType2c.setArtefactTypeName("Poster");
        artefactType2c.setNumReviewers(4);
        artefactType2c.setDataType(ClientContributionArtefactDataType.TEXT);
        artefactType2c.setRequired(false);

        cartefactTypeResult.getArrayListResult().add(artefactType1c);
        cartefactTypeResult.getArrayListResult().add(artefactType2c);

        // --------------
        /**
         * SelectBoxItem sectionBoxItem1 = new SelectBoxItem();
         * sectionBoxItem1.setItem("uuid1"); sectionBoxItem1.setArtefactTypeName("Červená
         * sekce"); SelectBoxItem sectionBoxItem2 = new SelectBoxItem();
         * sectionBoxItem2.setItem("uuid2"); sectionBoxItem2.setArtefactTypeName("Zelená
         * sekce"); ArrayList<SelectBoxItem> sectionBoxItemList = new
         * ArrayList<SelectBoxItem>(); sectionBoxItemList.add(sectionBoxItem1);
         * sectionBoxItemList.add(sectionBoxItem2); SelectBoxResult
         * sectionBoxResult = new SelectBoxResult(sectionBoxItemList);
         * MockFactory.addActionMock(ListSectionAction.class, new
         * ActionMock<SelectBoxResult>(sectionBoxResult));
         */
        // -------------- Seznam topiku
        ArrayListResult<ClientTopic> topicResult = new ArrayListResult<ClientTopic>();
        ClientTopic topic1 = new ClientTopic();
        topic1.setId(1l);
        topic1.setName("Čistá voda");
        topicResult.getArrayListResult().add(topic1);
        ClientTopic topic2 = new ClientTopic();
        topic2.setId(2l);
        topic2.setName("Hořlaviny a výbušniny");
        topicResult.getArrayListResult().add(topic2);
        ClientTopic topic3 = new ClientTopic();
        topic3.setId(3l);
        topic3.setName("Výpočty");
        topicResult.getArrayListResult().add(topic3);
        ClientTopic topic4 = new ClientTopic();
        topic4.setId(4l);
        topic4.setName("Vizualizace");
        topicResult.getArrayListResult().add(topic4);
        ClientTopic topic5 = new ClientTopic();
        topic5.setId(5l);
        topic5.setName("Inovace v škudlení");
        topicResult.getArrayListResult().add(topic5);
        ClientTopic topic6 = new ClientTopic();
        topic6.setId(6l);
        topic6.setName("Průzkum vesmíru");
        topicResult.getArrayListResult().add(topic6);
        ClientTopic topic7 = new ClientTopic();
        topic7.setId(7l);
        topic7.setName("Paralelnost");
        topicResult.getArrayListResult().add(topic7);
        ClientTopic topic8 = new ClientTopic();
        topic8.setId(8l);
        topic8.setName("Development - Java");
        topicResult.getArrayListResult().add(topic8);
        ClientTopic topic9 = new ClientTopic();
        topic9.setId(9l);
        topic9.setName("Development - .Net");
        topicResult.getArrayListResult().add(topic9);
        MockFactory.addActionMock(ListTopicAction.class, new ActionMock<ArrayListResult<ClientTopic>>(topicResult));
        // -------------- Vytvoreni odeslaneho prispevku
        MockFactory.addActionMock(CreateContributionAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // -------------- Zmena pozice submissionu
        MockFactory.addActionMock(UpdateContributionTypePositionAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // -------------- Inicialni nacteni nastaveni
        ArrayListResult clientSubmissionTypeResult = new ArrayListResult();
        ClientContributionType submissionType1 = new ClientContributionType();
        submissionType1.setUuid("1235jedna");
        submissionType1.setName("submission type 1");
        submissionType1.setDescription("popisek k submission type 1");
        ClientArtefactTypeThumb artefactType1 = new ClientArtefactTypeThumb();
        artefactType1.setArtefactTypeUuid("434635345");
        artefactType1.setArtefactTypeName("ArtefactType 1 thumb ArtefactType 1 thumb ArtefactType 1 thumb ArtefactType 1 thumb ArtefactType 1 thumb ArtefactType 1 thumb");
        ClientArtefactTypeThumb artefactType2 = new ClientArtefactTypeThumb();
        artefactType2.setArtefactTypeUuid("4346353452");
        artefactType2.setArtefactTypeName("ArtefactType 2 thumb");
        ArrayList<ClientArtefactTypeThumb> artefactList = new ArrayList<ClientArtefactTypeThumb>();
        artefactList.add(artefactType1);
        artefactList.add(artefactType2);
        submissionType1.setClientArtefactTypeList(artefactList);
        ArrayList<ClientReviewCycleThumb> reviewCycleList = new ArrayList<ClientReviewCycleThumb>();
        ClientReviewCycleThumb clientReviewCycleNewThumb = new ClientReviewCycleThumb();
        clientReviewCycleNewThumb.setSubmitFrom(new Date());
        clientReviewCycleNewThumb.setSubmitTo(new Date());
        reviewCycleList.add(clientReviewCycleNewThumb);
        submissionType1.setClientReviewCycleList(reviewCycleList);
        clientSubmissionTypeResult.getArrayListResult().add(submissionType1);
        ClientContributionType submissionType2 = new ClientContributionType();
        submissionType2.setUuid("1235dva");
        submissionType2.setName("submission type 2");
        submissionType2.setDescription("popisek k submission type 2");
        clientSubmissionTypeResult.getArrayListResult().add(submissionType2);
        MockFactory.addActionMock(LoadContributionTypeAction.class,
                new ActionMock<ClientContributionType>(submissionType1));

        ArrayListResult settings = new ArrayListResult();
        CommonThumb setting1 = new CommonThumb();
        setting1.setUuid("1235jedna");
        setting1.setName("setting thumb 1");
        CommonThumb setting2 = new CommonThumb();
        setting2.setUuid("1235dva");
        setting2.setName("setting thumb 2");
        settings.getArrayListResult().add(setting1);
        settings.getArrayListResult().add(setting2);
        MockFactory.addActionMock(LoadContributionSettingAction.class,
                new ActionMock<ArrayListResult<CommonThumb>>(settings));
        // -------------- Vytvoreni typu prispevku
        ArrayListResult<String> contributionTypeResult = new ArrayListResult<String>();
        contributionTypeResult.getArrayListResult().add("34243");
        contributionTypeResult.getArrayListResult().add("3dfa3");
        MockFactory.addActionMock(CreateContributionTypeAction.class,
                new ActionMock<ArrayListResult<String>>(contributionTypeResult));
        // -------------- Vytvoreni typu artefaktu
        ArrayListResult<String> artefactTypeResult = new ArrayListResult<String>();
        artefactTypeResult.getArrayListResult().add("34423243");
        artefactTypeResult.getArrayListResult().add("3456dfa3");
        MockFactory.addActionMock(CreateArtefactTypeAction.class,
                new ActionMock<ArrayListResult<String>>(artefactTypeResult));
        // -------------- Vytvoreni kolecka hodnoceni
        ArrayListResult<Long> reviewCycleResult = new ArrayListResult<Long>();
        reviewCycleResult.getArrayListResult().add(3l);
        reviewCycleResult.getArrayListResult().add(4l);
        MockFactory.addActionMock(CreateReviewCycleAction.class,
                new ActionMock<ArrayListResult<Long>>(reviewCycleResult));
        // -------------- Vytvoreni kriteria
        ArrayListResult<Long> criteriaResult = new ArrayListResult<Long>();
        criteriaResult.getArrayListResult().add(1l);
        criteriaResult.getArrayListResult().add(2l);
        MockFactory.addActionMock(CreateCriteriaAction.class,
                new ActionMock<ArrayListResult<Long>>(criteriaResult));
        // -------------- Aktualizace typu prispevku
        MockFactory.addActionMock(UpdateContributionTypeAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // -------------- Aktualizace typu artefaktu
        MockFactory.addActionMock(UpdateArtefactTypeAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // -------------- Aktualizace kolecka hodnoceni
        MockFactory.addActionMock(UpdateReviewCycleAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // -------------- Aktualizace kriteria
        MockFactory.addActionMock(UpdateCriteriaAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // -------------- Smazani typu prispevku
        MockFactory.addActionMock(DeleteContributionTypeAction.class, new ActionMock<VoidResult>(new ServiceFailedException()));
        // -------------- Smazani typu artefaktu
        MockFactory.addActionMock(DeleteArtefactTypeAction.class, new ActionMock<VoidResult>(new ServiceFailedException()));
        // -------------- Smazani kolecka hodnoceni
        MockFactory.addActionMock(DeleteReviewCycleAction.class, new ActionMock<VoidResult>(new ServiceFailedException()));
        // -------------- Smazani kriteria
        MockFactory.addActionMock(DeleteCriteriaAction.class, new ActionMock<VoidResult>(new ServiceFailedException()));
        // -------------- Nacteni kompletnich dat kolecka hodnoceni
        ClientSettingsReviewCycle clientReviewCycleNew = new ClientSettingsReviewCycle();
        clientReviewCycleNew.setReviewFrom(new Date(1317600000000l)); // 3.10.2011 00:00 gmt --> event v europe/london (leto +1)
        clientReviewCycleNew.setReviewTo(new Date(1317942000000l)); // 6.10.2011 23:00 gmt --> event v europe/london (leto +1)
        clientReviewCycleNew.setSubmitFrom(new Date(1317600000000l));
        clientReviewCycleNew.setSubmitTo(new Date(1317942000000l));
        ArrayList<String> assignedArtefacts = new ArrayList<String>();
        assignedArtefacts.add("434635345");
        clientReviewCycleNew.setAssignedArtefacts(assignedArtefacts);
        MockFactory.addActionMock(LoadReviewCycleAction.class, new ActionMock<ClientSettingsReviewCycle>(clientReviewCycleNew));
        // -------------- Nacteni kompletnich dat kriteria
        ClientCriteria criteriaNew = new ClientCriteria();
        criteriaNew.setName("Criteria New");
        criteriaNew.setDescription("Criteria desription New");
        criteriaNew.setMinimumPoints(2);
        criteriaNew.getPoints().add(new ClientCriteriaItem(1L, "Slabota"));
        criteriaNew.getPoints().add(new ClientCriteriaItem(1L, "Nic moc"));
        criteriaNew.getPoints().add(new ClientCriteriaItem(1L, "Průměrné"));
        criteriaNew.getPoints().add(new ClientCriteriaItem(1L, "Ujde to"));
        criteriaNew.getPoints().add(new ClientCriteriaItem(1L, "Super"));
        MockFactory.addActionMock(LoadCriteriaAction.class, new ActionMock<ClientCriteria>(criteriaNew));
        // -------------- Nacteni kompletnich dat artefaktu (kriteria a kolecka jsou seznamy nahledu)
        ClientSettingsArtefactType artefactTypeNew = new ClientSettingsArtefactType();
        artefactTypeNew.setArtefactTypeUuid("434635345");
        artefactTypeNew.setNumReviewers(3);
        artefactTypeNew.setArtefactTypeName("ArtefactType New");
        artefactTypeNew.setArtefactTypeDescription("description artefact type New");
        artefactTypeNew.setRequired(true);
        artefactTypeNew.setDataType(ClientContributionArtefactDataType.FILE);
        artefactTypeNew.setFileFormats("pdf");
        artefactTypeNew.setMaxInputLength(255);
        ClientCriteriaThumb criteriaNewThumb = new ClientCriteriaThumb();
        criteriaNewThumb.setName("Criteria New thumb");
        ArrayList<ClientCriteriaThumb> criteriasNew = new ArrayList<ClientCriteriaThumb>();
        criteriasNew.add(criteriaNewThumb);
        artefactTypeNew.setCriteriaList(criteriasNew);
        artefactTypeNew.getUploadedExamples().add(new CommonThumb("1444", "Příklad 1 s trochu delším názvem"));
        MockFactory.addActionMock(LoadArtefactTypeAction.class, new ActionMock<ClientSettingsArtefactType>(artefactTypeNew));
        //------------------------------------------------------------------------
        // LOAD USER TOPICS
        //------------------------------------------------------------------------
        UserTopicsResult utr = new UserTopicsResult();
        utr.getSelectedTopics().add(1L);
        utr.getSelectedTopics().add(3L);

        for (int i = 0; i < 20; i++) {
            ClientTopic ct = new ClientTopic();
            ct.setId((long) i);
            ct.setName(i + " topik");
            utr.getTopics().add(ct);
        }
        MockFactory.addActionMock(ListReviewerPreferredTopics.class, new ActionMock<UserTopicsResult>(utr));
        //------------------------------------------------------------------------
        // UPDATE USER TOPICS
        //------------------------------------------------------------------------
        MockFactory.addActionMock(UpdateReviewerTopicsAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //------------------------------------------------------------------------
        // NACTENI DAT K HODNOCENI PRISPEVKU PRO RECENZENTA
        //------------------------------------------------------------------------
        ClientContributionReview eval = new ClientContributionReview();

        ClientReviewFormArtefactEvaluation cae = new ClientReviewFormArtefactEvaluation();
        cae.setPrivateNote("soukromá poznámka");
        cae.setPublicNote("veřejná poznámka");

        Evaluation<EvaluationValue, EvaluationValue> contriEval = new Evaluation<EvaluationValue, EvaluationValue>();
        contriEval.setOverallScore(new EvaluationValue(30));
        contriEval.getPartialScores().add(new EvaluationValue("345252526784", 30));
        contriEval.getPartialScores().add(new EvaluationValue("3452525267845", 30));
        contriEval.getPartialScores().add(new EvaluationValue());

        /*ClientContributionHeadData contri = new ClientContributionHeadData();
         contri.setAuthors("Autor Jedna, Autor Dva, Autor Tři");
         contri.setSubtitle("Podtitulek příspěvku");
         contri.setTitle("Dvacet minut do konce");
         contri.setTopics("Topic 1, Topic 1, Topic 1, Topic 1, Topic 1, Topic 1, Topic 1");
         contri.setUuid("3492JRJ");
         contri.setEvaluations(contriEval);*/

        eval.setContributionHead(contri);

        ClientReviewFormCycleArtefact crfca1 = new ClientReviewFormCycleArtefact();
        crfca1.setAlreadyReviewed(false);
        crfca1.setArtefactTypeName("Půlhodinový talk");
        crfca1.setArtefactUuid("124");

        ClientReviewFormCycleArtefact crfca2 = new ClientReviewFormCycleArtefact();
        crfca2.setAlreadyReviewed(true);
        crfca2.setArtefactTypeName("Hodinový talk");
        crfca2.setArtefactUuid("1243");

        ClientReviewFormCycle crfc1 = new ClientReviewFormCycle();
        crfc1.setCanReview(true);
        crfc1.setReviewFrom(new Date());
        crfc1.setReviewTo(new Date());
        crfc1.getArtefacts().add(crfca1);
        crfc1.getArtefacts().add(crfca2);

        ClientReviewFormCycleArtefact crfca3 = new ClientReviewFormCycleArtefact();
        crfca3.setAlreadyReviewed(false);
        crfca3.setArtefactTypeName("Poster");
        crfca3.setArtefactUuid(null);

        ClientReviewFormCycle crfc2 = new ClientReviewFormCycle();
        crfc2.setCanReview(false);
        crfc2.setReviewFrom(new Date());
        crfc2.setReviewTo(new Date());
        crfc2.getArtefacts().add(crfca3);

        eval.getCycles().add(crfc1);
        eval.getCycles().add(crfc2);

        ArrayList<ClientCriteria> criterias = cae.getCriteria();

        ClientCriteria mcc1 = new ClientCriteria();
        mcc1.setId(1l);
        mcc1.setName("Kriterium 1");
        mcc1.setMinimumPoints(1);
        mcc1.setDescription("Popisek ke kriteriu jedna");
        mcc1.getPoints().add(new ClientCriteriaItem(1L, "yes"));
        mcc1.getPoints().add(new ClientCriteriaItem(1L, "no"));

        ClientCriteria mcc2 = new ClientCriteria();
        mcc2.setId(2l);
        mcc2.setName("Kriterium 2");
        mcc2.setMinimumPoints(2);
        mcc2.setDescription("Popisek ke kriteriu dva");
        mcc2.getPoints().add(new ClientCriteriaItem(1L, "yes"));
        mcc2.getPoints().add(new ClientCriteriaItem(1L, "no"));
        mcc2.getPoints().add(new ClientCriteriaItem(1L, "maybe"));

        ClientCriteria mcc3 = new ClientCriteria();
        mcc3.setId(3l);
        mcc3.setName("Kriterium 3");
        mcc3.setMinimumPoints(3);
        mcc3.setDescription("Popisek ke kriteriu tri");
        mcc3.getPoints().add(new ClientCriteriaItem(1L, "dif1"));
        mcc3.getPoints().add(new ClientCriteriaItem(1L, "dif2"));
        mcc3.getPoints().add(new ClientCriteriaItem(1L, "dif3"));
        mcc3.getPoints().add(new ClientCriteriaItem(1L, "dif4"));
        mcc3.getPoints().add(new ClientCriteriaItem(1L, "dif5"));


        ClientCriteria mcc4 = new ClientCriteria();
        mcc4.setId(4l);
        mcc4.setName("Kriterium 4");
        mcc4.setMinimumPoints(2);
        mcc4.setDescription("Popisek ke kriteriu ctyri");
        mcc4.getPoints().add(new ClientCriteriaItem(1L, "dif1"));
        mcc4.getPoints().add(new ClientCriteriaItem(1L, "dif2"));
        mcc4.getPoints().add(new ClientCriteriaItem(1L, "dif3"));
        mcc4.getPoints().add(new ClientCriteriaItem(1L, "dif5"));
        mcc4.getPoints().add(new ClientCriteriaItem(1L, "dif4"));

        ClientCriteria mcc5 = new ClientCriteria();
        mcc5.setId(5l);
        mcc5.setName("Kriterium 5");
        mcc5.setMinimumPoints(1);
        mcc5.setDescription("Popisek ke kriteriu pet");
        mcc5.getPoints().add(new ClientCriteriaItem(1L, "yes"));
        mcc5.getPoints().add(new ClientCriteriaItem(1L, "no"));


        ClientCriteria mcc6 = new ClientCriteria();
        mcc6.setId(6l);
        mcc6.setName("Kriterium 6");
        mcc6.setMinimumPoints(3);
        mcc6.setDescription("Popisek ke kriteriu sest");
        mcc6.getPoints().add(new ClientCriteriaItem(1L, "dif1"));
        mcc6.getPoints().add(new ClientCriteriaItem(1L, "dif2"));
        mcc6.getPoints().add(new ClientCriteriaItem(1L, "dif3"));
        mcc6.getPoints().add(new ClientCriteriaItem(1L, "dif4"));
        mcc6.getPoints().add(new ClientCriteriaItem(1L, "dif5"));

        criterias.add(mcc1);
        criterias.add(mcc2);
        criterias.add(mcc3);
        criterias.add(mcc4);
        criterias.add(mcc5);
        criterias.add(mcc6);

        HashMap<Long, Integer> hm = cae.getEvaluation();
        hm.put(1l, 2);
        //hm.put(2l, 3);
        hm.put(3l, 2);
        hm.put(4l, 3);
        hm.put(5l, 1);
        hm.put(6l, 5);


        //------------------------------------------------------------------------
        // ODESLANI HODNOCENI RECENZENTA
        //------------------------------------------------------------------------
        MockFactory.addActionMock(UpdateContributionArtefactEvaluationAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //------------------------------------------------------------------------
        // NACTENI DAT K HODNOCENI PRISPEVKU PRO CHAIRA
        //------------------------------------------------------------------------
        ClientContributionEvaluationChair evalc = new ClientContributionEvaluationChair();

        ArrayList<ClientCriteriaThumb> criteriasThumbs = new ArrayList<ClientCriteriaThumb>();
        for (ClientCriteria cc : criterias) {
            ClientCriteriaThumb t = new ClientCriteriaThumb();
            t.setId(cc.getId());
            t.setMaxPoints(cc.getMaxPoints());
            t.setMinimumPoints(cc.getMinimumPoints());
            t.setName(cc.getName());
            criteriasThumbs.add(t);
        }


        ClientContributionHeadData mcc = new ClientContributionHeadData();
        mcc.setAuthors("Autor 1, Autor 2, Autor 3");
        mcc.setContributors(new ArrayList<ClientContributor>());
        mcc.setSubtitle("aaa");
        mcc.setTitle("Contribution title");
        mcc.setTopics("Topic 1 , Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2, Topic 2");
        mcc.setUuid("12314");
        evalc.setContributionHead(mcc);

        //------------------------------------------------------------------------
        // ROZHODNUTI O PRIJETI/NEPRIJETI PRISPEVKU
        //------------------------------------------------------------------------
        MockFactory.addActionMock(ContributionStateAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //------------------------------------------------------------------------
        // NACTENI PRIRAZENI RECENZENTU K PRISPEVKU
        //------------------------------------------------------------------------
        ClientContributionHeadData clientContribution = new ClientContributionHeadData();
        clientContribution.setTitle("Příspěvek k tématu podvodní mlýn");
        clientContribution.setAuthors("Tomáš Staudek, Jaroslav Škrabálek");
        clientContribution.setTopics(topic3.getName() + ", " + topic5.getName() + ", " + topic6.getName() + ", " + topic7.getName());
        clientContribution.setState(ClientContributionStateEnum.PENDING);
        ArrayList<ClientTopic> topics = new ArrayList<ClientTopic>();

        topics.add(topic3);
        topics.add(topic5);
        topics.add(topic6);
        topics.add(topic7);

        ArrayList<ClientReviewer> reviewers = new ArrayList<ClientReviewer>();

        ClientReviewer cr1 = new ClientReviewer();
        cr1.setUuid("123");
        cr1.setName("David Grešek");
        cr1.setLoad(5);
        cr1.setMaxLoad(10);
        cr1.getTopics().add(topic7);
        cr1.getTopics().add(topic3);
        cr1.getTopics().add(topic4);
        cr1.setInterest(ReviewerInterestEnum.LIKE);
        reviewers.add(cr1);
        ClientReviewer cr2 = new ClientReviewer();
        cr2.setUuid("1234");
        cr2.setReviewing(true);
        cr2.setName("Drahomíra Bémová");
        cr2.setLoad(2);
        cr2.setMaxLoad(5);
        cr2.getTopics().add(topic2);
        cr2.getTopics().add(topic1);
        cr2.getTopics().add(topic4);
        cr2.setInterest(ReviewerInterestEnum.NOT_WANT);
        reviewers.add(cr2);
        ClientReviewer cr3 = new ClientReviewer();
        cr3.setUuid("12345");
        cr3.setName("Marie Čadková");
        cr3.setLoad(6);
        cr3.setMaxLoad(14);
        cr3.getTopics().add(topic2);
        cr3.getTopics().add(topic1);
        cr3.getTopics().add(topic4);
        cr3.getTopics().add(topic5);
        cr3.setInterest(ReviewerInterestEnum.NOT_WANT);
        reviewers.add(cr3);

        ClientAssignReviewers car = new ClientAssignReviewers();
        car.setContributionHead(clientContribution);
        car.setReviewers(reviewers);
        car.setTopics(topics);
        MockFactory.addActionMock(ListContributionReviewersBidAction.class, new ActionMock<ClientAssignReviewers>(car));
        // ---------------------------------------------------------------------------------------
        MockFactory.addActionMock(ContributionReviewerAssignmentAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ---------------------------------------------------------------------------------------
        LongResult createTopicAction = new LongResult(4);
        MockFactory.addActionMock(CreateTopicAction.class, new ActionMock<LongResult>(createTopicAction));
        // ---------------------------------------------------------------------------------------
        MockFactory.addActionMock(DeleteTopicAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ---------------------------------------------------------------------------------------
        MockFactory.addActionMock(UpdateTopicAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ---------------------------------------------------------------------------------------

        // ---------------------------------------------------------------------------------------
        ArrayListResult<CommonThumb> cts = new ArrayListResult<CommonThumb>();
        cts.getArrayListResult().add(new CommonThumb("34231", "název souboru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34232", "název souboru dva"));
        cts.getArrayListResult().add(new CommonThumb("34233", "název dfa souboru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34234", "název soubdasffadf  oru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34234", "název soubdasffadf  oru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34234", "název soubdasffadf  oru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34234", "název soubdasffadf  oru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34234", "název soubdasffadf  oru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34234", "název soubdasffadf  oru jedna"));
        cts.getArrayListResult().add(new CommonThumb("34234", "název soubdasffadf  oru jedna"));
        MockFactory.addActionMock(ListUploadedArtefactTemplateAction.class, new ActionMock<ArrayListResult<CommonThumb>>(cts));
        // ---------------------------------------------------------------------------------------
        ClientUploadedArtefactTemplate uat = new ClientUploadedArtefactTemplate();
        uat.setUuid("2342");
        uat.setName("test_name_file");
        uat.setDescription("a dfaasdf df jfa afj aůs fdasf  afj asdjf adjf adfsj asdfkzrqpwzre fhqpfh dgfa f.gkjhjfha ae rparzapr arqewrzrpeqwr šá afj fapefz");
        MockFactory.addActionMock(LoadUploadedArtefactTemplateAction.class, new ActionMock<ClientUploadedArtefactTemplate>(uat));
        // ---------------------------------------------------------------------------------------
        MockFactory.addActionMock(DeleteUploadedArtefactTemplateAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ---------------------------------------------------------------------------------------
        // -- NOVY PRISPEVEK
        // ---------------------------------------------------------------------------------------

        ClientNewContributionActionData csct = new ClientNewContributionActionData();
        csct.setContributionUuid("123");
        csct.setContributionTypeUuid("21341");

        ClientContributionTypeThumb stt1 = new ClientContributionTypeThumb();
        stt1.setUuid("2134");
        stt1.setName("Paper");
        stt1.setDescription("Paper abou one thing");

        ClientContributionTypeThumb stt2 = new ClientContributionTypeThumb();
        stt2.setUuid("21341");
        stt2.setName("Short Paper");
        stt2.setDescription("Paper abou one thing");

        ClientContributionTypeThumb stt3 = new ClientContributionTypeThumb();
        stt3.setUuid("21341");
        stt3.setName("Challange in doing the best");
        stt3.setDescription("Paper abou one thing");

        ClientContributionTypeThumb stt4 = new ClientContributionTypeThumb();
        stt4.setUuid("21341a");
        stt4.setName("Něco delšího, aby se to zalomilo na další řádek");
        stt4.setDescription("Popisek delšího popisku");

        csct.getTypes().add(stt1);
        csct.getTypes().add(stt2);
        csct.getTypes().add(stt3);
        //csct.getTypes().add(stt4);

        ClientArtefactType csat1 = new ClientArtefactType();
        csat1.setArtefactTypeDescription("csat1 description a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef");
        csat1.setDataType(ClientContributionArtefactDataType.FILE);
        csat1.setFileFormats("pdf");
        csat1.setMaxInputLength(10);
        csat1.setArtefactTypeName("Abstract");
        csat1.setRequired(true);
        csat1.setArtefactTypeUuid("W23");
        csat1.getUploadedExamples().add(new CommonThumb("123", "příklad"));
        csat1.getUploadedTemplates().add(new CommonThumb("1231", "šablona"));

        ClientArtefactType csat2 = new ClientArtefactType();
        csat2.setArtefactTypeDescription("csat2 description a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef");
        csat2.setDataType(ClientContributionArtefactDataType.TEXT);
        csat2.setFileFormats("jpg");
        csat2.setMaxInputLength(0);
        csat2.setArtefactTypeName("Keywords");
        csat2.setRequired(true);
        csat2.setArtefactTypeUuid("W231");

        ClientArtefactType csat3 = new ClientArtefactType();
        csat3.setArtefactTypeDescription("csat2 description a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef");
        csat3.setDataType(ClientContributionArtefactDataType.FILE);
        csat3.setFileFormats("ppt,pptx,pdf");
        csat3.setMaxInputLength(0);
        csat3.setArtefactTypeName("Presentation");
        csat3.setRequired(false);
        csat3.setArtefactTypeUuid("W231");

        ClientArtefactType csat4 = new ClientArtefactType();
        csat4.setArtefactTypeDescription("csat2 description a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef a je  fdaj dfůa daůj jazizv zasf dpzasdf sadfasf asdfasdfasdf as gtaf agef");
        csat4.setDataType(ClientContributionArtefactDataType.TEXT);
        csat4.setFileFormats("ppt,pptx,pdf");
        csat4.setMaxInputLength(0);
        csat4.setArtefactTypeName("Camera ready paper");
        csat4.setRequired(false);
        csat4.setArtefactTypeUuid("W231");


        ClientReviewCycle rc1 = new ClientReviewCycle();
        rc1.setEditable(true);
        rc1.setFrom(new Date());
        rc1.setTo((new Date(rc1.getFrom().getTime() + (1000 * 60 * 60 * 24 * 10))));
        //rc1.getArtefacts().add(csat1);
        //rc1.getArtefacts().add(csat2);


        ClientReviewCycle rc2 = new ClientReviewCycle();
        rc2.setEditable(false);
        rc2.setFrom(rc1.getTo());
        rc2.setTo((new Date(rc2.getFrom().getTime() + (1000 * 60 * 60 * 24 * 10))));
        rc2.getArtefacts().add(csat3);
        rc2.getArtefacts().add(csat4);

        ClientReviewCycle rc3 = new ClientReviewCycle();
        rc3.setEditable(false);
        rc3.setFrom(rc1.getTo());
        rc3.setTo((new Date(rc2.getFrom().getTime() + (1000 * 60 * 60 * 24 * 10))));
        rc3.getArtefacts().add(csat4);

        csct.getSelectedCycles().add(rc1);
        csct.getSelectedCycles().add(rc2);
        MockFactory.addActionMock(LoadNewContributionAction.class, new ActionMock<ClientNewContributionActionData>(csct));


        ArrayList<ClientReviewCycle> out = new ArrayList<ClientReviewCycle>();
        out.add(rc1);
        out.add(rc2);
        out.add(rc3);
        MockFactory.addActionMock(LoadContributionTypeCyclesAction.class, new ActionMock<ArrayListResult<ClientReviewCycle>>(new ArrayListResult<ClientReviewCycle>(out)));
        // ---------------------------------------------------------------------------------------

        // ---------------------------------------------------------------------------------------
        ArrayListResult<Long> longs = new ArrayListResult<Long>();
        longs.getArrayListResult().add(3l);
        longs.getArrayListResult().add(5l);
        MockFactory.addActionMock(ListReviewerPreferredTopicsIdsAction.class, new ActionMock<ArrayListResult<Long>>(longs));
        // ---------------------------------------------------------------------------------------
        MockFactory.addActionMock(UpdateContributionCommentAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ---------------------------------------------------------------------------------------
        ArrayList<ClientListContributionItem> items = new ArrayList<ClientListContributionItem>();

        ClientListContributionItem i1 = new ClientListContributionItem();
        i1.setOverview(contri);
        i1.setUuid("4323432342");

        ClientListContributionItem i2 = new ClientListContributionItem();
        i2.setOverview(contri);
        i2.setUuid("432342");

        items.add(i1);
        items.add(i2);

        ArrayList<ClientListContributionItem> items2 = new ArrayList<ClientListContributionItem>();
        ClientListContributionItem i3 = new ClientListContributionItem();
        i3.setOverview(contri);
        items2.add(i3);
        items2.add(i3);

        ArrayList<ClientListBidForReviewItem> items3 = new ArrayList<ClientListBidForReviewItem>();
        ClientListBidForReviewItem itm1 = new ClientListBidForReviewItem();
        itm1.setUuid("itm1");
        itm1.setArtefacts(new ArrayList<CommonThumb>());
        itm1.setAuthors("autoři 1");
        itm1.setIntereset(ReviewerInterestEnum.LIKE);
        itm1.setSubtitle(null);
        itm1.setTitle("Název trochu delší ať je vidět špatné natažení pravé části, která se musí natahovat podle výšky buňky");
        itm1.setTopics(topics);
        items3.add(itm1);
        ClientListBidForReviewItem itm2 = new ClientListBidForReviewItem();
        itm2.setUuid("itm2");
        itm2.setArtefacts(new ArrayList<CommonThumb>());
        itm2.setAuthors("autoři 2");
        itm2.setIntereset(ReviewerInterestEnum.NOT_CARE);
        itm2.setSubtitle(null);
        itm2.setTitle("Název trochu delší ať je vidět špatné natažení pravé části, která se musí natahovat podle výšky buňky Název trochu delší ať je vidět špatné natažení pravé části, která se musí natahovat podle výšky buňky Název trochu delší ať je vidět špatné natažení pravé části, která se musí natahovat podle výšky buňky");
        itm2.setTopics(topics);
        items3.add(itm2);
        ClientListBidForReviewItem itm3 = new ClientListBidForReviewItem();
        itm3.setUuid("itm3");
        itm3.setArtefacts(new ArrayList<CommonThumb>());
        itm3.setAuthors("autoři 3");
        itm3.setIntereset(ReviewerInterestEnum.NOT_WANT);
        itm3.setSubtitle(null);
        itm3.setTitle("Název trochu delší ať je vidět špatné natažení pravé části, která se musí natahovat podle výšky buňky");
        itm3.setTopics(topics);
        items3.add(itm3);

        ArrayList<ClientListContributionItem> items4 = new ArrayList<ClientListContributionItem>();

        ClientListContributionItem itmmr1 = new ClientListContributionItem();
        itmmr1.setOverview(contri);
        items4.add(itmmr1);

        ListDataSourceResult ldsr = new ListDataSourceResult(items, 1);
        MockFactory.addActionMock(ListDataSourceRequest.class, new ActionMock<ListDataSourceResult>(ldsr));
        // ---------------------------------------------------------------------------------------

        ClientUserCommonInfo cui = new ClientUserCommonInfo();
        cui.setFullName("Lojzin Statečný");
        cui.setPosition("Popelář");
        cui.setOrganization(".A.S.A.");
        //cui.setImageUuid("32424");
        cui.setUuid("user123123");
        cui.setEmail("mail@email.djdj.co");
        MockFactory.addActionMock(LoadUserInfoAction.class, new ActionMock<ClientUserCommonInfo>(cui));

        MockFactory.addActionMock(UpdateBidForReviewAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));


        // mock na chaira
        ClientContributionChairHead chairHead = new ClientContributionChairHead();
        chairHead.setAuthors("Josef Hubr, Pavol Gressa, Jaroslav Skrabalek, Milan Zornik");
        chairHead.setState(ClientContributionStateEnum.ACCEPTED);
        chairHead.setTitle("Business Development: A Barometer of Future Success");
        chairHead.setTopics("Business development, Digital society, Event management");
        chairHead.setType("Paper");
        chairHead.setUuid("123");

        ArrayList<ClientContributor> contributors = new ArrayList<ClientContributor>();
        contributors.add(new ClientContributor("1", "Jaroslav Skrabalek"));
        contributors.add(new ClientContributor("2", "Milan Zornik"));
        contributors.add(new ClientContributor("4", "Kristian Maly"));
        chairHead.setContributors(contributors);


        Evaluation evaluation = new Evaluation();
        EvaluationValue overall = new EvaluationValue(82);
        evaluation.setOverallScore(overall);
        evaluation.getPartialScores().add(new EvaluationValue("uuid 1", 52));
        evaluation.getPartialScores().add(new EvaluationValue("uuid 2", 98));
        evaluation.getPartialScores().add(new EvaluationValue("uuid 3", 88));
        evaluation.getPartialScores().add(new EvaluationValue("uuid 4", 61));
        chairHead.setEvaluations(evaluation);

        ReviewScoreAndCommentsData rsacd = new ReviewScoreAndCommentsData();
        rsacd.setReviewerName("Andrea Vranova");
        rsacd.setReviewerOrganization("Business Developer at Commsys");
        rsacd.setEvaluation(evaluation);
        rsacd.setPrivateComment("This is comment that is intended only for other reviewers and chairs.");
        rsacd.setPublicComment("This is public comment for contributors. You can here left a comment with recommendations or notes for authors of this contribution.");
        rsacd.setReviewUuid("uuid recenze");

        ArrayList<ReviewScoreAndCommentsData> chairScores = new ArrayList<ReviewScoreAndCommentsData>();
        //chairScores.add(rsacd);
        //chairScores.add(rsacd);
        //chairScores.add(rsacd);

        ClientContributionChairFormData chairFormData = new ClientContributionChairFormData();
        chairFormData.setHead(chairHead);
        chairFormData.setEvaluated(chairScores);


        MockFactory.addActionMock(LoadContributionChairFormAction.class, new ActionMock<ClientContributionChairFormData>(chairFormData));

        //// ---------------
        ClientReviewCriterionItem<EvaluationValue> crciev1 = new ClientReviewCriterionItem<EvaluationValue>();
        crciev1.setReviewUuid("324243");
        crciev1.setName("Leden Únorový");
        crciev1.setValue(new EvaluationValue(2));

        ClientReviewCriterionItem<EvaluationValue> crciev2 = new ClientReviewCriterionItem<EvaluationValue>();
        crciev2.setReviewUuid("3243");
        crciev2.setName("Únor Březnový");
        crciev2.setValue(new EvaluationValue(2));

        ClientReviewCriterion<EvaluationValue> criterion1 = new ClientReviewCriterion<EvaluationValue>(overall, "Název kritéria jedna", "Popisek kritéria jedna");
        criterion1.getItems().add(crciev1);
        criterion1.getItems().add(crciev2);
        criterion1.setMaxPoints(3);
        ClientReviewCriterion<EvaluationValue> criterion2 = new ClientReviewCriterion<EvaluationValue>(overall, "Název kritéria dva", "Popisek kritéria dva");
        criterion2.getItems().add(crciev1);
        criterion2.getItems().add(crciev2);
        criterion2.setMaxPoints(5);

        ClientReviewArtefactChair artefact1 = new ClientReviewArtefactChair("123", overall, "Název artefaktu jedna", ClientContributionArtefactDataType.TEXT);
        artefact1.setFileName("kuk1.buk");
        artefact1.setArtefactUuid("32424a");
        artefact1.setTotalScore(overall);
        artefact1.setArtefactTypeUuid("1235dva");
        //artefact1.getCriterias().add(criterion1);
        //artefact1.getCriterias().add(criterion2);
        artefact1.setDataType(ClientContributionArtefactDataType.TEXT);
        ClientReviewArtefactChair artefact2 = new ClientReviewArtefactChair("123", overall, "Název artefaktu dva", ClientContributionArtefactDataType.FILE);
        artefact2.setFileName("kuk2.buk");
        artefact2.setArtefactUuid("32424b");
        artefact2.setArtefactTypeUuid("1235jedna");
        artefact2.getCriterias().add(criterion1);
        artefact2.getCriterias().add(criterion2);
        artefact2.setDataType(ClientContributionArtefactDataType.FILE);

        consys.event.conference.gwt.client.bo.shared.ClientReviewCycle reviewCycle1 = new consys.event.conference.gwt.client.bo.shared.ClientReviewCycle();
        reviewCycle1.setFrom(new Date());
        reviewCycle1.setTo(new Date(System.currentTimeMillis() + 30000000));
        reviewCycle1.getArtefacts().add(artefact1);
        reviewCycle1.getArtefacts().add(new ClientArtefactType());
        reviewCycle1.getArtefacts().add(new ClientArtefactType());
        consys.event.conference.gwt.client.bo.shared.ClientReviewCycle reviewCycle2 = new consys.event.conference.gwt.client.bo.shared.ClientReviewCycle();
        reviewCycle2.setFrom(new Date());
        reviewCycle2.setTo(new Date());
        //reviewCycle2.getArtefacts().add(artefact1);
        //reviewCycle2.getArtefacts().add(artefact2);
        reviewCycle2.getArtefacts().add(new ClientArtefactType());
        reviewCycle2.getArtefacts().add(new ClientArtefactType());

        ArrayListResult<consys.event.conference.gwt.client.bo.shared.ClientReviewCycle> cycles = new ArrayListResult<consys.event.conference.gwt.client.bo.shared.ClientReviewCycle>();
        cycles.getArrayListResult().add(reviewCycle1);
        cycles.getArrayListResult().add(reviewCycle2);
        MockFactory.addActionMock(ListContributionChairReviewCyclesAction.class, new ActionMock<ArrayListResult<ClientReviewCycle>>(cycles));

        //-------------------------------------------
        ClientContributionReviewerHead reviewerHead = new ClientContributionReviewerHead();
        reviewerHead.setAuthors("Autor 1, Autor 2");
        reviewerHead.setContributors(contributors);
        reviewerHead.setEvaluations(contriEval);
        reviewerHead.setState(ClientContributionStateEnum.PENDING);
        reviewerHead.setTitle("Titulek příspěvku");
        reviewerHead.setTopics("Čvachtání, stříkání, kalení");
        reviewerHead.setType("Přednáška");
        reviewerHead.setUuid("12241");

        ClientReviewArtefactReviewer crar1 = new ClientReviewArtefactReviewer();
        crar1.setArtefactTypeName("Slidy");
        crar1.setAutoAccepted(false);
        crar1.setDataType(ClientContributionArtefactDataType.FILE);
        crar1.setArtefactUuid("345252526784");
        crar1.setArtefactTypeUuid("345252526784123");
        crar1.setTotalScore(new EvaluationValue(30));

        ClientReviewArtefactReviewer crar3 = new ClientReviewArtefactReviewer();
        crar3.setArtefactTypeName("Slidy tři");
        crar3.setFileName("soubor_slidy_tři.pdf");
        crar3.setAutoAccepted(false);
        crar3.setDataType(ClientContributionArtefactDataType.FILE);
        crar3.setArtefactUuid("3452525267843");
        crar3.setArtefactTypeUuid("3452525267841233");
        crar3.setTotalScore(new EvaluationValue(30));

        ClientReviewArtefactReviewer crar4 = new ClientReviewArtefactReviewer();
        crar4.setArtefactTypeName("Slidy čtyři");
        crar4.setAutoAccepted(false);
        crar4.setDataType(ClientContributionArtefactDataType.FILE);
        crar4.setArtefactUuid("3452525267844");
        crar4.setArtefactTypeUuid("3452525267841234");
        crar4.setTotalScore(new EvaluationValue(30));

        ClientReviewArtefactReviewer crar5 = new ClientReviewArtefactReviewer();
        crar5.setArtefactTypeName("Slidy pět");
        crar5.setAutoAccepted(false);
        crar5.setDataType(ClientContributionArtefactDataType.FILE);
        crar5.setArtefactUuid("3452525267845");
        crar5.setArtefactTypeUuid("3452525267841235");
        crar5.setTotalScore(new EvaluationValue(30));

        ClientReviewArtefactReviewer crar2 = new ClientReviewArtefactReviewer();
        crar2.setArtefactTypeName("Abstrakt");
        crar2.setDataType(ClientContributionArtefactDataType.TEXT);
        crar2.setAutoAccepted(true);
        crar2.setTotalScore(new EvaluationValue());
        crar2.setArtefactTypeUuid("34");

        ClientReviewCycle crcr1 = new ClientReviewCycle();
        crcr1.setFrom(new Date());
        crcr1.setTo(new Date(System.currentTimeMillis() + 30000000));
        crcr1.getArtefacts().add(crar1);
        crcr1.getArtefacts().add(crar2);

        ClientReviewCycle crcr2 = new ClientReviewCycle();
        crcr2.setFrom(new Date(System.currentTimeMillis() - 300000000));
        crcr2.setTo(new Date(System.currentTimeMillis() - 3600000));
        crcr2.getArtefacts().add(crar3);
        crcr2.getArtefacts().add(crar4);
        crcr2.getArtefacts().add(crar5);

        ClientContributionReviewerFormData reviewerFormData = new ClientContributionReviewerFormData();
        reviewerFormData.setHead(reviewerHead);
        reviewerFormData.setPrivateComment("soukromý komentář od recenzenta");
        reviewerFormData.setPublicComment("veřejný komentář od recenzenta");
        reviewerFormData.getReviewCycles().add(crcr1);
        reviewerFormData.getReviewCycles().add(crcr2);
        MockFactory.addActionMock(LoadContributionReviewerFormAction.class, new ActionMock<ClientContributionReviewerFormData>(reviewerFormData));
        //---------------------------
        MockFactory.addActionMock(UpdateContributionCommentAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------------------
        ClientReviewCriterion<Long> long1 = new ClientReviewCriterion<Long>();
        long1.setName("název :-) kriteria jedna");
        long1.setDescription("popopopisek jedna");
        long1.setValue(new EvaluationValue(3));
        long1.getItems().add(new ClientReviewCriterionItem<Long>(1l, "Jedna"));
        long1.getItems().add(new ClientReviewCriterionItem<Long>(2l, "Dva"));
        long1.getItems().add(new ClientReviewCriterionItem<Long>(3l, "Tři"));
        long1.getItems().add(new ClientReviewCriterionItem<Long>(4l, "Čtyři"));
        //long1.getItems().add(new ClientReviewCriterionItem<Long>(5l, "Pět"));

        ClientReviewCriterion<Long> long2 = new ClientReviewCriterion<Long>();
        long2.setName("název :-) kriteria dva");
        long2.setDescription("popisek kriteria dva");
        long2.setValue(new EvaluationValue(5));
        long2.getItems().add(new ClientReviewCriterionItem<Long>(6l, "Šest"));
        long2.getItems().add(new ClientReviewCriterionItem<Long>(7l, "Sedm"));
        long2.getItems().add(new ClientReviewCriterionItem<Long>(8l, "Osm"));
        long2.getItems().add(new ClientReviewCriterionItem<Long>(9l, "Devět"));
        long2.getItems().add(new ClientReviewCriterionItem<Long>(10l, "Deset"));

        ClientReviewReviewerData crrd = new ClientReviewReviewerData();
        //crrd.getEnteredEvaluations().add(1l);
        crrd.getEnteredEvaluations().add(7l);
        //crrd.getCriterias().add(long1);
        //crrd.getCriterias().add(long2);
        crrd.getEnteredEvaluations();
        crrd.setScore(new EvaluationValue(30));
        MockFactory.addActionMock(consys.event.conference.gwt.client.action.reviewer.LoadContributionArtefactReviewAction.class,
                new ActionMock<ClientReviewReviewerData>(crrd));

        // -------------------------------------
        ClientContributionActionData ccad = new ClientContributionActionData();
        ccad.setHead(contri);
        ccad.setReviews(chairScores);
        ccad.setSelectedCycles(out);
        MockFactory.addActionMock(LoadContributionAction.class, new ActionMock<ClientContributionActionData>(ccad));
        // -------------------------------------
        MockFactory.addActionMock(UpdateContributionAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        // -------------------------------------
        MockFactory.addActionMock(LoadContributionHeadAction.class, new ActionMock<ClientContributionHeadData>(contri));
        // -------------------------------------
        MockFactory.addActionMock(UpdateContributionTextArtefactAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        // -------------------------------------
        ArrayList<String> unassigned = new ArrayList<String>();
        unassigned.add("4346353452");
        ClientEditReviewCycle cerc = new ClientEditReviewCycle();
        cerc.setAssignedArtefacts(assignedArtefacts);
        cerc.setNotAssignedArtefacts(unassigned);
        cerc.setSubmitFrom(new Date());
        cerc.setSubmitTo(new Date(cerc.getSubmitFrom().getTime() + 100000000));
        MockFactory.addActionMock(LoadEditReviewCycleAction.class, new ActionMock<ClientEditReviewCycle>(cerc));
        // -------------------------------------
        ClientContributionReviewerFormData ccd = new ClientContributionReviewerFormData();
        ccd.setHead(reviewerHead);
        ccd.setIsAuthor(false);
        ccd.setPrivateComment("Soukromý komentář");
        ccd.setPublicComment("Veřejný komentář");
        ccd.getReviewCycles().add(rc3);
        MockFactory.addActionMock(LoadContributionReviewFormAction.class, new ActionMock<ClientContributionReviewerFormData>(ccd));
    }

    @Override
    public void registerModules(List<Module> modules) {
        modules.add(new OverseerModule());
        modules.add(new EventCoreModule());
        modules.add(new ConferenceModule());
    }

    @Override
    public void registerForms(Map<String, Widget> formMap) {
        Cache.get().register(ConferenceProperties.SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER, "10");
        Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, TimeZoneProvider.getTimeZoneByID("Europe/London"));
        //// ---------------
        //formMap.put("Event Wall", new ConferenceWallContent());

        ClientReviewArtefactReviewer cra = new ClientReviewArtefactReviewer();
        cra.setArtefactTypeName("Abstrakt");
        cra.setDataType(ClientContributionArtefactDataType.FILE);
        cra.setAutoAccepted(true);
        cra.setTotalScore(new EvaluationValue());
        cra.setArtefactTypeUuid("34");
        cra.setArtefactTypeDescription("popisek artefaktu");
        for (int i = 1; i <= 40; i++) {
            cra.getUploadedExamples().add(new CommonThumb("123" + i, "Příklad" + i + ".txt"));
        }
        cra.getUploadedTemplates().add(new CommonThumb("t123", "Template1.txt"));
        cra.setFileName("můj-strašně-dlouhý-název-souboru-který-upladuju-na-server.jpg");

        ClientCriteria criteria1 = new ClientCriteria();
        criteria1.setName("Pepovo kriterium");
        criteria1.setDescription("Popisek pepova kriteria");
        criteria1.getPoints().add(new ClientCriteriaItem(1l, "Jedna"));
        criteria1.getPoints().add(new ClientCriteriaItem(2l, "Dva"));
        criteria1.getPoints().add(new ClientCriteriaItem(3l, "Tři"));
        criteria1.getPoints().add(new ClientCriteriaItem(4l, "Čtyři"));
        //criteria1.getPoints().add(new ClientCriteriaItem(5l, "Pět"));
        //criteria1.getPoints().add(new ClientCriteriaItem(6l, "Šest"));
        //criteria1.getPoints().add(new ClientCriteriaItem(7l, "Sedm"));
        //criteria1.getPoints().add(new ClientCriteriaItem(8l, "Osm"));
        //criteria1.getPoints().add(new ClientCriteriaItem(9l, "Devět"));
        criteria1.getPoints().add(new ClientCriteriaItem(10l, "Deset"));
        criteria1.setMaxPoints(5);
        ClientCriteria criteria2 = new ClientCriteria();
        criteria2.setName("Palovo kriterium");
        criteria2.setDescription("Popisek palova kriteria");
        criteria2.getPoints().add(new ClientCriteriaItem(11l, "Jedna"));
        criteria2.getPoints().add(new ClientCriteriaItem(12l, "Dva"));
        criteria2.getPoints().add(new ClientCriteriaItem(13l, "Tři"));
        criteria2.getPoints().add(new ClientCriteriaItem(14l, "Čtyři"));
        //criteria2.getPoints().add(new ClientCriteriaItem(15l, "Pět"));
        //criteria2.getPoints().add(new ClientCriteriaItem(16l, "Šest"));
        //criteria2.getPoints().add(new ClientCriteriaItem(17l, "Sedm"));
        //criteria2.getPoints().add(new ClientCriteriaItem(18l, "Osm"));
        //criteria2.getPoints().add(new ClientCriteriaItem(19l, "Devět"));
        criteria2.getPoints().add(new ClientCriteriaItem(20l, "Deset"));
        criteria2.setMaxPoints(5);
        ClientCriteria criteria3 = new ClientCriteria();
        criteria3.setName("Jardovo kriterium");
        criteria3.setDescription("Popisek jardova kriteria");
        criteria3.getPoints().add(new ClientCriteriaItem(21l, "Jedna"));
        criteria3.getPoints().add(new ClientCriteriaItem(22l, "Dva"));
        criteria3.getPoints().add(new ClientCriteriaItem(23l, "Tři"));
        criteria3.getPoints().add(new ClientCriteriaItem(24l, "Čtyři"));
        //criteria3.getPoints().add(new ClientCriteriaItem(25l, "Pět"));
        //criteria3.getPoints().add(new ClientCriteriaItem(26l, "Šest"));
        //criteria3.getPoints().add(new ClientCriteriaItem(27l, "Sedm"));
        //criteria3.getPoints().add(new ClientCriteriaItem(28l, "Osm"));
        //criteria3.getPoints().add(new ClientCriteriaItem(29l, "Devět"));
        criteria3.getPoints().add(new ClientCriteriaItem(30l, "Deset"));
        criteria3.setMaxPoints(5);

        ArrayList<Long> selected = new ArrayList<Long>();
        selected.add(3l);

        ClientReviewCriterion<Long> criterion = new ClientReviewCriterion<Long>();
        criterion.setName("Názevk kriteria");
        criterion.setDescription("Popisek kriteria");
        criterion.getItems().add(new ClientReviewCriterionItem<Long>(1l, "Hodnota jedna"));
        criterion.getItems().add(new ClientReviewCriterionItem<Long>(2l, "Hodnota dva"));
        criterion.getItems().add(new ClientReviewCriterionItem<Long>(3l, "Hodnota tři"));
        criterion.getItems().add(new ClientReviewCriterionItem<Long>(4l, "Hodnota čtyři"));

        ClientReviewReviewerData reviewerEvaluationData = new ClientReviewReviewerData();
        reviewerEvaluationData.setPublicComment("veřejný koment");
        reviewerEvaluationData.setPrivateComment("soukromý koment");
        reviewerEvaluationData.getCriterias().add(criterion);

        ArrayList<ClientCriteria> evaluatorList = new ArrayList<ClientCriteria>();
        evaluatorList.add(criteria1);
        evaluatorList.add(criteria2);
        evaluatorList.add(criteria3);
        CriteriaEvaluatorWrapper evaluatorWrapper = new CriteriaEvaluatorWrapper();
        evaluatorWrapper.setCriterias(reviewerEvaluationData.getCriterias());
        formMap.put("EvaluatorWrapper", evaluatorWrapper);
        CriteriaEvaluatorWrapper evaluatorWrapperPreset = new CriteriaEvaluatorWrapper();
        evaluatorWrapperPreset.setCriterias(reviewerEvaluationData.getCriterias());
        evaluatorWrapperPreset.setValues(selected);
        formMap.put("EvaluatorWrapper preset", evaluatorWrapperPreset);
        ClientPreviews preview = new ClientPreviews();
        preview.setName("Název kritéria / artefaktu");
        preview.setOverallScore(new EvaluationItemValue(21));
        preview.getReviewers().add(new ClientPreview("123", new EvaluationItemValue(65), "Matěj Kotěhůlek"));
        preview.getReviewers().add(new ClientPreview("321", new EvaluationItemValue(83), "Ludmila Miladová"));
        ArrayList<ClientPreviews> evaluatorListR = new ArrayList<ClientPreviews>();
        evaluatorListR.add(preview);
        PreviewEvaluatorWrapper evaluatorWrapperR = new PreviewEvaluatorWrapper("Tadddy", "Titulek příspěvku");
        evaluatorWrapperR.setPreviews(evaluatorListR);
        formMap.put("EvaluatorWrapper reviewers", evaluatorWrapperR);

        Evaluation evaluation = new Evaluation();
        EvaluationItemValue overall = new EvaluationItemValue(3, 2, 10, "Celkové hodnocení", "Jedná se o průměr všech zadaných hodnocení");
        evaluation.setOverallScore(new EvaluationValue(34));
        for (int i = 0; i < 5; i++) {
            if (i % 2 == 0) {
                evaluation.getPartialScores().add(new EvaluationItemValue("Sudé hodnocení", ""));
            } else {
                evaluation.getPartialScores().add(new EvaluationItemValue(3, 2, 10, "Liché hodnocení", ""));
            }
        }

        EvaluationDisplay display1 = EvaluationDisplay.forHead();
        display1.setOverallScore(overall);
        EvaluationDisplay display2 = EvaluationDisplay.forList();
        display1.setOverallScore(overall);

        FlowPanel fp1 = new FlowPanel();
        fp1.add(display1);
        fp1.add(display2);

        formMap.put("EvaluationDisplay", fp1);

        ReviewerEvaluation re = new ReviewerEvaluation();
        re.setData(reviewerEvaluationData, "123", "234", "324");

        formMap.put("ReviewerEvaluation", re);

        ReviewScoreAndCommentsData rsacd = new ReviewScoreAndCommentsData();
        rsacd.setReviewerName("Spejblův Hurvínek");
        rsacd.setReviewerOrganization("Borec ve Firmě");
        rsacd.setEvaluation(evaluation);
        rsacd.setPrivateComment("tajný komentář tajný komentář tajný komentář tajný komentář tajný komentář tajný komentář tajný komentář tajný komentář tajný komentář tajný komentář tajný komentář ");


        List<ReviewScoreAndCommentsData> l = new ArrayList<ReviewScoreAndCommentsData>();
        l.add(rsacd);
        l.add(rsacd);
        l.add(rsacd);
        ReviewScoreAndComments sac = new ReviewScoreAndComments(l, contri);
        formMap.put("ReviewScoreAndComments", sac);

        EvaluationItemValue medi = new EvaluationItemValue(new EvaluationValue(EvaluationValue.AUTOACCEPTED));
        medi.setHelpTitle("Nápověda pro střední");
        medi.setHelpText("help text");

        final EvaluationItem sei = EvaluationItem.small(medi/*new EvaluationItemValue("Nápověda pro malý", "")*/);
        final EvaluationItem seim = EvaluationItem.medium(medi);
        final EvaluationItem seib = EvaluationItem.big(medi/*new EvaluationItemValue("Nápověda pro velký", "")*/);
        final TextBox tb1 = new TextBox();
        tb1.setText("0");
        final TextBox tb2 = new TextBox();
        tb2.setText("10");
        ActionLabel al = new ActionLabel("set value");
        al.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                sei.setValue(new EvaluationItemValue(Integer.valueOf(tb1.getText().trim()), 2, Integer.valueOf(tb2.getText().trim())));
                seim.setValue(new EvaluationItemValue(Integer.valueOf(tb1.getText().trim()), 2, Integer.valueOf(tb2.getText().trim())));
                seib.setValue(new EvaluationItemValue(Integer.valueOf(tb1.getText().trim()), 2, Integer.valueOf(tb2.getText().trim())));
            }
        });
        ActionLabel al2 = new ActionLabel("no evaluation");
        al2.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                sei.setValue(new EvaluationItemValue());
                seim.setValue(new EvaluationItemValue());
                seib.setValue(new EvaluationItemValue());
            }
        });
        FlowPanel p = new FlowPanel();
        p.add(seib);
        p.add(seim);
        p.add(sei);
        p.add(new Label("hodnoceni"));
        p.add(tb1);
        p.add(new Label("rozsah"));
        p.add(tb2);
        p.add(al);
        p.add(al2);

        // test novych vlastnosti
        FlowPanel panel = new FlowPanel();
        contri.setAuthors("Janko Hrasko, Michal Podolan, Jiri Jirasek");
        contri.setTitle("Dvacet minut do konce");
        contri.setTopics("Vizualizace, Inovace v škudlení");
        contri.setUuid("3492JRJ");
        contri.setType("Paper");
        contri.setEvaluations(new Evaluation<EvaluationValue, EvaluationValue>());

        ArrayList<ClientContributor> ccc = new ArrayList<ClientContributor>();
        ccc.add(new ClientContributor("1", "Master Blaster"));
        ccc.add(new ClientContributor("2", "Josef Hubr"));
        contri.setContributors(ccc);


        panel.add(new ContributionHeadPanel(contri, new HeadPanelReadInputFactory(true), false));
        panel.add(new ContributionHeadPanel(contri, new HeadPanelUpdateInputFactory(), false));
        panel.add(new ContributionHeadPanel(new ClientContributionHeadData(), new HeadPanelUpdateInputFactory(), false));



        formMap.put("Settings", new ContributionTypeContent());
        formMap.put("EvaluationItem", p);
        //// ---------------
        formMap.put("ContributionItems", panel);
        formMap.put("Bid for review", new BidForReviewForm());
        formMap.put("Contribution type content", new ContributionTypeContent());
        formMap.put("Assignment of Reviewers Form", new AssignmentReviewersForm("45234"));
        //// ---------------
        formMap.put("My reviews", new MyReviewsList());
        //// ---------------
        formMap.put("MyContributionList", new MyContributionList());
        //// ---------------
        ClientContribution cc = new ClientContribution();
        cc.setAuthors("autor jedna, autor dva");
        cc.setSubtitle("contribution subtitle");
        cc.setTitle("contribution title");
        cc.getSubmitters().add(new CommonThumb("123", "Marcel Chroustal"));
        cc.setUuid("uuid");
        cc.getTopics().add(new ClientTopic(1l, "Topik jedna"));
        cc.getTopics().add(new ClientTopic(2l, "Topik dva"));
        cc.getTopics().add(new ClientTopic(3l, "Topik tři"));


        formMap.put("Contribution Review Form", new ContributionReviewerForm("Business Development: A Barometer of Future Success", "123"));

        formMap.put("Contributions list", new ChairContributionsList());

        formMap.put("Contribution Chair Form", new ContributionChairForm("Business Development: A Barometer of Future Success", "123"));

        formMap.put("Contribution Chair Form", new ContributionChairForm("Business Development: A Barometer of Future Success", "123"));
        formMap.put("Send submission", new NewContributionForm());
        formMap.put("Send submission - internal", new NewContributionForm(true));

        formMap.put("ArtefactUpload", new ArtefactUpload(cra, "3423"));

    }
}
