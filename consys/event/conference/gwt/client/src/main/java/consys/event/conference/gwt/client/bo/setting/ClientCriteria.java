package consys.event.conference.gwt.client.bo.setting;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Jedno kriterium se seznamem bodu a popiskem
 * @author pepa
 */
public class ClientCriteria extends ClientCriteriaThumb implements Result {

    // konstanty
    private static final long serialVersionUID = -1156660036358391330L;
    // data
    /** seznam popisku k bodum kriterii, prvni hodnota je popisek pro nejnizzsi hodnoceni, urcuje i pocet bodu */
    private ArrayList<ClientCriteriaItem> points;
    private String description;
    

    public ClientCriteria() {
        points = new ArrayList<ClientCriteriaItem>();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * seznam popisku k bodum kriterii, prvni hodnota je popisek pro nejnizzsi hodnoceni, urcuje i pocet bodu
     * @return the points
     */
    public ArrayList<ClientCriteriaItem> getPoints() {
        return points;
    }

    /**
     * seznam popisku k bodum kriterii, prvni hodnota je popisek pro nejnizzsi hodnoceni, urcuje i pocet bodu
     * @param pointLabels the pointLabels to set
     */
    public void setPoints(ArrayList<ClientCriteriaItem> points) {
        this.points = points;
    }

    
}
