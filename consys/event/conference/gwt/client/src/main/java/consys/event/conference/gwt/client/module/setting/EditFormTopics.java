package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Remover;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.client.action.exception.DeleteRelationException;
import consys.event.conference.gwt.client.action.setting.CreateTopicAction;
import consys.event.conference.gwt.client.action.setting.DeleteTopicAction;
import consys.event.conference.gwt.client.action.contribution.ListTopicAction;
import consys.event.conference.gwt.client.action.setting.UpdateTopicAction;
import consys.event.conference.gwt.client.action.exception.TopicUniqueException;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class EditFormTopics extends RootPanel {

    // komponenty
    private FlowPanel panel;

    public EditFormTopics() {
        super(ECMessageUtils.c.const_topics());

        ActionImage backButton = ActionImage.getBackButton(ECMessageUtils.c.contributionTypeContent_title(), true);
        backButton.addClickHandler(FormUtils.breadcrumbBackClickHandler());
        addLeftControll(backButton);

        init();
        EventDispatchEvent loadEvent = new EventDispatchEvent(new ListTopicAction(),
                new AsyncCallback<ArrayListResult<ClientTopic>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientTopic> result) {
                        ArrayList<ClientTopic> list = result.getArrayListResult();
                        int i = 0;
                        for (i = 0; i < list.size(); i++) {
                            panel.add(new TopicItem(list.get(i)));
                        }
                        panel.add(new TopicItem());
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);
    }

    private void init() {
        panel = new FlowPanel();
        addWidget(panel);
        addWidget(StyleUtils.clearDiv());
    }

    /** topic item */
    private class TopicItem extends FlowPanel {

        // komponenty
        private TextBox textBox;
        private ActionLabel save;
        private Remover remover;
        // data
        private ClientTopic topic;

        public TopicItem() {
            this(null);
        }

        public TopicItem(ClientTopic topic) {
            super();
            setStyleName(MARGIN_TOP_5);
            this.topic = topic;

            final Timer t = new Timer() {

                @Override
                public void run() {
                    hideControl();
                }
            };

            textBox = StyleUtils.getFormInput();
            textBox.setText(topic != null ? topic.getName() : "");
            textBox.addStyleName(FLOAT_LEFT);
            textBox.addFocusHandler(new FocusHandler() {

                @Override
                public void onFocus(FocusEvent event) {
                    t.cancel();
                    showControl(TopicItem.this.topic != null);
                }
            });
            textBox.addBlurHandler(new BlurHandler() {

                @Override
                public void onBlur(BlurEvent event) {
                    if (TopicItem.this.topic == null && textBox.getText().trim().equals("")) {
                        t.schedule(500);
                    } else if (TopicItem.this.topic != null && textBox.getText().trim().equals(TopicItem.this.topic.getName())) {
                        t.schedule(500);
                    }
                }
            });
            textBox.addKeyPressHandler(new KeyPressHandler() {

                @Override
                public void onKeyPress(KeyPressEvent event) {
                    if ((int) event.getCharCode() == KeyCodes.KEY_ENTER) {
                        if (TopicItem.this.topic == null) {
                            saveTopic();
                        } else {
                            updateTopic();
                        }
                    }
                }
            });
            add(textBox);

            save = new ActionLabel(UIMessageUtils.c.const_save(), MARGIN_LEFT_10 + " " + FLOAT_LEFT + " " + MARGIN_TOP_3);
            save.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    saveTopic();
                }
            });
            add(save);

            ActionLabel update = new ActionLabel(UIMessageUtils.c.const_update(), MARGIN_LEFT_10 + " " + FLOAT_LEFT + " " + MARGIN_TOP_3);
            update.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    updateTopic();
                }
            });

            Image remove = new Image(ResourceUtils.system().removeCross());
            remove.setStyleName(StyleUtils.HAND);
            remove.addStyleName(FLOAT_LEFT);
            remove.addStyleName(MARGIN_LEFT_10);
            remove.addStyleName(MARGIN_TOP_3);
            ConsysAction removeAction = new ConsysAction() {

                @Override
                public void run() {
                    EventDispatchEvent deleteEvent = new EventDispatchEvent(new DeleteTopicAction(TopicItem.this.topic.getId()),
                            new AsyncCallback<VoidResult>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    // obecne chyby zpracovava event action executor
                                    if (caught instanceof DeleteRelationException) {
                                        getFailMessage().setText(ECMessageUtils.c.const_exceptionDeleteRelation());
                                    } else if (caught instanceof NoRecordsForAction) {
                                        getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                    }
                                }

                                @Override
                                public void onSuccess(VoidResult result) {
                                    removeFromParent();
                                }
                            }, EditFormTopics.this);
                    EventBus.get().fireEvent(deleteEvent);
                }
            };

            FlowPanel controlPanel = new FlowPanel();
            controlPanel.addStyleName(FLOAT_LEFT);
            controlPanel.add(update);
            controlPanel.add(remove);

            remover = new Remover(controlPanel, remove, removeAction) {

                @Override
                protected void onQuestionSwap() {
                    t.cancel();
                }

                @Override
                protected void onDefaultSwap() {
                    focus();
                }
            };
            remover.addStyleName(FLOAT_LEFT);
            remover.addQuestionStyleName(MARGIN_TOP_3);
            remover.addQuestionStyleName(MARGIN_LEFT_10);
            add(remover);
            hideControl();

            add(StyleUtils.clearDiv());
        }

        /** zobrazi prislusne ovladace */
        private void showControl(boolean doUpdate) {
            if (!doUpdate) {
                save.setVisible(true);
                remover.setVisible(false);
            } else {
                save.setVisible(false);
                remover.setVisible(true);
            }
        }

        /** skryje vsechny ovladace */
        private void hideControl() {
            save.setVisible(false);
            remover.setVisible(false);
        }

        public void focus() {
            textBox.setFocus(true);
        }

        /** spusti ulozeni topiku */
        private void saveTopic() {
            String name = textBox.getText().trim();
            if (name.isEmpty()) {
                getFailMessage().setText(ECMessageUtils.c.editFormTopics_text_enteredEmptyValue());
                return;
            }
            final ClientTopic ct = new ClientTopic();
            ct.setName(name);
            EventDispatchEvent createEvent = new EventDispatchEvent(new CreateTopicAction(ct.getName()),
                    new AsyncCallback<LongResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                            if (caught instanceof TopicUniqueException) {
                                getFailMessage().setText(ECMessageUtils.c.const_exceptionTopicUnique());
                            }
                        }

                        @Override
                        public void onSuccess(LongResult result) {
                            ct.setId(result.getValue());
                            TopicItem.this.topic = ct;

                            hideControl();

                            TopicItem ti = new TopicItem();
                            panel.add(ti);
                            ti.focus();
                        }
                    }, EditFormTopics.this);
            EventBus.get().fireEvent(createEvent);
        }

        /** spusti aktualizaci topiku */
        private void updateTopic() {
            final String name = textBox.getText().trim();
            if (name.isEmpty()) {
                getFailMessage().setText(ECMessageUtils.c.editFormTopics_text_enteredEmptyValue());
                return;
            }

            EventDispatchEvent updateEvent = new EventDispatchEvent(new UpdateTopicAction(new ClientTopic(topic.getId(), name)),
                    new AsyncCallback<VoidResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                            if (caught instanceof NoRecordsForAction) {
                                getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                            } else if (caught instanceof TopicUniqueException) {
                                getFailMessage().setText(ECMessageUtils.c.const_exceptionTopicUnique());
                            }
                        }

                        @Override
                        public void onSuccess(VoidResult result) {
                            TopicItem.this.topic.setName(name);
                        }
                    }, EditFormTopics.this);
            EventBus.get().fireEvent(updateEvent);
        }
    }
}
