package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.CommonThumb;

/**
 * Akce pro nacteni jiz uploadovanych vzoru
 * @author pepa
 */
public class ListUploadedArtefactTemplateAction extends EventAction<ArrayListResult<CommonThumb>> {

    private static final long serialVersionUID = 7653144388410691692L;

    private boolean examples;

    public ListUploadedArtefactTemplateAction() {
    }

    public ListUploadedArtefactTemplateAction(boolean examples) {
        this.examples = examples;
    }

    public boolean isExamples() {
        return examples;
    }

}
