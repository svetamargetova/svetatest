package consys.event.conference.gwt.client.comp.evaluator;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;

/**
 * Kontejner pro skupinu Evaluatoru
 * @author pepa
 */
public abstract class EvaluatorWrapper<T, E extends Evaluator> extends FlowPanel {

    // data
    private ArrayList<T> data;

    public void setData(ArrayList<T> data) {
        this.data = data;
    }

    @Override
    protected void onLoad() {
        clear();
        generateContent();
    }

    private void generateContent() {
        if (data != null && !data.isEmpty()) {
            E evaluator = createEvaluator(data.get(0));
            postProcessEvaluator(evaluator);
            add(evaluator);
            for (int i = 1; i < data.size(); i++) {
                evaluator = createEvaluator(data.get(i));
                evaluator.addStyleName(ECResourceUtils.css().evaluatorWrapperItem());
                postProcessEvaluator(evaluator);
                add(evaluator);
            }
        }
    }

    /** urceno pro pretizeni, pokud je potreba jeste nejak nastavit vytvoreny evaluator */
    protected void postProcessEvaluator(E evaluator) {
    }

    /** vytvori evaluator pro zadany typ dat */
    protected abstract E createEvaluator(T data);
}
