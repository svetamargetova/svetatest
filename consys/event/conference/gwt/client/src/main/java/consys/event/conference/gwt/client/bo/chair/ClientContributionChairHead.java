package consys.event.conference.gwt.client.bo.chair;

import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import java.util.Date;

/**
 * Data pre recenzencny formular
 * @author palo
 */
public class ClientContributionChairHead extends ClientContributionHeadData {

    private static final long serialVersionUID = 5760048450671747014L;
    
     /** Datum kedy sa zmenil stav */
    private Date changedDate;

    /**
     * Datum kedy sa zmenil stav
     * @return the changedDate
     */
    public Date getChangedDate() {
        return changedDate;
    }

    /**
     * Datum kedy sa zmenil stav
     * @param changedDate the changedDate to set
     */
    public void setChangedDate(Date changedDate) {
        this.changedDate = changedDate;
    }
}
