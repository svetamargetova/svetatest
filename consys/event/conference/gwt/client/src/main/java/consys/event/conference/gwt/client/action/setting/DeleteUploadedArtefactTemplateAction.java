package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro odstraneni uploadovaneho souboru
 * @author pepa
 */
public class DeleteUploadedArtefactTemplateAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 6271725007047231953L;
    // data
    private String uuid;
    private boolean pattern;

    public DeleteUploadedArtefactTemplateAction() {
    }

    public DeleteUploadedArtefactTemplateAction(String uuid, boolean pattern) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public boolean isPattern() {
        return pattern;
    }        
}
