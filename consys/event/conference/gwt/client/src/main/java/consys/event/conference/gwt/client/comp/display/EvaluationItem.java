package consys.event.conference.gwt.client.comp.display;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.Help;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Ctverecek s vysledkem hodnoceni, podle vstupu konstruktoru (maly, stredni, velky)
 * @author pepa
 */
public class EvaluationItem extends Composite {

    // komponenty
    private FlowPanel panel;
    private FlowPanel scorePanel;
    private Label integerLabel;
    private Label decimalLabel;
    private Help help;
    // data
    private EvaluationItemValue value;
    private EvaluationItemType size;
    private String additionalStyles;
    private boolean startFromOne;

    /** enum velikosti polozky EvaluationItem */
    public enum EvaluationItemType {

        SMALL, MEDIUM, BIG, MIDDLE, BOTTOM;
        private static final long serialVersionUID = -990884051540780395L;
    }

    public EvaluationItem(EvaluationItemValue value, EvaluationItemType size) {
        this(value, size, false);
    }

    public EvaluationItem(EvaluationItemValue value, EvaluationItemType size, boolean startFromOne) {
        this.value = value;
        this.size = size;
        this.startFromOne = startFromOne;

        additionalStyles = "";

        initComponents();
        initHandlers();
    }

    private void initComponents() {
        integerLabel = new Label();

        decimalLabel = new Label();
        decimalLabel.setVisible(false);

        scorePanel = new FlowPanel();
        scorePanel.add(integerLabel);
        scorePanel.add(decimalLabel);
        scorePanel.add(StyleUtils.clearDiv());

        panel = new FlowPanel();
        panel.setStyleName(ECResourceUtils.css().evaluationItem());
        panel.add(scorePanel);

        initWidget(panel);
        initComponentsStyle();
        refreshItem();
    }

    private void initComponentsStyle() {
        switch (size) {
            case SMALL:
            case MIDDLE:
            case BOTTOM:
                integerLabel.setStyleName(ECResourceUtils.css().smallEvaluationItemInteger());
                decimalLabel.setStyleName(ECResourceUtils.css().smallEvaluationItemDecimal());
                break;
            case MEDIUM:
                integerLabel.setStyleName(ECResourceUtils.css().mediumEvaluationItemInteger());
                decimalLabel.setStyleName(ECResourceUtils.css().mediumEvaluationItemDecimal());
                break;
            case BIG:
                integerLabel.setStyleName(ECResourceUtils.css().bigEvaluationItemInteger());
                decimalLabel.setStyleName(ECResourceUtils.css().bigEvaluationItemDecimal());
                break;
        }
    }

    public EvaluationItemValue getValue() {
        return value;
    }

    public void setValue(EvaluationItemValue value) {
        this.value = value;
        refreshItem();
    }

    private void refreshItem() {
        if (value.isInteger() || value.isTen()) {
            if (value.isNA()) {
                naState();
            } else {
                // funguje i pro stav nezadano
                onlyIntegerState();
            }
        } else {
            integerAndDecimalState();
        }

        refreshValue();
        changeBackgroundColor();
        showPointer();
    }

    private void onlyIntegerState() {
        switch (size) {
            case MIDDLE:
            case BOTTOM:
                scorePanel.setStyleName(value.isTen()
                        ? ECResourceUtils.css().middleOrBottomScorePanelInteger10()
                        : ECResourceUtils.css().middleOrBottomScorePanelInteger09());
                break;
            case SMALL:
                scorePanel.setStyleName(value.isTen()
                        ? ECResourceUtils.css().smallScorePanelInteger10()
                        : ECResourceUtils.css().smallScorePanelInteger09());
                break;
            case MEDIUM:
                scorePanel.setStyleName(value.isTen()
                        ? ECResourceUtils.css().mediumScorePanelInteger10()
                        : ECResourceUtils.css().mediumScorePanelInteger09());
                break;
            case BIG:
                scorePanel.setStyleName(value.isTen()
                        ? ECResourceUtils.css().bigScorePanelInteger10()
                        : ECResourceUtils.css().bigScorePanelInteger09());
                break;
        }
        decimalLabel.setVisible(false);
    }

    private void integerAndDecimalState() {
        switch (size) {
            case SMALL:
            case MIDDLE:
            case BOTTOM:
                scorePanel.setStyleName(ECResourceUtils.css().smallScorePanelIntegerDecimal());
                break;
            case MEDIUM:
                scorePanel.setStyleName(ECResourceUtils.css().mediumScorePanelIntegerDecimal());
                break;
            case BIG:
                scorePanel.setStyleName(ECResourceUtils.css().bigScorePanelIntegerDecimal());
                break;
        }
        decimalLabel.setVisible(true);
    }

    private void naState() {
        switch (size) {
            case SMALL:
            case MIDDLE:
            case BOTTOM:
                scorePanel.setStyleName(ECResourceUtils.css().smallScorePanelNA());
                break;
            case MEDIUM:
                scorePanel.setStyleName(ECResourceUtils.css().mediumScorePanelNA());
                break;
            case BIG:
                scorePanel.setStyleName(ECResourceUtils.css().bigScorePanelNA());
                break;
        }
        decimalLabel.setVisible(true);
    }

    private void refreshValue() {
        if (value.isNotEntered()) {
            if (value.isNA()) {
                integerLabel.setText("N");
                decimalLabel.setText("/A");
            } else if (value.isAutoaccepted()) {
                integerLabel.setText(FormUtils.NDASH);
                decimalLabel.setText("");
            } else {
                integerLabel.setText("?");
                decimalLabel.setText("");
            }
        } else {
            integerLabel.setText(String.valueOf(value.getInteger()));
            decimalLabel.setText("." + value.getDecimal());
        }
    }

    private void changeBackgroundColor() {
        BackgroundEnum bge = BackgroundEnum.color(value, startFromOne);
        switch (size) {
            case SMALL:
                setStyleName(ECResourceUtils.css().smallEvaluationItem());
                super.addStyleName(bge.getStyleSmall());
                break;
            case MEDIUM:
                setStyleName(ECResourceUtils.css().mediumEvaluationItem());
                super.addStyleName(bge.getStyleMedium());
                break;
            case BIG:
                setStyleName(ECResourceUtils.css().bigEvaluationItem());
                super.addStyleName(bge.getStyleBig());
                break;
            case MIDDLE:
                setStyleName(ECResourceUtils.css().middleOrBottomEvaluationItem());
                super.addStyleName(bge.getStyleMiddle());
                break;
            case BOTTOM:
                setStyleName(ECResourceUtils.css().middleOrBottomEvaluationItem());
                super.addStyleName(bge.getStyleBottom());
                break;
        }
        if (!additionalStyles.isEmpty()) {
            super.addStyleName(additionalStyles);
        }
    }

    /** pokud se jedna o klikatelno polozku, zobrazuje se rucicka */
    private void showPointer() {
        boolean containsHand = panel.getStyleName().contains(ECResourceUtils.css().evaluationItemClickable());
        if (value.getOnClickAction() != null && !containsHand) {
            panel.addStyleName(ECResourceUtils.css().evaluationItemClickable());
        } else {
            panel.removeStyleName(ECResourceUtils.css().evaluationItemClickable());
        }
    }

    @Override
    public void addStyleName(String style) {
        if (!additionalStyles.isEmpty()) {
            additionalStyles += " ";
        }
        additionalStyles += style;
        super.addStyleName(style);
    }

    private void initHandlers() {
        boolean isHelp = StringUtils.isNotEmpty(value.getHelpTitle()) || StringUtils.isNotEmpty(value.getHelpText());
        boolean isClick = value.getOnClickAction() != null;

        if (isHelp && isClick) {
            sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT | Event.ONCLICK);
        } else if (isHelp && !isClick) {
            sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
        } else if (!isHelp && isClick) {
            sinkEvents(Event.ONCLICK);
        }

        if (isHelp) {
            addHandler(overHandler(), MouseOverEvent.getType());
            addHandler(outHandler(), MouseOutEvent.getType());
        }
        if (isClick) {
            addHandler(clickHandler(), ClickEvent.getType());
        }
    }

    private MouseOverHandler overHandler() {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                showHelp();
            }
        };
    }

    private void showHelp() {
        if (help == null) {
            help = Help.left(value.getHelpTitle(), value.getHelpText());
        }
        int left = getAbsoluteLeft();
        int top = getAbsoluteTop();
        switch (size) {
            case SMALL:
                left += 25;
                top += 13;
                break;
            case MEDIUM:
                left += 56;
                top += 29;
                break;
            case BIG:
                left += 91;
                top += 46;
                break;
        }
        help.setPopupPosition(left, top);
        help.show();
    }

    private MouseOutHandler outHandler() {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                hideHelp();
            }
        };
    }

    private void hideHelp() {
        if (help != null) {
            help.hide();
        }
    }

    private ClickHandler clickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                value.getOnClickAction().run();
            }
        };
    }

    /** tovarni metoda pro maly ctverecek */
    public static EvaluationItem small(EvaluationItemValue value) {
        return small(value, false);
    }

    /** tovarni metoda pro maly ctverecek */
    public static EvaluationItem small(EvaluationItemValue value, boolean startFromOne) {
        return new EvaluationItem(value, EvaluationItemType.SMALL, startFromOne);
    }

    /** tovarni metoda pro stredni ctverecek */
    public static EvaluationItem medium(EvaluationItemValue value) {
        return new EvaluationItem(value, EvaluationItemType.MEDIUM);
    }

    /** tovarni metoda pro velky ctverecek */
    public static EvaluationItem big(EvaluationItemValue value) {
        return new EvaluationItem(value, EvaluationItemType.BIG);
    }
}
