package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 * Podcast SubmissionItem, tvori seznam artefaktu
 * @author pepa
 */
public class ArtefactList extends FlowPanel {

    // komponenty
    private FlowPanel content;
    private ConsysMessage helpMessage;
    // data
    private String submissionTypeUuid;

    public ArtefactList(String submissionTypeUuid) {
        super();
        this.submissionTypeUuid = submissionTypeUuid;
        setStyleName(StyleUtils.MARGIN_TOP_10);
        setWidth("100%");

        Label titleLabel = StyleUtils.getStyledLabel(ECMessageUtils.c.const_artefacts(), StyleUtils.FONT_16PX);
        titleLabel.addStyleName(StyleUtils.FONT_BOLD);
        titleLabel.addStyleName(StyleUtils.TEXT_GRAY);
        titleLabel.addStyleName(StyleUtils.FLOAT_LEFT);
        titleLabel.addStyleName(StyleUtils.MARGIN_RIGHT_10);

        ActionLabel addArtefact = new ActionLabel(ECMessageUtils.c.artefactList_action_addArtefact(),
                StyleUtils.MARGIN_TOP_3 + " " + StyleUtils.FLOAT_LEFT);
        addArtefact.setClickHandler(addArtefactClickHandler());

        FlowPanel head = new FlowPanel();
        head.setStyleName(StyleUtils.MARGIN_TOP_10);
        head.add(titleLabel);
        head.add(addArtefact);
        head.add(StyleUtils.clearDiv());

        content = new FlowPanel() {

            @Override
            public boolean remove(Widget w) {
                boolean result = super.remove(w);
                if (result && getWidgetCount() == 0) {
                    helpMessage.setVisible(true);
                }
                return result;
            }
        };
        content.setWidth("100%");

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ECMessageUtils.c.artefactList_text_beginByAddingArtefact());
        helpMessage.setStyleName(StyleUtils.MARGIN_TOP_10);
        helpMessage.hideClose(true);

        add(head);
        add(content);
        add(helpMessage);
    }

    /** vraci seznam obsazenych typu artefaktu */
    public ArrayList<ClientArtefactTypeThumb> getClientArtefactTypeList() {
        ArrayList<ClientArtefactTypeThumb> list = new ArrayList<ClientArtefactTypeThumb>();
        for (int i = 0; i < content.getWidgetCount(); i++) {
            ArtefactItem item = (ArtefactItem) content.getWidget(i);
            list.add(item.getArtefactType());
        }
        return list;
    }

    /** nastavi seznam typu artefaktu */
    public void setClientArtefactTypeList(ArrayList<ClientArtefactTypeThumb> list) {
        if (list != null) {
            content.clear();
            for (ClientArtefactTypeThumb type : list) {
                content.add(new ArtefactItem(type));
            }
            helpMessage.setVisible(content.getWidgetCount() == 0);
        }
    }

    /** clickhandler pro pridani artefaktu */
    private ClickHandler addArtefactClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final NewItemDialog<NewItemUI> cd = new NewItemDialog<NewItemUI>(ECMessageUtils.c.artefactList_text_newArtefact(), helpMessage) {

                    @Override
                    public void doCreateAction(ArrayList<NewItemUI> uis, final ConsysMessage fail) {
                        for (NewItemUI ui : uis) {
                            if (!ui.form().validate(fail)) {
                                return;
                            }
                        }

                        final ArrayList<ClientArtefactTypeThumb> items = new ArrayList<ClientArtefactTypeThumb>();
                        for (NewItemUI ui : uis) {
                            ClientArtefactTypeThumb item = new ClientSettingsArtefactType();
                            item.setArtefactTypeName(ui.getName());
                            item.setArtefactTypeDescription(ui.getDescription());
                            items.add(item);
                        }

                        // vytvoreni na serveru
                        EventDispatchEvent addArtefactTypeEvent = new EventDispatchEvent(
                                new CreateArtefactTypeAction(submissionTypeUuid, items),
                                new AsyncCallback<ArrayListResult<String>>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne vyjimky zpracovava ActionExecutor
                                        if (caught instanceof NoRecordsForAction) {
                                            fail.setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                        }
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<String> result) {
                                        ArrayList<String> uuids = result.getArrayListResult();
                                        // vlozeni polozek
                                        for (int i = 0; i < items.size(); i++) {
                                            ClientArtefactTypeThumb item = items.get(i);
                                            item.setArtefactTypeUuid(uuids.get(i));
                                            content.add(new ArtefactItem(item));
                                        }
                                        successCreateAction();
                                    }
                                }, this);
                        EventBus.get().fireEvent(addArtefactTypeEvent);
                    }

                    @Override
                    public NewItemUI newItem() {
                        return new NewItemUI();
                    }
                };
                cd.showCentered();
            }
        };
    }
}
