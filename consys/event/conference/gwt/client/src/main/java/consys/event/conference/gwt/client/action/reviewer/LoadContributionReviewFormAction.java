package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData;

/**
 * Akce pro nacteni dat do formulare hodnoceni recenzenta
 * <p/>
 * @author pepa
 */
public class LoadContributionReviewFormAction extends EventAction<ClientContributionReviewerFormData> {

    private static final long serialVersionUID = -2067905143406851094L;
    // data    
    private String contributionUuid;
    private String reviewUuid;

    public LoadContributionReviewFormAction() {
    }

    public LoadContributionReviewFormAction(String contributionUuid, String reviewUuid) {
        this.contributionUuid = contributionUuid;
        this.reviewUuid = reviewUuid;
    }

    public String getReviewUuid() {
        return reviewUuid;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }
}
