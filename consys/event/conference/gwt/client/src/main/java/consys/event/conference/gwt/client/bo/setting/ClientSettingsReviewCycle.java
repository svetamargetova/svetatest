package consys.event.conference.gwt.client.bo.setting;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;
import java.util.Date;

/**
 * Recenzecni kolo artefaktu
 * @author pepa
 */
public class ClientSettingsReviewCycle extends ClientReviewCycleThumb implements Result {

    // konstanty
    private static final long serialVersionUID = -4819768743197090344L;
    // data
    private Date reviewFrom;
    private Date reviewTo;
    private ArrayList<String> assignedArtefacts;

    public ClientSettingsReviewCycle() {
        assignedArtefacts = new ArrayList<String>();
    }

    public ArrayList<String> getAssignedArtefacts() {
        return assignedArtefacts;
    }

    public void setAssignedArtefacts(ArrayList<String> assignedArtefacts) {
        this.assignedArtefacts = assignedArtefacts;
    }

    public Date getReviewFrom() {
        return reviewFrom;
    }

    public void setReviewFrom(Date reviewFrom) {
        this.reviewFrom = reviewFrom;
    }

    public Date getReviewTo() {
        return reviewTo;
    }

    public void setReviewTo(Date reviewTo) {
        this.reviewTo = reviewTo;
    }
}
