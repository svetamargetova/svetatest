package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;

/**
 * Akce pro aktualizaci kolecka hodnoceni
 * @author pepa
 */
public class UpdateReviewCycleAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = 486636026761105297L;
    // data
    private ClientSettingsReviewCycle reviewCycle;

    public UpdateReviewCycleAction() {
    }

    public UpdateReviewCycleAction(ClientSettingsReviewCycle reviewCycle) {
        this.reviewCycle = reviewCycle;
    }

    public ClientSettingsReviewCycle getReviewCycle() {
        return reviewCycle;
    }
}
