package consys.event.conference.gwt.client.bo.reviewer;

import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import java.util.ArrayList;

/**
 * Data pro zrecenzovani recenze + pripadne predesle hodnoceni
 * @author pepa
 */
public class ClientReviewReviewerData implements Result {

    private static final long serialVersionUID = -1804852375666284825L;
    // data
    private EvaluationValue score;
    private ArrayList<ClientReviewCriterion<Long>> criterias;
    private ArrayList<Long> enteredEvaluations;
    private String publicComment;
    private String privateComment;

    public ClientReviewReviewerData() {        
        enteredEvaluations = new ArrayList<Long>();
        criterias = new ArrayList<ClientReviewCriterion<Long>>();
    }

    public ArrayList<ClientReviewCriterion<Long>> getCriterias() {        
        return criterias;
    }

    public void setCriterias(ArrayList<ClientReviewCriterion<Long>> criterias) {
        this.criterias = criterias;
    }

    public ArrayList<Long> getEnteredEvaluations() {
        return enteredEvaluations;
    }

    public void setEnteredEvaluations(ArrayList<Long> enteredEvaluations) {
        this.enteredEvaluations = enteredEvaluations;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    public String getPublicComment() {
        return publicComment;
    }

    public void setPublicComment(String publicComment) {
        this.publicComment = publicComment;
    }

    public EvaluationValue getScore() {
        if (score == null) {
            return new EvaluationValue();
        }
        return score;
    }

    public void setScore(EvaluationValue score) {
        this.score = score;
    }
}
