package consys.event.conference.gwt.client.bo.contribution;

import consys.common.gwt.shared.bo.CommonThumb;

/**
 *
 * @author pepa
 */
public class ClientContributionTypeThumb extends CommonThumb {

    private String description;
    /** pouze interni typ prispevku */
    private boolean internal;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isInternal() {
        return internal;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }
}
