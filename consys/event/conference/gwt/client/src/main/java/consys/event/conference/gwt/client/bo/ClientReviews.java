package consys.event.conference.gwt.client.bo;

import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Prehled hodnoceni recenzentu pro jeden prispevek
 * @author pepa
 */
@Deprecated
public class ClientReviews implements Result {

    private static final long serialVersionUID = -8293823853001421452L;
    // data
    private ClientContributionHeadData head;
    private ArrayList<String> mainNotes;
    private ArrayList<ClientArtefactReviews> artefactNotes;

    public ClientReviews() {
        mainNotes = new ArrayList<String>();
        artefactNotes = new ArrayList<ClientArtefactReviews>();
    }

    public ArrayList<ClientArtefactReviews> getArtefactNotes() {
        return artefactNotes;
    }

    public void setArtefactNotes(ArrayList<ClientArtefactReviews> artefactNotes) {
        this.artefactNotes = artefactNotes;
    }

    public ClientContributionHeadData getHead() {
        return head;
    }

    public void setHead(ClientContributionHeadData head) {
        this.head = head;
    }

    public ArrayList<String> getMainNotes() {
        return mainNotes;
    }

    public void setMainNotes(ArrayList<String> mainNotes) {
        this.mainNotes = mainNotes;
    }
}
