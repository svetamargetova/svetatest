/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 * Event ktory je volany akonahle dojde k uspesnemu uploadu suboroveho artefaktu
 * na server.
 * 
 * @author palo
 */
public class UploadedArtefactCanceledEvent extends GwtEvent<UploadedArtefactCanceledEvent.Handler>{
   
    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();        
            
    private final String artefactUuid;
    private final String artefactTypeUuid;

    public UploadedArtefactCanceledEvent(String artefactUuid, String artefactTypeUuid) {        
        this.artefactUuid = artefactUuid;
        this.artefactTypeUuid = artefactTypeUuid;
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }
    

    public String getArtefactUuid() {
        return artefactUuid;
    }

    

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onCancel(this);
    }            
    
     /** interface eventu */
    public interface Handler extends EventHandler {

        public void onCancel(UploadedArtefactCanceledEvent event);
    }
}
