package consys.event.conference.gwt.client.module.contributor;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.event.conference.gwt.client.action.contribution.LoadContributionAction;
import consys.event.conference.gwt.client.bo.ClientContributionActionData;
import consys.event.conference.gwt.client.comp.ReviewScoreAndComments;
import consys.event.conference.gwt.client.module.contribution.item.EditableContributionHeadPanel;
import consys.event.conference.gwt.client.module.contributor.item.UpdateContributionArtefactPanel;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 *
 * @author palo
 */
public class UpdateContributionForm extends RootPanel {

    // data
    private String contributionUuid;

    public UpdateContributionForm(String title, String contributionUuid) {
        super(title);

        this.contributionUuid = contributionUuid;

        addStyleName(ECResourceUtils.css().newContributionForm());
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        EventBus.fire(new EventDispatchEvent(new LoadContributionAction(contributionUuid),
                new AsyncCallback<ClientContributionActionData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientContributionActionData result) {
                        initializeForm(result);
                    }
                }, UpdateContributionForm.this));
    }

    private void initializeForm(ClientContributionActionData data) {
        clear();

        // hlavicka       
        addWidget(new EditableContributionHeadPanel(data.getHead(), false));

        // recenze
        addWidget(new ReviewScoreAndComments(data.getReviews(), true, data.getHead()));

        // pridavani artefaktu        
        addWidget(new UpdateContributionArtefactPanel(data.getSelectedCycles(), data.getHead()));
    }
}
