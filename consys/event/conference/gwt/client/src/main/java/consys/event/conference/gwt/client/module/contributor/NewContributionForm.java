package consys.event.conference.gwt.client.module.contributor;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.action.ActionImageBig;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.HeadlinePanel;
import consys.common.gwt.client.ui.comp.panel.InfoPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.contribution.*;
import consys.event.conference.gwt.client.bo.ClientNewContributionActionData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.client.comp.ArtefactUpload;
import consys.event.conference.gwt.client.event.ArtefactUploadedEvent;
import consys.event.conference.gwt.client.event.UploadedArtefactCanceledEvent;
import consys.event.conference.gwt.client.module.contribution.item.ContributionHeadPanel;
import consys.event.conference.gwt.client.module.contribution.item.HeadPanelNewContributionInputFactory;
import consys.event.conference.gwt.client.module.contributor.item.NewContributionArtefactPanel;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;
import java.util.Date;

/**
 * Formular noveho prispevku.
 *
 * @author palo
 */
public class NewContributionForm extends RootPanel implements
        HeadPanelNewContributionInputFactory.ContributionTypeChangedHandler,
        ArtefactUploadedEvent.Handler,
        UploadedArtefactCanceledEvent.Handler {

    // komponenty
    private ClientContributionHeadData contributionHead;
    private ConsysFlowPanel artefactsPanel;
    private CreateContributionAction contributionAction;
    private HeadPanelNewContributionInputFactory inputFactory;
    private NewContributionArtefactPanel artefactPanel;
    private SimplePanel submitPanel;
    private ContributionHeadPanel headPanel;
    // data
    boolean internal;

    public NewContributionForm() {
        this(false);
    }

    public NewContributionForm(boolean internal) {
        super(ECMessageUtils.c.newContributionForm_title());
        this.internal = internal;

        addStyleName(ECResourceUtils.css().newContributionForm());

        contributionHead = new ClientContributionHeadData();
        submitPanel = new SimplePanel();
    }

    @Override
    protected void onUnload() {
        super.onUnload();
        EventBus.get().removeHandler(ArtefactUploadedEvent.TYPE, this);
        EventBus.get().removeHandler(UploadedArtefactCanceledEvent.TYPE, this);
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        EventBus.get().addHandler(ArtefactUploadedEvent.TYPE, this);
        EventBus.get().addHandler(UploadedArtefactCanceledEvent.TYPE, this);
        EventBus.get().fireEvent(new EventDispatchEvent(new LoadNewContributionAction(internal), new AsyncCallback<ClientNewContributionActionData>() {

            @Override
            public void onFailure(Throwable caught) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void onSuccess(ClientNewContributionActionData result) {
                initializeForm(result);
            }
        }, this));
    }

    @Override
    public void onArtefactUpload(ArtefactUploadedEvent event) {
        contributionAction.addFileArtefact(event.getArtefactTypeUuid(), event.getArtefactUuid());
    }

    @Override
    public void onCancel(UploadedArtefactCanceledEvent event) {
        contributionAction.removeFileArtefact(event.getArtefactUuid());
        EventBus.fire(new EventDispatchEvent(new DeleteUploadedContributionArtefactAction(contributionAction.getUuid(), event.getArtefactUuid(), event.getArtefactTypeUuid()), new AsyncCallback<VoidResult>() {

            @Override
            public void onFailure(Throwable caught) {
                // nezajem
            }

            @Override
            public void onSuccess(VoidResult result) {
                // nezajem
            }
        }, null));
    }

    private void initializeForm(ClientNewContributionActionData data) {
        clear();

        // nastavime do objektu
        contributionAction = new CreateContributionAction();
        contributionAction.setUuid(data.getContributionUuid());
        contributionAction.setContributionTypeUuid(data.getContributionTypeUuid());

        contributionHead.setUuid(data.getContributionUuid());

        // inicializujeme input factory aby vedela reagovat na zmemy                 
        inputFactory = new HeadPanelNewContributionInputFactory(data.getTypes());
        inputFactory.setAppendUserAsContributor(!internal);
        inputFactory.setChangeContributionTypeHandler(this);

        // hlavicka
        headPanel = new ContributionHeadPanel(contributionHead, inputFactory, true);
        headPanel.addStyleName(ECResourceUtils.css().newContributionFormHeadPanel());
        addWidget(headPanel);

        // Artefacts Upload
        artefactPanel = new NewContributionArtefactPanel(contributionHead, data.getSelectedCycles());
        addWidget(artefactPanel);

        // Submit
        allCyclesClosed(data.getSelectedCycles());
        addWidget(submitPanel);
    }

    private FlowPanel submitPanel() {
        ActionImageBig submit = ActionImageBig.blue(UIMessageUtils.c.const_submit());
        submit.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                submit();
            }
        });

        ActionImageBig clearForm = ActionImageBig.clear(UIMessageUtils.c.const_clearForm());
        clearForm.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                clearForm();
            }
        });

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ECResourceUtils.css().newContributionFormSubmitPanel());
        panel.add(new HeadlinePanel(ECMessageUtils.c.newContributionForm_text_submitPanel_title(), ECMessageUtils.c.newContributionForm_text_submitPanel_title(), ECMessageUtils.c.newContributionForm_text_submitPanel_description()));
        panel.add(submit);
        panel.add(clearForm);
        panel.add(StyleUtils.clearDiv());
        return panel;
    }

    private void submit() {
        
        if(!headPanel.validate()){
            return;
        }
        
        contributionAction.setTitle(inputFactory.getTitle());
        contributionAction.setSubtitle(inputFactory.getSubTitle());
        contributionAction.setAuthors(inputFactory.getAuthors());
        contributionAction.getContributors().clear();
        contributionAction.getContributors().addAll(inputFactory.getContributors());
        contributionAction.getTopics().clear();
        contributionAction.getTopics().addAll(inputFactory.getTopics());

        for (ArtefactUpload au : artefactPanel.getActiveTextArtefactsUpload()) {
            if (StringUtils.isNotBlank(au.getTextInput())) {
                LoggerFactory.log(NewContributionForm.class, "Processing text artefact: " + au.getTitle() + " with content:" + au.getTextInput());
                contributionAction.addTextArtefact(au.getClientArtefactType().getArtefactTypeUuid(), au.getTextInput());
            }
        }

        EventBus.fire(new EventDispatchEvent(contributionAction, new AsyncCallback<VoidResult>() {

            @Override
            public void onFailure(Throwable caught) {
                // server zatim nevyhazuje zadne nestandardni vyjimky, ktere se nezpracovavaji automatikcy
            }

            @Override
            public void onSuccess(VoidResult result) {
                UpdateContributionForm update = new UpdateContributionForm(contributionAction.getTitle(), contributionAction.getUuid());
                FormUtils.fireSameBreadcrumb(contributionAction.getTitle(), update);
            }
        }, NewContributionForm.this));
    }

    private void clearForm() {
        // vycistime head panel
        inputFactory.clear();

        // vycistime uploadnute artefakty
        contributionAction.getFileArtefacts().clear();

        // pregenerujeme
        artefactPanel.regenerate();

        // posleme info na server aby sa odstranili nahrate artefakty
        deleteAllUploadedArtefacts();
    }

    @Override
    public void handleChange(final ClientContributionTypeThumb type) {
        EventBus.get().fireEvent(new EventDispatchEvent(new LoadContributionTypeCyclesAction(type.getUuid()), new AsyncCallback<ArrayListResult<ClientReviewCycle>>() {

            @Override
            public void onFailure(Throwable caught) {
                // todo nejaky default
                if (caught instanceof NoRecordsForAction) {
                    allCyclesClosed(null);
                }
            }

            @Override
            public void onSuccess(ArrayListResult<ClientReviewCycle> result) {
                contributionAction.setContributionTypeUuid(type.getUuid());
                artefactPanel.setReviewCycles(result.getArrayListResult());

                // zrusime vsetky upladnute artefakty
                if (!contributionAction.getFileArtefacts().isEmpty()) {
                    contributionAction.getFileArtefacts().clear();
                    deleteAllUploadedArtefacts();
                }

                // kontrola jestli je otevreny nejaky cyklus
                allCyclesClosed(result.getArrayListResult());
            }
        }, artefactsPanel));
    }

    private void deleteAllUploadedArtefacts() {
        EventBus.get().fireEvent(new EventDispatchEvent(new DeleteAllUploadedContributionArtefactsAction(contributionAction.getUuid()), new AsyncCallback<VoidResult>() {

            @Override
            public void onFailure(Throwable caught) {
                // nezajima nas aj tak sa to automaticky potom zmaze
            }

            @Override
            public void onSuccess(VoidResult result) {
                // nezajima nas
            }
        }, null));
    }

    /** pokud jsou vsechny cykly uzavreny vypise hlasku, jinak zobrazi ovladaci tlacitka */
    private void allCyclesClosed(ArrayList<ClientReviewCycle> cycles) {
        boolean allClosed = true;

        if (cycles != null && !cycles.isEmpty()) {
            Date now = new Date();
            for (ClientReviewCycle c : cycles) {
                if (c.getFrom() == null && c.getTo() == null) {
                    // bez prispevek cyklu
                    allClosed = false;
                    break;
                }
                boolean open = now.after(c.getFrom()) && now.before(c.getTo()) && c.isEditable();
                if (open) {
                    allClosed = false;
                    break;
                }
            }
        }

        if (allClosed) {
            InfoPanel infoPanel = new InfoPanel(ECMessageUtils.c.newContributionForm_text_termClosed());
            submitPanel.setWidget(infoPanel);
        } else {
            submitPanel.setWidget(submitPanel());
        }
    }
}
