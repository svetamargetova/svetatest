package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.CreateCriteriaAction;
import consys.event.conference.gwt.client.action.setting.LoadCriteriaAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 * Podcast ReviewCycleItem, tvori seznam kriterii kolecka
 * @author pepa
 */
public class CriteriaList extends FlowPanel {

    // konstanty
    public static final String[] DEFAULT_POINTS = {
        ECMessageUtils.c.criteriaList_text_poor(),
        ECMessageUtils.c.criteriaList_text_tolerable(),
        ECMessageUtils.c.criteriaList_text_adequate(),
        ECMessageUtils.c.criteriaList_text_good(),
        ECMessageUtils.c.criteriaList_text_excellent()};
    public static final double MIN_POINTS = 2.5d;
    private static final String WIDTH = (ContributionTypeItem.WIDTH - 38) + "px";
    // komponenty
    private FlowPanel content;
    private ConsysMessage helpMessage;
    // data
    private String artefactUuid;

    public CriteriaList(String artefactUuid) {
        super();
        this.artefactUuid = artefactUuid;
        setStyleName(StyleUtils.MARGIN_BOT_10);
        setWidth(WIDTH);

        Label titleLabel = StyleUtils.getStyledLabel(ECMessageUtils.c.criteriaList_title(),
                StyleUtils.FONT_16PX, StyleUtils.FONT_BOLD, StyleUtils.TEXT_GRAY, StyleUtils.FLOAT_LEFT);

        ActionLabel addCriterion = new ActionLabel(ECMessageUtils.c.criteriaList_action_addCriterion(),
                StyleUtils.MARGIN_TOP_3 + " " + StyleUtils.MARGIN_LEFT_10 + " " + StyleUtils.FLOAT_LEFT);
        addCriterion.setClickHandler(addCriterionClickHandler());

        FlowPanel head = new FlowPanel();
        head.setStyleName(StyleUtils.MARGIN_TOP_10);
        head.add(titleLabel);
        head.add(addCriterion);
        head.add(StyleUtils.clearDiv());

        content = new FlowPanel() {

            @Override
            public boolean remove(Widget w) {
                boolean result = super.remove(w);
                if (result && getWidgetCount() == 0) {
                    helpMessage.setVisible(true);
                }
                return result;
            }
        };
        content.setWidth(WIDTH);

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ECMessageUtils.c.criteriaList_text_beginByAddingCriterion());
        helpMessage.setStyleName(StyleUtils.MARGIN_TOP_10);

        add(head);
        add(content);
        add(helpMessage);
    }

    /** vraci seznam obsazenych kriterii */
    public ArrayList<ClientCriteriaThumb> getClientCriteriaList() {
        ArrayList<ClientCriteriaThumb> list = new ArrayList<ClientCriteriaThumb>();
        for (int i = 0; i < content.getWidgetCount(); i++) {
            CriteriaItem item = (CriteriaItem) content.getWidget(i);
            list.add(item.getCriteria());
        }
        return list;
    }

    /** nastavuje seznam kriterii */
    public void setClientCriteriaList(ArrayList<ClientCriteriaThumb> list) {
        if (list != null) {
            content.clear();
            for (ClientCriteriaThumb criteria : list) {
                content.add(new CriteriaItem(criteria));
            }
            helpMessage.setVisible(content.getWidgetCount() == 0);
        }
    }

    /** vraci clickhandler pro kliknuti na addCriterion */
    private ClickHandler addCriterionClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                NewItemDialog<NewItemUI> cd = new NewItemDialog<NewItemUI>(ECMessageUtils.c.criteriaList_text_newCriterion(), helpMessage) {

                    @Override
                    public void doCreateAction(final ArrayList<NewItemUI> uis, final ConsysMessage fail) {
                        for (NewItemUI ui : uis) {
                            if (!ui.form().validate(fail)) {
                                return;
                            }
                        }

                        if (content.getWidgetCount() == 0) {
                            // prvni
                            ArrayList<String> points = new ArrayList<String>();
                            for (int i = 0; i < DEFAULT_POINTS.length; i++) {
                                points.add(DEFAULT_POINTS[i]);
                            }
                            insertItems(uis, fail);
                        } else {
                            // z predchoziho
                            CriteriaItem ci = (CriteriaItem) content.getWidget(content.getWidgetCount() - 1);
                            EventDispatchEvent loadEvent = new EventDispatchEvent(
                                    new LoadCriteriaAction(ci.getCriteria().getId()),
                                    new AsyncCallback<ClientCriteria>() {

                                        @Override
                                        public void onFailure(Throwable caught) {
                                            // obecne chyby zpracovava ActionExecutor
                                            if (caught instanceof NoRecordsForAction) {
                                                fail.setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                            }
                                        }

                                        @Override
                                        public void onSuccess(ClientCriteria result) {
                                            insertItems(uis, fail);
                                        }
                                    }, this);
                            EventBus.get().fireEvent(loadEvent);
                        }
                    }

                    @Override
                    public NewItemUI newItem() {
                        return new NewItemUI();
                    }

                    /** vytvori polozky na serveru a vlozi je do obsahu */
                    private void insertItems(ArrayList<NewItemUI> uis, final ConsysMessage fail) {
                        final ArrayList<ClientCriteriaThumbCreate> items = new ArrayList<ClientCriteriaThumbCreate>();
                        for (NewItemUI ui : uis) {
                            ClientCriteriaThumbCreate item = new ClientCriteriaThumbCreate();
                            item.setName(ui.getName());
                            item.setDescription(ui.getDescription());
                            items.add(item);
                        }

                        // vytvoreni na serveru
                        EventDispatchEvent criteriaEvent = new EventDispatchEvent(
                                new CreateCriteriaAction(artefactUuid, items),
                                new AsyncCallback<ArrayListResult<Long>>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava EventActionExcecutor
                                        if (caught instanceof NoRecordsForAction) {
                                            fail.setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                        }
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<Long> result) {
                                        ArrayList<Long> ids = result.getArrayListResult();
                                        // vlozeni polozek
                                        for (int i = 0; i < items.size(); i++) {
                                            ClientCriteriaThumbCreate tc = items.get(i);
                                            ClientCriteriaThumb item = new ClientCriteriaThumb();
                                            item.setId(ids.get(i));
                                            item.setName(tc.getName());
                                            content.add(new CriteriaItem(item));
                                        }
                                        successCreateAction();
                                    }
                                }, this);
                        EventBus.get().fireEvent(criteriaEvent);
                    }
                };
                cd.showCentered();
            }
        };
    }
}
