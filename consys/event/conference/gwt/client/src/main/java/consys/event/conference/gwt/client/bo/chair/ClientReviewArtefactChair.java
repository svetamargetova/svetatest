package consys.event.conference.gwt.client.bo.chair;

import consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import java.util.ArrayList;

/**
 * Jeden zrecenzovaný artefakt chaira
 * 
 * @author pepa
 */
public class ClientReviewArtefactChair extends ClientReviewArtefact {

    private static final long serialVersionUID = -4261744185105047581L;
    // data
    private ArrayList<ClientReviewCriterion<EvaluationValue>> criterias;
    private String contributionTypeUuid;

    public ClientReviewArtefactChair() {
        criterias = new ArrayList<ClientReviewCriterion<EvaluationValue>>();
    }

    public ClientReviewArtefactChair(String uuid, EvaluationValue totalScore, String name, ClientContributionArtefactDataType dataType) {
        this();
        setArtefactTypeUuid(uuid);
        setTotalScore(totalScore);
        setArtefactTypeName(name);
        setDataType(dataType);
    }

    public String getContributionTypeUuid() {
        return contributionTypeUuid;
    }

    public void setContributionTypeUuid(String contributionTypeUuid) {
        this.contributionTypeUuid = contributionTypeUuid;
    }

    public ArrayList<ClientReviewCriterion<EvaluationValue>> getCriterias() {
        return criterias;
    }
}
