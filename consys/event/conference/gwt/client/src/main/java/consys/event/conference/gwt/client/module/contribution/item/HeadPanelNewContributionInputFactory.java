package consys.event.conference.gwt.client.module.contribution.item;

import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;

/**
 *
 * @author palo
 */
public class HeadPanelNewContributionInputFactory extends HeadPanelUpdateInputFactory {
    
    private ArrayList<ClientContributionTypeThumb> contributionTypes;
    private ContributionTypeChangedHandler changeContributionTypeHandler;

    public HeadPanelNewContributionInputFactory(ArrayList<ClientContributionTypeThumb> contributionTypes) {
        this.contributionTypes = contributionTypes;
    }

    @Override
    public boolean showType() {
        return true;
    }
    
    
    
    @Override
    public Widget getContributionTypeWidget(ClientContributionHeadData data) {
        SimplePanel typesPanel = new SimplePanel();
        typesPanel.addStyleName(ECResourceUtils.css().newContributionFormHeadPanelContributionTypes());

        UListElement ul = Document.get().createULElement();
        for (int i = 0; i < contributionTypes.size(); i++) {
            ul.appendChild(createTypeListElement(contributionTypes.get(i), i == 0));
        }
        typesPanel.getElement().appendChild(ul);
        return typesPanel;
    }      

    private Element createTypeListElement(final ClientContributionTypeThumb type, boolean checked) {
        final InputElement ie = Document.get().createRadioInputElement("contribution-type");
        ie.setChecked(checked);

        SpanElement le = Document.get().createSpanElement();
        le.setInnerText(type.getName());

        LIElement li = Document.get().createLIElement();
        li.appendChild(ie);
        li.appendChild(le);
        DOM.setEventListener(li.<com.google.gwt.user.client.Element>cast(), new EventListener() {

            @Override
            public void onBrowserEvent(Event event) {
                if (event.getTypeInt() == Event.ONCLICK) {
                    ie.setChecked(true);
                    notifyAboutContributionChanged(type);
                }
            }
        });
        DOM.sinkEvents(li.<com.google.gwt.user.client.Element>cast(), Event.ONCLICK);
        return li;
    }

    private void notifyAboutContributionChanged(ClientContributionTypeThumb type) {
        if (getChangeContributionTypeHandler() != null) {
            getChangeContributionTypeHandler().handleChange(type);
        }
    }

    public ContributionTypeChangedHandler getChangeContributionTypeHandler() {
        return changeContributionTypeHandler;
    }

    public void setChangeContributionTypeHandler(ContributionTypeChangedHandler changeContributionTypeHandler) {
        this.changeContributionTypeHandler = changeContributionTypeHandler;
    }

    public interface ContributionTypeChangedHandler {

        public void handleChange(ClientContributionTypeThumb type);
    }
}
