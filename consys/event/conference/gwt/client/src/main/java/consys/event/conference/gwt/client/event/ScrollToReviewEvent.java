package consys.event.conference.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Akce ktera necha prescrollovat na recenzi artefaktu i s rozklikem
 * @author pepa
 */
public class ScrollToReviewEvent extends GwtEvent<ScrollToReviewEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private String artefactUuid;

    public ScrollToReviewEvent(String artefactUuid) {
        this.artefactUuid = artefactUuid;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.scrollToReview(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void scrollToReview(ScrollToReviewEvent event);
    }
}
