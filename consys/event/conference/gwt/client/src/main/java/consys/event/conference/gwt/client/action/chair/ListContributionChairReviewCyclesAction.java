package consys.event.conference.gwt.client.action.chair;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;

/**
 * Akce pro dotazeni cyklu hodnoceni pro dany prispevek
 * @author pepa
 */
public class ListContributionChairReviewCyclesAction extends EventAction<ArrayListResult<ClientReviewCycle>> {

    private static final long serialVersionUID = -7743713082400793794L;
    // data
    private String contributionUuid;

    public ListContributionChairReviewCyclesAction() {
    }

    public ListContributionChairReviewCyclesAction(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }
}
