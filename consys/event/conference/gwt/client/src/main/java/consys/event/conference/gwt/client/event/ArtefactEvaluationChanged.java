package consys.event.conference.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;

/**
 * Event vystreleny kdyz se zmeni hodnoceni artefaktu
 * @author pepa
 */
public class ArtefactEvaluationChanged extends GwtEvent<ArtefactEvaluationChanged.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private EvaluationValue value;

    public ArtefactEvaluationChanged(EvaluationValue value) {
        this.value = value;
    }

    public EvaluationValue getValue() {
        return value;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onArtefactEvaluationChanged(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onArtefactEvaluationChanged(ArtefactEvaluationChanged event);
    }
}
