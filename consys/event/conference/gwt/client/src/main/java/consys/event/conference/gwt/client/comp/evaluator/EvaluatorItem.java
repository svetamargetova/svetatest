package consys.event.conference.gwt.client.comp.evaluator;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItem.EvaluationItemType;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 *
 * @author pepa
 */
public class EvaluatorItem<T> extends Composite {

    // komponenty
    private FocusPanel panel;
    private Label label;
    private EvaluationItem score;
    private SimplePanel textPanel;
    private SimplePanel endPanel;
    // data
    private final EvaluatorItemData<T> data;
    private boolean isBottom;
    private boolean enabled;
    private HandlerRegistration clickRegistration;
    private HandlerRegistration overRegistration;
    private HandlerRegistration outRegistration;

    public EvaluatorItem(EvaluatorItemData<T> data, boolean isLastItem) {
        this.data = data;
        this.enabled = true;

        isBottom = isLastItem;
        label = new Label();

        panel = new FocusPanel();
        panel.setStyleName(ECResourceUtils.css().evaluatorHead());
        handlers();
        if (!data.isIsValue()) {
            panel.addMouseOverHandler(ActionLabel.inverseMouseOverHandler(label));
            panel.addMouseOutHandler(ActionLabel.inverseMouseOutHandler(label));
        }
        initWidget(panel);
    }

    @Override
    protected void onLoad() {
        generateContent();
    }

    /** odregistruje aktualni handlery a pokud je komponenta enablovana, zaregistruje nove */
    private void handlers() {
        removeRegistration(clickRegistration);
        removeRegistration(overRegistration);
        removeRegistration(outRegistration);

        if (enabled) {
            clickRegistration = panel.addClickHandler(itemClickHandler());
            overRegistration = panel.addMouseOverHandler(overHandler());
            outRegistration = panel.addMouseOutHandler(outHandler());
        }
    }

    /** odebere registraci handleru */
    private void removeRegistration(HandlerRegistration registration) {
        if (registration != null) {
            registration.removeHandler();
        }
    }

    private void generateContent() {
        panel.clear();

        EvaluationItemType type = isBottom ? EvaluationItemType.BOTTOM : EvaluationItemType.MIDDLE;
        score = new EvaluationItem(data.getScore(), type, data.getScore().isStartFromOne());
        score.addStyleName(StyleUtils.FLOAT_LEFT);

        label.setText(data.getName());

        textPanel = new SimplePanel();
        textPanel.setStyleName(ECResourceUtils.css().evaluatorItemText());
        textPanel.addStyleName(isBottom ? ECResourceUtils.css().evaluatorItemBottom() : ECResourceUtils.css().evaluatorItemMiddle());
        textPanel.add(label);

        endPanel = new SimplePanel();
        endPanel.setStyleName(ECResourceUtils.css().evaluatorItemEnd());
        endPanel.setWidget(new Image(isBottom
                ? ECResourceUtils.bundle().evaluatorItemTextBottomEnd()
                : ECResourceUtils.bundle().evaluatorItemTextMiddleEnd()));

        FlowPanel content = new FlowPanel();
        content.add(score);
        content.add(textPanel);
        content.add(endPanel);
        content.add(StyleUtils.clearDiv());

        panel.setWidget(content);
    }

    public EvaluatorItemData<T> getData() {
        return data;
    }

    private ClickHandler itemClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                data.getAction().run(data);
            }
        };
    }

    private MouseOverHandler overHandler() {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                textPanel.setStyleName(ECResourceUtils.css().evaluatorItemText());
                textPanel.addStyleName(isBottom ? ECResourceUtils.css().evaluatorItemBottomOver() : ECResourceUtils.css().evaluatorItemMiddleOver());
                endPanel.setWidget(new Image(isBottom
                        ? ECResourceUtils.bundle().evaluatorItemTextBottomEndOver()
                        : ECResourceUtils.bundle().evaluatorItemTextMiddleEndOver()));
            }
        };
    }

    private MouseOutHandler outHandler() {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                textPanel.setStyleName(ECResourceUtils.css().evaluatorItemText());
                textPanel.addStyleName(isBottom ? ECResourceUtils.css().evaluatorItemBottom() : ECResourceUtils.css().evaluatorItemMiddle());
                endPanel.setWidget(new Image(isBottom
                        ? ECResourceUtils.bundle().evaluatorItemTextBottomEnd()
                        : ECResourceUtils.bundle().evaluatorItemTextMiddleEnd()));
            }
        };
    }

    /** enabluje komponentu */
    public void setEnabled(boolean enabled) {
        if(this.enabled == enabled) {
            return;
        }
        this.enabled = enabled;
        if (enabled) {
            panel.removeStyleName(ECResourceUtils.css().disabled());
        } else {
            StyleUtils.addStyleNameIfNotSet(panel, ECResourceUtils.css().disabled());
        }
        handlers();
    }
}
