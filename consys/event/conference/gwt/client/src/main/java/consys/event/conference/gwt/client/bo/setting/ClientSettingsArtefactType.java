package consys.event.conference.gwt.client.bo.setting;


import consys.event.conference.gwt.client.bo.ClientArtefactType;
import java.util.ArrayList;

/**
 * Artefakt prispevku
 * @author pepa
 */
public class ClientSettingsArtefactType extends ClientArtefactType {

    // konstanty
    private static final long serialVersionUID = 5393770740986386447L;
    // data
    private int numReviewers;
    private ArrayList<ClientCriteriaThumb> criteriaList;
    
    public ClientSettingsArtefactType() {
        criteriaList = new ArrayList<ClientCriteriaThumb>();    
    }

    public ArrayList<ClientCriteriaThumb> getCriteriaList() {
        return criteriaList;
    }

    public void setCriteriaList(ArrayList<ClientCriteriaThumb> criteriaList) {
        this.criteriaList = criteriaList;
    }

    
    public int getNumReviewers() {
        return numReviewers;
    }

    public void setNumReviewers(int numReviewers) {
        this.numReviewers = numReviewers;
    }  
}
