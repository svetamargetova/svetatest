package consys.event.conference.gwt.client.bo.contribution;

import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb;
import java.util.ArrayList;

/**
 * Simuluje db objekt SubmissionType + castecne SubmissionArtefactType
 * @author pepa
 */
public class ClientContributionType extends ClientContributionTypeThumb implements Result {

    // konstanty
    private static final long serialVersionUID = -5970654675658703268L;
    // data
    private ArrayList<ClientArtefactTypeThumb> clientArtefactTypeList;
    private ArrayList<ClientReviewCycleThumb> clientReviewCycleList;

    public ClientContributionType() {
        clientArtefactTypeList = new ArrayList<ClientArtefactTypeThumb>();
        clientReviewCycleList = new ArrayList<ClientReviewCycleThumb>();
    }

    public ArrayList<ClientArtefactTypeThumb> getClientArtefactTypeList() {
        return clientArtefactTypeList;
    }

    public void setClientArtefactTypeList(ArrayList<ClientArtefactTypeThumb> clientArtefactTypeList) {
        this.clientArtefactTypeList = clientArtefactTypeList;
    }

    public ArrayList<ClientReviewCycleThumb> getClientReviewCycleList() {
        return clientReviewCycleList;
    }

    public void setClientReviewCycleList(ArrayList<ClientReviewCycleThumb> clientReviewCycleList) {
        this.clientReviewCycleList = clientReviewCycleList;
    }
}
