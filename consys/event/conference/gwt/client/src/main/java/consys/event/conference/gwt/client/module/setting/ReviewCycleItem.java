package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysAsyncAction;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.RollOutPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.DeleteReviewCycleAction;
import consys.event.conference.gwt.client.action.setting.LoadReviewCycleAction;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCycleException;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;
import java.util.Date;

/**
 * Jedna polozka kolecka
 * @author pepa
 */
public class ReviewCycleItem extends RollOutPanel implements ActionExecutionDelegate {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(ReviewCycleItem.class);
    // komponenty
    private Label titleLabel;
    private HTML submissionHTML;
    private HTML acceptanceHTML;
    private HTML artefactsHTML;
    private FlowPanel failPanel;
    // data
    private ClientSettingsReviewCycle rc;
    private ConsysAction updateAction;
    private ArtefactList artefactList;

    public ReviewCycleItem(final ClientReviewCycleThumb crct, ConsysAction updateAction, ArtefactList artefactList) {
        super();
        this.updateAction = updateAction;
        this.artefactList = artefactList;
        setStyleName(StyleUtils.MARGIN_TOP_10);
        setWidth("100%");
        enableWaiting();

        rc = new ClientSettingsReviewCycle();
        rc.setId(crct.getId());
        rc.setSubmitFrom(crct.getSubmitFrom());
        rc.setSubmitTo(crct.getSubmitTo());
        if (crct instanceof ClientSettingsReviewCycle) {
            ClientSettingsReviewCycle crc = (ClientSettingsReviewCycle) crct;
            rc.setReviewFrom(crc.getReviewFrom());
            rc.setReviewTo(crc.getReviewTo());
        }

        // hlavicka
        titleLabel = StyleUtils.getStyledLabel(getTitleLabel(rc.getSubmitFrom(), rc.getSubmitTo()), RollOutPanel.TITLE_TEXT_STYLE);

        ConsysAction editAction = new ConsysAction() {

            @Override
            public void run() {
                EditFormReviewCycle w = new EditFormReviewCycle(rc.getId(), ReviewCycleItem.this, getArtefactThumbs());
                FormUtils.fireNextBreadcrumb(ECMessageUtils.c.editFormReviewCycle_title(), w);
            }
        };

        ConsysAction removeAction = new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent removeEvent = new EventDispatchEvent(new DeleteReviewCycleAction(crct.getId()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby obstarava ActionExecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                } else if (caught instanceof DeleteReviewCycleException) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionDeleteReviewCycle());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                ReviewCycleItem.this.removeFromParent();
                            }
                        }, ReviewCycleItem.this);
                EventBus.get().fireEvent(removeEvent);
            }
        };

        EditWithRemover ewr = new EditWithRemover(editAction, removeAction);
        ewr.atRight();

        failPanel = new FlowPanel();

        FlowPanel titlePanel = new FlowPanel();
        titlePanel.addStyleName(StyleUtils.FLOAT_LEFT);
        titlePanel.add(titleLabel);
        titlePanel.add(failPanel);

        String headWidth = (ContributionTypeItem.WIDTH - RollOutPanel.LEFT_PART_WIDTH) + "px";

        FlowPanel head = new FlowPanel();
        head.setWidth(headWidth);
        head.add(ewr);
        head.add(titlePanel);
        head.add(StyleUtils.clearDiv());

        setTitle(head);
        setMainPanelWidth(headWidth);

        // obsah
        submissionHTML = new HTML(submissionText());
        acceptanceHTML = new HTML(acceptanceText());
        artefactsHTML = new HTML(artefactsText());

        FlowPanel content = new FlowPanel();
        content.setWidth((ContributionTypeItem.WIDTH - RollOutPanel.LEFT_PART_WIDTH) + "px");
        content.setStyleName(StyleUtils.MARGIN_TOP_10);
        content.add(submissionHTML);
        content.add(acceptanceHTML);
        content.add(artefactsHTML);

        setContent(content);
    }

    /** vraci text s datumy podani submission */
    private String submissionText() {
        TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
        String text = "<div>" + ECMessageUtils.c.editFormReviewCycle_form_submissionPeriod() + ": <b>";
        text += DateTimeUtils.getDateAndTime(rc.getSubmitFrom(), tz) + "</b> " + UIMessageUtils.c.const_toLower() + " <b>";
        text += DateTimeUtils.getDateAndTime(rc.getSubmitTo(), tz) + "</b></div>";
        return text;
    }

    /** vraci text s datumy hodnoceni */
    private String acceptanceText() {
        TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
        String text = "<div>" + ECMessageUtils.c.reviewCycle_form_reviewPeriod() + ": <b>";
        text += DateTimeUtils.getDateAndTime(rc.getReviewFrom(), tz) + "</b> " + UIMessageUtils.c.const_toLower() + " <b>";
        text += DateTimeUtils.getDateAndTime(rc.getReviewTo(), tz) + "</b></div>";
        return text;
    }

    /** vraci text s prirazenymi artefakty */
    private String artefactsText() {
        String text = ECMessageUtils.c.const_artefacts() + ": ";
        if (rc.getAssignedArtefacts().isEmpty()) {
            text += FormUtils.MDASH;
        } else {
            ArrayList<CommonThumb> thumbs = getArtefactThumbs();
            for (String uuid : rc.getAssignedArtefacts()) {
                for (CommonThumb t : thumbs) {
                    if (t.getUuid().equalsIgnoreCase(uuid)) {
                        text += t.getName() + ", ";
                    }
                }
            }
            if (text.endsWith(", ")) {
                text = text.substring(0, text.length() - 2);
            }
        }
        return text;
    }

    /** vraci ClientSettingsReviewCycle */
    public ClientSettingsReviewCycle getReviewCycle() {
        return rc;
    }

    /** nastavuje ClientSettingsReviewCycle */
    public void setReviewCycle(ClientSettingsReviewCycle rc) {
        this.rc = rc;
        titleLabel.setText(getTitleLabel(rc.getSubmitFrom(), rc.getSubmitTo()));
        submissionHTML.setHTML(submissionText());
        acceptanceHTML.setHTML(acceptanceText());
        artefactsHTML.setHTML(artefactsText());
        updateAction.run();
    }

    /** vraci chybovou zpravu */
    private ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.setStyleName(StyleUtils.MARGIN_TOP_10);
        failMessage.setVisible(false);
        failPanel.add(failMessage);
        return failMessage;
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    @Override
    protected void onOpenRoll(final ConsysAsyncAction onAction) {
        EventDispatchEvent loadEvent = new EventDispatchEvent(new LoadReviewCycleAction(rc.getId()),
                new AsyncCallback<ClientSettingsReviewCycle>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava ActionExecutor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientSettingsReviewCycle result) {
                        logger.info("incoming cycle dates from " + result.getSubmitFrom() + " to " + result.getSubmitTo());
                        setReviewCycle(result);
                        onAction.onSuccess();
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);
    }

    /** vygeneruje text rozsahu z datumu */
    private String getTitleLabel(Date from, Date to) {
        TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
        String result = from == null ? ECMessageUtils.c.const_notSet() : DateTimeUtils.getMonthDayYear(from, tz);
        result += FormUtils.UNB_SPACE + FormUtils.NDASH + FormUtils.UNB_SPACE;
        result += to == null ? ECMessageUtils.c.const_notSet() : DateTimeUtils.getMonthDayYear(to, tz);
        return result;
    }

    /** vraci datum od kdy zasilat (pro potreby razeni) */
    public Date getSubmitFrom() {
        return rc.getSubmitFrom();
    }

    /** vraci aktualni thumby artefaku */
    private ArrayList<CommonThumb> getArtefactThumbs() {
        ArrayList<ClientArtefactTypeThumb> art = artefactList.getClientArtefactTypeList();
        ArrayList<CommonThumb> thumbs = new ArrayList<CommonThumb>();
        for (ClientArtefactTypeThumb t : art) {
            thumbs.add(new CommonThumb(t.getArtefactTypeUuid(), t.getArtefactTypeName()));
        }
        return thumbs;
    }
}
