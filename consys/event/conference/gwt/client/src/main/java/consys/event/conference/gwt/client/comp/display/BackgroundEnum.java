package consys.event.conference.gwt.client.comp.display;

import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 *
 * @author pepa
 */
public enum BackgroundEnum {

    // enum
    VALUE_1(ECResourceUtils.css().smallEvaluation1(), ECResourceUtils.css().mediumEvaluation1(), ECResourceUtils.css().bigEvaluation1(), ECResourceUtils.css().middleEvaluation1(), ECResourceUtils.css().bottomEvaluation1()),
    VALUE_2(ECResourceUtils.css().smallEvaluation2(), ECResourceUtils.css().mediumEvaluation2(), ECResourceUtils.css().bigEvaluation2(), ECResourceUtils.css().middleEvaluation2(), ECResourceUtils.css().bottomEvaluation2()),
    VALUE_3(ECResourceUtils.css().smallEvaluation3(), ECResourceUtils.css().mediumEvaluation3(), ECResourceUtils.css().bigEvaluation3(), ECResourceUtils.css().middleEvaluation3(), ECResourceUtils.css().bottomEvaluation3()),
    VALUE_4(ECResourceUtils.css().smallEvaluation4(), ECResourceUtils.css().mediumEvaluation4(), ECResourceUtils.css().bigEvaluation4(), ECResourceUtils.css().middleEvaluation4(), ECResourceUtils.css().bottomEvaluation4()),
    VALUE_5(ECResourceUtils.css().smallEvaluation5(), ECResourceUtils.css().mediumEvaluation5(), ECResourceUtils.css().bigEvaluation5(), ECResourceUtils.css().middleEvaluation5(), ECResourceUtils.css().bottomEvaluation5()),
    VALUE_6(ECResourceUtils.css().smallEvaluation6(), ECResourceUtils.css().mediumEvaluation6(), ECResourceUtils.css().bigEvaluation6(), ECResourceUtils.css().middleEvaluation6(), ECResourceUtils.css().bottomEvaluation6()),
    VALUE_7(ECResourceUtils.css().smallEvaluation7(), ECResourceUtils.css().mediumEvaluation7(), ECResourceUtils.css().bigEvaluation7(), ECResourceUtils.css().middleEvaluation7(), ECResourceUtils.css().bottomEvaluation7()),
    VALUE_8(ECResourceUtils.css().smallEvaluation8(), ECResourceUtils.css().mediumEvaluation8(), ECResourceUtils.css().bigEvaluation8(), ECResourceUtils.css().middleEvaluation8(), ECResourceUtils.css().bottomEvaluation8()),
    VALUE_9(ECResourceUtils.css().smallEvaluation9(), ECResourceUtils.css().mediumEvaluation9(), ECResourceUtils.css().bigEvaluation9(), ECResourceUtils.css().middleEvaluation9(), ECResourceUtils.css().bottomEvaluation9()),
    VALUE_10(ECResourceUtils.css().smallEvaluation10(), ECResourceUtils.css().mediumEvaluation10(), ECResourceUtils.css().bigEvaluation10(), ECResourceUtils.css().middleEvaluation10(), ECResourceUtils.css().bottomEvaluation10()),
    VALUE_NONE(ECResourceUtils.css().smallEvaluationNone(), ECResourceUtils.css().mediumEvaluationNone(), ECResourceUtils.css().bigEvaluationNone(), ECResourceUtils.css().middleEvaluationNone(), ECResourceUtils.css().bottomEvaluationNone());
    // konstanty
    private static final long serialVersionUID = -4096554277173615041L;
    private static int[][] ORDER_COLORS = {
        {0, 9},
        {0, 5, 9},
        {0, 3, 6, 9},
        {0, 2, 5, 7, 9},
        {0, 2, 4, 5, 7, 9},
        {0, 2, 3, 4, 5, 7, 9},
        {0, 2, 3, 4, 5, 6, 7, 9},
        {0, 2, 3, 4, 5, 6, 7, 8, 9}};
    // data
    private String styleSmall;
    private String styleMedium;
    private String styleBig;
    private String styleMiddle;
    private String styleBottom;

    BackgroundEnum(String styleSmall, String styleMedium, String styleBig, String middle, String bottom) {
        this.styleSmall = styleSmall;
        this.styleMedium = styleMedium;
        this.styleBig = styleBig;
        this.styleMiddle = middle;
        this.styleBottom = bottom;
    }

    public String getStyleBig() {
        return styleBig;
    }

    public String getStyleMedium() {
        return styleMedium;
    }

    public String getStyleSmall() {
        return styleSmall;
    }

    public String getStyleMiddle() {
        return styleMiddle;
    }

    public String getStyleBottom() {
        return styleBottom;
    }

    public static BackgroundEnum color(EvaluationItemValue value, boolean startFromOne) {
        int integer = startFromOne ? value.getInteger() - 1 : value.getInteger();
        if (value.isNotEntered()) {
            return VALUE_NONE;
        }

        if (value.getMaxValue() == EvaluationItemValue.MAX_VALUE) {
            return byId(integer);
        } else {
            try {
                int id = ORDER_COLORS[value.getMaxValue() - 2][integer];
                return byId(id);
            } catch (IndexOutOfBoundsException ex) {
                return VALUE_NONE;
            }
        }
    }

    private static BackgroundEnum byId(int id) {
        switch (id) {
            case 0:
                return VALUE_1;
            case 1:
                return VALUE_2;
            case 2:
                return VALUE_3;
            case 3:
                return VALUE_4;
            case 4:
                return VALUE_5;
            case 5:
                return VALUE_6;
            case 6:
                return VALUE_7;
            case 7:
                return VALUE_8;
            case 8:
                return VALUE_9;
            case 9:
                return VALUE_10;
            case 10:
                return VALUE_10;
            default:
                return VALUE_NONE;
        }
    }
}
