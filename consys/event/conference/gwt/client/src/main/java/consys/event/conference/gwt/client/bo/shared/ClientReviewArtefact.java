package consys.event.conference.gwt.client.bo.shared;

import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;

/**
 * Recenzovany artefakt
 *
 * @author pepa
 */
public class ClientReviewArtefact extends ClientArtefactType {

    private static final long serialVersionUID = -2484714782018676102L;
    /**
     * UUID Artefaktu
     */
    private String artefactUuid;
    /**
     * Nazov suboru
     */
    private String fileName;
    /**
     * Celkove skore recenzie artefaktu - priemer kriterii
     */
    private EvaluationValue totalScore;

    /**
     * Celkove skore recenzie artefaktu - priemer kriterii
     */
    public EvaluationValue getTotalScore() {
        if (totalScore == null) {
            return new EvaluationValue();
        }
        return totalScore;
    }

    /**
     * Celkove skore recenzie artefaktu - priemer kriterii
     */
    public void setTotalScore(EvaluationValue totalScore) {
        this.totalScore = totalScore;
    }

    /**
     * UUID Artefaktu
     *
     * @return the artefactUuid
     */
    public String getArtefactUuid() {
        return artefactUuid;
    }

    /**
     * UUID Artefaktu
     *
     * @param artefactUuid the artefactUuid to set
     */
    public void setArtefactUuid(String artefactUuid) {
        this.artefactUuid = artefactUuid;
    }

    /**
     * Nazov suboru
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Nazov suboru
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void initializeType(ClientArtefactType cat) {
        setArtefactTypeDescription(cat.getArtefactTypeDescription());
        setArtefactTypeName(cat.getArtefactTypeName());
        setArtefactTypeUuid(cat.getArtefactTypeUuid());
        setAutoAccepted(cat.isAutoAccepted());
        setDataType(cat.getDataType());
        setFileFormats(cat.getFileFormats());
        setMaxInputLength(cat.getMaxInputLength());
        setRequired(cat.isRequired());
        setUploadedExamples(cat.getUploadedExamples());
        setUploadedTemplates(cat.getUploadedTemplates());
    }
}
