package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.ArrayList;

/**
 * Akce pro aktualizaci uzivatelovych topiku
 * @author pepa
 */
public class UpdateReviewerTopicsAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 4741995447072509768L;
    // data
    private ArrayList<Long> userTopics;

    public UpdateReviewerTopicsAction() {
        userTopics = new ArrayList<Long>();
    }

    public ArrayList<Long> getUserTopics() {
        return userTopics;
    }
}
