package consys.event.conference.gwt.client.bo.list;

import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ClientListContributionItem extends DataListUuidItem {

    // konstanty
    private static final long serialVersionUID = -7594588797241949762L;
    // data
    private ClientContributionHeadData overview;
    private Date decidedDate;

    public ClientContributionHeadData getOverview() {
        return overview;
    }

    public void setOverview(ClientContributionHeadData overview) {
        this.overview = overview;
    }

    /**
     * @return the decidedDate
     */
    public Date getDecidedDate() {
        return decidedDate;
    }

    /**
     * @param decidedDate the decidedDate to set
     */
    public void setDecidedDate(Date decidedDate) {
        this.decidedDate = decidedDate;
    }
}
