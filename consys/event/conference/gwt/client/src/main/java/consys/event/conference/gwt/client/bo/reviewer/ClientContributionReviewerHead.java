package consys.event.conference.gwt.client.bo.reviewer;

import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;

/**
 * Data pro recenzenci formular recenzenta
 * @author pepa
 */
public class ClientContributionReviewerHead extends ClientContributionHeadData {

    private static final long serialVersionUID = -3343139836113037364L;
    // data
    private String reviewUuid;
    private String reviewerUuid; 
    private String reviewerName; 

    public String getReviewUuid() {
        return reviewUuid;
    }

    public void setReviewUuid(String reviewUuid) {
        this.reviewUuid = reviewUuid;
    }

    /**
     * @return the reviewerName
     */
    public String getReviewerName() {
        return reviewerName;
    }

    /**
     * @param reviewerName the reviewerName to set
     */
    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    /**
     * @return the reviewerUuid
     */
    public String getReviewerUuid() {
        return reviewerUuid;
    }

    /**
     * @param reviewerUuid the reviewerUuid to set
     */
    public void setReviewerUuid(String reviewerUuid) {
        this.reviewerUuid = reviewerUuid;
    }
}
