package consys.event.conference.gwt.client.module.contribution.item;

import com.google.code.gwt.component.tag.InputTag;
import com.google.code.gwt.component.tag.SelectBoxInputTag;
import com.google.code.gwt.component.tag.StringTag;
import com.google.code.gwt.component.tag.Tag;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.form.FormResources;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.common.gwt.client.ui.comp.input.Validable;
import consys.common.gwt.client.ui.comp.input.ValidableItem;
import consys.common.gwt.client.ui.comp.input.validator.StringValidator;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.comp.wrapper.InputTextWrapper;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.conference.gwt.client.action.contribution.ListTopicAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributor;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author palo
 */
public class HeadPanelUpdateInputFactory implements ContributionHeadPanel.InputFactory {

    private Logger logger = LoggerFactory.getLogger(HeadPanelUpdateInputFactory.class);
    // konstanty
    private static final int COMPONENT_WIDTH = 500;
    // data
    private boolean appendUserAsContributor;
    private ConsysTextBox titleTextBox;
    private ConsysTextBox subtitleTextBox;
    private InputTag<StringTag> authorsInputTag;
    private SelectBoxInputTag<TopicTag> topicsSelectBoxInputTag;
    private List<ClientInvitedUser> contributors;
    private ConsysFlowPanel contributorsPanel;

    public HeadPanelUpdateInputFactory() {
    }

    public String getTitle() {
        logger.info("Title: " + titleTextBox.getText());
        return titleTextBox.getText();
    }

    public String getSubTitle() {
        logger.info("SubTitle: " + subtitleTextBox.getText());
        return subtitleTextBox.getText();
    }

    public String[] getAuthors() {
        List<? extends Tag> authorsTags = authorsInputTag.getTags();
        logger.info("Authors: " + authorsTags.size());
        String[] authors = new String[authorsTags.size()];
        for (int i = 0; i < authorsTags.size(); i++) {
            logger.info(" + " + authorsTags.get(i).getTag());
            authors[i] = authorsTags.get(i).getTag();
        }
        return authors;
    }

    public List<Long> getTopics() {
        logger.info("Topics: ");
        List<TopicTag> topicTags = topicsSelectBoxInputTag.getTags();
        List<Long> topics = new ArrayList<Long>(topicTags.size());
        for (TopicTag topicTag : topicTags) {
            logger.info(" + " + topicTag.getValue().getId());
            topics.add(topicTag.getValue().getId());
        }

        return topics;
    }

    public List<ClientInvitedUser> getContributors() {
        logger.info("Contributors: ");
        for (ClientInvitedUser clientInvitedUser : contributors) {
            logger.info(" + " + clientInvitedUser.getName());
        }
        return contributors;
    }

    public void clear() {
        titleTextBox.setText("");
        subtitleTextBox.setText("");
        authorsInputTag.clear();
        topicsSelectBoxInputTag.clear();
        contributors.clear();
        contributorsPanel.clear();

        // ak je appendUserAsContributor
        if (appendUserAsContributor) {
            appendCurrentUserAsContributor();
        }


    }

    @Override
    public Widget getTitleWidget(ClientContributionHeadData data, ConsysAction clickAction) {
        titleTextBox = new ConsysTextBox(0, 255, COMPONENT_WIDTH);
        titleTextBox.setText(data.getTitle());
        ((StringValidator)titleTextBox.getValidators().get(0)).setNotEnteredText(ECMessageUtils.c.contributionHeadPanel_error_noTitleEntered());       
        return new ValidableItem(titleTextBox);
    }

    @Override
    public Widget getSubtitleWidget(ClientContributionHeadData data) {
        subtitleTextBox = new ConsysTextBox(0, 255, COMPONENT_WIDTH);
        subtitleTextBox.setText(data.getSubtitle());
        return subtitleTextBox;
    }

    @Override
    public Widget getAuthorsWidget(ClientContributionHeadData data) {                        
        createAuthorsInputTag(data);                        
        Validable v = new Validable() {

            @Override
            public String validate() {
                if(authorsInputTag.getTags().isEmpty()){
                    return ECMessageUtils.c.contributionHeadPanel_error_noAuthorsEntered();
                }
                return null;
            }

            @Override
            public Widget asWidget() {
                return inputTextWrapper(authorsInputTag, COMPONENT_WIDTH);
            }
        };                
        return new ValidableItem(v);
    }

    protected InputTag<StringTag> createAuthorsInputTag(ClientContributionHeadData data) {
        if (StringUtils.isNotBlank(data.getAuthors())) {
            List<StringTag> list = new ArrayList<StringTag>();
            String[] authors = data.getAuthors().split(",");
            for (int i = 0; i < authors.length; i++) {
                list.add(new StringTag(authors[i], authors[i]));
            }
            authorsInputTag = new InputTag<StringTag>(list);
        } else {
            authorsInputTag = new InputTag<StringTag>();
        }
        authorsInputTag.setAllowWhiteSpaceInTag(true);
        authorsInputTag.setMode(InputTag.Mode.WRITE);
        return authorsInputTag;
    }

    @Override
    public Widget getContributorsWidget(ClientContributionHeadData data) {
        contributors = new ArrayList<ClientInvitedUser>();
        contributorsPanel = new ConsysFlowPanel();


        ActionLabel addSubmitter = new ActionLabel(ECMessageUtils.c.submission_action_addContributor());
        addSubmitter.setHeight("18px");
        addSubmitter.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog sd = new UserSearchDialog();
                sd.setDelegate(new SearchDelegate<ClientInvitedUser>() {

                    @Override
                    public void didSelect(ClientInvitedUser item) {
                        appendContributor(item, true);
                    }
                });
                sd.showCentered();
            }
        });

        if (data.getContributors() != null && !data.getContributors().isEmpty()) {
            for (ClientContributor contributor : data.getContributors()) {
                appendContributor(new ClientInvitedUser(contributor.getUuid(), null, contributor.getName()), true);
            }
        } else if (appendUserAsContributor) {
            appendCurrentUserAsContributor();
        }

        final FlowPanel panel = new FlowPanel();
        panel.add(addSubmitter);
        panel.add(contributorsPanel);
        panel.add(StyleUtils.clearDiv());
        panel.addStyleName(StyleUtils.MARGIN_BOT_10);
        
        
         Validable v = new Validable() {

            @Override
            public String validate() {
                if(contributorsPanel.getWidgetCount() == 0){
                    return ECMessageUtils.c.contributionHeadPanel_error_noContributorsEntered();
                }
                return null;
            }

            @Override
            public Widget asWidget() {
                return panel;
            }
        };                
        return new ValidableItem(v);                                       
    }

    private void appendCurrentUserAsContributor() {
        // prida aktualniho uzivatele automaticky a nesmazatelne
        try {
            ClientUser cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
            // pridame do contributoru
            appendContributor(new ClientInvitedUser(cu.getUuid(), null, cu.name()), !appendUserAsContributor);

            // pridame do autorov
            Tag currentUserTag = new Tag(cu.name(), cu.name());            
            authorsInputTag.appendTag(currentUserTag);

        } catch (NotInCacheException ex) {
            // uzivatel neni v cache, ale neni tu zasadni problem
        }
    }

    private void appendContributor(ClientInvitedUser ciu, boolean canRemove) {
        contributorsPanel.add(new ContributorEditPanel(ciu, canRemove));
        contributors.add(ciu);
    }

    @Override
    public Widget getTopicsWidget(final ClientContributionHeadData data) {
        createTopicsSelectBoxInputTag(data);                        
        Validable v = new Validable() {
            @Override
            public String validate() {
                if(!topicsSelectBoxInputTag.getSelectBoxTags().isEmpty() && topicsSelectBoxInputTag.getTags().isEmpty()){
                    return ECMessageUtils.c.contributionHeadPanel_error_noTopicsSelected();
                }
                return null;
            }

            @Override
            public Widget asWidget() {                
                FlowPanel fp = new FlowPanel();
                fp.setStyleName(ECResourceUtils.css().tags());
                fp.add(inputTextWrapper(topicsSelectBoxInputTag, COMPONENT_WIDTH));
                fp.getElement().appendChild(topicsSelectBoxInputTag.getElement().getChild(2));                                
                return fp;
            }
        };                
        return new ValidableItem(v);                                
    }

    protected InputTextWrapper inputTextWrapper(Widget w, int width) {                                        
        FormResources.INSTANCE.formCss().ensureInjected();
        InputTextWrapper wrapper = new InputTextWrapper(w, width);
        wrapper.addStyleName(FormResources.INSTANCE.formCss().inputWrapper());                        
        return wrapper;
    }

    protected SelectBoxInputTag<TopicTag> createTopicsSelectBoxInputTag(final ClientContributionHeadData data) {
        topicsSelectBoxInputTag = new SelectBoxInputTag<TopicTag>();

        EventBus.get().fireEvent(new EventDispatchEvent(new ListTopicAction(), new AsyncCallback<ArrayListResult<ClientTopic>>() {

            @Override
            public void onFailure(Throwable caught) {
                
            }

            @Override
            public void onSuccess(ArrayListResult<ClientTopic> result) {
                List<TopicTag> topicTags = new ArrayList<TopicTag>(result.getArrayListResult().size());
                String[] cTopics = null;
                if (StringUtils.isNotBlank(data.getTopics())) {
                    cTopics = data.getTopics().split(",");
                }

                for (ClientTopic topic : result.getArrayListResult()) {
                    topicTags.add(new TopicTag(topic));
                    if (cTopics != null) {
                        for (int i = 0; i < cTopics.length; i++) {
                            if (topic.getName().equalsIgnoreCase(cTopics[i].trim())) {
                                topicsSelectBoxInputTag.appendTag(new TopicTag(topic));
                            }
                        }
                    }
                }
                topicsSelectBoxInputTag.setSelectBoxTags(topicTags);
            }
        }, null));

        return topicsSelectBoxInputTag;
    }

    @Override
    public Widget getContributionTypeWidget(ClientContributionHeadData data) {
        return FormV2.getValueWidget(data.getType(), null);
    }

    public void setAppendUserAsContributor(boolean appendUserAsContributor) {
        this.appendUserAsContributor = appendUserAsContributor;
    }

    @Override
    public boolean showEvaluationDisplay() {
        return false;
    }

    @Override
    public boolean showState() {
        return false;
    }

    @Override
    public boolean showSubTitle() {
        return true;
    }

    @Override
    public boolean showType() {
        return false;
    }

    public class TopicTag extends Tag<ClientTopic> {

        private static final long serialVersionUID = -8597724307624830768L;

        public TopicTag(ClientTopic t) {
            super(t, t.getName());
        }
    }
} 
