package consys.event.conference.gwt.client.module.contributor;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.client.comp.ContributionsListItem;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 *
 * @author pepa
 */
public class MyContributionListItem extends ContributionsListItem {

    public MyContributionListItem(final ClientListContributionItem data) {
        super(data, true, true, new ConsysAction() {

            @Override
            public void run() {
                showDetails(data);
            }
        });
    }

    private ClickHandler showDetailsClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showDetails(getData());
            }
        };
    }

    private static void showDetails(ClientListContributionItem data) {
        UpdateContributionForm uc = new UpdateContributionForm(data.getOverview().getTitle(), data.getOverview().getUuid());
        FormUtils.fireNextBreadcrumb(data.getOverview().getTitle(), uc);
    }

    @Override
    protected void createActionButtons() {
        addControl(ECMessageUtils.c.const_showDetails(), showDetailsClickHandler());
    }
}
