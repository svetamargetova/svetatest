package consys.event.conference.gwt.client.module.setting.upload;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Dialog;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Dialog pro upload a spravu uploadovanych polozek
 * @author pepa
 */
public class UploadArtefactDialog extends Dialog implements ActionExecutionDelegate {

    // komponenty
    private FlowPanel mainPanel;
    private Waiting waiting;
    private FlowPanel failPanel;
    private SimplePanel mainContent;
    private SimplePanel page;
    private SelectPage pageSelect;
    private CreatePage pageCreate;
    // data
    private final FlowPanel target;
    private final boolean isTemplate;
    private final String title;

    public UploadArtefactDialog(FlowPanel target, boolean isTemplate, String title) {
        super();
        this.target = target;
        this.isTemplate = isTemplate;
        this.title = title;

        mainPanel = new FlowPanel();
        mainPanel.setWidth(DEFAULT_WIDTH_PX);
        waiting = new Waiting(mainPanel);
        mainPanel.add(waiting);
        failPanel = new FlowPanel();
        mainPanel.add(failPanel);
        mainContent = new SimplePanel();
        mainPanel.add(mainContent);
    }

    @Override
    protected Widget createContent() {
        mainContent.setWidget(initContent());
        return mainPanel;
    }

    private FlowPanel initContent() {
                
        FlowPanel left = new FlowPanel();
        left.addStyleName(ECResourceUtils.bundle().css().uaLeft());

        final MenuPanel existingItem = new MenuPanel(ECMessageUtils.c.uploadArtefactDialog_text_existingFiles());
        pageSelect = new SelectPage(isTemplate, this);

        final MenuPanel uploadItem = new MenuPanel(ECMessageUtils.c.uploadArtefactDialog_text_uploadNewFile());
        pageCreate = new CreatePage(isTemplate, pageSelect, this);

        existingItem.select();
        existingItem.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                existingItem.select();
                uploadItem.unselect();
                page.setWidget(pageSelect);
            }
        });
        left.add(existingItem);

        uploadItem.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                uploadItem.select();
                existingItem.unselect();
                page.setWidget(pageCreate);
            }
        });
        left.add(uploadItem);

        FlowPanel right = new FlowPanel();
        right.addStyleName(ECResourceUtils.bundle().css().uaRight());

        page = new SimplePanel();
        page.setWidget(pageSelect);
        right.add(page);

        // hlavicka nesuca nazov akcie 
        final FlowPanel head = new FlowPanel();
        head.setStyleName(ECResourceUtils.css().artefactSettingsUploadFileHead());
        head.add(new Label(title));
        head.add(Separator.separator());
                
        FlowPanel wrapper = new FlowPanel();
        wrapper.add(head);
        wrapper.add(left);
        wrapper.add(right);
        wrapper.add(StyleUtils.clearDiv());
        return wrapper;
    }

    void showSelect() {
        page.setWidget(pageSelect);
    }

    FlowPanel getTarget() {
        return target;
    }

    ConsysMessage getFailMessage() {
        ConsysMessage fail = ConsysMessage.getFail();
        fail.addStyleName(StyleUtils.MARGIN_CENTER);
        fail.addStyleName(StyleUtils.MARGIN_VER_5);
        failPanel.add(fail);
        return fail;
    }

    private void showWaiting(boolean value) {
        if (value) {
            waiting.show(mainPanel);
        } else {
            waiting.hide();
        }
    }

    @Override
    public void actionStarted() {
        showWaiting(true);
    }

    @Override
    public void actionEnds() {
        showWaiting(false);
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }
}
