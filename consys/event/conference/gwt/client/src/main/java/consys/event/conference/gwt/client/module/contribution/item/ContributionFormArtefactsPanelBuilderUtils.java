package consys.event.conference.gwt.client.module.contribution.item;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.InfoPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem;
import consys.event.conference.gwt.client.comp.PreviewEvaluatorWrapper;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.comp.evaluator.ClientPreview;
import consys.event.conference.gwt.client.comp.evaluator.ClientPreviews;
import consys.event.conference.gwt.client.module.setting.ContributionTypeContent;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;

/**
 *
 * @author palo
 */
public class ContributionFormArtefactsPanelBuilderUtils {

    public static void addArtefactOverallReview(final ClientReviewArtefactChair cra, FormV2 form,
            String contributionUuid, String contributionTitle, boolean isChair) {
        EvaluationItemValue value = new EvaluationItemValue(cra.getTotalScore());
        value.setHelpTitle(ECMessageUtils.c.reviewCycles_text_artefactScoreTitle());
        value.setHelpText(ECMessageUtils.c.reviewCycles_text_artefactScoreText());
        EvaluationItem totalItem = EvaluationItem.medium(value);

        ArrayList<ClientPreviews> previews = new ArrayList<ClientPreviews>();
        for (ClientReviewCriterion<EvaluationValue> crc : cra.getCriterias()) {
            previews.add(createClientPreview(crc));
        }

        PreviewEvaluatorWrapper preview = new PreviewEvaluatorWrapper(contributionUuid, contributionTitle);
        preview.setPreviews(previews);

        if (cra.isAutoAccepted() || !cra.getCriterias().isEmpty()) {
            form.add(createTotalItemWrapper(totalItem, false), preview, false);
        } else {
            InfoPanel infoPanel;
            if (isChair) {
                infoPanel = new InfoPanel(ECMessageUtils.c.reviewerEvaluation_text_noEvaluationCriterias(),
                        ECMessageUtils.c.contributionFormArtefactsPanelBuilderUtils_action_setCriterias(),
                        new ConsysAction() {

                            @Override
                            public void run() {
                                Cache.get().registerSelectedTab(ConsysTabPanel.cachePanelName(ContributionTypeContent.class.getName()),
                                        cra.getContributionTypeUuid());
                                ContributionTypeContent w = new ContributionTypeContent();
                                FormUtils.fireNextBreadcrumb(ECMessageUtils.c.conferenceModule_text_navigationModelSettings(), w);
                            }
                        });
            } else {
                infoPanel = new InfoPanel(ECMessageUtils.c.reviewerEvaluation_text_noEvaluationCriterias());
            }
            form.addValueWidget(infoPanel);
        }
    }

    public static FlowPanel createTotalItemWrapper(EvaluationItem item, boolean displayWrapperStyle) {
        item.addStyleName(ECResourceUtils.css().displayInWrapper());

        FlowPanel p = new FlowPanel();
        if (displayWrapperStyle) {
            p.setStyleName(ECResourceUtils.css().displayWrapper());
        }
        p.add(item);
        p.add(StyleUtils.clearDiv());
        return p;
    }

    public static ClientPreviews createClientPreview(ClientReviewCriterion<EvaluationValue> crc) {
        EvaluationItemValue value = new EvaluationItemValue(crc.getValue());
        ClientPreviews cp = new ClientPreviews(value, crc.getName(), crc.getDescription(), crc.getMaxPoints());

        for (ClientReviewCriterionItem<EvaluationValue> crcr : crc.getItems()) {
            EvaluationValue v = crcr.getValue();
            if (v == null) {
                v = new EvaluationValue();
            }
            EvaluationItemValue item = new EvaluationItemValue(v);
            cp.getReviewers().add(new ClientPreview(crcr.getReviewUuid(), item, crcr.getName()));
        }
        return cp;
    }
}
