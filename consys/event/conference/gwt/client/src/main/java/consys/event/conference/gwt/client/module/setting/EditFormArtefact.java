package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ConsysIntTextBox;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction;
import consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCriteriaException;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType;
import consys.event.conference.gwt.client.module.setting.upload.UploadArtefactDialog;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Editacni formular pro nastaveni Artefact
 *
 * @author pepa
 */
public class EditFormArtefact extends RootPanel {

    // konstanty
    private static final String WIDTH = "320px";
    private static final String TXT = "txt";
    // komponenty
    private SimpleFormWithHelpPanel mainPanel;
    private BaseForm form;
    private ConsysStringTextBox nameBox;
    private ConsysTextArea descBox;
    private SelectBox<Integer> reviewersBox;
    private ConsysIntTextBox maxInputBox;
    private RadioButtonPanel necessityPanel;
    private RadioButtonPanel documentTypePanel;
    private ConsysStringTextBox fileFormatsBox;
    private FlowPanel templatePanel;
    private FlowPanel examplePanel;
    // data
    private ClientSettingsArtefactType cat;
    private ArtefactItem source;

    public EditFormArtefact(String uuid, final ArtefactItem source) {
        super(ECMessageUtils.c.editFormArtefact_title_editArtefact());
        this.source = source;

        mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        // dotazeni aktualnich dat
        EventDispatchEvent loadEvent = new EventDispatchEvent(new LoadArtefactTypeAction(uuid),
                new AsyncCallback<ClientSettingsArtefactType>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava ActionExecutor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientSettingsArtefactType result) {
                        setTitleText(ECMessageUtils.c.editFormArtefact_title_editArtefact() + " " + result.getArtefactTypeName());
                        cat = result;
                        source.setArtefactType(result);
                        init();
                    }
                }, this);
        EventBus.get().fireEvent(loadEvent);

        //mainPanel.addHelpTitle("Help title");
        //mainPanel.addHelpText("Help text");
    }

    private void init() {
        form = new BaseForm("200px");
        form.setStyleName(StyleUtils.MARGIN_TOP_10);

        nameBox = new ConsysStringTextBox(1, 255, UIMessageUtils.c.const_name());
        nameBox.setText(cat.getArtefactTypeName());
        nameBox.setWidth(WIDTH);

        descBox = new ConsysTextArea(0, 255, UIMessageUtils.c.const_description());
        descBox.setText(cat.getArtefactTypeDescription());
        descBox.setWidth(WIDTH);

        ArrayList<SelectBoxItem<Integer>> items = new ArrayList<SelectBoxItem<Integer>>();
        items.add(new SelectBoxItem<Integer>(0, ECMessageUtils.c.artefact_text_automaticAccept()));
        for (int i = 1; i <= 23; i++) {
            items.add(new SelectBoxItem<Integer>(i, String.valueOf(i)));
        }

        reviewersBox = new SelectBox<Integer>();
        reviewersBox.setItems(items);
        reviewersBox.selectItemByIndex(cat.getNumReviewers());
        reviewersBox.setWidth(320);

        maxInputBox = new ConsysIntTextBox(WIDTH, 0, 4000, ECMessageUtils.c.editFormArtefact_form_maxInputWords());
        maxInputBox.setText(cat.getMaxInputLength());

        fileFormatsBox = new ConsysStringTextBox(0, 255, ECMessageUtils.c.artefact_text_fileTypes());
        fileFormatsBox.setText(cat.getFileFormats());
        fileFormatsBox.setWidth(WIDTH);

        ConsysFlowPanel fileFormatsPanel = new ConsysFlowPanel();
        fileFormatsPanel.add(fileFormatsBox);
        fileFormatsPanel.add(StyleUtils.getStyledLabel(ECMessageUtils.c.editFormArtefact_text_fileFormatDelimiterHelp(), FONT_11PX, TEXT_GRAY));

        ConsysFlowPanel maxInputPanel = new ConsysFlowPanel();
        maxInputPanel.add(maxInputBox);
        maxInputPanel.add(StyleUtils.getStyledLabel(ECMessageUtils.c.editFormArtefact_text_zeroIsUnlimited(), FONT_11PX, TEXT_GRAY));

        necessityPanel = new RadioButtonPanel(cat.isRequired(), ECMessageUtils.c.editFormArtefact_text_required(), ECMessageUtils.c.editFormArtefact_text_optional(), "g1");
        documentTypePanel = new RadioButtonPanel(cat.isFile(), ECMessageUtils.c.editFormArtefact_text_fileUpload(), ECMessageUtils.c.editFormArtefact_text_direct(), "g2");
        documentTypePanel.addFirstRBHandler(documentTypeFileHandler());
        documentTypePanel.addSecondRBHandler(documentTypeTextareaHandler());

        

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        form.addRequired(0, UIMessageUtils.c.const_name(), nameBox);
        form.addOptional(1, UIMessageUtils.c.const_description(), descBox);
        form.addRequired(2, ECMessageUtils.c.editFormArtefact_form_numReviewers(), reviewersBox);
        form.addRequired(3, ECMessageUtils.c.editFormArtefact_form_necessity(), necessityPanel);
        form.addRequired(4, ECMessageUtils.c.editFormArtefact_form_documentType(), documentTypePanel);
        form.addOptional(5, ECMessageUtils.c.artefact_text_fileTypes(), fileFormatsPanel);
        form.addOptional(6, ECMessageUtils.c.editFormArtefact_form_maxInputWords(), maxInputPanel);
        form.addOptional(7, ECMessageUtils.c.editFormArtefact_form_uploadTemplate(), createFilePanel(true));
        form.addOptional(8, ECMessageUtils.c.editFormArtefact_form_uploadExample(), createFilePanel(false));
        form.addActionMembers(9, button, ActionLabel.breadcrumbBackCancel(), WIDTH);
        mainPanel.addWidget(form);

        if (documentTypePanel.isSelectFirst()) {
            form.showRow(5, true);
            form.showRow(6, false);
        } else {
            form.showRow(5, false);
            form.showRow(6, true);
        }
    }

    private ValueChangeHandler<Boolean> documentTypeFileHandler() {
        return new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                // funguje stejne jen na true, ale pro jistotu
                if (event.getValue()) {
                    form.showRow(5, true);
                    form.showRow(6, false);
                }
            }
        };
    }

    private ValueChangeHandler<Boolean> documentTypeTextareaHandler() {
        return new ValueChangeHandler<Boolean>() {

            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                // funguje stejne jen na true, ale pro jistotu
                if (event.getValue()) {
                    form.showRow(5, false);
                    form.showRow(6, true);
                }
            }
        };
    }

    private Widget createFilePanel(final boolean template) {
        
        final String labelText = template ? ECMessageUtils.c.editFormArtefact_action_addTemplate() : ECMessageUtils.c.editFormArtefact_action_addExample();
        final FlowPanel filePanel = new FlowPanel();
        if (template) {
            templatePanel = filePanel;
        } else {
            examplePanel = filePanel;
        }

        // Odkaz na pridanie noveho panela
        ActionLabel addNewFile = new ActionLabel(labelText);
        addNewFile.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new UploadArtefactDialog(filePanel, template,labelText).showCentered();
            }
        });
        
        // nainicializovane uz pridanych
        List<CommonThumb> files = template ? cat.getUploadedTemplates() : cat.getUploadedExamples();
        for (CommonThumb ct : files) {
            filePanel.add(new UploadFormItem(ct));
        }        

        // vytvorenie finalneho
        final FlowPanel wrapper = new FlowPanel();
        wrapper.add(addNewFile);
        wrapper.add(filePanel);
        wrapper.add(StyleUtils.clearDiv());
        return wrapper;
    }

   

    /** potvrzovaci clickhandler */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (documentTypePanel.isSelectFirst()) {
                    maxInputBox.setText(0);
                }
                // validace
                if (!form.validate(getFailMessage())) {
                    return;
                }
                if (documentTypePanel.isSelectFirst()) {
                    // je vybrany soubor, tak musi byt nastaveny povolene pripony
                    String[] formats = fileFormatsBox.getText().trim().split(",");
                    boolean valid = formats.length > 0;
                    for (String s : formats) {
                        if (s.trim().isEmpty()) {
                            valid = false;
                        }
                    }
                    if (!valid) {
                        getFailMessage().setText(ECMessageUtils.m.editFormArtefact_error_invalidNotation(ECMessageUtils.c.artefact_text_fileTypes()));
                        return;
                    }
                }

                final UpdateArtefactTypeAction action = prepareUpdateAction();

                EventDispatchEvent updateEvent = new EventDispatchEvent(action,
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava ActionExecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.editFormArtefact_error_problemWithTemplateOrPattern());
                                }
                                if (caught instanceof DeleteReviewCriteriaException) {
                                    // tady se nic nemaze takze by nemel byt problem
                                    getFailMessage().setText("DeleteReviewCriteriaException");
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                // aktualizace dat
                                cat.setArtefactTypeName(action.getName());
                                cat.setArtefactTypeDescription(action.getDescription());
                                cat.setNumReviewers(action.getNumReviewers());
                                cat.setRequired(action.isRequired());
                                cat.setDataType(action.isFile() ? ClientContributionArtefactDataType.FILE : ClientContributionArtefactDataType.TEXT);
                                cat.setFileFormats(action.getFileFormats());
                                cat.setMaxInputLength(action.getMaxInputLength());

                                cat.getUploadedTemplates().clear();
                                for (int i = 0; i < templatePanel.getWidgetCount(); i++) {
                                    Widget w = templatePanel.getWidget(i);
                                    if (w instanceof UploadFormItem) {
                                        cat.getUploadedTemplates().add(((UploadFormItem) w).getItem());
                                    }
                                }

                                cat.getUploadedExamples().clear();
                                for (int i = 0; i < examplePanel.getWidgetCount(); i++) {
                                    Widget w = examplePanel.getWidget(i);
                                    if (w instanceof UploadFormItem) {
                                        cat.getUploadedExamples().add(((UploadFormItem) w).getItem());
                                    }
                                }
                                // aktualizace zobrazovaciho formulare
                                source.setArtefactType(cat);
                                // navrat na predchozi formular
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, EditFormArtefact.this);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }

    /** nastavi data do aktualizacni akce */
    private UpdateArtefactTypeAction prepareUpdateAction() {
        String format = documentTypePanel.isSelectFirst() ? fileFormatsBox.getText().trim() : TXT;

        UpdateArtefactTypeAction action = new UpdateArtefactTypeAction();
        action.setUuid(cat.getArtefactTypeUuid());
        action.setName(nameBox.getText().trim());
        action.setDescription(descBox.getText().trim());
        action.setNumReviewers(reviewersBox.getSelectedItem().getItem());
        action.setRequired(necessityPanel.isSelectFirst());
        action.setFile(documentTypePanel.isSelectFirst());
        action.setFileFormats(format);
        action.setMaxInputLength(maxInputBox.getTextInt());

        action.getUploadedTemplates().clear();
        for (int i = 0; i < templatePanel.getWidgetCount(); i++) {
            Widget w = templatePanel.getWidget(i);
            if (w instanceof UploadFormItem) {
                action.getUploadedTemplates().add(((UploadFormItem) w).getItem().getUuid());
            }
        }

        action.getUploadedExamples().clear();
        for (int i = 0; i < examplePanel.getWidgetCount(); i++) {
            Widget w = examplePanel.getWidget(i);
            if (w instanceof UploadFormItem) {
                action.getUploadedExamples().add(((UploadFormItem) w).getItem().getUuid());
            }
        }
        return action;
    }

    /** pomocny objekt se dvema radio buttony a jejich popisky */
    private class RadioButtonPanel extends ConsysFlowPanel {

        private RadioButton first;
        private RadioButton second;

        public RadioButtonPanel(boolean selectFirst, String firstLabel, String secondLabel, String group) {
            super();
            setHeight("20px");

            first = new RadioButton(group);
            first.setValue(selectFirst);
            first.addStyleName(FLOAT_LEFT);
            add(first);

            Label fl = StyleUtils.getStyledLabel(firstLabel, MARGIN_LEFT_20, FLOAT_LEFT);
            fl.setWidth("120px");
            add(fl);

            second = new RadioButton(group);
            second.setValue(!selectFirst);
            second.addStyleName(FLOAT_LEFT);
            add(second);

            Label sl = StyleUtils.getStyledLabel(secondLabel, MARGIN_LEFT_20, FLOAT_LEFT);
            sl.setWidth("120px");
            add(sl);
        }

        /** vraci true pokud je vybrany prvni radiobutton */
        public boolean isSelectFirst() {
            return first.getValue();
        }

        /** prida handler po vyberu prvniho radiobuttonu */
        public void addFirstRBHandler(ValueChangeHandler<Boolean> handler) {
            first.addValueChangeHandler(handler);
        }

        /** prida handler po vyberu druheho radiobuttonu */
        public void addSecondRBHandler(ValueChangeHandler<Boolean> handler) {
            second.addValueChangeHandler(handler);
        }

        /** disabluje radiobuttony */
        public void disable() {
            first.setEnabled(false);
            second.setEnabled(false);
        }

        /** enabluje radiobuttony */
        public void enable() {
            first.setEnabled(true);
            second.setEnabled(true);
        }
    }
}
