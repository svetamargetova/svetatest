package consys.event.conference.gwt.client.bo.setting;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Objekt ktery se pouziva pri vytvareni kriteria
 * @author pepa
 */
public class ClientCriteriaThumbCreate implements IsSerializable {

    private String name;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
