package consys.event.conference.gwt.client.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface EventConferenceConstants extends Constants {

    String addPublicAndPrivateComment_text_addPrivateComment();

    String addPublicAndPrivateComment_text_addPublicComment();

    String addPublicAndPrivateComment_text_updateComments();

    String artefact_text_automaticAccept();

    String artefact_text_fileTypes();

    String artefactFileUpload_error_artefactUploadFailed();

    String artefactItem_text_uploadedExamples();

    String artefactItem_text_uploadedTemplates();

    int artefactItem_title_width();

    String artefactList_action_addArtefact();

    String artefactList_text_beginByAddingArtefact();

    String artefactList_text_newArtefact();

    String artefactUpload_action_change();

    String artefactUpload_action_remove();

    int artefactUpload_int_textBoxWidth();

    String artefactUpload_text_confirmTitle();

    String assignmentReviewersForm_text_assignConflictTitle();

    String assignmentReviewersForm_text_assignLikeTitle();

    String assignmentReviewersForm_text_assignNotCareTitle();

    String assignmentReviewersForm_text_assignNotWantTitle();

    String assignmentReviewersForm_text_nobody();

    String bidForReviewForm_action_interest();

    String bidForReviewForm_text_conflict();

    String bidForReviewForm_text_dislike();

    String bidForReviewForm_text_like();

    String bidForReviewForm_text_notMind();

    String bidForReviewForm_title();

    String chairContributionsListItem_action_assignReviewers();

    int changeArtefactUpload_button_width();

    String conferenceModule_text_moduleName();

    String conferenceModule_text_moduleRightReviewer();

    String conferenceModule_text_moduleRightReviewerDescription();

    String conferenceModule_text_moduleRightAddInternSubmissions();

    String conferenceModule_text_moduleRightAddInternSubmissionsDescription();

    String conferenceModule_text_moduleRightSubmissionSettings();

    String conferenceModule_text_moduleRightSubmissionSettingsDescription();

    String conferenceModule_text_moduleRightAssignsSubmissionReviewers();

    String conferenceModule_text_moduleRightAssignsSubmissionReviewersDescription();

    String conferenceModule_text_moduleRightAcceptSubmissions();

    String conferenceModule_text_moduleRightAcceptSubmissionsDescription();

    String conferenceModule_text_navigationModelSubmission();

    String conferenceModule_text_navigationModelMySubmissions();

    String conferenceModule_text_navigationModelMyReviews();

    String conferenceModule_text_navigationModelPrefferedTopics();

    String conferenceModule_text_navigationModelBidForReview();

    String conferenceModule_text_navigationModelSettings();

    String conferenceModule_text_navigationModelContributions();

    String conferenceModule_text_navigationModelAddSubmission();

    String conferencePropertyPanel_property_maxReviewsPerReviewer();

    String conferencePropertyPanel_property_visibleAuthors();

    String const_actionUrlFail();

    String const_addTopic();

    String const_artefacts();

    String const_authors();

    String const_authorsNotVisible();

    String const_automaticallyAccepted();

    String const_comment();

    String const_commentHelpText();

    String const_contributors();

    String const_evaluationTerm();

    String const_exceptionDeleteRelation();

    String const_exceptionDeleteReviewCriteria();

    String const_exceptionDeleteReviewCycle();

    String const_exceptionNoRecordsForAction();

    String const_exceptionNotInCache();

    String const_exceptionSubmissionTypeUnique();

    String const_exceptionTopicUnique();

    String const_file();

    String const_noSubtitle();

    String const_notSet();

    String const_privateComment();

    String const_privateCommentHelpText();

    String const_privateNote();

    String const_publicComment();

    String const_publicCommentHelpText();

    String const_publicNote();

    String const_review();

    String const_score();

    String const_showDetails();

    String const_showReview();

    String const_state();

    String const_state_accepted();

    String const_state_declined();

    String const_state_pending();

    String const_submitDate();

    String const_submitContribution();

    String const_subtitle();

    String const_title();

    String const_topics();

    String const_type();

    String contributionArtefactsChairReviewPanel_text_noArtefacts();

    String contributionChairForm_text_assignReviewers();

    String contributionChairForm_text_evaluateThisReview();

    String contributionChairForm_text_noReviewers();

    String contributionEvaluationReviewer_title();

    String contributionFormArtefactsPanelBuilder_text_noArtefactInCycle();

    String contributionFormArtefactsPanelBuilderUtils_action_setCriterias();
        
    String contributionHeadPanel_error_noAuthorsEntered();
    
    String contributionHeadPanel_error_noTitleEntered();
    
    String contributionHeadPanel_error_noTopicsSelected();
    
    String contributionHeadPanel_error_noContributorsEntered();
    
    String contributionHeadPanel_overallScore_helpTitle();

    String contributionHeadPanel_overallScore_helpText();

    String contributionHeadPanel_partialScore_helpTitle();

    String contributionHeadPanel_partialScore_helpText();

    String contributionHeadPanel_state_helpTitle();

    String contributionHeadPanel_state_helpText();        

    String contributionList_title();

    String contributionReviewerForm_error_reviewNotFound();

    String contributionReviewerForm_text_artifactsOfContribution();

    String contributionReviewerForm_text_reviewArtifacts();

    String contributionReviewerHeadPanel_text_reviewer();

    String contributionTypeContent_action_addContributionType();

    String contributionTypeContent_text_beginByAddingContributionType();

    String contributionTypeContent_text_contributionTypes();

    String contributionTypeContent_text_newContributionType();

    String contributionTypeContent_title();

    String contributionTypePanel_text_noArtefactPleaseInformEventAdministrator();

    String createPage_button_upload();

    String createPage_text_uploadItem();

    String criteriaList_action_addCriterion();

    String criteriaList_text_adequate();

    String criteriaList_text_beginByAddingCriterion();

    String criteriaList_text_excellent();

    String criteriaList_text_good();

    String criteriaList_text_newCriterion();

    String criteriaList_text_poor();

    String criteriaList_text_tolerable();

    String criteriaList_title();

    String editFormArtefact_action_addExample();

    String editFormArtefact_action_addTemplate();

    String editFormArtefact_error_problemWithTemplateOrPattern();

    String editFormArtefact_form_documentType();

    String editFormArtefact_form_maxInputWords();

    String editFormArtefact_form_necessity();

    String editFormArtefact_form_numReviewers();

    String editFormArtefact_form_uploadExample();

    String editFormArtefact_form_uploadTemplate();

    String editFormArtefact_text_direct();

    String editFormArtefact_text_fileFormatDelimiterHelp();

    String editFormArtefact_text_fileUpload();

    String editFormArtefact_text_optional();

    String editFormArtefact_text_required();

    String editFormArtefact_text_zeroIsUnlimited();

    String editFormArtefact_title_editArtefact();

    String editFormContributionType_form_internal();

    String editFormContributionType_title();

    String editFormCriteria_error_labelsNotUnique();

    String editFormCriteria_form_points();

    String editFormCriteria_text_maximumValue();

    String editFormCriteria_text_minimumPerAllReviewers();

    String editFormCriteria_text_minimumPoints();

    String editFormCriteria_text_minimumValue();

    String editFormCriteria_title();

    String editFormReviewCycle_error_reviewEndsBeforeSubmit();

    String editFormReviewCycle_error_reviewStartsBeforeSubmit();

    String editFormReviewCycle_error_submissionPeriodDatesMustBeSet();

    String editFormReviewCycle_error_whenFillOneReviewDate();

    String editFormReviewCycle_error_wrongReviewPeriod();

    String editFormReviewCycle_error_wrongSubmissionPeriod();

    String editFormReviewCycle_form_submissionPeriod();

    String editFormReviewCycle_text_noArtefactAdded();

    String editFormReviewCycle_text_noItem();

    String editFormReviewCycle_title();

    String editFormTopics_text_enteredEmptyValue();

    String evaluationChairForm_text_minLower();

    String evaluationChairForm_title();

    String evaluationReviewerForm_action_review();

    String evaluationReviewerForm_action_updateNotes();

    String evaluationReviewerForm_button_sendEvaluation();

    String evaluationReviewerForm_text_artefactNotUpladedYet();

    String evaluationReviewerForm_text_cyclePeriodOutOfDate();

    String evaluationReviewerForm_text_evaluationTerm();

    String evaluationReviewerForm_text_notes();

    String evaluationReviewerForm_text_whitoutEvaluationTerm();

    String evaluationReviewerForm_text_showReview();

    String evaluator_text_empty();

    String fileRecordUI_text_examples();

    String fileRecordUI_text_templates();

    String myContributionReviews_text_evaluateThisArtefact();

    String myContributionReviews_text_evaluationOfThisArtefact();

    String myContributionsListItem_action_reviews();

    String myReviewsForm_text_submitted();

    String myReviewsForm_title();

    String myReviewsFormFilter_action_all();

    String newContributionForm_text_artefactsPanel_description();

    String newContributionForm_text_artefactsPanel_title();

    String newContributionForm_text_submitPanel_description();

    String newContributionForm_text_submitPanel_title();

    String newContributionForm_text_termClosed();

    String newContributionForm_title();

    String preferredTopicSelectForm_title();

    String reviewCycle_form_reviewPeriod();

    String reviewCycleList_action_addReviewCycle();

    String reviewCycleList_text_beginByAddingReviewCycle();

    String reviewCycleList_text_newReviewCycle();

    String reviewCycleList_title();

    String reviewCycles_text_artefactIsNotUploaded();

    String reviewCycles_text_artefactScoreText();

    String reviewCycles_text_artefactScoreTitle();

    String reviewCycles_text_noCycles();

    String reviewCycles_text_wasAutoAccept();

    String reviewCycles_text_willAutoAccept();

    String reviewCycles_title();

    String reviewCycles_title_view();

    String reviewerEvaluation_action_sendEvaluation();

    String reviewerEvaluation_helpText_totalScore();

    String reviewerEvaluation_helpTitle_totalScore();

    String reviewerEvaluation_text_noEvaluationCriterias();

    String reviewerEvaluation_text_youMustEnterAllEvaluationOfArtefact();

    String reviewScoreAndComments_title();

    String reviewScoreAndComments_overallEvaluationHelp_title();

    String reviewScoreAndComments_overallEvaluationHelp_text();

    String reviewScoreAndComments_partialEvaluationHelp_title();

    String reviewScoreAndComments_partialEvaluationHelp_text();

    String reviewsForm_action_assignmentReviewers();

    String reviewsForm_action_evaluate();

    String reviewsForm_title();

    String selectPage_button_select();

    String submission_action_addContributor();

    String submission_text_contributionType();

    String submission_text_warnCantUpdate();

    String submission_action_addSubtitle();

    String submissionReviewsOverview_text_artefactsNotes();

    String submissionReviewsOverview_text_mainNotes();

    String submissionReviewsOverview_text_noneNotes();

    String submissionReviewsOverview_title();

    String unknownNameForm_text_contributionAbstract();

    String updateContributionForm_text_submitPanel_title();

    String updateContributionForm_text_submitPanel_description();

    String uploadArtefactDialog_text_existingFiles();

    String uploadArtefactDialog_text_uploadNewFile();
}
