package consys.event.conference.gwt.client.bo.contribution;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.ClientReviewFormCycle;
import java.util.ArrayList;

/**
 * Zakladny objekt pre stahnutie recenzie
 * @author pepa
 */
@Deprecated
public class ClientContributionReview implements IsSerializable, Result {

    private static final long serialVersionUID = 2634484619802220208L;
    // Data pre hlavicku
    private ClientContributionHeadData contribution;
    // Cykly s artefaktami
    private ArrayList<ClientReviewFormCycle> cycles;
    //
    private String publicNote;
    private String privateNote;

    public ClientContributionReview() {
        cycles = new ArrayList<ClientReviewFormCycle>();
    }

    public ClientContributionHeadData getContribution() {
        return contribution;
    }

    public void setContributionHead(ClientContributionHeadData contribution) {
        this.contribution = contribution;
    }

    public ArrayList<ClientReviewFormCycle> getCycles() {
        return cycles;
    }

    public void setCycles(ArrayList<ClientReviewFormCycle> evaluations) {
        this.cycles = evaluations;
    }

    public String getPrivateNote() {
        return privateNote;
    }

    public void setPrivateNote(String privateNote) {
        this.privateNote = privateNote;
    }

    public String getPublicNote() {
        return publicNote;
    }

    public void setPublicNote(String publicNote) {
        this.publicNote = publicNote;
    }
}
