package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.InvitedUser;
import java.util.Set;

/**
 * Upravi zakladne informacie o prispevku
 * @author palo
 */
public class UpdateContributionAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -7922406791070886776L;
    // data
    private String contributionUuid;
    private String title;
    private String subtitle;
    private String[] authors;
    private Set<Long> topics;
    private Set<InvitedUser> contributors;

    public UpdateContributionAction() {
    }

    /**
     * @return the contributionUuid
     */
    public String getContributionUuid() {
        return contributionUuid;
    }

    /**
     * @param contributionUuid the contributionUuid to set
     */
    public void setContributionUuid(String contributionUuid) {
        this.contributionUuid = contributionUuid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the topics
     */
    public Set<Long> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(Set<Long> topics) {
        this.topics = topics;
    }

    /**
     * @return the contributors
     */
    public Set<InvitedUser> getContributors() {
        return contributors;
    }

    /**
     * @param contributors the contributors to set
     */
    public void setContributors(Set<InvitedUser> contributors) {
        this.contributors = contributors;
    }

    /**
     * @return the authors
     */
    public String[] getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String[] authors) {
        this.authors = authors;
    }
}
