package consys.event.conference.gwt.client.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConfirmDialog;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.comp.input.ConsysTextArea;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.common.gwt.client.ui.comp.input.ConsysWordArea;
import consys.common.gwt.client.ui.comp.input.InputResources;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.api.servlet.ConferenceDownloadSubmissionArtefactServlet;
import consys.event.conference.gwt.client.action.contribution.UpdateContributionTextArtefactAction;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType;
import consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.shared.ClientReviewArtefact;
import consys.event.conference.gwt.client.event.UploadedArtefactCanceledEvent;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;

/**
 * Komponentka pro upload/ulozeni a editaci artefaktu
 * <p/>
 * @author pepa
 */
public class ArtefactUpload extends Composite {

    // komponenty
    private FlowPanel panel;
    private ConsysWordArea wordArea;
    private ArtefactFileUpload upload;
    private ChangeUploadComponent changeUpload;
    private SimplePanel headPanel;
    private ActionImage textArtefactUpdate;
    // data
    private ClientArtefactType crt;
    private String contributionUuid;
    private String fileUuid;
    private boolean fileUploading;
    private boolean fileUploaded;
    private boolean enabled;
    private boolean updateUploadUrl;
    private HandlerRegistration wordAreaHandlerRegistration;

    public ArtefactUpload(ClientArtefactType crt, String contributionUuid) {
        this.crt = crt;
        this.contributionUuid = contributionUuid;
        this.enabled = true;

        panel = new FlowPanel();
        panel.setStyleName(ECResourceUtils.css().artefactUpload());

        initWidget(panel);
    }

    public ClientArtefactType getClientArtefactType() {
        return crt;
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        panel.clear();
        generate();
    }

    /**
     * vygeneruje komponentku podle typu dat
     */
    private void generate() {
        headPanel = new SimplePanel();
        panel.add(headPanel);

        switch (crt.getDataType()) {
            case FILE:
                generateFile();
                break;
            case TEXT:
                generateText();
                break;
        }
        generateCombinedPart();
    }

    /**
     * vygeneruje cast pro souborovy artefakt
     */
    private void generateFile() {
        if (crt instanceof ClientReviewArtefact) {
            ClientReviewArtefact cra = (ClientReviewArtefact) crt;
            fileUploaded = true;
            headPanel.setWidget(updateFileInput(cra, false));
        } else {
            headPanel.setWidget(newFileInput());
        }

        if (upload != null) {
            upload.setEnabled(enabled);
        }
        if (changeUpload != null) {
            changeUpload.setEnabled(enabled);
        }
    }

    /**
     * vygeneruje cast pro textovy artefakt
     */
    private void generateText() {
        wordArea = new ConsysWordArea(
                crt.isRequired() ? 1 : 0,
                crt.getMaxInputLength() == 0 ? ConsysWordArea.UNLIMITED : crt.getMaxInputLength(),
                515,
                ConsysTextArea.HEIGHT,
                true,
                crt.getArtefactTypeName());

        textArtefactUpdate = ActionImage.confirm(UIMessageUtils.c.const_update());
        textArtefactUpdate.setVisible(false);
        textArtefactUpdate.setClickHandler(updateTextArtefactClickHandler());

        panel.add(wordArea);
        panel.add(textArtefactUpdate);
        wordArea.setEnabled(enabled);

        // ak sa jedna o uz postnuty artefakt tak nacitame obsah
        downloadTextArtefact();

        // moynost upravit artefakt ale len ak je enabled
        setHandlerForUpdate();


    }

    private void setHandlerForUpdate() {        
        if (enabled && updateUploadUrl && wordArea != null) {
            if (wordAreaHandlerRegistration != null) {
                wordAreaHandlerRegistration.removeHandler();
            }
            wordAreaHandlerRegistration = wordArea.addFocusHandler(updateTextArtefact());
        }

    }

    private void downloadTextArtefact() {
        if (crt instanceof ClientReviewArtefact) {
            ClientReviewArtefact cra = (ClientReviewArtefact) crt;
            try {
                String url = EventURLFactory.overseerURL(ConferenceDownloadSubmissionArtefactServlet.URL);

                RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, EventURLFactory.joinQueryUrl(url, ConferenceDownloadSubmissionArtefactServlet.ARTEFACT_TAG, cra.getArtefactUuid()));
                rb.sendRequest(null, new RequestCallback() {

                    @Override
                    public void onResponseReceived(Request request, Response response) {
                        if (response.getStatusCode() == 200) {
                            wordArea.setText(response.getText());
                        } else {
                            LoggerFactory.log(ArtefactUpload.class, "Download TEXT artefact failed!" + response.getStatusText() + " Headers:" + response.getHeadersAsString());
                        }
                    }

                    @Override
                    public void onError(Request request, Throwable exception) {
                        LoggerFactory.log(ArtefactUpload.class, "Download TEXT artefact failed!" + exception.getMessage());
                    }
                });
            } catch (Exception ex) {
                LoggerFactory.log(ArtefactUpload.class, "Download TEXT artefact failed!" + ex.getMessage());
            }
        }
    }

    /**
     * vygeneruje spolecnou cast pro souborovy i textovy artefakt
     */
    private void generateCombinedPart() {
        if (crt.getDataType().equals(ClientContributionArtefactDataType.FILE)) {
            // povolene pripony
            if (StringUtils.isNotEmpty(crt.getFileFormats())) {
                Label formats = new Label(ECMessageUtils.m.fileRecordUI_text_allowedFileFormats(crt.getFileFormats()));
                formats.addStyleName(ECResourceUtils.css().description());
                panel.add(formats);
            }
        }

        // popisek pokud existuje
        if (StringUtils.isNotEmpty(crt.getArtefactTypeDescription())) {
            Label description = new Label(crt.getArtefactTypeDescription());
            description.addStyleName(ECResourceUtils.css().description());
            panel.add(description);
        }

        // pridat templaty pokud existuji
        if (crt.getUploadedTemplates().size() > 0) {
            addUploadedFilesTitle(ECMessageUtils.c.fileRecordUI_text_templates());
            addFiles(crt.getUploadedTemplates());
            panel.add(StyleUtils.clearDiv());
        }

        // pridat vzory pokud existuji
        if (crt.getUploadedExamples().size() > 0) {
            addUploadedFilesTitle(ECMessageUtils.c.fileRecordUI_text_examples());
            addFiles(crt.getUploadedExamples());
            panel.add(StyleUtils.clearDiv());
        }
    }

    /**
     * prida titulek k templatum nebo paternum
     */
    private void addUploadedFilesTitle(String text) {
        Label label = new Label(text + ":");
        label.setStyleName(ECResourceUtils.css().uploadedTitle());
        panel.add(label);
    }

    /**
     * prida uploadovane soubory
     */
    private void addFiles(ArrayList<CommonThumb> files) {
        FlowPanel p = new FlowPanel();
        p.setStyleName(ECResourceUtils.css().uploadedFiles());

        for (int i = 0; i < files.size(); i++) {
            if (i != 0) {
                p.add(new InlineLabel(", "));
            }
            CommonThumb ct = files.get(i);
            p.add(new Anchor(ct.getName(), ECResourceUtils.downloadArtefactUrl(ct.getUuid()), "_blank"));
        }

        panel.add(p);
    }

    /**
     * vytvori vstup pro novy soubor
     */
    private Widget newFileInput() {
        upload = new ArtefactFileUpload(ECResourceUtils.uploadArtefactUrl(updateUploadUrl), contributionUuid, crt.getArtefactTypeUuid());
        upload.uploadBegin(uploadBeginAction());
        upload.uploadEnd(uploadEndAction(upload));
        upload.setValidSuffixs(crt.getFileFormats());
        return upload;
    }

    /**
     * vytvori vstup, kdyz uz je nejaky soubor uploadly
     */
    private Widget updateFileInput(ClientReviewArtefact cra, boolean afterUploaded) {
        changeUpload = new ChangeUploadComponent(cra, afterUploaded);
        return changeUpload;
    }

    /**
     * vraci uuid uploadleho souboru (pouzivat jen pro souborovy artefakt!!!)
     */
    public String getFileUuid() {
        return fileUuid;
    }

    public boolean isFileUploaded() {
        return fileUploaded;
    }

    /**
     * pokud se jedna o soubor a zrovna se uploaduje, vraci true
     */
    public boolean isFileUploading() {
        return fileUploading;
    }

    /**
     * vraci textovy vstup (pouzivat jen pro textovy artefakt!!!)
     */
    public String getTextInput() {
        return wordArea.getText();
    }

    /**
     * nastavuje text do textoveho vstupu (pouzivat jen pro textovy artefakt!!!)
     */
    public void setTextInput(String text) {
        if (wordArea != null) {
            wordArea.setText(text);
        }
    }

    /**
     * enabluje/disabluje komponentu
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;


    }

    /**
     * vraci zda je komponenta enablovana/disablovana
     */
    public boolean isEnabled() {
        return enabled;
    }

    private ConsysAction deleteAction(final ChangeArtefactFileUpload change, final ConfirmDialog dialog) {
        return new ConsysAction() {

            @Override
            public void run() {
                fileUploaded = false;
                fileUploading = false;
                EventBus.fire(new UploadedArtefactCanceledEvent(change.getArtefactUuid(), change.getArtefactTypeUuid()));
                headPanel.setWidget(newFileInput());
                dialog.hide();
            }
        };
    }

    /**
     * akce spustena na zacatku uploadovani souboru
     */
    private ConsysAction uploadBeginAction() {
        return new ConsysAction() {

            @Override
            public void run() {
                fileUploading = true;
            }
        };
    }

    /**
     * akce spustena po skonceni uploadovani souboru
     */
    private ConsysActionWithValue<Boolean> uploadEndAction(final ArtefactFileUpload afu) {
        return new ConsysActionWithValue<Boolean>() {

            @Override
            public void run(Boolean data) {
                fileUploading = false;
                if (data) {
                    fileUploaded = true;
                    if (afu != null) {
                        ClientReviewArtefact cra = new ClientReviewArtefact();
                        cra.initializeType(crt);
                        cra.setFileName(afu.getFileName());
                        cra.setArtefactUuid(afu.getArtefactUuid());
                        cra.setTotalScore(new EvaluationValue());
                        headPanel.setWidget(updateFileInput(cra, true));
                    }
                } else {
                    // TODO: dosla negativni odpoved, nekde napsat ze se to pokazilo
                }
            }
        };
    }

    public boolean isUpdateUploadUrl() {
        return updateUploadUrl;
    }

    public void setUpdateUploadUrl(boolean updateUploadUrl) {
        this.updateUploadUrl = updateUploadUrl;
    }

    /**
     * zobrazi tlacitko pro aktualizaci textoveho artefaktu
     */
    private FocusHandler updateTextArtefact() {
        return new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                textArtefactUpdate.setVisible(true);
            }
        };
    }

    private ClickHandler updateTextArtefactClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UpdateContributionTextArtefactAction action = new UpdateContributionTextArtefactAction();
                action.setTextContributionArtefact(new TextContributionArtefact(crt.getArtefactTypeUuid(), wordArea.getText()));
                action.setUuid(contributionUuid);

                EventBus.fire(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        textArtefactUpdate.setVisible(false);
                    }
                }, null));
                // TODO: nasetovat ActionExecutionDelegate
            }
        };
    }

    /**
     * komponentka ktera zobrazuje aktualizaci uploadovaneho souboru
     */
    private class ChangeUploadComponent extends Composite {

        // komponenty
        private ConsysTextBox fileNameBox;
        private ChangeArtefactFileUpload changeUpload;
        private ActionImage delete;
        private final ClientReviewArtefact artefact;

        public ChangeUploadComponent(ClientReviewArtefact artefact, boolean afterUploaded) {
            this.artefact = artefact;

            fileNameBox = new ConsysTextBox(0, 0, ECMessageUtils.c.artefactUpload_int_textBoxWidth());
            fileNameBox.setText(artefact.getFileName());
            fileNameBox.setEnabled(false);
            fileNameBox.getTextBox().addStyleName(ECResourceUtils.css().artefactUploadUpdateFileNameBoxEnabled());

            FlowPanel wrapper = new FlowPanel();
            wrapper.setStyleName(ECResourceUtils.css().wrapper());
            wrapper.add(fileNameBox);
            wrapper.add(downloadButton());
            wrapper.add(StyleUtils.clearDiv());

            changeUpload = changeUpload(fileNameBox, wrapper);
            if (afterUploaded) {
                StyleUtils.addStyleNameIfNotSet(changeUpload.getInfoLabel(), InputResources.INSTANCE.css().uploaded());
                changeUpload.getInfoLabel().setText(UIMessageUtils.c.consysAutoFileUpload_text_uploaded());
            }

            delete = deleteUpload(changeUpload);

            FlowPanel p = new FlowPanel();
            p.setStyleName(ECResourceUtils.css().artefactUploadFileUploaded());
            p.add(wrapper);
            p.add(changeUpload);
            p.add(delete);
            p.add(StyleUtils.clearDiv());
            p.add(changeUpload.getInfoLabel());

            initWidget(p);
        }

        public void setEnabled(boolean enabled) {
            changeUpload.setEnabled(enabled);
            delete.setEnabled(enabled);
            if (enabled) {
                StyleUtils.addStyleNameIfNotSet(fileNameBox.getTextBox(), ECResourceUtils.css().artefactUploadUpdateFileNameBoxEnabled());
            } else {
                fileNameBox.getTextBox().removeStyleName(ECResourceUtils.css().artefactUploadUpdateFileNameBoxEnabled());

            }
        }

        /**
         * vytvori stahovaci tlacitko
         */
        private ActionImage downloadButton() {
            ActionImage button = ActionImage.confirm(UIMessageUtils.c.const_download());
            button.setClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    Window.open(ECResourceUtils.downloadArtefactUrl(artefact.getArtefactUuid()), "_blank", "");
                }
            });
            return button;
        }

        /**
         * vytvori upload pro aktualizaci nahrateho souboru
         */
        private ChangeArtefactFileUpload changeUpload(ConsysTextBox textBox, final FlowPanel p) {
            final String servletUrl = ECResourceUtils.uploadArtefactUrl(updateUploadUrl);
            ChangeArtefactFileUpload change = new ChangeArtefactFileUpload(servletUrl, contributionUuid,
                    artefact.getArtefactTypeUuid(), artefact.getArtefactUuid());
            change.setTextBoxWithFilename(textBox);
            change.uploadBegin(uploadBeginAction());
            change.uploadEnd(uploadEndAction(null));
            change.setValidSuffixs(artefact.getFileFormats());
            change.setHideAction(new ConsysActionWithValue<Boolean>() {

                @Override
                public void run(Boolean data) {
                    p.setVisible(!data.booleanValue());
                }
            });
            return change;
        }

        private ActionImage deleteUpload(ChangeArtefactFileUpload change) {
            ActionImage button = ActionImage.withoutIcon(ECMessageUtils.c.artefactUpload_action_remove());
            button.addStyleName(ECResourceUtils.css().deleteUpload());
            button.setClickHandler(deleteClickHandler(change));
            return button;
        }

        private ClickHandler deleteClickHandler(final ChangeArtefactFileUpload change) {
            return new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    ConfirmDialog confirm = new ConfirmDialog(ECMessageUtils.c.artefactUpload_text_confirmTitle());
                    confirm.setAction(deleteAction(change, confirm));
                    confirm.showCentered();
                }
            };
        }
    }
}
