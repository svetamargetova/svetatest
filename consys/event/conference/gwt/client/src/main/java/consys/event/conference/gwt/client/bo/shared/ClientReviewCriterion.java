package consys.event.conference.gwt.client.bo.shared;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import java.util.ArrayList;

/**
 * Jedno recenzencni kriterium
 * @author pepa
 */
public class ClientReviewCriterion<CRITERION_VALUE_TYPE> implements IsSerializable {

    // data
    private EvaluationValue value;
    private Long id;
    private String name;
    private String description;
    private ArrayList<ClientReviewCriterionItem<CRITERION_VALUE_TYPE>> items;
    private double minAverageAccept;
    private double minPointsAccept;
    private int maxPoints;

    public ClientReviewCriterion() {
        items = new ArrayList<ClientReviewCriterionItem<CRITERION_VALUE_TYPE>>();
    }

    public ClientReviewCriterion(EvaluationValue value, String name, String description) {
        this();
        this.value = value;
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<ClientReviewCriterionItem<CRITERION_VALUE_TYPE>> getItems() {
        return items;
    }

    public void setItems(ArrayList<ClientReviewCriterionItem<CRITERION_VALUE_TYPE>> items) {
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EvaluationValue getValue() {
        if (value == null) {
            return new EvaluationValue();
        } else {
            return value;
        }
    }

    public void setValue(EvaluationValue value) {
        this.value = value;
    }

    /**
     * @return the minAverageAccept
     */
    public double getMinAverageAccept() {
        return minAverageAccept;
    }

    /**
     * @param minAverageAccept the minAverageAccept to set
     */
    public void setMinAverageAccept(double minAverageAccept) {
        this.minAverageAccept = minAverageAccept;
    }

    /**
     * @return the minPointsAccept
     */
    public double getMinPointsAccept() {
        return minPointsAccept;
    }

    /**
     * @param minPointsAccept the minPointsAccept to set
     */
    public void setMinPointsAccept(double minPointsAccept) {
        this.minPointsAccept = minPointsAccept;
    }

    public int getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }
}
