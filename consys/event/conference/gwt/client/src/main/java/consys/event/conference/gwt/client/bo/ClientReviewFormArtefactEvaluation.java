package consys.event.conference.gwt.client.bo;

import consys.event.conference.gwt.client.bo.setting.ClientCriteria;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Dotiahnutelny objekt recenzie
 * @author pepa
 */
@Deprecated
public class ClientReviewFormArtefactEvaluation implements Result {

    private static final long serialVersionUID = 3767881228982733789L;
    // data    
    private ArrayList<ClientCriteria> criteria;
    /** Ak uz niekedy recenzent hodnotil dany artefakt tak toto je datum posledneho hodnotenia.  */
    private Date reviewed;
    /** Ak uz niekedy recenzent hodnotil dany artefakt tak toto je mapa ID_CRITERIA, VALUE  */
    private HashMap<Long, Integer> evaluation;
    /** verejna poznamka */
    private String publicNote;
    /** soukroma poznamka */
    private String privateNote;

    public ClientReviewFormArtefactEvaluation() {
        criteria = new ArrayList<ClientCriteria>();
        evaluation = new HashMap<Long, Integer>();
    }

    public ArrayList<ClientCriteria> getCriteria() {
        return criteria;
    }

    public void setCriteria(ArrayList<ClientCriteria> criteria) {
        this.criteria = criteria;
    }

    public HashMap<Long, Integer> getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(HashMap<Long, Integer> evaluation) {
        this.evaluation = evaluation;
    }

    /**
     * Ak uz niekedy recenzent hodnotil dany artefakt tak toto je datum posledneho hodnotenia.
     * @return the reviewed
     */
    public Date getReviewed() {
        return reviewed;
    }

    /**
     * Ak uz niekedy recenzent hodnotil dany artefakt tak toto je datum posledneho hodnotenia.
     * @param reviewed the reviewed to set
     */
    public void setReviewed(Date reviewed) {
        this.reviewed = reviewed;
    }

    /** soukroma poznamka */
    public String getPrivateNote() {
        return privateNote;
    }

    /** soukroma poznamka */
    public void setPrivateNote(String privateNote) {
        this.privateNote = privateNote;
    }

    /** verejna poznamka */
    public String getPublicNote() {
        return publicNote;
    }

    /** verejna poznamka */
    public void setPublicNote(String publicNote) {
        this.publicNote = publicNote;
    }
}
