package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;

/**
 * Akce pro nacteni review cycle
 * @author pepa
 */
public class LoadReviewCycleAction extends EventAction<ClientSettingsReviewCycle> {

    // konstanty
    private static final long serialVersionUID = -3134184178713848193L;
    // data
    private Long reviewCycleId;

    public LoadReviewCycleAction() {
    }

    public LoadReviewCycleAction(Long reviewCycleId) {
        this.reviewCycleId = reviewCycleId;
    }

    public Long getReviewCycleId() {
        return reviewCycleId;
    }
}
