package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.ArrayList;

/**
 * Akce pro aktualizaci typu artefaktu (thumb obsahuje data bez seznamu jinych komponent)
 * @author pepa
 */
public class UpdateArtefactTypeAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = -4878686385409968410L;
    // data
    private String uuid;
    private String name;
    private String description;
    private int numReviewers;
    private boolean required;
    private boolean file;
    private String fileFormats;
    private int maxInputLength;
    private ArrayList<String> uploadedTemplates;
    private ArrayList<String> uploadedExamples;

    public UpdateArtefactTypeAction() {
        uploadedTemplates = new ArrayList<String>();
        uploadedExamples = new ArrayList<String>();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFile() {
        return file;
    }

    public void setFile(boolean file) {
        this.file = file;
    }

    public String getFileFormats() {
        return fileFormats;
    }

    public void setFileFormats(String fileFormats) {
        this.fileFormats = fileFormats;
    }

    public int getMaxInputLength() {
        return maxInputLength;
    }

    public void setMaxInputLength(int maxInputLength) {
        this.maxInputLength = maxInputLength;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumReviewers() {
        return numReviewers;
    }

    public void setNumReviewers(int numReviewers) {
        this.numReviewers = numReviewers;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public ArrayList<String> getUploadedExamples() {
        return uploadedExamples;
    }

    public void setUploadedExamples(ArrayList<String> uploadedExamples) {
        this.uploadedExamples = uploadedExamples;
    }

    public ArrayList<String> getUploadedTemplates() {
        return uploadedTemplates;
    }

    public void setUploadedTemplates(ArrayList<String> uploadedTemplates) {
        this.uploadedTemplates = uploadedTemplates;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
