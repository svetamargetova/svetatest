package consys.event.conference.gwt.client.module.reviewer;

import consys.event.conference.gwt.client.module.reviewer.item.BidForReviewSelectPanel;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.DataListCommonOrderer;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.ListUtils;
import consys.event.conference.api.list.BidForReviewList;
import consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction;
import consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;
import java.util.ArrayList;

/**
 * Formular pro recenzenty, ve kterem jsi vybiraji o jake prispevky by meli zajem
 * @author pepa
 */
public class BidForReviewForm extends DataListPanel implements BidForReviewList {

    // data    
    private ArrayList<Long> myTopics;

    public BidForReviewForm() {
        super(ECMessageUtils.c.bidForReviewForm_title(), TAG);
        setListDelegate(new BidForReviewListDelegate());

        addLeftControll(ActionImage.breadcrumbBackBigBack());

        setDefaultFilter(new ListFilter(Filter_INTEREST) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });

        DataListCommonOrderer orderer = new DataListCommonOrderer();
        orderer.addOrderer(ECMessageUtils.c.const_title(), Order_TITLE, true);
        orderer.addOrderer(ECMessageUtils.c.const_submitDate(), Order_SUBMIT_DATE);
        orderer.addOrderer(ECMessageUtils.c.bidForReviewForm_action_interest(), Order_INTEREST);
        addHeadPanel(orderer);
    }

    @Override
    protected void onLoad() {
        if (ListUtils.isEmpty(myTopics)) {
            EventBus.get().fireEvent(new EventDispatchEvent(new ListReviewerPreferredTopicsIdsAction(),
                    new AsyncCallback<ArrayListResult<Long>>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                        }

                        @Override
                        public void onSuccess(ArrayListResult<Long> result) {
                            myTopics = result.getArrayListResult();
                            BidForReviewForm.super.onLoad();
                        }
                    }, this));
        }
    }

    /** delegat seznamu */
    private class BidForReviewListDelegate implements ListDelegate<ClientListBidForReviewItem, BidForReviewItem> {

        @Override
        public BidForReviewItem createCell(ClientListBidForReviewItem item) {
            return new BidForReviewItem(item);
        }
    }

    /** jedna polozka seznamu */
    private class BidForReviewItem extends ListItem<ClientListBidForReviewItem> {

        public BidForReviewItem(ClientListBidForReviewItem value) {
            super(value);
        }

        @Override
        protected void createCell(ClientListBidForReviewItem item, FlowPanel panel) {
            FlowPanel content = new FlowPanel();
            content.setWidth((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 170) + "px");
            content.addStyleName(FLOAT_LEFT);
            content.addStyleName(MARGIN_LEFT_10);
            content.addStyleName(MARGIN_TOP_10);

            fillContent(item, content);

            BidForReviewSelectPanel control = new BidForReviewSelectPanel(item.getUuid(), item.getIntereset(),
                    BidForReviewForm.this);

            FlexTable ft = new FlexTable();
            ft.setStyleName(ECResourceUtils.bundle().css().bidForReviewTable());
            ft.setWidth(LayoutManager.LAYOUT_CONTENT_WIDTH);
            ft.setWidget(0, 0, content);
            ft.setWidget(0, 1, control);

            Element cell = ft.getFlexCellFormatter().getElement(0, 1);
            cell.addClassName(ECResourceUtils.bundle().css().controlCell());
            control.init(cell);

            panel.clear();
            panel.setStyleName(ECResourceUtils.bundle().css().noMargin());
            panel.add(ft);
        }

        /** naplni vlastni obsah polozky */
        private void fillContent(ClientListBidForReviewItem item, FlowPanel content) {
            Label title = StyleUtils.getStyledLabel(item.getTitle(), FONT_17PX, FONT_BOLD);
            String authorsContent = item.getAuthors() == null ? ECMessageUtils.c.const_authorsNotVisible() : item.getAuthors();
            HTML authors = new HTML("<b>" + ECMessageUtils.c.const_authors() + ":</b> <i>" + authorsContent + "</i>");
            authors.addStyleName(FONT_13PX);

            String topcs = "";
            for (ClientTopic ct : item.getTopics()) {
                if (myTopics.contains(ct.getId())) {
                    topcs += "<b>" + ct.getName() + ",</b> ";
                } else {
                    topcs += ct.getName() + ", ";
                }
            }
            if (!topcs.isEmpty()) {
                topcs = topcs.substring(0, topcs.length() - 2);
            }
            HTML topics = new HTML("<b>" + ECMessageUtils.c.const_topics() + ":</b> " + topcs);

            FlowPanel download = new FlowPanel();
            HTML downloadLabel = new HTML("<b>" + UIMessageUtils.c.const_download() + ":</b> ");
            downloadLabel.setStyleName(FLOAT_LEFT);
            downloadLabel.addStyleName(MARGIN_RIGHT_10);
            download.add(downloadLabel);
            for (int idx = 0; idx < item.getArtefacts().size(); idx++) {
                CommonThumb thumb = item.getArtefacts().get(idx);
                boolean addDelimiter = idx < (item.getArtefacts().size() - 1);
                Anchor anchor = new Anchor(thumb.getName() + (addDelimiter ? ", " : ""), ECResourceUtils.downloadArtefactUrl(thumb.getUuid()));
                download.add(anchor);
            }

            content.add(title);
            content.add(StyleUtils.clearDiv("5px"));
            content.add(new Label(item.getSubtitle()));
            content.add(StyleUtils.clearDiv("10px"));
            content.add(authors);
            content.add(StyleUtils.clearDiv("5px"));
            content.add(topics);
            content.add(StyleUtils.clearDiv("5px"));
            content.add(download);
            content.add(StyleUtils.clearDiv("10px"));
        }
    }
}
