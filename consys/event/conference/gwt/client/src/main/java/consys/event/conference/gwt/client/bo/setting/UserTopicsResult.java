package consys.event.conference.gwt.client.bo.setting;

import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Vysledek pro nacteni topiku s vyznacenim, ktere ma uzivatel vybrane
 * @author pepa
 */
public class UserTopicsResult implements Result {

    private static final long serialVersionUID = 3366327186456011058L;
    // data
    private ArrayList<ClientTopic> topics;
    private ArrayList<Long> selectedTopics;

    public UserTopicsResult() {
        topics = new ArrayList<ClientTopic>();
        selectedTopics = new ArrayList<Long>();
    }

    public ArrayList<Long> getSelectedTopics() {
        return selectedTopics;
    }

    public void setSelectedTopics(ArrayList<Long> selectedTopics) {
        this.selectedTopics = selectedTopics;
    }

    public ArrayList<ClientTopic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<ClientTopic> topics) {
        this.topics = topics;
    }
}
