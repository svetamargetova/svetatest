package consys.event.conference.gwt.client.comp.evaluator;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.ui.comp.Help;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.comp.display.EvaluationItemValue;
import consys.event.conference.gwt.client.module.reviewer.ContributionReviewerForm;
import consys.event.conference.gwt.client.utils.CalculationUtils;

/**
 *
 * @author pepa
 */
public class PreviewEvaluator extends Evaluator {

    // data
    private ClientPreviews preview;
    private String contributionUuid;
    private String contributionTitle;

    public PreviewEvaluator(ClientPreviews preview, String contributionUuid, String contributionTile) {
        super();
        this.preview = preview;
        this.contributionUuid = contributionUuid;
        this.contributionTitle = contributionTile;
    }

    @Override
    protected EvaluationItem generateChoosedValue() {
        return EvaluationItem.small(new EvaluationItemValue(), true);
    }

    @Override
    protected String evaluatorTitleText() {
        return preview.getName();
    }

    @Override
    protected void itemsContent(FlowPanel panel) {
        if (!preview.getReviewers().isEmpty()) {
            int values = 0;
            int counter = 0;
            final int count = preview.getReviewers().size();
            for (int i = 0; i < count; i++) {
                ClientPreview reviewer = preview.getReviewers().get(i);

                EvaluationItemValue reviewerScore;
                if (reviewer.getScore().getValue().isEntered()) {
                    int value = reviewer.getScore().getValue().getValue();
                    reviewerScore = new EvaluationItemValue(value, 0, preview.getMaxPoints());
                    values += value;
                    counter++;
                } else {
                    reviewerScore = reviewer.getScore();
                }
                reviewerScore.setStartFromOne(true);

                EvaluatorItemData<String> data = new EvaluatorItemData<String>();
                data.setName(reviewer.getReviewerName());
                data.setScore(reviewerScore);
                data.setAction(reviewerItemAction(reviewer.getReviewUuid()));
                panel.add(new EvaluatorItem(data, i + 1 == count));
            }

            EvaluationValue eValue;
            if (counter > 0) {
                eValue = new EvaluationValue(CalculationUtils.percent(values / counter, preview.getMaxPoints()));
            } else {
                eValue = new EvaluationValue();
            }
            getChoosedValue().setValue(new EvaluationItemValue(eValue));
        } else {
            empty();
        }
    }

    private ConsysActionWithValue<EvaluatorItemData<String>> reviewerItemAction(final String reviewUuid) {
        return new ConsysActionWithValue<EvaluatorItemData<String>>() {

            @Override
            public void run(EvaluatorItemData<String> data) {
                ContributionReviewerForm review = new ContributionReviewerForm(contributionTitle, contributionUuid,
                        reviewUuid, false);
                FormUtils.fireNextBreadcrumb(contributionTitle, review);
            }
        };
    }

    @Override
    protected Help createHelp() {
        return Help.left(preview.getName(), preview.getDescription());
    }
}
