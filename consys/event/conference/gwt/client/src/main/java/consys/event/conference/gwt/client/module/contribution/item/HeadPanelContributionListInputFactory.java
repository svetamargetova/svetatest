/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.client.module.contribution.item;

/**
 *
 * @author palo
 */
public class HeadPanelContributionListInputFactory extends HeadPanelReadInputFactory{

    public HeadPanelContributionListInputFactory(boolean authorsVisible) {
        super(authorsVisible);
    }

    @Override
    public boolean showSubTitle() {
        return false;
    }            
    
}
