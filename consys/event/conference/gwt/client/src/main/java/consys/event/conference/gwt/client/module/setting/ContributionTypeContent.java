package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.OrderDialog;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.OrderPanel.OrderPanelListener;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabOrderPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItemUI;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.CreateContributionTypeAction;
import consys.event.conference.gwt.client.action.setting.LoadContributionSettingAction;
import consys.event.conference.gwt.client.action.setting.LoadContributionTypeAction;
import consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction;
import consys.event.conference.gwt.client.action.exception.SubmissionTypeUniqueException;
import consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;

/**
 * Nastaveni submission typu
 * @author pepa
 */
public class ContributionTypeContent extends RootPanel implements OrderPanelListener<ConsysTabOrderPanelItem<String>> {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(ContributionTypeContent.class);
    // komponenty
    private ActionLabel addTab;
    private ConsysTabPanel<String> tabs;
    private ConsysMessage helpMessage;
    private OrderDialog orderDialog;

    public ContributionTypeContent() {
        super(ECMessageUtils.c.contributionTypeContent_title());

        ActionImage topicsButton = ActionImage.getPersonButton(ECMessageUtils.c.const_topics(), true);
        topicsButton.addClickHandler(topicsHandler());
        addLeftControll(topicsButton);

        ActionImage propertyButton = ActionImage.getPersonButton(UIMessageUtils.c.const_property(), true);
        propertyButton.addClickHandler(propertyHandler());
        addLeftControll(propertyButton);

        setStyleName(StyleUtils.MARGIN_BOT_10);

        addTab = new ActionLabel(ECMessageUtils.c.contributionTypeContent_action_addContributionType(),
                StyleUtils.TEXT_NOWRAP + " " + MARGIN_VER_10);
        addTab.removeStyleName(INLINE);
        addTab.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        addTab.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                NewItemDialog<NewItemUI> cd = new NewItemDialog<NewItemUI>(ECMessageUtils.c.contributionTypeContent_text_newContributionType(), helpMessage) {

                    @Override
                    public void doCreateAction(ArrayList<NewItemUI> uis, final ConsysMessage fail) {
                        final ArrayList<ClientContributionTypeThumb> types = new ArrayList<ClientContributionTypeThumb>();
                        for (NewItemUI ui : uis) {
                            ClientContributionTypeThumb type = new ClientContributionTypeThumb();
                            type.setName(ui.getName());
                            type.setDescription(ui.getDescription());
                            types.add(type);
                        }

                        for (NewItemUI ui : uis) {
                            if (!ui.form().validate(fail)) {
                                return;
                            }
                        }

                        // vytvoreni na serveru
                        EventDispatchEvent addContributionTypeEvent = new EventDispatchEvent(new CreateContributionTypeAction(types),
                                new AsyncCallback<ArrayListResult<String>>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava ActionExcecutor
                                        if (caught instanceof SubmissionTypeUniqueException) {
                                            fail.setText(ECMessageUtils.c.const_exceptionSubmissionTypeUnique());
                                        }
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<String> result) {
                                        ArrayList<String> uuids = result.getArrayListResult();
                                        // vlozeni zalozek
                                        for (int i = 0; i < types.size(); i++) {
                                            ClientContributionTypeThumb type = types.get(i);
                                            type.setUuid(uuids.get(i));
                                            ClientTabItem cti = new ClientTabItem();
                                            cti.setTitle(type.getName());
                                            cti.setUuid(type.getUuid());
                                            addContributionTypeTab(tabs, cti, helpMessage);
                                            if (!tabs.isVisible()) {
                                                tabs.setVisible(true);
                                            }
                                            tabs.selectTabById(type.getUuid());
                                        }
                                        successCreateAction();
                                    }
                                }, this);
                        EventBus.get().fireEvent(addContributionTypeEvent);
                    }

                    @Override
                    public NewItemUI newItem() {
                        return new NewItemUI();
                    }
                };
                cd.showCentered();
            }
        });

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ECMessageUtils.c.contributionTypeContent_text_beginByAddingContributionType());
        helpMessage.hideClose(true);
        addWidget(helpMessage);

        tabs = new ConsysTabPanel(ContributionTypeContent.class.getName());
        tabs.setVisible(false);
        tabs.showOrderIcon(new ConsysAction() {

            @Override
            public void run() {
                ArrayList<ConsysTabOrderPanelItem<String>> itemList = new ArrayList<ConsysTabOrderPanelItem<String>>();
                ArrayList<ConsysTabPanelItemUI<String>> ts = tabs.getTabs();
                for (ConsysTabPanelItemUI<String> ui : ts) {
                    itemList.add(new ConsysTabOrderPanelItem(ui, ui.getTabName()));
                }

                orderDialog = new OrderDialog<ConsysTabOrderPanelItem<String>>(itemList, ContributionTypeContent.this);
                orderDialog.showCentered();
                orderDialog.getOrderPanel().setFinalPositionOffset(0);
            }
        });

        addWidget(addTab);
        addWidget(tabs);

        EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(new LoadContributionSettingAction(),
                new AsyncCallback<ArrayListResult<CommonThumb>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava ActionExcecutor
                        if (caught instanceof NoRecordsForAction) {
                            // nezobrazovat chybu no records for action #96
                            //getFailMessage().setText(ECMessageUtils.c.exceptionNoRecordsForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ArrayListResult<CommonThumb> result) {
                        setClientSubmissionTypeList(result.getArrayListResult());
                    }
                }, getSelf());
        EventBus.get().fireEvent(loadSettingsEvent);
    }

    /** vraci nastaveni submissionu */
    public ArrayList<ClientContributionType> getClientSubmissionTypeList() {
        ArrayList<ClientContributionType> list = new ArrayList<ClientContributionType>();
        for (int i = 0; i < tabs.getTabCount(); i++) {
            ContributionTypeItem item = (ContributionTypeItem) tabs.getTabContent(i);
            list.add(item.getSubmissionType());
        }
        return list;
    }

    /** nastavuje formulare podle ulozenych dat submissionu */
    public void setClientSubmissionTypeList(ArrayList<CommonThumb> thumbList) {
        tabs.clear();
        if (thumbList != null && thumbList.size() > 0) {
            helpMessage.setVisible(false);
            tabs.setVisible(true);
            for (CommonThumb thumb : thumbList) {
                ClientTabItem cti = new ClientTabItem();
                cti.setTitle(thumb.getName());
                cti.setUuid(thumb.getUuid());
                addContributionTypeTab(tabs, cti, helpMessage);
            }
        }
    }

    /** clickhandler pro prepnuti do nastaveni topiku */
    private ClickHandler topicsHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                FormUtils.fireNextBreadcrumb(ECMessageUtils.c.const_topics(), new EditFormTopics());
            }
        };
    }

    /** clickhandler pro prepnuti do nastaveni property */
    private ClickHandler propertyHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ConferencePropertyPanel w = new ConferencePropertyPanel();
                FormUtils.fireNextBreadcrumb(UIMessageUtils.c.const_property(), w);
            }
        };
    }

    /** prida zalozku s typem prispevku */
    private void addContributionTypeTab(ConsysTabPanel tabs, final ClientTabItem item, ConsysMessage helpMessage) {
        ConsysTabPanelLabel tl = new ConsysTabPanelLabel(item.getUuid(), item.getTitle());
        final ContributionTypeItem ctItem = new ContributionTypeItem(tl, item, helpMessage);
        ConsysAction action = new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(new LoadContributionTypeAction(item.getUuid()),
                        new AsyncCallback<ClientContributionType>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                }
                            }

                            @Override
                            public void onSuccess(ClientContributionType result) {
                                for (ClientReviewCycleThumb t : result.getClientReviewCycleList()) {
                                    logger.info("incoming cycle dates from " + t.getSubmitFrom() + " to " + t.getSubmitTo());
                                }
                                ctItem.setSubmissionType(result);
                            }
                        }, ctItem);
                EventBus.get().fireEvent(loadSettingsEvent);
            }
        };
        tabs.addTabItem(tl, ctItem, action);
    }

    @Override
    public void onOrderChange(final ConsysTabOrderPanelItem<String> object, final int oldPosition, final int newPosition) {
        String tabUuid = object.getUi().getId();
        EventBus.get().fireEvent(new EventDispatchEvent(
                new UpdateContributionTypePositionAction(tabUuid, oldPosition, newPosition),
                new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                        orderDialog.getOrderPanel().back();
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        tabs.changeTabPosition(object.getUi(), oldPosition, newPosition);
                    }
                }, orderDialog));
    }
}
