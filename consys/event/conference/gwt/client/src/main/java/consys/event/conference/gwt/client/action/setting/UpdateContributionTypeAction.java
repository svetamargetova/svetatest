package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;

/**
 * Akce pro aktualizaci typu prispevku (thumb obsahuje data bez seznamu jinych komponent)
 * @author pepa
 */
public class UpdateContributionTypeAction extends EventAction<VoidResult> {

    // konstanty
    private static final long serialVersionUID = -3604816944192672927L;
    // data
    private ClientContributionTypeThumb thumb;

    public UpdateContributionTypeAction() {
    }

    public UpdateContributionTypeAction(ClientContributionTypeThumb thumb) {
        this.thumb = thumb;
    }

    public ClientContributionTypeThumb getThumb() {
        return thumb;
    }
}
