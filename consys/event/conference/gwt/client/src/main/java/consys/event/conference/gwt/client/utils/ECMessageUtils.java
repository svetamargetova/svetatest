package consys.event.conference.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.event.conference.gwt.client.message.EventConferenceConstants;
import consys.event.conference.gwt.client.message.EventConferenceMessages;

/**
 *
 * @author pepa
 */
public interface ECMessageUtils {

    /** systemove konstanty modulu event conference */
    public static final EventConferenceConstants c = (EventConferenceConstants) GWT.create(EventConferenceConstants.class);
    /** systemove zpravy modulu event conference */
    public static final EventConferenceMessages m = (EventConferenceMessages) GWT.create(EventConferenceMessages.class);
}
