package consys.event.conference.gwt.client.comp;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.HiddenTextBox;
import consys.common.gwt.client.ui.comp.input.ConsysAutoFileUpload;
import consys.common.gwt.client.ui.comp.input.ConsysFileUpload;
import consys.common.gwt.client.ui.comp.input.InputResources;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.api.servlet.ConferenceSubmissionArtefactUploadServlet;
import consys.event.conference.gwt.client.event.ArtefactUploadedEvent;
import consys.event.conference.gwt.client.utils.ECMessageUtils;

/**
 * Uploadovaci komponenta noveho artefaktu
 * TODO: vytvorit spolecneho predka pro tento upload a ChangeArtefactFileUpload
 * @author pepa
 */
public class ArtefactFileUpload extends ConsysAutoFileUpload<ConsysFileUpload> {

    // komponenty
    private ConsysFileUpload upload;
    private HiddenTextBox contribution;
    private HiddenTextBox artefactType;
    // data
    private String contributionUuid;
    private String artefactTypeUuid;
    private String[] validSuffixs;
    private ConsysAction beginAction;
    private ConsysActionWithValue<Boolean> endAction;
    private String artefactUuid;

    public ArtefactFileUpload(String servletUrl, String contributionUuid, String artefactTypeUuid) {
        super(servletUrl, 515);
        this.contributionUuid = contributionUuid;
        this.artefactTypeUuid = artefactTypeUuid;

        contribution.setValue(contributionUuid);
        artefactType.setValue(artefactTypeUuid);
    }

    @Override
    protected ConsysFileUpload uploadComponent() {
        upload = new ConsysFileUpload(getIntWidth());
        upload.setName(ConferenceSubmissionArtefactUploadServlet.FIELD_FILE);
        return upload;
    }

    @Override
    protected void additionalFormItems(FlowPanel panel) {
        contribution = new HiddenTextBox("", ConferenceSubmissionArtefactUploadServlet.FIELD_SUBMISSION_UUID);
        panel.add(contribution);

        artefactType = new HiddenTextBox("", ConferenceSubmissionArtefactUploadServlet.FIELD_ARTEFACT_TYPE);
        panel.add(artefactType);
    }

    @Override
    protected void onSubmit() {
        if (beginAction != null) {
            beginAction.run();
        }
    }

    @Override
    protected boolean processResult(String result) {
        UploadResponse response = processResponse(result);
        if (response.isSuccess()) {
            artefactUuid = response.getResponse();
        }

        if (endAction != null) {
            endAction.run(response.isSuccess());
        }

        if (response.isSuccess()) {
            // potvrzeni noveho artefaktu
            EventBus.fire(new ArtefactUploadedEvent(contributionUuid, artefactTypeUuid, artefactUuid));
            return true;
        } else {
            errorUpload(getInfoLabel());
            return false;
        }
    }

    @Override
    protected String[] validSuffixs() {
        return validSuffixs;
    }

    public void setValidSuffixs(String suffixList) {
        validSuffixs = parseValidSuffixs(suffixList);
    }

    public static String[] parseValidSuffixs(String suffixList) {
        String[] suffixs = null;
        if (suffixList != null) {
            // TODO: potreba dodelat rozpoznani oddelovace
            // pokud je oddelovac carka
            suffixs = suffixList.replace(" ", "").toLowerCase().split(",");
        }
        return suffixs;
    }

    public void uploadBegin(ConsysAction beginAction) {
        this.beginAction = beginAction;
    }

    public void uploadEnd(ConsysActionWithValue<Boolean> endAction) {
        this.endAction = endAction;
    }

    public String getFileName() {
        return upload.getFilename();
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    /** vycte z odpovedi serveru jestli se akce zdarila a jaka je odpoved */
    public static UploadResponse processResponse(String result) {
        result = result.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
        LoggerFactory.log(ArtefactFileUpload.class, "Upload result: " + result);

        String response;
        boolean success = false;

        if (result.contains("ok")) {
            // ok: <ok>%s</ok>            
            success = true;
            response = responseText(result, "ok");
            LoggerFactory.log(ArtefactFileUpload.class, "Upload result: [ok] " + response);
        } else { // chyba vzdy ak nie ok
            // chyba: <error>%s</error>
            response = responseText(result, "error");
            LoggerFactory.log(ArtefactFileUpload.class, "Upload result: [error] " + response);
        }

        return new UploadResponse(success, response);
    }

    private static String responseText(String result, String tagName) {
        String beginTag = "<" + tagName + ">";
        int begin = result.indexOf(beginTag);
        int end = result.indexOf("</" + tagName + ">");
        return result.substring(begin + beginTag.length(), end);
    }

    public static void errorUpload(Label label) {
        label.removeStyleName(InputResources.INSTANCE.css().error());
        label.removeStyleName(InputResources.INSTANCE.css().uploading());
        StyleUtils.addStyleNameIfNotSet(label, InputResources.INSTANCE.css().error());
        label.setText(ECMessageUtils.c.artefactFileUpload_error_artefactUploadFailed());
    }
}
