package consys.event.conference.gwt.client.bo;

import consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb;
import consys.common.gwt.shared.action.Result;

/**
 * Objekt zaobalujuci artefakt v pre cyklus hodnotenia
 * @author palo
 */
@Deprecated
public class ClientReviewFormCycleArtefact extends ClientReviewCycleThumb implements Result {

    private static final long serialVersionUID = -8774216591401056737L;
    /** IF CYCLE is Active -> IF alreadyReviewed THEN show ""Show Review" ELSE show "Review" */
    private boolean alreadyReviewed = false;
    private boolean automaticallyAccepted = false;
    private String artefactTypeName;
    /** Ak je null artefact nebol zatial submitnuty */
    private String artefactUuid;

    /**
     * @return the alreadyReviewed
     */
    public boolean isAlreadyReviewed() {
        return alreadyReviewed;
    }

    /**
     * @param alreadyReviewed the alreadyReviewed to set
     */
    public void setAlreadyReviewed(boolean alreadyReviewed) {
        this.alreadyReviewed = alreadyReviewed;
    }

    /**
     * @return the artefactTypeName
     */
    public String getArtefactTypeName() {
        return artefactTypeName;
    }

    /**
     * @param artefactTypeName the artefactTypeName to set
     */
    public void setArtefactTypeName(String artefactTypeName) {
        this.artefactTypeName = artefactTypeName;
    }

    /**
     * @return the artefactUuid
     */
    public String getArtefactUuid() {
        return artefactUuid;
    }

    /**
     * @param artefactUuid the artefactUuid to set
     */
    public void setArtefactUuid(String artefactUuid) {
        this.artefactUuid = artefactUuid;
    }

    /**
     * @return the automaticallyAccepted
     */
    public boolean isAutomaticallyAccepted() {
        return automaticallyAccepted;
    }

    /**
     * @param automaticallyAccepted the automaticallyAccepted to set
     */
    public void setAutomaticallyAccepted(boolean automaticallyAccepted) {
        this.automaticallyAccepted = automaticallyAccepted;
    }
}
