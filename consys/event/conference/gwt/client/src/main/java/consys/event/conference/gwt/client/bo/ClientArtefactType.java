package consys.event.conference.gwt.client.bo;

import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import java.util.ArrayList;

/**
 * Typ artefaktu
 * @author pepa
 */
public class ClientArtefactType extends ClientArtefactTypeThumb {

    private static final long serialVersionUID = -2913228576592574661L;
    // data
    private boolean required;
    private boolean autoAccepted;
    private ClientContributionArtefactDataType dataType;
    private String fileFormats;
    private int maxInputLength;
    private ArrayList<CommonThumb> uploadedTemplates;
    private ArrayList<CommonThumb> uploadedExamples;

    public ClientArtefactType() {
        uploadedExamples = new ArrayList<CommonThumb>();
        uploadedTemplates = new ArrayList<CommonThumb>();
        dataType = ClientContributionArtefactDataType.TEXT;
    }

    public boolean isFile() {
        return getDataType().equals(ClientContributionArtefactDataType.FILE);
    }

    public String getFileFormats() {
        return fileFormats;
    }

    public void setFileFormats(String fileFormats) {
        this.fileFormats = fileFormats;
    }

    public int getMaxInputLength() {
        return maxInputLength;
    }

    public void setMaxInputLength(int maxInputLength) {
        this.maxInputLength = maxInputLength;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public ArrayList<CommonThumb> getUploadedExamples() {
        return uploadedExamples;
    }

    public void setUploadedExamples(ArrayList<CommonThumb> uploadedExamples) {
        this.uploadedExamples = uploadedExamples;
    }

    public ArrayList<CommonThumb> getUploadedTemplates() {
        return uploadedTemplates;
    }

    public void setUploadedTemplates(ArrayList<CommonThumb> uploadedTemplates) {
        this.uploadedTemplates = uploadedTemplates;
    }

    public ClientContributionArtefactDataType getDataType() {
        return dataType;
    }

    public void setDataType(ClientContributionArtefactDataType dataType) {
        this.dataType = dataType;
    }

    public boolean isAutoAccepted() {
        return autoAccepted;
    }

    public void setAutoAccepted(boolean autoAccepted) {
        this.autoAccepted = autoAccepted;
    }
}
