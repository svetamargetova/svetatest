package consys.event.conference.gwt.client.bo.chair;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author palo
 */
public class ClientContributionChairFormData implements IsSerializable, Result {

    private static final long serialVersionUID = -8045940969610152860L;
    // head panel data    
    private ClientContributionChairHead head;
    /** Komentare */
    private ArrayList<ReviewScoreAndCommentsData> evaluated;
           
    public ClientContributionChairHead getHead() {
        return head;
    }

    public void setHead(ClientContributionChairHead head) {
        this.head = head;
    }

    public List<ReviewScoreAndCommentsData> getEvaluated() {
        if(evaluated == null){
            evaluated = new ArrayList<ReviewScoreAndCommentsData>();
        }
        return evaluated;
    }

    public void setEvaluated(ArrayList<ReviewScoreAndCommentsData> evaluated) {
        this.evaluated = evaluated;
    }   
}
