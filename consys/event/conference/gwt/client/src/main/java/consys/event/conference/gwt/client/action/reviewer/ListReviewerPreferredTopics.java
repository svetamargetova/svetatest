package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.UserTopicsResult;

/**
 * Akce pro nacteni topiku s vyznacenim, ktere si recenzent jiz vybral
 * @author pepa
 */
public class ListReviewerPreferredTopics extends EventAction<UserTopicsResult> {

    private static final long serialVersionUID = 8640776246018550949L;
}
