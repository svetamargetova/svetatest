package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionType;

/**
 * Akce pro nacteni typu prispevku (s thumby artefaktu)
 * @author pepa
 */
public class LoadContributionTypeAction extends EventAction<ClientContributionType> {

    private static final long serialVersionUID = 3496775276003693809L;
    // data
    private String uuid;

    public LoadContributionTypeAction() {
    }

    public LoadContributionTypeAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
