package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Aktualizace verejneho a soukromeho komentare hodnoceni prispevku
 * @author pepa
 */
public class UpdateContributionCommentAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 5305924042263280774L;
    // data
    private String contributionUuid;
    private String publicComment;
    private String privateComment;

    public UpdateContributionCommentAction() {
    }

    public UpdateContributionCommentAction(String contributionUuid, String publicComment, String privateComment) {
        this.contributionUuid = contributionUuid;
        this.publicComment = publicComment;
        this.privateComment = privateComment;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }

    public String getPrivateComment() {
        return privateComment;
    }

    public String getPublicComment() {
        return publicComment;
    }
}
