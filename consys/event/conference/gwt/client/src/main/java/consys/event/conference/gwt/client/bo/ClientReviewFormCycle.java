package consys.event.conference.gwt.client.bo;

import consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;
import java.util.Date;

/**
 * Objekt zaobalujuci prvu uroven artefaktov
 * @author palo
 */
@Deprecated
public class ClientReviewFormCycle extends ClientReviewCycleThumb implements Result {
    private static final long serialVersionUID = -505298530718650437L;
        
    // data
    private boolean canReview;
    private Date reviewFrom;
    private Date reviewTo;
    private ArrayList<ClientReviewFormCycleArtefact> artefacts;

    public ClientReviewFormCycle() {
        artefacts = new ArrayList<ClientReviewFormCycleArtefact>();
    }
   
    public Date getReviewFrom() {
        return reviewFrom;
    }

    public void setReviewFrom(Date reviewFrom) {
        this.reviewFrom = reviewFrom;
    }

    public Date getReviewTo() {
        return reviewTo;
    }

    public void setReviewTo(Date reviewTo) {
        this.reviewTo = reviewTo;
    }

    public void setCanReview(boolean canReview) {
        this.canReview = canReview;
    }

    public boolean canReview() {
        return canReview;
    }



    /**
     * @return the artefacts
     */
    public ArrayList<ClientReviewFormCycleArtefact> getArtefacts() {
        return artefacts;
    }

    /**
     * @param artefacts the artefacts to set
     */
    public void setArtefacts(ArrayList<ClientReviewFormCycleArtefact> artefacts) {
        this.artefacts = artefacts;
    }
}
