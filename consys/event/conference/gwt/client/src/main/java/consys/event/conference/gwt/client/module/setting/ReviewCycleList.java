package consys.event.conference.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Podcast ArtefactItem, tvori seznam kolecek
 * @author pepa
 */
public class ReviewCycleList extends FlowPanel {

    // komponenty
    private FlowPanel content;
    private ConsysMessage helpMessage;
    // data
    private String contributionTypeUuid;
    private ConsysAction updateAction;
    private ArtefactList artefactList;

    public ReviewCycleList(String contributionTypeUuid, ArtefactList artefactList) {
        super();
        this.contributionTypeUuid = contributionTypeUuid;
        this.artefactList = artefactList;
        setStyleName(StyleUtils.MARGIN_BOT_10);
        setWidth("100%");

        Label titleLabel = StyleUtils.getStyledLabel(ECMessageUtils.c.reviewCycleList_title(), StyleUtils.FONT_16PX);
        titleLabel.addStyleName(StyleUtils.FONT_BOLD);
        titleLabel.addStyleName(StyleUtils.TEXT_GRAY);
        titleLabel.addStyleName(StyleUtils.FLOAT_LEFT);
        titleLabel.addStyleName(StyleUtils.MARGIN_RIGHT_10);

        ActionLabel addReviewCycle = new ActionLabel(ECMessageUtils.c.reviewCycleList_action_addReviewCycle(),
                StyleUtils.MARGIN_TOP_3 + " " + StyleUtils.FLOAT_LEFT);
        addReviewCycle.setClickHandler(addReviewCycleClickHandler());

        FlowPanel head = new FlowPanel();
        head.setStyleName(StyleUtils.MARGIN_TOP_10);
        head.add(titleLabel);
        head.add(addReviewCycle);
        head.add(StyleUtils.clearDiv());

        content = new FlowPanel() {

            @Override
            public boolean remove(Widget w) {
                boolean result = super.remove(w);
                if (result && getWidgetCount() == 0) {
                    helpMessage.setVisible(true);
                }
                return result;
            }
        };
        content.setWidth("100%");

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ECMessageUtils.c.reviewCycleList_text_beginByAddingReviewCycle());
        helpMessage.setStyleName(StyleUtils.MARGIN_TOP_10);
        helpMessage.hideClose(true);

        add(head);
        add(content);
        add(helpMessage);

        final Comparator<ReviewCycleItem> comparator = new Comparator<ReviewCycleItem>() {

            @Override
            public int compare(ReviewCycleItem o1, ReviewCycleItem o2) {
                return o1.getSubmitFrom().compareTo(o2.getSubmitFrom());
            }
        };
        updateAction = new ConsysAction() {

            @Override
            public void run() {
                ArrayList<ReviewCycleItem> comp = new ArrayList<ReviewCycleItem>();
                for (int i = 0; i < content.getWidgetCount(); i++) {
                    comp.add((ReviewCycleItem) content.getWidget(i));
                }
                content.clear();
                Collections.sort(comp, comparator);
                for (ReviewCycleItem item : comp) {
                    content.add(item);
                }
                comp = null;
            }
        };
    }

    /** vraci seznam obsazenych ReviewCycle */
    public ArrayList<ClientReviewCycleThumb> getClientReviewCycleList() {
        ArrayList<ClientReviewCycleThumb> list = new ArrayList<ClientReviewCycleThumb>();
        for (int i = 0; i < content.getWidgetCount(); i++) {
            ReviewCycleItem item = (ReviewCycleItem) content.getWidget(i);
            list.add(item.getReviewCycle());
        }
        return list;
    }

    /** nastavuje seznam ReviewCycle */
    public void setClientReviewCycleList(ArrayList<ClientReviewCycleThumb> list) {
        if (list != null) {
            content.clear();
            Collections.sort(list, new Comparator<ClientReviewCycleThumb>() {

                @Override
                public int compare(ClientReviewCycleThumb o1, ClientReviewCycleThumb o2) {
                    return o1.getSubmitFrom().compareTo(o2.getSubmitFrom());
                }
            });
            for (ClientReviewCycleThumb cycle : list) {
                content.add(new ReviewCycleItem(cycle, updateAction, artefactList));
            }
            helpMessage.setVisible(content.getWidgetCount() == 0);
        }
    }

    /** vraci clickhandler pro kliknuti na addReviewCycle */
    private ClickHandler addReviewCycleClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                NewItemDialog<NewReviewCycleUI> cd = new NewItemDialog<NewReviewCycleUI>(
                        ECMessageUtils.c.reviewCycleList_text_newReviewCycle(), helpMessage) {

                    @Override
                    public void doCreateAction(ArrayList<NewReviewCycleUI> uis, final ConsysMessage fail) {
                        final ArrayList<ClientSettingsReviewCycle> items = new ArrayList<ClientSettingsReviewCycle>();
                        for (NewReviewCycleUI ui : uis) {
                            ClientSettingsReviewCycle item = new ClientSettingsReviewCycle();
                            item.setSubmitFrom(ui.getSubmitFromBox().getValue());
                            item.setSubmitTo(ui.getSubmitToBox().getValue());
                            item.setReviewFrom(ui.getReviewFromBox().getValue());
                            item.setReviewTo(ui.getReviewToBox().getValue());
                            items.add(item);
                        }

                        for (ClientSettingsReviewCycle i : items) {
                            if (i.getSubmitFrom() == null || i.getSubmitTo() == null) {
                                fail.setText(ECMessageUtils.c.editFormReviewCycle_error_submissionPeriodDatesMustBeSet());
                                return;
                            }
                            if (i.getSubmitFrom().after(i.getSubmitTo())) {
                                fail.setText(ECMessageUtils.c.editFormReviewCycle_error_wrongSubmissionPeriod());
                                return;
                            }
                            if ((i.getReviewFrom() == null && i.getReviewTo() != null)
                                    || (i.getReviewFrom() != null && i.getReviewTo() == null)) {
                                getFailMessage().setText(ECMessageUtils.c.editFormReviewCycle_error_whenFillOneReviewDate());
                                return;
                            }
                            if (i.getReviewFrom() != null && i.getReviewTo() != null) {
                                if (i.getReviewFrom().after(i.getReviewTo())) {
                                    fail.setText(ECMessageUtils.c.editFormReviewCycle_error_wrongReviewPeriod());
                                    return;
                                }
                                if (i.getReviewFrom().before(i.getSubmitFrom())) {
                                    fail.setText(ECMessageUtils.c.editFormReviewCycle_error_reviewStartsBeforeSubmit());
                                    return;
                                }
                                if (i.getReviewTo().before(i.getSubmitTo())) {
                                    fail.setText(ECMessageUtils.c.editFormReviewCycle_error_reviewEndsBeforeSubmit());
                                    return;
                                }
                            }
                        }

                        // vytvoreni na serveru
                        EventDispatchEvent reviewCycleEvent = new EventDispatchEvent(
                                new CreateReviewCycleAction(contributionTypeUuid, items),
                                new AsyncCallback<ArrayListResult<Long>>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava ActionExecutor
                                        if (caught instanceof NoRecordsForAction) {
                                            fail.setText(ECMessageUtils.c.const_exceptionNoRecordsForAction());
                                        }
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<Long> result) {
                                        ArrayList<Long> ids = result.getArrayListResult();
                                        // vlozeni polozek
                                        for (int i = 0; i < items.size(); i++) {
                                            ClientSettingsReviewCycle item = items.get(i);
                                            item.setId(ids.get(i));
                                            content.add(new ReviewCycleItem(item, updateAction, artefactList));
                                        }
                                        updateAction.run();
                                        successCreateAction();
                                    }
                                }, this);
                        EventBus.get().fireEvent(reviewCycleEvent);
                    }

                    @Override
                    public NewReviewCycleUI newItem() {
                        return new NewReviewCycleUI();
                    }
                };
                cd.showCentered();
            }
        };
    }
}
