package consys.event.conference.gwt.client.comp.evaluator;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.Help;
import consys.common.gwt.client.ui.comp.RollAnimation;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.conference.gwt.client.comp.display.EvaluationItem;
import consys.event.conference.gwt.client.utils.ECMessageUtils;
import consys.event.conference.gwt.client.utils.ECResourceUtils;

/**
 * Komponenta pro zadavani hodnoceni od recenzenta
 * @author pepa
 */
public abstract class Evaluator extends Composite {

    // komponenty
    private FlowPanel content;
    private FlowPanel pointsPanel;
    private SimplePanel wrapper;
    private EvaluationItem choosedValue;
    private SimplePanel iconPanel;
    private Help help;
    // data
    private RollAnimation animation;
    private boolean rolled;

    public Evaluator() {
        content = new FlowPanel();
        content.setStyleName(ECResourceUtils.css().evaluator());
        initWidget(content);
    }

    @Override
    protected void onLoad() {
        content.clear();
        rolled = false;
        generateContent();
    }

    private void generateContent() {
        choosedValue = generateChoosedValue();
        choosedValue.addStyleName(ECResourceUtils.css().evaluatorResult());

        content.add(choosedValue);
        content.add(detailPanel());
        content.add(StyleUtils.clearDiv());
    }

    protected EvaluationItem getChoosedValue() {
        return choosedValue;
    }

    /** vytvori EvaluationItem pro vybranou hodnotu evaluatoru */
    protected abstract EvaluationItem generateChoosedValue();

    /** vytvori polozky vysouvaciho panelu */
    protected abstract void itemsContent(FlowPanel panel);

    /** vraci nazev evaluatoru */
    protected abstract String evaluatorTitleText();

    private FlowPanel detailPanel() {
        pointsPanel = new FlowPanel();
        pointsPanel.setStyleName(ECResourceUtils.css().evaluatorPointsPanel());

        itemsContent(pointsPanel);

        wrapper = new SimplePanel();
        wrapper.setStyleName(ECResourceUtils.css().evaluatorPointsPanelWrapper());
        wrapper.setWidget(pointsPanel);
        wrapper.setVisible(false);

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ECResourceUtils.css().evaluatorSelector());
        panel.add(head());
        panel.add(StyleUtils.clearDiv());
        panel.add(wrapper);
        return panel;
    }

    private FocusPanel head() {
        animation = new RollAnimation();
        animation.setFixedHeight(true);

        iconPanel = new SimplePanel();
        iconPanel.setStyleName(StyleUtils.FLOAT_LEFT);
        refreshIconPanel();

        SimplePanel textPanel = textPanel();
        SimplePanel endPanel = endPanel();

        FlowPanel p = new FlowPanel();
        p.add(iconPanel);
        p.add(textPanel);
        p.add(endPanel);
        p.add(StyleUtils.clearDiv());

        MouseOverHandler overHandler = headOverHandler();
        MouseOutHandler outHandler = headOutHandler();
        ClickHandler clickHandler = headClickHandler();

        FocusPanel head = new FocusPanel();
        head.setStyleName(ECResourceUtils.css().evaluatorHead());
        head.addClickHandler(rollClickHandler());
        head.addMouseOverHandler(headOverHandler(textPanel, endPanel));
        head.addMouseOutHandler(headOutHandler(textPanel, endPanel));
        if (overHandler != null) {
            head.addMouseOverHandler(overHandler);
        }
        if (outHandler != null) {
            head.addMouseOutHandler(outHandler);
        }
        if (clickHandler != null) {
            head.addClickHandler(clickHandler);
        }
        head.setWidget(p);
        return head;
    }

    private SimplePanel textPanel() {
        SimplePanel textPanel = new SimplePanel();
        textPanel.setStyleName(ECResourceUtils.css().evaluatorTitleText());
        textPanel.addStyleName(ECResourceUtils.css().evaluatorTitle());
        textPanel.setWidget(new Label(evaluatorTitleText()));
        return textPanel;
    }

    private SimplePanel endPanel() {
        SimplePanel endPanel = new SimplePanel();
        endPanel.setStyleName(ECResourceUtils.css().evaluatorTitleEnd());
        endPanel.setWidget(new Image(ECResourceUtils.bundle().evaluatorTextTitleEnd()));
        return endPanel;
    }

    protected void empty() {
        Label empty = new Label(ECMessageUtils.c.evaluator_text_empty());
        SimplePanel emptyPanel = new SimplePanel();
        emptyPanel.setStyleName(ECResourceUtils.css().evaluatorEmpty());
        emptyPanel.addStyleName(ECResourceUtils.css().evaluatorItemBottom());
        emptyPanel.setWidget(empty);
        pointsPanel.add(emptyPanel);
    }

    private ClickHandler rollClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                rollPanel();
            }
        };
    }

    private MouseOverHandler headOverHandler(final SimplePanel textPanel, final SimplePanel endPanel) {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                textPanel.setStyleName(ECResourceUtils.css().evaluatorTitleText());
                textPanel.addStyleName(ECResourceUtils.css().evaluatorTitleOver());
                endPanel.setWidget(new Image(ECResourceUtils.bundle().evaluatorTextTitleEndOver()));
            }
        };
    }

    private MouseOutHandler headOutHandler(final SimplePanel textPanel, final SimplePanel endPanel) {
        return new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                textPanel.setStyleName(ECResourceUtils.css().evaluatorTitleText());
                textPanel.addStyleName(ECResourceUtils.css().evaluatorTitle());
                endPanel.setWidget(new Image(ECResourceUtils.bundle().evaluatorTextTitleEnd()));
            }
        };
    }

    /** over handler po najeti nad hlavicku komponenty, urceno pro pretizeni */
    protected MouseOverHandler headOverHandler() {
        return new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                if (rolled && !help.isShowing()) {
                    showHelp(false);
                }
            }
        };
    }

    /** out handler po odjeti z hlavicky komponenty, urceno pro pretizeni */
    protected MouseOutHandler headOutHandler() {
        return null;
    }

    /** click handler po kliknuti na hlavicku komponenty, urceno pro pretizeni */
    protected ClickHandler headClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showHelp(true);
            }
        };
    }

    public void rollPanel() {
        animation.rollWidget(pointsPanel, wrapper, rolled);
        rolled = !rolled;
        refreshIconPanel();
    }

    private void refreshIconPanel() {
        iconPanel.setWidget(new Image(rolled
                ? ECResourceUtils.bundle().evaluatorIconOpened()
                : ECResourceUtils.bundle().evaluatorIconClosed()));
    }

    protected Help getHelp() {
        return help;
    }

    protected void showHelp(boolean noticeRolled) {
        if (noticeRolled && !rolled) {
            // help se vykresli jen pri rozbaleni
            return;
        }
        if (help != null) {
            showHelpOnPosition();
        } else {
            help = createHelp();
            if (help == null) {
                return;
            }
            help.setAutoHideEnabled(true);
            showHelpOnPosition();
        }
    }

    protected void showHelpOnPosition() {
        help.setPopupPosition(getAbsoluteLeft() + 270, getAbsoluteTop() + 12);
        help.show();
    }

    /** urceno k pretizeni pokud se ma zobrazovat help */
    protected Help createHelp() {
        return null;
    }
}
