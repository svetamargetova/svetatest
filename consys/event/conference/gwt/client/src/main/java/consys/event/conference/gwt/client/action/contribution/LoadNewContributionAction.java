package consys.event.conference.gwt.client.action.contribution;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.ClientNewContributionActionData;

/**
 * Nacte typy prispevku prispevku pro formular odeslani/editace prispevku
 * @author pepa
 */
public class LoadNewContributionAction extends EventAction<ClientNewContributionActionData> {

    private static final long serialVersionUID = -2111335352131036898L;
    // data
    private boolean internal;

    public LoadNewContributionAction() {
    }

    public LoadNewContributionAction(boolean internal) {
        this.internal = internal;
    }

    /** maji se nacist interni typy prispevku? */
    public boolean isInternal() {
        return internal;
    }
}
