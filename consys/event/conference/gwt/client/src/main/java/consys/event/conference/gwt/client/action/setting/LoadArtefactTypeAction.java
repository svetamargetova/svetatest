package consys.event.conference.gwt.client.action.setting;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType;

/**
 * Akce pro nacteni typu artefaktu
 * @author pepa
 */
public class LoadArtefactTypeAction extends EventAction<ClientSettingsArtefactType> {

    // konstanty
    private static final long serialVersionUID = -4057346947084775204L;
    // data
    private String uuid;

    public LoadArtefactTypeAction() {
    }

    public LoadArtefactTypeAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
