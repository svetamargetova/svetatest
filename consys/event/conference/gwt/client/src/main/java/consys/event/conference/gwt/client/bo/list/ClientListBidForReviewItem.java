package consys.event.conference.gwt.client.bo.list;

import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientListBidForReviewItem extends  DataListUuidItem {
    private static final long serialVersionUID = -4876191059013822239L;
    
    private String title;
    private String subtitle;
    private String authors;
    private ArrayList<ClientTopic> topics;    
    private ReviewerInterestEnum intereset;
    private ArrayList<CommonThumb> artefacts;

    public ClientListBidForReviewItem() {
        topics = new ArrayList<ClientTopic>();
        artefacts = new ArrayList<CommonThumb>();
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public ReviewerInterestEnum getIntereset() {
        return intereset;
    }

    public void setIntereset(ReviewerInterestEnum intereset) {
        this.intereset = intereset;
    }  

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ClientTopic> getTopics() {
        return topics;
    }

    public void setTopics(ArrayList<ClientTopic> topics) {
        this.topics = topics;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the artefacts
     */
    public ArrayList<CommonThumb> getArtefacts() {
        return artefacts;
    }

    /**
     * @param artefacts the artefacts to set
     */
    public void setArtefacts(ArrayList<CommonThumb> artefacts) {
        this.artefacts = artefacts;
    }
}
