package consys.event.conference.gwt.client.action.reviewer;

import consys.common.gwt.shared.action.EventAction;
import consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData;

/**
 * Nacte hodnoceni artefaktu a data pro zadani hodnoceni
 * @author pepa
 */
public class LoadContributionArtefactReviewAction extends EventAction<ClientReviewReviewerData> {

    private static final long serialVersionUID = -1992375820243412048L;
    // data
    private String artefactUuid;
    private String artefactTypeUuid;
    private String contributionUuid;
    private String reviewUuid;

    public LoadContributionArtefactReviewAction() {
    }

    /**
     * Poziadavka na nacitanie hodnotenia artefaktu od recenzenta ktory je prave prihlaseny a 
     * posiela tuto akciu.
     * 
     * @param artefactUuid
     * @param contributionUuid
     * @param artefactTypeUuid 
     */
    public LoadContributionArtefactReviewAction(String artefactUuid, String contributionUuid, String artefactTypeUuid) {
        this.artefactUuid = artefactUuid;
        this.contributionUuid = contributionUuid;
        this.artefactTypeUuid = artefactTypeUuid;
    }

    /**
     * Poziadavka na nacitanie hodnotenia artefaktu od recenzenta ktory je identifikovany
     * parametrom <code>reviewUuid</code>
     * 
     * @param artefactUuid
     * @param artefactTypeUuid
     * @param contributionUuid
     * @param reviewUuid 
     */
    public LoadContributionArtefactReviewAction(String artefactUuid, String artefactTypeUuid, String contributionUuid, String reviewUuid) {
        this.artefactUuid = artefactUuid;
        this.artefactTypeUuid = artefactTypeUuid;
        this.contributionUuid = contributionUuid;
        this.reviewUuid = reviewUuid;
    }

    public String getArtefactUuid() {
        return artefactUuid;
    }

    public String getContributionUuid() {
        return contributionUuid;
    }

    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }

    public String getReviewUuid() {
        return reviewUuid;
    }
}
