package consys.event.conference.gwt.server.list;

import consys.event.common.gwt.server.GwtListHandler;
import consys.event.conference.api.list.ReviewerReviewsList;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import consys.event.conference.gwt.server.action.utils.SubmissionUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerReviewsListHandler implements GwtListHandler<ClientListContributionItem, SubmissionListItem>, ReviewerReviewsList {

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public void execute(List<SubmissionListItem> in, ArrayList<ClientListContributionItem> out) {
       for (SubmissionListItem s : in) {                                    
            out.add(SubmissionUtils.fromListItem(s));
        }
        
    }
}
