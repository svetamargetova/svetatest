package consys.event.conference.gwt.server.action.utils;

import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ArtefactUtils {

    
    public static ArrayList<CommonThumb> fromListUuidItems(List<ListUuidItem> items) {
        ArrayList<CommonThumb> out = Lists.newArrayList();
        for (ListUuidItem item : items) {
            CommonThumb tp = new CommonThumb();
            tp.setName(item.getName());
            tp.setUuid(item.getUuid());
            out.add(tp);
        }
        return out;
    }

   
    
    
    
    public static void toClientReviewCycle(ReviewCycle cycle, ClientSettingsReviewCycle crc){                        
            crc.setSubmitFrom(cycle.getSubmitFrom());
            crc.setSubmitTo(cycle.getSubmitTo());
            crc.setReviewFrom(cycle.getReviewFrom());
            crc.setReviewTo(cycle.getReviewTo());
            crc.setId(cycle.getId());
            for (SubmissionTypeArtefact at : cycle.getArtefacts()) {                                
                crc.getAssignedArtefacts().add(at.getUuid());                
            }                        
    }
    
}
