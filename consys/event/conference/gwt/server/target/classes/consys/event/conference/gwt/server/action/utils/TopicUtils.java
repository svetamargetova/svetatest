package consys.event.conference.gwt.server.action.utils;

import consys.common.utils.collection.Lists;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import java.util.ArrayList;
import java.util.Collection;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TopicUtils {

    public static ClientTopic fromSubmissionTopic(SubmissionTopic topic){
        ClientTopic tp = new ClientTopic();
        tp.setName(topic.getName());
        tp.setId(topic.getId());
        return tp;
    }

    public static ArrayList<ClientTopic> fromSubmissionTopicList(Collection<SubmissionTopic> topics){
        ArrayList<ClientTopic> out = Lists.newArrayList();
        for(SubmissionTopic topic : topics){
            ClientTopic tp = new ClientTopic();
            tp.setName(topic.getName());
            tp.setId(topic.getId());
            out.add(tp);
        }
        return out;
    }




}
