package consys.event.conference.gwt.server.list.export;

import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.list.ListService;
import consys.event.common.gwt.server.list.export.xls.AbstractTabsXlsExportHandler;
import consys.event.common.gwt.server.list.export.xls.SheetInfo;
import consys.event.conference.api.list.ContributionsList;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import consys.event.conference.core.bo.thumb.SubmissionListItem.Evaluation;
import consys.event.conference.core.service.ContributionsListService;
import java.text.DecimalFormat;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ContributionsListExportHandler extends AbstractTabsXlsExportHandler<SubmissionListItem, SubmissionListItem> {

    // konstanty
    private static final String[] TITLES = {"Title", "Submitter", "Email", "Topics", "Score", "Number of reviews", "Score of each review"};
    // servicy
    @Autowired
    private ContributionsListService listService;
    // data
    private DecimalFormat decimalFormat;

    public ContributionsListExportHandler() {
        super(ContributionsList.TAG);
        decimalFormat = new DecimalFormat("#.0");
    }

    @Override
    protected void createTabsWithHeaders(HSSFWorkbook workbook, Map<String, SheetInfo> sheets) {
        HSSFSheet sheet = workbook.createSheet(sheetName("Reviews"));
        createHeader(TITLES, sheet);
        sheets.put("Reviews", new SheetInfo(sheet));
    }

    @Override
    protected void createDataRow(SubmissionListItem item, Row row, boolean allSheet) {
        int counter = 0;

        StringBuilder submitters = new StringBuilder();
        boolean notFirst = false;
        for (UserEvent user : item.getContributors()) {
            if (notFirst) {
                submitters.append(", ");
            }
            submitters.append(user.getFullName());
            notFirst = true;
        }

        StringBuilder emails = new StringBuilder();
        notFirst = false;
        for (UserEvent user : item.getContributors()) {
            if (notFirst) {
                emails.append(", ");
            }
            emails.append(user.getEmail().contains("@") ? user.getEmail() : "-");
            notFirst = true;
        }

        String score = item.getOverallScore().getScore() < 0 ? "-" : decimalFormat.format(item.getOverallScore().getScore() / 10d);

        int reviews = 0;
        StringBuilder scores = new StringBuilder();
        notFirst = false;
        for (Evaluation e : item.getPartialScore()) {
            if (notFirst) {
                scores.append(", ");
            }
            if (e.getScore() < 0) {
                scores.append("-");
            } else {
                scores.append(decimalFormat.format(e.getScore() / 10d));
                reviews++;
            }
            notFirst = true;
        }

        setTextCell(row, counter++, string(item.getTitle()));
        setTextCell(row, counter++, string(submitters.toString()));
        setTextCell(row, counter++, string(emails.toString()));
        setTextCell(row, counter++, string(item.getAgregatedTopics()));
        setTextCell(row, counter++, string(score));
        setTextCell(row, counter++, string(String.valueOf(reviews)));
        setTextCell(row, counter++, string(scores.toString()));
    }

    @Override
    protected ListService<SubmissionListItem, SubmissionListItem> listService() {
        return listService;
    }

    @Override
    protected String getSheetKey(SubmissionListItem item) {
        return "Reviews";
    }

    @Override
    protected String[] allSheetTitles() {
        return TITLES;
    }

    @Override
    protected int[] autosizeColumnIndexes() {
        return new int[]{0, 1, 2, 3, 4, 5, 6};
    }

    @Override
    protected int[] allTabAutosizeColumnIndexes() {
        return new int[]{0, 1, 2, 3, 4, 5, 6};
    }
}
