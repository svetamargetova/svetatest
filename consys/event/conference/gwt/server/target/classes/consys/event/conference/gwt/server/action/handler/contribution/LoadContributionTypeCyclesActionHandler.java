package consys.event.conference.gwt.server.action.handler.contribution;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.contribution.LoadContributionTypeCyclesAction;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.server.action.utils.NewContributionArtefactsBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni seznamu typu artefaktu pro formular pridani prispevku
 * @author pepa
 */
public class LoadContributionTypeCyclesActionHandler 
implements EventActionHandler<LoadContributionTypeCyclesAction, ArrayListResult<ClientReviewCycle>>,InitializingBean {

    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private ReviewService reviewService;

    private NewContributionArtefactsBuilder builder;
    
    @Override
    public Class<LoadContributionTypeCyclesAction> getActionType() {
        return LoadContributionTypeCyclesAction.class;
    }

    @Override
    public ArrayListResult<ClientReviewCycle> execute(LoadContributionTypeCyclesAction action) throws ActionException {
        try {            
            return new ArrayListResult<ClientReviewCycle>(builder.createCycles(action.getContributionTypeUuid(), true));
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }

    }    
    
    
    @Override
    public void afterPropertiesSet() throws Exception {
        builder = new NewContributionArtefactsBuilder(reviewService, submissionService);
    }
    
}
