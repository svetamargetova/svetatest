package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.exception.DeleteCriteriaException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCriteriaException;
import consys.event.conference.gwt.client.action.setting.UpdateArtefactTypeAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Aktualizace typu artefaktu (thumb obsahuje data bez seznamu jinych komponent)
 * @author pepa
 */
public class UpdateArtefactTypeActionHandler implements EventActionHandler<UpdateArtefactTypeAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<UpdateArtefactTypeAction> getActionType() {
        return UpdateArtefactTypeAction.class;
    }

    @Override
    public VoidResult execute(UpdateArtefactTypeAction action) throws ActionException {
        try {
            SubmissionTypeArtefact a = settingsService.loadTypeArtefactWithArtefactByUuid(action.getUuid());
            a.getArtefactType().setFormats(action.getFileFormats());
            a.getArtefactType().setDescription(action.getDescription());
            a.getArtefactType().setName(action.getName());
            a.getArtefactType().setFile(action.isFile());
            a.getArtefactType().setInputLength(action.getMaxInputLength());
            a.setNumOfReviewers(action.getNumReviewers());
            a.setRequired(action.isRequired());
            
            settingsService.updateSubmissionArtefactType(a,action.getUploadedTemplates(),action.getUploadedExamples());
            return VoidResult.RESULT();
        } catch (DeleteCriteriaException ex) {
            throw new DeleteReviewCriteriaException();
        } catch (RequiredPropertyNullException ex) {
            // Missing artefact name
            throw new BadInputException();
        } catch (NoRecordException ex) {
            // NoRecords moze sposobit ze patterny alebo templaty nevyhladane 
            throw new NoRecordsForAction();
        }
    }
}
