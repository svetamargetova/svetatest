package consys.event.conference.gwt.server.action.handler.contribution;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.contribution.DeleteUploadedContributionArtefactAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class DeleteUploadedContributionArtefactActionHandler implements EventActionHandler<DeleteUploadedContributionArtefactAction, VoidResult> {

    @Autowired
    private SubmissionService submissionService;
   

    @Override
    public Class<DeleteUploadedContributionArtefactAction> getActionType() {
        return DeleteUploadedContributionArtefactAction.class;
    }

    @Override
    public VoidResult execute(DeleteUploadedContributionArtefactAction action) throws ActionException {
        try {
            submissionService.deleteCachedSubmissionArtefact(action.getSubmissionUuid(), action.getArtefactTypeUuid(), action.getArtefactUuid());
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }

    }
}
