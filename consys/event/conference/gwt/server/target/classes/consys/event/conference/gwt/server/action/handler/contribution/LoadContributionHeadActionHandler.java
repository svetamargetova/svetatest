package consys.event.conference.gwt.server.action.handler.contribution;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.contribution.LoadContributionHeadAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.server.action.utils.SubmissionUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacte hlavickova data podle uuid prispevku
 * <p/>
 * @author pepa
 */
public class LoadContributionHeadActionHandler implements EventActionHandler<LoadContributionHeadAction, ClientContributionHeadData> {

    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<LoadContributionHeadAction> getActionType() {
        return LoadContributionHeadAction.class;
    }

    @Override
    public ClientContributionHeadData execute(LoadContributionHeadAction action) throws ActionException {
        try {
            ClientContributionHeadData data = new ClientContributionHeadData();
            // nacitame prispevek s artefaktama
            Submission submission = submissionService.loadSubmissionWithArtefacts(action.getContributionUuid());

            // vytvorime hlavicku                        
            SubmissionUtils.copySubmission(submission, data);
            SubmissionUtils.copyContributors(submissionService.listSubmissionContributors(action.getContributionUuid()), data);
            return data;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
