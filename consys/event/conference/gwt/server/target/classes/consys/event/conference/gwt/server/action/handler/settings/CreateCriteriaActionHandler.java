package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.utils.collection.Lists;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.CreateCriteriaAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumbCreate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Vytvoreni kriterii
 * @author pepa
 */
public class CreateCriteriaActionHandler implements EventActionHandler<CreateCriteriaAction, ArrayListResult<Long>> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<CreateCriteriaAction> getActionType() {
        return CreateCriteriaAction.class;
    }

    @Override
    public ArrayListResult<Long> execute(CreateCriteriaAction action) throws ActionException {
        List<ReviewCriteria> criterias = Lists.newArrayList();
        for (ClientCriteriaThumbCreate criteria : action.getCriteria()) {
            ReviewCriteria c = new ReviewCriteria();
            c.setName(criteria.getName());
            c.setDescription(criteria.getDescription());
            criterias.add(c);
        }
        try {
            settingsService.createReviewCriterias(action.getArtefactUuid(), criterias);
            ArrayList<Long> out = Lists.newArrayList();
            for (ReviewCriteria criteria : criterias) {
                out.add(criteria.getId());
            }

            return new ArrayListResult<Long>(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            // ReviewCriteria nema name
            throw new BadInputException();
        }
    }  
}
