/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.server.action.handler.chair;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.chair.ListContributionChairReviewCyclesAction;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import consys.event.conference.gwt.server.action.utils.ClientContributionReviewCycleBuilder;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class ListContributionChairReviewCyclesActionHandler implements EventActionHandler<ListContributionChairReviewCyclesAction, ArrayListResult<ClientReviewCycle>> {

    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<ListContributionChairReviewCyclesAction> getActionType() {
        return ListContributionChairReviewCyclesAction.class;
    }

    @Override
    public ArrayListResult<ClientReviewCycle> execute(ListContributionChairReviewCyclesAction action) throws ActionException {
        try {

            // nacitame prispevek s artefaktama
            Submission submission = submissionService.loadSubmissionWithArtefacts(action.getContributionUuid());

            // nacitame recenzie
            List<ContributionReviewerReview> reviews = reviewService.listSubmissionReviews(submission.getId());

            // nacitame data artefactov
            ClientContributionReviewCycleBuilder builder = new ClientContributionReviewCycleBuilder(submission, reviews,true, reviewService, submissionService);
            return new ArrayListResult<ClientReviewCycle>(builder.createCycles(submission.getType().getUuid(), true));
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}