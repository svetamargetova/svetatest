package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.CreateReviewCycleAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Vytvoreni review cycle
 * @author pepa
 */
public class CreateReviewCycleActionHandler implements EventActionHandler<CreateReviewCycleAction, ArrayListResult<Long>> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<CreateReviewCycleAction> getActionType() {
        return CreateReviewCycleAction.class;
    }

    @Override
    public ArrayListResult<Long> execute(CreateReviewCycleAction action) throws ActionException {
        List<ReviewCycle> cycles = Lists.newArrayList();
        for (ClientSettingsReviewCycle cycle : action.getCycles()) {
            ReviewCycle c = new ReviewCycle();
            c.setReviewFrom(cycle.getReviewFrom());
            c.setReviewTo(cycle.getReviewTo());
            c.setSubmitFrom(cycle.getSubmitFrom());
            c.setSubmitTo(cycle.getSubmitTo());
            cycles.add(c);
        }
        try {
            settingsService.createReviewCycles(action.getContributionTypeUuid(), cycles);
            ArrayList<Long> out = Lists.newArrayList();
            for (ReviewCycle cycle : cycles) {
                out.add(cycle.getId());
            }

            return new ArrayListResult<Long>(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            // ReviewCycle nema nastaveny submitfrom alebo submit to
            throw new BadInputException();
        }
    }   
}
