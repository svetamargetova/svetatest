package consys.event.conference.gwt.server.action.handler.contribution;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.contribution.LoadContributionAction;
import consys.event.conference.gwt.client.bo.ClientContributionActionData;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead;
import consys.event.conference.gwt.server.action.utils.ClientContributionReviewCycleBuilder;
import consys.event.conference.gwt.server.action.utils.ReviewUtils;
import consys.event.conference.gwt.server.action.utils.SubmissionUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadContributionActionHandler implements EventActionHandler<LoadContributionAction, ClientContributionActionData> {

    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<LoadContributionAction> getActionType() {
        return LoadContributionAction.class;
    }

    @Override
    public ClientContributionActionData execute(LoadContributionAction action) throws ActionException {
        try {
            ClientContributionActionData data = new ClientContributionActionData();

            // nacitame prispevek s artefaktama
            Submission submission = submissionService.loadSubmissionWithArtefacts(action.getContributionUuid());

            // vytvorime hlavicku
            data.setHead(new ClientContributionChairHead());
            SubmissionUtils.copySubmission(submission, data.getHead());
            SubmissionUtils.copyContributors(submissionService.listSubmissionContributors(action.getContributionUuid()), data.getHead());

            // nacitame recenzie
            List<ContributionReviewerReview> reviews = reviewService.listSubmissionReviews(submission.getId());

            data.setReviews(ReviewUtils.createOverallReviews(submission, reviews, data.getHead(), false, submissionService));

            // nacitame data artefactov
            ClientContributionReviewCycleBuilder builder = new ClientContributionReviewCycleBuilder(submission, reviews, false, reviewService, submissionService);
            data.getSelectedCycles().addAll(builder.createCycles(submission.getType().getUuid(), false));

            return data;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
