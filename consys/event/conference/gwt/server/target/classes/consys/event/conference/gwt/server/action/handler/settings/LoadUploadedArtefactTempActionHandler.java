package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionArtefactPattern;
import consys.event.conference.core.bo.SubmissionArtefactTemplate;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.LoadUploadedArtefactTemplateAction;
import consys.event.conference.gwt.client.bo.setting.ClientUploadedArtefactTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadUploadedArtefactTempActionHandler implements EventActionHandler<LoadUploadedArtefactTemplateAction, ClientUploadedArtefactTemplate> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<LoadUploadedArtefactTemplateAction> getActionType() {
        return LoadUploadedArtefactTemplateAction.class;
    }

    @Override
    public ClientUploadedArtefactTemplate execute(LoadUploadedArtefactTemplateAction action) throws ActionException {
        try {
            ClientUploadedArtefactTemplate out = new ClientUploadedArtefactTemplate();
            if (action.isPattern()) {
                SubmissionArtefactPattern p = settingsService.loadPatternByUuid(action.getUuid());
                out.setUuid(p.getUuid());
                out.setName(p.getTitle());
                out.setDescription(p.getDesciption());                
            } else {
                SubmissionArtefactTemplate t = settingsService.loadTemplateByUuid(action.getUuid());
                out.setUuid(t.getUuid());
                out.setName(t.getTitle());
                out.setDescription(t.getDesciption());                
            }            
            return out;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
