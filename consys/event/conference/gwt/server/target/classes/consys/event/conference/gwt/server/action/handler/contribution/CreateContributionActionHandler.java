package consys.event.conference.gwt.server.action.handler.contribution;

import com.google.common.base.Joiner;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.InvitedUser;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.NewUserInvitation;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.bo.SubmissionArtefact;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.contribution.CreateContributionAction;
import consys.event.conference.gwt.client.bo.contribution.FileContributionArtefact;
import consys.event.conference.gwt.client.bo.contribution.TextContributionArtefact;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateContributionActionHandler implements EventActionHandler<CreateContributionAction, VoidResult> {

    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private TopicService topicService;

    @Override
    public Class<CreateContributionAction> getActionType() {
        return CreateContributionAction.class;
    }

    @Override
    public VoidResult execute(CreateContributionAction action) throws ActionException {
        try {
            Submission s = new Submission();
            s.setUuid(action.getUuid());
            s.setType(submissionService.loadSubmissionTypeWithArtefactTypes(action.getContributionTypeUuid()));
            s.getTopics().addAll(topicService.listTopicsByList(action.getTopics()));
            s.setAuthors(Joiner.on(",").join(action.getAuthors()));
            s.setSubTitle(action.getSubtitle());
            s.setTitle(action.getTitle());

            Set<Invitation> contributors = Sets.newHashSet();
            for (InvitedUser i : action.getContributors()) {
                Invitation invitation;
                if (i.isUserNew()) {
                    invitation = new NewUserInvitation(i.getEmail(), i.getName());
                } else {
                    invitation = new ExistingUserInvitation(i.getUuid());
                }
                contributors.add(invitation);
            }

            for (FileContributionArtefact uca : action.getFileArtefacts()) {
                for (SubmissionTypeArtefact sta : s.getType().getArtefacts()) {
                    if (uca.getArtefactTypeUuid().equals(sta.getUuid())) {
                        SubmissionArtefact sa = new SubmissionArtefact();
                        sa.setTypeArtefact(sta);
                        sa.setUuid(uca.getArtefactUuid());
                        s.getArtefacts().add(sa);
                    }
                }
            }

            for (TextContributionArtefact tca : action.getTextArtefacts()) {
                for (SubmissionTypeArtefact sta : s.getType().getArtefacts()) {
                    if (tca.getArtefactTypeUuid().equals(sta.getUuid())) {
                        // vytvorime na AWS                                                
                        String uuid = submissionService.createSubmissionTextArtefact(s.getUuid(), tca.getArtefactTypeUuid(), tca.getArtefactText());
                        // vytvorime ako artefakt
                        SubmissionArtefact sa = new SubmissionArtefact();
                        sa.setTypeArtefact(sta);
                        sa.setUuid(uuid);
                        s.getArtefacts().add(sa);
                    }
                }
            }

            // vytvorime
            submissionService.createSubmission(s, contributors);

            // potvrdime vytvorenie
            submissionService.approveSubmissionArtefacts(s.getUuid());

            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
