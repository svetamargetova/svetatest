package consys.event.conference.gwt.server.action.handler.contribution;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.utils.collection.Lists;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.contribution.ListTopicAction;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.server.action.utils.TopicUtils;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni seznamu topiku
 * @author pepa
 */
public class ListTopicActionHandler implements EventActionHandler<ListTopicAction, ArrayListResult<ClientTopic>> {

    @Autowired
    private TopicService topicService;

    @Override
    public Class<ListTopicAction> getActionType() {
        return ListTopicAction.class;
    }

    @Override
    public ArrayListResult<ClientTopic> execute(ListTopicAction action) throws ActionException {
        List<SubmissionTopic> topics = topicService.listTopics();
        ArrayList<ClientTopic> out = Lists.newArrayList();
        for(SubmissionTopic topic : topics){
            out.add(TopicUtils.fromSubmissionTopic(topic));
        }
        return new ArrayListResult<ClientTopic>(out);
    }   
}
