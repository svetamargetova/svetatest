package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewerFormAction;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData;
import consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer;
import consys.event.conference.gwt.server.action.utils.ClientReviewCycleBuilder;
import consys.event.conference.gwt.server.action.utils.ReviewUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadContributionReviewerFormActionHandler implements EventActionHandler<LoadContributionReviewerFormAction, ClientContributionReviewerFormData> {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<LoadContributionReviewerFormAction> getActionType() {
        return LoadContributionReviewerFormAction.class;
    }

    @Override
    public ClientContributionReviewerFormData execute(LoadContributionReviewerFormAction action) throws ActionException {
        try {            
            // Nacitame si recenziu so vsetkymi recenziami artefaktov - ale len prva urovne
            Review review = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(action.getContributionUuid(), action.getUserEventUuid());                        
            return ReviewUtils.createAndLoadReviewFormData(action.getContributionUuid(), action.getUserEventUuid(), review, submissionService, reviewService);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }


    }

    private class CycleBuilder extends ClientReviewCycleBuilder {

        private final Submission submission;
        private final Review review;

        public CycleBuilder(Review review, Submission submission, ReviewService reviewService, SubmissionService submissionService) {
            super(reviewService, submissionService);
            this.submission = submission;
            this.review = review;
        }

        @Override
        public boolean useCycleSubmitDate() {
            // false - aby sa pouzil review
            return false;
        }

        @Override
        public ClientArtefactType createClientArtefact(SubmissionTypeArtefact sta) {
            ClientReviewArtefactReviewer artefactReview = new ClientReviewArtefactReviewer();

            // Pridame vsetky artefakty
            Long existingArtefactId = null;

            // nasetujeme UUID uz existujuceho artefaktu
            for (SubmissionArtefact sa : submission.getArtefacts()) {
                if (sa.getTypeArtefact().getId().equals(sta.getId())) {
                    artefactReview.setArtefactUuid(sa.getUuid());
                    artefactReview.setFileName(sa.getFileName());
                    existingArtefactId = sa.getId();
                    break;
                }
            }
            // zistime ci uz bol recenzovany ale len ak vobec existuje
            if (existingArtefactId != null) {
                for (ReviewArtefact ra : review.getArtefactEvaluations()) {
                    if (ra.getArtefact().getId().equals(existingArtefactId)) {
                        artefactReview.setTotalScore(new EvaluationValue(ra.getScore()));
                        artefactReview.setPrivateComment(ra.getPrivateComment());
                        artefactReview.setPublicComment(ra.getPublicComment());
                    }
                }
                return artefactReview;
            } else {
                return new ClientArtefactType();
            }

        }
    }
}
