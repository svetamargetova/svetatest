package consys.event.conference.gwt.server.action.utils;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ArtefactReviewThumb;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.chair.ClientReviewArtefactChair;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author palo
 */
public class ClientContributionReviewCycleBuilder extends ClientReviewCycleBuilder {

    private Map<Long, Map<Long, List<ArtefactReviewThumb>>> artefactCriteriaReviews;
    private Submission submission;
    private List<ContributionReviewerReview> reviews;
    private Map<Long, List<ReviewCriteria>> artefactCriterias;
    private boolean editableAlways;

    public ClientContributionReviewCycleBuilder(Submission submission, List<ContributionReviewerReview> reviews, boolean editableAlways,
            ReviewService reviewService, SubmissionService submissionService) throws RequiredPropertyNullException {

        super(reviewService, submissionService);
        this.submission = submission;
        this.reviews = reviews;
        this.editableAlways = editableAlways;
        artefactCriteriaReviews = reviewService.loadArtefactCriteriaEvaluations(submission.getId());
        // Vytvorime mapu artefakt typov a kriterii
        artefactCriterias = Maps.newHashMap();
        try {
            List<ReviewCriteria> criterias = reviewService.listCriteriasForSubmissionType(submission.getType().getUuid());
            for (ReviewCriteria reviewCriteria : criterias) {
                List<ReviewCriteria> cc = artefactCriterias.get(reviewCriteria.getSubmissionTypeArtefact().getId());
                if (cc == null) {
                    cc = Lists.newArrayList();
                    artefactCriterias.put(reviewCriteria.getSubmissionTypeArtefact().getId(), cc);
                }
                cc.add(reviewCriteria);
            }
        } catch (NoRecordException ex) {
            logger.debug("{} has no artefact criterias", submission.getType());
        }
    }

    @Override
    protected boolean isAlwaysEditable() {
        return editableAlways;
    }

    @Override
    protected boolean canBeUpdated() {
        return !submission.getState().equals(SubmissionStateEnum.DECLINED);
    }

    @Override
    public ClientArtefactType createClientArtefact(SubmissionTypeArtefact artefactType) {
        // ak mame uz poslany artefact tak vlozime detail o artefakte inak posielame default
        for (SubmissionArtefact artefact : submission.getArtefacts()) {
            if (artefact.getTypeArtefact().getId().equals(artefactType.getId())) {
                final boolean autoaccepted = artefact.getTypeArtefact().isAutomaticallyAccepted();
                ClientReviewArtefactChair reviewed = createReviewArtefactChair(artefact, autoaccepted);
                // nastavime pripsevku kriteria a recenzie k tomu
                Map<Long, List<ArtefactReviewThumb>> criteriaReviewsMap = artefactCriteriaReviews.get(artefact.getId());
                List<ReviewCriteria> criterias = artefactCriterias.get(artefactType.getId());
                // iterujeme cez kriteria artefaktu a nastavujeme     
                if (criterias != null) {
                    for (ReviewCriteria reviewCriteria : criterias) {
                        // Recenzie kriterii
                        List<ArtefactReviewThumb> criteriaReviews = criteriaReviewsMap == null
                                ? new ArrayList<ArtefactReviewThumb>()
                                : criteriaReviewsMap.get(reviewCriteria.getId());

                        ClientReviewCriterion<EvaluationValue> aec = createReviewCriterion(reviewCriteria, autoaccepted);
                        reviewed.getCriterias().add(aec);
                        // Vytvorime mapu IDcek a hodnot kriteria
                        Map<Long, Integer> criteriaValuesMap = createCriteriaValuesMap(reviewCriteria);
                        // Pre vsetkych recenzentov vytvorime zaznam pre kazde kriterium
                        for (ContributionReviewerReview reviewer : reviews) {
                            ClientReviewCriterionItem<EvaluationValue> crci = createReviewCriterionItem(reviewer,
                                    autoaccepted, reviewCriteria, criteriaReviews, criteriaValuesMap);
                            aec.getItems().add(crci);
                        }
                    }
                }
                return reviewed;
            }
        }
        return new ClientArtefactType();
    }

    private Map<Long, Integer> createCriteriaValuesMap(ReviewCriteria reviewCriteria) {
        Map<Long, Integer> criteriaValuesMap = Maps.newHashMap();
        for (ReviewCriteriaItem rci : reviewCriteria.getItems()) {
            criteriaValuesMap.put(rci.getId(), rci.getValue());
        }
        return criteriaValuesMap;
    }

    private ClientReviewArtefactChair createReviewArtefactChair(SubmissionArtefact artefact, boolean autoaccepted) {
        ClientReviewArtefactChair reviewed = new ClientReviewArtefactChair();
        reviewed.setArtefactUuid(artefact.getUuid());
        reviewed.setFileName(artefact.getFileName());
        reviewed.setTotalScore(new EvaluationValue(autoaccepted ? EvaluationValue.AUTOACCEPTED : artefact.getScore()));
        reviewed.setContributionTypeUuid(artefact.getSubmission().getType().getUuid());
        return reviewed;
    }

    private ClientReviewCriterion<EvaluationValue> createReviewCriterion(ReviewCriteria reviewCriteria, boolean autoaccepted) {
        ClientReviewCriterion<EvaluationValue> crc = new ClientReviewCriterion<EvaluationValue>();
        crc.setName(reviewCriteria.getName());
        crc.setDescription(reviewCriteria.getDescription());
        crc.setValue(new EvaluationValue(autoaccepted ? EvaluationValue.AUTOACCEPTED : EvaluationValue.NOT_ENTERED));
        crc.setMaxPoints(reviewCriteria.getMaxPoints());
        return crc;
    }

    private ClientReviewCriterionItem<EvaluationValue> createReviewCriterionItem(ContributionReviewerReview reviewer,
            boolean autoaccepted, ReviewCriteria reviewCriteria,
            List<ArtefactReviewThumb> criteriaReviews, Map<Long, Integer> criteriaValuesMap) {

        ClientReviewCriterionItem<EvaluationValue> crci = new ClientReviewCriterionItem<EvaluationValue>();
        crci.setName(reviewer.getReviewerName());
        crci.setReviewUuid(reviewer.getReviewUuid());

        // Najdeme hodnotenie recenzenta alebo nastavime defaultny
        int value = autoaccepted ? EvaluationValue.AUTOACCEPTED : EvaluationValue.NOT_ENTERED;
        if (!autoaccepted) {
            for (ArtefactReviewThumb thumb : criteriaReviews) {
                if (thumb.getReviewUuid().equalsIgnoreCase(reviewer.getReviewUuid()) && thumb.getCriteriaId().equals(reviewCriteria.getId())) {
                    value = criteriaValuesMap.get(thumb.getCriteriaItemValue());
                    break;
                }
            }
        }
        crci.setValue(new EvaluationValue(value));

        return crci;
    }
}
