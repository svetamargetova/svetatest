package consys.event.conference.gwt.server.action.handler.contribution;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.contribution.LoadNewContributionAction;
import consys.event.conference.gwt.client.bo.ClientNewContributionActionData;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import consys.event.conference.gwt.server.action.utils.NewContributionArtefactsBuilder;
import java.util.List;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacte typy prispevku prispevku pro formular odeslani/editace prispevku
 * @author pepa
 */
public class LoadNewContributionActionHandler implements EventActionHandler<LoadNewContributionAction, ClientNewContributionActionData>, InitializingBean {

    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private ReviewService reviewService;
    
    private NewContributionArtefactsBuilder builder;

    @Override
    public Class<LoadNewContributionAction> getActionType() {
        return LoadNewContributionAction.class;
    }

    @Override
    public ClientNewContributionActionData execute(LoadNewContributionAction action) throws ActionException {
        try {
            // select type thumbs            
            List<SubmissionTypeThumb> types = submissionService.listSubmissionTypesThumbs(action.isInternal());
            
            ClientNewContributionActionData out = new ClientNewContributionActionData(types.get(0).getUuid());
            out.setContributionUuid(submissionService.createSubmissionUuid());           
            for (SubmissionTypeThumb st : types) {
                ClientContributionTypeThumb type = new ClientContributionTypeThumb();
                type.setUuid(st.getUuid());
                type.setName(st.getName());
                type.setDescription(st.getDescription());
                out.getTypes().add(type);
            }            
            out.getSelectedCycles().addAll(builder.createCycles(out.getContributionTypeUuid(), true));
            return out;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        builder = new NewContributionArtefactsBuilder(reviewService, submissionService);
    }
    
    
    
}
