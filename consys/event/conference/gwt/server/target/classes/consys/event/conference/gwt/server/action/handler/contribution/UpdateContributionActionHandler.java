package consys.event.conference.gwt.server.action.handler.contribution;

import com.google.common.base.Joiner;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.InvitedUser;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.NewUserInvitation;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.contribution.UpdateContributionAction;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateContributionActionHandler implements EventActionHandler<UpdateContributionAction, VoidResult> {

    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private TopicService topicService;

    @Override
    public Class<UpdateContributionAction> getActionType() {
        return UpdateContributionAction.class;
    }

    @Override
    public VoidResult execute(UpdateContributionAction action) throws ActionException {
        try {                                                         
            Set<Invitation> contributors = Sets.newHashSet();
            for (InvitedUser i : action.getContributors()) {
                Invitation invitation = null;
                if (i.isUserNew()) {
                    invitation = new NewUserInvitation(i.getEmail(), i.getName());
                } else {
                    invitation = new ExistingUserInvitation(i.getUuid());
                }
                contributors.add(invitation);
            }
            
            submissionService.updateSubmission(
                    action.getContributionUuid(), 
                    action.getTitle(), 
                    action.getSubtitle(), Joiner.on(",").join(action.getAuthors()),
                    contributors, 
                    topicService.listTopicsByList(action.getTopics()));                                                            
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }

    }
}
