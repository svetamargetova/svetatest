package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.LoadEditReviewCycleAction;
import consys.event.conference.gwt.client.bo.ClientEditReviewCycle;
import consys.event.conference.gwt.server.action.utils.ArtefactUtils;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni review cycle
 * @author pepa
 */
public class LoadEditReviewCycleActionHandler implements EventActionHandler<LoadEditReviewCycleAction, ClientEditReviewCycle> {

    
    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<LoadEditReviewCycleAction> getActionType() {
        return LoadEditReviewCycleAction.class;
    }

    @Override
    public ClientEditReviewCycle execute(LoadEditReviewCycleAction action) throws ActionException {
        try {
            ReviewCycle cycle = settingsService.loadReviewCycleWithTypeById(action.getReviewCycleId());
            ClientEditReviewCycle crc = new ClientEditReviewCycle();
            ArtefactUtils.toClientReviewCycle(cycle, crc);
            crc.setNotAssignedArtefacts((ArrayList<String>) settingsService.listNotAssignedArtefactsUuids(cycle.getSubmissionType().getId()));
            return crc;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
