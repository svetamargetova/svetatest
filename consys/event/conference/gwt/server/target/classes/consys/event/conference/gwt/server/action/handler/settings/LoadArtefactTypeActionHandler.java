package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.thumb.SubmissionTypeArtefactThumb;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.LoadArtefactTypeAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaThumb;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsArtefactType;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni typu artefaktu
 * @author pepa
 */
public class LoadArtefactTypeActionHandler implements EventActionHandler<LoadArtefactTypeAction, ClientSettingsArtefactType> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<LoadArtefactTypeAction> getActionType() {
        return LoadArtefactTypeAction.class;
    }
 
    @Override
    public ClientSettingsArtefactType execute(LoadArtefactTypeAction action) throws ActionException {
        ClientSettingsArtefactType out = new ClientSettingsArtefactType();
        try {
            SubmissionTypeArtefactThumb artefactThumb = settingsService.loadTypeArtefactThumbByUuid(action.getUuid());
            // criteria
            for (ListLongItem criteria : artefactThumb.getCriterions()) {
                ClientCriteriaThumb ct = new ClientCriteriaThumb();
                ct.setName(criteria.getName());
                ct.setId(criteria.getId());
                out.getCriteriaList().add(ct);
            }
            // properties
            out.setArtefactTypeDescription(artefactThumb.getDescription());
            out.setDataType(artefactThumb.isFile() ? ClientContributionArtefactDataType.FILE : ClientContributionArtefactDataType.TEXT);
            out.setFileFormats(artefactThumb.getFormats());
            out.setMaxInputLength(artefactThumb.getInputLength());
            out.setArtefactTypeName(artefactThumb.getName());
            out.setRequired(artefactThumb.isRequired());
            out.setNumReviewers(artefactThumb.getNumOfReviewers());
            // artefact pattern
            for (ListUuidItem item : artefactThumb.getPatterns()) {
                CommonThumb ct = new CommonThumb();
                ct.setName(item.getName());
                ct.setUuid(item.getUuid());
                out.getUploadedExamples().add(ct);
            }
            // artefact templates
            for (ListUuidItem item : artefactThumb.getTemplates()) {
                CommonThumb ct = new CommonThumb();
                ct.setName(item.getName());
                ct.setUuid(item.getUuid());
                out.getUploadedTemplates().add(ct);
            }
            out.setArtefactTypeUuid(action.getUuid());
            return out;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }   
}
