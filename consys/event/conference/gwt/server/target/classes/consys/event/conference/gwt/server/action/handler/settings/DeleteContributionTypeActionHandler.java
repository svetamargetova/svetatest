package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.client.action.exception.DeleteRelationException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.exception.DeleteRelationsException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.DeleteContributionTypeAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Smazani typu prispevku
 * @author pepa
 */
public class DeleteContributionTypeActionHandler implements EventActionHandler<DeleteContributionTypeAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<DeleteContributionTypeAction> getActionType() {
        return DeleteContributionTypeAction.class;
    }

    @Override
    public VoidResult execute(DeleteContributionTypeAction action) throws ActionException {
        try {
            settingsService.deleteSubmissionType(action.getUuid());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (DeleteRelationsException ex) {
            throw new DeleteRelationException();
        }
    }    
}
