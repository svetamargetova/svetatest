package consys.event.conference.gwt.server.servlet;

import consys.event.common.core.bo.DataFile;
import consys.event.common.gwt.server.servlet.AbstractFileUploadServlet;
import consys.event.conference.core.service.SubmissionSettingsService;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstraktny predok pre upload templetu alebo patternu
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractUploadHelpFileServlet extends AbstractFileUploadServlet {

    private static final long serialVersionUID = 1540975108423221667L;
    @Autowired
    private SubmissionSettingsService settingsService;

    protected SubmissionSettingsService getSettingsService() {
        return settingsService;
    }

    @Override
    protected void saveFile(List<DataFile> files, Map<String, String> formFields, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (files.isEmpty()) {
            logger.warn("File is required!");
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        if (files.size() > 1 && logger.isWarnEnabled()) {
            logger.warn("Only ONE file is required! Getting first");
        }

        String title = formFields.get("t");
        String description = formFields.get("d");
        DataFile file = files.get(0);

        if (StringUtils.isBlank(title) || file == null) {
            logger.warn("Uploading file failed! Missing title or file itself!");
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }
        logger.debug("Uploading file: {}", title);
        String uuid = saveDataFile(file, title, description);
        response.getOutputStream().print(String.format("<ok>%s</ok>", uuid));
    }

    protected abstract String saveDataFile(DataFile file, String title, String description);
}
