package consys.event.conference.gwt.server.servlet;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.DataFile;
import consys.event.conference.api.servlet.ConferenceSubmissionArtefactUploadServlet;
import consys.event.conference.core.bo.thumb.ArtefactData;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet ktory spracuvava suborove artefakty pre novy prispevok
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class NewSubmissionArtefactServlet extends AbstractSubmitServlet implements ConferenceSubmissionArtefactUploadServlet {

    private static final long serialVersionUID = 1540975108423221667L;

    @Override
    protected void saveFile(List<DataFile> files, Map<String, String> formFields, HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            
            ArtefactData data = new ArtefactData();
            data.setByteData(files.get(0).getByteData());
            data.setContentType(files.get(0).getContentType());
            data.setFile(true);
            data.setFileName(files.get(0).getFileName());
            
            String artefactUuid = getSubmissionService().createSubmissionArtefact(getSubmissionUuid(formFields), getArtefactTypeUuid(formFields), data);
            response.getOutputStream().print(String.format("<ok>%s</ok>", artefactUuid));
        } catch (ServiceExecutionFailed ex) {
            sendError("Service failed", response);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (RequiredPropertyNullException ex) {
            sendError("Missing uuids", response);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
