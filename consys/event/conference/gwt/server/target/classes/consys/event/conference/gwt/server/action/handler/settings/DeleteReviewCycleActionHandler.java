package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.exception.DeleteCycleException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.DeleteReviewCycleAction;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCycleException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Smazani kolecka hodnoceni
 * @author pepa
 */
public class DeleteReviewCycleActionHandler implements EventActionHandler<DeleteReviewCycleAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<DeleteReviewCycleAction> getActionType() {
        return DeleteReviewCycleAction.class;
    }

    @Override
    public VoidResult execute(DeleteReviewCycleAction action) throws ActionException {
        try {
            settingsService.deleteReviewCycle(action.getCycleId());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (DeleteCycleException ex) {
            throw new DeleteReviewCycleException();
        }
    }
}
