package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.LoadContributionSettingAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni nastaveni prispevku
 * @author pepa
 */
public class LoadContributionSettingActionHandler implements EventActionHandler<LoadContributionSettingAction, ArrayListResult<CommonThumb>> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<LoadContributionSettingAction> getActionType() {
        return LoadContributionSettingAction.class;
    }

    @Override
    public ArrayListResult<CommonThumb> execute(LoadContributionSettingAction action) throws ActionException {
        try {
            List<ListUuidItem> types = settingsService.listSettingsThumbTypes();
            ArrayList<CommonThumb> out = Lists.newArrayList();
            for (ListUuidItem type : types) {
                CommonThumb th = new CommonThumb();
                th.setName(type.getName());
                th.setUuid(type.getUuid());
                out.add(th);
            }
            return new ArrayListResult<CommonThumb>(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }    
}
