package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.exception.TopicNotUniqueException;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.exception.TopicUniqueException;
import consys.event.conference.gwt.client.action.setting.CreateTopicAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CreateTopicActionHandler implements EventActionHandler<CreateTopicAction, LongResult> {

    @Autowired
    private TopicService topicService;

    @Override
    public Class<CreateTopicAction> getActionType() {
        return CreateTopicAction.class;
    }

    @Override
    public LongResult execute(CreateTopicAction action) throws ActionException {
        SubmissionTopic newTopic = new SubmissionTopic();
        try {
            newTopic.setName(action.getName());
            topicService.createTopic(newTopic);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (TopicNotUniqueException ex) {
            throw new TopicUniqueException();
        }
        return new LongResult(newTopic.getId());
    }
}
