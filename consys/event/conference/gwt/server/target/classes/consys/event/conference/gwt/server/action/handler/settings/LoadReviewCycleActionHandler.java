package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.LoadReviewCycleAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import consys.event.conference.gwt.server.action.utils.ArtefactUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni review cycle
 * @author pepa
 */
public class LoadReviewCycleActionHandler implements EventActionHandler<LoadReviewCycleAction, ClientSettingsReviewCycle> {
    
    // servica
    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<LoadReviewCycleAction> getActionType() {
        return LoadReviewCycleAction.class;
    }

    @Override
    public ClientSettingsReviewCycle execute(LoadReviewCycleAction action) throws ActionException {
        try {                        
            ReviewCycle cycle = settingsService.loadReviewCycleWithTypeById(action.getReviewCycleId());            
            ClientSettingsReviewCycle crc = new ClientSettingsReviewCycle();
            ArtefactUtils.toClientReviewCycle(cycle, crc);
            return crc;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }  
}
