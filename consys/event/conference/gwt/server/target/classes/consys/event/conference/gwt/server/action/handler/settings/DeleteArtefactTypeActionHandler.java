package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.exception.DeleteCriteriaException;
import consys.event.conference.core.exception.DeleteCycleException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.DeleteArtefactTypeAction;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCriteriaException;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCycleException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Smazani typu artefaktu
 * @author pepa
 */
public class DeleteArtefactTypeActionHandler implements EventActionHandler<DeleteArtefactTypeAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<DeleteArtefactTypeAction> getActionType() {
        return DeleteArtefactTypeAction.class;
    }

    @Override
    public VoidResult execute(DeleteArtefactTypeAction action) throws ActionException {
        try {
            settingsService.deleteSubmissionTypeArtefact(action.getUuid());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (DeleteCriteriaException ex) {
            throw new DeleteReviewCriteriaException();
        } catch (DeleteCycleException ex) {
            throw new DeleteReviewCycleException();
        }
    }  
}
