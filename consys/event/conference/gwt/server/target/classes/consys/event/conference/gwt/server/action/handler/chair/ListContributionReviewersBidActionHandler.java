package consys.event.conference.gwt.server.action.handler.chair;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.bo.thumb.ReviewerThumb;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.chair.ListContributionReviewersBidAction;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead;
import consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum;
import consys.event.conference.gwt.client.bo.setting.ClientAssignReviewers;
import consys.event.conference.gwt.client.bo.setting.ClientReviewer;
import consys.event.conference.gwt.server.action.utils.SubmissionUtils;
import consys.event.conference.gwt.server.action.utils.TopicUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni dat prirazeni recenzentu k prispevku
 * @author pepa
 */
public class ListContributionReviewersBidActionHandler implements EventActionHandler<ListContributionReviewersBidAction, ClientAssignReviewers> {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<ListContributionReviewersBidAction> getActionType() {
        return ListContributionReviewersBidAction.class;
    }

    @Override
    public ClientAssignReviewers execute(ListContributionReviewersBidAction action) throws ActionException {
        try {
            ClientAssignReviewers car = new ClientAssignReviewers();
            Submission s = submissionService.loadSubmissonWithTypeAndTopicsByUuid(action.getUuid());
            
            car.setContributionHead(new ClientContributionChairHead());            
            SubmissionUtils.copySubmission(s, car.getContributionHead());
            SubmissionUtils.copyContributors(submissionService.listSubmissionContributors(s.getUuid()), car.getContributionHead());
            
            car.setTopics(TopicUtils.fromSubmissionTopicList(s.getTopics()));
            List<ReviewerThumb> reviewers = reviewService.listReviewersPropertiesToSubmission(s.getUuid());
            for (ReviewerThumb reviewer : reviewers) {
                ClientReviewer rev = new ClientReviewer();
                switch (reviewer.getInterest()) {
                    case CONFLICT:
                        rev.setInterest(ReviewerInterestEnum.CONFLICT);
                        break;
                    case DONT_LIKE:
                        rev.setInterest(ReviewerInterestEnum.NOT_WANT);
                        break;
                    case LIKE:
                        rev.setInterest(ReviewerInterestEnum.LIKE);
                        break;
                    case NOT_CARE:
                        rev.setInterest(ReviewerInterestEnum.NOT_CARE);
                        break;
                }
                rev.setLoad(reviewer.getLoad());
                rev.setName(reviewer.getFullName());
                rev.setTopics(TopicUtils.fromSubmissionTopicList(reviewer.getPrefferedTopics()));
                rev.setUuid(reviewer.getUserUuid());
                rev.setReviewing(reviewer.isReviewing());
                car.getReviewers().add(rev);
            }
            return car;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }  
}
