package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.utils.collection.Lists;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionArtefactType;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.setting.LoadContributionTypeAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionType;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import consys.event.conference.gwt.client.bo.setting.ClientReviewCycleThumb;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni typu prispevku (s thumby artefaktu)
 * @author pepa
 */
public class LoadContributionTypeActionHandler implements EventActionHandler<LoadContributionTypeAction, ClientContributionType> {
    
    @Autowired
    private SubmissionService settingsService;

    @Override
    public Class<LoadContributionTypeAction> getActionType() {
        return LoadContributionTypeAction.class;
    }

    @Override
    public ClientContributionType execute(LoadContributionTypeAction action) throws ActionException {
        try {
            SubmissionType st = settingsService.loadSubmissionTypeWithArtefactsAndCyclesByUuid(action.getUuid());
            ClientContributionType ct = new ClientContributionType();
            ct.setName(st.getName());
            ct.setDescription(st.getDescription());
            ct.setUuid(st.getUuid());
            ct.setInternal(st.isInternType());            
            ArrayList<ClientArtefactTypeThumb> out = Lists.newArrayList();
            for (SubmissionTypeArtefact at : st.getArtefacts()) {
                SubmissionArtefactType sat = at.getArtefactType();                
                ClientArtefactTypeThumb t = new ClientArtefactTypeThumb();
                t.setArtefactTypeName(sat.getName());
                t.setArtefactTypeUuid(at.getUuid());
                out.add(t);
            }
            ct.setClientArtefactTypeList(out);
            // Thumby cyklov
            ArrayList<ClientReviewCycleThumb> cycles = Lists.newArrayList();
            for (ReviewCycle cycle : st.getCycles()) {
                ClientReviewCycleThumb cthumb = new ClientReviewCycleThumb();
                cthumb.setId(cycle.getId());
                cthumb.setSubmitFrom(cycle.getSubmitFrom());
                cthumb.setSubmitTo(cycle.getSubmitTo());
                cycles.add(cthumb);                
            }
            ct.setClientReviewCycleList(cycles);
            return ct;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }   
}
