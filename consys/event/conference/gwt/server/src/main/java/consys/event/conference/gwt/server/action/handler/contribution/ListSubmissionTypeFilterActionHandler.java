package consys.event.conference.gwt.server.action.handler.contribution;

import com.google.common.collect.Lists;
import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.contribution.ListSubmissionTypeFilterAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListSubmissionTypeFilterActionHandler implements EventActionHandler<ListSubmissionTypeFilterAction, ArrayListResult<CommonThumb>> {

    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<ListSubmissionTypeFilterAction> getActionType() {
        return ListSubmissionTypeFilterAction.class;
    }

    @Override
    public ArrayListResult<CommonThumb> execute(ListSubmissionTypeFilterAction action) throws ActionException {
        try {
            List<SubmissionTypeThumb> thumbs = submissionService.listSubmissionTypesThumbs(true);
            ArrayList<CommonThumb> out = Lists.newArrayListWithCapacity(thumbs.size());
            for(SubmissionTypeThumb s : thumbs){
                out.add(new CommonThumb(s.getUuid(), s.getName()));
            }
            return new ArrayListResult<CommonThumb>(out);
        } catch (NoRecordException ex) {
            return new ArrayListResult<CommonThumb>();
        }
    }
}
