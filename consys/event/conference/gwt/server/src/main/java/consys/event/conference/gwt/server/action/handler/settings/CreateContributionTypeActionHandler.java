package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.utils.collection.Lists;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.exception.SubmissionTypeUniqueException;
import consys.event.conference.gwt.client.action.setting.CreateContributionTypeAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Vytvoreni typu prispevku
 * @author pepa
 */
public class CreateContributionTypeActionHandler implements EventActionHandler<CreateContributionTypeAction, ArrayListResult<String>> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<CreateContributionTypeAction> getActionType() {
        return CreateContributionTypeAction.class;
    }

    @Override
    public ArrayListResult<String> execute(CreateContributionTypeAction action) throws ActionException {

        List<SubmissionType> types = Lists.newArrayList();

        for (ClientContributionTypeThumb cct : action.getTypes()) {
            SubmissionType type = new SubmissionType();
            type.setName(cct.getName());
            type.setDescription(cct.getDescription());
            types.add(type);
        }
        try {
            settingsService.createSubmissionTypes(types);

            ArrayList<String> uuids = Lists.newArrayList();
            for (SubmissionType type : types) {
                uuids.add(type.getUuid());
            }
            return new ArrayListResult<String>(uuids);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (SubmissionTypeNotUniqueException ex) {
            throw new SubmissionTypeUniqueException();
        }
    }   
}
