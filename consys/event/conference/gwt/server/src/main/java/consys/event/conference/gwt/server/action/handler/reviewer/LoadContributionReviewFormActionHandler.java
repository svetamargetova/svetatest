package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Review;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.reviewer.LoadContributionReviewFormAction;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData;
import consys.event.conference.gwt.server.action.utils.ReviewUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadContributionReviewFormActionHandler implements EventActionHandler<LoadContributionReviewFormAction, ClientContributionReviewerFormData> {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<LoadContributionReviewFormAction> getActionType() {
        return LoadContributionReviewFormAction.class;
    }

    @Override
    public ClientContributionReviewerFormData execute(LoadContributionReviewFormAction action) throws ActionException {
        try {            
            Review review = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(action.getReviewUuid());            
            return ReviewUtils.createAndLoadReviewFormData(action.getContributionUuid(), action.getUserEventUuid(), review, submissionService, reviewService);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    
}
