package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.gwt.client.action.reviewer.UpdateContributionCommentAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateContributionCommentActionHandler implements EventActionHandler<UpdateContributionCommentAction, VoidResult>{

    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<UpdateContributionCommentAction> getActionType() {
        return UpdateContributionCommentAction.class;
    }

    @Override
    public VoidResult execute(UpdateContributionCommentAction action) throws ActionException {
        try {
            reviewService.updateSubmissionReviewComments(action.getContributionUuid(), action.getUserEventUuid(), action.getPublicComment(), action.getPrivateComment());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } 
    }   
}
