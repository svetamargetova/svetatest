package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewArtefact;
import consys.event.conference.core.bo.ReviewArtefactCriteriaEvaluation;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.gwt.client.action.reviewer.LoadContributionArtefactReviewAction;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.reviewer.ClientReviewReviewerData;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.server.action.utils.ReviewUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadContributionArtefactReviewActionHandler implements EventActionHandler<LoadContributionArtefactReviewAction, ClientReviewReviewerData> {

    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<LoadContributionArtefactReviewAction> getActionType() {
        return LoadContributionArtefactReviewAction.class;
    }

    @Override
    public ClientReviewReviewerData execute(LoadContributionArtefactReviewAction action) throws ActionException {
        try {
            ClientReviewReviewerData data = new ClientReviewReviewerData();

            // nastavime kriteria ktore sa recenzuju
            List<ReviewCriteria> criterias = reviewService.listCriteriasForSubmissionTypeArtefact(action.getArtefactTypeUuid());
            data.setCriterias(ReviewUtils.toReviewCriterion(criterias));

            // Ak uz bol artefakt recenzovany nastavime hodnotenie
            try {

                ReviewArtefact ra;
                if (action.getReviewUuid() == null) {
                    ra = reviewService.loadReviewArtefactEvaluation(action.getArtefactUuid(), action.getUserEventUuid());
                } else {
                    ra = reviewService.loadReviewArtefactEvaluationForReview(action.getArtefactUuid(), action.getReviewUuid());
                }


                data.setScore(new EvaluationValue(ra.getScore()));
                data.setPrivateComment(ra.getPrivateComment());
                data.setPublicComment(ra.getPublicComment());
                // pre kazde kriterium vyberieme vyhodnitenie a nastavime hodnoty
                for (ClientReviewCriterion criterion : data.getCriterias()) {
                    for (ReviewArtefactCriteriaEvaluation race : ra.getCriteriaEvaluations()) {
                        if (criterion.getId().equals(race.getCriteria().getId())) {
                            data.getEnteredEvaluations().add(race.getCriteriaItem().getId());
                        }
                    }
                }
            } catch (NoRecordException e) {
                // este nema hodnotenie
                data.setScore(new EvaluationValue());
            }

            return data;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }

    }
}
