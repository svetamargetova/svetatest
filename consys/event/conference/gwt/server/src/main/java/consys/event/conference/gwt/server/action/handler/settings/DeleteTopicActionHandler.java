package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.client.action.exception.DeleteRelationException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.exception.DeleteRelationsException;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.setting.DeleteTopicAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DeleteTopicActionHandler implements EventActionHandler<DeleteTopicAction, VoidResult> {

    @Autowired
    private TopicService topicService;

    @Override
    public Class<DeleteTopicAction> getActionType() {
        return DeleteTopicAction.class;
    }

    @Override
    public VoidResult execute(DeleteTopicAction action) throws ActionException {
        try {
            topicService.deleteTopic(action.getId());
            return VoidResult.RESULT();
        } catch (DeleteRelationsException ex) {
            throw new DeleteRelationException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
