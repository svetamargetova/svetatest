package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopicsIdsAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListReviewerPreferredTopicsIdsActionHandler implements EventActionHandler<ListReviewerPreferredTopicsIdsAction, ArrayListResult<Long>>{


    @Autowired
    private TopicService topicService;


    @Override
    public Class<ListReviewerPreferredTopicsIdsAction> getActionType() {
        return ListReviewerPreferredTopicsIdsAction.class;
    }

    @Override
    public ArrayListResult<Long> execute(ListReviewerPreferredTopicsIdsAction action) throws ActionException {
        List<Long> reviewersTopics = topicService.listReviewerPreferredTopics(action.getUserEventUuid());        
        return new ArrayListResult<Long>(new ArrayList<Long>(reviewersTopics));
    }
}
