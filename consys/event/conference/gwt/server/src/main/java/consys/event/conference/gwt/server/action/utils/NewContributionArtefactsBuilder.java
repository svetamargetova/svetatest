/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.server.action.utils;

import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.bo.ClientArtefactType;

/**
 *
 * @author palo
 */
public class NewContributionArtefactsBuilder extends ClientReviewCycleBuilder {

    public NewContributionArtefactsBuilder(ReviewService reviewService, SubmissionService submissionService) {
        super(reviewService, submissionService);
    }

    @Override
    public ClientArtefactType createClientArtefact(SubmissionTypeArtefact artefact) {
        return new ClientArtefactType();
    }
    
}
