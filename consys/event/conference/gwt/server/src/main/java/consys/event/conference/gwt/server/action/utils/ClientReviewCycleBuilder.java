/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.server.action.utils;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionArtefactDataType;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCycle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Builder na generovanie kleisntkych artefaktov vzhladom na charakter
 * poziadavku.
 * <p/>
 * @author palo
 */
public abstract class ClientReviewCycleBuilder {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ReviewService reviewService;
    private final SubmissionService submissionService;

    public ClientReviewCycleBuilder(ReviewService reviewService, SubmissionService submissionService) {
        this.reviewService = reviewService;
        this.submissionService = submissionService;
    }

    public boolean useCycleSubmitDate() {
        return true;
    }

    /**
     * Utils metoda ktora spracuje vstup pre forular odoslania prispevkov
     *
     * @param submissionType UUID typu pre ktory sa maju vyhladat cykly a ich
     * artefakty
     * @param forNewSubmission priznak ci prvy cyklus definuje vlastnosti dalsich tj.
     * prvy false => ostatne false
     * @param reviewService sluzba ktora je potrebna na dodatocne ziskanie dat
     *
     * @return Zoznam klientskych cyklov s priznakom ci su editovatelne
     */
    public ArrayList<ClientReviewCycle> createCycles(String submissionType, boolean forNewSubmission)
            throws NoRecordException, RequiredPropertyNullException {
        ArrayList<ClientReviewCycle> out = Lists.newArrayList();

        try {
            // Ak NoRecordsException tak posleme jeden cyklus so vsetkymi artefaktami
            List<ReviewCycle> cycles = reviewService.listReviewCyclesForType(submissionType);

            /*
             * Ak je to first cycle tak v pripade ze je prvy cyklus uz
             * needitovatelny tak vsetky ostatne budu tiez needitovatelne.
             */
            boolean allEditable = !forNewSubmission;

            final Date today = DateProvider.getCurrentDate();
            for (int i = 0; i < cycles.size(); i++) {
                ReviewCycle cycle = cycles.get(i);

                // Nastavime datumy vzhladem na pouzity interval - posilani vs. recenzia               
                ClientReviewCycle csc = new ClientReviewCycle();
                if (useCycleSubmitDate()) {
                    csc.setFrom(cycle.getSubmitFrom());
                    csc.setTo(cycle.getSubmitTo());
                } else {
                    csc.setFrom(cycle.getReviewFrom());
                    csc.setTo(cycle.getReviewTo());
                }


                // Ak je firstCycle==true potom prvy cyklus urcuje moznost editovat dalsie                                                          
                if (i == 0 && forNewSubmission) {
                    if (today.compareTo(cycle.getSubmitFrom()) >= 0 && today.compareTo(cycle.getSubmitTo()) <= 0) {
                        allEditable = true;
                    } else {
                        allEditable = false;
                    }
                }


                boolean isEditable = false;

                if (isAlwaysEditable()) {
                    isEditable = true;
                } else if (canBeUpdated()) {
                    if (csc.getFrom() == null || csc.getTo() == null) {
                        isEditable = true;
                    } else if (allEditable && today.compareTo(csc.getFrom()) >= 0 && today.compareTo(csc.getTo()) <= 0) {
                        isEditable = true;
                    }
                } else{
                    isEditable = false;
                }

                csc.setEditable(isEditable);

                logger.info("Cycle {} - {}: editable {}", new Object[]{csc.getFrom(), csc.getTo(), csc.isEditable()});

                // Spracujeme vsetky artefakty                
                csc.setArtefacts(processArtefacts(cycle.getArtefacts()));
                out.add(csc);
            }
        } catch (NoRecordException nre) {
            List<SubmissionTypeArtefact> artefacts = submissionService.listAllArtefactsForType(submissionType);
            ClientReviewCycle csc = new ClientReviewCycle();
            csc.setEditable(true);
            csc.setFrom(null); // znaci ze vsetky artefakty su editovatelne
            csc.setTo(null);
            csc.setArtefacts(processArtefacts(artefacts));
            out.add(csc);
        }
        return out;
    }

    /**
     * Priznak ze prispevok je mozne upravit kedykolvek bez ohladu na cykly
     * Funguje ot tak ze: IF[always editable] => vsetky cykly editovatelne ELSE
     * IF[can be updated] => spracuj cykly a nastav priznak na zaklade casu ELSE
     * => vsetko needitovatelne
     *
     */
    protected boolean isAlwaysEditable() {
        return false;
    }

    /**
     * Priznak ze je mozne prispevok upravovat
     */
    protected boolean canBeUpdated() {
        return true;
    }

    protected ArrayList<ClientArtefactType> processArtefacts(List<SubmissionTypeArtefact> artefacts) throws RequiredPropertyNullException {

        ArrayList<ClientArtefactType> clientArtefacts = new ArrayList<ClientArtefactType>();

        for (SubmissionTypeArtefact submissionTypeArtefact : artefacts) {
            ClientArtefactType artefact = createClientArtefact(submissionTypeArtefact);
            copyArtefactType(submissionTypeArtefact, artefact, true);

            clientArtefacts.add(artefact);
        }

        return clientArtefacts;
    }

    public abstract ClientArtefactType createClientArtefact(SubmissionTypeArtefact artefact);

    private void copyArtefactType(SubmissionTypeArtefact artefact, ClientArtefactType a, boolean insertTemplates) throws RequiredPropertyNullException {
        a.setArtefactTypeUuid(artefact.getUuid());
        a.setArtefactTypeName(artefact.getArtefactType().getName());
        a.setArtefactTypeDescription(artefact.getArtefactType().getDescription());
        a.setRequired(artefact.isRequired());
        a.setAutoAccepted(artefact.isAutomaticallyAccepted());
        a.setDataType(artefact.getArtefactType().isFile() ? ClientContributionArtefactDataType.FILE : ClientContributionArtefactDataType.TEXT);
        a.setFileFormats(artefact.getArtefactType().getFormats());
        a.setMaxInputLength(artefact.getArtefactType().getInputLength());
        // Ak je cyklus editovatelny tak sa inicializuju patterny a templaty
        if (insertTemplates) {
            List<ListUuidItem> items = submissionService.listArtefactPatterns(artefact.getArtefactType().getId());
            a.setUploadedExamples(ArtefactUtils.fromListUuidItems(items));
            items = submissionService.listArtefactTemplates(artefact.getArtefactType().getId());
            a.setUploadedTemplates(ArtefactUtils.fromListUuidItems(items));
        }
    }
}
