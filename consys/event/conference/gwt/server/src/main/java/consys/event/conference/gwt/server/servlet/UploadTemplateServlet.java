package consys.event.conference.gwt.server.servlet;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.DataFile;
import consys.event.conference.core.bo.SubmissionArtefactTemplate;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UploadTemplateServlet extends AbstractUploadHelpFileServlet {

    private static final long serialVersionUID = 7885738397801648485L;

    @Override
    protected String saveDataFile(DataFile file, String title, String description) {
        try {
            SubmissionArtefactTemplate artefactTemplate = new SubmissionArtefactTemplate();
            artefactTemplate.setDesciption(description);
            artefactTemplate.setTitle(title);
            getSettingsService().createSubmissionTemplate(artefactTemplate, file);
            return artefactTemplate.getUuid();
        } catch (ServiceExecutionFailed ex) {
            throw new IllegalArgumentException(ex);
        } catch (RequiredPropertyNullException ex) {
            throw new IllegalArgumentException(ex);
        }

    }
}
