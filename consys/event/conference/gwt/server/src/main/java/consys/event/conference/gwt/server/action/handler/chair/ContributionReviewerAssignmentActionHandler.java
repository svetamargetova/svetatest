package consys.event.conference.gwt.server.action.handler.chair;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.gwt.client.action.chair.ContributionReviewerAssignmentAction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ContributionReviewerAssignmentActionHandler implements EventActionHandler<ContributionReviewerAssignmentAction, VoidResult> {

    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<ContributionReviewerAssignmentAction> getActionType() {
        return ContributionReviewerAssignmentAction.class;
    }

    @Override
    public VoidResult execute(ContributionReviewerAssignmentAction action) throws ActionException {
        try {
            // oznacnie
            if (action.isAssigned()) {
                reviewService.createReviewRelation(action.getSubmissionUuid(), action.getEventUserUuid());
            } else {                
                reviewService.deleteReviewerForSubmission(action.getSubmissionUuid(), action.getEventUserUuid(),action.isForce());
            }
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }   
}
