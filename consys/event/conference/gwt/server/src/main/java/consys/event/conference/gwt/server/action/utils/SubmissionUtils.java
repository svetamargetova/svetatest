package consys.event.conference.gwt.server.action.utils;

import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.common.core.bo.UserEvent;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead;
import consys.event.conference.gwt.client.bo.contribution.*;
import consys.event.conference.gwt.client.bo.evaluation.Evaluation;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.list.ClientListContributionItem;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionUtils {

    public static void copySubmission(Submission overview, ClientContributionHeadData out) {
        out.setAuthors(overview.getAuthors());
        out.setTitle(overview.getTitle());
        out.setSubtitle(overview.getSubTitle());
        out.setTopics(overview.getAgregatedTopics());
        out.setUuid(overview.getUuid());
        out.setSubmittedDate(overview.getSubmittedDate());
        out.setType(overview.getType().getName());
        out.setEvaluations(new Evaluation<EvaluationValue, EvaluationValue>());
        out.getEvaluations().setOverallScore(new EvaluationValue(overview.getScore()));
        switch (overview.getState()) {
            case ACCEPTED:
                out.setState(ClientContributionStateEnum.ACCEPTED);
                break;
            case DECLINED:
                out.setState(ClientContributionStateEnum.DECLINED);
                break;
            default:
                out.setState(ClientContributionStateEnum.PENDING);
                break;
        }

    }

    public static void copyContributors(List<UserEvent> contributors, ClientContributionHeadData out) {
        for (UserEvent ue : contributors) {
            out.getContributors().add(new ClientContributor(ue.getUuid(), ue.getFullName()));
        }
    }

    public static ClientContributionHeadData fromSubmission(Submission overview) {
        ClientContributionHeadData head = new ClientContributionHeadData();
        copySubmission(overview, head);
        return head;
    }

    public static ClientContribution fromSubmission(Submission s, boolean addUserEventsAuthors) {
        ClientContribution c = new ClientContribution();
        c.setAuthors(s.getAuthors());
        c.setSubtitle(s.getSubTitle());
        c.setTitle(s.getTitle());
        c.setUuid(s.getUuid());
        c.setTopics(TopicUtils.fromSubmissionTopicList(s.getTopics()));
        if (addUserEventsAuthors) {
            for (UserEvent ue : s.getContributors()) {
                c.getSubmitters().add(new CommonThumb(ue.getUuid(), ue.getFullName()));
            }
        }
        return c;
    }

    public static ClientContributionTypeThumb fromSubmissionType(SubmissionType type) {
        ClientContributionTypeThumb thumb = new ClientContributionTypeThumb();
        thumb.setDescription(type.getDescription());
        thumb.setName(type.getName());
        thumb.setUuid(type.getUuid());
        return thumb;
    }

    public static ClientListContributionItem fromListItem(SubmissionListItem s) {
        // vytvorime zakladny popis prisevku
        ClientContributionHeadData o = new ClientContributionChairHead();
        o.setAuthors(s.getAuthors());
        o.setSubtitle(s.getSubtitle());
        o.setTitle(s.getTitle());
        o.setUuid(s.getUuid());
        o.setSubmittedDate(s.getSubmitDate());
        o.setTopics(s.getAgregatedTopics());
        o.setType(s.getType());
        switch (s.getStateEnum()) {
            case ACCEPTED:
                o.setState(ClientContributionStateEnum.ACCEPTED);
                break;
            case DECLINED:
                o.setState(ClientContributionStateEnum.DECLINED);
                break;
            default:
                o.setState(ClientContributionStateEnum.PENDING);
                break;
        }
        
        // nastavime hodnotenia
        o.setEvaluations(new Evaluation<EvaluationValue, EvaluationValue>());
        o.getEvaluations().setOverallScore(new EvaluationValue(s.getOverallScore().getUuid(), s.getOverallScore().getScore()));
        for(SubmissionListItem.Evaluation eval : s.getPartialScore()){
            o.getEvaluations().getPartialScores().add(new EvaluationValue(eval.getUuid(), eval.getScore()));
        }
        
        // nastavime contributorov
        if(s.getContributors().size() > 0){
            for(UserEvent ue : s.getContributors()){
                o.getContributors().add(new ClientContributor(ue.getUuid(), ue.getFullName()));                
            }        
        }

        // vytvorime finalny objekt
        ClientListContributionItem item = new ClientListContributionItem();
        item.setOverview(o);
        item.setUuid(o.getUuid());
        item.setDecidedDate(s.getDecideDate());
        return item;
    }
}
