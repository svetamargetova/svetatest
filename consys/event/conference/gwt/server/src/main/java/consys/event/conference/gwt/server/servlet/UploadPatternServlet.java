package consys.event.conference.gwt.server.servlet;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.DataFile;
import consys.event.conference.core.bo.SubmissionArtefactPattern;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UploadPatternServlet extends AbstractUploadHelpFileServlet {

    private static final long serialVersionUID = 7885738397801648485L;

    @Override
    protected String saveDataFile(DataFile file, String title, String description) {
        try {
            SubmissionArtefactPattern artefactPattern = new SubmissionArtefactPattern();
            artefactPattern.setDesciption(description);
            artefactPattern.setTitle(title);
            getSettingsService().createSubmissionPattern(artefactPattern, file);
            return artefactPattern.getUuid();
        } catch (ServiceExecutionFailed ex) {
            throw new IllegalArgumentException(ex);
        } catch (RequiredPropertyNullException ex) {
            throw new IllegalArgumentException(ex);
        }
    }
}
