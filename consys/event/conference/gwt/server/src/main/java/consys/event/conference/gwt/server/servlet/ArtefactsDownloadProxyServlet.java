/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.gwt.server.servlet;


import com.google.common.base.Joiner;
import consys.event.common.gwt.server.servlet.AbstractEventServlet;
import consys.event.conference.api.servlet.ConferenceDownloadSubmissionArtefactServlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author palo
 */
public class ArtefactsDownloadProxyServlet extends AbstractEventServlet implements ConferenceDownloadSubmissionArtefactServlet {
       
    private String url;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
        String awsS3Bucket = config.getInitParameter("aws_s3_bucket");
        if(StringUtils.isBlank(awsS3Bucket)){
            throw new ServletException("Missing AWS S3 Bucket for proxying requests");
        }
        
        url = String.format("http://%s.s3-website-eu-west-1.amazonaws.com/",awsS3Bucket);
        logger.info("Proxy initialized for: AWS S3 EUROPE - {}. Proxy: {}",awsS3Bucket,url);        
    }
    
   
    
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
            final String artefactUuid = request.getParameter(ARTEFACT_TAG);
            if (StringUtils.isBlank(artefactUuid)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }

            URLConnection connection = new URL(url+artefactUuid).openConnection();
            connection.setDoInput(true);    
            connection.setUseCaches(false);
            
            

            InputStream input = connection.getInputStream();
            OutputStream output = response.getOutputStream();
            
            
            // Nastavime hlavicky
            Map<String,List<String>> headers = connection.getHeaderFields();
            for (Map.Entry<String, List<String>> entry : headers.entrySet()) {                
                List<String> list = entry.getValue();
                if(list.isEmpty()){
                    response.setHeader(entry.getKey(), "");
                }else if(list.size() == 1){
                    response.setHeader(entry.getKey(), list.get(0));
                }else{
                    logger.debug("More response headers: {}",Joiner.on(" -#- ").join(list));
                    response.setHeader(entry.getKey(), Joiner.on(",").join(list));
                }
            }            
             
            
            byte[] buffer = new byte[1024]; // Uses only 1KB of memory!
            for (int length = 0; (length = input.read(buffer)) > 0;) {
                output.write(buffer, 0, length);
                output.flush();
            }

            output.close();
            connection.getInputStream(); // Important! It's lazily executed.

        } catch (IOException ex) {
            logger.error("Error occured",ex);
        }





    }
}
