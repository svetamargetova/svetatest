package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.exception.TopicNotUniqueException;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.exception.TopicUniqueException;
import consys.event.conference.gwt.client.action.setting.UpdateTopicAction;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateTopicActionHandler implements EventActionHandler<UpdateTopicAction, VoidResult> {

    @Autowired
    private TopicService topicService;

    @Override
    public Class<UpdateTopicAction> getActionType() {
        return UpdateTopicAction.class;
    }

    @Override
    public VoidResult execute(UpdateTopicAction action) throws ActionException {
        ClientTopic ct = action.getTopic();
        try {
            SubmissionTopic topic = topicService.loadTopic(ct.getId());
            if (!topic.getName().equals(ct.getName())) {
                topic.setName(ct.getName());
                topicService.updateTopic(topic);
            }
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (TopicNotUniqueException ex) {
            throw new TopicUniqueException();
        }
    }   
}
