package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.exception.SubmissionTypeUniqueException;
import consys.event.conference.gwt.client.action.setting.UpdateContributionTypeAction;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionTypeThumb;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Aktualizace typu prispevku (thumb obsahuje data bez seznamu jinych komponent)
 * @author pepa
 */
public class UpdateContributionTypeActionHandler implements EventActionHandler<UpdateContributionTypeAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;
    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<UpdateContributionTypeAction> getActionType() {
        return UpdateContributionTypeAction.class;
    }

    @Override
    public VoidResult execute(UpdateContributionTypeAction action) throws ActionException {
        try {
            ClientContributionTypeThumb type = action.getThumb();
            SubmissionType st = submissionService.loadSubmissionTypeByUuid(type.getUuid());
            st.setDescription(type.getDescription());
            st.setName(type.getName());
            st.setInternType(type.isInternal());
            // order ?
            settingsService.updateSubmissionType(st);
            return VoidResult.RESULT();
        } catch (RequiredPropertyNullException ex) {
            // ak je name == null
            throw new BadInputException();
        } catch (SubmissionTypeNotUniqueException ex) {
            throw new SubmissionTypeUniqueException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }  
}
