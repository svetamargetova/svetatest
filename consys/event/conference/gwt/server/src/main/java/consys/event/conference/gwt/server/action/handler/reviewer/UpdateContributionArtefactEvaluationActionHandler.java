package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.gwt.client.action.reviewer.UpdateContributionArtefactEvaluationAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Zaslani hodnoceni od recenzenta
 * @author pepa
 */
public class UpdateContributionArtefactEvaluationActionHandler implements EventActionHandler<UpdateContributionArtefactEvaluationAction, VoidResult> {

    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<UpdateContributionArtefactEvaluationAction> getActionType() {
        return UpdateContributionArtefactEvaluationAction.class;
    }

    @Override
    public VoidResult execute(UpdateContributionArtefactEvaluationAction action) throws ActionException {
        try {
            reviewService.updateReviewEvaluation(action.getReviewUuid(), action.getContributionUuid(), action.getArtefactUuid(), action.getEvaluations(), action.getPublicComment(), action.getPrivateComment());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        }
    }
}
