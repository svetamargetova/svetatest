package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.bo.ReviewCriteriaItem;
import consys.event.conference.core.exception.DeleteCriteriaException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCriteriaException;
import consys.event.conference.gwt.client.action.setting.UpdateCriteriaAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Aktualizace kriteria
 * @author pepa
 */
public class UpdateCriteriaActionHandler implements EventActionHandler<UpdateCriteriaAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<UpdateCriteriaAction> getActionType() {
        return UpdateCriteriaAction.class;
    }

    @Override
    public VoidResult execute(UpdateCriteriaAction action) throws ActionException {
        try {
            ClientCriteria criteria = action.getCriteria();
            ReviewCriteria c = settingsService.loadReviewCriteria(criteria.getId());

            c.setDescription(c.getDescription());
            c.setName(criteria.getName());
            c.setAutoMinAccept(criteria.getMinimumPoints());

            if (c.getItems().isEmpty()) {
                // Este nema nastavene ziadne tak len pridame
                for (int i = 0; i < criteria.getPoints().size(); i++) {
                    ReviewCriteriaItem item = new ReviewCriteriaItem();
                    item.setName(criteria.getPoints().get(i).getName());
                    item.setValue(i + 1);
                    c.getItems().add(item);
                }
            } else {
                // Uz ma nejake nastavene --> Merge
                int clientItems = criteria.getPoints().size();
                int serverItems = c.getItems().size();

                // AK je na servery viacej items tak odstranime
                if(serverItems > clientItems){
                    int idx = clientItems;
                    for( ; idx < serverItems ; idx++){
                        c.getItems().remove(idx);
                    }
                }
                // Mergujeme a vytvarame
                for (int i = 0; i < clientItems; i++) {
                    if (i < serverItems) {
                        // Mergujeme
                        ReviewCriteriaItem item = c.getItems().get(i);
                        item.setName(criteria.getPoints().get(i).getName());
                    } else {
                        // Pridavame nove
                        ReviewCriteriaItem item = new ReviewCriteriaItem();
                        item.setName(criteria.getPoints().get(i).getName());
                        item.setValue(i + 1);
                        c.getItems().add(item);
                    }
                }
            }
            settingsService.updateReviewCriteria(c);

            return VoidResult.RESULT();
        } catch (RequiredPropertyNullException ex) {
            // Vyhodene ak criteria name empty
            throw new BadInputException();
        } catch (DeleteCriteriaException ex) {
            // Na CriteriaItem/Point je naviazane uz nejake hodnotenie
            throw new DeleteReviewCriteriaException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
