package consys.event.conference.gwt.server.action.utils;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ContributionReviewerArtefactReview;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.bo.ClientArtefactType;
import consys.event.conference.gwt.client.bo.contribution.ClientContributionHeadData;
import consys.event.conference.gwt.client.bo.evaluation.Evaluation;
import consys.event.conference.gwt.client.bo.evaluation.EvaluationValue;
import consys.event.conference.gwt.client.bo.evaluation.ReviewScoreAndCommentsData;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerFormData;
import consys.event.conference.gwt.client.bo.reviewer.ClientContributionReviewerHead;
import consys.event.conference.gwt.client.bo.reviewer.ClientReviewArtefactReviewer;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;
import consys.event.conference.gwt.client.bo.setting.ClientCriteriaItem;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterion;
import consys.event.conference.gwt.client.bo.shared.ClientReviewCriterionItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewUtils {

    public static ArrayList<ClientReviewCriterion<Long>> toReviewCriterion(List<ReviewCriteria> cc) {
        ArrayList<ClientReviewCriterion<Long>> out = Lists.newArrayList();
        for (ReviewCriteria criteria : cc) {
            ClientReviewCriterion<Long> crc = new ClientReviewCriterion<Long>();
            crc.setId(criteria.getId());
            crc.setDescription(criteria.getDescription());
            crc.setName(criteria.getName());
            crc.setMinAverageAccept(criteria.getAutoAvergeAccept());
            crc.setMinPointsAccept(criteria.getAutoMinAccept());
            crc.setMaxPoints(criteria.getMaxPoints());
            out.add(crc);
            for (ReviewCriteriaItem item : criteria.getItems()) {
                ClientReviewCriterionItem<Long> crci = new ClientReviewCriterionItem<Long>();
                crci.setName(item.getName());
                crci.setValue(item.getId());
                crc.getItems().add(crci);
            }
        }
        return out;
    }

    public static ClientCriteria fromReviewCriteria(ReviewCriteria c) {
        ClientCriteria criteria = new ClientCriteria();
        criteria.setId(c.getId());
        criteria.setDescription(c.getDescription());
        criteria.setName(c.getName());
        criteria.setMinimumPoints(c.getAutoMinAccept());
        for (ReviewCriteriaItem item : c.getItems()) {
            criteria.getPoints().add(new ClientCriteriaItem(item.getId(), item.getName()));
        }
        return criteria;
    }

    public static ArrayList<ClientCriteria> fromReviewCriterias(List<ReviewCriteria> cc) {
        ArrayList<ClientCriteria> out = Lists.newArrayList();
        for (ReviewCriteria criteria : cc) {
            ClientCriteria c = fromReviewCriteria(criteria);
            out.add(c);
        }
        return out;
    }

    public static ArrayList<ReviewScoreAndCommentsData> createOverallReviews(Submission submission, List<ContributionReviewerReview> reviews, ClientContributionHeadData head, boolean appendPrivateComments, SubmissionService submissionService) throws RequiredPropertyNullException {
        // automaticky prijimane typy
        Map<Long, Boolean> aa = Maps.newHashMap();

        for (SubmissionArtefact artefact : submission.getArtefacts()) {
            aa.put(artefact.getTypeArtefact().getId(), artefact.getTypeArtefact().isAutomaticallyAccepted());
        }

        ArrayList<ReviewScoreAndCommentsData> data = Lists.newArrayList();

        // nacitame datove typy artefaktov ake existuju pre dany typ 
        List<Long> artefactTypes = submissionService.listSubmissionTypeArtefactsIds(submission.getType().getId());

        for (ContributionReviewerReview review : reviews) {
            EvaluationValue reviewerValue = new EvaluationValue(review.getReviewUuid(), review.getReviewerReviewScore());

            ReviewScoreAndCommentsData reviewData = new ReviewScoreAndCommentsData();
            if (appendPrivateComments) {
                reviewData.setPrivateComment(review.getReviewerPrivateComment());
            }
            reviewData.setPublicComment(review.getReviewerPublicComment());
            reviewData.setReviewUuid(review.getReviewUuid());
            reviewData.setReviewerOrganization(review.getReviewerOrganization());
            reviewData.setReviewerPosition(review.getReviewerPosition());
            reviewData.setReviewerImageUuid(review.getReviewerImageUuidPrefix());
            reviewData.setReviewerName(review.getReviewerName());
            reviewData.setEvaluation(new Evaluation<EvaluationValue, EvaluationValue>());
            reviewData.getEvaluation().setOverallScore(reviewerValue);
            // nastavime aj celkove skore do hlavicky
            head.getEvaluations().getPartialScores().add(reviewerValue);

            for (Long artefactType : artefactTypes) {
                EvaluationValue value = null;
                for (ContributionReviewerArtefactReview crar : review.getArtefactReviews()) {
                    if (crar.getArtefactTypeId().equals(artefactType)) {
                        int score = isAutoaccepted(aa.get(artefactType)) ? EvaluationValue.AUTOACCEPTED : crar.getScore();
                        value = new EvaluationValue(crar.getArtefactUuid(), score);
                        break;
                    }
                }
                if (value == null) {
                    value = new EvaluationValue(isAutoaccepted(aa.get(artefactType))
                            ? EvaluationValue.AUTOACCEPTED : EvaluationValue.NOT_ENTERED);
                }
                reviewData.getEvaluation().getPartialScores().add(value);
            }
            data.add(reviewData);
        }
        return data;
    }

    private static boolean isAutoaccepted(Boolean autoaccepted) {
        if (autoaccepted == null) {
            return false;
        } else {
            return autoaccepted.booleanValue();
        }
    }

    public static ClientContributionReviewerFormData createAndLoadReviewFormData(String submissionUuid, String callerUuid, Review review, SubmissionService submissionService, ReviewService reviewService) throws NoRecordException, RequiredPropertyNullException {
        ClientContributionReviewerFormData data = new ClientContributionReviewerFormData();
        // nacitame submission s submission artefaktami ale bez artefat typov
        Submission submission = submissionService.loadSubmissionWithArtefacts(submissionUuid);
        data.setHead(new ClientContributionReviewerHead());
        SubmissionUtils.copySubmission(submission, data.getHead());
        // TODO: ak je anonymous review  = false            
        SubmissionUtils.copyContributors(submissionService.listSubmissionContributors(submissionUuid), data.getHead());

        // Nacitame si recenziu so vsetkymi recenziami artefaktov - ale len prva urovne            
        data.getHead().setReviewUuid(review.getUuid());

        // nasetujeme globalne poznamky
        data.setPrivateComment(review.getPrivateComment());
        data.setPublicComment(review.getPublicComment());

        // nasetujeme ci je toto vlastnik recenzie
        data.setIsAuthor(callerUuid.equalsIgnoreCase(review.getReviewer().getUuid()));
        data.getHead().setReviewerName(review.getReviewer().getFullName());
        data.getHead().setReviewerUuid(review.getReviewer().getUuid());

        // nasetujeme celkove skore skore
        data.getHead().getEvaluations().setOverallScore(new EvaluationValue(review.getScore()));

        // nastavime hodnotenia jednoltivych artefaktov
        for (SubmissionArtefact sa : submission.getArtefacts()) {
            for (ReviewArtefact ra : review.getArtefactEvaluations()) {
                if (ra.getArtefact().getId().equals(sa.getId())) {
                    data.getHead().getEvaluations().getPartialScores().add(new EvaluationValue(sa.getUuid(), ra.getScore()));
                }
            }
        }

        CycleBuilder builder = new CycleBuilder(review, submission, reviewService, submissionService);
        data.getReviewCycles().addAll(builder.createCycles(submission.getType().getUuid(), false));

        return data;
    }

    private static class CycleBuilder extends ClientReviewCycleBuilder {

        private final Submission submission;
        private final Review review;

        public CycleBuilder(Review review, Submission submission, ReviewService reviewService, SubmissionService submissionService) {
            super(reviewService, submissionService);
            this.submission = submission;
            this.review = review;
        }

        @Override
        public boolean useCycleSubmitDate() {
            // false - aby sa pouzil review
            return false;
        }

        @Override
        public ClientArtefactType createClientArtefact(SubmissionTypeArtefact sta) {
            ClientReviewArtefactReviewer artefactReview = new ClientReviewArtefactReviewer();

            // Pridame vsetky artefakty
            Long existingArtefactId = null;

            // nasetujeme UUID uz existujuceho artefaktu
            for (SubmissionArtefact sa : submission.getArtefacts()) {
                if (sa.getTypeArtefact().getId().equals(sta.getId())) {
                    artefactReview.setArtefactUuid(sa.getUuid());
                    artefactReview.setFileName(sa.getFileName());
                    existingArtefactId = sa.getId();
                    break;
                }
            }
            // zistime ci uz bol recenzovany ale len ak vobec existuje
            if (existingArtefactId != null) {
                for (ReviewArtefact ra : review.getArtefactEvaluations()) {
                    if (ra.getArtefact().getId().equals(existingArtefactId)) {
                        artefactReview.setTotalScore(new EvaluationValue(ra.getScore()));
                        artefactReview.setPrivateComment(ra.getPrivateComment());
                        artefactReview.setPublicComment(ra.getPublicComment());
                    }
                }
                return artefactReview;
            } else {
                return new ClientArtefactType();
            }

        }
    }
}
