package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.gwt.client.action.setting.LoadCriteriaAction;
import consys.event.conference.gwt.client.bo.setting.ClientCriteria;
import consys.event.conference.gwt.server.action.utils.ReviewUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni jednoho kriteria
 * @author pepa
 */
public class LoadCriteriaActionHandler implements EventActionHandler<LoadCriteriaAction, ClientCriteria> {

    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<LoadCriteriaAction> getActionType() {
        return LoadCriteriaAction.class;
    }

    @Override
    public ClientCriteria execute(LoadCriteriaAction action) throws ActionException {
        try {
            ReviewCriteria c = reviewService.loadCriteriaWithItems(action.getCriteriaId());            
            return ReviewUtils.fromReviewCriteria(c);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }    
}
