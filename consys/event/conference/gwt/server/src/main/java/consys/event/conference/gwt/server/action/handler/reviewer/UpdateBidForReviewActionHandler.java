package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewerBidInterestEnum;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.gwt.client.action.reviewer.UpdateBidForReviewAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateBidForReviewActionHandler implements EventActionHandler<UpdateBidForReviewAction, VoidResult> {

    @Autowired
    private ReviewService reviewService;

    @Override
    public Class<UpdateBidForReviewAction> getActionType() {
        return UpdateBidForReviewAction.class;
    }

    @Override
    public VoidResult execute(UpdateBidForReviewAction action) throws ActionException {
        try {
            ReviewerBidInterestEnum interest = ReviewerBidInterestEnum.NOT_CARE;
            switch (action.getCh()) {
                case CONFLICT:
                    interest = ReviewerBidInterestEnum.CONFLICT;
                    break;
                case LIKE:
                    interest = ReviewerBidInterestEnum.LIKE;
                    break;
                case NOT_CARE:
                    interest = ReviewerBidInterestEnum.NOT_CARE;
                    break;
                case NOT_WANT:
                    interest = ReviewerBidInterestEnum.DONT_LIKE;
                    break;
            }
            reviewService.updateReviewerBid(action.getUserEventUuid(), action.getUuid(), interest);
            return VoidResult.RESULT();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
