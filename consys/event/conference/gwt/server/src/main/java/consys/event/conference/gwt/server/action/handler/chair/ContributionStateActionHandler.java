package consys.event.conference.gwt.server.action.handler.chair;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionStateEnum;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.chair.ContributionStateAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * Rozhodnuti jestli je prispevek prijat nebo odmitnut
 *
 * @author pepa
 */
public class ContributionStateActionHandler implements EventActionHandler<ContributionStateAction, VoidResult> {

    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<ContributionStateAction> getActionType() {
        return ContributionStateAction.class;
    }

    @Override
    public VoidResult execute(ContributionStateAction action) throws ActionException {
        try {

            SubmissionStateEnum toState = SubmissionStateEnum.PENDING;

            switch (action.getToStateEnum()) {
                case ACCEPTED:
                    toState = SubmissionStateEnum.ACCEPTED;
                    break;
                case DECLINED:
                    toState = SubmissionStateEnum.DECLINED;
                    break;
            }

            submissionService.updateSubmissonState(action.getUuid(), toState);
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
