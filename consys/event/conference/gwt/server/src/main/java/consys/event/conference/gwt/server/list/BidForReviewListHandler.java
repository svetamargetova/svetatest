package consys.event.conference.gwt.server.list;

import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.gwt.server.GwtListHandler;
import consys.event.conference.api.list.BidForReviewList;
import consys.event.conference.core.bo.thumb.ReviewerBidListItem;
import consys.event.conference.gwt.client.bo.list.ClientListBidForReviewItem;
import consys.event.conference.gwt.client.bo.reviewer.ReviewerInterestEnum;
import consys.event.conference.gwt.server.action.utils.TopicUtils;
import java.util.ArrayList;
import java.util.List;

/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class BidForReviewListHandler implements GwtListHandler<ClientListBidForReviewItem, ReviewerBidListItem>,BidForReviewList{
    
    @Override
    public String getTag() {
       return TAG;
    }

    @Override
    public void execute(List<ReviewerBidListItem> in, ArrayList<ClientListBidForReviewItem> out) {
        for(ReviewerBidListItem b : in){
            ClientListBidForReviewItem c = new ClientListBidForReviewItem();
            c.setAuthors(b.getAuthors());
            switch(b.getBidInterestEnum()){
                case CONFLICT: c.setIntereset(ReviewerInterestEnum.CONFLICT); break;
                case DONT_LIKE: c.setIntereset(ReviewerInterestEnum.NOT_WANT); break;
                case LIKE: c.setIntereset(ReviewerInterestEnum.LIKE); break;
                case NOT_CARE: c.setIntereset(ReviewerInterestEnum.NOT_CARE); break;
            }
            c.setSubtitle(b.getSubtitle());
            c.setTitle(b.getTitle());  
            c.setTopics(TopicUtils.fromSubmissionTopicList(b.getTopics()));
            c.setUuid(b.getUuid());
            for(ListUuidItem item : b.getArtefacts()){
                c.getArtefacts().add(new CommonThumb(item.getUuid(), item.getName()));
            }
            out.add(c);
        }
    }




}
