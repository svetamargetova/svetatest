package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.reviewer.ListReviewerPreferredTopics;
import consys.event.conference.gwt.client.bo.contribution.ClientTopic;
import consys.event.conference.gwt.client.bo.setting.UserTopicsResult;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Nacteni topiku s vyznacenim, ktere si uzivatel jiz vybral.
 * Je to podobne ako {@link ListReviewerTopicIdsActionHandler} ale s tym ze tato
 * sluzba obsahuje aj samostatne topicy.
 *
 * @author pepa
 */
public class ListReviewerPreferredTopicsHandler implements EventActionHandler<ListReviewerPreferredTopics, UserTopicsResult> {

    @Autowired
    private TopicService topicService;

    @Override
    public Class<ListReviewerPreferredTopics> getActionType() {
        return ListReviewerPreferredTopics.class;
    }

    @Override
    public UserTopicsResult execute(ListReviewerPreferredTopics action) throws ActionException {
        List<SubmissionTopic> topics = topicService.listTopics();
        List<Long> reviewersTopics = topicService.listReviewerPreferredTopics(action.getUserEventUuid());
        UserTopicsResult result = new UserTopicsResult();
        result.setSelectedTopics(new ArrayList<Long>(reviewersTopics));
        for(SubmissionTopic topic : topics){
            ClientTopic ct = new ClientTopic();
            ct.setId(topic.getId());
            ct.setName(topic.getName());
            result.getTopics().add(ct);
        }
        return result;
    }
}
