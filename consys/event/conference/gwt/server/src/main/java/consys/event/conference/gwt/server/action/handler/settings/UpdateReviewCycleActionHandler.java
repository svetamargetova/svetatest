package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.exception.ArtefactInDifferentCycleException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.exception.AssignArtefactToCycleException;
import consys.event.conference.gwt.client.action.setting.UpdateReviewCycleAction;
import consys.event.conference.gwt.client.bo.setting.ClientSettingsReviewCycle;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Aktualizace kolecka hodnoceni
 * @author pepa
 */
public class UpdateReviewCycleActionHandler implements EventActionHandler<UpdateReviewCycleAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<UpdateReviewCycleAction> getActionType() {
        return UpdateReviewCycleAction.class;
    }

    @Override
    public VoidResult execute(UpdateReviewCycleAction action) throws ActionException {
        try {
            ClientSettingsReviewCycle cycle = action.getReviewCycle();
            ReviewCycle rc = settingsService.loadReviewCycleWithTypeById(cycle.getId());
            rc.setReviewFrom(cycle.getReviewFrom());
            rc.setReviewTo(cycle.getReviewTo()); 
            rc.setSubmitFrom(cycle.getSubmitFrom());
            rc.setSubmitTo(cycle.getSubmitTo());                                    
            settingsService.updateReviewCycle(rc,action.getReviewCycle().getAssignedArtefacts());
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (ArtefactInDifferentCycleException ex) {
            throw new AssignArtefactToCycleException();
        } catch (RequiredPropertyNullException ex) {
            // ak je submit from-to null alebo nieje before.
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
