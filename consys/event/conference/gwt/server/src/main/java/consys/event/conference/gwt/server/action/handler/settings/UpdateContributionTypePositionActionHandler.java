package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.UpdateContributionTypePositionAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateContributionTypePositionActionHandler implements EventActionHandler<UpdateContributionTypePositionAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;
    
    @Override
    public Class<UpdateContributionTypePositionAction> getActionType() {
        return UpdateContributionTypePositionAction.class;
    }

    @Override
    public VoidResult execute(UpdateContributionTypePositionAction action) throws ActionException {
        try {
            settingsService.updateSubmissionTypeOrder(action.getUuid(), action.getOldPosition(), action.getNewPosition());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
