package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.ListUploadedArtefactTemplateAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUploadedArtefactTemplatesActionHandler implements EventActionHandler<ListUploadedArtefactTemplateAction, ArrayListResult<CommonThumb>> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<ListUploadedArtefactTemplateAction> getActionType() {
        return ListUploadedArtefactTemplateAction.class;
    }

    @Override
    public ArrayListResult<CommonThumb> execute(ListUploadedArtefactTemplateAction action) throws ActionException {
        try {
            List<ListUuidItem> items = null;
            if (action.isExamples()) {
                items = settingsService.listPatternsThumbs();
            } else {
                items = settingsService.listTemplatesThumbs();
            }
            ArrayList<CommonThumb> out = Lists.newArrayList();
            for (ListUuidItem item : items) {
                out.add(new CommonThumb(item.getUuid(), item.getName()));
            }
            return new ArrayListResult<CommonThumb>(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }    
}
