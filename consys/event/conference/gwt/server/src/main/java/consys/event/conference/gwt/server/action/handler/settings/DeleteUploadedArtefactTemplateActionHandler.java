package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.DeleteUploadedArtefactTemplateAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DeleteUploadedArtefactTemplateActionHandler implements EventActionHandler<DeleteUploadedArtefactTemplateAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<DeleteUploadedArtefactTemplateAction> getActionType() {
        return DeleteUploadedArtefactTemplateAction.class;
    }

    @Override
    public VoidResult execute(DeleteUploadedArtefactTemplateAction action) throws ActionException {
        try {            
            if (action.isPattern()) {
                settingsService.deleteArtefactPattern(action.getUuid());
            } else {
                settingsService.deleteArtefactTemplate(action.getUuid());
            }            
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
