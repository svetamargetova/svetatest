package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.exception.DeleteCriteriaException;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.DeleteCriteriaAction;
import consys.event.conference.gwt.client.action.exception.DeleteReviewCriteriaException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Smazani kriteria
 * @author pepa
 */
public class DeleteCriteriaActionHandler implements EventActionHandler<DeleteCriteriaAction, VoidResult> {

    @Autowired
    private SubmissionSettingsService settingsService;

    @Override
    public Class<DeleteCriteriaAction> getActionType() {
        return DeleteCriteriaAction.class;
    }

    @Override
    public VoidResult execute(DeleteCriteriaAction action) throws ActionException {
        try {
            settingsService.deleteReviewCriteria(action.getCriteriaId());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (DeleteCriteriaException ex) {
            throw new DeleteReviewCriteriaException();
        }
    }    
}
