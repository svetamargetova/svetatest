package consys.event.conference.gwt.server.action.handler.reviewer;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.TopicService;
import consys.event.conference.gwt.client.action.reviewer.UpdateReviewerTopicsAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Aktualizace uzivatelovych topiku
 *
 * @author pepa
 */
public class UpdateReviewerTopicsActionHandler implements EventActionHandler<UpdateReviewerTopicsAction, VoidResult> {

    @Autowired
    private TopicService topicService;

    @Override
    public Class<UpdateReviewerTopicsAction> getActionType() {
        return UpdateReviewerTopicsAction.class;
    }

    @Override
    public VoidResult execute(UpdateReviewerTopicsAction action) throws ActionException {
        try {
            topicService.updateReviewerTopics(action.getUserEventUuid(), action.getUserTopics());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
