package consys.event.conference.gwt.server.action.handler.chair;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.chair.LoadContributionChairFormAction;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairFormData;
import consys.event.conference.gwt.client.bo.chair.ClientContributionChairHead;
import consys.event.conference.gwt.server.action.utils.ReviewUtils;
import consys.event.conference.gwt.server.action.utils.SubmissionUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class LoadContributionChairFormActionHandler implements EventActionHandler<LoadContributionChairFormAction, ClientContributionChairFormData> {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private SubmissionService submissionService;

    @Override
    public Class<LoadContributionChairFormAction> getActionType() {
        return LoadContributionChairFormAction.class;
    }

    @Override
    public ClientContributionChairFormData execute(LoadContributionChairFormAction action) throws ActionException {
        try {
            ClientContributionChairFormData data = new ClientContributionChairFormData();

            // nacitame prispevek s artefaktama
            Submission submission = submissionService.loadSubmissionWithArtefacts(action.getContributionUuid());

            // vytvorime hlavicku
            data.setHead(new ClientContributionChairHead());
            data.getHead().setChangedDate(submission.getDecidedDate());
            SubmissionUtils.copySubmission(submission, data.getHead());
            SubmissionUtils.copyContributors(submissionService.listSubmissionContributors(action.getContributionUuid()), data.getHead());

            List<ContributionReviewerReview> reviews = reviewService.listSubmissionReviews(submission.getId());

            data.setEvaluated(ReviewUtils.createOverallReviews(submission, reviews, data.getHead(), true, submissionService));

            return data;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
