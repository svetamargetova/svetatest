package consys.event.conference.gwt.server.servlet;

import consys.event.common.gwt.server.servlet.AbstractFileUploadServlet;
import consys.event.conference.api.servlet.ConferenceSubmissionArtefactUploadServlet;
import consys.event.conference.core.service.SubmissionService;
import java.io.IOException;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstraktny predok servletov k spracovaniu posielanych prispevkov.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractSubmitServlet extends AbstractFileUploadServlet implements ConferenceSubmissionArtefactUploadServlet {

    private static final long serialVersionUID = -5563830188745261096L;    
                
    @Autowired
    private SubmissionService submissionService;

    public SubmissionService getSubmissionService() {
        return submissionService;
    }              
    
    
    public String getSubmissionUuid(Map<String, String> formFields){
        return formFields.get(FIELD_SUBMISSION_UUID);        
    }
    
    public String getArtefactTypeUuid(Map<String, String> formFields){
        return formFields.get(FIELD_ARTEFACT_TYPE);        
    }
    
    public void sendSuccess(String artefactUuid, HttpServletResponse response) throws IOException{
        response.getOutputStream().print(String.format("<ok>%s</ok>", artefactUuid));
    }
    
    public void sendError(String msg, HttpServletResponse response) throws IOException{
        response.getOutputStream().print(String.format("<error>%s</error>", msg));
    }
    
}
