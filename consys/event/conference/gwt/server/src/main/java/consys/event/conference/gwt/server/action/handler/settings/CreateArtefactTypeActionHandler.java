package consys.event.conference.gwt.server.action.handler.settings;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.utils.collection.Lists;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.bo.SubmissionArtefactType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.service.SubmissionSettingsService;
import consys.event.conference.gwt.client.action.setting.CreateArtefactTypeAction;
import consys.event.conference.gwt.client.bo.setting.ClientArtefactTypeThumb;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Vytvoreni artefact type
 * @author pepa
 */
public class CreateArtefactTypeActionHandler implements EventActionHandler<CreateArtefactTypeAction, ArrayListResult<String>> {

    @Autowired
    private SubmissionSettingsService submissionService;

    @Override
    public Class<CreateArtefactTypeAction> getActionType() {
        return CreateArtefactTypeAction.class;
    }

    @Override
    public ArrayListResult<String> execute(CreateArtefactTypeAction action) throws ActionException {
        try {
            List<SubmissionTypeArtefact> artefacts = Lists.newArrayList();
            for (ClientArtefactTypeThumb type : action.getType()) {                
                SubmissionTypeArtefact sta = new SubmissionTypeArtefact();
                SubmissionArtefactType atype = new SubmissionArtefactType();
                atype.setName(type.getArtefactTypeName());
                atype.setDescription(type.getArtefactTypeDescription());
                sta.setArtefactType(atype);
                artefacts.add(sta);
            }

            submissionService.createSubmissionArtefactTypes(action.getSubmissionTypeUuid(), artefacts);

            ArrayList<String> out = Lists.newArrayList();
            for (SubmissionTypeArtefact artefact : artefacts) {
                out.add(artefact.getUuid());
            }
            return new ArrayListResult<String>(out);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
