package consys.event.conference.gwt.server.action.handler.contribution;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.gwt.client.action.contribution.DeleteContributionAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class DeleteContributionActionHandler implements EventActionHandler<DeleteContributionAction, VoidResult> {

    @Autowired
    private SubmissionService submissionService;
   

    @Override
    public Class<DeleteContributionAction> getActionType() {
        return DeleteContributionAction.class;
    }

    @Override
    public VoidResult execute(DeleteContributionAction action) throws ActionException {
        try {
            submissionService.deleteSubmisson(action.getSubmissionUuid());
            return VoidResult.RESULT();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }

    }
}
