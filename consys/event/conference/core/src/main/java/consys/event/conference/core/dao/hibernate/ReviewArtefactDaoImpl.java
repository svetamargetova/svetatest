package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.ReviewArtefact;
import consys.event.conference.core.bo.thumb.ArtefactReviewThumb;
import consys.event.conference.core.dao.ReviewArtefactDao;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.hibernate.LockMode;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewArtefactDaoImpl extends GenericDaoImpl<ReviewArtefact> implements ReviewArtefactDao {

    @Override
    public ReviewArtefact load(Long id) throws NoRecordException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ReviewArtefact loadForReview(String artefactUuid, String reviewUuid) throws NoRecordException {
        ReviewArtefact eval = (ReviewArtefact) session().
                createQuery("select r from ReviewArtefact r LEFT JOIN FETCH r.criteriaEvaluations where r.review.uuid=:RUUID and r.artefact.uuid=:AUUID").
                setString("RUUID", reviewUuid).
                setString("AUUID", artefactUuid).
                setLockMode("r", LockMode.PESSIMISTIC_WRITE).
                uniqueResult();
        if (eval == null) {
            throw new NoRecordException();
        }
        return eval;
    }

    @Override
    public ReviewArtefact loadByReview(String artefactUuid, String reviewUuid) throws NoRecordException {
        ReviewArtefact eval = (ReviewArtefact) session().
                createQuery("select r from ReviewArtefact r LEFT JOIN FETCH r.criteriaEvaluations as c where r.review.uuid=:RUUID and r.artefact.uuid=:AUUID").
                setString("RUUID", reviewUuid).
                setString("AUUID", artefactUuid).
                uniqueResult();
        if (eval == null) {
            throw new NoRecordException();
        }
        return eval;
    }

    @Override
    public ReviewArtefact load(String artefactUuid, String reviewerUuid) throws NoRecordException {
        ReviewArtefact eval = (ReviewArtefact) session().
                createQuery("select r from ReviewArtefact r LEFT JOIN FETCH r.criteriaEvaluations as c where r.review.reviewer.uuid=:RUUID and r.artefact.uuid=:AUUID").
                setString("RUUID", reviewerUuid).
                setString("AUUID", artefactUuid).
                uniqueResult();
        if (eval == null) {
            throw new NoRecordException();
        }
        return eval;
    }

    @Override
    public Map<Long, Map<Long, List<ArtefactReviewThumb>>> loadArtefactCriteriaEvaluations(Long submissionId) {
        Map<Long, Map<Long, List<ArtefactReviewThumb>>> reviews = Maps.newHashMap();
        List<Object[]> items = session().
                createSQLQuery("select r.uuid, ra.id_artefact, race.id_review_criteria, race.id_review_criteria_item from  conference.review r left join conference.review_artefact ra on ra.id_review=r.id left join conference.review_artefact_criteria_evaluation race on race.id_review_artefact=ra.id where r.id_submission=:ID_SUBMISSION and id_artefact is not null").
                setLong("ID_SUBMISSION", submissionId).
                list();
        for (Object[] item : items) {
            ArtefactReviewThumb review = new ArtefactReviewThumb((String) item[0], ((BigInteger) item[2]).longValue(), ((BigInteger) item[3]).longValue());
            // nacitame si recenzie artefaktu
            Map<Long, List<ArtefactReviewThumb>> artefactReview = reviews.get(((BigInteger) item[1]).longValue());
            if (artefactReview == null) {
                artefactReview = Maps.newHashMap();
                reviews.put(((BigInteger) item[1]).longValue(), artefactReview);
            }

            // Nacitame si recenzie kriteria
            List<ArtefactReviewThumb> criteriaReviews = artefactReview.get(review.getCriteriaId());
            if (criteriaReviews == null) {
                criteriaReviews = Lists.newArrayList();
                artefactReview.put(review.getCriteriaId(), criteriaReviews);
            }


            criteriaReviews.add(review);
        }
        return reviews;
    }

    @Override
    public void deleteAllArtefacts() {
        session().createQuery("delete from ReviewArtefact").executeUpdate();
    }
}
