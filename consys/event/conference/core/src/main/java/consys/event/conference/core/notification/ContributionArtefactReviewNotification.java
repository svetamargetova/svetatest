/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.notification;

import consys.common.core.notx.SystemMessage;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.bo.thumb.ArtefactReviewNotificationData;
import consys.event.conference.core.bo.thumb.ArtefactReviewNotificationData.CriteriaEvaluation;
import java.util.Map;

/**
 *
 * @author palo
 */
public class ContributionArtefactReviewNotification extends ContributionNotification {

    private final ArtefactReviewNotificationData data;

    public ContributionArtefactReviewNotification(Submission submission, ArtefactReviewNotificationData data) {
        super(submission, SystemMessage.CONTRIBUTION_REVIEWED);
        this.data = data;
    }

    @Override
    public void evaluatePlaceholders(Map<String, String> placeholders) {
        // zakladne informacie o prispevku
        super.evaluatePlaceholders(placeholders);


        // informacie o recenzii
        placeholders.put("artefact_title", data.getArtefactTitle());
        placeholders.put("reviewer", data.getReviewerName());
        placeholders.put("reviewer_comment", data.getReviewerArtefactPublicComment());
        placeholders.put("artefact_review_score", scoreOrDash(data.getArtefactReviewScore()));
        placeholders.put("artefact_score", scoreOrDash(data.getArtefactScore()));
        placeholders.put("review_score", scoreOrDash(data.getReviewScore()));

        StringBuilder evaluationsPlain = new StringBuilder();
        StringBuilder evaluationsHtml = new StringBuilder();

        for (CriteriaEvaluation ev : data.getEvaluations()) {
            evaluationsHtml.append("<tr><td align=\"left\" class=\"dc\">").
                    append(ev.getTitle()).
                    append("</td><td align=\"right\" class=\"dc\">").
                    append(ev.getPoints()).
                    append("</td></tr>");
            evaluationsPlain.append(ev.getTitle()).append("\t").append(ev.getPoints()).append("\n");
        }

        placeholders.put("evaluations_plain", evaluationsPlain.toString());
        placeholders.put("evaluations_html", evaluationsHtml.toString());
    }   
}
