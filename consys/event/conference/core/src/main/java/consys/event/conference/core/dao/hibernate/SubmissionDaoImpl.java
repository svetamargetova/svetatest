package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.Submission;
import consys.event.conference.core.dao.SubmissionDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionDaoImpl extends GenericDaoImpl<Submission> implements SubmissionDao {

    @Override
    public Submission load(Long id) throws NoRecordException {
        return load(id, Submission.class);
    }

    @Override
    public Submission loadByUuid(String uuid) throws NoRecordException {
        Submission s = (Submission) session().createQuery("from Submission s where s.uuid=:UUID").setString("UUID", uuid).uniqueResult();
        if (s == null) {
            throw new NoRecordException();
        }
        return s;
    }

    @Override
    public List<UserEvent> listSubmissionContributors(String submissionUuid) throws NoRecordException {
        List<UserEvent> contributors = session().createQuery("select c from Submission s join s.contributors as c where s.uuid=:UUID").
                setString("UUID", submissionUuid).
                list();
        if (contributors.isEmpty()) {
            throw new NoRecordException();
        }
        return contributors;
    }

    @Override
    public Submission loadSubmissionWithTopicsAndContributors(String uuid) throws NoRecordException {
        Submission s = (Submission) session().createQuery("select s from Submission s LEFT JOIN FETCH s.topics LEFT JOIN FETCH s.contributors where s.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (s == null) {
            throw new NoRecordException();
        }
        return s;
    }

    @Override
    public Submission loadSubmissionWithArtefacts(String uuid) throws NoRecordException {
        Submission s = (Submission) session().createQuery("select s from Submission s JOIN FETCH s.type LEFT JOIN FETCH s.artefacts as artefact LEFT JOIN FETCH artefact.typeArtefact where s.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (s == null) {
            throw new NoRecordException();
        }
        return s;
    }

    @Override
    public List<String> listCoauthors(Long submissionId) {
        return session().
                createQuery("select ca.uuid from Submission s join s.contributors as ca where s.id=:ID").
                setLong("ID", submissionId).
                list();
    }

    @Override
    public List<Submission> listSubmissionsByIds(List<Long> ids) {
        if (ids.isEmpty()) {
            return new ArrayList<Submission>();
        }
        final String query = "from Submission s where s.id in (:IDS) and s.state = 1";
        return session().createQuery(query).setParameterList("IDS", ids).list();
    }

    @Override
    public Submission loadSubmissonWithTypeAndTopicsByUuid(String uuid) throws NoRecordException {
        Submission s = (Submission) session().createQuery("select s from Submission s left join fetch s.type left join fetch s.topics where s.uuid=:UUID").setString("UUID", uuid).uniqueResult();
        if (s == null) {
            throw new NoRecordException();
        }
        return s;
    }

    @Override
    public Submission loadSubmissionForNotification(String uuid) throws NoRecordException {
        Submission s = (Submission) session().createQuery("select s from Submission s join fetch s.type join fetch s.contributors where s.uuid=:UUID").setString("UUID", uuid).uniqueResult();
        if (s == null) {
            throw new NoRecordException();
        }
        return s;
    }

    @Override
    public void deleteAllSubmissions() {
        session().createSQLQuery("delete from conference.submission_topic_rel").executeUpdate();
        session().createSQLQuery("delete from conference.submission_contributor").executeUpdate();
        session().createQuery("delete from Submission").executeUpdate();
    }
}
