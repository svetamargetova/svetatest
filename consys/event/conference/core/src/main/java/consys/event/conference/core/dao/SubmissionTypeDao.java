package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionTypeDao extends GenericDao<SubmissionType> {

    public Integer loadTypesCount();

    /**
     * Nacita SubmissionType s joinutymi typami artefaktovtj. Type -> TypeArtefact -> ArtefactType
     */
    public SubmissionType loadTypeWithArtefactsTypesByUuid(String uuid) throws NoRecordException;

    /**
     * Nacita SubmissionType s joinutymi  artefaktami tj. Type -> TypeArtefact
     */
    public SubmissionType loadTypeWithArtefactsAndCyclesByUuid(String uuid) throws NoRecordException;

    /**
     * Nacita SubmissionType 
     */
    public SubmissionType loadTypeByUuid(String uuid) throws NoRecordException;

    public List<ListUuidItem> listSettingsThumbTypes() throws NoRecordException;

    public List<SubmissionTypeThumb> listThumbTypesWithInternalWithDescription() throws NoRecordException;

    public List<SubmissionTypeThumb> listThumbTypesWithDescription() throws NoRecordException;
    
    public boolean checkSubmissionTypePosition(String uuid, int position);
    
    public void updateSubmissionTypeOrder(String typeUuid, int oldPosition, int newPosition);
}
