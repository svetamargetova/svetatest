package consys.event.conference.core.bo.thumb;

import com.google.common.collect.Lists;
import consys.event.common.core.bo.UserEvent;
import consys.event.conference.core.bo.SubmissionStateEnum;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionListItem {
 
    private Long id;
    private String uuid;
    private String title;
    private String subtitle;    
    private String type;    
    private String authors;
    private String agregatedTopics;
    private SubmissionStateEnum stateEnum;    
    private Date submitDate;
    private Date decideDate;    
    private List<UserEvent> contributors;
    private Evaluation overallScore;
    private List<Evaluation> partialScore;

    public SubmissionListItem() {
        partialScore = Lists.newArrayList();
        contributors = Lists.newArrayList();
    }

            
    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the authors
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    /**
     * @return the stateEnum
     */
    public SubmissionStateEnum getStateEnum() {
        return stateEnum;
    }

    /**
     * @param stateEnum the stateEnum to set
     */
    public void setStateEnum(SubmissionStateEnum stateEnum) {
        this.stateEnum = stateEnum;
    }
   
    /**
     * @return the agregatedTopics
     */
    public String getAgregatedTopics() {
        return agregatedTopics;
    }

    /**
     * @param agregatedTopics the agregatedTopics to set
     */
    public void setAgregatedTopics(String agregatedTopics) {
        this.agregatedTopics = agregatedTopics;
    }


    /**
     * @return the submitDate
     */
    public Date getSubmitDate() {
        return submitDate;
    }

    /**
     * @param submitDate the submitDate to set
     */
    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    /**
     * @return the decideDate
     */
    public Date getDecideDate() {
        return decideDate;
    }

    /**
     * @param decideDate the decideDate to set
     */
    public void setDecideDate(Date decideDate) {
        this.decideDate = decideDate;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    
    public List<Evaluation> getPartialScore() {
        return partialScore;
    }

    public List<UserEvent> getContributors() {
        return contributors;
    }

    /**
     * @return the overallScore
     */
    public Evaluation getOverallScore() {
        return overallScore;
    }

    /**
     * @param overallScore the overallScore to set
     */
    public void setOverallScore(Evaluation overallScore) {
        this.overallScore = overallScore;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    
    public class Evaluation {
        private Long id;
        private String uuid;
        private int score = -1;

        public Evaluation() {
        }
                
        public Evaluation(int score) {
            this.score = score;
        }
        

        public Evaluation(String uuid, int score) {
            this.uuid = uuid;
            this.score = score;
        }

        public Evaluation(Long id, String uuid, int score) {
            this.id = id;
            this.uuid = uuid;
            this.score = score;
        }

        public Long getId() {
            return id;
        }
                

        public int getScore() {
            return score;
        }

        public String getUuid() {
            return uuid;
        }

        /**
         * @param id the id to set
         */
        public void setId(Long id) {
            this.id = id;
        }

        /**
         * @param uuid the uuid to set
         */
        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        /**
         * @param score the score to set
         */
        public void setScore(int score) {
            this.score = score;
        }
    }
    
    
}
