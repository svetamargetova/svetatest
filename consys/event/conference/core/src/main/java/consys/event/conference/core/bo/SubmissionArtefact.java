package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import java.util.Date;

/**
 * Artefact ma vstavanu historiu v zozname suborov.
 * @author Palo 
 */
public class SubmissionArtefact implements ConsysObject {

    private static final long serialVersionUID = -5077464159044072552L;
    // data
    private Long id;
    private Date created;
    private String uuid;    
    private String fileName;
    private Submission submission;
    private SubmissionTypeArtefact typeArtefact;   
    private int score = -1;
    

    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the submission
     */
    public Submission getSubmission() {
        return submission;
    }

    /**
     * @param submission the submission to set
     */
    public void setSubmission(Submission submission) {
        this.submission = submission;
    }

    public void setTypeArtefact(SubmissionTypeArtefact typeArtefact) {
        this.typeArtefact = typeArtefact;
    }

    public SubmissionTypeArtefact getTypeArtefact() {
        return typeArtefact;
    }
     
    @Override
    public boolean equals(Object obj) {
        if (obj == null || uuid == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        SubmissionArtefact e = (SubmissionArtefact) obj;
        return uuid.equalsIgnoreCase(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("SubmissionArtefact[uuid=%s score=%s]", uuid,score);
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
