package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author Palo
 */
public class SubmissionTypeArtefact implements ConsysObject{
    private static final long serialVersionUID = -1181301270455007471L;

    private static final int AUTO_ACCEPT = 0;

    private Long id;
    private SubmissionType type;
    private SubmissionArtefactType artefactType;
    private int numOfReviewers;
    private boolean required;
    private int order;
    private String uuid;

    public boolean isAutomaticallyAccepted(){
        return numOfReviewers == AUTO_ACCEPT;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public SubmissionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(SubmissionType type) {
        this.type = type;
    }

    /**
     * @return the artefactType
     */
    public SubmissionArtefactType getArtefactType() {
        return artefactType;
    }

    /**
     * @param artefactType the artefactType to set
     */
    public void setArtefactType(SubmissionArtefactType artefactType) {
        this.artefactType = artefactType;
    }

    /**
     * @return the numOfReviewers
     */
    public int getNumOfReviewers() {
        return numOfReviewers;
    }

    /**
     * @param numOfReviewers the numOfReviewers to set
     */
    public void setNumOfReviewers(int numOfReviewers) {
        this.numOfReviewers = numOfReviewers;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

     @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        SubmissionTypeArtefact e = (SubmissionTypeArtefact) obj;
        return uuid.equals(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

   

    @Override
    public String toString() {
        return String.format("SubmissionTypeArtefact[requred=%s]",required);
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the order
     */
    public int getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }


}
