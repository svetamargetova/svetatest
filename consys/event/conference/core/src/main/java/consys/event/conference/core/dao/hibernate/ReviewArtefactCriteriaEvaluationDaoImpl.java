package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.ReviewArtefactCriteriaEvaluation;
import consys.event.conference.core.dao.ReviewArtefactCriteriaEvaluationDao;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewArtefactCriteriaEvaluationDaoImpl extends GenericDaoImpl<ReviewArtefactCriteriaEvaluation> implements ReviewArtefactCriteriaEvaluationDao {

    @Override
    public ReviewArtefactCriteriaEvaluation load(Long id) throws NoRecordException {
        throw new IllegalArgumentException("Nema ID");
    }

    @Override
    public List<String> listUuidsForReviewedArtefacts(String reviewerUuid, String submissionUuid) {
        List<String> out = session().
                createQuery("select distinct evaluation.artefact.uuid from Review review join review.evaluations as evaluation  where review.reviewer.uuid=:RUUID and review.submission.uuid=:SUUID").
                setString("SUUID", submissionUuid).
                setString("RUUID", reviewerUuid).
                list();
        return out;
    }

    @Override
    public ReviewArtefactCriteriaEvaluation loadEvaluationFor(String reviewerUuid, String submissionArtefactUuid) throws NoRecordException {
        ReviewArtefactCriteriaEvaluation eval = (ReviewArtefactCriteriaEvaluation) session().
                createQuery("select evaluation from Review review join review.evaluations as evaluation  where review.reviewer.uuid=:RUUID and evaluation.artefact.uuid=:AUUID").
                setString("AUUID", submissionArtefactUuid).
                setString("RUUID", reviewerUuid).
                uniqueResult();
        if (eval == null) {
            throw new NoRecordException();
        }
        return eval;
    }

    @Override
    public List<ReviewArtefactCriteriaEvaluation> listEvaluationsFor(String artefactUuid, String reviewerUuid) throws NoRecordException {
        List<ReviewArtefactCriteriaEvaluation> eval = session().
                createQuery("select eval from ReviewArtefactCriteriaEvaluation eval JOIN FETCH eval.criteriaItem where eval.review.reviewer.uuid=:RUUID and eval.artefact.uuid=:AUUID").
                setString("AUUID", artefactUuid).
                setString("RUUID", reviewerUuid).
                list();
        if (eval == null || eval.isEmpty()) {
            throw new NoRecordException();
        }
        return eval;
    }

    @Override
    public void deleteAll() {
        session().createQuery("delete from ReviewArtefactCriteriaEvaluation").executeUpdate();
    }
}
