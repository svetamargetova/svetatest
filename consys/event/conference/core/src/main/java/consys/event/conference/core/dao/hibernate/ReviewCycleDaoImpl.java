package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.date.DateProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.bo.thumb.ReviewCycleThumb;
import consys.event.conference.core.dao.ReviewCycleDao;
import java.util.Date;
import java.util.List;
import java.util.Map;

/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewCycleDaoImpl extends GenericDaoImpl<ReviewCycle> implements ReviewCycleDao {

    @Override
    public ReviewCycle load(Long id) throws NoRecordException {
        return load(id, ReviewCycle.class);
    }

    @Override
    public List<ReviewCycleThumb> listCycleThumbForType(String typeUuid) {
        List<Object[]> os = session().
                createQuery("select c.id,c.submitFrom,c.submitTo from ReviewCycle c where c.submissionType.uuid = :TYPE_UUID order by c.submitFrom").
                setString("TYPE_UUID", typeUuid).
                list();

        List<ReviewCycleThumb> thumbs = Lists.newArrayList();
        for (Object[] item : os) {
            thumbs.add(new ReviewCycleThumb((Long) item[0], (Date) item[1], (Date) item[2]));
        }
        return thumbs;
    }

    @Override
    public void deleteAllCyclesForType(String typeUuid) {
        List<ReviewCycle> cycles = session().
                createQuery("from ReviewCycle c where c.submissionType.uuid = :TYPE_UUID").
                setString("TYPE_UUID", typeUuid).
                list();
        for (ReviewCycle reviewCycle : cycles) {
            if (log.isDebugEnabled()) {
                log.debug("Delete " + reviewCycle);
            }
            delete(reviewCycle);
        }
    }

    @Override
    public Map<String, Long> listReviewCycleIdsFor(String submissionType, List<String> submissionTypeArtefacs) {
        List<Object[]> items = session().
                createQuery("select artefact.uuid, c.id  from ReviewCycle c join c.artefacts as artefact where c.submissionType.uuid = :TYPE and artefact.uuid IN (:UUIDS)").
                setParameterList("UUIDS", submissionTypeArtefacs).
                setString("TYPE", submissionType).
                list();
        Map<String, Long> out = Maps.newHashMap();
        for (Object[] item : items) {
            out.put((String) item[0], (Long) item[1]);
        }
        return out;
    }

    @Override
    public void deleteCycle(Long id) throws NoRecordException {
        ReviewCycle r = load(id);
        if (log.isDebugEnabled()) {
            log.debug("Delete " + r);
        }
        delete(r);
    }

    @Override
    public void deleteArtefactFromCycle(SubmissionTypeArtefact artefact) {
        List<ReviewCycle> cycles = session().
                createQuery("select c from ReviewCycle c join c.artefacts as artefact where artefact.id = :ID").
                setLong("ID", artefact.getId()).
                list();
        for (ReviewCycle reviewCycle : cycles) {
            if (log.isDebugEnabled()) {
                log.debug("Removing artefact " + artefact + " and updating cycle " + reviewCycle);
            }
            reviewCycle.getArtefacts().remove(artefact);
            update(reviewCycle);
        }
    }
    
    @Override
    public void deleteAllCyclesAndCyclesArtefacts() {
        session().createSQLQuery("delete from conference.review_cycle_artefact").executeUpdate();
        session().createQuery("delete from ReviewCycle").executeUpdate();
    }
    
    @Override
    public ReviewCycle loadReviewCycleWithType(Long id) throws NoRecordException {
        ReviewCycle rc = (ReviewCycle) session().
                createQuery("select c from ReviewCycle c JOIN FETCH c.submissionType where c.id = :ID").
                setLong("ID", id).uniqueResult();
        if (rc == null) {
            throw new NoRecordException("No Review Cycle with id " + id + " exists!");
        }
        return rc;
    }

    /**
     * Nacita cykly pre novy submission. 
     */
    @Override
    public List<ReviewCycle> listCyclesWithArtefactTypesForSubmissionType(String typeUuid) throws NoRecordException {
        List<ReviewCycle> cycles = session().
                createQuery("select distinct c from ReviewCycle c LEFT JOIN FETCH c.artefacts as artefact LEFT JOIN FETCH artefact.artefactType where c.submissionType.uuid = :TYPE_UUID order by c.submitFrom").
                setString("TYPE_UUID", typeUuid).
                list();
        if (cycles == null || cycles.isEmpty()) {
            throw new NoRecordException("SubmissionType with UUID " + typeUuid + " has no cycles. Every artefact is submittable.");
        }
        return cycles;
    }     

    /**
     * Nacita cykly pre novy submission.
     */
    @Override
    public List<ReviewCycle> listOnlyActiveCyclesForSubmissionForType(String typeUuid) throws NoRecordException {
        List<ReviewCycle> cycles = session().
                createQuery("select distinct c from ReviewCycle c LEFT JOIN FETCH c.artefacts as artefact where c.submissionType.uuid = :TYPE_UUID and c.submitFrom <= :TODAY and c.submitTo >= :TODAY order by c.submitFrom ").
                setString("TYPE_UUID", typeUuid).
                setTimestamp("TODAY", DateProvider.getCurrentDate()).
                list();
        if (cycles == null || cycles.isEmpty()) {
            throw new NoRecordException("SubmissionType with UUID " + typeUuid + " has no active cycles or whole type has no cycles.");
        }
        return cycles;
    }

    @Override
    public ReviewCycle loadReviewCycleForTypeArtefact(String submissionTypeArtefactUuid) throws NoRecordException {
        List<ReviewCycle> cycles = session().
                createQuery("select c from ReviewCycle c join c.artefacts as artefact where artefact.uuid=:AUUID").
                setString("AUUID", submissionTypeArtefactUuid).
                list();
        if (cycles == null || cycles.isEmpty()) {
            throw new NoRecordException();
        }
        return cycles.iterator().next();
    }

    @Override
    public ReviewCycle loadReviewCycleWithArtefacts(Long id) throws NoRecordException {
        List<ReviewCycle> cycles = session().
                createQuery("select c from ReviewCycle c join fetch c.artefacts where c.id=:ID").
                setLong("ID", id).
                list();
        if (cycles == null || cycles.isEmpty()) {
            throw new NoRecordException();
        }
        return cycles.iterator().next();
    }

    @Override
    public Date loadFirstSubmissionDate() {
        return (Date) session().createQuery("select min(c.submitFrom) from ReviewCycle c").uniqueResult();
    }

    @Override
    public Date loadLastSubmissionDate() {
        return (Date) session().createQuery("select max(c.submitTo) from ReviewCycle c").uniqueResult();
    }

    
}
