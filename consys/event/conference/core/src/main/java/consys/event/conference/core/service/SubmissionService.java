package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ArtefactData;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Sluzba obsahuje metody k uzivatelskej praci z modulom.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionService {

    /*
     * ------------------------------------------------------------------------
     * --- L I S T ---
     * ------------------------------------------------------------------------
     */
    /**
     * Vylistuje SubmissionType thumbs. Na zaklade includePrivate vlozi aj
     * internal typy.
     *
     * @return vsetky thumby prispevkov ktore maju aspon cyklus alebo artefakt.
     */
    public List<SubmissionTypeThumb> listSubmissionTypesThumbs(boolean includePrivate)
            throws NoRecordException;

    public List<SubmissionTypeArtefact> listArtefactsWithTypesByUuids(List<String> artefacts)
            throws NoRecordException;

    /**
     * Nacita thumby pre vzory pre dany artefakt
     */
    public List<ListUuidItem> listArtefactPatterns(Long artefactId)
            throws RequiredPropertyNullException;

    /**
     * Nacita thumby pre sablony pre dany artefakt
     */
    public List<ListUuidItem> listArtefactTemplates(Long artefactId)
            throws RequiredPropertyNullException;

    /**
     * Nacita vsetky artefakty pre dany typ prispevku
     */
    public List<SubmissionTypeArtefact> listAllArtefactsForType(String uuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * nacte seznam submissionu podle zadanych id
     */
    public List<Submission> listSubmissionsByIds(List<Long> submissionIds) throws RequiredPropertyNullException;

    /**
     * Nacita prispevovatelov k prispevku.
     * 
     * @param submissionUuid
     * @return
     * @throws RequiredPropertyNullException
     * @throws NoRecordException 
     */
    public List<UserEvent> listSubmissionContributors(String submissionUuid)
            throws RequiredPropertyNullException, NoRecordException;

    /**
     * Nacita pre dany typ artefaktu zoznam ID artefact typov podla toho ako 
     * su priradene v cykloch.
     * 
     * @param submissionTypeUuid
     * @return
     * @throws RequiredPropertyNullException
     * @throws NoRecordException 
     */
    public List<Long> listSubmissionTypeArtefactsIds(Long submissionTypeId)
            throws RequiredPropertyNullException;
    /*
     * ------------------------------------------------------------------------     
     * --- L O A D ---     
     * ------------------------------------------------------------------------
     */

    public SubmissionType loadSubmissionTypeWithArtefactsAndCyclesByUuid(String uuid) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita submission type s artefaktami a typami
     * 
     * @param uuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public SubmissionType loadSubmissionTypeWithArtefactTypes(String uuid) throws NoRecordException, RequiredPropertyNullException;

    public SubmissionType loadSubmissionTypeByUuid(String uuid) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita Submission podla UUID s automaticky nacitanimy artefaktami prvej urovne
     * - bez typov.
     *
     * @param submissionUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    public Submission loadSubmissionWithArtefacts(String submissionUuid) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita Submission podla UUID. Nacitava len prvu uroven.
     *
     * @param uuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    public Submission loadSubmissonByUuid(String uuid) throws NoRecordException, RequiredPropertyNullException;

    public Submission loadSubmissonWithTypeAndTopicsByUuid(String uuid) throws NoRecordException, RequiredPropertyNullException;

    public DataFile loadActualDataFileForSubmissionArtefact(String subArtefactUuid) throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException;

    public SubmissionTypeArtefact loadTypeArtefact(String uuid) throws NoRecordException, RequiredPropertyNullException;

    public SubmissionArtefact loadArtefact(String artefactUuid) throws NoRecordException, RequiredPropertyNullException;

    /*
     * ------------------------------------------------------------------------
     * --- C R E A T E ---
     * ------------------------------------------------------------------------
     */
    public void createSubmission(Submission s, Set<Invitation> contributors) throws RequiredPropertyNullException, ServiceExecutionFailed;

    /**
     * Vygeneruje UUID pripsevku.
     *
     * Do buducna moze vytvorit koncept prispevku.
     *
     * @return
     */
    public String createSubmissionUuid();

    /**
     * Vytvori artefakt na AWS S3 a ulozi si identifikator do lokalnej submisson
     * cache.
     *
     * @param submissionUuid uuid ktore je predvygenerovane
     * @param artefactTypeUuid uuid typu artefaktu
     * @param dataFile dataoby subor ktory sa ulozi do AWS
     */
    public String createSubmissionArtefact(String submissionUuid, String artefactTypeUuid, ArtefactData dataFile)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Vytvori novy textovy artefakt na AWS S3 taky ktory bude mozne stiahnut ako plain text
     * 
     * @param submissionUuid
     * @param artefactTypeUuid
     * @param text
     * @return
     * @throws ServiceExecutionFailed
     * @throws RequiredPropertyNullException 
     */
    public String createSubmissionTextArtefact(String submissionUuid, String artefactTypeUuid, String text)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Obnovi time-to-live vytvroenych submisson artefakotv pre dany prispevok.
     * Ak sa neobnovi po presny cas tak sa automaticky straca.
     *
     * @param submissonUuid
     */
    public void refreshSubmissonArtefacts(String submissonUuid);

    /*
     * ------------------------------------------------------------------------
     * --- U P D A T E ---
     * ------------------------------------------------------------------------
     */
    /**
     * Potvrdi platnost artefaktov v Amazon AWS. Ukonci timer ktory automaticky 
     * odstrani prispevky z cache.
     *
     * @param submissionUuid
     */
    public void approveSubmissionArtefacts(String submissionUuid);

    /**
     * Update prispevku. Kontroluje sa standardne vsetko povinne + artefacts
     * budu spracovane len tie ktore su otvorene.
     */
    public void updateSubmission(
            String uuid,
            String title, String subtitle,
            String authors, Set<Invitation> contributors,
            Collection<SubmissionTopic> topics)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException;

    /**
     * Prida novy artefakt uz existujucemu prispevku. Tato zmena je persistentna
     *
     * <strong>POZOR, artafakt sa uklada do cache, takze je potrebne zavolat po
     * uspesnom dokonceni metody mimo transakcie zavolat metodu {@link #approveSubmissionArtefacts(java.lang.String)}</strong>
     *
     * @param submissionUuid
     * @param artefactTypeUuid
     * @param dataFile
     * @return
     */
    public String updateSubmissionArtefact(String submissionUuid, String artefactTypeUuid, ArtefactData dataFile)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException;

    public String updateSubmissionTextArtefact(String submissionUuid, String artefactTypeUuid, String text)
            throws ServiceExecutionFailed, RequiredPropertyNullException, NoRecordException;

    /**
     * Upravi stav prispevku a posle notifikaciu o tom. TODO notifikacia
     *
     * @param submissonUuid
     * @param toState
     * @throws RequiredPropertyNullException
     * @throws NoRecordException
     */
    public Submission updateSubmissonState(String submissonUuid, SubmissionStateEnum toState)
            throws RequiredPropertyNullException, NoRecordException;

    /**
     * vraci datum nejdrivejsiho prijimani prispevku
     */
    public Date loadSubmissionBegindSend();

    /**
     * vraci datum nejpozdejsiho prijimani prispevku
     */
    public Date loadSubmissionEndSend();

    /*
     * ------------------------------------------------------------------------
     * --- D E L E T E ---
     * ------------------------------------------------------------------------
     */
    /**
     * Odstrani prispevok z databaze a s nim vsetky recenzie.
     *
     * @param uuid prispevku
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    public void deleteSubmisson(String uuid) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Odstrani nahraty submission artefact z AWS S3. Ak je artefact uz sucastou pripsevku
     * vyhodi vunimku
     * 
     * @param submissionUuid
     * @param artefactTypeUuid
     * @param artefactUuid
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     * @throws ServiceExecutionFailed 
     */
    public void deleteCachedSubmissionArtefact(String submissionUuid, String artefactTypeUuid, String artefactUuid)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    public void deleteAllCachedSubmissionArtefacts(String submissionUuid)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;
}
