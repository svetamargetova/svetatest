package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.dao.ReviewCriteriaDao;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewCriteriaDaoImpl extends GenericDaoImpl<ReviewCriteria> implements ReviewCriteriaDao {

    @Override
    public ReviewCriteria load(Long id) throws NoRecordException {
        return load(id, ReviewCriteria.class);
    }

    @Override
    public List<ListLongItem> listCriteriaThumbsForType(String typeUuid) {
        List<Object[]> os = session().
                createQuery("select c.name,c.id from ReviewCriteria c where c.submissionTypeArtefact.uuid = :TYPE_UUID").
                setString("TYPE_UUID", typeUuid).
                list();

        List<ListLongItem> thumbs = Lists.newArrayList();
        for (Object[] item : os) {
            thumbs.add(new ListLongItem((String) item[0], (Long) item[1]));
        }
        return thumbs;
    }

    @Override
    public ReviewCriteria loadCriteriaWithItems(Long id) throws NoRecordException {
        ReviewCriteria c = (ReviewCriteria) session().
                createQuery("from ReviewCriteria c LEFT JOIN FETCH c.items where c.id=:ID").
                setLong("ID", id).
                uniqueResult();
        if (c == null) {
            throw new NoRecordException();
        }
        return c;
    }

    @Override
    public void deleteCriteriasForArtefactType(String typeUuid) {
        List<ReviewCriteria> criterias = session().
                createQuery("from ReviewCriteria c where c.submissionTypeArtefact.uuid = :TYPE_UUID").
                setString("TYPE_UUID", typeUuid).
                list();
        for (ReviewCriteria reviewCriteria : criterias) {
            if (log.isDebugEnabled()) {
                log.debug("Delete " + reviewCriteria);
            }
            delete(reviewCriteria);
        }
    }

    @Override
    public List<ReviewCriteria> listCriteriaForArtefactType(String artefactTypeUuid) throws NoRecordException {
        List<ReviewCriteria> criterias = session().
                createQuery("select distinct c from ReviewCriteria c LEFT JOIN FETCH c.items join c.submissionTypeArtefact as t where t.uuid = :TYPE_UUID order by c.name").
                setString("TYPE_UUID", artefactTypeUuid).
                list();
        if(criterias == null || criterias.isEmpty()){
            throw new NoRecordException();
        }
        return criterias;
    }

    @Override
    public List<ReviewCriteria> listCriteriaBySubmissionArtefact(String submissionArtefactUuid) throws NoRecordException {
        List<ReviewCriteria> criterias = session().
                createQuery("select distinct c from ReviewCriteria c LEFT JOIN FETCH c.items, Submission s join s.artefacts as artefact where artefact.uuid=:SUUID and c.submissionTypeArtefact.uuid = artefact.typeArtefact.uuid").
                setString("SUUID", submissionArtefactUuid).
                list();
        if(criterias == null || criterias.isEmpty()){
            throw new NoRecordException();
        }
        return criterias;
    }

    @Override
    public List<ReviewCriteria> listCriteriaForArtefact(String artefactUuid) throws NoRecordException {
        List<ReviewCriteria> criterias = session().
                createQuery("select distinct c from ReviewCriteria c LEFT JOIN FETCH c.items, SubmissionArtefact sa where sa.uuid=:SUUID and c.submissionTypeArtefact.id = sa.typeArtefact.id").
                setString("SUUID", artefactUuid).
                list();
        if(criterias.isEmpty()){
            throw new NoRecordException();
        }
        return criterias;
    }

    @Override
    public List<ReviewCriteria> listCriteriasForSubmissionType(String submissionTypeUuid) throws NoRecordException {
        List<ReviewCriteria> criterias = session().
                createQuery("select distinct c from ReviewCriteria c LEFT JOIN FETCH c.items join c.submissionTypeArtefact as sta where sta.type.uuid=:SUUID ").
                setString("SUUID", submissionTypeUuid).
                list();
        if(criterias.isEmpty()){
            throw new NoRecordException();
        }
        return criterias;
    }
}
