/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.service.cache;

/**
 *
 * @author palo
 */
public class CachedSubmissionArtefact {
    
    private final String artefactUuid;    
    private String fileName;
    

    public CachedSubmissionArtefact(String artefactUuid) {
        this.artefactUuid = artefactUuid;
    }



    public String getArtefactUuid() {
        return artefactUuid;
    }
    
    
    
    
    @Override
    public int hashCode() {
        return artefactUuid.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CachedSubmissionArtefact other = (CachedSubmissionArtefact) obj;
        if ((this.artefactUuid == null) ? (other.artefactUuid != null) : !this.artefactUuid.equals(other.artefactUuid)) {
            return false;
        }
        return true;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    
    
}
