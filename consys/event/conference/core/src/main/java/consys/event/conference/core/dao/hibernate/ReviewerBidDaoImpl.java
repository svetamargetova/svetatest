package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.ListUtils;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.api.list.BidForReviewList;
import consys.event.conference.core.bo.ReviewerBid;
import consys.event.conference.core.bo.ReviewerBidInterestEnum;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.bo.thumb.ReviewerBidListItem;
import consys.event.conference.core.dao.ReviewerBidDao;
import consys.event.conference.core.dao.SubmissionDao;
import java.util.List;
import java.util.Map;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerBidDaoImpl extends GenericDaoImpl<ReviewerBid> implements ReviewerBidDao {

    private SubmissionDao submissionDao;

    @Override
    public ReviewerBid load(Long id) throws NoRecordException {
        return load(id, ReviewerBid.class);
    }
    /*------------------------------------------------------------------------*/
    /*  REVIEWER REVIEWS LIST */
    /*------------------------------------------------------------------------*/
    // Pouziti funkcie je zatim neiste, musi sa vyresit vlozni filtru a ordereru
    // private static final String REVIEWER_REVIEWS_BID_QUERY = "select * from conference.bid_for_review_function(?,?,?) AS (sub_type varchar, sub_id bigint,sub_uuid varchar, sub_authors text,sub_state integer, sub_date timestamp without time zone, sub_title varchar,sub_subtitle varchar, bid_interest integer);";
    //private static final String REVIEWER_BID_QUERY = "typ.name,sub.id,sub.uuid,sub.authors,sub.state,sub.submitted_date,sub.title,sub.subtitle,	bid.interest FROM    conference.submission as sub inner join conference.submission_type as typ on typ.id=sub.id_submission_type left outer join conference.reviewer_bid as bid on bid.submission_id=sub.id  ";
    private static final String REVIEWER_BID_QUERY_ALL = "select count(s.id) from Submission s where s.state >= 0";
    //private static final String REVIEWER_BID_QUERY = "select s from Submission s JOIN FETCH s.type ";
    private static final String REVIEWER_BID_QUERY_SQL = "select s.uuid,s.title,s.subtitle,s.authors,(CASE WHEN bid.interest is not null THEN bid.interest ELSE 0 END) as rbid from conference.submission s left join (select b.* from conference.reviewer_bid b join public.user_event ue on ue.id=b.user_id where ue.uuid=:RUUID)bid on bid.submission_id=s.id where s.state >= 0 ";
    private static final String REVIEWER_BID_PAGING = " LIMIT :MAX OFFSET :START_PAGE";

    @Override
    public List<ReviewerBidListItem> listReviewerBids(Constraints constraints, Paging paging, boolean showAuthors) {
        // Vytvorenie zakladu
        StringBuilder sb = new StringBuilder(REVIEWER_BID_QUERY_SQL);
        SQLQuery query = null;
        setReviewerBidOrderers(constraints, sb);
        sb.append(REVIEWER_BID_PAGING);
        query = session().createSQLQuery(sb.toString());
        query.setString("RUUID", constraints.getUserUuid());
        query.setInteger("MAX", paging.getMaxResults());
        query.setInteger("START_PAGE", paging.getFirstResult());
        // natahneme si vsecky topiky pro submisny ktere tu mame

        Map<String, ReviewerBidListItem> bidMap = Maps.newHashMap();
        List<ReviewerBidListItem> out = Lists.newArrayList();

        List<Object[]> items = query.list();
        // Konverzia do vystupneho objektu. Je to v podsatte to co robi hibernate.

        if (items.isEmpty()) {
            return Lists.newArrayList();
        }

        for (Object[] item : items) {
            ReviewerBidListItem thumb = new ReviewerBidListItem();
            thumb.setUuid((String) item[0]);
            thumb.setTitle((String) item[1]);
            thumb.setSubtitle((String) item[2]);
            if (showAuthors) {
                thumb.setAuthors((String) item[3]);
            }
            thumb.setBidInterestEnum(ReviewerBidInterestEnum.fromId((Integer) item[4]));
            bidMap.put(thumb.getUuid(), thumb);
            out.add(thumb);
        }

        // stahneme si secky topiky
        List<Object[]> topics = session().
                createQuery("select s.uuid,topic from Submission s join s.topics as topic where s.uuid in (:UUIDS)").
                setParameterList("UUIDS", bidMap.keySet()).
                list();
        for (Object[] objects : topics) {
            String key = (String) objects[0];
            bidMap.get(key).getTopics().add((SubmissionTopic) objects[1]);
        }
        // stahneme si secky artefakty
        List<Object[]> artefacts = session().
                createQuery("select artefact.submission.uuid,artefact.uuid,tt.name from SubmissionArtefact artefact join artefact.typeArtefact as t join t.artefactType as tt where artefact.submission.uuid in (:UUIDS)").
                setParameterList("UUIDS", bidMap.keySet()).
                list();
        for (Object[] objects : artefacts) {
            String key = (String) objects[0];
            bidMap.get(key).getArtefacts().add(new ListUuidItem((String) objects[2], (String) objects[1]));
        }
        return out;
    }

    @Override
    public int loadParticipantsCount(Constraints constraints) {
        // Vytvorenie zakladu        
        Query query = null;
        // Filtrovanie podla Stavu prispevku

        // Vsetky prispevky
        query = session().createQuery(REVIEWER_BID_QUERY_ALL);

        return ((Long) query.list().get(0)).intValue();


    }

    private void setReviewerBidOrderers(Constraints constraints, StringBuilder sb) {
        if (constraints.getOrderers().length > 0) {
            sb.append(" order by ");
            if (ListUtils.hasOrderer(constraints.getOrderers(), BidForReviewList.Order_TITLE)) {
                sb.append(" s.title ");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), BidForReviewList.Order_SUBMIT_DATE)) {
                sb.append(" s.submitted_date desc ");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), BidForReviewList.Order_INTEREST)) {
                sb.append(" rbid desc ");
            }
        }
    }

    /*------------------------------------------------------------------------*/
    /*  REVIEWER REVIEWS LIST */
    /*------------------------------------------------------------------------*/
    @Override
    public ReviewerBid loadBidFor(String submissionUuid, String reviewerUuid) throws NoRecordException {
        ReviewerBid bid = (ReviewerBid) session().
                createQuery("from ReviewerBid bid where bid.submission.uuid=:SUUID and bid.reviewer.uuid=:RUUID").
                setString("SUUID", submissionUuid).
                setString("RUUID", reviewerUuid).
                uniqueResult();
        if (bid == null) {
            throw new NoRecordException();
        }
        return bid;
    }

    @Override
    public void deleteAllBids() {
        session().createQuery("delete from ReviewerBid").executeUpdate();
    }

    /*------------------------------------------------------------------------*/
    /*  SettersGetters */
    /*------------------------------------------------------------------------*/
    /**
     * @return the submissionDao
     */
    public SubmissionDao getSubmissionDao() {
        return submissionDao;
    }

    /**
     * @param submissionDao the submissionDao to set
     */
    public void setSubmissionDao(SubmissionDao submissionDao) {
        this.submissionDao = submissionDao;
    }
}
