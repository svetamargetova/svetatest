package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.DataFile;
import consys.event.conference.core.bo.SubmissionArtefact;

/** 
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionArtefactDao extends GenericDao<SubmissionArtefact> {

    public DataFile loadActualDataFileForArtefact(String artefactUuid) throws NoRecordException;

    public SubmissionArtefact loadArtefactWithType(String artefactUuid) throws NoRecordException;

    void deleteAllArtefacts();
}
