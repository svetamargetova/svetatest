package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import consys.event.conference.core.dao.SubmissionTypeDao;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionTypeDaoImpl extends GenericDaoImpl<SubmissionType> implements SubmissionTypeDao {

    @Override
    public SubmissionType load(Long id) throws NoRecordException {
        return load(id, SubmissionType.class);
    }

    @Override
    public Integer loadTypesCount() {
        Criteria criteria = session().createCriteria(SubmissionType.class);
        criteria.setProjection(Projections.rowCount());
        return ((Long) criteria.list().get(0)).intValue();
    }

    @Override
    public List<ListUuidItem> listSettingsThumbTypes() throws NoRecordException {
        List<Object[]> list = session().createQuery("select s.uuid, s.name from SubmissionType s order by s.order").list();
        if (list == null || list.isEmpty()) {
            throw new NoRecordException();
        }
        List<ListUuidItem> types = Lists.newArrayList();
        for (Object[] prop : list) {
            types.add(new ListUuidItem((String) prop[1], (String) prop[0]));
        }
        return types;
    }

    @Override
    public SubmissionType loadTypeWithArtefactsAndCyclesByUuid(String uuid) throws NoRecordException {
        SubmissionType s = (SubmissionType) session().
                createQuery("select s from SubmissionType s LEFT JOIN FETCH s.artefacts as type LEFT JOIN FETCH type.artefactType where s.uuid=:TYPE_UUID").
                setString("TYPE_UUID", uuid).
                uniqueResult();

        if (s == null) {
            throw new NoRecordException();
        }
        // dotahnuti
        s.getCycles().isEmpty();
        return s;
    }

    @Override
    public SubmissionType loadTypeWithArtefactsTypesByUuid(String uuid) throws NoRecordException {
        SubmissionType s = (SubmissionType) session().
                createQuery("from SubmissionType s LEFT JOIN FETCH s.artefacts as a LEFT JOIN FETCH a.artefactType  where s.uuid=:TYPE_UUID ").
                setString("TYPE_UUID", uuid).
                uniqueResult();
        if (s == null) {
            throw new NoRecordException();
        }
        return s;
    }

    @Override
    public SubmissionType loadTypeByUuid(String uuid) throws NoRecordException {
        SubmissionType s = (SubmissionType) session().
                createQuery("from SubmissionType s where s.uuid=:TYPE_UUID").
                setString("TYPE_UUID", uuid).
                uniqueResult();
        if (s == null) {
            throw new NoRecordException();
        }
        return s;
    }

    @Override
    public List<SubmissionTypeThumb> listThumbTypesWithInternalWithDescription() throws NoRecordException {
        List<Object[]> list = session().createQuery("select s.uuid, s.name, s.description from SubmissionType s where (select count(id) from SubmissionTypeArtefact where id_submission_type=s.id) > 0 order by s.order").list();
        if (list == null || list.size() == 0) {
            throw new NoRecordException();
        }
        List<SubmissionTypeThumb> types = Lists.newArrayList();
        for (Object[] prop : list) {
            SubmissionTypeThumb t = new SubmissionTypeThumb();
            t.setUuid((String) prop[0]);
            t.setName((String) prop[1]);
            t.setDescription((String) prop[2]);
            types.add(t);
        }
        return types;
    }

    @Override
    public List<SubmissionTypeThumb> listThumbTypesWithDescription() throws NoRecordException {
        List<Object[]> list = session().createQuery("select s.uuid, s.name, s.description from SubmissionType s where s.internType = false and (select count(id) from SubmissionTypeArtefact where id_submission_type=s.id) > 0 order by s.order").list();
        if (list == null || list.size() == 0) {
            throw new NoRecordException();
        }
        List<SubmissionTypeThumb> types = Lists.newArrayList();
        for (Object[] prop : list) {
            SubmissionTypeThumb t = new SubmissionTypeThumb();
            t.setUuid((String) prop[0]);
            t.setName((String) prop[1]);
            t.setDescription((String) prop[2]);
            types.add(t);
        }
        return types;
    }

    @Override
    public boolean checkSubmissionTypePosition(String uuid, int position) {
        Object resp = session().
                createQuery("select CASE WHEN t.order=:POS THEN true ELSE false END from SubmissionType t where t.uuid=:BUUID").
                setString("BUUID", uuid).
                setInteger("POS", position).
                uniqueResult();
        return resp == null ? false : (Boolean) resp;

    }

    @Override
    public void updateSubmissionTypeOrder(String uuid, int oldPosition, int newPosition) {
        if (oldPosition < newPosition) {
            // update bundle set order=order-1 where order <= old+1 && order >= new        
            session().
                    createQuery("update SubmissionType b set b.order=b.order-1 where b.order > :OPOS and b.order <= :NPOS").
                    setInteger("OPOS", oldPosition).
                    setInteger("NPOS", newPosition).
                    executeUpdate();

        } else {
            // update bundle set order=order+1 where order >= new && order < old
            session().
                    createQuery("update SubmissionType b set b.order=b.order+1 where b.order < :OPOS and b.order >= :NPOS").
                    setInteger("OPOS", oldPosition).
                    setInteger("NPOS", newPosition).
                    executeUpdate();
        }

        int result = session().
                createQuery("update SubmissionType t set t.order=:NPOS where t.uuid=:BUUID").
                setInteger("NPOS", newPosition).
                setString("BUUID", uuid).
                executeUpdate();
        if (result != 1) {
            throw new IllegalArgumentException("Update submission type position failed!");
        }
    }
}
