package consys.event.conference.core.service.impl;

import consys.common.core.service.SystemPropertyService;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.api.list.BidForReviewList;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.core.bo.thumb.ReviewerBidListItem;
import consys.event.conference.core.dao.ReviewerBidDao;
import consys.event.conference.core.service.BidForReviewListService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class BidForReviewListServiceImpl implements BidForReviewListService, BidForReviewList, ConferenceProperties {

    @Autowired
    private ReviewerBidDao reviewerBidDao;
    @Autowired
    private SystemPropertyService propertyService;

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return reviewerBidDao.loadParticipantsCount(constraints);
    }

    @Override
    public List<ReviewerBidListItem> listItems(Constraints constraints, Paging paging) {
        boolean authors = propertyService.getBoolean(SYSTEM_PROPERTY_VISIBLE_AUTHORS);
        return reviewerBidDao.listReviewerBids(constraints, paging, authors);
    }

    @Override
    public List<ReviewerBidListItem> listItemsExport(Constraints constraints, Paging paging) {
        return listItems(constraints, paging);
    }
}
