package consys.event.conference.core.bo.thumb;

public class ReviewCriteriaThumb {

    private Long id;
    private String name;
    private double autoMinAccept;
    private int maxPoints;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the autoMinAccept
     */
    public double getAutoMinAccept() {
        return autoMinAccept;
    }

    public void setAutoMinAccept(double autoMinAccept) {
        this.autoMinAccept = autoMinAccept;
    }

    /**
     * @return the maxPoints
     */
    public int getMaxPoints() {
        return maxPoints;
    }

    /**
     * @param maxPoints the maxPoints to set
     */
    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    
}
