package consys.event.conference.core.bo.thumb;

import consys.common.utils.collection.Lists;
import java.util.List;

public class SubmissionArtefactThumb {

    // zo SubmissionArtefact - nemusi obsahovat ak nepostol
    private String artefactUuid;
    // zo SubmissionTypeArtefact
    private String artefactTypeUuid;
    private int numOfReviewers;
    private boolean required;
    // zo SubmissionArtefactType
    private String name;
    private String description;
    // z Review Criteria
    private List<ReviewCriteriaThumb> criterias;


    public List<ReviewCriteriaThumb> getCriterias() {
        if (criterias == null) {
            criterias = Lists.newArrayList();
        }
        return criterias;
    }

    /**
     * @return the numOfReviewers
     */
    public int getNumOfReviewers() {
        return numOfReviewers;
    }

    public void setNumOfReviewers(int numOfReviewers) {
        this.numOfReviewers = numOfReviewers;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the artefactUuid
     */
    public String getArtefactUuid() {
        return artefactUuid;
    }

    /**
     * @param artefactUuid the artefactUuid to set
     */
    public void setArtefactUuid(String artefactUuid) {
        this.artefactUuid = artefactUuid;
    }

    /**
     * @return the artefactTypeUuid
     */
    public String getArtefactTypeUuid() {
        return artefactTypeUuid;
    }

    /**
     * @param artefactTypeUuid the artefactTypeUuid to set
     */
    public void setArtefactTypeUuid(String artefactTypeUuid) {
        this.artefactTypeUuid = artefactTypeUuid;
    }
}
