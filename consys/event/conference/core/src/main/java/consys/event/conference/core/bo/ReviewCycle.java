package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Palo
 */
public class ReviewCycle implements ConsysObject {
    private static final long serialVersionUID = -8614329744848298856L;

    private Long id;
    private Date submitFrom;
    private Date submitTo;
    private Date reviewFrom;
    private Date reviewTo;
    private SubmissionType submissionType;
    private List<SubmissionTypeArtefact> artefacts;

    public ReviewCycle() {
        artefacts = Lists.newArrayList();
    }



    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the submitFrom
     */
    public Date getSubmitFrom() {
        return submitFrom;
    }

    /**
     * @param submitFrom the submitFrom to set
     */
    public void setSubmitFrom(Date submitFrom) {
        this.submitFrom = submitFrom;
    }

    /**
     * @return the submitTo
     */
    public Date getSubmitTo() {
        return submitTo;
    }

    /**
     * @param submitTo the submitTo to set
     */
    public void setSubmitTo(Date submitTo) {
        this.submitTo = submitTo;
    }

    /**
     * @return the reviewFrom
     */
    public Date getReviewFrom() {
        return reviewFrom;
    }

    /**
     * @param reviewFrom the reviewFrom to set
     */
    public void setReviewFrom(Date reviewFrom) {
        this.reviewFrom = reviewFrom;
    }

    /**
     * @return the reviewTo
     */
    public Date getReviewTo() {
        return reviewTo;
    }

    /**
     * @param reviewTo the reviewTo to set
     */
    public void setReviewTo(Date reviewTo) {
        this.reviewTo = reviewTo;
    }

   

     @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ReviewCycle e = (ReviewCycle) obj;
        return getSubmissionType().equals(e.getSubmissionType()) && submitFrom.equals(e.submitFrom);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.submitFrom != null ? this.submitFrom.hashCode() : 0);
        hash = 71 * hash + (this.getSubmissionType() != null ? this.getSubmissionType().hashCode() : 0);
        return hash;
    }

        @Override
    public String toString() {
        return String.format("ReviewCycle[submit %s-%s review %s-%s]", submitFrom,submitTo,reviewFrom,reviewTo);
    }

    /**
     * @return the submissionType
     */
    public SubmissionType getSubmissionType() {
        return submissionType;
    }

    /**
     * @param submissionType the submissionType to set
     */
    public void setSubmissionType(SubmissionType submissionType) {
        this.submissionType = submissionType;
    }

    /**
     * @return the artefacts
     */
    public List<SubmissionTypeArtefact> getArtefacts() {
        return artefacts;
    }

    /**
     * @param artefacts the artefacts to set
     */
    public void setArtefacts(List<SubmissionTypeArtefact> artefacts) {
        this.artefacts = artefacts;
    }
}
