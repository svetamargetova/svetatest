package consys.event.conference.core.service.impl;

import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.api.list.ContributorContributionsList;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import consys.event.conference.core.dao.ReviewDao;
import consys.event.conference.core.service.ContributorContributionsListService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ContributorsContributionsListServiceImpl implements ContributorContributionsListService, ContributorContributionsList {

    @Autowired
    private ReviewDao reviewDao;

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public List<SubmissionListItem> listItems(Constraints constraints, Paging paging) {
        return reviewDao.listContributions(constraints, paging, true, true);
    }

    @Override
    public List<SubmissionListItem> listItemsExport(Constraints constraints, Paging paging) {
        return listItems(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return reviewDao.loadContributionsCount(constraints, true);
    }   
}
