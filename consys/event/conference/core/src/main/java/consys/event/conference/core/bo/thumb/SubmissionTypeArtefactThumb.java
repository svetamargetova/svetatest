package consys.event.conference.core.bo.thumb;

import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.bo.thumb.ListUuidItem;
import java.util.List;

/**
 * Thumb pre SubmissionTypeArtefact obsahuje zakladne informacie a thumby zoznamov
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionTypeArtefactThumb {

    private int numOfReviewers;
    private int inputLength;
    private boolean required;
    private String name;
    private String description;
    private boolean file;
    private String formats;
    private List<ListLongItem> criterions;
    private List<ReviewCycleThumb> cycles;
    private List<ListUuidItem> patterns;
    private List<ListUuidItem> templates;

    public SubmissionTypeArtefactThumb() {
        criterions = Lists.newArrayList();
        cycles = Lists.newArrayList();
        patterns = Lists.newArrayList();
        templates = Lists.newArrayList();

    }



    /**
     * @return the numOfReviewers
     */
    public int getNumOfReviewers() {
        return numOfReviewers;
    }

    /**
     * @param numOfReviewers the numOfReviewers to set
     */
    public void setNumOfReviewers(int numOfReviewers) {
        this.numOfReviewers = numOfReviewers;
    }

    /**
     * @return the required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * @param required the required to set
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the file
     */
    public boolean isFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(boolean file) {
        this.file = file;
    }

    /**
     * @return the formats
     */
    public String getFormats() {
        return formats;
    }

    /**
     * @param formats the formats to set
     */
    public void setFormats(String formats) {
        this.formats = formats;
    }

    /**
     * @return the criterions
     */
    public List<ListLongItem> getCriterions() {
        return criterions;
    }

    /**
     * @param criterions the criterions to set
     */
    public void setCriterions(List<ListLongItem> criterions) {
        this.criterions = criterions;
    }

    /**
     * @return the cycles
     */
    public List<ReviewCycleThumb> getCycles() {
        return cycles;
    }

    /**
     * @param cycles the cycles to set
     */
    public void setCycles(List<ReviewCycleThumb> cycles) {
        this.cycles = cycles;
    }

    /**
     * @return the patterns
     */
    public List<ListUuidItem> getPatterns() {
        return patterns;
    }

    /**
     * @param patterns the patterns to set
     */
    public void setPatterns(List<ListUuidItem> patterns) {
        this.patterns = patterns;
    }

    /**
     * @return the templates
     */
    public List<ListUuidItem> getTemplates() {
        return templates;
    }

    /**
     * @param templates the templates to set
     */
    public void setTemplates(List<ListUuidItem> templates) {
        this.templates = templates;
    }

    /**
     * @return the inputLength
     */
    public int getInputLength() {
        return inputLength;
    }

    /**
     * @param inputLength the inputLength to set
     */
    public void setInputLength(int inputLength) {
        this.inputLength = inputLength;
    }










}
