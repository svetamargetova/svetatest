package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import java.util.List;

/**
 *
 * @author Palo
 */
public class SubmissionType implements ConsysObject {

    private static final long serialVersionUID = -7781690549703432128L;
    private Long id;
    private String name;
    private String description;
    private String uuid;
    private int order = 0;
    private List<SubmissionTypeArtefact> artefacts;
    private List<ReviewCycle> cycles;
    private boolean internType = false;

    public SubmissionType() {
        artefacts = Lists.newArrayList();
        cycles = Lists.newArrayList();
    }



    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        SubmissionType e = (SubmissionType) obj;
        return name.equalsIgnoreCase(e.name);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("SubmissionType[name=%s]", name);
    }

    /**
     * @return the artefacts
     */
    public List<SubmissionTypeArtefact> getArtefacts() {
        return artefacts;
    }

    /**
     * @param artefacts the artefacts to set
     */
    public void setArtefacts(List<SubmissionTypeArtefact> artefacts) {
        this.artefacts = artefacts;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the order
     */
    public int getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * @return the cycles
     */
    public List<ReviewCycle> getCycles() {
        return cycles;
    }

    /**
     * @param cycles the cycles to set
     */
    public void setCycles(List<ReviewCycle> cycles) {
        this.cycles = cycles;
    }

    /**
     * @return the internType
     */
    public boolean isInternType() {
        return internType;
    }

    /**
     * @param internType the internType to set
     */
    public void setInternType(boolean internType) {
        this.internType = internType;
    }
}
