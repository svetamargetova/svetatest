package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.UserEvent;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author Palo
 */
public class Submission implements ConsysObject {

    private static final long serialVersionUID = 5307542642193891013L;
    private Long id;
    private String uuid;
    private SubmissionType type;
    private Set<UserEvent> contributors;
    private Set<SubmissionTopic> topics;
    private String title;
    private String subTitle;
    private String authors;
    private SubmissionStateEnum state = SubmissionStateEnum.PENDING;
    private Date decidedDate;
    private Date submittedDate;
    private Set<SubmissionArtefact> artefacts;
    private String agregatedTopics;
    private int score = -1;

    public Submission() {
        artefacts = Sets.newHashSet();
        contributors = Sets.newHashSet();
        topics = Sets.newHashSet();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the type
     */
    public SubmissionType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(SubmissionType type) {
        this.type = type;
    }

    /**
     * @return the contributors
     */
    public Set<UserEvent> getContributors() {
        return contributors;
    }

    /**
     * @param contributors the contributors to set
     */
    public void setContributors(Set<UserEvent> contributors) {
        if (contributors == null) {
            contributors = Sets.newHashSet();
        }
        this.contributors = contributors;
    }

    /**
     * @return the topics
     */
    public Set<SubmissionTopic> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(Set<SubmissionTopic> topics) {
        this.topics = topics;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subTitle
     */
    public String getSubTitle() {
        return subTitle;
    }

    /**
     * @param subTitle the subTitle to set
     */
    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Submission e = (Submission) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Submission[uuid=%s title=%s authors=%s]", getUuid(), getTitle(), getAuthors());
    }

    /**
     * @return the authors
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public Date getDecidedDate() {
        return decidedDate;
    }

    public void setDecidedDate(Date decidedDate) {
        this.decidedDate = decidedDate;
    }

    /**
     * @return the state
     */
    public SubmissionStateEnum getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(SubmissionStateEnum state) {
        this.state = state;
    }

    /**
     * @return the artefacts
     */
    public Set<SubmissionArtefact> getArtefacts() {
        return artefacts;
    }

    /**
     * @param artefacts the artefacts to set
     */
    public void setArtefacts(Set<SubmissionArtefact> artefacts) {
        this.artefacts = artefacts;
    }

    /**
     * @return the submittedDate
     */
    public Date getSubmittedDate() {
        return submittedDate;
    }

    /**
     * @param submittedDate the submittedDate to set
     */
    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    /**
     * @return the agregatedTopics
     */
    public String getAgregatedTopics() {
        return agregatedTopics;
    }

    /**
     * @param agregatedTopics the agregatedTopics to set
     */
    public void setAgregatedTopics(String agregatedTopics) {
        this.agregatedTopics = agregatedTopics;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }
}
