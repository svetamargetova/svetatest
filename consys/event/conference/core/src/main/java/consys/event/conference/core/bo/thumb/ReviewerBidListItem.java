package consys.event.conference.core.bo.thumb;

import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.ReviewerBidInterestEnum;
import consys.event.conference.core.bo.SubmissionTopic;
import java.util.List;
import java.util.Set;

/**
 * Thumb pre list bid for review
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerBidListItem {

    private ReviewerBidInterestEnum bidInterestEnum;
    private String uuid;
    private String title;
    private String subtitle;
    private String authors;
    private Set<SubmissionTopic> topics;
    private List<ListUuidItem> artefacts;

    public ReviewerBidListItem() {
        topics = Sets.newHashSet();
        artefacts = Lists.newArrayList();
    }



    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * @param subtitle the subtitle to set
     */
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    /**
     * @return the authors
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    /**
     * @return the topics
     */
    public Set<SubmissionTopic> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(Set<SubmissionTopic> topics) {
        this.topics = topics;
    }   

    /**
     * @return the bidInterestEnum
     */
    public ReviewerBidInterestEnum getBidInterestEnum() {
        return bidInterestEnum;
    }

    /**
     * @param bidInterestEnum the bidInterestEnum to set
     */
    public void setBidInterestEnum(ReviewerBidInterestEnum bidInterestEnum) {
        this.bidInterestEnum = bidInterestEnum;
    }

    /**
     * @return the artefacts
     */
    public List<ListUuidItem> getArtefacts() {
        return artefacts;
    }

    /**
     * @param artefacts the artefacts to set
     */
    public void setArtefacts(List<ListUuidItem> artefacts) {
        this.artefacts = artefacts;
    }
}
