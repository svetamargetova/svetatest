package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.bo.thumb.ReviewCycleThumb;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewCycleDao extends GenericDao<ReviewCycle> {

    public ReviewCycle loadReviewCycleWithType(Long id) throws NoRecordException;

    public ReviewCycle loadReviewCycleWithArtefacts(Long id) throws NoRecordException;

    public ReviewCycle loadReviewCycleForTypeArtefact(String submissionTypeArtefactUuid) throws NoRecordException;

    public List<ReviewCycleThumb> listCycleThumbForType(String typeUuid);

    public List<ReviewCycle> listCyclesWithArtefactTypesForSubmissionType(String typeUuid) throws NoRecordException;

    public List<ReviewCycle> listOnlyActiveCyclesForSubmissionForType(String typeUuid) throws NoRecordException;

    public void deleteCycle(Long id) throws NoRecordException;

    public void deleteAllCyclesForType(String typeUuid) throws NoRecordException;

    public void deleteArtefactFromCycle(SubmissionTypeArtefact artefact);

    void deleteAllCyclesAndCyclesArtefacts();

    /**
     * Nacita pre UUID typov artefaktov daneho Typu Prispevku ID cyklov.
     *
     * @return kluc - uuid artefaktu, hodnota - ID cyklu
     */
    public Map<String, Long> listReviewCycleIdsFor(String submissionType, List<String> submissionTypeArtefacs);

    /** nacte prvni datum zasilani prispevku */
    public Date loadFirstSubmissionDate();

    /** nacte posledni datum zasilani prispevku */
    public Date loadLastSubmissionDate();
}
