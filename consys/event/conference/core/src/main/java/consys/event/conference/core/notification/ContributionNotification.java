/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.notification;

import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import consys.common.utils.collection.Maps;
import consys.event.conference.core.bo.Submission;
import java.util.Map;
import net.notx.client.PlaceHolders;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author palo
 */
public class ContributionNotification extends AbstractSystemNotification {

    private final Submission submission;
    private Map<String, String> evaluatedPlaceholders;

    public ContributionNotification(Submission submission, SystemMessage systemMessage) {
        super(systemMessage, null);
        this.submission = submission;
    }

    public void evaluatePlaceholders(Map<String, String> placeholders) {
        evaluatedPlaceholders.put("type", submission.getType().getName());
        evaluatedPlaceholders.put("title", submission.getTitle());
        evaluatedPlaceholders.put("subtitle", StringUtils.isBlank(submission.getSubTitle()) ? "\u2014" : submission.getSubTitle());
        evaluatedPlaceholders.put("authors", submission.getAuthors());
        evaluatedPlaceholders.put("topics", StringUtils.isBlank(submission.getAgregatedTopics()) ? "\u2014" : submission.getAgregatedTopics());
        evaluatedPlaceholders.put("score", scoreOrDash(submission.getScore()));
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {
        if (evaluatedPlaceholders == null) {
            evaluatedPlaceholders = Maps.newHashMap();
            evaluatePlaceholders(evaluatedPlaceholders);
        }

        for (Map.Entry<String, String> entry : evaluatedPlaceholders.entrySet()) {
            placeHolders.addPlaceHolder(entry.getKey(), entry.getValue());
        }
    }

    protected static String scoreOrDash(int score) {
        return score >= 0 ? Integer.toString(score) : "\u2014";
    }

    public Submission getSubmission() {
        return submission;
    }
}
