package consys.event.conference.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.core.properties.SystemPropertyUpdateHandler;
import consys.event.common.core.utils.SystemPropertyUtils;
import consys.event.conference.api.properties.ConferenceProperties;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class VisibleAuthorsPropertyHandler implements SystemPropertyUpdateHandler {    

    @Override
    public String getKey() {
        return ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
        SystemPropertyUtils.isBooleanProperty(newValue);
    }
}
