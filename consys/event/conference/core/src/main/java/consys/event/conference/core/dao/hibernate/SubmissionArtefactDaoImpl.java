package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.SubmissionArtefact;
import consys.event.conference.core.dao.SubmissionArtefactDao;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionArtefactDaoImpl extends GenericDaoImpl<SubmissionArtefact> implements SubmissionArtefactDao {

    @Override
    public SubmissionArtefact load(Long id) throws NoRecordException {
        return load(id, SubmissionArtefact.class);
    }

    @Override
    public DataFile loadActualDataFileForArtefact(String artefactUuid) throws NoRecordException {
        DataFile file = (DataFile) session().
                createQuery("select file from SubmissionArtefact ar join ar.files as file where ar.uuid=:UUID order by file.dateInserted desc").
                setString("UUID", artefactUuid).
                setMaxResults(1).
                uniqueResult();
        if (file == null) {
            throw new NoRecordException("No SubmissionArtefact with uuid " + artefactUuid + " has datafile");
        }
        return file;
    }

    @Override
    public SubmissionArtefact loadArtefactWithType(String artefactUuid) throws NoRecordException {
        SubmissionArtefact artefact = (SubmissionArtefact) session().
                createQuery("select a from SubmissionArtefact a LEFT JOIN FETCH a.typeArtefact where a.uuid=:AUUID").
                setString("AUUID", artefactUuid).
                uniqueResult();
        if (artefact == null) {
            throw new NoRecordException();
        }
        return artefact;
    }

    @Override
    public void deleteAllArtefacts() {
        // toto je tabulka, ktera se uz nepouziva a ma referenci na submissionArtefact, tak at se smaze kdyz existuje
        session().createSQLQuery("drop table if exists conference.submission_file").executeUpdate();
        session().createQuery("delete from SubmissionArtefact").executeUpdate();
    }
}
