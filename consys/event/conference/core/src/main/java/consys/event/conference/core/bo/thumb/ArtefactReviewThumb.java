/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.bo.thumb;

/**
 *
 * @author palo
 */
public class ArtefactReviewThumb {

    private String reviewUuid;
    private Long criteriaId;
    private Long criteriaItemValue;

    public ArtefactReviewThumb(String reviewUuid, Long criteriaId, Long criteriaItemValue) {
        this.reviewUuid = reviewUuid;
        this.criteriaId = criteriaId;
        this.criteriaItemValue = criteriaItemValue;
    }

    /**
     * @return the reviewUuid
     */
    public String getReviewUuid() {
        return reviewUuid;
    }

    /**
     * @return the criteriaId
     */
    public Long getCriteriaId() {
        return criteriaId;
    }

    /**
     * @return the criteriaValue
     */
    public Long getCriteriaItemValue() {
        return criteriaItemValue;
    }
}
