package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import consys.event.conference.core.bo.ReviewerPreferredTopic;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewerPreferredTopicDao extends GenericDao<ReviewerPreferredTopic> {

    public List<Long> listTopicsIdsForUser(String userUuid);

    public List<ReviewerPreferredTopic> listTopicsForUser(String userUuid);

    public void delete(UserEvent user, Long topic) throws NoRecordException;

    public void create(UserEvent user, Long topic) throws NoRecordException;
}
