package consys.event.conference.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class TopicNotUniqueException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public TopicNotUniqueException() {
        super();
    }

     public TopicNotUniqueException(String message) {
	super(message);
    }

}
