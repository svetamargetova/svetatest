package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 * Recenzia konkretneho kriteria artefaktu
 *
 * @author Palo
 */
public class ReviewArtefactCriteriaEvaluation implements ConsysObject {

    private static final long serialVersionUID = -6064401177143665382L;
    /** Vybrana hodnota kriteria */
    private ReviewCriteriaItem criteriaItem;
    /** Kriterium */
    private ReviewCriteria criteria;
    /** Ku ktorej recenzii patri */
    private ReviewArtefact reviewArtefact;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        ReviewArtefactCriteriaEvaluation e = (ReviewArtefactCriteriaEvaluation) obj;
        return getReviewArtefact().equals(e.getReviewArtefact()) && criteria.equals(e.criteria) && criteriaItem.equals(e.criteriaItem);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.criteriaItem != null ? this.criteriaItem.hashCode() : 0);
        hash = 59 * hash + (this.criteria != null ? this.criteria.hashCode() : 0);
        hash = 59 * hash + (this.getReviewArtefact() != null ? this.getReviewArtefact().hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("ReviewArtefactCriteriaEvaluation[%s %s %s]", getReviewArtefact(), criteria, getCriteriaItem());
    }

    /**
     * @return the criteriaItem
     */
    public ReviewCriteriaItem getCriteriaItem() {
        return criteriaItem;
    }

    /**
     * @param criteriaItem the criteriaItem to set
     */
    public void setCriteriaItem(ReviewCriteriaItem criteriaItem) {
        this.criteriaItem = criteriaItem;
    }

    /**
     * @return the criteria
     */
    public ReviewCriteria getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(ReviewCriteria criteria) {
        this.criteria = criteria;
    }

    /**
     * Ku ktorej recenzii patri
     * @return the reviewArtefact
     */
    public ReviewArtefact getReviewArtefact() {
        return reviewArtefact;
    }

    /**
     * Ku ktorej recenzii patri
     * @param reviewArtefact the reviewArtefact to set
     */
    public void setReviewArtefact(ReviewArtefact reviewArtefact) {
        this.reviewArtefact = reviewArtefact;
    }
}
