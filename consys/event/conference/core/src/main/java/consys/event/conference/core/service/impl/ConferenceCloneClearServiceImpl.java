package consys.event.conference.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.conference.core.dao.*;
import consys.event.conference.core.service.ConferenceCloneClearService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ConferenceCloneClearServiceImpl extends AbstractService implements ConferenceCloneClearService {

    @Autowired
    private ReviewCycleDao reviewCycleDao;
    @Autowired
    private SubmissionArtefactDao submissionArtefactDao;
    @Autowired
    private SubmissionDao submissionDao;
    @Autowired
    private ReviewerBidDao reviewerBidDao;
    @Autowired
    private ReviewArtefactDao reviewArtefactDao;
    @Autowired
    private ReviewDao reviewDao;
    @Autowired
    private ReviewArtefactCriteriaEvaluationDao reviewArtefactCriteriaEvaluationDao;

    @Override
    public void clearClone() {
        // conference.reviewer_bid
        reviewerBidDao.deleteAllBids();

        // conference.review_cycle_artefact
        // conference.review_cycle
        reviewCycleDao.deleteAllCyclesAndCyclesArtefacts();

        // conference.review_artefact_criteria_evaluation
        reviewArtefactCriteriaEvaluationDao.deleteAll();

        // conference.review_artefact
        reviewArtefactDao.deleteAllArtefacts();
        
        // conference.submission_artefact
        submissionArtefactDao.deleteAllArtefacts();

        // conference.review
        reviewDao.deleteAllReviews();

        // conference.submission_contributor - maze se se smazanim submission
        // conference.submission_topic_rel - maze se se submission
        // conference.submission
        submissionDao.deleteAllSubmissions();
    }
}
