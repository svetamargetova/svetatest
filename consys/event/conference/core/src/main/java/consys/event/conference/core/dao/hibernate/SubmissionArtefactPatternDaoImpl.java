package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.SubmissionArtefactPattern;
import consys.event.conference.core.dao.SubmissionArtefactPatternDao;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionArtefactPatternDaoImpl extends GenericDaoImpl<SubmissionArtefactPattern> implements SubmissionArtefactPatternDao {

    @Override
    public SubmissionArtefactPattern load(Long id) throws NoRecordException {
        return load(id, SubmissionArtefactPattern.class);
    }

    @Override
    public List<ListUuidItem> listPatternsForArtefact(Long id) {
        List<Object[]> items = session().
                createQuery("select pattern.uuid,pattern.title from SubmissionArtefactType t join t.patterns as pattern where t.id=:ID").
                setLong("ID", id).
                list();
        List<ListUuidItem> out = Lists.newArrayList();
        for (Object[] o : items) {
            out.add(new ListUuidItem((String) o[1], (String) o[0]));
        }
        return out;
    }

    @Override
    public List<ListUuidItem> listPatternsThumbs() throws NoRecordException {
        List<Object[]> items = session().
                createQuery("select pattern.uuid,pattern.title from SubmissionArtefactPattern pattern order by pattern.title").
                list();
        List<ListUuidItem> out = Lists.newArrayList();
        for (Object[] o : items) {
            out.add(new ListUuidItem((String) o[1], (String) o[0]));
        }
        return out;
    }

    @Override
    public SubmissionArtefactPattern loadByDataFileUuid(String uuid) throws NoRecordException {
        SubmissionArtefactPattern p = (SubmissionArtefactPattern) session().
                createQuery("from SubmissionArtefactPattern pattern where pattern.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (p == null) {
            throw new NoRecordException();
        }
        return p;
    }

    @Override
    public List<SubmissionArtefactPattern> listPatternsByUuids(List<String> uuids) throws NoRecordException {
        List<SubmissionArtefactPattern> p = (List<SubmissionArtefactPattern>) session().
                createQuery("select pattern from SubmissionArtefactPattern pattern where pattern.uuid IN (:UUIDS)").
                setParameterList("UUIDS", uuids).
                list();
        if (p == null || p.isEmpty()) {
            throw new NoRecordException();
        }
        return p;
    }
}
