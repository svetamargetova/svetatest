package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.bo.SubmissionTopic;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionTopicDao extends GenericDao<SubmissionTopic>{

    public List<SubmissionTopic> listTopicsByName();

    public List<SubmissionTopic> listTopicsByList(Collection<Long> topics) throws NoRecordException;

}
