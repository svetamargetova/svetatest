package consys.event.conference.core.exception;

import consys.common.core.exception.ConsysException;

/**
 * Znamena to ze na TOPIC uz su naviazane nejake dalsie objektu [submission, preffered topics]
 * @author Palo
 */
public class DeleteCycleException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public DeleteCycleException() {
        super();
    }

     public DeleteCycleException(String message) {
	super(message);
    }

}
