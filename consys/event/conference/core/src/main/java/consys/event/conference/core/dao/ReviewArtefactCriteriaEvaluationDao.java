package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.bo.ReviewArtefactCriteriaEvaluation;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com> 
 */
public interface ReviewArtefactCriteriaEvaluationDao extends GenericDao<ReviewArtefactCriteriaEvaluation> {

    /**
     * Vylistuje UUID artefaktov ktore uz recenzent recenzoval
     */
    public List<String> listUuidsForReviewedArtefacts(String reviewerUuid, String submissionUuid);

    /**
     * Nacitame ReviewArtefactCriteriaEvaluation pre recenzenta a artefakt. 
     */
    public ReviewArtefactCriteriaEvaluation loadEvaluationFor(String reviewerUuid, String submissionArtefactUuid) throws NoRecordException;

    public List<ReviewArtefactCriteriaEvaluation> listEvaluationsFor(String artefactUuid, String reviewerUuid) throws NoRecordException;

    void deleteAll();
}
