package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewCriteriaItem implements ConsysObject{
    private static final long serialVersionUID = -163673389759852106L;

    private Long id;
    private ReviewCriteria criteria;
    private String name;
    private String description;
    private int value;

    public ReviewCriteriaItem() {
    }
    
    public ReviewCriteriaItem(String name, int value) {
        this.name = name;
        this.value = value;
    }

    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the criteria
     */
    public ReviewCriteria getCriteria() {
        return criteria;
    }

    /**
     * @param criteria the criteria to set
     */
    public void setCriteria(ReviewCriteria criteria) {
        this.criteria = criteria;
    }

   

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ReviewCriteriaItem e = (ReviewCriteriaItem) obj;
        return id.equals(e.id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("ReviewCriteriaItem[name=%s value=%d]", name,value);
    }
}
