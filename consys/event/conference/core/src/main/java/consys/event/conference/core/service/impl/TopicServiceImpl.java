package consys.event.conference.core.service.impl;

import com.google.common.collect.Lists;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.dao.ReviewerPreferredTopicDao;
import consys.event.conference.core.dao.SubmissionTopicDao;
import consys.event.conference.core.exception.DeleteRelationsException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import consys.event.conference.core.service.TopicService;
import java.util.Collection;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TopicServiceImpl extends AbstractService implements TopicService {

    @Autowired
    private SubmissionTopicDao topicDao;
    @Autowired
    private ReviewerPreferredTopicDao preferredTopicDao;
    @Autowired
    private UserEventService userEventService;          

    @Override
    public SubmissionTopic loadTopic(Long id) throws NoRecordException {
        return topicDao.load(id);
    }

    @Override
    public void createTopic(SubmissionTopic topic) throws RequiredPropertyNullException, TopicNotUniqueException {
        
        log().debug("Create {}", topic);
        

        if (StringUtils.isBlank(topic.getName())) {
            throw new RequiredPropertyNullException("Missing topic name");
        }
        try {
            topicDao.create(topic);
        } catch (ConstraintViolationException ex) {            
            log().debug("Topic with name '{}' already registred.",topic.getName());            
            throw new TopicNotUniqueException();
        }
    }

    @Override
    public void updateTopic(SubmissionTopic topic) throws RequiredPropertyNullException, TopicNotUniqueException {        
        log().debug("Update {}",topic);        
        if (StringUtils.isBlank(topic.getName())) {        
            log().debug("Topic name NULL! ");            
            throw new RequiredPropertyNullException();
        }
        try {
            topicDao.update(topic);
        } catch (ConstraintViolationException ex) {
            log().debug("Topic with name '{}' already registred.",topic.getName());            
            throw new TopicNotUniqueException();
        }
    }

    @Override
    public List<SubmissionTopic> listTopics() {
        return topicDao.listTopicsByName();
    }

    @Override
    public void deleteTopic(SubmissionTopic topic) throws DeleteRelationsException {        
        log().debug("Delete " + topic);        
        try {
            topicDao.delete(topic);
        } catch (ConstraintViolationException e) {            
            log().debug("{} can't be deleted because of other relations.",topic);            
            throw new DeleteRelationsException();
        }
    }

    @Override
    public void deleteTopic(Long id) throws DeleteRelationsException, NoRecordException {
        SubmissionTopic topic = loadTopic(id);
        deleteTopic(topic);
    }

    @Override
    public void deleteTopicForce(SubmissionTopic topic) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Long> listReviewerPreferredTopics(String reviewerUuid) {
        return preferredTopicDao.listTopicsIdsForUser(reviewerUuid);
    }

     /**
     * Update user preferred topics. Najskor sa nacitaju vsetky topiky a potom
     * sa zmerguju.
     */
    @Override
    public void updateReviewerTopics(String reviewerUuid, List<Long> updatedTopics) throws NoRecordException {        
        log().debug("Updating reviewer preferred topics");
                
        UserEvent ue = userEventService.loadUserEvent(reviewerUuid);

        List<Long> persistedTopics = preferredTopicDao.listTopicsIdsForUser(reviewerUuid);
        // Odstranime vsetky topiky
        for (Long l : persistedTopics) {            
            log().debug("  Removing topic");            
            preferredTopicDao.delete(ue, l);
        }
        // Nacitame vsetky nove topiky a vytvorime
         // Topiky ktore zostali v updatedTopics tak vytvorime nove
        for (Long l : updatedTopics) {            
            log().debug("   {} <=> {}",ue,l);            
            preferredTopicDao.create(ue, l);
        }
    }

    @Override
    public List<SubmissionTopic> listTopicsByList(Collection<Long> ids) throws NoRecordException {
        
        if(ids == null || ids.isEmpty()){
            return Lists.newArrayList();
        }
        
        return topicDao.listTopicsByList(ids); 
    }
}
