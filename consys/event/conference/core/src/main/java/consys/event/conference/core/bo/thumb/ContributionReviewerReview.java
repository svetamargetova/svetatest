/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.bo.thumb;


import com.google.common.collect.Lists;
import java.util.List;

/**
 * Celkova recenzia prispevku
 * @author palo
 */
public class ContributionReviewerReview {
            
    private String reviewerUuid;
    private String reviewerName;
    private String reviewerPosition;
    private String reviewerOrganization;
    private String reviewerImageUuidPrefix;
    private String reviewerPublicComment;
    private String reviewerPrivateComment;
    private String reviewUuid;
    private int reviewerReviewScore;    
    private List<ContributionReviewerArtefactReview> artefactReviews;

    public ContributionReviewerReview() {
        artefactReviews = Lists.newArrayList();
    }       
    
    /**
     * @return the reviewUuid
     */
    public String getReviewUuid() {
        return reviewUuid;
    }

    /**
     * @param reviewUuid the reviewUuid to set
     */
    public void setReviewUuid(String reviewUuid) {
        this.reviewUuid = reviewUuid;
    }

    /**
     * @return the reviewerUuid
     */
    public String getReviewerUuid() {
        return reviewerUuid;
    }

    /**
     * @param reviewerUuid the reviewerUuid to set
     */
    public void setReviewerUuid(String reviewerUuid) {
        this.reviewerUuid = reviewerUuid;
    }

    /**
     * @return the reviewerName
     */
    public String getReviewerName() {
        return reviewerName;
    }

    /**
     * @param reviewerName the reviewerName to set
     */
    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    /**
     * @return the reviewerPosition
     */
    public String getReviewerPosition() {
        return reviewerPosition;
    }

    /**
     * @param reviewerPosition the reviewerPosition to set
     */
    public void setReviewerPosition(String reviewerPosition) {
        this.reviewerPosition = reviewerPosition;
    }

    /**
     * @return the reviewerOrganization
     */
    public String getReviewerOrganization() {
        return reviewerOrganization;
    }

    /**
     * @param reviewerOrganization the reviewerOrganization to set
     */
    public void setReviewerOrganization(String reviewerOrganization) {
        this.reviewerOrganization = reviewerOrganization;
    }

    /**
     * @return the reviewerImageUuidPrefix
     */
    public String getReviewerImageUuidPrefix() {
        return reviewerImageUuidPrefix;
    }

    /**
     * @param reviewerImageUuidPrefix the reviewerImageUuidPrefix to set
     */
    public void setReviewerImageUuidPrefix(String reviewerImageUuidPrefix) {
        this.reviewerImageUuidPrefix = reviewerImageUuidPrefix;
    }

    /**
     * @return the reviewerPublicComment
     */
    public String getReviewerPublicComment() {
        return reviewerPublicComment;
    }

    /**
     * @param reviewerPublicComment the reviewerPublicComment to set
     */
    public void setReviewerPublicComment(String reviewerPublicComment) {
        this.reviewerPublicComment = reviewerPublicComment;
    }

    /**
     * @return the reviewerPrivateComment
     */
    public String getReviewerPrivateComment() {
        return reviewerPrivateComment;
    }

    /**
     * @param reviewerPrivateComment the reviewerPrivateComment to set
     */
    public void setReviewerPrivateComment(String reviewerPrivateComment) {
        this.reviewerPrivateComment = reviewerPrivateComment;
    }

    /**
     * @return the reviewerReviewScore
     */
    public int getReviewerReviewScore() {
        return reviewerReviewScore;
    }

    /**
     * @param reviewerReviewScore the reviewerReviewScore to set
     */
    public void setReviewerReviewScore(int reviewerReviewScore) {
        this.reviewerReviewScore = reviewerReviewScore;
    }

    /**
     * @return the artefactReviews
     */
    public List<ContributionReviewerArtefactReview> getArtefactReviews() {
        return artefactReviews;
    }    
}
