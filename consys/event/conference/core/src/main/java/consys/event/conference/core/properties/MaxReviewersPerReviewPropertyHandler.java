package consys.event.conference.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.core.properties.SystemPropertyUpdateHandler;
import consys.event.conference.api.properties.ConferenceProperties;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class MaxReviewersPerReviewPropertyHandler implements SystemPropertyUpdateHandler {

    @Override
    public String getKey() {
        return ConferenceProperties.SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
        try {
            int newIntValue = Integer.parseInt(newValue);
            if (newIntValue >= 0) {
                return;
            }
            throw new IllegalPropertyValue("New Value must be positive integer or 0!");

        } catch (NumberFormatException e) {
            throw new IllegalPropertyValue("SystemProperty have to be a number!");
        }
    }
}
