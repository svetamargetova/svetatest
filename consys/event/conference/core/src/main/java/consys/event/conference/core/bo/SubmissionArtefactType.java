package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Sets;
import java.util.Set;

/**
 *
 * @author Palo
 */
public class SubmissionArtefactType implements ConsysObject {

    private static final long serialVersionUID = 1994083590974433614L;
    private Long id;
    private Set<SubmissionArtefactPattern> patterns;
    private Set<SubmissionArtefactTemplate> templates;
    private String name;
    private String description;
    private boolean file;
    private String formats;
    private int inputLength = 0;

    public SubmissionArtefactType() {
        patterns = Sets.newHashSet();
        templates = Sets.newHashSet();
    }
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the patterns
     */
    public Set<SubmissionArtefactPattern> getPatterns() {
        return patterns;
    }

    /**
     * @param patterns the patterns to set
     */
    public void setPatterns(Set<SubmissionArtefactPattern> patterns) {
        this.patterns = patterns;
    }

    /**
     * @return the templates
     */
    public Set<SubmissionArtefactTemplate> getTemplates() {
        return templates;
    }

    /**
     * @param templates the templates to set
     */
    public void setTemplates(Set<SubmissionArtefactTemplate> templates) {
        this.templates = templates;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the file
     */
    public boolean isFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(boolean file) {
        this.file = file;
    }

    /**
     * @return the formats
     */
    public String getFormats() {
        return formats;
    }

    /**
     * @param formats the formats to set
     */
    public void setFormats(String formats) {
        this.formats = formats;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        SubmissionArtefactType e = (SubmissionArtefactType) obj;
        return name.equalsIgnoreCase(e.name);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("SubmissionArtefactType[title=%s file=%s formats=%s inputLength=%d]", name,file,formats,inputLength);
    }

    /**
     * @return the inputLength
     */
    public int getInputLength() {
        return inputLength;
    }

    /**
     * @param inputLength the inputLength to set
     */
    public void setInputLength(int inputLength) {
        this.inputLength = inputLength;
    }
}
