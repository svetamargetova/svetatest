package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.core.bo.Review;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.bo.thumb.ReviewerThumb;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewDao extends GenericDao<Review> {

    /**
     * Nacita prvu uroven recenzie. Nedotahuje ziadne dalsie podobjekty.
     * @param uuid
     * @return
     * @throws NoRecordException 
     */
    public Review loadReview(String uuid) throws NoRecordException;

    public List<ContributionReviewerReview> listSubmissionReviewerReviews(Long submissionId);

    public Integer actualizeScore(Long submissionId, Long artefactId, Long reviewId);

    /**
     * Vrati true ak ma uz recenzent priradeny tento prispevok na recenzovanie
     */
    public boolean isAlreadyReviewer(String reviewer, String submission);

    public Review loadReviewerReview(String reviewerUuid, String submissionUuid) throws NoRecordException;

    public Review loadReviewerReviewWithArtefactsReviewWithoutCriterias(String reviewerUuid, String submissionUuid) throws NoRecordException;

    public Review loadReviewerReviewWithArtefactsReviewWithoutCriterias(String reviewUuid) throws NoRecordException;

    public Review loadReviewerReviewWithDetailedSubmission(String reviewerUuid, String submissionUuid) throws NoRecordException;

    public Review loadReviewerReviewByArtefact(String reviewerUuid, String submissionArtefactUuid) throws NoRecordException;

    public List<ReviewerThumb> listAllReviewers(String submissionUuid) throws NoRecordException;

    public void deleteReview(String submissionUuid, String reviewerUuid) throws NoRecordException;

    /*
     * ------------------------------------------------------------------------
     * REVIEWER REVIEWS LIST
     * ------------------------------------------------------------------------
     */
    public List<SubmissionListItem> listReviewerReviews(Constraints constraints, Paging paging, boolean showAuthors);

    public int loadReviewerReviewsCount(Constraints constraints);

    /*
     * ------------------------------------------------------------------------
     * CHAIR AND CONTRIBUTOR LIST
     * ------------------------------------------------------------------------
     */
    public List<SubmissionListItem> listContributions(Constraints constraints, Paging paging, boolean showAuthors, boolean userAsContributor);

    public int loadContributionsCount(Constraints constraints, boolean userAsContributor);

    void deleteAllReviews();
}
