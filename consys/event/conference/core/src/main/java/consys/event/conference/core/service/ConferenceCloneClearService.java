package consys.event.conference.core.service;

/**
 * Service pro vycisteni naklonovanych dat
 * @author pepa
 */
public interface ConferenceCloneClearService {

    /** Vycisti naklonovana data */
    void clearClone();
}
