package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author Palo
 */
public class SubmissionArtefactPattern implements ConsysObject {

    private static final long serialVersionUID = -5354098506735837696L;
    private Long id;   
    /** UUID petternu v AWS */
    private String uuid;    
    private String desciption;
    private String title;    
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }   

    /**
     * @return the desciption
     */
    public String getDesciption() {
        return desciption;
    }

    /**
     * @param desciption the desciption to set
     */
    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    @Override
    public String toString() {
        return String.format("Pattern[uuid=%s name=%s]",uuid, title);
    }

    /**
     * UUID petternu v AWS
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * UUID petternu v AWS
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    
}
