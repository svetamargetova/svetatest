/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.bo.thumb;

import consys.common.utils.collection.Lists;
import java.util.List;

/**
 *
 * @author palo
 */
public class ArtefactReviewNotificationData {
    
    private String artefactTitle;
    private String reviewerName;
    private String reviewerArtefactPublicComment;
    private int reviewScore;
    private int artefactReviewScore;
    private int artefactScore;
    private List<CriteriaEvaluation> evaluations;

    public ArtefactReviewNotificationData() {
        evaluations = Lists.newArrayList();
    }

    
    
    /**
     * @return the artefactTitle
     */
    public String getArtefactTitle() {
        return artefactTitle;
    }

    /**
     * @param artefactTitle the artefactTitle to set
     */
    public void setArtefactTitle(String artefactTitle) {
        this.artefactTitle = artefactTitle;
    }

    /**
     * @return the reviewerName
     */
    public String getReviewerName() {
        return reviewerName;
    }

    /**
     * @param reviewerName the reviewerName to set
     */
    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    /**
     * @return the reviewerArtefactPublicComment
     */
    public String getReviewerArtefactPublicComment() {
        return reviewerArtefactPublicComment;
    }

    /**
     * @param reviewerArtefactPublicComment the reviewerArtefactPublicComment to set
     */
    public void setReviewerArtefactPublicComment(String reviewerArtefactPublicComment) {
        this.reviewerArtefactPublicComment = reviewerArtefactPublicComment;
    }

    /**
     * @return the reviewScore
     */
    public int getReviewScore() {
        return reviewScore;
    }

    /**
     * @param reviewScore the reviewScore to set
     */
    public void setReviewScore(int reviewScore) {
        this.reviewScore = reviewScore;
    }

    /**
     * @return the artefactReviewScore
     */
    public int getArtefactReviewScore() {
        return artefactReviewScore;
    }

    /**
     * @param artefactReviewScore the artefactReviewScore to set
     */
    public void setArtefactReviewScore(int artefactReviewScore) {
        this.artefactReviewScore = artefactReviewScore;
    }

    /**
     * @return the artefactScore
     */
    public int getArtefactScore() {
        return artefactScore;
    }

    /**
     * @param artefactScore the artefactScore to set
     */
    public void setArtefactScore(int artefactScore) {
        this.artefactScore = artefactScore;
    }

    public List<CriteriaEvaluation> getEvaluations() {        
        return evaluations;
    }
    
   public void addEvaluation(String title, int points){
       evaluations.add(new CriteriaEvaluation(title, points));
   }
    
        
    public class CriteriaEvaluation{
        final String title;
        final int points;

        public CriteriaEvaluation(String title, int points) {
            this.title = title;
            this.points = points;
        }

        public int getPoints() {
            return points;
        }

        public String getTitle() {
            return title;
        }
        
        
    
    }    
        
}
