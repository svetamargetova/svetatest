package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.SubmissionArtefactType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.dao.SubmissionArtefactTypeDao;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionArtefactTypeDaoImpl extends GenericDaoImpl<SubmissionArtefactType> implements SubmissionArtefactTypeDao {

    @Override
    public SubmissionArtefactType load(Long id) throws NoRecordException {
        return load(id, SubmissionArtefactType.class);
    }

    @Override
    public int loadArtefactTypeUseCountBySubmissionTypeUuid(String submissionTypeUuid) {
        return ((Long) session().
                createQuery("select count(type.id) from SubmissionTypeArtefact type where type.artefactType.id=(select atype.artefactType.id from SubmissionTypeArtefact atype where atype.uuid=:TYPE_UUID)").
                setString("TYPE_UUID", submissionTypeUuid).
                uniqueResult()).intValue();
    }

   
   
}
