/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.service.cache;

import com.google.common.collect.ImmutableList;
import consys.common.core.aws.AwsFileStorageService;
import consys.event.overseer.cache.AbstractCacheService;
import java.util.List;
import java.util.Map;
import net.sf.ehcache.CacheEntry;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.writer.AbstractCacheWriter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class SubmissionArtefactCache extends AbstractCacheService<String, Map<String, CachedSubmissionArtefact>> {

    private String submissonBucket;
    @Autowired
    private AwsFileStorageService awsFileStorageService;

    public SubmissionArtefactCache() {
        super("submission-artefacts");
    }

    @Override
    protected Ehcache decorate(Ehcache cache) {
        cache.registerCacheWriter(new AbstractCacheWriter() {
            @Override
            public void delete(CacheEntry entry) throws CacheException {
                if (entry.getElement().isExpired()) {
                    Map<String, CachedSubmissionArtefact> artefacts = (Map<String, CachedSubmissionArtefact>) entry.getElement();
                    ImmutableList.Builder<String> b = ImmutableList.builder();
                    for (Map.Entry<String, CachedSubmissionArtefact> entry1 : artefacts.entrySet()) {
                        b.add(entry1.getValue().getArtefactUuid());
                    }
                    List<String> items = b.build();
                    logger.debug("Expiration: Removing {} unused submission '{}' artefacts from AWS", items.size(), entry.getKey());
                    awsFileStorageService.deleteObject(submissonBucket, items);
                }
            }
        });
        return super.decorate(cache);
    }

    @Override
    protected CacheConfiguration getConfiguration(String name) {
        CacheConfiguration cacheConfig = new CacheConfiguration(name, 500).overflowToDisk(false).
                eternal(false).
                diskPersistent(false).
                timeToLiveSeconds(20 * 60).                
                timeToIdleSeconds(20 * 60);
        
        return cacheConfig;
    }

    /**
     * @return the submissonBucket
     */
    public String getSubmissonBucket() {
        return submissonBucket;
    }

    /**
     * @param submissonBucket the submissonBucket to set
     */
    public void setSubmissonBucket(String submissonBucket) {
        this.submissonBucket = submissonBucket;
    }
}
