package consys.event.conference.core.service.impl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import consys.admin.event.ws.Relation;
import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.aws.AwsFileStorageService.Cache;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.notx.SystemMessage;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ArtefactData;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import consys.event.conference.core.dao.*;
import consys.event.conference.core.notification.ContributionNotification;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import consys.event.conference.core.service.cache.CachedSubmissionArtefact;
import consys.event.conference.core.service.cache.SubmissionArtefactCache;
import consys.event.conference.core.utils.NotificationUtils;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionServiceImpl extends AbstractService implements SubmissionService {

    public static final String TOPIC_DELIMITER = ", ";
    @Autowired
    private SubmissionTypeDao submissionTypeDao;
    @Autowired
    private SubmissionTypeArtefactDao submissionTypeArtefactDao;
    @Autowired
    private SubmissionArtefactTypeDao submissionArtefactTypeDao;
    @Autowired
    private SubmissionDao submissionDao;
    @Autowired
    private SubmissionArtefactDao submissionArtefactDao;
    @Autowired
    private SubmissionArtefactPatternDao patternDao;
    @Autowired
    private SubmissionArtefactTemplateDao templateDao;
    @Autowired
    private ReviewCycleDao reviewCycleDao;
    @Autowired
    private UserEventService userEventService;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private AwsFileStorageService awsFileStorageService;
    @Autowired
    private SubmissionArtefactCache submissionArtefactCache;
    /**
     * AWS S3 bucket
     */
    private String submissonBucket;

    /*
     * ------------------------------------------------------------------------
     * --- L I S T ---
     * ------------------------------------------------------------------------
     */
    /**
     * Vylistuje SubmissionType thumbs ktore su intern=FALSE
     */
    @Override
    public List<SubmissionTypeThumb> listSubmissionTypesThumbs(boolean includePrivate)
            throws NoRecordException {
        List<SubmissionTypeThumb> thumbs;
        if (includePrivate) {
            thumbs = submissionTypeDao.listThumbTypesWithInternalWithDescription();
        } else {
            thumbs = submissionTypeDao.listThumbTypesWithDescription();
        }
        return thumbs;
    }

    @Override
    public List<SubmissionTypeArtefact> listArtefactsWithTypesByUuids(List<String> artefacts) throws NoRecordException {
        if (artefacts == null || artefacts.isEmpty()) {
            return Lists.newArrayList();
        }
        return submissionTypeArtefactDao.listSubmissionArtefacts(artefacts);
    }

    /**
     * Nacita thumby pre vzory pre dany artefakt
     */
    @Override
    public List<ListUuidItem> listArtefactPatterns(Long artefactId) throws RequiredPropertyNullException {
        if (artefactId != null && artefactId <= 0L) {
            throw new RequiredPropertyNullException();
        }
        return patternDao.listPatternsForArtefact(artefactId);
    }

    /**
     * Nacita thumby pre sablony pre dany artefakt
     */
    @Override
    public List<ListUuidItem> listArtefactTemplates(Long artefactId) throws RequiredPropertyNullException {
        if (artefactId != null && artefactId <= 0L) {
            throw new RequiredPropertyNullException();
        }
        return templateDao.listTemplatesForArtefact(artefactId);
    }

    @Override
    public List<SubmissionTypeArtefact> listAllArtefactsForType(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionTypeArtefactDao.listSubmissionArtefactsWithTypesByType(uuid);
    }

    @Override
    public List<Submission> listSubmissionsByIds(List<Long> submissionIds) throws RequiredPropertyNullException {
        if (submissionIds == null) {
            throw new RequiredPropertyNullException();
        }
        return submissionDao.listSubmissionsByIds(submissionIds);
    }

    @Override
    public List<UserEvent> listSubmissionContributors(String submissionUuid) throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(submissionUuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionDao.listSubmissionContributors(submissionUuid);
    }

    @Override
    public List<Long> listSubmissionTypeArtefactsIds(Long submissionTypeId) throws RequiredPropertyNullException {
        if (submissionTypeId == null || submissionTypeId < 1L) {
            throw new RequiredPropertyNullException();
        }
        List<Long> out = submissionTypeArtefactDao.listSubmissionTypeArtefactsIdsByCycles(submissionTypeId);
        if (out.isEmpty()) {
            // nacitame vsetky ake mame
            out = submissionTypeArtefactDao.listSubmissionTypeArtefactsIdsByOrderinSubmissionType(submissionTypeId);
        }
        return out;
    }


    /*
     * ------------------------------------------------------------------------
     * --- L O A D ---
     * ------------------------------------------------------------------------
     */
    @Override
    public SubmissionType loadSubmissionTypeWithArtefactsAndCyclesByUuid(String uuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionTypeDao.loadTypeWithArtefactsAndCyclesByUuid(uuid);
    }

    @Override
    public SubmissionType loadSubmissionTypeWithArtefactTypes(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionTypeDao.loadTypeWithArtefactsTypesByUuid(uuid);
    }

    @Override
    public Submission loadSubmissonByUuid(String uuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionDao.loadByUuid(uuid);
    }

    @Override
    public Submission loadSubmissonWithTypeAndTopicsByUuid(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionDao.loadSubmissonWithTypeAndTopicsByUuid(uuid);
    }

    @Override
    public SubmissionArtefact loadArtefact(String artefactUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(artefactUuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionArtefactDao.loadArtefactWithType(artefactUuid);
    }

    @Override
    public SubmissionType loadSubmissionTypeByUuid(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionTypeDao.loadTypeByUuid(uuid);
    }

    @Override
    public Submission loadSubmissionWithArtefacts(String submissionUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(submissionUuid)) {
            throw new RequiredPropertyNullException();
        }
        return submissionDao.loadSubmissionWithArtefacts(submissionUuid);
    }

    @Override
    public DataFile loadActualDataFileForSubmissionArtefact(String subArtefactUuid) throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException {
        if (StringUtils.isBlank(subArtefactUuid)) {
            throw new RequiredPropertyNullException();
        }
        try {
            if (StringUtils.isBlank(subArtefactUuid)) {
                throw new NoRecordException();
            }
            DataFile df = submissionArtefactDao.loadActualDataFileForArtefact(subArtefactUuid);
            df.setByteData(df.getData().getBytes(1, (int) df.getData().length()));
            return df;
        } catch (SQLException ex) {
            log().error("Error load datafile ", ex);
            throw new ServiceExecutionFailed();
        }
    }

    @Override
    public SubmissionTypeArtefact loadTypeArtefact(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /*
     * ------------------------------------------------------------------------
     * --- C R E A T E ---
     * ------------------------------------------------------------------------
     */
    @Override
    public void createSubmission(Submission s, Set<Invitation> contributors) throws RequiredPropertyNullException, ServiceExecutionFailed {
        // ToDo. vsetkym contributorom by sa mal nastavit do administracie ze su viac ako visitorovia
        if (StringUtils.isBlank(s.getTitle()) || contributors.isEmpty() || s.getType() == null) {
            throw new RequiredPropertyNullException("Missing contributors or type");
        }

        if (StringUtils.isBlank(s.getUuid())) {
            throw new RequiredPropertyNullException("Missing generated UUID");
        }

        // nacitame vsetky otvorene typy artefaktov s testom na prvy cyklus
        List<SubmissionTypeArtefact> activeArtefactTypes = listActiveArtefactTypes(s.getType().getUuid(), true);

        if (activeArtefactTypes == null || activeArtefactTypes.isEmpty()) {
            log().info("No active artefact types. Failed to create {}", s);
            throw new ServiceExecutionFailed();
        }

        //Iterujeme cez vsetky poslane artefakty a postupne vyhadzujeme z listov , rovno ich aj inicializujeme
        Date today = DateProvider.getCurrentDate();
        Map<String, CachedSubmissionArtefact> cachedArtefactTypes = submissionArtefactCache.get(s.getUuid());

        // ak nesedi pocet artefaktov tak exception                                
        if (cachedArtefactTypes != null && cachedArtefactTypes.size() != s.getArtefacts().size()) {
            throw new ServiceExecutionFailed("There are " + cachedArtefactTypes.size() + " uploaded artefacts, but submission contains " + s.getArtefacts().size());
        } else if (cachedArtefactTypes == null && !s.getArtefacts().isEmpty()) {
            throw new ServiceExecutionFailed("There are no uploaded artefacts but, submission contains " + s.getArtefacts().size());
        }


        // Iterujeme cez artefakty a kontrolujeme
        for (SubmissionArtefact userArtefact : s.getArtefacts()) {

            // Test na obsah UUID 
            if (StringUtils.isBlank(userArtefact.getUuid())) {
                throw new RequiredPropertyNullException("Missing submission artefact data file UUID");
            }

            // Test na vytvoreny artefact
            CachedSubmissionArtefact cachedArtefact = cachedArtefactTypes.get(userArtefact.getTypeArtefact().getUuid());
            if (cachedArtefact == null || !cachedArtefact.getArtefactUuid().equalsIgnoreCase(userArtefact.getUuid())) {
                throw new ServiceExecutionFailed("Missing or wrong uploaded artefact for type " + userArtefact.getTypeArtefact() + "; cachedArtefactType: " + cachedArtefact.getArtefactUuid());
            }

            // zistime ci sa artefakt nachadza v optional artefacts
            SubmissionTypeArtefact artefactType = removeFromActiveArtefacts(activeArtefactTypes, userArtefact.getTypeArtefact());
            if (artefactType == null) {
                // ak sa nenachadza v required tak chyba
                log().error("Create {} failed! There is no active artefact type with  '{}'.", s, userArtefact.getTypeArtefact().getArtefactType().getName());
                throw new ServiceExecutionFailed();
            }

            userArtefact.setCreated(today);
            userArtefact.setSubmission(s);
            if (userArtefact.getTypeArtefact().getArtefactType().isFile()) {
                userArtefact.setFileName(cachedArtefact.getFileName());
            }
        }

        // Skontrolujeme ci sa vsetky required artefakty odovzdali
        for (SubmissionTypeArtefact artefact : activeArtefactTypes) {
            if (artefact.isRequired()) {
                log().error("Create {} failed! Missing required artefact '{}'", s, artefact.getArtefactType().getName());
                throw new RequiredPropertyNullException();
            }
        }

        // Vsetko je OK - mozeme nacitat uzivatelov        
        log().debug("Create {}", s);

        List<UserEvent> users = userEventService.createUserOrLoad(new ArrayList<Invitation>(contributors));
        s.setContributors(new HashSet<UserEvent>(users));
        s.setSubmittedDate(DateProvider.getCurrentDate());
        s.setState(SubmissionStateEnum.PENDING);
        doTopicAgregation(s);
        submissionDao.create(s);

        // vsetko prebehlo OK mozeme upravit relacie uzivatelom s pozvanku do eventu
        List<String> changeRelation = Lists.newArrayList();
        for (UserEvent ue : users) {
            if (ue.isSystemUser()) {
                // aktualizujeme jen systemove uzivatele, kteri maji ucet
                changeRelation.add(ue.getUuid());
            }
        }
        userEventService.doUpdateRelationToEvent(Relation.REGISTRED, "", changeRelation);

        // Posleme notifikaciu
    }

    @Override
    public String createSubmissionUuid() {
        return UuidProvider.getUuid();
    }

    @Override
    public String createSubmissionTextArtefact(String submissionUuid, String artefactTypeUuid, String text)
            throws ServiceExecutionFailed, RequiredPropertyNullException {
        ArtefactData dataFile = new ArtefactData();
        try {
            dataFile.setByteData(text.getBytes("UTF-8"));
            dataFile.setContentType("text/plain");
            dataFile.setFile(false);
            dataFile.setFileName(String.format("%s_%s", submissionUuid, artefactTypeUuid));
        } catch (UnsupportedEncodingException ex) {
            log().error("When getting bytes from: " + text, ex);
            throw new ServiceExecutionFailed("When getting bytes from: " + text);
        }

        return createSubmissionArtefact(submissionUuid, artefactTypeUuid, dataFile);
    }

    @Override
    public String createSubmissionArtefact(String submissionUuid, String artefactTypeUuid, ArtefactData dataFile)
            throws ServiceExecutionFailed, RequiredPropertyNullException {

        if (StringUtils.isBlank(submissionUuid) || StringUtils.isBlank(artefactTypeUuid)) {
            throw new RequiredPropertyNullException("Missing submission UUID or artefact type Uuid");
        }

        Map<String, CachedSubmissionArtefact> submissionArtefacts = submissionArtefactCache.get(submissionUuid);
        if (submissionArtefacts == null) {
            submissionArtefacts = Maps.newHashMap();
            submissionArtefactCache.put(submissionUuid, submissionArtefacts);
        }

        // generujeme artefakt uuid
        CachedSubmissionArtefact artefact = submissionArtefacts.get(artefactTypeUuid);
        if (artefact == null) {
            artefact = new CachedSubmissionArtefact(String.format("%s_%s_%s", submissionUuid, artefactTypeUuid, UuidProvider.getUuid()));
        }
        artefact.setFileName(dataFile.getFileName());

        // ulozime do AWS
        try {
            awsFileStorageService.createObject(submissonBucket, artefact.getArtefactUuid(), dataFile.getFileName(), dataFile.getContentType(), Cache.NO_CACHE.getCache(), dataFile.isFile(), new ByteArrayInputStream(dataFile.getByteData()), dataFile.getByteData().length);
            dataFile.setUuid(artefact.getArtefactUuid());
        } catch (Exception e) {
            String msg = String.format("Failed to upload submission artefact for submission:%s artefactType:%s dataFile:%s", submissionUuid, artefactTypeUuid, dataFile);
            log().error(msg, e);
            throw new ServiceExecutionFailed(msg);
        }

        // ulozime do cache
        submissionArtefacts.put(artefactTypeUuid, artefact);

        // vratime nove uuid
        return artefact.getArtefactUuid();
    }

    @Override
    public void refreshSubmissonArtefacts(String submissonUuid) {
        submissionArtefactCache.get(submissonUuid);
    }

    /*
     * ------------------------------------------------------------------------
     * --- U P D A T E ---
     * ------------------------------------------------------------------------
     */
    @Override
    public void approveSubmissionArtefacts(String submissionUuid) {
        log().debug("Approving submission '{}' artefacts in AWS S3", submissionUuid);
        submissionArtefactCache.remove(submissionUuid);
    }

    @Override
    public String updateSubmissionTextArtefact(String submissionUuid, String artefactTypeUuid, String text)
            throws ServiceExecutionFailed, RequiredPropertyNullException, NoRecordException {

        ArtefactData dataFile = new ArtefactData();
        try {
            dataFile.setByteData(text.getBytes("UTF-8"));
            dataFile.setContentType("text/plain");
            dataFile.setFile(false);
            dataFile.setFileName(String.format("%s_%s", submissionUuid, artefactTypeUuid));
        } catch (UnsupportedEncodingException ex) {
            log().error("When getting bytes from: " + text, ex);
            throw new ServiceExecutionFailed("When getting bytes from: " + text);
        }

        return updateSubmissionArtefact(submissionUuid, artefactTypeUuid, dataFile);
    }

    @Override
    public String updateSubmissionArtefact(String submissionUuid, String artefactTypeUuid, ArtefactData dataFile)
            throws RequiredPropertyNullException, NoRecordException, ServiceExecutionFailed {
        if (StringUtils.isBlank(submissionUuid) || StringUtils.isBlank(artefactTypeUuid)) {
            throw new RequiredPropertyNullException("Missing uuids");
        }

        // nacitame submission
        Submission s = submissionDao.loadSubmissionWithArtefacts(submissionUuid);
        SubmissionTypeArtefact artefactType = null;
        SubmissionArtefact submissionArtefact = null;


        // nacitame vsetky otvorene typy artefaktov 
        List<SubmissionTypeArtefact> activeArtefactTypes = listActiveArtefactTypes(s.getType().getUuid(), false);
        if (activeArtefactTypes == null || activeArtefactTypes.isEmpty()) {
            log().info("No active artefact types. Failed to create {}", s);
            throw new ServiceExecutionFailed();
        }

        // ziskame artefact type ktory upravujeme
        for (SubmissionTypeArtefact submissionTypeArtefact : activeArtefactTypes) {
            if (submissionTypeArtefact.getUuid().equalsIgnoreCase(artefactTypeUuid)) {
                artefactType = submissionTypeArtefact;
                break;
            }
        }
        if (artefactType == null) {
            throw new NoRecordException("There is no open artefact type with uuid: " + artefactTypeUuid);
        }

        // Zkontrolujeme ci tu uz taky artefakt nemame ak ano tak ho len nahradime
        for (SubmissionArtefact sa : s.getArtefacts()) {
            if (sa.getTypeArtefact().equals(artefactType)) {
                submissionArtefact = sa;
                break;
            }
        }


        // Vytvorime subor na AWS
        String artefactUuid = createSubmissionArtefact(submissionUuid, artefactTypeUuid, dataFile);


        // novy artefact 
        if (submissionArtefact == null) {
            log().debug("New artefact '{}' for '{}'", artefactUuid, s);
            submissionArtefact = new SubmissionArtefact();
            submissionArtefact.setCreated(DateProvider.getCurrentDate());
            submissionArtefact.setSubmission(s);
            submissionArtefact.setTypeArtefact(artefactType);
            submissionArtefact.setUuid(artefactUuid);
            submissionArtefact.setFileName(dataFile.getFileName());
            s.getArtefacts().add(submissionArtefact);
            submissionDao.update(s);
        } else {
            // upraveny artefact
            log().debug("Update artefact for '{}'", artefactUuid, s);
            submissionArtefact.setUuid(artefactUuid);
            submissionArtefact.setFileName(dataFile.getFileName());
            submissionArtefactDao.update(submissionArtefact);
        }

        return artefactUuid;
    }

    @Override
    public Submission updateSubmissonState(String submissionUuid, SubmissionStateEnum toState) throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(submissionUuid) || toState == null) {
            throw new RequiredPropertyNullException();
        }

        // nacitat s contributorama z dovodu notifikacie
        Submission s = submissionDao.loadSubmissionForNotification(submissionUuid);
        log().debug("Updating state {} to {}", s, toState);
        s.setState(toState);
        submissionDao.update(s);

        // posleme notifikacie
        ContributionNotification notification = null;
        switch (toState) {
            case ACCEPTED:
                notification = new ContributionNotification(s, SystemMessage.CONTRIBUTION_ACCEPTED);
                break;
            case DECLINED:
                notification = new ContributionNotification(s, SystemMessage.CONTRIBUTION_REJECTED);
                break;
        }

        if (notification != null) {
            NotificationUtils.sendNotificationToAllContributors(notification);
        }
        return s;
    }

    /**
     * Update prispevku. Kontroluje sa standardne vsetko povinne + artefacts
     * budu spracovane len tie ktore su otvorene.
     */
    @Override
    public void updateSubmission(
            String uuid,
            String title, String subtitle,
            String authors, Set<Invitation> contributors,
            Collection<SubmissionTopic> topics)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException {
        // Validacia
        if (StringUtils.isBlank(uuid)
                || StringUtils.isBlank(title)
                || StringUtils.isBlank(authors)
                || contributors.isEmpty()) {
            throw new RequiredPropertyNullException();
        }


        Submission s = submissionDao.loadSubmissionWithTopicsAndContributors(uuid);
        s.setTitle(title);
        s.setSubTitle(subtitle);
        s.setAuthors(authors);

        s.getTopics().clear();
        for (SubmissionTopic submissionTopic : topics) {
            s.getTopics().add(submissionTopic);
        }
        doTopicAgregation(s);

        // Ak su nejaky novy pouzivateliatak sa nacitaju ak niesu tak sa nechaju tak.        
        List<UserEvent> users = userEventService.createUserOrLoad(new ArrayList<Invitation>(contributors));
        s.setContributors(new HashSet<UserEvent>(users));

        log().debug("Update {}", s);
        submissionDao.update(s);

        // vsetko prebehlo OK mozeme upravit relacie uzivatelom s pozvanku do eventu
        List<String> changeRelation = Lists.newArrayList();
        for (UserEvent ue : users) {
            if (ue.isSystemUser()) {
                changeRelation.add(ue.getUuid());
            }
        }
        userEventService.doUpdateRelationToEvent(Relation.REGISTRED, "", changeRelation);

        // Vygenerovat pridanym contributorom notifikaciu
    }

    @Override
    public void deleteSubmisson(String uuid) throws NoRecordException, RequiredPropertyNullException {

        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException("Missing submission uuid");
        }

        Submission s = submissionDao.loadByUuid(uuid);
        log().debug("Mark {} to state DELETED", s);
        s.setState(SubmissionStateEnum.DELETED);
        submissionDao.update(s);
    }

    @Override
    public void deleteAllCachedSubmissionArtefacts(String submissionUuid) throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(submissionUuid)) {
            throw new RequiredPropertyNullException("Missing submission uuid");
        }
        Map<String, CachedSubmissionArtefact> artefacts = (Map<String, CachedSubmissionArtefact>) submissionArtefactCache.remove(submissionUuid);
        ImmutableList.Builder<String> b = ImmutableList.builder();
        for (Map.Entry<String, CachedSubmissionArtefact> entry1 : artefacts.entrySet()) {
            b.add(entry1.getValue().getArtefactUuid());
        }

        List<String> artefactsUuids = b.build();
        if (!artefactsUuids.isEmpty()) {
            log().debug("Deleting {} uploaded artefacts of submission '{}'", artefactsUuids.size(), submissionUuid);
            awsFileStorageService.deleteObject(submissonBucket, artefactsUuids);
        }
    }

    @Override
    public void deleteCachedSubmissionArtefact(String submissionUuid, String artefactTypeUuid, String artefactUuid) throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(submissionUuid) || StringUtils.isBlank(artefactTypeUuid) || StringUtils.isBlank(artefactUuid)) {
            throw new RequiredPropertyNullException("Missing submission uuid");
        }
        Map<String, CachedSubmissionArtefact> artefacts = (Map<String, CachedSubmissionArtefact>) submissionArtefactCache.get(submissionUuid);
        boolean canBeDeleted = false;
        for (Map.Entry<String, CachedSubmissionArtefact> entry1 : artefacts.entrySet()) {
            if (entry1.getKey().equals(artefactTypeUuid) && entry1.getValue().getArtefactUuid().equals(artefactUuid)) {
                canBeDeleted = true;
                break;
            }
        }
        if (canBeDeleted) {
            log().debug("Delete cached submission artefact");
            awsFileStorageService.deleteObject(submissonBucket, artefactUuid);
            artefacts.remove(artefactUuid);
        }

    }

    /*
     * ------------------------------------------------------------------------
     * --- H E L P M E T H O D S ---
     * ------------------------------------------------------------------------
     */
    /**
     * Vrati zoznam aktivnych artefaktov z listu recenzencnych cyklov.
     *
     * @param allCyclesForType cykly z ktorych sa budu artefakty nacitavat
     * @param againstDay datum ktory musi byt v intervale cyklu
     * @return
     */
    private static List<SubmissionTypeArtefact> filterActiveArtefacts(List<ReviewCycle> allCyclesForType, Date againstDay) {
        List<SubmissionTypeArtefact> open = Lists.newArrayList();
        for (ReviewCycle c : allCyclesForType) {
            // ak je cyklus
            if (DateUtils.isDateInRange(againstDay, c.getSubmitFrom(), c.getSubmitTo())) {
                open.addAll(c.getArtefacts());
            }
        }
        return open;
    }

    /**
     * Najde v zozname typov artefaktov artefakt a odstraniho zo zoznamu
     */
    private SubmissionTypeArtefact removeFromActiveArtefacts(List<SubmissionTypeArtefact> inList, SubmissionTypeArtefact artefact) {
        SubmissionTypeArtefact out = null;
        for (int i = 0; i < inList.size(); i++) {
            SubmissionTypeArtefact t = inList.get(i);
            if (t.equals(artefact)) {
                out = inList.remove(i);
                break;
            }
        }
        return out;
    }

    /**
     * Pomocna
     */
    private List<SubmissionTypeArtefact> listActiveArtefactTypes(String typeUuid, boolean firstCycleTest) throws ServiceExecutionFailed, RequiredPropertyNullException {
        Date now = DateProvider.getCurrentDate();
        try {
            // Natiahneme si cykly s artefaktami a testujeme na prvy cyklus
            List<ReviewCycle> allCyclesForType = reviewService.listReviewCyclesForType(typeUuid);
            log().debug("Filtering artefact types for type '{}' with test for frist cycle '{}'.", typeUuid, firstCycleTest);
            // test na aktualnost prveho cyklu
            if (firstCycleTest) {
                // test ci je prvy cyklus aktivny, ak nie je tak hned service failed.
                ReviewCycle firstCycle = allCyclesForType.get(0);
                if (!DateUtils.isDateInRange(now, firstCycle.getSubmitFrom(), firstCycle.getSubmitTo())) {
                    log().error("Create submisson failed! First review cycle closed!");
                    throw new ServiceExecutionFailed();
                }
            }
            return filterActiveArtefacts(allCyclesForType, now);
        } catch (NoRecordException ex) {
            log().debug("Artefact types are not assigned into review cycles. Returning all available artefacts for type '{}'", typeUuid);
            // Submission nema nastavene cykly takze si nacitame vsetky artefakty ktore sa daju postnut.            
            try {
                return submissionTypeArtefactDao.listSubmissionArtefactsWithTypesByType(typeUuid);
            } catch (NoRecordException ex1) {
                log().error("Error load artefact type by UUID? " + typeUuid);
                throw new ServiceExecutionFailed();
            }
        }
    }

    protected void doTopicAgregation(Submission s) {
        // Vytvorime agregaciu topikov        
        StringBuilder sb = new StringBuilder();
        Iterator<SubmissionTopic> st = s.getTopics().iterator();
        for (int index = 1, size = s.getTopics().size(); st.hasNext(); index++) {
            SubmissionTopic t = st.next();
            sb.append(t.getName());
            if (index < size) {
                sb.append(TOPIC_DELIMITER);
            }
        }
        s.setAgregatedTopics(sb.toString());
    }

    /*
     * ========================================================================
     * ==== DATA PRO GENEROVANI PROFILU ====
     * ========================================================================
     */
    @Override
    public Date loadSubmissionBegindSend() {
        return reviewCycleDao.loadFirstSubmissionDate();
    }

    @Override
    public Date loadSubmissionEndSend() {
        return reviewCycleDao.loadLastSubmissionDate();
    }

    /**
     * AWS S3 bucket
     *
     * @return the submissonBucket
     */
    public String getSubmissonBucket() {
        return submissonBucket;
    }

    /**
     * AWS S3 bucket
     *
     * @param submissonBucket the submissonBucket to set
     */
    public void setSubmissonBucket(String submissonBucket) {
        this.submissonBucket = submissonBucket;
    }
}
