package consys.event.conference.core.bo.thumb;

import java.util.Date;

/**
 * Thumb pre review criteria
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewCycleThumb {

    private Long id;
    private Date from;
    private Date to;

    public ReviewCycleThumb(Long id, Date from, Date to) {
        this.id = id;
        this.from = from;
        this.to = to;
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }


    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

   
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

}
