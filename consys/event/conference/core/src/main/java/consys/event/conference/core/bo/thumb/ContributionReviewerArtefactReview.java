/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.bo.thumb;

/**
 * Recenzia artefaktu
 * @author palo
 */
public class ContributionReviewerArtefactReview {
        
    private String artefactUuid;   
    private Long artefactTypeId;
    private int score;

    public ContributionReviewerArtefactReview(String artefactUuid, Long artefactTypeId, int score) {
        this.artefactUuid = artefactUuid;
        this.artefactTypeId = artefactTypeId;
        this.score = score;
    }

    public Long getArtefactTypeId() {
        return artefactTypeId;
    }       
    
    /**
     * @return the artefactUuid
     */
    public String getArtefactUuid() {
        return artefactUuid;
    }

    /**
     * @param artefactUuid the artefactUuid to set
     */
    public void setArtefactUuid(String artefactUuid) {
        this.artefactUuid = artefactUuid;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }
    
    
    
}
