package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.core.bo.ReviewerBid;
import consys.event.conference.core.bo.thumb.ReviewerBidListItem;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewerBidDao extends GenericDao<ReviewerBid> {

    public List<ReviewerBidListItem> listReviewerBids(Constraints constraints, Paging paging, boolean showAuthors);

    public int loadParticipantsCount(Constraints constraints);

    public ReviewerBid loadBidFor(String submissionUuid, String reviewerUuid) throws NoRecordException;

    void deleteAllBids();
}
