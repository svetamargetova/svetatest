package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.SubmissionArtefactPattern;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionArtefactPatternDao extends GenericDao<SubmissionArtefactPattern> {

    public List<ListUuidItem> listPatternsForArtefact(Long id);

    public List<ListUuidItem> listPatternsThumbs() throws NoRecordException;

    public SubmissionArtefactPattern loadByDataFileUuid(String uuid) throws NoRecordException;

    public List<SubmissionArtefactPattern> listPatternsByUuids(List<String> uuids) throws NoRecordException;
}
