package consys.event.conference.core.service;

import consys.event.common.core.service.list.ListService;
import consys.event.conference.core.bo.thumb.SubmissionListItem;


/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ContributionsListService extends ListService<SubmissionListItem, SubmissionListItem> {
}
