package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import consys.event.conference.core.bo.Submission;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionDao extends GenericDao<Submission> {

    /**
     * Nacita len prvu uroven objektu. Nic nedotahuje
     */
    public Submission loadByUuid(String uuid) throws NoRecordException;

    public Submission loadSubmissonWithTypeAndTopicsByUuid(String uuid) throws NoRecordException;

    public Submission loadSubmissionWithArtefacts(String uuid) throws NoRecordException;

    /**
     * Nacita prispevok s contributorami a typom.
     * 
     * @param uuid
     * @return
     * @throws NoRecordException 
     */
    public Submission loadSubmissionForNotification(String uuid) throws NoRecordException;

    public Submission loadSubmissionWithTopicsAndContributors(String uuid) throws NoRecordException;

    public List<String> listCoauthors(Long submissionId);

    /** nacte seznam submission podle zadanych id */
    public List<Submission> listSubmissionsByIds(List<Long> ids);

    public List<UserEvent> listSubmissionContributors(String submissionUuid) throws NoRecordException;

    void deleteAllSubmissions();
}
