package consys.event.conference.core.service;

import consys.event.common.core.service.list.ListService;
import consys.event.conference.core.bo.thumb.ReviewerBidListItem;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface BidForReviewListService extends ListService<ReviewerBidListItem, ReviewerBidListItem> {
}
