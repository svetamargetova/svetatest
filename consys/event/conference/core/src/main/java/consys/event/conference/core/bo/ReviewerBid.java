package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.event.common.core.bo.UserEvent;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerBid implements ConsysObject{
    private static final long serialVersionUID = 3403105092278778933L;

    private UserEvent reviewer;
    private Submission submission;
    private ReviewerBidInterestEnum interest;

    /**
     * @return the reviewer
     */
    public UserEvent getReviewer() {
        return reviewer;
    }

    /**
     * @param reviewer the reviewer to set
     */
    public void setReviewer(UserEvent reviewer) {
        this.reviewer = reviewer;
    }

    /**
     * @return the submission
     */
    public Submission getSubmission() {
        return submission;
    }

    /**
     * @param submission the submission to set
     */
    public void setSubmission(Submission submission) {
        this.submission = submission;
    }

    /**
     * @return the interest
     */
    public ReviewerBidInterestEnum getInterest() {
        return interest;
    }

    /**
     * @param interest the interest to set
     */
    public void setInterest(ReviewerBidInterestEnum interest) {
        this.interest = interest;
    }

      @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ReviewerBid e = (ReviewerBid) obj;
        return submission.equals(e.submission) && reviewer.equals(e.reviewer);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (this.reviewer != null ? this.reviewer.hashCode() : 0);
        hash = 47 * hash + (this.submission != null ? this.submission.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("ReviewerBid[reviewer=%s submission=%s interest=%s]", reviewer,submission,interest.toString());
    }
}
