package consys.event.conference.core.service.impl;

import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.UuidProvider;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.core.context.PropertiesChangedEvent;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.bo.ReviewCriteriaItem;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionArtefactPattern;
import consys.event.conference.core.bo.SubmissionArtefactTemplate;
import consys.event.conference.core.bo.SubmissionArtefactType;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.bo.thumb.ReviewCycleThumb;
import consys.event.conference.core.bo.thumb.SubmissionTypeArtefactThumb;
import consys.event.conference.core.dao.ReviewCriteriaDao;
import consys.event.conference.core.dao.ReviewCycleDao;
import consys.event.conference.core.dao.SubmissionArtefactPatternDao;
import consys.event.conference.core.dao.SubmissionArtefactTemplateDao;
import consys.event.conference.core.dao.SubmissionArtefactTypeDao;
import consys.event.conference.core.dao.SubmissionTypeArtefactDao;
import consys.event.conference.core.dao.SubmissionTypeDao;
import consys.event.conference.core.exception.ArtefactInDifferentCycleException;
import consys.event.conference.core.exception.DeleteCriteriaException;
import consys.event.conference.core.exception.DeleteCycleException;
import consys.event.conference.core.exception.DeleteRelationsException;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.service.SubmissionSettingsService;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionSettingsServiceImpl extends AbstractService implements SubmissionSettingsService, ApplicationEventPublisherAware {

    @Autowired
    private SubmissionTypeDao submissionTypeDao;
    @Autowired
    private SubmissionTypeArtefactDao submissionTypeArtefactDao;
    @Autowired
    private SubmissionArtefactTypeDao submissionArtefactTypeDao;
    @Autowired
    private ReviewCycleDao reviewCycleDao;
    @Autowired
    private ReviewCriteriaDao reviewCriteriaDao;
    @Autowired
    private SubmissionArtefactPatternDao artefactPatternDao;
    @Autowired
    private SubmissionArtefactTemplateDao artefactTemplateDao;
    @Autowired
    private AwsFileStorageService awsFileStorageService;
    // publisher    
    private ApplicationEventPublisher publisher;
    // data
    /**
     * AWS S3 bucket
     */
    private String submissonBucket;

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * --- L I S T ---
     */
    /*
     * ------------------------------------------------------------------------
     */
    /**
     * Vylistovanie thumbov typov prispevkov
     */
    @Override
    public List<ListUuidItem> listSettingsThumbTypes() throws NoRecordException {
        if (log().isDebugEnabled()) {
            log().debug("List types in thumbs");
        }
        return submissionTypeDao.listSettingsThumbTypes();
    }

    /**
     * Vylistovanie thumbov vsetkych sablon
     */
    @Override
    public List<ListUuidItem> listTemplatesThumbs() throws NoRecordException {
        return artefactTemplateDao.listTemplatesThumbs();
    }

    /**
     * Vylistovanie thumbov vsetkych vzorov
     */
    @Override
    public List<ListUuidItem> listPatternsThumbs() throws NoRecordException {
        return artefactPatternDao.listPatternsThumbs();
    }

    /**
     * Vylistovanie vsetkych vzorov podla vlozneych uuid vzorov
     *
     * @param uuids zoznam uuid vzorov
     */
    @Override
    public List<SubmissionArtefactPattern> listPatternsByUuids(List<String> uuids) throws NoRecordException {
        return artefactPatternDao.listPatternsByUuids(uuids);
    }

    /**
     * Vylistovanie vsetkych sablon podla vlozneych uuid sablon
     *
     * @param uuids zoznam uuid sablon
     */
    @Override
    public List<SubmissionArtefactTemplate> listTemplateByUuids(List<String> uuids) throws NoRecordException {
        return artefactTemplateDao.listTemplatesByUuids(uuids);
    }

    /**
     * Vylistovanie artefaktov podla UUID artefaktov.
     */
    @Override
    public List<SubmissionTypeArtefact> listArtefactsInList(List<String> artefacts) throws NoRecordException {
        return submissionTypeArtefactDao.listSubmissionArtefacts(artefacts);
    }
    /*
     * ------------------------------------------------------------------------
     */
    /*
     * --- L O A D ---
     */
    /*
     * ------------------------------------------------------------------------
     */

    @Override
    public SubmissionTypeArtefactThumb loadTypeArtefactThumbByUuid(String uuid) throws NoRecordException {
        SubmissionTypeArtefactThumb thumb = new SubmissionTypeArtefactThumb();

        SubmissionTypeArtefact typeArtefact = submissionTypeArtefactDao.loadArtefactWithTypeByUuid(uuid);
        SubmissionArtefactType artefactType = typeArtefact.getArtefactType();
        thumb.setDescription(artefactType.getDescription());
        thumb.setFile(artefactType.isFile());
        thumb.setFormats(artefactType.getFormats());
        thumb.setName(artefactType.getName());
        thumb.setNumOfReviewers(typeArtefact.getNumOfReviewers());
        thumb.setRequired(typeArtefact.isRequired());
        thumb.setInputLength(artefactType.getInputLength());
        // Review cycles
        List<ReviewCycleThumb> cycleThumbs = reviewCycleDao.listCycleThumbForType(uuid);
        thumb.setCycles(cycleThumbs);
        // Criterions
        List<ListLongItem> criteriaThumbs = reviewCriteriaDao.listCriteriaThumbsForType(uuid);
        thumb.setCriterions(criteriaThumbs);
        // Patterns
        List<ListUuidItem> patternsThumbs = artefactPatternDao.listPatternsForArtefact(artefactType.getId());
        thumb.setPatterns(patternsThumbs);
        // Templates
        List<ListUuidItem> templatesThumbs = artefactTemplateDao.listTemplatesForArtefact(artefactType.getId());
        thumb.setTemplates(templatesThumbs);

        return thumb;
    }

    /**
     * Nacita SubmissionTypeArtefact + ArtefactType
     */
    @Override
    public SubmissionTypeArtefact loadTypeArtefactWithArtefactByUuid(String uuid) throws NoRecordException {
        return submissionTypeArtefactDao.loadArtefactWithTypeByUuid(uuid);
    }

    @Override
    public ReviewCycle loadReviewCycleWithTypeById(Long id) throws NoRecordException {
        return reviewCycleDao.load(id);
    }

    @Override
    public ReviewCriteria loadReviewCriteria(Long id) throws NoRecordException {
        return reviewCriteriaDao.loadCriteriaWithItems(id);
    }

    @Override
    public SubmissionArtefactPattern loadPatternByUuid(String uuid) throws NoRecordException {
        return artefactPatternDao.loadByDataFileUuid(uuid);
    }

    @Override
    public SubmissionArtefactTemplate loadTemplateByUuid(String uuid) throws NoRecordException {
        return artefactTemplateDao.loadByDataFileUuid(uuid);
    }

    @Override
    public List<String> listNotAssignedArtefactsUuids(Long typeId) {
        return submissionTypeArtefactDao.listNotAssignedArtefactsUuids(typeId);

    }


    /*
     * ------------------------------------------------------------------------
     */
    /*
     * --- C R E A T E ---
     */
    /*
     * ------------------------------------------------------------------------
     */
    /**
     * Vytvorenie Typu prispevku
     */
    @Override
    public void createSubmissionType(SubmissionType type) throws RequiredPropertyNullException, SubmissionTypeNotUniqueException {
        if (log().isDebugEnabled()) {
            log().debug("Create " + type);
        }

        if (StringUtils.isBlank(type.getName())) {
            if (log().isDebugEnabled()) {
                log().debug("Submission Type name NULL! ");
            }
            throw new RequiredPropertyNullException();
        }
        try {
            int order = submissionTypeDao.loadTypesCount();
            type.setUuid(UuidProvider.getUuid());
            type.setOrder(order);
            submissionTypeDao.create(type);
        } catch (ConstraintViolationException ex) {
            if (log().isDebugEnabled()) {
                log().debug("SubmissionType with name " + type.getName() + " already registred. Throwing SubmissionTypeNotUniqueException");
            }
            throw new SubmissionTypeNotUniqueException();
        }

        notifyAboutChangedEventProperties();
    }

    /**
     * Vytvorenie niekolkych typov prispevku
     */
    @Override
    public void createSubmissionTypes(List<SubmissionType> types) throws RequiredPropertyNullException, SubmissionTypeNotUniqueException {
        int order = submissionTypeDao.loadTypesCount();

        for (SubmissionType type : types) {
            if (log().isDebugEnabled()) {
                log().debug("Create " + type);
            }

            if (StringUtils.isBlank(type.getName())) {
                if (log().isDebugEnabled()) {
                    log().debug("Submission Type name NULL! ");
                }
                throw new RequiredPropertyNullException();
            }
            try {
                type.setUuid(UuidProvider.getUuid());
                type.setOrder(order++);
                submissionTypeDao.create(type);
            } catch (ConstraintViolationException ex) {
                if (log().isDebugEnabled()) {
                    log().debug("SubmissionType with name " + type.getName() + " already registred. Throwing SubmissionTypeNotUniqueException");
                }
                throw new SubmissionTypeNotUniqueException();
            }
        }

        notifyAboutChangedEventProperties();
    }

    /**
     * Vytvorenie typov artefaktov k typu prispevku
     */
    @Override
    public void createSubmissionArtefactTypes(String submissionTypeUuid, List<SubmissionTypeArtefact> artefacts) throws RequiredPropertyNullException, NoRecordException {
        SubmissionType type = submissionTypeDao.loadTypeWithArtefactsAndCyclesByUuid(submissionTypeUuid);
        int index = type.getArtefacts().size();
        for (SubmissionTypeArtefact artefact : artefacts) {
            if (StringUtils.isBlank(artefact.getArtefactType().getName())) {
                throw new RequiredPropertyNullException();
            }

            if (log().isDebugEnabled()) {
                log().debug("Create " + artefact + " for uuid " + submissionTypeUuid);
            }

            // Najskor vytvorime samotny Artefact Type
            submissionArtefactTypeDao.create(artefact.getArtefactType());


            // Potom vytvorime relacnu tabulku SUBMISSION_TYPE <-> SUBMISSION_TYPE_ARTEFACT <-> ARTEFACT_TYPE            
            artefact.setType(type);
            artefact.setUuid(UuidProvider.getUuid());
            submissionTypeArtefactDao.create(artefact);
            // Update submission typu
            type.getArtefacts().add(artefact);
        }
        submissionTypeDao.update(type);
    }

    /**
     * Vytvorenie recenzencnych kriterii pre typ artefaktu - kontroluje ci
     * prispevok nie je nahodou automaticky prijatelny
     */
    @Override
    public void createReviewCriterias(String artefactUuid, List<ReviewCriteria> criterias) throws NoRecordException, RequiredPropertyNullException {
        SubmissionTypeArtefact artefact = submissionTypeArtefactDao.loadArtefactByUuid(artefactUuid);

        for (ReviewCriteria c : criterias) {
            if (StringUtils.isBlank(c.getName())) {
                throw new RequiredPropertyNullException();
            }
            if (log().isDebugEnabled()) {
                log().debug("Create {}", c);
            }
            // test na nastavenie reviewcriteria
            if (!c.getItems().isEmpty()) {
                int max = c.getItems().get(0).getValue();
                int min = max;
                for (ReviewCriteriaItem item : c.getItems()) {
                    if (StringUtils.isBlank(item.getName())) {
                        throw new RequiredPropertyNullException();
                    }
                    item.setCriteria(c);
                    min = min > item.getValue() ? item.getValue() : min;
                    max = max < item.getValue() ? item.getValue() : max;
                }
                c.setMinPoints(min);
                c.setMaxPoints(max);
            }
            c.setSubmissionTypeArtefact(artefact);

            reviewCriteriaDao.create(c);
        }
    }

    /**
     * Vytvornenie recenzencnych/submitacnych cyklov
     */
    @Override
    public void createReviewCycles(String submissionType, List<ReviewCycle> cycles) throws NoRecordException, RequiredPropertyNullException {
        SubmissionType artefact = submissionTypeDao.loadTypeByUuid(submissionType);

        for (ReviewCycle c : cycles) {
            if (c.getSubmitFrom() == null || c.getSubmitTo() == null) {
                throw new RequiredPropertyNullException();
            }
            if (log().isDebugEnabled()) {
                log().debug("For " + artefact.getName() + "Create " + c);
            }
            c.setSubmissionType(artefact);
            artefact.getCycles().add(c);
        }

        notifyAboutChangedEventProperties();
    }

    /**
     * Vytvorenie vzoru artefaktu
     */
    @Override
    public void createSubmissionPattern(SubmissionArtefactPattern pattern, DataFile dataFile) throws RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(pattern.getTitle())) {
            throw new RequiredPropertyNullException();
        }
        testDataFile(dataFile);

        log().debug("Create {}", pattern);
        pattern.setUuid(String.format("patt-%s", UuidProvider.getUuid()));
        artefactPatternDao.create(pattern);

        // ulozime do AWS
        try {
            awsFileStorageService.createObject(submissonBucket, pattern.getUuid(), dataFile.getFileName(), dataFile.getContentType(), AwsFileStorageService.Cache.NO_CACHE.getCache(), true, new ByteArrayInputStream(dataFile.getByteData()), dataFile.getByteData().length);
        } catch (Exception e) {
            String msg = String.format("Failed to upload pattern %s using %s", pattern, dataFile);
            log().error(msg, e);
            throw new ServiceExecutionFailed(msg);
        }

    }

    /**
     * Vytvorenie sablony artefaktu
     */
    @Override
    public void createSubmissionTemplate(SubmissionArtefactTemplate template, DataFile dataFile) throws RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(template.getTitle())) {
            throw new RequiredPropertyNullException();
        }
        testDataFile(dataFile);



        log().debug("Create {}", template);
        template.setUuid(String.format("temp-%s", UuidProvider.getUuid()));
        artefactTemplateDao.create(template);
        // ulozime na AWS S3
        try {
            awsFileStorageService.createObject(submissonBucket, template.getUuid(), dataFile.getFileName(), dataFile.getContentType(), AwsFileStorageService.Cache.NO_CACHE.getCache(), true, new ByteArrayInputStream(dataFile.getByteData()), dataFile.getByteData().length);
        } catch (Exception e) {
            String msg = String.format("Failed to upload template %s using %s", template, dataFile);
            log().error(msg, e);
            throw new ServiceExecutionFailed(msg);
        }
    }

    private void testDataFile(DataFile dataFile) throws RequiredPropertyNullException {
        if (dataFile == null || StringUtils.isBlank(dataFile.getContentType())
                || StringUtils.isBlank(dataFile.getFileName()) || dataFile.getByteData() == null
                || dataFile.getByteData().length == 0) {
            log().error("{} is missing required properties.", dataFile);
            throw new RequiredPropertyNullException();
        }
    }

    /*
     * ------------------------------------------------------------------------
     * --- U P D A T E ---
     * ------------------------------------------------------------------------
     */
    @Override
    public void updateReviewCriteria(ReviewCriteria criteria) throws NoRecordException, RequiredPropertyNullException, DeleteCriteriaException {
        if (log().isDebugEnabled()) {
            log().debug("Update " + criteria);
        }

        if (StringUtils.isBlank(criteria.getName())) {
            if (log().isDebugEnabled()) {
                log().debug("Missing name!");
            }
            throw new RequiredPropertyNullException();
        }
        try {
            if (!criteria.getItems().isEmpty()) {
                int max = criteria.getItems().get(0).getValue();
                int min = max;
                for (ReviewCriteriaItem item : criteria.getItems()) {
                    if (StringUtils.isBlank(item.getName())) {
                        throw new RequiredPropertyNullException();
                    }
                    item.setCriteria(criteria);
                    min = min > item.getValue() ? item.getValue() : min;
                    max = max < item.getValue() ? item.getValue() : max;
                }
                criteria.setMinPoints(min);
                criteria.setMaxPoints(max);
            }
            reviewCriteriaDao.update(criteria);
        } catch (ConstraintViolationException e) {
            throw new DeleteCriteriaException();
        }
    }

    @Override
    public void updateSubmissionType(SubmissionType type) throws RequiredPropertyNullException, SubmissionTypeNotUniqueException {
        if (StringUtils.isBlank(type.getName())) {
            if (log().isDebugEnabled()) {
                log().debug("Submission Type name NULL! ");
            }
            throw new RequiredPropertyNullException();
        }
        try {
            submissionTypeDao.update(type);
        } catch (ConstraintViolationException ex) {
            if (log().isDebugEnabled()) {
                log().debug("SubmissionType with name " + type.getName() + " already registred. Throwing SubmissionTypeNotUniqueException");
            }
            throw new SubmissionTypeNotUniqueException();
        }

        notifyAboutChangedEventProperties();
    }

    @Override
    public void updateReviewCycle(ReviewCycle cycle, List<String> artefacts) throws RequiredPropertyNullException, NoRecordException, ArtefactInDifferentCycleException, ServiceExecutionFailed {
        if (cycle.getSubmitFrom() == null || cycle.getSubmitTo() == null || cycle.getSubmissionType() == null) {
            throw new RequiredPropertyNullException();
        }
        /*
         * Musime zistit ci nevzniken duplikacia artefaktu
         */
        cycle.getArtefacts().clear();

        log().debug("Update {} with artefacts {}", cycle, artefacts);

        // Zoznam id review cyclov pre vsetky artefakty
        if (CollectionUtils.isNotEmpty(artefacts)) {
            Map<String, Long> artefactsInCycles = reviewCycleDao.listReviewCycleIdsFor(cycle.getSubmissionType().getUuid(), artefacts);
            // Vsetky artefakty ktore sa maju pridat musia byt v tomto cykle alebo nemaju este ziadny cyklus
            for (Map.Entry<String, Long> item : artefactsInCycles.entrySet()) {
                if (!item.getValue().equals(cycle.getId())) {
                    throw new ArtefactInDifferentCycleException(cycle.getId() + " <> " + item.getValue());
                }
            }

            // Nacitame si teda vsetky artefact a nastavime ich
            List<SubmissionTypeArtefact> lartefacts = submissionTypeArtefactDao.listSubmissionArtefacts(artefacts);
            for (String uuid : artefacts) {
                for (SubmissionTypeArtefact artefact : lartefacts) {
                    if (uuid.equalsIgnoreCase(artefact.getUuid())) {
                        cycle.getArtefacts().add(artefact);
                    }
                }
            }
        }

        // Test datumov.
        if (!cycle.getSubmitFrom().before(cycle.getSubmitTo())) {
            throw new RequiredPropertyNullException();
        }
        if (cycle.getReviewFrom() != null && cycle.getReviewTo() != null) {
            if (!cycle.getReviewFrom().before(cycle.getReviewTo())) {
                throw new RequiredPropertyNullException();
            }
        }
        reviewCycleDao.update(cycle);

        notifyAboutChangedEventProperties();
    }

    @Override
    public void updateSubmissionArtefactType(SubmissionTypeArtefact artefact, List<String> templates, List<String> patterns)
            throws NoRecordException, RequiredPropertyNullException, DeleteCriteriaException {
        updateArtefactType(artefact.getArtefactType(), templates, patterns);
        updateSubmissionTypeArtefact(artefact);
    }

    /**
     * Uprava artefaktu prispevku
     *
     */
    private void updateSubmissionTypeArtefact(SubmissionTypeArtefact artefact) throws NoRecordException, DeleteCriteriaException {
        // Ak je autoaccept musia sa zrusit vsetky review kriteria
        if (artefact.isAutomaticallyAccepted()) {
            try {
                List<ReviewCriteria> criterias = reviewCriteriaDao.listCriteriaForArtefactType(artefact.getUuid());
                log().debug("Artefact is autoaccepted now thus all review criterias have to be removed.");
                for (ReviewCriteria criteria : criterias) {
                    deleteReviewCriteria(criteria.getId());
                }
            } catch (NoRecordException nre) {
            }
        }

        // Update typ        
        log().debug("Update {}", artefact);
        submissionTypeArtefactDao.update(artefact);
    }

    private void updateArtefactType(SubmissionArtefactType artefactType, List<String> templates, List<String> patterns)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(artefactType.getName())) {
            log().debug("Missing artefact type name!");
            throw new RequiredPropertyNullException();
        }

        // Update typ artefaktu        
        log().debug("Update Artefact Type {}", artefactType);
        // Update typu artefaktu a merge zo session
        submissionArtefactTypeDao.update(artefactType);

        // Ak sa tu nachadzaju templaty alebo patterny tak musime vykonat znovu update
        boolean updateTypeAgain = false;

        // Vlozenie templates
        if (templates != null) {
            log().debug("Update templates...");
            artefactType.getTemplates().clear();
            if (!templates.isEmpty()) {
                artefactType.getTemplates().addAll(listTemplateByUuids(templates));
            }
            updateTypeAgain = true;
        }

        // Vlozenie patterns
        if (patterns != null) {
            log().debug("Update patterns...");
            artefactType.getPatterns().clear();
            if (!patterns.isEmpty()) {
                artefactType.getPatterns().addAll(listPatternsByUuids(patterns));
            }
            updateTypeAgain = true;
        }

        /*
         * Nevykona SQL Update ale vytvori len nove zaznamy relacnej tabulky
         * Artefact_Type <-- Artefact_XX --> XX.
         */
        if (updateTypeAgain) {
            submissionArtefactTypeDao.update(artefactType);
        }
    }

    @Override
    public void updateSubmissionTypeOrder(String typeUuid, int oldPosition, int newPosition)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(typeUuid) || oldPosition < 0 || newPosition < 0) {
            throw new RequiredPropertyNullException("Missing typeUUID or positions are negative");
        }

        if (submissionTypeDao.checkSubmissionTypePosition(typeUuid, oldPosition)) {
            submissionTypeDao.updateSubmissionTypeOrder(typeUuid, oldPosition, newPosition);
        } else {
            throw new NoRecordException("There is no submission type with uuid " + typeUuid + " on position " + oldPosition);
        }
    }


    /*
     * ------------------------------------------------------------------------
     */
    /*
     * --- D E L E T E ---
     */
    /*
     * ------------------------------------------------------------------------
     */
    /**
     * Zmazanie typu prispevku. System musi zmazat: - SubmissionTypeArtefact -
     * SubmissionArtefactType
     *
     * Ak existuju nejake relacie tak sa musi vyhodit vynimka
     *
     * @throws DeleteRelationsException ak existuju relacie na nejaku cast
     * @throws NoRecordException ak neexistuje submissiontype s vlozenym uuid
     */
    @Override
    public void deleteSubmissionType(String typeUuid) throws NoRecordException, DeleteRelationsException {

        SubmissionType type = submissionTypeDao.loadTypeWithArtefactsAndCyclesByUuid(typeUuid);
        log().debug("Delete {}", type);
        for (SubmissionTypeArtefact artefact : type.getArtefacts()) {
            // najskor si ulozime referncui
            SubmissionArtefactType artefactType = artefact.getArtefactType();
            // zmazeme submission type artefact
            try {
                // ToDo - delete registration cycles
                // ToDo - delete criteria
                submissionTypeArtefactDao.delete(artefact);
            } catch (ConstraintViolationException cve) {
                log().debug("Delete {} failed. Still has references", artefact);
                throw new DeleteRelationsException();
            }

            // zmazeme submission artefact type
            try {
                submissionArtefactTypeDao.delete(artefactType);
            } catch (ConstraintViolationException cve) {
                log().debug("Delete {} failed. Still has references", artefactType);
                throw new DeleteRelationsException();
            }
        }
        submissionTypeDao.delete(type);

        notifyAboutChangedEventProperties();
    }

    /**
     * Odstrani artefakt z typu prispevku. Automaticky maze - zmaze sa z cyklu v
     * ktorom je - zmaze Criteria - Artefact type - ak je jeho referencia len
     * jedna, ak ju ma nejaky iny submissionType tak ostava. - patterns a
     * templates ostavaju
     *
     * @throws DeleteRelationsException vyhodi ak: - artefactType ma nejaky
     * prispevok. - criterion ma nejaku recenziu
     *
     */
    @Override
    public void deleteSubmissionTypeArtefact(String uuid) throws NoRecordException, DeleteCriteriaException, DeleteCycleException {


        log().debug("Delete artefact typeL {} ", uuid);


        SubmissionTypeArtefact artefact = submissionTypeArtefactDao.loadArtefactWithReferencesByUuid(uuid);

        // delete cycles - zmaze vyskyt artefaktu v cykloch
        try {

            log().debug("Delete artefact type from cycle if any exists");

            reviewCycleDao.deleteArtefactFromCycle(artefact);
        } catch (ConstraintViolationException cve) {
            throw new DeleteCycleException();
        }
        // delete criterias
        try {
            log().debug("Delete artefact type review criterias");
            reviewCriteriaDao.deleteCriteriasForArtefactType(uuid);
        } catch (ConstraintViolationException cve) {
            throw new DeleteCriteriaException();
        }
        // delete artefactType - zistime ci sa nahodou uz nepouziva niekde
        int artefactTypeUseCount = submissionArtefactTypeDao.loadArtefactTypeUseCountBySubmissionTypeUuid(uuid);

        SubmissionArtefactType artefactType = artefact.getArtefactType();

        // Odstranime artefact z submission typu        
        log().debug("Remove artefact type from submission type");


        SubmissionType type = artefact.getType();
        type.getArtefacts().remove(artefact);
        submissionTypeDao.update(type);


        // Odstranime artefact        
        log().debug("Delete artefact type");

        submissionTypeArtefactDao.delete(artefact);


        // Odstranime artefactType
        if (artefactTypeUseCount == 1) {
            log().debug("Used only {}: Delete ", artefactType);
            submissionArtefactTypeDao.delete(artefactType);
        }
    }

    @Override
    public void deleteReviewCriteria(Long id) throws NoRecordException, DeleteCriteriaException {
        ReviewCriteria c = reviewCriteriaDao.load(id);
        log().debug("Delete ", c);

        try {
            reviewCriteriaDao.delete(c);
        } catch (ConstraintViolationException cve) {
            throw new DeleteCriteriaException();
        }
    }

    @Override
    public void deleteReviewCycle(Long id) throws NoRecordException, DeleteCycleException {
        ReviewCycle c = reviewCycleDao.load(id);
        log().debug("Delete ", c);
        try {
            reviewCycleDao.delete(c);
        } catch (ConstraintViolationException cve) {
            throw new DeleteCycleException();
        }

        notifyAboutChangedEventProperties();
    }

    @Override
    public void deleteArtefactTemplate(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        SubmissionArtefactTemplate art = artefactTemplateDao.loadByDataFileUuid(uuid);
        throw new NullPointerException("Not implemented");
    }

    @Override
    public void deleteArtefactPattern(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        throw new NullPointerException("Not implemented");
    }

    public void setArtefactTemplateDao(SubmissionArtefactTemplateDao artefactTemplateDao) {
        this.artefactTemplateDao = artefactTemplateDao;
    }

    public SubmissionArtefactTemplateDao getArtefactTemplateDao() {
        return artefactTemplateDao;
    }

    public void setArtefactPatternDao(SubmissionArtefactPatternDao artefactPatternDao) {
        this.artefactPatternDao = artefactPatternDao;
    }

    public SubmissionArtefactPatternDao getArtefactPatternDao() {
        return artefactPatternDao;
    }

    public ReviewCriteriaDao getReviewCriteriaDao() {
        return reviewCriteriaDao;
    }

    public void setReviewCriteriaDao(ReviewCriteriaDao reviewCriteriaDao) {
        this.reviewCriteriaDao = reviewCriteriaDao;
    }

    public ReviewCycleDao getReviewCycleDao() {
        return reviewCycleDao;
    }

    public void setReviewCycleDao(ReviewCycleDao reviewCycleDao) {
        this.reviewCycleDao = reviewCycleDao;
    }

    public SubmissionTypeArtefactDao getSubmissionTypeArtefactDao() {
        return submissionTypeArtefactDao;
    }

    public void setSubmissionTypeArtefactDao(SubmissionTypeArtefactDao submissionTypeArtefactDao) {
        this.submissionTypeArtefactDao = submissionTypeArtefactDao;
    }

    /**
     * @return the submissionTypeDao
     */
    public SubmissionTypeDao getSubmissionTypeDao() {
        return submissionTypeDao;
    }

    /**
     * @param submissionTypeDao the submissionTypeDao to set
     */
    public void setSubmissionTypeDao(SubmissionTypeDao submissionTypeDao) {
        this.submissionTypeDao = submissionTypeDao;
    }

    /**
     * @return the submissionArtefactTypeDao
     */
    public SubmissionArtefactTypeDao getSubmissionArtefactTypeDao() {
        return submissionArtefactTypeDao;
    }

    /**
     * @param submissionArtefactTypeDao the submissionArtefactTypeDao to set
     */
    public void setSubmissionArtefactTypeDao(SubmissionArtefactTypeDao submissionArtefactTypeDao) {
        this.submissionArtefactTypeDao = submissionArtefactTypeDao;
    }

    private void notifyAboutChangedEventProperties() {
        publisher.publishEvent(new PropertiesChangedEvent(this));
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher aep) {
        this.publisher = aep;
    }

    /**
     * AWS S3 bucket
     *
     * @return the submissonBucket
     */
    public String getSubmissonBucket() {
        return submissonBucket;
    }

    /**
     * AWS S3 bucket
     *
     * @param submissonBucket the submissonBucket to set
     */
    public void setSubmissonBucket(String submissonBucket) {
        this.submissonBucket = submissonBucket;
    }
}
