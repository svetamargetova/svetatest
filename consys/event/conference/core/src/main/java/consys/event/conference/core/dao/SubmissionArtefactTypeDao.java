package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.bo.SubmissionArtefactType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionArtefactTypeDao extends GenericDao<SubmissionArtefactType>{

    public int loadArtefactTypeUseCountBySubmissionTypeUuid(String submissionTypeUuid);    
    
}
