package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionTypeArtefactDao extends GenericDao<SubmissionTypeArtefact> {

    /**
     * Nacita podla uuid a k nemu nacita autoamticky ArtefactType
     */
    public SubmissionTypeArtefact loadArtefactWithTypeByUuid(String uuid) throws NoRecordException;

    /**
     * Nacita podla uuid a nic nedotahuje
     */
    public SubmissionTypeArtefact loadArtefactByUuid(String uuid) throws NoRecordException;

    /**
     * Nacita podla uuid a dotiahnt typ a artefacttype
     */
    public SubmissionTypeArtefact loadArtefactWithReferencesByUuid(String uuid) throws NoRecordException;

    /**
     * @throws NoRecordException ak sa pocet vstupov nerovna vystupom
     */
    public List<SubmissionTypeArtefact> listSubmissionArtefacts(List<String> artefactsUuids) throws NoRecordException;

    /**
     * Vsetky artefakty s typami pre submission typ
     */
    public List<SubmissionTypeArtefact> listSubmissionArtefactsWithTypesByType(String typeUuid) throws NoRecordException;

    /**
     * Vrati UUID vsetkych nepriradenych artefaktov k cyklu.
     *
     * @param typeId submisson typ
     * @return zoznam uuid nepriradenych artefaktov
     */
    public List<String> listNotAssignedArtefactsUuids(Long typeId);

    public List<Long> listSubmissionTypeArtefactsIdsByCycles(Long submissionTypeId);
    
    public List<Long> listSubmissionTypeArtefactsIdsByOrderinSubmissionType(Long submissionTypeId);
}
