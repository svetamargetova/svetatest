package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.bo.ReviewCriteriaItem;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewCriteriaItemDao extends GenericDao<ReviewCriteriaItem>{

    public List<ReviewCriteriaItem> listReviewCriteriaItemsWithParentCriteria(List<Long> ids) throws NoRecordException;
}
