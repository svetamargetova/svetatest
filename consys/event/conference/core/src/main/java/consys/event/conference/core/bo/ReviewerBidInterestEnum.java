/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.conference.core.bo;

/**
 *
 * @author palo
 */
public enum ReviewerBidInterestEnum {

    LIKE(3),DONT_LIKE(2),CONFLICT(1),NOT_CARE(0);

    private Integer id;

    private ReviewerBidInterestEnum(Integer id) {
        this.id = id;
    }

    public static ReviewerBidInterestEnum fromId(Integer id){
        switch(id){
            case 0: return NOT_CARE;
            case 1: return CONFLICT;
            case 2: return DONT_LIKE;
            case 3: return LIKE;
            default: return NOT_CARE;
        }
    }

    public Integer getId(){
        return id;
    }

    @Override
    public String toString() {
        return name();
    }



}
