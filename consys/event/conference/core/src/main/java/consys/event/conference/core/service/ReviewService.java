package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ArtefactReviewThumb;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.bo.thumb.ReviewerThumb;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewService {

    /* ------------------------------------------------------------------------ */
    /* --- L O A D --- */
    /* ------------------------------------------------------------------------ */
    public ReviewCriteria loadCriteriaWithItems(Long id)
            throws NoRecordException, RequiredPropertyNullException;

    public Review loadReview(String submissionUuid, String reviewerUuid)
            throws NoRecordException, RequiredPropertyNullException;
    
    public Review loadReviewWithArtefactReviewsWithoutCriterias(String submissionUuid, String reviewerUuid)
            throws NoRecordException, RequiredPropertyNullException;
    
    public Review loadReviewWithArtefactReviewsWithoutCriterias(String reviewUuid)
            throws NoRecordException, RequiredPropertyNullException;    
    
    public Review loadReviewByArtefact(String artefactUuid, String reviewerUuid)
            throws NoRecordException, RequiredPropertyNullException;

    
    

    /**
     * Pre vlozeny artefactUuid a reviewerUuid nacita evaluaciu artefaktu s 
     * vyhodnoteniami itemami.
     * <p/>
     * @param artefactUuid
     * @param reviewerUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    public ReviewArtefact loadReviewArtefactEvaluation(String artefactUuid, String reviewerUuid)
            throws NoRecordException, RequiredPropertyNullException;
    
    public ReviewArtefact loadReviewArtefactEvaluationForReview(String artefactUuid, String reviewUuid)
            throws NoRecordException, RequiredPropertyNullException;
    
    /**
     * Pre submission nacita vsetky recenzie artefaktu po kriteriach.
     * 
     * @param submissionId
     * @return kluc - artefact id, kluc - criteria id
     * @throws RequiredPropertyNullException 
     */
    public Map<Long,Map<Long,List<ArtefactReviewThumb>>> loadArtefactCriteriaEvaluations(Long submissionId)
            throws RequiredPropertyNullException;
            
    
    /* ------------------------------------------------------------------------ */
    /* --- L I S T --- */
    /* ------------------------------------------------------------------------ */             
    
    
    public List<ReviewCriteria> listCriteriasForSubmissionType(String uuid)
            throws NoRecordException, RequiredPropertyNullException;
    
    public List<ReviewCriteria> listCriteriasForSubmissionTypeArtefact(String uuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita kriteria aj s hodnotami ktore maju byt hodnotene v ramci artefakt
     * typu ktoreho uuid je ako argument.
     * 
     * @param artefactUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public List<ReviewCriteria> listCriteriasForBySubmissionArtefact(String artefactUuid)
            throws NoRecordException, RequiredPropertyNullException;    

    public List<String> listUuidsForReviewedArtefacts(String submissionUuid, String reviewerUuid)
            throws NoRecordException, RequiredPropertyNullException;

    public List<ReviewArtefactCriteriaEvaluation> listReviewerEvaluationsForArtefact(String submissionArtefatUuid, String reviewerUuid)
            throws NoRecordException, RequiredPropertyNullException;
            
    
    /**
     * Nacita 
     * 
     * @param typeUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public List<ReviewCycle> listReviewCyclesForType(String typeUuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita artefakty pre prve kolo posielania prispevkov.
     * 
     * @param typeUuid
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public List<ReviewCycle> listOnlyActiveCyclesForNewSubmission(String typeUuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Vytvori objekty Reviewer vzhladom k Submission podla ID
     */
    public List<ReviewerThumb> listReviewersPropertiesToSubmission(String submissionId) throws RequiredPropertyNullException, NoRecordException;

    /**
     * Nacita vsetky recenzie (celkove recenzie bez detailu) pre dany prispevok.
     * @param submissionUuid
     * @return
     * @throws RequiredPropertyNullException 
     */
    public List<ContributionReviewerReview> listSubmissionReviews(Long submissionId) throws RequiredPropertyNullException;

    /* ------------------------------------------------------------------------ */
    /* --- C R E A T E --- */
    /* ------------------------------------------------------------------------ */
    public void createReviewRelation(String submissionUuid, String reviewerUuid)
            throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException;
    
    /* ------------------------------------------------------------------------ */
    /* --- U P D A T E --- */
    /* ------------------------------------------------------------------------ */

   
    /**
     * Upravi recenzentov zaujem pre dany submission. Defaultne maju recenzenti
     * nastavene ako NOT_CARE.
     */
    public void updateReviewerBid(String reviewerUuid, String submissionUuid, ReviewerBidInterestEnum bid)
            throws NoRecordException, RequiredPropertyNullException;
   
    /**
     * Vykona create/update hodnotenia. Kontroluje ci su vsetky kriteria daneho
     * typu artefaktu ohodnotene.
     * <p/>
     * @check vsetky kriteria artefaktu vyhodnotene
     * @check je mozne recenzovat
     * @check
     * <p/>
     * @throws RequiredPropertyNullException ak chyba ohodnotenie nejakeho kriteria
     * alebo sa tam ohodnotenie nachadza viac krat pre jedno kriterium
     *
     */
    public void updateReviewEvaluation(String reviewUuid, String submissonUuid, String artefactUuid, Map<Long, Long> evaluatedCriteriaValues, String publicComment, String privateComment)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;
   
    
    /**
     * Upravi komentar celeho prispevku konrketneho recenzneta
     * 
     * @param submissionUuid uuid prispevku
     * @param reviewerUuid uuid recenzenta
     * @param publicComment verejny komentar
     * @param privateComment privatny komentar
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public void updateSubmissionReviewComments(String submissionUuid, String reviewerUuid, String publicComment, String privateComment)
            throws NoRecordException, RequiredPropertyNullException;
    

    /* ------------------------------------------------------------------------ */
    /* --- D E L E T E --- */
    /* ------------------------------------------------------------------------ */
    public void deleteReviewerForSubmission(String submissionUuid, String reviewerUuid, boolean force)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;
}
