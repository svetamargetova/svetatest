package consys.event.conference.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class SubmissionTypeNotUniqueException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public SubmissionTypeNotUniqueException() {
        super();
    }

     public SubmissionTypeNotUniqueException(String message) {
	super(message);
    }

}
