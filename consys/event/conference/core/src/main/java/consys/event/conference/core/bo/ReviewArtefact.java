package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Sets;
import java.util.Date;
import java.util.Set;

/**
 * Recenzia artefaktu ako celku
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewArtefact implements ConsysObject{
    private static final long serialVersionUID = 5288687446202347792L;

    private Long id;
    private String uuid;    
    private SubmissionArtefact artefact;
    private Review review;
    private String privateComment;
    private String publicComment;
    private Set<ReviewArtefactCriteriaEvaluation> criteriaEvaluations;
    private Date reviewDate;
    private int score = -1;

    public ReviewArtefact() {
        criteriaEvaluations = Sets.newHashSet();
    }
    

    
    
    /**
     * @return the artefact
     */
    public SubmissionArtefact getArtefact() {
        return artefact;
    }

    /**
     * @param artefact the artefact to set
     */
    public void setArtefact(SubmissionArtefact artefact) {
        this.artefact = artefact;
    }

    /**
     * @return the review
     */
    public Review getReview() {
        return review;
    }

    /**
     * @param review the review to set
     */
    public void setReview(Review review) {
        this.review = review;
    }

   

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ReviewArtefact e = (ReviewArtefact) obj;
        return getUuid().equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.getUuid() != null ? this.getUuid().hashCode() : 0);
        return hash;
    }


    @Override
    public String toString() {
        return String.format("ReviewArtefact[%s %s]", getArtefact(),getReview());
    }

    /**
     * @return the reviewDate
     */
    public Date getReviewDate() {
        return reviewDate;
    }

    /**
     * @param reviewDate the reviewDate to set
     */
    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the criteriaEvaluations
     */
    public Set<ReviewArtefactCriteriaEvaluation> getCriteriaEvaluations() {
        return criteriaEvaluations;
    }

    /**
     * @param criteriaEvaluations the criteriaEvaluations to set
     */
    public void setCriteriaEvaluations(Set<ReviewArtefactCriteriaEvaluation> criteriaEvaluations) {
        this.criteriaEvaluations = criteriaEvaluations;
    }

    /**
     * @return the privateComment
     */
    public String getPrivateComment() {
        return privateComment;
    }

    /**
     * @param privateComment the privateComment to set
     */
    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    /**
     * @return the publicComment
     */
    public String getPublicComment() {
        return publicComment;
    }

    /**
     * @param publicComment the publicComment to set
     */
    public void setPublicComment(String publicComment) {
        this.publicComment = publicComment;
    }

   


}
