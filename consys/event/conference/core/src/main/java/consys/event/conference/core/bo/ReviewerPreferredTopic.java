package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.event.common.core.bo.UserEvent;

/**
 *
 * @author Palo
 */
public class ReviewerPreferredTopic implements ConsysObject {

    private static final long serialVersionUID = 543802249874077606L;
    private UserEvent reviewer;
    private SubmissionTopic topic;

    /**
     * @return the reviewer
     */
    public UserEvent getReviewer() {
        return reviewer;
    }

    /**
     * @param reviewer the reviewer to set
     */
    public void setReviewer(UserEvent reviewer) {
        this.reviewer = reviewer;
    }

    /**
     * @return the topic
     */
    public SubmissionTopic getTopic() {
        return topic;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(SubmissionTopic topic) {
        this.topic = topic;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ReviewerPreferredTopic e = (ReviewerPreferredTopic) obj;
        return e.getReviewer().equals(e.reviewer) && e.getTopic().equals(e.topic);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.reviewer != null ? this.reviewer.hashCode() : 0);
        hash = 29 * hash + (this.topic != null ? this.topic.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Contributor[name=%s submission=%s]", getReviewer(), getTopic());
    }
}
