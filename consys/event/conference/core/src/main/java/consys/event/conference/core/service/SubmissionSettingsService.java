package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionArtefactPattern;
import consys.event.conference.core.bo.SubmissionArtefactTemplate;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.bo.thumb.SubmissionTypeArtefactThumb;
import consys.event.conference.core.exception.ArtefactInDifferentCycleException;
import consys.event.conference.core.exception.DeleteCriteriaException;
import consys.event.conference.core.exception.DeleteCycleException;
import consys.event.conference.core.exception.DeleteRelationsException;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import java.util.List;

/**
 * Sluzba na nastavenie submission modulu
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionSettingsService {

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    /**
     * Vylistovanie thumbov typov prispevkov
     */
    public List<ListUuidItem> listSettingsThumbTypes() throws NoRecordException;

    /**
     * Vylistovanie thumbov vsetkych sablon
     */
    public List<ListUuidItem> listTemplatesThumbs() throws NoRecordException;

    /**
     * Vylistovanie thumbov vsetkych vzorov
     */
    public List<ListUuidItem> listPatternsThumbs() throws NoRecordException;

    /**
     * Vylistovanie vsetkych vzorov podla vlozneych uuid vzorov
     * @param uuids zoznam uuid vzorov
     */
    public List<SubmissionArtefactPattern> listPatternsByUuids(List<String> uuids) throws NoRecordException;

    /**
     * Vylistovanie vsetkych sablon podla vlozneych uuid sablon
     * @param uuids zoznam uuid sablon
     */
    public List<SubmissionArtefactTemplate> listTemplateByUuids(List<String> uuids) throws NoRecordException;

    /**
     * Vylistovanie artefaktov podla UUID artefaktov.
     */
    public List<SubmissionTypeArtefact> listArtefactsInList(List<String> artefacts) throws NoRecordException;
    /*------------------------------------------------------------------------*/
    /* --- L O A D --- */
    /*------------------------------------------------------------------------*/

    public SubmissionTypeArtefactThumb loadTypeArtefactThumbByUuid(String uuid) throws NoRecordException;

    /**
     * Nacita SubmissionTypeArtefact + ArtefactType
     */
    public SubmissionTypeArtefact loadTypeArtefactWithArtefactByUuid(String uuid) throws NoRecordException;

    public ReviewCycle loadReviewCycleWithTypeById(Long id) throws NoRecordException;
    
    public List<String> listNotAssignedArtefactsUuids(Long typeId) throws NoRecordException;

    public ReviewCriteria loadReviewCriteria(Long id) throws NoRecordException;

    public SubmissionArtefactPattern loadPatternByUuid(String uuid) throws NoRecordException;

    public SubmissionArtefactTemplate loadTemplateByUuid(String uuid) throws NoRecordException;

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    /**
     * Vytvorenie Typu prispevku
     */
    public void createSubmissionType(SubmissionType type) throws RequiredPropertyNullException, SubmissionTypeNotUniqueException;

    /**
     * Vytvorenie niekolkych typov prispevku
     */
    public void createSubmissionTypes(List<SubmissionType> types) throws RequiredPropertyNullException, SubmissionTypeNotUniqueException;

    /**
     * Vytvorenie typov artefaktov k typu prispevku
     */
    public void createSubmissionArtefactTypes(String submissionTypeUuid, List<SubmissionTypeArtefact> artefacts) throws RequiredPropertyNullException, NoRecordException;

    /**
     * Vytvorenie recenzencnych kriterii pre typ artefaktu
     * - kontroluje ci prispevok nie je nahodou automaticky prijatelny
     */
    public void createReviewCriterias(String artefactUuid, List<ReviewCriteria> criterias) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Vytvornenie recenzencnych/submitacnych  cyklov
     */
    public void createReviewCycles(String submissionType, List<ReviewCycle> cycles) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Vytvorenie vzoru artefaktu
     */
    public void createSubmissionPattern(SubmissionArtefactPattern pattern, DataFile datafile) throws RequiredPropertyNullException,ServiceExecutionFailed;

    /**
     * Vytvorenie sablony artefaktu
     */
    public void createSubmissionTemplate(SubmissionArtefactTemplate template,  DataFile datafile) throws RequiredPropertyNullException,ServiceExecutionFailed;

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    public void updateReviewCriteria(ReviewCriteria criteria)
            throws NoRecordException, RequiredPropertyNullException, DeleteCriteriaException;

    public void updateSubmissionType(SubmissionType type)
            throws RequiredPropertyNullException, SubmissionTypeNotUniqueException;

    public void updateReviewCycle(ReviewCycle cycle, List<String> artefacts)
            throws RequiredPropertyNullException, NoRecordException, ServiceExecutionFailed, ArtefactInDifferentCycleException;

    public void updateSubmissionArtefactType(SubmissionTypeArtefact artefact, List<String> templates, List<String> patterns) 
            throws NoRecordException, RequiredPropertyNullException,DeleteCriteriaException;
    
    public void updateSubmissionTypeOrder(String typeUuid, int oldPosition, int newPosition)
            throws NoRecordException,RequiredPropertyNullException;    
    /*------------------------------------------------------------------------*/
    /* ---  D E L E T E --- */
    /*------------------------------------------------------------------------*/

    public void deleteSubmissionType(String typeUuid) throws NoRecordException, DeleteRelationsException;

    public void deleteSubmissionTypeArtefact(String uuid) throws NoRecordException, DeleteCriteriaException, DeleteCycleException;

    public void deleteReviewCriteria(Long id) throws NoRecordException, DeleteCriteriaException;

    public void deleteReviewCycle(Long id) throws NoRecordException, DeleteCycleException;
    /**
     * Odstrani sablonu podla UUID.
     * 
     * @param uuid
     */
    public void deleteArtefactTemplate(String uuid)
            throws NoRecordException,RequiredPropertyNullException;
    
    /**
     * Odstrani vzor vyplnenia podla UUID.
     * @param uuid
     */
    public void deleteArtefactPattern(String uuid)
            throws NoRecordException,RequiredPropertyNullException;
}
