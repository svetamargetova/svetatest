package consys.event.conference.core.module;

import consys.common.core.bo.SystemProperty;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.api.right.Role;
import consys.event.common.core.AbstractEventModuleRegistrator;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;

/**
 *
 * @author Palo
 */
public class ConferenceModuleRegistrator extends AbstractEventModuleRegistrator implements ConferenceProperties {

    private static final String SCHEMA = "conference";
    private static final String MODULE = "conference";

    @Override
    public String getSchemaName() {
        return SCHEMA;
    }

    @Override
    public void doAfterSessionFactoryCreate(Session session) {
    }

    @Override
    public List<SystemProperty> getModuleProperties() {
        SystemProperty visibleAuthors = new SystemProperty();
        visibleAuthors.setKey(SYSTEM_PROPERTY_VISIBLE_AUTHORS);
        visibleAuthors.setValueFromBoolean(Boolean.FALSE);
        visibleAuthors.setToClient(true);
        SystemProperty maxReviews = new SystemProperty();
        maxReviews.setKey(SYSTEM_PROPERTY_MAX_REVIEWS_PER_REVIEWER);
        maxReviews.setValue("10");
        maxReviews.setToClient(true);
        return Lists.newArrayList(visibleAuthors, maxReviews);
    }

    @Override
    public Module getModule() {
        Module m = new Module();
        m.setName(MODULE);
        EventConferenceModuleRoleModel rights = new EventConferenceModuleRoleModel();
        for (Role r : rights.getRoles()) {
            ModuleRight right = new ModuleRight();
            right.setName(r.getName());
            right.setUniqueValue(r.getIdentifier());
            m.getRights().add(right);
        }

        return m;
    }

    @Override
    public Map<String, String> getSQLScripts() {
        Map<String, String> map = Maps.newHashMap();
        // map.put("bid_for_review_list_function", "/SQL/bid_for_review.sql");
        map.put("recount_score_on_new_artefact_review", "/SQL/recount_score_on_new_artefact_review.sql");
        return map;
    }
}
