package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.ReviewerPreferredTopic;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.dao.ReviewerPreferredTopicDao;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerPreferredTopicDaoImpl extends GenericDaoImpl<ReviewerPreferredTopic> implements ReviewerPreferredTopicDao {

    @Override
    public ReviewerPreferredTopic load(Long id) throws NoRecordException {
        return load(id, ReviewerPreferredTopic.class);
    }

    @Override
    public List<Long> listTopicsIdsForUser(String userUuid) {
        return session().
                createQuery("select r.topic.id from ReviewerPreferredTopic r where r.reviewer.uuid=:USER_UUID").
                setString("USER_UUID", userUuid).
                list();
    }

    @Override
    public List<ReviewerPreferredTopic> listTopicsForUser(String userUuid) {
        return session().
                createQuery("select r from ReviewerPreferredTopic r where r.reviewer.uuid=:USER_UUID").
                setString("USER_UUID", userUuid).
                list();
    }

    @Override
    public void delete(UserEvent user, Long topic) throws NoRecordException {
        ReviewerPreferredTopic t = (ReviewerPreferredTopic) session().
                createQuery("from ReviewerPreferredTopic r where r.reviewer.id=:USER_ID and r.topic.id=:TOPIC_ID").
                setLong("USER_ID", user.getId()).
                setLong("TOPIC_ID", topic).
                uniqueResult();
        if (t == null) {
            throw new NoRecordException();
        }
        session().delete(t);
    }

    @Override
    public void create(UserEvent user, Long topic) throws NoRecordException {
        SubmissionTopic t = (SubmissionTopic) session().createQuery("from SubmissionTopic t where t.id=:ID").setLong("ID", topic).uniqueResult();
        if (t == null) {
            throw new NoRecordException();
        }
        ReviewerPreferredTopic tp = new ReviewerPreferredTopic();
        tp.setReviewer(user);
        tp.setTopic(t);
        create(tp);
    }
}
