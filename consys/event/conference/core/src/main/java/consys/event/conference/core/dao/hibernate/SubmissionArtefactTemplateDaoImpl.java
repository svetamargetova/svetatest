package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.SubmissionArtefactTemplate;
import consys.event.conference.core.dao.SubmissionArtefactTemplateDao;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionArtefactTemplateDaoImpl extends GenericDaoImpl<SubmissionArtefactTemplate> implements SubmissionArtefactTemplateDao{

    @Override
    public SubmissionArtefactTemplate load(Long id) throws NoRecordException {
       return load(id, SubmissionArtefactTemplate.class);
    }

    @Override
    public List<ListUuidItem> listTemplatesForArtefact(Long id) {
        List<Object[]> items = session().
                createQuery("select template.uuid,template.title from SubmissionArtefactType t join t.templates as template where t.id=:ID").
                setLong("ID", id).
                list();
        List<ListUuidItem> out = Lists.newArrayList();
        for(Object[] o: items){
            out.add(new ListUuidItem((String)o[1], (String)o[0]));
        }
        return out;
    }

    @Override
    public List<ListUuidItem> listTemplatesThumbs() throws NoRecordException {
        List<Object[]> items = session().
                createQuery("select template.uuid,template.title from SubmissionArtefactTemplate template order by template.title").                
                list();
        List<ListUuidItem> out = Lists.newArrayList();
        for(Object[] o: items){
            out.add(new ListUuidItem((String)o[1], (String)o[0]));
        }
        return out;
    }

    @Override
    public SubmissionArtefactTemplate loadByDataFileUuid(String uuid) throws NoRecordException {
        SubmissionArtefactTemplate p = (SubmissionArtefactTemplate) session().
                createQuery("from SubmissionArtefactTemplate template where template.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if(p == null){
            throw new NoRecordException();
        }
        return p;
    }

    @Override
    public List<SubmissionArtefactTemplate> listTemplatesByUuids(List<String> uuids) throws NoRecordException {
        List<SubmissionArtefactTemplate> p = (List<SubmissionArtefactTemplate>) session().
                createQuery("select template from SubmissionArtefactTemplate template where template.uuid IN (:UUIDS)").
                setParameterList("UUIDS", uuids).
                list();
        if (p == null || p.isEmpty()) {
            throw new NoRecordException();
        }
        return p;
    }
    




}
