package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.ReviewCriteriaItem;
import consys.event.conference.core.dao.ReviewCriteriaItemDao;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewCriteriaItemDaoImpl extends GenericDaoImpl<ReviewCriteriaItem> implements ReviewCriteriaItemDao {
 
    @Override
    public ReviewCriteriaItem load(Long id) throws NoRecordException {
        return load(id, ReviewCriteriaItem.class);
    }

    @Override
    public List<ReviewCriteriaItem> listReviewCriteriaItemsWithParentCriteria(List<Long> ids) throws NoRecordException {
        List<ReviewCriteriaItem> items = session().
                createQuery("select item from ReviewCriteriaItem item LEFT JOIN FETCH item.criteria where item.id in (:IDS)").
                setParameterList("IDS", ids).list();
        if (items.size() != ids.size()) {
            throw new NoRecordException();
        }
        return items;
    }
}
