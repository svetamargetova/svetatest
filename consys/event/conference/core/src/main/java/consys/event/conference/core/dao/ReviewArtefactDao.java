package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.bo.ReviewArtefact;
import consys.event.conference.core.bo.thumb.ArtefactReviewThumb;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewArtefactDao extends GenericDao<ReviewArtefact> {

    /**
     * Nacita recenziu artefaktu pre vykonanie samotnej recenzie - nastavuje zamok 
     * 
     * @param artefactUuid
     * @param reviewUuid
     * @return
     * @throws NoRecordException 
     */
    public ReviewArtefact loadForReview(String artefactUuid, String reviewUuid) throws NoRecordException;

    public ReviewArtefact loadByReview(String artefactUuid, String reviewUuid) throws NoRecordException;

    public ReviewArtefact load(String artefactUuid, String reviewUuid) throws NoRecordException;

    /**
     * Nacita pre prispevok vsetky vyhodnitenia kriterii
     * 
     * @param submissionUuid
     * @return Kluc je ID artefaktu, dalsia mapa je kluc ID kriteria 
     * @throws NoRecordException 
     */
    public Map<Long, Map<Long, List<ArtefactReviewThumb>>> loadArtefactCriteriaEvaluations(Long submissionId);

    void deleteAllArtefacts();
}
