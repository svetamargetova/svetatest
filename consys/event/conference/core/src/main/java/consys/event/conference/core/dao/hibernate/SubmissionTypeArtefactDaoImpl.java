package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import consys.event.conference.core.dao.SubmissionTypeArtefactDao;
import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionTypeArtefactDaoImpl extends GenericDaoImpl<SubmissionTypeArtefact> implements SubmissionTypeArtefactDao {

    @Override
    public SubmissionTypeArtefact load(Long id) throws NoRecordException {
        return load(id, SubmissionTypeArtefact.class);
    }

    @Override
    public SubmissionTypeArtefact loadArtefactWithTypeByUuid(String uuid) throws NoRecordException {

        SubmissionTypeArtefact sta = (SubmissionTypeArtefact) session().
                createQuery("from SubmissionTypeArtefact s JOIN FETCH s.artefactType where s.uuid=:A_UUID").
                setString("A_UUID", uuid).
                uniqueResult();

        if (sta == null) {
            throw new NoRecordException();
        }

        return sta;

    }

    @Override
    public SubmissionTypeArtefact loadArtefactByUuid(String uuid) throws NoRecordException {
        SubmissionTypeArtefact sta = (SubmissionTypeArtefact) session().
                createQuery("from SubmissionTypeArtefact s where s.uuid=:A_UUID").
                setString("A_UUID", uuid).
                uniqueResult();

        if (sta == null) {
            throw new NoRecordException();
        }

        return sta;

    }

    @Override
    public SubmissionTypeArtefact loadArtefactWithReferencesByUuid(String uuid) throws NoRecordException {
        SubmissionTypeArtefact sta = (SubmissionTypeArtefact) session().
                createQuery("from SubmissionTypeArtefact s JOIN FETCH s.artefactType JOIN FETCH s.type where s.uuid=:A_UUID").
                setString("A_UUID", uuid).
                uniqueResult();
        if (sta == null) {
            throw new NoRecordException();
        }

        return sta;
    }

    @Override
    public List<SubmissionTypeArtefact> listSubmissionArtefacts(List<String> artefactsUuids) throws NoRecordException {
        List<SubmissionTypeArtefact> list = session().
                createQuery("select s from SubmissionTypeArtefact s JOIN FETCH s.artefactType where s.uuid IN (:UUIDS)").
                setParameterList("UUIDS", artefactsUuids).list();
        if (list.size() != artefactsUuids.size()) {
            throw new NoRecordException();
        }
        return list;
    }

    @Override
    public List<SubmissionTypeArtefact> listSubmissionArtefactsWithTypesByType(String typeUuid) throws NoRecordException {
        List<SubmissionTypeArtefact> list = session().
                createQuery("select s from SubmissionTypeArtefact s JOIN FETCH s.artefactType where s.type.uuid = :UUID").
                setString("UUID", typeUuid).list();
        if (list == null || list.isEmpty()) {
            throw new NoRecordException();
        }
        return list;
    }

    @Override
    public List<String> listNotAssignedArtefactsUuids(Long typeId) {
        List<String> list = session().
                createQuery("select sta.uuid from SubmissionTypeArtefact sta where sta.type.id=:ID and sta.id not in (select a.id from ReviewCycle rc join rc.artefacts as a where rc.submissionType.id=:ID)").
                setLong("ID", typeId).list();
        return list;
    }

    @Override
    public List<Long> listSubmissionTypeArtefactsIdsByCycles(Long submissionTypeId) {
        List<BigInteger> ids = session().
                createSQLQuery("select rca.id_submission_type_artefact from conference.review_cycle_artefact rca left join conference.review_cycle c on c.id=rca.id_review_cycle join conference.submission_type_artefact sta on sta.id=rca.id_submission_type_artefact where sta.id_submission_type=:ID order by c.submit_from").
                setLong("ID", submissionTypeId).list();
        List<Long> idsInLong = Lists.newArrayListWithCapacity(ids.size());
        for (BigInteger bigInteger : ids) {
            idsInLong.add(bigInteger.longValue());
        }

        return idsInLong;
    }

    @Override
    public List<Long> listSubmissionTypeArtefactsIdsByOrderinSubmissionType(Long submissionTypeId) {
        List<Long> list = session().
                createQuery("select s.id from SubmissionTypeArtefact s where s.type.id=:ID order by s.order").
                setLong("ID", submissionTypeId).list();
        return list;
    }
}
