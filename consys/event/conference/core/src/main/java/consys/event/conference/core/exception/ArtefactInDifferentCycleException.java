package consys.event.conference.core.exception;

import consys.common.core.exception.ConsysException;

/**
 * Znamena to ze na TOPIC uz su naviazane nejake dalsie objektu [submission, preffered topics]
 * @author Palo
 */
public class ArtefactInDifferentCycleException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public ArtefactInDifferentCycleException() {
        super();
    }

     public ArtefactInDifferentCycleException(String message) {
	super(message);
    }

}
