package consys.event.conference.core.dao.hibernate;

import com.google.common.collect.ImmutableList;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.ListUtils;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.api.list.CommonFilters.States;
import consys.event.conference.api.list.ContributionsList;
import consys.event.conference.api.list.ReviewerReviewsList;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.core.bo.Review;
import consys.event.conference.core.bo.ReviewerBidInterestEnum;
import consys.event.conference.core.bo.SubmissionStateEnum;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.bo.thumb.ContributionReviewerArtefactReview;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.bo.thumb.ReviewerThumb;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import consys.event.conference.core.bo.thumb.SubmissionListItem.Evaluation;
import consys.event.conference.core.dao.ReviewDao;
import consys.event.conference.core.dao.SubmissionDao;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.jdbc.Work;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewDaoImpl extends GenericDaoImpl<Review> implements ReviewDao {

    private SubmissionDao submissionDao;

    public void setSubmissionDao(SubmissionDao submissionDao) {
        this.submissionDao = submissionDao;
    }

    public SubmissionDao getSubmissionDao() {
        return submissionDao;
    }


    /*
     * ------------------------------------------------------------------------
     * DAO
     * ------------------------------------------------------------------------
     */
    @Override
    public Review load(Long id) throws NoRecordException {
        return load(id, Review.class);
    }

    @Override
    public Review loadReview(String uuid) throws NoRecordException {
        Review review = (Review) session().
                createQuery("select r from Review r where r.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (review == null) {
            throw new NoRecordException();
        }
        return review;
    }

    @Override
    public Review loadReviewerReview(String reviewerUuid, String submissionUuid) throws NoRecordException {
        Review review = (Review) session().
                createQuery("select r from Review r JOIN FETCH r.submission as sub JOIN FETCH r.reviewer as reviewer where reviewer.uuid=:RUUID and sub.uuid=:SUUID").
                setString("RUUID", reviewerUuid).
                setString("SUUID", submissionUuid).
                uniqueResult();
        if (review == null) {
            throw new NoRecordException();
        }
        return review;
    }

    @Override
    public List<ContributionReviewerReview> listSubmissionReviewerReviews(Long submissionId) {

        ImmutableList.Builder<ContributionReviewerReview> b = ImmutableList.builder();
        List<Object[]> oreviews = session().createSQLQuery("select ue.uuid,ue.full_name, ue.position, ue.organization, ue.profile_img_prefix,r.uuid as review_uuid,r.score as review_score,r.public_comment,r.private_comment, sa.uuid as submission_artefact_uuid, sa.id_submission_type_artefact, ra.score from conference.review  r left join conference.review_artefact ra on ra.id_review=r.id left outer join conference.submission_artefact sa on sa.id=ra.id_artefact , user_event ue where r.id_submission=:SUBMISSION_ID and r.id_user=ue.id order by r.uuid").
                setLong("SUBMISSION_ID", submissionId).list();

        ContributionReviewerReview review = null;

        for (Object[] objects : oreviews) {
            if (review == null || !review.getReviewerUuid().equals(objects[0])) {
                review = new ContributionReviewerReview();
                review.setReviewerUuid((String) objects[0]);
                review.setReviewerName((String) objects[1]);
                review.setReviewerPosition((String) objects[2]);
                review.setReviewerOrganization((String) objects[3]);
                review.setReviewerImageUuidPrefix((String) objects[4]);
                review.setReviewUuid((String) objects[5]);
                review.setReviewerReviewScore((Integer) objects[6]);
                review.setReviewerPublicComment((String) objects[7]);
                review.setReviewerPrivateComment((String) objects[8]);
                b.add(review);
            }
            //  spracujeme artefact
            if (objects[9] != null) {
                review.getArtefactReviews().add(new ContributionReviewerArtefactReview((String) objects[9], ((BigInteger) objects[10]).longValue(), (Integer) objects[11]));
            }
        }
        return b.build();
    }

    @Override
    public Review loadReviewerReviewWithDetailedSubmission(String reviewerUuid, String submissionUuid) throws NoRecordException {
        Review review = (Review) session().
                createQuery("select r from Review r JOIN FETCH r.submission as s JOIN FETCH s.type as type JOIN FETCH s.artefacts as artefact JOIN FETCH artefact.typeArtefact join r.reviewer as reviewer where reviewer.uuid=:RUUID and s.uuid=:SUUID").
                setString("RUUID", reviewerUuid).
                setString("SUUID", submissionUuid).
                uniqueResult();
        if (review == null) {
            throw new NoRecordException();
        }
        return review;
    }

    @Override
    public Review loadReviewerReviewWithArtefactsReviewWithoutCriterias(String reviewerUuid, String submissionUuid) throws NoRecordException {
        Review review = (Review) session().
                createQuery("select r from Review r left join fetch r.artefactEvaluations as ae left join fetch ae.artefact left join fetch r.reviewer where r.reviewer.uuid=:RUUID and r.submission.uuid=:SUUID").
                setString("RUUID", reviewerUuid).
                setString("SUUID", submissionUuid).
                uniqueResult();
        if (review == null) {
            throw new NoRecordException();
        }
        return review;
    }

    @Override
    public Review loadReviewerReviewWithArtefactsReviewWithoutCriterias(String reviewUuid) throws NoRecordException {
        Review review = (Review) session().
                createQuery("select r from Review r left join fetch r.artefactEvaluations as ae left join fetch ae.artefact left join fetch r.reviewer where r.uuid=:RUUID").
                setString("RUUID", reviewUuid).
                uniqueResult();
        if (review == null) {
            throw new NoRecordException();
        }
        return review;
    }

    @Override
    public Review loadReviewerReviewByArtefact(String reviewerUuid, String submissionArtefactUuid) throws NoRecordException {
        Review review = (Review) session().
                createQuery("select r from Review r,Submission s join s.artefacts as artefact where r.reviewer.uuid=:RUUID and artefact.uuid=:SAUUID and s.id=r.submission.id").
                setString("RUUID", reviewerUuid).
                setString("SAUUID", submissionArtefactUuid).
                uniqueResult();
        if (review == null) {
            throw new NoRecordException();
        }
        return review;
    }

    @Override
    public List<ReviewerThumb> listAllReviewers(String submissionUuid) throws NoRecordException {

        List<ReviewerThumb> reviewers = Lists.newArrayList();


        List<Object[]> users = session().
                createSQLQuery("select distinct mem.id, mem.uuid, mem.full_name, topic.id as topic_id, topic.name from \"group\" g left join group_user as gu on gu.id_group=g.id left join user_event as mem on mem.id=gu.id_user_event left join group_right as gr on gr.id_group=g.id left join conference.reviewer_preferred_topic pf on pf.id_user_event=mem.id left join conference.submission_topic as topic on pf.id_topic=topic.id where gr.id_module_right=(select id from module_right where right_name=:RIGHT) order by mem.id").
                setString("RIGHT", EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER).
                list();


        // nacitame vytazenie
        List<Object[]> loadList = session().createQuery("select review.reviewer.uuid,count(review.submission.id) from Review review group by review.reviewer.uuid").list();
        Map<String, Integer> loadMap = Maps.newHashMap();
        for (Object[] objects : loadList) {
            loadMap.put((String) objects[0], ((Long) objects[1]).intValue());
        }
        // Nacitame uuid recenzentu ktery uz recenzuju tento submission
        List<String> submissionReviewers = session().createQuery("select review.reviewer.uuid from Review review where review.submission.uuid=:UUID").setString("UUID", submissionUuid).list();

        // Nacitame interest
        List<Object[]> interestList = session().
                createQuery("select bid.reviewer.uuid, bid.interest from ReviewerBid bid where bid.submission.uuid=:UUID").
                setString("UUID", submissionUuid).
                list();
        Map<String, ReviewerBidInterestEnum> interestMap = Maps.newHashMap();
        for (Object[] objects : interestList) {
            interestMap.put((String) objects[0], (ReviewerBidInterestEnum) objects[1]);
        }


        ReviewerThumb r = null;
        for (Object[] item : users) {
            String userUuid = (String) item[1];
            String userName = (String) item[2];
            Long userId = ((BigInteger) item[0]).longValue();
            if (r == null || !(r.getUserUuid().equalsIgnoreCase(userUuid))) {
                r = new ReviewerThumb();
                r.setFullName(userName);
                r.setId(userId);
                r.setUserUuid(userUuid);
                Integer load = loadMap.get(userUuid);
                ReviewerBidInterestEnum interest = interestMap.get(userUuid);
                r.setLoad(load == null ? 0 : load);
                r.setInterest(interest == null ? ReviewerBidInterestEnum.NOT_CARE : interest);
                r.setReviewing(submissionReviewers.remove(userUuid));
                reviewers.add(r);
            }


            BigInteger topicId = (BigInteger) item[3];

            if (topicId != null) {
                SubmissionTopic topic = new SubmissionTopic();
                topic.setId(topicId.longValue());
                topic.setName((String) item[4]);
                r.getPrefferedTopics().add(topic);
            }


        }
        return reviewers;
    }

    @Override
    public boolean isAlreadyReviewer(String reviewer, String submission) {
        Long id = (Long) session().
                createQuery("select r.id from Review r where r.reviewer.uuid=:RUUID and r.submission.uuid=:SUUID").
                setString("RUUID", reviewer).
                setString("SUUID", submission).
                uniqueResult();
        return id == null ? false : true;
    }

    @Override
    public Integer actualizeScore(Long submissionId, Long artefactId, Long reviewId) {
        // flusheme vsetko do DB
        session().setFlushMode(FlushMode.MANUAL);
        session().flush();
        session().clear();
        RecountScoreWork work = new RecountScoreWork(submissionId, artefactId, reviewId);
        session().doWork(work);
        return work.getScore();

    }

    @Override
    public void deleteReview(String submissionUuid, String reviewerUuid) throws NoRecordException {
        Review r = loadReviewerReview(reviewerUuid, submissionUuid);
        delete(r);
    }

    /*
     * ------------------------------------------------------------------------
     * REVIEWER REVIEWS LIST
     * ------------------------------------------------------------------------
     */
    private static final String REVIEWER_REVIEWS_QUERY = "select s.id, s.uuid, s.title, s.subTitle, s.state, s.submittedDate, s.decidedDate, s.authors, s.agregatedTopics, s.type.name, r.uuid, r.score, r.id from Review r left join r.submission as s where r.reviewer.uuid=:RUUID  ";
    private static final String REVIEWER_REVIEWS_QUERY_ALL = "select count(s.id) from Review r left join r.submission as s where r.reviewer.uuid=:RUUID and s.state >= 0";

    @Override
    public List<SubmissionListItem> listReviewerReviews(Constraints constraints, Paging paging, boolean showAuthors) {
        List<SubmissionListItem> out = Lists.newArrayList();

        // Nacitame si recenzie
        List<Object[]> items = createReviewerReviewsQuery(constraints).
                setFirstResult(paging.getFirstResult()).
                setMaxResults(paging.getMaxResults()).
                list();

        // Spracujeme recenzie            
        List<Long> submissionIds = processReviewerReviews(items, showAuthors, out);

        // Spracuejme recenzie artefaktov
        processReviewerArtefactReviews(submissionIds, constraints.getUserUuid(), out);

        return out;
    }

    private void processReviewerArtefactReviews(List<Long> reviewsToLoad, String reviewerUuid, List<SubmissionListItem> out) throws NullPointerException, HibernateException {
        if (out.isEmpty()) {
            return;
        }
        // Podla vytvoreneho zoznamu recenzii nacitame vsetky hodnotenia k artefaktom ktore uz mame
        List<Object[]> artefacts = session().
                createSQLQuery("select s.id, sa.id, ra.score from conference.submission_artefact sa inner join conference.submission s on s.id=sa.id_submission left join (select ra.* from conference.review_artefact ra left join conference.review r on r.id=ra.id_review left join public.user_event ue on ue.id=r.id_user where ue.uuid=:R_UUID)ra on ra.id_artefact=sa.id where s.id in ( :S_IDS ) order by s.id,sa.created").
                setParameterList("S_IDS", reviewsToLoad).
                setString("R_UUID", reviewerUuid).
                list();

        SubmissionListItem actualListItem = null;
        // Iterujeme cez vsetky nacitane recenzie artefaktov
        for (Object[] artefactWithReview : artefacts) {

            // nacitame si uz vytvorenu recenziu 
            if (actualListItem == null || !actualListItem.getId().equals(artefactWithReview[0])) {
                actualListItem = null;
                for (SubmissionListItem sli : out) {
                    if (BigInteger.valueOf(sli.getId()).compareTo((BigInteger) artefactWithReview[0]) == 0) {
                        actualListItem = sli;
                        break;
                    }
                }
                if (actualListItem == null) {
                    throw new NullPointerException("There is no Review for artefact review?");
                }
            }

            // nastavime artefakt a jeho skore artefaktu
            if (artefactWithReview.length == 2 || artefactWithReview[2] == null) {
                actualListItem.getPartialScore().add(actualListItem.new Evaluation());
            } else {
                actualListItem.getPartialScore().add(actualListItem.new Evaluation((Integer) artefactWithReview[2]));
            }
        }
    }

    private List<Long> processReviewerReviews(List<Object[]> items, boolean showAuthors, List<SubmissionListItem> out) {
        // Nacitame zakladne informacie o prispevkoch a artefaktoch        
        List<Long> submissionIds = Lists.newArrayList();
        SubmissionListItem listItem;
        // nacitame si prispevky ktore recenzent recenzuje
        for (Object[] item : items) {
            submissionIds.add((Long) item[0]);
            listItem = new SubmissionListItem();
            listItem.setId((Long) item[0]);
            listItem.setUuid((String) item[1]);
            listItem.setTitle((String) item[2]);
            listItem.setSubtitle((String) item[3]);
            listItem.setStateEnum((SubmissionStateEnum) item[4]);
            listItem.setSubmitDate((Date) item[5]);
            listItem.setDecideDate((Date) item[6]);
            if (showAuthors) {
                listItem.setAuthors((String) item[7]);
            }
            listItem.setAgregatedTopics((String) item[8]);
            listItem.setType((String) item[9]);
            listItem.setOverallScore(listItem.new Evaluation((Long) item[12], (String) item[10], (Integer) item[11]));
            out.add(listItem);
        }
        // nacitame este vsetkych prispevovatelov
        if (showAuthors) {
            fillSubmissionContributors(submissionIds, out);
        }
        return submissionIds;
    }

    private Query createReviewerReviewsQuery(Constraints constraints) throws HibernateException {
        // Vytvorenie zakladu
        StringBuilder sb = new StringBuilder(REVIEWER_REVIEWS_QUERY);
        Query query;
        // Filtrovanie podla Stavu prispevku
        if (ListUtils.isFilter(ReviewerReviewsList.Filter_STATE, constraints)) {
            sb.append(" and s.state=:STATE ");
            setReviewerReviewsOrderers(constraints, sb);
            query = session().createQuery(sb.toString()).
                    setString("RUUID", constraints.getUserUuid()).
                    setInteger("STATE", constraints.getIntFilterValue());
        } else if (ListUtils.isFilter(ReviewerReviewsList.Filter_TYPE, constraints) && StringUtils.isNotBlank(constraints.getStringFilterValue())) {
            sb.append(" and s.type.uuid=:TYPE_UUID and s.state >= 0 ");
            setReviewerReviewsOrderers(constraints, sb);
            query = session().createQuery(sb.toString()).
                    setString("RUUID", constraints.getUserUuid()).
                    setString("TYPE_UUID", constraints.getStringFilterValue());
        } else {
            // Vsetky prispevky 
            sb.append(" and s.state >= 0 ");
            setReviewerReviewsOrderers(constraints, sb);
            query = session().createQuery(sb.toString()).setString("RUUID", constraints.getUserUuid());
        }
        return query;
    }

    private void setReviewerReviewsOrderers(Constraints constraints, StringBuilder sb) {
        //order by s.title , artefact.created
        if (constraints.getOrderers().length > 0) {
            sb.append(" order by ");
            if (ListUtils.hasOrderer(constraints.getOrderers(), ReviewerReviewsList.Order_TITLE)) {
                sb.append(" s.title ");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), ReviewerReviewsList.Order_SUBMIT_DATE)) {
                sb.append(" s.submittedDate desc ");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), ReviewerReviewsList.Order_SCORE)) {
                sb.append(" s.score");
            }
        }
    }

    @Override
    public int loadReviewerReviewsCount(Constraints constraints) {
        // Vytvorenie zakladu
        StringBuilder sb = new StringBuilder(REVIEWER_REVIEWS_QUERY_ALL);
        Query query;
        // Filtrovanie podla Stavu prispevku
        if (ListUtils.isFilter(ReviewerReviewsList.Filter_STATE, constraints)) {
            sb.append(" and s.state=:STATE");
            query = session().createQuery(sb.toString()).
                    setString("RUUID", constraints.getUserUuid()).
                    setInteger("STATE", filterStateToCoreState(constraints.getIntFilterValue()));
        } else if (ListUtils.isFilter(ReviewerReviewsList.Filter_TYPE, constraints) && StringUtils.isNotBlank(constraints.getStringFilterValue())) {
            sb.append(" and s.type.uuid=:TYPE_UUID ");
            query = session().createQuery(sb.toString()).
                    setString("RUUID", constraints.getUserUuid()).
                    setString("TYPE_UUID", constraints.getStringFilterValue());
        } else {
            // Vsetky prispevky            
            query = session().createQuery(sb.toString()).setString("RUUID", constraints.getUserUuid());
        }
        return ((Long) query.list().get(0)).intValue();
    }
    /*
     * ------------------------------------------------------------------------
     * CHAIR AND CONTRIBUTOR LIST
     * ------------------------------------------------------------------------
     */
    // typ a contributory
    private static final String CONTRIBUTIONS_QUERY = "select s.id, s.uuid, s.title, s.subTitle, s.state, s.submittedDate, s.decidedDate, s.authors, s.agregatedTopics, s.score, s.type.name from Submission s ";
    private static final String CONTRIBUTIONS_QUERY_COUNT = "select count(*) from Submission s ";

    @Override
    public List<SubmissionListItem> listContributions(Constraints constraints, Paging paging, boolean showAuthors, boolean userAsContributor) {
        List<SubmissionListItem> out = Lists.newArrayList();


        // Vytvorenie zakladu        
        StringBuilder sb = new StringBuilder(CONTRIBUTIONS_QUERY);
        Query query = appendQueryConditions(constraints, sb, userAsContributor, true);

        // Ziskanie vysledku zapracovanim strankovaca
        List<Object[]> items = query.setFirstResult(paging.getFirstResult()).setMaxResults(paging.getMaxResults()).list();

        processContributions(items, showAuthors, out);
        return out;
    }

    @Override
    public int loadContributionsCount(Constraints constraints, boolean userAsContributor) {
        // Vytvorenie zakladu
        StringBuilder sb = new StringBuilder(CONTRIBUTIONS_QUERY_COUNT);
        Query query = appendQueryConditions(constraints, sb, userAsContributor, false);
        return ((Long) query.list().get(0)).intValue();
    }

    private Query appendQueryConditions(Constraints constraints, StringBuilder sb, boolean userAsContributor, boolean addOrderers) throws HibernateException {
        Query query;

        if (userAsContributor) {
            sb.append(" join s.contributors as c");
        }

        // Filtrovanie podla Stavu prispevku
        if (ListUtils.isFilter(ContributionsList.Filter_STATE, constraints)) {
            sb.append(" where s.state=:STATE");
            if (userAsContributor) {
                sb.append(" and c.uuid=:UUID ");
            }
            appendContributionListOrderers(constraints, sb, addOrderers);
            query = session().createQuery(sb.toString()).
                    setInteger("STATE", filterStateToCoreState(constraints.getIntFilterValue()));
        } else if (ListUtils.isFilter(ContributionsList.Filter_TYPE, constraints) && StringUtils.isNotBlank(constraints.getStringFilterValue())) {
            sb.append(" where s.type.uuid=:TYPE_UUID and s.state >= 0 ");
            if (userAsContributor) {
                sb.append(" and c.uuid=:UUID ");
            }
            appendContributionListOrderers(constraints, sb, addOrderers);
            query = session().createQuery(sb.toString()).
                    setString("TYPE_UUID", constraints.getStringFilterValue());
        } else {
            // Vsetky prispevky            
            sb.append(" where s.state >= 0 ");
            if (userAsContributor) {
                sb.append(" and c.uuid=:UUID ");
            }
            appendContributionListOrderers(constraints, sb, addOrderers);
            query = session().createQuery(sb.toString());
        }
        if (userAsContributor) {
            query.setString("UUID", constraints.getUserUuid());
        }
        return query;
    }

    private void processContributions(List<Object[]> items, boolean showAuthors, List<SubmissionListItem> out) {
        // Nacitame zakladne informacie o prispevkoch a artefaktoch

        List<Long> submissionIds = Lists.newArrayList();
        SubmissionListItem listItem;
        for (Object[] item : items) {
            submissionIds.add((Long) item[0]);

            listItem = new SubmissionListItem();
            listItem.setId((Long) item[0]);
            listItem.setUuid((String) item[1]);
            listItem.setTitle((String) item[2]);
            listItem.setSubtitle((String) item[3]);
            listItem.setStateEnum((SubmissionStateEnum) item[4]);
            listItem.setSubmitDate((Date) item[5]);
            listItem.setDecideDate((Date) item[6]);
            if (showAuthors) {
                listItem.setAuthors((String) item[7]);
            }
            listItem.setAgregatedTopics((String) item[8]);
            listItem.setOverallScore(listItem.new Evaluation((Integer) item[9]));
            listItem.setType((String) item[10]);
            out.add(listItem);
        }

        // donacitame vsetkych contributorov
        if (showAuthors) {
            fillSubmissionContributors(submissionIds, out);
        }

        // nacitame este vsetky recenzie
        if (!submissionIds.isEmpty()) {
            List<Object[]> reviews = session().
                    createQuery("select s.id, r.uuid, r.score from Review r join r.submission as s where s.id in (:S_IDS) order by s.id,r.id").
                    setParameterList("S_IDS", submissionIds).
                    list();
            listItem = null;
            for (Object[] review : reviews) {
                if (listItem == null || !listItem.getId().equals(review[0])) {
                    for (SubmissionListItem item : out) {
                        if (item.getId().equals(review[0])) {
                            listItem = item;
                            break;
                        }
                    }
                    if (listItem == null) {
                        throw new NullPointerException("There is no list item for review");
                    }
                }
                listItem.getPartialScore().add(listItem.new Evaluation((String) review[1], (Integer) review[2]));
            }
        }
    }

    private void appendContributionListOrderers(Constraints constraints, StringBuilder sb, boolean append) {
        if (append && constraints.getOrderers().length > 0) {
            sb.append(" order by ");
            if (ListUtils.hasOrderer(constraints.getOrderers(), ContributionsList.Order_TITLE)) {
                sb.append(" s.title");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), ContributionsList.Order_SUBMIT_DATE)) {
                sb.append(" s.submittedDate desc ");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), ContributionsList.Order_SCORE)) {
                sb.append(" s.score desc");
            }
        }
    }

    // LIST HELP METHODS
    private static int filterStateToCoreState(int i) {
        States s = States.values()[i];
        switch (s) {
            case ACCEPTED:
                return SubmissionStateEnum.ACCEPTED.getId();
            case REJECTED:
                return SubmissionStateEnum.DECLINED.getId();
            case PENDING:
            default:
                return SubmissionStateEnum.PENDING.getId();
        }
    }

    public void fillSubmissionContributors(List<Long> submissionIds, List<SubmissionListItem> submissions) {
        if (submissionIds.isEmpty()) {
            return;
        }

        List<Object[]> contributors = session().
                createQuery("select s.id, c from Submission s join s.contributors as c where s.id in (:S_IDS) order by s.id").
                setParameterList("S_IDS", submissionIds).
                list();
        SubmissionListItem listItem = null;
        for (Object[] contributor : contributors) {
            if (listItem == null || !listItem.getId().equals(contributor[0])) {
                for (SubmissionListItem item : submissions) {
                    if (item.getId().equals(contributor[0])) {
                        listItem = item;
                        break;
                    }
                }
                if (listItem == null) {
                    throw new NullPointerException("There is no list item for contributor");
                }
            }
            listItem.getContributors().add((UserEvent) contributor[1]);
        }
    }

    private class RecountScoreWork implements Work {

        private final Long submissionId;
        private final Long artefactId;
        private final Long reviewId;
        private int score;

        public RecountScoreWork(Long submissionId, Long artefactId, Long reviewId) {
            this.submissionId = submissionId;
            this.artefactId = artefactId;
            this.reviewId = reviewId;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            CallableStatement call = connection.prepareCall("{ ? = call conference.count_score(?,?,?) }");
            call.registerOutParameter(1, Types.INTEGER); // or whatever it is                
            call.setLong(2, submissionId);
            call.setLong(3, artefactId);
            call.setLong(4, reviewId);
            call.execute();
            score = call.getInt(1); // propagate
        }

        public int getScore() {
            return score;
        }
    }

    @Override
    public void deleteAllReviews() {
        session().createQuery("delete from Review").executeUpdate();
    }
}
