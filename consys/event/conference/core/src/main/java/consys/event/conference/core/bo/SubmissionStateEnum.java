/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.bo;

/**
 *
 * @author palo
 */
public enum SubmissionStateEnum {

    DELETED(-1), PENDING(0), ACCEPTED(1), DECLINED(2);
    private Integer id;

    private SubmissionStateEnum(Integer id) {
        this.id = id;
    }

    public static SubmissionStateEnum fromId(Integer id) {
        switch (id) {
            case 0:
                return PENDING;
            case 1:
                return ACCEPTED;
            case 2:
                return DECLINED;            
            case -1:
                return DELETED;
            default:
                return PENDING;
        }
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return name();
    }
}
