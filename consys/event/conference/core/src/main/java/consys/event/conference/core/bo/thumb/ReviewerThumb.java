package consys.event.conference.core.bo.thumb;

import consys.common.utils.collection.Lists;
import consys.event.conference.core.bo.ReviewerBidInterestEnum;
import consys.event.conference.core.bo.SubmissionTopic;
import java.util.List;


/**
 * Recenzent vzhladom k submissnu
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerThumb {
    private Long id;
    private String userUuid;
    private String fullName;
    private List<SubmissionTopic> prefferedTopics;
    private ReviewerBidInterestEnum interest;
    private int load;
    private boolean reviewing;

    public ReviewerThumb() {
        interest = ReviewerBidInterestEnum.NOT_CARE;
        prefferedTopics = Lists.newArrayList();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the userUuid
     */
    public String getUserUuid() {
        return userUuid;
    }

    /**
     * @param userUuid the userUuid to set
     */
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the prefferedTopics
     */
    public List<SubmissionTopic> getPrefferedTopics() {
        return prefferedTopics;
    }

    /**
     * @param prefferedTopics the prefferedTopics to set
     */
    public void setPrefferedTopics(List<SubmissionTopic> prefferedTopics) {
        this.prefferedTopics = prefferedTopics;
    }

    /**
     * @return the interest
     */
    public ReviewerBidInterestEnum getInterest() {
        return interest;
    }

    /**
     * @param interest the interest to set
     */
    public void setInterest(ReviewerBidInterestEnum interest) {
        this.interest = interest;
    }

    /**
     * @return the load
     */
    public int getLoad() {
        return load;
    }

    /**
     * @param load the load to set
     */
    public void setLoad(int load) {
        this.load = load;
    }

    /**
     * @return the reviewing
     */
    public boolean isReviewing() {
        return reviewing;
    }

    /**
     * @param reviewing the reviewing to set
     */
    public void setReviewing(boolean reviewing) {
        this.reviewing = reviewing;
    }






}
