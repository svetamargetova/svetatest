package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.exception.DeleteRelationsException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface TopicService {

    public SubmissionTopic loadTopic(Long id) throws NoRecordException;

    public void createTopic(SubmissionTopic topic) throws RequiredPropertyNullException,TopicNotUniqueException;

    public void updateTopic(SubmissionTopic topic) throws RequiredPropertyNullException, TopicNotUniqueException;

    public void deleteTopic(SubmissionTopic topic) throws DeleteRelationsException;

    public void deleteTopic(Long id) throws DeleteRelationsException,NoRecordException;

    public void deleteTopicForce(SubmissionTopic topic);

    public List<SubmissionTopic> listTopics();

    public List<Long> listReviewerPreferredTopics(String reviewerUuid);

    public List<SubmissionTopic> listTopicsByList(Collection<Long> ids) throws NoRecordException;

    public void updateReviewerTopics(String reviewerUuid, List<Long> topicsIds) throws NoRecordException;

}
