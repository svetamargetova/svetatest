package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import java.util.List;

/**
 * TODO: chyba reprezentacia graficka a definicia jedneho bodu
 * @author Palo
 */
public class ReviewCriteria implements ConsysObject {

    private static final long serialVersionUID = 8574774279545867901L;
    private Long id;
    private String name;
    private String description;
    private SubmissionTypeArtefact submissionTypeArtefact;
    private int maxPoints; // Maximum bodov == count()
    private int minPoints; // minimum bodov == 1
    private double autoAvergeAccept;
    private double autoMinAccept;    
    private List<ReviewCriteriaItem> items;

    public ReviewCriteria() {
        maxPoints = -1;
        minPoints = -1;        
        autoAvergeAccept = -1;
        autoMinAccept = -1;
        items = Lists.newArrayList();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the items
     */
    public List<ReviewCriteriaItem> getItems() {
        return items;
    }

    public void setItems(List<ReviewCriteriaItem> items) {
        this.items = items;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the submissionTypeArtefact
     */
    public SubmissionTypeArtefact getSubmissionTypeArtefact() {
        return submissionTypeArtefact;
    }

    /**
     * @param submissionTypeArtefact the submissionTypeArtefact to set
     */
    public void setSubmissionTypeArtefact(SubmissionTypeArtefact submissionTypeArtefact) {
        this.submissionTypeArtefact = submissionTypeArtefact;
    }

    /**
     * @return the maxPoints
     */
    public int getMaxPoints() {
        return maxPoints;
    }

    /**
     * @param maxPoints the maxPoints to set
     */
    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
    }

    /**
     * @return the minPoints
     */
    public int getMinPoints() {
        return minPoints;
    }

    /**
     * @param minPoints the minPoints to set
     */
    public void setMinPoints(int minPoints) {
        this.minPoints = minPoints;
    }

    /**
     * @return the autoAvergeAccept
     */
    public double getAutoAvergeAccept() {
        return autoAvergeAccept;
    }

    /**
     * @param autoAvergeAccept the autoAvergeAccept to set
     */
    public void setAutoAvergeAccept(double autoAvergeAccept) {
        this.autoAvergeAccept = autoAvergeAccept;
    }

    /**
     * @return the autoMinAccept
     */
    public double getAutoMinAccept() {
        return autoMinAccept;
    }

    /**
     * @param autoMinAccept the autoMinAccept to set
     */
    public void setAutoMinAccept(double autoMinAccept) {
        this.autoMinAccept = autoMinAccept;
    }   

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ReviewCriteria e = (ReviewCriteria) obj;
        return id.equals(e.id);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("ReviewCriteria[name=%s]", name);
    }
}
