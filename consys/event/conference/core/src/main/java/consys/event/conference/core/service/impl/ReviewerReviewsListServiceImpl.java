package consys.event.conference.core.service.impl;

import consys.common.core.service.SystemPropertyService;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.api.list.ReviewerReviewsList;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import consys.event.conference.core.dao.ReviewDao;
import consys.event.conference.core.service.ReviewerReviewsListService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerReviewsListServiceImpl implements ReviewerReviewsListService, ReviewerReviewsList, ConferenceProperties {

    @Autowired
    private ReviewDao reviewDao;
    @Autowired
    private SystemPropertyService propertyService;

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public List<SubmissionListItem> listItems(Constraints constraints, Paging paging) {
        boolean authors = propertyService.getBoolean(SYSTEM_PROPERTY_VISIBLE_AUTHORS);
        return reviewDao.listReviewerReviews(constraints, paging, authors);
    }

    @Override
    public List<SubmissionListItem> listItemsExport(Constraints constraints, Paging paging) {
        return listItems(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return reviewDao.loadReviewerReviewsCount(constraints);
    } 
}
