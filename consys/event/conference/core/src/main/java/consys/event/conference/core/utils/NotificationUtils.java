/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.conference.core.utils;

import consys.event.common.core.bo.UserEvent;
import consys.event.conference.core.notification.ContributionNotification;
import java.util.Set;

/**
 *
 * @author palo
 */
public class NotificationUtils {
    
    public static void sendNotificationToAllContributors(ContributionNotification notification){
        Set<UserEvent> contributor = notification.getSubmission().getContributors();
        for (UserEvent userEvent : contributor) {
            notification.setTarget(userEvent.getUuid(),true);
            notification.sendNotification();
        }
    }
}
