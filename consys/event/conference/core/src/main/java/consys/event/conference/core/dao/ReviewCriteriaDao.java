package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.conference.core.bo.ReviewCriteria;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ReviewCriteriaDao extends GenericDao<ReviewCriteria>{

    public ReviewCriteria loadCriteriaWithItems(Long id) throws NoRecordException;

    public List<ReviewCriteria> listCriteriaForArtefact(String artefactUuid) throws NoRecordException;
    
    public List<ReviewCriteria> listCriteriaForArtefactType(String artefactTypeUuid) throws NoRecordException;

    public List<ReviewCriteria> listCriteriaBySubmissionArtefact(String submissionArtefactUuid) throws NoRecordException;
    
    public List<ReviewCriteria> listCriteriasForSubmissionType(String submissionTypeUuid) throws NoRecordException;

    public List<ListLongItem> listCriteriaThumbsForType(String typeUuid);

    public void deleteCriteriasForArtefactType(String typeUuid);

}
