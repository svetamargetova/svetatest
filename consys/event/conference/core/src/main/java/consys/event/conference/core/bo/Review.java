package consys.event.conference.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.UserEvent;
import java.util.Date;
import java.util.Set;

/**
 * Recenzia konkretneho prispevku od konkretneho recenzenta
 * <p/>
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Review implements ConsysObject {

    private static final long serialVersionUID = -6845218097303905352L;
    private Long id;
    private String uuid;
    private UserEvent reviewer;
    private Submission submission;
    /** Poznamka pre vybor */
    private String privateComment;
    /** Datum zmeny privantej poznamky */
    private Date privateCommentChange;
    /** Poznamka pre contributorov */
    private String publicComment;
    /** Datum zmeny verejnej poznamky */
    private Date publicCommentChange;
    /** Vyhodnotene artefakty */
    private Set<ReviewArtefact> artefactEvaluations;
    private int score = -1;

    public Review() {
        artefactEvaluations = Sets.newHashSet();
    }

    /**
     * @return the reviewer
     */
    public UserEvent getReviewer() {
        return reviewer;
    }

    /**
     * @param reviewer the reviewer to set
     */
    public void setReviewer(UserEvent reviewer) {
        this.reviewer = reviewer;
    }

    /**
     * @return the submission
     */
    public Submission getSubmission() {
        return submission;
    }

    /**
     * @param submission the submission to set
     */
    public void setSubmission(Submission submission) {
        this.submission = submission;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the artefactEvaluations
     */
    public Set<ReviewArtefact> getArtefactEvaluations() {
        return artefactEvaluations;
    }

    /**
     * @param artefactEvaluations the artefactEvaluations to set
     */
    public void setArtefactEvaluations(Set<ReviewArtefact> artefactEvaluations) {
        this.artefactEvaluations = artefactEvaluations;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Poznamka pre vybor
     * <p/>
     * @return the privateComment
     */
    public String getPrivateComment() {
        return privateComment;
    }

    /**
     * Poznamka pre vybor
     * <p/>
     * @param privateComment the privateComment to set
     */
    public void setPrivateComment(String privateComment) {
        this.privateComment = privateComment;
    }

    /**
     * Datum zmeny privantej poznamky
     * <p/>
     * @return the privateCommentChange
     */
    public Date getPrivateCommentChange() {
        return privateCommentChange;
    }

    /**
     * Datum zmeny privantej poznamky
     * <p/>
     * @param privateCommentChange the privateCommentChange to set
     */
    public void setPrivateCommentChange(Date privateCommentChange) {
        this.privateCommentChange = privateCommentChange;
    }

    /**
     * Poznamka pre contributorov
     * <p/>
     * @return the publicComment
     */
    public String getPublicComment() {
        return publicComment;
    }

    /**
     * Poznamka pre contributorov
     * <p/>
     * @param publicComment the publicComment to set
     */
    public void setPublicComment(String publicComment) {
        this.publicComment = publicComment;
    }

    /**
     * Datum zmeny verejnej poznamky
     * <p/>
     * @return the publicCommentChange
     */
    public Date getPublicCommentChange() {
        return publicCommentChange;
    }

    /**
     * Datum zmeny verejnej poznamky
     * <p/>
     * @param publicCommentChange the publicCommentChange to set
     */
    public void setPublicCommentChange(Date publicCommentChange) {
        this.publicCommentChange = publicCommentChange;
    }

    @Override
    public String toString() {
        return String.format("Review[reviewer=%s submission=%s]", reviewer, submission);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Review e = (Review) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }
}
