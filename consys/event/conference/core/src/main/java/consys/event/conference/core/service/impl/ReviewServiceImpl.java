package consys.event.conference.core.service.impl;

import com.google.common.collect.Lists;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ArtefactReviewThumb;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.bo.thumb.ReviewerThumb;
import consys.event.conference.core.dao.*;
import consys.event.conference.core.service.ReviewService;
import consys.event.conference.core.service.SubmissionService;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewServiceImpl extends AbstractService implements ReviewService {

    // Service
    @Autowired
    private UserEventService userEventService;
    @Autowired
    private SystemPropertyService systemPropertyService;
    @Autowired
    private SubmissionService submissionService;
    // Review Daos
    @Autowired
    private ReviewDao reviewDao;
    @Autowired
    private ReviewerBidDao reviewerBidDao;
    @Autowired
    private ReviewCriteriaDao reviewCriteriaDao;
    @Autowired
    private ReviewArtefactDao reviewArtefactDao;
    @Autowired
    private ReviewCycleDao reviewCycleDao;
    @Autowired
    private ReviewArtefactCriteriaEvaluationDao reviewArtefactCriteriaEvaluationDao;

    /*
     * ------------------------------------------------------------------------
     * --- L O A D ---
     * ------------------------------------------------------------------------
     */
    @Override
    public ReviewCriteria loadCriteriaWithItems(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null || id <= 0L) {
            throw new RequiredPropertyNullException();
        }
        return reviewCriteriaDao.loadCriteriaWithItems(id);
    }

    @Override
    public Review loadReview(String submissionUuid, String reviewerUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(reviewerUuid) || StringUtils.isBlank(submissionUuid)) {
            throw new RequiredPropertyNullException();
        }
        log().debug("Loading review for reviewer: {} submission: {}", reviewerUuid, submissionUuid);
        return reviewDao.loadReviewerReview(reviewerUuid, submissionUuid);
    }

    @Override
    public Review loadReviewByArtefact(String artefactUuid, String reviewerUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(reviewerUuid) || StringUtils.isBlank(artefactUuid)) {
            throw new RequiredPropertyNullException();
        }
        log().debug("Loading review for reviewer: {} artefactUuid: {}", reviewerUuid, artefactUuid);
        return reviewDao.loadReviewerReviewByArtefact(reviewerUuid, artefactUuid);
    }

    @Override
    public Review loadReviewWithArtefactReviewsWithoutCriterias(String submissionUuid, String reviewerUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(submissionUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewDao.loadReviewerReviewWithArtefactsReviewWithoutCriterias(reviewerUuid, submissionUuid);
    }

    @Override
    public Review loadReviewWithArtefactReviewsWithoutCriterias(String reviewUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(reviewUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewDao.loadReviewerReviewWithArtefactsReviewWithoutCriterias(reviewUuid);

    }

    @Override
    public ReviewArtefact loadReviewArtefactEvaluation(String artefactUuid, String reviewerUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(artefactUuid) || StringUtils.isBlank(reviewerUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewArtefactDao.load(artefactUuid, reviewerUuid);
    }
    
    @Override
    public ReviewArtefact loadReviewArtefactEvaluationForReview(String artefactUuid, String reviewUuid)
            throws NoRecordException, RequiredPropertyNullException{
        if (StringUtils.isBlank(artefactUuid) || StringUtils.isBlank(reviewUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewArtefactDao.loadByReview(artefactUuid, reviewUuid);
    }

    /**
     * Nacita artefakty pre prve kolo posielania prispevkov.
     */
    @Override
    public List<ReviewCycle> listReviewCyclesForType(String typeUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(typeUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewCycleDao.listCyclesWithArtefactTypesForSubmissionType(typeUuid);
    }

    /**
     * Nacita len aktivne cykly pre tento typ artefaktu TODO: Do review service
     */
    @Override
    public List<ReviewCycle> listOnlyActiveCyclesForNewSubmission(String typeUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(typeUuid)) {
            throw new RequiredPropertyNullException();
        }
        log().debug("Active cycles and artefacts for new submission of type with uuid: '{}'", typeUuid);

        return reviewCycleDao.listOnlyActiveCyclesForSubmissionForType(typeUuid);
    }

    @Override
    public Map<Long, Map<Long, List<ArtefactReviewThumb>>> loadArtefactCriteriaEvaluations(Long submissionId)
            throws RequiredPropertyNullException {
        if (submissionId == null || submissionId <= 0L) {
            throw new RequiredPropertyNullException("Bad or missing submissionId");
        }
        return reviewArtefactDao.loadArtefactCriteriaEvaluations(submissionId);
    }

    /*
     * ------------------------------------------------------------------------
     * --- L I S T ---
     * ------------------------------------------------------------------------
     */
    @Override
    public List<ReviewCriteria> listCriteriasForSubmissionType(String uuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewCriteriaDao.listCriteriasForSubmissionType(uuid);

    }

    @Override
    public List<ReviewCriteria> listCriteriasForSubmissionTypeArtefact(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewCriteriaDao.listCriteriaForArtefactType(uuid);
    }

    @Override
    public List<String> listUuidsForReviewedArtefacts(String submissionUuid, String reviewerUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(submissionUuid) || StringUtils.isBlank(reviewerUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewArtefactCriteriaEvaluationDao.listUuidsForReviewedArtefacts(reviewerUuid, submissionUuid);
    }

    @Override
    public List<ReviewCriteria> listCriteriasForBySubmissionArtefact(String artefactUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(artefactUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewCriteriaDao.listCriteriaBySubmissionArtefact(artefactUuid);
    }

    @Override
    public List<ReviewArtefactCriteriaEvaluation> listReviewerEvaluationsForArtefact(String submissionArtefatUuid, String reviewerUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(submissionArtefatUuid) || StringUtils.isBlank(reviewerUuid)) {
            throw new RequiredPropertyNullException();
        }
        return reviewArtefactCriteriaEvaluationDao.listEvaluationsFor(submissionArtefatUuid, reviewerUuid);
    }

    @Override
    public List<ReviewerThumb> listReviewersPropertiesToSubmission(String submissionId) throws RequiredPropertyNullException, NoRecordException {
        if (StringUtils.isBlank(submissionId)) {
            throw new RequiredPropertyNullException();
        }
        return reviewDao.listAllReviewers(submissionId);
    }

    @Override
    public List<ContributionReviewerReview> listSubmissionReviews(Long submissionId) throws RequiredPropertyNullException {
        if (submissionId == null || submissionId <= 0) {
            throw new RequiredPropertyNullException();
        }
        return reviewDao.listSubmissionReviewerReviews(submissionId);
    }

    /*
     * ------------------------------------------------------------------------
     * --- C R E A T E ---
     * ------------------------------------------------------------------------
     */
    @Override
    public void createReviewRelation(String submissionUuid, String reviewerUuid) throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException {
        if (StringUtils.isBlank(submissionUuid) || StringUtils.isBlank(reviewerUuid)) {
            throw new RequiredPropertyNullException();
        }

        // Test na to ze uzivatel je recenzent
        if (!userEventService.hasUserRight(reviewerUuid, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER)) {
            log().error("User with UUID '{}' don't have requested role '{}'", reviewerUuid, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER);
            throw new ServiceExecutionFailed();
        }
        // Test na to ze Recenzent uz ma priradeny tento prispevok na recenznovanie
        if (reviewDao.isAlreadyReviewer(reviewerUuid, submissionUuid)) {
            log().warn("Reviewer '{}' is already reviewing submission '{}'", reviewerUuid, submissionUuid);
            return;
        }

        Submission s = submissionService.loadSubmissonByUuid(submissionUuid);
        UserEvent reviewer = userEventService.loadUserEvent(reviewerUuid);

        Review r = new Review();
        r.setReviewer(reviewer);
        r.setSubmission(s);
        r.setUuid(UuidProvider.getUuid());

        log().debug("New review relation {}", r);
        reviewDao.create(r);
    }
    /*
     * ------------------------------------------------------------------------
     * --- U P D A T E ---
     * ------------------------------------------------------------------------
     */

    @Override
    public void updateSubmissionReviewComments(String submissionUuid, String reviewerUuid, String publicComment, String privateComment) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(reviewerUuid) || StringUtils.isBlank(submissionUuid) || StringUtils.isBlank(reviewerUuid)) {
            throw new RequiredPropertyNullException();
        }
        Review r = reviewDao.loadReviewerReview(reviewerUuid, submissionUuid);
        if (!StringUtils.equals(r.getPrivateComment(), privateComment)) {
            r.setPrivateComment(privateComment);
            r.setPrivateCommentChange(DateProvider.getCurrentDate());
        }

        if (!StringUtils.equals(r.getPublicComment(), publicComment)) {
            r.setPublicComment(publicComment);
            r.setPublicCommentChange(DateProvider.getCurrentDate());
        }
        reviewDao.update(r);
    }

    @Override
    public void updateReviewerBid(String reviewerUuid, String submissionUuid, ReviewerBidInterestEnum bid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(reviewerUuid) || StringUtils.isBlank(submissionUuid) || bid == null) {
            throw new RequiredPropertyNullException();
        }

        ReviewerBid reviewerBid = null;
        try {
            // Upravime uz existujuce
            reviewerBid = reviewerBidDao.loadBidFor(submissionUuid, reviewerUuid);
            reviewerBid.setInterest(bid);
            log().debug("Update bid {}", reviewerBid);
            reviewerBidDao.update(reviewerBid);
        } catch (NoRecordException e) {
            Submission s = submissionService.loadSubmissonByUuid(submissionUuid);

            // Vytvorime nove
            reviewerBid = new ReviewerBid();
            // Nastavime submission

            reviewerBid.setSubmission(s);
            // Nastavime recenzenta
            UserEvent reviewer = userEventService.loadUserEvent(reviewerUuid);
            reviewerBid.setReviewer(reviewer);
            // Nastaviem zaujem
            reviewerBid.setInterest(bid);
            log().debug("New {}", reviewerBid);
            reviewerBidDao.create(reviewerBid);
        }
    }

    @Override
    public void updateReviewEvaluation(String reviewUuid, String submissonUuid, String artefactUuid, Map<Long, Long> evaluatedCriteriaValues, String publicComment, String privateComment)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(reviewUuid) || StringUtils.isBlank(submissonUuid) || StringUtils.isBlank(artefactUuid)) {
            throw new RequiredPropertyNullException();
        }
        // kriteria typy artefaktu
        List<ReviewCriteria> artefactCriterias = null;

        log().debug("Review '{}' of artefact '{}'", reviewUuid, artefactUuid);
        // nacitame vsetky potrebne kriteria k vykonaniu recenzie
        // a otestujeme ci mame vsetky       
        try {
            artefactCriterias = reviewCriteriaDao.listCriteriaForArtefact(artefactUuid);
            if (artefactCriterias.size() > evaluatedCriteriaValues.size()) {
                throw new RequiredPropertyNullException("There are missing criterias");
            }
        } catch (NoRecordException e) {
            // ziadne kriteria tak otestujeme ci mame nejake hodnoty a ked nie tak pokracujeme 
            if (!evaluatedCriteriaValues.isEmpty()) {
                throw new NoRecordException("There are no criterias for this artefact type bude we have obtained some");
            }
            artefactCriterias = Lists.newArrayList();
        }


        // nacitame/vytvorime recenziu artefaktu
        Review review = reviewDao.loadReview(reviewUuid);
        ReviewArtefact reviewArtefact = null;
        try {
            reviewArtefact = reviewArtefactDao.loadForReview(artefactUuid, reviewUuid);
        } catch (NoRecordException e) {
            SubmissionArtefact artefact = submissionService.loadArtefact(artefactUuid);


            reviewArtefact = new ReviewArtefact();
            reviewArtefact.setArtefact(artefact);
            reviewArtefact.setReview(review);
            reviewArtefact.setUuid(UuidProvider.getUuid());
            reviewArtefactDao.create(reviewArtefact);
        }

        // update objektu recenzie artefaktu        
        reviewArtefact.setPrivateComment(privateComment);
        reviewArtefact.setPublicComment(publicComment);
        reviewArtefact.setReviewDate(DateProvider.getCurrentDate());
        reviewArtefactDao.update(reviewArtefact);

        // aktualizacia objektu hodnoteni
        for (ReviewCriteria criteria : artefactCriterias) {
            // nacitame vyhodnotene kriterium
            Long criteriaValue = evaluatedCriteriaValues.get(criteria.getId());
            ReviewCriteriaItem criteriaItem = getItemById(criteriaValue, criteria);

            // pokusime sa najst uz vytvorenu recenziu
            ReviewArtefactCriteriaEvaluation evaluation = null;
            for (ReviewArtefactCriteriaEvaluation alreadyEvaluated : reviewArtefact.getCriteriaEvaluations()) {
                if (alreadyEvaluated.getCriteria().getId().equals(criteria.getId())) {
                    evaluation = alreadyEvaluated;
                }
            }
            // vytvorime/aktualizujeme objekt recenzie jednym kriteriom
            if (evaluation == null) {
                evaluation = new ReviewArtefactCriteriaEvaluation();
                evaluation.setReviewArtefact(reviewArtefact);
                evaluation.setCriteria(criteria);
                evaluation.setCriteriaItem(criteriaItem);
                log().debug("Evaluate {}", criteriaItem);
                reviewArtefactCriteriaEvaluationDao.create(evaluation);
            } else {
                log().debug("Already evaluated: old: {} new: {}", evaluation.getCriteriaItem(), criteriaItem);
                reviewArtefactCriteriaEvaluationDao.delete(evaluation);
                evaluation.setCriteriaItem(criteriaItem);
                reviewArtefactCriteriaEvaluationDao.create(evaluation);
            }
        }

        // zavoleme si recenzencu funkciu ktora prepocita skore
        reviewDao.actualizeScore(review.getSubmission().getId(), reviewArtefact.getArtefact().getId(), review.getId());
    }

    private static ReviewCriteriaItem getItemById(Long id, ReviewCriteria criteria) throws NoRecordException {
        for (ReviewCriteriaItem reviewCriteriaItem : criteria.getItems()) {
            if (reviewCriteriaItem.getId().equals(id)) {
                return reviewCriteriaItem;
            }
        }
        throw new NoRecordException("There is no criteria item with id " + id + " in " + criteria);
    }

    /*
     * ------------------------------------------------------------------------
     */
    /*
     * --- D E L E T E ---
     */
    /*
     * ------------------------------------------------------------------------
     */
    @Override
    public void deleteReviewerForSubmission(String submissionUuid, String reviewerUuid, boolean force)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(reviewerUuid) || StringUtils.isBlank(submissionUuid)) {
            throw new RequiredPropertyNullException();
        }



        log().debug("Remove reviewerer '{}' review for submisson: '{}'. If already exists remove by force: {}", new Object[]{reviewerUuid, submissionUuid, force});
        reviewDao.deleteReview(submissionUuid, reviewerUuid);
    }
}
