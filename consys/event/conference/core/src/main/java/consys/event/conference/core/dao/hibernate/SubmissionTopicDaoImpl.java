package consys.event.conference.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.dao.SubmissionTopicDao;
import java.util.Collection;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionTopicDaoImpl  extends GenericDaoImpl<SubmissionTopic> implements SubmissionTopicDao{

    @Override
    public SubmissionTopic load(Long id) throws NoRecordException {
        return load(id, SubmissionTopic.class);
    }

    @Override
    public List<SubmissionTopic> listTopicsByName() {
        return session().createQuery("from SubmissionTopic s order by s.name").list();
    }

    @Override
    public List<SubmissionTopic> listTopicsByList(Collection<Long> topics) throws NoRecordException {
        List<SubmissionTopic> out = session().
                createQuery("from SubmissionTopic s where s.id in (:TOPICS)").
                setParameterList("TOPICS", topics).
                list();
        if(out.size() != topics.size()){
            throw new NoRecordException();
        }
        return out;
    }
    




}
