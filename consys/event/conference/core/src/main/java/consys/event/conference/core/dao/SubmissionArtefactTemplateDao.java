package consys.event.conference.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.bo.SubmissionArtefactTemplate;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SubmissionArtefactTemplateDao extends GenericDao<SubmissionArtefactTemplate>{

    public List<ListUuidItem> listTemplatesForArtefact(Long id);

    public List<ListUuidItem> listTemplatesThumbs() throws NoRecordException;

    public SubmissionArtefactTemplate loadByDataFileUuid(String uuid) throws NoRecordException;

    public List<SubmissionArtefactTemplate> listTemplatesByUuids(List<String> uuids) throws NoRecordException;
}
