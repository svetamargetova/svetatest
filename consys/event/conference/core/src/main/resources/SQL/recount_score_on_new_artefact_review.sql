CREATE OR REPLACE FUNCTION conference.count_score(arg_id_submission bigint, arg_id_artefact bigint, arg_id_review bigint) RETURNS int AS $$

  DECLARE                
    _artefact_average INT;
    _id_review_artefact BIGINT;
  BEGIN        

    -- Zistime ID ktore ma novu recenziu    
    SELECT ra.id INTO _id_review_artefact FROM conference.review_artefact ra WHERE arg_id_artefact=ra.id_artefact and arg_id_review=ra.id_review;
    IF(_id_review_artefact is null) THEN
       RAISE EXCEPTION 'Review artefact for id_review=% and id_artefact=% not exists!',arg_id_review, arg_id_artefact;
    END IF;

    -- Priemer hodnotenia artefaktu v recenzii
    select round( avg((rci.value*100)/(select max(i.value) from conference.review_criteria_item i where i.id_review_criteria=rci.id_review_criteria))) into _artefact_average from conference.review_artefact_criteria_evaluation as ace left join conference.review_criteria_item as rci on rci.id=ace.id_review_criteria_item where ace.id_review_artefact=_id_review_artefact;
    update conference.review_artefact set score=_artefact_average where id=_id_review_artefact;	

    -- Priemer recenzie
    update conference.review set score=(select avg(score) from conference.review_artefact where id_review=arg_id_review and score!=(-1)) where id=arg_id_review;

    -- Priemer artefaktu
    update conference.submission_artefact set score=(select avg(score) from conference.review_artefact where id_artefact=arg_id_artefact and score!=(-1)) where id=arg_id_artefact;

    -- Priemer prispevku
    update conference.submission set score=(select avg(score) from conference.review where id_submission=arg_id_submission and score!=(-1)) where id=arg_id_submission;
        
 RETURN _artefact_average;
END;
$$ LANGUAGE 'plpgsql'
 
