
-- pridame novy riadok
ALTER TABLE conference.review_cycle_artefact ADD COLUMN IDX_ARTEFACT_ORDER INTEGER;

-- vytvorime funkciu ktora inicializuje index od 0 - N
CREATE OR REPLACE FUNCTION conference.migrate_2_7_1_to_2_7_2() RETURNS integer AS $$


	DECLARE
        _id_cycle BIGINT;
        _id_review_cycle BIGINT;
        -- stara registracia
	_review_cycle RECORD;
	_idx integer;


        BEGIN
		-- nacitame si vsetky cykly a postupne cez ne iterujeme
		FOR _id_cycle IN SELECT id FROM conference.REVIEW_CYCLE LOOP
                        -- iterujeme cez jeho artefakty a postupne updatujeme idx_
                        _idx:=0;
                        FOR _review_cycle IN SELECT * FROM conference.REVIEW_CYCLE_ARTEFACT WHERE ID_REVIEW_CYCLE=_id_cycle LOOP
                            -- update postupne
                            UPDATE conference.REVIEW_CYCLE_ARTEFACT SET IDX_ARTEFACT_ORDER=_idx
                                WHERE ID_REVIEW_CYCLE=_review_cycle.ID_REVIEW_CYCLE and ID_SUBMISSION_TYPE_ARTEFACT=_review_cycle.ID_SUBMISSION_TYPE_ARTEFACT;
			    _idx:=_idx+1;
                        END LOOP;
		END LOOP;
                RETURN 0;
        END;
$$ LANGUAGE plpgsql;
select conference.migrate_2_7_1_to_2_7_2();
