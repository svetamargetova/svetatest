package consys.event.conference.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.date.DateProvider;
import consys.common.utils.collection.Lists;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.ReviewCriteria;
import consys.event.conference.core.bo.ReviewCriteriaItem;
import consys.event.conference.core.bo.ReviewCycle;
import consys.event.conference.core.bo.SubmissionArtefactType;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import java.util.Date;
import java.util.Map;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewCycleAndCriteriaDaoTest extends AbstractConferenceTest {

    private SubmissionTypeDao typeDao;
    private SubmissionTypeArtefactDao typeArtefactDao;
    private SubmissionArtefactTypeDao artefactTypeDao;
    private ReviewCycleDao cycleDao;
    private ReviewCriteriaDao criteriaDao;

    @Autowired(required = true)
    public void setCriteriaDao(ReviewCriteriaDao criteriaDao) {
        this.criteriaDao = criteriaDao;
    }

    @Autowired(required = true)
    public void setArtefactTypeDao(SubmissionArtefactTypeDao artefactTypeDao) {
        this.artefactTypeDao = artefactTypeDao;
    }

    @Autowired(required = true)
    public void setCycleDao(ReviewCycleDao cycleDao) {
        this.cycleDao = cycleDao;
    }

    @Autowired(required = true)
    public void setTypeArtefactDao(SubmissionTypeArtefactDao typeArtefactDao) {
        this.typeArtefactDao = typeArtefactDao;
    }

    @Autowired(required = true)
    public void setTypeDao(SubmissionTypeDao typeDao) {
        this.typeDao = typeDao;
    }
  
    @Test(groups = {"dao"})
    public void createCyclesAndCriteriaLoadTest() throws NoRecordException {
        Date nullMinDate = cycleDao.loadFirstSubmissionDate();
        assertNull(nullMinDate, "minimalni datum ma byt null");
        Date nullMaxDate = cycleDao.loadLastSubmissionDate();
        assertNull(nullMaxDate, "maximalni datum ma byt null");

        // create type
        SubmissionType type = new SubmissionType();
        type.setName("A");
        type.setUuid("1");
        type.setOrder(0);
        typeDao.create(type);

        // create artefact 1
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("A");
        artefactTypeDao.create(at1);
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("B");
        artefactTypeDao.create(at2);

        // create type artefacts
        SubmissionTypeArtefact sta1 = new SubmissionTypeArtefact();
        sta1.setArtefactType(at1);
        sta1.setType(type);
        sta1.setUuid("1");
        typeArtefactDao.create(sta1);
        SubmissionTypeArtefact sta2 = new SubmissionTypeArtefact();
        sta2.setArtefactType(at2);
        sta2.setType(type);
        sta2.setUuid("2");
        typeArtefactDao.create(sta2);
        SubmissionTypeArtefact sta3 = new SubmissionTypeArtefact();
        sta3.setArtefactType(at2);
        sta3.setType(type);
        sta3.setUuid("3");
        typeArtefactDao.create(sta3);

        // set and update type
        type.getArtefacts().add(sta1);
        type.getArtefacts().add(sta2);
        type.getArtefacts().add(sta3);
        typeDao.update(type);

        Date minDate = DateUtils.addDays(DateProvider.getCurrentDate(), -5);
        Date maxDate = DateUtils.addDays(DateProvider.getCurrentDate(), +10);

        // Create cycles
        ReviewCycle c1 = new ReviewCycle();
        c1.setSubmissionType(type);
        c1.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), +5));
        c1.setSubmitTo(maxDate);
        cycleDao.create(c1);
        ReviewCycle c2 = new ReviewCycle();
        c2.setSubmissionType(type);
        c2.setSubmitFrom(minDate);
        c2.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +5));
        c2.getArtefacts().add(sta1);
        cycleDao.create(c2);
        ReviewCycle c3 = new ReviewCycle();
        c3.setSubmissionType(type);
        c3.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), +5));
        c3.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +7));
        c3.getArtefacts().add(sta2);
        cycleDao.create(c3);

        // create criteria
        ReviewCriteria cr1 = new ReviewCriteria();
        cr1.setName("A");
        cr1.setSubmissionTypeArtefact(sta1);
        criteriaDao.create(cr1);
        ReviewCriteria cr2 = new ReviewCriteria();
        cr2.setName("A");
        cr2.setSubmissionTypeArtefact(sta2);
        criteriaDao.create(cr2);

        Date resultMinDate = cycleDao.loadFirstSubmissionDate();
        Date resultMaxDate = cycleDao.loadLastSubmissionDate();

        assertEquals(resultMinDate, minDate, "Nesedi nejdrivejsi datum zaslani");
        assertEquals(resultMaxDate, maxDate, "Nesedi nejpozdejsi datum zaslani");

        // TEST nacitanie ID cyklov pre arteafakty
        Map<String, Long> cycles = cycleDao.listReviewCycleIdsFor(type.getUuid(), Lists.newArrayList(sta1.getUuid(), sta2.getUuid()));
        assertTrue(cycles.size() == 2);
        assertEquals(cycles.get(sta1.getUuid()),c2.getId());
        assertEquals(cycles.get(sta2.getUuid()),c3.getId());

        // Test na update
        



        // start testing
        // Test load thumbs by type artefact uuid
        // List<ReviewCycleThumb> thumbs = cycleDao.listCycleThumbForType(type.getUuid());
        // assertTrue(thumbs.size() == 2);

//        List<SubmissionTypeArtefact> artefacts = artefactTypeDao.listFirstCycleArtefactsForSubmissionType(type.getUuid());
        //     assertTrue(artefacts.size() == 1);
        //   assertEquals(artefacts.get(0), sta1);

    }

    @Test(groups = {"dao"})
    public void deleteCriterias() {

        // create type
        SubmissionType type = new SubmissionType();
        type.setName("A");
        type.setUuid("1");
        type.setOrder(0);
        typeDao.create(type);

        // create artefact 1
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("A");
        artefactTypeDao.create(at1);

        // create type artefacts
        SubmissionTypeArtefact sta1 = new SubmissionTypeArtefact();
        sta1.setArtefactType(at1);
        sta1.setType(type);
        sta1.setUuid("1");
        typeArtefactDao.create(sta1);

        // create criteria
        ReviewCriteria cr1 = new ReviewCriteria();
        cr1.setName("A");
        cr1.setSubmissionTypeArtefact(sta1);
        ReviewCriteria cr2 = new ReviewCriteria();
        cr2.setName("A");
        cr2.setSubmissionTypeArtefact(sta1);
        for (int i = 0; i < 5; i++) {
            ReviewCriteriaItem itemC1 = new ReviewCriteriaItem();
            itemC1.setName("A");
            itemC1.setValue(i);
            itemC1.setCriteria(cr1);
            cr1.getItems().add(itemC1);

            ReviewCriteriaItem itemC2 = new ReviewCriteriaItem();
            itemC2.setName("A");
            itemC2.setValue(i);
            itemC2.setCriteria(cr2);
            cr2.getItems().add(itemC2);
        }
        criteriaDao.create(cr1);
        criteriaDao.create(cr2);

        criteriaDao.deleteCriteriasForArtefactType(sta1.getUuid());

    }
}
