package consys.event.conference.core.dao;

import org.hibernate.Hibernate;
import consys.common.core.exception.NoRecordException;
import consys.common.utils.date.DateProvider;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.SubmissionArtefactPattern;
import consys.event.conference.core.bo.SubmissionArtefactTemplate;
import consys.event.conference.core.bo.SubmissionArtefactType;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class PatternAndTemplateDaoTest extends AbstractConferenceTest {

    private SubmissionTypeDao typeDao;
    private SubmissionTypeArtefactDao typeArtefactDao;
    private SubmissionArtefactTypeDao artefactTypeDao;
    private ReviewCycleDao cycleDao;
    private ReviewCriteriaDao criteriaDao;
    private SubmissionArtefactPatternDao patternDao;
    private SubmissionArtefactTemplateDao templateDao;

    @Autowired(required = true)
    public void setTemplateDao(SubmissionArtefactTemplateDao templateDao) {
        this.templateDao = templateDao;
    }

    @Autowired(required = true)
    public void setPatternDao(SubmissionArtefactPatternDao patternDao) {
        this.patternDao = patternDao;
    }

    @Autowired(required = true)
    public void setCriteriaDao(ReviewCriteriaDao criteriaDao) {
        this.criteriaDao = criteriaDao;
    }

    @Autowired(required = true)
    public void setArtefactTypeDao(SubmissionArtefactTypeDao artefactTypeDao) {
        this.artefactTypeDao = artefactTypeDao;
    }
 
    @Autowired(required = true)
    public void setCycleDao(ReviewCycleDao cycleDao) {
        this.cycleDao = cycleDao;
    }

    @Autowired(required = true)
    public void setTypeArtefactDao(SubmissionTypeArtefactDao typeArtefactDao) {
        this.typeArtefactDao = typeArtefactDao;
    }

    @Autowired(required = true)
    public void setTypeDao(SubmissionTypeDao typeDao) {
        this.typeDao = typeDao;
    }

    @Test(groups = {"dao"})
    public void createAndLoadPatternsTemplates() throws NoRecordException {

        


    }
}
