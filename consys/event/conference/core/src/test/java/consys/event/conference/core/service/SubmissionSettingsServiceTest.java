package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.bo.thumb.ListUuidItem;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.dao.*;
import consys.event.conference.core.exception.*;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionSettingsServiceTest extends AbstractConferenceTest {

    
    @Autowired    
    private SubmissionTypeArtefactDao typeArtefactDao;
    @Autowired
    private SubmissionTypeDao typeDao;
    @Autowired
    private SubmissionArtefactTypeDao artefactTypeDao;
    @Autowired
    private ReviewCycleDao cycleDao;
    @Autowired
    private ReviewCriteriaDao criteriaDao;
    

  
    @Test(groups = {"service"})
    public void testCreateTypes() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException {
        SubmissionType t1 = new SubmissionType();
        t1.setName("A");
        SubmissionType t2 = new SubmissionType();
        t2.setName("B");
        SubmissionType t3 = new SubmissionType();
        t3.setName("C");

        settingsService.createSubmissionTypes(Lists.newArrayList(t1, t2, t3));
        assertTrue((t1.getOrder() < t2.getOrder()) && (t2.getOrder() < t3.getOrder()));

        List<ListUuidItem> types = settingsService.listSettingsThumbTypes();
        assertTrue(types.size() == 3);
        assertEquals(types.get(0).getUuid(), t1.getUuid());
        assertEquals(types.get(1).getUuid(), t2.getUuid());
        assertEquals(types.get(2).getUuid(), t3.getUuid());

        SubmissionType s1l = submissionService.loadSubmissionTypeWithArtefactsAndCyclesByUuid(t1.getUuid());
        assertEquals(t1, s1l);
    }

    @Test(groups = {"service"})
    public void testCreateArtefactTypes() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException, DeleteCriteriaException, DeleteCycleException {
        SubmissionType t1 = new SubmissionType();
        t1.setName("A");

        settingsService.createSubmissionType(t1);



        SubmissionTypeArtefact a1 = new SubmissionTypeArtefact();
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("Abstract");
        a1.setArtefactType(at1);

        SubmissionTypeArtefact a2 = new SubmissionTypeArtefact();
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("Keywords");
        a2.setArtefactType(at2);

        settingsService.createSubmissionArtefactTypes(t1.getUuid(), Lists.newArrayList(a1, a2));

        ReviewCriteria c1 = new ReviewCriteria();
        c1.setName("C1");
        ReviewCriteriaItem item = new ReviewCriteriaItem();
        item.setName("Dobre");
        c1.getItems().add(item);
        item = new ReviewCriteriaItem();
        item.setName("Dobre");
        c1.getItems().add(item);


        settingsService.createReviewCriterias(a1.getUuid(), Lists.newArrayList(c1));

        // vytvorime dva revew cykle
        ReviewCycle rc1 = new ReviewCycle();
        rc1.setSubmitFrom(DateProvider.getCurrentDate());
        rc1.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), 2));

        ReviewCycle rc2 = new ReviewCycle();
        rc2.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), 2));
        rc2.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), 5));

        ReviewCycle rc3 = new ReviewCycle();
        rc3.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), 6));
        rc3.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), 8));

        settingsService.createReviewCycles(t1.getUuid(), Lists.newArrayList(rc1, rc2, rc3));


        //
        SubmissionType lt = submissionService.loadSubmissionTypeWithArtefactsAndCyclesByUuid(t1.getUuid());
        assertEquals(lt.getArtefacts().size(), 2);
        assertEquals(lt.getCycles().size(), 3);



        SubmissionTypeArtefact at2l = typeArtefactDao.loadArtefactWithTypeByUuid(a2.getUuid());
        assertEquals(a2, at2l);

        settingsService.deleteSubmissionTypeArtefact(a1.getUuid());

        SubmissionType t1l = submissionService.loadSubmissionTypeWithArtefactsAndCyclesByUuid(t1.getUuid());
        assertEquals(t1l.getArtefacts().size(), 1);
    }

    @Test(groups = {"service"})
    public void testDeleteSubmissionType() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException, DeleteRelationsException {
        SubmissionType t1 = new SubmissionType();
        t1.setName("A");

        settingsService.createSubmissionType(t1);



        SubmissionTypeArtefact a1 = new SubmissionTypeArtefact();
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("Abstract");
        a1.setArtefactType(at1);
        SubmissionTypeArtefact a2 = new SubmissionTypeArtefact();
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("Keywords");
        a2.setArtefactType(at2);
        settingsService.createSubmissionArtefactTypes(t1.getUuid(), Lists.newArrayList(a1, a2));

        settingsService.deleteSubmissionType(t1.getUuid());
    }

    @Test(groups = {"service"})
    public void loadArtefactTypeThumb() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException, DeleteCycleException, DeleteCriteriaException, ServiceExecutionFailed, ArtefactInDifferentCycleException {
        SubmissionType t1 = new SubmissionType();
        t1.setName("A");
        settingsService.createSubmissionType(t1);

        // Vytvorime Artefakty a k nim typu pre typ 1
        SubmissionTypeArtefact a1 = new SubmissionTypeArtefact();
        SubmissionTypeArtefact a2 = new SubmissionTypeArtefact();

        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("A");
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("B");

        a1.setArtefactType(at1);
        a2.setArtefactType(at2);

        

        artefactTypeDao.create(at1);
        artefactTypeDao.create(at2);



        // create submission type
        settingsService.createSubmissionArtefactTypes(t1.getUuid(), Lists.newArrayList(a1, a2));

        // create cycles
        ReviewCycle c1 = new ReviewCycle();
        c1.setSubmissionType(t1);
        //c1.setSubmitFrom(DateProvider.getCurrentDate()); // ododneska.. aj to sa testuje ==
        Date fromToday = DateUtils.setHours(DateProvider.getCurrentDate(), 0);
        fromToday = DateUtils.setMinutes(fromToday, 0);
        fromToday = DateUtils.setSeconds(fromToday, 0);
        c1.setSubmitFrom(fromToday);
        c1.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +2));
        logger.error(fromToday.toString());
        logger.error(DateProvider.getCurrentDate().toString());
        cycleDao.create(c1);
        ReviewCycle c2 = new ReviewCycle();
        c2.setSubmissionType(t1);
        c2.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), +1));
        c2.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +2));
        cycleDao.create(c2);
        ReviewCycle c3 = new ReviewCycle();
        c3.setSubmissionType(t1);
        c3.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), +1));
        c3.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +2));
        cycleDao.create(c3);

        String submissionTypeUuid = t1.getUuid();
        


        // create criteria
        ReviewCriteria cr1 = new ReviewCriteria();
        cr1.setName("A");
        cr1.setSubmissionTypeArtefact(a1);
        criteriaDao.create(cr1);

        // TEST LOAD THUMB FROM SERVICE
        settingsService.loadTypeArtefactThumbByUuid(a1.getUuid());

        a1.setNumOfReviewers(5);
        a1.getArtefactType().setName("BB");
        settingsService.updateSubmissionArtefactType(a1, null, null);

        SubmissionTypeArtefact a = settingsService.loadTypeArtefactWithArtefactByUuid(a1.getUuid());
        assertTrue(a.getNumOfReviewers() == 5);
        assertEquals(a.getArtefactType().getName(), a1.getArtefactType().getName());

        // TEST na naitanie nepriradenych artefaktov
        List<String> uuids = settingsService.listNotAssignedArtefactsUuids(t1.getId());
        assertEquals(uuids.size(), 2);
        
        
        // TEST Update cycle - vlozime ze je spravovany cykom 1
        settingsService.updateReviewCycle(c1, Lists.newArrayList(a1.getUuid()));
        uuids = settingsService.listNotAssignedArtefactsUuids(t1.getId());
        assertEquals(uuids.size(), 1);

        // TEST Update cycle - vlozime ze a1 je spravovany cyklom c1 opat
        settingsService.updateReviewCycle(c1, Lists.newArrayList(a1.getUuid()));

        // TEST Update cycle - vlozime ze a1 je spravovany cyklom c2 - CHYBA
        try {
            settingsService.updateReviewCycle(c2, Lists.newArrayList(a1.getUuid()));
            fail();
        } catch (ArtefactInDifferentCycleException e) {
        }

        // TEST Update cycle - vlozime ze a2 je spravovany cyklom c1 tj. zmaze sa a2 a vlozime ze c2 spravuje teraz a1
        settingsService.updateReviewCycle(c1, Lists.newArrayList(a2.getUuid()));
        settingsService.updateReviewCycle(c2, Lists.newArrayList(a1.getUuid()));
        uuids = settingsService.listNotAssignedArtefactsUuids(t1.getId());        
        assertEquals(uuids.size(), 0);
        

        // TEST nacitanie pre odoslavaci formular
        List<ReviewCycle> cycles = reviewService.listReviewCyclesForType(submissionTypeUuid);
        assertTrue(cycles.size() == 3);

        // TEST nacitanie aktivnych cyklov pre odoslavaci formular
        cycles = reviewService.listOnlyActiveCyclesForNewSubmission(submissionTypeUuid);
        assertTrue(cycles.size() == 1);
        
        // TEST DELETE
        settingsService.deleteSubmissionTypeArtefact(a1.getUuid());

        // Nacitame submission type
        SubmissionType type = submissionService.loadSubmissionTypeWithArtefactsAndCyclesByUuid(submissionTypeUuid);        
        assertEquals(type.getArtefacts().size(), 1);
        
    }
    
    
    @Test(groups = {"service"})
    public void updateArtefactOrderTest() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException, DeleteCycleException, DeleteCriteriaException, ServiceExecutionFailed, ArtefactInDifferentCycleException {
        SubmissionType t1 = new SubmissionType();
        t1.setName("A");
        settingsService.createSubmissionType(t1);

        // Vytvorime Artefakty a k nim typu pre typ 1
        SubmissionTypeArtefact a1 = new SubmissionTypeArtefact();
        SubmissionTypeArtefact a2 = new SubmissionTypeArtefact();
        SubmissionTypeArtefact a3 = new SubmissionTypeArtefact();
        SubmissionTypeArtefact a4 = new SubmissionTypeArtefact();

        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("A");
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("B");
        SubmissionArtefactType at3 = new SubmissionArtefactType();
        at3.setName("C");
        SubmissionArtefactType at4 = new SubmissionArtefactType();
        at4.setName("D");

        a1.setArtefactType(at1);
        a2.setArtefactType(at2);
        a3.setArtefactType(at3);
        a4.setArtefactType(at4);

       

        artefactTypeDao.create(at1);
        artefactTypeDao.create(at2);
        artefactTypeDao.create(at3);
        artefactTypeDao.create(at4);



        // create submission type
        settingsService.createSubmissionArtefactTypes(t1.getUuid(), Lists.newArrayList(a1, a2, a3, a4));

        // create cycles
        ReviewCycle c1 = new ReviewCycle();
        c1.setSubmissionType(t1);
        //c1.setSubmitFrom(DateProvider.getCurrentDate()); // ododneska.. aj to sa testuje ==
        Date fromToday = DateUtils.setHours(DateProvider.getCurrentDate(), 0);
        fromToday = DateUtils.setMinutes(fromToday, 0);
        fromToday = DateUtils.setSeconds(fromToday, 0);
        c1.setSubmitFrom(fromToday);
        c1.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +2));
        logger.error(fromToday.toString());
        logger.error(DateProvider.getCurrentDate().toString());
        cycleDao.create(c1);
        ReviewCycle c2 = new ReviewCycle();
        c2.setSubmissionType(t1);
        c2.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), +1));
        c2.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +2));
        cycleDao.create(c2);
        ReviewCycle c3 = new ReviewCycle();
        c3.setSubmissionType(t1);
        c3.setSubmitFrom(DateUtils.addDays(DateProvider.getCurrentDate(), +1));
        c3.setSubmitTo(DateUtils.addDays(DateProvider.getCurrentDate(), +2));
        cycleDao.create(c3);
               

        settingsService.updateReviewCycle(c1, Lists.newArrayList(a1.getUuid(),a2.getUuid(), a3.getUuid(), a4.getUuid()));                
        c1 = settingsService.loadReviewCycleWithTypeById(c1.getId());
        assertEquals(a1.getUuid(), c1.getArtefacts().get(0).getUuid());
        assertEquals(a2.getUuid(), c1.getArtefacts().get(1).getUuid());
        assertEquals(a3.getUuid(), c1.getArtefacts().get(2).getUuid());
        assertEquals(a4.getUuid(), c1.getArtefacts().get(3).getUuid());
        
        settingsService.updateReviewCycle(c1, Lists.newArrayList(a2.getUuid(),a1.getUuid(), a4.getUuid(),a3.getUuid()));        
        c1 = settingsService.loadReviewCycleWithTypeById(c1.getId());
        assertEquals(a2.getUuid(), c1.getArtefacts().get(0).getUuid());
        assertEquals(a1.getUuid(), c1.getArtefacts().get(1).getUuid());
        assertEquals(a4.getUuid(), c1.getArtefacts().get(2).getUuid());
        assertEquals(a3.getUuid(), c1.getArtefacts().get(3).getUuid());
        
        settingsService.updateReviewCycle(c1, Lists.newArrayList(a4.getUuid(),a3.getUuid(), a2.getUuid(),a1.getUuid()));        
        c1 = settingsService.loadReviewCycleWithTypeById(c1.getId());
        assertEquals(a4.getUuid(), c1.getArtefacts().get(0).getUuid());
        assertEquals(a3.getUuid(), c1.getArtefacts().get(1).getUuid());
        assertEquals(a2.getUuid(), c1.getArtefacts().get(2).getUuid());
        assertEquals(a1.getUuid(), c1.getArtefacts().get(3).getUuid());                                       
    }
    
    
    
}
