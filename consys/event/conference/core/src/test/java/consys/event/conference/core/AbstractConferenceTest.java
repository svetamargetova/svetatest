package consys.event.conference.core;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.SystemPropertyService;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.service.UserEventService;
import consys.event.common.test.AbstractEventDaoTestCase;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ArtefactData;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import consys.event.conference.core.service.*;
import consys.event.conference.core.service.cache.SubmissionArtefactCache;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

/**
 *
 * @author Palo
 */
@ContextConfiguration(locations = {"classpath:/consys/event/common/core/event-common-spring.xml", "event-conference-spring.xml", "classpath:consys/event/common/test/core/service-mocks.xml"})
public abstract class AbstractConferenceTest extends AbstractEventDaoTestCase {

    protected static final String REVIEWER_1 = "1";
    protected static final String REVIEWER_2 = "2";
    protected static final String AUTHOR_1 = "3";
    protected static final String AUTHOR_2 = "4";
    // Sluzby
    @Autowired
    protected UserEventService eventService;
    @Autowired
    protected ReviewService reviewService;
    @Autowired
    protected SubmissionSettingsService settingsService;
    @Autowired
    protected TopicService topicService;
    @Autowired
    protected ReviewerReviewsListService myReviewsListService;
    @Autowired
    protected BidForReviewListService bidForReviewListService;
    @Autowired
    protected ContributorContributionsListService mySubmissionsListService;
    @Autowired
    protected ContributionsListService reviewsListService;
    @Autowired
    protected SubmissionService submissionService;
    @Autowired
    protected SystemPropertyService propertyService;
    @Autowired
    protected SubmissionArtefactCache artefactCache;

    public List<Submission> prepareData() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, TopicNotUniqueException, NoRecordException, ServiceExecutionFailed {
        // aby sme pretestovali aj visible authors
        propertyService.updateBooleanProperty(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS, Boolean.TRUE);

        // Vytvorime si typy prispevkov
        SubmissionType st1 = new SubmissionType();
        st1.setName("Type 1");
        settingsService.createSubmissionType(st1);

        SubmissionType st2 = new SubmissionType();
        st2.setName("Type 2");
        settingsService.createSubmissionType(st2);

        // Vytvorime topicy
        SubmissionTopic t1 = new SubmissionTopic();
        t1.setName("t1");
        topicService.createTopic(t1);
        SubmissionTopic t2 = new SubmissionTopic();
        t2.setName("t2");
        topicService.createTopic(t2);
        SubmissionTopic t3 = new SubmissionTopic();
        t3.setName("t3");
        topicService.createTopic(t3);
        SubmissionTopic t4 = new SubmissionTopic();
        t4.setName("t4");
        topicService.createTopic(t4);
        SubmissionTopic t5 = new SubmissionTopic();
        t5.setName("t5");
        topicService.createTopic(t5);
        SubmissionTopic t6 = new SubmissionTopic();
        t6.setName("t6");
        topicService.createTopic(t6);

        // vytvorime dva typy artefaktov a natavime ich
        SubmissionArtefactType artefactType1 = new SubmissionArtefactType();
        artefactType1.setName("Abstract");
        SubmissionArtefactType artefactType2 = new SubmissionArtefactType();
        artefactType2.setName("Paper");


        // Pridame do submisson typov                        
        SubmissionTypeArtefact typeArtefact1_1 = new SubmissionTypeArtefact();
        typeArtefact1_1.setArtefactType(artefactType1);
        typeArtefact1_1.setRequired(true);
        SubmissionTypeArtefact typeArtefact1_2 = new SubmissionTypeArtefact();
        typeArtefact1_2.setArtefactType(artefactType2);
        typeArtefact1_2.setRequired(true);
        settingsService.createSubmissionArtefactTypes(st1.getUuid(), Lists.newArrayList(typeArtefact1_1, typeArtefact1_2));

        SubmissionTypeArtefact typeArtefact2_1 = new SubmissionTypeArtefact();
        typeArtefact2_1.setArtefactType(artefactType1);
        typeArtefact2_1.setRequired(true);
        SubmissionTypeArtefact typeArtefact2_2 = new SubmissionTypeArtefact();
        typeArtefact2_2.setArtefactType(artefactType2);
        typeArtefact2_2.setRequired(true);
        settingsService.createSubmissionArtefactTypes(st2.getUuid(), Lists.newArrayList(typeArtefact2_1, typeArtefact2_2));

        // vytvorime 4 kriteria po 5 hodnotach
        // CRITERIUM 1
        ReviewCriteria criteria1 = new ReviewCriteria();
        criteria1.setName("C1 for A1");
        criteria1.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria1.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria1.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria1.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria1.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria2 = new ReviewCriteria();
        criteria2.setName("C2 for A1");
        criteria2.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria2.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria2.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria2.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria2.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria3 = new ReviewCriteria();
        criteria3.setName("C3 for A1");
        criteria3.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria3.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria3.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria3.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria3.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria4 = new ReviewCriteria();
        criteria4.setName("C4 for A1");
        criteria4.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria4.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria4.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria4.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria4.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria5 = new ReviewCriteria();
        criteria5.setName("C5 for A1");
        criteria5.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria5.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria5.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria5.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria5.getItems().add(new ReviewCriteriaItem("4", 4));
        settingsService.createReviewCriterias(typeArtefact1_1.getUuid(), Lists.newArrayList(criteria1, criteria2, criteria3, criteria4, criteria5));

        criteria1 = new ReviewCriteria();
        criteria1.setName("C1 for A2");
        criteria1.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria1.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria1.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria1.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria1.getItems().add(new ReviewCriteriaItem("4", 4));


        // CRITERIUM 2
        criteria2 = new ReviewCriteria();
        criteria2.setName("C2 for A2");
        criteria2.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria2.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria2.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria2.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria2.getItems().add(new ReviewCriteriaItem("4", 4));
        settingsService.createReviewCriterias(typeArtefact1_2.getUuid(), Lists.newArrayList(criteria1, criteria2));


        // Vytvorime recenzentov
        UserEvent r1 = new UserEvent();
        r1.setFullName("1");
        r1.setLastName("2");
        r1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        r1.setUuid(REVIEWER_1);
        UserEvent r2 = new UserEvent();
        r2.setFullName("1");
        r2.setLastName("2");
        r2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        r2.setUuid(REVIEWER_2);
        eventService.createUserEventWithRights(r1, new String[]{EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER});
        eventService.createUserEventWithRights(r2, new String[]{EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER});

        // Vytvorime co-authorov
        UserEvent a1 = new UserEvent();
        a1.setFullName("1");
        a1.setLastName("2");
        a1.setUuid(AUTHOR_1);
        a1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        UserEvent a2 = new UserEvent();
        a2.setFullName("1");
        a2.setLastName("2");
        a2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        a2.setUuid(AUTHOR_2);
        eventService.createUserEventWithRights(a1, ArrayUtils.EMPTY_STRING_ARRAY);
        eventService.createUserEventWithRights(a2, ArrayUtils.EMPTY_STRING_ARRAY);

        // Vytvorime prispevok 1: A1
        Submission s1 = new Submission();
        s1.setUuid(submissionService.createSubmissionUuid());
        s1.setTitle("Title 1");
        s1.getTopics().add(t1);
        s1.getTopics().add(t2);
        s1.setType(st1);

        SubmissionArtefact artefact1 = new SubmissionArtefact();
        artefact1.setTypeArtefact(typeArtefact1_1);
        artefact1.setUuid(submissionService.createSubmissionArtefact(s1.getUuid(), typeArtefact1_1.getUuid(), generate()));
        s1.getArtefacts().add(artefact1);

        SubmissionArtefact artefact2 = new SubmissionArtefact();
        artefact2.setTypeArtefact(typeArtefact1_2);
        artefact2.setUuid(submissionService.createSubmissionArtefact(s1.getUuid(), typeArtefact1_2.getUuid(), generate()));
        s1.getArtefacts().add(artefact2);

        Set<Invitation> contributors = Sets.newHashSet();
        contributors.add(new ExistingUserInvitation(a1.getUuid()));
        submissionService.createSubmission(s1, contributors);

        // Vytvorime prispevok 2: A1 A2
        Submission s2 = new Submission();
        s2.setTitle("Title 2");
        s2.getTopics().add(t3);
        s2.getTopics().add(t4);
        s2.setUuid(UuidProvider.getUuid());
        s2.setType(st1);

        artefact1 = new SubmissionArtefact();
        artefact1.setTypeArtefact(typeArtefact1_1);
        artefact1.setUuid(submissionService.createSubmissionArtefact(s2.getUuid(), typeArtefact1_1.getUuid(), generate()));
        s2.getArtefacts().add(artefact1);

        artefact2 = new SubmissionArtefact();
        artefact2.setTypeArtefact(typeArtefact1_2);
        artefact2.setUuid(submissionService.createSubmissionArtefact(s2.getUuid(), typeArtefact1_2.getUuid(), generate()));
        s2.getArtefacts().add(artefact2);

        contributors = Sets.newHashSet();
        contributors.add(new ExistingUserInvitation(a1.getUuid()));
        contributors.add(new ExistingUserInvitation(a2.getUuid()));
        submissionService.createSubmission(s2, contributors);

        // Vytvorime prispevok 2: A2       

        return Lists.newArrayList(s1, s2);
    }

    public static ArtefactData generate() {
        ArtefactData file = new ArtefactData();
        file.setContentType("jpeg");
        file.setFileName("fileName");
        file.setByteData("test".getBytes());
        return file;
    }
}
