package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.ReviewerPreferredTopic;
import consys.event.conference.core.bo.SubmissionTopic;
import consys.event.conference.core.dao.ReviewerPreferredTopicDao;
import consys.event.conference.core.exception.DeleteRelationsException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import java.util.List;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TopicServiceTest extends AbstractConferenceTest {


    @Autowired
    private UserEventService userEventService;
    @Autowired
    private ReviewerPreferredTopicDao preferredTopicDao;


    @Test(groups = {"service"})
    public void testCreateTopic() throws RequiredPropertyNullException, TopicNotUniqueException {
        SubmissionTopic topic = new SubmissionTopic();
        topic.setName("A");
        topicService.createTopic(topic);
        assertTrue(topic.getId() > 0L);

        SubmissionTopic sameNameTopic = new SubmissionTopic();
        sameNameTopic.setName("A");
        try {
            topicService.createTopic(sameNameTopic);
            fail();
        } catch (TopicNotUniqueException e) {
        }

        SubmissionTopic nullNameTopic = new SubmissionTopic();
        try {
            topicService.createTopic(nullNameTopic);
            fail();
        } catch (RequiredPropertyNullException e) {
        }
    }

    @Test(groups = {"service"})
    public void testUpdateTopic() throws RequiredPropertyNullException, TopicNotUniqueException {
        SubmissionTopic topic = new SubmissionTopic();
        topic.setName("A");
        topicService.createTopic(topic);
        assertTrue(topic.getId() > 0L);

        SubmissionTopic otherTopic = new SubmissionTopic();
        otherTopic.setName("B");
        topicService.createTopic(otherTopic);
        assertTrue(otherTopic.getId() > 0L);

        otherTopic.setName("A");

        try {
            topicService.createTopic(otherTopic);
            fail();
        } catch (TopicNotUniqueException e) {
        }

        otherTopic.setName("");
        try {
            topicService.createTopic(otherTopic);
            fail();
        } catch (RequiredPropertyNullException e) {
        }
    }

    @Test(groups = {"service"})
    public void testDeleteTopic() throws RequiredPropertyNullException, TopicNotUniqueException, DeleteRelationsException, NoRecordException {
        SubmissionTopic topic = new SubmissionTopic();
        topic.setName("A");
        topicService.createTopic(topic);
        assertTrue(topic.getId() > 0L);

         // Vytvorime usera
        UserEvent ue = new UserEvent();
        ue.setFullName("1");
        ue.setLastName("1");
        ue.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        ue.setUuid("12");
        userEventService.createUserEventWithRights(ue, ArrayUtils.EMPTY_STRING_ARRAY);

        
        ReviewerPreferredTopic rpt = new ReviewerPreferredTopic();
        rpt.setReviewer(ue);
        rpt.setTopic(topic);

        preferredTopicDao.create(rpt);
        try {
            topicService.deleteTopic(topic);
            fail();
        } catch (DeleteRelationsException e) {
        }
    }

    @Test(groups = {"service"})
    public void testListTopic() throws RequiredPropertyNullException, TopicNotUniqueException, NoRecordException {
        SubmissionTopic topic = new SubmissionTopic();
        topic.setName("BA");
        topicService.createTopic(topic);
        assertTrue(topic.getId() > 0L);

        SubmissionTopic otherTopic = new SubmissionTopic();
        otherTopic.setName("AB");
        topicService.createTopic(otherTopic);
        assertTrue(otherTopic.getId() > 0L);

        List<SubmissionTopic> list = topicService.listTopics();
        assertTrue(list.size() == 2);
        assertEquals(list.get(0), otherTopic);
        assertEquals(list.get(1), topic);

        list = topicService.listTopicsByList(Lists.newArrayList(topic.getId(),otherTopic.getId()));
        assertTrue(list.size() == 2);
    }

    @Test(groups = {"service"})
    public void testUpdatePreferredTopics() throws RequiredPropertyNullException, TopicNotUniqueException, NoRecordException {
        // Vytvorime usera
        UserEvent ue = new UserEvent();
        ue.setFullName("1");
        ue.setLastName("1");
        ue.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        ue.setUuid("12");
        userEventService.createUserEventWithRights(ue, ArrayUtils.EMPTY_STRING_ARRAY);
        // Vytvorime topiky
        Long t1,t2,t3,t4,t5;
        SubmissionTopic topic = new SubmissionTopic();
        topic.setName("1");
        topicService.createTopic(topic);
        t1 = topic.getId();
        topic = new SubmissionTopic();
        topic.setName("2");
        topicService.createTopic(topic);
        t2 = topic.getId();
        topic = new SubmissionTopic();
        topic.setName("3");
        topicService.createTopic(topic);
        t3 = topic.getId();
        topic = new SubmissionTopic();
        topic.setName("4");
        topicService.createTopic(topic);
        t4 = topic.getId();
        topic = new SubmissionTopic();
        topic.setName("5");
        topicService.createTopic(topic);
        t5 = topic.getId();
        // TEST 1 - Pridame dva topiky
        topicService.updateReviewerTopics(ue.getUuid(), Lists.newArrayList(t1,t5));

        // nacitame userove topiku a test na pocet
        List<Long> ids = topicService.listReviewerPreferredTopics(ue.getUuid());
        assertTrue(ids.size() == 2);

        // TEST 2 - Pridame dalsie dva topiky
        topicService.updateReviewerTopics(ue.getUuid(), Lists.newArrayList(t1,t5,t2,t3));
        ids = topicService.listReviewerPreferredTopics(ue.getUuid());
        assertTrue(ids.size() == 4);

        // TEST 3 - Pridame jeden topik a dva odstranime
        topicService.updateReviewerTopics(ue.getUuid(), Lists.newArrayList(t4,t5,t2));
        ids = topicService.listReviewerPreferredTopics(ue.getUuid());
        assertTrue(ids.size() == 3);
    }
}
