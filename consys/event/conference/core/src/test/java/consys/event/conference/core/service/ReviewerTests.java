package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ReviewerThumb;
import consys.event.conference.core.dao.ReviewDao;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewerTests extends AbstractConferenceTest {

    @Autowired
    private ReviewDao reviewDao;
    @Autowired
    private UserEventService userEventService;

    @Test(groups = {"service"})
    public void testLoadReviewers() throws NoRecordException, RequiredPropertyNullException, SubmissionTypeNotUniqueException, TopicNotUniqueException, ServiceExecutionFailed {

        UserEvent ue1 = new UserEvent();
        ue1.setFullName("A");
        ue1.setLastName("A");
        ue1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        ue1.setUuid(UuidProvider.getUuid());
        userEventService.createUserEventWithRights(ue1, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER);
        UserEvent ue2 = new UserEvent();
        ue2.setFullName("B");
        ue2.setLastName("B");
        ue2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        ue2.setUuid(UuidProvider.getUuid());
        userEventService.createUserEventWithRights(ue2, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_ACCEPT_SUBMISSIONS, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER);

        UserEvent ue3 = new UserEvent();
        ue3.setFullName("C");
        ue3.setLastName("C");
        ue3.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        ue3.setUuid(UuidProvider.getUuid());
        userEventService.createUserEventWithRights(ue3, EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER);

        // Vytvorime si typy prispevkov
        SubmissionType st = new SubmissionType();
        st.setName("Type 1");
        settingsService.createSubmissionType(st);
        st = new SubmissionType();
        st.setName("Type 2");
        settingsService.createSubmissionType(st);

        // Vytvorime topicy
        SubmissionTopic t1 = new SubmissionTopic();
        t1.setName("t1");
        topicService.createTopic(t1);
        SubmissionTopic t2 = new SubmissionTopic();
        t2.setName("t2");
        topicService.createTopic(t2);
        SubmissionTopic t3 = new SubmissionTopic();
        t3.setName("t3");
        topicService.createTopic(t3);
        SubmissionTopic t4 = new SubmissionTopic();
        t4.setName("t4");
        topicService.createTopic(t4);
        SubmissionTopic t5 = new SubmissionTopic();
        t5.setName("t5");
        topicService.createTopic(t5);
        SubmissionTopic t6 = new SubmissionTopic();
        t6.setName("t6");
        topicService.createTopic(t6);

        // vytvorime dva typy artefaktov a natavime ich
        SubmissionArtefactType artefactType1 = new SubmissionArtefactType();
        artefactType1.setName("Abstract");
        SubmissionTypeArtefact typeArtefact1 = new SubmissionTypeArtefact();
        typeArtefact1.setArtefactType(artefactType1);
        typeArtefact1.setRequired(false);
        SubmissionArtefactType artefactType2 = new SubmissionArtefactType();
        artefactType2.setName("Paper");
        SubmissionTypeArtefact typeArtefact2 = new SubmissionTypeArtefact();
        typeArtefact2.setArtefactType(artefactType2);
        typeArtefact2.setRequired(false);
        settingsService.createSubmissionArtefactTypes(st.getUuid(), Lists.newArrayList(typeArtefact1, typeArtefact2));

        UserEvent a1 = new UserEvent();
        a1.setFullName("1");
        a1.setLastName("2");
        a1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        a1.setUuid(UuidProvider.getUuid());
        userEventService.createUserEventWithRights(a1, ArrayUtils.EMPTY_STRING_ARRAY);

        // Vytvorime prispevok 1: A1
        Submission s1 = new Submission();
        s1.setTitle("Title 1");
        s1.getTopics().add(t1);
        s1.getTopics().add(t2);
        s1.setType(st);
        s1.setUuid(UuidProvider.getUuid());

        Set<Invitation> contributors = Sets.newHashSet();
        contributors.add(new ExistingUserInvitation(a1.getUuid()));
        submissionService.createSubmission(s1, contributors);

        // Vytvorime prispevok 2: A1 A2
        Submission s2 = new Submission();
        s2.setTitle("Title 2");
        s2.getTopics().add(t3);
        s2.getTopics().add(t4);
        s2.setUuid(UuidProvider.getUuid());
        s2.setType(st);
        contributors = Sets.newHashSet();
        contributors.add(new ExistingUserInvitation(a1.getUuid()));
        submissionService.createSubmission(s2, contributors);

        topicService.updateReviewerTopics(ue1.getUuid(), Lists.newArrayList(t1.getId(), t2.getId(), t4.getId()));
        topicService.updateReviewerTopics(ue2.getUuid(), Lists.newArrayList(t2.getId(), t3.getId(), t4.getId()));

        List<ReviewerThumb> reviewers = reviewDao.listAllReviewers(s1.getUuid());
        assertEquals(reviewers.size(), 3);
        for (ReviewerThumb reviewer : reviewers) {
            if (!reviewer.getUserUuid().equalsIgnoreCase(ue3.getUuid())) {
                assertEquals(reviewer.getPrefferedTopics().size(), 3);
                assertEquals(reviewer.getLoad(), 0);
            } else {
                assertEquals(reviewer.getPrefferedTopics().size(), 0);
                assertEquals(reviewer.getLoad(), 0);
            }
        }


        topicService.updateReviewerTopics(ue1.getUuid(), Lists.newArrayList(t1.getId()));
        topicService.updateReviewerTopics(ue2.getUuid(), Lists.newArrayList(t2.getId()));
        reviewService.createReviewRelation(s2.getUuid(), ue2.getUuid());
        reviewService.createReviewRelation(s2.getUuid(), ue3.getUuid());
        reviewers = reviewDao.listAllReviewers(s1.getUuid());
        assertEquals(reviewers.size(), 3);
        for (ReviewerThumb reviewer : reviewers) {
            if (reviewer.getUserUuid().equalsIgnoreCase(ue3.getUuid())) {
                assertEquals(reviewer.getPrefferedTopics().size(), 0);
                assertEquals(reviewer.getLoad(), 1);
            } else if (reviewer.getUserUuid().equalsIgnoreCase(ue2.getUuid())) {
                assertEquals(reviewer.getPrefferedTopics().size(), 1);
                assertEquals(reviewer.getLoad(), 1);
            }
        }

        reviewService.createReviewRelation(s1.getUuid(), ue1.getUuid());
        reviewService.createReviewRelation(s2.getUuid(), ue1.getUuid());

        reviewService.updateReviewerBid(ue1.getUuid(), s1.getUuid(), ReviewerBidInterestEnum.LIKE);

        reviewers = reviewDao.listAllReviewers(s1.getUuid());
        for (ReviewerThumb reviewer : reviewers) {
            if (reviewer.getUserUuid().equalsIgnoreCase(ue1.getUuid())) {
                assertEquals(reviewer.getLoad(), 2);
                assertEquals(reviewer.getInterest(), ReviewerBidInterestEnum.LIKE);
                assertTrue(reviewer.isReviewing());
            }
        }
    }
}
