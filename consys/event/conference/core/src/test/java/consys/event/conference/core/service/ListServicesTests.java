package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.conference.api.list.BidForReviewList;
import consys.event.conference.api.list.ContributionsList;
import consys.event.conference.api.list.ReviewerReviewsList;
import consys.event.conference.api.list.ContributorContributionsList;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ReviewerBidListItem;
import consys.event.conference.core.bo.thumb.SubmissionListItem;
import consys.event.conference.core.bo.thumb.SubmissionListItem.Evaluation;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import java.util.*;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListServicesTests extends AbstractConferenceTest {

    // Identifikacne konstanty    
    private String SUBMISSION_1_D_UUID;
    private String SUBMISSION_2_P_UUID;
    private String SUBMISSION_3_P_UUID;
    private String SUBMISSION_4_A_UUID;
    private String SUBMISSION_5_A_UUID;
    private String SUBMISSION_6_R_UUID;
    private String SUBMISSION_7_R_UUID;
    private String SUBMISSION_TYPE_1_UUID;
    private String SUBMISSION_TYPE_2_UUID;
    private String REVIEWER_3 = "3";
    SubmissionTopic t1, t2;
    private String CONTRIBUTOR_1 = "123"; // 1,2,3 
    private String CONTRIBUTOR_2 = "234"; // 2,3,4
    private String CONTRIBUTOR_3 = "3457"; // 3,4,5,7
    private String CONTRIBUTOR_4 = "456"; // 4,5,6
    @Autowired
    private ContributionsListService contributionsListService;

    /**
     *
     * Prispevky
     * Deleted: 1
     * Pending: 2
     * Accepted: 2
     * Declined: 2  
     */
    @BeforeMethod(alwaysRun = true, groups = {"list-service"})
    public void prepareData2() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, TopicNotUniqueException, NoRecordException, ServiceExecutionFailed {
        // aby sme pretestovali aj visible authors
        propertyService.updateBooleanProperty(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS, Boolean.TRUE);

        // Vytvorime si typy prispevkov
        SubmissionType st = new SubmissionType();
        st.setName("Type 1");
        settingsService.createSubmissionType(st);
        SUBMISSION_TYPE_1_UUID = st.getUuid();


        // vytvorime tri typy artefaktov a natavime ich
        SubmissionArtefactType artefactType11 = new SubmissionArtefactType();
        artefactType11.setName("Abstract");
        SubmissionTypeArtefact typeArtefact11 = new SubmissionTypeArtefact();
        typeArtefact11.setArtefactType(artefactType11);
        typeArtefact11.setRequired(false);

        SubmissionArtefactType artefactType12 = new SubmissionArtefactType();
        artefactType12.setName("Paper");
        SubmissionTypeArtefact typeArtefact12 = new SubmissionTypeArtefact();
        typeArtefact12.setArtefactType(artefactType12);
        typeArtefact12.setRequired(false);

        SubmissionArtefactType artefactType13 = new SubmissionArtefactType();
        artefactType13.setName("Presentation");
        SubmissionTypeArtefact typeArtefact13 = new SubmissionTypeArtefact();
        typeArtefact13.setArtefactType(artefactType13);
        typeArtefact13.setRequired(false);
        settingsService.createSubmissionArtefactTypes(st.getUuid(), Lists.newArrayList(typeArtefact11, typeArtefact12, typeArtefact13));

        createArtefactCriteria(typeArtefact11.getUuid());
        createArtefactCriteria(typeArtefact12.getUuid());
        createArtefactCriteria(typeArtefact13.getUuid());

        st = new SubmissionType();
        st.setName("Type 2");
        settingsService.createSubmissionType(st);
        SUBMISSION_TYPE_2_UUID = st.getUuid();

        SubmissionArtefactType artefactType21 = new SubmissionArtefactType();
        artefactType21.setName("Abstract");
        SubmissionTypeArtefact typeArtefact21 = new SubmissionTypeArtefact();
        typeArtefact21.setArtefactType(artefactType21);
        typeArtefact21.setRequired(false);

        SubmissionArtefactType artefactType22 = new SubmissionArtefactType();
        artefactType22.setName("Paper");
        SubmissionTypeArtefact typeArtefact22 = new SubmissionTypeArtefact();
        typeArtefact22.setArtefactType(artefactType12);
        typeArtefact22.setRequired(false);

        SubmissionArtefactType artefactType23 = new SubmissionArtefactType();
        artefactType23.setName("Presentation");
        SubmissionTypeArtefact typeArtefact23 = new SubmissionTypeArtefact();
        typeArtefact23.setArtefactType(artefactType23);
        typeArtefact23.setRequired(false);
        settingsService.createSubmissionArtefactTypes(st.getUuid(), Lists.newArrayList(typeArtefact21, typeArtefact22, typeArtefact23));

        createArtefactCriteria(typeArtefact21.getUuid());
        createArtefactCriteria(typeArtefact22.getUuid());
        createArtefactCriteria(typeArtefact23.getUuid());

        // Vytvorime topicy
        t1 = new SubmissionTopic();
        t1.setName("t1");
        topicService.createTopic(t1);
        t2 = new SubmissionTopic();
        t2.setName("t2");
        topicService.createTopic(t2);

        // Vytvorime recenzentov
        UserEvent r1 = new UserEvent();
        r1.setFullName("1");
        r1.setLastName("2");
        r1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        r1.setUuid(REVIEWER_1);
        UserEvent r2 = new UserEvent();
        r2.setFullName("1");
        r2.setLastName("2");
        r2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        r2.setUuid(REVIEWER_2);
        UserEvent r3 = new UserEvent();
        r3.setFullName("1");
        r3.setLastName("2");
        r3.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        r3.setUuid(REVIEWER_3);

        // vytvorime contributorov
        UserEvent a1 = new UserEvent();
        a1.setFullName("1");
        a1.setLastName("2");
        a1.setUuid(CONTRIBUTOR_1);
        a1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");

        UserEvent a2 = new UserEvent();
        a2.setFullName("1");
        a2.setLastName("2");
        a2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        a2.setUuid(CONTRIBUTOR_2);

        UserEvent a3 = new UserEvent();
        a3.setFullName("1");
        a3.setLastName("2");
        a3.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        a3.setUuid(CONTRIBUTOR_3);

        UserEvent a4 = new UserEvent();
        a4.setFullName("1");
        a4.setLastName("2");
        a4.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        a4.setUuid(CONTRIBUTOR_4);

        eventService.createUserEventWithRights(a1, ArrayUtils.EMPTY_STRING_ARRAY);
        eventService.createUserEventWithRights(a2, ArrayUtils.EMPTY_STRING_ARRAY);
        eventService.createUserEventWithRights(a3, ArrayUtils.EMPTY_STRING_ARRAY);
        eventService.createUserEventWithRights(a4, ArrayUtils.EMPTY_STRING_ARRAY);

        eventService.createUserEventWithRights(r1, new String[]{EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER});
        eventService.createUserEventWithRights(r2, new String[]{EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER});
        eventService.createUserEventWithRights(r3, new String[]{EventConferenceModuleRoleModel.RIGHT_SUBMISSION_REVIEWER});

        // vytvorime prispevky
        SUBMISSION_1_D_UUID = generateSubmission("DELETED", SUBMISSION_TYPE_1_UUID, 2, CONTRIBUTOR_1).getUuid();

        SUBMISSION_2_P_UUID = generateSubmission("PENDING 1", SUBMISSION_TYPE_1_UUID, 2, CONTRIBUTOR_1, CONTRIBUTOR_2).getUuid();
        SUBMISSION_3_P_UUID = generateSubmission("PENDING 2", SUBMISSION_TYPE_2_UUID, 2, CONTRIBUTOR_1, CONTRIBUTOR_2, CONTRIBUTOR_3).getUuid();

        SUBMISSION_4_A_UUID = generateSubmission("ACCEPTED 1", SUBMISSION_TYPE_1_UUID, 3, CONTRIBUTOR_2, CONTRIBUTOR_3, CONTRIBUTOR_4).getUuid();
        SUBMISSION_5_A_UUID = generateSubmission("ACCEPTED 2", SUBMISSION_TYPE_2_UUID, 3, CONTRIBUTOR_3, CONTRIBUTOR_4).getUuid();

        SUBMISSION_6_R_UUID = generateSubmission("REJECTED 1", SUBMISSION_TYPE_1_UUID, 2, CONTRIBUTOR_4).getUuid();
        SUBMISSION_7_R_UUID = generateSubmission("REJECTED 1", SUBMISSION_TYPE_1_UUID, 2, CONTRIBUTOR_3).getUuid();

        // Priradime k recenzentom prispveky
        reviewService.createReviewRelation(SUBMISSION_1_D_UUID, REVIEWER_1);

        reviewService.createReviewRelation(SUBMISSION_2_P_UUID, REVIEWER_1);
        reviewService.createReviewRelation(SUBMISSION_2_P_UUID, REVIEWER_2);

        reviewService.createReviewRelation(SUBMISSION_3_P_UUID, REVIEWER_2);
        reviewService.createReviewRelation(SUBMISSION_3_P_UUID, REVIEWER_3);

        reviewService.createReviewRelation(SUBMISSION_4_A_UUID, REVIEWER_2);
        reviewService.createReviewRelation(SUBMISSION_5_A_UUID, REVIEWER_1);

        reviewService.createReviewRelation(SUBMISSION_6_R_UUID, REVIEWER_1);
        reviewService.createReviewRelation(SUBMISSION_7_R_UUID, REVIEWER_3);

        doReview(SUBMISSION_2_P_UUID, REVIEWER_1, 2);
        doReview(SUBMISSION_2_P_UUID, REVIEWER_2, 2);

        doReview(SUBMISSION_3_P_UUID, REVIEWER_2, 1);
        doReview(SUBMISSION_3_P_UUID, REVIEWER_3, 1);

        doReview(SUBMISSION_4_A_UUID, REVIEWER_2, 3);
        doReview(SUBMISSION_5_A_UUID, REVIEWER_1, 3);

        // Upravime prispevky tak ako maju byt podla stavov
        submissionService.deleteSubmisson(SUBMISSION_1_D_UUID);

        submissionService.updateSubmissonState(SUBMISSION_4_A_UUID, SubmissionStateEnum.ACCEPTED);
        submissionService.updateSubmissonState(SUBMISSION_5_A_UUID, SubmissionStateEnum.ACCEPTED);

        submissionService.updateSubmissonState(SUBMISSION_6_R_UUID, SubmissionStateEnum.DECLINED);
        submissionService.updateSubmissonState(SUBMISSION_7_R_UUID, SubmissionStateEnum.DECLINED);

        // nastavime ze chjceme aby boli recenzenti vidiet
        propertyService.updateBooleanProperty(ConferenceProperties.SYSTEM_PROPERTY_VISIBLE_AUTHORS, Boolean.TRUE);

        forceSessionFlush();
    }

    private Submission generateSubmission(String title, String typeUuid, int artefactsCount, String... contributors) throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException {
        SubmissionType type = submissionService.loadSubmissionTypeWithArtefactTypes(typeUuid);

        // Vytvorime prispevok 1: A1
        Submission s = new Submission();
        s.setUuid(submissionService.createSubmissionUuid());
        s.setTitle(title);
        s.getTopics().add(t1);
        s.getTopics().add(t2);
        s.setType(type);

        int artefacts = 0;
        for (SubmissionTypeArtefact sat : type.getArtefacts()) {
            if (artefacts == artefactsCount) {
                break;
            }
            SubmissionArtefact artefact = new SubmissionArtefact();
            artefact.setTypeArtefact(sat);
            artefact.setUuid(submissionService.createSubmissionArtefact(s.getUuid(), sat.getUuid(), generate()));
            s.getArtefacts().add(artefact);
            artefacts += 1;
        }

        Set<Invitation> invitations = new HashSet<Invitation>();
        for (String uuid : contributors) {
            invitations.add(new ExistingUserInvitation(uuid));
        }
        submissionService.createSubmission(s, invitations);
        return s;
    }

    private void createArtefactCriteria(String typeArtefactUuid) throws NoRecordException, RequiredPropertyNullException {

        // vytvorime 4 kriteria po 5 hodnotach
        // CRITERIUM 1
        ReviewCriteria criteria1 = new ReviewCriteria();
        criteria1.setName("C1 for A1");
        criteria1.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria1.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria1.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria1.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria1.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria2 = new ReviewCriteria();
        criteria2.setName("C2 for A1");
        criteria2.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria2.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria2.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria2.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria2.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria3 = new ReviewCriteria();
        criteria3.setName("C3 for A1");
        criteria3.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria3.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria3.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria3.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria3.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria4 = new ReviewCriteria();
        criteria4.setName("C4 for A1");
        criteria4.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria4.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria4.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria4.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria4.getItems().add(new ReviewCriteriaItem("4", 4));

        ReviewCriteria criteria5 = new ReviewCriteria();
        criteria5.setName("C5 for A1");
        criteria5.getItems().add(new ReviewCriteriaItem("0", 0));
        criteria5.getItems().add(new ReviewCriteriaItem("1", 1));
        criteria5.getItems().add(new ReviewCriteriaItem("2", 2));
        criteria5.getItems().add(new ReviewCriteriaItem("3", 3));
        criteria5.getItems().add(new ReviewCriteriaItem("4", 4));
        settingsService.createReviewCriterias(typeArtefactUuid, Lists.newArrayList(criteria1, criteria2, criteria3, criteria4, criteria5));
    }

    private void doReview(String submissionUuid, String reviewerUuid, int reviewArtefacts) throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {

        Submission s = submissionService.loadSubmissionWithArtefacts(submissionUuid);
        Review r = reviewService.loadReview(submissionUuid, reviewerUuid);
        Random rand = new Random();
        int review = 0;
        for (SubmissionArtefact a : s.getArtefacts()) {
            if (review == reviewArtefacts) {
                break;
            }
            List<ReviewCriteria> criterias = reviewService.listCriteriasForBySubmissionArtefact(a.getUuid());
            Map<Long, Long> areview = new HashMap<Long, Long>(criterias.size());
            for (ReviewCriteria reviewCriteria : criterias) {
                areview.put(reviewCriteria.getId(), reviewCriteria.getItems().get(rand.nextInt(reviewCriteria.getItems().size())).getId());
            }
            reviewService.updateReviewEvaluation(r.getUuid(), s.getUuid(), a.getUuid(), areview, "Comment", null);
            review += 1;
        }
    }

    @Test(groups = {"list-service"})
    public void testGeneratedData() throws NoRecordException, RequiredPropertyNullException {

        // test na to ci je prispevok zmazany - tj. stav deleted        
        assertEquals(submissionService.loadSubmissonByUuid(SUBMISSION_1_D_UUID).getState(), SubmissionStateEnum.DELETED);

        Review r21 = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(SUBMISSION_2_P_UUID, REVIEWER_1);
        assertEquals(r21.getArtefactEvaluations().size(), 2);

        Review r22 = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(SUBMISSION_2_P_UUID, REVIEWER_2);
        assertEquals(r22.getArtefactEvaluations().size(), 2);

        Review r32 = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(SUBMISSION_3_P_UUID, REVIEWER_2);
        assertEquals(r32.getArtefactEvaluations().size(), 1);

        Review r33 = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(SUBMISSION_3_P_UUID, REVIEWER_3);
        assertEquals(r33.getArtefactEvaluations().size(), 1);

        Review r42 = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(SUBMISSION_4_A_UUID, REVIEWER_2);
        assertEquals(r42.getArtefactEvaluations().size(), 3);

        Review r51 = reviewService.loadReviewWithArtefactReviewsWithoutCriterias(SUBMISSION_5_A_UUID, REVIEWER_1);
        assertEquals(r51.getArtefactEvaluations().size(), 3);
    }

    @Test(groups = {"list-service"}, dependsOnMethods = {"testGeneratedData"})
    public void testReviewerReviewsList() throws NoRecordException, RequiredPropertyNullException {

        Constraints constraint = new Constraints();
        constraint.setOrderers(new int[]{ReviewerReviewsList.Order_TITLE});
        constraint.setUserUuid(REVIEWER_3);

        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(2);

        // Test: ALL
        constraint.setFilterTag(0);
        Integer c1 = myReviewsListService.listAllCount(constraint);
        List<SubmissionListItem> items = myReviewsListService.listItems(constraint, p);
        assertEquals(c1, new Integer(2));
        assertEquals(items.size(), 2);


        Review r3 = reviewService.loadReview(SUBMISSION_3_P_UUID, REVIEWER_3);
        Review r7 = reviewService.loadReview(SUBMISSION_7_R_UUID, REVIEWER_3);

        for (SubmissionListItem submissionListItem : items) {
            if (submissionListItem.getOverallScore().getUuid().equalsIgnoreCase(r3.getUuid())) {
                assertEquals(submissionListItem.getAgregatedTopics(), r3.getSubmission().getAgregatedTopics());
                assertEquals(submissionListItem.getAuthors(), r3.getSubmission().getAuthors());
                assertEquals(submissionListItem.getContributors().size(), r3.getSubmission().getContributors().size());
                assertEquals(submissionListItem.getDecideDate(), r3.getSubmission().getDecidedDate());
                assertEquals(submissionListItem.getOverallScore().getScore(), r3.getScore());
                assertEquals(submissionListItem.getOverallScore().getUuid(), r3.getUuid());
                assertEquals(submissionListItem.getPartialScore().size(), 2);
                assertEquals(submissionListItem.getStateEnum(), SubmissionStateEnum.PENDING);
                assertEquals(submissionListItem.getSubmitDate(), r3.getSubmission().getSubmittedDate());
                assertEquals(submissionListItem.getSubtitle(), r3.getSubmission().getSubTitle());
                assertEquals(submissionListItem.getTitle(), r3.getSubmission().getTitle());
                assertEquals(submissionListItem.getType(), r3.getSubmission().getType().getName());
                assertEquals(submissionListItem.getUuid(), r3.getSubmission().getUuid());
            } else if (submissionListItem.getOverallScore().getUuid().equalsIgnoreCase(r7.getUuid())) {
                assertEquals(submissionListItem.getAgregatedTopics(), r7.getSubmission().getAgregatedTopics());
                assertEquals(submissionListItem.getAuthors(), r7.getSubmission().getAuthors());
                assertEquals(submissionListItem.getContributors().size(), r7.getSubmission().getContributors().size());
                assertEquals(submissionListItem.getDecideDate(), r7.getSubmission().getDecidedDate());
                assertEquals(submissionListItem.getOverallScore().getScore(), r7.getScore());
                assertEquals(submissionListItem.getOverallScore().getUuid(), r7.getUuid());
                assertEquals(submissionListItem.getPartialScore().size(), 2);
                for (Evaluation e : submissionListItem.getPartialScore()) {
                    assertEquals(e.getScore(), -1);
                }
                assertEquals(submissionListItem.getStateEnum(), SubmissionStateEnum.DECLINED);
                assertEquals(submissionListItem.getSubmitDate(), r7.getSubmission().getSubmittedDate());
                assertEquals(submissionListItem.getSubtitle(), r7.getSubmission().getSubTitle());
                assertEquals(submissionListItem.getTitle(), r7.getSubmission().getTitle());
                assertEquals(submissionListItem.getType(), r7.getSubmission().getType().getName());
                assertEquals(submissionListItem.getUuid(), r7.getSubmission().getUuid());
            } else {
                fail("No match");
            }
        }

        // Test : Filter TYPE                
        constraint.setFilterTag(ReviewerReviewsList.Filter_TYPE);
        constraint.setStringFilterValue(SUBMISSION_TYPE_2_UUID);
        c1 = myReviewsListService.listAllCount(constraint);
        items = myReviewsListService.listItems(constraint, p);
        assertEquals(c1, new Integer(1));
        assertEquals(items.size(), 1);

        // Test : Filter STATE       
        constraint.setFilterTag(ReviewerReviewsList.Filter_STATE);
        constraint.setIntFilterValue(ReviewerReviewsList.States.REJECTED.ordinal());
        c1 = myReviewsListService.listAllCount(constraint);
        items = myReviewsListService.listItems(constraint, p);
        assertEquals(c1, new Integer(1));
        assertEquals(items.size(), 1);

        constraint.setIntFilterValue(ReviewerReviewsList.States.PENDING.ordinal());
        c1 = myReviewsListService.listAllCount(constraint);
        items = myReviewsListService.listItems(constraint, p);
        assertEquals(c1, new Integer(1));
        assertEquals(items.size(), 1);

        constraint.setIntFilterValue(ReviewerReviewsList.States.ACCEPTED.ordinal());
        c1 = myReviewsListService.listAllCount(constraint);
        items = myReviewsListService.listItems(constraint, p);
        assertEquals(c1, new Integer(0));
        assertEquals(items.size(), 0);

        // TEST NA ZMAZANY PRISPEVOK
        constraint.setUserUuid(REVIEWER_1);
        constraint.setFilterTag(0);
        p.setMaxResults(3);
        c1 = myReviewsListService.listAllCount(constraint);
        items = myReviewsListService.listItems(constraint, p);
        assertEquals(c1, new Integer(3));
        assertEquals(items.size(), 3);
    }

    @Test(groups = {"list-service"}, dependsOnMethods = {"testGeneratedData"})
    public void testContributionsListService() throws NoRecordException, RequiredPropertyNullException {
        // Test : Filter ALL
        logger.warn("----------------------------- TEST CONTRIBUTIONS ");
        Constraints constraint = new Constraints();
        constraint.setFilterTag(ContributionsList.Filter_ALL);
        constraint.setOrderers(new int[]{ContributionsList.Order_TITLE});
        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(6);
        List<SubmissionListItem> thumbs = contributionsListService.listItems(constraint, p);
        assertEquals(thumbs.size(), 6);
        Integer c1 = contributionsListService.listAllCount(constraint);
        assertEquals(c1, new Integer(6));

        // Test: Filter Type
        constraint.setFilterTag(ContributionsList.Filter_TYPE);
        constraint.setStringFilterValue(SUBMISSION_TYPE_1_UUID);
        assertEquals(contributionsListService.listAllCount(constraint), 4);
        assertEquals(contributionsListService.listItems(constraint, p).size(), 4);

        constraint.setStringFilterValue(SUBMISSION_TYPE_2_UUID);
        assertEquals(contributionsListService.listAllCount(constraint), 2);
        thumbs = contributionsListService.listItems(constraint, p);
        assertEquals(thumbs.size(), 2);

        Submission s3 = submissionService.loadSubmissionWithArtefacts(SUBMISSION_3_P_UUID);
        Submission s5 = submissionService.loadSubmissionWithArtefacts(SUBMISSION_5_A_UUID);

        for (SubmissionListItem submissionListItem : thumbs) {
            if (submissionListItem.getUuid().equalsIgnoreCase(SUBMISSION_3_P_UUID)) {
                assertEquals(submissionListItem.getAgregatedTopics(), s3.getAgregatedTopics());
                assertEquals(submissionListItem.getAuthors(), s3.getAuthors());
                assertEquals(submissionListItem.getContributors().size(), s3.getContributors().size());
                assertEquals(submissionListItem.getDecideDate(), s3.getDecidedDate());
                assertEquals(submissionListItem.getOverallScore().getScore(), s3.getScore());
                assertEquals(submissionListItem.getPartialScore().size(), 2);
                assertEquals(submissionListItem.getStateEnum(), SubmissionStateEnum.PENDING);
                assertEquals(submissionListItem.getSubmitDate(), s3.getSubmittedDate());
                assertEquals(submissionListItem.getSubtitle(), s3.getSubTitle());
                assertEquals(submissionListItem.getTitle(), s3.getTitle());
                assertEquals(submissionListItem.getType(), s3.getType().getName());
                assertEquals(submissionListItem.getUuid(), s3.getUuid());
            } else if (submissionListItem.getUuid().equalsIgnoreCase(SUBMISSION_5_A_UUID)) {
                assertEquals(submissionListItem.getAgregatedTopics(), s5.getAgregatedTopics());
                assertEquals(submissionListItem.getAuthors(), s5.getAuthors());
                assertEquals(submissionListItem.getContributors().size(), s5.getContributors().size());
                assertEquals(submissionListItem.getDecideDate(), s5.getDecidedDate());
                assertEquals(submissionListItem.getOverallScore().getScore(), s5.getScore());
                assertEquals(submissionListItem.getPartialScore().size(), 1);
                assertEquals(submissionListItem.getStateEnum(), SubmissionStateEnum.ACCEPTED);
                assertEquals(submissionListItem.getSubmitDate(), s5.getSubmittedDate());
                assertEquals(submissionListItem.getSubtitle(), s5.getSubTitle());
                assertEquals(submissionListItem.getTitle(), s5.getTitle());
                assertEquals(submissionListItem.getType(), s5.getType().getName());
                assertEquals(submissionListItem.getUuid(), s5.getUuid());
            } else {
                fail("No match");
            }
        }

        // Test : Filter STATE
        constraint.setFilterTag(ContributionsList.Filter_STATE);
        constraint.setIntFilterValue(ContributionsList.States.PENDING.ordinal());
        assertEquals(contributionsListService.listAllCount(constraint), 2);
        assertEquals(contributionsListService.listItems(constraint, p).size(), 2);

        constraint.setIntFilterValue(ContributionsList.States.ACCEPTED.ordinal());
        assertEquals(contributionsListService.listAllCount(constraint), 2);
        assertEquals(contributionsListService.listItems(constraint, p).size(), 2);

        constraint.setIntFilterValue(ContributionsList.States.REJECTED.ordinal());
        assertEquals(contributionsListService.listAllCount(constraint), 2);
        assertEquals(contributionsListService.listItems(constraint, p).size(), 2);
    }

    @Test(groups = {"list-service"}, dependsOnMethods = {"testGeneratedData"})
    public void testContributorContributionsListService() {
        // Test : Filter ALL
        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(4);

        Constraints constraint = new Constraints();
        constraint.setFilterTag(ContributorContributionsList.Filter_ALL);
        constraint.setOrderers(new int[]{ContributorContributionsList.Order_SUBMIT_DATE});
        constraint.setUserUuid(CONTRIBUTOR_3);

        assertEquals(mySubmissionsListService.listAllCount(constraint), 4);
        List<SubmissionListItem> items = mySubmissionsListService.listItems(constraint, p);
        assertEquals(items.size(), 4);

        // Test: Filter Type
        constraint.setFilterTag(ContributionsList.Filter_TYPE);
        constraint.setStringFilterValue(SUBMISSION_TYPE_1_UUID);
        assertEquals(mySubmissionsListService.listAllCount(constraint), 2);
        assertEquals(mySubmissionsListService.listItems(constraint, p).size(), 2);

        constraint.setStringFilterValue(SUBMISSION_TYPE_2_UUID);
        assertEquals(mySubmissionsListService.listAllCount(constraint), 2);
        assertEquals(mySubmissionsListService.listItems(constraint, p).size(), 2);

        // Test : Filter STATE
        constraint.setFilterTag(ContributionsList.Filter_STATE);
        constraint.setIntFilterValue(ContributionsList.States.PENDING.ordinal());
        assertEquals(mySubmissionsListService.listAllCount(constraint), 1);
        assertEquals(mySubmissionsListService.listItems(constraint, p).size(), 1);

        constraint.setIntFilterValue(ContributionsList.States.ACCEPTED.ordinal());
        assertEquals(mySubmissionsListService.listAllCount(constraint), 2);
        assertEquals(mySubmissionsListService.listItems(constraint, p).size(), 2);

        constraint.setIntFilterValue(ContributionsList.States.REJECTED.ordinal());
        assertEquals(mySubmissionsListService.listAllCount(constraint), 1);
        assertEquals(mySubmissionsListService.listItems(constraint, p).size(), 1);
    }

    @Test(groups = {"list-service"}, dependsOnMethods = {"testGeneratedData"})
    public void testBidForReviewListService() throws NoRecordException, RequiredPropertyNullException {
        // Test : Filter ALL
        Constraints constraint = new Constraints();
        constraint.setFilterTag(BidForReviewList.Filter_INTEREST);
        constraint.setIntFilterValue(0);
        constraint.setOrderers(new int[]{ContributorContributionsList.Order_TITLE});
        constraint.setUserUuid(REVIEWER_1);
        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(10);
        reviewService.updateReviewerBid(REVIEWER_1, SUBMISSION_2_P_UUID, ReviewerBidInterestEnum.LIKE);
        reviewService.updateReviewerBid(REVIEWER_1, SUBMISSION_3_P_UUID, ReviewerBidInterestEnum.CONFLICT);
        Integer c1 = bidForReviewListService.listAllCount(constraint);
        assertEquals(c1, new Integer(6));
        List<ReviewerBidListItem> bids = bidForReviewListService.listItems(constraint, p);
        assertEquals(bids.size(), 6);
        for (ReviewerBidListItem item : bids) {
            if (item.getUuid().equalsIgnoreCase(SUBMISSION_2_P_UUID)) {
                assertEquals(item.getTopics().size(), 2);
                assertEquals(item.getBidInterestEnum(), ReviewerBidInterestEnum.LIKE);

            } else if (item.getUuid().equalsIgnoreCase(SUBMISSION_3_P_UUID)) {
                assertEquals(item.getTopics().size(), 2);
                assertEquals(item.getBidInterestEnum(), ReviewerBidInterestEnum.CONFLICT);
            } else {
                assertEquals(item.getBidInterestEnum(), ReviewerBidInterestEnum.NOT_CARE);
            }
        }
    }
}
