package consys.event.conference.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.SubmissionArtefactType;
import consys.event.conference.core.bo.SubmissionType;
import consys.event.conference.core.bo.SubmissionTypeArtefact;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionTypeDaoTest extends AbstractConferenceTest {

    private SubmissionTypeDao typeDao;
    private SubmissionTypeArtefactDao typeArtefactDao;
    private SubmissionArtefactTypeDao artefactTypeDao;
    private ReviewCycleDao cycleDao;
    private ReviewCriteriaDao criteriaDao;
    private SubmissionArtefactPatternDao patternDao;
    private SubmissionArtefactTemplateDao templateDao;

    @Autowired(required = true)
    public void setTemplateDao(SubmissionArtefactTemplateDao templateDao) {
        this.templateDao = templateDao;
    }

    @Autowired(required = true)
    public void setPatternDao(SubmissionArtefactPatternDao patternDao) {
        this.patternDao = patternDao;
    }

    @Autowired(required = true)
    public void setCriteriaDao(ReviewCriteriaDao criteriaDao) {
        this.criteriaDao = criteriaDao;
    }

    @Autowired(required = true)
    public void setArtefactTypeDao(SubmissionArtefactTypeDao artefactTypeDao) {
        this.artefactTypeDao = artefactTypeDao;
    }

    @Autowired(required = true)
    public void setCycleDao(ReviewCycleDao cycleDao) {
        this.cycleDao = cycleDao;
    }

    @Autowired(required = true)
    public void setTypeArtefactDao(SubmissionTypeArtefactDao typeArtefactDao) {
        this.typeArtefactDao = typeArtefactDao;
    }

    @Autowired(required = true)
    public void setTypeDao(SubmissionTypeDao typeDao) {
        this.typeDao = typeDao;
    }

    @Test(groups = {"dao"})
    public void createAndLoadTypes() throws NoRecordException {
        // create type
        SubmissionType type = new SubmissionType();
        type.setName("A");
        type.setUuid("1");
        type.setOrder(0);
        typeDao.create(type);

        // create artefact 1
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("A");
        artefactTypeDao.create(at1);


        // create type artefacts
        SubmissionTypeArtefact sta1 = new SubmissionTypeArtefact();
        sta1.setArtefactType(at1);
        sta1.setType(type);
        sta1.setUuid("1");
        typeArtefactDao.create(sta1);
        SubmissionTypeArtefact sta2 = new SubmissionTypeArtefact();
        sta2.setArtefactType(at1);
        sta2.setType(type);
        sta2.setUuid("2");
        typeArtefactDao.create(sta2);

        // set and update type
        type.getArtefacts().add(sta1);
        type.getArtefacts().add(sta2);
        typeDao.update(type);

        int count = artefactTypeDao.loadArtefactTypeUseCountBySubmissionTypeUuid(sta1.getUuid());
        assertTrue(count == 2);       
    }


}
