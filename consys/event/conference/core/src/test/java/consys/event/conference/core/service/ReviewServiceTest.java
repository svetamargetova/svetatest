package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.SystemPropertyService;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.common.utils.collection.Sets;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.service.UserEventService;
import consys.event.conference.api.properties.ConferenceProperties;
import consys.event.conference.api.right.EventConferenceModuleRoleModel;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.ContributionReviewerReview;
import consys.event.conference.core.dao.SubmissionTypeArtefactDao;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReviewServiceTest extends AbstractConferenceTest {

    
    // data
    private Long reviewCriteria1Id;
    private Long reviewCriteria2Id;
    
    @Autowired
    private SubmissionTypeArtefactDao submissionTypeArtefactDao;

    @Test(groups = {"service"})    
    public void createSimpleReview() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException, TopicNotUniqueException, ServiceExecutionFailed {

        List<Submission> submissions = prepareData();

        forceSessionFlush();

        // recenzia prispevku
        Submission s1 = submissions.get(0);
        SubmissionArtefact artefact = null;
        Iterator<SubmissionArtefact> iterator = s1.getArtefacts().iterator();
        while (iterator.hasNext()) {
            artefact = iterator.next();
            if (artefact.getTypeArtefact().getArtefactType().getName().equals("Abstract")) {
                break;
            }
        }

        reviewService.createReviewRelation(s1.getUuid(), REVIEWER_1);
        Review review_1 = reviewService.loadReview(s1.getUuid(), REVIEWER_1);

        assertEquals(review_1.getScore(), -1);
        assertEquals(review_1.getArtefactEvaluations().size(), 0);
                       
        

        List<ReviewCriteria> criterias = reviewService.listCriteriasForBySubmissionArtefact(artefact.getUuid());
        assertEquals(criterias.size(), 5);

        Map<Long, Long> evaluation = Maps.newHashMap();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(0).getId());      // 0                 
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(0).getId());      // 0
        evaluation.put(criterias.get(2).getId(), criterias.get(2).getItems().get(0).getId());      // 0
        evaluation.put(criterias.get(3).getId(), criterias.get(3).getItems().get(0).getId());      // 0
        evaluation.put(criterias.get(4).getId(), criterias.get(4).getItems().get(0).getId());      // 0
        reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");

        forceSessionFlush();

        review_1 = reviewService.loadReview(s1.getUuid(), REVIEWER_1);
        assertEquals(review_1.getScore(), 0);
        assertEquals(review_1.getArtefactEvaluations().size(), 1);
        assertEquals(review_1.getArtefactEvaluations().iterator().next().getScore(), 0);


        forceSessionFlush();

        evaluation.clear();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(4).getId());      // 4                 
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(4).getId());      // 4
        evaluation.put(criterias.get(2).getId(), criterias.get(2).getItems().get(4).getId());      // 4
        evaluation.put(criterias.get(3).getId(), criterias.get(3).getItems().get(4).getId());      // 4
        evaluation.put(criterias.get(4).getId(), criterias.get(4).getItems().get(4).getId());      // 4
        reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");

        review_1 = reviewService.loadReview(s1.getUuid(), REVIEWER_1);
        assertEquals(review_1.getScore(), 100);
        assertEquals(review_1.getArtefactEvaluations().size(), 1);
        assertEquals(review_1.getArtefactEvaluations().iterator().next().getScore(), 100);

        forceSessionFlush();

        evaluation.clear();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(0).getId());      // 0                 
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(1).getId());      // 1
        evaluation.put(criterias.get(2).getId(), criterias.get(2).getItems().get(2).getId());      // 2
        evaluation.put(criterias.get(3).getId(), criterias.get(3).getItems().get(3).getId());      // 3
        evaluation.put(criterias.get(4).getId(), criterias.get(4).getItems().get(4).getId());      // 4
        reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");

        review_1 = reviewService.loadReview(s1.getUuid(), REVIEWER_1);
        assertEquals(review_1.getScore(), 50);
        assertEquals(review_1.getArtefactEvaluations().size(), 1);
        assertEquals(review_1.getArtefactEvaluations().iterator().next().getScore(), 50);


        /**
         *
         * Pridame nove kriterium
         * <p/>
         */
        ReviewCriteria newCriteria = new ReviewCriteria();
        newCriteria.setName("C3 for A1");
        newCriteria.getItems().add(new ReviewCriteriaItem("c0", 0));
        newCriteria.getItems().add(new ReviewCriteriaItem("c1", 1));
        newCriteria.getItems().add(new ReviewCriteriaItem("c2", 2));
        newCriteria.getItems().add(new ReviewCriteriaItem("c3", 3));
        newCriteria.getItems().add(new ReviewCriteriaItem("c4", 4));
        settingsService.createReviewCriterias(artefact.getTypeArtefact().getUuid(), Lists.newArrayList(newCriteria));


        try {
            evaluation.clear();
            evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(0).getId());      // 0                 
            evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(1).getId());      // 1
            evaluation.put(criterias.get(2).getId(), criterias.get(2).getItems().get(2).getId());      // 2
            evaluation.put(criterias.get(3).getId(), criterias.get(3).getItems().get(3).getId());      // 3
            evaluation.put(criterias.get(4).getId(), criterias.get(4).getItems().get(4).getId());      // 4
            reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");
            fail();
        } catch (RequiredPropertyNullException e) {
        }
        
        criterias = reviewService.listCriteriasForBySubmissionArtefact(artefact.getUuid());
        assertEquals(criterias.size(), 6);
        
        forceSessionFlush();
        
        evaluation.clear();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(0).getId());      // 0                 
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(1).getId());      // 1
        evaluation.put(criterias.get(2).getId(), criterias.get(2).getItems().get(2).getId());      // 2
        evaluation.put(criterias.get(3).getId(), criterias.get(3).getItems().get(3).getId());      // 3
        evaluation.put(criterias.get(4).getId(), criterias.get(4).getItems().get(4).getId());      // 4
        evaluation.put(criterias.get(5).getId(), criterias.get(5).getItems().get(4).getId());      // 4
        reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");
        review_1 = reviewService.loadReview(s1.getUuid(), REVIEWER_1);
        assertEquals(review_1.getScore(), 58);
        assertEquals(review_1.getArtefactEvaluations().size(), 1);
        assertEquals(review_1.getArtefactEvaluations().iterator().next().getScore(), 58);
    }

    @Test(groups = {"service"}, dependsOnMethods = {"createSimpleReview"})    
    
    public void createMoreReviews() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException, TopicNotUniqueException, ServiceExecutionFailed {

        List<Submission> submissions = prepareData();

        forceSessionFlush();

        
        
        // recenzia na prvy
        Submission s1 = submissions.get(0);
        SubmissionArtefact artefact = null;
        Iterator<SubmissionArtefact> iterator = s1.getArtefacts().iterator();
        while (iterator.hasNext()) {
            artefact = iterator.next();
            if (artefact.getTypeArtefact().getArtefactType().getName().equals("Abstract")) {
                break;
            }
        }
                
        reviewService.createReviewRelation(s1.getUuid(), REVIEWER_1);
        reviewService.createReviewRelation(s1.getUuid(), REVIEWER_2);
        Review review_1 = reviewService.loadReview(s1.getUuid(), REVIEWER_1);
        Review review_2 = reviewService.loadReview(s1.getUuid(), REVIEWER_2);

        
        
                
        assertEquals(reviewService.listSubmissionReviews(submissionService.loadSubmissonByUuid(s1.getUuid()).getId()).size(), 2);
        
        
        
        List<ReviewCriteria> criterias = reviewService.listCriteriasForBySubmissionArtefact(artefact.getUuid());
        assertEquals(criterias.size(), 5);

        Map<Long, Long> evaluation = Maps.newHashMap();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(0).getId());


        // test na malo kriterii
        try {
            reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");
            fail();
        } catch (RequiredPropertyNullException e) {
        }

        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(1).getId());
        evaluation.put(criterias.get(2).getId(), criterias.get(2).getItems().get(2).getId());
        evaluation.put(criterias.get(3).getId(), criterias.get(3).getItems().get(3).getId());
        evaluation.put(criterias.get(4).getId(), criterias.get(4).getItems().get(4).getId());
        reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");

        evaluation.clear();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(4).getId());
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(3).getId());
        evaluation.put(criterias.get(2).getId(), criterias.get(2).getItems().get(4).getId());
        evaluation.put(criterias.get(3).getId(), criterias.get(3).getItems().get(3).getId());
        evaluation.put(criterias.get(4).getId(), criterias.get(4).getItems().get(4).getId());
        reviewService.updateReviewEvaluation(review_2.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");
        
        ReviewArtefact ra = reviewService.loadReviewArtefactEvaluation(artefact.getUuid(),REVIEWER_1);
        assertNotNull(ra);
        
        
        

        // recenzia na druhy prispevok    
        artefact = null;
        iterator = s1.getArtefacts().iterator();
        while (iterator.hasNext()) {
            artefact = iterator.next();
            if (artefact.getTypeArtefact().getArtefactType().getName().equals("Paper")) {
                break;
            }
        }

        criterias = reviewService.listCriteriasForBySubmissionArtefact(artefact.getUuid());

        evaluation.clear();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(4).getId());
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(3).getId());
        reviewService.updateReviewEvaluation(review_2.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");

        evaluation.clear();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(3).getId());
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(3).getId());
        reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");


        forceSessionFlush();

        // uprava hodnotenia prispevku
        evaluation.clear();
        evaluation.put(criterias.get(0).getId(), criterias.get(0).getItems().get(1).getId());
        evaluation.put(criterias.get(1).getId(), criterias.get(1).getItems().get(1).getId());
        reviewService.updateReviewEvaluation(review_1.getUuid(), s1.getUuid(), artefact.getUuid(), evaluation, "PUBLIC_NOTE", "PRIVATE_NOTE");
        
        
        
        List<ContributionReviewerReview> reviews = reviewService.listSubmissionReviews(s1.getId());
        assertEquals(reviews.size(), 2);
        
        List<Long> ids = submissionService.listSubmissionTypeArtefactsIds(s1.getType().getId());
        assertEquals(ids.size(),2);
        
        
        ids = submissionTypeArtefactDao.listSubmissionTypeArtefactsIdsByOrderinSubmissionType(s1.getType().getId());
        assertEquals(ids.size(),2);
        
        

   }
}
