package consys.event.conference.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Sets;
import consys.common.utils.date.DateProvider;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.NewUserInvitation;
import consys.event.conference.core.AbstractConferenceTest;
import consys.event.conference.core.bo.*;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import consys.event.conference.core.exception.SubmissionTypeNotUniqueException;
import consys.event.conference.core.exception.TopicNotUniqueException;
import consys.event.conference.core.service.cache.CachedSubmissionArtefact;
import java.util.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SubmissionServiceTests extends AbstractConferenceTest {

    @Test(groups = {"service"})
    public void testBasicCreateScenario() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, TopicNotUniqueException, NoRecordException, ServiceExecutionFailed {

        // Vytvorime si typy prispevkov
        SubmissionType st = new SubmissionType();
        st.setName("Type 1");
        settingsService.createSubmissionType(st);

        SubmissionTypeArtefact sta1 = new SubmissionTypeArtefact();
        sta1.setRequired(true);
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("Abstract");
        sta1.setArtefactType(at1);
        SubmissionTypeArtefact sta2 = new SubmissionTypeArtefact();
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("Keywords");
        sta2.setArtefactType(at2);
        settingsService.createSubmissionArtefactTypes(st.getUuid(), Lists.newArrayList(sta1, sta2));

        // Vytvorime topicy
        SubmissionTopic t1 = new SubmissionTopic();
        t1.setName("t1");
        topicService.createTopic(t1);
        SubmissionTopic t2 = new SubmissionTopic();
        t2.setName("t2");
        topicService.createTopic(t2);
        SubmissionTopic t3 = new SubmissionTopic();
        t3.setName("t3");
        topicService.createTopic(t3);

        // Nacitame UUID pre novy prispevok
        String submissionUuid_1 = submissionService.createSubmissionUuid();

        // Najskor vytvorime nove artefakty
        String sta_1 = submissionService.createSubmissionArtefact(submissionUuid_1, sta1.getUuid(), generate());
        String sta_2 = submissionService.createSubmissionArtefact(submissionUuid_1, sta2.getUuid(), generate());

        assertFalse(StringUtils.isBlank(sta_1));
        assertFalse(StringUtils.isBlank(sta_2));

        // @TEST na co ci su v nahrate v kase
        Map<String, CachedSubmissionArtefact> cachedArtefacts = artefactCache.get(submissionUuid_1);
        assertEquals(cachedArtefacts.size(), 2);
        assertEquals(cachedArtefacts.get(sta1.getUuid()).getArtefactUuid(), sta_1);
        assertEquals(cachedArtefacts.get(sta2.getUuid()).getArtefactUuid(), sta_2);

        // Vytvorime prispevok
        Set<Invitation> contributors = Sets.newHashSet();
        contributors.add(new NewUserInvitation(System.currentTimeMillis() + "@email.cz", "Name"));

        Submission s1 = new Submission();
        s1.setUuid(submissionUuid_1);
        s1.setTitle("Title 1");
        s1.getTopics().add(t1);
        s1.getTopics().add(t2);
        s1.setType(st);

        // @TEST na zamenu UUID artefaktov

        SubmissionArtefact sa1 = new SubmissionArtefact();
        sa1.setTypeArtefact(sta1);
        sa1.setUuid(sta_2);
        s1.getArtefacts().add(sa1);

        SubmissionArtefact sa2 = new SubmissionArtefact();
        sa2.setTypeArtefact(sta2);
        sa2.setUuid(sta_1);
        s1.getArtefacts().add(sa2);

        try {
            submissionService.createSubmission(s1, contributors);
            fail("Zamena UUID artefaktov nepresla");
        } catch (ServiceExecutionFailed failed) {
        }

        // Opravime na spravne hodnoty a vytvorime 
        sa1.setUuid(sta_1);
        sa2.setUuid(sta_2);
        submissionService.createSubmission(s1, contributors);

        // @TEST nacitame a testujeme hodnoty        
        Submission s1_l = submissionService.loadSubmissionWithArtefacts(submissionUuid_1);
        assertEquals(s1_l.getUuid(), submissionUuid_1);
        assertEquals(s1_l.getArtefacts().size(), 2);

        for (SubmissionArtefact sa : s1_l.getArtefacts()) {
            if (sa.getTypeArtefact().equals(sta1)) {
                assertEquals(sa.getUuid(), sta_1);
            }
            if (sa.getTypeArtefact().equals(sta2)) {
                assertEquals(sa.getUuid(), sta_2);
            }
        }

        // @TEST v cache
        cachedArtefacts = artefactCache.get(submissionUuid_1);
        assertEquals(cachedArtefacts.size(), 2);
        assertEquals(cachedArtefacts.get(sta1.getUuid()).getArtefactUuid(), sta_1);
        assertEquals(cachedArtefacts.get(sta2.getUuid()).getArtefactUuid(), sta_2);

        // potvrdime ze sa to spravne ulozilo
        submissionService.approveSubmissionArtefacts(submissionUuid_1);

        // @TEST uz nemoze byt v cache
        cachedArtefacts = artefactCache.get(submissionUuid_1);
        assertTrue(cachedArtefacts == null || cachedArtefacts.isEmpty());
    }

    @Test(groups = {"service"}, dependsOnMethods = {"testBasicCreateScenario"})
    public void testListTypes() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException {
        SubmissionType t1 = new SubmissionType();
        t1.setName("Test 1");

        settingsService.createSubmissionType(t1);

        t1 = new SubmissionType();
        t1.setName("Test 2");
        settingsService.createSubmissionType(t1);

        t1 = new SubmissionType();
        t1.setName("Test 3");

        settingsService.createSubmissionType(t1);

        SubmissionTypeArtefact a1 = new SubmissionTypeArtefact();
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("Abstract");
        a1.setArtefactType(at1);
        SubmissionTypeArtefact a2 = new SubmissionTypeArtefact();
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("Keywords");
        a2.setArtefactType(at2);
        settingsService.createSubmissionArtefactTypes(t1.getUuid(), Lists.newArrayList(a1, a2));

        List<SubmissionTypeThumb> thumbs = submissionService.listSubmissionTypesThumbs(true);
        assertEquals(thumbs.size(), 1);
        thumbs = submissionService.listSubmissionTypesThumbs(false);
        assertEquals(thumbs.size(), 1);

        List<SubmissionTypeArtefact> artefacts = submissionService.listAllArtefactsForType(t1.getUuid());
        assertEquals(artefacts.size(), 2);
    }

    @Test(groups = {"service"}, dependsOnMethods = {"testBasicCreateScenario"})
    public void testLoadSubmissions() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, NoRecordException, TopicNotUniqueException, ServiceExecutionFailed {
        // Test : Filter ALL
        List<Submission> ss = prepareData();
        Submission s1 = ss.get(0);
        assertNotNull(s1);

        Submission sl1 = submissionService.loadSubmissionWithArtefacts(s1.getUuid());
        assertEquals(s1, sl1);
    }

    @Test(groups = {"service", "actual"}, dependsOnMethods = {"testBasicCreateScenario"})
    public void testCreateAndUpdate() throws RequiredPropertyNullException, SubmissionTypeNotUniqueException, TopicNotUniqueException, NoRecordException, ServiceExecutionFailed {
        // Vytvorime si typy prispevkov
        SubmissionType st = new SubmissionType();
        st.setName("Type 1");
        settingsService.createSubmissionType(st);

        SubmissionTypeArtefact sta1 = new SubmissionTypeArtefact();
        sta1.setRequired(true);
        SubmissionArtefactType at1 = new SubmissionArtefactType();
        at1.setName("Abstract");
        sta1.setArtefactType(at1);
        SubmissionTypeArtefact sta2 = new SubmissionTypeArtefact();
        SubmissionArtefactType at2 = new SubmissionArtefactType();
        at2.setName("Keywords");
        sta2.setArtefactType(at2);

        settingsService.createSubmissionArtefactTypes(st.getUuid(), Lists.newArrayList(sta1, sta2));

        // Vytvorime topicy
        SubmissionTopic t1 = new SubmissionTopic();
        t1.setName("t1");
        topicService.createTopic(t1);
        SubmissionTopic t2 = new SubmissionTopic();
        t2.setName("t2");
        topicService.createTopic(t2);
        SubmissionTopic t3 = new SubmissionTopic();
        t3.setName("t3");
        topicService.createTopic(t3);

        // Vytvorime co-authorov
        UserEvent a1 = new UserEvent();
        a1.setFullName("1");
        a1.setLastName("2");
        a1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        a1.setUuid(AUTHOR_1);
        UserEvent a2 = new UserEvent();
        a2.setFullName("1");
        a2.setLastName("2");
        a2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        a2.setUuid(AUTHOR_2);
        eventService.createUserEventWithRights(a1, ArrayUtils.EMPTY_STRING_ARRAY);
        eventService.createUserEventWithRights(a2, ArrayUtils.EMPTY_STRING_ARRAY);

        Set<Invitation> contributors = null;

        // Vytvorime prispevok 1: A1
        Submission s1 = new Submission();
        s1.setTitle("Title 1");
        s1.getTopics().add(t1);
        s1.getTopics().add(t2);
        s1.setUuid(UuidProvider.getUuid());
        s1.setType(st);

        // TEST 1 - nemame nastavene cykly takze sa bude aplikovat na vsetky artefakty
        logger.error("............................ TEST 1");
        try {
            contributors = Sets.newHashSet();
            contributors.add(new ExistingUserInvitation(a1.getUuid()));
            submissionService.createSubmission(s1, contributors);

            fail("Required property excpetion");
        } catch (RequiredPropertyNullException e) {
        }

        // TEST 2 - vytvorime                        
        logger.error("............................ TEST 2");
        SubmissionArtefact sa1 = new SubmissionArtefact();
        sa1.setTypeArtefact(sta1);
        sa1.setUuid(submissionService.createSubmissionArtefact(s1.getUuid(), sta1.getUuid(), generate()));
        s1.getArtefacts().add(sa1);

        submissionService.createSubmission(s1, contributors);

        s1 = submissionService.loadSubmissionWithArtefacts(s1.getUuid());
        assertEquals(s1.getTopics().size(), 2);
        assertEquals(s1.getContributors().size(), 1);

        contributors = Sets.newHashSet();
        contributors.add(new ExistingUserInvitation(a1.getUuid()));
        contributors.add(new ExistingUserInvitation(a2.getUuid()));

        // TEST 3 - upravime pridanim noveho kontributora a topiku
        logger.error("............................ TEST 3");
        submissionService.updateSubmission(s1.getUuid(),
                "TT", null,
                "A", contributors,
                Sets.newHashSet(t3));

        s1 = submissionService.loadSubmissionWithArtefacts(s1.getUuid());
        assertEquals(s1.getTopics().size(), 1);
        assertEquals(s1.getContributors().size(), 2);

        // TEST 4 - pridame novy artefakt
        logger.error("............................ TEST 4");
        SubmissionArtefact sa2 = new SubmissionArtefact();
        sa2.setTypeArtefact(sta2);
        sa2.setUuid(UuidProvider.getUuid());

        // ulozit sa1u do DB jako na update
        submissionService.updateSubmission(s1.getUuid(),
                "TT", null,
                "A", contributors,
                Sets.newHashSet(t3));

        s1 = submissionService.loadSubmissionWithArtefacts(s1.getUuid());

        submissionService.updateSubmissionArtefact(s1.getUuid(), sta2.getUuid(), generate());

        // nevolali sme zadny approve
        assertEquals(artefactCache.get(s1.getUuid()).size(), 2);

        assertEquals(s1.getArtefacts().size(), 2);
        // TEST 5 - pridame novy artefakt
        logger.error("............................ TEST 5");

        submissionService.updateSubmission(s1.getUuid(),
                "TT", null,
                "A", contributors,
                Sets.newHashSet(t3));
        s1 = submissionService.loadSubmissionWithArtefacts(s1.getUuid());
        assertEquals(s1.getArtefacts().size(), 2);


        // vytvorime cykly
        // TEST 6 - pridame novy artefakt
        logger.error("............................ TEST 6");
        ReviewCycle c1 = new ReviewCycle();
        c1.setSubmissionType(st);
        Calendar c = GregorianCalendar.getInstance();
        DateProvider.substractDays(c, 2);
        c1.setSubmitFrom(c.getTime());
        c = GregorianCalendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 3);
        c1.setSubmitTo(c.getTime());
        c1.getArtefacts().add(sta1);
        c1.getArtefacts().add(sta2);
        settingsService.createReviewCycles(st.getUuid(), Lists.newArrayList(c1));


        s1 = new Submission();
        s1.setTitle("Title 1");
        s1.getTopics().add(t1);
        s1.getTopics().add(t2);
        s1.setType(st);

        contributors = Sets.newHashSet();
        contributors.add(new ExistingUserInvitation(a1.getUuid()));
        try {
            submissionService.createSubmission(s1, contributors);
            fail("Required property excpetion");
        } catch (RequiredPropertyNullException e) {
        }
    }
}
