CREATE OR REPLACE FUNCTION conference.bid_for_review_function(reviewer_uuid TEXT, start_offset INT, max_resulst INT) RETURNS setof RECORD AS
$BODY$
DECLARE

	rec	RECORD;
BEGIN
	-- Najskor najdeme vsetky artefaty ktore su pre dany typ prispevku
        FOR rec IN
		SELECT
			typ.name, -- z typu artefaktu
			sub.id,sub.uuid,sub.authors,sub.state,sub.submitted_date,sub.title,sub.subtitle, -- z prispevku
			bid.interest -- Reviewer bid ak existuje
		FROM    conference.submission as sub inner join conference.submission_type as typ on typ.id=sub.id_submission_type left outer join conference.reviewer_bid as bid on bid.submission_id=sub.id
		WHERE   bid.user_id=1 or true
		ORDER BY sub.submitted_date
		LIMIT $3 OFFSET $2

	LOOP
		RETURN NEXT rec;
	END LOOP;

        RETURN;
END;
$BODY$
LANGUAGE plpgsql