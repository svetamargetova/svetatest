package consys.event.badges.core.export;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import consys.event.export.pdf.FontProvider;
import consys.event.export.pdf.FontProvider.FontStyle;
import consys.event.export.pdf.PdfA4Utils;
import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.lang.SystemUtils;

/**
 * Generator vysacek
 *
 * @author pepa
 */
public class PdfBadgesGenerator {

    // data
    private Document document;
    private PdfWriter writer;

    private void addMetadata() {
        document.addTitle("Badges");
        document.addAuthor("Takeplace");
        document.addCreator("Takeplace");
        document.setMargins(0, 0, 0, 0);
    }

    public void createPdfBadges(OutputStream os, List<BadgeParticipantInfo> participants, BadgeSizeType size,
            boolean closeOutputStream) throws Exception {
        generate(os, participants, size, closeOutputStream);
    }

    private void generate(OutputStream os, List<BadgeParticipantInfo> participants, final BadgeSizeType size,
            boolean closeOutputStream) throws Exception {
        startWork(size, os);

        Badge badge = new Badge(document, writer, size);

        switch (size) {
            case SIZE_90_X_50:
                drawBadges90x50(badge, participants);
                break;
            case SIZE_101_X_76:
                drawBadges101x76(badge, participants);
                break;
        }

        stopWork(closeOutputStream);
    }

    private void startWork(BadgeSizeType size, OutputStream os) throws Exception {
        switch (size) {
            case SIZE_90_X_50:
                document = new Document(PageSize.A4);
                break;
            case SIZE_101_X_76:
                document = new Document(PageSize.A4.rotate());
                break;
            default:
                document = new Document(PageSize.A4);
                break;
        }

        writer = PdfWriter.getInstance(document, os);
        writer.setPageEvent(new PageHelper());

        addMetadata();

        document.open();
    }

    private void stopWork(boolean closeOutputStream) {
        if (closeOutputStream) {
            writer.flush();
            document.close();
        }
    }

    private void drawBadges90x50(Badge badge, List<BadgeParticipantInfo> participants) throws Exception {
        final int left = 15;
        final int top = 10;
        final float badgeHeight = PdfA4Utils.yps(badge.getHeight_MM());
        int counter = 0;

        for (BadgeParticipantInfo bpi : participants) {
            int x = left + ((counter % 2) * badge.getWidth_MM());
            int y = top + ((counter / 2) * badge.getHeight_MM());
            float end = badge.generateOnPosition(x, y, bpi);

            counter++;

            if (end < badgeHeight && (counter - 1) % 2 != 0) {
                newPage();
                counter = 0;
            }
        }
    }

    private void drawBadges101x76(Badge badge, List<BadgeParticipantInfo> participants) throws Exception {
        final int left = 15;
        final int top = 100;
        final float badgeHeight = PdfA4Utils.yps(badge.getHeight_MM());
        int counter = 0;

        for (BadgeParticipantInfo bpi : participants) {
            int x = left + ((counter % 2) * badge.getWidth_MM());
            int y = top + ((counter / 2) * badge.getHeight_MM());
            float end = badge.generateOnPosition(x, y, bpi);

            counter++;

            if (end < badgeHeight && (counter - 1) % 2 != 0) {
                newPage();
                counter = 0;
            }
        }
    }

    private void newPage() throws Exception {
        document.newPage();
    }

    private class PageHelper extends PdfPageEventHelper {

        // komponenty
        private PdfTemplate template;

        @Override
        public void onOpenDocument(PdfWriter writer, Document document) {
            super.onOpenDocument(writer, document);
            template = writer.getDirectContent().createTemplate(PdfA4Utils.xps(10), PdfA4Utils.yps(30));
        }

        @Override
        public void onCloseDocument(PdfWriter writer, Document document) {
            super.onCloseDocument(writer, document);
            try {
                template.beginText();
                FontProvider.changeFont(FontStyle.ARIAL_8_REGULAR, template);
                template.showText(String.valueOf(writer.getPageNumber() - 1));
                template.endText();
            } catch (Exception ex) {
                throw new IllegalStateException("Problem input total page number");
            }
        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            super.onEndPage(writer, document);
            try {
                Paragraph number = new Paragraph(writer.getPageNumber() + " /", FontProvider.font(FontStyle.ARIAL_8_REGULAR));
                number.setAlignment(Paragraph.ALIGN_RIGHT);
                ColumnText ct = PdfA4Utils.columnText(writer, 0, 286.7f, 19, 10);
                ct.addElement(number);
                ct.go();
                writer.getDirectContent().addTemplate(template, PdfA4Utils.xps(20), PdfA4Utils.yps(6));
            } catch (Exception ex) {
                throw new IllegalStateException("Problem input page number");
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Image image = null;
        //image = ImageIO.read(new File("c:/data/nydb.jpg"));

        List<BadgeParticipantInfo> participants = new ArrayList<BadgeParticipantInfo>();
        participants.add(info("Josef HUBR", "Java Developer, Freelancer", "NYBD", image));
        participants.add(info("Jaroslav ŠKRABÁLEK", "acemcee", "Match2NMP", image));
        participants.add(new BadgeParticipantInfo("Skřítek Pajdálínek", "Java Developer, ACEMCEE", "NYBD"));
        participants.add(info("Bernd Ottovordemgentschenfelde", "Bender", "Match2NMP", image));
        participants.add(new BadgeParticipantInfo("Pavol \"Borec\" Grešša, jediný na světě celičkém", "Regional Center for Advanced Technologies and Materials", "Match2NMP"));
        participants.add(new BadgeParticipantInfo("Josef Hubr", "Riga Technical University Institute of General Chemical Engineering"));
        participants.add(new BadgeParticipantInfo("Jaroslav Škrabálek", "Šéf nejvyšší vládnoucí tvrdou rukou v ACEMCEE", "Match2NMP"));
        participants.add(info("Pavol \"Borec\" Grešša, jediný na světě", "Java Developer & Software Architect & DB specialist, ACEMCEE", "Match2NMP", image));
        participants.add(new BadgeParticipantInfo("Josef Hubr", "Java Developer, Freelancer", "Match2NMP"));
        participants.add(new BadgeParticipantInfo("Jaroslav Škrabálek", "acemcee", ""));
        participants.add(new BadgeParticipantInfo("Pavol Grešša", "Java Developer, ACEMCEE", "Match2NMP"));
//        participants.add(new BadgeParticipantInfo("Jaroslav Škrabálek", "acemcee", ""));

        try {
            PdfBadgesGenerator gen = new PdfBadgesGenerator();
            BadgeSizeType size = BadgeSizeType.SIZE_101_X_76;

            if (SystemUtils.IS_OS_LINUX) {
                gen.createPdfBadges(new FileOutputStream(new File("/tmp/consys-badges.pdf")), participants, size, true);
            } else if (SystemUtils.IS_OS_WINDOWS) {
                gen.createPdfBadges(new FileOutputStream(new File("c:\\data\\consys-badges.pdf")), participants, size, true);
            } else if (SystemUtils.IS_OS_MAC_OSX) {
                gen.createPdfBadges(new FileOutputStream(new File("/Users/palo/consys-badges.pdf")), participants, size, true);
            }
        } catch (Exception ex) {
            System.err.println("Chyba " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }

    private static BadgeParticipantInfo info(String name, String affiliation, String eventAcronym, Image image) {
        BadgeParticipantInfo i = new BadgeParticipantInfo(name, affiliation, eventAcronym);
        i.setEventLogo(image);
        i.setDate("September 1-2, 2012");
        return i;
    }
}
