package consys.event.badges.core.export;

import java.awt.Image;
import org.apache.commons.lang.StringUtils;

/**
 * Data do PdfBadgesGenerator
 * @author pepa
 */
public class BadgeParticipantInfo {

    // data
    private String eventAcronym;
    private Image eventLogo;
    private String name;
    private String affiliation;
    private String date;

    public BadgeParticipantInfo(String name, String affiliation) {
        this(name, affiliation, "");
    }

    public BadgeParticipantInfo(String name, String affiliation, String eventAcronym) {
        this.name = name;
        this.affiliation = affiliation;
        this.eventAcronym = eventAcronym;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public boolean isAffiliation() {
        return StringUtils.isNotEmpty(affiliation);
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getEventAcronym() {
        return eventAcronym;
    }

    public void setEventAcronym(String eventAcronym) {
        this.eventAcronym = eventAcronym;
    }

    public Image getEventLogo() {
        return eventLogo;
    }

    public void setEventLogo(Image eventLogo) {
        this.eventLogo = eventLogo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
