package consys.event.badges.core.export;

/**
 * Typy rozmeru badge
 * @author pepa
 */
public enum BadgeSizeType {

    SIZE_90_X_50(90, 50),
    SIZE_101_X_76(101, 76);
    // data
    private int widthMM;
    private int heightMM;

    private BadgeSizeType(int widthMM, int heightMM) {
        this.widthMM = widthMM;
        this.heightMM = heightMM;
    }

    public int getWidthMM() {
        return widthMM;
    }

    public int getHeightMM() {
        return heightMM;
    }
}
