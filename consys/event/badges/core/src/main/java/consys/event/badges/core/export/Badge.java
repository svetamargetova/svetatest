package consys.event.badges.core.export;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfWriter;
import consys.event.export.pdf.FontProvider;
import consys.event.export.pdf.FontProvider.FontStyle;
import consys.event.export.pdf.PdfA4Utils;

/**
 * Stará se o vykreslení jednoho badge
 * @author pepa
 */
public class Badge {

    // konstanty - barvy
    public static final BaseColor COLOR_BLACK = BaseColor.BLACK;
    public static final BaseColor COLOR_WHITE = BaseColor.WHITE;
    // data - pdf
    private Document document;
    private PdfWriter writer;
    // data
    private BadgeSizeType size;

    public Badge(Document document, PdfWriter writer, BadgeSizeType size) {
        this.document = document;
        this.writer = writer;
        this.size = size;
    }

    public int getHeight_MM() {
        return size.getHeightMM();
    }

    public int getWidth_MM() {
        return size.getWidthMM();
    }

    public float generateOnPosition(final int x_mm, final int y_mm, BadgeParticipantInfo info) throws Exception {
        // ramecek
        float end = drawBorder(x_mm, y_mm);

        // logo nebo zkratka eventu
        drawEventLogoOrAcronym(x_mm, y_mm, info);

        // jmeno
        float endName = drawName(x_mm, y_mm, info);

        // vykresli afilaci
        float endAffiliation = drawAffiliation(x_mm, endName + 0.5f, info);

        switch (size) {
            case SIZE_90_X_50:
                drawDateIfVacancy(endAffiliation, x_mm, y_mm, info);
                break;
            case SIZE_101_X_76:
                drawDate(x_mm, y_mm, info);
                break;
            default:
                drawDateIfVacancy(endAffiliation, x_mm, y_mm, info);
                break;
        }

        return end;
    }

    /** @return spodni pozici okraje */
    private float drawBorder(int x_mm, int y_mm) throws Exception {
        Rectangle rectBlack = PdfA4Utils.rectangle(x_mm, y_mm, size.getWidthMM(), size.getHeightMM());
        rectBlack.setBackgroundColor(COLOR_BLACK);
        document.add(rectBlack);

        Rectangle rectWhite = PdfA4Utils.rectangle(x_mm + 0.5f, y_mm + 0.5f, size.getWidthMM() - 1f, size.getHeightMM() - 1f);
        rectWhite.setBackgroundColor(COLOR_WHITE);
        document.add(rectWhite);

        return rectBlack.getBottom();
    }

    /** vykresli logo akce, pokud neni zadano tak na stred zkratku akce */
    private void drawEventLogoOrAcronym(int x_mm, int y_mm, BadgeParticipantInfo info) throws Exception {
        final int x = x_mm + 5;
        final int y = y_mm + 3;
        final int width = size.getWidthMM() - 10;
        final int height = 12;

        if (info.getEventLogo() == null) {
            Paragraph p = new Paragraph(info.getEventAcronym(), FontProvider.font(FontStyle.ARIAL_13_BOLD_ITALICS));
            p.setAlignment(Paragraph.ALIGN_CENTER);
            drawColumnText(p, x, y, width, height);
        } else {
            PdfA4Utils.drawImage(writer, info.getEventLogo(), x, y, width, height);
        }
    }

    /** 
     * Vykresli jmeno ucastnika
     * @return spodni pozici okraje 
     */
    private float drawName(int x_mm, int y_mm, BadgeParticipantInfo info) throws Exception {
        final int x = x_mm + 5;
        final int width = size.getWidthMM() - 10;
        final int height = 12;
        final int y;

        switch (size) {
            case SIZE_90_X_50:
                y = y_mm + 18;
                break;
            case SIZE_101_X_76:
                y = y_mm + 28;
                break;
            default:
                y = y_mm + 18;
                break;
        }

        Paragraph p = new Paragraph(info.getName(), FontProvider.font(FontStyle.ARIAL_17_BOLD));
        p.setAlignment(Paragraph.ALIGN_CENTER);

        return drawColumnText(p, x, y, width, height);
    }

    /** vykresli afilaci */
    private float drawAffiliation(int x_mm, float y, BadgeParticipantInfo info) throws Exception {
        final int x = x_mm + 5;
        final int width = size.getWidthMM() - 10;
        final int height = 8;

        Paragraph p = new Paragraph(info.getAffiliation(), FontProvider.font(FontStyle.ARIAL_11_REGULAR));
        p.setAlignment(Paragraph.ALIGN_CENTER);

        return drawColumnText(p, x, y, width, height);
    }

    /** vykresli datum poradani akce */
    private void drawDate(int x_mm, int y_mm, BadgeParticipantInfo info) throws Exception {
        final int x = x_mm + 5;
        final int width = size.getWidthMM() - 10;
        final int height = 8;
        final int y;

        switch (size) {
            case SIZE_90_X_50:
                y = y_mm + 41;
                break;
            case SIZE_101_X_76:
                y = y_mm + 61;
                break;
            default:
                y = y_mm + 41;
                break;
        }

        Paragraph p = new Paragraph(info.getDate(), FontProvider.font(FontStyle.ARIAL_9_REGULAR));
        p.setAlignment(Paragraph.ALIGN_CENTER);

        drawColumnText(p, x, y, width, height);
    }

    /** vykresli datum poradani akce, pokud je jeste na visacce misto */
    private void drawDateIfVacancy(final float endAffiliation, final int x_mm, final int y_mm, BadgeParticipantInfo info) throws Exception {
        if (endAffiliation - y_mm < 40) {
            drawDate(x_mm, y_mm, info);
        }
    }

    /** umi vykresli column a umi pripadne odradkovat */
    private float drawColumnText(Element e, float x, float y, float width, float height) throws Exception {
        ColumnText ct = PdfA4Utils.columnText(writer, x, y, width, height);
        final float yLine = ct.getYLine();
        ct.addElement(e);

        if (ct.go(true) == ColumnText.NO_MORE_TEXT) {
            PdfA4Utils.regenColumnText(ct, yLine, e);
            ct.go();
        } else {
            PdfA4Utils.resizeColumnText(ct, x, y, width, height * 2);
            PdfA4Utils.regenColumnText(ct, yLine, e);
            ct.go();
        }

        return PdfA4Utils.yPositionInMM(ct.getYLine());
    }
}
