package consys.event.badges.gwt.client.module;

import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.module.event.EventNavigationModel;
import java.util.List;

/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class BadgesModule implements EventModule {

    @Override
    public String getLocalizedModuleName() {
        return "";
    }

    @Override
    public void registerModuleRoles(List<ModuleRole> moduleRoles) {
    }

    @Override
    public void registerEventNavigation(EventNavigationModel model) {
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
    }

    @Override
    public void registerModule() {
    }

    @Override
    public void unregisterModule() {
    }
}
