package consys.event.badges.gwt.client;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.event.badges.gwt.client.module.BadgesModule;
import consys.event.common.gwt.client.module.EventCoreModule;
import consys.event.common.gwt.client.module.OverseerModule;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class EventBadgesEntryPoint extends DebugModuleEntryPoint {

    @Override
    public void initMocks() {
    }

    @Override
    public void registerModules(List<Module> modules) {
        modules.add(new OverseerModule());
        modules.add(new EventCoreModule());
        modules.add(new BadgesModule());
    }

    @Override
    public void registerForms(Map<String, Widget> formMap) {
    }
}
