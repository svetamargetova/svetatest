package consys.event.overseer.management.core.dao.jdbc;

import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.event.overseer.management.core.OverseerManager;
import consys.event.overseer.management.core.bo.Event;
import consys.event.overseer.management.core.exception.DatabaseUserCreateException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */
@ContextConfiguration(locations = {"overseer-core-spring.xml", "classpath:consys/event/common/test/hibernate.conf.xml"})
public class EventDaoTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private EventDaoImpl dao;
    @Autowired
    private OverseerManager manager;

    @Test(groups = {"dbNonTransactional"})
    public void createEventTest() {
        Event e = new Event();
        e.setAcronym("ACMC");
        e.setCreated(DateProvider.getCurrentDate());
        e.setEventUuid("aa");
        dao.createEvent(e);
    }

    @Test(groups = {"dao"})
    public void CRUD() throws DatabaseUserCreateException, InstantiationException {
        Event e1 = new Event();
        e1.setAcronym("A");
        e1.setCreated(DateProvider.getCurrentDate());
        e1.setDbPassword("Pass");
        e1.setDbUrl("url");
        e1.setDbUser("uiser");
        e1.setEventUuid(UuidProvider.getUuid());
        manager.createEvent(e1);

        Map<String, Long> data = new HashMap<String, Long>();
        data.put(e1.getEventUuid(), System.currentTimeMillis());
        dao.updateLastEventActivity(data);

        Event e2 = new Event();
        e2.setAcronym("B");
        e2.setCreated(DateProvider.getCurrentDate());
        e2.setDbPassword("Pass");
        e2.setDbUrl("url");
        e2.setDbUser("uiser");
        e2.setEventUuid(UuidProvider.getUuid());
        manager.createEvent(e2);


        List<Event> events = dao.listAllEvents();
        assertEquals(events.size(), 2);

        for (Event event : events) {
            assertNotNull(event.getAcronym());
            assertNotNull(event.getCreated());
            assertNotNull(event.getDbPassword());
            assertNotNull(event.getDbUrl());
            assertNotNull(event.getDbUser());
            assertNotNull(event.getEventUuid());
            assertNotNull(event.getId());
        }

        for (Event event : events) {
            manager.deleteEvent(event.getEventUuid());
        }

        events = dao.listAllEvents();
        assertEquals(events.size(), 0);
    }

    @Test(groups = {"dao"})
    public void dbConnections() {
        dao.loadDbSessionCount();
    }
}
