package consys.event.overseer.management.core.monitor;

import consys.event.overseer.connector.EventId;
import consys.event.overseer.management.core.OverseerManager;
import consys.event.overseer.management.core.bo.Event;
import consys.event.overseer.management.core.dao.EventDao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pepa
 */
public class EventMonitorSessionRemover implements Runnable {

    // logger
    private static Logger log = LoggerFactory.getLogger(EventMonitorSessionRemover.class);
    // konstanty
    private static final int FIVE_MINUTES_IN_MILISECONDS = 300000;
    private static final int SESSION_THRESHOLD = 50;
    private static final int WARN_THRESHOLD = 180;
    private static final int TO_MINUTES = FIVE_MINUTES_IN_MILISECONDS / 60000;
    // dao
    private EventDao eventDao;
    // data
    private final long removerCreated = System.currentTimeMillis();
    private EventMonitor monitor;
    /** uchovava kolikrat byla u konkretni akce ukoncena session, pro chytrejsi ukoncovani v zavislosti na pouzivani akce */
    private Map<String, Counter> history;
    private OverseerManager manager;

    public EventMonitorSessionRemover() {
        history = new HashMap<String, Counter>();
    }

    @Override
    public void run() {
        log.info("Monitor session remover start");

        // aktualni pocet session do databaze
        final int dbSessions = eventDao.loadDbSessionCount();

        if (dbSessions <= WARN_THRESHOLD) {
            log.info("   Db session opened: " + dbSessions);
        } else {
            log.warn("   Is opened too much sessions: " + dbSessions);
        }

        /** pokud je pocet spojeni nizsi nez prah, nebudeme nic procistovat */
        if (dbSessions > SESSION_THRESHOLD) {
            // nacteme z databaze akce
            List<Event> events = eventDao.listEventsWithActivity(removerCreated, System.currentTimeMillis());
            log.debug("   Events loaded: " + events.size());

            if (!events.isEmpty() && manager != null) {
                // nacteme aktualni stav aktivity
                Map<String, Long> lastActivity = monitor.getLastActivity();
                long now = System.currentTimeMillis();

                for (Event event : events) {
                    if (event.getPriorityTo() != null && event.getPriorityTo().getTime() > now) {
                        // akce ma prioritu, takze preskakujeme ruseni session factory
                        continue;
                    }
                    processEventSessionFactory(event, now, lastActivity);
                }
            }
        }

        log.info("Monitor session remover end");
    }

    /** rozhoduje jestli ze zrusi session k akci nebo jeste zustane pripojena */
    private void processEventSessionFactory(Event event, long now, Map<String, Long> lastActivity) {
        final long diffInMiliSeconds = now - event.getLastActivity().getTime();
        final Long activity = lastActivity.get(event.getEventUuid());
        final boolean wasUpdate = !activity.equals(event.getLastActivity().getTime());

        Counter counter = history.get(event.getEventUuid());
        if (counter == null) {
            counter = new Counter(event);
            history.put(event.getEventUuid(), counter);
        }

        if (counter.toDestroy(activity, diffInMiliSeconds, wasUpdate)) {
            //log.info("   Event included to destroy session: " + event.getEventUuid() + " " + event.getAcronym());
            destroySession(event);
        } /*else {
         if (log.isDebugEnabled()) {
         StringBuilder sb = new StringBuilder("   Event NOT included to destroy session: ");
         sb.append(event.getEventUuid());
         sb.append(" ");
         sb.append(event.getAcronym());
         sb.append(" (");
         sb.append(counter.getCount() * TO_MINUTES);
         sb.append(" minutes )");
         log.debug(sb.toString());
         }
         }*/
    }

    private void destroySession(Event event) {
        manager.getFactoryPool().destroySessionFactory(new EventId(event.getEventUuid()));
    }

    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    public void setMonitor(EventMonitor monitor) {
        this.monitor = monitor;
    }

    public void setManager(OverseerManager manager) {
        this.manager = manager;
    }

    /** pomocna trida pro pocitadlo kolikrat byla session akce zrusena */
    private class Counter {

        private static final int RISE_TOP = 10;
        /** citac */
        private int count;
        /** vytvoreni citace */
        private long lastAccess;
        /** uuid akce */
        private String uuid;

        public Counter(Event event) {
            this.uuid = event.getEventUuid();
            lastAccess = event.getLastActivity().getTime();
            reset();
        }

        private void rise() {
            if (count < RISE_TOP) {
                count++;
            }
        }

        private long getLimit() {
            return count * FIVE_MINUTES_IN_MILISECONDS;
        }

        private void reset() {
            count = 1;
        }

        public int getCount() {
            return count;
        }

        public boolean toDestroy(final long lastActivity, final long diffInMiliSeconds, final boolean wasUpdate) {
            if (lastActivity != lastAccess) {
                final long activityDiff = lastActivity - lastAccess;
                final long actualLimit = getLimit();
                final long risedLimit = actualLimit + FIVE_MINUTES_IN_MILISECONDS;
                if (activityDiff > actualLimit && activityDiff < risedLimit) {
                    // je pres limit, ale v povolenem presahu, zvysime interval
                    rise();
                    //log.debug("   Rised interval for event " + uuid + " to " + (count * TO_MINUTES) + " minutes");
                }
                lastAccess = lastActivity;
            }

            final boolean inRange = diffInMiliSeconds <= getLimit();
            if (inRange || wasUpdate) {
                // nachazi se v povolenem intervalu nebo byla aktualizovana hodnota, ktera jeste neni v db
                return false;
            }

            final boolean result = getLimit() < diffInMiliSeconds;
            if (result) {
                // vynulujeme pocitatadlo
                reset();
            }
            return result;
        }
    }
}
