package consys.event.overseer.management.core.dao.jdbc;

import consys.common.utils.AsciiUtils;
import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateProvider;
import consys.event.common.core.EventModuleRegistrator;
import consys.event.overseer.management.core.bo.Event;
import consys.event.overseer.management.core.dao.EventDao;
import consys.event.overseer.management.core.exception.DatabaseCreateException;
import consys.event.overseer.management.core.exception.DatabaseUserCreateException;
import consys.event.overseer.management.core.exception.DeleteDatabaseException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

/**
 *
 * @author Palo
 */
public class EventDaoImpl implements EventDao, ApplicationContextAware {

    private static final int MAX_DB_NAME_TRY = 100;
    private static final int MAX_USER_NAME_TRY = 50;
    private static final Logger logger = LoggerFactory.getLogger(EventDaoImpl.class);
    private SimpleJdbcTemplate jdbcTemplate;
    private TransactionTemplate txTemplate;
    private ApplicationContext context;
    private String databaseUrl;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new SimpleJdbcTemplate(dataSource);
    }

    public void setTransactionManager(PlatformTransactionManager txManager) {
        this.txTemplate = new TransactionTemplate(txManager);
        this.txTemplate.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_REQUIRED);
    }

    private boolean isDbNameReserved(final String name) {
        try {
            long l = jdbcTemplate.queryForLong("SELECT id FROM event WHERE db_url = ?", new Object[]{name});
            return l == 0 ? false : true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    private boolean isDbUserReserved(final String user) {
        try {
            int i = jdbcTemplate.queryForInt("SELECT usesysid FROM pg_user WHERE usename = ?", new Object[]{user});
            return i == 0 ? false : true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    private String getDbUrl(String dbName) {
        return getDatabaseUrlPrefix() + dbName;
    }

    private String getDbName(String dbName) {
        return dbName + "_db";
    }

    @Override
    public Event loadEvent(String eventUuid) {
        if (StringUtils.isBlank(eventUuid)) {
            throw new NullPointerException("Missing event uuid");
        }
        String sql = "SELECT * FROM event WHERE event_uuid=:uuid";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("uuid", eventUuid, java.sql.Types.VARCHAR);
        return jdbcTemplate.queryForObject(sql, new EventRowMapper(), params);
    }

    @Override
    public void createEvent(final Event eD) throws DatabaseUserCreateException {
        // test ci ma overseer prava na vytvaranie eventu
        if (!testPermissions(jdbcTemplate)) {
            logger.error("Overseers database user has not SUPERUSER and CREATEDB permissions!");
            throw new DatabaseCreateException("Overseers permission expcetion!");
        }

        // pripravime nazvy a zistime ci su volne
        String tmp = prepareDBName(eD);
        String dbName = tmp;
        String dbUserName = tmp;
        String password = UuidProvider.getUuid();

        int id = 0;
        int tests = 0;
        // Zistime ci je volne meno databaze
        while (true) {
            if (!isDbNameReserved(getDbUrl(getDbName(dbName + id)))) {
                break;
            }
            id++;
            tests++;
            if (tests > MAX_DB_NAME_TRY) {
                throw new DatabaseCreateException("Max DB tries !!");
            }
        }

        dbName = getDbName(dbName + id);
        String dbUrl = getDbUrl(dbName);

        // Zistime ci je volny meno uzivatela
        id = 0;
        tests = 0;
        while (true) {
            if (!isDbUserReserved(dbUserName + id)) {
                break;
            }
            id++;
            tests++;
            if (tests > MAX_USER_NAME_TRY) {
                throw new DatabaseUserCreateException("Max User tries !!");
            }
        }


        dbUserName = dbUserName + id;


        eD.setDbPassword(password);
        eD.setDbUrl(dbUrl);
        eD.setDbUser(dbUserName);

        // CREATE USER

        txTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    logger.info("-- Creating ROLE " + eD.getDbUser());
                    String sql = "CREATE ROLE "
                            + eD.getDbUser()
                            + " WITH LOGIN ENCRYPTED PASSWORD '"
                            + eD.getDbPassword()
                            + "' NOSUPERUSER NOCREATEDB NOCREATEROLE";
                    try {
                        jdbcTemplate.getJdbcOperations().execute(sql);
                    } catch (BadSqlGrammarException bsge) {
                        logger.error("Error creating new database user!", bsge);
                        throw new DatabaseUserCreateException("User propably already exists!");
                    }

                    if (!isDbUserReserved(eD.getDbUser())) {
                        throw new DatabaseUserCreateException("User is not in database!");
                    }

                    doCreateEvent(eD);
                } catch (DataAccessException ex) {
                    logger.error("Create new Event failed!", ex);
                    throw ex;
                }
            }
        });

        // vytvorenie databaze - nemoze byt v transakcii
        String sql = String.format("CREATE DATABASE \"%s\" WITH OWNER = %s ENCODING = 'UTF8' CONNECTION LIMIT = -1;", dbName, dbUserName);
        jdbcTemplate.getJdbcOperations().execute(sql);
        // v databazi vytvorime schemata pre event

        DriverManagerDataSource dm = new DriverManagerDataSource(eD.getDbUrl(), eD.getDbUser(), eD.getDbPassword());
        dm.setDriverClassName("org.postgresql.Driver");
        SimpleJdbcTemplate template = new SimpleJdbcTemplate(dm);

        createSchemas(eD, template);
        createLanguage(template);
        logger.info("Create event completed.");
    }

    /** pripravi hlavni cast nazvu databaze a uzivatele databaze */
    private String prepareDBName(Event eD) {
        String tmp = eD.getAcronym().trim().toLowerCase().replace(' ', '_');
        tmp = AsciiUtils.foldToASCII(tmp.toCharArray(), tmp.length());
        return tmp;
    }

    protected void doCreateEvent(Event eD) {
        logger.info("-- Creating Event ..." + eD);
        final String sql = "INSERT INTO event "
                + "(id,event_uuid, db_pass, db_user, db_url, db_created,acronym )"
                + " VALUES (nextval('event_seq'),:event_uuid,:db_pass, :db_user, :db_url, :db_created,:acronym)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("event_uuid", eD.getEventUuid(), java.sql.Types.VARCHAR);
        params.addValue("db_pass", eD.getDbPassword(), java.sql.Types.VARCHAR);
        params.addValue("db_user", eD.getDbUser(), java.sql.Types.VARCHAR);
        params.addValue("db_url", eD.getDbUrl(), java.sql.Types.VARCHAR);
        params.addValue("db_created", DateProvider.getCurrentDate(), java.sql.Types.TIMESTAMP);
        params.addValue("acronym", eD.getAcronym(), java.sql.Types.VARCHAR);
        jdbcTemplate.update(sql, params);
    }

    private boolean testPermissions(SimpleJdbcTemplate template) {
        String sql = "select 1 as has from pg_user where usename=CURRENT_USER and usecreatedb=true and usesuper=true";
        int has = template.queryForInt(sql, Collections.EMPTY_MAP);
        return has == 1 ? true : false;
    }

    private void createLanguage(SimpleJdbcTemplate template) {
        logger.info("Selecting language plpgsql");
        String testLoadLanguage = "select * from pg_language where lanname='plpgsql'";
        List languages = template.getJdbcOperations().query(testLoadLanguage, new RowMapper() {
            @Override
            public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
                logger.info("Language plpgsql already registred from template");
                return Boolean.TRUE;
            }
        });
        if (languages == null || languages.isEmpty()) {
            logger.info("Creating language plpgsql");
            template.getJdbcOperations().execute("CREATE LANGUAGE plpgsql");
        }
    }

    private void createSchemas(Event event, SimpleJdbcTemplate template) {
        String owner = event.getDbUser();

        Collection<EventModuleRegistrator> l = eventModules();

        for (Iterator<EventModuleRegistrator> it = l.iterator(); it.hasNext();) {
            EventModuleRegistrator eventModule = it.next();
            String schema = eventModule.getSchemaName();
            if (schema != null) {
                logger.info("Creating schema " + schema + " ...");
                String sql = String.format("CREATE SCHEMA %s AUTHORIZATION %s", schema, owner);
                template.getJdbcOperations().execute(sql);
                sql = String.format("GRANT ALL ON SCHEMA %s TO %s", schema, owner);
                template.getJdbcOperations().execute(sql);
            }
        }
    }

    @Override
    public void cloneEvent(Event originalEvent, final Event cloneEvent) throws DatabaseUserCreateException, DatabaseCreateException {
        // test ci ma overseer prava na vytvaranie eventu
        if (!testPermissions(jdbcTemplate)) {
            logger.error("Overseers database user has not SUPERUSER and CREATEDB permissions!");
            throw new DatabaseCreateException("Overseers permission expcetion!");
        }

        // pripravime nazvy a zistime ci su volne
        String tmp = prepareDBName(cloneEvent);
        String dbName = createDbName(tmp);

        cloneEvent.setDbUrl(getDbUrl(dbName));
        // uzivatele nechame puvodniho, at se nemusi prirazovat novy ke vsem puvodnim tabulkam
        cloneEvent.setDbUser(originalEvent.getDbUser());
        cloneEvent.setDbPassword(originalEvent.getDbPassword());

        // odseknem jdbc:postgresql://localhost:5432/ at mame nazev databaze
        final String originalDbName = originalEvent.getDbUrl().substring(33);
        // upravime limit pripojeni puvodni databaze
        jdbcTemplate.getJdbcOperations().execute(String.format("ALTER DATABASE \"%s\" CONNECTION LIMIT 1;", originalDbName));

        // odpojeni sessions na puvodni db, uz by tam nic nemelo byt, ale pokud se bude nekdo stourat na serveru, tak at to nevadi pri vytvareni
        jdbcTemplate.getJdbcOperations().execute(String.format("SELECT pg_terminate_backend(procpid) FROM pg_stat_activity WHERE datname = '%s' AND procpid <> pg_backend_pid();", originalDbName));

        // vytvorenie databaze - nemoze byt v transakcii
        String sql = String.format("CREATE DATABASE \"%s\" WITH OWNER = %s ENCODING = 'UTF8' CONNECTION LIMIT = -1 TEMPLATE \"%s\";", dbName, originalEvent.getDbUser(), originalDbName);
        jdbcTemplate.getJdbcOperations().execute(sql);

        // vraceni limitu puvodni databaze
        jdbcTemplate.getJdbcOperations().execute(String.format("ALTER DATABASE \"%s\" CONNECTION LIMIT -1;", originalDbName));

        DriverManagerDataSource dm = new DriverManagerDataSource(cloneEvent.getDbUrl(), cloneEvent.getDbUser(), cloneEvent.getDbPassword());
        dm.setDriverClassName("org.postgresql.Driver");
        SimpleJdbcTemplate template = new SimpleJdbcTemplate(dm);

        createLanguage(template);

        doCreateEvent(cloneEvent);

        logger.info("Cloning event completed.");
    }

    /** vygeneruje unikatni nazev databaze */
    private String createDbName(String dbName) {
        int id = 0;
        int tests = 0;
        // Zistime ci je volne meno databaze
        while (true) {
            if (!isDbNameReserved(getDbUrl(getDbName(dbName + id)))) {
                break;
            }
            id++;
            tests++;
            if (tests > MAX_DB_NAME_TRY) {
                throw new DatabaseCreateException("Max DB tries !!");
            }
        }
        return getDbName(dbName + id);
    }

    /** vygeneruje unikatni nazev uzivatele databaze */
    private String createDbUserName(String dbUserName) {
        int id = 0;
        int tests = 0;
        while (true) {
            if (!isDbUserReserved(dbUserName + id)) {
                break;
            }
            id++;
            tests++;
            if (tests > MAX_USER_NAME_TRY) {
                throw new DatabaseUserCreateException("Max User tries !!");
            }
        }
        return dbUserName + id;
    }

    @Override
    public List<Event> listAllEvents() {
        String sql = "SELECT * FROM event";
        List<Event> list = jdbcTemplate.query(sql, new EventRowMapper(), Collections.EMPTY_MAP);
        return list;
    }

    @Override
    public List<Event> listEventsWithActivity(Long from, Long to) {
        Timestamp f = new Timestamp(from);
        Timestamp t = new Timestamp(to);
        StringBuilder sql = new StringBuilder("SELECT * FROM event WHERE last_activity > '");
        sql.append(f.toString());
        sql.append("' AND last_activity < '");
        sql.append(t.toString());
        sql.append("'");
        List<Event> list = jdbcTemplate.query(sql.toString(), new EventRowMapper(), Collections.EMPTY_MAP);
        return list;
    }

    @Override
    public void deleteEvent(final Event eD) {

        logger.info("Delete Event: {}", eD);

        /**
         * Najskor zmazeme zaznam v event tabulke v overseer databazi.
         */
        txTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    String sql = String.format("DELETE FROM event where id=%d", eD.getId());
                    int ops = jdbcTemplate.update(sql);
                    if (ops == 0) {
                        throw new DeleteDatabaseException("Delete event from overseer failed! Request " + sql + " returned ZERO ops.");
                    }
                } catch (DataAccessException ex) {
                    logger.error("Delete event record failed!", ex);
                    throw ex;
                }
            }
        });

        /**
         * Potom zmazeme samotnu databazu
         */
        String dbName = eD.getDbUrl().substring(eD.getDbUrl().lastIndexOf('/') + 1);
        String sql = String.format("DROP DATABASE \"%s\";", dbName);
        jdbcTemplate.getJdbcOperations().execute(sql);

        /**
         * Nakoniec zmazeme samotnu rolu databaze
         */
        txTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    logger.info("-- Delete db role: {}", eD.getDbUser());
                    String sql = String.format("DROP ROLE %s;", eD.getDbUser());
                    try {
                        jdbcTemplate.getJdbcOperations().execute(sql);
                    } catch (BadSqlGrammarException bsge) {
                        logger.error("Error dropping database role!", bsge);
                        throw new DeleteDatabaseException("Drop role failed!");
                    }
                } catch (DataAccessException ex) {
                    logger.error("Delete event failed!", ex);
                    throw ex;
                }
            }
        });
    }

    @Override
    public void updateEvent(Event eventDescriptor) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void executeSqlOnEvent(Event event, String sql) throws SQLException {
        SingleConnectionDataSource source = new SingleConnectionDataSource(event.getDbUrl(), event.getDbUser(), event.getDbPassword(), true);
        source.setAutoCommit(false);
        Connection c = source.getConnection();
        try {
            Statement t = c.createStatement();
            t.execute(sql);
            t.close();
            c.commit();
        } catch (SQLException e) {
            logger.error("Error executing SQL: " + sql, e);
            c.rollback();
            throw e;
        } catch (RuntimeException re) {
            logger.error("Error executing SQL: " + sql, re);
            c.rollback();
            throw re;
        } finally {
            c.close();
        }
    }

    /**
     * @return the databaseUrl
     */
    public String getDatabaseUrlPrefix() {
        return databaseUrl;
    }

    /**
     * @param databaseUrl the databaseUrl to set
     */
    public void setDatabaseUrlPrefix(String databaseUrl) {
        this.databaseUrl = databaseUrl;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    private Collection<EventModuleRegistrator> eventModules() {
        return context.getBeansOfType(EventModuleRegistrator.class).values();
    }

    @Override
    public void updateLastEventActivity(Map<String, Long> data) {
        if (data.size() > 0) {
            // TODO: zkusit najit neco rychlejsiho pokud neco je
            for (Map.Entry<String, Long> entry : data.entrySet()) {
                StringBuilder sql = new StringBuilder("update event set last_activity='");
                sql.append(new Timestamp(entry.getValue()));
                sql.append("' where event_uuid='");
                sql.append(entry.getKey());
                sql.append("'");
                jdbcTemplate.getJdbcOperations().execute(sql.toString());
            }
        }
    }

    @Override
    public int loadDbSessionCount() {
        return jdbcTemplate.queryForInt("select count(*) from pg_stat_activity");
    }

    /**
     *
     * @author Palo
     */
    private static final class EventRowMapper implements RowMapper<Event> {

        @Override
        public Event mapRow(ResultSet rs, int rowNum) throws SQLException {
            Event e = new Event();
            e.setAcronym(rs.getString("acronym"));
            e.setDbPassword(rs.getString("db_pass"));
            e.setDbUrl(rs.getString("db_url"));
            e.setDbUser(rs.getString("db_user"));
            e.setCreated(rs.getDate("db_created"));
            e.setEventUuid(rs.getString("event_uuid"));
            e.setId(rs.getLong("id"));
            e.setLastActivity(rs.getTimestamp("last_activity"));
            e.setPriorityTo(rs.getDate("priority_to"));
            return e;
        }
    }
}
