/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.overseer.management.core.exception;

/**
 *
 * @author Palo
 */
public class DatabaseUserCreateException extends RuntimeException{
    private static final long serialVersionUID = 415542451468664433L;

    public DatabaseUserCreateException(String message) {
        super(message);
    }

}
