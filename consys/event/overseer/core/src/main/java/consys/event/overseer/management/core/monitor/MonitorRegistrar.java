package consys.event.overseer.management.core.monitor;

import com.google.common.collect.Maps;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 *
 * @author pepa
 */
public class MonitorRegistrar extends ScheduledTaskRegistrar implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger log = LoggerFactory.getLogger(MonitorRegistrar.class);
    // data
    private boolean initialized = false;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (!initialized) {
            initialized = true;

            Map<Runnable, String> cronTasks = Maps.newHashMapWithExpectedSize(1);

            EventMonitor monitor = event.getApplicationContext().getBean(EventMonitor.class);
            log.info("MonitorScheduler registred");
            cronTasks.put(monitor, "0 0/1 * * * ?");

            EventMonitorSessionRemover remover = event.getApplicationContext().getBean(EventMonitorSessionRemover.class);
            remover.setMonitor(monitor);
            log.info("MonitorSessionRemoverScheduler registred");
            cronTasks.put(remover, "0 0/5 * * * ?");

            // nastavime
            setCronTasks(cronTasks);

            // pustime namapovanie
            afterPropertiesSet();
        }
    }
}
