/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.overseer.management.core.exception;

/**
 * Nepodarila sa vytvorit databaza
 * @author Palo
 */
public class DatabaseCreateException extends RuntimeException{
    private static final long serialVersionUID = 415542451468664433L;

    public DatabaseCreateException(String message) {
        super(message);
    }

}
