/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.overseer.management.core.exception;

/**
 * Nepodarila sa odstranit databaza
 * @author Palo
 */
public class DeleteDatabaseException extends RuntimeException{
    private static final long serialVersionUID = 415542451468664433L;

    public DeleteDatabaseException(String message) {
        super(message);
    }

}
