package consys.event.overseer.management.core.bo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Palo
 */
public class Event implements Serializable {

    private static final long serialVersionUID = 7593428461800247913L;
    private Long id;
    private Date created;
    private String acronym;
    private String eventUuid;
    private String dbUser;
    private String dbPassword;
    private String dbUrl;
    private Timestamp lastActivity;
    private Date priorityTo;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * @return the dbUser
     */
    public String getDbUser() {
        return dbUser;
    }

    /**
     * @param dbUser the dbUser to set
     */
    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    /**
     * @return the dbPassword
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * @param dbPassword the dbPassword to set
     */
    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    /**
     * @return the dbUrl
     */
    public String getDbUrl() {
        return dbUrl;
    }

    /**
     * @param dbUrl the dbUrl to set
     */
    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    /**
     * @return the eventUuid
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * @param eventUuid the eventUuid to set
     */
    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Event eventId1 = (Event) o;
        return eventUuid.equals(eventId1.getEventUuid());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (this.eventUuid != null ? this.eventUuid.hashCode() : 0);
        return hash;
    }

    /**
     * @return the acronym
     */
    public String getAcronym() {
        return acronym;
    }

    /**
     * @param acronym the acronym to set
     */
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @Override
    public String toString() {
        return String.format("Event[ id=%s acronym=%s dbUser=%s dbUrl=%s uuid=%s]", id, acronym, dbUser, dbUrl, eventUuid);
    }

    /** posledni pristup k akci */
    public Timestamp getLastActivity() {
        return lastActivity;
    }

    /** posledni pristup k akci */
    public void setLastActivity(Timestamp lastActivity) {
        this.lastActivity = lastActivity;
    }

    /** do kdy nebude rusena session factory teto akce */
    public Date getPriorityTo() {
        return priorityTo;
    }

    /** do kdy nebude rusena session factory teto akce */
    public void setPriorityTo(Date priorityTo) {
        this.priorityTo = priorityTo;
    }
}
