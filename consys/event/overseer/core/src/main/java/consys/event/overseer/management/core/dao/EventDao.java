package consys.event.overseer.management.core.dao;

import consys.event.overseer.management.core.bo.Event;
import consys.event.overseer.management.core.exception.DatabaseCreateException;
import consys.event.overseer.management.core.exception.DatabaseUserCreateException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * DAO pre akcie nad databazov overseera, nad ktorou sa budu vytvara udalosti
 * @author Palo
 */
public interface EventDao {

    /**
     * Vytvori novu akciu. Uskutocni sa to tak ze:
     * <ul>
     *  <li>
     *      Vytvori noveho uzivatela databaze so ziadnymi pravami
     * </li>
     *  <li>
     *      Vytvory novu databazu s vlastnikom ktory zvniukol v predoslom kroku
     * </li>
     * <li>
     *      Na zaklade toho ake moduly su zaplatene sa vytvori schema databaze.
     *      (Pozn. zatial sa vytvoria vsetky schemy)
     * </li>
     *
     * </ul>
     */
    public void createEvent(Event event) throws DatabaseUserCreateException, DatabaseCreateException;

    public void cloneEvent(Event originalEvent, Event cloneEvent) throws DatabaseUserCreateException, DatabaseCreateException;

    public Event loadEvent(String eventUuid);

    /** vrati aktualni celkovy pocet pripojeni k databazi */
    int loadDbSessionCount();

    public List<Event> listAllEvents();

    /** nacte akce s aktivitou od, do */
    List<Event> listEventsWithActivity(Long from, Long to);

    public void updateEvent(Event eventDescriptor);

    public void deleteEvent(Event eventDescriptor);

    public void executeSqlOnEvent(Event e, String sql) throws SQLException;

    /** aktulizuje u akci jejich posledni aktivitu */
    void updateLastEventActivity(Map<String, Long> data);
}
