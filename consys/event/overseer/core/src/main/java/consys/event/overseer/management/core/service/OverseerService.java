package consys.event.overseer.management.core.service;

import consys.event.overseer.management.core.bo.Event;
import consys.event.overseer.management.core.exception.DatabaseUserCreateException;
import java.util.List;

/**
 *
 * @author Palo
 */
public interface OverseerService {

    /** List vseckych eventu */
    public List<Event> listAll();

    /** vytvori event */
    public void createEvent(Event descriptor) throws DatabaseUserCreateException, InstantiationException;

    /** naklonuje event */
    public void cloneEvent(Event originalEvent, Event cloneEvent) throws DatabaseUserCreateException, InstantiationException;

    public Event loadEvent(String uuid);

    public void deleteEvent(String uuid);

    /** zaktualizuje event */
    public void updateEvent(Event descriptor);
}
