package consys.event.overseer.management.core.monitor;

import consys.event.overseer.connector.monitor.EventActivityMonitor;
import consys.event.overseer.management.core.dao.EventDao;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class EventMonitor implements EventActivityMonitor, Runnable {

    // dao
    private EventDao eventDao;
    // data
    /** uchovava systemovy cas posledniho dotazu na akci */
    private Map<String, Long> lastAccess;

    public EventMonitor() {
        lastAccess = new HashMap<String, Long>(50);
    }

    @Override
    public void eventActivity(String eventUuid) {
        lastAccess.put(eventUuid, System.currentTimeMillis());
    }

    @Override
    public void run() {
        eventDao.updateLastEventActivity(lastAccess);
    }

    public Map<String, Long> getLastActivity() {
        return lastAccess;
    }

    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }
}
