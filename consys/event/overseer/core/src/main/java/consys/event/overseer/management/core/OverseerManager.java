package consys.event.overseer.management.core;

import consys.event.overseer.connector.EventId;
import consys.event.overseer.connector.OverseerEvent;
import consys.event.overseer.connector.OverseerSessionFactoryPool;
import consys.event.overseer.management.core.bo.Event;
import consys.event.overseer.management.core.dao.EventDao;
import consys.event.overseer.management.core.exception.DatabaseUserCreateException;
import consys.event.overseer.management.core.service.OverseerService;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class OverseerManager implements OverseerService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(OverseerManager.class);
    private EventDao eventDao;
    private HashMap<String, Event> allEventsMap;
    /* Factory pool tu musi byt z dovodu vytvorenie a spravy sessions */
    private OverseerSessionFactoryPool factoryPool;

    public OverseerManager() {
        allEventsMap = new HashMap<String, Event>(100);
    }

    @Override
    public Event loadEvent(String uuid) {
        return allEventsMap.get(uuid);
    }

    @Override
    public void createEvent(Event descriptor) throws DatabaseUserCreateException, InstantiationException {
        getEventDao().createEvent(descriptor);
        Event event = eventDao.loadEvent(descriptor.getEventUuid());
        descriptor.setId(event.getId());
        addEventSessionFactory(descriptor);
    }

    @Override
    public void cloneEvent(Event originalEvent, Event cloneEvent) throws DatabaseUserCreateException, InstantiationException {
        EventId eventId = new EventId(originalEvent.getEventUuid());
        factoryPool.destroySessionFactory(eventId);

        getEventDao().cloneEvent(originalEvent, cloneEvent);
        addEventSessionFactory(originalEvent);

        Event event = eventDao.loadEvent(cloneEvent.getEventUuid());
        cloneEvent.setId(event.getId());
        addEventSessionFactory(cloneEvent);
    }

    @Override
    public void deleteEvent(String uuid) {
        if (StringUtils.isBlank(uuid)) {
            throw new NullPointerException();
        }
        Event event = loadEvent(uuid);
        // zavreme session factory
        factoryPool.destroySessionFactory(new EventId(uuid));
        // odstranime event
        getEventDao().deleteEvent(event);
    }

    @Override
    public List<Event> listAll() {
        return getEventDao().listAllEvents();
    }

    @Override
    public void updateEvent(Event descriptor) {
        getEventDao().updateEvent(descriptor);
    }

    /**
     * Po nastaveni vsetkych properties sa natiahne z DB vsetky eventy, ktore
     * tu budu drzane ako taka "cache". 
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Initializing Session factories");
        List<Event> events = listAll();
        for (Event e : events) {
            // pripojeni budeme spustet az na dotaz
            OverseerEvent connector = new OverseerEvent(e.getDbUrl(), e.getDbPassword(), e.getDbUser(), e.getEventUuid());
            getFactoryPool().addManagedEventConnector(connector);
            allEventsMap.put(e.getEventUuid(), e);
        }
    }

    private void addEventSessionFactory(Event ped) throws InstantiationException {
        allEventsMap.put(ped.getEventUuid(), ped);

        OverseerEvent connector = new OverseerEvent(ped.getDbUrl(), ped.getDbPassword(), ped.getDbUser(), ped.getEventUuid());
        logger.info("---> " + ped.getAcronym() + "(" + ped.getEventUuid() + ") to SessionFactoryPool");
        getFactoryPool().addNewSessionFactory(connector);
    }

    /**
     * @return the eventDao
     */
    public EventDao getEventDao() {
        return eventDao;
    }

    /**
     * @param eventDao the eventDao to set
     */
    @Autowired
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    /**
     * @return the factoryPool
     */
    public OverseerSessionFactoryPool getFactoryPool() {
        return factoryPool;
    }

    /**
     * @param factoryPool the factoryPool to set
     */
    @Autowired(required = true)
    public void setFactoryPool(OverseerSessionFactoryPool factoryPool) {
        this.factoryPool = factoryPool;
    }
}
