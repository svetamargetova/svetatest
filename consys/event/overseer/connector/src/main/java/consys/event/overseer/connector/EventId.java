package consys.event.overseer.connector;

/**
 *
 * @author Palo
 */
public class EventId {

    private final String id;

    public EventId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final EventId eventId1 = (EventId) o;
        return id.equals(eventId1.id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }





}
