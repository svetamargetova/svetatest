package consys.event.overseer.connector;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTransactionManager;

/**
 *
 * @author Palo
 */
public class OverseerTransactionManager extends HibernateTransactionManager {

    private static final long serialVersionUID = 211903180795610509L;
    private OverseerSessionFactoryPool sessionFactoryPool;
    private EventRequestContextProvider contextProvider;

    public OverseerTransactionManager() {
    }

    @Override
    public void afterPropertiesSet() {
        if (getSessionFactoryPool() == null) {
            throw new NullPointerException("Property 'sessionFactory' is required");
        }
        if (getContextProvider() == null) {
            throw new NullPointerException("Property 'contextProvider' is required");
        }
    }

    @Override
    public SessionFactory getSessionFactory() {
        EventRequestContext e = getContextProvider().getContext();
        return getSessionFactoryPool().getSessionFactory(e.getEventUuid());
    }

    /**
     * @return the sessionFactoryPool
     */
    public OverseerSessionFactoryPool getSessionFactoryPool() {
        return sessionFactoryPool;
    }

    /**
     * @param sessionFactoryPool the sessionFactoryPool to set
     */
    @Autowired(required = true)
    public void setSessionFactoryPool(OverseerSessionFactoryPool sessionFactoryPool) {
        this.sessionFactoryPool = sessionFactoryPool;
    }

    /**
     * @return the contextProvider
     */
    public EventRequestContextProvider getContextProvider() {
        return contextProvider;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    public void setContextProvider(EventRequestContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }
}
