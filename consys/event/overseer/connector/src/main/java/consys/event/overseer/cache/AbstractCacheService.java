/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.overseer.cache;

import consys.event.overseer.cache.ehcache.EhCacheService;



/**
 * Implementacia cachovacej sluzby ktora zaobaluje konkretnu implementaciu. 
 * Potomkovia uz len definuju konstruktor 
 * 
 * @author palo
 */
public abstract class AbstractCacheService<K,V> extends EhCacheService<K,V>{

    public AbstractCacheService(String cacheName) {
        super(cacheName);
    }
                        
    
}
