package consys.event.overseer.connector;

import java.io.Serializable;

/**
 * Identifikator pre connector
 * @author Palo
 */
public class OverseerEvent implements Serializable {

    private static final long serialVersionUID = -4036629472980066783L;
    // data
    private String dbUrl;
    private String dbPass;
    private String dbUser;
    private EventId eventUuid;

    public OverseerEvent(String dbUrl, String dbPass, String dbUser, String eventUuid) {
        this.dbUrl = dbUrl;
        this.dbPass = dbPass;
        this.dbUser = dbUser;
        this.eventUuid = new EventId(eventUuid);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final OverseerEvent eventId1 = (OverseerEvent) o;
        return getEventUuid().equals(eventId1.getEventUuid());
    }

    @Override
    public int hashCode() {
        return getEventUuid().hashCode();
    }

    @Override
    public String toString() {
        return getEventUuid().toString();
    }

    /**
     * @return the connectorId
     */
    public EventId getEventUuid() {
        return eventUuid;
    }

    /**
     * @return the dbUrl
     */
    public String getDbUrl() {
        return dbUrl;
    }

    /**
     * @return the dbPass
     */
    public String getDbPass() {
        return dbPass;
    }

    /**
     * @return the dbUser
     */
    public String getDbUser() {
        return dbUser;
    }
}
