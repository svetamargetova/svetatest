package consys.event.overseer.connector.dao;

import consys.common.utils.collection.Maps;
import consys.event.overseer.connector.EventId;
import consys.event.overseer.connector.EventRequestContext;
import consys.event.overseer.connector.EventRequestContextProvider;
import consys.event.overseer.connector.OverseerSessionFactoryPool;
import java.util.HashMap;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.support.DaoSupport;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

/**
 * Plne implementujuce ako HibernateDaoSupport
 * @author Palo
 */
public class OverseerDaoSupport extends DaoSupport {

    private OverseerSessionFactoryPool sessionPool;
    private EventRequestContextProvider contextProvider;
    /* Pre kazdu SessionFactory ktora je pouzita sa vytvori HibernateTemplate */
    private HashMap<EventId, HibernateTemplate> hibernateTemplateMap = Maps.newHashMap();

    @Override
    protected void checkDaoConfig() throws IllegalArgumentException {
        if (this.getSessionPool() == null) {
            throw new IllegalArgumentException("ConnectorSessionFactoryPool is not null!");
        }
    }

    public final void setSessionFactoryPool(OverseerSessionFactoryPool pool) {
        setSessionPool(pool);
    }

    public final OverseerSessionFactoryPool getSessionFactoryPool() {
        return getSessionPool();
    }

    /**
     * Return the Hibernate SessionFactory used by this DAO.
     */
    private SessionFactory getSessionFactoryFromPool(EventId eventUuid) {
        return getSessionFactoryPool().getSessionFactory(eventUuid);
    }

    /**
     * Create a HibernateTemplate for the given SessionFactory.
     * Only invoked if populating the DAO with a SessionFactory reference!
     * <p>Can be overridden in subclasses to provide a HibernateTemplate instance
     * with different configuration, or a custom HibernateTemplate subclass.
     * @param sessionFactory the Hibernate SessionFactory to create a HibernateTemplate for
     * @return the new HibernateTemplate instance
     * @see #setSessionFactory
     */
    private HibernateTemplate createHibernateTemplate(SessionFactory sessionFactory) {
        return new HibernateTemplate(sessionFactory);
    }

    /**
     * Vytvori HibernateTemplate a vlozi do mapy s hibernate tempaltama
     */
    private HibernateTemplate createHibernateTemplate(EventId connectorId) {
        SessionFactory sessionFactory = getSessionFactoryFromPool(connectorId);
        HibernateTemplate templateForSession = createHibernateTemplate(sessionFactory);
        hibernateTemplateMap.put(connectorId, templateForSession);
        return templateForSession;
    }

    /**
     * Ziska na zaklade ConnectorId hibernate template alebo vyhodi null
     */
    private HibernateTemplate getHibernateTemplate(EventId id) {
        HibernateTemplate template = hibernateTemplateMap.get(id);
        if (template == null) {
            template = createHibernateTemplate(id);
        }
        return template;
    }

    /**
     * Return the Hibernate SessionFactory used by this DAO.
     */
    public final SessionFactory getSessionFactory() {
        EventRequestContext erq = contextProvider.getContext();
        HibernateTemplate hibernateTemplate = getHibernateTemplate(erq.getEventUuid());
        return (hibernateTemplate != null ? hibernateTemplate.getSessionFactory() : null);
    }

    /**
     * Return the HibernateTemplate for this DAO,
     * pre-initialized with the SessionFactory or set explicitly.
     * <p><b>Note: The returned HibernateTemplate is a shared instance.</b>
     * You may introspect its configuration, but not modify the configuration
     * (other than from within an {@link #initDao} implementation).
     * Consider creating a custom HibernateTemplate instance via
     * <code>new HibernateTemplate(getSessionFactory())</code>, in which
     * case you're allowed to customize the settings on the resulting instance.
     */
    public final HibernateTemplate getHibernateTemplate() {
        EventRequestContext erq = contextProvider.getContext();
        return getHibernateTemplate(erq.getEventUuid());
    }

    /**
     * Obtain a Hibernate Session, either from the current transaction or
     * a new one. The latter is only allowed if the
     * {@link org.springframework.orm.hibernate3.HibernateTemplate#setAllowCreate "allowCreate"}
     * setting of this bean's {@link #setHibernateTemplate HibernateTemplate} is "true".
     * <p><b>Note that this is not meant to be invoked from HibernateTemplate code
     * but rather just in plain Hibernate code.</b> Either rely on a thread-bound
     * Session or use it in combination with {@link #releaseSession}.
     * <p>In general, it is recommended to use HibernateTemplate, either with
     * the provided convenience operations or with a custom HibernateCallback
     * that provides you with a Session to work on. HibernateTemplate will care
     * for all resource management and for proper exception conversion.
     * @return the Hibernate Session
     * @throws DataAccessResourceFailureException if the Session couldn't be created
     * @throws IllegalStateException if no thread-bound Session found and allowCreate=false
     * @see org.springframework.orm.hibernate3.SessionFactoryUtils#getSession(SessionFactory, boolean)
     */
    protected final Session getSession()
            throws DataAccessResourceFailureException, IllegalStateException {

        return getSession(getHibernateTemplate().isAllowCreate());
    }

    /**
     * Obtain a Hibernate Session, either from the current transaction or
     * a new one. The latter is only allowed if "allowCreate" is true.
     * <p><b>Note that this is not meant to be invoked from HibernateTemplate code
     * but rather just in plain Hibernate code.</b> Either rely on a thread-bound
     * Session or use it in combination with {@link #releaseSession}.
     * <p>In general, it is recommended to use
     * {@link #getHibernateTemplate() HibernateTemplate}, either with
     * the provided convenience operations or with a custom
     * {@link org.springframework.orm.hibernate3.HibernateCallback} that
     * provides you with a Session to work on. HibernateTemplate will care
     * for all resource management and for proper exception conversion.
     * @param allowCreate if a non-transactional Session should be created when no
     * transactional Session can be found for the current thread
     * @return the Hibernate Session
     * @throws DataAccessResourceFailureException if the Session couldn't be created
     * @throws IllegalStateException if no thread-bound Session found and allowCreate=false
     * @see org.springframework.orm.hibernate3.SessionFactoryUtils#getSession(SessionFactory, boolean)
     */
    protected final Session getSession(boolean allowCreate)
            throws DataAccessResourceFailureException, IllegalStateException {

        return (!allowCreate ? SessionFactoryUtils.getSession(getSessionFactory(), false) : SessionFactoryUtils.getSession(
                getSessionFactory(),
                getHibernateTemplate().getEntityInterceptor(),
                getHibernateTemplate().getJdbcExceptionTranslator()));
    }

    /**
     * Convert the given HibernateException to an appropriate exception from the
     * <code>org.springframework.dao</code> hierarchy. Will automatically detect
     * wrapped SQLExceptions and convert them accordingly.
     * <p>Delegates to the
     * {@link org.springframework.orm.hibernate3.HibernateTemplate#convertHibernateAccessException}
     * method of this DAO's HibernateTemplate.
     * <p>Typically used in plain Hibernate code, in combination with
     * {@link #getSession} and {@link #releaseSession}.
     * @param ex HibernateException that occured
     * @return the corresponding DataAccessException instance
     * @see org.springframework.orm.hibernate3.SessionFactoryUtils#convertHibernateAccessException
     */
    protected final DataAccessException convertHibernateAccessException(HibernateException ex) {
        return getHibernateTemplate().convertHibernateAccessException(ex);
    }

    /**
     * Close the given Hibernate Session, created via this DAO's SessionFactory,
     * if it isn't bound to the thread (i.e. isn't a transactional Session).
     * <p>Typically used in plain Hibernate code, in combination with
     * {@link #getSession} and {@link #convertHibernateAccessException}.
     * @param session the Session to close
     * @see org.springframework.orm.hibernate3.SessionFactoryUtils#releaseSession
     */
    protected final void releaseSession(Session session) {
        SessionFactoryUtils.releaseSession(session, getSessionFactory());
    }

    /**
     * @return the sessionPool
     */
    public OverseerSessionFactoryPool getSessionPool() {
        return sessionPool;
    }

    /**
     * @param sessionPool the sessionPool to set
     */
    @Autowired(required = true)
    public void setSessionPool(OverseerSessionFactoryPool sessionPool) {
        this.sessionPool = sessionPool;
        sessionPool.addListenerOnDestroySessionFactory(this);
    }

    /**
     * @return the contextProvider
     */
    public EventRequestContextProvider getContextProvider() {
        return contextProvider;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    @Autowired(required = true)
    public void setContextProvider(EventRequestContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }

    /** provede akci pri zruseni session factory nektereho z eventu */
    public void onDestroyEventSessionFactory(EventId eventId) {
        hibernateTemplateMap.remove(eventId);
    }
}
