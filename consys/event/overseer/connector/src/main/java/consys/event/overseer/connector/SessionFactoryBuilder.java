package consys.event.overseer.connector;

import consys.event.overseer.connector.datasource.DataSourceFactoryPool;
import java.io.Serializable;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.springframework.core.io.Resource;

/**
 *
 * @author Palo
 */
public class SessionFactoryBuilder implements Serializable, ApplicationContextAware {

    private static final long serialVersionUID = 1427419802541156606L;
    private static final Logger log = LoggerFactory.getLogger(SessionFactoryBuilder.class);
    private static int counter = 0;
    private DataSourceFactoryPool dataSourceFactory;
    private Resource[] mappingLocations;
    private Properties hibernateProperties;
    private ApplicationContext applicationContext;

    public synchronized void destroySessionFactory(OverseerEvent event) {
        dataSourceFactory.destroyDataSource(event);
    }

    /**
     * Vytvorenie session factory. Potreba prevolat vsetky postprocesory springu preto
     * sa musi aolikovat autovire capable bean
     */
    public synchronized LocalSessionFactoryBean buildNewSessionFactory(OverseerEvent o) throws InstantiationException {
        AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();

        log.info("Creating new session factory for " + o);
        LocalSessionFactoryBean sFactory = new LocalSessionFactoryBean();
        sFactory.setDataSource(dataSourceFactory.createDataSource(o));
        sFactory.setMappingLocations(mappingLocations);
        sFactory.setHibernateProperties(hibernateProperties);
        Object initialized = beanFactory.initializeBean(sFactory, "sessionFactory" + counter++);

        return (LocalSessionFactoryBean) initialized;
    }

    /**
     * @param dataSourceFactory the dataSourceFactory to set
     */
    public void setDataSourceFactory(DataSourceFactoryPool dataSourceFactory) {
        this.dataSourceFactory = dataSourceFactory;
    }

    /**
     * Set locations of Hibernate mapping files, for example as classpath
     * resource "classpath:example.hbm.xml". Supports any resource location
     * via Spring's resource abstraction, for example relative paths like
     * "WEB-INF/mappings/example.hbm.xml" when running in an application context.
     * <p>Can be used to add to mappings from a Hibernate XML config file,
     * or to specify all mappings locally.
     * @see org.hibernate.cfg.Configuration#addInputStream
     */
    public void setMappingLocations(Resource[] mappingLocations) {
        this.mappingLocations = mappingLocations;
    }

    /**
     * Set Hibernate properties, such as "hibernate.dialect".
     * <p>Can be used to override values in a Hibernate XML config file,
     * or to specify all necessary properties locally.
     * <p>Note: Do not specify a transaction provider here when using
     * Spring-driven transactions. It is also advisable to omit connection
     * provider settings and use a Spring-set DataSource instead.
     * @see #setDataSource
     */
    public void setHibernateProperties(Properties hibernateProperties) {
        this.hibernateProperties = hibernateProperties;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
