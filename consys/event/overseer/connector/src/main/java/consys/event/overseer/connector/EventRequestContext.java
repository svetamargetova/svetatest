package consys.event.overseer.connector;

import java.io.Serializable;
import org.apache.commons.lang.StringUtils;



/**
 *
 * @author Palo
 */
public class EventRequestContext implements Serializable{
    private static final long serialVersionUID = -7558903409678930634L;

    private boolean initialized = false;
    private EventId eventUuid;
    private String userEventUuid;

    public EventRequestContext() {
    }


    public void initContext(String userEventUuid, String eventUuid){
        if(StringUtils.isBlank(eventUuid)){
            throw new IllegalArgumentException("Context missing properties");
        }
        this.eventUuid = new EventId(eventUuid);
        this.userEventUuid = userEventUuid;
        initialized = true;
    }

    
    /**
     * @return the userEventUuid
     */
    public String getUserEventUuid() {
        return userEventUuid;
    }

    /**
     * @return the eventUuid
     */
    public EventId getEventUuid() {
        return eventUuid;
    }

    /**
     * @return the initialized
     */
    public boolean isInitialized() {
        return initialized;
    }
}
