/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.overseer.connector.datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

/**
 *
 * @author Palo
 */
public class C3P0DataSourceConfigurator {

    enum Property {
        driverClass,
        acquireIncrement,
        acquireRetryAttempts,
        minPoolSize,
        maxPoolSize,
        initialPoolSize,
        preferredTestQuery,
        testConnectionOnCheckin,
        testConnectionOnCheckout,
        idleConnectionTestPeriod,
        maxIdleTime,
        maxConntectionAge;
    }

    public static void configure(Properties p, ComboPooledDataSource cpds) throws PropertyVetoException {
        int lenght = Property.values().length;
        for (int i = 0; i < lenght; i++) {
            Property prop = Property.values()[i];
            if (p.containsKey(prop.name())) {
                setProperty(prop, p.getProperty(prop.name()), cpds);
            }
        }
    }

    private static void setProperty(Property property, String value, ComboPooledDataSource cpds) throws PropertyVetoException {
        switch (property) {
            case driverClass:
                cpds.setDriverClass(value);
                break;
            case acquireIncrement:
                cpds.setAcquireIncrement(Integer.parseInt(value));                
                break;
            case minPoolSize:
                cpds.setMinPoolSize(Integer.parseInt(value));
                break;
            case maxPoolSize:
                cpds.setMaxPoolSize(Integer.parseInt(value));
                break;
            case maxIdleTime:
                cpds.setMaxIdleTime(Integer.parseInt(value));
                break;
            case acquireRetryAttempts:
                cpds.setAcquireRetryAttempts(Integer.parseInt(value));
                break;
            case preferredTestQuery:
                cpds.setPreferredTestQuery(value);
                break;
            case testConnectionOnCheckin:
                cpds.setTestConnectionOnCheckin(Boolean.parseBoolean(value));
                break;
            case testConnectionOnCheckout:
                cpds.setTestConnectionOnCheckout(Boolean.parseBoolean(value));
                break;
            case idleConnectionTestPeriod:
                cpds.setIdleConnectionTestPeriod(Integer.parseInt(value));
                break;
            case maxConntectionAge:
                cpds.setMaxConnectionAge(Integer.parseInt(value));
                break;
            case initialPoolSize:
                cpds.setInitialPoolSize(Integer.parseInt(value));
                break;
            default:
                throw new RuntimeException("Property neni podporovana!");

        }
    }
}
