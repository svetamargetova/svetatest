/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.overseer.connector;

import consys.common.core.exception.ConsysException;

/**
 * Realne nemapuje transakciu do abstraktnej metody ale mapuje hibernati session 
 * na konkretny data source.
 * @author palo
 */
public abstract class OverseerTransactionCallback<T>{
    
    /** Event id. pre aky event sa vykona  */
    private EventId eventId;

    public OverseerTransactionCallback() {
    }

    public OverseerTransactionCallback(EventId eventId) {
        this.eventId = eventId;
    }

    /**
     * Event id. pre aky event sa vykona
     * @return the eventId
     */
    public EventId getEventId() {
        return eventId;
    }

    /**
     * Event id. pre aky event sa vykona
     * @param eventId the eventId to set
     */
    public void setEventId(EventId eventId) {
        this.eventId = eventId;
    }    
    
    
    
    public abstract T doInEventTransaction(EventId event) throws ConsysException;
}
