package consys.event.overseer.connector.filter;

import consys.event.common.api.servlet.CommonServletProperties;
import consys.event.overseer.connector.EventRequestContextProvider;
import java.io.IOException;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Filter ktory nastavi kontext pre kazdy request na ktori bude namapovany. Ak
 * request nebude obsahovat povinne parametry <b>eid</b> uuid eventu a <b>uid</b>
 * uuid uzivatela tak nepusti vykonavanie dalej
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConnectorEventContextFilter implements Filter , CommonServletProperties{

    private static final Logger logger = LoggerFactory.getLogger(ConnectorEventContextFilter.class);
    
    private EventRequestContextProvider contextProvider;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        if (logger.isDebugEnabled()) {
            logger.debug("Initializing ConnectorFilter");
        }
        ApplicationContext ctx =
                WebApplicationContextUtils.getRequiredWebApplicationContext(filterConfig.getServletContext());
        Map<String, EventRequestContextProvider> providers = ctx.getBeansOfType(EventRequestContextProvider.class);
        if (providers.isEmpty()) {
            throw new ServletException("Application Context has no EventRequestContextProvider");
        } else if (providers.size() > 1) {
            throw new ServletException("Application Context has more than one EventRequestContextProvider");
        } else {
            contextProvider = providers.values().iterator().next();
            if (logger.isDebugEnabled()) {
                logger.debug("found EventRequestContextProvider");
            }
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String user = request.getParameter(FILTER_USER_TAG);
        String event = request.getParameter(FILTER_EVENT_TAG);

        if (StringUtils.isBlank(event)) {
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing event identificator ?eid=...");
            return;
        }

        
        contextProvider.initContext(user, event);        
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
