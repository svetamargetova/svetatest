package consys.event.overseer.connector.providers;

import consys.event.overseer.connector.EventRequestContext;
import consys.event.overseer.connector.EventRequestContextProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;



/**
 * Provider event contextu
 * @author Palo
 */
public class AppContextEventProvider implements ApplicationContextAware,EventRequestContextProvider{

    /**
     * Resolvovany alebo prednastaveny kontext.
     */
    private final ThreadLocal<EventRequestContext> currentContext = new ThreadLocal<EventRequestContext>();
    
    
    private static final Logger log = LoggerFactory.getLogger(AppContextEventProvider.class);

    private static final String DEFAULT_BEAN_NAME = "eventRequestContext";

    private ApplicationContext applicationContext;
    private String beanName;

    public AppContextEventProvider() {
        setBeanName(DEFAULT_BEAN_NAME);
        log.info("Initializing with bean_name="+getBeanName());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public EventRequestContext getContext(){
        EventRequestContext e = null;
        if(currentContext.get() == null){
            e = (EventRequestContext) applicationContext.getBean(getBeanName(), EventRequestContext.class);           
        }else{
            e = currentContext.get();
        }
        if(!e.isInitialized()){
            throw new IllegalArgumentException("EventRequestContext is not initialized!");
        }
        return e;
    }

    @Override
    public void initContext(String user, String event) {
        EventRequestContext e = (EventRequestContext) applicationContext.getBean(getBeanName(), EventRequestContext.class);
        e.initContext(user, event);
    }
    
    /**
     * Pozor musi sa aj cistit lebo to ostane ve vlaknu
     * @param ctx 
     */
    public void injectContext(EventRequestContext ctx){
        currentContext.set(ctx);
    }

    /**
     * @return the beanName
     */
    public final String getBeanName() {
        return beanName;
    }

    /**
     * @param beanName the beanName to set
     */
    public final void setBeanName(String beanName) {
        this.beanName = beanName;
    }


}
