/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.overseer.connector.datasource;


import com.google.common.collect.Maps;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import consys.common.utils.collection.Lists;
import consys.event.overseer.connector.EventId;
import consys.event.overseer.connector.OverseerEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;


/**
 * Factory pre ziskavanie pripojeni do databaz na zaklade jednotneho nastavenia
 * v properties. 
 * @author Palo
 */
public class DataSourceFactoryPool implements DisposableBean,PropertyChangeListener{

    private static final Logger log = LoggerFactory.getLogger(DataSourceFactoryPool.class);

    private Map<EventId,ComboPooledDataSource> mapDataSources;
    private Properties properties;

    public DataSourceFactoryPool() {
        mapDataSources = Maps.newHashMap();
    }

    public void destroyDataSource(OverseerEvent o){
        ComboPooledDataSource ds = mapDataSources.remove(o.getEventUuid());
        ds.close();
    }

    public ComboPooledDataSource createDataSource(OverseerEvent o) throws InstantiationException {
        ComboPooledDataSource dataSource;
        try {
            dataSource = new ComboPooledDataSource();
            C3P0DataSourceConfigurator.configure(properties, dataSource);
            dataSource.setProperties(properties);            
            dataSource.setUser(o.getDbUser());
            dataSource.setPassword(o.getDbPass());
            dataSource.setJdbcUrl(o.getDbUrl());
            dataSource.addPropertyChangeListener(this);            
        } catch (Exception ex) {
            throw new InstantiationException("Create DataSource failed! "+ex.toString());
        }
        mapDataSources.put(o.getEventUuid(), dataSource);
        log.info("Create data source successful "+dataSource.getJdbcUrl());
        return dataSource;
    }

    public void setConnectionProperties(Properties p){
        properties = p;
    }

    public Properties getConnectionProperties(){
        return properties;
    }

    /**
     * Autoamticke uzatvaranie pripojenia
     */
    @Override
    public void destroy() throws Exception {
        for (ComboPooledDataSource comboPooledDataSource: mapDataSources.values()) {            
            comboPooledDataSource.close();
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        log.debug(evt.getPropertyName()+" "+evt.getOldValue()+" -> "+evt.getNewValue());
    }
}
