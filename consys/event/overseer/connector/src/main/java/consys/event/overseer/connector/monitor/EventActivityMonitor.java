package consys.event.overseer.connector.monitor;

/**
 * Pro sledovani aktivity akci na overseeru
 * @author pepa
 */
public interface EventActivityMonitor {

    /** provolano pri aktivite akce */
    void eventActivity(String eventUuid);
}
