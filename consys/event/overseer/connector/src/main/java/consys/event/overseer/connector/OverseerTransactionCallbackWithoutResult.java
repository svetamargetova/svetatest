/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.overseer.connector;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author palo
 */
public abstract class OverseerTransactionCallbackWithoutResult extends OverseerTransactionCallback<Object> {

    @Override
    public Object doInEventTransaction(EventId event) throws ConsysException {
        doInTransactionWithoutResult(event);
        return null;
    }
            
    
    protected abstract void doInTransactionWithoutResult(EventId event) throws ConsysException;
}
