package consys.event.overseer.connector;

import consys.common.core.exception.ConsysException;
import org.hibernate.*;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.SQLExceptionTranslator;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 *
 * @author palo
 */
public class OverseerTransactionCallbackExecutor extends HibernateAccessor {

    @Autowired
    private OverseerSessionFactoryPool factoryPool;

    public OverseerTransactionCallbackExecutor() {
    }

    public <T extends Object> T execute(OverseerTransactionCallback<T> callback) {

        // session factory pre tento event
        final SessionFactory sf = factoryPool.getSessionFactory(callback.getEventId());

        // defaultny SQL translator
        SQLExceptionTranslator defaultJdbcExceptionTranslator = SessionFactoryUtils.newJdbcExceptionTranslator(sf);

        // konkretna session
        final Session session = SessionFactoryUtils.getSession(sf, getEntityInterceptor(), defaultJdbcExceptionTranslator);

        // session holder - ak nahodov mame uz session ale to nemame v kazdom pripade nechame podla vzoru HibernateInterceptora
        SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager.getResource(sf);

        // priznak ci sme nasli transkaciu alebo je potreba vytvorit novu
        boolean existingTransaction = (sessionHolder != null && sessionHolder.containsSession(session));

        if (existingTransaction) {
            logger.debug("Found thread-bound Session for HibernateInterceptor");
        } else {
            if (sessionHolder != null) {
                sessionHolder.addSession(session);
            } else {
                TransactionSynchronizationManager.bindResource(sf, new SessionHolder(session));
            }
        }

        FlushMode previousFlushMode = null;
        T t = null;
        try {
            previousFlushMode = applyFlushMode(session, existingTransaction);
            enableFilters(session);
            t = (T) callback.doInEventTransaction(callback.getEventId());
            flushIfNecessary(session, existingTransaction);
        } catch (ConsysException ex) {
            logger.debug("Throwed exception, should roll back", ex);
        } catch (HibernateException ex) {
            if (defaultJdbcExceptionTranslator != null && ex instanceof JDBCException) {
                throw convertJdbcAccessException((JDBCException) ex, defaultJdbcExceptionTranslator);
            } else if (GenericJDBCException.class.equals(ex.getClass())) {
                throw convertJdbcAccessException((GenericJDBCException) ex, defaultJdbcExceptionTranslator);
            }
            throw SessionFactoryUtils.convertHibernateAccessException(ex);
        } finally {
            if (existingTransaction) {
                logger.debug("Not closing pre-bound Hibernate Session after HibernateInterceptor");
                disableFilters(session);
                if (previousFlushMode != null) {
                    session.setFlushMode(previousFlushMode);
                }
            } else {
                SessionFactoryUtils.releaseSession(session, sf);
                if (sessionHolder == null || sessionHolder.doesNotHoldNonDefaultSession()) {
                    TransactionSynchronizationManager.unbindResource(sf);
                }
            }
        }
        return t;
    }

    @Override
    public DataAccessException convertHibernateAccessException(HibernateException ex) {
        throw new IllegalArgumentException("This method is forbidden!");
    }

    @Override
    public SessionFactory getSessionFactory() {
        throw new IllegalArgumentException("This method is forbidden!");
    }

    @Override
    public void afterPropertiesSet() {
    }
}
