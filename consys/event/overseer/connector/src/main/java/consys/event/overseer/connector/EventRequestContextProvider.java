package consys.event.overseer.connector;

/**
 * RequestContext provider 
 * @author palo
 */
public interface EventRequestContextProvider {

    public EventRequestContext getContext();

    public void initContext(String user, String event);
}
