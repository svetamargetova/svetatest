/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.overseer.cache.ehcache;

import consys.common.core.cache.EhCacheFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import consys.event.overseer.connector.EventRequestContextProvider;
import java.util.List;
import java.util.Map;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.bootstrap.BootstrapCacheLoader;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Searchable;
import net.sf.ehcache.search.Result;
import net.sf.ehcache.search.Results;
import net.sf.ehcache.search.expression.AlwaysMatch;
import net.sf.ehcache.search.expression.Criteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public abstract class EhCacheService<K, V> {

    protected Logger logger = LoggerFactory.getLogger(getClass());
    private final String baseCacheName;
    
    @Autowired
    private EhCacheFactory cacheFactory;
    
    @Autowired
    private EventRequestContextProvider contextProvider;

    public EhCacheService(String cacheName) {
        this.baseCacheName = cacheName;
    }

    public void put(final K key, final V value) {
        getCache().put(new Element(key, value));
    }

    public V get(final K key) {
        Element element = getCache().get(key);

        if (element != null) {
            return (V) element.getValue();
        }
        return null;
    }

    public ImmutableMap<K, V> map(List<K> keys) {
        ImmutableMap.Builder<K, V> builder = ImmutableMap.builder();
        Map<K, Element> items = (Map<K, Element>) getCache().getAll(keys);
        for (Map.Entry<K, Element> entry : items.entrySet()) {
            K k = entry.getKey();
            Element element = entry.getValue();
            builder.put(k, (V) element.getValue());
        }
        return builder.build();
    }

    public List<V> listAll() {
        return listByCriteria(new AlwaysMatch());
    }

    public List<V> listByCriteria(Criteria c) {
        ImmutableList.Builder<V> builder = ImmutableList.builder();
        Results rs = getCache().createQuery().addCriteria(c).includeValues().execute();
        for (Result r : rs.all()) {
            builder.add((V) r.getValue());
        }
        return builder.build();
    }

    public List<V> listAll(List<K> keys) {
        ImmutableList.Builder<V> builder = ImmutableList.builder();
        Map<K, Element> items = (Map<K, Element>) getCache().getAll(keys);
        for (Map.Entry<K, Element> entry : items.entrySet()) {
            Element element = entry.getValue();
            builder.add((V) element.getValue());
        }
        return builder.build();
    }

    public V remove(final K key) {
        Element element = getCache().get(key);
        if (element != null) {
            getCache().removeElement(element);
            return (V) element.getValue();
        }
        return null;
    }

    protected CacheConfiguration getConfiguration(String name) {
        CacheConfiguration cacheConfig = new CacheConfiguration(name, 500).overflowToDisk(false).eternal(true).searchable(new Searchable()).timeToLiveSeconds(60 * 10).timeToIdleSeconds(60 * 10);
        cacheConfig.getSearchable().keys();
        cacheConfig.getSearchable().values();
        return cacheConfig;
    }

    private Ehcache getCache() {
        // test na to ci existuje
        final String finalCacheName = String.format("%s-%s", contextProvider.getContext().getEventUuid().getId(), baseCacheName);


        if (cacheFactory.exists(finalCacheName)) {
            return cacheFactory.getCache(finalCacheName);
        } else {

            // synchronizacia len pre pripady ze su dve volania zaroven
            // a vytvori sa uz raz
            synchronized (this) {

                if (cacheFactory.exists(finalCacheName)) {
                    return cacheFactory.getCache(finalCacheName);
                }

                Ehcache cache = cacheFactory.create(finalCacheName, getConfiguration(finalCacheName));
                cache = decorate(cache);
                cache.setBootstrapCacheLoader(new BootstrapCacheLoader() {

                    @Override
                    public void load(Ehcache cache) throws CacheException {
                        initializeCache(cache);
                    }

                    @Override
                    public boolean isAsynchronous() {
                        return false;
                    }

                    @Override
                    public Object clone() throws CloneNotSupportedException {
                        return super.clone();
                    }
                });
                cacheFactory.initializeCache(cache);
                return cache;
            }
        }
    }

    /**
     * Metoda je volana pri prvom pouziti cache. Teda v momente kedy su potrebne
     * data. Implementacia nemusi nic robit. V pripade ze sa bude nieco
     * inicializovat je potrebne pouzit vlozenu instanciu {@link Ehcache}.
     *
     * @param cache
     */
    protected void initializeCache(Ehcache cache) {
    }

    protected Ehcache decorate(Ehcache cache) {
        return cache;
    }
}
