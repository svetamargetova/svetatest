package consys.event.overseer.connector;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import consys.event.overseer.connector.dao.OverseerDaoSupport;
import consys.event.overseer.connector.monitor.EventActivityMonitor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.collections.list.SynchronizedList;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;

/**
 *
 * @author Palo
 */
public class OverseerSessionFactoryPool implements Serializable, InitializingBean, DisposableBean, ApplicationContextAware {

    private static final long serialVersionUID = 2915401644659950377L;
    private static final Logger logger = LoggerFactory.getLogger(OverseerSessionFactoryPool.class);
    private SessionFactoryBuilder builder;
    private final Map<EventId, LocalSessionFactoryBean> sessionFactories;
    private List<Delegate> delegates;
    private List<OverseerDaoSupport> destroyListeners;
    private Map<EventId, OverseerEvent> managedEventsConnector;
    private final Map<EventId, Object> lockMap;
    private EventActivityMonitor monitor;

    public OverseerSessionFactoryPool() {
        sessionFactories = Maps.newConcurrentMap();
        managedEventsConnector = Maps.newConcurrentMap();
        lockMap = Maps.newConcurrentMap();
        delegates = SynchronizedList.decorate(new ArrayList());
        destroyListeners = SynchronizedList.decorate(new ArrayList());
    }

    public void addDelegate(Delegate delegate) {
        delegates.add(delegate);
    }

    public SessionFactory getSessionFactory(EventId event) {
        LocalSessionFactoryBean bean;
        synchronized (sessionFactories) {
            bean = sessionFactories.get(event);
        }

        if (bean == null) {
            final Object sLock;
            synchronized (lockMap) {
                Object lock = lockMap.get(event);
                if (lock == null) {
                    lockMap.put(event, new Object());
                    sLock = lockMap.get(event);
                } else {
                    sLock = lock;
                }
            }
            synchronized (sLock) {
                bean = createSessionForEventId(event);
            }
            // TODO: promazavani mapy zamku
        }
        if (monitor != null) {
            monitor.eventActivity(event.getId());
        }
        return (SessionFactory) bean.getObject();
    }

    private LocalSessionFactoryBean createSessionForEventId(EventId eventId) {
        LocalSessionFactoryBean bean = sessionFactories.get(eventId);
        if (bean == null) {
            logger.info("---> event " + eventId + " to SessionFactoryPool");
            // jeste nebyla vytvorena session pro dany event, zkusime ji vytvorit
            OverseerEvent oe = managedEventsConnector.get(eventId);
            if (oe == null) {
                throwNullPointerForEventId(eventId);
            }

            // nechame vytvorit session
            try {
                addNewSessionFactory(oe);
            } catch (InstantiationException ex) {
                throwNullPointerForEventId(eventId);
            }

            // zkusime znova nacist po vytvoreni
            bean = sessionFactories.get(eventId);
            if (bean == null) {
                throwNullPointerForEventId(eventId);
            }
        }
        return bean;
    }

    /** vyhodi nullpointer pro chybu pri vytvareni session pro db akce */
    private void throwNullPointerForEventId(EventId eventId) {
        throw new NullPointerException("There is no such SessionFactory for Event " + eventId);
    }

    /** prida connector jiz existujiciho eventu pri startu overseera, aby se mohla vytvorit session az na vyzadani */
    public void addManagedEventConnector(OverseerEvent connector) {
        managedEventsConnector.put(connector.getEventUuid(), connector);
    }

    public void destroySessionFactory(EventId eventId) {
        synchronized (sessionFactories) {
            LocalSessionFactoryBean bean = sessionFactories.remove(eventId);
            if (bean == null) {
                return;
            }
            logger.info("Destroing session factory for: {}", eventId.getId());
            bean.destroy();
            OverseerEvent e = new OverseerEvent(null, null, null, eventId.getId());
            builder.destroySessionFactory(e);
            notifyListenersOnDestroySessionFactory(eventId);
        }
    }

    public void addNewSessionFactory(OverseerEvent event) throws InstantiationException {
        synchronized (sessionFactories) {
            LocalSessionFactoryBean bean = builder.buildNewSessionFactory(event);
            sessionFactories.put(event.getEventUuid(), bean);
            // delegujeme vytvorenie
            logger.info("Delegating that new session factory exists");
            for (Delegate d : delegates) {
                d.onSessionFactoryCreated((SessionFactory) bean.getObject());
            }
        }
    }

    /**
     * Vrati <strong>vsetky</strong> session fatories ktore overseer spravuje.
     * <p/>
     * @return
     */
    public ImmutableMap<EventId, SessionFactory> getOverseerManagedSessionFactories() {
        ImmutableMap.Builder<EventId, SessionFactory> b = ImmutableMap.builder();
        Set<Entry<EventId, LocalSessionFactoryBean>> set = sessionFactories.entrySet();

        for (Entry<EventId, LocalSessionFactoryBean> entry : set) {
            b.put(entry.getKey(), entry.getValue().getObject());
        }
        return b.build();
    }

    /**
     * Vrati <strong>vsetky</strong> event ID's ktore overseer spravuje.
     * <p/>
     * @return
     */
    public ImmutableSet<EventId> getOverseerManagedEvents() {
        return ImmutableSet.<EventId>builder().addAll(sessionFactories.keySet()).build();
    }

    /**
     * @return the builder
     */
    public SessionFactoryBuilder getBuilder() {
        return builder;
    }

    /**
     * @param builder the builder to set
     */
    public void setBuilder(SessionFactoryBuilder builder) {
        this.builder = builder;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (builder == null) {
            throw new NullPointerException("Session Builder is missing!");
        }
    }

    @Override
    public void destroy() throws Exception {
        for (LocalSessionFactoryBean s : sessionFactories.values()) {
            logger.info("Destroyng session factory");
            s.destroy();
        }
    }

    /** prida listenera na ruseni session factory */
    public void addListenerOnDestroySessionFactory(OverseerDaoSupport ods) {
        destroyListeners.add(ods);
    }

    /** notifikuje vsechny zaregistrovane listenery, ze byla zrusena session factory pro zadany event */
    private void notifyListenersOnDestroySessionFactory(final EventId eventId) {
        for (OverseerDaoSupport ods : destroyListeners) {
            ods.onDestroyEventSessionFactory(eventId);
        }
    }

    /** pro vlozeni monitoru aktivity akci */
    public void setMonitor(EventActivityMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    }

    public interface Delegate {

        public void onSessionFactoryCreated(SessionFactory sessionFactory);
    }
}
