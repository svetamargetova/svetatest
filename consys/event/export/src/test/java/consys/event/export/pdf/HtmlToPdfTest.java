/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.export.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.*;

/**
 *
 * @author palo
 */
public class HtmlToPdfTest {

    public static void main(String[] args) {

        try {
            Document document = new Document(PageSize.A4);
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("/Users/palo/htmltopdf.pdf"));
            
            document.open();
            document.addAuthor("Author of the Doc");
            document.addCreationDate();
            document.addTitle("This is the title");
//SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
//SAXmyHtmlHandler shh = new SAXmyHtmlHandler(document);
            HTMLWorker htmlWorker = new HTMLWorker(document);
            String str = "<html><head><title>titlu</title></head><body><table><tr><td><p style=’font-size: 40pt; font-family: Times’>"
                    + "Cher Monsieur,</p><br><p align=’justify’ style=’text-indent: 2em; font-size: 10pt; font-family: Times; color:purple;’>"
                    + "asdasdasdsadas<br></p><p align=’justify’ style=’text-indent: 2em; font-size: 10pt; font-family: Times’>"
                    + "En vous remerciant &agrave; nouveau de la confiance que vous nous t&eacute;moignez,</p>"
                    + "<br><p style=’font-size: 10pt; font-family: Times’>Bien Cordialement,<br>"
                    + "<br>ADMINISTRATEUR ADMINISTRATEUR<br>Ligne directe : 04 42 91 52 10<br>Acadomia&reg; – "
                    + "37 BD Aristide Briand  – 13100 Aix en Provence  </p></td></tr></table></body></html>";
            htmlWorker.parse(new StringReader(str));
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
