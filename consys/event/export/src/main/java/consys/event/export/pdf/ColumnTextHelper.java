package consys.event.export.pdf;

import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.ColumnText;
import java.util.ArrayList;
import java.util.List;

/**
 * Pomocny objekt pro strankovani columnTextu
 * @author pepa
 */
public class ColumnTextHelper {

    // konstanty
    private static final float DEFAULT_INCREMENT = 5;
    private static final float DEFAULT_PAGE_START = 20;
    private static final float DEFAULT_PAGE_END = 270;
    // data
    private ColumnText ct;
    private List<Element> elements;
    private NewPage page;
    private float increment;
    private float pageStart;
    private float pageEnd;
    private float xPositionMM;
    private float yPositionMM;
    private float ctWidth;
    private float ctHeight;
    private List<Float> ctHeights;

    public ColumnTextHelper(ColumnText ct, List<Element> elements, NewPage page) {
        this.ct = ct;
        this.elements = elements;
        this.page = page;
        increment = DEFAULT_INCREMENT;
        pageStart = DEFAULT_PAGE_START;
        pageEnd = DEFAULT_PAGE_END;

        ctHeights = new ArrayList<Float>();
    }

    public void setColumnTextPostion(float xPositionMM, float yPositionMM) {
        this.xPositionMM = xPositionMM;
        this.yPositionMM = yPositionMM;
    }

    public void setColumnTextSize(float ctWidth, float ctHeight) {
        this.ctWidth = ctWidth;
        this.ctHeight = ctHeight;
    }

    public void setIncrement(float increment) {
        this.increment = increment;
    }

    public void setPageContentPosition(float pageStart, float pageEnd) {
        this.pageStart = pageStart;
        this.pageEnd = pageEnd;
    }

    public void run(float yLine) throws Exception {
        if (ct.go(true) == ColumnText.NO_MORE_TEXT) {
            PdfA4Utils.regenColumnText(ct, yLine, elements);
            ct.go();
        } else {
            simulate(yLine);
            // zregenerujeme obsah pro vykresleni
            PdfA4Utils.regenColumnText(ct, yLine, elements);
            // TODO: nasimulovat vykresleni s rozdelenim na stranky a zkontrolovat jestli na posledni nebude potreba
            // jeste pridat vysku pripadne i odstrankovat
            // vykreslime s odstrankovanima
            float yLineMM = yPositionMM;
            for (int i = 0; i < ctHeights.size(); i++) {
                Float f = ctHeights.get(i);
                PdfA4Utils.resizeColumnText(ct, xPositionMM, yLineMM, ctWidth, f);
                ct.go();
                if (i < ctHeights.size() - 1) {
                    page.newPage();
                    yLineMM = pageStart;
                }
            }
        }
    }

    /** nasimulujeme a zjistime vyslednou vysku a rozpocitame na stranky */
    private void simulate(float yLine) throws Exception {
        ctHeights.clear();
        float y = yPositionMM;
        int counter = 0;

        float height;
        do {
            counter++;
            height = (increment * counter) + ctHeight;

            PdfA4Utils.resizeColumnText(ct, xPositionMM, y, ctWidth, height);
            PdfA4Utils.regenColumnText(ct, yLine, elements);
        } while (ct.go(true) != ColumnText.NO_MORE_TEXT);

        if (yPositionMM + height > pageEnd) {
            // do konce prvni stranky
            float diff = pageEnd - yPositionMM;
            ctHeights.add(diff);

            final float pageHeight = pageEnd - pageStart;
            boolean end = false;
            do {
                float h = height - diff;
                if (h <= pageHeight) {
                    // zbytek se vleze cely do stranky
                    ctHeights.add(h < 0 ? height : h);
                    end = true;
                } else {
                    // je vetsi nez misto na strance, takze zabere minimalne pristi a kousek dalsi stranky
                    ctHeights.add(pageHeight);
                    height -= pageHeight;
                }
                diff = pageHeight;
            } while (!end);
        } else {
            // vleze se do konce stranky
            ctHeights.add(height);
        }
    }
}
