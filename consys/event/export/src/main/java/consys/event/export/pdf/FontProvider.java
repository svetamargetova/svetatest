package consys.event.export.pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import java.io.IOException;
import org.apache.commons.lang.SystemUtils;

/**
 * Poskytuje fonty
 * @author pepa
 */
public class FontProvider {

    public enum FontStyle {

        ARIAL_8_REGULAR,
        ARIAL_8_ITALICS,
        ARIAL_9_BOLD,
        ARIAL_9_REGULAR,
        ARIAL_10_REGULAR,
        ARIAL_10_BOLD,
        ARIAL_11_BOLD,
        ARIAL_11_BOLD_ITALICS,
        ARIAL_11_ITALICS,
        ARIAL_11_REGULAR,
        ARIAL_12_ITALICS,
        ARIAL_12_REGULAR,
        ARIAL_13_BOLD,
        ARIAL_13_BOLD_ITALICS,
        ARIAL_14_BOLD,
        ARIAL_15_BOLD,
        ARIAL_17_BOLD,
        ARIAL_20_BOLD;
        private static final long serialVersionUID = 4161985003421531008L;
    }

    /** nastavi font */
    public static void changeFont(FontStyle font, PdfContentByte cb) throws DocumentException, IOException {
        String fontString = getFontLocation("arial.ttf");

        switch (font) {
            case ARIAL_8_REGULAR:
                fontString = getFontLocation("arial.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 8);
                return;
            case ARIAL_8_ITALICS:
                fontString = getFontLocation("ariali.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 8);
                return;
            case ARIAL_9_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 9);
                return;
            case ARIAL_9_REGULAR:
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 9);
                return;
            case ARIAL_10_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 10);
                return;
            case ARIAL_10_REGULAR:
                fontString = getFontLocation("arial.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 10);
                return;
            case ARIAL_11_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
                return;
            case ARIAL_11_BOLD_ITALICS:
                fontString = getFontLocation("arialbi.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
                return;
            case ARIAL_11_ITALICS:
                fontString = getFontLocation("ariali.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
                return;
            case ARIAL_11_REGULAR:
                fontString = getFontLocation("arial.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
                return;
            case ARIAL_12_ITALICS:
                fontString = getFontLocation("ariali.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 12);
                return;
            case ARIAL_12_REGULAR:
                fontString = getFontLocation("arial.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 12);
                return;
            case ARIAL_13_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 13);
                return;
            case ARIAL_13_BOLD_ITALICS:
                fontString = getFontLocation("arialbi.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 13);
                return;
            case ARIAL_14_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 14);
                return;
            case ARIAL_15_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 15);
                return;
            case ARIAL_17_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 20);
                return;
            case ARIAL_20_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                cb.setFontAndSize(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 17);
                return;
        }

        throw new IllegalArgumentException();
    }

    /** vraci font */
    public static Font font(FontStyle font) throws DocumentException, IOException {
        String fontString = getFontLocation("arial.ttf");

        switch (font) {
            case ARIAL_8_REGULAR:
                fontString = getFontLocation("arial.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 8);
            case ARIAL_8_ITALICS:
                fontString = getFontLocation("ariali.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 8);
            case ARIAL_9_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 9);
            case ARIAL_9_REGULAR:
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 9);
            case ARIAL_10_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 10);
            case ARIAL_10_REGULAR:
                fontString = getFontLocation("arial.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 10);
            case ARIAL_11_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
            case ARIAL_11_BOLD_ITALICS:
                fontString = getFontLocation("arialbi.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
            case ARIAL_11_ITALICS:
                fontString = getFontLocation("ariali.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
            case ARIAL_11_REGULAR:
                fontString = getFontLocation("arial.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 11);
            case ARIAL_12_ITALICS:
                fontString = getFontLocation("ariali.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 12);
            case ARIAL_12_REGULAR:
                fontString = getFontLocation("arial.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 12);
            case ARIAL_13_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 13);
            case ARIAL_13_BOLD_ITALICS:
                fontString = getFontLocation("arialbi.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 13);
            case ARIAL_14_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 14);
            case ARIAL_15_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 15);
            case ARIAL_17_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 17);
            case ARIAL_20_BOLD:
                fontString = getFontLocation("arialbd.ttf");
                return new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 20);
        }

        throw new IllegalArgumentException();
    }

    /** cesta k fontu */
    public static String getFontLocation(String fontName) {
        String fontDir = "";
        if (SystemUtils.IS_OS_LINUX) {
            // adresar s ms core fonty Arial, Verdana, Trebuchet MS, Times New Roman, Andale Mono, Comic Sans MS, Courier New, Webdings
            fontDir = "/usr/share/fonts/truetype/msttcorefonts/";
        } else if (SystemUtils.IS_OS_WINDOWS) {
            fontDir = "c:\\WINDOWS\\Fonts\\";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            fontDir = "/Library/Fonts/";
        }
        return fontDir + fontName;
    }
}
