package consys.event.export.common;

import consys.common.utils.enums.Currency;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 *
 * @author pepa
 */
public class Formats {

    /** priradi znacku meny k hodnote */
    public static String assignCurrency(Currency currency, String value) {
        switch (currency) {
            case CZK:
                return value + " Kč";
            case EUR:
                return "€ " + value;
            case USD:
                return "$ " + value;
            default:
                return currency + " " + value;
        }
    }

    /** vraci DecimalFormat podle meny */
    public static DecimalFormat decimalFormat(Currency currency) {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        switch (currency) {
            case CZK:
                symbols.setDecimalSeparator(',');
                return new DecimalFormat("########0.00", symbols);
            case EUR:
            case USD:
                symbols.setDecimalSeparator('.');
                return new DecimalFormat("########0.00", symbols);
            default:
                throw new IllegalArgumentException("Unknown currency");
        }
    }
}
