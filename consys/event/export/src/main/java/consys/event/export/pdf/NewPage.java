package consys.event.export.pdf;

/**
 * Interface pro predavani moznosti strankovat dokument do utils
 * @author pepa
 */
public interface NewPage {

    /** podminene odstrankovani, vraci pozici od horni hrany v mm */
    float newPage(float yLineMM) throws Exception;

    /** odstrankuje na novou stranku */
    float newPage() throws Exception;
}
