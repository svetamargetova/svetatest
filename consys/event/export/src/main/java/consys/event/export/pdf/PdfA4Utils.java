package consys.event.export.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Hubr
 */
public final class PdfA4Utils {

    // konstanty
    public static final float PAGE_WIDTH = PageSize.A4.getWidth();
    public static final float PAGE_HEIGHT = PageSize.A4.getHeight();
    public static final float PAGE_WIDTH_CONST = PAGE_WIDTH / 210f;
    public static final float PAGE_HEIGHT_CONST = PAGE_HEIGHT / 297f;

    private PdfA4Utils() {
    }

    public static float xps(float mm) {
        return mm * PAGE_WIDTH_CONST;
    }

    public static float yps(float mm) {
        return mm * PAGE_HEIGHT_CONST;
    }

    /** pozice a rozmery v mm */
    public static Rectangle rectangle(float x, float y, float width, float height) {
        PositionSize ps = new PositionSize(x, y, width, height);
        return new Rectangle(ps.getLlx(), ps.getLly(), ps.getUrx(), ps.getUry());
    }

    /** pozice a rozmery v mm */
    public static ColumnText columnText(PdfWriter writer, float x, float y, float width, float height) {
        ColumnText ct = new ColumnText(writer.getDirectContent());
        resizeColumnText(ct, x, y, width, height);
        return ct;
    }

    /** pozice a rozmery v mm */
    public static void resizeColumnText(ColumnText ct, float x, float y, float width, float height) {
        PositionSize ps = new PositionSize(x, y, width, height);
        ct.setSimpleColumn(ps.getLlx(), ps.getLly(), ps.getUrx(), ps.getUry());
    }

    /**
     * regenerace ColumnText po go(true)
     * @param ct ColumnText k regeneraci
     * @param yLine vychozi yLine
     * @param elements elementy, ktere prijdou do ct v poradi jake chceme
     */
    public static void regenColumnText(ColumnText ct, float yLine, Element... elements) {
        ct.setText(null);
        ct.setYLine(yLine);
        if (elements != null) {
            for (Element e : elements) {
                ct.addElement(e);
            }
        }
    }

    /**
     * regenerace ColumnText po go(true)
     * @param ct ColumnText k regeneraci
     * @param yLine vychozi yLine
     * @param elements elementy, ktere prijdou do ct v poradi jake chceme
     */
    public static void regenColumnText(ColumnText ct, float yLine, List<Element> elements) {
        ct.setText(null);
        ct.setYLine(yLine);
        if (elements != null) {
            for (Element e : elements) {
                ct.addElement(e);
            }
        }
    }

    /** pozice a rozmery v mm */
    public static void drawImage(PdfWriter writer, java.awt.Image image, float x, float y, float width, float height) throws BadElementException, IOException, DocumentException {
        Image logo = Image.getInstance(image, null);
        final float ulx = xps(x);
        final float uly = PAGE_HEIGHT - yps(y + height);
        final float h = yps(height);
        final float ratio = h / logo.getHeight();
        final float w = logo.getWidth() * ratio;
        writer.getDirectContent().addImage(logo, w, 0, 0, h, ulx, uly);
    }

    public static float yPositionInMM(float yLine) {
        return (PAGE_HEIGHT - yLine) / PAGE_HEIGHT_CONST;
    }

    /** 
     * roztahne ColumnText podle delky obsahu (strankuje cely columnText na novou stranku), 
     * pokud se proiteruje vickrat nez je prah,
     * spusti vykresleni a vraci false
     */
    public static boolean fitContentToColumnText(ColumnText ct, List<Element> elements, float increment, float yLine,
            final float xPosition, final float yPosition, final float columnTextWidth, int threshold, NewPage newPage) throws Exception {
        boolean result = true;
        float y = yPosition;
        if (ct.go(true) == ColumnText.NO_MORE_TEXT) {
            regenColumnText(ct, yLine, elements);
            ct.go();
        } else {
            int counter = 2;
            do {
                float height = increment * counter;
                if (y + height > 280) {
                    y = newPage.newPage(y + height);
                    yLine = PAGE_HEIGHT - yps(y);
                }

                resizeColumnText(ct, xPosition, y, columnTextWidth, height);
                regenColumnText(ct, yLine, elements);

                counter++;
                if (counter > threshold) {
                    result = false;
                    break;
                }
            } while (ct.go(true) != ColumnText.NO_MORE_TEXT);
            regenColumnText(ct, yLine, elements);
            ct.go();
        }
        return result;
    }
}
