package consys.event.export.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactoryImp;
import com.itextpdf.text.pdf.BaseFont;

/**
 * FontProvider pro HTMLWorker (aby spravne fungovalo kodovani)
 * @author pepa
 */
public class DefaultFontProvider extends FontFactoryImp {

    private final Font font;

    public DefaultFontProvider() throws Exception {
        this(null);
    }

    public DefaultFontProvider(Font font) throws Exception {
        if (font == null) {
            String fontString = FontProvider.getFontLocation("arial.ttf");
            this.font = new Font(BaseFont.createFont(fontString, BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 10);
        } else {
            this.font = font;
        }
    }

    @Override
    public Font getFont(String fontName, String encoding, boolean embedded, float size, int style, BaseColor color, boolean cached) {
        if (fontName == null || size == 0) {
            return font;
        }

        return super.getFont(fontName, encoding, embedded, size, style, color, cached);
    }
}
