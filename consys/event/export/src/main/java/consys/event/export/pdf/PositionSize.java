package consys.event.export.pdf;

/**
 *
 * @author Hubr
 */
public class PositionSize {

    // data
    private final float llx;
    private final float lly;
    private final float urx;
    private final float ury;

    public PositionSize(float x, float y, float width, float height) {
        llx = PdfA4Utils.xps(x);
        lly = PdfA4Utils.PAGE_HEIGHT - PdfA4Utils.yps(y + height);
        urx = PdfA4Utils.xps(x + width);
        ury = PdfA4Utils.PAGE_HEIGHT - PdfA4Utils.yps(y);
    }

    public float getLlx() {
        return llx;
    }

    public float getLly() {
        return lly;
    }

    public float getUrx() {
        return urx;
    }

    public float getUry() {
        return ury;
    }
}
