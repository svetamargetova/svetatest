package consys.event.profile.core.service.impl;

import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.service.impl.AbstractService;
import consys.common.gwt.client.utils.EventUtils;
import consys.common.utils.UuidProvider;
import consys.common.utils.enums.Currency;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.context.PropertiesChangedEvent;
import consys.event.common.core.service.impl.SystemPropertyServiceImpl;
import consys.event.conference.core.bo.thumb.SubmissionTypeThumb;
import consys.event.conference.core.service.SubmissionService;
import consys.event.profile.core.bo.*;
import consys.event.profile.core.locale.LocaleFileName;
import consys.event.profile.core.locale.ProfileLocale;
import consys.event.profile.core.service.EventProfileService;
import consys.event.profile.core.template.EventProfileTemplate;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;

/**
 *
 * @author pepa
 */
public class EventProfileServiceImpl extends AbstractService implements EventProfileService, ApplicationListener<PropertiesChangedEvent> {

    // servicy
    @Autowired
    private RegistrationBundleService registrationBundleService;
    @Autowired
    private SystemPropertyService systemPropertyService;
    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private AwsFileStorageService fileStorageService;
    // data
    private String administrationURL;
    private String awsS3BucketName;

    @Override
    public EventInformation generateEventProfileWebPage(Writer writer, LocaleFileName locale)
            throws ServiceExecutionFailed, NoRecordException {
        try {
            EventInformation ei = getActualEventInformation();
            EventProfileTemplate ept = new EventProfileTemplate(ei, administrationURL);
            ept.generateEventProfile(writer, locale);
            return ei;
        } catch (Exception ex) {
            log().error("Generate event profile page failed!", ex);
            throw new ServiceExecutionFailed("Generate event profile page failed!");
        }
    }

    @Override
    public void generateEventProfileWebPage() {
        EventInformation ei = getActualEventInformation();
        for (LocaleFileName locale : LocaleFileName.values()) {
            ByteArrayInputStream bais = null;
            try {
                log().info("Generating event web page for: {}", ei.getEventName());
                ei.setLocale(locale.name().toLowerCase());
                EventProfileTemplate ept = new EventProfileTemplate(ei, administrationURL);
                String page = ept.generateEventProfile(locale);
                bais = new ByteArrayInputStream(page.getBytes("UTF-8"));
                String fileName = EventUtils.profileFileName(ei.getEventAcronym(), ei.getEventYear(), ei.getEventSeries(), locale.name().toLowerCase());
                fileStorageService.createObject(awsS3BucketName, fileName, fileName, AwsFileStorageService.FileType.HTML, false, bais);
            } catch (UnsupportedEncodingException ex) {
                log().error("Generate web page " + locale + " failed!", ex);
            } catch (Exception ex) {
                log().error("Generate web page " + locale + " failed!", ex);
            } finally {
                try {
                    if (bais != null) {
                        bais.close();
                    }
                } catch (IOException ex) {
                    // BAIS nema imlementovanu close(), je to tu len pro forma
                }
            }
        }
    }

    @Override
    public void onApplicationEvent(PropertiesChangedEvent event) {
        generateEventProfileWebPage();
    }

    private EventInformation getActualEventInformation() {
        EventInformation ei = new EventInformation();
        loadAndSetDataFromProperty(ei);
        loadAndSetRegistrations(ei);
        loadAndSetContribution(ei);
        loadAndSetJsonApiPlaceholders(ei);
        return ei;
    }

    private void loadAndSetJsonApiPlaceholders(EventInformation ei) {
        ei.setAdministrationUrl(administrationURL);
        // Zatim na pevno lebo mame len jedneho overseera
        ei.setOverseerJsonAPIUrl(String.format("%s/overseer/json/api", administrationURL));
    }

    private void loadAndSetDataFromProperty(EventInformation ei) {
        List<SystemProperty> properties = systemPropertyService.listAllProperties();

        Contact contact = new Contact();
        Date eventFrom = null;
        Date eventTo = null;

        for (SystemProperty sp : properties) {
            if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_NAME) == 0) {
                ei.setEventName(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_ACRONYM) == 0) {
                ei.setEventAcronym(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_YEAR) == 0) {
                ei.setEventYear(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_DESCRIPTION) == 0) {
                ei.setEventDescription(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_PROFILE_IMAGE_UUID) == 0) {
                if (StringUtils.isNotBlank(sp.getValue())) {
                    ei.setEventProfileLogoUrl(imageUrl(sp.getValue()));
                }
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_PARTNER_FOOTER_IMAGE) == 0) {
                ei.setPartnersImageUuid(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_UUID) == 0) {
                ei.setEventUuid(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_ADDRESS) == 0) {
                contact.setAddress(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_ADDRESS_PLACE) == 0) {
                contact.setPlace(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_WEB_EMAIL) == 0) {
                contact.setEmail(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_WEB_PHONE) == 0) {
                contact.setPhoneNumber(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_WEB) == 0) {
                contact.setWeb(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_CURRENCY) == 0) {
                Registration r = new Registration();
                r.setCurrency(Currency.valueOf(sp.getValue()));
                ei.setRegistration(r);
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_FROM) == 0) {
                if (StringUtils.isNotBlank(sp.getValue())) {
                    eventFrom = getDate(sp.getValue());
                }
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_TO) == 0) {
                if (StringUtils.isNotBlank(sp.getValue())) {
                    eventTo = getDate(sp.getValue());
                }
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_SERIES) == 0) {
                ei.setEventSeries(StringUtils.isNotBlank(sp.getValue()) ? sp.getValue() : "1");
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_CORPORATION) == 0) {
                ei.setCorporatePrefix(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_TIMEZONE) == 0) {
                ei.setTimeZone(sp.getValue());
            }
        }

        if (eventFrom != null || eventTo != null) {
            ei.getImportantDates(LocaleFileName.EN).add(new DateRecord(eventFrom, eventTo, ei.getEventName(), true));
            ei.getImportantDates(LocaleFileName.CS).add(new DateRecord(eventFrom, eventTo, ei.getEventName(), true));
        }

        ei.setEventBegin(eventFrom);

        if (contact.wasContact()) {
            ei.setContactInformation(contact);
        }
    }

    private void loadAndSetRegistrations(EventInformation ei) {
        try {
            List<RegistrationOption> registrationList = registrationBundleService.listMainRegistrationBundlesOptions();

            Registration r = ei.getRegistration();

            for (RegistrationOption ro : registrationList) {
                if (ro.getToDate() == null) {
                    // kdyz uz je neaktivni
                    continue;
                }
                Ticket t = new Ticket();
                t.setName(ro.getTitle());
                t.setPrice(ro.getPrice());
                t.setUrl(registerUrl(ro, ei.getEventUuid(), ei.getCorporatePrefix()));
                r.getTickets().add(t);
            }

            Date beginDate = registrationBundleService.loadFirstRegistrationBegin();
            Date endDate = registrationBundleService.loadLastRegistrationEnd();

            if (beginDate != null || endDate != null) {
                ei.getImportantDates(LocaleFileName.EN).add(new DateRecord(beginDate, endDate,
                        ProfileLocale.value(LocaleFileName.EN, ProfileLocale.PARTICIPANTS_REGISTRATION)));
                ei.getImportantDates(LocaleFileName.CS).add(new DateRecord(beginDate, endDate,
                        ProfileLocale.value(LocaleFileName.CS, ProfileLocale.PARTICIPANTS_REGISTRATION)));
            }
        } catch (NoRecordException ex) {
            // nenacetly se zadne zaznamy, nechame nevyplneno
        }
    }

    private String registerUrl(RegistrationOption ro, String eventUuid, String corporationPrefix) {
        String url = modifyUrl(administrationURL, corporationPrefix);
        StringBuilder sb = new StringBuilder(url);
        sb.append("/register/register.html?cmd=Register&eid=");
        sb.append(eventUuid);
        sb.append("&api_key=");
        sb.append(UuidProvider.getUuid());
        sb.append("&pid=");
        sb.append(ro.getBundleUuid());
        return sb.toString();
    }

    /** pokud se jedna o korporatni akci, upravi odkazy na korporatni adresu */
    private String modifyUrl(String url, String corporationPrefix) {
        if (StringUtils.isNotBlank(corporationPrefix)) {
            final boolean isHttps = administrationURL.startsWith("https://app.");
            final boolean isHttp = administrationURL.startsWith("http://app.");
            if (isHttps || isHttp) {
                String end;
                String begin;
                if (isHttps) {
                    begin = "https://";
                    end = administrationURL.substring(12);
                } else {
                    begin = "http://";
                    end = administrationURL.substring(11);
                }
                StringBuilder sb = new StringBuilder(begin);
                sb.append(corporationPrefix);
                sb.append(".");
                sb.append(end);
                return sb.toString();
            } else {
                return url;
            }
        } else {
            return url;
        }
    }

    private String imageUrl(String imageUuid) {
        StringBuilder url = new StringBuilder(administrationURL);
        url.append("/takeplace/");
        url.append("user/profile/load?id=");
        url.append(imageUuid);
        url.append("-001");
        return url.toString();
    }

    private Date getDate(String value) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(SystemPropertyServiceImpl.ISO_DATE_FORMAT);
            return value == null ? null : sdf.parse(value);
        } catch (ParseException ex) {
            log().warn("Can't parse date!");
            return null;
        }
    }

    private void loadAndSetContribution(EventInformation ei) {
        try {
            List<SubmissionTypeThumb> list = submissionService.listSubmissionTypesThumbs(false);
            for (SubmissionTypeThumb thumb : list) {
                ei.getContributions(LocaleFileName.EN).add(new Contribution(thumb.getName(), thumb.getDescription()));
                ei.getContributions(LocaleFileName.CS).add(new Contribution(thumb.getName(), thumb.getDescription()));
            }
        } catch (NoRecordException ex) {
            // nenacetly se zadne zaznamy, nechame nevyplneno
        }

        Date beginDate = submissionService.loadSubmissionBegindSend();
        Date endDate = submissionService.loadSubmissionEndSend();

        if (beginDate != null || endDate != null) {
            ei.getImportantDates(LocaleFileName.EN).add(new DateRecord(beginDate, endDate,
                    ProfileLocale.value(LocaleFileName.EN, ProfileLocale.COLLECTING_CONTRIBUTIONS)));
            ei.getImportantDates(LocaleFileName.CS).add(new DateRecord(beginDate, endDate,
                    ProfileLocale.value(LocaleFileName.CS, ProfileLocale.COLLECTING_CONTRIBUTIONS)));
        }
    }

    public void setRegistrationBundleService(RegistrationBundleService registrationBundleService) {
        this.registrationBundleService = registrationBundleService;
    }

    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    public void setAdministrationURL(String administrationURL) {
        this.administrationURL = administrationURL;
    }

    public void setAwsS3BucketName(String awsS3BucketName) {
        this.awsS3BucketName = awsS3BucketName;
    }

    public void setSubmissionService(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }
}
