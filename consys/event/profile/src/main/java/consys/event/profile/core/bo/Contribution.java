package consys.event.profile.core.bo;

/**
 *
 * @author pepa
 */
public class Contribution {

    private String name;
    private String description;

    public Contribution(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
