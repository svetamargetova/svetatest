package consys.event.profile.core.bo;

/**
 *
 * @author pepa
 */
public class Contact {

    // data
    private String address;
    private String place;
    private String email;
    private String web;
    private String phoneNumber;
    // data - rizeni chovani
    private boolean wasContact = false;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        wasContact = true;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        if (this.email != null) {
            this.email = this.email.trim();
        }
        wasContact = true;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        wasContact = true;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean wasContact() {
        return wasContact;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
        if (this.web != null) {
            this.web = this.web.toLowerCase().trim();
            if (this.web.startsWith("http://")) {
                this.web = this.web.substring(7);
            }
        }
        wasContact = true;
    }
}
