package consys.event.profile.core.locale;

import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Hubr
 */
public class ProfileLocale {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(ProfileLocale.class);
    // instance
    private static ProfileLocale instance;
    // klice
    public static final String LOCALE_VALUE = "localeValue";
    public static final String COLLECTING_CONTRIBUTIONS = "collectingContributions";
    public static final String CONTACT_ADDRESS = "contactAddress";
    public static final String CONTACT_EMAIL = "contactEmail";
    public static final String CONTACT_INFORMATION = "contactInformation";
    public static final String CONTACT_PHONE = "contactPhone";
    public static final String CONTACT_WEB = "contactWeb";
    public static final String CONTRIBUTIONS = "contributions";
    public static final String DESCRIPTION_TITLE = "descriptionTitle";
    public static final String EVENT_BEGIN = "eventBegin";
    public static final String FREE = "free";
    public static final String GO_TO_TAKEPLACE = "goToTakeplace";
    public static final String IMPORTANT_DATES = "importantDates";
    public static final String PARTICIPANTS_REGISTRATION = "participantsRegistration";
    public static final String PARTNERS = "partners";
    public static final String REGISTER = "register";
    public static final String REGISTRATION = "registration";
    public static final String VIEW_LARGER_MAP = "viewLargerMap";
    // data
    private static Map<LocaleFileName, Map<String, String>> locales;

    private ProfileLocale() {
        locales = new EnumMap<LocaleFileName, Map<String, String>>(LocaleFileName.class);

        for (LocaleFileName f : LocaleFileName.values()) {
            Map<String, String> texts = new HashMap<String, String>();

            try {
                InputStream inputStream = getClass().getResourceAsStream(f.getFilename());

                // nacteni properties
                Properties properties = new Properties();
                properties.load(inputStream);

                for (Entry<Object, Object> entry : properties.entrySet()) {
                    texts.put(entry.getKey().toString(), entry.getValue().toString());
                }
            } catch (IOException ex) {
                logger.error("Fail while reading " + f.getFilename(), ex);
            }
            locales.put(f, texts);
        }
    }

    /** vraci hodnotu v zavislosti na lokalizaci a klic */
    public static String value(LocaleFileName locale, String key) {
        if (instance == null) {
            instance = new ProfileLocale();
        }
        return instance.getValue(locale, key);
    }

    private String getValue(LocaleFileName locale, String key) {
        Map<String, String> texts = locales.get(locale);
        if (texts == null) {
            return "";
        }

        String result = texts.get(key);
        return result == null ? "" : result;
    }
}
