package consys.event.profile.core.locale;

/**
 *
 * @author pepa
 */
public enum LocaleFileName {

    EN("ProfileLocale.properties"), CS("ProfileLocale_cs.properties");
    // data
    private String filename;

    private LocaleFileName(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }
}
