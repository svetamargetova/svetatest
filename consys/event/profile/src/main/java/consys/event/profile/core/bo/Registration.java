package consys.event.profile.core.bo;

import consys.common.utils.enums.Currency;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class Registration {

    private Currency currency;
    private List<Ticket> tickets;

    public Registration() {
        tickets = new ArrayList<Ticket>();
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }
}
