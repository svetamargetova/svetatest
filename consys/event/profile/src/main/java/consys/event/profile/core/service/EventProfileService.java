package consys.event.profile.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.profile.core.bo.EventInformation;
import consys.event.profile.core.locale.LocaleFileName;
import java.io.Writer;

/**
 *
 * @author palo
 */
public interface EventProfileService {

    public EventInformation generateEventProfileWebPage(Writer writer, LocaleFileName locale)
            throws ServiceExecutionFailed, NoRecordException;
    
    public void generateEventProfileWebPage();
}
