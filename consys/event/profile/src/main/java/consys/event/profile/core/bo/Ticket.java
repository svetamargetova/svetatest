package consys.event.profile.core.bo;

import java.math.BigDecimal;

/**
 *
 * @author pepa
 */
public class Ticket {

    // konstanty
    private static final String EMPTY_URL = "#";
    // data
    private BigDecimal price;
    private String url = EMPTY_URL;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        if (url == null) {
            this.url = EMPTY_URL;
        } else {
            this.url = url;
        }
    }
}
