package consys.event.profile.core.bo;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class DateRecord {

    // data
    private Date from;
    private Date to;
    private String text;
    private boolean eventDate;

    public DateRecord(Date from, Date to, String text) {
        this.from = from;
        this.to = to;
        this.text = text;
    }

    public DateRecord(Date from, Date to, String text, boolean eventDate) {
        this(from, to, text);
        this.eventDate = eventDate;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public boolean isEventDate() {
        return eventDate;
    }

    /** pokud se jedna o stejny den, vraci true */
    public boolean isInSameDay() {
        if (getFrom() == null || getTo() == null) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(getFrom());
        cal2.setTime(getTo());
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }
}
