package consys.event.profile.core.bo;

import consys.event.profile.core.locale.LocaleFileName;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;

/**
 * Objekt s daty pro vyskladani profilove stranky
 * @author pepa
 */
public class EventInformation {

    // data
    private String eventAcronym;
    private String eventName;
    private String eventDescription;
    private String eventProfileLogoUrl;
    private String eventUuid;
    private String eventYear;
    private String eventSeries;
    private EnumMap<LocaleFileName, List<DateRecord>> importantDates;
    private Registration registration;
    private EnumMap<LocaleFileName, List<Contribution>> contributions;
    private Contact contactInformation;
    private String partnersImageUuid;
    private String administrationUrl;
    private String overseerJsonAPIUrl;
    private String locale;
    private String corporatePrefix;
    private String timeZone;
    private Date eventBegin;

    public EventInformation() {
        importantDates = new EnumMap<LocaleFileName, List<DateRecord>>(LocaleFileName.class);
        contributions = new EnumMap<LocaleFileName, List<Contribution>>(LocaleFileName.class);
    }

    public Contact getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(Contact contactInformation) {
        this.contactInformation = contactInformation;
    }

    public List<Contribution> getContributions(LocaleFileName locale) {
        List<Contribution> list = contributions.get(locale);
        if (list == null) {
            list = new ArrayList<Contribution>();
            contributions.put(locale, list);
        }
        return list;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventProfileLogoUrl() {
        return eventProfileLogoUrl;
    }

    public void setEventProfileLogoUrl(String eventProfileLogoUrl) {
        this.eventProfileLogoUrl = eventProfileLogoUrl;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    public List<DateRecord> getImportantDates(LocaleFileName locale) {
        List<DateRecord> list = importantDates.get(locale);
        if (list == null) {
            list = new ArrayList<DateRecord>();
            importantDates.put(locale, list);
        }
        return list;
    }

    public String getPartnersImageUuid() {
        return partnersImageUuid;
    }

    public void setPartnersImageUuid(String partnersImageUuid) {
        this.partnersImageUuid = partnersImageUuid;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public String getEventAcronym() {
        return eventAcronym;
    }

    public void setEventAcronym(String eventAcronym) {
        this.eventAcronym = eventAcronym;
    }

    public String getEventYear() {
        return eventYear;
    }

    public void setEventYear(String eventYear) {
        this.eventYear = eventYear;
    }

    public String getEventSeries() {
        return eventSeries;
    }

    public void setEventSeries(String eventSeries) {
        this.eventSeries = eventSeries;
    }

    public String getAdministrationUrl() {
        return administrationUrl;
    }

    public void setAdministrationUrl(String administrationUrl) {
        this.administrationUrl = administrationUrl;
    }

    public String getOverseerJsonAPIUrl() {
        return overseerJsonAPIUrl;
    }

    public void setOverseerJsonAPIUrl(String overseerJsonAPIUrl) {
        this.overseerJsonAPIUrl = overseerJsonAPIUrl;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getCorporatePrefix() {
        return corporatePrefix;
    }

    public void setCorporatePrefix(String corporatePrefix) {
        this.corporatePrefix = corporatePrefix;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Date getEventBegin() {
        return eventBegin;
    }

    public void setEventBegin(Date eventBegin) {
        this.eventBegin = eventBegin;
    }
}
