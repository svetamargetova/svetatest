package consys.event.profile.core.template;

import consys.common.constants.img.ImageConstants;
import consys.common.gwt.client.utils.EventUtils;
import consys.common.utils.enums.Currency;
import consys.event.export.common.Formats;
import consys.event.profile.core.bo.*;
import consys.event.profile.core.locale.LocaleFileName;
import consys.event.profile.core.locale.ProfileLocale;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author pepa
 */
public class EventProfileTemplate {

    // konstanty - cesty
    private static final String TEMPLATE_PATH = "/html/event_profile_templateV4.html";
    private static final String NO_EVENT_PROFILE_IMAGE = "http://static.takeplace.eu/no-event-logo.png";
    // konstanty - text
    private static final String NA = "n/a";
    private static final String COLON_SPACE = ": ";
    // konstanty - tagy
    private static final String DIV_START = "<div>";
    private static final String DIV_END = "</div>";
    private static final String DIV_CLEAR = "<div class=\"clear\"></div>";
    private static final String DIV_REGISTRATION_SEPARATOR = "<div class=\"rSeparator\"></div>";
    // konstanty - placeholders
    private static final String CURRENT_LOCALE = "current_locale";
    private static final String GO_TO_TAKEPLACE = "go_to_takeplace";
    private static final String EVENT_NAME = "event_name";
    private static final String EVENT_DESCRIPTION_TITLE = "event_description_title";
    private static final String EVENT_DESCRIPTION = "event_description";
    private static final String EVENT_PROFILE_LOGO_URL = "event_profile_logo_url";
    private static final String PAGE_NAME = "page_name";
    private static final String PART_EVENT_BEGIN = "part_event_begin";
    private static final String PART_IMPORTANT_DATES = "part_important_dates";
    private static final String PART_REGISTRATION = "part_registration";
    private static final String PART_CONTRIBUTIONS = "part_contributions";
    private static final String PART_CONTACT = "part_contact_information";
    private static final String PART_PARTNERS = "part_partners";
    private static final String JSON_EVENT_UUID = "eventUuid";
    private static final String JSON_OVERSEER_URL = "overseerUrl";
    private static final String JSON_ADMIN_URL = "administrationUrl";
    private static final String JSON_LOCALE = "localization";
    private static final String JSON_EVENT_CURRENCY = "eventCurrency";
    // konstanty - ostatni
    private static final String TIME_FORMAT = "HH:mm";
    private static final String DATE_FORMAT_CZ = "dd.MM.yyyy";
    private static final String DATE_FORMAT_EN = "yyyy/MM/dd";
    private static final String DATE_TIME_FORMAT_CZ = "dd.MM. HH:mm";
    private static final String DATE_TIME_FORMAT_EN = "MM/dd HH:mm";
    // data
    private StringTemplate stringTemplate;
    private EventInformation ei;
    private SimpleDateFormat sdfYearDate;
    private SimpleDateFormat sdf;
    private SimpleDateFormat sdfTime;
    private SimpleDateFormat sdfe;
    private SimpleDateFormat sdfeTime;
    private String administrationUrl;

    public EventProfileTemplate(EventInformation ei, String administrationUrl) {
        this.ei = ei;
        this.administrationUrl = administrationUrl;
    }

    private void divStart(StringBuilder sb, String style) {
        sb.append("<div class=\"");
        sb.append(style);
        sb.append("\">");
    }

    public void generateEventProfile(Writer out, LocaleFileName locale) throws Exception {
        PrintWriter pw = new PrintWriter(out, true);
        pw.print(generateEventProfile(locale));
        pw.flush();
    }

    public String generateEventProfile(LocaleFileName locale) throws Exception {
        String template = getTemplate(this.getClass().getResourceAsStream(TEMPLATE_PATH));
        stringTemplate = new StringTemplate(template);
        generate(locale);
        return stringTemplate.toString();
    }

    public String getTemplate(InputStream is) throws Exception {
        if (is != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    private void generate(LocaleFileName locale) {
        final String dateTimeFormat;
        final String yearDateFormat;
        switch (locale) {
            case CS:
                dateTimeFormat = DATE_TIME_FORMAT_CZ;
                yearDateFormat = DATE_FORMAT_CZ;
                break;
            case EN:
                dateTimeFormat = DATE_TIME_FORMAT_EN;
                yearDateFormat = DATE_FORMAT_EN;
                break;
            default:
                dateTimeFormat = DATE_TIME_FORMAT_EN;
                yearDateFormat = DATE_FORMAT_EN;
                break;
        }
        // formater datumu zacatku konani akce
        sdfYearDate = new SimpleDateFormat(yearDateFormat);
        // formatery pro datumy posunute do casoveho pasma akce
        sdf = new SimpleDateFormat(dateTimeFormat);
        sdfTime = new SimpleDateFormat(TIME_FORMAT);
        if (StringUtils.isNotBlank(ei.getTimeZone())) {
            sdf.setTimeZone(TimeZone.getTimeZone(ei.getTimeZone()));
            sdfTime.setTimeZone(TimeZone.getTimeZone(ei.getTimeZone()));
        }
        // formatery pro datum konani akce
        sdfe = new SimpleDateFormat(dateTimeFormat);
        sdfeTime = new SimpleDateFormat(TIME_FORMAT);

        fillGoToTakeplaceAndLocale(locale);
        fillEventNameDescriptionAndLogo(locale);
        fillJsonApiPlaceholders();
        addEventBegin(locale);
        addImportantDates(locale);
        addRegistration(locale);
        addContributions(locale);
        addContact(locale);
        addPartners(locale);
    }

    private void fillJsonApiPlaceholders() {
        stringTemplate.setAttribute(JSON_EVENT_UUID, ei.getEventUuid());
        stringTemplate.setAttribute(JSON_OVERSEER_URL, ei.getOverseerJsonAPIUrl());
        stringTemplate.setAttribute(JSON_ADMIN_URL, ei.getAdministrationUrl());
        stringTemplate.setAttribute(JSON_LOCALE, ei.getLocale());
        stringTemplate.setAttribute(JSON_EVENT_CURRENCY, ei.getRegistration().getCurrency().name());
    }

    private void fillGoToTakeplaceAndLocale(LocaleFileName locale) {
        StringBuilder sb = new StringBuilder("<a href=\"https://");
        if (StringUtils.isBlank(ei.getCorporatePrefix())) {
            sb.append("app.takeplace.eu/takeplace/takeplace.html?locale=");
        } else {
            sb.append(ei.getCorporatePrefix());
            sb.append(".takeplace.eu/takeplace/takeplace.html?locale=");
        }
        sb.append(locale.name().toLowerCase());
        sb.append("#Event?event=");
        sb.append(ei.getEventUuid());
        sb.append("\" target=\"_blank\">");
        sb.append(ProfileLocale.value(locale, ProfileLocale.GO_TO_TAKEPLACE));
        sb.append("</a>");
        stringTemplate.setAttribute(GO_TO_TAKEPLACE, sb.toString());
        stringTemplate.setAttribute(CURRENT_LOCALE, locale.name().toLowerCase());
        stringTemplate.setAttribute(PAGE_NAME, EventUtils.profileName(ei.getEventAcronym(), ei.getEventYear(), ei.getEventSeries()));
    }

    private void fillEventNameDescriptionAndLogo(LocaleFileName locale) {
        stringTemplate.setAttribute(EVENT_NAME, ei.getEventName());
        stringTemplate.setAttribute(EVENT_DESCRIPTION_TITLE, ProfileLocale.value(locale, ProfileLocale.DESCRIPTION_TITLE));
        stringTemplate.setAttribute(EVENT_DESCRIPTION, ei.getEventDescription());

        String url;
        if (!StringUtils.isEmpty(ei.getEventProfileLogoUrl())) {
            url = ei.getEventProfileLogoUrl();
        } else {
            url = NO_EVENT_PROFILE_IMAGE;
        }
        stringTemplate.setAttribute(EVENT_PROFILE_LOGO_URL, url);
    }

    private void addEventBegin(LocaleFileName locale) {
        if (ei.getEventBegin() != null) {
            StringBuilder sb = new StringBuilder();
            divStart(sb, "eventBegin");
            sb.append(ProfileLocale.value(locale, ProfileLocale.EVENT_BEGIN));
            sb.append(" ");
            sb.append(sdfYearDate.format(ei.getEventBegin()));
            sb.append(" ");
            sb.append(sdfeTime.format(ei.getEventBegin()));
            sb.append(DIV_END);
            stringTemplate.setAttribute(PART_EVENT_BEGIN, sb.toString());
        }
    }

    private void addImportantDates(LocaleFileName locale) {
        List<DateRecord> records = ei.getImportantDates(locale);
        if (records == null) {
            records = new ArrayList<DateRecord>();
        }
        if (records.isEmpty()) {
            // nejsou data, nevykreslujeme
            return;
        }
        StringBuilder sb = new StringBuilder(DIV_START);
        addDateRecords(sb, records);
        sb.append(DIV_END);
        addNextPart(ProfileLocale.value(locale, ProfileLocale.IMPORTANT_DATES), sb.toString(), PART_IMPORTANT_DATES);
    }

    private void addDateRecords(StringBuilder sb, List<DateRecord> list) {
        sortDateRecords(list);

        for (DateRecord dr : list) {
            divStart(sb, "date1");
            sb.append(dr.getFrom() == null ? NA : (dr.isEventDate() ? sdfe.format(dr.getFrom()) : sdf.format(dr.getFrom())));
            if (!dr.isInSameDay() && dr.getTo() != null) {
                sb.append(" - ");
                sb.append(dr.isEventDate() ? sdfe.format(dr.getTo()) : sdf.format(dr.getTo()));
            } else if (dr.isInSameDay() && dr.getTo() != null) {
                sb.append(" - ");
                sb.append(dr.isEventDate() ? sdfeTime.format(dr.getTo()) : sdfTime.format(dr.getTo()));
            }
            sb.append(DIV_END);
            divStart(sb, "date2");
            sb.append(dr.getText());
            sb.append(DIV_END);
            sb.append(DIV_CLEAR);
        }
    }

    private void sortDateRecords(List<DateRecord> list) {
        Collections.sort(list, new Comparator<DateRecord>() {
            @Override
            public int compare(DateRecord o1, DateRecord o2) {
                if (o1.getFrom() == null) {
                    return Integer.MIN_VALUE;
                }
                if (o2.getFrom() == null) {
                    return Integer.MAX_VALUE;
                }
                return o1.getFrom().compareTo(o2.getTo());
            }
        });
    }

    private void addRegistration(LocaleFileName locale) {
        if (ei.getRegistration() == null || ei.getRegistration().getTickets().isEmpty()) {
            // nejsou data, nevykreslujeme
            return;
        }
        final Currency currency = ei.getRegistration().getCurrency();
        StringBuilder sb = new StringBuilder(DIV_START);
        List<Ticket> tickets = ei.getRegistration().getTickets();
        addRegistrationTicket(locale, sb, tickets.get(0), currency);
        for (int i = 1; i < tickets.size(); i++) {
            sb.append(DIV_REGISTRATION_SEPARATOR);
            addRegistrationTicket(locale, sb, tickets.get(i), currency);
        }
        sb.append(DIV_END);
        addNextPart(ProfileLocale.value(locale, ProfileLocale.REGISTRATION), sb.toString(), PART_REGISTRATION);
    }

    private void addRegistrationTicket(LocaleFileName locale, StringBuilder sb, Ticket t, Currency currency) {
        boolean isZeroPrice = t.getPrice().compareTo(BigDecimal.ZERO) == 0;
        String price = Formats.decimalFormat(currency).format(t.getPrice().doubleValue());
        divStart(sb, "rborder");
        sb.append("<table width=\"100%\"><tr><td valign=\"middle\">");
        divStart(sb, "date1r");
        sb.append(t.getName());
        sb.append(DIV_END);
        sb.append("</td><td valign=\"middle\">");
        divStart(sb, "date3r");
        divStart(sb, "rbL");
        sb.append(DIV_END);
        divStart(sb, "rbC");
        sb.append("<a href=\"");
        sb.append(t.getUrl());
        sb.append("&locale=");
        sb.append(locale.name().toLowerCase());
        sb.append("\" target=\"_blank\">");
        sb.append(ProfileLocale.value(locale, ProfileLocale.REGISTER));
        sb.append("</a>");
        sb.append(DIV_END);
        divStart(sb, "rbR");
        sb.append(DIV_END);
        sb.append(DIV_CLEAR);
        sb.append(DIV_END);
        divStart(sb, "date2r");
        sb.append(isZeroPrice ? ProfileLocale.value(locale, ProfileLocale.FREE) : Formats.assignCurrency(currency, price));
        sb.append(DIV_END);
        sb.append(DIV_CLEAR);
        sb.append("</td></tr></table>");
        sb.append(DIV_END);
    }

    private void addContributions(LocaleFileName locale) {
        List<Contribution> contributions = ei.getContributions(locale);
        if (contributions == null) {
            contributions = new ArrayList<Contribution>();
        }
        if (contributions.isEmpty()) {
            // nejsou data, nevykreslujeme
            return;
        }
        StringBuilder sb = new StringBuilder(DIV_START);
        addContribution(sb, contributions.get(0));
        for (int i = 1; i < contributions.size(); i++) {
            sb.append(DIV_REGISTRATION_SEPARATOR);
            addContribution(sb, contributions.get(i));
        }
        sb.append(DIV_END);
        addNextPart(ProfileLocale.value(locale, ProfileLocale.CONTRIBUTIONS), sb.toString(), PART_CONTRIBUTIONS);
    }

    private void addContribution(StringBuilder sb, Contribution c) {
        sb.append(DIV_START);
        sb.append(c.getName());
        sb.append(DIV_END);
        if (!StringUtils.isEmpty(c.getDescription())) {
            divStart(sb, "contributionDescription");
            sb.append(c.getDescription());
            sb.append(DIV_END);
        }
    }

    private void addContact(LocaleFileName locale) {
        if (ei.getContactInformation() == null) {
            // nejsou data, nevykreslujeme
            return;
        }
        Contact c = ei.getContactInformation();
        StringBuilder sb = new StringBuilder();
        if (!StringUtils.isEmpty(c.getAddress())) {
            divStart(sb, "address");
            sb.append(ProfileLocale.value(locale, ProfileLocale.CONTACT_ADDRESS));
            sb.append(COLON_SPACE);
            if (StringUtils.isNotBlank(c.getPlace())) {
                sb.append(c.getPlace());
                sb.append(", ");
            }
            sb.append(c.getAddress());
            sb.append(DIV_END);
        }
        if (!StringUtils.isEmpty(c.getEmail())) {
            divStart(sb, "address");
            sb.append(ProfileLocale.value(locale, ProfileLocale.CONTACT_EMAIL));
            sb.append(COLON_SPACE);
            sb.append("<a href=\"mailto:");
            sb.append(c.getEmail());
            sb.append("\">");
            sb.append(c.getEmail());
            sb.append("</a>");
            sb.append(DIV_END);
        }
        if (!StringUtils.isEmpty(c.getPhoneNumber())) {
            divStart(sb, "address");
            sb.append(ProfileLocale.value(locale, ProfileLocale.CONTACT_PHONE));
            sb.append(COLON_SPACE);
            sb.append(c.getPhoneNumber());
            sb.append(DIV_END);
        }
        if (!StringUtils.isEmpty(c.getWeb())) {
            divStart(sb, "address");
            sb.append(ProfileLocale.value(locale, ProfileLocale.CONTACT_WEB));
            sb.append(COLON_SPACE);
            sb.append("<a href=\"http://");
            sb.append(c.getWeb());
            sb.append("\" target=\"_blank\">");
            sb.append(c.getWeb());
            sb.append("</a>");
            sb.append(DIV_END);
        }
        if (!StringUtils.isEmpty(c.getAddress())) {
            try {
                sb.append(EventUtils.googleMapProfilePage(URLEncoder.encode(c.getAddress(), "utf-8"), ei.getLocale(),
                        ProfileLocale.value(locale, ProfileLocale.VIEW_LARGER_MAP)));
            } catch (Exception ex) {
                // nebude se vkladat mapa protoze se neco pokazilo pri enkodovani adresy
            }
        }
        String result = sb.toString();
        if (!StringUtils.isEmpty(result)) {
            addNextPart(ProfileLocale.value(locale, ProfileLocale.CONTACT_INFORMATION), result, PART_CONTACT);
        }
    }

    private void addPartners(LocaleFileName locale) {
        if (StringUtils.isBlank(ei.getPartnersImageUuid())) {
            // nejsou data, nevykreslujeme
            return;
        }
        StringBuilder sb = new StringBuilder("<img src=\"");
        sb.append(administrationUrl).append("/").append(ImageConstants.LOAD_IMAGE_SERVLET_PATH);
        sb.append(ei.getPartnersImageUuid());
        sb.append("\" alt=\"");
        sb.append(ei.getEventName());
        sb.append(" partners\" style=\"max-width: 100%\">");
        addNextPart(ProfileLocale.value(locale, ProfileLocale.PARTNERS), sb.toString(), PART_PARTNERS);
    }

    private void addNextPart(String title, String content, String key) {
        StringBuilder sb = new StringBuilder("<h2>");
        sb.append(title);
        sb.append("</h2>");
        divStart(sb, "dataDiv");
        sb.append(content);
        sb.append(DIV_END);
        stringTemplate.setAttribute(key, sb.toString());
    }

    public static void main(String[] args) throws Exception {
        Contact contact = new Contact();
        contact.setAddress("Božetěchova 1, Brno, Czech Republic");
        contact.setEmail("email@em.al");
        contact.setPhoneNumber("420 000 111 222");
        contact.setPlace("FIT");
        contact.setWeb("www.u-nas-doma.cz");

        Ticket ticket1 = new Ticket();
        ticket1.setName("Název typu lístku");
        ticket1.setPrice(BigDecimal.ZERO);
        ticket1.setUrl("akce-lístku");

        Ticket ticket2 = new Ticket();
        ticket2.setName("Název typu lístku co je druhý v pořadí co má delší text k tlačítku");
        ticket2.setPrice(new BigDecimal(50));
        ticket2.setUrl("akce-lístku2");

        Registration registration = new Registration();
        registration.setCurrency(Currency.CZK);
        registration.getTickets().add(ticket1);
        registration.getTickets().add(ticket2);

        DateRecord registrationBegin = new DateRecord(new Date(), new Date(), "Blabla bla vstupenka pro VIP a jiné lidi");
        DateRecord party = new DateRecord(new Date(), new Date(), "Noční pařba při svíčkách", true);

        EventInformation ei = new EventInformation();
        ei.setAdministrationUrl("adminUrl");
        ei.setContactInformation(contact);
        ei.setEventBegin(new Date());
        ei.setEventAcronym("Zkratka acke");
        ei.setEventDescription("Praesent consectetur dignissim sapien ut feugiat. Suspendisse blandit congue eleifend. Maecenas sollicitudin consectetur sem, a molestie mauris feugiat quis. Etiam ut ultrices nibh. Sed nec ligula tortor, id dapibus est. Etiam vel turpis vitae lectus condimentum vulputate. Nulla bibendum congue lorem fringilla tempus. Pellentesque facilisis vestibulum ante, quis tempor felis vehicula et. In hac habitasse platea dictumst. Nam nibh leo, mattis nec porta non, commodo eget ligula. Nulla tellus nibh, semper et convallis et, porta imperdiet mi. Integer quis risus eget lorem convallis vehicula. ");
        ei.setEventName("Název akce");
        ei.setEventSeries("série akce");
        ei.setEventUuid("uuidakce");
        ei.setEventYear("2012");
        ei.setLocale("en");
        ei.setRegistration(registration);
        ei.getImportantDates(LocaleFileName.EN).add(registrationBegin);
        ei.getImportantDates(LocaleFileName.EN).add(party);
        ei.setTimeZone("America/Los_Angeles");

        FileWriter out = new FileWriter("c:/data/page.html");
        EventProfileTemplate ept = new EventProfileTemplate(ei, "adminUrl");
        ept.generateEventProfile(out, LocaleFileName.EN);
    }
}
