package consys.event.registration.api.list;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationsList {

    public static final String LIST_TAG = "1c4cdfe1740f03e0ba28b0ce8ed6b0df";
    /**
     * Filter podla stavu registracie.
     */
    public static final int Filter_STATE_ALL = 1;
    public static final int Filter_STATE_PREREGISTERED = 11;
    public static final int Filter_STATE_REGISTERED = 12;
    public static final int Filter_STATE_WAITING = 13;
    public static final int Filter_STATE_CANCELED = 14;
    /**
     * Order podla stavu registracie
     */
    public static final int Order_REGISTRATION_STATE = 1;
    /**
     * Order podla datumu registracie
     */
    public static final int Order_REGISTRATION_DATE = 2;
    /**
     * Order podla datumu registracie
     */
    public static final int Order_LAST_NAME = 3;
}
