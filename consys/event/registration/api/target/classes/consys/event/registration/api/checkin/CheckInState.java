package consys.event.registration.api.checkin;

/**
 *
 * @author pepa
 */
public interface CheckInState {

    public static final int CHECK_IN_ALL = -1;
    public static final int CHECK_OUT_ALL = -2;
}
