/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.registration.api.notx;

/**
 * Tagy v NotX pre module Registration:
 *
 *
 *
 * @author palo
 */
public interface NotxTags {

    /**
     * Vsetci participanti - potvrdeny a nepotvrdeny
     */
    public static final String PARTICIPANTS = "participants";
    /**
     * Len potvrdeni
     */
    public static final String CONFIRMED = "confirmed";
    /**
     * Len predregistrovany
     */
    public static final String PREREGISTRED = "preregistred";
    /**
     * Otagovany podla toho ake maju zaregistrovane bundle
     */
    public static final String BUNDLE = "bundle";
}
