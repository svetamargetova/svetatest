package consys.event.registration.api.list;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface CheckInList {

    public static final String LIST_TAG = "9cd87fbc016fa715a0f3969a4d8b7543";

    public static final int Order_LAST_NAME = 1;

    public static final int Filter_ALL = 1;
    public static final int Filter_CHECKED = 2;
    public static final int Filter_NOT_CHECKED = 3;

}
