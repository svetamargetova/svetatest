package consys.event.registration.api.list;

/**
 *
 * @author pepa
 */
public interface RegistrationCouponList {

    public static final String LIST_TAG = "7d6054164fc98813b0c82aec4733baab";
    // filtry
    public static final int Filter_ALL = 1;
    public static final int Filter_USED = 2;
    public static final int Filter_NOT_USED = 3;
    // razeni
    public static final int Order_DEFAULT = 1;
}
