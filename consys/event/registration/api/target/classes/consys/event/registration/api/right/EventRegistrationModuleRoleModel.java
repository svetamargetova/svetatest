package consys.event.registration.api.right;

import consys.event.common.api.right.ModuleRoleModel;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventRegistrationModuleRoleModel extends ModuleRoleModel {

    /** Pravo/role uzivatela nastavovat registracie */
    public static final String RIGHT_REGISTRATION_SETTINGS = "RIGHT_REGISTRATION_SETTINGS";
    /** Administrator registracii */
    public static final String RIGHT_REGISTRATION_ADMINISTRATOR = "RIGHT_REGISTRATION_ADMINISTRATOR";
    /** Check in administrator */
    public static final String RIGHT_REGISTRATION_CHECK_IN = "RIGHT_REGISTRATION_CHECK_IN";


    public EventRegistrationModuleRoleModel() {
        super(3);
    }

    @Override
    public void registerRights() {
        addRole(RIGHT_REGISTRATION_SETTINGS,true);
        addRole(RIGHT_REGISTRATION_ADMINISTRATOR,true);
        addRole(RIGHT_REGISTRATION_CHECK_IN, true);
    }
}
