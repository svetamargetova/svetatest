package consys.event.registration.api.properties;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationProperties {

    public static final String PARTICIPANT_LIST_VISIBILE = "participant_list_visible";
    /** bude mozne registrovat nekoho jineho */
    public static final String REGISTER_ANOTHER = "register_another";
}
