package consys.event.registration.api.list;

/**
 *
 * @author pepa
 */
public interface InternalRegistrationListApi {

    public static final String LIST_TAG = "07954d6980b948b183a4b8cf3b437e32";
    public static final int Order_LAST_NAME = 1;
    public static final int Filter_ALL = 1;
}
