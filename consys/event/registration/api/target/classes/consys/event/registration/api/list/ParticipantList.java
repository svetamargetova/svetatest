package consys.event.registration.api.list;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ParticipantList {

    public static final String LIST_TAG = "e8a2e8752227e04a4d72023d7d127e5b";

    public static final int Order_LAST_NAME = 1;

    public static final int Filter_ALL = 1;

}
