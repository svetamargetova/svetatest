package consys.event.registration.api.navigation;

/**
 *
 * @author palo
 */
public interface Navigation {

    public static final String REGISTER = "registration";
    public static final String REGISTER_ANOTHER = "registration-another";
    public static final String PARTICIPANT_LIST = "participant-list";
}
