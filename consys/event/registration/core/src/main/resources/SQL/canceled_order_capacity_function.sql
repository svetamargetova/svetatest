CREATE OR REPLACE FUNCTION registration.canceled_order_capacity_function() RETURNS trigger AS $canceled_order_capacity_function$

         DECLARE
        _id_bundle BIGINT;
	-- objednavka baliku
	_bundle_registration RECORD;
        -- bundle
	_bundle RECORD;

	BEGIN

	-- Ak je stara objednavka ORDERED (alebo confirmed)a novy stav je CANCELED alebo TIMEOUTED        
	IF ((OLD.state = 0 or OLD.state = 1) and (NEW.state = 2 or NEW.state = 3)) THEN
            -- Potom pre vsetky registracie
            FOR _bundle_registration IN SELECT * FROM registration.USER_BUNDLE_REGISTRATION where id_registration_order = OLD.id LOOP
                -- zamkneme bundle pre update
                select bundle.id into _id_bundle from registration.registration_bundle as bundle left join registration.registration_cycle as c on c.id_bundle=bundle.id where c.id=_bundle_registration.id_reg_cycle;
                select * into _bundle from registration.registration_bundle bundle where _id_bundle=bundle.id FOR UPDATE;
                -- odcitame z kapacity
                update registration.registration_bundle SET registred=registred-_bundle_registration.quantity where id=_bundle.id;
            END LOOP;
	END IF;	

	RETURN NEW;

	END;
$canceled_order_capacity_function$ LANGUAGE 'plpgsql';