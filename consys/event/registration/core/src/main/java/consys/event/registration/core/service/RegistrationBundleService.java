package consys.event.registration.core.service;

import consys.common.core.exception.AlreadyUsedException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.exception.CouponCodeException;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.event.registration.core.bo.thumb.RegistrationCouponStats;
import consys.event.registration.core.exception.CycleNotEmptyException;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.event.registration.core.exception.CyclesOverlapsException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationBundleService {

    /*------------------------------------------------------------------------*/
    /* ----  D E L E T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Odstrani registracny bundle. V pripade ze uz existuje na nejaky reg. cyklus
     * registracia tak sa vyhodi vynimka.
     */
    public void deleteBundle(String bundleUuid) throws CycleNotEmptyException, NoRecordException, RequiredPropertyNullException;

    /**
     * Odstrani registracny cuklus. V pripade ze uz existuje na nejaky reg. cyklus
     * registracia tak sa vyhodi vynimka.
     * @param cycleUuid uuid cyklu
     */
    public void deleteCycle(String cycleUuid) throws CycleNotEmptyException, NoRecordException, RequiredPropertyNullException;

    /**
     * Odstrani registracni kupon, pokud neni jiz nekde pouzity
     * @param couponUuid uuid slevoveho kuponu
     */
    public void deleteRegistrationCoupon(String couponUuid) throws NoRecordException, RequiredPropertyNullException, AlreadyUsedException;

    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Update bundlu. Pri update sa zisti ci sa upravou kapacity neporusi integrita.
     *
     */
    public void updateRegistrationBundle(RegistrationBundle bundle)
            throws BundleNotUniqueException, BundleCapacityFullException, RequiredPropertyNullException;

    public void updateRegistrationCycle(RegistrationCycle cycle)
            throws CyclesOverlapsException, NoRecordException, RequiredPropertyNullException;

    public void updateRegistrationBundleOrder(String bundleUuid, int oldPosition, int newPosition, boolean subbundle)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Provede aktualizaci kapacity a data konce platnosti slevoveho kuponu
     * @param couponUuid uuid slevoveho kuponu
     * @param capacity nova kapacita kuponu
     * @param endDate datum konce platnosti kuponu
     */
    public void updateRegistrationCoupon(String couponUuid, int capacity, Date endDate)
            throws NoRecordException, RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    public RegistrationBundle createDuplicate(String bundleUuid, String bundleTitle)
            throws BundleNotUniqueException, NoRecordException, RequiredPropertyNullException;

    /**
     * Vytvori novy registracny bundle. Title musi byt unique
     */
    public void createBundle(RegistrationBundle bundle)
            throws BundleNotUniqueException, RequiredPropertyNullException;

    /**
     * Vytvori novy registracne bundle. Title musi byt unique
     */
    public void createBundles(List<RegistrationBundle> bundles)
            throws BundleNotUniqueException, RequiredPropertyNullException;

    /**
     * Vytvori nove registracne baliky ktore su vzhladom na priznak <code>subbundle</code>
     * subbalikami alebo hlavnymi balikami.
     * <p>
     * Balik mus mat unikatny title
     *
     * @param bundles baliky co sa maju vytvorit
     * @param subbundle priznak ak <code>true</code> potom sa jedna o subbalik inak o hlavny balik
     * @throws BundleNotUniqueException ak balik
     * @throws RequiredPropertyNullException ak chyba nazov balika
     */
    public void createBundles(List<RegistrationBundle> bundles, boolean subbundle)
            throws BundleNotUniqueException, RequiredPropertyNullException;

    /**
     * Vytvori registracne cykly pre bundle
     */
    public void createRegistrationCycles(String bundleUuid, List<RegistrationCycle> newCycles)
            throws CyclesOverlapsException, ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Vytvori novy registracny kupon pre balicke identifikovany <code>bundleUuid</code>
     * 
     * @param bundleUuid uuid pre ktory balicek sa ma kupon vytvorit
     * @param coupon kupon
     * @throws NoRecordException ak neexistuje balicek
     * @throws CouponCodeException ak kod kuponu uz pre balicek existuje
     * @throws RequiredPropertyNullException ak chyba nieco v kupone
     */
    public void createRegistrationCoupon(String bundleUuid, RegistrationCoupon coupon)
            throws NoRecordException, CouponCodeException, RequiredPropertyNullException;

    /**
     * Vytvori novy registracny kupon pre balicek <code>bundle</code>
     * 
     * @param bundle balicek pre ktory sa ma kupon vytvorit
     * @param coupon kupon
     * @throws NoRecordException ak neexistuje balicek
     * @throws CouponCodeException ak kod kuponu uz pre balicek existuje
     * @throws RequiredPropertyNullException ak chyba nieco v kupone
     */
    public void createRegistrationCoupon(RegistrationBundle bundle, RegistrationCoupon coupon)
            throws NoRecordException, CouponCodeException, RequiredPropertyNullException;

    /**
     * Vygeneruje novy kluc pre discount kupon.
     * @param bundleUuid pre aky bundle sa to ma generovat
     * @return novy kluc     
     * @throws RequiredPropertyNullException ak chyba vstup
     */
    public String generateRegistrationCouponKey(String bundleUuid)
            throws RequiredPropertyNullException;

    /**
     * Zvaliduje ci je dany kluc platnym pre registration coupon.
     * @param bundleUuid uuid bundlu
     * @param key kluc ktoru sa ma overit
     * @return <code>true</code> ak je validny, inak nie.
     * @throws RequiredPropertyNullException 
     */
    public boolean isValidRegistrationCouponKey(String bundleUuid, String key)
            throws RequiredPropertyNullException;


    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    public RegistrationBundle loadById(Long id) throws NoRecordException, RequiredPropertyNullException;

    public RegistrationCycle loadRegistrationCycle(String cycleUuid)
            throws NoRecordException, RequiredPropertyNullException;

    public RegistrationBundle loadRegistrationBundle(String uuid)
            throws NoRecordException, RequiredPropertyNullException;

    public RegistrationBundle loadRegistrationBundleWithCycles(String uuid)
            throws NoRecordException, RequiredPropertyNullException;

    public Integer loadBundleRegistrationCount(Long bundleId);

    public RegistrationCouponStats loadRegistrationCouponsStats(String bundleUuid) throws RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  L I S T  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita len hlavne registracne baliky
     *
     * @return zoznam hlavnych registracnych balikov
     * @throws NoRecordException
     */
    public List<RegistrationBundle> listRegistrationMainBundles() throws NoRecordException;

    /**
     * Nacita len registracne sub baliky
     *
     * @return zoznam registracnych subbalikov
     * @throws NoRecordException
     */
    public List<RegistrationBundle> listRegistrationSubBundles() throws NoRecordException;

    /**
     * Nacita hlavne registracne bundle a dotiahne k nim aj vsetky ich cykly
     *
     * @return zoznam hlavnych registracnych balikov s cyklami
     * @throws NoRecordException
     */
    public List<RegistrationBundle> listRegistrationMainBundlesWithCycles() throws NoRecordException;

    /**
     * Nacita registracne sub baliky a dotiahne k nim aj vsetky ich cykly
     *
     * @return zoznam registracnych sub balikov s cyklami
     * @throws NoRecordException
     */
    public List<RegistrationBundle> listRegistrationSubBundlesWithCycles() throws NoRecordException;

    /**
     * Nacita registracne moznosti k hlavnym balikom. Teda vyberie cyklus ktory je prave
     * aktualny
     *
     * @return registracne moznosti hlavnych balikov
     * @throws NoRecordException
     */
    public List<RegistrationOption> listMainRegistrationBundlesOptions() throws NoRecordException;

    /**
     * Nacita registracne moznosti subbalikov k registracii uzivatela. Teda vyberie cyklus ktory je prave
     * aktualny pre dany subbalik a pre hlavny balik a odstrani uz zakupene resp. objednane subbaliky.
     *
     * @return registracne moznosti subbalikov
     * @throws NoRecordException
     */
    public List<RegistrationOption> listRegistrationSubBundlesOptionsForUser(String userUuid) throws NoRecordException;

    /**
     * Nacita subbaliky vzhladom k urcenemu halvnemu baliku.
     *
     * @return registracne moznosti subbalikov
     * @throws NoRecordException
     */
    public List<RegistrationOption> listRegistrationSubBundlesOptions(String bundleUuid) throws NoRecordException;

    /**
     * Nacita subbaliky vzhladom k urcenemu halvnemu baliku aj s hlavnym balikom
     *
     * @return registracne moznosti subbalikov s hlavym balikom
     * @throws NoRecordException
     */
    public List<RegistrationOption> listRegistrationSubBundlesWithMainBundle(String bundleUuid) throws NoRecordException;

    /**
     * Nacita vsetky baliky
     * @return
     * @throws NoRecordException
     */
    public List<RegistrationBundle> listAllBundles() throws NoRecordException;

    public List<RegistrationCycle> listNextCyclesAfter(String bundleUuid) throws NoRecordException, RequiredPropertyNullException;

    /** vraci datum zacatku nejdrivejsiho registracniho cyklu */
    public Date loadFirstRegistrationBegin();

    /** vraci datum konce nejpozdejsiho registracniho cyklu */
    public Date loadLastRegistrationEnd();
}
