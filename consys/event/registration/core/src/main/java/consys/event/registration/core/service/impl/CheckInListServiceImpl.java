package consys.event.registration.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.CheckInList;
import consys.event.registration.core.bo.thumb.CheckInListItem;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.event.registration.core.service.CheckInListService;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CheckInListServiceImpl extends AbstractService implements CheckInListService {

    private UserRegistrationDao registrationDao;

    @Override
    public String getTag() {
        return CheckInList.LIST_TAG;
    }

    @Override
    public List<CheckInListItem> listItems(Constraints constraints, Paging paging) {
        return registrationDao.listParticipantsForCheckInList(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return registrationDao.loadParticipantsCountForCheckInList(constraints).intValue();
    }

    /**
     * @return the registrationDao
     */
    public UserRegistrationDao getRegistrationDao() {
        return registrationDao;
    }

    /**
     * @param registrationDao the registrationDao to set
     */
    public void setRegistrationDao(UserRegistrationDao registrationDao) {
        this.registrationDao = registrationDao;
    }

    @Override
    public List<CheckInListItem> listItemsExport(Constraints constraints, Paging paging) {
        return listItems(constraints, paging);
    }
}
