package consys.event.registration.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.ListUtils;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.RegistrationsList;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.*;
import consys.event.registration.core.dao.UserRegistrationDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserRegistrationDaoImpl extends GenericDaoImpl<UserRegistration> implements UserRegistrationDao {

    private static final String ZERO = "0";

    @Override
    public UserRegistration load(Long id) throws NoRecordException {
        return load(id, UserRegistration.class);
    }

    @Override
    public Long loadRegistrationCountForBundle(String bundleUuid) {
        return (Long) session().
                createQuery("select count(r.id) from UserBundleRegistration r,RegistrationCycle rc where r.registrationCycle.id = rc.id and rc.bundle.uuid = :BUNDLE_UUID").
                setString("BUNDLE_UUID", bundleUuid).
                uniqueResult();
    }

    @Override
    public Long loadRegistrationCountForCycle(String cycleUuid) {
        return (Long) session().
                createQuery("select count(r.id) from UserBundleRegistration r where r.registrationCycle.uuid = :CYCLE_UUID").
                setString("CYCLE_UUID", cycleUuid).
                uniqueResult();
    }

    @Override
    public boolean isUserAlreadyRegistred(String userUuid) {
        String has = (String) session().
                createQuery("select r.uuid from UserRegistration r where r.user.uuid=:USER_UUID and (r.state=0 or r.state=1 or r.state=4)").
                setString("USER_UUID", userUuid).
                uniqueResult();
        return has != null && has.length() > 0 ? true : false;
    }

    @Override
    public UserRegistration loadUserRegistrationDetailedByUserUuid(String userUuid) throws NoRecordException {
        UserRegistration reg = (UserRegistration) session().
                createQuery("select r from UserRegistration r LEFT JOIN FETCH r.orders as o LEFT JOIN FETCH o.registredBundles as registredBundle LEFT JOIN FETCH registredBundle.registrationCoupon LEFT JOIN FETCH registredBundle.registrationCycle as cycle LEFT JOIN FETCH cycle.bundle LEFT JOIN FETCH r.user as ur where ur.uuid=:USER_UUID and (r.state=0 or r.state=1 or r.state=4 or r.state=5)").
                setString("USER_UUID", userUuid).
                //setMaxResults(1).
                uniqueResult();

        if (reg == null) {
            throw new NoRecordException();
        }
        return reg;
    }

    @Override
    public UserRegistration loadUserRegistrationDetailedByRegistrationUuid(String registrationUuid) throws NoRecordException {
        UserRegistration reg = (UserRegistration) session().
                createQuery("select r from UserRegistration r LEFT JOIN FETCH r.orders as o LEFT JOIN FETCH o.registredBundles as registredBundle LEFT JOIN FETCH registredBundle.registrationCoupon LEFT JOIN FETCH registredBundle.registrationCycle as cycle LEFT JOIN FETCH cycle.bundle JOIN FETCH r.user where r.uuid=:UUID").
                setString("UUID", registrationUuid).
                uniqueResult();

        if (reg == null) {
            throw new NoRecordException();
        }
        return reg;
    }

    @Override
    public UserRegistration loadRegistrationByUuid(String uuid) throws NoRecordException {
        UserRegistration reg = (UserRegistration) session().
                createQuery("select r from UserRegistration r  where r.uuid=:UUID").
                setString("UUID", uuid).
                setMaxResults(1).
                uniqueResult();

        if (reg == null) {
            throw new NoRecordException();
        }
        return reg;
    }

    @Override
    public UserRegistration loadWaitingRegistration(String registrationUuid) throws NoRecordException {
        UserRegistration reg = (UserRegistration) session().
                createQuery("select r from UserRegistration r left join fetch r.orders join fetch r.user where r.uuid=:REG_UUID and r.state=5").
                setString("REG_UUID", registrationUuid).setMaxResults(1).uniqueResult();
        if (reg == null) {
            throw new NoRecordException();
        }
        return reg;
    }

    @Override
    public UserRegistration loadRegistrationToPay(String registrationUuid) throws NoRecordException {
        UserRegistration reg = (UserRegistration) session().
                createQuery("select r from UserRegistration r where r.uuid=:USER_UUID and r.state=0").
                setString("USER_UUID", registrationUuid).
                uniqueResult();

        if (reg == null) {
            throw new NoRecordException();
        }
        return reg;
    }

    @Override
    public List<UserEvent> listAllActiveParticiapntsForBadges() throws NoRecordException {
        List<UserEvent> users = session().createQuery("select ue from UserRegistration r join r.user as ue where r.state=0 or r.state=1 or r.state=4").list();
        return users;
    }

    @Override
    public List<UserRegistration> listManagedRegistrations(String uuidManager) {
        Query query = session().createQuery("SELECT ur FROM UserRegistration ur LEFT JOIN FETCH ur.user "
                + "LEFT JOIN FETCH ur.orders as o LEFT JOIN FETCH o.registredBundles as rb "
                + "LEFT JOIN FETCH rb.registrationCoupon LEFT JOIN FETCH rb.registrationCycle as c "
                + "LEFT JOIN FETCH c.bundle"
                + ", ParticipantManager pm WHERE ur.user.id=pm.participant.id and pm.manager.uuid=:MANAGER "
                + "and (ur.state=0 or ur.state=1 or ur.state=4 or ur.state=5)");
        return query.setString("MANAGER", uuidManager).list();
    }

    /* ------------------------------------------------------------------------ */
    /* ---- P A R T I C I P A N T L I S T ---- */
    /* ------------------------------------------------------------------------ */
    @Override
    public List<ParticipantListItem> listParticipants(Constraints constraints, Paging paging) {
        // mame zatim len jeden order a jeden filter
        Query query = session().
                createQuery("select r.uuid, o.orderDate, u.uuid, u.fullName, u.organization, u.profileImagePrefix, u.position from UserBundleRegistration br left join br.registrationOrder as o left join o.userRegistration as r left join r.user as u where (o.state=0 or o.state=1 or o.state=4) and br.registrationCycle.bundle.subBundle=false order by u.lastName ");
        if (paging != null) {
            query.setFirstResult(paging.getFirstResult());
            query.setMaxResults(paging.getMaxResults());
        }
        List<Object[]> list = query.list();
        List<ParticipantListItem> out = Lists.newArrayList();
        for (Object[] item : list) {
            ParticipantListItem r = new ParticipantListItem();
            r.setRegistrationUuid((String) item[0]);
            r.setRegistrationDate((Date) item[1]);
            r.setUserUuid((String) item[2]);
            r.setUserName((String) item[3]);
            r.setUserPosition((String) item[6]);
            r.setUserOrganization((String) item[4]);
            r.setProfileImagePrefix((String) item[5]);
            out.add(r);
        }
        return out;
    }

    @Override
    public Long loadParticipantsCount(Constraints constraints) {
        return (Long) session().
                createQuery("select count(*) from UserRegistration br where br.state=0 or br.state=1 or br.state=4").
                uniqueResult();
    }
    /* ------------------------------------------------------------------------ */
    /* ---- R E G I S T R A T I O N L I S T ---- */
    /* ------------------------------------------------------------------------ */
    //private static final String LIST_REGISTRATIONS_QUERY = "select r.uuid,r.registredDate,r.state,r.paid,b.title,u.uuid,u.fullName,u.organization from UserRegistration r join r.user as u join r.cycle.bundle as b ";
    private static final String LIST_REGISTRATIONS_QUERY = "select r.uuid, o.orderDate, r.state, o.overallPrice,cycle.bundle.title, u.uuid, u.fullName, u.organization, u.profileImagePrefix, u.position, br.quantity from UserBundleRegistration br left join br.registrationOrder as o left join o.userRegistration as r left join br.registrationCycle as cycle left join r.user as u ";
    private static final String LIST_ALL_COUNT_QUERY = "select count(*) from UserRegistration o ";
    private static final String LIST_BUNDLE_COUNT_QUERY = "select count(*) from UserBundleRegistration br left join br.registrationOrder as o left join o.userRegistration as r left join br.registrationCycle as cycle ";

    @Override
    public List<RegistrationListItem> listRegistrations(Constraints constraints, Paging paging) {
        Query query;
        List<RegistrationListItem> result = new ArrayList<RegistrationListItem>();
        if (constraints.getFilterTag() != RegistrationsList.Filter_STATE_WAITING) {
            // mame zatim len jeden order a jeden filter
            StringBuilder queryString = new StringBuilder(LIST_REGISTRATIONS_QUERY);

            // vsetky bundle 
            if (showAll(constraints)) {
                queryString.append(" where cycle.bundle.subBundle=false and ");
                setStateFilter(constraints, queryString);
                setRegistrationListOrderers(constraints, queryString);
                query = session().createQuery(queryString.toString());
            } // podla hodnotuy uuid budnle
            else {
                queryString.append(" where cycle.bundle.uuid=:BUNDLE and ");
                setStateFilter(constraints, queryString);
                setRegistrationListOrderers(constraints, queryString);
                query = session().createQuery(queryString.toString());
                query.setString("BUNDLE", constraints.getStringFilterValue());
            }

            if (paging != null) {
                query.setFirstResult(paging.getFirstResult());
                query.setMaxResults(paging.getMaxResults());
            }
            List<Object[]> list = query.list();
            result.addAll(createResultListRegistrations(list));
        }

        if (paging.getMaxResults() > result.size() && (constraints.getFilterTag() == RegistrationsList.Filter_STATE_ALL || constraints.getFilterTag() == RegistrationsList.Filter_STATE_WAITING)) {
            // nacteme lidi ve waitingu
            StringBuilder sb = new StringBuilder("from UserRegistration r left join fetch r.user join fetch r.waitingBundle where r.state=5");

            boolean showByBundle = !showAll(constraints);
            if (showByBundle) {
                sb.append(" and r.waitingBundle.uuid=:BUNDLE");
            }

            query = session().createQuery(sb.toString());
            if (showByBundle) {
                query.setString("BUNDLE", constraints.getStringFilterValue());
            }

            if (constraints.getFilterTag() == RegistrationsList.Filter_STATE_WAITING) {
                if (paging != null) {
                    query.setFirstResult(paging.getFirstResult());
                    query.setMaxResults(paging.getMaxResults());
                }
            } else {
                // TODO: toto je potreba vyresit, strankovani pro vsechny
            }

            List<UserRegistration> urs = query.list();

            List<RegistrationListItem> out = Lists.newArrayList();
            for (UserRegistration ur : urs) {
                RegistrationListItem r = new RegistrationListItem();
                r.setRegistrationUuid(ur.getUuid());
                r.setState(RegistrationState.WAITING);
                r.setBundleName(ur.getWaitingBundle() == null ? "?" : ur.getWaitingBundle().getTitle());
                r.setUserUuid(ur.getUser().getUuid());
                r.setUserName(ur.getUser().getFullName());
                r.setUserPosition(ur.getUser().getPosition());
                r.setUserOrganization(ur.getUser().getOrganization());
                r.setProfileImagePrefix(ur.getUser().getProfileImagePrefix());
                r.setQuantity(ur.getWaitingQuantity());
                out.add(r);
            }

            result.addAll(out);
        }

        return result;
    }

    private List<RegistrationListItem> createResultListRegistrations(List<Object[]> list) {
        List<RegistrationListItem> out = Lists.newArrayList();
        for (Object[] item : list) {
            RegistrationListItem r = new RegistrationListItem();
            r.setRegistrationUuid((String) item[0]);
            r.setRegistrationDate((Date) item[1]);
            r.setState((RegistrationState) item[2]);
            r.setPaid((BigDecimal) item[3]);
            r.setBundleName((String) item[4]);
            r.setUserUuid((String) item[5]);
            r.setUserName((String) item[6]);
            r.setUserPosition((String) item[9]);
            r.setUserOrganization((String) item[7]);
            r.setProfileImagePrefix((String) item[8]);

            Integer q = (Integer) item[10];
            int quantity = 1;
            if (q != null) {
                quantity = q.intValue();
            }
            r.setQuantity(quantity);

            out.add(r);
        }
        return out;
    }

    @Override
    public List<RegistrationListExportItem> listRegistrationsExport(Constraints constraints, Paging paging) {
        StringBuilder queryString = new StringBuilder("select ");
        queryString.append("r.uuid, o.orderDate, r.state, o.overallPrice, cycle.bundle.title, ");
        queryString.append("u.uuid, u.fullName, u.organization, u.profileImagePrefix, u.position, ");
        queryString.append("r.specialWish, cycle.bundle.subBundle, u.celiac, u.vegetarian, u.email ");
        queryString.append("from ");
        queryString.append("UserBundleRegistration br ");
        queryString.append("left join br.registrationOrder as o ");
        queryString.append("left join o.userRegistration as r ");
        queryString.append("left join br.registrationCycle as cycle ");
        queryString.append("left join r.user as u");

        Query query;
        if (showAll(constraints)) {
            // vsetky bundle
            queryString.append(" where ");
            setStateFilter(constraints, queryString);
            setRegistrationListOrderers(constraints, queryString);
            query = session().createQuery(queryString.toString());
        } else {
            // podla hodnoty uuid budnle
            queryString.append(" where cycle.bundle.uuid=:BUNDLE and ");
            setStateFilter(constraints, queryString);
            setRegistrationListOrderers(constraints, queryString);
            query = session().createQuery(queryString.toString());
            query.setString("BUNDLE", constraints.getStringFilterValue());
        }

        if (paging != null) {
            query.setFirstResult(paging.getFirstResult());
            query.setMaxResults(paging.getMaxResults());
        }
        List<Object[]> list = query.list();
        List<RegistrationListExportItem> out = Lists.newArrayList();
        for (Object[] item : list) {
            RegistrationListExportItem r = new RegistrationListExportItem();
            r.setRegistrationUuid((String) item[0]);
            r.setRegistrationDate((Date) item[1]);
            r.setState((RegistrationState) item[2]);
            r.setPaid((BigDecimal) item[3]);
            r.setBundleName((String) item[4]);
            r.setUserUuid((String) item[5]);
            r.setUserName((String) item[6]);
            r.setUserOrganization((String) item[7]);
            r.setProfileImagePrefix((String) item[8]);
            r.setUserPosition((String) item[9]);
            r.setSpecialWish((String) item[10]);
            r.setSubbundle((Boolean) item[11]);
            r.setCeliac((Boolean) item[12]);
            r.setVegetarian((Boolean) item[13]);
            r.setEmail((String) item[14]);
            out.add(r);
        }
        return out;
    }

    @Override
    public Long listRegistrationsCount(Constraints constraints) {
        Query query;
        StringBuilder queryString;
        Long result = 0l;

        if (constraints.getFilterTag() != RegistrationsList.Filter_STATE_WAITING) {
            if (showAll(constraints)) {
                queryString = new StringBuilder(LIST_ALL_COUNT_QUERY);
                queryString.append(" where ");
                setStateFilter(constraints, queryString);
                query = session().createQuery(queryString.toString());
            } else {
                queryString = new StringBuilder(LIST_BUNDLE_COUNT_QUERY);
                queryString.append(" where cycle.bundle.uuid=:BUNDLE and ");
                setStateFilter(constraints, queryString);
                query = session().createQuery(queryString.toString());
                query.setString("BUNDLE", constraints.getStringFilterValue());
            }
            result += (Long) query.list().iterator().next();
        }


        if (constraints.getFilterTag() == RegistrationsList.Filter_STATE_ALL || constraints.getFilterTag() == RegistrationsList.Filter_STATE_WAITING) {
            queryString = new StringBuilder("select count(*) from UserRegistration r where r.state=5");

            boolean showByBundle = !showAll(constraints);
            if (showByBundle) {
                queryString.append(" and r.waitingBundle.uuid=:BUNDLE");
            }

            query = session().createQuery(queryString.toString());
            if (showByBundle) {
                query.setString("BUNDLE", constraints.getStringFilterValue());
            }

            result += (Long) query.list().iterator().next();
        }

        return result;
    }

    private void setStateFilter(Constraints constraints, StringBuilder queryString) {

        if (constraints.getFilterTag() == RegistrationsList.Filter_STATE_PREREGISTERED) {
            queryString.append(" (o.state=0 or o.state=4)");
        } else if (constraints.getFilterTag() == RegistrationsList.Filter_STATE_REGISTERED) {
            queryString.append(" o.state=1 ");
        } else if (constraints.getFilterTag() == RegistrationsList.Filter_STATE_CANCELED) {
            queryString.append(" o.state=2 ");
        } else {
            queryString.append(" ( o.state=0 or o.state=1 or o.state=4)");
        }

    }

    private boolean showAll(Constraints constraints) {
        if (StringUtils.isBlank(constraints.getStringFilterValue()) || constraints.getStringFilterValue().equals(ZERO)) {
            return true;
        } else {
            return false;
        }

    }

    private void setRegistrationListOrderers(Constraints constraints, StringBuilder sb) {
        if (constraints.getOrderers() != null && constraints.getOrderers().length > 0) {
            sb.append(" order by ");
            if (ListUtils.hasOrderer(constraints.getOrderers(), RegistrationsList.Order_LAST_NAME)) {
                sb.append(" u.lastName ");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), RegistrationsList.Order_REGISTRATION_DATE)) {
                sb.append(" o.orderDate desc ");
            } else if (ListUtils.hasOrderer(constraints.getOrderers(), RegistrationsList.Order_REGISTRATION_STATE)) {
                sb.append(" r.state ");
            }
        }
    }
    /* ------------------------------------------------------------------------ */
    /* ---- C H E C K I N L I S T ---- */
    /* ------------------------------------------------------------------------ */
    private static final String CHECK_IN_QUERY = "select r.uuid,u.uuid,u.fullName,u.organization,"
            + "r.checkInDate,r.checked,u.profileImagePrefix,u.position,u.email,r.maxQuantityChecked,r.quantityChecked"
            + " from UserRegistration r left join r.user as u where r.state=1 ";

    @Override
    public List<CheckInListItem> listParticipantsForCheckInList(Constraints constraints, Paging paging) {
        StringBuilder sb = new StringBuilder(CHECK_IN_QUERY);
        if (constraints.getFilterTag() == 1) {
            // all
        } else if (constraints.getFilterTag() == 2) {
            // checked
            sb.append(" and r.quantityChecked>0 ");
        } else if (constraints.getFilterTag() == 3) {
            // not checked
            sb.append(" and r.quantityChecked=0 ");
        }
        sb.append(" order by u.lastName");

        // mame zatim len jeden order a jeden filter
        Query query = session().
                createQuery(sb.toString());
        if (paging != null) {
            query.setFirstResult(paging.getFirstResult());
            query.setMaxResults(paging.getMaxResults());
        }

        List<Object[]> list = query.list();
        List<CheckInListItem> out = Lists.newArrayList();
        for (Object[] item : list) {
            CheckInListItem r = new CheckInListItem();
            r.setRegistrationUuid((String) item[0]);
            r.setUserUuid((String) item[1]);
            r.setUserName((String) item[2]);
            r.setUserPosition((String) item[7]);
            r.setUserOrganization((String) item[3]);
            r.setCheckInDate((Date) item[4]);
            r.setChecked((Boolean) item[5]);
            r.setProfileImagePrefix((String) item[6]);
            r.setEmail((String) item[8]);
            Integer max = (Integer) item[9];
            r.setMaxQuantity(max == null ? 1 : max);
            Integer quantityChecked = (Integer) item[10];
            r.setQuantityChecked(quantityChecked == null ? 0 : quantityChecked);
            out.add(r);
        }
        return out;
    }

    @Override
    public Long loadParticipantsCountForCheckInList(Constraints constraints) {
        return (Long) session().
                createQuery("select count(*) from UserRegistration br where br.state=1 " + (constraints.getFilterTag() == 2 ? " and br.quantityChecked>0" : constraints.getFilterTag() == 3 ? " and br.quantityChecked=0" : "")).
                uniqueResult();
    }

    @Override
    public void deleteAllRegistrations() {
        session().createQuery("delete from UserRegistration").executeUpdate();
    }

    @Override
    public void updateWaitingCancel(String registrationUuid) {
        session().createQuery("delete from UserRegistration r where r.uuid=:REG_UUID and r.state=5").
                setString("REG_UUID", registrationUuid).executeUpdate();
    }

    /* ------------------------------------------------------------------------ */
    /* ---- INTERNAL REGISTRATION LIST ---- */
    /* ------------------------------------------------------------------------ */
    private static final String INTERNAL_REGISTRATION_QUERY = "select r.uuid, cycle.bundle.title, u.uuid, u.fullName, "
            + "u.organization, u.position, br.quantity "
            + "from UserBundleRegistration br left join br.registrationOrder as o "
            + "left join o.userRegistration as r left join br.registrationCycle as cycle left join r.user as u "
            + "where u.internalUser=true and r.state=1 and cycle.bundle.subBundle=false order by u.lastName";

    @Override
    public List<InternalRegistrationListItem> listInternalRegistrations(Constraints constraints, Paging paging) {
        Query query = session().createQuery(INTERNAL_REGISTRATION_QUERY);
        if (paging != null) {
            query.setFirstResult(paging.getFirstResult());
            query.setMaxResults(paging.getMaxResults());
        }
        List<Object[]> list = query.list();
        List<InternalRegistrationListItem> out = Lists.newArrayList();
        for (Object[] item : list) {
            InternalRegistrationListItem r = new InternalRegistrationListItem();
            r.setRegistrationUuid((String) item[0]);
            r.setBundleName((String) item[1]);
            r.setUserUuid((String) item[2]);
            r.setUserName((String) item[3]);
            r.setUserOrganization((String) item[4]);
            r.setUserPosition((String) item[5]);
            r.setQuantity((Integer) item[6]);
            out.add(r);
        }
        return out;
    }

    @Override
    public Long listInternalRegistrationsCount(Constraints constraints) {
        String query = "select count(*) from UserRegistration r left join r.user as u where u.internalUser=true and r.state=1";
        return (Long) session().createQuery(query).uniqueResult();
    }
}
