package consys.event.registration.core.bo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Jedna položka na tiketu
 * @author pepa
 */
public class TicketOrderItem {

    // data
    private Date date;
    private String title;
    private BigDecimal price;
    private int quantity;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }
}
