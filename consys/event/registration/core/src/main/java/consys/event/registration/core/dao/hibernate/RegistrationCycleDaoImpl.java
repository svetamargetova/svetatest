package consys.event.registration.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.date.DateProvider;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.dao.RegistrationCycleDao;
import consys.event.registration.core.exception.NoActiveCycleForBundleException;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCycleDaoImpl extends GenericDaoImpl<RegistrationCycle> implements RegistrationCycleDao {

    @Override
    public RegistrationCycle load(Long id) throws NoRecordException {
        return load(id, RegistrationCycle.class);
    }

    @Override
    public RegistrationCycle loadRegistrationCycleByUuid(String uuid) throws NoRecordException {
        return (RegistrationCycle) session().
                createQuery("from RegistrationCycle rc where rc.uuid=:CYCLE_UUID").
                setString("CYCLE_UUID", uuid).
                uniqueResult();
    }

    @Override
    public RegistrationCycle loadActiveCycleForBundle(String bundleUuid) throws NoActiveCycleForBundleException {
        Date date = DateProvider.getCurrentDate();
        log.debug("loadActiveCycleForBundle: uuid->" + bundleUuid + ", date->" + date);
        RegistrationCycle cycle = (RegistrationCycle) session().
                createQuery("select c from RegistrationCycle c JOIN FETCH c.bundle where c.bundle.uuid=:BUNDLE_UUID and c.from <= :TODAY and c.to >= :TODAY").
                setString("BUNDLE_UUID", bundleUuid).
                setTimestamp("TODAY", date).
                uniqueResult();
        if (cycle == null) {
            throw new NoActiveCycleForBundleException();
        }
        return cycle;
    }

    @Override
    public List<RegistrationCycle> listCyclesAfter(String bundleUuid) throws NoRecordException {
        List<RegistrationCycle> cycles = session().
                createQuery("from RegistrationCycle rc where rc.bundle.uuid=:BUUID order by rc.from").
                setString("BUUID", bundleUuid).
                list();
        if (cycles == null || cycles.isEmpty()) {
            throw new NoRecordException();
        }
        return cycles;
    }

    @Override
    public Date loadFirstRegistration() {
        return (Date) session().createQuery("select min(rc.from) from RegistrationCycle rc").uniqueResult();
    }

    @Override
    public Date loadLastRegistration() {
        return (Date) session().createQuery("select max(rc.to) from RegistrationCycle rc").uniqueResult();
    }

    @Override
    public void deleteAllCycles() {
        // driv existovala tabulka registration_user s referenci na registrationCycle, tak ji preventivne odstranime
        session().createSQLQuery("drop table if exists registration.registration_user").executeUpdate();
        session().createQuery("delete from RegistrationCycle").executeUpdate();
    }
}
