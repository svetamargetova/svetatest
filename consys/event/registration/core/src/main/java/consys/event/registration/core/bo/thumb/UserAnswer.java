package consys.event.registration.core.bo.thumb;

/**
 *
 * @author pepa
 */
public class UserAnswer implements Comparable<UserAnswer> {

    private int order;
    private String answer;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public int compareTo(UserAnswer o) {
        return this.order - o.getOrder();
    }
}
