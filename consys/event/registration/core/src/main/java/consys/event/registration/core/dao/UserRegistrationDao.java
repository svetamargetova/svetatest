package consys.event.registration.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.*;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface UserRegistrationDao extends GenericDao<UserRegistration> {

    /**
     * Zisti kolko je uz registracii vytvorenych pre dany Bundle. Pocita aj
     * neaktivne registracie.
     *
     * @param bundleUuid uuid bundlu
     * @return pocet registracii pre dany bundle
     */
    public Long loadRegistrationCountForBundle(String bundleUuid);

    /**
     * Zisti kolko je uz registracii vytv orenych pre dany cyklus. Pocita aj 
     * neaktivne registracie
     *
     *
     * @param cycleUuid uuid cyklu
     * @return pocet registracii pre dany bundle
     */
    public Long loadRegistrationCountForCycle(String cycleUuid);

    /**
     * Na zaklade toho ci ma uzivatel uz nejaku registraciu v stave validna vrati
     * bud true alebo false
     * @param userUuid uuid usera
     * @return true ak ma uz uzivatel spravenu registraciu
     */
    public boolean isUserAlreadyRegistred(String userUuid);

    public UserRegistration loadUserRegistrationDetailedByRegistrationUuid(String registrationUuid) throws NoRecordException;

    public UserRegistration loadUserRegistrationDetailedByUserUuid(String userUuid) throws NoRecordException;

    public UserRegistration loadRegistrationToPay(String registrationUuid) throws NoRecordException;

    public UserRegistration loadRegistrationByUuid(String uuid) throws NoRecordException;

    UserRegistration loadWaitingRegistration(String registrationUuid) throws NoRecordException;

    public List<UserEvent> listAllActiveParticiapntsForBadges() throws NoRecordException;

    List<UserRegistration> listManagedRegistrations(String uuidManager);

    /*------------------------------------------------------------------------*/
    /* ----  P A R T I C I P A N T     L I S T  ---- */
    /*------------------------------------------------------------------------*/
    public List<ParticipantListItem> listParticipants(Constraints constraints, Paging paging);

    public Long loadParticipantsCount(Constraints constraints);


    /*------------------------------------------------------------------------*/
    /* ----  R E G I S T R A T I O N     L I S T  ---- */
    /*------------------------------------------------------------------------*/
    public List<RegistrationListItem> listRegistrations(Constraints constraints, Paging paging);

    public List<RegistrationListExportItem> listRegistrationsExport(Constraints constraints, Paging paging);

    public Long listRegistrationsCount(Constraints constraints);
    
    /*------------------------------------------------------------------------*/
    /* ---- I N T E R N A L    R E G I S T R A T I O N     L I S T  ---- */
    /*------------------------------------------------------------------------*/
    public List<InternalRegistrationListItem> listInternalRegistrations(Constraints constraints, Paging paging);

    public Long listInternalRegistrationsCount(Constraints constraints);

    /*------------------------------------------------------------------------*/
    /* ----  C H E C K   I N   L I S T  ---- */
    /*------------------------------------------------------------------------*/
    public List<CheckInListItem> listParticipantsForCheckInList(Constraints constraints, Paging paging);

    public Long loadParticipantsCountForCheckInList(Constraints constraints);

    /** smaze vsechny registrace */
    void deleteAllRegistrations();

    /** zrusi waiting registraci */
    void updateWaitingCancel(String registrationUuid);
}
