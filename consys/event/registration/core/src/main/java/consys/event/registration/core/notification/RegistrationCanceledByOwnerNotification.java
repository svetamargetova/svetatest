package consys.event.registration.core.notification;

import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.event.registration.core.bo.RegistrationOrder;
import java.text.SimpleDateFormat;
import net.notx.client.PlaceHolders;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCanceledByOwnerNotification extends AbstractRegistrationNotification{    

    private final RegistrationOrder registrationOrder;

    public RegistrationCanceledByOwnerNotification(RegistrationOrder registrationOrder, SystemPropertyService systemPropertyService) {
        super(systemPropertyService, SystemMessage.REGISTRATION_CANCELED_BY_OWNER, registrationOrder.getUserRegistration().getUser().getUuid());
        this.registrationOrder = registrationOrder;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
         SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        // vsetko defaultne placeholdere
        placeHolders.addPlaceHolder("registration_date", sdf.format(registrationOrder.getOrderDate()));
    }


    

}
