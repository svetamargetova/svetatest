package consys.event.registration.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class BundleNotUniqueException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public BundleNotUniqueException() {
        super();
    }

     public BundleNotUniqueException(String message) {
	super(message);
    }

}
