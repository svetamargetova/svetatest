package consys.event.registration.core;

/**
 *
 * @author palo
 */
public interface SystemNativeProperties {

    /**
     * Konstanta ktora definuje ako dlho moze byt objednavka aktivna. Toto by malo 
     * byt v databazi pre kazdy event osobitne, pripadne pre kazdu objednavku osobitne.
     */
    public static final int DUE_DATE = 21;
}
