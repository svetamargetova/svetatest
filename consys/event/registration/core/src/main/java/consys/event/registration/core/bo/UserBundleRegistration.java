package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;
import java.math.BigDecimal;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserBundleRegistration implements ConsysObject {

    private static final long serialVersionUID = 2187843102246497722L;
    // data
    private Long id;
    private String uuid;
    /** Finalna cena balicku so zapocitanim zlavy, resp. nastavenim */
    private BigDecimal bundleFinalPrice;
    /** Cyklus */
    private RegistrationCycle registrationCycle;
    /** Zlavovy kupon */
    private RegistrationCoupon registrationCoupon;
    /** Objednavka na ktorej sa nachadza budne registracia */
    private RegistrationOrder registrationOrder;
    /** Kvantita */
    private Integer quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        UserBundleRegistration e = (UserBundleRegistration) obj;
        return uuid.equals(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    public BigDecimal getBundleFinalPrice() {
        return bundleFinalPrice;
    }

    public void setBundleFinalPrice(BigDecimal bundleFinalPrice) {
        this.bundleFinalPrice = bundleFinalPrice;
    }

    public RegistrationCycle getRegistrationCycle() {
        return registrationCycle;
    }

    public void setRegistrationCycle(RegistrationCycle registrationCycle) {
        this.registrationCycle = registrationCycle;
    }

    public RegistrationCoupon getRegistrationCoupon() {
        return registrationCoupon;
    }

    public void setRegistrationCoupon(RegistrationCoupon registrationCoupon) {
        this.registrationCoupon = registrationCoupon;
    }

    public RegistrationOrder getRegistrationOrder() {
        return registrationOrder;
    }

    public void setRegistrationOrder(RegistrationOrder registrationOrder) {
        this.registrationOrder = registrationOrder;
    }

    public Integer getQuantity() {
        if (quantity == null || quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        if (quantity == null || quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }
}
