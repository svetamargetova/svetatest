package consys.event.registration.core.service.impl;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.AlreadyUsedException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.collection.Lists;
import consys.common.utils.UuidProvider;
import consys.common.utils.date.DateRange;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.dao.RegistrationBundleDao;
import consys.event.registration.core.dao.RegistrationCycleDao;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.service.SystemPropertyService;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.context.PropertiesChangedEvent;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.event.registration.core.bo.thumb.RegistrationCouponStats;
import consys.event.registration.core.dao.RegistrationCouponDao;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.event.registration.core.exception.CycleNotEmptyException;
import consys.event.registration.core.exception.CyclesOverlapsException;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.payment.webservice.ProductWebService;
import consys.payment.ws.bo.Product;
import consys.payment.ws.bo.ProductCategory;
import consys.payment.ws.product.CreateEventProductsRequest;
import consys.payment.ws.product.CreateEventProductsResponse;
import consys.payment.ws.product.DeleteEventProductsRequest;
import consys.payment.ws.product.DeleteEventProductsResponse;
import consys.payment.ws.product.UpdateEventProductRequest;
import consys.payment.ws.product.UpdateEventProductsRequest;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationBundleServiceImpl extends AbstractService implements RegistrationBundleService, ApplicationEventPublisherAware {

    private RegistrationCouponDao couponDao;
    private UserRegistrationDao userRegistrationDao;
    private RegistrationBundleDao registrationBundleDao;
    private RegistrationCycleDao cycleDao;
    private ProductWebService productWebService;
    private SystemPropertyService systemPropertyService;
    // publisher
    private ApplicationEventPublisher publisher;

    /*
     * ------------------------------------------------------------------------
     * ---- C R E A T E ----
     * ------------------------------------------------------------------------
     */
    @Override
    public void createBundle(RegistrationBundle bundle) throws BundleNotUniqueException, RequiredPropertyNullException {
        if (StringUtils.isBlank(bundle.getTitle())) {
            throw new RequiredPropertyNullException();
        }
        try {
            bundle.setUuid(UuidProvider.getUuid());
            Long order = registrationBundleDao.loadBundlesCount(false);
            bundle.setOrder(order.intValue() + 1);
            if (bundle.getVat() == null) {
                bundle.setVat(loadDefaultVat());
            }
            log().debug("+ {}", bundle);
            registrationBundleDao.create(bundle, true);
            processBundleCyclesToPaymentPortal(bundle);
        } catch (ConstraintViolationException dive) {
            log().warn("Bundle title uniquity exception");
            throw new BundleNotUniqueException();
        }
    }

    private BigDecimal loadDefaultVat() throws NumberFormatException {
        // nacitame defaultnu vat
        BigDecimal defaultVat = null;
        try {
            SystemProperty sp = systemPropertyService.loadSystemProperty(CommonProperties.EVENT_VAT_RATE);
            if (StringUtils.isNotBlank(sp.getValue())) {
                defaultVat = BigDecimal.valueOf(Double.parseDouble(sp.getValue()));
            } else {
                defaultVat = BigDecimal.ZERO;
            }
        } catch (NoRecordException ex) {
            defaultVat = BigDecimal.ZERO;
        }
        return defaultVat;
    }

    private void processBundleCyclesToPaymentPortal(RegistrationBundle bundle) {
        if (CollectionUtils.isNotEmpty(bundle.getCycles())) {
            CreateEventProductsRequest req = ProductWebService.OBJECT_FACTORY.createCreateEventProductsRequest();
            for (RegistrationCycle cycle : bundle.getCycles()) {
                Product product = createPaymentPortalProduct(cycle);
                log().debug("Adding cycle to payment-portal request {}", product.toString());
                req.getProducts().add(product);
            }
            CreateEventProductsResponse response = productWebService.createProducts(req);
        }
    }

    @Override
    public void createBundles(List<RegistrationBundle> bundles) throws BundleNotUniqueException, RequiredPropertyNullException {
        createBundles(bundles, false);
    }

    @Override
    public void createBundles(List<RegistrationBundle> bundles, boolean subbundle)
            throws BundleNotUniqueException, RequiredPropertyNullException {
        try {
            int order = registrationBundleDao.loadBundlesCount(subbundle).intValue();
            BigDecimal defaultVat = loadDefaultVat();
            for (RegistrationBundle bundle : bundles) {
                if (StringUtils.isBlank(bundle.getTitle())) {
                    throw new RequiredPropertyNullException();
                }
                if (bundle.getVat() == null) {
                    bundle.setVat(defaultVat);
                }
                bundle.setUuid(UuidProvider.getUuid());
                bundle.setOrder(++order);
                bundle.setSubBundle(subbundle);
                registrationBundleDao.create(bundle);
                processBundleCyclesToPaymentPortal(bundle);
            }
        } catch (ConstraintViolationException dive) {
            log().warn("Bundles title uniquity exception");
            throw new BundleNotUniqueException();
        }

    }

    @Override
    public void createRegistrationCycles(String bundleUuid, List<RegistrationCycle> newCycles)
            throws CyclesOverlapsException, ServiceExecutionFailed, RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid)) {
            throw new RequiredPropertyNullException();
        }
        try {
            RegistrationBundle bundle = registrationBundleDao.loadBundleWithCyclesByUuid(bundleUuid);

            List<DateRange> checkedCycles = Lists.newArrayList();
            for (RegistrationCycle c : bundle.getCycles()) {
                checkedCycles.add(new DateRange(c.getFrom(), c.getTo()));
            }

            // Validacia toho ze nove cykly nezasahuju do uz otestovanych
            for (RegistrationCycle c : newCycles) {
                if (c.getFrom() == null && c.getTo() == null) {
                    throw new RequiredPropertyNullException();
                }
                DateRange newRange = new DateRange(c.getFrom(), c.getTo());
                for (DateRange checked : checkedCycles) {
                    if (checked.intersects(newRange, true, false)) {

                        log().debug("Cycles overlap exception: {} overlaps {}", newRange, checked);

                        throw new CyclesOverlapsException();
                    }
                }
                // Tento novy cyklus je validny tak ho pridame do valinych a nastavime
                checkedCycles.add(newRange);
                c.setBundle(bundle);
                c.setUuid(UuidProvider.getUuid());
                if (log().isDebugEnabled()) {
                    log().debug("Add " + c);
                }
            }

            //podla seckeho je to validne takze len pridame nove cykly a zosortujeme ich
            CreateEventProductsRequest req = ProductWebService.OBJECT_FACTORY.createCreateEventProductsRequest();
            for (RegistrationCycle newCycle : newCycles) {
                bundle.getCycles().add(newCycle);

                Product product = createPaymentPortalProduct(newCycle);

                log().debug("Adding cycle to payment-portal request {}", product.toString());

                req.getProducts().add(product);
            }

            // Zesortujeme
            Collections.sort(bundle.getCycles());
            registrationBundleDao.update(bundle);
            // vytvorime produkty            
            productWebService.createProducts(req);


        } catch (NoRecordException ex) {
            throw new ServiceExecutionFailed();
        }

        notifyAboutChangedEventProperties();
    }

    @Override
    public RegistrationBundle createDuplicate(String bundleUuid, String bundleTitle) throws BundleNotUniqueException, NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid) || StringUtils.isBlank(bundleTitle)) {
            throw new RequiredPropertyNullException();
        }
        // nasjkur nacitame bundle z ktereho ideme spravit kopiu
        RegistrationBundle bundle = registrationBundleDao.loadBundleWithCyclesByUuid(bundleUuid);

        log().debug("Duplicating Bundle {}", bundle);

        RegistrationBundle newBundle = new RegistrationBundle();
        newBundle.setTitle(bundleTitle);
        newBundle.setSubBundle(bundle.isSubBundle());
        newBundle.setCapacity(bundle.getCapacity());
        newBundle.setDescription(bundle.getDescription());
        newBundle.setB2b(bundle.isB2b());
        // Neprenastavujeme , lebo nema priradene ziadne kody
        // newBundle.setWithCode(bundle.isWithCode());
        newBundle.setVat(bundle.getVat());
        for (RegistrationCycle cycle : bundle.getCycles()) {
            RegistrationCycle newCycle = new RegistrationCycle();
            newCycle.setBundle(newBundle);
            newCycle.setFrom(cycle.getFrom());
            newCycle.setPrice(cycle.getPrice());
            newCycle.setTo(cycle.getTo());
            newCycle.setUuid(UuidProvider.getUuid());
            newBundle.getCycles().add(newCycle);
        }
        createBundle(newBundle);

        notifyAboutChangedEventProperties();

        return newBundle;
    }

    @Override
    public void createRegistrationCoupon(String bundleUuid, RegistrationCoupon coupon)
            throws NoRecordException, CouponCodeException, RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid)) {
            throw new RequiredPropertyNullException("Missing bundle uuid");
        }
        RegistrationBundle b = loadRegistrationBundle(bundleUuid);
        createRegistrationCoupon(b, coupon);
    }

    @Override
    public void createRegistrationCoupon(RegistrationBundle bundle, RegistrationCoupon coupon)
            throws NoRecordException, CouponCodeException, RequiredPropertyNullException {
        if (bundle == null) {
            throw new RequiredPropertyNullException("Missing bundle");
        }

        if (coupon == null || StringUtils.isBlank(coupon.getKey()) || coupon.getDiscount() <= 0) {
            throw new RequiredPropertyNullException("Missing coupn or coupon key or discount is negative/zero");
        }

        if (getCouponDao().existsCouponWithKey(coupon.getKey(), bundle.getUuid())) {
            throw new CouponCodeException("Coupon with key already exists!");
        }
        coupon.setUuid(UuidProvider.getUuid());
        coupon.setBundle(bundle);
        couponDao.create(coupon);
        // ak nema bundle nastavene ze ma kupon , potom nastavime priznak
        if (!bundle.isWithCode()) {
            bundle.setWithCode(true);
            registrationBundleDao.update(bundle);
        }
    }

    @Override
    public String generateRegistrationCouponKey(String bundleUuid)
            throws RequiredPropertyNullException {

        boolean valid;
        String key = null;
        final int treshold = 20;
        for (int tries = 0; tries < treshold; tries++) {
            key = RandomStringUtils.random(10, true, true);
            valid = isValidRegistrationCouponKey(bundleUuid, key);
            if (valid) {
                break;
            }
        }
        return key;
    }

    @Override
    public boolean isValidRegistrationCouponKey(String bundleUuid, String key)
            throws RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid) || StringUtils.isBlank(key)) {
            throw new RequiredPropertyNullException("Missing budnle or key");
        }
        return !couponDao.existsCouponWithKey(key, bundleUuid);

    }


    /*
     * ------------------------------------------------------------------------
     * ---- U P D A T E ----
     * ------------------------------------------------------------------------
     */
    /**
     * Update Bundlu.
     */
    @Override
    public void updateRegistrationBundle(RegistrationBundle bundle) throws BundleNotUniqueException, BundleCapacityFullException {
        int activeRegistrations = bundle.getRegistred();
        if (bundle.hasCapacity() && bundle.getCapacity() < activeRegistrations) {            
            log().debug("Update bundle failed! Cannot set lower capacity than active registred");            
            throw new BundleCapacityFullException(bundle.getUuid());
        }
        try {
            registrationBundleDao.update(bundle);

            // Upravime vsetky cykly v payment portalu - zmena nazvu
            UpdateEventProductsRequest req = ProductWebService.OBJECT_FACTORY.createUpdateEventProductsRequest();
            for (RegistrationCycle cycle : bundle.getCycles()) {
                req.getProducts().add(createPaymentPortalProduct(cycle));
            }
            productWebService.updateProducts(req);

            // aktualizujeme prava            
            registrationBundleDao.updateUserB2bAgentRightsForBundle(bundle.isB2b(), bundle.getId());
            log().debug("Updated to: {} {} B2B Agent rights",bundle.isB2b(),bundle.getId());                        
            
        } catch (ConstraintViolationException dive) {
            log().warn("Update bundle failed! Bundle title uniquity exception!");
            throw new BundleNotUniqueException();
        }
    }
       
    /**
     * @param cycleReadOnly neni v hibernate session takze ho len pouzivame na
     * prebrani informacii
     */
    @Override
    public void updateRegistrationCycle(RegistrationCycle cycleReadOnly) throws CyclesOverlapsException, NoRecordException {
        RegistrationBundle bundle = registrationBundleDao.loadBundleWithCyclesByCycleUuid(cycleReadOnly.getUuid());
        DateRange cycleRange = new DateRange(cycleReadOnly.getFrom(), cycleReadOnly.getTo());
        RegistrationCycle updateCycle = null;

        boolean updateProductInPaymentPortal = false;

        for (RegistrationCycle acycle : bundle.getCycles()) {
            if (!acycle.equals(cycleReadOnly)) {
                DateRange range = new DateRange(acycle.getFrom(), acycle.getTo());
                if (range.intersects(cycleRange, true, false)) {
                    log().debug("Update cycle failed! New Cycle settings overlaps other cycle.");
                    throw new CyclesOverlapsException();
                }
            } else {
                // presetujeme hodnoty
                updateCycle = acycle;
                updateCycle.setFrom(cycleReadOnly.getFrom());
                updateCycle.setTo(cycleReadOnly.getTo());
                if (updateCycle.getPrice().compareTo(cycleReadOnly.getPrice()) != 0) {
                    updateProductInPaymentPortal = true;
                }
                updateCycle.setPrice(cycleReadOnly.getPrice());
            }
        }
        log().debug("Updating {}", updateCycle);
        cycleDao.update(updateCycle);

        // Update registracie ako produktu v payment portalu ak je to potreba
        // tj zmenila sa cena produktu
        if (updateProductInPaymentPortal) {
            UpdateEventProductRequest req = ProductWebService.OBJECT_FACTORY.createUpdateEventProductRequest();
            req.setProduct(createPaymentPortalProduct(updateCycle));
            productWebService.updateProduct(req);
        }

        notifyAboutChangedEventProperties();
    }

    private Product createPaymentPortalProduct(RegistrationCycle cycle) {
        Product product = new Product();
        product.setCategory(ProductCategory.TICKET);
        product.setName(cycle.getBundle().getTitle());
        product.setUnitPrice(cycle.getPrice());
        product.setUuid(cycle.getUuid());
        product.setVat(cycle.getBundle().getVat());
        return product;
    }

    @Override
    public void updateRegistrationBundleOrder(String bundleUuid, int oldPosition, int newPosition, boolean subbundle)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid) || oldPosition < 1 || newPosition < 1) {
            throw new RequiredPropertyNullException("Missing bundle uuid or positions are negative");
        }
        // test na to ci naozaj ma dany bundle taky order        
        log().debug("Update bundle position: {} from: {} -> {}", new Object[]{bundleUuid, oldPosition, newPosition});

        if (registrationBundleDao.checkBundlePosition(bundleUuid, oldPosition)) {
            registrationBundleDao.updateBundlePosition(bundleUuid, oldPosition, newPosition, subbundle);
        } else {
            throw new NoRecordException("Bundle with uuid " + bundleUuid + " has not given order " + oldPosition);
        }
    }

    @Override
    public void updateRegistrationCoupon(String couponUuid, int capacity, Date endDate) throws NoRecordException, RequiredPropertyNullException {
        if (couponUuid == null || couponUuid.isEmpty() || capacity < 0) {
            throw new RequiredPropertyNullException("Missing couponUuid or capacity is negative");
        }

        RegistrationCoupon coupon = couponDao.loadByUuid(couponUuid);
        coupon.setCapacity(capacity);
        coupon.setEndDate(endDate);

        couponDao.update(coupon);
    }

    /*
     * ------------------------------------------------------------------------
     * ---- D E L E T E ----
     * ------------------------------------------------------------------------
     */
    @Override
    public void deleteBundle(String bundleUuid) throws CycleNotEmptyException, NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid)) {
            throw new RequiredPropertyNullException();
        }
        // najskor zistime ci sa uz nekdo nezaregistroval
        Long registred = userRegistrationDao.loadRegistrationCountForBundle(bundleUuid);
        if (registred > 0) {
            log().debug("Delete Bundle with {} registrations!! Throwing CycleNotEmptyException!", registred);
            throw new CycleNotEmptyException();
        }

        // inac mazeme




        RegistrationBundle bundle = registrationBundleDao.loadBundleWithCyclesByUuid(bundleUuid);
        log().debug("Deleting {}", bundle);

        DeleteEventProductsRequest request = ProductWebService.OBJECT_FACTORY.createDeleteEventProductsRequest();
        for (RegistrationCycle cycle : bundle.getCycles()) {
            request.getProducts().add(cycle.getUuid());
        }
        registrationBundleDao.delete(bundle);

        if (!request.getProducts().isEmpty()) {
            log().debug("Deleting products from Payment-Portal ");
            DeleteEventProductsResponse response = productWebService.deleteEventProducts(request);
            log().debug("Deleted {} products from {}", response.getDeletedCount(), request.getProducts().size());
        }

    }

    @Override
    public void deleteCycle(String cycleUuid) throws CycleNotEmptyException, NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(cycleUuid)) {
            throw new RequiredPropertyNullException();
        }
        // najskor zistime ci sa uz nekdo nezaregistroval
        Long registred = userRegistrationDao.loadRegistrationCountForCycle(cycleUuid);
        if (registred > 0) {
            log().debug("Delete Cycle with {} registrations!! Throwing CycleNotEmptyException!", registred);
            throw new CycleNotEmptyException();
        }

        // inac mazeme
        RegistrationBundle bundle = registrationBundleDao.loadBundleWithCyclesByCycleUuid(cycleUuid);
        RegistrationCycle cycleToDelete = null;
        for (RegistrationCycle cycle : bundle.getCycles()) {
            if (cycle.getUuid().equalsIgnoreCase(cycleUuid)) {
                cycleToDelete = cycle;
                break;
            }
        }

        if (cycleToDelete == null) {
            throw new NoRecordException();
        }


        log().debug("Deleting {}", cycleToDelete);


        // pripravime si request do portalu
        DeleteEventProductsRequest request = ProductWebService.OBJECT_FACTORY.createDeleteEventProductsRequest();
        request.getProducts().add(cycleToDelete.getUuid());

        // zmazeme z db
        bundle.getCycles().remove(cycleToDelete);
        registrationBundleDao.update(bundle);

        // zmazeme z portalu
        DeleteEventProductsResponse response = productWebService.deleteEventProducts(request);
        log().debug("Deleted cycle as product from payment portal {}", response.getDeletedCount());
        
        notifyAboutChangedEventProperties();
    }

    @Override
    public void deleteRegistrationCoupon(String couponUuid) throws NoRecordException, RequiredPropertyNullException, AlreadyUsedException {
        if (couponUuid == null || couponUuid.isEmpty()) {
            throw new RequiredPropertyNullException("Missing couponUuid");
        }

        RegistrationCoupon coupon = couponDao.loadByUuid(couponUuid);
        if (coupon.getUsedCount() > 0) {
            throw new AlreadyUsedException("Coupon already used " + coupon.getUsedCount() + "x");
        }
        couponDao.delete(coupon);
    }

    /*
     * ------------------------------------------------------------------------
     * ---- L I S T ----
     * ------------------------------------------------------------------------
     */
    @Override
    public List<RegistrationBundle> listRegistrationMainBundles() throws NoRecordException {        
        log().debug("List registration bundle");        
        return getRegistrationBundleDao().listMainBundles();
    }

    @Override
    public List<RegistrationBundle> listRegistrationMainBundlesWithCycles() throws NoRecordException {        
        log().debug("List main registration bundles");        
        return getRegistrationBundleDao().listMainBundlesWithCycles();
    }

    @Override
    public List<RegistrationBundle> listRegistrationSubBundles() throws NoRecordException {        
        log().debug("List registration subbundles");        
        return getRegistrationBundleDao().listSubBundles();
    }

    @Override
    public List<RegistrationBundle> listRegistrationSubBundlesWithCycles() throws NoRecordException {        
        log().debug("List registration sub bundles");        
        return getRegistrationBundleDao().listSubBundlesWithCycles();
    }

    @Override
    public List<RegistrationOption> listMainRegistrationBundlesOptions() throws NoRecordException {        
        log().debug("List main registration options");        
        return getRegistrationBundleDao().listRegistrationOptions(false);
    }

    @Override
    public List<RegistrationOption> listRegistrationSubBundlesOptionsForUser(String userUuid) throws NoRecordException {        
        log().debug("List subundle registration options - USER_UUID not reflected");        
        return getRegistrationBundleDao().listRegistrationOptions(true);
    }

    @Override
    public List<RegistrationOption> listRegistrationSubBundlesOptions(String bundleUuid) throws NoRecordException {        
        log().debug("List subundle registration options - BUNDLE_UUID not reflected");        
        return getRegistrationBundleDao().listRegistrationOptions(true);
    }

    @Override
    public List<RegistrationOption> listRegistrationSubBundlesWithMainBundle(String bundleUuid) throws NoRecordException {        
        log().debug("List subundle registration options  with for main bundle: {}",bundleUuid);                        
        List<RegistrationOption> options = getRegistrationBundleDao().listSubbundlesWithMainBundle(bundleUuid);                        
        return options;
    }

    @Override
    public List<RegistrationCycle> listNextCyclesAfter(String bundleUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid)) {
            throw new RequiredPropertyNullException();
        }
        return getCycleDao().listCyclesAfter(bundleUuid);
    }

    @Override
    public List<RegistrationBundle> listAllBundles() throws NoRecordException {
        return registrationBundleDao.listAllBundles();
    }

    @Override
    public RegistrationCouponStats loadRegistrationCouponsStats(String bundleUuid) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(bundleUuid)) {
            throw new RequiredPropertyNullException();
        }
        return couponDao.loadCouponsStats(bundleUuid);
    }

    /*
     * ------------------------------------------------------------------------     
     * ---- L O A D ----     
     * ------------------------------------------------------------------------
     */
    @Override
    public RegistrationBundle loadById(Long id) throws NoRecordException, RequiredPropertyNullException {
        if (id == null || id <= 0) {
            throw new RequiredPropertyNullException();
        }
        return registrationBundleDao.load(id);
    }

    @Override
    public RegistrationCycle loadRegistrationCycle(String cycleUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(cycleUuid)) {
            throw new RequiredPropertyNullException();
        }
        return cycleDao.loadRegistrationCycleByUuid(cycleUuid);
    }

    @Override
    public RegistrationBundle loadRegistrationBundle(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return registrationBundleDao.loadBundleByUuid(uuid);
    }

    @Override
    public RegistrationBundle loadRegistrationBundleWithCycles(String uuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(uuid)) {
            throw new RequiredPropertyNullException();
        }
        return registrationBundleDao.loadBundleWithCyclesByUuid(uuid);
    }

    @Override
    public Integer loadBundleRegistrationCount(Long bundleId) {
        return registrationBundleDao.loadBundleRegistrationsCount(bundleId);
    }

    /*
     * ========================================================================
     */
    /**
     * @return the productWebService
     */
    public ProductWebService getProductWebService() {
        return productWebService;
    }

    /**
     * @param productWebService the productWebService to set
     */
    public void setProductWebService(ProductWebService productWebService) {
        this.productWebService = productWebService;

    }

    /**
     * @return the registrationBundleDao
     */
    public RegistrationBundleDao getRegistrationBundleDao() {
        return registrationBundleDao;
    }

    /**
     * @param registrationBundleDao the registrationBundleDao to set
     */
    public void setRegistrationBundleDao(RegistrationBundleDao registrationBundleDao) {
        this.registrationBundleDao = registrationBundleDao;
    }

    /**
     * @return the userRegistrationDao
     */
    public UserRegistrationDao getUserRegistrationDao() {
        return userRegistrationDao;
    }

    /**
     * @param userRegistrationDao the userRegistrationDao to set
     */
    public void setUserRegistrationDao(UserRegistrationDao userRegistrationDao) {
        this.userRegistrationDao = userRegistrationDao;
    }

    /**
     * @return the cycleDao
     */
    public RegistrationCycleDao getCycleDao() {
        return cycleDao;
    }

    /**
     * @param cycleDao the cycleDao to set
     */
    public void setCycleDao(RegistrationCycleDao cycleDao) {
        this.cycleDao = cycleDao;
    }

    /**
     * @return the systemPropertyService
     */
    public SystemPropertyService getSystemPropertyService() {
        return systemPropertyService;
    }

    /**
     * @param systemPropertyService the systemPropertyService to set
     */
    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    /**
     * @return the couponDao
     */
    public RegistrationCouponDao getCouponDao() {
        return couponDao;
    }

    /**
     * @param couponDao the couponDao to set
     */
    public void setCouponDao(RegistrationCouponDao couponDao) {
        this.couponDao = couponDao;
    }

    @Override
    public Date loadFirstRegistrationBegin() {
        return cycleDao.loadFirstRegistration();
    }

    @Override
    public Date loadLastRegistrationEnd() {
        return cycleDao.loadLastRegistration();
    }

    private void notifyAboutChangedEventProperties() {
        publisher.publishEvent(new PropertiesChangedEvent(this));
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher aep) {
        this.publisher = aep;
    }
}
