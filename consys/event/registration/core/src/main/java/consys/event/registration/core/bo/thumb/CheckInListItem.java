package consys.event.registration.core.bo.thumb;

import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CheckInListItem extends UserThumbListItem {

    private String registrationUuid;
    private Date checkInDate;
    private boolean checked;
    private String email;
    private int maxQuantity;
    private int quantityChecked;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistrationUuid() {
        return registrationUuid;
    }

    public void setRegistrationUuid(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(int maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public int getQuantityChecked() {
        return quantityChecked;
    }

    public void setQuantityChecked(int quantityChecked) {
        this.quantityChecked = quantityChecked;
    }
}
