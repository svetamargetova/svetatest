package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Sets;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationBundle implements ConsysObject {

    private static final long serialVersionUID = 5684875794872919792L;
    public static final int INFINITE_CAPACITY = 0;
    private Long id;
    private String title;
    private String description;
    private boolean withCode = false;
    private boolean subBundle = false;
    private int capacity;
    /** Cislo vsetkych registrovanych */
    private int registred;
    private int order = 1;
    /** Dan daneho baliku */
    private BigDecimal vat;
    private String uuid;
    private List<RegistrationCycle> cycles;
    private Set<RegistrationCoupon> coupons;
    /** Priznak ci tento bundle predstavuej zaroven aj vstup do B2B Matchmaking */
    private boolean b2b;
    private Integer quantity;

    public RegistrationBundle() {
        capacity = INFINITE_CAPACITY;
        cycles = Lists.newArrayList();
        coupons = Sets.newHashSet();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the withCode
     */
    public boolean isWithCode() {
        return withCode;
    }

    /**
     * @param withCode the withCode to set
     */
    public void setWithCode(boolean withCode) {
        this.withCode = withCode;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the cycles
     */
    public List<RegistrationCycle> getCycles() {
        return cycles;
    }

    /**
     * @param cycles the cycles to set
     */
    public void setCycles(List<RegistrationCycle> cycles) {
        this.cycles = cycles;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        RegistrationBundle e = (RegistrationBundle) obj;
        return title.equals(e.title);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (this.title != null ? this.title.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("RegistrationBundle[title=%s code=%s capacity=%d]", title, withCode, capacity);
    }

    public boolean hasCapacity() {
        return capacity > INFINITE_CAPACITY;
    }

    /**
     * @return the order
     */
    public int getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the registred
     */
    public int getRegistred() {
        return registred;
    }

    /**
     * @param registred the registred to set
     */
    public void setRegistred(int registred) {
        this.registred = registred;
    }

    /**
     * @return the coupons
     */
    public Set<RegistrationCoupon> getCoupons() {
        return coupons;
    }

    /**
     * @param coupons the coupons to set
     */
    public void setCoupons(Set<RegistrationCoupon> coupons) {
        this.coupons = coupons;
    }

    /**
     * @return the subBundle
     */
    public boolean isSubBundle() {
        return subBundle;
    }

    /**
     * @param subBundle the subBundle to set
     */
    public void setSubBundle(boolean subBundle) {
        this.subBundle = subBundle;
    }

    /**
     * @return the vat
     */
    public BigDecimal getVat() {
        return vat;
    }

    /**
     * @param vat the vat to set
     */
    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    /**
     * Priznak ci tento bundle predstavuej zaroven aj vstup do B2B Matchmaking
     * @return the b2b
     */
    public boolean isB2b() {
        return b2b;
    }

    /**
     * Priznak ci tento bundle predstavuej zaroven aj vstup do B2B Matchmaking
     * @param b2b the b2b to set
     */
    public void setB2b(boolean b2b) {
        this.b2b = b2b;
    }

    /**
     * Maximalni ocet zakoupenych listku (minimum je 1)
     * @return the quantity
     */
    public Integer getQuantity() {
        if (quantity == null || quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    /**
     * Maximalni pocet zakoupenych listku
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        if (quantity == null || quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }
}
