package consys.event.registration.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.UserEvent;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.ReservedRegistrationOrder;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.core.so.RegistrationOtherInfo;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.event.registration.core.bo.RegistrationOrder;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.core.exception.RegistrationNotActiveException;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.ws.bo.Profile;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface UserRegistrationService {


    /*------------------------------------------------------------------------*/
    /* ----  C R E A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Vytvori registraciu uzivatela pre zadany hlavny bundle a volitelne subbaliky.
     * Nastavi relaciu v administracii na event a vytvori pro forma fakturu resp.
     * fakturu.
     *
     * @param userUuid uuid uzivatela
     * @param mainBundle hlavny balik <strong>povinne</strong>
     * @param subBundles podbaliky <strong>nepovinne</strong>
     * @param profileUuid uuid profilu pre ktory bude vytvoreny doklad
     * @return registraciu uzivatela
     *
     * @throws ServiceExecutionFailed
     * @throws BundleCapacityFullException ak ma bundle obmedzenu kapacitu a uz nie je miesto
     * @throws AlreadyRegistredException  uzivatel so zadany uuid ma aktivnu registraciu
     * @throws CouponAlreadyUsedException zadany zlavovy kupon je uz pouzity
     * @throws NoRecordException nenasial sa bundle, user alebo ko
     * @throws IbanFormatException chyba v IBAN kode
     * @throws EventNotActiveException akcia nie je este aktivovana
     * @throws RequiredPropertyNullException chybaju niektore dolezite udaje
     */
    public UserRegistration createUserRegistration(String userUuid, boolean internEventUser, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, String profileUuid, RegistrationOtherInfo otherInfo)
            throws
            ServiceExecutionFailed,
            BundleCapacityFullException,
            AlreadyRegistredException,
            CouponCodeException,
            NoRecordException,
            IbanFormatException,
            EventNotActiveException,
            RequiredPropertyNullException,
            CouponCapacityException,
            CouponOutOfDateException;

    /**
     * Vytvori registraciu uzivatela pre zadany hlavny bundle a volitelne subbaliky.
     * Nastavi relaciu v administracii na event a vytvori pro forma fakturu resp.
     * fakturu.
     *
     * @param userUuid uuid uzivatela
     * @param mainBundle hlavny balik <strong>povinne</strong>
     * @param subBundles podbaliky <strong>nepovinne</strong>
     * @param profile profil pre ktory bude vytvoreny doklad
     * @return registraciu uzivatela
     *
     * @throws ServiceExecutionFailed
     * @throws BundleCapacityFullException ak ma bundle obmedzenu kapacitu a uz nie je miesto
     * @throws AlreadyRegistredException  uzivatel so zadany uuid ma aktivnu registraciu
     * @throws CouponAlreadyUsedException zadany zlavovy kupon je uz pouzity
     * @throws NoRecordException nenasial sa bundle, user alebo ko
     * @throws IbanFormatException chyba v IBAN kode
     * @throws EventNotActiveException akcia nie je este aktivovana
     * @throws RequiredPropertyNullException chybaju niektore dolezite udaje
     */
    public UserRegistration createUserRegistration(String userUuid, boolean internEventUser, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, Profile profile, RegistrationOtherInfo otherInfo)
            throws ServiceExecutionFailed,
            BundleCapacityFullException,
            AlreadyRegistredException,
            CouponCodeException,
            NoRecordException,
            IbanFormatException,
            EventNotActiveException,
            RequiredPropertyNullException,
            CouponCapacityException,
            CouponOutOfDateException;

    /** Zaregistruje uzivatele do cekaciho seznamu */
    UserRegistration createUserRegistrationToWaiting(String userUuid, boolean internEventUser, String productUuid, int quantity, RegistrationOtherInfo otherInfo)
            throws ServiceExecutionFailed,
            AlreadyRegistredException,
            NoRecordException,
            EventNotActiveException,
            RequiredPropertyNullException;

    /**
     * Zaregistruje noveho uzivatela bez volania do administracie o zmenu relacie
     * ci nacitani udajov. Uzivatela vytvori v databazi ,zaregistruje a vystavi
     * doklady/fakturu/ v platobnom portale.
     *
     * @param userEvent novy uzivatel
     * @param bundleUuid bundle
     * @param code zlavovy kupon
     * @param profile profile     
     * @throws ServiceExecutionFailed
     * @throws BundleCapacityFullException ak ma bundle obmedzenu kapacitu a uz nie je miesto
     * @throws AlreadyRegistredException  uzivatel so zadany uuid ma aktivnu registraciu
     * @throws CouponAlreadyUsedException zadany zlavovy kupon je uz pouzity
     * @throws NoRecordException nenasial sa bundle, user alebo ko
     * @throws IbanFormatException chyba v IBAN kode
     * @throws EventNotActiveException akcia nie je este aktivovana
     * @throws RequiredPropertyNullException chybaju niektore dolezite udaje
     */
    public void createUserRegistrationForNewUser(UserEvent userEvent, String userEmail, BundleOrderRequest mainBundle,
            List<BundleOrderRequest> subBundles, Profile profile, RegistrationOtherInfo otherInfo)
            throws RequiredPropertyNullException,
            ServiceExecutionFailed,
            NoRecordException,
            BundleCapacityFullException,
            CouponCodeException,
            EventNotActiveException,
            IbanFormatException,
            CouponCapacityException,
            CouponOutOfDateException;

    void createUserRegistrationForNewManagedUser(String uuidManager, String firstName, String lastName, String organization, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, Profile profile)
            throws RequiredPropertyNullException,
            ServiceExecutionFailed,
            NoRecordException,
            BundleCapacityFullException,
            CouponCodeException,
            EventNotActiveException,
            IbanFormatException,
            CouponCapacityException,
            CouponOutOfDateException;

    /**
     * Prida novu objednavku k uzivatelskej registracii. Automaticky kontroluje
     * ci uz uzivatel nema dany subbalik zakupeny, resp. cakajuci na zaplatenie.
     *
     * @param userUuid uuid uzivatela     
     * @param subBundles podbaliky <strong>nepovinne</strong>
     * @param profileUuid uuid profilu pre ktory bude vytvoreny doklad
     * @return objednavka uzivatela
     *
     * @throws ServiceExecutionFailed
     * @throws BundleCapacityFullException ak ma bundle obmedzenu kapacitu a uz nie je miesto
     * @throws AlreadyRegistredException  uzivatel so zadany uuid ma aktivnu registraciu
     * @throws CouponAlreadyUsedException zadany zlavovy kupon je uz pouzity
     * @throws NoRecordException nenasial sa bundle, user alebo ko
     * @throws IbanFormatException chyba v IBAN kode
     * @throws EventNotActiveException akcia nie je este aktivovana
     * @throws RequiredPropertyNullException chybaju niektore dolezite udaje
     */
    public RegistrationOrder addOrderToRegistration(String userUuid, List<BundleOrderRequest> subBundles, String profileUuid)
            throws
            ServiceExecutionFailed,
            BundleCapacityFullException,
            AlreadyRegistredException,
            CouponCodeException,
            NoRecordException,
            IbanFormatException,
            EventNotActiveException,
            RequiredPropertyNullException,
            CouponCapacityException,
            CouponOutOfDateException;

    /**
     * Prida novu objednavku k uzivatelskej registracii. Automaticky kontroluje
     * ci uz uzivatel nema dany subbalik zakupeny, resp. cakajuci na zaplatenie.
     *
     * @param userUuid uuid uzivatela
     * @param subBundles podbaliky <strong>nepovinne</strong>
     * @param profile upraveny platobny profil
     * @return objednavka uzivatela
     *
     * @throws ServiceExecutionFailed
     * @throws BundleCapacityFullException ak ma bundle obmedzenu kapacitu a uz nie je miesto
     * @throws AlreadyRegistredException  uzivatel so zadany uuid ma aktivnu registraciu
     * @throws CouponAlreadyUsedException zadany zlavovy kupon je uz pouzity
     * @throws NoRecordException nenasial sa bundle, user alebo ko
     * @throws IbanFormatException chyba v IBAN kode
     * @throws EventNotActiveException akcia nie je este aktivovana
     * @throws RequiredPropertyNullException chybaju niektore dolezite udaje
     */
    public RegistrationOrder addOrderToRegistration(String userUuid, List<BundleOrderRequest> subBundles, Profile profile)
            throws
            ServiceExecutionFailed,
            BundleCapacityFullException,
            AlreadyRegistredException,
            CouponCodeException,
            NoRecordException,
            IbanFormatException,
            EventNotActiveException,
            RequiredPropertyNullException,
            CouponCapacityException,
            CouponOutOfDateException;

    /**
     * Vytvori pre uzivatela rezervaciu na registraciu pocas ktorej ma moznost
     * vyplnit registracne udaje.
     *
     * TODO: momentalne len nacita registraciu. Rezrvacia samotna nie je implementovana
     *
     * @param userUuid uuid uzivatela
     * @param bundleUuid uuid bundlu
     * @param code zlavovy kupon
     * @return
     * @throws BundleCapacityFullException
     * @throws AlreadyRegistredException
     * @throws CouponAlreadyUsedException
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     * @throws ServiceExecutionFailed
     */
    public ReservedRegistrationOrder createRegistrationOrderReservation(String userUuid, List<BundleOrderRequest> bundles)
            throws BundleCapacityFullException,
            AlreadyRegistredException,
            NoRecordException,
            RequiredPropertyNullException,
            ServiceExecutionFailed,
            CouponCodeException,
            CouponOutOfDateException;

    /** interni registrace uzivatele */
    void createInternalRegistration(String managerUuid, UserEvent user, BundleOrderRequest mainBundle,
            List<BundleOrderRequest> subBundles, Profile profile, RegistrationOtherInfo otherInfo)
            throws BundleCapacityFullException, NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed, EventNotActiveException, IbanFormatException, AlreadyRegistredException, CouponCodeException, CouponCapacityException, CouponOutOfDateException;
    
    /*------------------------------------------------------------------------*/
    /* ----  L O A D  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Nacita kupon podla kodu a baliku ku ktoremu patri. Ak je kupon uz pouzity
     * teda, jedna sa o oneTimeDiscount
     * @param bundleUuid uuid balicku
     * @param code kod ktory sa ma nacitat
     * @return zlava
     * @throws NoRecordException ak neexistuje balicek alebo kupon
     * @throws CouponAlreadyUsedException ak je kupon uz pouzity
     */
    public RegistrationCoupon loadActiveRegistrationCouponByCode(String bundleUuid, String code)
            throws NoRecordException,
            CouponCodeException,
            CouponOutOfDateException,
            RequiredPropertyNullException,
            CouponCapacityException;

    /**
     * Nacita prave aktivnu registraciu. Z registracie odstrani vsetky zrusene objednavky
     * a ostanu v nej len objednavky v stave ORDERED a CONFIRMED.
     * @param userUuid uuid uzivatela
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    public UserRegistration loadUserActiveRegistrationDetailedByUserUuid(String userUuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Vrati <code>true</code> ak ma uzivatel uz <strong>aktivnu</strong> registraciu.
     * Inak vrati <code>false</code>.
     * 
     * @param userUuid
     * @return
     * @throws RequiredPropertyNullException 
     */
    public boolean isUserAlreadyRegistred(String userUuid)
            throws RequiredPropertyNullException;

    /**
     * Nacita registraciu podla uuid. Z registracie odstrani vsetky zrusene objednavky
     * a ostanu v nej len objednavky v stave ORDERED a CONFIRMED.
     * @param userUuid uuid uzivatela
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    public UserRegistration loadRegistrationDetailed(String registrationUuid)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Nacita registraciu s cyklom a bundlom podla registracnej UUID
     * 
     * @param registrationUuid
     * @return
     * @throws NoRecordException
     */
    public UserRegistration loadRegistration(String registrationUuid)
            throws NoRecordException, RequiredPropertyNullException;

    UserRegistration loadWaitingRegistration(String registrationUuid) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Pre vlozeny uuid balicku zlavovy kupon a vzhladom na prave aktivny cyklus
     * vypocita zlavnenu cenu.
     *
     * @param bundleUuid uuid balicku
     * @param discountCode zlavovy kod kuponu
     * @return vysledna cena po zlavneni
     * @throws NoRecordException ak nie je ziadny otvoreny cycklus
     * @throws CouponAlreadyUsedException ak je kod pouzity
     * @throws CouponCodeException ak kod neexistuje
     */
    public BigDecimal loadDiscount(String bundleUuid, String discountCode)
            throws NoRecordException,
            CouponCodeException,
            ServiceExecutionFailed,
            RequiredPropertyNullException,
            CouponCapacityException;

    /**
     * Nacita kupon podla uuid
     * @param couponUuid
     * @return
     * @throws NoRecordException 
     */
    public RegistrationCoupon loadCoupon(String couponUuid) throws NoRecordException, RequiredPropertyNullException;

    /*------------------------------------------------------------------------*/
    /* ----  L I S T ---- */
    /*------------------------------------------------------------------------*/
    public List<UserEvent> listAllActiveParticiapntsForBadges() throws NoRecordException;

    /** nacte registrace spravovane danym uzivatelem */
    List<UserRegistration> listManagedRegistrations(String uuidManager);

    /*------------------------------------------------------------------------*/
    /* ----  U P D A T E  ---- */
    /*------------------------------------------------------------------------*/
    /**
     * Metoda ktora upravuje stav registracie na zaklade zmeny ktora nastala v
     * platobnom portale.
     * 
     * @param order uuid registracie
     * @param toState do akeho stavu
     * @param notifyOwner priznak ci sa ma notifikovat vlastnik     
     * @param changeDate kedy nastala tato zmena
     * @return priznak ci bola objednavka spracovana
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public boolean updateRegistrationOrderState(String order, RegistrationState toState, boolean notifyOwner, Date changeDate)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Zrusi objednavku na zaklade poziadavky organizatora. Informacia o zruseni
     * je preposlana platobnemu portalu a uzivatel je o tejto skutocnosti notifkovany. 
     * <br/>
     * V pripade ze objednavka obsahuje hlavny bundle potom rusi celu registraciu
     * 
     * @param registrationOrderUuid uuid {@link RegistrationOrder}
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public void updateCancelRegistrationOrderFromOrganizator(String registrationOrderUuid)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    /**
     * Zrusi objednavku na zaklade poziadavky vlastnika. Informacia o zruseni
     * je preposlana platobnemu portalu a uzivatel je o tejto skutocnosti notifkovany. 
     * <br/>
     * V pripade ze objednavka obsahuje hlavny bundle potom rusi celu registraciu
     * 
     * @param registrationOrderUuid uuid {@link RegistrationOrder}
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public void updateCancelRegistrationOrderFromOwner(String registrationOrderUuid)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    /**
     * Potvrdi objednavku na zaklade poziadavky organizatora. Objednavka bude potvrdena
     * s priznakom ze ju potvrdil organizator a v platobnom portale oznacena patricnym sposobom.
     * Nebude generovana faktura.
     * 
     * @param registrationOrderUuid
     * @throws NoRecordException
     * @throws RequiredPropertyNullException 
     */
    public void updateConfirmRegistrationOrderFromOrganizator(String registrationOrderUuid, String sourceUserUuid, String note, boolean notifyUser)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    /**
     * Upravi poznamku uzivatela. Ak je <code>adminNote=true</code> potom sa upravi
     * administratorska poznamka, v opacnom pripade sa uprav specialwish
     *
     * @param userRegistrationUuid uuid uzivatela
     * @param note poznamka
     * @throws NoRecordException ak neexistuje uzivatel
     * @throws RequiredPropertyNullException ak chyba uuid uzivatela
     */
    public void updateRegistrationNote(String userRegistrationUuid, String note, boolean adminNote)
            throws NoRecordException, RequiredPropertyNullException;

    /**
     * Na zaklade priznaku <code>checked</code> oznaci registraciu za spracovanu
     * checkinom alebo zrusi checkin.
     *
     * @param userRegistrationUuid uuid uzivatela
     * @param note poznamka
     * @throws NoRecordException ak neexistuje uzivatel
     * @throws RequiredPropertyNullException ak chyba uuid uzivatela
     */
    public Date updateRegistrationCheckIn(String userRegistrationUuid, boolean checked, int count)
            throws NoRecordException, RequiredPropertyNullException, RegistrationNotActiveException;

    /**
     * Nacita vsetky objednavky ktore su po datumu splatnosti a zrusi ich. Ak sa jedna
     * o objednavku s tiketom zrusi celu registraciu.
     * 
     * @return pocet 
     */
    public long updateAndCancelAfterDueDateOrders() throws ServiceExecutionFailed;

    /** odstrani cekaci registraci */
    void updateCancelWaiting(String registrationUuid) throws ServiceExecutionFailed;
}
