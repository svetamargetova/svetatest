package consys.event.registration.core.bo.thumb;

/**
 * Objekt ktory uchovava poziadavku v objednavke
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class BundleOrderRequest {

    private String bundleUuid;
    private String discountCode;
    private int quantity = 1;

    public BundleOrderRequest(String bundleUuid, String discountCode, int quantity) {
        this.bundleUuid = bundleUuid;
        this.discountCode = discountCode;
        this.quantity = quantity;
    }

    public BundleOrderRequest(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return String.format("BundleOrderRequest[uuid=%s discount=%s quantity=%d]", bundleUuid, discountCode, quantity);
    }
}
