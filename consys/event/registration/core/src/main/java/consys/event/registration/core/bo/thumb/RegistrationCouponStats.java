package consys.event.registration.core.bo.thumb;

/**
 * Trieda uchovava statistiky o pouzity zlavovych kuponov
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCouponStats {

    private int couponsAll;
    private int couponsUsed;

    public RegistrationCouponStats(int couponsAll, int couponsUsed) {
        this.couponsAll = couponsAll;
        this.couponsUsed = couponsUsed;
    }

    public int getCouponsAll() {
        return couponsAll;
    }

    public int getCouponsUsed() {
        return couponsUsed;
    }
                    
}
