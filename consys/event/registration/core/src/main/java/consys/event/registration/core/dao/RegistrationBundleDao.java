package consys.event.registration.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import java.util.List;

/** 
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationBundleDao extends GenericDao<RegistrationBundle> {

    public List<RegistrationBundle> listMainBundlesWithCycles() throws NoRecordException;

    public List<RegistrationBundle> listSubBundlesWithCycles() throws NoRecordException;

    public List<RegistrationBundle> listMainBundles() throws NoRecordException;

    public List<RegistrationBundle> listSubBundles() throws NoRecordException;

    public List<RegistrationBundle> listAllBundles() throws NoRecordException;

    public RegistrationBundle loadBundleWithCyclesByUuid(String uuid) throws NoRecordException;

    public RegistrationBundle loadBundleWithCyclesByCycleUuid(String cycleUuid) throws NoRecordException;

    public RegistrationBundle loadBundleByUuid(String uuid) throws NoRecordException;

    public List<RegistrationOption> listRegistrationOptions(boolean subbundles) throws NoRecordException;

    /**
     * Upravi order bundlu na zaklade toho ci sa jedna o podbalicek alebo balicek. 
     * @param uuid balicku ktoreho pozicia sa meni
     * @param newPostion nova pozicia balicku
     * @param oldPosition stara pozicia balicku
     * @param subbundle  priznak ci sa jedna o podbalik
     */
    public void updateBundlePosition(String uuid, int oldPosition, int newPosition, boolean subbundle);

    /**
     * Zkontroluje ci bundle so zadanim <code>uuid</code> ma v skutocnosti nastavenu danu poziciu.
     * @param uuid
     * @param position
     * @return
     * @throws IllegalPropertyValue 
     */
    public boolean checkBundlePosition(String uuid, int position);

    /**
     * Nacita vsetky subbaliky s hlavnym balikom.
     * 
     * @param mainBundleUuid
     * @return
     * @throws NoRecordException
     */
    public List<RegistrationOption> listSubbundlesWithMainBundle(String mainBundleUuid) throws NoRecordException;

    public Long loadBundlesCount(boolean subbundles);

    /**
     * Nacita explicitne s dotazom do databaze aka je hodnota pola 'registred' v
     * balicku, este pocas ORM session.
     * @param bundleId
     * @return
     */
    public Integer loadBundleRegistrationsCount(Long bundleId);

    public List<RegistrationOption> listUserNotRegistredSubbundlesOptions(String userUuid) throws NoRecordException;

    public int updateUserB2bAgentRightsForBundle(boolean add, Long bundleId);
    
    void resetRegisteredCounters();
}
