package consys.event.registration.core.service;

/**
 * Service pro vycisteni naklonovanych dat
 * @author pepa
 */
public interface RegistrationCloneClearService {

    /** Vycisti naklonovana data */
    void clearClone();
}
