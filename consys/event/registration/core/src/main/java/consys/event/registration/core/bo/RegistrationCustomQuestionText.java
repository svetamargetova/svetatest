package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.core.bo.enums.SystemLocale;

/**
 * Lokalizovany text doplnujici otazky pri registraci
 * @author pepa
 */
public class RegistrationCustomQuestionText implements ConsysObject {

    private static final long serialVersionUID = -710641647676811673L;
    // data
    private Long id;
    private RegistrationCustomQuestion questionInfo;
    private String question;
    private SystemLocale locale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RegistrationCustomQuestion getQuestionInfo() {
        return questionInfo;
    }

    public void setQuestionInfo(RegistrationCustomQuestion questionInfo) {
        this.questionInfo = questionInfo;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public SystemLocale getLocale() {
        return locale;
    }

    public void setLocale(SystemLocale locale) {
        this.locale = locale;
    }
}
