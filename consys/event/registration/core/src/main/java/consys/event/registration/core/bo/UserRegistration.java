package consys.event.registration.core.bo;

import com.google.common.collect.ImmutableList;
import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.UserEvent;
import java.util.Date;
import java.util.List;

/**
 *  
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserRegistration implements ConsysObject {

    private static final long serialVersionUID = 4788944416338289168L;
    private Long id;
    /** UUID registracie */
    private String uuid;
    /** Registracia uzivatelova */
    private UserEvent user;
    /** Stav registreace */
    private RegistrationState state = RegistrationState.ORDERED;
    /** Poznamka k registracii - viditelna len administratorovi */
    private String note;
    /** Specialne prianie ucastnika */
    private String specialWish;
    /** Zoznam vsetkych objednavok */
    private List<RegistrationOrder> orders;
    /** Registracne cislo, ktore vidi len vlastnik */
    private String registrationNumber;
    /** Datum checkinu  */
    private Date checkInDate;
    /** TODO: odstranit, priznak ci je user checinuty */
    private boolean checked;
    /** bundle o ktery ma uzivatel zajem ale neni v nem volna kapacita */
    private RegistrationBundle waitingBundle;
    /** pozadovana kvantita v cekaci fronte */
    private Integer waitingQuantity;
    /** kolik je odskrtnuto kvantity */
    private Integer quantityChecked;
    /** maximum kolik jde odskrtnout uzivatelu */
    private Integer maxQuantityChecked;

    public UserRegistration() {
        orders = Lists.newArrayList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserEvent getUser() {
        return user;
    }

    public void setUser(UserEvent user) {
        this.user = user;
    }

    public RegistrationState getState() {
        return state;
    }

    public void setState(RegistrationState state) {
        this.state = state;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        UserRegistration e = (UserRegistration) obj;
        return uuid.equals(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("UserRegistration[user=%s ]", user);
    }

    public List<RegistrationOrder> getOrders() {
        return orders;
    }

    public ImmutableList<RegistrationOrder> getConfirmedOrders() {
        ImmutableList.Builder<RegistrationOrder> b = ImmutableList.builder();
        for (RegistrationOrder order : getOrders()) {
            if (order.isConfirmed()) {
                b.add(order);
            }
        }
        return b.build();
    }

    public ImmutableList<RegistrationOrder> getActiveOrders() {
        ImmutableList.Builder<RegistrationOrder> b = ImmutableList.builder();
        for (RegistrationOrder order : getOrders()) {
            if (order.isActive()) {
                b.add(order);
            }
        }
        return b.build();
    }

    public void setOrders(List<RegistrationOrder> orders) {
        this.orders = orders;
    }

    /**
     * Registracne cislo, ktore vidi len vlastnik
     * @return the registrationNumber
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * Registracne cislo, ktore vidi len vlastnik
     * @param registrationNumber the registrationNumber to set
     */
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Date getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    /** TODO: odstranit promennou checked */
    public boolean isChecked() {
        return checked;
    }

    /** TODO: odstranit promennou checked */
    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isActive() {
        return RegistrationState.CONFIRMED.equals(state) && RegistrationState.ORDERED.equals(state);
    }

    /**
     * Specialne prianie ucastnika
     * @return the specialWish
     */
    public String getSpecialWish() {
        return specialWish;
    }

    /**
     * Specialne prianie ucastnika
     * @param specialWish the specialWish to set
     */
    public void setSpecialWish(String specialWish) {
        this.specialWish = specialWish;
    }

    public RegistrationBundle getWaitingBundle() {
        return waitingBundle;
    }

    public void setWaitingBundle(RegistrationBundle waitingBundle) {
        this.waitingBundle = waitingBundle;
    }

    public Integer getWaitingQuantity() {
        if (waitingQuantity == null || waitingQuantity < 1) {
            waitingQuantity = 1;
        }
        return waitingQuantity;
    }

    public void setWaitingQuantity(Integer waitingQuantity) {
        if (waitingQuantity == null || waitingQuantity < 1) {
            this.waitingQuantity = 1;
        } else {
            this.waitingQuantity = waitingQuantity;
        }
    }

    public Integer getQuantityChecked() {
        if (quantityChecked == null) {
            quantityChecked = 0;
        }
        return quantityChecked;
    }

    public void setQuantityChecked(Integer quantityChecked) {
        if (quantityChecked == null) {
            this.quantityChecked = 0;
        } else {
            this.quantityChecked = quantityChecked;
        }
    }

    public Integer getMaxQuantityChecked() {
        if (maxQuantityChecked == null) {
            maxQuantityChecked = 1;
        }
        return maxQuantityChecked;
    }

    public void setMaxQuantityChecked(Integer maxQuantityChecked) {
        if (maxQuantityChecked == null) {
            this.maxQuantityChecked = 1;
        } else {
            this.maxQuantityChecked = maxQuantityChecked;
        }
    }
}
