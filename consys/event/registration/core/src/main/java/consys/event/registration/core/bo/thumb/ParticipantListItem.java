package consys.event.registration.core.bo.thumb;

import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ParticipantListItem extends UserThumbListItem {

    private Date registrationDate;
    private String registrationUuid;

    /**
     * @return the registrationDate
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * @param registrationDate the registrationDate to set
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * @return the registrationUuid
     */
    public String getRegistrationUuid() {
        return registrationUuid;
    }

    /**
     * @param registrationUuid the registrationUuid to set
     */
    public void setRegistrationUuid(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }
}
