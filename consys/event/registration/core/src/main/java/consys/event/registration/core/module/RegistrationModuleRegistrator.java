package consys.event.registration.core.module;

import consys.common.core.bo.SystemProperty;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.api.right.Role;
import consys.event.common.core.AbstractEventModuleRegistrator;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import consys.event.registration.api.properties.RegistrationProperties;
import consys.event.registration.api.right.EventRegistrationModuleRoleModel;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationModuleRegistrator extends AbstractEventModuleRegistrator {

    public static final String MODULE_NAME = "registration";

    @Override
    public String getSchemaName() {
        return MODULE_NAME;
    }

    @Override
    public void doAfterSessionFactoryCreate(Session session) {
    }

    @Override
    public List<SystemProperty> getModuleProperties() {
        SystemProperty participantListVisible = new SystemProperty();
        participantListVisible.setKey(RegistrationProperties.PARTICIPANT_LIST_VISIBILE);
        participantListVisible.setValue("true");
        participantListVisible.setToClient(true);

        SystemProperty registerAnother = new SystemProperty();
        registerAnother.setKey(RegistrationProperties.REGISTER_ANOTHER);
        registerAnother.setValue("false");
        registerAnother.setToClient(true);

        return Lists.newArrayList(participantListVisible, registerAnother);
    }

    @Override
    public Module getModule() {
        Module m = new Module();
        m.setName(MODULE_NAME);
        EventRegistrationModuleRoleModel rights = new EventRegistrationModuleRoleModel();
        for (Role r : rights.getRoles()) {
            ModuleRight right = new ModuleRight();
            right.setName(r.getName());
            right.setUniqueValue(r.getIdentifier());
            m.getRights().add(right);
        }
        return m;
    }

    @Override
    public Map<String, String> getSQLScripts() {
        // Linked hash map lebo najprv musime mat funkciu a potom az kapacitu
        Map<String, String> scripts = Maps.newLinkedHashMap();
        scripts.put("Canceled order - return capacity to bundle function", "/SQL/canceled_order_capacity_function.sql");
        scripts.put("Canceled order - return capacity to bundle trigger", "/SQL/canceled_order_capacity_trigger.sql");
        scripts.put("New order - test and reserve capacity function", "/SQL/new_order_capacity_function.sql");
        scripts.put("New order - test and reserve capacity trigger", "/SQL/new_order_capacity_trigger.sql");
        scripts.put("Append B2B Agent right to all active bundles", "/SQL/append_B2B_right_to_all_active.sql");
        return scripts;
    }
}
