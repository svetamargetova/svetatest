package consys.event.registration.core.exception;

import consys.common.core.exception.ConsysException;

/**
 * Ak sa odstranuje bundle ktory uz ma v nejakom cykle zaregistrovaneho uzivatela.
 * @author Palo
 */
public class CycleNotEmptyException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public CycleNotEmptyException() {
        super();
    }

     public CycleNotEmptyException(String message) {
	super(message);
    }

}
