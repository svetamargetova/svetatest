package consys.event.registration.core.service.impl;

import com.google.common.base.Joiner;
import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.Relation;
import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.*;
import consys.common.core.service.NotX;
import consys.common.core.service.NotxService;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.service.impl.AbstractService;
import consys.common.core.so.RegistrationOtherInfo;
import consys.common.utils.BigDecimalUtils;
import consys.common.utils.UuidProvider;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.event.b2b.api.right.EventB2BModuleRoleModel;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.core.service.UserEventService;
import consys.event.registration.api.checkin.CheckInState;
import consys.event.registration.api.notx.NotxTags;
import consys.event.registration.api.properties.RegistrationProperties;
import consys.event.registration.core.bo.*;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.core.bo.thumb.ReservedRegistrationOrder;
import consys.event.registration.core.bo.thumb.ReservedUserBundleRegistration;
import consys.event.registration.core.dao.*;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.event.registration.core.exception.NoActiveCycleForBundleException;
import consys.event.registration.core.exception.RegistrationNotActiveException;
import consys.event.registration.core.export.PdfTicketGenerator;
import consys.event.registration.core.export.utils.TicketDataConverter;
import consys.event.registration.core.notification.*;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.webservice.OrderWebService;
import consys.payment.ws.bo.OrderState;
import consys.payment.ws.bo.Profile;
import consys.payment.ws.order.*;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserRegistrationServiceImpl extends AbstractService implements UserRegistrationService {

    // -- DAO
    private UserRegistrationDao registrationDao;
    private RegistrationCycleDao cycleDao;
    private UserBundleRegistrationDao userBundleRegistrationDao;
    private RegistrationCouponDao couponDao;
    private RegistrationOrderDao registrationOrderDao;
    private RegistrationBundleDao registrationBundleDao;
    // -- Services
    private UserEventService userEventService;
    private AdminEventWebService administrationWebService;
    private OrderWebService orderWebService;
    private SystemPropertyService systemPropertyService;
    private AwsFileStorageService storageService;
    @Autowired
    private ParticipantManagerService mpService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;
    @Autowired
    private RegistrationBundleService bundleService;
    // -- Misc
    private TicketDataConverter converter;
    private String ticketBucket;


    /*
     * ------------------------------------------------------------------------
     * ---- C R E A T E ----
     * ------------------------------------------------------------------------
     */
    @Override
    public UserRegistration createUserRegistration(String userUuid, boolean internEventUser, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, String profileUuid, RegistrationOtherInfo otherInfo)
            throws ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, IbanFormatException, EventNotActiveException, RequiredPropertyNullException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {

        if (StringUtils.isBlank(userUuid) || mainBundle == null || StringUtils.isBlank(mainBundle.getBundleUuid())) {
            throw new RequiredPropertyNullException("Missin useruuid or mainBundle order request");
        }

        // nacitame uzivatela
        UserEvent user = getUserEventService().createUserOrLoad(new ExistingUserInvitation(userUuid));

        // Vytvorime jeho registraciu pre dany hlavny bundle a podbaliky
        RegistrationOrder order = checkAvailabilityAndCreateNewRegistrationOrder(user, mainBundle, subBundles);

        // ulozime pripadne uzivatelovy odpovedi
        addAnswers(userUuid, otherInfo);

        // Vystavime doklady
        createOrderInPaymentPortal(order, profileUuid, userUuid);

        // upravime relaciu uzivatela
        if (!internEventUser) {
            getUserEventService().doUpdateRelationToEvent(Relation.REGISTRED, "", Lists.newArrayList(userUuid));
        }

        // Otagujeme uzivatela v NotX
        createOrRemoveOrderTags(order, true);

        return order.getUserRegistration();
    }

    @Override
    public UserRegistration createUserRegistration(String userUuid, boolean internEventUser, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, Profile profile, RegistrationOtherInfo otherInfo)
            throws ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, IbanFormatException, EventNotActiveException, RequiredPropertyNullException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {

        if (StringUtils.isBlank(userUuid) || mainBundle == null || StringUtils.isBlank(mainBundle.getBundleUuid())) {
            throw new RequiredPropertyNullException("Missin useruuid or mainBundle order request");
        }

        // nacitame uzivatela
        UserEvent user = getUserEventService().createUserOrLoad(new ExistingUserInvitation(userUuid));

        // Vytvorime jeho registraciu pre dany bundle a kod
        RegistrationOrder order = checkAvailabilityAndCreateNewRegistrationOrder(user, mainBundle, subBundles);

        // ulozime pripadne uzivatelovy odpovedi
        addAnswers(userUuid, otherInfo);

        // Vystavime doklady
        createOrderInPaymentPortal(order, profile, userUuid, false);

        // upravime relaciu uzivatela
        if (!internEventUser) {
            getUserEventService().doUpdateRelationToEvent(Relation.REGISTRED, "", Lists.newArrayList(userUuid));
        }

        // Otagujeme uzivatela v NotX
        createOrRemoveOrderTags(order, true);

        return order.getUserRegistration();
    }

    @Override
    public UserRegistration createUserRegistrationToWaiting(String userUuid, boolean internEventUser, String productUuid, int quantity, RegistrationOtherInfo otherInfo)
            throws ServiceExecutionFailed, AlreadyRegistredException, NoRecordException,
            EventNotActiveException, RequiredPropertyNullException {

        if (quantity < 1) {
            throw new RequiredPropertyNullException("Bad value of quantity: " + quantity);
        }

        // nacitame uzivatela
        UserEvent userEvent = getUserEventService().createUserOrLoad(new ExistingUserInvitation(userUuid));

        // nasjkur sa pokusime nacitat ci ma user uz nejaku aktivnu registraciu
        checkUserNotRegistredYet(userEvent.getUuid());

        // nacteme si balicek do ktereho se chce uzivatel zaregistrovat
        RegistrationBundle rb = registrationBundleDao.loadBundleByUuid(productUuid);

        // Vytvorime novu registraciu
        UserRegistration registration = new UserRegistration();
        registration.setState(RegistrationState.WAITING);
        registration.setUser(userEvent);
        registration.setUuid(UuidProvider.getUuid());
        registration.setRegistrationNumber(UuidProvider.getUuid());
        registration.setWaitingBundle(rb);
        log().debug("Create new user event registration to waiting: {}", registration);
        registrationDao.create(registration);

        // upravime relaciu uzivatela
        if (!internEventUser) {
            getUserEventService().doUpdateRelationToEvent(Relation.WAITING, "", Lists.newArrayList(userUuid));
        }

        return registration;
    }

    @Override
    public void createUserRegistrationForNewUser(UserEvent userEvent, String userEmail, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, Profile profile, RegistrationOtherInfo otherInfo)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException, BundleCapacityFullException, EventNotActiveException, IbanFormatException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        try {
            if (mainBundle == null || StringUtils.isBlank(mainBundle.getBundleUuid())) {
                throw new RequiredPropertyNullException("Missin useruuid or mainBundle order request");
            }

            try {
                userEvent = getUserEventService().loadUserEvent(userEvent.getUuid());
                // test na to ci uz nahodov nema registraciu
                if (isUserAlreadyRegistred(userEvent.getUuid())) {
                    log().info("{} is already registred.", userEvent);
                    return;
                }
            } catch (NoRecordException e) {
                // vytorim noveho uzivatela
                getUserEventService().createUser(userEvent);
            }

            // zaregistrujeme
            RegistrationOrder order = checkAvailabilityAndCreateNewRegistrationOrder(userEvent, mainBundle, subBundles);

            // ulozime pripadne uzivatelovy odpovedi
            addAnswers(userEvent.getUuid(), otherInfo);

            // Vystavime doklady
            createOrderInPaymentPortal(order, profile, userEvent.getUuid(), false);

            // Otagujeme uzivatela v NotX
            createOrRemoveOrderTags(order, true);
        } catch (AlreadyRegistredException ex) {
            log().warn("{} is already registred. But didn't catch it before ", userEvent);
        }
    }

    @Override
    public void createUserRegistrationForNewManagedUser(String uuidManager, String firstName, String lastName,
            String organization, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, Profile profile)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException, BundleCapacityFullException,
            CouponCodeException, EventNotActiveException, IbanFormatException, CouponCapacityException, CouponOutOfDateException {

        if (mainBundle == null || StringUtils.isBlank(mainBundle.getBundleUuid()) || StringUtils.isBlank(firstName)
                || StringUtils.isBlank(lastName) || StringUtils.isBlank(organization) || StringUtils.isBlank(uuidManager)) {
            throw new RequiredPropertyNullException("Missing mainBundle order request or firstName or lastName or organization or uuidManager");
        }

        try {
            SystemProperty sp = systemPropertyService.loadSystemProperty(RegistrationProperties.REGISTER_ANOTHER);
            if (sp.getValue().equalsIgnoreCase("false")) {
                throw new ServiceExecutionFailed();
            }
        } catch (NoRecordException ex) {
            log().warn("Fail load property" + RegistrationProperties.REGISTER_ANOTHER);
            throw new ServiceExecutionFailed();
        }

        UserEvent participant = new UserEvent();
        participant.setFullName(firstName + " " + lastName);
        participant.setLastName(lastName);
        participant.setOrganization(organization);

        mpService.createNewManagedUser(uuidManager, participant);

        // testovat jestli ma ucastnik registraci nemusime, vytvarime noveho

        try {
            // zaregistrujeme
            RegistrationOrder order = checkAvailabilityAndCreateNewRegistrationOrder(participant, mainBundle, subBundles);

            // Vystavime doklady
            createOrderInPaymentPortal(order, profile, null, false);

            // Otagujeme uzivatela v NotX
            createOrRemoveOrderTags(order, true);

        } catch (AlreadyRegistredException ex) {
            log().warn("{} is already registred. But didn't catch it before ", participant);
        }
    }

    @Override
    public RegistrationOrder addOrderToRegistration(String userUuid, List<BundleOrderRequest> subBundles, String profileUuid)
            throws ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, CouponCodeException, NoRecordException, IbanFormatException, EventNotActiveException, RequiredPropertyNullException, CouponCapacityException, CouponOutOfDateException {
        if (StringUtils.isBlank(userUuid) || subBundles == null || subBundles.isEmpty() || StringUtils.isBlank(profileUuid)) {
            throw new RequiredPropertyNullException("Missing one argument");
        }
        // Vytvorime novu objednavku a pridame do registracie
        RegistrationOrder newOrder = addNewOrderIntoActiveRegistration(userUuid, subBundles, false);

        // Posleme do payment portalu
        createOrderInPaymentPortal(newOrder, profileUuid, userUuid);

        // Otagujeme uzivatela v NotX
        createOrRemoveOrderTags(newOrder, true);

        return newOrder;
    }

    @Override
    public RegistrationOrder addOrderToRegistration(String userUuid, List<BundleOrderRequest> subBundles, Profile profile) throws ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, CouponCodeException, NoRecordException, IbanFormatException, EventNotActiveException, RequiredPropertyNullException, CouponCapacityException, CouponOutOfDateException {
        if (StringUtils.isBlank(userUuid) || subBundles == null || subBundles.isEmpty() || profile == null) {
            throw new RequiredPropertyNullException("Missing one argument");
        }
        // Vytvorime novu objednavku a pridame do registracie
        RegistrationOrder newOrder = addNewOrderIntoActiveRegistration(userUuid, subBundles, false);

        // Posleme do payment portalu
        createOrderInPaymentPortal(newOrder, profile, userUuid, false);

        // Otagujeme uzivatela v NotX
        createOrRemoveOrderTags(newOrder, true);

        return newOrder;
    }

    private RegistrationOrder addNewOrderIntoActiveRegistration(String userUuid, List<BundleOrderRequest> additionalSubbundles, boolean internalRegistration)
            throws NoRecordException, AlreadyRegistredException, RequiredPropertyNullException, ServiceExecutionFailed, CouponCodeException, BundleCapacityFullException, CouponCapacityException, CouponOutOfDateException {
        // Nacitame aktivnu objednavku
        UserRegistration activeRegistration = loadUserActiveRegistrationDetailedByUserUuid(userUuid);

        // ak nie je hlavna objednavka potvrdena nie je mozne pridavat dalsie objednavkky
        if (RegistrationState.ORDERED.equals(activeRegistration.getState())) {
            throw new ServiceExecutionFailed("You can't add another order if " + activeRegistration + " is not CONFIRMED");
        }

        // Test ci uz uzivatel nema danu objednavku objednanu
        for (RegistrationOrder order : activeRegistration.getActiveOrders()) {
            // testujeme jednotlive baliky
            for (UserBundleRegistration bundleRegistration : order.getRegistredBundles()) {
                for (BundleOrderRequest req : additionalSubbundles) {
                    if (bundleRegistration.getRegistrationCycle().getBundle().getUuid().equalsIgnoreCase(req.getBundleUuid())) {
                        throw new AlreadyRegistredException(req.getBundleUuid());
                    }
                }
            }
        }

        RegistrationOrder newOrder = createOrder(activeRegistration, null, additionalSubbundles, internalRegistration);
        return newOrder;
    }

    @Override
    public ReservedRegistrationOrder createRegistrationOrderReservation(String userUuid, List<BundleOrderRequest> bundles)
            throws BundleCapacityFullException,
            AlreadyRegistredException,
            NoRecordException,
            RequiredPropertyNullException,
            ServiceExecutionFailed,
            CouponCodeException {

        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }

        ReservedRegistrationOrder registration = new ReservedRegistrationOrder();

        for (BundleOrderRequest request : bundles) {
            RegistrationCycle cycle = loadActiveCycleForBundle(request.getBundleUuid());
            ReservedUserBundleRegistration userBundleRegistration = new ReservedUserBundleRegistration();
            userBundleRegistration.setBundle(cycle.getBundle());
            userBundleRegistration.setQuantity(request.getQuantity());
            // nacitanie kuponu
            if (StringUtils.isNotBlank(request.getDiscountCode())) {
                RegistrationCoupon coupon = loadDiscountCouponForRead(request.getDiscountCode(), request.getBundleUuid());
                BigDecimal finalPrice = countFinalBundlePrice(cycle, coupon, request.getQuantity());
                userBundleRegistration.setDiscountCoupon(coupon);
                userBundleRegistration.setTotal(finalPrice);
            } else {
                userBundleRegistration.setTotal(countFinalBundlePrice(cycle, null, request.getQuantity()));
            }
            registration.getRegistrations().add(userBundleRegistration);
            // pripocitame k celkovej sume
            registration.setTotal(registration.getTotal().add(userBundleRegistration.getTotal()));
        }
        return registration;
    }

    @Override
    public void createInternalRegistration(String managerUuid, UserEvent user, BundleOrderRequest mainBundle,
            List<BundleOrderRequest> subBundles, Profile profile, RegistrationOtherInfo otherInfo)
            throws BundleCapacityFullException, NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed, EventNotActiveException, IbanFormatException, AlreadyRegistredException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {

        // vytvorime uzivatele
        mpService.createNewManagedUser(managerUuid, user);
        // nastavime jeho uuid do platebniho profilu
        profile.setUuid(user.getUuid());

        // pridame odpovedi na otazky
        addAnswers(user.getUuid(), otherInfo);

        // vytvorime slevove kupony 100% pro vsechny polozky
        discount100All(mainBundle, subBundles);

        // vytvorime objednavku z balicku a subbalicku
        RegistrationOrder order = checkAvailabilityAndCreateNewRegistrationOrder(user, mainBundle, subBundles, true);

        // posleme na payment portal
        createOrderInPaymentPortal(order, profile, user.getUuid(), true);

        // otagujeme v notx
        createOrRemoveOrderTags(order, true);
    }

    /** vygeneruje kody pro 100 slevu na vsechny polozky v objednavce */
    private void discount100All(BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles) throws RequiredPropertyNullException, NoRecordException, CouponCodeException {
        RegistrationCoupon coupon = discount100AllCoupon(mainBundle.getBundleUuid(), 1);
        mainBundle.setDiscountCode(coupon.getKey());

        for (BundleOrderRequest sub : subBundles) {
            RegistrationCoupon subCoupon = discount100AllCoupon(sub.getBundleUuid(), 1);
            sub.setDiscountCode(subCoupon.getKey());
        }
    }

    /** vygeneruje slevovy kupon 100 pro zadany balicek */
    private RegistrationCoupon discount100AllCoupon(String bundleUuid, int quantity) throws RequiredPropertyNullException, NoRecordException, CouponCodeException {
        String key = bundleService.generateRegistrationCouponKey(bundleUuid);
        RegistrationCoupon coupon = new RegistrationCoupon(RegistrationCoupon.INTERNAL_COUPONT_KEY_BEGIN + key, 100, quantity);
        bundleService.createRegistrationCoupon(bundleUuid, coupon);
        return coupon;
    }

    /**
     * Pre dany registraciu vytvori proforma fakturu v platobnom portale.
     * Platobny profil je odkazovany cez profileUuid teda bez zmeny.
     *
     * @param registration
     * @param profileUuid
     * @throws RequiredPropertyNullException
     * @throws IbanFormatException
     * @throws NoRecordException
     * @throws EventNotActiveException
     */
    private void createOrderInPaymentPortal(RegistrationOrder order, String profileUuid, String managedUserUuid)
            throws RequiredPropertyNullException, IbanFormatException, NoRecordException, EventNotActiveException, ServiceExecutionFailed {
        CreateOrderRequest request = OrderWebService.OBJECT_FACTORY.createCreateOrderRequest();
        prepareNewOrderRequest(request, order, false);
        request.setUserProfileUuid(profileUuid);
        request.setManagedUserUuid(managedUserUuid);

        log().debug("Creating Order on PaymentPortal => {}", request.toString());

        CreateOrderResponse response = orderWebService.createOrder(request);

        // Spracujeme vynimky
        processRegistrationExceptions(response.getException());

        // Zistime ci finalna suma je ina ako suma vypocitana v portale - toto 
        // moze nastat situacia ze sa odpocita DPH

        if (response.getFinalPrice().compareTo(order.getOverallPrice()) != 0) {
            log().debug("Updating {} final price to {}", order, response.getFinalPrice());
            order.setOverallPrice(response.getFinalPrice());
            registrationOrderDao.update(order);
        }

        // spracujeme ci bola zaplatena - ak je discount 100 napriklad
        if (response.isConfirmed()) {
            log().debug("Order is for free -> Order Confirmed");
            confirmRegistrationOrder(order, null, null, true, false, DateProvider.getCurrentDate());
        } else {
            log().debug("Sending notification");
            // Posleme notifikaciu
            sendNewRegistrationNotification(order);
        }
    }

    /**
     * Pre dany registraciu vytvori proforma fakturu v platobnom portale.
     * Platobny profil je novy alebo zmeneny
     *
     * @param registration
     * @param profileUuid
     * @throws RequiredPropertyNullException
     * @throws IbanFormatException
     * @throws NoRecordException
     * @throws EventNotActiveException
     */
    private void createOrderInPaymentPortal(RegistrationOrder order, Profile profile, String managedUserUuid, boolean internal)
            throws NoRecordException, EventNotActiveException, IbanFormatException, RequiredPropertyNullException, ServiceExecutionFailed {
        CreateOrderWithProfileRequest request = OrderWebService.OBJECT_FACTORY.createCreateOrderWithProfileRequest();
        prepareNewOrderRequest(request, order, internal);
        request.setUserProfile(profile);
        request.setUserProfileUuid(profile.getUuid());
        request.setManagedUserUuid(managedUserUuid);
        log().debug("Creating Order on PaymentPortal => {}", request.toString());

        CreateOrderWithProfileResponse response = orderWebService.createOrderWithProfile(request);
        // Spracujeme vynimky
        processRegistrationExceptions(response.getException());

        // Zistime ci finalna suma je ina ako suma vypocitana v portale - toto 
        // moze nastat situacia ze sa odpocita DPH


        if (response.getFinalPrice().compareTo(order.getOverallPrice()) != 0) {
            log().debug("Updating {} final price to {}", order, response.getFinalPrice());
            order.setOverallPrice(response.getFinalPrice());
            registrationOrderDao.update(order);
        }


        // spracujeme ci bola zaplatena - ak je discount 100 napriklad
        if (response.isConfirmed()) {
            log().debug("Order is for free -> Order Confirmed");
            confirmRegistrationOrder(order, null, null, true, false, DateProvider.getCurrentDate());
        } else {
            log().debug("Sending notification");
            // Posleme notifikaciu
            sendNewRegistrationNotification(order);
        }
    }

    /**
     * Privatna metoda ktora len zisti ci je mozne sa zaregistrovat a
     * zaregistruje daneho uzivatela ktory <strong>existuje v priestore
     * akcie</strong> a vytvori jeho rezervaciu na zadny bundle s pripadnym
     * kodom.
     *
     * @param userUuid
     * @param bundleUuid
     * @param code
     * @return
     * @throws NoRecordException
     * @throws AlreadyRegistredException
     * @throws BundleCapacityFullException
     * @throws CouponAlreadyUsedException
     * @throws ServiceExecutionFailed
     * @throws RequiredPropertyNullException
     */
    private RegistrationOrder checkAvailabilityAndCreateNewRegistrationOrder(UserEvent userEvent, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles)
            throws NoRecordException, AlreadyRegistredException, BundleCapacityFullException, ServiceExecutionFailed, RequiredPropertyNullException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        return checkAvailabilityAndCreateNewRegistrationOrder(userEvent, mainBundle, subBundles, false);
    }

    private RegistrationOrder checkAvailabilityAndCreateNewRegistrationOrder(UserEvent userEvent, BundleOrderRequest mainBundle, List<BundleOrderRequest> subBundles, boolean internalRegistration)
            throws NoRecordException, AlreadyRegistredException, BundleCapacityFullException, ServiceExecutionFailed, RequiredPropertyNullException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // nasjkur sa pokusime nacitat ci ma user uz nejaku aktivnu registraciu
        checkUserNotRegistredYet(userEvent.getUuid());

        // Vytvorime novu registraciu
        UserRegistration registration = new UserRegistration();
        registration.setState(RegistrationState.ORDERED);
        registration.setUser(userEvent);
        registration.setUuid(UuidProvider.getUuid());
        registration.setRegistrationNumber(UuidProvider.getUuid());
        log().debug("Create new user event registration: {}", registration);
        registrationDao.create(registration);

        RegistrationOrder order = createOrder(registration, mainBundle, subBundles, internalRegistration);

        order.getUserRegistration().setQuantityChecked(0);
        order.getUserRegistration().setMaxQuantityChecked(mainBundle.getQuantity());

        return order;
    }

    /**
     * Vytvori novu objednavku na zaklade poziadaviek
     *
     * @param registration
     * @param mainBundle
     * @param subBundles
     * @return
     * @throws ServiceExecutionFailed
     * @throws CouponAlreadyUsedException
     * @throws CouponCodeException
     * @throws BundleCapacityFullException
     */
    private RegistrationOrder createOrder(UserRegistration registration, BundleOrderRequest mainBundle,
            List<BundleOrderRequest> subBundles, boolean internalRegistration)
            throws
            ServiceExecutionFailed, // brutal fail
            // kupon je uz pouzity
            CouponCodeException, // kupon neexistuje
            BundleCapacityFullException,
            CouponCapacityException,
            CouponOutOfDateException,
            NoRecordException,
            RequiredPropertyNullException // Bundle je plny
    {
        // Vytvorime novu objednavku
        RegistrationOrder newOrder = new RegistrationOrder();
        newOrder.setOrderDate(DateProvider.getCurrentDate());
        newOrder.setState(RegistrationState.ORDERED);
        newOrder.setUserRegistration(registration);
        newOrder.setUuid(UuidProvider.getUuid());
        newOrder.setOverallPrice(BigDecimal.ZERO);
        getRegistrationOrderDao().create(newOrder);

        // Pridame objednavku do registracie
        registration.getOrders().add(newOrder);
        registrationDao.update(registration);

        /*
         * ---- Pridame veci do objednavky ----
         */
        if (mainBundle == null && (subBundles == null || subBundles.isEmpty())) {
            throw new NullPointerException("Main bundle and subbundles are empty. At least one option have to be inserted!");
        }

        // Spracujeme hlavny ak je vlozeny a pridame na objednavku
        if (mainBundle != null) {
            log().debug("Processing Ticket: {}", mainBundle);
            createUserBundleRegistration(newOrder, mainBundle, true, internalRegistration);
        }

        // spracujeme subbalicky
        if (subBundles != null) {
            for (BundleOrderRequest bundleOrderRequest : subBundles) {
                log().debug("Processing Accessory: {}", bundleOrderRequest);
                createUserBundleRegistration(newOrder, bundleOrderRequest, false, internalRegistration);
            }
        }

        log().debug("Creating order: {}", newOrder);

        getRegistrationOrderDao().update(newOrder);
        return newOrder;
    }

    /**
     * Vytvori jednu registraciu na bundle pre danu objednavku a poziadavku.
     * Priznakom mainBundle sa urcuje ci sa jedna o hlavny balik alebo podbalik.
     *
     * @param order
     * @param orderRequest
     * @param mainBundle
     * @throws ServiceExecutionFailed
     * @throws CouponAlreadyUsedException
     * @throws CouponCodeException
     * @throws BundleCapacityFullException
     */
    private void createUserBundleRegistration(RegistrationOrder order, BundleOrderRequest orderRequest, boolean mainBundle, boolean internalRegistration)
            throws ServiceExecutionFailed, CouponCodeException, BundleCapacityFullException, CouponCapacityException, CouponOutOfDateException, NoRecordException, RequiredPropertyNullException {

        // test na subbalik vs. hlavny balik
        RegistrationCycle cycle = loadActiveCycleForBundle(orderRequest.getBundleUuid());
        if (mainBundle && cycle.getBundle().isSubBundle()) {
            log().error("Registration failed! Claimed main bundle is marked as sub bundle!");
            throw new ServiceExecutionFailed("Registration failed! Claimed main bundle is marked as sub bundle!");
        } else if (!mainBundle && !cycle.getBundle().isSubBundle()) {
            log().error("Registration failed! Claimed sub bundle is marked as main bundle!");
            throw new ServiceExecutionFailed("Registration failed! Claimed sub bundle is marked as main bundle!");
        }

        // Vytvorime novu bundle objednavku
        UserBundleRegistration bundleRegistration = new UserBundleRegistration();
        bundleRegistration.setRegistrationCycle(cycle);
        bundleRegistration.setUuid(UuidProvider.getUuid());
        bundleRegistration.setRegistrationOrder(order);
        bundleRegistration.setQuantity(orderRequest.getQuantity());

        if (internalRegistration) {
            useCoupon(orderRequest, cycle, bundleRegistration, true);
        } else {
            // Spracujeme zlavovy kupon
            if (StringUtils.isNotBlank(orderRequest.getDiscountCode()) && cycle.getBundle().isWithCode()) {
                useCoupon(orderRequest, cycle, bundleRegistration, false);
            } else {
                bundleRegistration.setBundleFinalPrice(countFinalBundlePrice(cycle, null, orderRequest.getQuantity()));
            }
        }

        // vytvorime registraciu balika
        try {
            getUserBundleRegistrationDao().create(bundleRegistration, true);
        } catch (HibernateException he) {
            log().debug("Capacity full in: {} ", cycle.getBundle());
            throw new BundleCapacityFullException(orderRequest.getBundleUuid());
        }

        // Aktualizujeme objednavku
        order.setOverallPrice(order.getOverallPrice().add(bundleRegistration.getBundleFinalPrice()));
        order.getRegistredBundles().add(bundleRegistration);

        // ak je bundle zaroven vstupenkou do B2B tak nastavime pravo
        if (cycle.getBundle().isB2b()) {
            userEventService.addUserPersonalRight(EventB2BModuleRoleModel.RIGHT_B2B_AGENT, order.getUserRegistration().getUser());
        }
    }

    /** pouzije slevovy kupon */
    private void useCoupon(BundleOrderRequest orderRequest, RegistrationCycle cycle,
            UserBundleRegistration bundleRegistration, boolean internalRegistration)
            throws CouponCapacityException, CouponCodeException, CouponOutOfDateException {
        try {
            RegistrationCoupon coupon = loadCoupon(orderRequest.getDiscountCode(), cycle.getBundle().getUuid(), true);
            if (!coupon.hasFreeCapacity()) {
                String msg = String.format("%s has no free capacity", coupon);
                log().debug(msg);
                throw new CouponCapacityException(msg);
            }

            BigDecimal discounted = internalRegistration
                    ? BigDecimal.ZERO
                    : countFinalBundlePrice(cycle, coupon, orderRequest.getQuantity());
            // nastavenie hodnoty a pouziteho kuponu
            bundleRegistration.setBundleFinalPrice(discounted);
            bundleRegistration.setRegistrationCoupon(coupon);
            addCouponUsage(coupon, 1);
        } catch (NoRecordException ex) {
            throw new CouponCodeException();
        }
    }

    private void prepareNewOrderRequest(CreateOrderReq request, RegistrationOrder order, boolean internal) {
        request.setOrderUuid(order.getUuid());
        for (UserBundleRegistration bundleRegistration : order.getRegistredBundles()) {
            OrderItem orderItem = OrderWebService.OBJECT_FACTORY.createOrderItem();
            orderItem.setProductUuid(bundleRegistration.getRegistrationCycle().getUuid());
            if (bundleRegistration.getQuantity() == 1) {
                orderItem.setDiscount(bundleRegistration.getRegistrationCoupon() == null ? 0 : bundleRegistration.getRegistrationCoupon().getDiscount());
                orderItem.setQuantity(1);
            } else {
                if (!internal) {
                    // obecna registrace
                    boolean withCoupon = bundleRegistration.getRegistrationCoupon() != null;
                    if (withCoupon) {
                        OrderItem firstItem = OrderWebService.OBJECT_FACTORY.createOrderItem();
                        firstItem.setDiscount(bundleRegistration.getRegistrationCoupon().getDiscount());
                        firstItem.setProductUuid(bundleRegistration.getRegistrationCycle().getUuid());
                        firstItem.setQuantity(1);
                        request.getOrderItems().add(firstItem);
                    }
                    orderItem.setDiscount(0);
                    orderItem.setQuantity(withCoupon ? bundleRegistration.getQuantity() - 1 : bundleRegistration.getQuantity());
                } else {
                    // vnitrni registrace administratorem
                    orderItem.setDiscount(bundleRegistration.getRegistrationCoupon().getDiscount());
                    orderItem.setQuantity(bundleRegistration.getQuantity());
                }
            }
            request.getOrderItems().add(orderItem);
        }
        request.setUserUuid(order.getUserRegistration().getUser().getUuid());
    }

    private void processRegistrationExceptions(CreateOrderExceptionEnum ex)
            throws IbanFormatException, EventNotActiveException, NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (ex == null) {
            return;
        }
        if (CreateOrderExceptionEnum.EVENT_NOT_ACTIVE_EXCEPTION.equals(ex)) {
            throw new EventNotActiveException("Event is not activated YET");
        } else if (CreateOrderExceptionEnum.IBAN_FORMAT_EXCEPTION.equals(ex)) {
            throw new IbanFormatException("Bad Iban format");
        } else if (CreateOrderExceptionEnum.NO_RECORD_EXCEPTION.equals(ex)) {
            throw new NoRecordException();
        } else if (CreateOrderExceptionEnum.REQUIRED_PROPERTY_EXCEPTION.equals(ex)) {
            throw new RequiredPropertyNullException();
        } else if (CreateOrderExceptionEnum.PRODUCT_INCOMPATIBILITY_EXCEPTION.equals(ex)) {
            throw new ServiceExecutionFailed();
        }

    }

    /*
     * ------------------------------------------------------------------------     
     * ---- L O A D ----     
     * ------------------------------------------------------------------------
     */
    @Override
    public UserRegistration loadRegistration(String registrationUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(registrationUuid)) {
            throw new RequiredPropertyNullException();
        }
        return registrationDao.loadRegistrationByUuid(registrationUuid);
    }

    @Override
    public UserRegistration loadWaitingRegistration(String registrationUuid)
            throws NoRecordException, RequiredPropertyNullException {
        return registrationDao.loadWaitingRegistration(registrationUuid);
    }

    @Override
    public UserRegistration loadRegistrationDetailed(String registrationUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(registrationUuid)) {
            throw new RequiredPropertyNullException();
        }
        return registrationDao.loadUserRegistrationDetailedByRegistrationUuid(registrationUuid);
    }

    @Override
    public UserRegistration loadUserActiveRegistrationDetailedByUserUuid(String userUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        UserRegistration userRegistration = registrationDao.loadUserRegistrationDetailedByUserUuid(userUuid);
        return userRegistration;
    }

    @Override
    public boolean isUserAlreadyRegistred(String userUuid)
            throws RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        return registrationDao.isUserAlreadyRegistred(userUuid);
    }

    @Override
    public RegistrationCoupon loadActiveRegistrationCouponByCode(String bundleUuid, String code)
            throws NoRecordException, RequiredPropertyNullException, CouponCapacityException {
        if (StringUtils.isBlank(bundleUuid) || StringUtils.isBlank(code)) {
            throw new RequiredPropertyNullException("Missing bundleUuid or discountCode");
        }

        RegistrationCoupon coupon = loadDiscountCouponForRead(code, bundleUuid);
        if (coupon.hasFreeCapacity()) {
            return coupon;
        } else {
            throw new CouponCapacityException();
        }
    }

    @Override
    public BigDecimal loadDiscount(String bundleUuid, String discountCode)
            throws NoRecordException, CouponCodeException, RequiredPropertyNullException, ServiceExecutionFailed, CouponCapacityException {
        if (StringUtils.isBlank(discountCode) || StringUtils.isBlank(bundleUuid)) {
            throw new RequiredPropertyNullException("Missing discount coupon or bundle uuid");
        }

        RegistrationCoupon coupon = loadDiscountCouponForRead(discountCode, bundleUuid);
        if (!coupon.hasFreeCapacity()) {
            throw new CouponCapacityException();
        }
        RegistrationCycle cycle = loadActiveCycleForBundle(bundleUuid);
        if (coupon.getDiscount() < 0 || coupon.getDiscount() > 100) {
            throw new IllegalArgumentException("Discount out of boundary 0 <= discount <= 100 : " + coupon.getDiscount());
        } else if (coupon.getDiscount() == 100) {
            return cycle.getPrice();
        } else {
            // zlavnena hodnota
            return BigDecimalUtils.countPercent(cycle.getPrice(), new BigDecimal(coupon.getDiscount()));
        }
    }

    @Override
    public RegistrationCoupon loadCoupon(String couponUuid)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(couponUuid)) {
            throw new RequiredPropertyNullException();
        }
        return couponDao.loadByUuid(couponUuid);
    }

    /* ------------------------------------------------------------------------ */
    /* ---- L I S T ---- */
    /* ------------------------------------------------------------------------ */
    @Override
    public List<UserEvent> listAllActiveParticiapntsForBadges() throws NoRecordException {
        return registrationDao.listAllActiveParticiapntsForBadges();
    }

    @Override
    public List<UserRegistration> listManagedRegistrations(String uuidManager) {
        try {
            SystemProperty sp = systemPropertyService.loadSystemProperty(RegistrationProperties.REGISTER_ANOTHER);
            if (sp.getValue().equalsIgnoreCase("false")) {
                return new ArrayList<UserRegistration>();
            }
        } catch (NoRecordException ex) {
            log().warn("Fail load property" + RegistrationProperties.REGISTER_ANOTHER);
            return new ArrayList<UserRegistration>();
        }
        return registrationDao.listManagedRegistrations(uuidManager);
    }

    /*
     * ------------------------------------------------------------------------
     * ---- U P D A T E ----
     * ------------------------------------------------------------------------
     */
    /**
     *
     * @return
     * @throws ServiceExecutionFailed
     */
    @Override
    public long updateAndCancelAfterDueDateOrders() throws ServiceExecutionFailed {
        final int ORDERS_PER_CYCLE = 100;

        long remove = registrationOrderDao.listActiveOrdersAfterDueDateWithRegistrationCount();
        log().debug("Remove {} orders after due date.", remove);
        try {
            for (int i = 0; i < remove; i += ORDERS_PER_CYCLE) {
                List<RegistrationOrder> ordersToCancel = registrationOrderDao.listActiveOrdersAfterDueDateWithRegistration(i, ORDERS_PER_CYCLE);
                for (RegistrationOrder registrationOrder : ordersToCancel) {
                    cancelRegistrationOrder(registrationOrder, true, true, false, true, false);
                }
            }
        } catch (ConsysException e) {
            log().error("Fialed to cancel after due date orders!", e);
            throw new ServiceExecutionFailed("Failed !");
        }

        return remove;
    }

    /**
     * POUZIVA LEN LISTENER Z PAYMENT PORTALU !!!, INAK NIKTO
     * <p/>
     * @param order
     * @param toState
     * @param notifyOwner
     * @param changeDate
     * @return
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     */
    @Override
    public boolean updateRegistrationOrderState(String order, RegistrationState toState, boolean notifyOwner, Date changeDate)
            throws NoRecordException, RequiredPropertyNullException {
        try {
            if (StringUtils.isBlank(order) || toState == null) {
                throw new RequiredPropertyNullException("Missing order or toState");
            }

            switch (toState) {
                case CANCELED:
                    cancelRegistrationOrder(order, notifyOwner, false, false, false, true);
                    break;
                case CONFIRMED:
                    confirmRegistrationOrder(order, null, null, notifyOwner, false, changeDate);
                    break;
                case APPROVE:
                    updateState(order, toState, notifyOwner, false);
                    break;
            }

            return true;
        } catch (ServiceExecutionFailed ex) {
            return false;
        }



    }

    @Override
    public void updateCancelRegistrationOrderFromOrganizator(String registrationOrderUuid)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        cancelRegistrationOrder(registrationOrderUuid, true, true, false, false, false);
    }

    @Override
    public void updateCancelRegistrationOrderFromOwner(String registrationOrderUuid)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        cancelRegistrationOrder(registrationOrderUuid, true, true, true, false, false);
    }

    @Override
    public void updateConfirmRegistrationOrderFromOrganizator(String registrationOrderUuid, String sourceUserUuid, String note, boolean notifyUser)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        confirmRegistrationOrder(registrationOrderUuid, sourceUserUuid, note, notifyUser, true, DateProvider.getCurrentDate());
    }

    @Override
    public void updateRegistrationNote(String userRegistrationUuid, String note, boolean adminNote)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userRegistrationUuid)) {
            throw new RequiredPropertyNullException();
        }
        UserRegistration reg = registrationDao.loadRegistrationByUuid(userRegistrationUuid);
        if (adminNote) {
            reg.setNote(note);
        } else {
            reg.setSpecialWish(note);
        }
        registrationDao.update(reg);
    }

    @Override
    public Date updateRegistrationCheckIn(String userRegistrationUuid, boolean checked, final int count)
            throws NoRecordException, RequiredPropertyNullException, RegistrationNotActiveException {
        if (StringUtils.isBlank(userRegistrationUuid) || count == 0 || count < -2) {
            throw new RequiredPropertyNullException();
        }
        UserRegistration userRegistration = registrationDao.loadRegistrationByUuid(userRegistrationUuid);
        if (userRegistration.getState().equals(RegistrationState.CONFIRMED)) {
            switch (count) {
                case CheckInState.CHECK_IN_ALL:
                    if (userRegistration.getQuantityChecked() == 0) {
                        userRegistration.setCheckInDate(DateProvider.getCurrentDate());
                    }
                    userRegistration.setQuantityChecked(userRegistration.getMaxQuantityChecked());
                    break;
                case CheckInState.CHECK_OUT_ALL:
                    userRegistration.setCheckInDate(null);
                    userRegistration.setQuantityChecked(0);
                    break;
                default:
                    // jiny pocet nez vse najednou
                    if (checked) {
                        // bude se zaskrtavat
                        int prechecked = userRegistration.getQuantityChecked() + count;
                        int value = prechecked > userRegistration.getMaxQuantityChecked()
                                ? userRegistration.getMaxQuantityChecked() : prechecked;
                        if (userRegistration.getQuantityChecked() == 0) {
                            // prvni zaskrtnuti, nastavime datum
                            userRegistration.setCheckInDate(DateProvider.getCurrentDate());
                        }
                        userRegistration.setQuantityChecked(value);
                    } else {
                        // bude se odskrtavat
                        int diff = userRegistration.getQuantityChecked() - count;
                        userRegistration.setQuantityChecked(diff < 0 ? 0 : diff);
                        if (userRegistration.getQuantityChecked() == 0) {
                            // nic neni zaskrtnuto, zrusime datum
                            userRegistration.setCheckInDate(null);
                        }
                    }
                    break;
            }

            registrationDao.update(userRegistration);
            return userRegistration.getCheckInDate();
        } else {
            throw new RegistrationNotActiveException();
        }
    }

    private void cancelRegistrationOrder(String registrationOrderUuid, boolean notifyUser, boolean notifyPaymentPortal, boolean fromOwner, boolean timeouted, boolean refund) throws RequiredPropertyNullException, NoRecordException, ServiceExecutionFailed {
        if (StringUtils.isBlank(registrationOrderUuid)) {
            throw new RequiredPropertyNullException("Missing registrationOrder uuid");
        }

        RegistrationOrder orderToBeCanceled = registrationOrderDao.loadOrderByUuidWithAllFetch(registrationOrderUuid);
        if (orderToBeCanceled == null) {
            log().debug("RegistrationOrder({}) not found", registrationOrderUuid);
            throw new NoRecordException("Order with uuid " + registrationOrderUuid + " not found!");
        }

        cancelRegistrationOrder(orderToBeCanceled, notifyUser, notifyPaymentPortal, fromOwner, timeouted, refund);
    }

    private void cancelUserRegistration(UserRegistration registration, boolean notifyUser, boolean notifyPortal, boolean sourceOwner, boolean timeouted, boolean refund)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException {

        if (registration == null) {
            throw new RequiredPropertyNullException("Missing registration");
        }

        boolean isConfirmed = registration.getState().equals(RegistrationState.CONFIRMED);

        //
        // Ak je confirmed tak moze byt zrusena len v pripade ze ma sucet objednavok NULA.         
        //
        if (!refund && isConfirmed) {
            List<RegistrationOrder> orders = registration.getOrders();
            for (RegistrationOrder o : orders) {
                if (o.isConfirmed() && o.getOverallPrice().compareTo(BigDecimal.ZERO) != 0) {
                    String msg = String.format("%s can't be canceled because contains confirmed order with price >0 %s. This is not supported yet.", registration, o);
                    log().error(msg);
                    throw new ServiceExecutionFailed(msg);
                }
            }
        }

        if (registration.getState().equals(RegistrationState.TIMEOUTED)) {
            log().debug("{} already canceled: TIMEOUTED", registration);
            return;
        }

        log().info("Cancel {} from owner: {} ,notifyOwner: {}, notifyPortal: ()", new Object[]{registration, sourceOwner, notifyUser, notifyPortal});
        registration.setState(timeouted ? RegistrationState.TIMEOUTED : RegistrationState.CANCELED);
        getRegistrationDao().update(registration);

        // vytovrime si objekt na zrusenie, ak bude potreba pouzije sa inak nie
        CancelOrdersRequest cancelOrderInPaymentPortal = OrderWebService.OBJECT_FACTORY.createCancelOrdersRequest();
        for (RegistrationOrder order : registration.getActiveOrders()) {
            cancelOrder(order, timeouted);
            cancelOrderInPaymentPortal.getOrderUuids().add(order.getUuid());
        }

        if (notifyPortal) {
            // Zrusime vsetky objednavky
            cancelOrderInPaymentPortal.setReason(sourceOwner ? CancelOrderReason.OWNER : CancelOrderReason.ORGANIZATOR);
            CancelOrdersResponse response = orderWebService.cancelOrders(cancelOrderInPaymentPortal);
            if (response.getException() != null) {
                String msg = String.format("Orders (%s) can't be canceled in payment portal: %s", Joiner.on(", ").join(cancelOrderInPaymentPortal.getOrderUuids()), response.getException().name());
                log().error(msg);
                throw new ServiceExecutionFailed(msg);
            }
        }

        // ak bola registracia potvrdena odstranime z S3
        if (isConfirmed) {
            removeTicketFromStore(registration);
        }

        if (notifyUser) {
            AbstractRegistrationNotification email;
            if (timeouted) {
                email = new RegistrationCanceledBySystemNotification(registration.getOrders().get(0), systemPropertyService);
            } else {
                // Posleme notifkaciu o zruseni celej registracie
                email = new RegistrationCanceledByOwnerNotification(registration.getOrders().get(0), systemPropertyService);
            }
            email.sendNotification();
        }
    }

    /**
     * Vykona zrusenie objednavky a vykona akcie ktore su vlozene cez prepinace
     * <p/>
     * @param orderToBeCanceled objednavka ktora sa ma zrusit
     * @param notifyUser priznak notifikacie uzivatela
     * @param notifyPortal priznak ze sa to ma prejavit do platobneho portalu
     * @param sourceOwner ci je zdroj vlastnik
     * @param refund ci sa jedna o refund - moze zrusit platnu objednavku
     * @throws NoRecordException
     * @throws RequiredPropertyNullException
     * @throws ServiceExecutionFailed
     */
    private void cancelRegistrationOrder(RegistrationOrder orderToBeCanceled, boolean notifyUser, boolean notifyPortal, boolean sourceOwner, boolean timeouted, boolean refund)
            throws RequiredPropertyNullException, ServiceExecutionFailed, NoRecordException {

        if (orderToBeCanceled == null) {
            throw new RequiredPropertyNullException("Missing order to be canceled ");
        }


        // Zkontrolujeme ci rusena objednavka nema nahodov halvny tiket alebo 
        // ci cela registracia neobsahuje len jednu objednavku, ak ano rusime cely 
        // tiket
        UserRegistration ur = orderToBeCanceled.getUserRegistration();
        List<RegistrationOrder> orders = ur.getActiveOrders();
        if (orders.size() == 1 || orderToBeCanceled.hasMainBundle()) {
            cancelUserRegistration(ur, notifyUser, notifyPortal, sourceOwner, timeouted, refund);
        } else {
            if (!refund && orderToBeCanceled.isConfirmed() && orderToBeCanceled.getOverallPrice().compareTo(BigDecimal.ZERO) != 0) {
                String msg = String.format("%s can't be canceled is confirmed and price is >0. This is not supported yet.", orderToBeCanceled);
                log().error(msg);
                throw new ServiceExecutionFailed(msg);
            }

            log().info("Cancel {} from owner: {} ,notifyOwner: {}, notifyPortal: {}", new Object[]{orderToBeCanceled, sourceOwner, notifyUser, notifyPortal});

            cancelOrder(orderToBeCanceled, timeouted);

            if (notifyPortal) {
                sendOrderCanceledToPaymentPortal(orderToBeCanceled);
            }

            if (notifyUser) {
                AbstractRegistrationNotification email;
                if (timeouted) {
                    email = new OrderCanceledBySystemNotification(orderToBeCanceled, systemPropertyService);
                } else {
                    email = new OrderCanceledByOwnerNotification(systemPropertyService, orderToBeCanceled);
                }
                email.sendNotification();
            }
        }
    }

    private void sendOrderCanceledToPaymentPortal(RegistrationOrder orderToBeCanceled) throws ServiceExecutionFailed {
        CancelOrdersRequest request = OrderWebService.OBJECT_FACTORY.createCancelOrdersRequest();
        request.setReason(CancelOrderReason.OWNER);
        request.getOrderUuids().add(orderToBeCanceled.getUuid());
        CancelOrdersResponse response = orderWebService.cancelOrders(request);
        if (response.getException() != null) {
            String msg = String.format("Order(%s) can't be canceled in payment portal: %s", orderToBeCanceled.getUuid(), response.getException().name());
            log().error(msg);
            throw new ServiceExecutionFailed(msg);
        }
    }

    private void cancelOrder(RegistrationOrder orderToBeCanceled, boolean timeouted) throws NoRecordException, RequiredPropertyNullException {
        if (orderToBeCanceled.getState().equals(RegistrationState.TIMEOUTED)) {
            log().debug("{} already canceled: TIMEOUTED", orderToBeCanceled);
            return;
        }

        // Odtagujeme uzivatela v NotX
        createOrRemoveOrderTags(orderToBeCanceled, false);

        // zrusime objednavku
        orderToBeCanceled.setCancelDate(DateProvider.getCurrentDate());
        orderToBeCanceled.setState(timeouted ? RegistrationState.TIMEOUTED : RegistrationState.CANCELED);
        // test na registracne kupony
        for (UserBundleRegistration ubr : orderToBeCanceled.getRegistredBundles()) {
            if (ubr.getRegistrationCoupon() != null) {
                returnDiscountUsage(ubr.getRegistrationCoupon(), 1);
                // pokud jde o interni registraci, tak smazeme i slevovy kupon
                // ??? je dostatecna podminka nebo jeste kontrolovat ze jde o interniho uzivatele
                boolean oneNoUsed = ubr.getRegistrationCoupon().isOneTimeDiscount() && ubr.getRegistrationCoupon().getUsedCount() == 0;
                if (oneNoUsed && ubr.getRegistrationCoupon().getKey().startsWith(RegistrationCoupon.INTERNAL_COUPONT_KEY_BEGIN)) {
                    couponDao.delete(ubr.getRegistrationCoupon());
                }
            }
            if (ubr.getRegistrationCycle().getBundle().isB2b()) {
                userEventService.removeUserPersonalRight(EventB2BModuleRoleModel.RIGHT_B2B_AGENT, orderToBeCanceled.getUserRegistration().getUser());
            }
        }
        log().debug("Cancel ", orderToBeCanceled);
        getRegistrationOrderDao().update(orderToBeCanceled);
    }

    private void confirmRegistrationOrder(String registrationOrderUuid, String sourceUserUuid, String note, boolean notifyUser, boolean notifyPortal, Date confirmDate)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {

        if (StringUtils.isBlank(registrationOrderUuid)) {
            throw new RequiredPropertyNullException("Missing order or paid date");
        }

        RegistrationOrder orderToBeConfirmed = registrationOrderDao.loadOrderByUuidWithAllFetch(registrationOrderUuid);
        if (orderToBeConfirmed == null) {
            String msg = String.format("%s can't be confirmed because it doesn't exists!", registrationOrderUuid);
            log().error(msg);
            throw new ServiceExecutionFailed(msg);
        }

        confirmRegistrationOrder(orderToBeConfirmed, sourceUserUuid, note, notifyUser, notifyPortal, confirmDate);

    }

    private void confirmRegistrationOrder(RegistrationOrder orderToBeConfirmed, String sourceUserUuid, String note, boolean notifyUser, boolean notifyPortal, Date confirmDate)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {

        if (orderToBeConfirmed == null) {
            throw new RequiredPropertyNullException("Missing order or user registration");
        }

        if (confirmDate == null) {
            confirmDate = DateProvider.getCurrentDate();
        }

        UserRegistration ur = orderToBeConfirmed.getUserRegistration();

        //
        // Ak je zrusena tak sa pokusime zistit ci ma uzivatel nejake dalsie objednavky 
        // ak nie tak potvrdime, pripadne porovname objednavky vzhladom na sumu.
        //
        if (ur.getState().equals(RegistrationState.CANCELED) || ur.getState().equals(RegistrationState.TIMEOUTED)) {
        } else if (orderToBeConfirmed.isConfirmed()) {
            log().info("{} is already confirmed - do nothing.", orderToBeConfirmed);
            return;
        } else if (orderToBeConfirmed.getState().equals(RegistrationState.CANCELED) || orderToBeConfirmed.getState().equals(RegistrationState.TIMEOUTED)) {
            throw new ServiceExecutionFailed();
        }


        boolean mainRegistration = orderToBeConfirmed.hasMainBundle();

        if (mainRegistration) {
            ur.setState(RegistrationState.CONFIRMED);
            getRegistrationDao().update(ur);
        }

        orderToBeConfirmed.setState(RegistrationState.CONFIRMED);
        orderToBeConfirmed.setPaidDate(confirmDate);
        registrationOrderDao.update(orderToBeConfirmed);

        if (notifyPortal) {
            ConfirmOrderRequest request = OrderWebService.OBJECT_FACTORY.createConfirmOrderRequest();
            request.setOrderUuid(orderToBeConfirmed.getUuid());
            request.setUserUuid(sourceUserUuid);
            request.setNote(note);
            ConfirmOrderResponse resp = orderWebService.confirmOrder(request);
            // ak nastane problem .. 
            if (resp.getOrderState().equals(OrderState.WAITING_TO_APPROVE)) {
                if (mainRegistration) {
                    ur.setState(RegistrationState.APPROVE);
                    getRegistrationDao().update(ur);
                }
                orderToBeConfirmed.setState(RegistrationState.APPROVE);
            }
        }

        // pregenerujeme tiket
        generateTicketToStore(orderToBeConfirmed.getUserRegistration());


        // Odtagujeme uzivatela v NotX
        confirmOrderTags(orderToBeConfirmed);

        // posleme email        
        if (notifyUser) {
            AbstractRegistrationNotification notification;

            if (mainRegistration) {
                if (orderToBeConfirmed.isTotalZero()) {
                    notification = new RegistrationZeroConfirmedNotification(orderToBeConfirmed, systemPropertyService);
                } else {
                    notification = new RegistrationConfirmedNotification(orderToBeConfirmed, systemPropertyService);
                }
            } else {
                if (orderToBeConfirmed.isTotalZero()) {
                    notification = new OrderZeroConfirmedNotification(orderToBeConfirmed, systemPropertyService);
                } else {
                    notification = new OrderConfirmedNotification(orderToBeConfirmed, systemPropertyService);
                }

            }
            notification.sendNotification();
        }
    }

    /**
     * Len upravi stav a nic neriesi.
     *
     * @param orderUuid
     * @param state
     * @param notifyUser
     * @param notifyPortal
     */
    private void updateState(String orderUuid, RegistrationState state, boolean notifyUser, boolean notifyPortal) throws RequiredPropertyNullException, NoRecordException, ServiceExecutionFailed {
        if (StringUtils.isBlank(orderUuid)) {
            throw new RequiredPropertyNullException("Missing orderuuid");
        }

        RegistrationOrder orderToBeChanged = registrationOrderDao.loadOrderByUuidWithAllFetch(orderUuid);
        if (orderToBeChanged == null) {
            String msg = String.format("%s can't be changed to state %s because it doesn't exists!", orderUuid, state.name());
            log().error(msg);
            throw new ServiceExecutionFailed(msg);
        }

        UserRegistration ur = orderToBeChanged.getUserRegistration();

        // upravime aj stav registracie
        if (ur.getOrders().size() == 1) {
            ur.setState(state);
            registrationDao.update(ur);
        }
        orderToBeChanged.setState(state);
        registrationOrderDao.update(orderToBeChanged);

        if (notifyPortal) {
            switch (state) {
                case TIMEOUTED:
                    sendOrderCanceledToPaymentPortal(orderToBeChanged);
                    break;
                default:
                    String msg = String.format("Can't notify PaymentPortal abou %s state changed to %s. Use different methods, this works only for TIMEOUTED", orderToBeChanged, state);
                    log().error(msg);
                    throw new ServiceExecutionFailed(msg);
            }
        }

        if (notifyUser) {
            // TODO na zaklade teho ci je to approve, cancel poslat spravu     
        }
    }

    /*
     * ------------------------------------------------------------------------
     * ---- H E L P M E T H O D S ----
     * ------------------------------------------------------------------------
     */
    private boolean containsOrderMainBundle(RegistrationOrder order) {
        for (UserBundleRegistration r : order.getRegistredBundles()) {
            if (!r.getRegistrationCycle().getBundle().isSubBundle()) {
                return true;
            }
        }
        return false;
    }

    private void sendNewRegistrationNotification(RegistrationOrder order) {
        if (containsOrderMainBundle(order)) {
            NewRegistrationNotification registrationRegistred = new NewRegistrationNotification(order, systemPropertyService);
            registrationRegistred.sendNotification();
        } else {
            NewOrderNotification orderEmail = new NewOrderNotification(order, systemPropertyService);
            orderEmail.sendNotification();
        }
    }

    /** spocita konecnou cenu balicku */
    public BigDecimal countFinalBundlePrice(RegistrationCycle cycle, RegistrationCoupon coupon, final int quantity)
            throws CouponCodeException, NoRecordException {

        // cena pro prvni polozku
        BigDecimal firstUserPrice = cycle.getPrice();
        if (coupon == null) {
            log().debug("No discount code inserted.");
        } else {
            if (coupon.getDiscount() < 0 || coupon.getDiscount() > 100) {
                throw new IllegalArgumentException("Discount out of boundary 0 <= discount <= 100 : " + coupon.getDiscount());
            } else if (coupon.getDiscount() == 100) {
                firstUserPrice = BigDecimal.ZERO;
            } else {
                firstUserPrice = BigDecimalUtils.countPercentage(cycle.getPrice(), new BigDecimal(coupon.getDiscount()));
            }
        }

        BigDecimal finalPrice = firstUserPrice;

        final int maxQuantity = cycle.getBundle().getQuantity();
        final int requestQuantity = quantity < 1 ? 1 : quantity;

        // ceny pro dalsi polozky
        if (requestQuantity > 1) {
            if (requestQuantity > maxQuantity) {
                throw new IllegalArgumentException(String.format("Request quantity is higher than allowed req:%d max:%d",
                        requestQuantity, maxQuantity));
            }
            for (int i = 2; i <= requestQuantity; i++) {
                finalPrice = finalPrice.add(cycle.getPrice());
            }
        }

        return finalPrice;
    }

    private void checkUserNotRegistredYet(String userUuid) throws AlreadyRegistredException {
        if (registrationDao.isUserAlreadyRegistred(userUuid)) {
            throw new AlreadyRegistredException();
        }
    }

    private RegistrationCycle loadActiveCycleForBundle(String bundleUuid) throws ServiceExecutionFailed {
        RegistrationCycle cycle;
        try {
            cycle = cycleDao.loadActiveCycleForBundle(bundleUuid);
        } catch (NoActiveCycleForBundleException ex) {
            if (log().isDebugEnabled()) {
                log().debug("Load registration cycle with bad UUID");
            }
            throw new ServiceExecutionFailed();
        }
        return cycle;
    }

    private RegistrationCoupon loadDiscountCouponForRead(String couponKey, String bundleUuid) throws NoRecordException {
        return loadCoupon(couponKey, bundleUuid, false);
    }

    private RegistrationCoupon loadCoupon(String couponKey, String bundleUuid, boolean forUpdate) throws NoRecordException {
        couponKey = StringUtils.trim(couponKey);
        RegistrationCoupon coupon = couponDao.loadCouponByKey(couponKey, bundleUuid, forUpdate);
        log().debug("Loading coupon {} for update {}", coupon, forUpdate);
        return coupon;
    }

    public void addCouponUsage(RegistrationCoupon coupon, int capacity) throws CouponCapacityException, CouponOutOfDateException {
        if (capacity < 1) {
            throw new IllegalArgumentException("Trying to reserve negative capacity! " + capacity);
        } else {
            if (coupon.getEndDate() != null) {
                Date now = DateProvider.getCurrentDate();
                if (now.after(coupon.getEndDate())) {
                    throw new CouponOutOfDateException(coupon.getEndDate());
                }
            }


            if (coupon.isInfiniteDiscount() || coupon.getUsedCount() + capacity <= coupon.getCapacity()) {
                coupon.setUsedCount(coupon.getUsedCount() + capacity);
            } else {
                throw new CouponCapacityException("Requested capacity is not acceptable.");
            }
            couponDao.update(coupon);
        }
    }

    /**
     * Moznost nahradit triggerem
     *
     * @param coupon
     * @param capacity
     * @throws CouponCapacityException
     */
    private void returnDiscountUsage(RegistrationCoupon coupon, int capacity) {
        if (capacity < 1) {
            throw new IllegalArgumentException("Trying to return negative capacity! " + capacity);
        } else {
            if (coupon.getUsedCount() - capacity < 0) {
                coupon.setUsedCount(0);
            } else {
                coupon.setUsedCount(coupon.getUsedCount() - capacity);
            }
            couponDao.update(coupon);
        }
    }

    private void removeTicketFromStore(UserRegistration registration) throws ServiceExecutionFailed {
        storageService.deleteObject(ticketBucket, registration.getUuid());
    }

    private void generateTicketToStore(UserRegistration registration) throws ServiceExecutionFailed {

        TicketData data;
        try {
            data = converter.createTicketData(registration, systemPropertyService.listAllProperties());
        } catch (Exception ex) {
            log().error("Execution failed when converting registration to TicketData", ex);
            throw new ServiceExecutionFailed("Execution failed when converting registration to TicketData");
        }


        try {
            PdfTicketGenerator gen = new PdfTicketGenerator();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            gen.createPdfTicket(out, data, true);
            storageService.createObject(ticketBucket, registration.getUuid(), String.format("Ticket - %s.pdf", data.getEventName()), AwsFileStorageService.FileType.PDF, true, new ByteArrayInputStream(out.toByteArray()), out.size());
        } catch (Exception ex) {
            log().error("Failed to create ticket!", ex);
            throw new ServiceExecutionFailed("Failed to generate ticket!");
        }
    }

    private void createOrRemoveOrderTags(RegistrationOrder order, boolean create) {
        NotxService.TaggingBuilder builder = NotX.getService().createTaggingBuilder();


        // ak obshauje tiket tak dalsie inak len budnle
        if (order.hasMainBundle()) {
            // automaticky rovno participants
            builder.addTag(NotxTags.PARTICIPANTS);
            builder.addTag(NotxTags.PARTICIPANTS, order.isConfirmed() ? NotxTags.CONFIRMED : NotxTags.PREREGISTRED);
        }

        // pre vsetky bundle vygenerujeme tag
        for (UserBundleRegistration r : order.getRegistredBundles()) {
            builder.addTag(NotxTags.PARTICIPANTS, NotxTags.BUNDLE, r.getRegistrationCycle().getBundle().getUuid());
        }

        builder.addUser(order.getUserRegistration().getUser().getUuid());
        // posleme do queue
        if (create) {
            NotX.getService().tag(builder);
        } else {
            NotX.getService().unTag(builder);
        }
    }

    private void confirmOrderTags(RegistrationOrder order) {
        // otagujeme ze je potvrdeny
        NotxService.TaggingBuilder builder = NotX.getService().createTaggingBuilder();
        builder.addUser(order.getUserRegistration().getUser().getUuid());
        builder.addTag(NotxTags.PARTICIPANTS, NotxTags.CONFIRMED);
        NotX.getService().tag(builder);

        // odtagujeme ze je predregistrovany
        builder = NotX.getService().createTaggingBuilder();
        builder.addUser(order.getUserRegistration().getUser().getUuid());
        builder.addTag(NotxTags.PARTICIPANTS, NotxTags.PREREGISTRED);
        NotX.getService().unTag(builder);
    }

    @Override
    public void updateCancelWaiting(String registrationUuid) throws ServiceExecutionFailed {
        registrationDao.updateWaitingCancel(registrationUuid);
    }

    private void addAnswers(String userUuid, RegistrationOtherInfo otherInfo) throws NoRecordException, RequiredPropertyNullException {
        for (Entry<String, String> answer : otherInfo.getAnswers().entrySet()) {
            if (StringUtils.isNotBlank(answer.getValue())) {
                qaService.addAnswer(userUuid, answer.getKey(), answer.getValue());
            }
        }
    }

    /*
     * ------------------------------------------------------------------------
     * ======================================================================
     * ------------------------------------------------------------------------
     */
    /**
     * @return the registrationDao
     */
    public UserRegistrationDao getRegistrationDao() {
        return registrationDao;
    }

    /**
     * @param registrationDao the registrationDao to set
     */
    public void setRegistrationDao(UserRegistrationDao registrationDao) {
        this.registrationDao = registrationDao;
    }

    /**
     * @return the userEventService
     */
    public UserEventService getUserEventService() {
        return userEventService;
    }

    /**
     * @param userEventService the userEventService to set
     */
    public void setUserEventService(UserEventService userEventService) {
        this.userEventService = userEventService;
    }

    /**
     * @return the cycleDao
     */
    public RegistrationCycleDao getCycleDao() {
        return cycleDao;
    }

    /**
     * @param cycleDao the cycleDao to set
     */
    public void setCycleDao(RegistrationCycleDao cycleDao) {
        this.cycleDao = cycleDao;
    }

    /**
     * @return the couponDao
     */
    public RegistrationCouponDao getCouponDao() {
        return couponDao;
    }

    /**
     * @param couponDao the couponDao to set
     */
    public void setCouponDao(RegistrationCouponDao couponDao) {
        this.couponDao = couponDao;
    }

    /**
     * @return the administrationWebService
     */
    public AdminEventWebService getAdminEventWebService() {
        return administrationWebService;
    }

    /**
     * @param administrationWebService the administrationWebService to set
     */
    public void setAdminEventWebService(AdminEventWebService administrationWebService) {
        this.administrationWebService = administrationWebService;
    }

    /**
     * @return the systemPropertyService
     */
    public SystemPropertyService getSystemPropertyService() {
        return systemPropertyService;
    }

    /**
     * @param systemPropertyService the systemPropertyService to set
     */
    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;
    }

    /**
     * @return the userBundleRegistrationDao
     */
    public UserBundleRegistrationDao getUserBundleRegistrationDao() {
        return userBundleRegistrationDao;
    }

    /**
     * @param userBundleRegistrationDao the userBundleRegistrationDao to set
     */
    public void setUserBundleRegistrationDao(UserBundleRegistrationDao userBundleRegistrationDao) {
        this.userBundleRegistrationDao = userBundleRegistrationDao;
    }

    /**
     * @return the registrationOrderDao
     */
    public RegistrationOrderDao getRegistrationOrderDao() {
        return registrationOrderDao;
    }

    /**
     * @param registrationOrderDao the registrationOrderDao to set
     */
    public void setRegistrationOrderDao(RegistrationOrderDao registrationOrderDao) {
        this.registrationOrderDao = registrationOrderDao;
    }

    /**
     * @return the orderWebService
     */
    public OrderWebService getOrderWebService() {
        return orderWebService;
    }

    /**
     * @param orderWebService the orderWebService to set
     */
    public void setOrderWebService(OrderWebService orderWebService) {
        this.orderWebService = orderWebService;
    }

    /**
     * @return the storageService
     */
    public AwsFileStorageService getStorageService() {
        return storageService;
    }

    /**
     * @param storageService the storageService to set
     */
    public void setStorageService(AwsFileStorageService storageService) {
        this.storageService = storageService;
    }

    /**
     * @return the ticketBucket
     */
    public String getTicketBucket() {
        return ticketBucket;
    }

    /**
     * @param ticketBucket the ticketBucket to set
     */
    public void setTicketBucket(String ticketBucket) {
        this.ticketBucket = ticketBucket;
    }

    /**
     * @return the converter
     */
    public TicketDataConverter getConverter() {
        return converter;
    }

    /**
     * @param converter the converter to set
     */
    public void setConverter(TicketDataConverter converter) {
        this.converter = converter;
    }

    public RegistrationBundleDao getRegistrationBundleDao() {
        return registrationBundleDao;
    }

    public void setRegistrationBundleDao(RegistrationBundleDao registrationBundleDao) {
        this.registrationBundleDao = registrationBundleDao;
    }
}
