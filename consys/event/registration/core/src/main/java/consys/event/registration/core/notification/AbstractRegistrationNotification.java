package consys.event.registration.core.notification;

import consys.common.core.mail.AbstractSystemNotification;
import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.common.utils.date.DateUtils;
import consys.event.common.api.properties.CommonProperties;
import consys.event.registration.core.bo.RegistrationOrder;
import consys.event.registration.core.bo.UserBundleRegistration;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import net.notx.client.PlaceHolders;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractRegistrationNotification extends AbstractSystemNotification {

    private SystemPropertyService propertyService;

    public AbstractRegistrationNotification(SystemPropertyService systemPropertyService, SystemMessage message, String target) {
        super(message, target);
        this.propertyService = systemPropertyService;
    }

    public void appendRegistrationOrderDetails(RegistrationOrder registrationOrder, PlaceHolders placeHolders) {
        String currency = getPropertyService().getString(CommonProperties.EVENT_CURRENCY);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        DecimalFormat frmt = new DecimalFormat(" ### ###.##");
        // vsetko defaultne placeholdere
        placeHolders.addPlaceHolder("order_uuid", registrationOrder.getUuid());
        placeHolders.addPlaceHolder("registration_uuid", registrationOrder.getUserRegistration().getUuid());
        placeHolders.addPlaceHolder("order_date", sdf.format(registrationOrder.getOrderDate()));
        placeHolders.addPlaceHolder("order_due_date", sdf.format(DateUtils.addDays(registrationOrder.getOrderDate(), 14)));
        placeHolders.addPlaceHolder("bundle_name", registrationOrder.getRegistredBundles().get(0).getRegistrationCycle().getBundle().getTitle());
        if (registrationOrder.getPaidDate() != null) {
            placeHolders.addPlaceHolder("order_paid_date", sdf.format(registrationOrder.getPaidDate()));
        }
        placeHolders.addPlaceHolder("total", currency + frmt.format(registrationOrder.getOverallPrice()));
        StringBuilder itemsHtml = new StringBuilder();
        StringBuilder itemsPlain = new StringBuilder();

        for (int i = 0; i < registrationOrder.getRegistredBundles().size(); i++) {
            UserBundleRegistration br = registrationOrder.getRegistredBundles().get(i);
            String finalPrice = br.getBundleFinalPrice().compareTo(BigDecimal.ZERO) == 0
                    ? "FREE"
                    : frmt.format(br.getBundleFinalPrice());

            itemsHtml.append("<tr><td align=\"left\" class=\"dc\">").
                    append(br.getRegistrationCycle().getBundle().getTitle()).
                    append("</td><td align=\"middle\">").
                    append(br.getQuantity()).
                    append("</td><td align=\"right\" class=\"dc\">").
                    append(finalPrice).
                    append("</td></tr>");
            itemsPlain.append(i).append(". \"").
                    append(br.getRegistrationCycle().getBundle().getTitle()).
                    append(("\", ")).append(br.getQuantity()).append("x ").
                    append(frmt.format(br.getBundleFinalPrice())).append("\n");
        }
        placeHolders.addPlaceHolder("items_html", itemsHtml.toString());
        placeHolders.addPlaceHolder("items_plain", itemsPlain.toString());
    }

    public SystemPropertyService getPropertyService() {
        return propertyService;
    }
}
