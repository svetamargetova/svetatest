package consys.event.registration.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.registration.core.bo.RegistrationCustomQuestion;

/**
 *
 * @author pepa
 */
public interface RegistrationCustomQuestionDao extends GenericDao<RegistrationCustomQuestion> {

    /** nacte informace k otazce podle uuid */
    RegistrationCustomQuestion loadQuestionByUuid(String questionUuid) throws NoRecordException;

    /** nacte kolik je jiz vytvoreno otazek */
    Long loadQuestionCount();

    /** aktualizuje poradi */
    int updateQuestionPosition(String questionUuid, int oldPosition, int newPosition);

    /** smaze vlastni objekt otazky a upravi poradi ostatnich otazek ktere se nachazi pod smazanou */
    int deleteQuestion(RegistrationCustomQuestion question);
}
