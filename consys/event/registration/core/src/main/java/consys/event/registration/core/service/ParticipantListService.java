package consys.event.registration.core.service;

import consys.event.common.core.service.list.ListService;
import consys.event.registration.core.bo.thumb.ParticipantListItem;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ParticipantListService extends ListService<ParticipantListItem, ParticipantListItem> {
}
