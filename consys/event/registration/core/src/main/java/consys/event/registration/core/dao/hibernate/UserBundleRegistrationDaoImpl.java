package consys.event.registration.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.registration.core.bo.UserBundleRegistration;
import consys.event.registration.core.dao.UserBundleRegistrationDao;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserBundleRegistrationDaoImpl extends GenericDaoImpl<UserBundleRegistration> implements UserBundleRegistrationDao {

    @Override
    public UserBundleRegistration load(Long id) throws NoRecordException {
        return load(id, UserBundleRegistration.class);
    }

    @Override
    public void deleteAllRegistrations() {
        session().createQuery("delete from UserBundleRegistration").executeUpdate();
    }
}
