package consys.event.registration.core.dao.hibernate;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableMap;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.thumb.UserEventThumb;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.RegistrationCouponList;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.event.registration.core.bo.thumb.RegistrationCouponListItem;
import consys.event.registration.core.bo.thumb.RegistrationCouponStats;
import consys.event.registration.core.dao.RegistrationCouponDao;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.LockMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCouponDaoImpl extends GenericDaoImpl<RegistrationCoupon> implements RegistrationCouponDao {

    @Override
    public RegistrationCoupon load(Long id) throws NoRecordException {
        return load(id, RegistrationCoupon.class);
    }

    @Override
    public RegistrationCoupon loadCouponByKey(String key, String bundleUuid, boolean forUpdate) throws NoRecordException {
        RegistrationCoupon coupon = (RegistrationCoupon) session().
                createQuery("from RegistrationCoupon c where c.key=:CKEY and c.bundle.uuid=:BUUID").
                setString("CKEY", key).
                setString("BUUID", bundleUuid).
                setLockMode("c", forUpdate ? LockMode.PESSIMISTIC_WRITE : LockMode.NONE).
                uniqueResult();
        if (coupon == null) {
            throw new NoRecordException("No RegistrationCoupon with key " + key + " for bundle " + bundleUuid + " exists");
        }
        return coupon;
    }

    @Override
    public boolean existsCouponWithKey(String key, String bundleUuid) {
        return !session().
                createQuery("select c.id from RegistrationCoupon c where c.bundle.uuid=:BUNDLE_UUID and c.key=:CKEY").
                setString("CKEY", key).
                setString("BUNDLE_UUID", bundleUuid).
                list().isEmpty();
    }

    @Override
    public int loadCouponUsedCount(String key, String bundleUuid) {
        int count = session().
                createQuery("select r.id from UserBundleRegistration r where r.registrationCoupon.bundle.uuid=:BUNDLE_UUID and r.registrationCoupon.key=:CKEY and (r.registrationOrder.state=0 or r.registrationOrder.state=1)").
                setString("CKEY", key).
                setString("BUNDLE_UUID", bundleUuid).
                list().size();

        return count;
    }

    @Override
    public RegistrationCouponStats loadCouponsStats(String bundleUuid) {
        List<Object[]> s = session().createSQLQuery("select count(r.id), sum((case when r.used_count > 0 then 1 else 0 end)) from registration.registration_coupon r left join registration.registration_bundle b on b.id=r.id_bundle where b.uuid=:UUID HAVING count(r.id) > 0").
                setString("UUID", bundleUuid).
                list();
        if (s == null || s.isEmpty()) {
            return new RegistrationCouponStats(0, 0);
        } else {
            return new RegistrationCouponStats(((BigInteger) s.get(0)[0]).intValue(), ((BigInteger) s.get(0)[1]).intValue());
        }
    }

    @Override
    public RegistrationCoupon loadByUuid(String uuid) throws NoRecordException {
        RegistrationCoupon coupon = (RegistrationCoupon) session().
                createQuery("from RegistrationCoupon c where  c.uuid=:UUID").
                setString("UUID", uuid).
                uniqueResult();
        if (coupon == null) {
            throw new NoRecordException("No RegistrationCoupon with uuid " + uuid);
        }
        return coupon;
    }

    @Override
    public List<RegistrationCouponListItem> listRegistrationCoupons(Constraints constraints, Paging paging) {

        List<RegistrationCoupon> coupons = discountListCriteria(constraints).
                addOrder(Order.asc("key")).
                setFetchMode("bundle", FetchMode.JOIN).
                setFirstResult(paging.getFirstResult()).
                setMaxResults(paging.getMaxResults()).
                list();

        Builder<RegistrationCouponListItem> items = ImmutableList.builder();
        Builder<Long> ids = ImmutableList.builder();
        ImmutableMap.Builder<Long, RegistrationCouponListItem> mitems = ImmutableMap.builder();
        for (RegistrationCoupon c : coupons) {
            RegistrationCouponListItem item = new RegistrationCouponListItem();
            item.setBundleUuid(c.getBundle().getUuid());
            ids.add(c.getId());
            item.setCouponUuid(c.getUuid());
            item.setCapacity(c.getCapacity());
            item.setDiscount(c.getDiscount());
            item.setEndDate(c.getEndDate());
            item.setKey(c.getKey());
            item.setUsedCount(c.getUsedCount());
            item.setUsers(new ArrayList<UserEventThumb>());
            items.add(item);
            mitems.put(c.getId(), item);
        }

        ImmutableList<RegistrationCouponListItem> out = items.build();
        if (!out.isEmpty()) {

            // dotahneme ludi ktery pouzili kupon
            List<Object[]> users = session().
                    createQuery("select ue.fullName, ue.uuid, ue.profileImagePrefix,ue.organization, ubr.registrationCoupon.id,ue.position from UserBundleRegistration ubr left join ubr.registrationOrder as o left join o.userRegistration as ur left join ur.user as ue where (o.state = 0 or o.state=1) and ubr.registrationCoupon.id in (:IDS)").
                    setParameterList("IDS", ids.build()).
                    list();

            Map<Long, RegistrationCouponListItem> map = mitems.build();
            for (Object[] user : users) {
                UserEventThumb u = new UserEventThumb();
                u.setName((String) user[0]);
                u.setUuid((String) user[1]);
                u.setImageProfileUuidPrefix((String) user[2]);
                u.setPosition((String) user[5]);
                u.setOrganization((String) user[3]);

                RegistrationCouponListItem item = map.get((Long) user[4]);
                if (item != null) {
                    item.getUsers().add(u);
                }
            }
        }

        return out;

    }

    @Override
    public Long loadRegistrationCouponCount(Constraints constraints) {
        return (Long) discountListCriteria(constraints).setProjection(Projections.rowCount()).list().get(0);
    }

    private Criteria discountListCriteria(Constraints c) {
        Criteria criteria = session().createCriteria(RegistrationCoupon.class);


        if (StringUtils.isNotBlank(c.getStringFilterValue()) && c.getStringFilterValue().length() > 1) {
            criteria.createAlias("bundle", "b").
                    add(Restrictions.eq("b.uuid", c.getStringFilterValue()));
        }

        if (c.getFilterTag() == RegistrationCouponList.Filter_NOT_USED) {
            criteria.add(Restrictions.eq("usedCount", 0));
        } else if (c.getFilterTag() == RegistrationCouponList.Filter_USED) {
            criteria.add(Restrictions.ge("usedCount", 1));
        }

        criteria.add(Restrictions.eq("deleted", false));

        return criteria;
    }

    @Override
    public void deleteAllCoupons() {
        session().createQuery("delete from RegistrationCoupon").executeUpdate();
    }

    @Override
    public void delete(RegistrationCoupon coupon) {
        coupon.setDeleted(Boolean.TRUE);
        coupon.setKey("-");
        update(coupon);
    }
}
