package consys.event.registration.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.RegistrationsList;
import consys.event.registration.core.bo.thumb.RegistrationListExportItem;
import consys.event.registration.core.bo.thumb.RegistrationListItem;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.event.registration.core.service.RegistrationsListService;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationsListServiceImpl extends AbstractService implements RegistrationsListService {

    private UserRegistrationDao registrationDao;

    @Override
    public String getTag() {
        return RegistrationsList.LIST_TAG;
    }

    @Override
    public List<RegistrationListItem> listItems(Constraints constraints, Paging paging) {
        return getRegistrationDao().listRegistrations(constraints, paging);
    }

    @Override
    public List<RegistrationListExportItem> listItemsExport(Constraints constraints, Paging paging) {
        return getRegistrationDao().listRegistrationsExport(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return getRegistrationDao().listRegistrationsCount(constraints).intValue();
    }

    /**
     * @return the registrationDao
     */
    public UserRegistrationDao getRegistrationDao() {
        return registrationDao;
    }

    /**
     * @param registrationDao the registrationDao to set
     */
    public void setRegistrationDao(UserRegistrationDao registrationDao) {
        this.registrationDao = registrationDao;
    }
}
