package consys.event.registration.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.registration.core.bo.RegistrationCustomQuestion;
import consys.event.registration.core.dao.RegistrationCustomQuestionDao;
import org.hibernate.Query;

public class RegistrationCustomQuestionDaoImpl extends GenericDaoImpl<RegistrationCustomQuestion> implements RegistrationCustomQuestionDao {

    @Override
    public RegistrationCustomQuestion load(Long id) throws NoRecordException {
        return load(id, RegistrationCustomQuestion.class);
    }

    @Override
    public RegistrationCustomQuestion loadQuestionByUuid(String questionUuid) throws NoRecordException {
        Query query = session().createQuery("from RegistrationCustomQuestion q where q.uuid=:UUID");
        query.setString("UUID", questionUuid);
        RegistrationCustomQuestion q = (RegistrationCustomQuestion) query.uniqueResult();
        if (q == null) {
            throw new NoRecordException();
        }
        return q;
    }

    @Override
    public Long loadQuestionCount() {
        Query query = session().createQuery("select count(*) from RegistrationCustomQuestion q");
        return (Long) query.uniqueResult();
    }

    @Override
    public int updateQuestionPosition(String questionUuid, int oldPosition, int newPosition) {
        Query query;

        if (oldPosition < newPosition) {
            query = session().createQuery("update RegistrationCustomQuestion q set q.questionOrder=(q.questionOrder-1) where q.questionOrder > :OLDPOS and q.questionOrder <= :NEWPOS");
        } else {
            query = session().createQuery("update RegistrationCustomQuestion q set q.questionOrder=(q.questionOrder+1) where q.questionOrder < :OLDPOS and q.questionOrder >= :NEWPOS");
        }
        query.setInteger("OLDPOS", oldPosition);
        query.setInteger("NEWPOS", newPosition);
        query.executeUpdate();

        query = session().createQuery("update RegistrationCustomQuestion q set q.questionOrder=:NEWPOS where q.uuid=:UUID");
        query.setInteger("NEWPOS", newPosition);
        query.setString("UUID", questionUuid);
        return query.executeUpdate();
    }

    @Override
    public int deleteQuestion(RegistrationCustomQuestion question) {
        Query query = session().createQuery("update RegistrationCustomQuestion q set q.questionOrder=(q.questionOrder-1) where q.questionOrder > :ORDER");
        query.setInteger("ORDER", question.getQuestionOrder());
        query.executeUpdate();

        query = session().createQuery("delete from RegistrationCustomQuestion q where q.uuid=:UUID");
        query.setString("UUID", question.getUuid());
        return query.executeUpdate();
    }
}
