package consys.event.registration.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * Vynimka ktora indikuje to ze sa cykly prekryvaju.
 *
 * @author Palo
 */
public class CyclesOverlapsException extends ConsysException{
    private static final long serialVersionUID = -8256239364550109878L;
    

    public CyclesOverlapsException() {
        super();
    }

     public CyclesOverlapsException(String message) {
	super(message);
    }

}
