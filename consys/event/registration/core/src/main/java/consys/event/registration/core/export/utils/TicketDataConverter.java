package consys.event.registration.core.export.utils;

import com.amazonaws.services.s3.model.S3Object;
import consys.common.constants.img.EventLogoImageEnum;
import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.bo.SystemProperty;
import consys.common.utils.enums.Currency;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.service.impl.SystemPropertyServiceImpl;
import consys.event.registration.core.bo.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class TicketDataConverter {

    private String administrationURL;
    private String imageS3Bucket;
    @Autowired
    private AwsFileStorageService fileStorageService;

    public TicketData createTicketData(UserRegistration ur, List<SystemProperty> properties) throws Exception {
        TicketData data = new TicketData();
        data.setUserName(ur.getUser().getFullName());
        data.setTicketCode(ur.getRegistrationNumber());

        for (SystemProperty sp : properties) {
            if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_NAME) == 0) {
                data.setEventName(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_FROM) == 0) {
                if (StringUtils.isNotBlank(sp.getValue())) {
                    data.setFrom(getDate(sp.getValue()));
                }
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_IMAGE_UUID) == 0) {
                if (StringUtils.isNotBlank(sp.getValue())) {
                    S3Object logo = fileStorageService.getObject(imageS3Bucket, EventLogoImageEnum.TICKET.getFullUuid(sp.getValue()));
                    if (logo != null) {
                        data.setEventLogo(ImageIO.read(logo.getObjectContent()));
                        logo.getObjectContent().close();
                    }
                }
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_ADDRESS) == 0) {
                data.setAddress(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_ADDRESS_PLACE) == 0) {
                data.setPlace(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_TIMEZONE) == 0) {
                data.setTimeZone(sp.getValue());
            } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_CURRENCY) == 0) {
                if (StringUtils.isNotBlank(sp.getValue())) {
                    data.setCurrency(Currency.valueOf(sp.getValue()));
                }
            }
        }

        fillTicketOrders(ur.getConfirmedOrders(), data);

        return data;
    }

    private Date getDate(String value) throws Exception {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(SystemPropertyServiceImpl.ISO_DATE_FORMAT);
            return value == null ? null : sdf.parse(value);
        } catch (ParseException ex) {
            throw new Exception("Can't parse date!", ex);
        }
    }

    /**
     * TODO: Obrazok nacitat priamo z S3
     *
     * @param imageUuid
     * @return
     */
    private String imageUrl(String imageUuid) {
        StringBuilder url = new StringBuilder(getAdministrationURL());
        url.append("/takeplace/");
        url.append("user/profile/load?id=");
        url.append(imageUuid);
        url.append("-300");
        return url.toString();
    }

    private static void fillTicketOrders(List<RegistrationOrder> orders, TicketData data) {
        for (RegistrationOrder ro : orders) {
            for (UserBundleRegistration ubr : ro.getRegistredBundles()) {
                TicketOrderItem toi = new TicketOrderItem();
                toi.setDate(ro.getOrderDate());
                toi.setTitle(ubr.getRegistrationCycle().getBundle().getTitle());
                toi.setPrice(ubr.getBundleFinalPrice());
                toi.setQuantity(ubr.getQuantity());
                data.getOrders().add(toi);
            }
        }
    }

    /**
     * @return the administrationURL
     */
    public String getAdministrationURL() {
        return administrationURL;
    }

    /**
     * @param administrationURL the administrationURL to set
     */
    public void setAdministrationURL(String administrationURL) {
        this.administrationURL = administrationURL;
    }

    /**
     * @return the imageS3Bucket
     */
    public String getImageS3Bucket() {
        return imageS3Bucket;
    }

    /**
     * @param imageS3Bucket the imageS3Bucket to set
     */
    public void setImageS3Bucket(String imageS3Bucket) {
        this.imageS3Bucket = imageS3Bucket;
    }
}
