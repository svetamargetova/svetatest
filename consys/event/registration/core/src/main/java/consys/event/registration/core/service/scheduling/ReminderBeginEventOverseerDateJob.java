package consys.event.registration.core.service.scheduling;

import consys.common.core.exception.ConsysException;
import consys.event.common.core.service.scheduling.ScheduledJob;
import consys.event.overseer.connector.EventId;

/**
 * Rozesila upominky ze zacne akce
 * @author pepa
 */
public class ReminderBeginEventOverseerDateJob extends ScheduledJob {

    @Override
    public String getName() {
        return "Reminder - Begin of Event";
    }

    @Override
    protected void doJobForEvent(EventId event) throws ConsysException {
        // TODO: podívej se jestli za dva dny nezacina akce, pokud jo tak posli vsem registrovanym mail
        //logger.debug("doing send notifications");
    }
}
