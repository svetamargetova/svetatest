package consys.event.registration.core.bo.thumb;

import consys.event.common.core.bo.thumb.UserEventThumb;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCouponListItem {

    private String couponUuid;
    private String bundleUuid;
    private String key;
    private int discount;
    private int capacity;
    private int usedCount;
    private Date endDate;
    private List<UserEventThumb> users;

    /**
     * @return the bundleUuid
     */
    public String getBundleUuid() {
        return bundleUuid;
    }

    /**
     * @param bundleUuid the bundleUuid to set
     */
    public void setBundleUuid(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the discount
     */
    public int getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(int discount) {
        this.discount = discount;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the usedCount
     */
    public int getUsedCount() {
        return usedCount;
    }

    /**
     * @param usedCount the usedCount to set
     */
    public void setUsedCount(int usedCount) {
        this.usedCount = usedCount;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the users
     */
    public List<UserEventThumb> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<UserEventThumb> users) {
        this.users = users;
    }

    /**
     * @return the couponUuid
     */
    public String getCouponUuid() {
        return couponUuid;
    }

    /**
     * @param couponUuid the couponUuid to set
     */
    public void setCouponUuid(String couponUuid) {
        this.couponUuid = couponUuid;
    }
    
    
}
