package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 * Odpoved na doplnujici otazku pri registraci
 * @author pepa
 */
public class RegistrationCustomQuestionAnswer implements ConsysObject {

    private static final long serialVersionUID = 5689982250004021225L;
    // data
    private UserRegistration registration;
    private RegistrationCustomQuestion question;
    private String answer;

    public UserRegistration getRegistration() {
        return registration;
    }

    public void setRegistration(UserRegistration registration) {
        this.registration = registration;
    }

    public RegistrationCustomQuestion getQuestion() {
        return question;
    }

    public void setQuestion(RegistrationCustomQuestion question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
