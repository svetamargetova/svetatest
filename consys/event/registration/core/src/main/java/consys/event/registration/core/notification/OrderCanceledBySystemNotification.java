package consys.event.registration.core.notification;

import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.event.registration.core.bo.RegistrationOrder;
import java.text.SimpleDateFormat;
import net.notx.client.PlaceHolders;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OrderCanceledBySystemNotification extends AbstractRegistrationNotification{
   
    private RegistrationOrder order;

    public OrderCanceledBySystemNotification(RegistrationOrder registrationOrder, SystemPropertyService systemPropertyService) {
        super(systemPropertyService, SystemMessage.ORDER_CANCELED_BY_SYSTEM, registrationOrder.getUserRegistration().getUser().getUuid());
        this.order = registrationOrder;
    }   

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        // nothing
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        placeHolders.addPlaceHolder("registration_date", sdf.format(order.getOrderDate()));                 
    }

}
