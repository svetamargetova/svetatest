package consys.event.registration.core.notification;

import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.event.registration.core.bo.RegistrationOrder;
import net.notx.client.PlaceHolders;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OrderCanceledByOwnerNotification extends AbstractRegistrationNotification{    

    public OrderCanceledByOwnerNotification(SystemPropertyService propertyService, RegistrationOrder registrationOrder) {
        super(propertyService,SystemMessage.ORDER_CANCELED_BY_OWNER, registrationOrder.getUserRegistration().getUser().getUuid());
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        // nothing
    }

    

}
