package consys.event.registration.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.date.DateProvider;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.registration.core.SystemNativeProperties;
import consys.event.registration.core.bo.RegistrationOrder;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.dao.RegistrationOrderDao;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationOrderDaoImpl extends GenericDaoImpl<RegistrationOrder> implements RegistrationOrderDao {

    @Override
    public RegistrationOrder load(Long id) throws NoRecordException {
        return load(id, RegistrationOrder.class);
    }

    @Override
    public RegistrationOrder loadOrderByUuid(String uuid) throws NoRecordException {
        RegistrationOrder order = (RegistrationOrder) session().
                createQuery("select o from RegistrationOrder o JOIN FETCH o.userRegistration  where o.uuid=:O_UUID").
                setString("O_UUID", uuid).
                uniqueResult();
        if (order == null) {
            throw new NoRecordException();
        }
        return order;
    }

    @Override
    public RegistrationOrder loadOrderByUuidWithAllFetch(String registrationOrderUuid) {
        RegistrationOrder order = (RegistrationOrder) session().
                createQuery("select o from RegistrationOrder o left join fetch o.userRegistration as r left join fetch o.registredBundles left join fetch r.orders where o.uuid=:O_UUID").
                setString("O_UUID", registrationOrderUuid).
                uniqueResult();
        return order;
    }

    @Override
    public long listActiveOrdersAfterDueDateWithRegistrationCount() {
        return (Long) session().
                createQuery("select count(o.id) from  RegistrationOrder o where o.state = :STATE and o.orderDate < :TODAY_MINUS_DUE").
                setDate("TODAY_MINUS_DUE", DateUtils.addDays(DateProvider.getCurrentDate(), -SystemNativeProperties.DUE_DATE)).
                setParameter("STATE", RegistrationState.ORDERED).
                uniqueResult();
    }

    @Override
    public List<RegistrationOrder> listActiveOrdersAfterDueDateWithRegistration(int start, int max) {
        return session().
                createQuery("select o from  RegistrationOrder o  join fetch o.userRegistration as r join fetch o.registredBundles as b join fetch b.registrationCycle as c join fetch c.bundle join fetch r.orders where o.state = :STATE and o.orderDate < :TODAY_MINUS_DUE").
                setDate("TODAY_MINUS_DUE", DateUtils.addDays(DateProvider.getCurrentDate(), -SystemNativeProperties.DUE_DATE)).
                setParameter("STATE", RegistrationState.ORDERED).
                setFirstResult(start).
                setMaxResults(max).
                list();
    }

    @Override
    public void deleteAllOrders() {
        session().createQuery("delete from RegistrationOrder").executeUpdate();
    }
}
