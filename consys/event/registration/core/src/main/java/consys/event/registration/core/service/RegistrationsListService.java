package consys.event.registration.core.service;

import consys.event.common.core.service.list.ListService;
import consys.event.registration.core.bo.thumb.RegistrationListExportItem;
import consys.event.registration.core.bo.thumb.RegistrationListItem;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationsListService extends ListService<RegistrationListItem, RegistrationListExportItem> {
}
