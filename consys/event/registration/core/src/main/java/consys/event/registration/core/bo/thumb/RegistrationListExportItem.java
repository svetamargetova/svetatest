package consys.event.registration.core.bo.thumb;

import consys.event.registration.core.bo.RegistrationState;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class RegistrationListExportItem extends UserThumbListItem {

    private String registrationUuid;
    private Date registrationDate;
    private RegistrationState state;
    private BigDecimal paid;
    private String bundleName;
    private String specialWish;
    private String email;
    private boolean subbundle;
    private boolean celiac;
    private boolean vegetarian;

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public boolean isCeliac() {
        return celiac;
    }

    public void setCeliac(boolean celiac) {
        this.celiac = celiac;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getRegistrationUuid() {
        return registrationUuid;
    }

    public void setRegistrationUuid(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    public String getSpecialWish() {
        return specialWish;
    }

    public void setSpecialWish(String specialWish) {
        this.specialWish = specialWish;
    }

    public RegistrationState getState() {
        return state;
    }

    public void setState(RegistrationState state) {
        this.state = state;
    }

    public boolean isSubbundle() {
        return subbundle;
    }

    public void setSubbundle(boolean subbundle) {
        this.subbundle = subbundle;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }
}
