package consys.event.registration.core.dao.hibernate;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.registration.core.bo.RegistrationCustomQuestionAnswer;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.dao.RegistrationCustomQuestionAnswerDao;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;

public class RegistrationCustomQuestionAnswerDaoImpl extends GenericDaoImpl<RegistrationCustomQuestionAnswer> implements RegistrationCustomQuestionAnswerDao {

    @Override
    public List<RegistrationCustomQuestionAnswer> listAllAnswersForQuestion(String questionUuid) {
        Query query = session().createQuery("from RegistrationCustomQuestionAnswer a where a.question.uuid=:QUESTION");
        query.setString("QUESTION", questionUuid);
        return query.list();
    }

    @Override
    public List<DetailedUserAnswer> listDetailedUserAnswers(String userUuid, SystemLocale systemLocale) {
        Query query = session().createQuery("select a.answer, q.questionOrder, t.question "
                + "from RegistrationCustomQuestionAnswer a "
                + "left join a.question q, "
                + "RegistrationCustomQuestionText t "
                + "where a.registration.user.uuid=:UUID and a.registration.state in (0,1,4) "
                + "and t.questionInfo.id=q.id "
                + "and t.locale=:ORDER "
                + "order by q.questionOrder");
        query.setString("UUID", userUuid);
        query.setInteger("ORDER", systemLocale.getId());
        List<Object[]> list = query.list();

        List<DetailedUserAnswer> result = new ArrayList<DetailedUserAnswer>();
        for (Object[] o : list) {
            DetailedUserAnswer dua = new DetailedUserAnswer();
            dua.setAnswer((String) o[0]);
            dua.setOrder((Integer) o[1]);
            dua.setQuestion((String) o[2]);
            result.add(dua);
        }

        return result;
    }

    @Override
    public List<RegistrationCustomQuestionAnswer> listUserAnswers(String userUuid) {
        Query query = session().createQuery("from RegistrationCustomQuestionAnswer a where a.registration.user.uuid=:UUID");
        query.setString("UUID", userUuid);
        return query.list();
    }

    @Override
    public List<RegistrationCustomQuestionAnswer> listUsersAnswers() {
        Query query = session().createQuery("from RegistrationCustomQuestionAnswer a "
                + "left join fetch a.question q left join fetch a.registration.user u where a.registration.state in (0,1,4)");
        return query.list();
    }

    @Override
    public RegistrationCustomQuestionAnswer load(Long id) throws NoRecordException {
        return load(id, RegistrationCustomQuestionAnswer.class);
    }

    @Override
    public int deleteAllAnswers() {
        return session().createQuery("delete from RegistrationCustomQuestionAnswer").executeUpdate();
    }
}
