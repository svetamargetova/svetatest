package consys.event.registration.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.core.properties.SystemPropertyUpdateHandler;
import consys.event.common.core.utils.SystemPropertyUtils;
import consys.event.registration.api.properties.RegistrationProperties;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ParticipantListVisiblePropertyHandler implements SystemPropertyUpdateHandler  {

    @Override
    public String getKey() {
        return RegistrationProperties.PARTICIPANT_LIST_VISIBILE;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
        SystemPropertyUtils.isBooleanProperty(newValue);
    }
    




}
