package consys.event.registration.core.bo;

import consys.common.utils.enums.Currency;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Prenosovy objekt data pro export tiketu do pdf
 * @author pepa
 */
public class TicketData {

    private String ticketCode;
    private String eventName;
    private Image eventLogo;
    private String userName;
    private String address;
    private String place;
    private Date from;
    private String timeZone;
    private List<TicketOrderItem> orders;
    private Currency currency;

    public TicketData() {
        orders = new ArrayList<TicketOrderItem>();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Image getEventLogo() {
        return eventLogo;
    }

    public void setEventLogo(Image eventLogo) {
        this.eventLogo = eventLogo;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(String ticketCode) {
        this.ticketCode = ticketCode;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<TicketOrderItem> getOrders() {
        return orders;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
