package consys.event.registration.core.dao;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.dao.GenericDao;
import consys.event.registration.core.bo.RegistrationCustomQuestionAnswer;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface RegistrationCustomQuestionAnswerDao extends GenericDao<RegistrationCustomQuestionAnswer> {

    /** nacte vsechny odpovedi pro zadanou otazku */
    List<RegistrationCustomQuestionAnswer> listAllAnswersForQuestion(String questionUuid);

    /** nacte uzivatelovy odpovedi vcetne otazek */
    List<DetailedUserAnswer> listDetailedUserAnswers(String userUuid, SystemLocale systemLocale);

    /** nacte vsechny uzivatelovy odpovedi */
    List<RegistrationCustomQuestionAnswer> listUserAnswers(String userUuid);

    /** nacte vsechny platne odpovedi uzivatelu */
    List<RegistrationCustomQuestionAnswer> listUsersAnswers();
    
    /** smaze vsechny odpovedi (urceno pro cisteni po naklonovani) */
    int deleteAllAnswers();
}
