package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;
import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCoupon implements ConsysObject {

    private static final long serialVersionUID = -4906939973237912094L;
    public static final int INFINITE_CAPACITY = 0;
    public static final String INTERNAL_COUPONT_KEY_BEGIN = "internal-";
    private Long id;
    private String uuid;
    /** Kluc */
    private String key;
    /** Zlava v percentach  */
    private Integer discount;
    /** Bundle kuponu */
    private RegistrationBundle bundle;
    /** Kapacita discount kodu */
    private int capacity;
    /** Pocet kolkokrat uz je kupon pouzity */
    private int usedCount;
    /** optional koniec platnosti, event date. */
    private Date endDate;
    /** je oznacen za smazany */
    private Boolean deleted;

    public RegistrationCoupon() {
        deleted = false;
    }

    public RegistrationCoupon(String key, Integer discount, int capacity) {
        this();
        this.key = key;
        this.discount = discount;
        this.capacity = capacity;
    }

    public void setDiscount(Integer discount) {
        if (discount < 0) {
            throw new IllegalArgumentException();
        }
        this.discount = discount;
    }

    /**
     * Zlava v percentach
     * @return the discount
     */
    public Integer getDiscount() {
        return discount;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the bundle
     */
    public RegistrationBundle getBundle() {
        return bundle;
    }

    /**
     * @param bundle the bundle to set
     */
    public void setBundle(RegistrationBundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        RegistrationCoupon e = (RegistrationCoupon) obj;
        return uuid.equals(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("RegistrationCoupoun[key=%s discount=%d capacity=%s used=%s end=%s uuid=%s]", key, discount, capacity, usedCount, endDate != null ? endDate.toString() : '-', uuid);
    }

    /**
     * Kapacita discount kodu
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Kapacita discount kodu
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * optional koniec platnosti, event date.
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * optional koniec platnosti, event date.
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isOneTimeDiscount() {
        return capacity == 1;
    }

    public boolean isInfiniteDiscount() {
        return capacity == INFINITE_CAPACITY;
    }

    public boolean hasFreeCapacity() {
        return isInfiniteDiscount() || usedCount < capacity;
    }

    /**
     * Pocet kolkokrat uz je kupon pouzity
     * @return the usedCount
     */
    public int getUsedCount() {
        return usedCount;
    }

    /**
     * Pocet kolkokrat uz je kupon pouzity
     * @param usedCount the usedCount to set
     */
    public void setUsedCount(int usedCount) {
        this.usedCount = usedCount;
    }

    /** je oznacen za smazany */
    public Boolean getDeleted() {
        if (deleted == null) {
            deleted = false;
        }
        return deleted;
    }

    /** oznaceni za smazany */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
