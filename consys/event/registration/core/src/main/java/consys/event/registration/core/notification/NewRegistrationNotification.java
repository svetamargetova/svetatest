package consys.event.registration.core.notification;

import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.event.registration.core.bo.RegistrationOrder;
import net.notx.client.PlaceHolders;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class NewRegistrationNotification extends AbstractRegistrationNotification {

    private final RegistrationOrder registrationOrder;

    public NewRegistrationNotification(RegistrationOrder registrationOrder, SystemPropertyService systemPropertyService) {
        super(systemPropertyService, SystemMessage.NEW_REGISTRATION, registrationOrder.getUserRegistration().getUser().getUuid());
        this.registrationOrder = registrationOrder;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        appendRegistrationOrderDetails(registrationOrder, placeHolders);
    }
}
