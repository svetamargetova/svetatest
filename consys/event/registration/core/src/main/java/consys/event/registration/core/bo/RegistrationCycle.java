package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;
import java.math.BigDecimal;
import java.util.Date;

/**
 *  
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCycle implements ConsysObject,Comparable<RegistrationCycle>{
    private static final long serialVersionUID = -7518708930102050533L;


    private Long id;
    private RegistrationBundle bundle;
    private Date from;
    private Date to;
    private BigDecimal price;
    private String uuid;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the bundle
     */
    public RegistrationBundle getBundle() {
        return bundle;
    }

    /**
     * @param bundle the bundle to set
     */
    public void setBundle(RegistrationBundle bundle) {
        this.bundle = bundle;
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

     /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
  
    @Override
    public String toString() {
        return String.format("RegistrationCycle[%s - %s , %s, %s]", from,to,price.toString(),bundle);
    }



     @Override
    public boolean equals(Object obj) {
         if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        RegistrationCycle e = (RegistrationCycle) obj;
        return uuid.equals(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(RegistrationCycle o) {
        return from.before(o.from) ? 1 : -1;
    }
}
