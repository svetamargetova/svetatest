package consys.event.registration.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.ParticipantList;
import consys.event.registration.core.bo.thumb.ParticipantListItem;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.event.registration.core.service.ParticipantListService;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ParticipantListServiceImpl extends AbstractService implements ParticipantListService {

    private UserRegistrationDao registrationDao;

    @Override
    public String getTag() {
        return ParticipantList.LIST_TAG;
    }

    @Override
    public List<ParticipantListItem> listItems(Constraints constraints, Paging paging) {
        return getRegistrationDao().listParticipants(constraints, paging);
    }

    @Override
    public List<ParticipantListItem> listItemsExport(Constraints constraints, Paging paging) {
        return listItems(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return getRegistrationDao().loadParticipantsCount(constraints).intValue();
    }

    /**
     * @return the registrationDao
     */
    public UserRegistrationDao getRegistrationDao() {
        return registrationDao;
    }

    /**
     * @param registrationDao the registrationDao to set
     */
    public void setRegistrationDao(UserRegistrationDao registrationDao) {
        this.registrationDao = registrationDao;
    }
}
