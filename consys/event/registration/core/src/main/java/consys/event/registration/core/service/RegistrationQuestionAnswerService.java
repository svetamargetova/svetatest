package consys.event.registration.core.service;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.event.registration.core.bo.RegistrationCustomQuestion;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.bo.thumb.UserAnswer;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public interface RegistrationQuestionAnswerService {

    /** prida odpoved */
    void addAnswer(String userUuid, String questionUuid, String text) throws NoRecordException, RequiredPropertyNullException;

    /** prida novou lokalizaci otazky */
    RegistrationCustomQuestionText addLocalizedText(String questionUuid, String text, SystemLocale locale) throws NoRecordException, RequiredPropertyNullException;

    /** vytvori otazku */
    RegistrationCustomQuestion createQuestion(boolean required, int order, String text) throws RequiredPropertyNullException;

    /** nacte seznam otazek (text otazky podle lokalizace + dotazene zakladni informace otazky) */
    List<RegistrationCustomQuestionText> listDetailedQuestions(SystemLocale locale);

    /** nacte vsechny odpovedi uzivatelu */
    Map<String, List<UserAnswer>> listExportUsersAnswers();

    /** nacte seznam otazek */
    List<RegistrationCustomQuestion> listQuestions();

    /** nacte odpovedi ucastnika */
    List<DetailedUserAnswer> listUserAnswers(String userUuid);

    /** nacte detailne jednu otazku */
    RegistrationCustomQuestionText loadDetaileQuestion(String questionUuid, SystemLocale locale) throws NoRecordException, RequiredPropertyNullException;

    /** vrati pocet otazek */
    Long loadQuestionsCount();

    /** aktualizuje informace k otazce */
    void updateQuestion(String questionUuid, boolean required) throws NoRecordException, RequiredPropertyNullException;

    /** aktualizuje poradi otazky */
    int updateQuestionOrder(String questionUuid, int oldPosition, int newPosition) throws NoRecordException, RequiredPropertyNullException;

    /** aktualizuje text otazky */
    void updateQuestionText(String questionUuid, String text, SystemLocale locale) throws NoRecordException, RequiredPropertyNullException;

    /** smaze odpovedi pro zadaneho uzivatele */
    void deleteAnswers(String userUuid) throws RequiredPropertyNullException;

    /** smaze otazku */
    void deleteQuestion(String questionUuid) throws NoRecordException, RequiredPropertyNullException;
}
