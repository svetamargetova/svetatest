package consys.event.registration.core.service.impl;

import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.RegistrationCouponList;
import consys.event.registration.core.bo.thumb.RegistrationCouponListItem;
import consys.event.registration.core.dao.RegistrationCouponDao;
import consys.event.registration.core.service.RegistrationCouponListService;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCouponListServiceImpl implements RegistrationCouponListService, RegistrationCouponList {

    private RegistrationCouponDao couponDao;

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public List<RegistrationCouponListItem> listItems(Constraints constraints, Paging paging) {
        return couponDao.listRegistrationCoupons(constraints, paging);
    }

    @Override
    public List<RegistrationCouponListItem> listItemsExport(Constraints constraints, Paging paging) {
        return listItems(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return couponDao.loadRegistrationCouponCount(constraints).intValue();
    }

    /**
     * @return the couponDao
     */
    public RegistrationCouponDao getCouponDao() {
        return couponDao;
    }

    /**
     * @param couponDao the couponDao to set
     */
    public void setCouponDao(RegistrationCouponDao couponDao) {
        this.couponDao = couponDao;
    }
}
