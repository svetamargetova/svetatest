package consys.event.registration.core.bo.thumb;

/**
 *
 * @author pepa
 */
public class UserThumbListItem {

    private String userName;
    private String userUuid;
    private String userPosition;
    private String userOrganization;
    private String profileImagePrefix;

    public String getProfileImagePrefix() {
        return profileImagePrefix;
    }

    public void setProfileImagePrefix(String profileImagePrefix) {
        this.profileImagePrefix = profileImagePrefix;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserOrganization() {
        return userOrganization;
    }

    public void setUserOrganization(String userOrganization) {
        this.userOrganization = userOrganization;
    }

    public String getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(String userPosition) {
        this.userPosition = userPosition;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }
}
