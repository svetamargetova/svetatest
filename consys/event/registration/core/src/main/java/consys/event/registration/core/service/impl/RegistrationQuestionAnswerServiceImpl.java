package consys.event.registration.core.service.impl;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.UuidProvider;
import consys.event.registration.core.bo.RegistrationCustomQuestion;
import consys.event.registration.core.bo.RegistrationCustomQuestionAnswer;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.bo.thumb.UserAnswer;
import consys.event.registration.core.dao.RegistrationCustomQuestionAnswerDao;
import consys.event.registration.core.dao.RegistrationCustomQuestionDao;
import consys.event.registration.core.dao.RegistrationCustomQuestionTextDao;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class RegistrationQuestionAnswerServiceImpl implements RegistrationQuestionAnswerService {

    @Autowired
    private RegistrationCustomQuestionDao questionDao;
    @Autowired
    private RegistrationCustomQuestionTextDao questionTextDao;
    @Autowired
    private RegistrationCustomQuestionAnswerDao questionAnswerDao;
    @Autowired
    private UserRegistrationDao userRegistrationDao;

    @Override
    public void addAnswer(String userUuid, String questionUuid, String text) throws NoRecordException, RequiredPropertyNullException {
        if (isBlank(userUuid, questionUuid, text)) {
            throw new RequiredPropertyNullException();
        }

        RegistrationCustomQuestion question = questionDao.loadQuestionByUuid(questionUuid);
        UserRegistration registration = userRegistrationDao.loadUserRegistrationDetailedByUserUuid(userUuid);

        RegistrationCustomQuestionAnswer answer = new RegistrationCustomQuestionAnswer();
        answer.setAnswer(text);
        answer.setQuestion(question);
        answer.setRegistration(registration);

        questionAnswerDao.create(answer);
    }

    @Override
    public RegistrationCustomQuestionText addLocalizedText(String questionUuid, String text, SystemLocale locale) throws NoRecordException, RequiredPropertyNullException {
        if (isBlank(questionUuid, text) || locale == null) {
            throw new RequiredPropertyNullException();
        }

        RegistrationCustomQuestion questionInfo = questionDao.loadQuestionByUuid(questionUuid);

        RegistrationCustomQuestionText questionText = new RegistrationCustomQuestionText();
        questionText.setLocale(locale);
        questionText.setQuestion(text);
        questionText.setQuestionInfo(questionInfo);
        questionTextDao.create(questionText);

        return questionText;
    }

    @Override
    public RegistrationCustomQuestion createQuestion(boolean required, int order, String text) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(text)) {
            throw new RequiredPropertyNullException();
        }

        RegistrationCustomQuestion question = new RegistrationCustomQuestion();
        question.setQuestionOrder(order);
        question.setRequired(required);
        question.setUuid(UuidProvider.getUuid());

        RegistrationCustomQuestionText questionText = new RegistrationCustomQuestionText();
        questionText.setLocale(SystemLocale.DEFAULT);
        questionText.setQuestion(text);
        questionText.setQuestionInfo(question);

        questionDao.create(question);
        questionTextDao.create(questionText);

        return question;
    }

    @Override
    public List<RegistrationCustomQuestionText> listDetailedQuestions(SystemLocale locale) {
        if (locale == null) {
            return questionTextDao.listDetailedQuestions(SystemLocale.DEFAULT);
        } else {
            return questionTextDao.listDetailedQuestions(locale);
        }
    }

    @Override
    public Map<String, List<UserAnswer>> listExportUsersAnswers() {
        List<RegistrationCustomQuestionAnswer> answers = questionAnswerDao.listUsersAnswers();
        Map<String, List<UserAnswer>> response = new HashMap<String, List<UserAnswer>>();
        for (RegistrationCustomQuestionAnswer answer : answers) {
            String userUuid = answer.getRegistration().getUser().getUuid();
            List<UserAnswer> list = response.get(userUuid);
            if (list == null) {
                list = new ArrayList<UserAnswer>();
                response.put(userUuid, list);
            }
            
            UserAnswer a = new UserAnswer();
            a.setAnswer(answer.getAnswer());
            a.setOrder(answer.getQuestion().getQuestionOrder());
            list.add(a);
        }
        return response;
    }

    @Override
    public List<RegistrationCustomQuestion> listQuestions() {
        return questionDao.listAll(RegistrationCustomQuestion.class);
    }

    @Override
    public List<DetailedUserAnswer> listUserAnswers(String userUuid) {
        return questionAnswerDao.listDetailedUserAnswers(userUuid, SystemLocale.DEFAULT);
    }

    @Override
    public RegistrationCustomQuestionText loadDetaileQuestion(String questionUuid, SystemLocale locale) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(questionUuid) || locale == null) {
            throw new RequiredPropertyNullException();
        }
        return questionTextDao.loadByUuidAndLocale(questionUuid, locale);
    }

    @Override
    public Long loadQuestionsCount() {
        return questionDao.loadQuestionCount();
    }

    @Override
    public void updateQuestion(String questionUuid, boolean required) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(questionUuid)) {
            throw new RequiredPropertyNullException();
        }
        RegistrationCustomQuestion question = questionDao.loadQuestionByUuid(questionUuid);
        question.setRequired(required);
        questionDao.update(question);
    }

    @Override
    public int updateQuestionOrder(String questionUuid, int oldPosition, int newPosition) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(questionUuid) || oldPosition < 1 || newPosition < 1) {
            throw new RequiredPropertyNullException();
        }
        RegistrationCustomQuestion question = questionDao.loadQuestionByUuid(questionUuid);

        if (oldPosition != question.getQuestionOrder()) {
            throw new RequiredPropertyNullException();
        }

        return questionDao.updateQuestionPosition(questionUuid, oldPosition, newPosition);
    }

    @Override
    public void updateQuestionText(String questionUuid, String text, SystemLocale locale) throws NoRecordException, RequiredPropertyNullException {
        if (isBlank(questionUuid, text) || locale == null) {
            throw new RequiredPropertyNullException();
        }
        RegistrationCustomQuestionText questionText = questionTextDao.loadByUuidAndLocale(questionUuid, locale);
        questionText.setQuestion(text);
        questionTextDao.update(questionText);
    }

    @Override
    public void deleteAnswers(String userUuid) throws RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        List<RegistrationCustomQuestionAnswer> answers = questionAnswerDao.listUserAnswers(userUuid);
        for (RegistrationCustomQuestionAnswer answer : answers) {
            questionAnswerDao.delete(answer);
        }
    }

    @Override
    public void deleteQuestion(String questionUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(questionUuid)) {
            throw new RequiredPropertyNullException();
        }

        RegistrationCustomQuestion question = questionDao.loadQuestionByUuid(questionUuid);

        // smazem odpovedi
        List<RegistrationCustomQuestionAnswer> answers = questionAnswerDao.listAllAnswersForQuestion(question.getUuid());
        for (RegistrationCustomQuestionAnswer a : answers) {
            questionAnswerDao.delete(a);
        }

        // smazeme texty otazek
        List<RegistrationCustomQuestionText> texts = questionTextDao.listAllQuestionTextsForQuestion(question.getUuid());
        for (RegistrationCustomQuestionText t : texts) {
            questionTextDao.delete(t);
        }

        // smazeme otazku
        questionDao.deleteQuestion(question);
    }

    /** pomocna metoda na kontrolu jestli jsou zadany naplnene stringy */
    private static boolean isBlank(String... strings) {
        for (String s : strings) {
            if (StringUtils.isBlank(s)) {
                return true;
            }
        }
        return false;
    }
}
