package consys.event.registration.core.module;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.event.common.core.EventOrderStateChangeListener;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.service.UserRegistrationService;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationStateChangeListener implements EventOrderStateChangeListener {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationStateChangeListener.class);
    private UserRegistrationService userRegistrationService;

    @Override
    public boolean orderStateChanged(String orderUuid, OrderState state, boolean notify, Date paidDate) {
        try {

            logger.info("Delegate registration change: {} to state {}", orderUuid, state.name());

            RegistrationState s = null;
            switch (state) {
                case APPROVE:
                    s = RegistrationState.APPROVE;
                    break;
                case CANCELED:
                    s = RegistrationState.CANCELED;
                    break;
                case CONFIRMED:
                    s = RegistrationState.CONFIRMED;
                    break;
            }
            return userRegistrationService.updateRegistrationOrderState(orderUuid, s, notify, paidDate);            
        } catch (NoRecordException ex) {
            logger.debug("No registrations to delegate change", ex);
            return false;
        } catch (RequiredPropertyNullException ex) {
            logger.debug("Missing atrtibutes", ex);
            return false;
        }
    }

    /**
     * @return the userRegistrationService
     */
    public UserRegistrationService getUserRegistrationService() {
        return userRegistrationService;
    }

    /**
     * @param userRegistrationService the userRegistrationService to set
     */
    public void setUserRegistrationService(UserRegistrationService userRegistrationService) {
        this.userRegistrationService = userRegistrationService;
    }
}
