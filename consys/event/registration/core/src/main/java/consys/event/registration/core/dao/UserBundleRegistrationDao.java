package consys.event.registration.core.dao;

import consys.common.core.dao.GenericDao;
import consys.event.registration.core.bo.UserBundleRegistration;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface UserBundleRegistrationDao extends GenericDao<UserBundleRegistration> {

    void deleteAllRegistrations();
}
