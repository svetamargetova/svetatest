package consys.event.registration.core.notification;

import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.event.common.api.properties.CommonProperties;
import consys.event.registration.core.bo.RegistrationOrder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import net.notx.client.PlaceHolders;
import org.apache.commons.lang.time.DateUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class NewRegistrationExpirationWarningNotification extends AbstractRegistrationNotification {
    
    private final RegistrationOrder registrationOrder;

    public NewRegistrationExpirationWarningNotification(RegistrationOrder registrationOrder, SystemPropertyService systemPropertyService) {
        super(systemPropertyService, SystemMessage.NEW_REGISTRATION_EXPIRATION_WARNING, registrationOrder.getUserRegistration().getUser().getUuid());
        this.registrationOrder = registrationOrder;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        String currency = getPropertyService().getString(CommonProperties.EVENT_CURRENCY);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        DecimalFormat frmt = new DecimalFormat(currency + " ### ###.##");
        // vsetko defaultne placeholdere
        placeHolders.addPlaceHolder("ticket_name", registrationOrder.getRegistredBundles().get(0).getRegistrationCycle().getBundle().getTitle());
        placeHolders.addPlaceHolder("registration_date", sdf.format(registrationOrder.getOrderDate()));
        placeHolders.addPlaceHolder("registration_fee", frmt.format(registrationOrder.getOverallPrice()));
        placeHolders.addPlaceHolder("due_date", sdf.format(DateUtils.addDays(registrationOrder.getOrderDate(), 14)));
    }
}
