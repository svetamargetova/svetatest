package consys.event.registration.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.exception.NoActiveCycleForBundleException;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationCycleDao extends GenericDao<RegistrationCycle> {

    public RegistrationCycle loadRegistrationCycleByUuid(String uuid) throws NoRecordException;

    public RegistrationCycle loadActiveCycleForBundle(String bundleUuid) throws NoActiveCycleForBundleException;

    public List<RegistrationCycle> listCyclesAfter(String bundleUuid) throws NoRecordException;

    /** vraci prvni moznost registrace */
    public Date loadFirstRegistration();

    /** vraci posledni moznost registrace */
    public Date loadLastRegistration();

    /** smaze vsechny cykly */
    public void deleteAllCycles();
}
