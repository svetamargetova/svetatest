package consys.event.registration.core.bo.thumb;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

/**
 * Thumb pre registracny bundle
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationOption {

    private String bundleUuid;
    private String cycleUuid;
    private String title;
    private String description;
    private Date toDate;
    private BigDecimal price;
    private int capacity;
    private int registred;
    private boolean withCode;
    private int maxQuantity;
    private HashMap<Date, BigDecimal> pricing;

    public RegistrationOption() {
        pricing = new HashMap<Date, BigDecimal>();
    }

    /**
     * @return the bundleUuid
     */
    public String getBundleUuid() {
        return bundleUuid;
    }

    /**
     * @param bundleUuid the bundleUuid to set
     */
    public void setBundleUuid(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    /**
     * @return the cycleUuid
     */
    public String getCycleUuid() {
        return cycleUuid;
    }

    /**
     * @param cycleUuid the cycleUuid to set
     */
    public void setCycleUuid(String cycleUuid) {
        this.cycleUuid = cycleUuid;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }

    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @return the registred
     */
    public int getRegistred() {
        return registred;
    }

    /**
     * @param registred the registred to set
     */
    public void setRegistred(int registred) {
        this.registred = registred;
    }

    /**
     * @return the withCode
     */
    public boolean isWithCode() {
        return withCode;
    }

    /**
     * @param withCode the withCode to set
     */
    public void setWithCode(boolean withCode) {
        this.withCode = withCode;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(int maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public HashMap<Date, BigDecimal> getPricing() {
        return pricing;
    }

    public void setPricing(HashMap<Date, BigDecimal> pricing) {
        this.pricing = pricing;
    }
}
