/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.registration.core.bo;

/**
 *
 * @author palo
 */
public enum RegistrationState {
    /* Zaregistrovane ale nezaplatene */
    ORDERED(0),
    /* Zaregistroavny a zaplateny */
    CONFIRMED(1),
    /* Zrusene - externe user, organizator */
    CANCELED(2),
    /* Zrusene - timeout */
    TIMEOUTED(3),
    /* Cakajuci na potvrdenie */
    APPROVE(4),
    /* Zaregistrovany (po precerpani limitu), nezaplaceny */
    WAITING(5);

    private Integer id;

    private RegistrationState(Integer id) {
        this.id = id;
    }

    public static RegistrationState fromId(Integer i){
        switch(i){
            case 0: return ORDERED;
            case 1: return CONFIRMED;
            case 2: return CANCELED;
            case 3: return TIMEOUTED;
            case 4: return APPROVE;
            case 5: return WAITING;
        }
        throw new RuntimeException();
    }

    public Integer getId(){
        return id;
    }


}
