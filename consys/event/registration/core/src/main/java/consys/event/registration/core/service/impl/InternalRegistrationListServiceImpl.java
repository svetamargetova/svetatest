package consys.event.registration.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.InternalRegistrationListApi;
import consys.event.registration.core.bo.thumb.InternalRegistrationListItem;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.event.registration.core.service.InternalRegistrationListService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class InternalRegistrationListServiceImpl extends AbstractService implements InternalRegistrationListService {

    @Autowired
    private UserRegistrationDao userRegistrationDao;

    @Override
    public String getTag() {
        return InternalRegistrationListApi.LIST_TAG;
    }

    @Override
    public List<InternalRegistrationListItem> listItems(Constraints constraints, Paging paging) {
        return userRegistrationDao.listInternalRegistrations(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return userRegistrationDao.listInternalRegistrationsCount(constraints).intValue();
    }

    @Override
    public List<InternalRegistrationListItem> listItemsExport(Constraints constraints, Paging paging) {
        return userRegistrationDao.listInternalRegistrations(constraints, paging);
    }
}
