package consys.event.registration.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.registration.core.dao.*;
import consys.event.registration.core.service.RegistrationCloneClearService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class RegistrationCloneClearServiceImpl extends AbstractService implements RegistrationCloneClearService {

    // servicy
    @Autowired
    private RegistrationCycleDao cycleDao;
    @Autowired
    private UserBundleRegistrationDao userBundleDao;
    @Autowired
    private UserRegistrationDao userRegistrationDao;
    @Autowired
    private RegistrationOrderDao orderDao;
    @Autowired
    private RegistrationCouponDao couponDao;
    @Autowired
    private RegistrationBundleDao registrationBundleDao;
    @Autowired
    private RegistrationCustomQuestionAnswerDao questionAnswerDao;

    @Override
    public void clearClone() {
        registrationBundleDao.resetRegisteredCounters();
        
        // smaze vsechny odpovedi
        questionAnswerDao.deleteAllAnswers();
        
        // smazeme vsechny zaregistrovane balicky (zavislost id_reg_cycle, id_coupon, id_registration_order) - registration.user_bundle_registration
        userBundleDao.deleteAllRegistrations();

        // smazat vsechny objednavky (zavislost id_user_registration) - registration.registration_order
        orderDao.deleteAllOrders();

        // smazeme vsechny registrace (zavislost id_user_event) - registration.user_registration
        userRegistrationDao.deleteAllRegistrations();

        // smazat vsechny cykly registraci (zavislost id_bundle) - registration_cycle
        cycleDao.deleteAllCycles();

        // smazat slevove klice (zavislost id_bundle) - registration.registration_coupon
        couponDao.deleteAllCoupons();
    }
}
