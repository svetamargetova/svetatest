package consys.event.registration.core.bo.thumb;

import consys.event.registration.core.bo.RegistrationState;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationListItem extends UserThumbListItem {

    private String registrationUuid;
    private Date registrationDate;
    private RegistrationState state;
    private BigDecimal paid;
    private String bundleName;
    private int quantity;

    /**
     * @return the registrationUuid
     */
    public String getRegistrationUuid() {
        return registrationUuid;
    }

    /**
     * @param registrationUuid the registrationUuid to set
     */
    public void setRegistrationUuid(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    /**
     * @return the state
     */
    public RegistrationState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(RegistrationState state) {
        this.state = state;
    }

    /**
     * @return the paid
     */
    public BigDecimal getPaid() {
        return paid;
    }

    /**
     * @param paid the paid to set
     */
    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    /**
     * @return the bundleName
     */
    public String getBundleName() {
        return bundleName;
    }

    /**
     * @param bundleName the bundleName to set
     */
    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    /**
     * @return the registrationDate
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * @param registrationDate the registrationDate to set
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }
}
