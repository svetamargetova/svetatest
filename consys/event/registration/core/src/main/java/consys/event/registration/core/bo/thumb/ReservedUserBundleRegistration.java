package consys.event.registration.core.bo.thumb;

import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCoupon;
import java.math.BigDecimal;

/**
 * Schranka rezervovanej registracie.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReservedUserBundleRegistration {

    private RegistrationBundle bundle;
    private RegistrationCoupon coupon;
    private BigDecimal total;
    private int quantity;

    public RegistrationBundle getBundle() {
        return bundle;
    }

    public void setBundle(RegistrationBundle bundle) {
        this.bundle = bundle;
    }

    public RegistrationCoupon getDiscountCoupon() {
        return coupon;
    }

    public void setDiscountCoupon(RegistrationCoupon discountCoupon) {
        this.coupon = discountCoupon;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
