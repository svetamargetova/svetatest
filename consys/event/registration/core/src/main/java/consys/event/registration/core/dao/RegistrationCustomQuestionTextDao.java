package consys.event.registration.core.dao;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface RegistrationCustomQuestionTextDao extends GenericDao<RegistrationCustomQuestionText> {

    /** nacte vsechny texty pro zadanou otazku */
    List<RegistrationCustomQuestionText> listAllQuestionTextsForQuestion(String questionUuid);

    /** nacte seznam otazek s detailnimi informacemi o otazce (uuid, poradi, ...) */
    List<RegistrationCustomQuestionText> listDetailedQuestions(SystemLocale locale);

    /** nacte text otazky podle uuid a typu lokalizace */
    RegistrationCustomQuestionText loadByUuidAndLocale(String questionUuid, SystemLocale locale) throws NoRecordException;
}
