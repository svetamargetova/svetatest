package consys.event.registration.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class AlreadyRegistredException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    private String bundleUuid;

    public AlreadyRegistredException() {
        super();
    }

     public AlreadyRegistredException(String bundleUuid) {
	super();
        this.bundleUuid = bundleUuid;
    }

    /**
     * @return the bundleUuid
     */
    public String getBundleUuid() {
        return bundleUuid;
    }

}
