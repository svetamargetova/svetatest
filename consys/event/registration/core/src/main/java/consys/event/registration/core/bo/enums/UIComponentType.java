package consys.event.registration.core.bo.enums;

/**
 * Typ graficke komponenty pro vstup
 * @author pepa
 */
public enum UIComponentType {

    /** jednoradkovy textovy vstup */
    INPUT(1);
    // data
    private Integer id;

    private UIComponentType(Integer id) {
        this.id = id;
    }

    public static UIComponentType fromId(Integer i) {
        switch (i) {
            case 1:
                return INPUT;
        }
        throw new RuntimeException();
    }

    public Integer getId() {
        return id;
    }
}
