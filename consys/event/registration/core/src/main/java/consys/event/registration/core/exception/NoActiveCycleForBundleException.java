package consys.event.registration.core.exception;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class NoActiveCycleForBundleException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public NoActiveCycleForBundleException() {
        super();
    }

     public NoActiveCycleForBundleException(String message) {
	super(message);
    }

}
