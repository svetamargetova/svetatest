package consys.event.registration.core.service;

import consys.event.common.core.service.list.ListService;
import consys.event.registration.core.bo.thumb.InternalRegistrationListItem;

/**
 *
 * @author pepa
 */
public interface InternalRegistrationListService extends ListService<InternalRegistrationListItem, InternalRegistrationListItem> {
}
