package consys.event.registration.core.export.utils;

import com.google.zxing.*;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.codec.PngImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.CharacterCodingException;
import java.util.Hashtable;

/**
 *
 * @author pepa
 */
public class BarCodeGenerator {

    // konstanty
    private static final int SIZE_QR = 200;
    private static final int SIZE_128_W = 900;
    private static final int SIZE_128_H = 50;
    // data
    private BarcodeFormat format;
    private int w;
    private int h;

    public BarCodeGenerator(BarcodeFormat format) {
        this.format = format;

        if (format == BarcodeFormat.QR_CODE) {
            w = SIZE_QR;
            h = SIZE_QR;
        } else if (format == BarcodeFormat.CODE_128) {
            // max 80 znaku
            w = SIZE_128_W;
            h = SIZE_128_H;
        } else {
            throw new IllegalArgumentException("Unsupported barcode format");
        }
    }

    public Image generateCode(final String text) throws IOException {
        try {
            ByteArrayOutputStream barStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(generateMatrix(text), "PNG", barStream);
            return PngImage.getImage(barStream.toByteArray());
        } catch (CharacterCodingException ex) {
            throw new IOException("Errow while generate matrix (CharacterCoding)", ex);
        } catch (UnsupportedEncodingException ex) {
            throw new IOException("Errow while generate matrix (UnsuportedEncoding)", ex);
        } catch (WriterException ex) {
            throw new IOException("Errow while generate matrix (Writer)", ex);
        }
    }

    private BitMatrix generateMatrix(final String text) throws CharacterCodingException, UnsupportedEncodingException, WriterException {
        String data = preprocessInput(text);
        Writer writer = new MultiFormatWriter();
        Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>(1);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        return writer.encode(data, format, w, h, hints);
    }

    private String preprocessInput(final String text) {
        final String editedText;
        if (format == BarcodeFormat.QR_CODE) {
            editedText = text;
        } else {
            editedText = text.replaceAll("[^0-9a-zA-Z]", "X");
        }
        return editedText;
    }
}
