package consys.event.registration.core.notification;

import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.event.registration.core.bo.RegistrationOrder;
import net.notx.client.PlaceHolders;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class NewOrderNotification extends AbstractRegistrationNotification {
    
    private final RegistrationOrder registrationOrder;

    public NewOrderNotification(RegistrationOrder registrationOrder, SystemPropertyService systemPropertyService) {
        super(systemPropertyService, SystemMessage.NEW_ORDER, registrationOrder.getUserRegistration().getUser().getUuid());
        this.registrationOrder = registrationOrder;
    }

    @Override
    protected void fillPlaceholders(PlaceHolders placeHolders) {        
        appendRegistrationOrderDetails(registrationOrder, placeHolders);    
    }    
}
