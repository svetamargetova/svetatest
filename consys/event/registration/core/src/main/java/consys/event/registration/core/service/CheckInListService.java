package consys.event.registration.core.service;

import consys.event.common.core.service.list.ListService;
import consys.event.registration.core.bo.thumb.CheckInListItem;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface CheckInListService extends ListService<CheckInListItem, CheckInListItem> {
}
