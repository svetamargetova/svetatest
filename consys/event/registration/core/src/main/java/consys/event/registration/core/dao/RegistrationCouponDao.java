package consys.event.registration.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.event.registration.core.bo.thumb.RegistrationCouponListItem;
import consys.event.registration.core.bo.thumb.RegistrationCouponStats;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationCouponDao extends GenericDao<RegistrationCoupon> {

    public RegistrationCoupon loadByUuid(String uuid) throws NoRecordException;

    /**
     * Nacita registracny kupon podla kluca pre dany bundle.
     */
    public RegistrationCoupon loadCouponByKey(String key, String bundleUuid, boolean forUpdate) throws NoRecordException;

    public RegistrationCouponStats loadCouponsStats(String bundleUuid);

    /**
     * Nacita kolko krat je kupom pouzity v aktivne registracii REGISTRED,VALIDATED
     */
    public int loadCouponUsedCount(String key, String bundleUuid);

    public boolean existsCouponWithKey(String key, String bundleUuid);

    /*------------------------------------------------------------------------*/
    /* ----  D I S C O U  N T    L I S T    ---- */
    /*------------------------------------------------------------------------*/
    public List<RegistrationCouponListItem> listRegistrationCoupons(Constraints constraints, Paging paging);

    public Long loadRegistrationCouponCount(Constraints constraints);

    /** odstrani vsechny cykly */
    void deleteAllCoupons();
}
