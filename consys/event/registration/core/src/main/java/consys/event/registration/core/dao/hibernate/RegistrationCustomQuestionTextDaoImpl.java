package consys.event.registration.core.dao.hibernate;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.dao.RegistrationCustomQuestionTextDao;
import java.util.List;
import org.hibernate.Query;

public class RegistrationCustomQuestionTextDaoImpl extends GenericDaoImpl<RegistrationCustomQuestionText> implements RegistrationCustomQuestionTextDao {

    @Override
    public List<RegistrationCustomQuestionText> listAllQuestionTextsForQuestion(String questionUuid) {
        Query query = session().createQuery("from RegistrationCustomQuestionText t where t.questionInfo.uuid=:QUESTION");
        query.setString("QUESTION", questionUuid);
        return query.list();
    }

    @Override
    public List<RegistrationCustomQuestionText> listDetailedQuestions(SystemLocale locale) {
        Query query = session().createQuery("from RegistrationCustomQuestionText t left join fetch t.questionInfo q where t.locale=:LOCALE order by q.questionOrder");
        query.setInteger("LOCALE", locale.getId());
        return query.list();
    }

    @Override
    public RegistrationCustomQuestionText load(Long id) throws NoRecordException {
        return load(id, RegistrationCustomQuestionText.class);
    }

    @Override
    public RegistrationCustomQuestionText loadByUuidAndLocale(String questionUuid, SystemLocale locale) throws NoRecordException {
        Query query = session().createQuery("from RegistrationCustomQuestionText t left join fetch t.questionInfo q where t.locale=:LOCALE and t.questionInfo.uuid=:UUID");
        query.setInteger("LOCALE", locale.getId());
        query.setString("UUID", questionUuid);

        RegistrationCustomQuestionText questionText = (RegistrationCustomQuestionText) query.uniqueResult();
        if (questionText == null) {
            throw new NoRecordException();
        }
        return questionText;
    }
}
