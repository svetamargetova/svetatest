package consys.event.registration.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.common.utils.collection.Lists;
import consys.event.common.core.dao.hibernate.GenericDaoImpl;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.dao.RegistrationBundleDao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.List;
import org.hibernate.jdbc.Work;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationBundleDaoImpl extends GenericDaoImpl<RegistrationBundle> implements RegistrationBundleDao {

    @Override
    public RegistrationBundle load(Long id) throws NoRecordException {
        return load(id, RegistrationBundle.class);
    }

    @Override
    public List<RegistrationBundle> listSubBundlesWithCycles() throws NoRecordException {
        return listBundlesWithCycles(true);
    }

    @Override
    public List<RegistrationBundle> listMainBundlesWithCycles() throws NoRecordException {
        return listBundlesWithCycles(false);
    }

    @Override
    public List<RegistrationBundle> listAllBundles() throws NoRecordException {
        List<RegistrationBundle> out =
                session().
                createQuery("select r from RegistrationBundle r order by r.title").
                list();
        if (out == null || out.isEmpty()) {
            throw new NoRecordException();
        }
        return out;
    }

    @Override
    public RegistrationBundle loadBundleWithCyclesByUuid(String uuid) throws NoRecordException {
        RegistrationBundle rb = (RegistrationBundle) session().
                createQuery("from RegistrationBundle b  LEFT JOIN FETCH b.cycles where b.uuid=:BUNDLE_UUID").
                setString("BUNDLE_UUID", uuid).
                uniqueResult();
        if (rb == null) {
            throw new NoRecordException("No registration bundle for uuid" + uuid);
        }
        return rb;
    }

    @Override
    public Long loadBundlesCount(boolean subbundle) {
        return (Long) session().
                createQuery("select count(*) from RegistrationBundle where subBundle=:SUBBUNDLE").
                setBoolean("SUBBUNDLE", subbundle).
                uniqueResult();
    }

    @Override
    public RegistrationBundle loadBundleByUuid(String uuid) throws NoRecordException {
        RegistrationBundle rb = (RegistrationBundle) session().
                createQuery("from RegistrationBundle b where b.uuid=:BUNDLE_UUID").
                setString("BUNDLE_UUID", uuid).
                uniqueResult();
        if (rb == null) {
            throw new NoRecordException("No registration bundle for uuid" + uuid);
        }
        return rb;
    }

    @Override
    public RegistrationBundle loadBundleWithCyclesByCycleUuid(String cycleUuid) throws NoRecordException {
        RegistrationBundle rb = (RegistrationBundle) session().
                createQuery("select b from RegistrationBundle b LEFT JOIN FETCH b.cycles as cycle where b.uuid=(select c.bundle.uuid from RegistrationCycle c where c.uuid=:CYCLE_UUID)").
                setString("CYCLE_UUID", cycleUuid).
                uniqueResult();
        if (rb == null) {
            throw new NoRecordException("No registration bundle for cycle uuid" + cycleUuid);
        }
        return rb;
    }

    @Override
    public List<RegistrationOption> listSubbundlesWithMainBundle(String mainBundleUuid) throws NoRecordException {
        List<RegistrationBundle> out =
                session().
                createQuery("select distinct r from RegistrationBundle r LEFT JOIN FETCH r.cycles where r.subBundle=true or r.uuid=:BUNDLE_UUID order by r.order").
                setString("BUNDLE_UUID", mainBundleUuid).
                list();
        if (out == null || out.isEmpty()) {
            throw new NoRecordException();
        }

        // presunieme hlavny balicek na prve miesto ak tam nie je
        if (out.get(0).isSubBundle()) {
            for (int mainIdx = 0; mainIdx < out.size(); mainIdx++) {
                if (!out.get(mainIdx).isSubBundle()) {
                    RegistrationBundle mainBundle = out.remove(mainIdx);
                    out.add(0, mainBundle);
                    break;
                }
            }
        }

        List<RegistrationOption> options = bundleListToRegistrationOptions(out);
        return options;
    }

    @Override
    public List<RegistrationOption> listRegistrationOptions(boolean subbundles) throws NoRecordException {
        List<RegistrationBundle> bundles = subbundles ? listSubBundlesWithCycles() : listMainBundlesWithCycles();
        List<RegistrationOption> options = bundleListToRegistrationOptions(bundles);
        return options;

    }

    @Override
    public List<RegistrationOption> listUserNotRegistredSubbundlesOptions(String userUuid) throws NoRecordException {
        throw new UnsupportedOperationException("Nie je podporovana - dotaz by bol prilis drahy, treba pouzit filtrovanie na urovni klientskeho kodu.");

    }

    private List<RegistrationOption> bundleListToRegistrationOptions(List<RegistrationBundle> bundles) {
        List<RegistrationOption> options = Lists.newArrayListWithCapacity(bundles.size());
        for (RegistrationBundle bundle : bundles) {
            RegistrationOption option = new RegistrationOption();
            option.setBundleUuid(bundle.getUuid());
            option.setCapacity(bundle.getCapacity());
            option.setDescription(bundle.getDescription());
            option.setRegistred(bundle.getRegistred());
            option.setTitle(bundle.getTitle());
            option.setWithCode(bundle.isWithCode());
            option.setMaxQuantity(bundle.getQuantity());
            // Najdeme cyklus , ak nema nevkladame do 
            Date now = DateProvider.getCurrentDate();
            boolean found = false;
            for (RegistrationCycle c : bundle.getCycles()) {
                if (!found) {
                    if (DateUtils.isDateInRange(now, c.getFrom(), c.getTo())) {
                        option.setCycleUuid(c.getUuid());
                        option.setPrice(c.getPrice());
                        option.setToDate(c.getTo());
                        options.add(option);
                        found = true;
                    }
                }
                option.getPricing().put(c.getTo(), c.getPrice());
            }

            // ak sa jedna o hlavny balik tak ten tam ide vzdy
            if (!found && !bundle.isSubBundle()) {
                options.add(option);
            }
        }
        return options;
    }

    @Override
    public List<RegistrationBundle> listMainBundles() throws NoRecordException {
        return listBundles(false);
    }

    @Override
    public List<RegistrationBundle> listSubBundles() throws NoRecordException {
        return listBundles(true);
    }

    private List<RegistrationBundle> listBundlesWithCycles(boolean subBundle) throws NoRecordException {
        List<RegistrationBundle> out =
                session().
                createQuery("select distinct r from RegistrationBundle r LEFT JOIN FETCH r.cycles where r.subBundle=:SUBBUNDLE order by r.order").
                setBoolean("SUBBUNDLE", subBundle).
                list();
        if (out == null || out.isEmpty()) {
            throw new NoRecordException();
        }
        return out;
    }

    private List<RegistrationBundle> listBundles(boolean subBundle) throws NoRecordException {
        List<RegistrationBundle> out =
                session().
                createQuery("select r from RegistrationBundle r where r.subBundle=:SUBBUNDLE order by r.order").
                setBoolean("SUBBUNDLE", subBundle).
                list();
        if (out == null || out.isEmpty()) {
            throw new NoRecordException();
        }
        return out;
    }

    @Override
    public Integer loadBundleRegistrationsCount(Long bundleId) {
        return (Integer) session().createQuery("select r.registred from RegistrationBundle r where r.id=:B_ID").setLong("B_ID", bundleId).uniqueResult();
    }

    @Override
    public boolean checkBundlePosition(String uuid, int position) {
        Object resp = session().
                createQuery("select CASE WHEN b.order=:POS THEN true ELSE false END from RegistrationBundle b where b.uuid=:BUUID").
                setString("BUUID", uuid).
                setInteger("POS", position).
                uniqueResult();
        return resp == null ? false : (Boolean) resp;

    }

    @Override
    public void updateBundlePosition(String uuid, int oldPosition, int newPosition, boolean subbundle) {



        if (oldPosition < newPosition) {
            // update bundle set order=order-1 where order <= old+1 && order >= new        
            session().
                    createQuery("update RegistrationBundle b set b.order=b.order-1 where b.order > :OPOS and b.order <= :NPOS and b.subBundle=:SUBBUNDLE").
                    setInteger("OPOS", oldPosition).
                    setInteger("NPOS", newPosition).
                    setBoolean("SUBBUNDLE", subbundle).
                    executeUpdate();

        } else {
            // update bundle set order=order+1 where order >= new && order < old
            session().
                    createQuery("update RegistrationBundle b set b.order=b.order+1 where b.order < :OPOS and b.order >= :NPOS and b.subBundle=:SUBBUNDLE").
                    setInteger("OPOS", oldPosition).
                    setInteger("NPOS", newPosition).
                    setBoolean("SUBBUNDLE", subbundle).
                    executeUpdate();
        }

        int result = session().
                createQuery("update RegistrationBundle bundle set bundle.order=:NPOS where bundle.uuid=:BUUID").
                setInteger("NPOS", newPosition).
                setString("BUUID", uuid).
                executeUpdate();
        if (result != 1) {
            throw new IllegalArgumentException("Update bundle position failed!");
        }
    }

    @Override
    public int updateUserB2bAgentRightsForBundle(boolean add, Long bundleId) {
        UpdateB2bRightWork work = new UpdateB2bRightWork(bundleId, add);
        session().doWork(work);
        return work.getUpdated();
    }

    private final class UpdateB2bRightWork implements Work {

        private final Long bundleId;
        private final boolean add;
        private int updated;

        public UpdateB2bRightWork(Long bundleId, boolean add) {
            this.bundleId = bundleId;
            this.add = add;
        }

        @Override
        public void execute(Connection connection) throws SQLException {
            CallableStatement call = connection.prepareCall("{ ? = call registration.append_B2B_right_to_all_active(?,?) }");
            call.registerOutParameter(1, Types.INTEGER); // or whatever it is                
            call.setLong(2, bundleId);
            call.setBoolean(3, add);
            call.execute();
            updated = call.getInt(1); // propagate
        }

        public int getUpdated() {
            return updated;
        }
    }

    @Override
    public void resetRegisteredCounters() {
        session().createSQLQuery("update registration.registration_bundle set registred=0").executeUpdate();
    }
}
