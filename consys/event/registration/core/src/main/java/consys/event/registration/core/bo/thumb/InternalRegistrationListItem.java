package consys.event.registration.core.bo.thumb;

/**
 *
 * @author pepa
 */
public class InternalRegistrationListItem extends UserThumbListItem {

    private String registrationUuid;
    private String bundleName;
    private Integer quantity;

    public String getRegistrationUuid() {
        return registrationUuid;
    }

    public void setRegistrationUuid(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public Integer getQuantity() {
        if (quantity == null) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
