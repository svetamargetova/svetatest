package consys.event.registration.core.service.scheduling;

import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.service.scheduling.ScheduledJob;
import consys.event.overseer.connector.EventId;
import consys.event.registration.core.service.UserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Rusi objednavky po dobe splatnosti
 * @author palo
 */
public class CancelOrdersAfterDueDateJob extends ScheduledJob {

    @Autowired
    private UserRegistrationService registrationService;

    @Override
    protected void doJobForEvent(EventId event) throws ServiceExecutionFailed {
        registrationService.updateAndCancelAfterDueDateOrders();
    }

    @Override
    protected String getName() {
        return "Cancel orders after due date.";
    }
}
