package consys.event.registration.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.registration.core.bo.RegistrationOrder;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface RegistrationOrderDao extends GenericDao<RegistrationOrder> {

    RegistrationOrder loadOrderByUuid(String uuid) throws NoRecordException;

    RegistrationOrder loadOrderByUuidWithAllFetch(String registrationOrderUuid);
    
    long listActiveOrdersAfterDueDateWithRegistrationCount();
    
    List<RegistrationOrder> listActiveOrdersAfterDueDateWithRegistration(int start, int max);
    
    void deleteAllOrders();
}
