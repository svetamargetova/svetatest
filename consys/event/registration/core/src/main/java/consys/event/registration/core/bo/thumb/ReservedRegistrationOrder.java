package consys.event.registration.core.bo.thumb;

import consys.common.utils.collection.Lists;
import java.math.BigDecimal;
import java.util.List;



/**
 * Schranka rezervovanej registracie.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ReservedRegistrationOrder {
    

    private List<ReservedUserBundleRegistration> registrations;
    private BigDecimal total;

    public ReservedRegistrationOrder() {
        registrations = Lists.newArrayList();
        total = BigDecimal.ZERO;
    }

    /**
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * @return the registrations
     */
    public List<ReservedUserBundleRegistration> getRegistrations() {
        return registrations;
    }
}
