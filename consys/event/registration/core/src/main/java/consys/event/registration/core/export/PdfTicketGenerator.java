package consys.event.registration.core.export;

import com.google.zxing.BarcodeFormat;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import consys.common.utils.enums.Currency;
import consys.event.export.pdf.FontProvider;
import consys.event.registration.core.bo.TicketData;
import consys.event.registration.core.bo.TicketOrderItem;
import consys.event.registration.core.export.utils.BarCodeGenerator;
import consys.event.export.pdf.PdfA4Utils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.SystemUtils;

/**
 *
 * @author Hubr
 */
public class PdfTicketGenerator {

    // konstanty
    private static final float TICKET_INNER_WIDTH_MM = 200;
    private static final BaseColor COLOR_LIGHT_GRAY = new BaseColor(230, 230, 230);
    private static final BaseColor COLOR_GRAY = new BaseColor(210, 211, 213);
    private static final BaseColor COLOR_WHITE = new BaseColor(255, 255, 255);
    private static final float ROW_HEIGHT_FONT13 = 5.6f;
    private static final float PARAGRAPH_SPACING = 1.5f;
    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String HOUR_FORMAT = "HH:mm";
    // data
    private Document document;
    private PdfWriter writer;
    private TicketData ticketData;

    private void addMetadata() {
        document.addTitle("Event Ticket: " + ticketData.getEventName());
        document.addAuthor("Takeplace");
        document.addCreator("Takeplace");
        document.setMargins(0, 0, 0, 0);
    }

    private void addQRCode(float endInfo) throws IOException, DocumentException {
        final float xy = 6.5f;
        final float width = 36;
        final float height = endInfo - xy;

        drawWhiteBackground(xy, xy, width, height);

        BarCodeGenerator barCodeGenerator = new BarCodeGenerator(BarcodeFormat.QR_CODE);
        Image pngImage = barCodeGenerator.generateCode(ticketData.getTicketCode());

        ColumnText ct = PdfA4Utils.columnText(writer, xy, xy, width, width);
        ct.addElement(pngImage);
        ct.go();
    }

    private void addEventLogo() throws DocumentException, BadElementException, IOException {
        final float x = 44;
        final float y = 6.5f;
        final float width = 113;
        final float height = 13;

        drawWhiteBackground(x, y, width, height);

        if (ticketData.getEventLogo() != null) {
            PdfA4Utils.drawImage(writer, ticketData.getEventLogo(), x, y, width, height);
        } else {
            Font font = FontProvider.font(FontProvider.FontStyle.ARIAL_11_BOLD_ITALICS);
            font.setColor(COLOR_LIGHT_GRAY);

            ColumnText ct = PdfA4Utils.columnText(writer, x + 2, y + 2, width - 2, height - 2);
            ct.addElement(new Paragraph("NO EVENT LOGO", font));
            ct.go();
        }
    }

    private void addTakeplaceLogo() throws DocumentException, IOException {
        drawGrayBackground(5, 16);
        drawWhiteBackground(158.5f, 6.5f, 45, 13);
        writer.getDirectContent().addTemplate(templateTakeplaceLogo(), PdfA4Utils.xps(0), PdfA4Utils.yps(0));
    }

    private float addInfo() throws DocumentException, IOException {
        final float x = 44;
        final float y = 21;
        final float ctx = x + 2;
        final float ctw = 155.5f;
        int rows = 3;

        ColumnText ct = PdfA4Utils.columnText(writer, ctx, y, ctw, ROW_HEIGHT_FONT13 * rows);
        final float yLine = ct.getYLine();
        prepareAddInfo(ct, yLine);

        float lowestPosition;
        if (ct.go(true) == ColumnText.NO_MORE_TEXT) {
            lowestPosition = drawAddInfo(ct, yLine, x, y, rows);
        } else {
            do {
                rows++;
                PdfA4Utils.resizeColumnText(ct, ctx, y, ctw, ROW_HEIGHT_FONT13 * rows);
                prepareAddInfo(ct, yLine);
            } while (ct.go(true) != ColumnText.NO_MORE_TEXT);
            lowestPosition = drawAddInfo(ct, yLine, x, y, rows);
        }

        return PdfA4Utils.yPositionInMM(lowestPosition);
    }

    private void prepareAddInfo(ColumnText ct, float yLine) throws DocumentException, IOException {
        Font font = FontProvider.font(FontProvider.FontStyle.ARIAL_13_BOLD);

        Paragraph userName = new Paragraph(ticketData.getUserName(), font);
        userName.setLeading(0f, 1.2f);

        Paragraph eventName = new Paragraph(ticketData.getEventName(), font);
        eventName.setLeading(0f, 1.2f);
        eventName.setSpacingBefore(PdfA4Utils.yps(PARAGRAPH_SPACING));

        Paragraph dateAndPlace = new Paragraph(dateAndPlace(), FontProvider.font(FontProvider.FontStyle.ARIAL_9_BOLD));
        dateAndPlace.setLeading(0f, 1.5f);
        dateAndPlace.setSpacingBefore(PdfA4Utils.yps(PARAGRAPH_SPACING));

        ct.setText(null);
        ct.setYLine(yLine);
        ct.addElement(userName);
        ct.addElement(eventName);
        ct.addElement(dateAndPlace);
    }

    private String dateAndPlace() {
        StringBuilder result = new StringBuilder();

        if (ticketData.getFrom() != null) {
            SimpleDateFormat dateFormater = new SimpleDateFormat(DATE_FORMAT);
            SimpleDateFormat hourFormater = new SimpleDateFormat(HOUR_FORMAT);

            result.append(dateFormater.format(ticketData.getFrom()));
            result.append(" from ");
            result.append(hourFormater.format(ticketData.getFrom()));
            result.append(", ");
        }
        if (StringUtils.isNotBlank(ticketData.getAddress())) {
            if (StringUtils.isNotBlank(ticketData.getPlace())) {
                result.append(ticketData.getPlace());
                result.append(", ");
            }
            result.append(ticketData.getAddress());
        }
        return result.toString();
    }

    private float drawAddInfo(ColumnText ct, float yLine, float x, float y, int rows) throws DocumentException, IOException {
        final float height = Math.max(21.5f, rows * ROW_HEIGHT_FONT13);

        drawGrayBackground(y, height + 1.5f);
        float bottom = drawWhiteBackground(x, y, 159.5f, height);

        prepareAddInfo(ct, yLine);
        ct.setYLine(yLine);
        ct.go(false);

        return bottom;
    }

    private float addAccessories(float endInfo) throws DocumentException, IOException {
        final float x = 6.5f;
        final float y = endInfo + 1.5f;
        int rows = 0;

        // priprava tabulky
        PdfTicketAccessoriesTable table = new PdfTicketAccessoriesTable();
        TimeZone timeZone = TimeZone.getTimeZone(ticketData.getTimeZone());
        for (TicketOrderItem item : ticketData.getOrders()) {
            table.addRow(item, timeZone, ticketData.getCurrency());
            rows++;
        }

        float height = 0;
        for (int i = 0; i < rows; i++) {
            height += table.getRowHeight(i) / PdfA4Utils.PAGE_HEIGHT_CONST;
        }
        height += 2.5f;

        // vykresleni ramecku a tabulky
        drawGrayBackground(y, height + 1.5f);
        drawWhiteBackground(x, y, TICKET_INNER_WIDTH_MM - 3, height);
        table.draw(writer, x, y);

        return y + height;
    }

    private void addCode128(float endAccessories) throws DocumentException, IOException {
        final float x = 6.5f;
        final float y = endAccessories + 1.5f;

        drawGrayBackground(y, 14.5f);
        drawWhiteBackground(x, y, TICKET_INNER_WIDTH_MM - 3, 13);

        // carkovy kod
        BarCodeGenerator barCodeGenerator = new BarCodeGenerator(BarcodeFormat.CODE_128);
        Image pngImage = barCodeGenerator.generateCode(ticketData.getTicketCode());

        ColumnText barCT = PdfA4Utils.columnText(writer, 60, y + 2, 100, 7);
        barCT.addElement(pngImage);
        barCT.go();

        // cislo kodu
        Paragraph code = new Paragraph(ticketData.getTicketCode(), FontProvider.font(FontProvider.FontStyle.ARIAL_9_REGULAR));
        code.setAlignment(Paragraph.ALIGN_CENTER);

        ColumnText codeCT = PdfA4Utils.columnText(writer, 60, y + 7, 100, 5);
        codeCT.addElement(code);
        codeCT.go();
    }

    public void createPdfTicket(OutputStream os, TicketData ticketData, boolean closeOutputStream) throws Exception {
        this.ticketData = ticketData;
        generate(os, closeOutputStream);
    }

    private void generate(OutputStream os, boolean closeOutputStream) throws Exception {
        startWork(os);

        addTakeplaceLogo();
        addEventLogo();

        final float endInfo = addInfo();
        addQRCode(endInfo);

        final float endAccessories = addAccessories(endInfo);
        addCode128(endAccessories);

        stopWork(closeOutputStream);
    }

    private void startWork(OutputStream os) throws Exception {
        document = new Document(PageSize.A4);
        writer = PdfWriter.getInstance(document, os);

        addMetadata();

        document.open();
    }

    private void stopWork(boolean closeOutputStream) {
        if (closeOutputStream) {
            writer.flush();
            document.close();
        }
    }

    private void drawGrayBackground(float y, float height) throws DocumentException {
        Rectangle rect = PdfA4Utils.rectangle(5, y, TICKET_INNER_WIDTH_MM, height);
        rect.setBackgroundColor(COLOR_GRAY);
        document.add(rect);
    }

    private float drawWhiteBackground(float x, float y, float width, float height) throws DocumentException {
        Rectangle rect = PdfA4Utils.rectangle(x, y, width, height);
        rect.setBackgroundColor(COLOR_WHITE);
        document.add(rect);
        return rect.getBottom();
    }

    /**
     * nacteni loga takeplace
     */
    private PdfImportedPage templateTakeplaceLogo() throws IOException {
        final String name = "/pdf/takeplace.pdf";
        PdfImportedPage page = writer.getImportedPage(new PdfReader(this.getClass().getResourceAsStream(name)), 1);
        return page;
    }

    public static void main(String[] args) {
        TicketData data = new TicketData();
        data.setEventName("NÁZEV EVENTU NÁZEV EVENTU");
        data.setUserName("Ing. Pavol Grešša");
        //data.setTicketCode("42342234TicketCode123123");
        //data.setTicketCode("73");
        data.setTicketCode("fce441b219cb48ae8883acea47c31fe8");
        //data.setTicketCode("fce441b219cb48ae8883acea47c31fe8fce441b219cb48ae8883acea47c31fe8");
        data.setAddress("Husitska 24, Brno, Czech Republic");
        data.setFrom(new Date());
        data.setTimeZone("America/Los_Angeles");
        data.setCurrency(Currency.CZK);

        for (int i = 0; i < 3; i++) {
            TicketOrderItem item = new TicketOrderItem();
            item.setDate(new Date());
            item.setPrice(new BigDecimal(100));
            item.setTitle("Popis objednaneho bundlu/subbundluPopis Popis objednaneho bundlu/subbundluPopis Popis objednaneho bundlu/subbundluPopis ");
            item.setQuantity(13);
            data.getOrders().add(item);
            TicketOrderItem item1 = new TicketOrderItem();
            item1.setDate(new Date());
            item1.setPrice(BigDecimal.ZERO);
            item1.setTitle("Popis objednaneho bundlu/subbundluPopis");
            data.getOrders().add(item1);
        }

        try {
            PdfTicketGenerator gen = new PdfTicketGenerator();

            if (SystemUtils.IS_OS_LINUX) {
                //data.setEventLogo(ImageIO.read(new File("/tmp/event-logo.jpg")));
                gen.createPdfTicket(new FileOutputStream(new File("/tmp/consys-ticket.pdf")), data, true);
            } else if (SystemUtils.IS_OS_WINDOWS) {
                //data.setEventLogo(ImageIO.read(new File("c:\\data\\event-logo.gif")));
                gen.createPdfTicket(new FileOutputStream(new File("c:\\data\\consys-ticket.pdf")), data, true);
            } else if (SystemUtils.IS_OS_MAC_OSX) {
                //data.setEventLogo(ImageIO.read(new File("/Users/palo/Acemcee/event-logo.jpg")));
                gen.createPdfTicket(new FileOutputStream(new File("/Users/palo/consys-ticket.pdf")), data, true);
            }
        } catch (Exception ex) {
            System.err.println("Chyba " + ex.getLocalizedMessage());
            ex.printStackTrace();
        }
    }
}
