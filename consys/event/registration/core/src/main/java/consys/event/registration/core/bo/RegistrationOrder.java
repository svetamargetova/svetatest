package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Lists;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationOrder implements ConsysObject {

    private static final long serialVersionUID = 2010280705831389234L;
    // data
    private Long id;
    private String uuid;
    private Date orderDate;
    private Date paidDate;
    private Date cancelDate;
    private RegistrationState state;
    private BigDecimal overallPrice;
    private UserRegistration userRegistration;
    private List<UserBundleRegistration> registredBundles;

    public RegistrationOrder() {
        registredBundles = Lists.newArrayList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public RegistrationState getState() {
        return state;
    }

    public void setState(RegistrationState state) {
        this.state = state;
    }

    public BigDecimal getOverallPrice() {
        return overallPrice;
    }

    public void setOverallPrice(BigDecimal overallPrice) {
        this.overallPrice = overallPrice;
    }

    public List<UserBundleRegistration> getRegistredBundles() {
        return registredBundles;
    }

    public void setRegistredBundles(List<UserBundleRegistration> registredBundles) {
        this.registredBundles = registredBundles;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        RegistrationOrder e = (RegistrationOrder) obj;
        return uuid.equalsIgnoreCase(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("RegistrationOrder[state=%s orderDate=%s paidDate=%s cancelDate=%s overallPrice=%s]", state, orderDate, paidDate, cancelDate, overallPrice.toPlainString());
    }

    public UserRegistration getUserRegistration() {
        return userRegistration;
    }

    public void setUserRegistration(UserRegistration userRegistration) {
        this.userRegistration = userRegistration;
    }

    public boolean isActive() {
        return getState().equals(RegistrationState.ORDERED)
                || getState().equals(RegistrationState.CONFIRMED)
                || getState().equals(RegistrationState.APPROVE);
    }

    public boolean isConfirmed() {
        return getState().equals(RegistrationState.CONFIRMED);
    }

    public boolean hasMainBundle() {
        if (getUserRegistration().getOrders().size() == 1) {
            return true;
        } else {
            return !getRegistredBundles().get(0).getRegistrationCycle().getBundle().isSubBundle();
        }
    }

    public boolean isTotalZero() {
        return getOverallPrice().compareTo(BigDecimal.ZERO) == 0;
    }
}
