package consys.event.registration.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.event.registration.core.bo.enums.UIComponentType;

/**
 * Doplnkova otazka pri registraci
 * @author pepa
 */
public class RegistrationCustomQuestion implements ConsysObject {

    private static final long serialVersionUID = -6882545998685487478L;
    // data
    private Long id;
    private String uuid;
    private boolean required;
    private int questionOrder;
    private UIComponentType component;

    public RegistrationCustomQuestion() {
        component = UIComponentType.INPUT;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public int getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(int questionOrder) {
        this.questionOrder = questionOrder;
    }

    public UIComponentType getComponent() {
        return component;
    }

    public void setComponent(UIComponentType component) {
        this.component = component;
    }
}
