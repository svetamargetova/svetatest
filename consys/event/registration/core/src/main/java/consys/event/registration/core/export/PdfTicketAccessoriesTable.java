package consys.event.registration.core.export;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import consys.common.utils.enums.Currency;
import consys.event.export.common.Formats;
import consys.event.export.pdf.FontProvider;
import consys.event.export.pdf.FontProvider.FontStyle;
import consys.event.export.pdf.PdfA4Utils;
import consys.event.registration.core.bo.TicketOrderItem;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 *
 * @author pepa
 */
public class PdfTicketAccessoriesTable extends PdfPTable {

    // konstanty
    public static final String DATE_FORMAT = "dd.MM.yyyy";
    // data
    private Font font;

    public PdfTicketAccessoriesTable() throws DocumentException, IOException {
        // datum, text, cena
        //super(3);
        //setTotalWidth(new float[]{PdfA4Utils.xps(38), PdfA4Utils.xps(115), PdfA4Utils.xps(44)});
        // text, cena
        //super(2);
        //setTotalWidth(new float[]{PdfA4Utils.xps(153), PdfA4Utils.xps(44)});
        // kvantita, text, cena
        super(3);
        setTotalWidth(new float[]{PdfA4Utils.xps(12), PdfA4Utils.xps(141), PdfA4Utils.xps(44)});
        font = FontProvider.font(FontStyle.ARIAL_10_BOLD);
    }

    private void addQuantity(int quantity) throws DocumentException, IOException {
        PdfPCell cell = pdfCell(new Paragraph(quantity + "x", font));
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        addCell(cell);
    }

    private void addDate(Date date, TimeZone timeZone) throws DocumentException, IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        dateFormat.setTimeZone(timeZone);

        PdfPCell cell = pdfCell(new Paragraph(dateFormat.format(date), font));
        cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        addCell(cell);
    }

    private void addText(String text) throws DocumentException, IOException {
        PdfPCell cell = pdfCell(new Paragraph(text, font));
        addCell(cell);
    }

    private void addPrice(BigDecimal price, Currency currency) throws DocumentException, IOException {
        String text;
        Paragraph p;
        if (price.compareTo(BigDecimal.ZERO) > 0) {
            text = Formats.decimalFormat(currency).format(price.doubleValue());
            p = new Paragraph(Formats.assignCurrency(currency, text), font);
        } else {
            text = "FREE";
            p = new Paragraph(text, font);
        }

        PdfPCell cell = pdfCell(p);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        addCell(cell);
    }

    public void addRow(TicketOrderItem rowData, TimeZone timeZone, Currency c) throws DocumentException, IOException {
        //addDate(rowData.getDate(), timeZone);
        addQuantity(rowData.getQuantity());
        addText(rowData.getTitle());
        addPrice(rowData.getPrice(), c);
    }

    public void draw(PdfWriter writer, float x, float y) {
        final float fromY = PdfA4Utils.PAGE_HEIGHT - PdfA4Utils.yps(y);
        writeSelectedRows(0, -1, PdfA4Utils.xps(x), fromY, writer.getDirectContent());
    }

    private PdfPCell pdfCell(Paragraph p) throws DocumentException, IOException {
        final float padding = PdfA4Utils.xps(2);
        PdfPCell cell = new PdfPCell(p);
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setPaddingLeft(padding);
        cell.setPaddingRight(padding);
        cell.setPaddingTop(padding);
        return cell;
    }
}
