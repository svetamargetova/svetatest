package consys.event.registration.core.service;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.utils.UuidProvider;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.registration.core.AbstractRegistrationTest;
import consys.event.registration.core.bo.RegistrationCustomQuestion;
import consys.event.registration.core.bo.RegistrationCustomQuestionAnswer;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.dao.RegistrationCustomQuestionAnswerDao;
import consys.event.registration.core.dao.RegistrationCustomQuestionDao;
import consys.event.registration.core.dao.RegistrationCustomQuestionTextDao;
import consys.event.registration.core.dao.UserRegistrationDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class RegistrationQuestionAnswerServiceTest extends AbstractRegistrationTest {

    @Autowired
    private RegistrationCustomQuestionDao questionDao;
    @Autowired
    private RegistrationCustomQuestionTextDao questionTextDao;
    @Autowired
    private RegistrationCustomQuestionAnswerDao questionAnswerDao;
    @Autowired
    private RegistrationQuestionAnswerService qService;
    @Autowired
    private UserRegistrationDao userRegistrationDao;
    @Autowired
    private UserEventService userEventService;

    private RegistrationCustomQuestion createQuestion(boolean reqired, int questionOrder) {
        RegistrationCustomQuestion q = new RegistrationCustomQuestion();
        q.setUuid(UuidProvider.getUuid());
        q.setRequired(reqired);
        q.setQuestionOrder(questionOrder);

        try {
            questionDao.create(q);
        } catch (Exception ex) {
            fail("Question not created");
        }

        assertNotNull(q.getId());
        assertNotNull(q.getComponent());

        return q;
    }

    private UserRegistration createUserRegistration() throws Exception {
        UserEvent userEvent = new UserEvent();
        userEvent.setEmail("em@em.em");
        userEvent.setFullName("Test User");
        userEvent.setLastName("User");
        userEvent.setOrganization("Org");
        userEvent.setPrivateGroup(new Group());
        userEvent.setUuid(UuidProvider.getUuid());
        userEventService.createUser(userEvent);

        UserRegistration ur = new UserRegistration();
        ur.setRegistrationNumber("123");
        ur.setState(RegistrationState.ORDERED);
        ur.setUser(userEvent);
        ur.setUuid(UuidProvider.getUuid());
        userRegistrationDao.create(ur);
        return ur;
    }

    @Test(groups = {"service"})
    public void testCreateQuestion() {
        final String text = "Text otázky";
        try {
            RegistrationCustomQuestion question = qService.createQuestion(true, 1, text);
            assertNotNull(question.getId());

            RegistrationCustomQuestion question1 = questionDao.loadQuestionByUuid(question.getUuid());
            assertTrue(question1.isRequired());

            List<RegistrationCustomQuestionText> texts = questionTextDao.listAllQuestionTextsForQuestion(question.getUuid());
            assertEquals(texts.size(), 1);

            assertEquals(texts.get(0).getQuestion(), text);
        } catch (Exception ex) {
            fail("Question not created");
        }
    }

    @Test(groups = {"service"})
    public void testAddQuestion() {
        final String text = "Text otázky";
        try {
            RegistrationCustomQuestion question = qService.createQuestion(true, 1, text);
            assertNotNull(question.getId());

            RegistrationCustomQuestionText qt = qService.addLocalizedText(question.getUuid(), text + " anglicky", SystemLocale.ENGLISH);
            assertNotNull(qt.getId());

            List<RegistrationCustomQuestionText> texts = questionTextDao.listAllQuestionTextsForQuestion(question.getUuid());
            assertEquals(texts.size(), 2);
        } catch (Exception ex) {
            fail("Question not added");
        }
    }

    @Test(groups = {"service"})
    public void testUpdateQuestion() {
        final String text = "Co budeme dělat?";
        try {
            RegistrationCustomQuestion question = qService.createQuestion(true, 1, text);
            assertNotNull(question.getId());
            RegistrationCustomQuestion questionSecond = qService.createQuestion(true, 2, text);
            assertNotNull(questionSecond.getId());

            RegistrationCustomQuestion question1 = questionDao.loadQuestionByUuid(question.getUuid());
            assertEquals(question1.getQuestionOrder(), 1);
            assertTrue(question1.isRequired());

            qService.updateQuestion(question1.getUuid(), false);

            RegistrationCustomQuestion question2 = questionDao.loadQuestionByUuid(question.getUuid());
            assertFalse(question2.isRequired());

            RegistrationCustomQuestion question3 = questionDao.loadQuestionByUuid(questionSecond.getUuid());
            assertEquals(question3.getQuestionOrder(), 2);
        } catch (Exception ex) {
            fail("Question not updated");
        }
    }

    @Test(groups = {"service"})
    public void testUpdateQuestionText() {
        final String text = "Co budeme dělat?";
        final String textUpdated = "Oblíbená barva";
        try {
            RegistrationCustomQuestion question = qService.createQuestion(true, 1, text);
            assertNotNull(question.getId());

            qService.updateQuestionText(question.getUuid(), textUpdated, SystemLocale.DEFAULT);

            RegistrationCustomQuestionText questionText = questionTextDao.loadByUuidAndLocale(question.getUuid(), SystemLocale.DEFAULT);
            assertEquals(questionText.getQuestion(), textUpdated);
        } catch (Exception ex) {
            fail("Question text not updated");
        }
    }

    @Test(groups = {"service"})
    public void testDeleteQuestion() {
        RegistrationCustomQuestion question = createQuestion(true, 1);

        List<RegistrationCustomQuestion> list = questionDao.listAll(RegistrationCustomQuestion.class);
        assertEquals(list.size(), 1);

        try {
            qService.deleteQuestion(question.getUuid());
        } catch (Exception ex) {
            fail("Question not deleted");
        }

        list = questionDao.listAll(RegistrationCustomQuestion.class);
        assertEquals(list.size(), 0);
    }

    @Test(groups = {"service"})
    public void testAddAndDeleteAnswers() {
        final String text1 = "Co budeme dělat?";
        final String text2 = "Co myslíš, že se naučíš?";
        final String answerText = "To fakt nevím";
        try {
            RegistrationCustomQuestion question1 = qService.createQuestion(true, 1, text1);
            assertNotNull(question1.getId());
            RegistrationCustomQuestion question2 = qService.createQuestion(false, 2, text2);
            assertNotNull(question2.getId());

            UserRegistration userRegistration = createUserRegistration();

            qService.addAnswer(userRegistration.getUser().getUuid(), question1.getUuid(), answerText);

            List<RegistrationCustomQuestionAnswer> list = questionAnswerDao.listAllAnswersForQuestion(question1.getUuid());
            assertEquals(list.size(), 1);
            assertEquals(list.get(0).getAnswer(), answerText);

            list = questionAnswerDao.listAllAnswersForQuestion(question2.getUuid());
            assertEquals(list.size(), 0);

            List<DetailedUserAnswer> ans = qService.listUserAnswers(userRegistration.getUser().getUuid());
            assertEquals(ans.size(), 1);
            assertEquals(ans.get(0).getAnswer(), answerText);
            assertEquals(ans.get(0).getOrder(), 1);
            assertEquals(ans.get(0).getQuestion(), text1);
            
            // promezeme
            qService.deleteAnswers(userRegistration.getUser().getUuid());

            list = questionAnswerDao.listAllAnswersForQuestion(question1.getUuid());
            assertEquals(list.size(), 0);

            list = questionAnswerDao.listAll(RegistrationCustomQuestionAnswer.class);
            assertEquals(list.size(), 0);
        } catch (Exception ex) {
            fail("Answers not deleted");
        }
    }

    @Test(groups = {"service"})
    public void testUpdateQuestionPosition() {
        RegistrationCustomQuestion question1 = null;
        RegistrationCustomQuestion question2 = null;
        RegistrationCustomQuestion question3 = null;

        try {
            question1 = qService.createQuestion(true, 1, "text1");
            question2 = qService.createQuestion(true, 2, "text2");
            question3 = qService.createQuestion(true, 3, "text3");
        } catch (Exception ex) {
            fail("Questions not created");
        }

        forceSessionFlush();

        // smer zmeny dolu
        try {
            int updated = qService.updateQuestionOrder(question1.getUuid(), 1, 2);
            assertEquals(updated, 1);
        } catch (Exception ex) {
            fail("Question position change not run");
        }

        forceSessionFlush();

        try {
            assertEquals(questionDao.loadQuestionByUuid(question1.getUuid()).getQuestionOrder(), 2);
            assertEquals(questionDao.loadQuestionByUuid(question2.getUuid()).getQuestionOrder(), 1);
            assertEquals(questionDao.loadQuestionByUuid(question3.getUuid()).getQuestionOrder(), 3);
        } catch (Exception ex) {
            fail("Question not loaded");
        }

        // smer zmeny nahoru
        try {
            int updated = qService.updateQuestionOrder(question3.getUuid(), 3, 1);
            assertEquals(updated, 1);
        } catch (Exception ex) {
            fail("Question position change not run");
        }

        forceSessionFlush();

        try {
            assertEquals(questionDao.loadQuestionByUuid(question1.getUuid()).getQuestionOrder(), 3);
            assertEquals(questionDao.loadQuestionByUuid(question2.getUuid()).getQuestionOrder(), 2);
            assertEquals(questionDao.loadQuestionByUuid(question3.getUuid()).getQuestionOrder(), 1);
        } catch (Exception ex) {
            fail("Question not loaded");
        }
    }

    @Test(groups = {"service"})
    public void testQuestionCount() {
        assertEquals(qService.loadQuestionsCount().intValue(), 0);

        try {
            qService.createQuestion(true, 1, "text 1");
        } catch (Exception ex) {
            fail("Otazka nebyla vytvorena");
        }

        assertEquals(qService.loadQuestionsCount().intValue(), 1);
    }
}
