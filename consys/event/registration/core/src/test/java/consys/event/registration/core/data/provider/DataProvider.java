package consys.event.registration.core.data.provider;

import consys.event.registration.core.bo.RegistrationCycle;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DataProvider {

    public static final SimpleDateFormat FORMAT = new SimpleDateFormat("dd.MM.yy hh:mm");

    public static RegistrationCycle getRegistrationCycle(Double price, int yearFrom, int monthFrom, int dayFrom, int yearTo, int monthTo, int dayTo) {
        RegistrationCycle cycle = new RegistrationCycle();
        cycle.setPrice(BigDecimal.valueOf(price));
        Calendar c2s = GregorianCalendar.getInstance();
        c2s.set(yearFrom, monthFrom, dayFrom);
        cycle.setFrom(c2s.getTime());

        Calendar c2e = GregorianCalendar.getInstance();
        c2e.set(yearTo, monthTo, dayTo);
        cycle.setTo(c2e.getTime());
        return cycle;
    }

    public static RegistrationCycle getRegistrationCycle(Double price, String fromDate, String toDate) throws ParseException {


        RegistrationCycle cycle = new RegistrationCycle();
        cycle.setPrice(BigDecimal.valueOf(price));
        cycle.setFrom(FORMAT.parse(fromDate));
        cycle.setTo(FORMAT.parse(toDate));
        return cycle;
    }

    /**
     * Vrati registracny cyklus s intervalom inicializovanym na:
     * <p>
     * <strong> -1 mesiac &gt; dnes &lt; +1 mesiac</strong>
     * @param price
     * @return
     */
    public static RegistrationCycle getRegistrationCycle(Double price) {
        Calendar c2s = GregorianCalendar.getInstance();
        c2s.roll(Calendar.MONTH, false);

        Calendar c2e = GregorianCalendar.getInstance();
        c2e.roll(Calendar.MONTH, true);

        if (c2e.get(Calendar.MONTH) < c2s.get(Calendar.MONTH)) {
            c2e.roll(Calendar.YEAR, true);
        }

        RegistrationCycle cycle = new RegistrationCycle();
        cycle.setPrice(BigDecimal.valueOf(price));
        cycle.setFrom(c2s.getTime());
        cycle.setTo(c2e.getTime());

        return cycle;
    }
}
