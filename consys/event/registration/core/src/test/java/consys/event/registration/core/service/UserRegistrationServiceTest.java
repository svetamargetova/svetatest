package consys.event.registration.core.service;

import java.util.Date;
import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.utils.date.DateProvider;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.common.utils.UuidProvider;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.registration.core.AbstractRegistrationTest;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.so.RegistrationOtherInfo;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.common.utils.date.DateUtils;
import consys.event.b2b.api.right.EventB2BModuleRoleModel;
import consys.event.common.core.EventOrderStateChangeListener.OrderState;
import consys.event.common.core.bo.Group;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.test.core.mock.PaymentWebServiceMock;
import consys.event.registration.api.properties.RegistrationProperties;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.core.bo.thumb.RegistrationCouponStats;
import consys.event.registration.core.dao.RegistrationOrderDao;
import consys.event.registration.core.data.provider.DataProvider;
import consys.event.registration.core.exception.CyclesOverlapsException;
import consys.event.registration.core.module.RegistrationStateChangeListener;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.ws.bo.Profile;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserRegistrationServiceTest extends AbstractRegistrationTest {

    // konstanty
    private static final int QUANTITY_1 = 1;
    // sluzby
    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private RegistrationBundleService bundleService;
    @Autowired
    private UserRegistrationDao registrationDao;
    @Autowired
    private UserEventService userEventService;
    @Autowired
    private RegistrationStateChangeListener registrationInvoiceProcessor;
    @Autowired
    private RegistrationOrderDao registrationOrderDao;
    @Autowired
    private PaymentWebServiceMock mockService;
    @Autowired
    private SystemPropertyService spService;
    @Autowired
    private ParticipantManagerService pmService;

    @Test(groups = {"service"}, description = "Test na matody ktore generuju kod balika")
    public void testDiscountCouponKeyGenerator() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {

        // Vytvorime si dva hlavne baliky 
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundle1.setWithCode(true);
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));
        RegistrationCoupon rc1 = new RegistrationCoupon("A1", 20, 1);
        bundleService.createRegistrationCoupon(bundle1, rc1);

        assertFalse(bundleService.isValidRegistrationCouponKey(bundle1.getUuid(), rc1.getKey()));

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundle2.setWithCode(true);
        bundleService.createBundle(bundle2);
        assertTrue(bundleService.isValidRegistrationCouponKey(bundle2.getUuid(), rc1.getKey()));

        String generatedKey1 = bundleService.generateRegistrationCouponKey(bundle1.getUuid());
        String generatedKey2 = bundleService.generateRegistrationCouponKey(bundle1.getUuid());
        String generatedKey3 = bundleService.generateRegistrationCouponKey(bundle1.getUuid());
    }

    // Test na standardny proces registracie     
    // 1] Nova registracia uzivatela do eventu vyberom jedneho hlavneho balika
    @Test(groups = {"service"}, description = "Nova registracia uzivatela do eventu vyberom jedneho hlavneho balika")
    public void newUserRegistrationTest1() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky a jeden subbalik
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Nacitame bundle 2 a zistime ci je pocet registrovanych 0
        bundle2 = bundleService.loadById(bundle2.getId());
        assertEquals(bundle2.getRegistred(), 0);

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
        mockService.setCreateOrderFinalPrice(new BigDecimal(500));
        // vytvorime si registraciu na bundle 2
        BundleOrderRequest orderRequest = new BundleOrderRequest(bundle2.getUuid());
        UserRegistration registration = registrationService.createUserRegistration("123", false, orderRequest, null, "123", otherInfo);
        assertEquals(registration.getOrders().size(), 1);
        assertEquals(registration.getOrders().get(0).getRegistredBundles().size(), 1);
        assertEquals(registration.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal(500)), 0);


        // Zistime ci je pocet zaregistrovanych +1
        assertEquals(bundleService.loadBundleRegistrationCount(bundle2.getId()), new Integer(1));

        // Nacitame aktivnu registraciu
        UserRegistration reqiRegistrationl = registrationService.loadUserActiveRegistrationDetailedByUserUuid(registration.getUser().getUuid());

        // Zrusime objednavku
        registrationService.updateCancelRegistrationOrderFromOrganizator(registration.getOrders().get(0).getUuid());
        assertEquals(bundleService.loadBundleRegistrationCount(bundle2.getId()), new Integer(0));

        // Skusime nacitat aktivnu registraciu - skonci exception
        try {
            registrationService.loadUserActiveRegistrationDetailedByUserUuid(registration.getUser().getUuid());
            fail("Nemoze sa nacitat takato registracia");
        } catch (NoRecordException e) {
        }
    }

    // 2] Nova registracia uzivatela do eventu vyberom jedneho hlavneho balika a par subbalikov
    @Test(groups = {"service"}, description = "Nova registracia uzivatela do eventu vyberom jedneho hlavneho balika a par subbalikov")
    public void newUserRegistrationTest2() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky 
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Vytvorime si subbaliky
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(sc1));

        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle sc2 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(sc2));

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest orderRequest2 = new BundleOrderRequest(subbundle1.getUuid());
        BundleOrderRequest orderRequest3 = new BundleOrderRequest(subbundle2.getUuid());

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
        mockService.setCreateOrderFinalPrice(new BigDecimal(700));
        UserRegistration registration = registrationService.createUserRegistration("123", false, orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), "123", otherInfo);
        assertEquals(registration.getOrders().size(), 1);
        assertEquals(registration.getOrders().get(0).getRegistredBundles().size(), 3);
        assertEquals(registration.getOrders().get(0).getRegistredBundles().get(0).getRegistrationCycle(), c1);
        assertEquals(registration.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal(700)), 0);

        // Zistime ci je pocet zaregistrovanych +1
        assertEquals(bundleService.loadBundleRegistrationCount(bundle1.getId()), new Integer(1));
        assertEquals(bundleService.loadBundleRegistrationCount(bundle2.getId()), new Integer(0));
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle1.getId()), new Integer(1));
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle2.getId()), new Integer(1));

        // Pridame novy subbundle a objednakvu
        RegistrationBundle subbundle3 = new RegistrationBundle();
        subbundle3.setTitle("Subbundle 3");
        subbundle3.setSubBundle(true);
        bundleService.createBundle(subbundle3);
        RegistrationCycle sc3 = DataProvider.getRegistrationCycle(1000.0);
        bundleService.createRegistrationCycles(subbundle3.getUuid(), Lists.newArrayList(sc3));

        // Nacitame aktivnu registraciu
        UserRegistration reqiRegistrationl = registrationService.loadUserActiveRegistrationDetailedByUserUuid(registration.getUser().getUuid());
        assertEquals(reqiRegistrationl.getOrders().size(), 1);
        assertEquals(reqiRegistrationl.getOrders().get(0).getRegistredBundles().size(), 3);
        assertEquals(reqiRegistrationl.getOrders().get(0).getRegistredBundles().get(0).getRegistrationCycle(), c1);
        assertEquals(reqiRegistrationl.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal(700)), 0);

        // Pridame novu objednavku
        registrationInvoiceProcessor.orderStateChanged(registration.getOrders().get(0).getUuid(), OrderState.CONFIRMED, true, DateProvider.getCurrentDate());
        mockService.setCreateOrderFinalPrice(new BigDecimal(1000));
        registrationService.addOrderToRegistration("123", Lists.newArrayList(new BundleOrderRequest(subbundle3.getUuid())), "123");

        reqiRegistrationl = registrationService.loadUserActiveRegistrationDetailedByUserUuid(registration.getUser().getUuid());
        assertEquals(reqiRegistrationl.getOrders().size(), 2);
        assertEquals(reqiRegistrationl.getOrders().get(0).getRegistredBundles().size(), 3);
        assertEquals(reqiRegistrationl.getOrders().get(0).getRegistredBundles().get(0).getRegistrationCycle(), c1);
        assertEquals(reqiRegistrationl.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal(700)), 0);
        assertEquals(reqiRegistrationl.getOrders().get(1).getRegistredBundles().size(), 1);
        assertEquals(reqiRegistrationl.getOrders().get(1).getRegistredBundles().get(0).getRegistrationCycle(), sc3);
        assertEquals(reqiRegistrationl.getOrders().get(1).getOverallPrice().compareTo(new BigDecimal(1000)), 0);
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle3.getId()), new Integer(1));

        registrationService.loadRegistrationDetailed(reqiRegistrationl.getUuid());

        // Zrusime objednavku
        registrationService.updateCancelRegistrationOrderFromOwner(registration.getOrders().get(1).getUuid());
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle3.getId()), new Integer(0));

        reqiRegistrationl = registrationService.loadUserActiveRegistrationDetailedByUserUuid(registration.getUser().getUuid());
        assertEquals(reqiRegistrationl.getActiveOrders().size(), 1);
        assertEquals(reqiRegistrationl.getOrders().size(), 2);
        assertEquals(reqiRegistrationl.getOrders().get(0).getState(), RegistrationState.CONFIRMED);
        assertEquals(reqiRegistrationl.getOrders().get(1).getState(), RegistrationState.CANCELED);
        assertNotNull(reqiRegistrationl.getOrders().get(1).getCancelDate());

        // Pridame dalsieho uzivatela
        UserRegistration registration2 = registrationService.createUserRegistration("234", false, orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), "234", otherInfo);
        assertEquals(bundleService.loadBundleRegistrationCount(bundle1.getId()), new Integer(2));
        assertEquals(bundleService.loadBundleRegistrationCount(bundle2.getId()), new Integer(0));
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle1.getId()), new Integer(2));
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle2.getId()), new Integer(2));
    }

    // 3] Nova registracia uzivatela do eventu vyberom jedneho hlavneho balika a a zlavy pri balikoch
    @Test(groups = {"service"}, description = "Nova registracia uzivatela do eventu vyberom jedneho hlavneho balika a a zlavy pri balikoch")
    public void newUserRegistrationTest3() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky 
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundle1.setWithCode(true);
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));
        RegistrationCoupon rc1 = new RegistrationCoupon("A1", 20, 1);
        bundleService.createRegistrationCoupon(bundle1, rc1);

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundle2.setWithCode(true);
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));
        RegistrationCoupon rc2 = new RegistrationCoupon("A2", 20, 1);
        bundleService.createRegistrationCoupon(bundle2, rc2);

        // Vytvorime si subbaliky
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        subbundle1.setWithCode(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(sc1));
        RegistrationCoupon rc3 = new RegistrationCoupon("A3", 20, 1);
        bundleService.createRegistrationCoupon(subbundle1, rc3);

        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        subbundle2.setWithCode(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle sc2 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(sc2));
        RegistrationCoupon rc4 = new RegistrationCoupon("A4", 20, 1);
        bundleService.createRegistrationCoupon(subbundle2, rc4);

        // Test na zlavy
        assertEquals(registrationService.loadDiscount(bundle1.getUuid(), "A1").compareTo(new BigDecimal(80)), 0);
        assertEquals(registrationService.loadDiscount(bundle2.getUuid(), "A2").compareTo(new BigDecimal(100)), 0);
        assertEquals(registrationService.loadDiscount(subbundle1.getUuid(), "A3").compareTo(new BigDecimal(20)), 0);
        assertEquals(registrationService.loadDiscount(subbundle2.getUuid(), "A4").compareTo(new BigDecimal(40)), 0);

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(bundle1.getUuid(), rc1.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest2 = new BundleOrderRequest(subbundle1.getUuid());
        BundleOrderRequest orderRequest3 = new BundleOrderRequest(subbundle2.getUuid(), rc4.getKey(), QUANTITY_1);

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
        mockService.setCreateOrderFinalPrice(new BigDecimal(580));
        UserRegistration registration = registrationService.createUserRegistration("123", false, orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), "123", otherInfo);
        assertEquals(registration.getOrders().size(), 1);
        assertEquals(registration.getOrders().get(0).getRegistredBundles().size(), 3);
        assertEquals(registration.getOrders().get(0).getRegistredBundles().get(0).getRegistrationCycle(), c1);
        assertEquals(registration.getOrders().get(0).getRegistredBundles().get(0).getRegistrationCoupon(), rc1);
        assertEquals(registration.getOrders().get(0).getRegistredBundles().get(2).getRegistrationCoupon(), rc4);
        assertEquals(registration.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal(580)), 0, "Spatna cena? " + registration.getOrders().get(0).getOverallPrice().toPlainString());

        // Zistime ci je pocet zaregistrovanych +1
        assertEquals(bundleService.loadBundleRegistrationCount(bundle1.getId()), new Integer(1));
        assertEquals(bundleService.loadBundleRegistrationCount(bundle2.getId()), new Integer(0));
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle1.getId()), new Integer(1));
        assertEquals(bundleService.loadBundleRegistrationCount(subbundle2.getId()), new Integer(1));

        // Pokusime sa opet zaregistrovat na vlozeny discount kupon
        BundleOrderRequest orderRequest4 = new BundleOrderRequest(bundle1.getUuid(), rc1.getKey(), QUANTITY_1);
        try {
            registrationService.createUserRegistration("234", false, orderRequest4, null, "234", otherInfo);
            fail("Kupon je pouzity ale registracia projde");
        } catch (CouponCapacityException e) {
        }

        // Pokusime sa opet zaregistrovat na chybny discount kupon
        BundleOrderRequest orderRequest5 = new BundleOrderRequest(bundle1.getUuid(), "test", QUANTITY_1);
        try {
            registrationService.createUserRegistration("678", false, orderRequest5, null, "234", otherInfo);
            fail("Kupon je chybny ale projde registracia");
        } catch (CouponCodeException e) {
        }
    }

    // 4] Nova registracia uzivatela do eventu s obmedzenou kapacitou
    @Test(groups = {"service"}, description = " Nova registracia uzivatela do eventu s obmedzenou kapacitou")
    public void newUserRegistrationTest4() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky 
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundle1.setWithCode(true);
        bundle1.setCapacity(2);
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));
        RegistrationCoupon rc1 = new RegistrationCoupon("A1", 20, 0);
        bundleService.createRegistrationCoupon(bundle1, rc1);

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundle2.setWithCode(true);
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));
        RegistrationCoupon rc2 = new RegistrationCoupon("A2", 20, 10);
        bundleService.createRegistrationCoupon(bundle2, rc2);

        // Vytvorime si subbaliky
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        subbundle1.setWithCode(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(sc1));
        RegistrationCoupon rc3 = new RegistrationCoupon("A3", 20, 0);
        bundleService.createRegistrationCoupon(subbundle1, rc3);

        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        subbundle2.setWithCode(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle sc2 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(sc2));
        RegistrationCoupon rc4 = new RegistrationCoupon("A4", 20, 0);
        bundleService.createRegistrationCoupon(subbundle2, rc4);

        RegistrationBundle subbundle3 = new RegistrationBundle();
        subbundle3.setTitle("Subbundle 3");
        subbundle3.setSubBundle(true);
        subbundle3.setWithCode(false);
        bundleService.createBundle(subbundle3);
        RegistrationCycle sc3 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle3.getUuid(), Lists.newArrayList(sc3));

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest orderRequest2 = new BundleOrderRequest(subbundle1.getUuid());
        BundleOrderRequest orderRequest3 = new BundleOrderRequest(subbundle2.getUuid());

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

        UserRegistration registration1 = registrationService.createUserRegistration("123", false, orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), "123", otherInfo);
        UserRegistration registration2 = registrationService.createUserRegistration("345", false, orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), "123", otherInfo);
        try {
            UserRegistration registration3 = registrationService.createUserRegistration("567", false, orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), "123", otherInfo);
            fail("Kapacita je plna ale proslo to");
        } catch (BundleCapacityFullException e) {
        }

//         List<RegistrationOption> options = bundleService.listRegistrationSubBundlesOptionsForUser("123");
//         assertEquals(options.size(), 1);
//         assertEquals(options.get(0).getTitle(), "Subbundle 3");
    }

    // 5] Registracia priamo noveho uzivatela bez volania do administracie
    @Test(groups = {"service"}, description = "Registracia priamo noveho uzivatela bez volania do administracie")
    public void newUserRegistrationTest5() throws BundleNotUniqueException, RequiredPropertyNullException, CyclesOverlapsException, ServiceExecutionFailed, IbanFormatException, NoRecordException, BundleCapacityFullException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Vytvorime si subbaliky
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(sc1));

        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle sc2 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(sc2));

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest orderRequest2 = new BundleOrderRequest(subbundle1.getUuid());
        BundleOrderRequest orderRequest3 = new BundleOrderRequest(subbundle2.getUuid());

        UserEvent ue = new UserEvent();
        ue.setUuid(UuidProvider.getUuid());
        ue.setFullName("abc abc");
        ue.setLastName("abc");
        ue.setOrganization("Alabama");
        ue.setEmail("muj@email.cz");

        Profile p = new Profile();
        p.setName("albama");
        p.setCity("City");
        p.setCountry(65);
        p.setStreet("street");
        p.setUuid(c1.getUuid());
        p.setZip("606060");

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

        try {
            registrationService.createUserRegistrationForNewUser(ue, null, null, null, p, otherInfo);
            fail("Chyba bundle ale proslo");
        } catch (RequiredPropertyNullException e) {
        }
        mockService.setCreateOrderWithProfileFinalPrice(new BigDecimal(700));
        registrationService.createUserRegistrationForNewUser(ue, "email", orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), p, otherInfo);
        UserRegistration registration = registrationService.loadUserActiveRegistrationDetailedByUserUuid(ue.getUuid());
    }

    @Test(groups = {"service"}, description = "")
    public void discountComputingTests() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundle.setWithCode(true);
        bundleService.createBundle(bundle);

        RegistrationCoupon rc1 = new RegistrationCoupon("A", 20, 1);
        RegistrationCoupon rc2 = new RegistrationCoupon("B", 71, 1);
        RegistrationCoupon rc3 = new RegistrationCoupon("C", 32, 1);

        bundleService.createRegistrationCoupon(bundle, rc1);
        bundleService.createRegistrationCoupon(bundle, rc2);
        bundleService.createRegistrationCoupon(bundle, rc3);

        RegistrationCycle c1 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1));

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

        // TEST 1 - dicsount 20% -- 400
        mockService.setCreateOrderFinalPrice(new BigDecimal(400));
        registrationService.createUserRegistration("12", false, new BundleOrderRequest(bundle.getUuid(), rc1.getKey(), QUANTITY_1), null, "12", otherInfo);
        UserRegistration ur = registrationService.loadUserActiveRegistrationDetailedByUserUuid("12");
        assertEquals(ur.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal("400")), 0, "Cena? " + ur.getOrders().get(0).getOverallPrice().toPlainString());

        // TEST 2 - dicsount 71% -- 145
        mockService.setCreateOrderFinalPrice(new BigDecimal(145));
        registrationService.createUserRegistration("13", false, new BundleOrderRequest(bundle.getUuid(), rc2.getKey(), QUANTITY_1), null, "13", otherInfo);
        ur = registrationService.loadUserActiveRegistrationDetailedByUserUuid("13");
        assertEquals(ur.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal("145")), 0, "Cena? " + ur.getOrders().get(0).getOverallPrice().toPlainString());

        // TEST 3 - dicsount 32% -- 340
        mockService.setCreateOrderFinalPrice(new BigDecimal(340));
        registrationService.createUserRegistration("14", false, new BundleOrderRequest(bundle.getUuid(), rc3.getKey(), QUANTITY_1), null, "14", otherInfo);
        ur = registrationService.loadUserActiveRegistrationDetailedByUserUuid("14");
        assertEquals(ur.getOrders().get(0).getOverallPrice().compareTo(new BigDecimal("340")), 0, "Cena? " + ur.getOrders().get(0).getOverallPrice().toPlainString());

        // TEST 4 - opakovane pouzity one time registracie
        try {
            registrationService.createUserRegistration("15", false, new BundleOrderRequest(bundle.getUuid(), rc3.getKey(), QUANTITY_1), null, "14", otherInfo);
            fail("Opakovana registracie prosla!");
        } catch (CouponCapacityException cau) {
        }

        // TEST 5 - neexistujuci kluc
        try {
            registrationService.createUserRegistration("16", false, new BundleOrderRequest(bundle.getUuid(), "XXX", QUANTITY_1), null, "14", otherInfo);
            fail("Spracovalo neexistujuci kluc kuponu");
        } catch (CouponCodeException e) {
        }
    }

    // 5] Registracia priamo noveho uzivatela bez volania do administracie
    @Test(groups = {"service"}, description = "Registracia priamo noveho uzivatela bez volania do administracie")
    public void registrationValidateTest() throws BundleNotUniqueException, RequiredPropertyNullException, CyclesOverlapsException, ServiceExecutionFailed, IbanFormatException, NoRecordException, BundleCapacityFullException, EventNotActiveException, CouponCodeException, AlreadyRegistredException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Vytvorime si subbaliky
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(sc1));

        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle sc2 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(sc2));

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(bundle1.getUuid());

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

        UserRegistration registration1 = registrationService.createUserRegistration("123", false, orderRequest1, null, "123", otherInfo);
        registrationInvoiceProcessor.orderStateChanged(registration1.getOrders().get(0).getUuid(), OrderState.CONFIRMED, true, DateProvider.getCurrentDate());
        registration1 = registrationService.loadRegistration(registration1.getUuid());
        assertEquals(registration1.getState(), RegistrationState.CONFIRMED);
        assertEquals(registration1.getOrders().get(0).getState(), RegistrationState.CONFIRMED);

        UserRegistration registration2 = registrationService.createUserRegistration("567", false, orderRequest1, null, "567", otherInfo);
        registrationInvoiceProcessor.orderStateChanged(registration2.getOrders().get(0).getUuid(), OrderState.CONFIRMED, true, DateProvider.getCurrentDate());
        registrationService.addOrderToRegistration("567", Lists.newArrayList(new BundleOrderRequest(subbundle1.getUuid())), "123");
        registrationService.addOrderToRegistration("567", Lists.newArrayList(new BundleOrderRequest(subbundle2.getUuid())), "123");

        registration2 = registrationService.loadRegistration(registration2.getUuid());
        assertEquals(registration2.getState(), RegistrationState.CONFIRMED);
        assertEquals(registration2.getOrders().get(0).getState(), RegistrationState.CONFIRMED);
        registrationInvoiceProcessor.orderStateChanged(registration2.getOrders().get(2).getUuid(), OrderState.CONFIRMED, true, DateProvider.getCurrentDate());
        registration2 = registrationService.loadRegistration(registration2.getUuid());
        assertEquals(registration2.getOrders().get(1).getState(), RegistrationState.ORDERED);
        assertEquals(registration2.getOrders().get(2).getState(), RegistrationState.CONFIRMED);
    }

    @Test(groups = {"service"}, description = "Testovanie integrity zlavovych kuponov")
    public void discountCodesTest() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky 
        RegistrationBundle b1 = new RegistrationBundle();
        b1.setTitle("Title 1");
        b1.setWithCode(true);
        bundleService.createBundle(b1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(b1.getUuid(), Lists.newArrayList(c1));
        RegistrationCoupon rc1 = new RegistrationCoupon("A1", 20, RegistrationCoupon.INFINITE_CAPACITY);
        bundleService.createRegistrationCoupon(b1, rc1);

        RegistrationBundle b2 = new RegistrationBundle();
        b2.setTitle("Title 2");
        b2.setWithCode(true);
        bundleService.createBundle(b2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(b2.getUuid(), Lists.newArrayList(c2));
        RegistrationCoupon rc2 = new RegistrationCoupon("A2", 20, 1);
        bundleService.createRegistrationCoupon(b2, rc2);

        // Vytvorime si subbaliky
        RegistrationBundle b3 = new RegistrationBundle();
        b3.setTitle("Subbundle 1");
        b3.setWithCode(true);
        b3.setSubBundle(true);
        bundleService.createBundle(b3);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(b3.getUuid(), Lists.newArrayList(sc1));
        RegistrationCoupon rc3 = new RegistrationCoupon("A3", 20, 3);
        bundleService.createRegistrationCoupon(b3, rc3);
        Date now = DateProvider.getCurrentDate();
        RegistrationCoupon rc4 = new RegistrationCoupon("A4", 20, 20);
        rc4.setEndDate(DateUtils.addDays(now, -4));
        bundleService.createRegistrationCoupon(b3, rc4);

        RegistrationCoupon rc5 = new RegistrationCoupon("A5", 20, 20);
        rc5.setEndDate(DateUtils.addDays(now, 4));
        bundleService.createRegistrationCoupon(b3, rc5);

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1Infinite = new BundleOrderRequest(b1.getUuid(), rc1.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest2Single = new BundleOrderRequest(b2.getUuid(), rc2.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest3Three = new BundleOrderRequest(b3.getUuid(), rc3.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest4TimeBad = new BundleOrderRequest(b3.getUuid(), rc4.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest5TimeOk = new BundleOrderRequest(b3.getUuid(), rc5.getKey(), QUANTITY_1);

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

        mockService.setCreateOrderFinalPrice(new BigDecimal(100));
        registrationService.createUserRegistration("1", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "1", otherInfo);

        RegistrationCoupon rc1l = registrationService.loadCoupon(rc1.getUuid());
        RegistrationCoupon rc3l = registrationService.loadCoupon(rc3.getUuid());
        assertEquals(rc1l.getUsedCount(), 1);
        assertEquals(rc3l.getUsedCount(), 1);
        registrationService.createUserRegistration("2", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "2", otherInfo);
        registrationService.createUserRegistration("3", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "3", otherInfo);

        forceSessionFlush();

        rc1l = registrationService.loadCoupon(rc1.getUuid());
        rc3l = registrationService.loadCoupon(rc3.getUuid());
        assertEquals(rc1l.getUsedCount(), 3);
        assertEquals(rc3l.getUsedCount(), 3);

        RegistrationCouponStats stats = bundleService.loadRegistrationCouponsStats(b3.getUuid());
        assertEquals(stats.getCouponsAll(), 3);
        assertEquals(stats.getCouponsUsed(), 1);


        registrationService.createUserRegistration("4", false, orderRequest1Infinite, null, "2", otherInfo);
        rc1l = registrationService.loadCoupon(rc1.getUuid());
        assertEquals(rc1l.getUsedCount(), 4);
        try {
            registrationService.createUserRegistration("5", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "3", otherInfo);
            fail();
        } catch (CouponCapacityException ex) {
        }

        registrationService.createUserRegistration("6", false, orderRequest2Single, null, "2", otherInfo);
        try {
            registrationService.createUserRegistration("7", false, orderRequest2Single, null, "2", otherInfo);
        } catch (CouponCapacityException ex) {
        }
        try {
            registrationService.createUserRegistration("8", false, orderRequest1Infinite, Lists.newArrayList(orderRequest4TimeBad), "1", otherInfo);
        } catch (CouponOutOfDateException ex) {
        }
        registrationService.createUserRegistration("9", false, orderRequest1Infinite, Lists.newArrayList(orderRequest5TimeOk), "1", otherInfo);
    }

    @Test(groups = {"service"}, description = "Testovanie sluzby na zrusenie objednavok po due date")
    public void cancelRegistrationsAfterDueDate() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Vytvorime si dva hlavne baliky 
        RegistrationBundle b1 = new RegistrationBundle();
        b1.setTitle("Title 1");
        b1.setWithCode(true);
        bundleService.createBundle(b1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(b1.getUuid(), Lists.newArrayList(c1));
        RegistrationCoupon rc1 = new RegistrationCoupon("A1", 20, RegistrationCoupon.INFINITE_CAPACITY);
        bundleService.createRegistrationCoupon(b1, rc1);

        RegistrationBundle b2 = new RegistrationBundle();
        b2.setTitle("Title 2");
        b2.setWithCode(true);
        bundleService.createBundle(b2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(b2.getUuid(), Lists.newArrayList(c2));
        RegistrationCoupon rc2 = new RegistrationCoupon("A2", 20, 1);
        bundleService.createRegistrationCoupon(b2, rc2);

        // Vytvorime si subbaliky
        RegistrationBundle b3 = new RegistrationBundle();
        b3.setTitle("Subbundle 1");
        b3.setWithCode(true);
        b3.setSubBundle(true);
        bundleService.createBundle(b3);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(b3.getUuid(), Lists.newArrayList(sc1));

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1Infinite = new BundleOrderRequest(b1.getUuid(), rc1.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest3Three = new BundleOrderRequest(b3.getUuid(), "", QUANTITY_1);

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

        mockService.setCreateOrderFinalPrice(new BigDecimal(400));
        UserRegistration reg1 = registrationService.createUserRegistration("1", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "1", otherInfo);
        UserRegistration reg2 = registrationService.createUserRegistration("2", false, orderRequest1Infinite, null, "2", otherInfo);
        registrationService.updateConfirmRegistrationOrderFromOrganizator(reg2.getOrders().get(0).getUuid(), "1", null, false);
        registrationService.addOrderToRegistration("2", Lists.newArrayList(orderRequest3Three), "2");

        forceSessionFlush();
        Date orderDate = reg1.getActiveOrders().get(0).getOrderDate();
        reg1.getActiveOrders().get(0).setOrderDate(DateUtils.addDays(orderDate, -10));
        registrationOrderDao.update(reg1.getActiveOrders().get(0));
        // zavoleme a nemalo by sa nic zrusit        
        assertEquals(registrationService.updateAndCancelAfterDueDateOrders(), 0);

        // upravime ale az za DUE DATE - jedna musi byt zrusena
        reg1.getActiveOrders().get(0).setOrderDate(DateUtils.addDays(orderDate, -25));
        registrationOrderDao.update(reg1.getActiveOrders().get(0));
        forceSessionFlush();
        assertEquals(registrationService.updateAndCancelAfterDueDateOrders(), 1);

        reg1 = registrationService.loadRegistration(reg1.getUuid());
        assertEquals(reg1.getState(), RegistrationState.TIMEOUTED);
        assertEquals(reg1.getOrders().get(0).getState(), RegistrationState.TIMEOUTED);

        // upravime reg2 order 2
        reg2 = registrationService.loadRegistrationDetailed(reg2.getUuid());
        orderDate = reg2.getActiveOrders().get(1).getOrderDate();
        reg2.getActiveOrders().get(1).setOrderDate(DateUtils.addDays(orderDate, -25));
        forceSessionFlush();
        assertEquals(registrationService.updateAndCancelAfterDueDateOrders(), 1);

        reg2 = registrationService.loadRegistration(reg2.getUuid());
        assertEquals(reg2.getState(), RegistrationState.CONFIRMED);
        assertEquals(reg2.getOrders().get(0).getState(), RegistrationState.CONFIRMED);
        assertEquals(reg2.getOrders().get(1).getState(), RegistrationState.TIMEOUTED);
    }

    @Test(groups = {"service"}, description = "Registracia do balickov ktore su automaticky aj B2B vstupenkami")
    public void b2bRightsTests() throws BundleNotUniqueException, RequiredPropertyNullException, CyclesOverlapsException, ServiceExecutionFailed, IbanFormatException, NoRecordException, BundleCapacityFullException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException, AlreadyRegistredException {
        // Vytvorime si dva hlavne baliky
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Vytvorime si subbaliky
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(sc1));

        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle sc2 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(sc2));

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest main1 = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest main2 = new BundleOrderRequest(bundle2.getUuid());
        BundleOrderRequest sub1 = new BundleOrderRequest(subbundle1.getUuid());
        BundleOrderRequest sub2 = new BundleOrderRequest(subbundle2.getUuid());

        // Registracia 1: Vytvorime 3 registracie. BUNDLE1 + SUBBUNDLE1 a BUDNLE2+SUBBUNDLE2

        UserEvent ue = new UserEvent();
        ue.setUuid(UuidProvider.getUuid());
        ue.setFullName("abc abc");
        ue.setLastName("abc");
        ue.setOrganization("Alabama");
        ue.setEmail("muj@email.cz");

        Profile p = new Profile();
        p.setName("albama");
        p.setCity("City");
        p.setCountry(65);
        p.setStreet("street");
        p.setUuid(c1.getUuid());
        p.setZip("606060");

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

        // B2B
        mockService.setCreateOrderWithProfileFinalPrice(new BigDecimal(400));
        UserRegistration ur1 = registrationService.createUserRegistration("1", false, main1, Lists.newArrayList(sub1), p, otherInfo);
        // B2B
        mockService.setCreateOrderWithProfileFinalPrice(new BigDecimal(500));
        UserRegistration ur2 = registrationService.createUserRegistration("2", false, main2, Lists.newArrayList(sub2), p, otherInfo);
        // NIE B2B
        mockService.setCreateOrderWithProfileFinalPrice(new BigDecimal(500));
        UserRegistration ur3 = registrationService.createUserRegistration("3", false, main2, Lists.newArrayList(sub1), p, otherInfo);

        // Test ci uzivatelia maju prava:
        forceSessionFlush();
        UserEvent ue1 = userEventService.loadUserEvent("1");
        UserEvent ue2 = userEventService.loadUserEvent("2");
        UserEvent ue3 = userEventService.loadUserEvent("3");

        assertFalse(userEventService.hasUserRight("1", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));
        assertFalse(userEventService.hasUserRight("2", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));
        assertFalse(userEventService.hasUserRight("3", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));

        // Upravime hlavny balicek 1 na B2B - User1 musi mat pravo
        bundle1.setB2b(true);
        bundleService.updateRegistrationBundle(bundle1);
        forceSessionFlush();
        assertTrue(userEventService.hasUserRight("1", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));

        // Vratime balicek naspat - User1 nemoze mat pravo
        bundle1.setB2b(false);
        bundleService.updateRegistrationBundle(bundle1);
        forceSessionFlush();
        assertFalse(userEventService.hasUserRight("1", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));

        // Nastvime na Balik1 a Subbalik2
        bundle1.setB2b(true);
        bundleService.updateRegistrationBundle(bundle1);
        subbundle2.setB2b(true);
        bundleService.updateRegistrationBundle(subbundle2);

        forceSessionFlush();
        assertTrue(userEventService.hasUserRight("1", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));
        assertTrue(userEventService.hasUserRight("2", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));
        assertFalse(userEventService.hasUserRight("3", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));

        forceSessionFlush();
        // Vytvorime novu registraciu
        registrationService.createUserRegistration("4", false, main1, Lists.newArrayList(sub1), p, otherInfo);
        // B2B
        registrationService.createUserRegistration("5", false, main2, Lists.newArrayList(sub2), p, otherInfo);

        forceSessionFlush();

        assertTrue(userEventService.hasUserRight("4", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));
        assertTrue(userEventService.hasUserRight("5", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));

        forceSessionFlush();

        UserRegistration ur5 = registrationService.loadUserActiveRegistrationDetailedByUserUuid("5");
        assertEquals(ur5.getState(), RegistrationState.ORDERED);
        registrationService.updateCancelRegistrationOrderFromOrganizator(ur5.getOrders().get(0).getUuid());

        forceSessionFlush();
        assertTrue(userEventService.hasUserRight("4", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));
        assertFalse(userEventService.hasUserRight("5", EventB2BModuleRoleModel.RIGHT_B2B_AGENT));
    }

    @Test(groups = {"service"})
    public void createUserRegistrationForNewManagedUser() throws BundleNotUniqueException, RequiredPropertyNullException, CyclesOverlapsException, ServiceExecutionFailed, NoRecordException {
        // Vytvorime si dva hlavne baliky
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Vytvorime si subbaliky
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(sc1));

        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle sc2 = DataProvider.getRegistrationCycle(200.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(sc2));

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest orderRequest2 = new BundleOrderRequest(subbundle1.getUuid());
        BundleOrderRequest orderRequest3 = new BundleOrderRequest(subbundle2.getUuid());

        UserEvent manager = new UserEvent();
        manager.setEmail("my@mail.my");
        manager.setFullName("Jožin");
        manager.setLastName("Kdovi");
        manager.setOrganization("Takeplace");
        manager.setPosition("Dev");
        manager.setPrivateGroup(new Group());
        manager.setUuid(UuidProvider.getUuid());

        try {
            userEventService.createUser(manager);
        } catch (Exception ex) {
            fail("Fail", ex);
        }

        Profile p = new Profile();
        p.setName("Profile");
        p.setCity("City");
        p.setCountry(65);
        p.setStreet("Street");
        p.setUuid(UuidProvider.getUuid());
        p.setZip("PSČ");

        try {
            registrationService.createUserRegistrationForNewManagedUser(manager.getUuid(), "Jmeno", "Prijmeni", "Organizace",
                    null, null, p);
            fail("Chyba bundle ale proslo");
        } catch (RequiredPropertyNullException e) {
            // v poradku
        } catch (Exception ex) {
            fail("Chyba, vyletelo na jinou vyjimku", ex);
        }
        mockService.setCreateOrderWithProfileFinalPrice(new BigDecimal(700));

        spService.updateBooleanProperty(RegistrationProperties.REGISTER_ANOTHER, false);
        try {
            registrationService.createUserRegistrationForNewManagedUser(manager.getUuid(), "Jmeno", "Prijmeni", "Organizace",
                    orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), p);
            fail("Vytvorila se registrace i kdyz je zakazano registrovat spravovane uzivatele");
        } catch (ServiceExecutionFailed ex) {
            // melo by byt v poradku
        } catch (Exception ex) {
            fail("Chyba vytvoreni registrace pro spravovaneho ucastnika", ex);
        }
        spService.updateBooleanProperty(RegistrationProperties.REGISTER_ANOTHER, true);
        try {
            registrationService.createUserRegistrationForNewManagedUser(manager.getUuid(), "Jmeno", "Prijmeni", "Organizace",
                    orderRequest1, Lists.newArrayList(orderRequest2, orderRequest3), p);
        } catch (Exception ex) {
            fail("Chyba vytvoreni registrace pro spravovaneho ucastnika", ex);
        }

        List<UserEvent> managed = pmService.listManagedParticipants(manager.getUuid());
        assertEquals(managed.size(), 1);

        try {
            registrationService.loadUserActiveRegistrationDetailedByUserUuid(managed.get(0).getUuid());
        } catch (Exception ex) {
            fail("Chyba nacteni vytvorene registrace", ex);
        }
    }

    @Test(groups = {"service"})
    public void listManagedRegistrationsTest() throws Exception {
        UserEvent manager = new UserEvent();
        manager.setEmail("manager@man.ma");
        manager.setFullName("Manager");
        manager.setLastName("Manager");
        manager.setPrivateGroup(new Group());
        manager.setUuid(UuidProvider.getUuid());
        try {
            userEventService.createUser(manager);
        } catch (Exception ex) {
            fail("Fail", ex);
        }

        spService.updateBooleanProperty(RegistrationProperties.REGISTER_ANOTHER, true);

        // vytvorime jednu registraci
        Profile p = new Profile();
        p.setName("Profile");
        p.setCity("City");
        p.setCountry(65);
        p.setStreet("Street");
        p.setUuid(UuidProvider.getUuid());
        p.setZip("PSČ");

        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 1");
        bundleService.createBundle(bundle1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(bundle1.getUuid());

        try {
            mockService.setCreateOrderWithProfileFinalPrice(new BigDecimal(700));
            registrationService.createUserRegistrationForNewManagedUser(manager.getUuid(), "Jmeno", "Prijmeni", "Organizace",
                    orderRequest1, null, p);
        } catch (Exception ex) {
            fail("Chyba vytvoreni registrace pro spravovaneho ucastnika", ex);
        }

        // prepneme povoleni a zkusime nacitat
        spService.updateBooleanProperty(RegistrationProperties.REGISTER_ANOTHER, false);
        List<UserRegistration> list = registrationService.listManagedRegistrations(manager.getUuid());
        assertEquals(list.size(), 0);

        spService.updateBooleanProperty(RegistrationProperties.REGISTER_ANOTHER, true);
        list = registrationService.listManagedRegistrations(manager.getUuid());
        assertEquals(list.size(), 1);
    }
}
