package consys.event.registration.core;

import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.common.utils.date.DateProvider;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.test.AbstractEventDaoTestCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeMethod;

/**
 *
 * @author Palo
 */
@ContextConfiguration(locations = {"classpath:/consys/event/common/core/event-common-spring.xml", "event-registration-spring.xml","classpath:/consys/event/b2b/core/event-b2b-spring.xml", "classpath:consys/event/common/test/core/service-mocks.xml"})
public abstract class AbstractRegistrationTest extends AbstractEventDaoTestCase {

    @Autowired
    private SystemPropertyService spService;

    @BeforeMethod(alwaysRun = true)
    public void initializeSystemProperties() throws NoRecordException {
        spService.updateStringProperty(CommonProperties.EVENT_ADDRESS, "U Vodarny 2a");
        spService.updateStringProperty(CommonProperties.EVENT_CURRENCY, "CZK");
        spService.updateStringProperty(CommonProperties.EVENT_EMAIL, "info@takeplace.eu");
        spService.updateDateProperty(CommonProperties.EVENT_FROM, DateProvider.getCurrentDate());
        spService.updateStringProperty(CommonProperties.EVENT_NAME, "Event Name");
        spService.updateStringProperty(CommonProperties.EVENT_TIMEZONE, "Europe/London");
        spService.updateDateProperty(CommonProperties.EVENT_TO, DateProvider.getCurrentDate());
    }
}
