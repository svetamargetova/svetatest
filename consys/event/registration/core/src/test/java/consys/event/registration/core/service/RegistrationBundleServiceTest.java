package consys.event.registration.core.service;

import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.common.utils.UuidProvider;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.UserEventDao;
import consys.event.registration.core.AbstractRegistrationTest;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.dao.UserRegistrationDao;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.common.core.exception.BundleCapacityFullException;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.event.registration.core.data.provider.DataProvider;
import consys.event.registration.core.exception.CycleNotEmptyException;
import consys.event.registration.core.exception.CyclesOverlapsException;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationBundleServiceTest extends AbstractRegistrationTest {

    @Autowired(required = true)
    private RegistrationBundleService bundleService;
    @Autowired(required = true)
    private UserEventDao userEventDao;
    @Autowired(required = true)
    private UserRegistrationDao registrationDao;

    @Test(groups = {"service"})
    public void createBundle() throws NoRecordException, BundleNotUniqueException, RequiredPropertyNullException, CyclesOverlapsException, ServiceExecutionFailed {
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setOrder(1);
        bundle.setUuid(UuidProvider.getUuid());
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(DataProvider.getRegistrationCycle(200.0)));
        bundle = new RegistrationBundle();
        bundle.setTitle("Title 2");
        bundle.setOrder(2);
        bundle.setVat(BigDecimal.ONE);
        bundle.setUuid(UuidProvider.getUuid());
        bundleService.createBundle(bundle);
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(DataProvider.getRegistrationCycle(200.0)));

        RegistrationBundle lb = bundleService.loadRegistrationBundle(bundle.getUuid());
        assertEquals(lb.getVat().compareTo(BigDecimal.ONE), 0);

        RegistrationBundle subbundle = new RegistrationBundle();
        subbundle.setTitle("Subundle 1");
        subbundle.setSubBundle(true);
        subbundle.setOrder(1);
        subbundle.setUuid(UuidProvider.getUuid());
        bundleService.createBundle(subbundle);
        bundleService.createRegistrationCycles(subbundle.getUuid(), Lists.newArrayList(DataProvider.getRegistrationCycle(200.0)));

        subbundle = new RegistrationBundle();
        subbundle.setTitle("Subundle 2");
        subbundle.setSubBundle(true);
        subbundle.setOrder(4);
        subbundle.setUuid(UuidProvider.getUuid());
        bundleService.createBundle(subbundle);
        bundleService.createRegistrationCycles(subbundle.getUuid(), Lists.newArrayList(DataProvider.getRegistrationCycle(200.0)));

        lb = bundleService.loadRegistrationBundle(subbundle.getUuid());
        assertEquals(lb.getVat().compareTo(BigDecimal.ZERO), 0);

        List<RegistrationOption> options = bundleService.listRegistrationSubBundlesWithMainBundle(bundle.getUuid());
        assertEquals(options.size(), 3, "Nerovna sa nacitany pocet bundlu");

        List<RegistrationBundle> bs = bundleService.listRegistrationMainBundlesWithCycles();
        assertTrue(bs.size() == 2, "Nerovna sa nacitany pocet bundlu");

        assertEquals(bs.get(0).getTitle(), "Title 1");
        assertEquals(bs.get(1).getTitle(), "Title 2");

        bs = bundleService.listAllBundles();
        assertEquals(bs.size(), 4, "Nerovna sa nacitany pocet bundlu");
    }

    @Test(groups = {"service"}, expectedExceptions = {BundleNotUniqueException.class})
    public void createBundlesWithUniquityViolation() throws NoRecordException, BundleNotUniqueException, RequiredPropertyNullException {
        List<RegistrationBundle> bundles = Lists.newArrayList();
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setTitle("Title 1");
        bundles.add(bundle);
        bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundles.add(bundle);
        bundleService.createBundles(bundles);

    }

    @Test(groups = {"service"})
    public void createCycles() throws NoRecordException, BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, RequiredPropertyNullException {
        // Bundle 1
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        // Bundle 1 - Cycle 1
        RegistrationCycle c11 = DataProvider.getRegistrationCycle(500.99, 2000, 10, 3, 2000, 10, 13);
        // Bundle 1 - Cycle 2
        RegistrationCycle c12 = DataProvider.getRegistrationCycle(10000.56, 2000, 9, 1, 2000, 10, 2);

        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c11, c12));

        // Bundle 2
        bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setTitle("Title 2");
        bundleService.createBundle(bundle);

        // Bundle 2 - Cycle 1
        RegistrationCycle c21 = DataProvider.getRegistrationCycle(500.99, 2000, 10, 3, 2000, 10, 13);
        // Bundle 2 - Cycle 2
        RegistrationCycle c22 = DataProvider.getRegistrationCycle(10000.56, 2000, 9, 1, 2000, 10, 2);
        // Bundle 2 - Cycle 3
        RegistrationCycle c23 = DataProvider.getRegistrationCycle(10000.56, 2000, 8, 1, 2000, 8, 30);

        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c21, c22, c23));
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(DataProvider.getRegistrationCycle(200.0)));

        List<RegistrationBundle> bundles = bundleService.listRegistrationMainBundlesWithCycles();
        assertEquals(bundles.size(), 2);
        assertEquals(bundles.get(0).getCycles().size(), 2);
        assertEquals(bundles.get(1).getCycles().size(), 4);

        bundles = bundleService.listRegistrationMainBundles();
        assertEquals(bundles.size(), 2);

        List<RegistrationOption> options = bundleService.listMainRegistrationBundlesOptions();
        assertEquals(options.size(), 2);
    }

    @Test(groups = {"service"})
    public void createCyclesViolatingIntersections() throws NoRecordException, BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, RequiredPropertyNullException {
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        // Violating C1S < C2S < C1E < C2E
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(500.99, 2000, 10, 3, 2000, 10, 13);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(10000.56, 2000, 10, 5, 2000, 10, 20);
        try {
            bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1, c2));
            fail("1 - Musi violating!");
        } catch (CyclesOverlapsException c) {
        }

        // Violating C2S < C1S < C2E < C1E
        c1 = DataProvider.getRegistrationCycle(500.99, 2000, 10, 3, 2000, 10, 13);
        c2 = DataProvider.getRegistrationCycle(10000.56, 2000, 10, 1, 2000, 10, 5);
        try {
            bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1, c2));
            fail("2 - Musi violating!");
        } catch (CyclesOverlapsException c) {
        }

        // Violating C1S < C2S < C2E < C1E
        c1 = DataProvider.getRegistrationCycle(500.99, 2000, 10, 3, 2000, 10, 13);
        c2 = DataProvider.getRegistrationCycle(10000.56, 2000, 10, 4, 2000, 10, 6);
        try {
            bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1, c2));
            fail("3 - Musi violating!");
        } catch (CyclesOverlapsException c) {
        }
    }

    //
    // TODO: ENABLED = FALSE!!!
    //
    @Test(groups = {"service"}, expectedExceptions = {CycleNotEmptyException.class}, enabled = false)
    public void deleteBundleViolatingRegistrations() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, CycleNotEmptyException, NoRecordException, RequiredPropertyNullException {
        // Vytvorime si usera
        UserEvent ue = new UserEvent();
        ue.setFullName("1");
        ue.setLastName("1");
        ue.setUuid("aaa");
        ue.setEmail("muj@email.cz");
        Group g = new Group();
        ue.setPrivateGroup(g);
        userEventDao.create(ue);

        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1));

        // Vytvorime registraciu
        UserRegistration reg = new UserRegistration();
//        reg.setCycle(c1);
//        reg.setPaid(BigDecimal.ZERO);
//        reg.setRegistredDate(DateProvider.getCurrentDate());
        reg.setState(RegistrationState.ORDERED);
        reg.setUser(ue);
        reg.setUuid(UuidProvider.getUuid());
        registrationDao.create(reg);

        try {
            bundleService.deleteBundle(bundle.getUuid());
            fail();
        } catch (CycleNotEmptyException c) {
        }

        bundleService.deleteCycle(c1.getUuid());
    }

    @Test(groups = {"service"}, expectedExceptions = {NoRecordException.class})
    public void deleteBundle() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, CycleNotEmptyException, NoRecordException, RequiredPropertyNullException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1));

        bundleService.deleteBundle(bundle.getUuid());

        bundleService.listRegistrationMainBundlesWithCycles();
    }

    @Test(groups = {"service"})
    public void deleteCycle() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, CycleNotEmptyException, NoRecordException, RequiredPropertyNullException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1));

        bundleService.deleteCycle(c1.getUuid());

        RegistrationBundle b = bundleService.loadById(bundle.getId());
        assertTrue(b.getCycles().isEmpty());
    }

    @Test(groups = {"service"})
    public void createDuplicate() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, NoRecordException, RequiredPropertyNullException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1));

        // Vytvorime duplikat
        RegistrationBundle newBundle = bundleService.createDuplicate(bundle.getUuid(), "Title 1 - Copy");
        RegistrationBundle bb1 = bundleService.loadById(newBundle.getId());
        assertEquals(bb1.getTitle(), "Title 1 - Copy");
        assertTrue(bb1.getCycles().size() == 1);
    }

    @Test(groups = {"service"})
    public void updateBundle() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, RequiredPropertyNullException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1));

        bundle.setDescription("Description test");
        bundleService.updateRegistrationBundle(bundle);
    }

    //
    // TODO: ENABLED = FALSE!!!
    //
    @Test(groups = {"service"}, expectedExceptions = {BundleCapacityFullException.class}, enabled = false)
    public void updateBundleViolatingCapacity() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, RequiredPropertyNullException, NoRecordException, IbanFormatException, EventNotActiveException, CouponCodeException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundle.setCapacity(5);
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.add(Calendar.DAY_OF_MONTH, -1);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.add(Calendar.DAY_OF_MONTH, 1);
        c1.setTo(c1e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1));


        //registrationService.createUserRegistrationAndIssueAnInvoice("111", bundle.getUuid(), null,"abc");
        //registrationService.createUserRegistrationAndIssueAnInvoice("112", bundle.getUuid(), null,"abc");
        //registrationService.createUserRegistrationAndIssueAnInvoice("113", bundle.getUuid(), null,"abc");

        bundle.setCapacity(2);
        bundleService.updateRegistrationBundle(bundle);
    }

    @Test(groups = {"service"})
    public void updateCycle() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, NoRecordException, RequiredPropertyNullException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());

        RegistrationCycle c2 = new RegistrationCycle();
        c2.setPrice(BigDecimal.valueOf(500.99));
        Calendar c2s = GregorianCalendar.getInstance();
        c2s.set(2000, 10, 14);
        c2.setFrom(c2s.getTime());

        Calendar c2e = GregorianCalendar.getInstance();
        c2e.set(2000, 10, 15);
        c2.setTo(c2e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1, c2));

        RegistrationCycle cycle = bundleService.loadRegistrationCycle(c2.getUuid());
        cycle.setPrice(new BigDecimal(800));
        c2e.set(2000, 10, 16);
        cycle.setTo(c2e.getTime());

        bundleService.updateRegistrationCycle(cycle);
    }

    @Test(groups = {"service"}, expectedExceptions = {CyclesOverlapsException.class})
    public void updateCycleViolatingOverlaping() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, NoRecordException, RequiredPropertyNullException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundleService.createBundle(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());

        RegistrationCycle c2 = new RegistrationCycle();
        c2.setPrice(BigDecimal.valueOf(500.99));
        Calendar c2s = GregorianCalendar.getInstance();
        c2s.set(2000, 10, 14);
        c2.setFrom(c2s.getTime());

        Calendar c2e = GregorianCalendar.getInstance();
        c2e.set(2000, 10, 15);
        c2.setTo(c2e.getTime());
        bundleService.createRegistrationCycles(bundle.getUuid(), Lists.newArrayList(c1, c2));

        RegistrationCycle cycle = bundleService.loadRegistrationCycle(c2.getUuid());
        cycle.setPrice(new BigDecimal(800));
        c2s.set(2000, 10, 12);
        cycle.setFrom(c2s.getTime());

        bundleService.updateRegistrationCycle(cycle);
    }

    @Test(groups = {"service"})
    public void bundleOrderTest() throws NoRecordException, BundleNotUniqueException, RequiredPropertyNullException, CyclesOverlapsException, ServiceExecutionFailed {
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setOrder(1);
        bundle.setUuid(UuidProvider.getUuid());
        bundle.setTitle("Bundle 1");
        bundleService.createBundle(bundle);

        bundle = new RegistrationBundle();
        bundle.setTitle("Bundle 2");
        bundle.setOrder(2);
        bundle.setVat(BigDecimal.ONE);
        bundle.setUuid(UuidProvider.getUuid());
        bundleService.createBundle(bundle);

        bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setOrder(3);
        bundle.setUuid(UuidProvider.getUuid());
        bundle.setTitle("Bundle 3");
        bundleService.createBundle(bundle);

        bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setOrder(4);
        bundle.setUuid(UuidProvider.getUuid());
        bundle.setTitle("Bundle 4");
        bundleService.createBundle(bundle);

        bundle = new RegistrationBundle();
        bundle.setCapacity(100);
        bundle.setOrder(5);
        bundle.setUuid(UuidProvider.getUuid());
        bundle.setTitle("Bundle 5");
        bundleService.createBundle(bundle);

        // test ze vsetko je jak ma byt teda poradi je spravne
        List<RegistrationBundle> b = bundleService.listRegistrationMainBundles();
        for (int i = 0; i < b.size(); i++) {
            assertEquals(b.get(i).getOrder(), i + 1);
            assertEquals(b.get(i).getTitle(), ("Bundle " + (i + 1)));
        }

        forceSessionFlush();

        // zmenime poradie ... najskor 4 -> 1
        bundleService.updateRegistrationBundleOrder(b.get(3).getUuid(), b.get(3).getOrder(), 1, false);

        b = bundleService.listRegistrationMainBundles();

        assertEquals(b.get(0).getOrder(), 1);
        assertEquals(b.get(0).getTitle(), "Bundle 4");

        assertEquals(b.get(1).getOrder(), 2);
        assertEquals(b.get(1).getTitle(), "Bundle 1");

        assertEquals(b.get(2).getOrder(), 3);
        assertEquals(b.get(2).getTitle(), "Bundle 2");

        assertEquals(b.get(3).getOrder(), 4);
        assertEquals(b.get(3).getTitle(), "Bundle 3");

        assertEquals(b.get(4).getOrder(), 5);
        assertEquals(b.get(4).getTitle(), "Bundle 5");

        // teraz dame poradie naspat
        forceSessionFlush();

        bundleService.updateRegistrationBundleOrder(b.get(0).getUuid(), b.get(0).getOrder(), 4, false);

        b = bundleService.listRegistrationMainBundles();

        assertEquals(b.get(0).getOrder(), 1);
        assertEquals(b.get(0).getTitle(), "Bundle 1");

        assertEquals(b.get(1).getOrder(), 2);
        assertEquals(b.get(1).getTitle(), "Bundle 2");

        assertEquals(b.get(2).getOrder(), 3);
        assertEquals(b.get(2).getTitle(), "Bundle 3");

        assertEquals(b.get(3).getOrder(), 4);
        assertEquals(b.get(3).getTitle(), "Bundle 4");

        assertEquals(b.get(4).getOrder(), 5);
        assertEquals(b.get(4).getTitle(), "Bundle 5");

        // zmenime poradie ... najskor 2 -> 1        
        forceSessionFlush();

        bundleService.updateRegistrationBundleOrder(b.get(1).getUuid(), b.get(1).getOrder(), 1, false);

        b = bundleService.listRegistrationMainBundles();

        assertEquals(b.get(0).getOrder(), 1);
        assertEquals(b.get(0).getTitle(), "Bundle 2");

        assertEquals(b.get(1).getOrder(), 2);
        assertEquals(b.get(1).getTitle(), "Bundle 1");

        assertEquals(b.get(2).getOrder(), 3);
        assertEquals(b.get(2).getTitle(), "Bundle 3");

        assertEquals(b.get(3).getOrder(), 4);
        assertEquals(b.get(3).getTitle(), "Bundle 4");

        assertEquals(b.get(4).getOrder(), 5);
        assertEquals(b.get(4).getTitle(), "Bundle 5");
    }
}
