package consys.event.registration.core.service;

import consys.event.registration.core.bo.thumb.RegistrationCouponListItem;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.event.registration.core.module.RegistrationStateChangeListener;
import consys.common.core.exception.CouponCodeException;
import consys.event.registration.api.list.RegistrationsList;
import consys.event.registration.core.bo.thumb.RegistrationListItem;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.registration.api.list.ParticipantList;
import consys.event.registration.core.AbstractRegistrationTest;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.ParticipantListItem;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.so.RegistrationOtherInfo;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.common.utils.UuidProvider;
import consys.event.common.core.EventOrderStateChangeListener.OrderState;
import consys.event.common.test.core.mock.PaymentWebServiceMock;
import consys.event.registration.api.list.CheckInList;
import consys.event.registration.api.list.RegistrationCouponList;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.core.bo.thumb.CheckInListItem;
import consys.event.registration.core.data.provider.DataProvider;
import consys.event.registration.core.exception.CyclesOverlapsException;
import consys.event.registration.core.exception.RegistrationNotActiveException;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationListServiceTest extends AbstractRegistrationTest {

    // konstanty
    private static final int QUANTITY_1 = 1;
    // sluzby
    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private RegistrationBundleService bundleService;
    @Autowired
    private ParticipantListService participantListService;
    @Autowired
    private RegistrationsListService registrationsListService;
    @Autowired
    private CheckInListService checkInListService;
    @Autowired
    private RegistrationStateChangeListener registrationInvoiceProcessor;
    @Autowired
    private RegistrationCouponListService couponListService;
    @Autowired
    private PaymentWebServiceMock mockService;

    @Test(groups = {"service"})
    public void testParticipantListServices() throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Bundle 1
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 11");
        bundleService.createBundle(bundle1);

        RegistrationCycle c1 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        // Bundle 2
        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 22");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Order reguesty
        BundleOrderRequest b1o = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest b2o = new BundleOrderRequest(bundle2.getUuid());

        /// Vytvorime si 5 registracii - 2 - Bundle1, 3 budnle 2
        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
        UserRegistration reg1 = registrationService.createUserRegistration("12", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg2 = registrationService.createUserRegistration("22", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg3 = registrationService.createUserRegistration("32", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg4 = registrationService.createUserRegistration("42", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg5 = registrationService.createUserRegistration("52", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg6canceled = registrationService.createUserRegistration("62", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg7canceled = registrationService.createUserRegistration("72", false, b2o, null, "uuid", otherInfo);
        registrationService.updateCancelRegistrationOrderFromOwner(reg6canceled.getOrders().get(0).getUuid());
        registrationService.updateCancelRegistrationOrderFromOwner(reg7canceled.getOrders().get(0).getUuid());

        // Nacitame zoznam - participant LIST
        Constraints participantListC = new Constraints();
        participantListC.setFilterTag(ParticipantList.Filter_ALL);
        participantListC.setOrderers(new int[]{ParticipantList.Order_LAST_NAME});
        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(10);
        List<ParticipantListItem> regs = participantListService.listItems(participantListC, p);
        assertEquals(regs.size(), 5);
        assertEquals(participantListService.listAllCount(participantListC), 5);

        for (ParticipantListItem item : regs) {
            if (item.getUserUuid().equals("12")) {
            } else if (item.getUserUuid().equals("22")) {
            } else if (item.getUserUuid().equals("32")) {
            } else if (item.getUserUuid().equals("42")) {
            } else if (item.getUserUuid().equals("52")) {
            } else {
                fail("User uuid sa nenaselo " + item.getUserUuid());
            }
        }
    }

    @Test(groups = {"service"})
    public void testRegistrationListServices()
            throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, CouponCapacityException, CouponOutOfDateException {
        // Bundle 1
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 11");
        bundleService.createBundle(bundle1);

        RegistrationCycle c1 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        // Bundle 2
        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 22");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Subbundle 1
        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setSubBundle(true);
        bundleService.createBundle(subbundle1);
        RegistrationCycle bc1 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(subbundle1.getUuid(), Lists.newArrayList(bc1));

        // Subbundle 2
        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setSubBundle(true);
        bundleService.createBundle(subbundle2);
        RegistrationCycle bc2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(subbundle2.getUuid(), Lists.newArrayList(bc2));

        // Subbundle 3
        RegistrationBundle subbundle3 = new RegistrationBundle();
        subbundle3.setTitle("Subbundle 3");
        subbundle3.setSubBundle(true);
        bundleService.createBundle(subbundle3);
        RegistrationCycle bc3 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(subbundle3.getUuid(), Lists.newArrayList(bc3));

        // Order reguesty
        BundleOrderRequest b1o = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest b2o = new BundleOrderRequest(bundle2.getUuid());

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
        /// Vytvorime si 5 registracii - 2 - Bundle1, 3 budnle 2
        UserRegistration reg1 = registrationService.createUserRegistration("11", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg2 = registrationService.createUserRegistration("22", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg3 = registrationService.createUserRegistration("33", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg4 = registrationService.createUserRegistration("44", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg5 = registrationService.createUserRegistration("55", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg6canceled = registrationService.createUserRegistration("66", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg7canceled = registrationService.createUserRegistration("77", false, b2o, null, "uuid", otherInfo);

        registrationInvoiceProcessor.orderStateChanged(reg1.getOrders().get(0).getUuid(), OrderState.CONFIRMED, true, null);
        registrationInvoiceProcessor.orderStateChanged(reg2.getOrders().get(0).getUuid(), OrderState.CONFIRMED, true, null);
        registrationInvoiceProcessor.orderStateChanged(reg3.getOrders().get(0).getUuid(), OrderState.CONFIRMED, true, null);
        registrationInvoiceProcessor.orderStateChanged(reg4.getOrders().get(0).getUuid(), OrderState.CONFIRMED, true, null);

        registrationService.updateCancelRegistrationOrderFromOwner(reg6canceled.getOrders().get(0).getUuid());
        registrationService.updateCancelRegistrationOrderFromOwner(reg7canceled.getOrders().get(0).getUuid());

        registrationService.addOrderToRegistration("11", Lists.newArrayList(new BundleOrderRequest(subbundle1.getUuid())), "uuid");
        registrationService.addOrderToRegistration("33", Lists.newArrayList(new BundleOrderRequest(subbundle1.getUuid())), "uuid");
        registrationService.addOrderToRegistration("44", Lists.newArrayList(new BundleOrderRequest(subbundle1.getUuid())), "uuid");
        String orderUuid = registrationService.addOrderToRegistration("22", Lists.newArrayList(new BundleOrderRequest(subbundle1.getUuid()), new BundleOrderRequest(subbundle2.getUuid())), "uuid").getUuid();
        registrationService.updateCancelRegistrationOrderFromOwner(orderUuid);
        registrationService.addOrderToRegistration("22", Lists.newArrayList(new BundleOrderRequest(subbundle1.getUuid()), new BundleOrderRequest(subbundle2.getUuid())), "uuid");

        // Zoznam registracny
        Constraints regListC = new Constraints();
        regListC.setFilterTag(RegistrationsList.Filter_STATE_ALL);
        regListC.setOrderers(new int[]{RegistrationsList.Order_LAST_NAME});
        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(10);
        // Vsecci participanti
        List<RegistrationListItem> regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 5);
        assertEquals(registrationsListService.listAllCount(regListC), 5);
        for (RegistrationListItem item : regItems) {
            if (item.getUserUuid().equals("11")) {
            } else if (item.getUserUuid().equals("22")) {
            } else if (item.getUserUuid().equals("33")) {
            } else if (item.getUserUuid().equals("44")) {
            } else if (item.getUserUuid().equals("55")) {
            } else {
                fail("User uuid sa nenaselo " + item.getUserUuid());
            }
        }

        // Len budnle 2 - 3 zaznamy
        regListC.setStringFilterValue(bundle2.getUuid());
        regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 3);
        assertEquals(registrationsListService.listAllCount(regListC), 3);

        // Vsecci aktivny - 0

        // spravi jednu registraciu ok
        registrationService.updateConfirmRegistrationOrderFromOrganizator(reg2.getOrders().get(0).getUuid(), UuidProvider.getUuid(), null, false);
        // nacitame vsecky validovane
        regListC.setFilterTag(RegistrationsList.Filter_STATE_REGISTERED);
        regListC.setStringFilterValue(null);
        regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 4);
        assertEquals(registrationsListService.listAllCount(regListC), 4);

        // nacitame vsecky validovane pro bundle 2 - 1
        regListC.setFilterTag(RegistrationsList.Filter_STATE_REGISTERED);
        regListC.setStringFilterValue(bundle2.getUuid());
        regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 2);
        assertEquals(registrationsListService.listAllCount(regListC), 2);

        // nacitame vsecky validovane pro bundle 1 - 1
        regListC.setFilterTag(RegistrationsList.Filter_STATE_REGISTERED);
        regListC.setStringFilterValue(bundle1.getUuid());
        regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 2);
        assertEquals(registrationsListService.listAllCount(regListC), 2);

        // nacitame len subbalicek 1
        regListC.setFilterTag(RegistrationsList.Filter_STATE_ALL);
        regListC.setStringFilterValue(subbundle1.getUuid());
        regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 4);
        assertEquals(registrationsListService.listAllCount(regListC), 4);

        // nacitame len subbalicek 2
        regListC.setFilterTag(RegistrationsList.Filter_STATE_ALL);
        regListC.setStringFilterValue(subbundle2.getUuid());
        regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 1);
        assertEquals(registrationsListService.listAllCount(regListC), 1);

        // nacitame len subbalicek 3
        regListC.setFilterTag(RegistrationsList.Filter_STATE_ALL);
        regListC.setStringFilterValue(subbundle3.getUuid());
        regItems = registrationsListService.listItems(regListC, p);
        assertEquals(regItems.size(), 0);
        assertEquals(registrationsListService.listAllCount(regListC), 0);

    }

    @Test(groups = {"service"})
    public void testCheckInListServices()
            throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, RegistrationNotActiveException, CouponCapacityException, CouponOutOfDateException {

        // Bundle 1
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setTitle("Title 11");
        bundleService.createBundle(bundle1);

        RegistrationCycle c1 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle1.getUuid(), Lists.newArrayList(c1));

        // Bundle 2
        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 22");
        bundleService.createBundle(bundle2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(bundle2.getUuid(), Lists.newArrayList(c2));

        // Order reguesty
        BundleOrderRequest b1o = new BundleOrderRequest(bundle1.getUuid());
        BundleOrderRequest b2o = new BundleOrderRequest(bundle2.getUuid());

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
        mockService.setCreateOrderFinalPrice(new BigDecimal(500));
        /// Vytvorime si 5 registracii - 2 - Bundle1, 3 budnle 2
        UserRegistration reg1 = registrationService.createUserRegistration("11", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg2 = registrationService.createUserRegistration("22", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg3 = registrationService.createUserRegistration("33", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg4 = registrationService.createUserRegistration("44", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg5 = registrationService.createUserRegistration("55", false, b2o, null, "uuid", otherInfo);
        UserRegistration reg6canceled = registrationService.createUserRegistration("66", false, b1o, null, "uuid", otherInfo);
        UserRegistration reg7canceled = registrationService.createUserRegistration("77", false, b2o, null, "uuid", otherInfo);
        registrationService.updateCancelRegistrationOrderFromOwner(reg6canceled.getOrders().get(0).getUuid());
        registrationService.updateCancelRegistrationOrderFromOwner(reg7canceled.getOrders().get(0).getUuid());

        // oznacime 4 za confirmed
        registrationService.updateConfirmRegistrationOrderFromOrganizator(reg1.getOrders().get(0).getUuid(), null, null, true);
        registrationService.updateConfirmRegistrationOrderFromOrganizator(reg2.getOrders().get(0).getUuid(), null, null, true);
        registrationService.updateConfirmRegistrationOrderFromOrganizator(reg4.getOrders().get(0).getUuid(), null, null, true);
        registrationService.updateConfirmRegistrationOrderFromOrganizator(reg5.getOrders().get(0).getUuid(), null, null, true);

        // nacitame vsetkych count = 5
        Constraints c = new Constraints();
        c.setFilterTag(CheckInList.Filter_ALL);
        List<CheckInListItem> items = checkInListService.listItems(c, null);
        assertEquals(items.size(), 4);
        assertEquals(checkInListService.listAllCount(c), 4);

        // oznacime si 3 za checknute uz
        registrationService.updateRegistrationCheckIn(reg2.getUuid(), true, 1);
        registrationService.updateRegistrationCheckIn(reg4.getUuid(), true, 1);
        registrationService.updateRegistrationCheckIn(reg5.getUuid(), true, 1);

        c.setFilterTag(CheckInList.Filter_CHECKED);
        items = checkInListService.listItems(c, null);
        assertEquals(items.size(), 3);
        assertEquals(checkInListService.listAllCount(c), 3);

        c.setFilterTag(CheckInList.Filter_NOT_CHECKED);
        items = checkInListService.listItems(c, null);
        assertEquals(items.size(), 1);
        assertEquals(checkInListService.listAllCount(c), 1);
    }

    @Test(groups = {"service"})
    public void testRegistrationCouponListService()
            throws BundleNotUniqueException, CyclesOverlapsException, ServiceExecutionFailed, BundleCapacityFullException, AlreadyRegistredException, NoRecordException, RequiredPropertyNullException, IbanFormatException, EventNotActiveException, CouponCodeException, RegistrationNotActiveException, CouponCapacityException, CouponOutOfDateException, ParseException {

        // Vytvorime si dva hlavne baliky 
        RegistrationBundle b1 = new RegistrationBundle();
        b1.setTitle("Title 1");
        b1.setWithCode(true);
        bundleService.createBundle(b1);
        RegistrationCycle c1 = DataProvider.getRegistrationCycle(400.0);
        RegistrationCycle c11 = DataProvider.getRegistrationCycle(400.0, "10.10.2011 12:00", "12.10.2011 12:00");
        bundleService.createRegistrationCycles(b1.getUuid(), Lists.newArrayList(c1, c11));
        RegistrationCoupon rc1 = new RegistrationCoupon("A1", 20, RegistrationCoupon.INFINITE_CAPACITY);
        bundleService.createRegistrationCoupon(b1, rc1);

        RegistrationBundle b2 = new RegistrationBundle();
        b2.setTitle("Title 2");
        b2.setWithCode(true);
        bundleService.createBundle(b2);
        RegistrationCycle c2 = DataProvider.getRegistrationCycle(500.0);
        bundleService.createRegistrationCycles(b2.getUuid(), Lists.newArrayList(c2));
        RegistrationCoupon rc2 = new RegistrationCoupon("A2", 20, 1);
        bundleService.createRegistrationCoupon(b2, rc2);

        // Vytvorime si subbaliky
        RegistrationBundle b3 = new RegistrationBundle();
        b3.setTitle("Subbundle 1");
        b3.setWithCode(true);
        b3.setSubBundle(true);
        bundleService.createBundle(b3);
        RegistrationCycle sc1 = DataProvider.getRegistrationCycle(100.0);
        bundleService.createRegistrationCycles(b3.getUuid(), Lists.newArrayList(sc1));
        RegistrationCoupon rc3 = new RegistrationCoupon("A3", 20, 3);
        bundleService.createRegistrationCoupon(b3, rc3);

        RegistrationCoupon rc4 = new RegistrationCoupon("A4", 20, 20);
        bundleService.createRegistrationCoupon(b3, rc4);

        // vytvorime si registraciu na bundle 1 a baliky
        BundleOrderRequest orderRequest1 = new BundleOrderRequest(b1.getUuid(), null, QUANTITY_1);
        BundleOrderRequest orderRequest1Infinite = new BundleOrderRequest(b1.getUuid(), rc1.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest2Single = new BundleOrderRequest(b2.getUuid(), rc2.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest3Three = new BundleOrderRequest(b3.getUuid(), rc3.getKey(), QUANTITY_1);
        BundleOrderRequest orderRequest4 = new BundleOrderRequest(b3.getUuid(), rc4.getKey(), QUANTITY_1);

        RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
        registrationService.createUserRegistration("01", false, orderRequest1, null, "1", otherInfo);
        registrationService.createUserRegistration("02", false, orderRequest1, null, "1", otherInfo);
        registrationService.createUserRegistration("1", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "1", otherInfo);
        registrationService.createUserRegistration("2", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "2", otherInfo);
        registrationService.createUserRegistration("3", false, orderRequest1Infinite, Lists.newArrayList(orderRequest3Three), "3", otherInfo);
        registrationService.createUserRegistration("4", false, orderRequest1, Lists.newArrayList(orderRequest4), "3", otherInfo);
        registrationService.createUserRegistration("5", false, orderRequest2Single, Lists.newArrayList(orderRequest4), "3", otherInfo);

        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(10);
        Constraints c = new Constraints();
        c.setFilterTag(RegistrationCouponList.Filter_ALL);

        List<RegistrationCouponListItem> items = couponListService.listItems(c, p);
        assertEquals(items.size(), 4);
        assertEquals(couponListService.listAllCount(c), 4);

        for (RegistrationCouponListItem item : items) {
            if (item.getCouponUuid().equals(rc1.getUuid())) {
                assertEquals(item.getUsedCount(), 3);
                assertEquals(item.getUsers().size(), 3);
            } else if (item.getCouponUuid().equals(rc3.getUuid())) {
                assertEquals(item.getUsedCount(), 3);
                assertEquals(item.getUsers().size(), 3);
            } else if (item.getCouponUuid().equals(rc4.getUuid())) {
                assertEquals(item.getUsedCount(), 2);
                assertEquals(item.getUsers().size(), 2);
            }
        }

        RegistrationCoupon rc5 = new RegistrationCoupon("A5", 20, 20);
        bundleService.createRegistrationCoupon(b3, rc5);

        c.setFilterTag(RegistrationCouponList.Filter_NOT_USED);
        items = couponListService.listItems(c, p);
        assertEquals(items.size(), 1);
        assertEquals(couponListService.listAllCount(c), 1);

        c.setFilterTag(RegistrationCouponList.Filter_USED);
        items = couponListService.listItems(c, p);
        assertEquals(items.size(), 4);
        assertEquals(couponListService.listAllCount(c), 4);
    }
}
