package consys.event.registration.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.UuidProvider;
import consys.event.registration.core.AbstractRegistrationTest;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.exception.NoActiveCycleForBundleException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationBundleDaoTest extends AbstractRegistrationTest {

    @Autowired(required = true)
    private RegistrationBundleDao bundleDao;
    @Autowired(required = true)
    private RegistrationCycleDao cycleDao;

    @Test(groups = {"dao"})
    public void createAndListBundles() throws NoRecordException {
        // vytvorime hlavne baliky
        RegistrationBundle bundle1 = new RegistrationBundle();
        bundle1.setCapacity(100);
        bundle1.setOrder(1);
        bundle1.setUuid(UuidProvider.getUuid());
        bundle1.setTitle("Title 1");
        bundleDao.create(bundle1);

        RegistrationBundle bundle2 = new RegistrationBundle();
        bundle2.setTitle("Title 2");
        bundle2.setOrder(2);
        bundle2.setUuid(UuidProvider.getUuid());
        bundleDao.create(bundle2);

        // vytvorime subbaliky
        RegistrationBundle subbundle2 = new RegistrationBundle();
        subbundle2.setTitle("Subbundle 2");
        subbundle2.setOrder(2);
        subbundle2.setSubBundle(true);
        subbundle2.setUuid(UuidProvider.getUuid());
        bundleDao.create(subbundle2);


        RegistrationBundle subbundle1 = new RegistrationBundle();
        subbundle1.setTitle("Subbundle 1");
        subbundle1.setOrder(1);
        subbundle1.setSubBundle(true);
        subbundle1.setUuid(UuidProvider.getUuid());
        bundleDao.create(subbundle1);

        RegistrationBundle subbundle3 = new RegistrationBundle();
        subbundle3.setTitle("Subbundle 3");
        subbundle3.setOrder(3);
        subbundle3.setSubBundle(true);
        subbundle3.setUuid(UuidProvider.getUuid());
        bundleDao.create(subbundle3);


        // nacitame hlavne baliky
        List<RegistrationBundle> bundles = bundleDao.listMainBundlesWithCycles();
        assertTrue(bundles.size() == 2, "Nerovna sa nacitany pocet bundlu");
        assertEquals(bundles.get(0).getTitle(), "Title 1");
        assertEquals(bundles.get(1).getTitle(), "Title 2");

        bundles = bundleDao.listMainBundles();
        assertTrue(bundles.size() == 2, "Nerovna sa nacitany pocet bundlu");
        assertEquals(bundles.get(0).getTitle(), "Title 1");
        assertEquals(bundles.get(1).getTitle(), "Title 2");

        // nacitame hlavne subbaliky
        List<RegistrationBundle> subbundles = bundleDao.listSubBundlesWithCycles();
        assertTrue(subbundles.size() == 3, "Nerovna sa nacitany pocet bundlu");
        assertEquals(subbundles.get(0).getTitle(), "Subbundle 1");
        assertEquals(subbundles.get(1).getTitle(), "Subbundle 2");
        assertEquals(subbundles.get(2).getTitle(), "Subbundle 3");

        subbundles = bundleDao.listSubBundles();
        assertTrue(subbundles.size() == 3, "Nerovna sa nacitany pocet bundlu");
        assertEquals(subbundles.get(0).getTitle(), "Subbundle 1");
        assertEquals(subbundles.get(1).getTitle(), "Subbundle 2");
        assertEquals(subbundles.get(2).getTitle(), "Subbundle 3");
    }

    @Test(groups = {"dao"})
    public void loadBundle() throws NoRecordException {
        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundle.setUuid(UuidProvider.getUuid());
        bundleDao.create(bundle);

        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        c1.setBundle(bundle);
        c1.setUuid(UuidProvider.getUuid());
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());
        cycleDao.create(c1);

        // Test ma load
        RegistrationBundle bl = bundleDao.loadBundleByUuid(bundle.getUuid());
        assertEquals(bl, bundle);

        bl = bundleDao.loadBundleWithCyclesByUuid(bundle.getUuid());
        assertEquals(bl, bundle);

        bl = bundleDao.loadBundleWithCyclesByCycleUuid(c1.getUuid());
        assertEquals(bl, bundle);

        RegistrationCycle c1l = cycleDao.loadRegistrationCycleByUuid(c1.getUuid());
        assertEquals(c1, c1l);
    }

    @Test(groups = {"dao"})
    public void loadActiveCycle() throws NoRecordException, NoActiveCycleForBundleException {
        Date nullMinDate = cycleDao.loadFirstRegistration();
        assertNull(nullMinDate, "minimalni datum ma byt null");
        Date nullMaxDate = cycleDao.loadLastRegistration();
        assertNull(nullMaxDate, "maximalni datum ma byt null");

        // Vytvorime si registrany bundle a cyklus
        RegistrationBundle bundle = new RegistrationBundle();
        bundle.setTitle("Title 1");
        bundle.setUuid(UuidProvider.getUuid());


        RegistrationCycle c1 = new RegistrationCycle();
        c1.setPrice(BigDecimal.valueOf(500.99));
        c1.setBundle(bundle);
        c1.setUuid(UuidProvider.getUuid());
        Calendar c1s = GregorianCalendar.getInstance();
        c1s.set(2000, 10, 3);
        c1.setFrom(c1s.getTime());

        Calendar c1e = GregorianCalendar.getInstance();
        c1e.set(2000, 10, 13);
        c1.setTo(c1e.getTime());


        RegistrationCycle c2 = new RegistrationCycle();
        c2.setPrice(BigDecimal.valueOf(500.99));
        c2.setBundle(bundle);
        c2.setUuid(UuidProvider.getUuid());
        Calendar c2s = GregorianCalendar.getInstance();
        c2s.add(Calendar.DAY_OF_MONTH, -5);
        c2.setFrom(c2s.getTime());

        Calendar c2e = GregorianCalendar.getInstance();
        c2e.add(Calendar.DAY_OF_MONTH, 5);
        c2.setTo(c2e.getTime());

        bundle.getCycles().add(c1);
        bundle.getCycles().add(c2);

        bundleDao.create(bundle);

        RegistrationCycle activeCycle = cycleDao.loadActiveCycleForBundle(bundle.getUuid());
        assertEquals(activeCycle, c2);


        Date resultMinDate = cycleDao.loadFirstRegistration();
        Date resultMaxDate = cycleDao.loadLastRegistration();
        
        assertEquals(resultMinDate, c1s.getTime(), "Nesedi nejdrivejsi datum registrace");
        assertEquals(resultMaxDate, c2e.getTime(), "Nesedi nejpozdejsi datum registrace");
    }
}
