-- Pridame dva stplce do registracie a nastavime checked na false
alter table registration.user_registration add column checked boolean;
alter table registration.user_registration add column checkin_date timestamp without time zone;

update registration.user_registration set checked=false;