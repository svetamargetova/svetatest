--
-- Migracny skript rozsirujuci registracny modul o subbalicky a prisposobeny na
-- pracu s nimi.
--

--
-- Table: REGISTRATION_BUNDLE
--
-- Odstranenie columnu
alter table registration.REGISTRATION_BUNDLE drop column code_title;
ALTER TABLE registration.registration_bundle ALTER description TYPE text;
ALTER TABLE registration.registration_bundle ALTER title TYPE character varying(255);


-- Pridanie noveho stlpca a nastavenie hodnot
update registration.REGISTRATION_BUNDLE set sub_bundle = false;
ALTER TABLE registration.REGISTRATION_BUNDLE ALTER COLUMN sub_bundle SET NOT NULL;


-- Odstranenie starej funkcie a triggeru
DROP TRIGGER registration_capacity_function ON registration.registration_user;
DROP FUNCTION registration.registration_capacity_function();

CREATE OR REPLACE FUNCTION registration.migrate_2_6_3_to_2_7_0() RETURNS integer AS $$


	DECLARE
        _id_new_registration BIGINT;
        _id_new_order BIGINT;
        _id_bundle BIGINT;
        -- stara registracia
	_old_registration RECORD;


        BEGIN
		-- nacitame si vsetky user registrations a postupne iterujeme cez ne
		FOR _old_registration IN SELECT * FROM registration.registration_user LOOP

			-- vytvorime novu registraciu a nastavime jej vlastnosti
			SELECT nextval('registration.REGISTRATION_SEQ') into _id_new_registration;
			INSERT INTO registration.user_registration(id, note, reg_number, state, id_user_event, uuid)
				VALUES (_id_new_registration, _old_registration.note, _old_registration.uuid, _old_registration.state, _old_registration.id_user_event, _old_registration.uuid);
			-- vytvorime objednavku

			SELECT nextval('registration.REG_ORDER_SEQ') into _id_new_order;
			INSERT INTO registration.registration_order(id, uuid, order_date, paid_date, cancel_date, state, overall_price, id_user_registration, idx_user_registration)
				VALUES (_id_new_order, _old_registration.uuid, _old_registration.registred_date, _old_registration.validate_date, _old_registration.cancel_date, _old_registration.state, _old_registration.paid,  _id_new_registration, 0);

			-- pre vsetky vlozenia sa zavola trigger ktory pricita +1 bundlu registracie, preto automaticky odcitame jedni
			select bundle.id into _id_bundle from registration.registration_bundle as bundle left join registration.registration_cycle as c on c.id_bundle=bundle.id where c.id=_old_registration.id_reg_cycle;
			update registration.registration_bundle set registred = registred-1 where id=_id_bundle;

			-- ak je naozaj registracia aktivna tak pripocitame 1
			IF(_old_registration.state=0 or  _old_registration.state=1) THEN
				update registration.registration_bundle set registred = registred+1 where id=_id_bundle;
			END IF;
			-- vytvorime zaznam na objednavke
			INSERT INTO registration.user_bundle_registration(id, uuid, id_reg_cycle, id_coupon, bundlefinalprice, id_registration_order, idx_registration_order)
				VALUES ((SELECT nextval('registration.BUNDLE_REGISTRATION_SEQ')), _old_registration.uuid, _old_registration.id_reg_cycle, _old_registration.id_coupon, _old_registration.paid, _id_new_order, 0);

		END LOOP;

                RETURN 0;
        END;
$$ LANGUAGE plpgsql;

select registration.migrate_2_6_3_to_2_7_0();

