CREATE OR REPLACE FUNCTION registration.new_order_capacity_function() RETURNS trigger AS $new_order_capacity_function$

        DECLARE
        _id_bundle BIGINT;
	-- bundle
	_bundle RECORD;

	BEGIN
	-- zistime bundle id cyklu
	select bundle.id into _id_bundle from registration.registration_bundle as bundle left join registration.registration_cycle as c on c.id_bundle=bundle.id where c.id=NEW.id_reg_cycle;
	-- Selectneme si bundle na update
	select * into _bundle from registration.registration_bundle bundle where _id_bundle=bundle.id FOR UPDATE;

            -- ak je pocet registrovanych vacsi ?? chyba ?? , rovnaky ako kapacita tak chyba
            IF (_bundle.capacity > 0 AND (_bundle.registred >= _bundle.capacity OR (NEW.quantity > _bundle.capacity - _bundle.registred))) THEN
                RAISE EXCEPTION 'Capacity full % = % required(%)',_bundle.capacity,_bundle.registred, NEW.quantity;
            END IF;
            -- update poctu registrovanych
            update registration.registration_bundle set registred = _bundle.registred+NEW.quantity where id=_bundle.id;
	RETURN NEW;
END;
$new_order_capacity_function$ LANGUAGE 'plpgsql';