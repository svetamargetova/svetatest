CREATE OR REPLACE FUNCTION registration.append_B2B_right_to_all_active(_id_bundle bigint, _add boolean) RETURNS integer AS $append_B2B_right_to_all_active$


	DECLARE
        _id_right BIGINT;                       
	_order RECORD;
	_user_rights_group RECORD;
	_actualized INTEGER;

        BEGIN

		-- nacitame pravo b2b agenta
               SELECT id INTO _id_right FROM public.module_right where right_name='RIGHT_B2B_AGENT';
               _actualized = 0;
		-- nacitame si vsetky objednavky
		FOR _order IN SELECT o.* FROM registration.registration_order o left join registration.user_bundle_registration ubr on ubr.id_registration_order=o.id left join registration.registration_cycle c on c.id=ubr.id_reg_cycle where (o.state=0 or o.state=1) and c.id_bundle=_id_bundle LOOP
			-- pre kazdu aktivnu objednavku si nacitame uzivatelovu skupinu prav
			SELECT g.* INTO _user_rights_group FROM registration.user_registration ur left join public.user_event ue on ue.id=ur.id_user_event left join public.group g on g.id=ue.id_group where ur.id=_order.id_user_registration;
			-- na zaklade priznaku _add bud pridavame pravo alebo odstranujeme
			IF (_add and (select case when count(id_group) = 0 then true else false end from public.group_right where id_group=_user_rights_group.id and id_module_right=_id_right)) THEN 
				-- pridavame nove pravo
				insert into public.group_right(id_group,id_module_right) values (_user_rights_group.id,_id_right);
				_actualized = _actualized + 1;
			ELSIF (not _add and (select case when count(id_group) > 0 then true else false end from public.group_right where id_group=_user_rights_group.id and id_module_right=_id_right)) THEN 
				-- odstranujeme
				delete from public.group_right where id_group=_user_rights_group.id and id_module_right=_id_right;
				_actualized = _actualized + 1;
			END IF;		
		END LOOP;
               RETURN _actualized;
        END;
$append_B2B_right_to_all_active$
LANGUAGE 'plpgsql';
