DROP TRIGGER IF EXISTS new_order_capacity_function on registration.USER_BUNDLE_REGISTRATION;
CREATE TRIGGER new_order_capacity_function BEFORE INSERT ON registration.USER_BUNDLE_REGISTRATION FOR EACH ROW EXECUTE PROCEDURE registration.new_order_capacity_function();
