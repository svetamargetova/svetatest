package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani slevoveho kuponu
 * @author pepa
 */
public class DeleteDiscountCouponAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -1922557872977843569L;
    // data
    private String uuid;

    public DeleteDiscountCouponAction() {
    }

    public DeleteDiscountCouponAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
