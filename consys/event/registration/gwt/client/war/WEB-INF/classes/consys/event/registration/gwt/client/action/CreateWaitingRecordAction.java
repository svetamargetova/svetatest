package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;

/**
 *
 * @author pepa
 */
public class CreateWaitingRecordAction extends EventAction<ClientRegistration> {

    private static final long serialVersionUID = 8452905539932562666L;
    // data
    private String productUuid;
    private int quantity;
    private String managedUserUuid;

    public CreateWaitingRecordAction() {
    }

    public CreateWaitingRecordAction(String productUuid, int quantity, String managedUserUuid) {
        this.productUuid = productUuid;
        this.quantity = quantity;
        this.managedUserUuid = managedUserUuid;
    }

    public String getProductUuid() {
        return productUuid;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getManagedUserUuid() {
        return managedUserUuid;
    }
}
