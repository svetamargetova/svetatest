package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class InternalRegistrationOption extends ConsysFlowPanel {

    // komponenty
    private RadioButton radioButton;
    private FlowPanel contentWrapper;
    private FlowPanel titlePanel;
    private FlowPanel detailPanel;
    private QuantityPanel quantityPanel;
    private Label priceLabel;
    // data
    private ClientRegistrationPack pack;
    private List<ClientRegistrationPack> subbundles;
    private String currency;
    private InternalRegistrationDialog parent;

    public InternalRegistrationOption(ClientRegistrationPack pack, String currency, InternalRegistrationDialog parent) {
        this.pack = pack;
        this.currency = currency;
        this.parent = parent;
    }

    @Override
    protected void onLoad() {
        clear();
        add(Separator.separator());
        add(StyleUtils.getPadder());
        generate();
        add(StyleUtils.getPadder());
    }

    @Override
    protected void onUnload() {
    }

    private void generate() {
        setStyleName(ERResourceUtils.bundle().css().registrationFormItem());

        detailPanel = new FlowPanel();
        quantityPanel = new QuantityPanel(pack.getUuidBundle(), pack.getMaxQuantity(), false, pack.getPrice());

        contentWrapper = new FlowPanel();
        contentWrapper.setStyleName(ERResourceUtils.bundle().css().registrationItemContent());
        contentWrapper.add(getTitlePanel());
        contentWrapper.add(detailPanel);
        add(contentWrapper);
    }

    private FlowPanel getTitlePanel() {
        radioButton = new RadioButton(InternalRegistrationDialog.RADIO_INTERNAL_REGISTRATION);
        radioButton.addStyleName(ResourceUtils.common().css().action());
        radioButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    selected();
                } else {
                    unselected();
                }
            }
        });

        SimplePanel titleActionPanel = new SimplePanel(radioButton);
        titleActionPanel.setStyleName(ERResourceUtils.bundle().css().registrationItemTitleAction());

        Label titleLabel = StyleUtils.getStyledLabel(pack.getBundleTitle(), ERResourceUtils.bundle().css().registrationItemTitleLabel());
        titleLabel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (!radioButton.getValue()) {
                    selected();
                }
            }
        });

        priceLabel = StyleUtils.getStyledLabel("", ERResourceUtils.bundle().css().registrationItemTitlePrice());

        titlePanel = new FlowPanel();
        titlePanel.add(titleActionPanel);
        titlePanel.add(titleLabel);
        titlePanel.add(StyleUtils.clearDiv());

        FlowPanel panel = RegistrationFormPanelItem.getBasePanel(titlePanel, priceLabel);
        panel.addStyleName(ERResourceUtils.bundle().css().registrationItemTitle());
        if (RegistrationFormPanelItem.isActiveBundle(pack)) {
            priceLabel.setText(UIMessageUtils.assignCurrency(currency, pack.getPrice()));
        } else {
            panel.addStyleName(ResourceUtils.common().css().notActive());
            priceLabel.setText(FormUtils.NDASH);
        }
        return panel;
    }

    /** oznaci polozku za vybranou */
    public void selected() {
        radioButton.setValue(true, false);
        addStyleName(ResourceUtils.common().css().active());
        parent.selectOption(this);
        loadSubbundlesAndShowDetails();
    }

    /** oznaci polozku za nevybranou */
    public void unselected() {
        removeStyleName(ResourceUtils.common().css().active());
        hideDetails();
    }

    /** zobrazi detail balicku */
    private void showDetails() {
        detailPanel.add(quantityPanel);
        for (ClientRegistrationPack p : subbundles) {
            detailPanel.add(new InternalRegistrationOptionSubItem(p, currency));
        }
    }

    private void hideDetails() {
        detailPanel.clear();
    }

    public List<ProductThumb> getOrderedItems() {
        List<ProductThumb> orderedItems = new ArrayList<ProductThumb>();
        orderedItems.add(getProductThumb());
        for (int i = 0; i < detailPanel.getWidgetCount(); i++) {
            Widget w = detailPanel.getWidget(i);
            if (w instanceof InternalRegistrationOptionSubItem) {
                InternalRegistrationOptionSubItem item = (InternalRegistrationOptionSubItem) w;
                if (item.isSelected()) {
                    orderedItems.add(item.getProductThumb());
                }
            }
        }
        return orderedItems;
    }

    private ProductThumb getProductThumb() {
        int quantity = quantityPanel != null ? quantityPanel.getQuantity() : 1;
        return new ProductThumb(pack.getUuidBundle(), null, quantity);
    }

    private void loadSubbundlesAndShowDetails() {
        if (subbundles == null) {
            EventBus.fire(new EventDispatchEvent(
                    new LoadUserRegistrationDetailsAction(pack.getUuidBundle()),
                    new AsyncCallback<ArrayListResult<ClientRegistrationPack>>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            subbundles = new ArrayList<ClientRegistrationPack>();
                        }

                        @Override
                        public void onSuccess(ArrayListResult<ClientRegistrationPack> result) {
                            subbundles = result.getArrayListResult();
                            showDetails();
                        }
                    }, parent));
        } else {
            showDetails();
        }
    }
}
