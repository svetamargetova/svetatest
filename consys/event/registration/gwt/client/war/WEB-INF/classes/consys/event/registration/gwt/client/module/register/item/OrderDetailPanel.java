package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 * Zobrazuje detaily objednavky
 * @author pepa
 */
public class OrderDetailPanel extends FlowPanel {

    // data
    private boolean mainTicket;
    private ClientRegistrationOrder order;

    public OrderDetailPanel(ClientRegistrationOrder order) {
        this(order, false);
    }

    public OrderDetailPanel(ClientRegistrationOrder order, boolean mainTicket) {
        this.order = order;
        this.mainTicket = mainTicket;

        setStyleName(ERResourceUtils.css().orderDetailPanel());
    }

    @Override
    protected void onLoad() {
        clear();
        generate();
    }

    private void generate() {
        String orderDate = DateTimeUtils.getDate(order.getOrderDate());
        Label registrationDateLabel = StyleUtils.getStyledLabel(ERMessageUtils.m.registrationDate(orderDate),
                StyleUtils.MARGIN_BOT_10, StyleUtils.FONT_BOLD);

        addTitleIfMain();
        addDetailIfAny();
        add(StyleUtils.clearDiv());
        add(registrationDateLabel);
        addOrderTotalPrice();

        final boolean paid = order.getState().equals(ClientRegistration.State.CONFIRMED);

        addIfMainAndPaid(paid);
        addIfNotPaid(paid);
    }

    private void addTitleIfMain() {
        if (mainTicket) {
            add(StyleUtils.getStyledLabel(order.getOrderItems().get(0).getTitle(), ERResourceUtils.css().title()));
        }
    }

    private void addDetailIfAny() {
        String descriptionText = order.getOrderItems().get(0).getDescription();
        Label mainDescription = new Label(descriptionText);

        FlowPanel description = new FlowPanel();
        description.setStyleName(StyleUtils.MARGIN_BOT_10);
        description.setVisible(false);
        if (mainTicket) {
            description.add(mainDescription);
        }

        for (int i = (mainTicket ? 1 : 0); i < order.getOrderItems().size(); i++) {
            ClientRegistrationOrderItem croi = order.getOrderItems().get(i);
            description.add(bulletedItem(croi));
        }

        ActionLabel bundleDetails = new ActionLabel(ERMessageUtils.c.registrationParticipantInfoPanel_action_details(),
                ERResourceUtils.css().detils());
        bundleDetails.setClickHandler(bundleDetailClickHandler(bundleDetails, description));

        if (StringUtils.isNotBlank(descriptionText) || order.getOrderItems().size() > (mainTicket ? 1 : 0)) {
            add(bundleDetails);
            add(StyleUtils.clearDiv());
            add(description);
        }
    }

    /** vygeneruje zaznam s odrazkou, pripadne se slevovym kodem */
    private FlowPanel bulletedItem(ClientRegistrationOrderItem croi) {
        SimplePanel bullet = new SimplePanel();
        bullet.setStyleName(ERResourceUtils.css().bulletItem());
        bullet.setWidget(new Image(ResourceUtils.system().textBullet()));

        Label label = StyleUtils.getStyledLabel(croi.getTitle(), StyleUtils.FONT_BOLD);

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ERResourceUtils.css().bulletItemPanel());
        panel.add(bullet);
        panel.add(label);
        panel.add(StyleUtils.clearDiv());

        if (StringUtils.isNotBlank(croi.getDiscountCode())) {
            panel.add(discountCodePanel(croi.getDiscountCode()));
        }

        return panel;
    }

    private FlowPanel discountCodePanel(String code) {
        HTML discHTML = new HTML(ERMessageUtils.m.registrationOrderPanel_text_discountCode(code));
        discHTML.setStyleName(ERResourceUtils.css().discount());
        FlowPanel panel = new FlowPanel();
        panel.add(discHTML);
        return panel;
    }

    private void addOrderTotalPrice() {
        String price = UIMessageUtils.assignCurrency(ECoMessageUtils.getCurrency(), order.getTotalPrice());
        String total = ERMessageUtils.c.registrationOrderPanel_text_total() + ": ";

        Label priceLabel = StyleUtils.getStyledLabel(price, StyleUtils.FLOAT_RIGHT);
        Label totalLabel = StyleUtils.getStyledLabel(total, StyleUtils.FLOAT_LEFT);

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ERResourceUtils.css().totalPrice());
        panel.add(priceLabel);
        panel.add(totalLabel);
        panel.add(StyleUtils.clearDiv());
        add(panel);
    }

    private void addIfMainAndPaid(boolean paid) {
        if (mainTicket && paid) {
            ConsysMessage message = ConsysMessage.getSuccess();
            message.setText(ERMessageUtils.c.registrationFormRegistered_text_registrationPaid());
            messageShowFactory(message);
            add(message);
        }
    }

    private void addIfNotPaid(boolean paid) {
        if (!paid) {
            ConsysMessage message = ConsysMessage.getInfo();
            message.setText(ERMessageUtils.m.registrationFormRegistered_text_successfulyRegisteredInfo(StyleUtils.TEXT_RED
                    + " " + StyleUtils.FONT_BOLD, DateTimeUtils.getDate(order.getDueDate())), true);
            messageShowFactory(message);
            add(message);
        }
    }

    private ClickHandler bundleDetailClickHandler(final ActionLabel action, final FlowPanel panel) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                action.setVisible(false);
                panel.setVisible(true);
            }
        };
    }

    public static void messageShowFactory(ConsysMessage message) {
        message.setWidth(270);
        message.hideClose(true);
        message.addStyleName(StyleUtils.MARGIN_BOT_10);
    }
}
