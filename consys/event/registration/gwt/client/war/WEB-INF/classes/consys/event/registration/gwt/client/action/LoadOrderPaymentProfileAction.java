package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;

/**
 * Akce pro nacteni platebniho profilu objednavky
 * @author pepa
 */
public class LoadOrderPaymentProfileAction extends EventAction<ClientUserPaymentProfile> {

    private static final long serialVersionUID = -5126663936624296534L;
    // data
    private String orderUuid;

    public LoadOrderPaymentProfileAction() {
    }

    public LoadOrderPaymentProfileAction(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public String getOrderUuid() {
        return orderUuid;
    }
}
