package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.OldTextBox;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 * Presenter pre jednu registracnu moznost.
 *
 * @author palo
 */
public class RegistrationFormPanelItem extends FlowPanel {

    private static long SECONDS_PER_DAY = 86400l;
    private static long SECONDS_PER_HOUR = 3600l;
    private static long SECONDS_PER_MINUTE = 60l;
    // komponenty
    private Label priceLabel;
    private FlowPanel contentWrapper;
    private FlowPanel titlePanel;
    private SimplePanel titleActionPanel;
    private FlowPanel descriptionPanel;
    private FlowPanel alertPanel;
    private FlowPanel detailPanel;
    private DiscountCodePanel discountCodePanel;
    private QuantityPanel quantityPanel;
    // data
    private ClientRegistrationPack pack;
    private ClickHandler titleClickHandler;
    private ClickHandler detailsClickHandler;
    private boolean detailed;
    private String currency;
    private boolean newComponents;
    // limita kteru sme schopny akceptovat aby sme zobrazili description thumb podla potrieb.
    private int descriptionThreshold = 150;

    public RegistrationFormPanelItem(ClientRegistrationPack data, String currency, boolean newComponents) {
        this.pack = data;
        this.currency = currency;
        this.newComponents = newComponents;
    }

    public ClientRegistrationPack getRegistrationPack() {
        return pack;
    }

    public void setDescriptionThreshold(int descriptionThreshold) {
        this.descriptionThreshold = descriptionThreshold;
    }

    public int getDescriptionThreshold() {
        return descriptionThreshold;
    }

    /**
     * Click handler musi byt nastaveny este pred zavedenim komponenty do DOMu.
     *
     * @param handler
     */
    public void setTitleClickHandle(ClickHandler handler) {
        this.titleClickHandler = handler;
    }

    /**
     * Prenastavy defaultny click handler ktory zobrazuje reaguje na rozkliknutie
     * detailov ak su prilis dlhe.
     * Click handler musi byt nastaveny este pred zavedenim komponenty do DOMu.
     *
     * @param detailsClickHandler
     */
    public void setDetailsClickHandler(ClickHandler detailsClickHandler) {
        this.detailsClickHandler = detailsClickHandler;
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        setStyleName(ERResourceUtils.bundle().css().registrationFormItem());

        contentWrapper = new FlowPanel();
        contentWrapper.setStyleName(ERResourceUtils.bundle().css().registrationItemContent());

        contentWrapper.add(getTitlePanel());
        contentWrapper.add(getAlertsPanel());
        contentWrapper.add(getDescriptionPanel());
        contentWrapper.add(getDetailPanel());
        contentWrapper.add(StyleUtils.clearDiv());
        add(contentWrapper);

        showDetailed(isDetailed());
    }

    protected Label getPriceLabel() {
        return priceLabel;
    }

    private FlowPanel getTitlePanel() {
        Label titleLabel = StyleUtils.getStyledLabel(pack.getBundleTitle(), ERResourceUtils.bundle().css().registrationItemTitleLabel());
        if (titleClickHandler != null) {
            titleLabel.addClickHandler(titleClickHandler);
        }

        priceLabel = StyleUtils.getStyledLabel("", ERResourceUtils.bundle().css().registrationItemTitlePrice());

        titlePanel = new FlowPanel();
        if (titleActionPanel != null) {
            titlePanel.add(titleActionPanel);
        }
        titlePanel.add(titleLabel);
        FlowPanel panel = getBasePanel(titlePanel, priceLabel);
        panel.addStyleName(ERResourceUtils.bundle().css().registrationItemTitle());

        if (isActiveBundle(pack)) {
            priceLabel.setText(UIMessageUtils.assignCurrency(currency, pack.getPrice()));
        } else {
            panel.addStyleName(ResourceUtils.common().css().notActive());
            priceLabel.setText(FormUtils.NDASH);
        }
        return panel;
    }

    private FlowPanel getAlertsPanel() {
        alertPanel = new FlowPanel();
        alertPanel.setStyleName(ERResourceUtils.bundle().css().registrationItemAlerts());
        FlowPanel warnings = warnings(pack);
        if (warnings.getWidgetCount() > 0) {
            alertPanel.add(warnings);
        } else {
            alertPanel.setVisible(false);
        }
        return getBasePanel(alertPanel, null);
    }

    protected DiscountCodePanel getDiscountCodePanel() {
        if (discountCodePanel == null) {
            if (newComponents) {
                discountCodePanel = new DiscountCodePanel<ConsysTextBox>(pack.getUuidBundle(), currency) {
                    @Override
                    protected ConsysTextBox getTextBox() {
                        return new ConsysTextBox(0, 0);
                    }
                };
            } else {
                discountCodePanel = new DiscountCodePanel<OldTextBox>(pack.getUuidBundle(), currency) {
                    @Override
                    protected OldTextBox getTextBox() {
                        return new OldTextBox();
                    }
                };
            }
        }
        return discountCodePanel;
    }

    protected QuantityPanel getQuantityPanel() {
        if (quantityPanel == null) {
            quantityPanel = new QuantityPanel(pack.getUuidBundle(), pack.getMaxQuantity(), newComponents, pack.getPrice());
        }
        return quantityPanel;
    }

    private FlowPanel getDescriptionPanel() {
        descriptionPanel = new FlowPanel();
        descriptionPanel.setStyleName(ERResourceUtils.bundle().css().registrationItemDescription());
        return getBasePanel(descriptionPanel, null);
    }

    private FlowPanel getDetailPanel() {
        detailPanel = new FlowPanel();
        return detailPanel;
    }

    private void showDescription(boolean show) {
        if (show) {
            showDescriptionDetailed();
        } else {
            showDescriptionThumb();
        }
    }

    private void showDescriptionDetailed() {
        descriptionPanel.clear();
        descriptionPanel.add(new Label(pack.getBundleDescription()));
    }

    private void showDescriptionThumb() {
        descriptionPanel.clear();
        if (pack.getBundleDescription() != null && pack.getBundleDescription().length() > getDescriptionThreshold()) {
            ActionLabel showDetails = new ActionLabel(ERMessageUtils.c.registrationFormItem_action_showDetails());
            if (detailsClickHandler == null) {
                detailsClickHandler = new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        showDetailed(true);
                    }
                };
            }
            showDetails.setClickHandler(detailsClickHandler);
            showDetails.removeStyleName(StyleUtils.FONT_BOLD);
            descriptionPanel.add(new InlineLabel(FormUtils.shortTextOnWord(getDescriptionThreshold(), pack.getBundleDescription())));
            descriptionPanel.add(showDetails);
        } else {
            // ak sa to zmesti podla kapacity tak ukazeme
            showDescriptionDetailed();
        }
    }

    /**
     * Vytvori panel ktory je rozdeleny do dvoch casti kde prava cast obsahuje
     * cenu, a lava cast obsahuje nejaky obsah.
     *
     * @param left
     * @param right
     * @return
     */
    public static FlowPanel getBasePanel(Widget left, Widget right) {
        SimplePanel leftPanel = new SimplePanel();
        if (left != null) {
            leftPanel.setWidget(left);
        }
        leftPanel.setStyleName(ResourceUtils.common().css().leftColumn());
        SimplePanel rightPanel = new SimplePanel(right);
        if (right != null) {
            rightPanel.setWidget(right);
        }
        rightPanel.setStyleName(ResourceUtils.common().css().rightColumn());

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(ERResourceUtils.bundle().css().registrationItemPricePanel());
        panel.add(leftPanel);
        panel.add(rightPanel);
        panel.add(StyleUtils.clearDiv());
        return panel;
    }

    public void setDetailed(boolean detailed) {
        this.detailed = detailed;
        if (isAttached()) {
            showDetailed(detailed);
        }
    }

    public ProductThumb getProductThumb() {
        return new ProductThumb(pack.getUuidBundle(),
                discountCodePanel != null ? discountCodePanel.getDiscountCode() : null,
                quantityPanel != null ? quantityPanel.getQuantity() : 1);
    }

    private void showDetailed(boolean detail) {
        this.detailed = detail;
        // nastavime plnu viditelnost popisku
        showDescription(detailed);
        // vlozime vyber kvantity
        showQuantityPanel(detail);
        // vlozime nacitanie kuponu
        showDiscountPanel(detailed);
    }

    protected void showDiscountPanel(boolean show) {
        if (isActiveBundle(pack)) {
            if (show && pack.isWithCode()) {
                // vlozime discount code panel pred vsetky alerty
                if (getDiscountCodePanel().isAttached()) {
                    getDiscountCodePanel().setVisible(true);
                } else {
                    detailPanel.add(getDiscountCodePanel());
                }
            } else {
                if (discountCodePanel != null) {
                    // ak ma nastavenu zlavu tak ju odcitame
                    getDiscountCodePanel().removeDiscount();
                    // odstranime panel
                    getDiscountCodePanel().setVisible(false);
                }
            }
        }
    }

    protected void showQuantityPanel(boolean show) {
        if (show && pack.isQuantity()) {
            if (getQuantityPanel().isAttached()) {
                getQuantityPanel().setVisible(true);
            } else {
                detailPanel.add(getQuantityPanel());
            }
        } else {
            if (quantityPanel != null) {
                getQuantityPanel().setQuantity(1);
                getQuantityPanel().setVisible(false);
            }
        }
    }

    public void setActionWidget(Widget widget) {
        titleActionPanel = new SimplePanel(widget);
        titleActionPanel.setStyleName(ERResourceUtils.bundle().css().registrationItemTitleAction());
        if (titlePanel != null) {
            titlePanel.insert(titleActionPanel, 0);
        }
    }

    public void insertIntoContent(Widget w, int beforeIndex) {
        contentWrapper.insert(w, beforeIndex);
    }

    public boolean isDetailed() {
        return detailed;
    }

    public String getBundleUuid() {
        return pack.getUuidBundle();
    }

    /** vraci true pokud se jedna o aktivni balicek */
    public static boolean isActiveBundle(ClientRegistrationPack pack) {
        return pack.getTo() != null && pack.getPrice() != null;
    }

    /** vytvori FlowPanel s vypisem chyb, pokud zadne nejsou vraci prazdny panel */
    public static FlowPanel warnings(ClientRegistrationPack data) {
        FlowPanel warning = new FlowPanel();
        if (data.getTo() != null) {
            final long time = (data.getTo().getTime() - System.currentTimeMillis()) / 1000; // rozdil v sekundach
            final long days = time / SECONDS_PER_DAY; // pocet dni
            if (days < 7) {
                if (days == 0) {
                    final long hours = time / SECONDS_PER_HOUR; // pocet hodin
                    if (hours == 0) {
                        final long minutes = time / SECONDS_PER_MINUTE;
                        addToAttentionalPanel(warning, getAttentionExpireMinutes((int) (minutes + 1)));
                    } else {
                        addToAttentionalPanel(warning, getAttentionExpireHours((int) (hours) + 1));
                    }
                } else {
                    addToAttentionalPanel(warning, getAttentionExpireDays((int) (days + 1)));
                }
            }
        }
        if (data.getCapacity() != 0) {
            int freeCapacity = data.getCapacity() - data.getRegistred();
            if (freeCapacity > 10) {
                addToAttentionalPanel(warning, getAttentionLimitedCapacity(data.getCapacity()));
            } else if (freeCapacity <= 10 && freeCapacity > 0) {
                addToAttentionalPanel(warning, getAttentionFreeCapacity(freeCapacity));
            } else if (freeCapacity <= 0) {
                addToAttentionalPanel(warning, getAttentionCapacityFilled());
            }
        }
        return warning;
    }

    /** pridata html do panelu a pokud jiz nejakou polozku obsahuje prida horni margin */
    private static void addToAttentionalPanel(FlowPanel panel, String text) {
        HTML html = new HTML(text);
        if (panel.getWidgetCount() > 0) {
            html.addStyleName(StyleUtils.MARGIN_TOP_10);
        }
        panel.add(html);
    }

    /** vytvori html string jednoho upozorneni */
    private static String attention(String text) {
        InlineLabel label = new InlineLabel(ERMessageUtils.c.registrationFormItem_text_attention() + "!");
        label.setStyleName(ERResourceUtils.bundle().css().registerAttention());
        return label.toString() + " " + text;
    }

    private static String getAttentionExpireDays(int days) {
        return attention(ERMessageUtils.m.registrationFormItem_text_registrationExpiresDays(days));
    }

    private static String getAttentionExpireHours(int hours) {
        return attention(ERMessageUtils.m.registrationFormItem_text_registrationExpiresHours(hours));
    }

    private static String getAttentionExpireMinutes(int minutes) {
        return attention(ERMessageUtils.m.registrationFormItem_text_registrationExpiresMinutes(minutes));
    }

    private static String getAttentionLimitedCapacity(int fullCapacity) {
        return attention(ERMessageUtils.m.registrationFormItem_text_limitedCapacity(fullCapacity));
    }

    /** posledni volna mista */
    private static String getAttentionFreeCapacity(int freeCapacity) {
        return attention(ERMessageUtils.m.registrationFormItem_text_lastFreeCapacity(freeCapacity));
    }

    /** kapacita obsazena */
    private static String getAttentionCapacityFilled() {
        return attention(ERMessageUtils.c.registrationFormItem_text_capacityFilled());
    }
}
