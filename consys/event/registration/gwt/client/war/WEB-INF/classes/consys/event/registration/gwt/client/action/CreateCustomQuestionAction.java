package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientCreateQuestion;
import java.util.ArrayList;

/**
 * Vytvori volitelnou otazku pro registraci
 * @author pepa
 */
public class CreateCustomQuestionAction extends EventAction<ArrayListResult<String>> {

    private static final long serialVersionUID = -7804017804392900269L;
    // data
    private ArrayList<ClientCreateQuestion> questions;

    public CreateCustomQuestionAction() {
        questions = new ArrayList<ClientCreateQuestion>();
    }

    public ArrayList<ClientCreateQuestion> getQuestions() {
        return questions;
    }
}
