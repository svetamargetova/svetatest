package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro potvrzeni objednavky, administratorem
 * @author pepa
 */
public class ConfirmOrderAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -1861922701641204541L;
    // data
    private String orderUuid;
    private String note;
    private boolean notifyUser;

    public ConfirmOrderAction() {
    }

    public ConfirmOrderAction(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public String getOrderUuid() {
        return orderUuid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isNotifyUser() {
        return notifyUser;
    }

    public void setNotifyUser(boolean notifyUser) {
        this.notifyUser = notifyUser;
    }
}
