package consys.event.registration.gwt.client.module.register;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConfirmDialog;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.registration.gwt.client.action.CancelWaitingAction;
import consys.event.registration.gwt.client.action.ListOpenMainRegistrationForWaitingAction;
import consys.event.registration.gwt.client.action.LoadWaitingRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo;
import consys.event.registration.gwt.client.module.register.item.ParticipantInfoPanel;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 *
 * @author pepa
 */
public class WaitingOverview extends RootPanel {

    // data
    private ClientRegistration registration;
    private String registrationUuid;

    public WaitingOverview(String registrationUuid) {
        super(ERMessageUtils.c.waitingOverview_title());
        this.registrationUuid = registrationUuid;
    }

    public WaitingOverview(ClientRegistration registration) {
        super(ERMessageUtils.c.waitingOverview_title());
        this.registration = registration;
    }

    @Override
    protected void onLoad() {
        if (registration == null) {
            EventBus.fire(new EventDispatchEvent(new LoadWaitingRegistrationAction(registrationUuid),
                    new AsyncCallback<ClientRegistration>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava event action executor
                        }

                        @Override
                        public void onSuccess(ClientRegistration result) {
                            WaitingOverview.this.registration = result;
                            generateContent();
                        }
                    }, this));
        } else {
            generateContent();
        }
    }

    private void generateContent() {
        clearControll();

        ActionImage cancel = ActionImage.getCrossButton(ERMessageUtils.c.waitingOverview_button_cancelWaiting(), true);
        cancel.addClickHandler(cancelClickHandler());
        addRightControll(cancel);

        clear();

        FlowPanel left = new FlowPanel();
        left.addStyleName(ResourceUtils.common().css().leftColumn());
        left.add(new ParticipantInfoPanel(registration.getEventUser()) {

            @Override
            public ConsysMessage getErrorMessage() {
                return getErrorMessage();
            }
        });

        FlowPanel right = new FlowPanel();
        right.addStyleName(ResourceUtils.common().css().rightColumn());
        bundlesInfo(right);

        FlowPanel wrapper = new FlowPanel();
        wrapper.setStyleName(ERResourceUtils.css().participantInfoPanel());
        wrapper.add(left);
        wrapper.add(right);
        wrapper.add(StyleUtils.clearDiv());

        addWidget(wrapper);
    }

    private ClickHandler cancelClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final ConfirmDialog dialog = new ConfirmDialog(ERMessageUtils.c.waitingOverview_cancel_registration_question_title());
                ConsysAction action = new ConsysAction() {

                    @Override
                    public void run() {
                        EventDispatchEvent cancelEvent = new EventDispatchEvent(
                                new CancelWaitingAction(registration.getRegistrationUuid()),
                                new AsyncCallback<VoidResult>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne vyjimky zpracovava EventActionExecuter
                                    }

                                    @Override
                                    public void onSuccess(VoidResult result) {
                                        RegistrationForm w = new RegistrationForm(null);
                                        FormUtils.fireSameBreadcrumb(ERMessageUtils.c.registrationForm_title(), w);
                                        dialog.hide();
                                    }
                                }, WaitingOverview.this);
                        EventBus.get().fireEvent(cancelEvent);
                    }
                };
                dialog.setAction(action);
                dialog.showCentered();
            }
        };
    }

    /** vykresli informace o kapacite otevrenych balicku */
    private void bundlesInfo(final FlowPanel panel) {
        EventBus.fire(new EventDispatchEvent(new ListOpenMainRegistrationForWaitingAction(),
                new AsyncCallback<ClientWaitingBundlesInfo>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientWaitingBundlesInfo result) {
                        panel.clear();
                        panel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.waitingOverview_text_capacity(), ERResourceUtils.css().waitingCapacity()));
                        for (ClientWaitingBundleInfo cbi : result.getInfos()) {
                            addInfoRow(panel, cbi);
                        }
                        if (result.getInfos().isEmpty()) {
                            panel.add(new Label(ERMessageUtils.c.waitingOverview_text_noOpenRegistration()));
                        }
                    }
                }, WaitingOverview.this));
    }

    private void addInfoRow(FlowPanel panel, ClientWaitingBundleInfo info) {
        boolean full = info.getFilledCapacity() >= info.getFullCapacity();
        String style = full ? ERResourceUtils.css().waitingCapacityFull() : ERResourceUtils.css().waitingCapacityFree();
        String capacity = info.getFullCapacity() == 0 ? UIMessageUtils.c.const_unlimited() : String.valueOf(Math.max(info.getFullCapacity() - info.getFilledCapacity(), 0));
        Label left = StyleUtils.getStyledLabel(info.getBundleName(), StyleUtils.FLOAT_LEFT, style);
        Label right = StyleUtils.getStyledLabel(capacity, StyleUtils.FLOAT_RIGHT, style);
        panel.add(right);
        panel.add(left);
        panel.add(StyleUtils.clearDiv());
    }
}
