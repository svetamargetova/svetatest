package consys.event.registration.gwt.client.module.coupon;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysIntTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.registration.gwt.client.action.CreateDiscountCouponAction;
import consys.event.registration.gwt.client.action.GenerateDiscountKeyAction;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class CreateDiscountCouponDialog extends SmartDialog {

    // komponenty
    private SelectBox<String> bundleBox;
    private ConsysStringTextBox keyBox;
    private ConsysIntTextBox discountBox;
    private ConsysStringTextBox capacityBox;
    private ConsysDateBox endDateBox;
    private Form form;
    // data
    private boolean individual;
    private ArrayList<SelectBoxItem<String>> bundleList;
    private String defaultBundle;
    private RefreshAfterCreateDiscount refreshable;

    public CreateDiscountCouponDialog(boolean individual, ArrayList<SelectBoxItem<String>> bundleList,
            String defaultBundle, RefreshAfterCreateDiscount refreshable) {
        this.individual = individual;
        this.bundleList = bundleList;
        this.defaultBundle = defaultBundle;
        this.refreshable = refreshable;
    }

    @Override
    protected void generateContent(SimpleFormPanel panel) {
        bundleBox = new SelectBox<String>();
        bundleBox.setItems(bundleList);
        bundleBox.selectItem(defaultBundle);
        bundleBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                regenerateKey(event.getValue().getItem(), null);
            }
        });

        keyBox = new ConsysStringTextBox(1, 64, ERMessageUtils.c.discountListItem_text_code());
        keyBox.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                regenerateKey(bundleBox.getSelectedItem().getItem(), keyBox.getText());
            }
        });
        if (defaultBundle != null) {
            regenerateKey(defaultBundle, null);
        }

        discountBox = new ConsysIntTextBox("33px", 1, 100, ERMessageUtils.c.discountListItem_text_discount());
        discountBox.addStyleName(StyleUtils.FLOAT_LEFT);
        discountBox.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        discountBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                int number = onlyNumber(discountBox);
                if (number > 100) {
                    discountBox.setText(discountBox.getText().substring(0, 2));
                }
            }
        });

        ConsysFlowPanel discountPanel = new ConsysFlowPanel();
        discountPanel.add(discountBox);
        discountPanel.add(StyleUtils.getStyledLabel("%", StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_TOP_3));
        discountPanel.add(StyleUtils.clearDiv());

        ActionLabel showRestriction = new ActionLabel(ERMessageUtils.c.createDiscountDialog_action_addRestriction());

        FlowPanel restrictionPanel = new FlowPanel();
        restrictionPanel.add(showRestriction);

        capacityBox = new ConsysStringTextBox(0, 20, ERMessageUtils.c.discountListItem_text_capacity());
        capacityBox.setText(individual ? "1" : ERMessageUtils.c.discountListItem_text_unlimited());
        addCapacityBoxHandlers(capacityBox);

        endDateBox = ConsysDateBox.getBox(true);
        endDateBox.setShowDateAs(ConsysDateBox.ShowDateAs.EVENT);

        ActionImage save = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        save.addClickHandler(saveClickHandler());

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(hideClickHandler());

        form = new Form();
        form.addStyleName(StyleUtils.MARGIN_TOP_10);
        form.addRequired(1, ERMessageUtils.c.discountListItem_text_ticket(), bundleBox);
        form.addRequired(2, ERMessageUtils.c.discountListItem_text_code(), keyBox);
        form.addRequired(3, ERMessageUtils.c.discountListItem_text_discount(), discountPanel);
        if (individual) {
            form.addContent(4, restrictionPanel);
            form.addOptional(5, ERMessageUtils.c.discountListItem_text_validThrough(), endDateBox);
        } else {
            form.addOptional(4, ERMessageUtils.c.discountListItem_text_capacity(), capacityBox);
            form.addContent(5, restrictionPanel);
            form.addOptional(6, ERMessageUtils.c.discountListItem_text_validThrough(), endDateBox);
        }

        form.addActionMembers(individual ? 6 : 7, save, cancel);
        form.showRow(individual ? 5 : 6, false);

        showRestriction.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                form.showRow(individual ? 5 : 6, true);
                form.removeRow(individual ? 4 : 5);
                endDateBox.setFocus(true);
            }
        });

        panel.addWidget(StyleUtils.getPadder());
        panel.addWidget(StyleUtils.getStyledLabel(ERMessageUtils.c.createDiscountDialog_title(),
                ResourceUtils.system().css().createDialogHeaderTitle()));
        panel.addWidget(StyleUtils.getPadder());
        panel.addWidget(form);
        panel.addWidget(StyleUtils.getPadder());
    }

    /**
     * pokud neobsahuje textBox cislo, smaze jeho obsah
     * @return hodnotu kladnou cisla pokud textBox obsahoval cislo, pokud ne vraci 0
     */
    private static int onlyNumber(TextBox textBox) {
        String text = textBox.getText();
        try {
            int number = Integer.parseInt(text);
            return number > 0 ? number : 0;
        } catch (NumberFormatException ex) {
            textBox.setText("");
            return 0;
        }
    }

    /** zavola na server pro vygenerovani/zkontrolovani klice */
    private void regenerateKey(final String bundleUuid, final String key) {
        EventBus.get().fireEvent(new EventDispatchEvent(new GenerateDiscountKeyAction(bundleUuid, key),
                new AsyncCallback<StringResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                        // TODO: specificke vyjimky
                    }

                    @Override
                    public void onSuccess(StringResult result) {
                        keyBox.setText(result.getStringResult());
                    }
                },
                CreateDiscountCouponDialog.this));
    }

    /** clickhandler pro ulozeni slevoveho klice */
    private ClickHandler saveClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                int capacity = 0;
                String capacityText = capacityBox.getText();
                try {
                    int number = Integer.parseInt(capacityText);
                    if (number > 0) {
                        capacity = number;
                    }
                } catch (NumberFormatException ex) {
                    // kapacita je nula
                }

                int discount = discountBox.getTextInt();

                CreateDiscountCouponAction action = new CreateDiscountCouponAction();
                action.setCapacity(capacity);
                action.setDiscount(discount);
                action.setEndDate(endDateBox.getValue());
                action.setKey(keyBox.getText());
                action.setBundleUuid(bundleBox.getSelectedItem().getItem());

                EventBus.get().fireEvent(new EventDispatchEvent(action,
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                                // TODO: specificke vyjimky
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                refreshable.refreshDiscount();
                                hide();
                            }
                        }, CreateDiscountCouponDialog.this));
            }
        };
    }

    public static void addCapacityBoxHandlers(final ConsysStringTextBox capacityBox) {
        capacityBox.addBlurHandler(new BlurHandler() {

            @Override
            public void onBlur(BlurEvent event) {
                String text = capacityBox.getText();
                try {
                    int number = Integer.parseInt(text);
                    if (number < 1) {
                        capacityBox.setText(ERMessageUtils.c.discountListItem_text_unlimited());
                    } else {
                        capacityBox.setText(String.valueOf(number));
                    }
                } catch (NumberFormatException ex) {
                    capacityBox.setText(ERMessageUtils.c.discountListItem_text_unlimited());
                }
            }
        });
        capacityBox.addFocusHandler(new FocusHandler() {

            @Override
            public void onFocus(FocusEvent event) {
                onlyNumber(capacityBox);
            }
        });
        capacityBox.addKeyUpHandler(new KeyUpHandler() {

            @Override
            public void onKeyUp(KeyUpEvent event) {
                onlyNumber(capacityBox);
            }
        });
    }

    /** interface pro komponenty predavane jako parametr do dialogu pro vytvoreni slevoveho kodu */
    public interface RefreshAfterCreateDiscount {

        public void refreshDiscount();
    }
}
