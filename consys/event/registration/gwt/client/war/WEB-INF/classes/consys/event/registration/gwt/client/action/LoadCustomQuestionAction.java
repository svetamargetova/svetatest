package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;

/**
 * Akce pro nacteni detailu volitelne otazky
 * @author pepa
 */
public class LoadCustomQuestionAction extends EventAction<ClientCustomQuestion> {

    private static final long serialVersionUID = 1017453639051914546L;
    // data
    private String questionUuid;

    public LoadCustomQuestionAction() {
    }

    public LoadCustomQuestionAction(String questionUuid) {
        this.questionUuid = questionUuid;
    }

    public String getQuestionUuid() {
        return questionUuid;
    }
}
