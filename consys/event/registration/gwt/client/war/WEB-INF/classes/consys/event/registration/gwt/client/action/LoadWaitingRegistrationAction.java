package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;

/**
 *
 * @author pepa
 */
public class LoadWaitingRegistrationAction extends EventAction<ClientRegistration> {

    private static final long serialVersionUID = 2091028104704666871L;
    // data
    private String registrationUuid;

    public LoadWaitingRegistrationAction() {
    }

    public LoadWaitingRegistrationAction(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    public String getRegistrationUuid() {
        return registrationUuid;
    }
}
