package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import java.util.Date;

/**
 * Akce pro aktualizaci dat na slevovem kuponu
 * @author pepa
 */
public class UpdateDiscountCouponAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -7242667207932462001L;
    // data
    private String uuid;
    private int capacity;
    private Date endDate;

    public UpdateDiscountCouponAction() {
    }

    public UpdateDiscountCouponAction(String uuid, int capacity, Date endDate) {
        this.uuid = uuid;
        this.capacity = capacity;
        this.endDate = endDate;
    }

    public int getCapacity() {
        return capacity;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getUuid() {
        return uuid;
    }
}
