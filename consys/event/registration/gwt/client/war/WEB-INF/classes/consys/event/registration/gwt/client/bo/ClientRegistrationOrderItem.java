package consys.event.registration.gwt.client.bo;

import java.io.Serializable;

/**
 *
 * @author pepa
 */
public class ClientRegistrationOrderItem implements Serializable {

    private static final long serialVersionUID = -8247031263967788279L;
    // data
    private String title;
    private String description;
    private String discountCode;
    private String bundleUuid;
    private int quantity;

    public ClientRegistrationOrderItem() {
    }

    public ClientRegistrationOrderItem(String title) {
        this.title = title;
    }

    public ClientRegistrationOrderItem(String title, String discountCode) {
        this.title = title;
        this.discountCode = discountCode;
    }

    public ClientRegistrationOrderItem(String title, String description, String discountCode, String uuid) {
        this.title = title;
        this.description = description;
        this.discountCode = discountCode;
        this.bundleUuid = uuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public void setBundleUuid(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }
}
