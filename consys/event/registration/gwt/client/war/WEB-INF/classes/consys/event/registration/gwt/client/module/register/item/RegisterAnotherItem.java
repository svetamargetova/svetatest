package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.user.NameWithAffiliation;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import consys.event.registration.gwt.client.module.register.UserRegistrationOverview;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Jedna polozka v seznamu spravovanych registraci
 * @author pepa
 */
public class RegisterAnotherItem extends FlowPanel {

    private EventUser eu;
    private ClientRegistration cr;

    public RegisterAnotherItem(EventUser eu, ClientRegistration cr) {
        this.eu = eu;
        this.cr = cr;
    }

    @Override
    protected void onLoad() {
        clear();
        generate();
    }

    private void generate() {
        FlowPanel textPanel = new FlowPanel();
        textPanel.setStyleName(StyleUtils.FLOAT_LEFT);
        textPanel.setWidth("320px");
        if (cr == null) {
            textPanel.add(new NameWithAffiliation(eu.getFullName(), eu.getAffiliation(UIMessageUtils.c.const_at())));
        } else {
            textPanel.add(new NameWithAffiliation(eu.getFullName(), eu.getAffiliation(UIMessageUtils.c.const_at()),
                    toRegistration(cr)));
        }

        boolean showButton = true;

        FlowPanel statePanel = new FlowPanel();
        statePanel.setStyleName(StyleUtils.FLOAT_RIGHT);
        if (cr != null && cr.getState() != null) {
            String text = "";
            String style = "";
            switch (cr.getState()) {
                case APPROVE:
                    text = ERMessageUtils.c.registrationList_text_waiting();
                    style = StyleUtils.TEXT_RED;
                    showButton = false;
                    break;
                case CANCELED:
                    text = ERMessageUtils.c.registerAnother_text_canceled();
                    style = StyleUtils.TEXT_GRAY;
                    break;
                case CONFIRMED:
                    text = ERMessageUtils.c.registrationList_text_registered();
                    style = StyleUtils.TEXT_GREEN;
                    showButton = false;
                    break;
                case ORDERED:
                    text = ERMessageUtils.c.registrationList_text_preregistered();
                    style = StyleUtils.TEXT_RED;
                    showButton = false;
                    break;
                case TIMEOUTED:
                    text = ERMessageUtils.c.registerAnother_text_canceled();
                    style = StyleUtils.TEXT_GRAY;
                    break;
                case WAITING:
                    text = ERMessageUtils.c.registrationList_text_waiting();
                    style = StyleUtils.TEXT_RED;
                    showButton = false;
                    break;
            }
            statePanel.add(StyleUtils.getStyledLabel(text, style, StyleUtils.FONT_BOLD));
        }

        FlowPanel control = new FlowPanel();
        control.setStyleName(StyleUtils.FLOAT_RIGHT);
        if (cr == null) {
            ActionImage registerButton = ActionImage.getConfirmButton(ERMessageUtils.c.registerAnother_action_registerParticipant());
            registerButton.addClickHandler(registerClickHandler(eu));
            control.add(registerButton);
        }

        setStyleName(StyleUtils.MARGIN_BOT_10);
        if (showButton) {
            add(control);
        } else {
            add(statePanel);
        }
        add(textPanel);
        add(StyleUtils.clearDiv());
    }

    private ConsysAction toRegistration(final ClientRegistration cr) {
        return new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextBreadcrumb(RegistrationForm.anotherRegistrationBreadcrumb(cr.getEventUser().getFullName()),
                        new UserRegistrationOverview(cr.getRegistrationUuid()));
            }
        };
    }

    private ClickHandler registerClickHandler(final EventUser eu) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                RegistrationForm form = new RegistrationForm(null);
                form.setManagedUser(eu);
                FormUtils.fireNextBreadcrumb(RegistrationForm.anotherRegistrationBreadcrumb(eu.getFullName()), form);
            }
        };
    }
}
