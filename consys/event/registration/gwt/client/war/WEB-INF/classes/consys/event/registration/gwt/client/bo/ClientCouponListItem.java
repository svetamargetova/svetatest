package consys.event.registration.gwt.client.bo;

import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import consys.common.gwt.shared.bo.EventUser;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class ClientCouponListItem extends DataListUuidItem {

    private static final long serialVersionUID = -4266384679718454866L;
    // data    
    private String bundleUuid;
    private String key;
    private int discount;
    private int capacity;
    private int usedCount;
    private Date endDate;
    private ArrayList<EventUser> users;

    public ClientCouponListItem() {
        users = new ArrayList<EventUser>();
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isUsed() {
        return users != null && users.size() > 0;
    }

    public ArrayList<EventUser> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<EventUser> users) {
        this.users = users;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public void setBundleUuid(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    /**
     * @return the usedCount
     */
    public int getUsedCount() {
        return usedCount;
    }

    /**
     * @param usedCount the usedCount to set
     */
    public void setUsedCount(int usedCount) {
        this.usedCount = usedCount;
    }    
}
