package consys.event.registration.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event vystreleny ked uzivatel vytvori novu objednavku
 * @author pepa
 */
public class NewOrderEvent extends GwtEvent<NewOrderEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    
    private final String token;
    
    public NewOrderEvent(String token) {        
        this.token = token;
    }

    public String getToken() {
        return token;
    }
        

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onOrderConfirmed(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onOrderConfirmed(NewOrderEvent event);
    }
}
