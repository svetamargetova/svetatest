package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;

/**
 *
 * @author pepa
 */
public class ClientCreateBundle implements Result {

    private static final long serialVersionUID = 8545248861077220151L;
    // data
    private String title;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
