package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo;

/**
 *
 * @author pepa
 */
public class ListOpenMainRegistrationForWaitingAction extends EventAction<ClientWaitingBundlesInfo> {

    private static final long serialVersionUID = -5890803707542958197L;
}
