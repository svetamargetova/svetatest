package consys.event.registration.gwt.client.module.lists;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.event.common.gwt.client.action.CurrentEventUserRightAction;
import consys.event.registration.api.right.EventRegistrationModuleRoleModel;

/**
 * 
 * @author pepa
 */
public class ParticipantList extends FlowPanel {

    public ParticipantList() {
        EventDispatchEvent loadEvent = new EventDispatchEvent(
                new CurrentEventUserRightAction(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR),
                new AsyncCallback<BooleanResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        add(result.isBool() ? new RegistrationList() : new UserParticipantList());
                    }
                }, null);
        EventBus.get().fireEvent(loadEvent);
    }
}
