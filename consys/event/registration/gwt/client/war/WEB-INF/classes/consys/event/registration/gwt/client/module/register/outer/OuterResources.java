package consys.event.registration.gwt.client.module.register.outer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.CssResource.Import;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;
import consys.common.gwt.client.ui.comp.form.FormCss;
import consys.common.gwt.client.ui.comp.input.InputCss;
import consys.common.gwt.client.ui.img.CssConstants;
import consys.common.gwt.client.ui.img.SystemCssResource;

/**
 *
 * @author pepa
 */
public interface OuterResources extends ClientBundle {

    public static final OuterResources INSTANCE = GWT.create(OuterResources.class);

    @Import(value = {SystemCssResource.class, CssConstants.class, FormCss.class, InputCss.class})
    @Source("outer.css")
    public OuterCss css();

    @Source("img/topCenterBackground.png")
    @ImageOptions(repeatStyle = RepeatStyle.Vertical)
    public ImageResource topCenterBackground();

    @Source("img/topSeparator.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource topSeparator();

    @Source("img/topTakeplace.png")
    public ImageResource topTakeplace();

    @Source("img/bottomBackground.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource bottomBackground();

    @Source("img/bottomTakeplace.png")
    public ImageResource bottomTakeplace();

    @Source("img/middlePartTop.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource middlePartTop();

    @Source("img/middlePartBottom.png")
    @ImageOptions(repeatStyle = RepeatStyle.Horizontal)
    public ImageResource middlePartBottom();

    @Source("img/middlePartSeparator.png")
    public ImageResource middlePartSeparator();

    @Source("img/bottomSeparator.png")
    public ImageResource bottomSeparator();
}
