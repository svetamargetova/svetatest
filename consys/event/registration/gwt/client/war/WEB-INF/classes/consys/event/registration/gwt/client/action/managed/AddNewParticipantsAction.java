package consys.event.registration.gwt.client.action.managed;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.managed.NewParticipant;
import java.util.ArrayList;

/**
 * Akce pro vytvoreni novych uzivatelu v akci
 * @author pepa
 */
public class AddNewParticipantsAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 375753093926321449L;
    // data
    private ArrayList<NewParticipant> participants;

    public AddNewParticipantsAction() {
        participants = new ArrayList<NewParticipant>();
    }

    public ArrayList<NewParticipant> getParticipants() {
        return participants;
    }
}
