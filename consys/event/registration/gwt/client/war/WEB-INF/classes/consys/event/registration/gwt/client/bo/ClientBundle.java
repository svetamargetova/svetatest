package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Klientsky bundle
 * @author pepa
 */
public class ClientBundle extends ClientBundleThumb implements IsSerializable, Result {

    private static final long serialVersionUID = 3082121402850349240L;
    // data
    private String description;
    private int capacity;
    private int quantity;
    private double vat;
    private boolean codeEnter;
    private ArrayList<ClientRegistrationCycle> cycles;
    private int discountCodes;
    private int discountUsed;
    private boolean b2BTicket;

    public ClientBundle() {
        cycles = new ArrayList<ClientRegistrationCycle>();
    }

    /** jedna se o vstupenku na b2B */
    public boolean isB2BTicket() {
        return b2BTicket;
    }

    /** jedna se o vstupenku na b2B */
    public void setB2BTicket(boolean b2BTicket) {
        this.b2BTicket = b2BTicket;
    }

    /** 0 = neomezene */
    public int getCapacity() {
        return capacity;
    }

    /** 0 = neomezene */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /** 1 = omezeno na jeden tiket na registraci */
    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    /** 1 = omezeno na jeden tiket na registraci */
    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }

    public boolean isCodeEnter() {
        return codeEnter;
    }

    public void setCodeEnter(boolean codeEnter) {
        this.codeEnter = codeEnter;
    }

    public ArrayList<ClientRegistrationCycle> getCycles() {
        return cycles;
    }

    public void setCycles(ArrayList<ClientRegistrationCycle> cycles) {
        this.cycles = cycles;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDiscountCodes() {
        return discountCodes;
    }

    public void setDiscountCodes(int discountCodes) {
        this.discountCodes = discountCodes;
    }

    public int getDiscountUsed() {
        return discountUsed;
    }

    public void setDiscountUsed(int discountUsed) {
        this.discountUsed = discountUsed;
    }

    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }
}
