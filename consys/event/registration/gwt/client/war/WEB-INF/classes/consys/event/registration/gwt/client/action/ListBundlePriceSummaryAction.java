package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientBundlePriceSummary;

/**
 * Akce pro nacteni prehledu cen bundlu
 * @author pepa
 */
public class ListBundlePriceSummaryAction extends EventAction<ArrayListResult<ClientBundlePriceSummary>> {

    private static final long serialVersionUID = -2916341642173976522L;
    // data
    private String bundleUuid;

    public ListBundlePriceSummaryAction() {
    }

    public ListBundlePriceSummaryAction(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }
}
