package consys.event.registration.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.shared.bo.Monetary;

/**
 * Event vystreleny ked sa prepocita cena.
 *
 * @author palo
 */
public class OrderTotalPriceEvent extends GwtEvent<OrderTotalPriceEvent.Handler> {

    /**
     * typ eventu
     */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data    
    private Monetary price;

    public OrderTotalPriceEvent(Monetary price) {
        this.price = price;
    }

    public Monetary getPrice() {
        return price;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onTotalPriceChanged(this);
    }

    /**
     * interface eventu
     */
    public interface Handler extends EventHandler {

        public void onTotalPriceChanged(OrderTotalPriceEvent event);
    }
}
