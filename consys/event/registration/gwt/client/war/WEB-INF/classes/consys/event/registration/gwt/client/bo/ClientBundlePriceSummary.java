package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.Monetary;
import java.util.Date;


/**
 *
 * @author pepa
 */
public class ClientBundlePriceSummary implements Result {
    private static final long serialVersionUID = 2087527157906632623L;

    private Date to;
    private Monetary price;

    public ClientBundlePriceSummary() {
    }

    public ClientBundlePriceSummary(Date to, Monetary price) {
        this.to = to;
        this.price = price;
    }

    public Monetary getPrice() {
        return price;
    }

    public void setPrice(Monetary price) {
        this.price = price;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
