package consys.event.registration.gwt.client.module.setting.question;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.common.gwt.client.module.payment.PaymentDialog;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import consys.event.registration.gwt.client.action.ListQuestionsForUserAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.bo.ClientQuestions;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class QuestionWidget extends FlowPanel {

    // komponenty
    private BaseForm form;
    private FlowPanel messagePart;
    // data
    private PaymentDialog payDialog;
    private List<ConsysStringTextBox> boxs;

    public QuestionWidget(PaymentDialog payDialog) {
        this.payDialog = payDialog;
        boxs = new ArrayList<ConsysStringTextBox>();

        messagePart = new FlowPanel();
    }

    @Override
    protected void onLoad() {
        if (boxs.isEmpty()) {
            EventBus.fire(new EventDispatchEvent(new ListQuestionsForUserAction(), new AsyncCallback<ClientQuestions>() {
                @Override
                public void onFailure(Throwable caught) {
                    // obecne chyby zpracovava event action executor
                }

                @Override
                public void onSuccess(ClientQuestions result) {
                    if (result.getQuestions().isEmpty()) {
                        payDialog.drawContent(true);
                    } else {
                        init(result);
                    }
                }
            }, payDialog));
        }
    }

    private void init(ClientQuestions result) {
        SimplePanel padder = new SimplePanel();
        padder.setSize("10px", "10px");

        SimplePanel padder2 = new SimplePanel();
        padder2.setSize("10px", "10px");

        form = new BaseForm("200px");
        form.setStyleName(StyleUtils.MARGIN_LEFT_10);
        int row = 0;
        for (ClientCustomQuestion q : result.getQuestions()) {
            ConsysStringTextBox qBox = new ConsysStringTextBox(q.isRequired() ? 1 : 0, 2048, q.getText());
            qBox.getElement().setId(q.getUuid());
            if (q.isRequired()) {
                form.addRequired(row, q.getText(), qBox);
            } else {
                form.addOptional(row, q.getText(), qBox);
            }
            boxs.add(qBox);
            row++;
        }

        ActionImage button = ActionImage.getConfirmButton(ERMessageUtils.c.questionWidget_action_next());
        button.addStyleName(ECoResourceUtils.bundle().css().paymentDialogOk());
        button.addClickHandler(nextClickHandler());

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), StyleUtils.FLOAT_LEFT + " " + StyleUtils.MARGIN_TOP_3);
        cancel.setClickHandler(hideClickHandler());

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(button);
        controlPanel.add(cancel);
        controlPanel.add(StyleUtils.clearDiv("10px"));

        add(padder);
        add(messagePart);
        add(StyleUtils.getStyledLabel(ERMessageUtils.c.questionWidget_title(), StyleUtils.FONT_16PX,
                StyleUtils.FONT_BOLD, StyleUtils.MARGIN_BOT_20, StyleUtils.MARGIN_RIGHT_20, StyleUtils.MARGIN_LEFT_20));
        add(form);
        add(padder2);
        add(controlPanel);
    }

    private ClickHandler nextClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (form.validate(getFailMessage())) {
                    payDialog.drawContent(false);
                }
            }
        };
    }

    private ClickHandler hideClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                payDialog.hide();
            }
        };
    }

    public ConsysMessage getFailMessage() {
        ConsysMessage fail = ConsysMessage.getFail();
        fail.setStyleName(StyleUtils.MARGIN_BOT_10);
        messagePart.add(fail);
        fail.setWidth(504);
        fail.addStyleName(StyleUtils.ALIGN_CENTER);
        fail.addStyleName(StyleUtils.MARGIN_CENTER);
        DOM.setStyleAttribute(fail.getParent().getElement(), "textAlign", "center");
        return fail;
    }

    public List<ConsysStringTextBox> getBoxs() {
        return boxs;
    }
}
