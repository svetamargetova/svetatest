package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author pepa
 */
public class ClientAnswer implements IsSerializable {

    // data
    private int order;
    private String question;
    private String answer;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
