package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientBundle;

/**
 * Akce pro nacteni nastaveni jednoho bundlu
 * @author pepa
 */
public class LoadRegistrationBundleAction extends EventAction<ClientBundle> {

    private static final long serialVersionUID = -1996697949905028366L;
    // data
    private String uuid;

    public LoadRegistrationBundleAction() {
    }

    public LoadRegistrationBundleAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
