package consys.event.registration.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event vystreleny, kdyz se chce uzivatel zaregistrovat do eventu
 * @author pepa
 */
public class OrderConfirmedEvent extends GwtEvent<OrderConfirmedEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private String orderUuid;

    public OrderConfirmedEvent(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    public String getOrderUuid() {
        return orderUuid;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onOrderConfirmed(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onOrderConfirmed(OrderConfirmedEvent event);
    }
}
