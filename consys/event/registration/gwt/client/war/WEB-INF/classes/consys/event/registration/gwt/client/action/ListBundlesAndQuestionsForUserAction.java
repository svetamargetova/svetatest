package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions;

/**
 * Akce pro nacteni balicku a otazek pro interni registrace
 * @author pepa
 */
public class ListBundlesAndQuestionsForUserAction extends EventAction<ClientBundlesAndQuestions> {

    private static final long serialVersionUID = 5446005666706745777L;
}
