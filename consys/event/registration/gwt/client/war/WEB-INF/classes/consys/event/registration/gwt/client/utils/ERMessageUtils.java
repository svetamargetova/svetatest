package consys.event.registration.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.event.registration.gwt.client.message.EventRegistrationConstants;
import consys.event.registration.gwt.client.message.EventRegistrationMessages;

/**
 *
 * @author pepa
 */
public class ERMessageUtils {

    /** systemove konstanty modulu event registration */
    public static final EventRegistrationConstants c = (EventRegistrationConstants) GWT.create(EventRegistrationConstants.class);
    /** systemove zpravy modulu event registration */
    public static final EventRegistrationMessages m = (EventRegistrationMessages) GWT.create(EventRegistrationMessages.class);
}
