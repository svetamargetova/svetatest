package consys.event.registration.gwt.client.module.register;

import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import consys.common.constants.img.EventLogoImageEnum;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.CreateOuterRegisterAction;
import consys.common.gwt.client.rpc.action.IsUserLoggedAction;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.comp.wrapper.ConsysBaseWrapperWithBorderFace;
import consys.common.gwt.client.ui.comp.wrapper.ConsysWrapper;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.event.FireAfterLoginEvent;
import consys.common.gwt.client.ui.event.PanelItemEvent;
import consys.common.gwt.client.ui.layout.panel.PanelDispatcher;
import consys.common.gwt.client.ui.layout.panel.PanelItemAction;
import consys.common.gwt.client.ui.layout.panel.PanelItemType;
import consys.common.gwt.client.ui.payment.PaymentProfilePanel;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.client.widget.AccountRegisterForm;
import consys.common.gwt.client.widget.LoginForm;
import consys.common.gwt.client.widget.exception.UserExistsException;
import consys.common.gwt.client.widget.utils.CWMessageUtils;
import consys.common.gwt.shared.bo.*;
import consys.common.gwt.shared.exception.*;
import consys.event.common.gwt.client.action.LoadOuterEventInfoAction;
import consys.event.common.gwt.client.bo.ClientOuterEventData;
import consys.event.common.gwt.client.event.EventOverseerInitializationEvent;
import consys.event.registration.gwt.client.action.LoadOuterRegisterAction;
import consys.event.registration.gwt.client.bo.ClientOuterRegisterData;
import consys.event.registration.gwt.client.event.OrderTotalPriceEvent;
import consys.event.registration.gwt.client.module.register.item.RegistrationFormPanel;
import consys.event.registration.gwt.client.module.register.panel.OuterRegistrationFormPanel;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Registracni formular pro prokliknuti ze stranek poradatele (obsahuje uz
 * balicek a dalsi potrebne veci) TODO: jinak pojmenovat login, on je to vlastne
 * login/potvrzeni ze chce prokliknout
 *
 * @author pepa
 */
public class OuterRegistrationForm extends SimpleFormPanel {

    // konstanty
    public static final String PARAM_API_KEY = "api_key";
    public static final String PARAM_EVENT_UUID = "eid";
    public static final String PARAM_PACKAGE_UUID = "pid";
    public static final String PARAM_NEW_USER = "newUser";
    public static final String PARAM_BUNDLE_UUID = "bundleUuid";
    // komponenty   
    private LoginPanel loginPanel;
    private RegistrationPanel registrationPanel;
    // data
    private String apiKey;
    private String eventUuid;
    private String packageUuid;

    private static Image getEventLogo(ClientEventThumb thumb) {
        Image logo = FormUtils.eventLogo(thumb.getLogoUuidPrefix(), EventLogoImageEnum.WALL);
        logo.setAltText(thumb.getFullName());
        logo.setTitle(thumb.getFullName());
        logo.addStyleName(MARGIN_VER_10);
        return logo;
    }

    @Override
    protected void onLoad() {
        clear();

        apiKey = null;
        eventUuid = null;
        packageUuid = null;

        if (processParameters()) {
            // zjisti jestli je user prihlaseny
            ActionExecutor.execute(new IsUserLoggedAction(), new AsyncCallback<BooleanResult>() {

                @Override
                public void onFailure(Throwable caught) {
                    throw new UnsupportedOperationException("Not supported yet.");
                }

                @Override
                public void onSuccess(BooleanResult result) {
                    if (result.isBool()) {
                        History.newItem(getAfterLoginToken(false));
                    } else {
                        // nacitame informacie o evente
                        EventBus.get().fireEvent(new DispatchEvent(new LoadOuterEventInfoAction(eventUuid),
                                new AsyncCallback<ClientOuterEventData>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        throw new UnsupportedOperationException("Not supported yet.");
                                    }

                                    @Override
                                    public void onSuccess(ClientOuterEventData result) {
                                        // nacachujeme meno eventu 
                                        Cache.get().register(OuterRegistrationFormPanel.OUTER_REGISTER_EVENT_NAME, result.getFullName());
                                        // Upravime pravy panel 
                                        EventBus.get().fireEvent(new PanelItemEvent(PanelItemAction.CLEAR, PanelItemType.ALL));
                                        PanelDispatcher.get().addPanelItem(new OuterRegistrationFormPanel(), PanelItemType.SYSTEM);
                                        // inicializujeme overseer dispatch service
                                        EventBus.get().fireEvent(new EventOverseerInitializationEvent(result.getUuid(), null, result.getOverseer()));
                                        // vygenerujeme zakladny layout
                                        generateContent(result);
                                    }
                                }, OuterRegistrationForm.this));
                    }
                }
            }, null);
        }
    }

    /**
     * vytvori obsah formulare
     */
    private void generateContent(ClientEventThumb event) {

        registrationPanel = new RegistrationPanel(event);
        loginPanel = new LoginPanel(event);

        // defaultne je skovany prihlasovaci formular
        loginPanel.setVisible(false);

        addWidget(loginPanel);
        addWidget(registrationPanel);
    }

    /**
     * Panel ktory preklikava medzi registrovanym a novym uzivatelom
     */
    private ConsysWrapper createChangePanel(String labelText, String actionText) {
        ActionLabel action = new ActionLabel(actionText, FLOAT_LEFT + " " + MARGIN_TOP_8);
        action.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                loginPanel.setVisible(!loginPanel.isVisible());
                registrationPanel.setVisible(!registrationPanel.isVisible());
            }
        });
        FlowPanel changePanel = new FlowPanel();
        changePanel.setSize((LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 12) + "px", "30px");
        changePanel.add(StyleUtils.getStyledLabel(labelText, MARGIN_LEFT_20, FONT_17PX, FONT_BOLD, FLOAT_LEFT, MARGIN_RIGHT_20, MARGIN_TOP_5));
        changePanel.add(action);
        changePanel.add(StyleUtils.clearDiv());
        return new ConsysBaseWrapperWithBorderFace(changePanel);
    }

    /**
     * nacte parametry z tokenu
     */
    private boolean processParameters() {        
        HashMap<String, String> params = HistoryUtils.getParameters();

        for (Map.Entry<String, String> e : params.entrySet()) {
            if (PARAM_API_KEY.equalsIgnoreCase(e.getKey())) {
                apiKey = e.getValue();
                LoggerFactory.log(OuterRegistrationForm.class, "Found API KEY " + apiKey);
            }
            if (PARAM_EVENT_UUID.equalsIgnoreCase(e.getKey())) {
                eventUuid = e.getValue();
                LoggerFactory.log(OuterRegistrationForm.class, "Found Event UUID " + eventUuid);
            }
            if (PARAM_PACKAGE_UUID.equalsIgnoreCase(e.getKey())) {
                packageUuid = e.getValue();
                LoggerFactory.log(OuterRegistrationForm.class, "Found Bundle UUID " + packageUuid);
            }
        }

        boolean out = false;

        if (apiKey == null) {
            getFailMessage().setText(UIMessageUtils.m.const_missingParameter(PARAM_API_KEY));
        } else if (eventUuid == null) {
            getFailMessage().setText(UIMessageUtils.m.const_missingParameter(PARAM_EVENT_UUID));
        } else if (packageUuid == null) {
            getFailMessage().setText(UIMessageUtils.m.const_missingParameter(PARAM_PACKAGE_UUID));
        } else {
            out = true;
        }
        return out;
    }

    /**
     * vrati token, na ktery se ma presmerovat po prihlaseni uzivatele
     */
    private String getAfterLoginToken(boolean newUser) {
        String token = "Event?event=" + eventUuid + "&";
        if (newUser) {
            token += PARAM_NEW_USER + "=yes";
        } else {
            token += PARAM_NEW_USER + "=no&" + PARAM_BUNDLE_UUID + "=" + packageUuid;
        }
        return token;
    }

    /**
     * Registracny panel
     */
    private class RegistrationPanel extends FlowPanel implements OrderTotalPriceEvent.Handler {

        private ClientEventThumb event;
        // komponenty - registracia uctu
        private SimpleFormWithHelpPanel accountRegistrationPanel;
        private AccountRegisterForm accountRegistrationForm;
        // komponenty - vyplnenie fakturacnych udajov
        private SimpleFormWithHelpPanel invoiceDetailsPanel;
        private PaymentProfilePanel paymentProfilePanel;
        // komponenty - vyber tiketu a balickov
        private RegistrationFormPanel registrationPanel;
        // komponenty - action Register
        private FlowPanel exceptionPanel;
        private ActionImage registerButton;

        public RegistrationPanel(ClientEventThumb event) {
            this.event = event;
        }

        /**
         * Na onLoad sa vytovri zakladna kostra panelu. A spusti sa nacitanie
         * dat
         */
        @Override
        protected void onLoad() {
            super.onLoad();

            // registrujeme handler na total panel
            EventBus.get().addHandler(OrderTotalPriceEvent.TYPE, this);

            add(createChangePanel(ERMessageUtils.c.outerRegisterContent_text_alreadyRegistered(), ERMessageUtils.c.outerRegisterContent_action_clickToLogin()));
            add(getEventLogo(event));
            add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationForm_title(), FONT_16PX, FONT_BOLD, MARGIN_RIGHT_10, MARGIN_TOP_10));
            add(new Separator("100%", MARGIN_VER_10));
            add(createAccountRegistrationPanel());
            add(new Separator("100%", MARGIN_VER_10));
            add(createInvoiceDetailsPanel());
            add(new Separator("100%", MARGIN_VER_10));
            add(createTicketRegistrationPanel());

        }

        @Override
        protected void onUnload() {
            super.onUnload();
            EventBus.get().removeHandler(OrderTotalPriceEvent.TYPE, this);
        }

        private FlowPanel createTicketRegistrationPanel() {
            final FlowPanel panel = new FlowPanel();
            panel.setStyleName(ERResourceUtils.bundle().css().outerRegistrationForm());
            EventBus.get().fireEvent(new EventDispatchEvent(new LoadOuterRegisterAction(packageUuid),
                    new AsyncCallback<ClientOuterRegisterData>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            throw new UnsupportedOperationException("Not supported yet.");
                        }

                        @Override
                        public void onSuccess(ClientOuterRegisterData result) {
                            if (!result.getItems().get(0).isUnlimitedCapacity() && result.getItems().get(0).getFreeCapacity() == 0) {
                                ConsysMessage m = ConsysMessage.getInfo();
                                m.setText(ERMessageUtils.c.outerRegisterContent_error_ticketFull());
                                panel.add(m);
                            } else {
                                // vygenerujeme zakladny layout
                                registrationPanel = new RegistrationFormPanel(result.getItems().get(0), result.getCurrency());
                                result.getItems().remove(0);
                                registrationPanel.setSubbundles(result.getItems(), true);
                                panel.add(registrationPanel);
                                panel.add(new Separator("100%", MARGIN_VER_10));
                                panel.add(createFooter());
                                refreshInvoiceDetailsPanel(registrationPanel.getTotal());
                            }
                        }
                    }, OuterRegistrationForm.this));
            return panel;
        }

        private SimpleFormWithHelpPanel createInvoiceDetailsPanel() {
            // vytvorime platobny profil

            paymentProfilePanel = new PaymentProfilePanel(OuterRegistrationForm.this, false);
            paymentProfilePanel.setProfile(new ClientUserPaymentProfile());
            paymentProfilePanel.setWidth("490px");


            invoiceDetailsPanel = new SimpleFormWithHelpPanel();
            invoiceDetailsPanel.addWidget(paymentProfilePanel);
            invoiceDetailsPanel.addHelpText(ERMessageUtils.c.outerRegisterContent_help5());
            invoiceDetailsPanel.addHelpText(ERMessageUtils.c.outerRegisterContent_help6());

            return invoiceDetailsPanel;
        }

        private SimpleFormWithHelpPanel createAccountRegistrationPanel() {
            // Registracia nove takeplace uctu
            accountRegistrationForm = new AccountRegisterForm("170px", false, true);
            accountRegistrationForm.addStyleName(MARGIN_TOP_10);
            // ak je invoicedTo prazdne tak tam autoamticky vlozime "firstName lastName"
            accountRegistrationForm.getLastNameBox().addBlurHandler(new BlurHandler() {

                @Override
                public void onBlur(BlurEvent event) {
                    if (StringUtils.isBlank(paymentProfilePanel.getNameBox().getValue())) {
                        StringBuilder sb = new StringBuilder();
                        if (StringUtils.isNotBlank(accountRegistrationForm.getFirstNameBox().getValue())) {
                            sb.append(accountRegistrationForm.getFirstNameBox().getValue());
                            sb.append(" ");
                        }
                        if (StringUtils.isNotBlank(accountRegistrationForm.getLastNameBox().getValue())) {
                            sb.append(accountRegistrationForm.getLastNameBox().getValue());
                        }
                        if (sb.length() > 0) {
                            paymentProfilePanel.getNameBox().setValue(sb.toString());
                        }
                    }
                }
            });

            accountRegistrationPanel = new SimpleFormWithHelpPanel();
            accountRegistrationPanel.addWidget(accountRegistrationForm);
            accountRegistrationPanel.addHelpText(ERMessageUtils.c.outerRegisterContent_help1());
            accountRegistrationPanel.addHelpText(ERMessageUtils.c.outerRegisterContent_help2());
            accountRegistrationPanel.addHelpText(ERMessageUtils.c.outerRegisterContent_help3());
            accountRegistrationPanel.addHelpText(ERMessageUtils.c.outerRegisterContent_help4());

            return accountRegistrationPanel;
        }

        /**
         * Vytvori paticku s registracnym tlacidlom
         */
        private FlowPanel createFooter() {
            HTML termsHtml = new HTML(CWMessageUtils.c.registerUser_text_agreeing());
            termsHtml.addStyleName(ResourceUtils.system().css().htmlAgreeing());
            termsHtml.setWidth("440px");
            termsHtml.addStyleName(MARGIN_TOP_10);
            termsHtml.addStyleName(FLOAT_LEFT);

            registerButton = ActionImage.getConfirmButton(ERMessageUtils.c.outerRegisterContent_button_register());
            registerButton.addClickHandler(registerButtonClickHandler());
            registerButton.addStyleName(FLOAT_RIGHT);
            registerButton.addStyleName(MARGIN_TOP_10);

            FlowPanel actionPanel = new FlowPanel();
            actionPanel.add(registerButton);
            actionPanel.add(termsHtml);
            actionPanel.add(StyleUtils.clearDiv());

            exceptionPanel = new FlowPanel();

            FlowPanel footer = new FlowPanel();
            footer.add(exceptionPanel);
            footer.add(actionPanel);
            return footer;
        }

        private void refreshInvoiceDetailsPanel(Monetary price) {
            invoiceDetailsPanel.setVisible(!price.isZero());
            invoiceDetailsPanel.getElement().getNextSiblingElement().getStyle().setVisibility(price.isZero() ? Visibility.HIDDEN : Visibility.VISIBLE);
        }

        /**
         * click handler po kliknuti na tlacitko zaregistrovat ucet
         */
        private ClickHandler registerButtonClickHandler() {
            return new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    boolean formValid = true;
                    // procistime vynimky
                    exceptionPanel.clear();

                    final ClientRegistration r = accountRegistrationForm.generateRegistration();
                    if (!accountRegistrationForm.validate(accountRegistrationPanel.getFailMessage())) {
                        formValid = false;
                    } else {
                        accountRegistrationPanel.clearMessageBox();
                    }

                    if (!ValidatorUtils.isValidEmailAddress(r.getContactEmail())) {
                        accountRegistrationPanel.getFailMessage().addOrSetText(UIMessageUtils.c.const_error_invalidEmailAddress());
                        formValid = false;
                    } else {
                        accountRegistrationPanel.clearMessageBox();
                    }

                    ClientUserPaymentProfile pp = paymentProfilePanel.getClientPaymentProfile();
                    if (invoiceDetailsPanel.isVisible() && !paymentProfilePanel.doValidate(paymentProfilePanel.getFailMessage())) {
                        formValid = false;
                    } else {
                        paymentProfilePanel.clearMessageBox();
                    }

                    if (!formValid) {
                        return;
                    }

                    CreateOuterRegisterAction rAction = new CreateOuterRegisterAction();
                    rAction.setApiKey(apiKey);
                    rAction.setEventUuid(eventUuid);
                    
                    ClientSsoUserDetails details = new ClientSsoUserDetails();
                    details.setAffiliation(r.getAffiliation());
                    details.setPassword(r.getPassword());
                    details.setLoginEmail(r.getContactEmail());
                    details.setFirstName(r.getFirstName());
                    details.setLastName(r.getLastName());                                        
                    rAction.setRegistration(details);
                    rAction.setPaymentProfile(pp);

                    List<ProductThumb> orderedItems = registrationPanel.getOrderedItems(false);
                    rAction.setPackageUuid(orderedItems.get(0).getProductUuid());
                    rAction.setDiscountTicket(orderedItems.get(0).getDiscountCode());
                    orderedItems.remove(0);
                    rAction.getSubbundles().addAll(orderedItems);


                    EventBus.get().fireEvent(new DispatchEvent(rAction,
                            new AsyncCallback<VoidResult>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    processRegisterException(caught);
                                }

                                @Override
                                public void onSuccess(VoidResult result) {
                                    loginPanel.doUserLogin(r.getContactEmail(), r.getPassword());
                                }
                            }, OuterRegistrationForm.this));
                }
            };
        }

        @Override
        public void onTotalPriceChanged(OrderTotalPriceEvent event) {
            refreshInvoiceDetailsPanel(event.getPrice());
        }

        public void processRegisterException(Throwable caught) {
            // obecne chyby zpracovava action executor
            if (caught instanceof BundleCapacityException) {
                ConsysMessage fail = ConsysMessage.getFail();
                fail.addOrSetText(ERMessageUtils.c.outerRegisterContent_error_ticketFull());
                exceptionPanel.add(fail);
            } else if (caught instanceof CouponInvalidException) {
                ConsysMessage fail = ConsysMessage.getFail();
                fail.addOrSetText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponInvalidException());
                exceptionPanel.add(fail);
            } else if (caught instanceof CouponUsedException) {
                ConsysMessage fail = ConsysMessage.getFail();
                fail.addOrSetText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponUsedException());
                exceptionPanel.add(fail);
            } else if (caught instanceof NoRecordsForAction) {
                getFailMessage().addOrSetText(ERMessageUtils.c.outerRegisterContent_error_noRecorsForParameterValues());
            } else if (caught instanceof UserExistsException) {
                accountRegistrationPanel.getFailMessage().addOrSetText(CWMessageUtils.c.registerContent_error_alreadyRegistered());
            } else if (caught instanceof IbanException) {
                invoiceDetailsPanel.getFailMessage().addOrSetText(UIMessageUtils.c.const_exceptionBadIban());
            }
        }
    }

    /**
     * Prihlasovaci panel
     */
    private class LoginPanel extends FlowPanel {

        private LoginForm loginForm;
        private ClientEventThumb eventThumb;

        public LoginPanel(ClientEventThumb eventThumb) {
            this.eventThumb = eventThumb;
        }

        @Override
        protected void onLoad() {
            loginForm = new LoginForm(new ConsysAction() {

                @Override
                public void run() {
                    EventBus.get().fireEvent(new FireAfterLoginEvent(getAfterLoginToken(false)));
                    loginForm.getHandler().doLogin();
                }
            });
            loginForm.enableLostPasswordHint();
            loginForm.addStyleName(MARGIN_BOT_10);


            add(getEventLogo(eventThumb));
            add(StyleUtils.getStyledLabel(CWMessageUtils.c.loginContent_title(), FONT_19PX, FONT_BOLD, MARGIN_RIGHT_10, MARGIN_TOP_10));
            add(new Separator("100%", MARGIN_VER_10));
            add(loginForm);
            add(createChangePanel(ERMessageUtils.c.outerRegisterContent_text_notYetRegistered(), ERMessageUtils.c.outerRegisterContent_action_clickToRegister()));
        }

        public void doUserLogin(String username, String password) {
            loginForm.setup(username, password);
            EventBus.get().fireEvent(new FireAfterLoginEvent(getAfterLoginToken(true)));
            loginForm.getHandler().doLogin();
        }
    }
}
