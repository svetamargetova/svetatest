package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro smazani otazky
 * @author pepa
 */
public class DeleteCustomQuestionAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 1911028363525783851L;
    // data
    private String questionUuid;

    public DeleteCustomQuestionAction() {
    }

    public DeleteCustomQuestionAction(String questionUuid) {
        this.questionUuid = questionUuid;
    }

    public String getQuestionUuid() {
        return questionUuid;
    }
}
