package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro zruseni objednavky
 * @author pepa
 */
public class CancelOrderAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -6742693404598932332L;
    // data
    private String orderUuid;
    // priznak ci objednavku rusi vlastnik (pouzivatel) alebo organizator
    private boolean sourceOwner;

    public CancelOrderAction() {
    }

    public CancelOrderAction(String orderUuid, boolean sourceOwner) {
        this.orderUuid = orderUuid;
        this.sourceOwner = sourceOwner;
    }

    public String getOrderUuid() {
        return orderUuid;
    }

    public boolean isSourceOwner() {
        return sourceOwner;
    }
        
}
