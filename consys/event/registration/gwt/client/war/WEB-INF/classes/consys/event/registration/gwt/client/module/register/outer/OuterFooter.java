package consys.event.registration.gwt.client.module.register.outer;

import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.registration.gwt.client.module.register.outer.comp.ULPanel;

/**
 *
 * @author pepa
 */
public class OuterFooter extends Composite {

    public OuterFooter() {
        FlowPanel footer = new FlowPanel();
        footer.getElement().setId("footer");

        initWidget(footer);
        generate(footer);
    }

    private void generate(FlowPanel panel) {
        SimplePanel separator = FormUtils.createPanel(OuterResources.INSTANCE.css().bottomSeparator());

        SimplePanel bottom1 = FormUtils.createPanel(OuterResources.INSTANCE.css().bottom1());
        bottom1.setWidget(bottom1());

        SimplePanel bottom2 = FormUtils.createPanel(OuterResources.INSTANCE.css().bottom2());
        bottom2.setWidget(bottom2());

        panel.add(separator);
        panel.add(bottom1);
        panel.add(bottom2);
    }

    private FlowPanel bottom1() {
        SimplePanel logo = FormUtils.createPanel(OuterResources.INSTANCE.css().bottomTakeplace());

        SimplePanel menu = FormUtils.createPanel(OuterResources.INSTANCE.css().bottomMenu());
        menu.setWidget(menu());

        FlowPanel bottom1 = new FlowPanel();
        bottom1.setStyleName(OuterResources.INSTANCE.css().bottomCenter());
        bottom1.add(menu);
        bottom1.add(logo);
        bottom1.add(StyleUtils.clearDiv());
        return bottom1;
    }

    private ULPanel menu() {
        ULPanel menu = new ULPanel(OuterResources.INSTANCE.css().bottomMenu());
        menu.add(new Anchor("Home", "http://www.takeplace.eu/en"));
        menu.add(textSeparator());
        menu.add(new Anchor("Features", "http://www.takeplace.eu/en/features"));
        menu.add(textSeparator());
        menu.add(new Anchor("Overview", "http://www.takeplace.eu/en/overview"));
        menu.add(textSeparator());
        menu.add(new Anchor("Customers", "http://www.takeplace.eu/en/customers"));
        menu.add(textSeparator());
        menu.add(new Anchor("Press & Media", "http://www.takeplace.eu/en/press-media"));
        menu.add(textSeparator());
        menu.add(new Anchor("About us", "http://www.takeplace.eu/en/about-us"));
        menu.add(textSeparator());
        menu.add(new Anchor("SIGN UP", "https://app.takeplace.eu/takeplace/takeplace.html#SignUp"));
        menu.add(textSeparator());
        menu.add(new Anchor("FAQ", "https://app.takeplace.eu/takeplace/takeplace.html#FAQ"));
        return menu;
    }

    private Label textSeparator() {
        Label textSeparator = new Label("|");
        textSeparator.setStyleName(OuterResources.INSTANCE.css().bottomTextSeparator());
        return textSeparator;
    }

    private SimplePanel bottom2() {
        SimplePanel bottom2 = FormUtils.createPanel(OuterResources.INSTANCE.css().bottomCenter());
        bottom2.setWidget(new HTML("2009&ndash;2013 &copy; ACEMCEE All rights reserved"));
        return bottom2;
    }
}
