package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 *
 * @author pepa
 */
public class CancelWaitingAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -448786205793263736L;
    // data
    private String registrationUuid;

    public CancelWaitingAction() {
    }

    public CancelWaitingAction(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    public String getRegistrationUuid() {
        return registrationUuid;
    }
}
