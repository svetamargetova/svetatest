package consys.event.registration.gwt.client.module.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class BundleUniquityException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public BundleUniquityException() {
        super("");
    }

    public BundleUniquityException(String message) {
        super(message);
    }

    public BundleUniquityException(Throwable cause) {
        super(cause);
    }

    public BundleUniquityException(String message, Throwable cause) {
        super(message, cause);
    }
}
