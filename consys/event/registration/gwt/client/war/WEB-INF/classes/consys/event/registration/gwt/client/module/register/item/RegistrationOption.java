package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.RadioButton;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.payment.PaymentProfilePanel;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.bo.EventUser;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.common.gwt.shared.exception.CouponExpiredException;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.common.gwt.shared.exception.CouponUsedException;
import consys.common.gwt.shared.exception.IbanException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.gwt.client.action.ListPaymentDataDialogAction;
import consys.event.common.gwt.client.action.exception.EventNotActivatedException;
import consys.event.common.gwt.client.module.payment.PaymentDialog;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.action.CreateWaitingRecordAction;
import consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction;
import consys.event.registration.gwt.client.action.RegisterUserAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.event.NewOrderEvent;
import consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import consys.event.registration.gwt.client.module.register.WaitingOverview;
import consys.event.registration.gwt.client.module.setting.question.QuestionWidget;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class RegistrationOption extends RegistrationFormPanel implements CssStyles {

    // komponenty
    private RadioButton radioButton;
    // data
    private final RegistrationForm parent;
    private final String newOrderEventToken;
    private EventUser managedUser;

    public RegistrationOption(ClientRegistrationPack ticket, String currency, RegistrationForm parent, String newOrderEventToken) {
        super(ticket, currency);
        this.parent = parent;
        this.newOrderEventToken = newOrderEventToken;
    }

    public void setManagedUser(EventUser managedUser) {
        this.managedUser = managedUser;
    }

    @Override
    protected void onLoad() {
        // nastavime click handler na titulku 
        changeTitleBehaviour();

        // nastavime rozklik celej polozky a jej oznacenia na rozklik description
        changeMoreDetailsBehaviour();

        // vtgenerujeme povodny formular
        super.onLoad();

        // pridame radio button
        radioButton = new RadioButton(RegistrationForm.RADIO_USER_REGISTRATION);
        radioButton.addStyleName(ResourceUtils.common().css().action());
        radioButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    selected();
                } else {
                    unselected();
                }
            }
        });
        getTicketPanel().setActionWidget(radioButton);

    }

    private void changeTitleBehaviour() {
        getTicketPanel().setTitleClickHandle(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (!radioButton.getValue()) {
                    selected();
                }
            }
        });
    }

    private void changeMoreDetailsBehaviour() {
        getTicketPanel().setDetailsClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                selected();
            }
        });
    }

    private InlineLabel getPricingLabel(String text, boolean bold) {
        InlineLabel label = new InlineLabel(" " + text);
        label.addStyleName(ERResourceUtils.bundle().css().registrationFormPricingLabel());
        if (bold) {
            label.addStyleName(FONT_BOLD);
        }
        return label;
    }

    @Override
    protected void showDetails(FlowPanel toPanel) {
        super.showDetails(toPanel);

        FlowPanel pricingInfo = new FlowPanel();
        pricingInfo.setStyleName(ERResourceUtils.bundle().css().registrationFormPricingInfo());
        processPricing(pricingInfo, getTicket());

        boolean isCapacity = getTicket().getFreeCapacity() != 0;

        // pridame do detailu panel s registracnym buttonom
        ActionImage registerButton = ActionImage.getConfirmButton(isCapacity
                ? ERMessageUtils.c.outerRegisterContent_button_register()
                : ERMessageUtils.c.registrationForm_action_toWaiting());
        // TODO: jak to udelat kdyz je castecna kapacita oproti kvantite
        registerButton.addClickHandler(isCapacity ? registerHandler() : waitingHandler(1));
        registerButton.addStyleName(FLOAT_RIGHT);
        registerButton.addStyleName(MARGIN_TOP_10);

        FlowPanel actionPanel = new FlowPanel();
        actionPanel.add(registerButton);
        actionPanel.add(pricingInfo);
        actionPanel.add(StyleUtils.clearDiv());
        toPanel.add(actionPanel);
    }

    private ClickHandler registerHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //if(!isForFree()){
                showPaymentDialog();
                //}
            }
        };
    }

    private ClickHandler waitingHandler(final int quantity) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ArrayList<ProductThumb> products = (ArrayList<ProductThumb>) getOrderedItems(false);
                String managedUserUuid = null;
                if (managedUser != null) {
                    managedUserUuid = managedUser.getUuid();
                }
                CreateWaitingRecordAction action = new CreateWaitingRecordAction(products.get(0).getProductUuid(), quantity, managedUserUuid);
                EventBus.fire(new EventDispatchEvent(action,
                        new AsyncCallback<ClientRegistration>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                                if (caught instanceof PermissionException) {
                                    parent.getFailMessage().setText(UIMessageUtils.c.const_error_noPermissionForAction());
                                }
                            }

                            @Override
                            public void onSuccess(ClientRegistration result) {
                                WaitingOverview w = new WaitingOverview(result);
                                FormUtils.fireSameBreadcrumb(ERMessageUtils.c.waitingOverview_title(), w);
                            }
                        }, parent));
            }
        };
    }

    /** zpracuje pricingy */
    private void processPricing(FlowPanel panel, ClientRegistrationPack ticket) {
        ArrayList<Pricing> pricings = new ArrayList<Pricing>();
        if (ticket.getPricing() != null && !ticket.getPricing().isEmpty()) {
            for (Map.Entry<Date, Monetary> entry : ticket.getPricing().entrySet()) {
                pricings.add(new Pricing(entry.getKey(), entry.getValue()));
            }

            Collections.sort(pricings);
            final String currency = ECoMessageUtils.getCurrency();

            FlowPanel pricingPanel = new FlowPanel();
            pricingPanel.addStyleName(ERResourceUtils.bundle().css().registrationFormPricingPanel());

            final int size = pricings.size() - 1;
            for (int i = 0; i <= size; i++) {
                Pricing pricing = pricings.get(i);
                pricingPanel.add(addPricing(pricing, currency, ticket.getTo(), i == size));
                pricingPanel.add(new InlineLabel(" "));
            }

            panel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationForm_text_pricing() + ":",
                    ERResourceUtils.bundle().css().registrationFormPricingTitle()));
            panel.add(pricingPanel);
            panel.add(StyleUtils.clearDiv());
        }
    }

    /** vygeneruje jeden termin cen */
    private InlineLabel addPricing(Pricing pricing, String currency, Date to, boolean last) {
        boolean isBold = pricing.getDate().equals(to);
        String text = ERMessageUtils.m.registrationForm_text_before(DateTimeUtils.getDate(pricing.getDate()),
                UIMessageUtils.assignCurrency(currency, pricing.getPrice()));
        if (!last) {
            text = text + ", ";
        }
        return getPricingLabel(text, isBold);
    }

    /**
     * Po stlaceni registrace
     * TODO: refaktorovat, je to moc velke a neprehledne
     */
    public void showPaymentDialog() {
        ClientUser cu;
        try {
            cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
        } catch (NotInCacheException ex) {
            parent.getFailMessage().setText(UIMessageUtils.m.const_notInCache("User"));
            return;
        }
        PaymentDialog payDialog = new PaymentDialog(cu.getUuid(), true, false, false) {
            @Override
            public void doAction(PaymentProfilePanel profilePanel, String userUuid, final ConsysMessage fail) {
                final PaymentDialog self = this;
                if (profilePanel.isEdited() && (wasZeroInvoice() ? false : !profilePanel.doValidate(fail))) {
                    return;
                }

                ArrayList<ProductThumb> products = (ArrayList<ProductThumb>) getOrderedItems(false);

                RegisterUserAction registerAction = new RegisterUserAction(userUuid,
                        profilePanel.getClientPaymentProfile().getUuid(), products);
                registerAction.setProfileEdited(profilePanel.isEdited());
                registerAction.setManagedUserUuid(managedUser == null ? null : managedUser.getUuid());

                if (profilePanel.isEdited()) {
                    registerAction.setEditedProfile(profilePanel.getClientPaymentProfile());
                }

                HashMap<String, String> answers = new HashMap<String, String>();
                QuestionWidget qw = (QuestionWidget) getCustomWidget();
                for (ConsysStringTextBox box : qw.getBoxs()) {
                    answers.put(box.getElement().getId(), box.getText());
                }

                registerAction.setAnswers(answers);

                EventBus.fire(new EventDispatchEvent(registerAction,
                        new AsyncCallback<ClientRegistration>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                processExceptions(caught, fail);
                            }

                            @Override
                            public void onSuccess(ClientRegistration result) {
                                result.getRegistrationOrders().get(0).getOrderItems().get(0).setTitle(getTicket().getBundleTitle());
                                result.getRegistrationOrders().get(0).getOrderItems().get(0).setDescription(getTicket().getBundleDescription());
                                parent.showRegistrationOverview(result);
                                EventBus.get().fireEvent(new NewOrderEvent(newOrderEventToken));
                                hide();
                            }
                        }, self));
            }

            @Override
            public ListPaymentDataDialogAction getDataAction() {
                LoadRegisterUserPaymentDialogAction action;
                if (managedUser == null) {
                    action = new LoadRegisterUserPaymentDialogAction();
                } else {
                    action = new LoadRegisterUserPaymentDialogAction(managedUser.getUuid());
                }
                ArrayList<ProductThumb> list = (ArrayList) getOrderedItems(false);
                for (ProductThumb thumb : list) {
                    LoggerFactory.log(RegistrationForm.class, "load for thumb: " + thumb);
                }
                action.addProducts(list);
                return action;
            }

            private void processExceptions(Throwable caught, ConsysMessage fail) {
                // obecne chyby zpracovava EventActionExcecutor
                if (caught instanceof UserAlreadyRegistredException) {
                    fail.setText(ERMessageUtils.c.const_exceptionUserAlreadyRegistered());
                } else if (caught instanceof BundleCapacityException) {
                    fail.setText(ERMessageUtils.c.const_exceptionTicketCapacity());
                } else if (caught instanceof IbanException) {
                    fail.setText(UIMessageUtils.c.const_exceptionBadIban());
                } else if (caught instanceof EventNotActivatedException) {
                    fail.setText(ERMessageUtils.c.const_exceptionEventNotActivated());
                } else if (caught instanceof CouponUsedException) {
                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponUsedException());
                } else if (caught instanceof NoRecordsForAction) {
                    fail.setText(ERMessageUtils.c.registrationFormItem_exception_noRecordsForAction());
                } else if (caught instanceof CouponInvalidException) {
                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponInvalidException());
                } else if (caught instanceof CouponExpiredException) {
                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponExpiredException());
                } else if (caught instanceof PermissionException) {
                    fail.setText(UIMessageUtils.c.const_error_noPermissionForAction());
                }
            }

            @Override
            public void processDataActionFailure(Throwable caught, ConsysMessage fail) {
                processExceptions(caught, fail);
            }
        };
        payDialog.setManagedUserUuid(managedUser == null ? null : managedUser.getUuid());
        payDialog.setCustomWidget(new QuestionWidget(payDialog));
        payDialog.showCentered();
    }

    /** oznaci polozku za vybranou */
    public void selected() {
        radioButton.setValue(true, false);
        addStyleName(ResourceUtils.common().css().active());
        showDetail();
        parent.selectItem(this);
    }

    /** oznaci polozku za nevybranou */
    public void unselected() {
        //radioButton.setValue(false, false);
        removeStyleName(ResourceUtils.common().css().active());
        hideDetail();
    }

    /** pomocny objekt pro serazeni hodnot podle datumu a vypsani ceny k datumu */
    private class Pricing implements Comparable<Pricing> {

        // data
        private Date date;
        private Monetary price;

        public Pricing(Date date, Monetary price) {
            this.date = date;
            this.price = price;
        }

        public Date getDate() {
            return date;
        }

        public Monetary getPrice() {
            return price;
        }

        @Override
        public int compareTo(Pricing o) {
            return date.compareTo(o.getDate());
        }
    }
}
