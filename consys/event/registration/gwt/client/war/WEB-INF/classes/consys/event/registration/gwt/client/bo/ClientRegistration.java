package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.EventUser;
import consys.common.gwt.shared.bo.list.SelectFilterItem;
import java.util.ArrayList;

/**
 * Registrace uzivatele
 * @author pepa
 */
public class ClientRegistration implements Result {

    private static final long serialVersionUID = -1952176060160440427L;
    // data
    private boolean celiac;
    private boolean vegetarian;
    private String registrationUuid;
    private String note;
    private String specialWish;
    private State state;
    private SelectFilterItem item;
    private EventUser eventUser;
    private ArrayList<ClientRegistrationOrder> registrationOrders;
    private int quantity;
    private ArrayList<ClientAnswer> answers;

    public ClientRegistration() {
        registrationOrders = new ArrayList<ClientRegistrationOrder>();
        answers = new ArrayList<ClientAnswer>();
    }

    public ArrayList<ClientAnswer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<ClientAnswer> answers) {
        this.answers = answers;
    }

    public SelectFilterItem getItem() {
        return item;
    }

    public void setItem(SelectFilterItem item) {
        this.item = item;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ArrayList<ClientRegistrationOrder> getRegistrationOrders() {
        return registrationOrders;
    }

    public void setRegistrationOrders(ArrayList<ClientRegistrationOrder> registrationOrders) {
        this.registrationOrders = registrationOrders;
    }

    public String getRegistrationUuid() {
        return registrationUuid;
    }

    public void setRegistrationUuid(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    /**
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * @return the specialWish
     */
    public String getSpecialWish() {
        return specialWish;
    }

    /**
     * @param specialWish the specialWish to set
     */
    public void setSpecialWish(String specialWish) {
        this.specialWish = specialWish;
    }

    /**
     * @return the eventUser
     */
    public EventUser getEventUser() {
        return eventUser;
    }

    /**
     * @param eventUser the eventUser to set
     */
    public void setEventUser(EventUser eventUser) {
        this.eventUser = eventUser;
    }

    /**
     * @return the celiac
     */
    public boolean isCeliac() {
        return celiac;
    }

    /**
     * @param celiac the celiac to set
     */
    public void setCeliac(boolean celiac) {
        this.celiac = celiac;
    }

    /**
     * @return the vegetarian
     */
    public boolean isVegetarian() {
        return vegetarian;
    }

    /**
     * @param vegetarian the vegetarian to set
     */
    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }

    public enum State {

        /* Zaregistrovane ale nezaplatene */
        ORDERED(0),
        /* Zaregistroavny a zaplateny */
        CONFIRMED(1),
        /* Zrusene - externe user, organizator */
        CANCELED(2),
        /* Zrusene - timeout */
        TIMEOUTED(3),
        /* Cakajuci na potvrdenie */
        APPROVE(4),
        /* Zaregistrovany po limitu */
        WAITING(5);
        private static final long serialVersionUID = -1985854087697284277L;
        private Integer id;

        private State(Integer id) {
            this.id = id;
        }

        public static State fromId(Integer i) {
            switch (i) {
                case 0:
                    return ORDERED;
                case 1:
                    return CONFIRMED;
                case 2:
                    return CANCELED;
                case 3:
                    return TIMEOUTED;
                case 4:
                    return APPROVE;
            }
            throw new RuntimeException();
        }

        public Integer getId() {
            return id;
        }
    }
}
