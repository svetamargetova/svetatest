package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.event.ChangeOrderPriceEvent;
import consys.event.registration.gwt.client.event.OrderTotalPriceEvent;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel ktory presentuje a implementuje vnutornu logiku vyberu subbalickov a
 * prepocitavanie ceny vzhladom na moznost vlozit zlavovy kod. Vstupom je hlavny
 * tiket ktory sa uz nemeni a subbalicky ktore je mozne pridat
 *
 * @see
 * consys.event.registration.gwt.client.module.register.outer.OuterRegisterContent
 * @see consys.event.registration.gwt.client.module.register.RegistrationForm
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationFormPanel extends ConsysFlowPanel implements ChangeOrderPriceEvent.Handler {

    // data
    private ClientRegistrationPack ticket;
    private List<ClientRegistrationPack> subbundles;
    private boolean showDetail;
    private Monetary total;
    private boolean newComponents;
    // komponenty
    private RegistrationFormPanelItem ticketPanel;
    private FlowPanel detailPanel;
    // data
    private String currency;

    public RegistrationFormPanel(ClientRegistrationPack ticket, String currency) {
        this(ticket, currency, false);
    }

    public RegistrationFormPanel(ClientRegistrationPack ticket, String currency, boolean newComponents) {
        this.ticket = ticket;
        this.currency = currency;
        this.total = ticket.getPrice();
        this.newComponents = newComponents;
    }

    /**
     * Nastavi subbalicky. Na zaklade priznaku
     * <code>showDetail</code> autopmaticky generuje detailny prehlad aj so
     * subbalickami. Inak caka na stlacenie ukazania detailov.
     */
    public void setSubbundles(List<ClientRegistrationPack> subbundles, boolean showDetail) {
        this.subbundles = subbundles;
        this.showDetail = showDetail;
        // ak je pripojeny a maju sa ukazat detailne tak sa prevola akcia
        if (isAttached() && showDetail) {
            showDetail();
        }
    }

    /** vraci seznam subbalicku */
    public List<ClientRegistrationPack> getSubbundles() {
        return subbundles;
    }

    /**
     * Vygeneruje zoznam objednanych produktov. AK je priznak
     * <code>onlySubbundles</code> <strong>true<strong> potom hlavny tiket nie
     * je obsahom vysledneho zoznamu.
     *
     * @param onlySubbundles
     * @return
     */
    public List<ProductThumb> getOrderedItems(boolean onlySubbundles) {
        List<ProductThumb> orderedItems = new ArrayList<ProductThumb>();
        if (!onlySubbundles) {
            orderedItems.add(getTicketPanel().getProductThumb());
        }
        for (int i = 0; i < detailPanel.getWidgetCount(); i++) {
            Widget w = detailPanel.getWidget(i);
            if (w instanceof RegistrationFormPanelSubItem) {
                RegistrationFormPanelSubItem item = (RegistrationFormPanelSubItem) w;
                if (item.isSelected()) {
                    orderedItems.add(item.getProductThumb());
                }
            }
        }
        return orderedItems;
    }

    public void showDetail() {
        // ukazeme hlavny tiket
        getTicketPanel().setDetailed(true);

        // generujeme subbalicky - ale len ak je aktivny 
        if (RegistrationFormPanelItem.isActiveBundle(ticket)) {
            if (subbundles == null) {
                loadSubbundlesAndShow();
            } else {
                showDetails(detailPanel);
            }
        }
    }

    public void hideDetail() {
        // skovame veci okolo title
        getTicketPanel().setDetailed(false);
        // skovame subbalicky
        detailPanel.clear();
    }

    /**
     * Generuje subbaliky - ak su a total Panel. Dediace triedy mozu upravit detail
     * panel. 
     * Panel sa generuje pri kazdom zobrazeni detailu registracneho panelu.
     */
    protected void showDetails(FlowPanel toPanel) {
        for (ClientRegistrationPack object : subbundles) {
            RegistrationFormPanelSubItem item = new RegistrationFormPanelSubItem(ticket.getUuidBundle(), object,
                    currency, ticket.getFreeCapacity() != 0, newComponents);
            toPanel.add(item);
        }
        RegistrationFormItemTotalPanel totalPanel = new RegistrationFormItemTotalPanel(ticket.getPrice(), currency);
        toPanel.add(totalPanel);
    }

    private void loadSubbundlesAndShow() {
        EventBus.fire(new EventDispatchEvent(
                new LoadUserRegistrationDetailsAction(ticket.getUuidBundle()),
                new AsyncCallback<ArrayListResult<ClientRegistrationPack>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        subbundles = new ArrayList<ClientRegistrationPack>();
                        showDetails(detailPanel);
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientRegistrationPack> result) {
                        subbundles = result.getArrayListResult();
                        showDetails(detailPanel);
                    }
                }, this));
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        EventBus.get().addHandler(ChangeOrderPriceEvent.TYPE, this);

        setStyleName(ERResourceUtils.bundle().css().registrationOrderPanel());

        FlowPanel wrapper = new FlowPanel();
        wrapper.setStyleName(ERResourceUtils.bundle().css().registrationOrderPanelWrapper());

        // pridame informacie o hlavnom balicku               
        wrapper.add(getTicketPanel());

        // pridamne detail panel
        detailPanel = new FlowPanel();
        wrapper.add(detailPanel);
        wrapper.add(StyleUtils.clearDiv());

        // vygenerujeme informacie o subbalickoch a total panel
        if (showDetail) {
            showDetail();
        }

        add(wrapper);
    }

    @Override
    protected void onUnload() {
        super.onUnload();
        EventBus.get().removeHandler(ChangeOrderPriceEvent.TYPE, this);
    }

    @Override
    public void onPriceChange(ChangeOrderPriceEvent event) {
        if (event.getGroupId().equals(ticket.getUuidBundle())) {
            Monetary newTotal = event.isSubstract() ? total.sub(event.getChange()) : total.add(event.getChange());
            if (!newTotal.equals(total)) {
                total = newTotal;
                EventBus.get().fireEvent(new OrderTotalPriceEvent(total));
            }
        }
    }

    public Monetary getTotal() {
        return total;
    }

    public boolean isForFree() {
        return total.isZero();
    }

    public RegistrationFormPanelItem getTicketPanel() {
        if (ticketPanel == null) {
            ticketPanel = new RegistrationFormPanelItem(ticket, currency, newComponents);
        }
        return ticketPanel;
    }

    public ClientRegistrationPack getTicket() {
        return ticket;
    }
}
