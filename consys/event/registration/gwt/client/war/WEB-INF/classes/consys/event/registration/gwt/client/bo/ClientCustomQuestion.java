package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;

/**
 *
 * @author pepa
 */
public class ClientCustomQuestion implements Result {

    private static final long serialVersionUID = -7179258255745801025L;
    // data
    private String uuid;
    private String text;
    private boolean required;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
