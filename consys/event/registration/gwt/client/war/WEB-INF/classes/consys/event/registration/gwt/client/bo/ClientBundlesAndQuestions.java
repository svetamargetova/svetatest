package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientBundlesAndQuestions implements Result {

    private static final long serialVersionUID = 5743842197427985367L;
    // data
    private ArrayList<ClientRegistrationPack> bundles;
    private ArrayList<ClientCustomQuestion> questions;

    public ClientBundlesAndQuestions() {
        bundles = new ArrayList<ClientRegistrationPack>();
        questions = new ArrayList<ClientCustomQuestion>();
    }

    public ArrayList<ClientRegistrationPack> getBundles() {
        return bundles;
    }

    public ArrayList<ClientCustomQuestion> getQuestions() {
        return questions;
    }
}
