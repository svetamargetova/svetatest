package consys.event.registration.gwt.client.module.coupon;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.UpdateDiscountCouponAction;
import consys.event.registration.gwt.client.bo.ClientCouponListItem;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
public class EditFormDiscountCoupon extends RootPanel {

    // komponenty
    private ConsysStringTextBox capacityBox;
    private ConsysDateBox endDateBox;
    // data
    private ClientCouponListItem item;
    private HashMap<String, String> bundles;

    public EditFormDiscountCoupon(ClientCouponListItem item, HashMap<String, String> bundles) {
        super(ERMessageUtils.c.editFormDiscountCoupon_title());
        this.item = item;
        this.bundles = bundles;
    }

    @Override
    protected void onLoad() {
        clear();
        generateContent();
    }

    private void generateContent() {
        capacityBox = new ConsysStringTextBox(0, 20, ERMessageUtils.c.discountListItem_text_capacity());
        capacityBox.setText(item.getCapacity() == 0
                ? ERMessageUtils.c.discountListItem_text_unlimited()
                : String.valueOf(item.getCapacity()));
        CreateDiscountCouponDialog.addCapacityBoxHandlers(capacityBox);

        endDateBox = ConsysDateBox.getBox(true);
        endDateBox.setShowDateAs(ConsysDateBox.ShowDateAs.EVENT);
        endDateBox.setValue(item.getEndDate());

        ActionImage update = ActionImage.getConfirmButton(UIMessageUtils.c.const_saveUpper());
        update.addClickHandler(updateClickHandler());

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(cancelClickHandler());

        BaseForm form = new BaseForm();
        form.addOptional(0, ERMessageUtils.c.discountListItem_text_ticket(), new Label(bundles.get(item.getBundleUuid())));
        form.addOptional(1, ERMessageUtils.c.discountListItem_text_code(), new Label(item.getKey()));
        form.addOptional(2, ERMessageUtils.c.discountListItem_text_discount(), new Label(item.getDiscount() + " %"));
        form.addOptional(3, ERMessageUtils.c.discountListItem_text_capacity(), capacityBox);
        form.addOptional(4, ERMessageUtils.c.discountListItem_text_validThrough(), endDateBox);
        form.addActionMembers(5, update, cancel);
        addWidget(form);
    }

    private ClickHandler updateClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                int capacity = 0;
                String capacityText = capacityBox.getText();
                try {
                    int number = Integer.parseInt(capacityText);
                    if (number > 0) {
                        capacity = number;
                    }
                } catch (NumberFormatException ex) {
                    // kapacita je nula
                }

                EventBus.get().fireEvent(new EventDispatchEvent(
                        new UpdateDiscountCouponAction(item.getUuid(), capacity, endDateBox.getValue()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava eventactionexecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ERMessageUtils.c.couponListItem_error_noRecords());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, EditFormDiscountCoupon.this));
            }
        };
    }

    private ClickHandler cancelClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                FormUtils.fireBreadcrumbBack();
            }
        };
    }
}
