package consys.event.registration.gwt.client.module.setting.bundle;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction;
import consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction;
import consys.event.registration.gwt.client.action.LoadRegistrationBundleAction;
import consys.event.registration.gwt.client.bo.ClientBundle;
import consys.event.registration.gwt.client.module.coupon.CreateDiscountCouponDialog.RefreshAfterCreateDiscount;
import consys.event.registration.gwt.client.module.exception.BundleUniquityException;
import consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Polozka bundle
 * @author pepa
 */
public class BundleItem extends SimpleFormPanel implements ConsysTabPanelItem<String>, RefreshAfterCreateDiscount {

    // komponenty
    private RegistrationCycleList registrationCycleList;
    private DiscountInfoPanel discountPanel;
    // data
    private ConsysTabPanel<String> tabPanel;
    private ConsysTabPanelLabel tabLabel;
    private ConsysMessage helpMessage;
    private ClientBundle cb;
    private boolean subbundle;

    public BundleItem(ConsysTabPanelLabel<String> tabLabel, ClientTabItem item, ConsysMessage helpMessage, boolean subbundle) {
        this.tabLabel = tabLabel;
        this.helpMessage = helpMessage;
        this.subbundle = subbundle;
    }

    /** nastavi nove hodnoty, jak pro bundle tak pro cykly */
    public void setData(ClientBundle cb) {
        this.cb = cb;
        tabLabel.setText(cb.getTitle());

        initContent();
    }

    private void initContent() {
        clear();

        // ovladaci prvky vpravo nahore
        ActionLabel duplicate = new ActionLabel(UIMessageUtils.c.const_duplicate(), FLOAT_LEFT);
        duplicate.setClickHandler(duplicateClickHandler());

        EditWithRemover ewr = new EditWithRemover(new ConsysAction() {
            @Override
            public void run() {
                EditFormBundle w = new EditFormBundle(cb, BundleItem.this);
                FormUtils.fireNextBreadcrumb(ERMessageUtils.c.editFormBundle_title(), w);
            }
        }, getRemoveAction());
        ewr.addStyleName(FLOAT_LEFT);

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.setStyleName(FLOAT_RIGHT);
        controlPanel.add(duplicate);
        controlPanel.add(ewr);
        controlPanel.add(StyleUtils.clearDiv());

        Label descriptionLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.const_description(), FONT_16PX, FONT_BOLD,
                MARGIN_BOT_10, CLEAR_BOTH);

        Label description = StyleUtils.getStyledLabel(FormUtils.valueOrDash(cb.getDescription()), MARGIN_BOT_20);

        if (registrationCycleList == null) {
            registrationCycleList = new RegistrationCycleList(cb.getUuid(), cb.getCycles());
            registrationCycleList.setCycles(cb.getCycles());
        }

        discountPanel = new DiscountInfoPanel(this, this);
        discountPanel.addStyleName(MARGIN_VER_10);
        discountPanel.refresh(cb);

        FlowPanel wrapper = new FlowPanel();
        wrapper.setStyleName(MARGIN_VER_10);
        wrapper.addStyleName(MARGIN_HOR_10);
        wrapper.add(controlPanel);
        wrapper.add(descriptionLabel);
        wrapper.add(description);
        wrapper.add(checkPanel());
        wrapper.add(new Separator());
        wrapper.add(discountPanel);
        wrapper.add(new Separator());
        wrapper.add(registrationCycleList);

        addWidget(wrapper);
    }

    private ClickHandler duplicateClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                int counter = 1;
                String title = "";
                do {
                    title = cb.getTitle() + " - " + UIMessageUtils.c.const_copy() + counter;
                    counter++;
                } while (existTabName(title));
                EventDispatchEvent duplicateEvent = new EventDispatchEvent(
                        new DuplicateRegistrationBundleAction(cb.getUuid(), title),
                        new AsyncCallback<ClientTabItem>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof BundleUniquityException) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionTicketUniquity());
                                }
                            }

                            @Override
                            public void onSuccess(ClientTabItem result) {
                                addToTab(tabPanel, result, helpMessage, subbundle);
                                tabPanel.selectTabById(result.getUuid());
                            }
                        }, BundleItem.this);
                EventBus.get().fireEvent(duplicateEvent);
            }
        };
    }

    private FlowPanel checkPanel() {
        FlowPanel checkVatPanel = new FlowPanel();
        checkVatPanel.setWidth("200px");
        checkVatPanel.setStyleName(FLOAT_LEFT);
        checkVatPanel.addStyleName(MARGIN_BOT_10);
        checkVatPanel.add(new HTML("<b>" + ERMessageUtils.c.subbundleItem_text_vat() + "</b>: "
                + FormUtils.doubleFormat.format(cb.getVat()) + " %"));

        FlowPanel checkCapacityPanel = new FlowPanel();
        checkCapacityPanel.setWidth("200px");
        checkCapacityPanel.setStyleName(FLOAT_LEFT);
        checkCapacityPanel.addStyleName(MARGIN_BOT_10);
        checkCapacityPanel.add(new HTML("<b>" + ERMessageUtils.c.editFormBundle_form_capacity() + "</b>: "
                + (cb.getCapacity() == 0 ? UIMessageUtils.c.const_unlimited() : cb.getCapacity())));

        FlowPanel quantityPanel = new FlowPanel();
        quantityPanel.setWidth("200px");
        quantityPanel.setStyleName(FLOAT_LEFT);
        quantityPanel.addStyleName(MARGIN_BOT_10);
        quantityPanel.add(new HTML("<b>" + ERMessageUtils.c.editFormBundle_form_maxQuantity() + "</b>: " + cb.getQuantity()));

        FlowPanel checkPanel = new FlowPanel();
        checkPanel.setHeight("30px");
        checkPanel.add(checkVatPanel);
        checkPanel.add(checkCapacityPanel);
        checkPanel.add(quantityPanel);
        checkPanel.add(StyleUtils.clearDiv());

        return checkPanel;
    }

    /** akce pro odebrani baliku */
    private ConsysAction getRemoveAction() {
        return new ConsysAction() {
            @Override
            public void run() {
                EventDispatchEvent deleteEvent = new EventDispatchEvent(
                        new DeleteRegistrationBundleAction(cb.getUuid()),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof RegistrationsInCycleException) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionRegistrationsInCycle());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                tabPanel.removeTab(BundleItem.this);
                                if (tabPanel.getTabCount() == 0) {
                                    tabPanel.setVisible(false);
                                    helpMessage.setVisible(true);
                                }
                            }
                        }, BundleItem.this);
                EventBus.get().fireEvent(deleteEvent);
            }
        };
    }

    /** prida do novou zalozku s akci pro dotazeni obsahu podle uuid */
    public static void addToTab(ConsysTabPanel<String> tabs, final ClientTabItem item, ConsysMessage helpMessage, boolean subbundle) {
        ConsysTabPanelLabel<String> tl = new ConsysTabPanelLabel<String>(item.getUuid(), item.getTitle());
        final BundleItem bundleItem = new BundleItem(tl, item, helpMessage, subbundle);
        ConsysAction action = new ConsysAction() {
            @Override
            public void run() {
                EventBus.get().fireEvent(new EventDispatchEvent(new LoadRegistrationBundleAction(item.getUuid()),
                        new AsyncCallback<ClientBundle>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof NoRecordsForAction) {
                                    bundleItem.getFailMessage().setText("NoRecordsForAction");
                                }
                            }

                            @Override
                            public void onSuccess(ClientBundle result) {
                                bundleItem.setData(result);
                            }
                        }, bundleItem));
            }
        };
        tabs.addTabItem(tl, bundleItem, action);
    }

    /** vraci true pokud takovy nazev uz v zalozkach existuje */
    private boolean existTabName(String title) {
        boolean exists = false;
        for (int i = 0; i < tabPanel.getTabCount(); i++) {
            String s = tabPanel.getTabName(i);
            if (s.equals(title)) {
                return true;
            }
        }
        return exists;
    }

    @Override
    public Widget getTitleNote() {
        return null;
    }

    @Override
    public Widget getContent() {
        return this;
    }

    @Override
    public void setConsysTabPanel(ConsysTabPanel<String> tabPanel) {
        this.tabPanel = tabPanel;
    }

    @Override
    public void refreshDiscount() {
        cb.setDiscountCodes(cb.getDiscountCodes() + 1);
        discountPanel.refresh(cb);
    }
}
