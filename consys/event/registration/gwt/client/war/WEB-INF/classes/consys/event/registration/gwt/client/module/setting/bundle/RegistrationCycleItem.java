package consys.event.registration.gwt.client.module.setting.bundle;

import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.RollOutPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.action.DeleteRegistrationCycleAction;
import consys.event.registration.gwt.client.action.LoadRegistrationCycleAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Polozka registracniho cyklu
 * @author pepa
 */
public class RegistrationCycleItem extends FlowPanel implements ActionExecutionDelegate {

    // konstanty
    public static final int WIDTH = LayoutManager.LAYOUT_CONTENT_WIDTH_INT - 2 - 20; // -ramecek -pravy a levy margin
    private static final String MIDDLE = FormUtils.UNB_SPACE + FormUtils.NDASH + FormUtils.UNB_SPACE;
    private static final String MIDDLE_PRICE = FormUtils.UNB_SPACE + FormUtils.UNB_SPACE + "/" + FormUtils.UNB_SPACE + FormUtils.UNB_SPACE;
    // komponenty
    private Label titleLabel;
    private FlowPanel failPanel;
    // data
    private ClientRegistrationCycle crc;
    private RegistrationCycleList parent;

    public RegistrationCycleItem(ClientRegistrationCycle crc, RegistrationCycleList parent) {
        super();
        this.crc = crc;
        this.parent = parent;
        setStyleName(StyleUtils.MARGIN_TOP_10);

        titleLabel = StyleUtils.getStyledLabel(getTitleLabel(), RollOutPanel.TITLE_TEXT_STYLE);

        EditWithRemover ewr = new EditWithRemover(editAction(), removeAction());
        ewr.atRight();

        failPanel = new FlowPanel();

        FlowPanel titlePanel = new FlowPanel();
        titlePanel.addStyleName(StyleUtils.FLOAT_LEFT);
        titlePanel.add(titleLabel);
        titlePanel.add(failPanel);

        FlowPanel content = new FlowPanel();
        content.setWidth(WIDTH + "px");
        content.add(ewr);
        content.add(titlePanel);
        content.add(StyleUtils.clearDiv());

        add(content);
    }

    /** zobrazi editacni formular */
    private void edit(ClientRegistrationCycle crc) {
        EditFormRegistrationCycle w = new EditFormRegistrationCycle(crc, this, parent);
        FormUtils.fireNextBreadcrumb(ERMessageUtils.c.editFormRegistrationCycle_title(), w);
    }

    /** zaktualizuje data polozky, ale prekresli se vlastni data, ale cykly se neprekresli */
    public void refreshClienRegistrationCycleItem(ClientRegistrationCycle crc) {
        this.crc = crc;
        titleLabel.setText(getTitleLabel());
    }

    /** vraci thumb polozky v novem objektu */
    public ClientRegistrationCycle getCycle() {
        ClientRegistrationCycle item = new ClientRegistrationCycle();
        item.setUuid(crc.getUuid());
        item.setFrom(crc.getFrom());
        item.setTo(crc.getTo());
        item.setValue(crc.getValue());
        return item;
    }

    /** vygeneruje text rozsahu z datumu a za to cenu */
    private String getTitleLabel() {
        TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
        boolean sameYear = DateTimeUtils.getYear(crc.getFrom()) == DateTimeUtils.getYear(crc.getTo());
        boolean sameDay = false;
        StringBuilder sb = new StringBuilder();

        if (sameYear) {
            // pokud je ve stejnem roce, zjistime jestli se nahodou nejedna o stejny den
            sameDay = DateTimeUtils.getDate(crc.getFrom(), tz).equals(DateTimeUtils.getDate(crc.getTo(), tz));
        }

        if (sameYear && sameDay) {
            // v jeden den
            sb.append(DateTimeUtils.getMonthDayTime(crc.getFrom(), crc.getTo(), tz));
        } else if (sameYear) {
            // v jednom roce
            sb.append(DateTimeUtils.getMonthDayTime(crc.getFrom(), tz));
            sb.append(MIDDLE);
            sb.append(DateTimeUtils.getMonthDayTime(crc.getTo(), tz));
        } else {
            // v jiných rocích
            sb.append(DateTimeUtils.getMonthDayYearTime(crc.getFrom(), tz));
            sb.append(MIDDLE);
            sb.append(DateTimeUtils.getMonthDayYearTime(crc.getTo(), tz));
        }

        sb.append(MIDDLE_PRICE);
        Monetary price = crc.getValue();
        sb.append(UIMessageUtils.assignCurrency(ECoMessageUtils.getCurrency(), price == null ? Monetary.ZERO : price));
        return sb.toString();
    }

    /** vraci nezobrazenou chybovou zpravu */
    private ConsysMessage getFailMessage() {
        return FormUtils.failMessage(failPanel, StyleUtils.MARGIN_TOP_10);
    }

    @Override
    public void actionStarted() {
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    private ConsysAction editAction() {
        return new ConsysAction() {

            @Override
            public void run() {
                if (RegistrationCycleItem.this.crc instanceof ClientRegistrationCycle) {
                    edit((ClientRegistrationCycle) RegistrationCycleItem.this.crc);
                } else {
                    EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(
                            new LoadRegistrationCycleAction(RegistrationCycleItem.this.crc.getUuid()),
                            new AsyncCallback<ClientRegistrationCycle>() {

                                @Override
                                public void onFailure(Throwable caught) {
                                    // obecne chyby zpracovava EventActionExcecutor
                                    if (caught instanceof NoRecordsForAction) {
                                        getFailMessage().setText("NoRecordsForAction");
                                    }
                                }

                                @Override
                                public void onSuccess(ClientRegistrationCycle result) {
                                    RegistrationCycleItem.this.crc = result;
                                    edit(result);
                                }
                            }, RegistrationCycleItem.this);
                    EventBus.get().fireEvent(loadSettingsEvent);
                }
            }
        };
    }

    private ConsysAction removeAction() {
        return new ConsysAction() {

            @Override
            public void run() {
                EventDispatchEvent deleteEvent = new EventDispatchEvent(
                        new DeleteRegistrationCycleAction(RegistrationCycleItem.this.crc.getUuid()),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof RegistrationsInCycleException) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionRegistrationsInCycle());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                RegistrationCycleItem.this.removeFromParent();
                            }
                        }, RegistrationCycleItem.this);
                EventBus.get().fireEvent(deleteEvent);
            }
        };
    }
}
