package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.Monetary;
import java.util.Date;
import java.util.HashMap;

/**
 * Klientsky objekt popisujuci balicek ktery si moze uzivatel vybrat. Je to
 * samostany objekt lebo tento objekt bude prenasany hodne.
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ClientRegistrationPack implements Result {

    private static final long serialVersionUID = -4801116660789989139L;
    /** Uuid Bundlu */
    private String uuidBundle;
    /** Uuid Cyklu */
    private String uuidCycle;
    /** Title bundlu */
    private String bundleTitle;
    /** Popis bundlu */
    private String bundleDescription;
    /** Plati do */
    private Date to;
    /** Hodnota */
    private Monetary price;
    /** Je umoznene vlozit kod */
    private boolean withCode;
    /** Je kapacitne omezeny ak > 0*/
    private int capacity = 0;
    /** Uz registrovanych do bundlu  */
    private int registred;
    /** seznam cen v case */
    private HashMap<Date, Monetary> pricing;
    /** Pokud je moznost zakoupit vic tiketu > 1 */
    private int maxQuantity = 1;

    public ClientRegistrationPack() {
        pricing = new HashMap<Date, Monetary>();
    }

    /**
     * Title bundlu
     * @return the bundleTitle
     */
    public String getBundleTitle() {
        return bundleTitle;
    }

    /**
     * Title bundlu
     * @param bundleTitle the bundleTitle to set
     */
    public void setBundleTitle(String bundleTitle) {
        this.bundleTitle = bundleTitle;
    }

    /**
     * Popis bundlu
     * @return the bundleDescription
     */
    public String getBundleDescription() {
        return bundleDescription;
    }

    /**
     * Popis bundlu
     * @param bundleDescription the bundleDescription to set
     */
    public void setBundleDescription(String bundleDescription) {
        this.bundleDescription = bundleDescription;
    }

    /**
     * Plati do
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * Plati do
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * Hodnota
     * @return the price
     */
    public Monetary getPrice() {
        return price;
    }

    /**
     * Hodnota
     * @param price the price to set
     */
    public void setPrice(Monetary price) {
        this.price = price;
    }

    /**
     * Je umoznene vlozit kod
     * @return the withCode
     */
    public boolean isWithCode() {
        return withCode;
    }

    /**
     * Je umoznene vlozit kod
     * @param withCode the withCode to set
     */
    public void setWithCode(boolean withCode) {
        this.withCode = withCode;
    }

    /**
     * Je kapacitne omezeny ak > 0
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Je kapacitne omezeny ak > 0
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFreeCapacity() {
        return getCapacity() == 0 ? -1 : (getCapacity() - getRegistred()) <= 0 ? 0 : getCapacity() - getRegistred();
    }

    /**
     * Uz registrovanych do bundlu
     * @return the registred
     */
    public int getRegistred() {
        return registred;
    }

    /**
     * Uz registrovanych do bundlu
     * @param registred the registred to set
     */
    public void setRegistred(int registred) {
        this.registred = registred;
    }

    public boolean isUnlimitedCapacity() {
        return capacity == 0;
    }

    /**
     * @return the uuidBundle
     */
    public String getUuidBundle() {
        return uuidBundle;
    }

    /**
     * @param uuidBundle the uuidBundle to set
     */
    public void setUuidBundle(String uuidBundle) {
        this.uuidBundle = uuidBundle;
    }

    /**
     * @return the uuidCycle
     */
    public String getUuidCycle() {
        return uuidCycle;
    }

    /**
     * @param uuidCycle the uuidCycle to set
     */
    public void setUuidCycle(String uuidCycle) {
        this.uuidCycle = uuidCycle;
    }

    public HashMap<Date, Monetary> getPricing() {
        return pricing;
    }

    public void setPricing(HashMap<Date, Monetary> pricing) {
        this.pricing = pricing;
    }

    /**
     * Pokud je moznost zakoupit vic tiketu > 1
     * @return the quantity
     */
    public int getMaxQuantity() {
        return maxQuantity;
    }

    /**
     * Pokud je moznost zakoupit vic tiketu > 1
     * @param quantity the quantity to set
     */
    public void setMaxQuantity(int maxQuantity) {
        if (maxQuantity < 0) {
            this.maxQuantity = 1;
        } else {
            this.maxQuantity = maxQuantity;
        }
    }

    /** pokud je povoleno nakupovat ve vetsi kvantite vraci true */
    public boolean isQuantity() {
        return maxQuantity > 1;
    }
}
