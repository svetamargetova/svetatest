package consys.event.registration.gwt.client.module.setting;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.base.TitlePanel;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.PropertyPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.api.properties.RegistrationProperties;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class RegistrationPropertyPanel extends PropertyPanel implements TitlePanel {

    // komponenty
    private HorizontalPanel controllPart;
    private Label titleLabel;

    public RegistrationPropertyPanel() {
        titleLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.const_property(), FONT_19PX, FONT_BOLD, MARGIN_RIGHT_10);

        ActionImage backButton = ActionImage.getBackButton(ERMessageUtils.c.registrationSettingsBundle_title(), true);
        backButton.addStyleName(MARGIN_LEFT_20);
        backButton.addClickHandler(FormUtils.breadcrumbBackClickHandler());

        controllPart = new HorizontalPanel();
        controllPart.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
        controllPart.add(titleLabel);
        controllPart.add(backButton);
    }

    @Override
    protected void onLoad() {
        clear();
        addWidget(controllPart);
        addWidget(Separator.addedStyle(MARGIN_VER_10));

        try {
            String value = (String) Cache.get().getSafe(RegistrationProperties.PARTICIPANT_LIST_VISIBILE);

            ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
            items.add(new SelectBoxItem<String>("true", UIMessageUtils.c.const_yes()));
            items.add(new SelectBoxItem<String>("false", UIMessageUtils.c.const_no()));

            addProperty(RegistrationProperties.PARTICIPANT_LIST_VISIBILE,
                    ERMessageUtils.c.registrationPropertyPanel_property_visibleParticipants(),
                    items, value, "", "280px");
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(RegistrationProperties.PARTICIPANT_LIST_VISIBILE));
        }

        /*try {
            String value = (String) Cache.get().getSafe(RegistrationProperties.REGISTER_ANOTHER);

            ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
            items.add(new SelectBoxItem<String>("true", UIMessageUtils.c.const_yes()));
            items.add(new SelectBoxItem<String>("false", UIMessageUtils.c.const_no()));

            addProperty(RegistrationProperties.REGISTER_ANOTHER,
                    ERMessageUtils.c.registrationPropertyPanel_property_allowRegisterAnotherParticipant(),
                    items, value, "", "280px");
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(RegistrationProperties.REGISTER_ANOTHER));
        }*/
    }

    @Override
    public String getPanelTitle() {
        return UIMessageUtils.c.const_property();
    }
}
