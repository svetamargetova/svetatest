package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.registration.gwt.client.action.UpdateRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientAnswer;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.ArrayList;

/**
 * Odpovedi na otazky organizatora a prani uzivatele
 * @author pepa
 */
public class UserAnswersAndWishs extends FlowPanel implements ActionExecutionDelegate {

    // komponenty
    private FlowPanel failPanel;
    // data
    private ClientRegistration registration;
    private boolean asAdmin;

    public UserAnswersAndWishs(ClientRegistration registration) {
        this(registration, false);
    }

    public UserAnswersAndWishs(ClientRegistration registration, boolean asAdmin) {
        this.registration = registration;
        this.asAdmin = asAdmin;
        setStyleName(ERResourceUtils.css().userAnswersAndWish());
    }

    @Override
    protected void onLoad() {
        clear();
        generate();
    }

    private void generate() {
        failPanel = new FlowPanel();

        FlowPanel leftCol = new FlowPanel();
        leftCol.setStyleName(ResourceUtils.common().css().leftColumn());
        if (asAdmin) {
            leftCol.addStyleName(ERResourceUtils.css().leftColumnAdmin());
            leftCol.add(userAnswers());
        } else {
            leftCol.add(userWishs());
        }

        FlowPanel rightCol = new FlowPanel();
        rightCol.setStyleName(ResourceUtils.common().css().rightColumn());
        if (asAdmin) {
            rightCol.addStyleName(ERResourceUtils.css().rightColumnAdmin());
        } else {
            rightCol.add(userAnswers());
        }

        add(StyleUtils.getPadder());
        add(new Separator());
        add(failPanel);
        add(rightCol);
        add(leftCol);
        add(StyleUtils.clearDiv());
    }

    /** vykresli odpovedi na otazky */
    private FlowPanel userAnswers() {
        FlowPanel fp = new FlowPanel();
        ArrayList<ClientAnswer> answers = registration.getAnswers();
        if (answers != null && answers.size() > 0) {
            for (ClientAnswer ca : answers) {
                HTML html = new HTML("<b>" + ca.getQuestion() + "</b><br/>" + ca.getAnswer());
                html.setStyleName(StyleUtils.MARGIN_TOP_10);
                fp.add(html);
            }
        }
        return fp;
    }

    private TextAreaUpdateBox userWishs() {
        final TextAreaUpdateBox updateSpecialWish = new TextAreaUpdateBox(ERMessageUtils.c.editRegistrationForm_text_special_wish(),
                ERMessageUtils.c.editRegistrationForm_text_special_wish_placeholder(), registration.getSpecialWish());
        updateSpecialWish.setHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EventBus.fire(new EventDispatchEvent(
                        new UpdateRegistrationAction(registration.getRegistrationUuid(), updateSpecialWish.getText().trim(), false),
                        new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        updateSpecialWish.onFailure();
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        updateSpecialWish.onSuccess();
                    }
                }, null));
            }
        });
        return updateSpecialWish;
    }

    @Override
    public void actionStarted() {
    }

    @Override
    public void actionEnds() {
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    private ConsysMessage getFailMessage() {
        return FormUtils.failMessage(failPanel, StyleUtils.MARGIN_TOP_10);
    }
}
