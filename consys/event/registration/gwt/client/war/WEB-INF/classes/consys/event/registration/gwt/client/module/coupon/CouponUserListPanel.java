package consys.event.registration.gwt.client.module.coupon;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.panel.ListPanel;
import consys.common.gwt.client.ui.comp.user.list.UserListItem;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.module.register.AdminRegistrationOverview;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class CouponUserListPanel extends ListPanel<CouponUserListItem> {

    // konstanty
    private static final int VISIBLE_ITEMS = 5;
    // data
    private CouponListItem.KeyUserDialog dialog;

    public CouponUserListPanel(CouponListItem.KeyUserDialog dialog) {
        super(UserListItem.HEIGHT, VISIBLE_ITEMS);
        setWidth("350px");
        this.dialog = dialog;
    }

    public void setUsers(ArrayList<EventUser> users) {
        ConsysActionWithValue<String> toRegistration = new ConsysActionWithValue<String>() {
            @Override
            public void run(String data) {
                loadRegistration(data);
            }
        };
        for (EventUser u : users) {
            addItem(new CouponUserListItem(u, toRegistration));
        }
    }

    private void loadRegistration(String userUuid) {
        EventBus.get().fireEvent(new EventDispatchEvent(
                new LoadEditRegistrationByUserUuidAction(userUuid),
                new AsyncCallback<ClientRegistration>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientRegistration result) {
                        fireNextBreadcrumb(result);
                        dialog.hide();
                    }
                }, dialog));
    }

    private void fireNextBreadcrumb(ClientRegistration data) {
        AdminRegistrationOverview w = new AdminRegistrationOverview(data);
        String title = RegistrationForm.anotherRegistrationBreadcrumb(data.getEventUser().getFullName());
        FormUtils.fireNextBreadcrumb(title, w);
    }
}
