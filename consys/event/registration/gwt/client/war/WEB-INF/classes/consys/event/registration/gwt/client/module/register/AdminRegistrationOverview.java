package consys.event.registration.gwt.client.module.register;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConfirmDialog;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.CancelRegistrationAction;
import consys.event.registration.gwt.client.action.LoadEditRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.event.OrderCanceledEvent;
import consys.event.registration.gwt.client.event.OrderConfirmedEvent;
import consys.event.registration.gwt.client.module.register.item.ConfirmOrderDialog;
import consys.event.registration.gwt.client.module.register.item.RegistrationOrderPanel;
import consys.event.registration.gwt.client.module.register.item.RegistrationParticipantInfoPanel;
import consys.event.registration.gwt.client.module.register.item.UserAnswersAndWishs;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class AdminRegistrationOverview extends RootPanel implements OrderCanceledEvent.Handler, OrderConfirmedEvent.Handler {

    // data
    private String registrationUuid;
    private ClientRegistration registration;
    private FlowPanel orderPanel;
    private boolean autoconfirm = false;

    public AdminRegistrationOverview(String registrationUuid) {
        super(ERMessageUtils.c.registrationForm_title());
        this.registrationUuid = registrationUuid;
    }

    public AdminRegistrationOverview(ClientRegistration registration) {
        super(ERMessageUtils.c.registrationForm_title());
        this.registration = registration;
        this.registrationUuid = registration.getRegistrationUuid();
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(OrderCanceledEvent.TYPE, this);
        EventBus.get().addHandler(OrderConfirmedEvent.TYPE, this);
        if (registration == null) {
            if (registrationUuid == null) {
                throw new NullPointerException("Missing registration UUID");
            }
            EventDispatchEvent loadEvent = new EventDispatchEvent(new LoadEditRegistrationAction(registrationUuid),
                    new AsyncCallback<ClientRegistration>() {
                @Override
                public void onFailure(Throwable caught) {
                    // obecne chyby zpracovava eventactionexecutor
                }

                @Override
                public void onSuccess(ClientRegistration result) {
                    registration = result;

                    generateContent();
                    if (autoconfirm) {
                        ClientRegistrationOrder order = registration.getRegistrationOrders().get(0);
                        new ConfirmOrderDialog(order.getUuid()).showCentered();
                    }
                }
            }, this);
            EventBus.fire(loadEvent);
        } else {
            generateContent();
        }
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(OrderCanceledEvent.TYPE, this);
        EventBus.get().removeHandler(OrderConfirmedEvent.TYPE, this);
    }

    /** spusti po zobrazeni automaticky potvrzeni hlavni objednavky */
    public void autoconfirm() {
        autoconfirm = true;
    }

    @Override
    public void onOrderCanceled(OrderCanceledEvent event) {
        ClientRegistrationOrder canceledOrder = event.getOrder();
        for (int i = 0; i < orderPanel.getWidgetCount(); i++) {
            Widget w = orderPanel.getWidget(i);
            if (w instanceof RegistrationOrderPanel) {
                RegistrationOrderPanel r = (RegistrationOrderPanel) w;
                if (r.getOrderUuid().equals(canceledOrder.getUuid())) {
                    r.removeFromParent();
                    break;
                }
            }
        }
        registration.getRegistrationOrders().remove(canceledOrder);
    }

    @Override
    public void onOrderConfirmed(OrderConfirmedEvent event) {
        for (ClientRegistrationOrder order : registration.getRegistrationOrders()) {
            if (order.getUuid().equals(event.getOrderUuid())) {
                order.setState(ClientRegistration.State.CONFIRMED);
                break;
            }
        }
        generateContent();
    }

    private boolean isOverallPriceZero() {
        boolean canBe = true;
        for (ClientRegistrationOrder order : registration.getRegistrationOrders()) {
            canBe &= order.getTotalPrice().isZero();
        }
        return canBe;
    }

    private void generateContent() {
        final boolean isPaid = isRegistrationValidated();
        clear();

        // Participant info panel - admin mode
        RegistrationParticipantInfoPanel info = new RegistrationParticipantInfoPanel(registration);
        info.setAdministratorMode(true);

        addWidget(info);
        addWidget(StyleUtils.clearDiv());

        // Objednavky
        orderPanel = new FlowPanel();
        for (int i = 1; i < registration.getRegistrationOrders().size(); i++) {
            ClientRegistrationOrder cro = registration.getRegistrationOrders().get(i);
            orderPanel.add(new RegistrationOrderPanel(cro, registration.getEventUser(), true));
        }

        // vyskladani formu
        addWidget(StyleUtils.clearDiv());
        addWidget(orderPanel);

        // Cancel button len ak neni este hlavna registracia zaplatena        
        boolean addCancelButton = false;

        if (isOverallPriceZero()) {
            addCancelButton = true;
        } else if (!isPaid) {
            addCancelButton = true;
        }

        if (addCancelButton) {
            clearControll();
            ActionImage cancel = ActionImage.getCrossButton(ERMessageUtils.c.registrationFormRegistered_button_cancelRegistration(), true);
            cancel.addClickHandler(cancelClickHandler());
            addRightControll(cancel);
        }

        addWidget(new UserAnswersAndWishs(registration, true));
    }

    private boolean isRegistrationValidated() {
        return registration.getRegistrationOrders().get(0).getState().equals(ClientRegistration.State.CONFIRMED);
    }

    /** clickhandler pro zruseni registrace */
    private ClickHandler cancelClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                ActionImage button = ActionImage.getCrossButton(ERMessageUtils.c.registrationFormRegistered_button_cancelRegistration(), false);
                final ConfirmDialog dialog = new ConfirmDialog(
                        button, ERMessageUtils.c.registrationFormRegistered_cancel_registration_question_title());
                ConsysAction action = new ConsysAction() {
                    @Override
                    public void run() {
                        EventDispatchEvent cancelEvent = new EventDispatchEvent(
                                new CancelRegistrationAction(registration.getRegistrationOrders().get(0).getUuid(), false),
                                new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne vyjimky zpracovava EventActionExecuter
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionCancelRegistrationNoRecords());
                                }
                                dialog.hide();
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                FormUtils.fireBreadcrumbBack();
                                dialog.hide();
                            }
                        }, AdminRegistrationOverview.this);
                        EventBus.fire(cancelEvent);
                    }
                };
                dialog.setAction(action);
                dialog.showCentered();
            }
        };
    }
}
