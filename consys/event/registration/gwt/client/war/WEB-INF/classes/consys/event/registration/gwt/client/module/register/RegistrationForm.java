package consys.event.registration.gwt.client.module.register;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.progress.Progress;
import consys.common.gwt.client.ui.comp.progress.ProgressItem;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.bo.EventUser;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.action.LoadUserRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.bo.ClientRegistrationResult;
import consys.event.registration.gwt.client.event.NewOrderEvent;
import consys.event.registration.gwt.client.module.register.item.RegistrationOption;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Formular s vyberem balicku
 *
 * @author pepa
 */
public class RegistrationForm extends RootPanel {

    public static final String RADIO_USER_REGISTRATION = "radio_user_registration";
    // komponenty
    private ConsysFlowPanel itemsPanel;
    // data
    private final String bundleUuid;
    private final String messageText;
    private final String newOrderEventToken;
    private EventUser managedUser;

    public RegistrationForm(String bundleUuid) {
        this(bundleUuid, null, null);
    }

    /**
     * Vytvori novy formular k registracii uzivatela do eventu. V pripade ze je
     * uzivatel uz zaregistrovany tak generuje formular prehladu registracie
     * {@link UserRegistrationOverview}.
     * <p/>
     * <p/>
     * <p/>
     * @param bundleUuid predvybrany bundle uuid
     * @param messageText text ktory sa nastavi ako success message s priznakom html=true
     * @param newOrderEventToken token ktory sa vlozi do event {@link NewOrderEvent} po uspesnom vytvoreni objednakvky
     */
    public RegistrationForm(String bundleUuid, String messageText, String newOrderEventToken) {
        super(ERMessageUtils.c.registrationForm_title());

        this.bundleUuid = bundleUuid;
        this.messageText = messageText;
        this.newOrderEventToken = newOrderEventToken;

        itemsPanel = new ConsysFlowPanel();

        addWidget(getProgress());
        addWidget(itemsPanel);
    }

    public void setManagedUser(EventUser managedUser) {
        this.managedUser = managedUser;
    }

    @Override
    protected void onLoad() {
        itemsPanel.setStyleName(ERResourceUtils.bundle().css().registrationForm());

        itemsPanel.clear();
        itemsPanel.add(Separator.separator());

        LoadUserRegistrationAction action;
        if (managedUser == null) {
            action = new LoadUserRegistrationAction();
        } else {
            action = new LoadUserRegistrationAction(managedUser.getUuid());
            setTitleText(anotherRegistrationBreadcrumb(managedUser.getFullName()));
        }

        EventBus.fire(new EventDispatchEvent(action,
                new AsyncCallback<ClientRegistrationResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                        if (caught instanceof PermissionException) {
                            getFailMessage().addOrSetText(UIMessageUtils.c.const_error_noPermissionForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientRegistrationResult result) {
                        ClientRegistration reg = result.getRegistration();
                        if (reg != null) {
                            if (ClientRegistration.State.WAITING.equals(reg.getState())) {
                                showWaitingOverview(reg);
                            } else {
                                showRegistrationOverview(reg);
                            }
                        } else {
                            generateTicketsOffer(result.getBundles());
                        }
                    }
                }, itemsPanel));
    }

    /**
     * vraci progress pro prvni krok
     */
    private Progress getProgress() {
        Progress progress = new Progress();
        progress.addStyleName(MARGIN_TOP_10);
        progress.addStyleName(MARGIN_BOT_20);

        progress.addProgressItem(new ProgressItem(true, 1, ERMessageUtils.c.registrationForm_progress_registerUpper()));
        progress.addSeparator();
        progress.addProgressItem(new ProgressItem(false, 2, ERMessageUtils.c.registrationForm_progress_payUpper()));
        progress.addSeparator();
        progress.addProgressItem(new ProgressItem(false, 3, ERMessageUtils.c.registrationForm_progress_enjoyUpper()));

        return progress;
    }

    /**
     * vygeneruje a vlozi polozky pro jednotlive vstupenky
     */
    private void generateTicketsOffer(ArrayList<ClientRegistrationPack> tickets) {
        itemsPanel.clear();
        if (tickets.isEmpty()) {
            itemsPanel.add(Separator.separator());
            itemsPanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationForm_text_noneTickets(),
                    StyleUtils.FONT_BOLD, StyleUtils.MARGIN_TOP_10));
        } else {
            sortBundles(tickets);
            boolean selected = false;
            for (ClientRegistrationPack pack : tickets) {
                RegistrationOption item = new RegistrationOption(pack, ECoMessageUtils.getCurrency(), this, newOrderEventToken);
                item.setManagedUser(managedUser);
                itemsPanel.add(Separator.separator());
                itemsPanel.add(item);
                if (StringUtils.equals(bundleUuid, pack.getUuidBundle())) {
                    item.selected();
                    selected = true;
                }
            }
            // vycpavka kvuli ie, aby spravne prekreslil styly, kdyz je vlozen jen jeden RegistrationOption
            itemsPanel.add(StyleUtils.getStyledLabel(".", StyleUtils.TEXT_WHITE));
            if (!selected) {
                for (int i = 0; i < itemsPanel.getWidgetCount(); i++) {
                    Widget w = itemsPanel.getWidget(i);
                    if (w instanceof RegistrationOption) {
                        ((RegistrationOption) w).selected();
                        break;
                    }
                }
            }
        }
    }

    /** serazeni balicku (neaktivni dolu) */
    public static void sortBundles(ArrayList<ClientRegistrationPack> list) {
        Collections.sort(list, new Comparator<ClientRegistrationPack>() {
            @Override
            public int compare(ClientRegistrationPack o1, ClientRegistrationPack o2) {
                boolean active1 = o1.getTo() != null && o1.getPrice() != null;
                boolean active2 = o2.getTo() != null && o2.getPrice() != null;

                int r1 = active1 ? 0 : -1;
                int r2 = active2 ? 0 : -1;

                return -(r1 - r2);
            }
        });
    }

    /**
     * vola se jako akce listeneru z nastavenych polozek
     */
    public void selectItem(RegistrationOption item) {
        for (int i = 0; i < itemsPanel.getWidgetCount(); i++) {
            Widget w = itemsPanel.getWidget(i);
            if (w instanceof RegistrationOption && !w.equals(item)) {
                ((RegistrationOption) w).unselected();
            }
        }
    }

    private void showWaitingOverview(ClientRegistration registration) {
        WaitingOverview w = new WaitingOverview(registration);
        FormUtils.fireSameBreadcrumb(ERMessageUtils.c.waitingOverview_title(), w);
    }

    public void showRegistrationOverview(ClientRegistration registration) {
        boolean anotherUser = managedUser != null;
        UserRegistrationOverview w = new UserRegistrationOverview(registration, anotherUser);
        w.setNewOderEventToken(newOrderEventToken);
        if (StringUtils.isNotBlank(messageText)) {
            w.getSuccessMessage().setText(messageText, true);
        }
        FormUtils.fireSameBreadcrumb(anotherUser
                ? anotherRegistrationBreadcrumb(managedUser.getFullName())
                : ERMessageUtils.c.registrationModule_text_navigationModelMyRegistration(), w);
    }

    /** generuje nazev drobeckove navigace pro jineho uzivatele */
    public static String anotherRegistrationBreadcrumb(String userName) {
        return ERMessageUtils.c.registrationForm_title() + " - " + userName;
    }
}
