package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientCreateBundle;
import java.util.ArrayList;

/**
 * Vytvori registracni bundly
 * @author pepa
 */
public class CreateRegistrationBundleAction extends EventAction<ArrayListResult<String>> {

    private static final long serialVersionUID = -1270460205595874701L;
    // data
    private ArrayList<ClientCreateBundle> bundles;
    private boolean subbundle;

    public CreateRegistrationBundleAction() {
    }

    public CreateRegistrationBundleAction(ArrayList<ClientCreateBundle> bundles, boolean subbundle) {
        this.bundles = bundles;
        this.subbundle = subbundle;
    }

    public ArrayList<ClientCreateBundle> getBundles() {
        return bundles;
    }

    public boolean isSubbundle() {
        return subbundle;
    }
}
