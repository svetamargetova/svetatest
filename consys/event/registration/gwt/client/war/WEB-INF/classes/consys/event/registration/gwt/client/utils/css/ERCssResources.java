package consys.event.registration.gwt.client.utils.css;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
public interface ERCssResources extends CssResource {

    String registerItemLeftPart();

    String registerItemLeftPartRegistered();

    String registerItemRightPart();

    String registerAttention();

    String registerItemUnselected();

    String registerItemSelected();

    String registrationOrderPanelDiscountText();

    String subbundlePanelCapacity();

    String subbundlePanelDiscount();

    /** --- Registration overview --- */
    String participantInfoPanel();

    String participantInfoPanelPortrait();

    String participantInfoPanelPersonal();

    String incomplete();

    String textAreaUpdateBox();

    /** --- Registration --- */
    String outerRegistrationForm();

    String registrationForm();

    String registrationFormPricingInfo();

    String registrationFormPricingLabel();

    String registrationFormPricingPanel();

    String registrationFormPricingTitle();

    /** Panel obsahujuci balicky a subbalicky */
    String registrationOrderPanel();

    String registrationOrderPanelWrapper();

    /** Samostatny panel reprezentujuci jednu zaregistrovantelnu vec */
    String registrationFormItem();

    String registrationFormSubItem();

    String registrationFormSubItemAction();

    /** obaluje cely content */
    String registrationItemContent();

    String registrationItemTitle();

    String registrationItemTitleLabel();

    String registrationItemTitlePrice();

    String registrationItemTitleAction();

    String registrationItemDescription();

    String registrationItemAlerts();

    String registrationItemDiscountCoupon();

    String registrationItemDiscountCouponAmount();

    String registrationItemDiscountCouponInput();

    String registrationItemPricePanel();

    String waitingCapacity();

    String waitingCapacityFull();

    String waitingCapacityFree();

    String registrationOptionItemPanel();

    String itemPanelLabel();

    String itemPanelBox();

    String quantityPanel();

    String quantityPanelBox();

    String quantityPanelLabel();

    String quantityPanelMultipleLabel();

    String badgesSizeDialogTitle();

    String badgesSizeDialogSizeButtonWrapper();

    String badgesSizeDialogSizeButton();

    String internalRegistrationDialog();

    String button();

    String cancel();

    String panel();

    String contentPanel();

    String internalRegistrationBundleLabel();

    String internalRegistrationUserName();

    String userAnswersAndWish();

    String leftColumnAdmin();

    String rightColumnAdmin();

    String orderControlPanel();

    String orderDetailPanel();

    String title();

    String detils();

    String totalPrice();

    String bulletItemPanel();

    String bulletItem();

    String discount();
    
    String payButtonCard();
    
    String payButtonPal();
}
