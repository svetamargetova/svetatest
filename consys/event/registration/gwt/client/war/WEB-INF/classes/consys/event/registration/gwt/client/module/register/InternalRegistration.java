package consys.event.registration.gwt.client.module.register;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.comp.user.NameWithAffiliation;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.UserUtils;
import consys.event.registration.api.list.InternalRegistrationListApi;
import consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem;
import consys.event.registration.gwt.client.module.register.item.InternalRegistrationDialog;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 *
 * @author pepa
 */
public class InternalRegistration extends DataListPanel implements InternalRegistrationListApi {

    // komponenty
    private ConsysMessage helpMessage;

    public InternalRegistration() {
        super(ERMessageUtils.c.internalRegistration_title(), LIST_TAG);

        setListDelegate(new InternalRegistrationDelegate());

        ActionImage add = ActionImage.getPlusButton(ERMessageUtils.c.internalRegistration_action_addNewRegistration(), true);
        add.addClickHandler(addClickHandler());
        addLeftControll(add);

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ERMessageUtils.c.registerAnother_text_pleaseAddParticipant());
        helpMessage.addStyleName(StyleUtils.MARGIN_TOP_10);
        helpMessage.hideClose(true);

        setDefaultFilter(new ListFilter(Filter_ALL) {
            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
    }

    private ClickHandler addClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                InternalRegistrationDialog dialog = new InternalRegistrationDialog(InternalRegistration.this);
                dialog.showCentered();
            }
        };
    }

    public void reload() {
        super.onLoad();
    }

    private class InternalRegistrationDelegate implements ListDelegate<ClientInternalRegistrationListItem, InternalRegistrationItem> {

        @Override
        public InternalRegistrationItem createCell(ClientInternalRegistrationListItem item) {
            return new InternalRegistrationItem(item);
        }
    }

    private class InternalRegistrationItem extends ListItem<ClientInternalRegistrationListItem> {

        public InternalRegistrationItem(ClientInternalRegistrationListItem value) {
            super(value);
        }

        @Override
        protected void createCell(ClientInternalRegistrationListItem item, FlowPanel panel) {
            String affiliation = UserUtils.affilation(item.getUserPosition(), item.getUserOrganization(), UIMessageUtils.c.const_at());

            NameWithAffiliation aff = new NameWithAffiliation(item.getUserName(), affiliation, toRegistration(item));
            aff.addStyleName(ERResourceUtils.css().internalRegistrationUserName());
            Label bundle = StyleUtils.getStyledLabel(item.getQuantity() + "x " + item.getBundleName(),
                    ERResourceUtils.css().internalRegistrationBundleLabel());
            
            panel.add(bundle);
            panel.add(aff);
            panel.add(StyleUtils.clearDiv());
        }

        private ConsysAction toRegistration(final ClientInternalRegistrationListItem item) {
            return new ConsysAction() {
                @Override
                public void run() {
                    FormUtils.fireNextBreadcrumb(RegistrationForm.anotherRegistrationBreadcrumb(item.getUserName()),
                            new AdminRegistrationOverview(item.getRegistrationUuid()));
                }
            };
        }
    }
}
