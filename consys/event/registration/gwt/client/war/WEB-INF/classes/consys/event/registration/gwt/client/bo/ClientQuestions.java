package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientQuestions implements Result {

    private static final long serialVersionUID = 3264707557304255278L;
    // data
    private ArrayList<ClientCustomQuestion> questions;

    public ClientQuestions() {
        questions = new ArrayList<ClientCustomQuestion>();
    }

    public ArrayList<ClientCustomQuestion> getQuestions() {
        return questions;
    }
}
