package consys.event.registration.gwt.client.bo;

/**
 * Jedna polozka seznamu internich registraci
 * @author pepa
 */
public class ClientInternalRegistrationListItem extends ClientListParticipantItem {

    private static final long serialVersionUID = 6857687957393160213L;
    // data
    private String registrationUuid;
    private String bundleName;
    private int quantity;

    public String getRegistrationUuid() {
        return registrationUuid;
    }

    public void setRegistrationUuid(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
