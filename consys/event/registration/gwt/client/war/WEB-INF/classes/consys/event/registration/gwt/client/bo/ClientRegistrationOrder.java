package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.Monetary;
import consys.event.registration.gwt.client.bo.ClientRegistration.State;
import java.util.ArrayList;
import java.util.Date;

/**
 * Objekt jedne objednane registrace
 * @author pepa
 */
public class ClientRegistrationOrder implements Result {

    private static final long serialVersionUID = -5537638390652037661L;
    // data
    private String uuid;
    private Monetary totalPrice;
    private Date orderDate;
    private Date dueDate;
    private Date paidDate;
    private State state;
    private ArrayList<ClientRegistrationOrderItem> orderItems;

    public ClientRegistrationOrder() {
        orderItems = new ArrayList<ClientRegistrationOrderItem>();
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public ArrayList<ClientRegistrationOrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<ClientRegistrationOrderItem> orderItems) {
        if (orderItems == null) {
            orderItems = new ArrayList<ClientRegistrationOrderItem>();
        }
        this.orderItems = orderItems;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Monetary getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Monetary totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
                if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ClientRegistrationOrder e = (ClientRegistrationOrder) obj;
        return uuid.equalsIgnoreCase(e.getUuid());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }


}
