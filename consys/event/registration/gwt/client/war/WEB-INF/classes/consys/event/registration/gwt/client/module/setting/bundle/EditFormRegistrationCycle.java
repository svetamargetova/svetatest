package consys.event.registration.gwt.client.module.setting.bundle;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysDoubleTextBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.shared.bo.Monetary;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.action.UpdateRegistrationCycleAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import consys.event.registration.gwt.client.module.exception.CyclesIntersectionException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ValidationUtils;
import java.util.Date;

/**
 * Editacni formular pro nastavovani registracniho cyklu
 * @author pepa
 */
public class EditFormRegistrationCycle extends RootPanel {

    // komponenty
    private SimpleFormWithHelpPanel mainPanel;
    private BaseForm form;
    private ConsysDateBox fromBox;
    private ConsysDateBox toBox;
    private ConsysDoubleTextBox valueBox;
    // data
    private ClientRegistrationCycle crc;
    private RegistrationCycleItem source;
    private RegistrationCycleList list;

    public EditFormRegistrationCycle(ClientRegistrationCycle crc, RegistrationCycleItem source, RegistrationCycleList list) {
        super(ERMessageUtils.c.editFormRegistrationCycle_title());

        mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        this.crc = crc;
        this.source = source;
        this.list = list;
        init();
    }

    /** vygeneruje editační formulář */
    private void init() {
        fromBox = ConsysDateBox.getEventHalfBox(true, crc.getFrom());
        toBox = ConsysDateBox.getEventHalfBox(true, crc.getTo());

        fromBox.addValueChangeHandler(DateTimeUtils.dateValueChangeHandler(fromBox, toBox));

        valueBox = new ConsysDoubleTextBox(ConsysDateBox.HALF_WIDTH, 0, 999999.99, ERMessageUtils.c.const_price());
        valueBox.addStyleName(StyleUtils.FLOAT_LEFT);
        if (crc.getValue() != null) {
            valueBox.setText(crc.getValue().toString());
        }

        ConsysFlowPanel valuePanel = new ConsysFlowPanel();
        valuePanel.add(valueBox);
        valuePanel.add(StyleUtils.getStyledLabel(ECoMessageUtils.getCurrency(), StyleUtils.MARGIN_LEFT_10, StyleUtils.FLOAT_LEFT));

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        form = new BaseForm();
        form.setStyleName(StyleUtils.MARGIN_TOP_10);
        form.addRequired(0, ECoMessageUtils.c.const_date_from(), fromBox);
        form.addRequired(1, ECoMessageUtils.c.const_date_to(), toBox);
        form.addRequired(2, ERMessageUtils.c.const_price(), valuePanel);
        form.addActionMembers(3, button, ActionLabel.breadcrumbBackCancel(), "200px");
        mainPanel.addWidget(form);

        //mainPanel.addHelpTitle("Help title");
        //mainPanel.addHelpText("help text ...");
    }

    /** ClickHandler pro potvrzovaci tlacitko */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // validace
                Date from = fromBox.getValue();
                if (from == null) {
                    getFailMessage().addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ECoMessageUtils.c.const_date_from()));
                    return;
                }
                Date to = toBox.getValue();
                if (to == null) {
                    getFailMessage().addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ECoMessageUtils.c.const_date_to()));
                    return;
                }
                if (!from.before(to)) {
                    getFailMessage().addOrSetText(UIMessageUtils.m.const_notAfter(
                            ECoMessageUtils.c.const_date_from(), ECoMessageUtils.c.const_date_to()));
                    return;
                }

                if (!ValidationUtils.areRegistrationCycleDatesValid(fromBox, toBox, getFailMessage())) {
                    return;
                }

                if (!form.validate(getFailMessage())) {
                    return;
                }

                // kontrola na presah cyklu

                if (ValidationUtils.isRegistrationCycleOverlapping(from, to, list.getCycles(source))) {
                    getFailMessage().setText(ERMessageUtils.c.const_registrationCycleNotOverlap());
                    return;
                }

                final ClientRegistrationCycle data = new ClientRegistrationCycle();
                data.setUuid(crc.getUuid());
                data.setFrom(from);
                data.setTo(to);
                data.setValue(Monetary.toMonetary(valueBox.getText()));

                EventDispatchEvent updateEvent = new EventDispatchEvent(
                        new UpdateRegistrationCycleAction(data),
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof CyclesIntersectionException) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionCyclesIntersection());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                source.refreshClienRegistrationCycleItem(data);
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, mainPanel);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }
}
