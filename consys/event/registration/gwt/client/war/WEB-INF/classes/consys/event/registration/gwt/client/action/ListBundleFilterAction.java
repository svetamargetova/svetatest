package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.CommonThumb;

/**
 * Akce pro nacteni seznamu bundlu pro filtrovani
 * @author pepa
 */
public class ListBundleFilterAction extends EventAction<ArrayListResult<CommonThumb>> {

    private static final long serialVersionUID = -8912301391031698052L;
}
