package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientTabItem;

/**
 * Akce pro nacteni nastaveni registraci, pokud je all true vraci se vsechny bez ohledu na hodnotu subbundle
 * @author pepa
 */
public class ListRegistrationSettingsAction extends EventAction<ArrayListResult<ClientTabItem>> {

    private static final long serialVersionUID = -3977051056242635414L;
    // data
    private boolean subbundle;
    private boolean all;

    public ListRegistrationSettingsAction() {
        all = true;
    }

    public ListRegistrationSettingsAction(boolean subbundle) {
        this.subbundle = subbundle;
    }

    public boolean isAll() {
        return all;
    }

    public boolean isSubbundle() {
        return subbundle;
    }
}
