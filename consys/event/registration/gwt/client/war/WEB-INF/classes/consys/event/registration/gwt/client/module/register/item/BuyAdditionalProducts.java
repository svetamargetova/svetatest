package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.payment.PaymentProfilePanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.common.gwt.shared.exception.CouponExpiredException;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.common.gwt.shared.exception.CouponUsedException;
import consys.common.gwt.shared.exception.IbanException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.gwt.client.action.ListPaymentDataDialogAction;
import consys.event.common.gwt.client.action.exception.EventNotActivatedException;
import consys.event.common.gwt.client.module.payment.PaymentDialog;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction;
import consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction;
import consys.event.registration.gwt.client.action.RegisterUserAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.event.ChangeOrderPriceEvent;
import consys.event.registration.gwt.client.event.OrderCanceledEvent;
import consys.event.registration.gwt.client.event.OrderTotalPriceEvent;
import consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class BuyAdditionalProducts extends ConsysFlowPanel implements OrderCanceledEvent.Handler, ChangeOrderPriceEvent.Handler {

    // konstanty
    private static final String GROUP_ID = "buyAdditional";
    // komponenty
    private ActionImage button;
    // data
    private Monetary total;
    private boolean isEnabled;
    private ArrayList<String> alreadyOrderedPacks = null;
    private ArrayList<ClientRegistrationPack> allPacks = null;
    private ConsysActionWithValue<ClientRegistration> afterRegistrationAction;
    private final String managedUserUuid;

    public BuyAdditionalProducts(ClientRegistration registration) {
        isEnabled = registration.getRegistrationOrders().get(0).getState().equals(ClientRegistration.State.CONFIRMED);

        alreadyOrderedPacks = new ArrayList<String>();
        for (ClientRegistrationOrder order : registration.getRegistrationOrders()) {
            for (ClientRegistrationOrderItem item : order.getOrderItems()) {
                alreadyOrderedPacks.add(item.getBundleUuid());
            }
        }

        ClientUser cu = Cache.get().get(UserCacheAction.USER);
        String registrationUserUuid = registration.getEventUser().getUuid();

        if (!cu.getUuid().equals(registrationUserUuid)) {
            managedUserUuid = registrationUserUuid;
        } else {
            managedUserUuid = null;
        }
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(OrderCanceledEvent.TYPE, this);
        EventBus.get().addHandler(ChangeOrderPriceEvent.TYPE, this);

        LoadAvailableSubbundlesAction action = new LoadAvailableSubbundlesAction(null);
        if (managedUserUuid != null) {
            action.setManagedUserUuid(managedUserUuid);
        }

        EventBus.get().fireEvent(new EventDispatchEvent(action,
                new AsyncCallback<ArrayListResult<ClientRegistrationPack>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        allPacks = new ArrayList<ClientRegistrationPack>();
                        generateContent();
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientRegistrationPack> result) {
                        allPacks = result.getArrayListResult();
                        generateContent();
                    }
                }, this));
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(OrderCanceledEvent.TYPE, this);
        EventBus.get().removeHandler(ChangeOrderPriceEvent.TYPE, this);
    }

    /** nastavi akci, ktera se spusti po uspesnem zaregistrovani dalsich produktu */
    public void setAfterRegistrationAction(ConsysActionWithValue<ClientRegistration> afterRegistrationAction) {
        this.afterRegistrationAction = afterRegistrationAction;
    }

    /** zaridi vygenerovani obsahu panelu */
    private void generateContent() {
        clear();

        total = Monetary.ZERO;
        ArrayList<ClientRegistrationPack> filtredSubbundles = new ArrayList<ClientRegistrationPack>();

        // najskor odstranime uz zakupene polozky
        for (ClientRegistrationPack pack : allPacks) {
            boolean append = true;
            for (String alreadyOrderedSubbundle : alreadyOrderedPacks) {
                if (pack.getUuidBundle().equalsIgnoreCase(alreadyOrderedSubbundle)) {
                    append = false;
                    break;
                }
            }
            if (append) {
                filtredSubbundles.add(pack);
            }
        }

        if (filtredSubbundles.isEmpty()) {
            setVisible(false);
        } else {
            setVisible(true);
            drawContent(filtredSubbundles);
        }
    }

    /** zaridi vykresleni obsahu panelu */
    private void drawContent(ArrayList<ClientRegistrationPack> availableSubbundles) {
        add(Separator.addedStyle(StyleUtils.MARGIN_BOT_20));
        add(StyleUtils.clearDiv("20px"));

        // popisek, ze lze zakupovat dalsi produkty nebo kdy je lze nakupovat
        if (isEnabled) {
            add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationAdditionalProducts_title(),
                    StyleUtils.FONT_BOLD, StyleUtils.FONT_17PX));
        } else {
            add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationAdditionalProducts_text_ifDisabled(),
                    StyleUtils.FONT_BOLD, StyleUtils.FONT_17PX));
        }
        add(StyleUtils.clearDiv("10px"));

        // vykresleni subbalicku
        final String currency = ECoMessageUtils.getCurrency();
        for (ClientRegistrationPack object : availableSubbundles) {
            RegistrationFormPanelSubItem item = new RegistrationFormPanelSubItem(GROUP_ID, object, currency, true, false);
            item.setEnabled(isEnabled && object.getFreeCapacity() != 0);
            add(item);
        }
        RegistrationFormItemTotalPanel totalPanel = new RegistrationFormItemTotalPanel(Monetary.ZERO, currency);
        add(totalPanel);

        button = registerButton();
        add(button);
        add(StyleUtils.clearDiv());
    }

    private ActionImage registerButton() {
        ActionImage register = ActionImage.getConfirmButton(ERMessageUtils.c.buyAdditionProducts_button_order());
        register.addStyleName(StyleUtils.FLOAT_RIGHT);
        register.addStyleName(StyleUtils.MARGIN_TOP_10);
        register.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                showPaymentDialog();
            }
        });
        register.setEnabled(isEnabled);
        register.setVisible(false);
        return register;
    }

    /** zobrazeni platebniho dialogu */
    public void showPaymentDialog() {
        ClientUser cu = null;
        try {
            cu = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
        } catch (NotInCacheException ex) {
            throw new NullPointerException(UserCacheAction.USER);
        }
        PaymentDialog payDialog = new PaymentDialog(cu.getUuid(), true, false, false) {
            @Override
            public void doAction(PaymentProfilePanel profilePanel, String userUuid, final ConsysMessage fail) {
                final PaymentDialog self = this;
                if (profilePanel.isEdited() && (wasZeroInvoice() ? false : !profilePanel.doValidate(fail))) {
                    return;
                }

                RegisterUserAction registerAction = new RegisterUserAction(userUuid,
                        profilePanel.getClientPaymentProfile().getUuid(), getSubbundles());
                registerAction.setProfileEdited(profilePanel.isEdited());
                registerAction.setAdditionalProducts(true);
                registerAction.setManagedUserUuid(managedUserUuid);

                if (profilePanel.isEdited()) {
                    registerAction.setEditedProfile(profilePanel.getClientPaymentProfile());
                }

                EventBus.get().fireEvent(new EventDispatchEvent(
                        registerAction,
                        new AsyncCallback<ClientRegistration>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof UserAlreadyRegistredException) {
                                    fail.setText(ERMessageUtils.c.const_exceptionUserAlreadyRegistered());
                                } else if (caught instanceof BundleCapacityException) {
                                    fail.setText(ERMessageUtils.c.const_exceptionTicketCapacity());
                                } else if (caught instanceof IbanException) {
                                    fail.setText(UIMessageUtils.c.const_exceptionBadIban());
                                } else if (caught instanceof EventNotActivatedException) {
                                    fail.setText(ERMessageUtils.c.const_exceptionEventNotActivated());
                                } else if (caught instanceof CouponUsedException) {
                                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponUsedException());
                                } else if (caught instanceof NoRecordsForAction) {
                                    fail.setText(ERMessageUtils.c.registrationFormItem_exception_noRecordsForAction());
                                } else if (caught instanceof CouponInvalidException) {
                                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponInvalidException());
                                } else if (caught instanceof CouponExpiredException) {
                                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponExpiredException());
                                } else if (caught instanceof PermissionException) {
                                    fail.setText(UIMessageUtils.c.const_error_noPermissionForAction());
                                }
                            }

                            @Override
                            public void onSuccess(ClientRegistration result) {
                                hide();
                                if (afterRegistrationAction != null) {
                                    afterRegistrationAction.run(result);
                                }
                            }
                        }, self));
            }

            @Override
            public ListPaymentDataDialogAction getDataAction() {
                LoadRegisterUserPaymentDialogAction action = new LoadRegisterUserPaymentDialogAction();
                action.addProducts(getSubbundles());
                return action;
            }

            @Override
            public void processDataActionFailure(Throwable caught, ConsysMessage fail) {
                if (caught instanceof CouponUsedException) {
                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponUsedException());
                } else if (caught instanceof NoRecordsForAction) {
                    // TODO: lepsi hlasku pro spatny slevovy kod
                    fail.setText(ERMessageUtils.m.registrationFormItem_exception_noRecordsForAction("?"));
                } else if (caught instanceof UserAlreadyRegistredException) {
                    fail.setText(ERMessageUtils.c.const_exceptionUserAlreadyRegistered());
                } else if (caught instanceof BundleCapacityException) {
                    fail.setText(ERMessageUtils.c.const_exceptionTicketCapacity());
                } else if (caught instanceof CouponExpiredException) {
                    fail.setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponExpiredException());
                } else if (caught instanceof PermissionException) {
                    fail.setText(UIMessageUtils.c.const_error_noPermissionForAction());
                }
            }
        };
        payDialog.setManagedUserUuid(managedUserUuid);
        payDialog.showCentered();
    }

    /** nactou se vybrane subbalicky */
    private ArrayList<ProductThumb> getSubbundles() {
        ArrayList<ProductThumb> orderedItems = new ArrayList<ProductThumb>();
        for (int i = 0; i < getWidgetCount(); i++) {
            Widget w = getWidget(i);
            if (w instanceof RegistrationFormPanelSubItem) {
                RegistrationFormPanelSubItem item = (RegistrationFormPanelSubItem) w;
                if (item.isSelected()) {
                    orderedItems.add(item.getProductThumb());
                }
            }
        }
        return orderedItems;
    }

    @Override
    public void onOrderCanceled(OrderCanceledEvent event) {
        // odstranime z already registred subbundles to co obsahuje zrusena objednavka
        for (ClientRegistrationOrderItem item : event.getOrder().getOrderItems()) {
            alreadyOrderedPacks.remove(item.getBundleUuid());
        }
        // pregenerujeme
        generateContent();
    }

    @Override
    public void onPriceChange(ChangeOrderPriceEvent event) {
        if (event.getGroupId().equals(GROUP_ID)) {
            Monetary newTotal = event.isSubstract() ? total.sub(event.getChange()) : total.add(event.getChange());
            if (!newTotal.equals(total)) {
                total = newTotal;
                EventBus.get().fireEvent(new OrderTotalPriceEvent(total));
            }
        }
        button.setVisible(!(total.equals(Monetary.ZERO) && getSubbundles().isEmpty()));
    }
}
