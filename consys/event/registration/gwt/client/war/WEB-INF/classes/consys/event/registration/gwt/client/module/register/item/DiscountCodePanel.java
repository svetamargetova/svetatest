package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.input.ConsysTextBoxInterface;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.payment.HasPrice;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.registration.gwt.client.action.LoadProductDiscountAction;
import consys.event.registration.gwt.client.event.ChangeOrderPriceEvent;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.common.gwt.shared.exception.CouponUsedException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 * Slevovy panel
 *
 * @author pepa
 */
public abstract class DiscountCodePanel<T extends ConsysTextBoxInterface> extends ConsysFlowPanel implements HasPrice {

    // komponenty
    private ConsysTextBoxInterface textBox;
    private Label priceLabel;
    private ActionLabel action;
    private Label errorLabel;
    // data
    private Monetary discount;
    private final String bundleUuid;
    private final String currency;
    private String groupId;

    DiscountCodePanel(String bundleUuid, String currency) {
        this.bundleUuid = bundleUuid;
        this.groupId = bundleUuid;
        this.currency = currency;
        setDefaultDiscount();
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    private void setDefaultDiscount() {
        discount = Monetary.ZERO;
    }

    protected abstract T getTextBox();

    @Override
    protected void onLoad() {
        // nastavenie zakladneho style class
        setStyleName(ERResourceUtils.bundle().css().registrationItemDiscountCoupon());

        Label label = StyleUtils.getStyledLabel(UIMessageUtils.c.const_discountCode(), ResourceUtils.common().css().title());

        textBox = getTextBox();
        textBox.addStyleName(ERResourceUtils.bundle().css().registrationItemDiscountCouponInput());

        action = new ActionLabel(UIMessageUtils.c.const_apply(), ResourceUtils.common().css().action());
        action.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                // Ak je textBox enabled je mozne vpisovat kod, ak nie je potom ma nastaveny nejaky kod a tymto ho rzusime
                if (textBox.isEnabled()) {
                    getApplyAction().run();
                } else {
                    removeDiscount();
                }
            }
        });

        priceLabel = StyleUtils.getStyledLabel("", ERResourceUtils.bundle().css().registrationItemDiscountCouponAmount());
        actualizePriceLabel();

        add(priceLabel);
        add(label);
        add(textBox);
        add(action);
        add(StyleUtils.clearDiv("10px"));
    }

    /** Odstrani nastavenu zlavu */
    public void removeDiscount() {
        // vyhodime zmenu zlavy - pripocirtanie hodnoty
        fireChangePrice(discount, false);
        // znovu moze nastavit kod
        textBox.setEnabled(true);
        textBox.setText("");
        // premenujeme action button
        action.setText(UIMessageUtils.c.const_apply());
        // nastavime zmenu ceny
        setDefaultDiscount();
        actualizePriceLabel();
    }

    private void fireChangePrice(Monetary monetary, boolean substract) {
        if (monetary != null && !monetary.isZero()) {
            EventBus.get().fireEvent(new ChangeOrderPriceEvent(groupId, monetary, substract));
        }
    }

    /** vraci zadany kod */
    public String getDiscountCode() {
        return textBox.getText().trim();
    }

    /** Aktualizuje cenu */
    private void actualizePriceLabel() {
        double price = 0;
        if (discount != null && !discount.isZero()) {
            price = Double.parseDouble(discount.toString());
        }
        priceLabel.setText(UIMessageUtils.assignCurrency(currency, FormUtils.NDASH + " " + FormUtils.doubleFormat.format(price)));
    }

    @Override
    public Monetary getPrice() {
        return discount;
    }

    /** aktivuje/deaktivuje vstup */
    public void setEnabled(boolean value) {
        textBox.setEnabled(value);

    }

    protected void setDiscountCodeConfirmed(Monetary discount) {
        this.discount = discount;
        removeError();
        fireChangePrice(discount, true);
        textBox.setEnabled(false);
        actualizePriceLabel();
        action.setText(UIMessageUtils.c.const_cancel());
    }

    protected void setDiscountCodeRejected(Throwable exception) {
        removeError();
        String text;
        if (exception instanceof CouponInvalidException || exception instanceof NoRecordsForAction || exception instanceof BadInputException) {
            text = ERMessageUtils.c.registrationFormItem_discount_exception_CouponInvalidException();
        } else if (exception instanceof CouponUsedException) {
            text = ERMessageUtils.c.registrationFormItem_discount_exception_CouponUsedException();
        } else {
            text = ERMessageUtils.c.registrationFormItem_discount_exception_Common();
        }

        errorLabel = StyleUtils.getErrorLabel(text);
        add(errorLabel);
    }

    private void removeError() {
        if (errorLabel != null) {
            errorLabel.removeFromParent();
            errorLabel = null;
        }
    }

    /** vraci akci po kliknuti na apply vedle slevoveho kodu */
    protected ConsysAction getApplyAction() {
        return new ConsysAction() {
            @Override
            public void run() {
                EventBus.get().fireEvent(new EventDispatchEvent(
                        new LoadProductDiscountAction(bundleUuid, DiscountCodePanel.this.getDiscountCode()),
                        new AsyncCallback<Monetary>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                setDiscountCodeRejected(caught);
                            }

                            @Override
                            public void onSuccess(Monetary result) {
                                setDiscountCodeConfirmed(result);
                            }
                        }, DiscountCodePanel.this));
            }
        };
    }
}
