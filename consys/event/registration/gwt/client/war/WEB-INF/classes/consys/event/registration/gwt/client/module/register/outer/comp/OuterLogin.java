package consys.event.registration.gwt.client.module.register.outer.comp;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.constants.sso.SSO;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.LoginPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.event.SSOButtonEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.module.register.outer.OuterResources;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Komponentka pro vnejsi prihlaseni
 * <p/>
 * @author pepa
 */
public class OuterLogin extends LoginPanel {

    // komponenty
    private ActionLabel dontHaveAny;
    // data
    private ConsysActionWithValue<ClientSsoUserDetails> userDetailAction;
    private final String synchroToken;

    public OuterLogin(String synchroToken) {
        dontHaveAny = new ActionLabel(true, ERMessageUtils.c.outerLogin_action_iDontHaveAny(), OuterResources.INSTANCE.css().dontHaveAny());
        this.synchroToken = synchroToken;
    }

    @Override
    protected void generateAdditional(FlowPanel panel) {
        panel.add(dontHaveAny);
    }

    @Override
    protected ConsysAction buttonAction(final SSO type) {
        return new ConsysAction() {

            @Override
            public void run() {
                // vyskladani url
                final String url = "/oauth/connect/login?state=" + synchroToken + "&service=" + type.name();
                // pop up window
                FormUtils.popupWindow(url, "_blank", 640, 480);

                // inicializujeme do server session zamok
                EventBus.fire(new DispatchEvent(new LoadSsoUserDetailsAction(synchroToken),
                        new AsyncCallback<ClientSsoUserDetails>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(UIMessageUtils.c.const_ssoNotWork());
                                }
                            }

                            @Override
                            public void onSuccess(ClientSsoUserDetails result) {
                                LoggerFactory.log(OuterLogin.class, "SSO DETAILS: " + result);
                                if (userDetailAction != null) {
                                    dontHaveAny.setVisible(false);
                                    userDetailAction.run(result);
                                }
                                if(result.getUserUuid()!=null) {
                                    // zkusime nacist platebni profil
                                    // TODO: zavolat si pro profil
                                }
                                EventBus.fire(new SSOButtonEvent(type));
                            }
                        }, OuterLogin.this));
            }
        };
    }

    /** nastavi clickhandler pro actionLabel */
    public void setUserDetailAction(final ConsysActionWithValue<ClientSsoUserDetails> userDetailAction) {
        this.userDetailAction = userDetailAction;
        dontHaveAny.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                dontHaveAny.setVisible(false);
                userDetailAction.run(null);
            }
        });
    }
}
