/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.registration.gwt.client.utils;

import consys.common.gwt.client.ui.comp.DownMenu;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.registration.gwt.client.module.coupon.CouponList;
import consys.event.registration.gwt.client.module.setting.RegistrationPropertyPanel;
import consys.event.registration.gwt.client.module.setting.RegistrationSettingsBundle;
import consys.event.registration.gwt.client.module.setting.RegistrationSettingsCustomQuestion;

/**
 *
 * @author palo
 */
public class ModuleNavigationUtils {

    public static DownMenu.DownMenuItem downMenuOptionBundlesSettings() {

        return new DownMenu.DownMenuItem() {
            @Override
            public String getTitle() {
                return ERMessageUtils.c.registrationSettingsBundle_title();
            }

            @Override
            public void doAction() {
                RegistrationSettingsBundle w = new RegistrationSettingsBundle(false);
                FormUtils.fireNextBreadcrumb(ERMessageUtils.c.registrationSettingsBundle_title(), w);
            }
        };
    }

    public static DownMenu.DownMenuItem downMenuOptionSubbundlesSettings() {

        return new DownMenu.DownMenuItem() {
            @Override
            public String getTitle() {
                return ERMessageUtils.c.registrationSettingsBundle_action_ticketAccessories();
            }

            @Override
            public void doAction() {
                RegistrationSettingsBundle w = new RegistrationSettingsBundle(true);
                FormUtils.fireNextBreadcrumb(ERMessageUtils.c.registrationSettingsSubbundle_title(), w);
            }
        };
    }

    public static DownMenu.DownMenuItem downMenuOptionSystemProperty() {

        return new DownMenu.DownMenuItem() {
            @Override
            public String getTitle() {
                return UIMessageUtils.c.const_property();
            }

            @Override
            public void doAction() {
                RegistrationPropertyPanel w = new RegistrationPropertyPanel();
                FormUtils.fireNextBreadcrumb(UIMessageUtils.c.const_property(), w);
            }
        };
    }

    public static DownMenu.DownMenuItem downMenuOptionCouponList() {
        return new DownMenu.DownMenuItem() {
            @Override
            public String getTitle() {
                return ERMessageUtils.c.discountList_title();
            }

            @Override
            public void doAction() {
                CouponList w = new CouponList(null);
                FormUtils.fireNextBreadcrumb(ERMessageUtils.c.discountList_title(), w);
            }
        };
    }

    public static DownMenu.DownMenuItem downMenuCustomFields() {
        return new DownMenu.DownMenuItem() {
            @Override
            public String getTitle() {
                return ERMessageUtils.c.registrationSettingsCustomQuestion_title();
            }

            @Override
            public void doAction() {
                FormUtils.fireNextBreadcrumb(getTitle(), new RegistrationSettingsCustomQuestion());
            }
        };
    }
}
