package consys.event.registration.gwt.client.module.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class UserAlreadyRegistredException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public UserAlreadyRegistredException() {
        super("");
    }

    public UserAlreadyRegistredException(String message) {
        super(message);
    }

    public UserAlreadyRegistredException(Throwable cause) {
        super(cause);
    }

    public UserAlreadyRegistredException(String message, Throwable cause) {
        super(message, cause);
    }
}
