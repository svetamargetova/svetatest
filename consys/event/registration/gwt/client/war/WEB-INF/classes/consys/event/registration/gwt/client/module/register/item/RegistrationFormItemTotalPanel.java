package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.Monetary;
import consys.event.registration.gwt.client.event.OrderTotalPriceEvent;

/**
 *
 * @author pepa
 */
public class RegistrationFormItemTotalPanel extends FlowPanel implements OrderTotalPriceEvent.Handler {

    // komponenty
    private Label priceLabel;
    // data
    private Monetary defaultPrice;
    private Monetary price;
    private final String currency;
    private RegisterTotalPanelListener listener;

    /** pokud je registerButtonAction null nevytvari se tlacitko */
    public RegistrationFormItemTotalPanel(Monetary defaultPrice, String currency) {
        this.defaultPrice = defaultPrice;
        this.currency = currency;
        resetPrice();

        Separator separator = Separator.addedStyle(StyleUtils.MARGIN_BOT_10);
        separator.setWidth("110px");
        separator.addStyleName(StyleUtils.FLOAT_RIGHT);

        priceLabel = StyleUtils.getStyledLabel("", StyleUtils.ALIGN_RIGHT, StyleUtils.FONT_BOLD, StyleUtils.FONT_19PX);

        add(separator);
        add(StyleUtils.clearDiv());
        add(priceLabel);

        refreshPrice();
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        EventBus.get().addHandler(OrderTotalPriceEvent.TYPE, this);
    }

    @Override
    protected void onUnload() {
        super.onUnload();

        EventBus.get().removeHandler(OrderTotalPriceEvent.TYPE, this);
    }

    /** nastavi vychozi cenu */
    public void resetPrice() {
        price = new Monetary(defaultPrice.getInteger(), defaultPrice.getDecimal());
        callListener();
    }

    /** vraci string s cenou a menou */
    private void refreshPrice() {
        priceLabel.setText(UIMessageUtils.assignCurrency(currency, price));
        callListener();
    }

    /** odecte od aktualni ceny */
    public void sub(Monetary sub) {
        price = price.sub(sub);
        refreshPrice();
    }

    /** price k aktualni cene */
    public void add(Monetary add) {
        price = price.add(add);
        refreshPrice();
    }

    /** provola spravnou metodu listeneru, je umozneno provolat z venku */
    public void callListener() {
        if (listener != null) {
            if (price.isZero()) {
                listener.isZero();
            } else {
                listener.isNotZero();
            }
        }
    }

    /** nastavi listener */
    public void setListener(RegisterTotalPanelListener listener) {
        this.listener = listener;
    }

    @Override
    public void onTotalPriceChanged(OrderTotalPriceEvent event) {
        price = event.getPrice();
        refreshPrice();
    }

    /** interface listeneru pro naslouchani celkove hodnoty faktury */
    public interface RegisterTotalPanelListener {

        /** provolava se kdyz se hodnota zmeni na nulu */
        public void isZero();

        /** provolava se kdyz se hodnota zmeni na cokoliv jineho nez nulu */
        public void isNotZero();
    }
}
