package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationResult;

/**
 * Akce pro nacteni seznamu registraci ktere jsou aktualne k dispozici
 * @author pepa
 */
public class LoadUserRegistrationAction extends EventAction<ClientRegistrationResult> {

    private static final long serialVersionUID = -8032492264188765629L;
    // data
    private String managedUserUuid;

    public LoadUserRegistrationAction() {
    }

    public LoadUserRegistrationAction(String managedUserUuid) {
        this.managedUserUuid = managedUserUuid;
    }

    public String getManagedUserUuid() {
        return managedUserUuid;
    }
}
