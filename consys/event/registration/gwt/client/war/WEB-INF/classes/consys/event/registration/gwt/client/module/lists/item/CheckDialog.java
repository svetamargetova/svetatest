package consys.event.registration.gwt.client.module.lists.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.DateResult;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Dialog;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.api.checkin.CheckInState;
import consys.event.registration.gwt.client.action.UpdateCheckInAction;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 *
 * @author pepa
 */
public class CheckDialog extends SmartDialog {

    // data - komponenty
    private CheckInItem parent;
    // data
    private String uuidRegistration;
    private boolean checkIn;

    public CheckDialog(String uuidRegistration, boolean checkIn, CheckInItem parent) {
        this.uuidRegistration = uuidRegistration;
        this.checkIn = checkIn;
        this.parent = parent;
    }

    @Override
    protected void generateContent(SimpleFormPanel panel) {
        FlowPanel fp = new FlowPanel();
        fp.addStyleName(StyleUtils.MARGIN_HOR_10);

        final int maxQuantity = parent.getData().getMaxQuantity();
        final int checkedQuantity = parent.getData().getQuantityChecked();

        if (checkIn) {
            if (maxQuantity - checkedQuantity > 1) {
                fp.add(getActionImage(ERMessageUtils.c.checkDialog_text_all(), CheckInState.CHECK_IN_ALL));
            }
            for (int i = 0; i < maxQuantity - checkedQuantity; i++) {
                fp.add(getActionImage(String.valueOf(i + 1), i + 1));
            }
        } else {
            if (checkedQuantity > 1) {
                fp.add(getActionImage(ERMessageUtils.c.checkDialog_text_all(), CheckInState.CHECK_OUT_ALL));
            }
            for (int i = 0; i < checkedQuantity; i++) {
                fp.add(getActionImage(String.valueOf(i + 1), i + 1));
            }
        }

        fp.add(StyleUtils.clearDiv());

        Separator separator1 = new Separator(Dialog.DEFAULT_WIDTH - 20 + "px", StyleUtils.MARGIN_VER_10);
        separator1.addStyleName(StyleUtils.MARGIN_LEFT_10);

        Separator separator2 = new Separator(Dialog.DEFAULT_WIDTH - 20 + "px", StyleUtils.MARGIN_TOP_10);
        separator2.addStyleName(StyleUtils.MARGIN_LEFT_10);

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), StyleUtils.MARGIN_LEFT_20);
        cancel.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });

        panel.addWidget(StyleUtils.clearDiv("10px"));
        panel.addWidget(StyleUtils.getStyledLabel(checkIn
                ? ERMessageUtils.c.checkDialog_titleIn()
                : ERMessageUtils.c.checkDialog_titleOut(),
                ERResourceUtils.css().badgesSizeDialogTitle()));
        panel.addWidget(separator1);
        panel.addWidget(StyleUtils.clearDiv("10px"));
        panel.addWidget(fp);
        panel.addWidget(separator2);
        panel.addWidget(StyleUtils.clearDiv("10px"));
        panel.addWidget(cancel);
        panel.addWidget(StyleUtils.clearDiv("10px"));
    }

    private ActionImage getActionImage(String text, int count) {
        ActionImage ai = checkIn
                ? ActionImage.getPlusButton(text, false)
                : ActionImage.getCrossButton(text, false);
        ai.setLabelWidth("30px");
        ai.addLabelStyleName(StyleUtils.ALIGN_CENTER);
        ai.addStyleName(StyleUtils.FLOAT_LEFT);
        ai.addStyleName(StyleUtils.MARGIN_LEFT_10);
        ai.addStyleName(StyleUtils.MARGIN_BOT_10);
        if (checkIn) {
            ai.addClickHandler(checkInHandler(count));
        } else {
            ai.addClickHandler(checkOutHandler(count));
        }
        return ai;
    }

    private ClickHandler checkInHandler(final int checkInCount) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                checkIn(checkInCount);
            }
        };
    }

    private ClickHandler checkOutHandler(final int checkOutCount) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                checkOut(checkOutCount);
            }
        };
    }

    private void checkIn(final int checkInCount) {
        EventBus.get().fireEvent(new EventDispatchEvent(new UpdateCheckInAction(true, uuidRegistration, checkInCount),
                new AsyncCallback<DateResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().addOrSetText("NoRecordsForAction");
                        }
                    }

                    @Override
                    public void onSuccess(DateResult result) {
                        if (checkInCount == CheckInState.CHECK_IN_ALL) {
                            parent.getData().setQuantityChecked(parent.getData().getMaxQuantity());
                        } else {
                            int prechecked = parent.getData().getQuantityChecked() + checkInCount;
                            int checked = prechecked > parent.getData().getMaxQuantity()
                                    ? parent.getData().getMaxQuantity()
                                    : prechecked;
                            parent.getData().setQuantityChecked(checked);
                        }

                        CheckDialog.this.hide();
                        parent.redrawItem();
                    }
                }, CheckDialog.this));
    }

    private void checkOut(final int checkOutCount) {
        EventBus.get().fireEvent(new EventDispatchEvent(new UpdateCheckInAction(false, uuidRegistration, checkOutCount),
                new AsyncCallback<DateResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                        if (caught instanceof NoRecordsForAction) {
                            getFailMessage().addOrSetText("NoRecordsForAction");
                        }
                    }

                    @Override
                    public void onSuccess(DateResult result) {
                        if (checkOutCount == CheckInState.CHECK_OUT_ALL) {
                            parent.getData().setQuantityChecked(0);
                        } else {
                            int diff = parent.getData().getQuantityChecked() - checkOutCount;
                            int checked = diff < 0 ? 0 : diff;
                            parent.getData().setQuantityChecked(checked);
                        }

                        CheckDialog.this.hide();
                        parent.redrawItem();
                    }
                }, CheckDialog.this));
    }

    /** zobrazi dialog a spusti akci pro checkin, po uspesnem provedeni akce se dialog opet zavre */
    public void checkInAutomatic() {
        showCentered();
        checkIn(1);
    }

    /** zobrazi dialog a spusti akci pro checkout, po uspesnem provedeni akce se dialog opet zavre */
    public void checkOutAutomatic() {
        showCentered();
        checkOut(1);
    }
}
