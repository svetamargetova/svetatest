package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;

/**
 * Nacte editacni registraci podle uuid uzivatele
 * @author pepa
 */
public class LoadEditRegistrationByUserUuidAction extends EventAction<ClientRegistration> {

    private static final long serialVersionUID = -3403486115757007618L;
    // data
    private String userUuid;

    public LoadEditRegistrationByUserUuidAction() {
    }

    public LoadEditRegistrationByUserUuidAction(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getUserUuid() {
        return userUuid;
    }
}
