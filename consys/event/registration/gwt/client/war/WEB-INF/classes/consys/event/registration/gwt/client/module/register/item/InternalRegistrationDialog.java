package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.payment.PaymentProfilePanel;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import consys.event.registration.gwt.client.action.CreateInternalRegistrationAction;
import consys.event.registration.gwt.client.action.ListBundlesAndQuestionsForUserAction;
import consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.module.register.InternalRegistration;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Dialog pro provedeni interni registrace
 * @author pepa
 */
public class InternalRegistrationDialog extends SmartDialog implements CssStyles {

    // konstanty
    public static final String RADIO_INTERNAL_REGISTRATION = "radio_internal_registration";
    // komponenty
    private BaseForm form;
    private BaseForm formQuestions;
    private ConsysStringTextBox firstNameBox;
    private ConsysStringTextBox lastNameBox;
    private ConsysStringTextBox organizationBox;
    private ConsysStringTextBox emailBox;
    private PaymentProfilePanel profilePanel;
    private FlowPanel questionPart;
    private List<ConsysStringTextBox> answers;
    private ConsysFlowPanel itemsPanel;
    private FlowPanel contentPanel;
    // data
    private InternalRegistration parent;
    private InternalRegistrationOption selectedOption;

    public InternalRegistrationDialog(InternalRegistration parent) {
        this.parent = parent;
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        EventBus.fire(new EventDispatchEvent(new ListBundlesAndQuestionsForUserAction(), new AsyncCallback<ClientBundlesAndQuestions>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava event action executor
            }

            @Override
            public void onSuccess(ClientBundlesAndQuestions result) {
                fillQuestions(result.getQuestions());
                fillTickets(result.getBundles());
            }
        }, this));
    }

    @Override
    protected void generateContent(SimpleFormPanel panel) {
        firstNameBox = new ConsysStringTextBox(1, 128, ERMessageUtils.c.addManagedUserUI_text_firstName());
        lastNameBox = new ConsysStringTextBox(1, 128, ERMessageUtils.c.addManagedUserUI_text_lastName());
        organizationBox = new ConsysStringTextBox(1, 512, ERMessageUtils.c.addManagedUserUI_text_organization());
        emailBox = new ConsysStringTextBox(1, 255, UIMessageUtils.c.const_email());

        int row = 0;

        form = new BaseForm("170px");
        form.addStyleName(MARGIN_LEFT_20);
        form.addRequired(row++, ERMessageUtils.c.addManagedUserUI_text_firstName(), firstNameBox);
        form.addRequired(row++, ERMessageUtils.c.addManagedUserUI_text_lastName(), lastNameBox);
        form.addRequired(row++, ERMessageUtils.c.addManagedUserUI_text_organization(), organizationBox);
        form.addRequired(row++, UIMessageUtils.c.const_email(), emailBox);

        profilePanel = new PaymentProfilePanel(this, false);
        profilePanel.addStyleName(StyleUtils.MARGIN_LEFT_20);
        profilePanel.setProfile(new ClientUserPaymentProfile());

        formQuestions = new BaseForm("170px");

        questionPart = new FlowPanel();
        questionPart.addStyleName(MARGIN_LEFT_20);
        questionPart.setVisible(false);
        questionPart.add(StyleUtils.getPadder("15px"));
        questionPart.add(StyleUtils.getStyledLabel(ERMessageUtils.c.questionWidget_title(), FONT_16PX, FONT_BOLD));
        questionPart.add(StyleUtils.getPadder("15px"));
        questionPart.add(formQuestions);

        itemsPanel = new ConsysFlowPanel();
        itemsPanel.addStyleName(ERResourceUtils.css().panel());

        contentPanel = new FlowPanel();
        contentPanel.addStyleName(ERResourceUtils.css().contentPanel());
        panel.addWidget(contentPanel);

        drawBundles();
    }

    private void drawBundles() {
        contentPanel.clear();
        contentPanel.add(padder());
        contentPanel.add(itemsPanel);
        contentPanel.add(padder());
        contentPanel.add(bundleSelectControl());
        contentPanel.add(padder());
    }

    private void drawUserForm() {
        contentPanel.clear();
        contentPanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.internalRegistrationDialog_text_userInformation(), FONT_16PX, FONT_BOLD, MARGIN_LEFT_20));
        contentPanel.add(padder());
        contentPanel.add(form);
        contentPanel.add(padder());
        contentPanel.add(profilePanel);
        contentPanel.add(questionPart);
        contentPanel.add(padder());
        contentPanel.add(controlPanel());
        contentPanel.add(padder());
    }

    private SimplePanel padder() {
        return StyleUtils.getPadder("15px");
    }

    /** vykresli odpovedi */
    private void fillQuestions(ArrayList<ClientCustomQuestion> questions) {
        answers = new ArrayList<ConsysStringTextBox>();
        for (int i = 0; i < questions.size(); i++) {
            ClientCustomQuestion q = questions.get(i);
            ConsysStringTextBox qBox = new ConsysStringTextBox(q.isRequired() ? 1 : 0, 2048, q.getText());
            qBox.getElement().setId(q.getUuid());
            if (q.isRequired()) {
                formQuestions.addRequired(i, q.getText(), qBox);
            } else {
                formQuestions.addOptional(i, q.getText(), qBox);
            }
            answers.add(qBox);
        }
        if (!questions.isEmpty()) {
            questionPart.setVisible(true);
        }
    }

    private void fillTickets(ArrayList<ClientRegistrationPack> tickets) {
        if (tickets.isEmpty()) {
            // TODO: vypsat ze nejsou aktivni zadne balicky
        } else {
            RegistrationForm.sortBundles(tickets);
            for (ClientRegistrationPack pack : tickets) {
                InternalRegistrationOption item = new InternalRegistrationOption(pack, ECoMessageUtils.getCurrency(), this);
                itemsPanel.add(item);
            }
            // vycpavka kvuli ie, aby spravne prekreslil styly, kdyz je vlozen jen jeden RegistrationOption
            itemsPanel.add(StyleUtils.getStyledLabel(".", StyleUtils.TEXT_WHITE));
        }
    }

    private FlowPanel controlPanel() {
        ActionImage button = ActionImage.getConfirmButton(ERMessageUtils.c.internalRegistrationDialog_action_register());
        button.addStyleName(ERResourceUtils.css().button());
        button.addClickHandler(buttonClickHandler());

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), ERResourceUtils.css().cancel());
        cancel.setClickHandler(cancelClickHandler());

        FlowPanel fp = new FlowPanel();
        fp.addStyleName(ERResourceUtils.css().internalRegistrationDialog());
        fp.add(button);
        fp.add(cancel);
        fp.add(StyleUtils.clearDiv());
        return fp;
    }

    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                // TODO: kontrola vstupu
                if (!form.validate(getFailMessage())) {
                    return;
                }
                if (!profilePanel.doValidate(getFailMessage())) {
                    return;
                }
                if (!formQuestions.validate(getFailMessage())) {
                    return;
                }

                CreateInternalRegistrationAction action = new CreateInternalRegistrationAction();
                action.setFirstName(firstNameBox.getText());
                action.setLastName(lastNameBox.getText());
                action.setUserEmail(emailBox.getText());
                action.setOrganization(organizationBox.getText());
                action.setProfile(profilePanel.getClientPaymentProfile());

                // odpovedi
                if (answers != null && !answers.isEmpty()) {
                    for (ConsysStringTextBox a : answers) {
                        action.getAnswers().put(a.getElement().getId(), a.getText());
                    }
                }

                // vybrane objednavky
                for (ProductThumb pt : selectedOption.getOrderedItems()) {
                    action.getProducts().add(pt);
                }

                EventBus.fire(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        parent.reload();
                        hide();
                    }
                }, InternalRegistrationDialog.this));
            }
        };
    }

    private ClickHandler cancelClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        };
    }

    public void selectOption(InternalRegistrationOption item) {
        selectedOption = item;
        for (int i = 0; i < itemsPanel.getWidgetCount(); i++) {
            Widget w = itemsPanel.getWidget(i);
            if (w instanceof InternalRegistrationOption && !w.equals(item)) {
                ((InternalRegistrationOption) w).unselected();
            }
        }
    }

    private FlowPanel bundleSelectControl() {
        ActionImage button = ActionImage.getConfirmButton(ERMessageUtils.c.questionWidget_action_next());
        button.addStyleName(ECoResourceUtils.bundle().css().paymentDialogOk());
        button.addClickHandler(nextClickHandler());

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), StyleUtils.FLOAT_LEFT + " " + StyleUtils.MARGIN_TOP_3);
        cancel.setClickHandler(hideClickHandler());

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(button);
        controlPanel.add(cancel);
        controlPanel.add(StyleUtils.clearDiv("10px"));
        return controlPanel;
    }

    private ClickHandler nextClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (selectedOption == null) {
                    getFailMessage().setText(ERMessageUtils.c.internalRegistrationDialog_text_mustSelectTicket());
                } else {
                    drawUserForm();
                }
            }
        };
    }
}
