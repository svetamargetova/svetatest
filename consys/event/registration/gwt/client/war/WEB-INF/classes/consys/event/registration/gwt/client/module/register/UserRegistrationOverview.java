package consys.event.registration.gwt.client.module.register;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConfirmDialog;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.progress.Progress;
import consys.common.gwt.client.ui.comp.progress.ProgressItem;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.CancelRegistrationAction;
import consys.event.registration.gwt.client.action.LoadEditRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.event.NewOrderEvent;
import consys.event.registration.gwt.client.event.OrderCanceledEvent;
import consys.event.registration.gwt.client.module.register.item.BuyAdditionalProducts;
import consys.event.registration.gwt.client.module.register.item.RegistrationOrderPanel;
import consys.event.registration.gwt.client.module.register.item.RegistrationParticipantInfoPanel;
import consys.event.registration.gwt.client.module.register.item.UserAnswersAndWishs;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class UserRegistrationOverview extends RootPanel implements OrderCanceledEvent.Handler {

    // data
    private ClientRegistration registration;
    private ArrayList<RegistrationOrderPanel> orderPanelList;
    private String newOderEventToken;
    private String registrationUuid;
    private boolean anotherUser = false;

    private UserRegistrationOverview() {
        super(ERMessageUtils.c.registrationForm_title());
        orderPanelList = new ArrayList<RegistrationOrderPanel>();
    }

    public UserRegistrationOverview(String registrationUuid) {
        this();
        this.registrationUuid = registrationUuid;
        anotherUser = true;
    }

    public UserRegistrationOverview(ClientRegistration registration, boolean anotherUser) {
        this();
        this.registration = registration;
        this.anotherUser = anotherUser;
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(OrderCanceledEvent.TYPE, this);
        if (registrationUuid == null) {
            generateContent();
        } else {
            EventBus.fire(new EventDispatchEvent(new LoadEditRegistrationAction(registrationUuid),
                    new AsyncCallback<ClientRegistration>() {
                @Override
                public void onFailure(Throwable caught) {
                    // obecne chyby zpracovava eventactionexecutor
                }

                @Override
                public void onSuccess(ClientRegistration result) {
                    UserRegistrationOverview.this.registration = result;
                    generateContent();
                }
            }, this));
        }
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(OrderCanceledEvent.TYPE, this);
    }

    private boolean isOverallPriceZero() {
        boolean canBe = true;
        for (ClientRegistrationOrder order : registration.getRegistrationOrders()) {
            canBe &= order.getTotalPrice().isZero();
        }
        return canBe;
    }

    /** vygeneruje obsah */
    private void generateContent() {
        clear();

        if (anotherUser) {
            StringBuilder tt = new StringBuilder(ERMessageUtils.c.registrationForm_title());
            tt.append(" - ");
            tt.append(registration.getEventUser().getFullName());
            setTitleText(tt.toString());
        } else {
            setTitleText(ERMessageUtils.c.registrationModule_text_navigationModelMyRegistration());
        }

        final boolean isPaid = registration.getRegistrationOrders().get(0).getState().equals(ClientRegistration.State.CONFIRMED);

        // progress bar: Registaer - Pay - Enjoy
        addWidget(getProgress(isPaid));

        // separator
        addWidget(Separator.separator());

        // Participant info panel
        RegistrationParticipantInfoPanel info = new RegistrationParticipantInfoPanel(registration);

        addWidget(info);
        addWidget(StyleUtils.clearDiv());

        orderPanelList.clear();

        // prvni zahodit protoze je hlavni
        for (int i = 1; i < registration.getRegistrationOrders().size(); i++) {
            ClientRegistrationOrder cro = registration.getRegistrationOrders().get(i);
            RegistrationOrderPanel orderPanel = new RegistrationOrderPanel(cro, registration.getEventUser());
            orderPanel.getElement().setId(cro.getUuid());
            addWidget(orderPanel);
            orderPanelList.add(orderPanel);
        }

        BuyAdditionalProducts additionalProductsPanel = new BuyAdditionalProducts(registration);
        additionalProductsPanel.setAfterRegistrationAction(afterRegistrationAction());
        addWidget(additionalProductsPanel);

        // Cancel button len ak neni este hlavna registracia zaplatena
        boolean addCancelButton = false;

        if (isOverallPriceZero()) {
            addCancelButton = true;
        } else if (!isPaid) {
            addCancelButton = true;
        }

        if (addCancelButton) {
            clearControll();
            ActionImage cancel = ActionImage.getCrossButton(ERMessageUtils.c.registrationFormRegistered_button_cancelRegistration(), true);
            cancel.addClickHandler(cancelClickHandler());
            addRightControll(cancel);
        }

        addWidget(new UserAnswersAndWishs(registration));
    }

    /** clickhandler pro zruseni registrace */
    private ClickHandler cancelClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final ConfirmDialog dialog = new ConfirmDialog(ERMessageUtils.c.registrationFormRegistered_cancel_registration_question_title());
                ConsysAction action = new ConsysAction() {
                    @Override
                    public void run() {
                        EventDispatchEvent cancelEvent = new EventDispatchEvent(
                                new CancelRegistrationAction(registration.getRegistrationOrders().get(0).getUuid(), true),
                                new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne vyjimky zpracovava EventActionExecuter
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionCancelRegistrationNoRecords());
                                }
                                dialog.hide();
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                if (anotherUser) {
                                    FormUtils.fireBreadcrumbBack();
                                } else {
                                    RegistrationForm w = new RegistrationForm(null);
                                    FormUtils.fireSameBreadcrumb(ERMessageUtils.c.registrationForm_title(), w);
                                }
                                dialog.hide();
                            }
                        }, UserRegistrationOverview.this);
                        EventBus.get().fireEvent(cancelEvent);
                    }
                };
                dialog.setAction(action);
                dialog.showCentered();
            }
        };
    }

    /** akce po uspesnem zaregistrovani dalsich produktu */
    protected ConsysActionWithValue<ClientRegistration> afterRegistrationAction() {
        return new ConsysActionWithValue<ClientRegistration>() {
            @Override
            public void run(ClientRegistration data) {
                UserRegistrationOverview w = new UserRegistrationOverview(data, anotherUser);
                FormUtils.fireSameBreadcrumb(ERMessageUtils.c.registrationForm_title(), w);
                EventBus.get().fireEvent(new NewOrderEvent(newOderEventToken));
            }
        };
    }

    @Override
    public void onOrderCanceled(OrderCanceledEvent event) {
        ClientRegistrationOrder canceledOrder = event.getOrder();
        // odstranime z DOMu
        for (RegistrationOrderPanel order : orderPanelList) {
            if (order.getOrderUuid().equals(canceledOrder.getUuid())) {
                order.removeFromParent();
                break;
            }
        }
        // Odstranime z registracie
        registration.getRegistrationOrders().remove(canceledOrder);
    }

    /** vraci progress pro zaplaceny nebo nezaplaceny stav */
    private Progress getProgress(boolean isPaid) {
        Progress progress = new Progress();
        progress.addStyleName(MARGIN_TOP_10);
        progress.addStyleName(MARGIN_BOT_20);

        progress.addProgressItem(new ProgressItem(false, 1, ERMessageUtils.c.registrationForm_progress_registerUpper()));
        progress.addSeparator();
        progress.addProgressItem(new ProgressItem(!isPaid, 2, ERMessageUtils.c.registrationForm_progress_payUpper()));
        progress.addSeparator();
        progress.addProgressItem(new ProgressItem(isPaid, 3, ERMessageUtils.c.registrationForm_progress_enjoyUpper()));

        return progress;
    }

    /**
     * @return the newOderEventToken
     */
    public String getNewOderEventToken() {
        return newOderEventToken;
    }

    /**
     * @param newOderEventToken the newOderEventToken to set
     */
    public void setNewOderEventToken(String newOderEventToken) {
        this.newOderEventToken = newOderEventToken;
    }
}
