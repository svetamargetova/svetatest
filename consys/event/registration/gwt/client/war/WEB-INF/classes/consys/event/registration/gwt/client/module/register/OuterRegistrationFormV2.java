package consys.event.registration.gwt.client.module.register;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.constants.img.EventProfileImageEnum;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.CreateOuterRegisterAction;
import consys.common.gwt.client.rpc.action.InitializeSsoAction;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.action.ActionImageBig;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.common.gwt.client.ui.comp.panel.InfoPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.client.widget.exception.UserExistsException;
import consys.common.gwt.client.widget.utils.CWMessageUtils;
import consys.common.gwt.shared.bo.ClientEventThumb;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.common.gwt.shared.exception.*;
import consys.event.common.gwt.client.action.LoadOuterEventInfoAction;
import consys.event.common.gwt.client.bo.ClientOuterEventData;
import consys.event.common.gwt.client.event.EventOverseerInitializationEvent;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import consys.event.registration.api.navigation.Navigation;
import consys.event.registration.gwt.client.module.register.outer.OuterFooter;
import consys.event.registration.gwt.client.module.register.outer.OuterLayout;
import consys.event.registration.gwt.client.module.register.outer.OuterResources;
import consys.event.registration.gwt.client.module.register.outer.comp.*;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 * Novy formular vnejsi registrace
 * @author pepa
 */
public class OuterRegistrationFormV2 implements ActionExecutionDelegate {

    // konstanty
    private static Logger logger = LoggerFactory.getLogger(OuterRegistrationFormV2.class);
    // konstanty - get
    public static final String PARAM_COMMAND = "cmd";
    public static final String PARAM_COMMAND_VALUE = "Register";
    public static final String PARAM_API_KEY = "api_key";
    public static final String PARAM_EVENT_UUID = "eid";
    public static final String PARAM_PACKAGE_UUID = "pid";
    public static final String PARAM_NEW_USER = "newUser";
    public static final String PARAM_BUNDLE_UUID = "bundleUuid";
    public static final String PARAM_HEADER_FOOTER = "takeplaceHeaderFooter";
    // komponenty
    private OuterUserInfo outerUserInfo;
    private OuterQuestions outerQuestions;
    private OuterInvoiceInfo outerInvoiceInfo;
    private OuterBundle bundle;
    private SimplePanel registerFailPanel;
    private SimplePanel portraitPanel;
    private SimplePanel bundleFailPanel;
    private SimplePanel middleBottomSeparator;
    private FlowPanel registerPart;
    // data
    private String apiKey;
    private String eventUuid;
    private String packageUuid;
    private String synchroToken;

    /** pripravi layout */
    public void prepare() {
        RootPanel.get().clear();

        OuterResources.INSTANCE.css().ensureInjected();

        boolean headerFooter = true;
        if (ParamUtils.isSet(PARAM_HEADER_FOOTER)) {
            String value = ParamUtils.getValue(PARAM_HEADER_FOOTER);
            headerFooter = value.equalsIgnoreCase("yes");
        }

        RootPanel.get().add(OuterLayout.get(headerFooter));
        if (headerFooter) {
            RootPanel.get().add(new OuterFooter());
        }
    }

    /** zacne zpracovavat */
    public void start() {
        if (processParameters()) {
            loadEventData();
        }
    }

    private void loadEventData() {
        EventBus.fire(new DispatchEvent(new LoadOuterEventInfoAction(eventUuid, true),
                new AsyncCallback<ClientOuterEventData>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne zpracovava action executor
                        if (caught instanceof NoRecordsForAction) {
                            OuterLayout.get().getFailMessage().setText(ERMessageUtils.c.outerRegistrationForm_error_eventNotFound());
                        }
                    }

                    @Override
                    public void onSuccess(final ClientOuterEventData result) {
                        logger.debug("Event info loaded");
                        // inicializujeme overseer dispatch service
                        EventBus.fire(new EventOverseerInitializationEvent(result.getUuid(), null, result.getOverseer()));

                        EventBus.fire(new DispatchEvent(new InitializeSsoAction(), new AsyncCallback<StringResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby bude zpracovavat action executor
                                // nic nevyhazuje
                            }

                            @Override
                            public void onSuccess(StringResult r) {
                                // vygenerujeme obsah layoutu
                                OuterLayout.get().setContent(generate(result, r.getStringResult()));
                            }
                        }, OuterLayout.get()));
                    }
                }, OuterLayout.get()));
    }

    private boolean processParameters() {
        // nejdrive zkontrolujeme jestli jsou nastaveny get parametry
        if (ParamUtils.isSetAll(PARAM_COMMAND, PARAM_API_KEY, PARAM_EVENT_UUID, PARAM_PACKAGE_UUID)) {
            // nasli jsme vsechny GET parametry
            if (ParamUtils.getValue(PARAM_COMMAND).endsWith(PARAM_COMMAND_VALUE)) {
                // command odpovida pozadovane hodnote, nasetujeme promenne

                apiKey = ParamUtils.getValue(PARAM_API_KEY);
                logger.info("Found API KEY " + apiKey);

                eventUuid = ParamUtils.getValue(PARAM_EVENT_UUID);
                logger.info("Found Event UUID " + eventUuid);

                packageUuid = ParamUtils.getValue(PARAM_PACKAGE_UUID);
                logger.info("Found Bundle UUID " + packageUuid);
            }
        } else {
            // pokud nejsou zkusime jestli jsou nastaveny history parametry
            HashMap<String, String> params = HistoryUtils.getParameters();

            for (Entry<String, String> e : params.entrySet()) {
                if (PARAM_API_KEY.equalsIgnoreCase(e.getKey())) {
                    apiKey = e.getValue();
                    logger.info("Found API KEY " + apiKey);
                }
                if (PARAM_EVENT_UUID.equalsIgnoreCase(e.getKey())) {
                    eventUuid = e.getValue();
                    logger.info("Found Event UUID " + eventUuid);
                }
                if (PARAM_PACKAGE_UUID.equalsIgnoreCase(e.getKey())) {
                    packageUuid = e.getValue();
                    logger.info("Found Bundle UUID " + packageUuid);
                }
            }
        }

        boolean out = false;

        if (apiKey == null) {
            OuterLayout.get().getFailMessage().setText(UIMessageUtils.m.const_missingParameter(PARAM_API_KEY));
        } else if (eventUuid == null) {
            OuterLayout.get().getFailMessage().setText(UIMessageUtils.m.const_missingParameter(PARAM_EVENT_UUID));
        } else if (packageUuid == null) {
            OuterLayout.get().getFailMessage().setText(UIMessageUtils.m.const_missingParameter(PARAM_PACKAGE_UUID));
        } else {
            out = true;
        }
        return out;
    }

    private Widget generate(ClientOuterEventData data, String synchroToken) {
        this.synchroToken = synchroToken;

        FlowPanel allParts = new FlowPanel();
        allParts.add(partTop(data));
        allParts.add(partMiddle(data));
        allParts.add(partBottom(data));

        return allParts;
    }

    private Widget partTop(ClientEventThumb data) {
        Image logo = FormUtils.eventProfileLogo(data.getProfileLogoUuidPrefix(), EventProfileImageEnum.WEB);
        logo.setAltText(data.getFullName());
        logo.setTitle(data.getFullName());
        SimplePanel logoPanel = FormUtils.createPanel(OuterResources.INSTANCE.css().eventLogo());
        logoPanel.setWidget(logo);

        FlowPanel descriptionPanel = new FlowPanel();
        descriptionPanel.setStyleName(OuterResources.INSTANCE.css().descriptionPanel());
        descriptionPanel.add(shortedDescriptionIfNeed(data.getDescription()));
        descriptionPanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.outerRegistrationForm_text_providedByTakeplace(),
                OuterResources.INSTANCE.css().providedByTakeplace()));

        FlowPanel part = new FlowPanel();
        part.setStyleName(OuterResources.INSTANCE.css().centerPanel());
        part.add(StyleUtils.getPadder("30px"));
        part.add(StyleUtils.getStyledLabel(data.getFullName(), OuterResources.INSTANCE.css().title()));
        part.add(logoPanel);
        part.add(descriptionPanel);
        part.add(StyleUtils.clearDiv());

        return part;
    }

    private FlowPanel shortedDescriptionIfNeed(String description) {
        final HTML descriptionHTML = new HTML(description);
        descriptionHTML.setStyleName(ECoResourceUtils.bundle().css().eventDescription());

        final FlowPanel wrapper = new FlowPanel();
        final FlowPanel descriptionPanel = new FlowPanel();
        descriptionPanel.setWidth("550px");
        descriptionPanel.setVisible(false);
        descriptionPanel.addStyleName(OuterResources.INSTANCE.css().eventDescriptionOverflow());
        descriptionPanel.add(descriptionHTML);

        final ActionLabel showDetails = new ActionLabel(ERMessageUtils.c.outerRegistrationFormV2_action_showDescription());
        showDetails.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                descriptionPanel.setVisible(true);
                showDetails.removeFromParent();
            }
        });

        wrapper.add(showDetails);
        wrapper.add(descriptionPanel);
        return wrapper;
    }

    private Widget partMiddle(ClientEventThumb data) {
        String url = eventRegisterUrl(data.getUuid());
        HTML html = new HTML(ERMessageUtils.m.outerRegistrationForm_text_alreadyTakeplaceMember(url));
        html.setStyleName(OuterResources.INSTANCE.css().htmlStyle());

        FlowPanel panel = new FlowPanel();
        panel.add(loginAndRegisterPart());
        panel.add(questionPart());
        panel.add(invoicePart());
        panel.add(StyleUtils.clearDiv());

        OuterMiddlePart part = new OuterMiddlePart();
        part.setHead(html);
        part.setContent(panel);
        return part;
    }

    private Widget loginAndRegisterPart() {
        FormV2 formServices = new FormV2();
        SimplePanel userDetailPanel = new SimplePanel();

        portraitPanel = FormUtils.createPanel(OuterResources.INSTANCE.css().portraitPanel());

        OuterLogin login = new OuterLogin(synchroToken);
        login.setUserDetailAction(userDetailAction(userDetailPanel));

        Widget title = FormV2.getTitleWidget(ERMessageUtils.c.outerRegistrationForm_text_register(),
                ERMessageUtils.c.outerRegistrationForm_text_registerHelp(), true, "");
        title.addStyleName(OuterResources.INSTANCE.css().formTitleBig());

        formServices.add(title, StyleUtils.clearDiv("20px"), true);
        formServices.add(portraitPanel, login, false);

        FlowPanel panel = new FlowPanel();
        panel.add(formServices);
        panel.add(userDetailPanel);
        return panel;
    }

    private Widget questionPart() {
        outerQuestions = new OuterQuestions();
        return outerQuestions;
    }

    private Widget invoicePart() {
        outerInvoiceInfo = new OuterInvoiceInfo();
        outerInvoiceInfo.setVisible(false);
        return outerInvoiceInfo;
    }

    private ConsysActionWithValue<ClientSsoUserDetails> userDetailAction(final SimplePanel panel) {
        return new ConsysActionWithValue<ClientSsoUserDetails>() {
            @Override
            public void run(ClientSsoUserDetails userDetails) {
                if (userDetails != null && StringUtils.isNotEmpty(userDetails.getProfilePictureUrl())) {
                    Image image = new Image(userDetails.getProfilePictureUrl());
                    image.setWidth("115px");
                    portraitPanel.setWidget(image);
                } else {
                    portraitPanel.setWidget(null);
                }

                outerUserInfo = new OuterUserInfo(panel, userDetails);
            }
        };
    }

    private Widget partBottom(ClientOuterEventData data) {
        bundle = new OuterBundle(packageUuid, outerInvoiceInfo, this);
        bundle.setVisible(data.isActive());

        bundleFailPanel = new SimplePanel();
        registerFailPanel = new SimplePanel();

        if (!data.isActive()) {
            showFailTextInPlaceOfBundles(ERMessageUtils.c.outerRegistrationForm_error_eventNotActivated());
        }

        middleBottomSeparator = FormUtils.createPanel(OuterResources.INSTANCE.css().middleBottomSeparator());
        middleBottomSeparator.setVisible(data.isActive());

        ActionImageBig register = ActionImageBig.blue(ERMessageUtils.c.outerRegistrationForm_text_register());
        register.setClickHandler(registerClickHandler());
        register.addStyleName(OuterResources.INSTANCE.css().registerButton());

        HTML agreeing = new HTML(ERMessageUtils.c.outerRegisterContent_text_agreeing());
        agreeing.setStyleName(OuterResources.INSTANCE.css().agreeing());
        agreeing.addStyleName(OuterResources.INSTANCE.css().htmlStyle());

        registerPart = new FlowPanel();
        registerPart.setVisible(data.isActive());
        registerPart.add(register);
        registerPart.add(agreeing);
        registerPart.add(StyleUtils.clearDiv("30px"));

        FlowPanel part = new FlowPanel();
        part.setStyleName(OuterResources.INSTANCE.css().centerPanel());
        part.add(StyleUtils.getPadder("30px"));
        part.add(bundle);
        part.add(bundleFailPanel);
        part.add(middleBottomSeparator);
        part.add(registerFailPanel);
        part.add(registerPart);
        return part;
    }

    private ClickHandler registerClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (outerUserInfo == null) {
                    getRegisterFail().setText(ERMessageUtils.c.outerRegistrationForm_error_userInfoNotEntered());
                    return;
                }

                boolean valid = true;

                // validace dat uzivatele
                valid &= outerUserInfo.validate();

                // validace organizatorovych otazek otazek
                valid &= outerQuestions.validate();

                if (outerInvoiceInfo.isVisible()) {
                    // validace platebnich informaci
                    valid &= outerInvoiceInfo.validate();
                }
                if (!valid) {
                    // platebni informace
                    return;
                }

                CreateOuterRegisterAction registerAction = new CreateOuterRegisterAction();
                registerAction.setApiKey(apiKey);
                registerAction.setEventUuid(eventUuid);
                registerAction.setRegistration(outerUserInfo.getUserDetails());
                registerAction.setPaymentProfile(outerInvoiceInfo.getPaymentProfile());

                List<ProductThumb> orderedItems = bundle.getOrderedItems();
                registerAction.setPackageUuid(orderedItems.get(0).getProductUuid());
                registerAction.setDiscountTicket(orderedItems.get(0).getDiscountCode());
                registerAction.setQuantity(orderedItems.get(0).getQuantity());
                orderedItems.remove(0);
                registerAction.getSubbundles().addAll(orderedItems);

                List<ConsysTextBox> boxs = outerQuestions.getBoxs();
                HashMap<String, String> answers = new HashMap<String, String>();
                for (ConsysTextBox box : boxs) {
                    answers.put(box.getElement().getId(), box.getText());
                }
                registerAction.setAnswers(answers);

                EventBus.fire(new DispatchEvent(registerAction, new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        processRegisterException(caught);
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        Window.Location.replace(eventRegisterUrl(eventUuid));
                        //Window.open(eventRegisterUrl(eventUuid), "_blank", "");
                    }
                }, OuterRegistrationFormV2.this));
            }
        };
    }

    /** vygeneruje url vedouci do registrace eventu uvnitr takeplace */
    private String eventRegisterUrl(String uuid) {
        String url = URLFactory.eventUrl(uuid, LocaleUtils.getLocale());
        url = URLFactory.appendNextParam(url, "page", Navigation.REGISTER);
        return url;
    }

    private void processRegisterException(Throwable caught) {
        // obecne chyby zpracovava action executor
        if (caught instanceof BundleCapacityException) {
            showFailTextInPlaceOfBundles(ERMessageUtils.c.outerRegistrationForm_error_registrationIsClosedDue());
            bundle.setVisible(false);
        } else if (caught instanceof CouponInvalidException) {
            getRegisterFail().setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponInvalidException());
        } else if (caught instanceof CouponUsedException) {
            getRegisterFail().setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponUsedException());
        } else if (caught instanceof CouponExpiredException) {
            getRegisterFail().setText(ERMessageUtils.c.registrationFormItem_discount_exception_CouponExpiredException());
        } else if (caught instanceof NoRecordsForAction) {
            getRegisterFail().setText(ERMessageUtils.c.outerRegisterContent_error_noRecorsForParameterValues());
        } else if (caught instanceof UserExistsException) {
            getRegisterFail().setText(CWMessageUtils.c.registerContent_error_alreadyRegistered());
        } else if (caught instanceof IbanException) {
            getRegisterFail().setText(UIMessageUtils.c.const_exceptionBadIban());
        }
    }

    /** zobrazi chybovou zpravu misto prehledu balicku */
    private void showFailTextInPlaceOfBundles(String failText) {
        bundleFailPanel.setWidget(new InfoPanel(failText));
    }

    /** zobrazeni pro stav registrace uzavrena */
    public void registrationClosedState() {
        errorStateWithoutBundlesAndRegister(ERMessageUtils.c.outerRegistrationForm_error_registrationIsClosed());
    }

    public void registrationTicketFull(String failText) {
        errorStateWithoutBundlesAndRegister(failText);
    }

    private void errorStateWithoutBundlesAndRegister(String failText) {
        showFailTextInPlaceOfBundles(failText);
        bundle.setVisible(false);
        middleBottomSeparator.setVisible(false);
        registerPart.setVisible(false);
    }

    private ConsysMessage getRegisterFail() {
        return FormUtils.failMessage(registerFailPanel, OuterResources.INSTANCE.css().fail());
    }

    @Override
    public void actionStarted() {
        OuterLayout.get().actionStarted();
    }

    @Override
    public void actionEnds() {
        OuterLayout.get().actionEnds();
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getRegisterFail().setText(UIMessageUtils.getFailMessage(value));
    }
}
