package consys.event.registration.gwt.client.module.register.item;

import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class AddManagedUserUI extends NewItemUI {

    // komponenty
    private ConsysStringTextBox firstNameBox;
    private ConsysStringTextBox lastNameBox;
    private ConsysStringTextBox organizationBox;

    @Override
    public void createForm(BaseForm form) {
        firstNameBox = new ConsysStringTextBox(1, 128, ERMessageUtils.c.addManagedUserUI_text_firstName());
        lastNameBox = new ConsysStringTextBox(1, 128, ERMessageUtils.c.addManagedUserUI_text_lastName());
        organizationBox = new ConsysStringTextBox(1, 512, ERMessageUtils.c.addManagedUserUI_text_organization());

        form.addRequired(0, ERMessageUtils.c.addManagedUserUI_text_firstName(), firstNameBox);
        form.addRequired(1, ERMessageUtils.c.addManagedUserUI_text_lastName(), lastNameBox);
        form.addRequired(2, ERMessageUtils.c.addManagedUserUI_text_organization(), organizationBox);
    }

    @Override
    public void focus() {
        firstNameBox.setFocus(true);
    }

    public String getFirstName() {
        return firstNameBox.getText();
    }

    public String getLastName() {
        return lastNameBox.getText();
    }

    public String getOrganization() {
        return organizationBox.getText();
    }
}
