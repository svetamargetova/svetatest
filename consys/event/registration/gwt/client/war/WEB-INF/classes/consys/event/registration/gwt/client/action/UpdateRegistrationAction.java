package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro aktualizaci registrace
 * @author pepa
 */
public class UpdateRegistrationAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 6444346549682042014L;
    // data
    private String registrationUuid;
    private String note;    
    private boolean admin;

    public UpdateRegistrationAction() {
    }

    public UpdateRegistrationAction(String registrationUuid, String note, boolean adminNote) {
        this.registrationUuid = registrationUuid;
        this.note = note;
        this.admin = adminNote;
    }

    public String getNote() {
        return note;
    }

    public boolean isAdmin() {
        return admin;
    }        

    public String getRegistrationUuid() {
        return registrationUuid;
    }
}
