package consys.event.registration.gwt.client.module;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.module.event.EventNavigationItem;
import consys.common.gwt.client.module.event.EventNavigationItemVisiblityResolver;
import consys.common.gwt.client.module.event.EventNavigationModel;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.ui.CommonHistoryHandler;
import consys.common.gwt.client.ui.CommonHistoryItem;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.ArrayUtils;
import consys.event.common.api.right.Role;
import consys.event.common.gwt.client.event.CheckUserIsRegistered;
import consys.event.common.gwt.client.event.EventRegistrationRequestEvent;
import consys.event.registration.api.navigation.Navigation;
import consys.event.registration.api.properties.RegistrationProperties;
import consys.event.registration.api.right.EventRegistrationModuleRoleModel;
import consys.event.registration.gwt.client.action.LoadIsUserRegisteredAction;
import consys.event.registration.gwt.client.module.lists.CheckInList;
import consys.event.registration.gwt.client.module.lists.ParticipantList;
import consys.event.registration.gwt.client.module.register.InternalRegistration;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import consys.event.registration.gwt.client.module.register.OuterRegistrationForm;
import consys.event.registration.gwt.client.module.register.OuterRegistrationFormV2;
import consys.event.registration.gwt.client.module.register.RegisterAnother;
import consys.event.registration.gwt.client.module.setting.RegistrationSettingsBundle;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.List;
import java.util.Map;

/** 
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationModule implements EventModule, EventRegistrationRequestEvent.Handler, CheckUserIsRegistered.Handler {

    @Override
    public String getLocalizedModuleName() {
        return ERMessageUtils.c.registrationModule_text_moduleName();
    }

    @Override
    public void registerModuleRoles(List<ModuleRole> moduleRoles) {
        EventRegistrationModuleRoleModel roles = new EventRegistrationModuleRoleModel();
        Map<String, Role> rightMap = roles.getModelInMap();

        // Registration Chair
        ModuleRole registrationChair = new ModuleRole();
        registrationChair.setId(rightMap.get(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_SETTINGS).getIdentifier());
        registrationChair.setSystem(rightMap.get(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_SETTINGS).isSystem());
        registrationChair.setShortcut(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_SETTINGS);
        registrationChair.setLocalizedName(ERMessageUtils.c.registrationModule_text_moduleRoleChair());
        registrationChair.setLocalizedDescription(ERMessageUtils.c.registrationModule_text_moduleRoleChairDescription());
        moduleRoles.add(registrationChair);

        // Registration Administrator
        ModuleRole registrationAdministrator = new ModuleRole();
        registrationAdministrator.setId(rightMap.get(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR).getIdentifier());
        registrationAdministrator.setSystem(rightMap.get(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR).isSystem());
        registrationAdministrator.setShortcut(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR);
        registrationAdministrator.setLocalizedName(ERMessageUtils.c.registrationModule_text_moduleRoleAdministrator());
        registrationAdministrator.setLocalizedDescription(ERMessageUtils.c.registrationModule_text_moduleRoleAdministratorDescription());
        moduleRoles.add(registrationAdministrator);

        // Check in
        ModuleRole checkInRole = new ModuleRole();
        checkInRole.setId(rightMap.get(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_CHECK_IN).getIdentifier());
        checkInRole.setSystem(rightMap.get(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_CHECK_IN).isSystem());
        checkInRole.setShortcut(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_CHECK_IN);
        checkInRole.setLocalizedName(ERMessageUtils.c.registrationModule_text_moduleCheckIn());
        checkInRole.setLocalizedDescription(ERMessageUtils.c.registrationModule_text_moduleCheckInDescription());
        moduleRoles.add(checkInRole);
    }
    public static final String NAVIGATION_PARENT_REGISTRATION = ERMessageUtils.c.registrationModule_text_navigationModelRegistration();

    @Override
    public void registerEventNavigation(EventNavigationModel model) {
        // Parent pre submuission module 30
        EventNavigationItem registration = new EventNavigationItem(NAVIGATION_PARENT_REGISTRATION, 30, new ConsysAction() {
            @Override
            public void run() {
                String title = ERMessageUtils.c.registrationModule_text_navigationModelRegistrationSettings();
                FormUtils.fireNextRootBreadcrumb(title, new RegistrationSettingsBundle(false));
            }
        }, true, EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR);

        model.insertFirstLevelAddon(registration, UIMessageUtils.c.const_settings(), 5, new ConsysAction() {
            @Override
            public void run() {
                String title = ERMessageUtils.c.registrationModule_text_navigationModelRegistrationSettings();
                FormUtils.fireNextRootBreadcrumb(title, new RegistrationSettingsBundle(false));
            }
        }, EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR);

        // Register [7] - Registration administrator
        model.insertFirstLevelAddon(registration, ERMessageUtils.c.internalRegistration_title(), 7, new ConsysAction() {
            @Override
            public void run() {
                String title = ERMessageUtils.c.internalRegistration_title();
                FormUtils.fireNextRootBreadcrumb(title, new InternalRegistration());
            }
        }, EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR);

        // Register [10] - Everyone
        model.insertFirstLevelAddon(registration, ERMessageUtils.c.registrationModule_text_navigationModelMyRegistration(), Navigation.REGISTER, 10, new ConsysAction() {
            @Override
            public void run() {
                String title = ERMessageUtils.c.registrationModule_text_navigationModelMyRegistration();
                FormUtils.fireNextRootBreadcrumb(title, new RegistrationForm(null));
            }
        }, ArrayUtils.EMPTY_STRING_ARRAY);

        // Register another [15] - everyone - if allowed
        /*model.insertFirstLevelAddon(registration, ERMessageUtils.c.registerAnother_title(), Navigation.REGISTER_ANOTHER, 15, new ConsysAction() {
         @Override
         public void run() {
         String title = ERMessageUtils.c.registerAnother_title();
         FormUtils.fireNextRootBreadcrumb(title, new RegisterAnother());
         }
         }, new EventNavigationItemVisiblityResolver() {
         @Override
         public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
         String value = systemProperties.get(RegistrationProperties.REGISTER_ANOTHER);
         return value.equalsIgnoreCase("true");
         }
         });*/

        // Participant List [30] - everyone
        model.insertFirstLevelAddon(registration, ERMessageUtils.c.registrationModule_text_navigationModelParticipantList(), Navigation.PARTICIPANT_LIST, 30, new ConsysAction() {
            @Override
            public void run() {
                String title = ERMessageUtils.c.registrationModule_text_navigationModelParticipantList();
                FormUtils.fireNextRootBreadcrumb(title, new ParticipantList());
            }
        }, new EventNavigationItemVisiblityResolver() {
            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                // Ak je system property participant list visible true tak vracame true inak kontrolujeme na prava
                String value = systemProperties.get(RegistrationProperties.PARTICIPANT_LIST_VISIBILE);
                if (value.equalsIgnoreCase("true")) {
                    return true;
                } else {
                    // viditelnost participant listu je zavisla na tom ci ma user prava
                    return rights.contains(EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR);
                }
            }
        });

        // Checkin List [40] - everyone
        model.insertFirstLevelAddon(registration, ERMessageUtils.c.registrationModule_text_navigationModelCheckInList(), 40, new ConsysAction() {
            @Override
            public void run() {
                String title = ERMessageUtils.c.registrationModule_text_navigationModelCheckInList();
                FormUtils.fireNextRootBreadcrumb(title, new CheckInList());
            }
            // TODO: pravo checkin
        }, EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_ADMINISTRATOR, EventRegistrationModuleRoleModel.RIGHT_REGISTRATION_CHECK_IN);

    }
    /** pro zobrazeni formu pro potvrzeni registrace do eventu */
    public static final String REGISTER = "Register";
    public static final String REGISTERV2 = "RegisterV2";

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
        EventBus.get().addHandler(EventRegistrationRequestEvent.TYPE, this);

        CommonHistoryHandler.get().registerOutToken(REGISTER, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                return new OuterRegistrationForm();
            }
        });

        CommonHistoryHandler.get().registerOutToken(REGISTERV2, new CommonHistoryItem() {
            @Override
            public Widget createInstace() {
                OuterRegistrationFormV2 outerRegistrationFormV2 = new OuterRegistrationFormV2();
                return new SimplePanel();

            }
        });

        EventBus.get().addHandler(CheckUserIsRegistered.TYPE, this);
    }

    @Override
    public void registerModule() {
    }

    @Override
    public void unregisterModule() {
    }

    @Override
    public void onEventRegister(EventRegistrationRequestEvent event) {
        RegistrationForm w = new RegistrationForm(event.getBundleUuid());
        String title = ERMessageUtils.c.registrationModule_text_navigationModelRegister();
        FormUtils.fireNextRootBreadcrumb(title, w);
    }

    @Override
    public void isUserRegistered(final CheckUserIsRegistered event) {
        EventBus.fire(new EventDispatchEvent(new LoadIsUserRegisteredAction(),
                new AsyncCallback<BooleanResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // neco se pokazilo ale s tim nic nenadelame a tlacitko skryjeme
                        event.getButton().setVisible(false);
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        event.getButton().setVisible(result.isBool() || event.isInRange());
                        if (result.isBool()) {
                            event.getButton().setText(ERMessageUtils.c.registrationModule_text_navigationModelMyRegistration());
                        }
                    }
                }, null));
    }
}
