package consys.event.registration.gwt.client.module.register.outer.comp;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.common.gwt.client.ui.comp.input.ValidableItem;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.registration.gwt.client.action.ListQuestionsForUserAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.bo.ClientQuestions;
import consys.event.registration.gwt.client.module.register.outer.OuterResources;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * Zobrazuje otazky, ktere si vymyslel organizator
 * @author pepa
 */
public class OuterQuestions extends Composite {

    // komponenty
    private FlowPanel mainPanel;
    private FormV2 form;
    // data
    private List<ConsysTextBox> boxs;

    public OuterQuestions() {
        OuterResources.INSTANCE.css().ensureInjected();

        mainPanel = new FlowPanel();
        initWidget(mainPanel);

        boxs = new ArrayList<ConsysTextBox>();
    }

    @Override
    protected void onLoad() {
        mainPanel.clear();
        EventBus.fire(new EventDispatchEvent(new ListQuestionsForUserAction(), new AsyncCallback<ClientQuestions>() {
            @Override
            public void onFailure(Throwable caught) {
                mainPanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.outerQuestions_error_loadQuestions(), StyleUtils.TEXT_RED));
            }

            @Override
            public void onSuccess(ClientQuestions result) {
                generate(result.getQuestions());
            }
        }, null));
    }

    private void generate(ArrayList<ClientCustomQuestion> data) {
        if (data.isEmpty()) {
            // neni co vykreslovat, nezobrazime
            return;
        }

        Widget title = FormV2.getTitleWidget(ERMessageUtils.c.questionWidget_title(), null, true, "");
        title.addStyleName(OuterResources.INSTANCE.css().formTitleBig());

        boxs.clear();

        form = new FormV2();
        form.add(title, StyleUtils.clearDiv("20px"), true);

        for (ClientCustomQuestion question : data) {
            ConsysTextBox box = new ConsysTextBox(question.isRequired() ? 1 : 0, 2048);
            box.getElement().setId(question.getUuid());
            form.add(question.getText(), new ValidableItem(box), question.isRequired());
            boxs.add(box);
        }

        mainPanel.add(FormUtils.createPanel(OuterResources.INSTANCE.css().middlePartSeparator()));
        mainPanel.add(form);
    }

    public boolean validate() {
        if (form != null) {
            return form.validate();
        }
        return true;
    }

    public List<ConsysTextBox> getBoxs() {
        return boxs;
    }
}
