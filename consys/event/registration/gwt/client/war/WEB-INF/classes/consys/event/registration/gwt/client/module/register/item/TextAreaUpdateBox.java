package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.HiddableLabel;
import static consys.common.gwt.client.ui.utils.CssStyles.FLOAT_LEFT;
import static consys.common.gwt.client.ui.utils.CssStyles.MARGIN_RIGHT_5;
import consys.common.gwt.client.ui.utils.LabelColorEnum;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 *
 * @author pepa
 */
public class TextAreaUpdateBox extends FlowPanel {

    private ActionImage updateSW;
    private HiddableLabel successUpdateLabel;
    private String title;
    private ConsysTextArea updateSWTextArea;

    public TextAreaUpdateBox(String title, String placeholder, String content) {
        this.title = title;
        setStyleName(ERResourceUtils.bundle().css().textAreaUpdateBox());

        updateSWTextArea = new ConsysTextArea(0, 10000, "350px", "70px", false, null);
        updateSWTextArea.setEmptyText(placeholder);
        updateSWTextArea.setText(content);

        successUpdateLabel = new HiddableLabel();
        successUpdateLabel.getElement().getStyle().setDisplay(Style.Display.INLINE);

        updateSW = ActionImage.getCheckButton(UIMessageUtils.c.const_update());
        updateSW.addStyleName(FLOAT_LEFT);
        updateSW.addStyleName(MARGIN_RIGHT_5);

        updateSWTextArea.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (!updateSW.isEnabled()) {
                    updateSW.setEnabled(true);
                } else if (StringUtils.isEmpty(updateSWTextArea.getText())) {
                    updateSW.setEnabled(false);
                }
            }
        });
        updateSW.setEnabled(false);

        FlowPanel actionPanel = new FlowPanel();
        actionPanel.add(updateSW);
        actionPanel.add(successUpdateLabel);
        actionPanel.add(StyleUtils.clearDiv());

        add(StyleUtils.getStyledLabel(title));
        add(updateSWTextArea);
        add(actionPanel);
    }

    public void setHandler(ClickHandler handler) {
        updateSW.addClickHandler(handler);
    }

    public void onSuccess() {
        updateSW.setEnabled(false);
        successUpdateLabel.show(UIMessageUtils.m.const_fieldUpdateSuccess(getTitleWithoutLastChar(title)));
    }

    public void onFailure() {
        updateSW.setEnabled(true);
        successUpdateLabel.show(UIMessageUtils.m.const_fieldUpdateFailed(getTitleWithoutLastChar(title)), LabelColorEnum.FAIL_COLOR);
    }

    public String getText() {
        return updateSWTextArea.getText();
    }

    private static String getTitleWithoutLastChar(String title) {
        return title.substring(0, title.length() - 1);
    }
}
