package consys.event.registration.gwt.client.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface EventRegistrationConstants extends Constants {

    String addManagedUserUI_text_firstName();

    String addManagedUserUI_text_lastName();

    String addManagedUserUI_text_organization();

    String badgesSizeDialog_title();

    String bundleItem_text_coupons();

    String bundleItem_text_discountTitle();

    String bundleItem_text_notUsed();

    String bundleItem_text_used();

    String buyAdditionProducts_button_order();

    String checkDialog_text_all();

    String checkDialog_titleIn();

    String checkDialog_titleOut();

    String checkInList_action_filterAll();

    String checkInList_action_buttonChecked();

    String checkInList_action_buttonCheckIn();

    String checkInList_action_buttonUncheck();

    String checkInList_action_filterChecked();

    String checkInList_action_filterNotChecked();

    String checkInList_action_name();

    String checkInList_title();

    String confirmOrderDialog_title();

    String confirmOrderDialog_text_note();

    String confirmOrderDialog_text_notifyUser();

    String const_exceptionCancelRegistrationNoRecords();

    String const_exceptionCyclesIntersection();

    String const_exceptionEventNotActivated();

    String const_exceptionRegistrationsInCycle();

    String const_exceptionTicketUniquity();

    String const_exceptionTicketCapacity();

    String const_exceptionUserAlreadyRegistered();

    String const_price();

    String const_registrationCycleNotOverlap();

    String const_registrationDate();

    String couponListItem_error_alreadyUsed();

    String couponListItem_error_noRecords();

    String createDiscountDialog_action_addRestriction();

    String createDiscountDialog_title();

    String customQuestionItem_text_question();

    String customQuestionItem_text_required();

    String discountFilterList_action_all();

    String discountFilterList_action_group();

    String discountFilterList_action_individual();

    String discountFilterList_action_notUsed();

    String discountFilterList_action_used();

    String discountInfoPanel_action_individualCoupon();

    String discountInfoPanel_action_groupCoupon();

    String discountList_title();

    String discountListItem_text_capacity();

    String discountListItem_text_code();

    String discountListItem_text_discount();

    String discountListItem_text_group();

    String discountListItem_text_individual();

    String discountListItem_text_notUsed();

    String discountListItem_text_restriction();

    String discountListItem_text_ticket();

    String discountListItem_text_unlimited();

    String discountListItem_text_validThrough();

    String editFormBundle_form_b2bTicket();

    String editFormBundle_form_b2bTicketAccessory();

    String editFormBundle_form_capacity();

    String editFormBundle_form_maxQuantity();

    String editFormBundle_form_quantity();

    String editFormBundle_title();

    String editFormDiscountCoupon_title();

    String editFormQuestion_title();

    String editFormRegistrationCycle_title();

    String editFormSubbundle_form_vatPercent();

    String editFormSubbundle_title();

    String editRegistrationForm_text_note();

    String editRegistrationForm_text_note_placeholder();

    String editRegistrationForm_text_special_wish();

    String editRegistrationForm_text_special_wish_placeholder();

    String internalRegistration_action_addNewRegistration();

    String internalRegistration_title();

    String internalRegistrationDialog_action_register();

    String internalRegistrationDialog_text_mustSelectTicket();

    String internalRegistrationDialog_text_userInformation();

    String orderControlPanel_action_downloadInvoice();

    String orderControlPanel_action_downloadProformaInvoice();

    String orderControlPanel_action_downloadTicket();

    String outerLogin_action_iDontHaveAny();

    String outerQuestions_error_loadQuestions();

    String outerRegistrationForm_error_eventNotActivated();

    String outerRegistrationForm_error_eventNotFound();

    String outerRegistrationForm_error_registrationIsClosed();

    String outerRegistrationForm_error_registrationIsClosedDue();

    String outerRegistrationForm_error_userInfoNotEntered();

    String outerRegistrationForm_text_completeYourProfile();

    String outerRegistrationForm_text_register();

    String outerRegistrationForm_text_registerHelp();

    String outerRegistrationForm_text_invoiceAddress();

    String outerRegistrationForm_text_invoiceAddressHelp();

    String outerRegistrationForm_text_invoiceTo();

    String outerRegistrationForm_text_providedByTakeplace();

    String outerRegistrationFormV2_action_showDescription();

    String outerRegisterContent_action_clickToLogin();

    String outerRegisterContent_action_clickToRegister();

    String outerRegisterContent_button_register();

    String outerRegisterContent_error_ticketFull();

    String outerRegisterContent_error_noRecorsForParameterValues();

    String outerRegisterContent_help1();

    String outerRegisterContent_help2();

    String outerRegisterContent_help3();

    String outerRegisterContent_help4();

    String outerRegisterContent_help5();

    String outerRegisterContent_help6();

    String outerRegisterContent_text_agreeing();

    String outerRegisterContent_text_alreadyRegistered();

    String outerRegisterContent_text_notYetRegistered();

    String outerRegisterPanel_text_beta();

    String outerRegisterPanel_title();

    String questionWidget_action_next();

    String questionWidget_title();

    String registerAnother_action_addManagedParticipant();

    String registerAnother_action_registerParticipant();

    String registerAnother_text_canceled();

    String registerAnother_text_pleaseAddParticipant();

    String registerAnother_title();

    String registrationAdditionalProducts_text_ifDisabled();

    String registrationAdditionalProducts_title();

    String registrationCycleList_action_addRC();

    String registrationCycleList_text_newRC();

    String registrationCycleList_text_pleaseAddRC();

    String registrationCycleList_title();

    String registrationForm_action_toWaiting();

    String registrationForm_progress_enjoyUpper();

    String registrationForm_progress_payUpper();

    String registrationForm_progress_registerUpper();

    String registrationForm_text_enteredCode();

    String registrationForm_text_insertDiscountCoupon();

    String registrationForm_text_noneTickets();

    String registrationForm_text_pricing();

    String registrationForm_text_validated();

    String registrationForm_title();

    String registrationFormItem_action_showDetails();

    String registrationFormItem_button_register();

    String registrationFormItem_discount_exception_CouponExpiredException();

    String registrationFormItem_discount_exception_CouponInvalidException();

    String registrationFormItem_discount_exception_CouponUsedException();

    String registrationFormItem_discount_exception_Common();

    String registrationFormItem_exception_noRecordsForAction();

    String registrationFormItem_text_attention();

    String registrationFormItem_text_capacityFilled();

    String registrationFormItem_text_notActive();

    String registrationFormItem_text_registrationClosed();

    String registrationFormRegistered_action_confirmOrder();

    String registrationFormRegistered_button_cancelRegistration();

    String registrationFormRegistered_button_cancelOrder();

    String registrationFormRegistered_button_sendCoupon();

    String registrationFormRegistered_cancel_registration_question_title();

    String registrationFormRegistered_cancel_order_question_title();

    String registrationFormRegistered_error_payTimeouted();

    String registrationFormRegistered_error_registrationCanceled();

    String registrationFormRegistered_text_registrationPaid();

    String registrationList_action_all();

    String registrationList_action_confirm();

    String registrationList_action_state();

    String registrationList_text_paid();

    String registrationList_text_preregistered();

    String registrationList_text_registered();

    String registrationList_text_state();

    String registrationList_text_waiting();

    String registrationList_title();

    String registrationModule_text_moduleCheckIn();

    String registrationModule_text_moduleCheckInDescription();

    String registrationModule_text_moduleName();

    String registrationModule_text_moduleRoleChair();

    String registrationModule_text_moduleRoleChairDescription();

    String registrationModule_text_moduleRoleAdministrator();

    String registrationModule_text_moduleRoleAdministratorDescription();

    String registrationModule_text_moduleRoleParticipantAdept();

    String registrationModule_text_moduleRoleParticipantAdeptDescription();

    String registrationModule_text_moduleRoleParticipant();

    String registrationModule_text_moduleRoleParticipantDescription();

    String registrationModule_text_navigationModelCheckInList();

    String registrationModule_text_navigationModelRegistration();

    String registrationModule_text_navigationModelRegister();

    String registrationModule_text_navigationModelMyRegistration();

    String registrationModule_text_navigationModelParticipantList();

    String registrationModule_text_navigationModelRegistrationList();

    String registrationModule_text_navigationModelRegistrationSettings();

    String registrationOrderPanel_action_downloadInvoice();

    String registrationOrderPanel_action_downloadInvoiceAdmin();

    String registrationOrderPanel_action_downloadProForma();

    String registrationOrderPanel_action_downloadProFormaAdmin();

    String registrationOrderPanel_text_additionalProducts();

    String registrationOrderPanel_text_dueDate();

    String registrationOrderPanel_text_invoice();

    String registrationOrderPanel_text_total();

    String registrationParticipantInfoPanel_action_details();

    String registrationPropertyPanel_property_allowRegisterAnotherParticipant();

    String registrationPropertyPanel_property_visibleParticipants();

    String registrationSettingsBundle_action_addTicket();

    String registrationSettingsBundle_action_ticketAccessories();

    String registrationSettingsBundle_text_newTicket();

    String registrationSettingsBundle_text_pleaseAddTicket();

    String registrationSettingsBundle_title();

    String registrationSettingsCustomQuestion_action_addField();

    String registrationSettingsCustomQuestion_text_permittedMax5Questions();

    String registrationSettingsCustomQuestion_text_pleaseAddField();

    String registrationSettingsCustomQuestion_title();

    String registrationSettingsSubbundle_action_addTicketAccessory();

    String registrationSettingsSubbundle_text_newTicketAccessory();

    String registrationSettingsSubbundle_text_pleaseAddTicketAccessory();

    String registrationSettingsSubbundle_title();

    String subbundleItem_text_vat();

    String userParticipantList_action_name();

    String userParticipantList_title();

    String waitingOverview_button_cancelWaiting();

    String waitingOverview_cancel_registration_question_title();

    String waitingOverview_text_capacity();

    String waitingOverview_text_noOpenRegistration();

    String waitingOverview_title();
}
