package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import java.util.ArrayList;

/**
 * Vytvori registracni cykly
 * @author pepa
 */
public class CreateRegistrationCycleAction extends EventAction<ArrayListResult<String>> {

    private static final long serialVersionUID = -1270460205595874701L;
    // data
    private String bundleUuid;
    private ArrayList<ClientRegistrationCycle> cycles;

    public CreateRegistrationCycleAction() {
    }

    public CreateRegistrationCycleAction(String bundleUuid, ArrayList<ClientRegistrationCycle> cycles) {
        this.bundleUuid = bundleUuid;
        this.cycles = cycles;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public ArrayList<ClientRegistrationCycle> getCycles() {
        return cycles;
    }
}
