package consys.event.registration.gwt.client.module.coupon;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.list.DataListCommonFiltersItem;
import consys.common.gwt.client.ui.comp.list.DataListCommonHeadPanel;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.HeadPanel;
import consys.common.gwt.client.ui.comp.list.ListPagerWidget;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.list.Filter;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class CouponListFilter extends HeadPanel implements DataListCommonHeadPanel, consys.event.registration.api.list.RegistrationCouponList {

    // komponenty
    private FlowPanel filters;
    private ListPagerWidget listPagerWidget;
    private SelectBox<String> bundleSelectBox;
    // data
    private ArrayList<DataListCommonFiltersItem> allFilters;
    private DataListCommonFiltersItem activeFilter;
    private String activeTicket = "0";
    private CouponList couponList;
    private DataListCommonFiltersItem used;
    private DataListCommonFiltersItem notUsed;

    public CouponListFilter(CouponList couponList) {
        this.couponList = couponList;
        allFilters = new ArrayList<DataListCommonFiltersItem>();

        bundleSelectBox = new SelectBox<String>();
        bundleSelectBox.addStyleName(StyleUtils.FLOAT_LEFT);
        bundleSelectBox.setWidth(200);
        bundleSelectBox.selectFirst(true);
        final SelectBoxItem<String> initItem = new SelectBoxItem<String>("0", ERMessageUtils.c.registrationList_action_all());
        ArrayList<SelectBoxItem<String>> initItems = new ArrayList<SelectBoxItem<String>>();
        initItems.add(initItem);
        bundleSelectBox.setItems(initItems);

        Label title = StyleUtils.getStyledLabel(UIMessageUtils.c.dataListCommon_text_show() + ":",
                ResourceUtils.system().css().dataListCommonOrdererTitle(), StyleUtils.FLOAT_LEFT);

        filters = new FlowPanel();
        filters.add(title);
        filters.addStyleName(StyleUtils.FLOAT_LEFT);

        listPagerWidget = new ListPagerWidget();

        SimplePanel sp = new SimplePanel();
        sp.setWidget(listPagerWidget);
        sp.addStyleName(StyleUtils.FLOAT_RIGHT);

        FlowPanel flowPanel = new FlowPanel();
        flowPanel.setHeight("25px");
        flowPanel.add(filters);
        flowPanel.add(sp);
        initWidget(flowPanel);

        initFilters();
    }

    private void initFilters() {
        final DataListCommonFiltersItem all = new DataListCommonFiltersItem(
                Filter_ALL, ERMessageUtils.c.discountFilterList_action_all(), this);
        all.addStyleName(StyleUtils.FLOAT_LEFT);
        all.select();
        all.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                all.setFilter(new Filter(Filter_ALL, activeTicket));
                activeFilter = all;
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        filters.add(all);
        allFilters.add(all);

        activeFilter = all;
        clearFilter();
        registerFilter(all);

        addSeparator();

        used = new DataListCommonFiltersItem(
                Filter_USED, ERMessageUtils.c.discountFilterList_action_used(), this);
        used.addStyleName(StyleUtils.FLOAT_LEFT);
        used.deselect();
        used.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                used.setFilter(new Filter(Filter_USED, activeTicket));
                activeFilter = used;
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        filters.add(used);
        allFilters.add(used);

        addSeparator();

        notUsed = new DataListCommonFiltersItem(
                Filter_NOT_USED, ERMessageUtils.c.discountFilterList_action_notUsed(), this);
        notUsed.addStyleName(StyleUtils.FLOAT_LEFT);
        notUsed.addStyleName(StyleUtils.MARGIN_RIGHT_20);
        notUsed.deselect();
        notUsed.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                notUsed.setFilter(new Filter(Filter_NOT_USED, activeTicket));
                activeFilter = notUsed;
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        filters.add(notUsed);
        allFilters.add(notUsed);

        bundleSelectBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                final String value = event.getValue().getItem();
                couponList.changeDefaultBundle(value.equals("0") ? null : value);
                bundleSelectBoxChangeAction(value);
            }
        });
        filters.add(bundleSelectBox);
    }

    /** akce spustena pri zmene filtru bundle */
    private void bundleSelectBoxChangeAction(String bundleUuid) {
        if (activeFilter != null) {
            activeFilter.deselect();
        }
        activeFilter.setFilter(new Filter(activeFilter.getTag(), bundleUuid));
        activeFilter.select();
        activeFilter.refresh();
        activeFilter.select();
        activeTicket = bundleUuid;
    }

    /** inicializuje selectbox */
    public void refreshBundles(ArrayList<SelectBoxItem<String>> bundleList) {
        final SelectBoxItem<String> initItem = new SelectBoxItem<String>("0", ERMessageUtils.c.registrationList_action_all());
        ArrayList<SelectBoxItem<String>> initItems = new ArrayList<SelectBoxItem<String>>();
        initItems.add(initItem);
        initItems.addAll(bundleList);
        bundleSelectBox.setItems(initItems);
    }

    /** vybere vychozi filtr tiketu */
    public void selectBundle(String bundleUuid) {
        bundleSelectBox.selectItem(bundleUuid);
        bundleSelectBoxChangeAction(bundleUuid);
    }

    /** vybere filtr pouzite/nepouzite */
    public void selectUsed(Boolean usedFilter) {
        if (activeFilter != null) {
            activeFilter.deselect();
        }
        if (usedFilter != null) {
            activeFilter = usedFilter ? used : notUsed;
        }
    }

    private void addSeparator() {
        Label separator = new Label("|");
        separator.setStyleName(ResourceUtils.system().css().dataListCommonOrdererBreak());
        separator.addStyleName(StyleUtils.FLOAT_LEFT);
        filters.add(separator);
    }

    @Override
    public void setParentList(DataListPanel parentList) {
        for (DataListCommonFiltersItem cf : allFilters) {
            cf.setParentList(parentList);
        }
    }

    @Override
    public ListPagerWidget getListPagerWidget() {
        return listPagerWidget;
    }
}
