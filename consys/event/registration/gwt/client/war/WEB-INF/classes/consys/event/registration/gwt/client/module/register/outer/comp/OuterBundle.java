package consys.event.registration.gwt.client.module.register.outer.comp;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.event.registration.gwt.client.action.LoadOuterRegisterAction;
import consys.event.registration.gwt.client.bo.ClientOuterRegisterData;
import consys.event.registration.gwt.client.event.OrderTotalPriceEvent;
import consys.event.registration.gwt.client.module.register.OuterRegistrationFormV2;
import consys.event.registration.gwt.client.module.register.item.RegistrationFormPanel;
import consys.event.registration.gwt.client.module.register.outer.OuterLayout;
import consys.event.registration.gwt.client.module.register.outer.OuterResources;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.List;

/**
 *
 * @author pepa
 */
public class OuterBundle extends Composite implements OrderTotalPriceEvent.Handler {

    // konstanty
    private static Logger logger = LoggerFactory.getLogger(OuterBundle.class);
    // komponenty
    private FlowPanel mainPanel;
    private RegistrationFormPanel registrationPanel;
    // data
    private String packageUuid;
    private OuterInvoiceInfo outerInvoiceInfo;
    private OuterRegistrationFormV2 fullForm;

    public OuterBundle(String packageUuid, OuterInvoiceInfo outerInvoiceInfo, OuterRegistrationFormV2 fullForm) {
        this.packageUuid = packageUuid;
        this.outerInvoiceInfo = outerInvoiceInfo;
        this.fullForm = fullForm;

        OuterResources.INSTANCE.css().ensureInjected();

        mainPanel = new FlowPanel();
        mainPanel.setStyleName(OuterResources.INSTANCE.css().outerBundle());
        mainPanel.addStyleName(OuterResources.INSTANCE.css().centerPanel());

        initWidget(mainPanel);
    }

    @Override
    protected void onLoad() {
        mainPanel.clear();
        logger.debug("firing: LoadOuterRegisterAction");

        EventBus.get().addHandler(OrderTotalPriceEvent.TYPE, this);
        EventBus.get().fireEvent(new EventDispatchEvent(new LoadOuterRegisterAction(packageUuid),
                new AsyncCallback<ClientOuterRegisterData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientOuterRegisterData result) {
                        if (result.getItems().get(0).getPrice() == null) {
                            // registrace je zavrena
                            fullForm.registrationClosedState();
                            return;
                        }
                        if (!result.getItems().get(0).isUnlimitedCapacity() && result.getItems().get(0).getFreeCapacity() == 0) {
                            fullForm.registrationTicketFull(ERMessageUtils.c.outerRegisterContent_error_ticketFull());
                        } else {
                            registrationPanel = new RegistrationFormPanel(result.getItems().get(0), result.getCurrency(), true);
                            result.getItems().remove(0);
                            registrationPanel.setSubbundles(result.getItems(), true);
                            mainPanel.add(registrationPanel);
                            refreshInvoiceDetailsPanel(registrationPanel.getTotal());
                        }
                    }
                }, OuterLayout.get()));
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(OrderTotalPriceEvent.TYPE, this);
    }

    @Override
    public void onTotalPriceChanged(OrderTotalPriceEvent event) {
        refreshInvoiceDetailsPanel(event.getPrice());
    }

    private void refreshInvoiceDetailsPanel(Monetary price) {
        outerInvoiceInfo.setVisible(!price.isZero());
    }

    public List<ProductThumb> getOrderedItems() {
        return registrationPanel.getOrderedItems(false);
    }
}
