package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Smaze registracni bundle podle jeho uuid
 * @author pepa
 */
public class DeleteRegistrationBundleAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -41060117631879931L;
    // data
    private String uuid;

    public DeleteRegistrationBundleAction() {
    }

    public DeleteRegistrationBundleAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
