package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.registration.gwt.client.action.ConfirmOrderAction;
import consys.event.registration.gwt.client.event.OrderConfirmedEvent;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class ConfirmOrderDialog extends SmartDialog {

    // komponenty
    private BaseForm form;
    private ConsysTextArea noteBox;
    private ConsysCheckBox notifyUserBox;
    // data
    private String orderUuid;

    public ConfirmOrderDialog(String orderUuid) {
        this.orderUuid = orderUuid;
    }

    @Override
    protected void generateContent(SimpleFormPanel panel) {
        noteBox = new ConsysTextArea(0, 0, ERMessageUtils.c.confirmOrderDialog_text_note());
        notifyUserBox = new ConsysCheckBox();

        ActionImage save = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        save.addClickHandler(saveClickHandler());

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.setClickHandler(hideClickHandler());

        form = new BaseForm();
        form.addStyleName(StyleUtils.MARGIN_TOP_10);
        form.addOptional(0, ERMessageUtils.c.confirmOrderDialog_text_note(), noteBox);
        form.addOptional(1, ERMessageUtils.c.confirmOrderDialog_text_notifyUser(), notifyUserBox);
        form.addActionMembers(2, save, cancel);

        panel.addWidget(StyleUtils.getPadder());
        panel.addWidget(StyleUtils.getStyledLabel(ERMessageUtils.c.confirmOrderDialog_title(),
                ResourceUtils.system().css().createDialogHeaderTitle()));
        panel.addWidget(StyleUtils.getPadder());
        panel.addWidget(form);
        panel.addWidget(StyleUtils.getPadder());
    }

    /** clickhandler pro potvrzeni objednavky */
    private ClickHandler saveClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                ConfirmOrderAction action = new ConfirmOrderAction(orderUuid);
                action.setNote(noteBox.getText().trim());
                action.setNotifyUser(notifyUserBox.getValue());

                EventBus.get().fireEvent(new EventDispatchEvent(action,
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                EventBus.get().fireEvent(new OrderConfirmedEvent(orderUuid));
                                hide();
                            }
                        }, ConfirmOrderDialog.this));
            }
        };
    }
}
