package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.bo.Monetary;
import java.util.Date;

/**
 * Rozsireny eventovy uzivatel o potrebna administratorska data
 * @author pepa
 */
public class ClientListRegistrationItem extends ClientListParticipantItem {

    private static final long serialVersionUID = -2999747844818684579L;
    // data
    private Date registrationDate;
    private Monetary paid;
    private String bundleName;
    private ClientRegistrationState state;
    private int quantity;

    /**
     * @return the registrationDate
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * @param registrationDate the registrationDate to set
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * @return the paid
     */
    public Monetary getPaid() {
        return paid;
    }

    /**
     * @param paid the paid to set
     */
    public void setPaid(Monetary paid) {
        this.paid = paid;
    }

    /**
     * @return the bundleName
     */
    public String getBundleName() {
        return bundleName;
    }

    /**
     * @param bundleName the bundleName to set
     */
    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public int getQuantity() {
        if (quantity < 1) {
            quantity = 1;
        }
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else {
            this.quantity = quantity;
        }
    }

    public ClientRegistrationState getState() {
        return state;
    }

    public void setState(ClientRegistrationState state) {
        this.state = state;
    }

    public enum ClientRegistrationState {

        REGISTERED, PREREGISTERED, WAITING;
    }
}
