package consys.event.registration.gwt.client.module.register.outer.comp;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author pepa
 */
public class ULPanel extends ComplexPanel {

    private UListElement list;

    public ULPanel() {
        this(null);
    }

    public ULPanel(String styleName) {
        list = Document.get().createULElement();

        if (styleName != null) {
            list.addClassName(styleName);
        }

        setElement(list);
    }

    @Override
    public void add(Widget child) {
        Element li = Document.get().createLIElement().cast();
        list.appendChild(li);
        super.add(child, li);
    }
}
