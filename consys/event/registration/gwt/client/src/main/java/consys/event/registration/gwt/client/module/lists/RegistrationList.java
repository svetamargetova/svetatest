package consys.event.registration.gwt.client.module.lists;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.DataListCommonOrderer;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.comp.user.NameWithAffiliation;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.bo.ClientListRegistrationItem;
import consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState;
import consys.event.registration.gwt.client.module.lists.item.BadgeSizeDialog;
import consys.event.registration.gwt.client.module.register.AdminRegistrationOverview;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import consys.event.registration.gwt.client.module.register.WaitingOverview;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Seznam s registracema
 * @author pepa
 */
public class RegistrationList extends DataListPanel implements consys.event.registration.api.list.RegistrationsList {

    public RegistrationList() {
        super(ERMessageUtils.c.registrationList_title(), LIST_TAG);

        setListDelegate(new RegistrationListDelegate());

        // generovani badgu
        ActionImage badges = new ActionImage(null, ERMessageUtils.c.badgesSizeDialog_title(), true);
        badges.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                BadgeSizeDialog dialog = new BadgeSizeDialog();
                dialog.showCentered();
            }
        });
        addRightControll(badges);

        enableExport(ListExportTypeEnum.EXCEL);

        setDefaultFilter(new ListFilter(Filter_STATE_ALL, "0") {
            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
        DataListCommonOrderer orderers = new DataListCommonOrderer();
        orderers.addOrderer(ECoMessageUtils.c.const_date(), Order_REGISTRATION_DATE, true);
        orderers.addOrderer(ERMessageUtils.c.registrationList_action_state(), Order_REGISTRATION_STATE);
        addHeadPanel(orderers);
        addHeadPanel(new RegistrationListFilter(false));
    }

    @Override
    protected boolean asyncPreprocesing() {
        return false;
    }

    @Override
    protected void doAsyncPreprocesing(ListDataSourceResult result, final ConsysAction action) {
    }

    /** delegat seznamu */
    private class RegistrationListDelegate implements ListDelegate<ClientListRegistrationItem, RegistrationItem> {

        @Override
        public RegistrationItem createCell(ClientListRegistrationItem item) {
            return new RegistrationItem(item);
        }
    }

    private class RegistrationItem extends ListItem<ClientListRegistrationItem> {

        public RegistrationItem(ClientListRegistrationItem value) {
            super(value);
        }

        @Override
        protected void createCell(final ClientListRegistrationItem userReg, FlowPanel panel) {
            panel.clear();

            Image portrait = FormUtils.userPortrait(userReg.getUuidPortraitImagePrefix(), UserProfileImageEnum.LIST);
            FlowPanel portraitWrapper = FormUtils.portraitWrapper(UserProfileImageEnum.LIST);
            portraitWrapper.add(portrait);
            panel.add(portraitWrapper);

            FlowPanel statePanel = new FlowPanel();
            statePanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationList_text_state() + ":",
                    StyleUtils.FLOAT_LEFT, StyleUtils.MARGIN_RIGHT_5, StyleUtils.FONT_BOLD));
            if (ClientRegistrationState.REGISTERED.equals(userReg.getState())) {
                statePanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationList_text_registered(),
                        StyleUtils.FLOAT_LEFT, StyleUtils.TEXT_GREEN, StyleUtils.FONT_BOLD));
            } else if (ClientRegistrationState.PREREGISTERED.equals(userReg.getState())) {
                statePanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationList_text_preregistered() + " ",
                        StyleUtils.FLOAT_LEFT, StyleUtils.TEXT_RED, StyleUtils.FONT_BOLD));

                ActionLabel confirm = new ActionLabel(ERMessageUtils.c.registrationList_action_confirm(),
                        MARGIN_LEFT_10 + " " + FLOAT_LEFT);
                confirm.removeStyleName(INLINE);
                confirm.setClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        toRegistration(true, userReg);
                    }
                });
                statePanel.add(confirm);
            } else if (ClientRegistrationState.WAITING.equals(userReg.getState())) {
                statePanel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.registrationList_text_waiting(),
                        StyleUtils.FLOAT_LEFT, StyleUtils.TEXT_GRAY, StyleUtils.FONT_BOLD));
            }
            statePanel.add(StyleUtils.clearDiv());

            ConsysAction action = new ConsysAction() {
                @Override
                public void run() {
                    if (ClientRegistrationState.WAITING.equals(userReg.getState())) {
                        toWaiting(userReg);
                    } else {
                        toRegistration(false, userReg);
                    }
                }
            };

            StringBuilder sb = new StringBuilder();
            if (userReg.getQuantity() > 1) {
                sb.append(userReg.getQuantity());
                sb.append("x ");
            }
            sb.append(userReg.getBundleName());

            FlowPanel textPanel = new FlowPanel();
            textPanel.setStyleName(FLOAT_LEFT);
            textPanel.addStyleName(MARGIN_BOT_10);
            textPanel.add(new NameWithAffiliation(userReg.getUserName(), userReg.getUserAffiliation(), action));
            textPanel.add(StyleUtils.getStyledLabel(sb.toString(), MARGIN_TOP_10, FONT_BOLD));
            textPanel.add(statePanel);
            if (!ClientRegistrationState.WAITING.equals(userReg.getState())) {
                textPanel.add(FormUtils.titleValue(ERMessageUtils.c.registrationList_text_paid(),
                        userReg.getPaid().toString() + " " + ECoMessageUtils.getCurrency()));
                textPanel.add(FormUtils.titleValue(ERMessageUtils.c.const_registrationDate(),
                        DateTimeUtils.getDateAndTime(userReg.getRegistrationDate())));
            }
            panel.add(textPanel);
            panel.add(StyleUtils.clearDiv());
        }
    }

    /** proklik do registrace uživatele */
    private void toRegistration(boolean autoconfirm, ClientListRegistrationItem userReg) {
        AdminRegistrationOverview w = new AdminRegistrationOverview(userReg.getUuid());
        if (autoconfirm) {
            w.autoconfirm();
        }
        String title = RegistrationForm.anotherRegistrationBreadcrumb(userReg.getUserName());
        FormUtils.fireNextBreadcrumb(title, w);
    }

    /** proklik na waiting */
    private void toWaiting(ClientListRegistrationItem userReg) {
        WaitingOverview w = new WaitingOverview(userReg.getUuid());
        String title = ERMessageUtils.c.waitingOverview_title() + " - " + userReg.getUserName();
        FormUtils.fireNextBreadcrumb(title, w);
    }
}
