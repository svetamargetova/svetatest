package consys.event.registration.gwt.client.module.setting.question;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.UpdateCustomQuestionAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class EditFormQuestion extends RootPanel {

    // komponenty
    private BaseForm form;
    private ConsysStringTextBox questionBox;
    private ConsysCheckBox requiredBox;
    // data
    private ClientCustomQuestion question;
    private CustomQuestionItem source;

    public EditFormQuestion(ClientCustomQuestion question, CustomQuestionItem source) {
        super(ERMessageUtils.c.editFormQuestion_title());

        this.question = question;
        this.source = source;

        init();
    }

    private void init() {
        form = new BaseForm();
        form.setStyleName(StyleUtils.MARGIN_TOP_10);

        questionBox = new ConsysStringTextBox(1, 1024, ERMessageUtils.c.customQuestionItem_text_question());
        questionBox.setText(question.getText());

        requiredBox = new ConsysCheckBox();
        requiredBox.setValue(question.isRequired());

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        form.addRequired(1, ERMessageUtils.c.customQuestionItem_text_question(), questionBox);
        form.addRequired(2, ERMessageUtils.c.customQuestionItem_text_required(), requiredBox);
        form.addActionMembers(5, button, ActionLabel.breadcrumbBackCancel());

        addWidget(form);
    }

    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                // validace
                if (!form.validate(getFailMessage())) {
                    return;
                }

                final UpdateCustomQuestionAction action = new UpdateCustomQuestionAction(question.getUuid(),
                        requiredBox.getValue(), questionBox.getText());

                EventBus.fire(new EventDispatchEvent(action,
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                if (caught instanceof BadInputException) {
                                    getFailMessage().addOrSetText("BadInputException");
                                } else if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().addOrSetText("NoRecordsForAction");
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                ClientCustomQuestion newData = new ClientCustomQuestion();
                                newData.setRequired(action.isRequired());
                                newData.setText(action.getText());
                                newData.setUuid(action.getQuestionUuid());

                                source.setData(newData);
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, EditFormQuestion.this));
            }
        };
    }
}
