package consys.event.registration.gwt.client.module.setting.question;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelLabel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.DeleteCustomQuestionAction;
import consys.event.registration.gwt.client.action.LoadCustomQuestionAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class CustomQuestionItem extends SimpleFormPanel implements ConsysTabPanelItem<String> {

    // komponenty
    private ConsysTabPanelLabel tabLabel;
    // data
    private ConsysMessage helpMessage;
    private ClientCustomQuestion question;
    private ConsysTabPanel<String> tabPanel;

    public CustomQuestionItem(ConsysTabPanelLabel tabLabel, ConsysMessage helpMessage) {
        this.tabLabel = tabLabel;
        this.helpMessage = helpMessage;
    }

    /** nastavi nove hodnoty, jak pro bundle tak pro cykly */
    public void setData(ClientCustomQuestion question) {
        this.question = question;
        tabLabel.setText(question.getText());

        initContent();
    }

    private void initContent() {
        clear();

        EditWithRemover ewr = new EditWithRemover(new ConsysAction() {
            @Override
            public void run() {
                EditFormQuestion w = new EditFormQuestion(question, CustomQuestionItem.this);
                FormUtils.fireNextBreadcrumb(ERMessageUtils.c.editFormQuestion_title(), w);
            }
        }, getRemoveAction());
        ewr.addStyleName(FLOAT_RIGHT);

        Label questionLabel = StyleUtils.getStyledLabel(ERMessageUtils.c.customQuestionItem_text_question(), FONT_16PX,
                FONT_BOLD, MARGIN_BOT_10, CLEAR_BOTH);

        Label questionText = StyleUtils.getStyledLabel(question.getText(), MARGIN_BOT_20);

        HTML requiredQuestion = new HTML("<b>" + ERMessageUtils.c.customQuestionItem_text_required() + "</b>: "
                + UIMessageUtils.yesNo(question.isRequired()));

        FlowPanel wrapper = new FlowPanel();
        wrapper.setStyleName(MARGIN_VER_10);
        wrapper.addStyleName(MARGIN_HOR_10);
        wrapper.add(ewr);
        wrapper.add(questionLabel);
        wrapper.add(questionText);
        wrapper.add(requiredQuestion);

        addWidget(wrapper);
    }

    @Override
    public Widget getTitleNote() {
        return null;
    }

    @Override
    public Widget getContent() {
        return this;
    }

    @Override
    public void setConsysTabPanel(ConsysTabPanel<String> tabPanel) {
        this.tabPanel = tabPanel;
    }

    /** akce pro odebrani baliku */
    private ConsysAction getRemoveAction() {
        return new ConsysAction() {
            @Override
            public void run() {
                EventBus.fire(new EventDispatchEvent(
                        new DeleteCustomQuestionAction(question.getUuid()),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof NoRecordsForAction) {
                                    getFailMessage().setText("NoRecordsForAction");
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                tabPanel.removeTab(CustomQuestionItem.this);
                                if (tabPanel.getTabCount() == 0) {
                                    tabPanel.setVisible(false);
                                    helpMessage.setVisible(true);
                                }
                            }
                        }, CustomQuestionItem.this));
            }
        };
    }

    public static void addToTab(ConsysTabPanel<String> tabs, final CommonThumb item, ConsysMessage helpMessage) {
        ConsysTabPanelLabel<String> tl = new ConsysTabPanelLabel<String>(item.getUuid(), item.getName());
        final CustomQuestionItem questionItem = new CustomQuestionItem(tl, helpMessage);
        ConsysAction action = new ConsysAction() {
            @Override
            public void run() {
                EventBus.fire(new EventDispatchEvent(new LoadCustomQuestionAction(item.getUuid()),
                        new AsyncCallback<ClientCustomQuestion>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof NoRecordsForAction) {
                                    questionItem.getFailMessage().setText("NoRecordsForAction");
                                } else if (caught instanceof BadInputException) {
                                    questionItem.getFailMessage().setText("BadInputException");
                                }
                            }

                            @Override
                            public void onSuccess(ClientCustomQuestion result) {
                                questionItem.setData(result);
                            }
                        }, questionItem));
            }
        };
        tabs.addTabItem(tl, questionItem, action);
    }
}
