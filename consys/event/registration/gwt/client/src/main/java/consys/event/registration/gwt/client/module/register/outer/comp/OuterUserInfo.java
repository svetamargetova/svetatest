package consys.event.registration.gwt.client.module.register.outer.comp;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.input.ValidableItem;
import consys.common.gwt.client.ui.comp.form.ValidablePanel;
import consys.common.gwt.client.ui.comp.form.Validator;
import consys.common.gwt.client.ui.comp.input.ConsysPasswordBox;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.ui.utils.ValidatorUtils;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.event.registration.gwt.client.module.register.outer.OuterResources;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Cast formulare pro zadani informaci o uzivatelovi
 * @author pepa
 */
public class OuterUserInfo {

    // konstanty
    private static final int PASSWORD_BOX_WIDTH = 120;
    // komponenty
    private FormV2 form;
    private ConsysTextBox firstName;
    private ConsysTextBox lastName;
    private ConsysTextBox organization;
    private ConsysTextBox email;
    private ConsysPasswordBox password;
    private ConsysPasswordBox confirmPassword;
    private ConsysCheckBox showPassword;
    // data
    private SimplePanel contentPanel;
    private ClientSsoUserDetails userDetails;
    private boolean nullUserDetails;

    public OuterUserInfo(SimplePanel contentPanel, ClientSsoUserDetails userDetails) {
        this.contentPanel = contentPanel;
        this.userDetails = userDetails;

        OuterResources.INSTANCE.css().ensureInjected();
        generate();
    }

    private void generate() {
        nullUserDetails = userDetails == null;
        if (nullUserDetails) {
            userDetails = new ClientSsoUserDetails();
        }

        initComponents();

        Label complete = new Label(ERMessageUtils.c.outerRegistrationForm_text_completeYourProfile() + ":");
        complete.setStyleName(OuterResources.INSTANCE.css().completeYourProfile());

        firstName.setText(userDetails.getFirstName());
        lastName.setText(userDetails.getLastName());
        organization.setText(userDetails.getAffiliation());
        email.setText(userDetails.getLoginEmail());

        form = new FormV2();
        form.addValueWidget(complete);
        form.add(UIMessageUtils.c.const_user_firstName(), new ValidableItem(firstName), true);
        form.add(UIMessageUtils.c.const_user_lastName(), new ValidableItem(lastName), true);
        form.add(UIMessageUtils.c.const_organization(), organization, false);
        form.add(UIMessageUtils.c.const_email(), new ValidableItem(email), true);

        if (isRequiredPassword()) {
            form.add(UIMessageUtils.c.const_password(), new ValidableItem(new PasswordPanel()), true);
            form.add(UIMessageUtils.c.const_confirmPassword(), confirmPassword, true);
        }

        contentPanel.setWidget(form);
    }

    private void initComponents() {
        firstName = new ConsysTextBox(1, 255);
        lastName = new ConsysTextBox(1, 255);
        organization = new ConsysTextBox(0, 255);
        email = new ConsysTextBox(1, 255);
        email.addCustomValidator(new Validator<String>() {

            @Override
            public String validate(String value) {
                return ValidatorUtils.isValidEmailAddress(value) ? null : UIMessageUtils.c.const_error_invalidEmailAddress();
            }
        });
        confirmPassword = new ConsysPasswordBox(PASSWORD_BOX_WIDTH, false);
        password = new ConsysPasswordBox(PASSWORD_BOX_WIDTH, true);
        password.setConfirmPassword(confirmPassword);

        showPassword = new ConsysCheckBox();
        showPassword.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                password.showPassword(event.getValue());
                confirmPassword.setEnabled(!event.getValue());
                confirmPassword.clearPassword();
            }
        });
    }

    /** vytvori novy objekt ClientSsoUserDetails a naplni ho dostupnymi daty */
    public ClientSsoUserDetails getUserDetails() {
        ClientSsoUserDetails details = new ClientSsoUserDetails();

        details.setSsoId(userDetails.getSsoId());
        details.setSsoStringId(userDetails.getSsoStringId());
        details.setFromSso(userDetails.getFromSso());
        details.setProfilePictureUrl(userDetails.getProfilePictureUrl());

        details.setFirstName(firstName.getText());
        details.setLastName(lastName.getText());
        details.setAffiliation(organization.getText());
        details.setLoginEmail(email.getText());
        details.setPassword(password.getPassword());

        return details;
    }

    /** vraci true pokud je vyzadovano zadani hesla */
    public boolean isRequiredPassword() {
        return nullUserDetails;
    }

    /** vraci true pokud je heslo zadano a je stejne v obou polich, pokud je opakovaci disablovane, vraci automaticky true */
    public boolean isSamePassword() {
        boolean notEmpty = !password.getPassword().isEmpty();
        if (confirmPassword.isEnabled()) {
            return notEmpty && password.getPassword().equals(confirmPassword.getPassword());
        } else {
            return notEmpty;
        }
    }

    public boolean validate() {
        return form.validate();
    }

    private class PasswordPanel extends ValidablePanel {

        public PasswordPanel() {
            setStyleName(OuterResources.INSTANCE.css().passwordPanel());
            setWidth("260px");

            FlowPanel showPasswordPanel = new FlowPanel();
            showPasswordPanel.setStyleName(OuterResources.INSTANCE.css().showPasswordPanel());
            showPasswordPanel.add(showPassword);
            showPasswordPanel.add(new Label(UIMessageUtils.c.const_showPassword()));
            showPasswordPanel.add(StyleUtils.clearDiv());

            add(password, true);
            add(showPasswordPanel);
            add(StyleUtils.clearDiv());
        }
    }
}
