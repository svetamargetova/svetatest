package consys.event.registration.gwt.client.module.lists;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.DataListCommonFiltersItem;
import consys.common.gwt.client.ui.comp.list.DataListCommonHeadPanel;
import consys.common.gwt.client.ui.comp.list.HeadPanel;
import consys.common.gwt.client.ui.comp.list.ListPagerWidget;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.bo.list.Filter;
import consys.event.registration.gwt.client.action.ListBundleFilterAction;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class RegistrationListFilter extends HeadPanel implements DataListCommonHeadPanel, consys.event.registration.api.list.RegistrationsList {

    // komponenty
    private FlowPanel filters;
    private ListPagerWidget listPagerWidget;
    private SelectBox<String> selectBox;
    // data
    private ArrayList<DataListCommonFiltersItem> allFilters;
    private DataListCommonFiltersItem activeFilter;
    private String activeBundle = "0";

    public RegistrationListFilter() {
        this(true);
    }

    public RegistrationListFilter(boolean showListPager) {
        allFilters = new ArrayList<DataListCommonFiltersItem>();

        selectBox = new SelectBox<String>();
        selectBox.addStyleName(StyleUtils.FLOAT_LEFT);
        selectBox.setWidth(240);
        selectBox.selectFirst(true);
        final SelectBoxItem<String> initItem = new SelectBoxItem<String>("0", ERMessageUtils.c.registrationList_action_all());
        ArrayList<SelectBoxItem<String>> initItems = new ArrayList<SelectBoxItem<String>>();
        initItems.add(initItem);
        selectBox.setItems(initItems);

        Label title = StyleUtils.getStyledLabel(UIMessageUtils.c.dataListCommon_text_show() + ":",
                ResourceUtils.system().css().dataListCommonOrdererTitle(), StyleUtils.FLOAT_LEFT);

        filters = new FlowPanel();
        filters.add(title);
        filters.addStyleName(StyleUtils.FLOAT_LEFT);

        listPagerWidget = new ListPagerWidget();

        SimplePanel sp = new SimplePanel();
        sp.setWidget(listPagerWidget);
        sp.addStyleName(StyleUtils.FLOAT_RIGHT);
        sp.setVisible(showListPager);

        FlowPanel flowPanel = new FlowPanel();
        flowPanel.setHeight("25px");
        flowPanel.add(filters);
        flowPanel.add(sp);
        initWidget(flowPanel);

        initFilters();

        EventBus.get().fireEvent(new EventDispatchEvent(new ListBundleFilterAction(),
                new AsyncCallback<ArrayListResult<CommonThumb>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ArrayListResult<CommonThumb> result) {
                        ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
                        items.add(initItem);
                        for (CommonThumb i : result.getArrayListResult()) {
                            items.add(new SelectBoxItem<String>(i.getUuid(), i.getName()));
                        }
                        selectBox.setItems(items);
                    }
                }, null));
    }

    private void initFilters() {
        final DataListCommonFiltersItem all = new DataListCommonFiltersItem(
                Filter_STATE_ALL, ERMessageUtils.c.registrationList_action_all(), this);
        all.addStyleName(StyleUtils.FLOAT_LEFT);
        all.addStyleName(StyleUtils.MARGIN_LEFT_20);
        all.select();
        all.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                all.setFilter(new Filter(Filter_STATE_ALL, activeBundle));
                activeFilter = all;
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        allFilters.add(all);

        activeFilter = all;
        clearFilter();
        registerFilter(all);

        final DataListCommonFiltersItem preregistered = new DataListCommonFiltersItem(
                Filter_STATE_PREREGISTERED, ERMessageUtils.c.registrationList_text_preregistered(), this);
        preregistered.addStyleName(StyleUtils.FLOAT_LEFT);
        preregistered.deselect();
        preregistered.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                preregistered.setFilter(new Filter(Filter_STATE_PREREGISTERED, activeBundle));
                activeFilter = preregistered;
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        allFilters.add(preregistered);

        final DataListCommonFiltersItem registered = new DataListCommonFiltersItem(
                Filter_STATE_REGISTERED, ERMessageUtils.c.registrationList_text_registered(), this);
        registered.addStyleName(StyleUtils.FLOAT_LEFT);
        registered.deselect();
        registered.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                registered.setFilter(new Filter(Filter_STATE_REGISTERED, activeBundle));
                activeFilter = registered;
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        allFilters.add(registered);
        
        final DataListCommonFiltersItem waiting = new DataListCommonFiltersItem(
                Filter_STATE_WAITING, ERMessageUtils.c.registrationList_text_waiting(), this);
        waiting.addStyleName(StyleUtils.FLOAT_LEFT);
        waiting.deselect();
        waiting.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                waiting.setFilter(new Filter(Filter_STATE_WAITING, activeBundle));
                activeFilter = waiting;
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
            }
        });
        allFilters.add(waiting);

        selectBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {

            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                if (activeFilter != null) {
                    activeFilter.deselect();
                }
                activeFilter.setFilter(new Filter(activeFilter.getTag(), event.getValue().getItem()));
                activeFilter.select();
                activeFilter.refresh();
                activeFilter.select();
                activeBundle = event.getValue().getItem();
            }
        });
        
        
        filters.add(selectBox);
        filters.add(all);
        addSeparator();
        filters.add(preregistered);
        addSeparator();
        filters.add(registered);
        addSeparator();
        filters.add(waiting);
    }

    private void addSeparator() {
        Label separator = new Label("|");
        separator.setStyleName(ResourceUtils.system().css().dataListCommonOrdererBreak());
        separator.addStyleName(StyleUtils.FLOAT_LEFT);
        filters.add(separator);
    }

    @Override
    public void setParentList(DataListPanel parentList) {
        for (DataListCommonFiltersItem cf : allFilters) {
            cf.setParentList(parentList);
        }
    }

    @Override
    public ListPagerWidget getListPagerWidget() {
        return listPagerWidget;
    }
}
