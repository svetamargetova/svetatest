package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Smaze registracni cyklus podle jeho uuid
 * @author pepa
 */
public class DeleteRegistrationCycleAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -2529395488999994738L;
    // data
    private String uuid;

    public DeleteRegistrationCycleAction() {
    }

    public DeleteRegistrationCycleAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
