package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro zruseni registrace
 * @author pepa
 */
public class CancelRegistrationAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -6742693404598932332L;
    // data
    private String registrationOrderUuid;
    private boolean sourceOwner;
    
    public CancelRegistrationAction() {
    }

    public CancelRegistrationAction(String registrationOrderUuid, boolean sourceOwner) {
        this.registrationOrderUuid = registrationOrderUuid;
        this.sourceOwner = sourceOwner;
    }

    public String getRegistrationOrderUuid() {
        return registrationOrderUuid;
    }

    public boolean isSourceOwner() {
        return sourceOwner;
    }
    
    
}
