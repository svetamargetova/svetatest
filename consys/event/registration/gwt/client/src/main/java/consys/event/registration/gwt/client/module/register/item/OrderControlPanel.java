package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.ui.comp.action.ActionImage;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.gwt.client.module.payment.PaymentPayPalButton;
import consys.event.common.gwt.client.module.payment.PaymentPayPalButtonType;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 * Zobrazuje ovladaci tlacitka objednavky
 * @author pepa
 */
public class OrderControlPanel extends FlowPanel {

    // data
    private final ClientRegistrationOrder order;
    private final ClientRegistration registration;
    private final boolean mainOrder;
    private final boolean adminView;
    private ClickHandler cancelHandler;

    /** konstruktor pro dalsi objednavky */
    public OrderControlPanel(ClientRegistrationOrder order, boolean adminView) {
        this(order, null, adminView);
    }

    /** konstruktor pro hlavni objednavku */
    public OrderControlPanel(ClientRegistrationOrder order, ClientRegistration registration, boolean adminView) {
        this.order = order;
        this.registration = registration;
        this.adminView = adminView;
        mainOrder = registration != null;

        setStyleName(ERResourceUtils.css().orderControlPanel());
    }

    @Override
    protected void onLoad() {
        clear();
        generate();
    }

    private void generate() {
        final boolean paid = order.getState().equals(ClientRegistration.State.CONFIRMED);

        addDownloadTicket(paid);

        if (!adminView) {
            addPayByPayPalOrCard(paid);
        }

        addDownloadInvoice(paid);
        addCancelOrder(paid);
    }

    private void addDownloadTicket(boolean paid) {
        if (mainOrder && paid) {
            ActionImage downloadTicket = ActionImage.withoutIcon(ERMessageUtils.c.orderControlPanel_action_downloadTicket());
            downloadTicket.setClickHandler(downloadTicketClickHandler());
            downloadTicket.addStyleName(StyleUtils.MARGIN_BOT_5);
            add(downloadTicket);
        }
    }

    private void addPayByPayPalOrCard(boolean paid) {
        if (!paid) {
            // panel pro tlacitko
            final FlowPanel panel = new FlowPanel();
            add(panel);

            // aktualni akce
            final ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().get(CurrentEventCacheAction.CURRENT_EVENT);
            if (cce == null) {
                throw new NullPointerException("Missing cached Current Event");
            }

            // nazev kupovane polozky
            final StringBuilder itemName = new StringBuilder(cce.getEvent().getAcronym());
            itemName.append(" - ");
            if (mainOrder) {
                itemName.append(order.getOrderItems().get(0).getTitle());
            } else {
                itemName.append(ERMessageUtils.c.registrationOrderPanel_text_additionalProducts());
            }

            // natazeni platebnich informaci
            EventBus.fire(new EventDispatchEvent(new LoadOrderPaymentProfileAction(order.getUuid()),
                    new AsyncCallback<ClientUserPaymentProfile>() {
                @Override
                public void onFailure(Throwable caught) {
                    panel.add(paypalButton(cce.getEvent().getUuid(), itemName.toString(), PaymentPayPalButtonType.PAYPAL));
                    panel.add(paypalButton(cce.getEvent().getUuid(), itemName.toString(), PaymentPayPalButtonType.CARD));
                }

                @Override
                public void onSuccess(ClientUserPaymentProfile result) {
                    ClientUser currentUser = Cache.get().get(UserCacheAction.USER);

                    PaymentPayPalButton paypal = paypalButton(cce.getEvent().getUuid(), itemName.toString(), PaymentPayPalButtonType.PAYPAL);
                    PaymentPayPalButton paypalCard = paypalButton(cce.getEvent().getUuid(), itemName.toString(), PaymentPayPalButtonType.CARD);

                    fillUserDataInPaypalButton(paypal, currentUser, result);
                    fillUserDataInPaypalButton(paypalCard, currentUser, result);

                    panel.add(paypal);
                    panel.add(paypalCard);
                }
            }, null));
        }
    }

    private PaymentPayPalButton paypalButton(String eventUuid, String itemName, PaymentPayPalButtonType type) {
        PaymentPayPalButton paypal = new PaymentPayPalButton(eventUuid, order.getUuid(), order.getUuid(), type);
        paypal.setAmount(order.getTotalPrice().toString());
        paypal.setCurrencyCode(ECoMessageUtils.getCurrency());
        paypal.setItemName(itemName);
        switch (type) {
            case CARD:
                paypal.addStyleName(ERResourceUtils.css().payButtonCard());
            case PAYPAL:
            default:
                paypal.addStyleName(ERResourceUtils.css().payButtonPal());
                break;
        }
        return paypal;
    }

    private void fillUserDataInPaypalButton(PaymentPayPalButton paypal, ClientUser currentUser, ClientUserPaymentProfile profile) {
        paypal.setFirstName(currentUser.getFirstName());
        paypal.setLastName(currentUser.getLastName());
        paypal.setAddress1(profile.getStreet());
        paypal.setCity(profile.getCity());
        paypal.setZip(profile.getZip());
        paypal.setCountry(profile.getCountry2ch());
        paypal.setEmail(currentUser.getLoginEmail());
    }

    private void addDownloadInvoice(boolean paid) {
        ActionImage downloadInvoice = ActionImage.withoutIcon(paid
                ? ERMessageUtils.c.orderControlPanel_action_downloadInvoice()
                : ERMessageUtils.c.orderControlPanel_action_downloadProformaInvoice());
        downloadInvoice.setClickHandler(downloadInvoiceClickHandler(order.getUuid()));
        downloadInvoice.addStyleName(StyleUtils.MARGIN_BOT_5);
        if (!order.getTotalPrice().isZero()) {
            add(downloadInvoice);
        }
    }

    private void addCancelOrder(boolean paid) {
        if ((!paid || (paid && order.getTotalPrice().isZero())) && cancelHandler != null) {
            ActionImage cancelOrder = ActionImage.delete(ERMessageUtils.c.registrationFormRegistered_button_cancelOrder());
            cancelOrder.setClickHandler(cancelHandler);
            add(cancelOrder);
        }
    }

    private ClickHandler downloadTicketClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                try {
                    Window.open(EventURLFactory.downloadTicket(registration.getRegistrationUuid()), "_blank", "");
                } catch (NotInCacheException ex) {
                    // nemelo by nikdy nastat
                    return;
                }
            }
        };
    }

    private ClickHandler downloadInvoiceClickHandler(final String orderUuid) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                try {
                    Window.open(EventURLFactory.downloadInvoice(orderUuid), "_blank", "");
                } catch (NotInCacheException ex) {
                    throw new NullPointerException("Payment URL is not in cache");
                }
            }
        };
    }

    public void setCancelClickHandler(ClickHandler cancelHandler) {
        this.cancelHandler = cancelHandler;
    }
}
