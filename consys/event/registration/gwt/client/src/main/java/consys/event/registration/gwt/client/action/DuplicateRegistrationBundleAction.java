package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientTabItem;

/**
 * Vytvori duplikat ClientBundle
 * @author pepa
 */ 
public class DuplicateRegistrationBundleAction extends EventAction<ClientTabItem> {

    private static final long serialVersionUID = 3003622355105070571L;
    // data
    private String uuid;
    private String title;

    public DuplicateRegistrationBundleAction() {
    }

    public DuplicateRegistrationBundleAction(String uuid, String title) {
        this.uuid = uuid;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getUuid() {
        return uuid;
    }
}
