package consys.event.registration.gwt.client.action.managed;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations;

/**
 * Akce pro nacteni spravovanych registraci
 * @author pepa
 */
public class ListManagedRegistrationsAction extends EventAction<ClientManagedRegistrations> {

    private static final long serialVersionUID = 5262464631207942405L;
}
