package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientQuestions;

/**
 * Akce pro nacteni otazek, ktere se zobrazuji uzivateli pri registrace
 * @author pepa
 */
public class ListQuestionsForUserAction extends EventAction<ClientQuestions> {

    private static final long serialVersionUID = 40315175353499439L;
}
