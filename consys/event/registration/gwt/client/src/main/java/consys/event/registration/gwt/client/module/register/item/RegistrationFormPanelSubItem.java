package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.ChangeValueEvent.Handler;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.event.ChangeOrderPriceEvent;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 * Komponenta na vyber zaujmu o subbalicek. Rozsiruje registraciu balicku o
 * action prvok checkbox, ktory automaticky zvecsuje, resp. zmensuje hodnotu
 * vyeslednej sumy.
 *
 * @author Pavol Gressa <gressa@acemcee.com>
 */
public class RegistrationFormPanelSubItem extends RegistrationFormPanelItem {

    // komponenty
    private ConsysCheckBox check;
    private String groupId;
    // data
    private boolean enabled;

    public RegistrationFormPanelSubItem(String groupId, ClientRegistrationPack data, String currency, boolean parentEnabled, boolean newComponent) {
        super(data, currency, newComponent);
        this.groupId = groupId;
        enabled = (data.getFreeCapacity() != 0) && parentEnabled;
        setDescriptionThreshold(75);
    }

    private void setPriceLabel(boolean active) {
        if (active) {
            getPriceLabel().removeStyleName(ResourceUtils.common().css().notActive());
        } else {
            getPriceLabel().addStyleName(ResourceUtils.common().css().notActive());
        }
        // aktualizujeme cenu
        EventBus.get().fireEvent(new ChangeOrderPriceEvent(groupId, getRegistrationPack().getPrice(), !active));
    }

    @Override
    protected DiscountCodePanel getDiscountCodePanel() {
        DiscountCodePanel panel = super.getDiscountCodePanel();
        panel.setGroupId(groupId);
        return panel;
    }

    @Override
    protected QuantityPanel getQuantityPanel() {
        QuantityPanel panel = super.getQuantityPanel();
        panel.setGroupId(groupId);
        return panel;
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        // prenastavime hlavny css styl
        setStyleName(ERResourceUtils.bundle().css().registrationFormSubItem());

        // nastavime defaultnu farbu neaktivnej ceny
        getPriceLabel().addStyleName(ResourceUtils.common().css().notActive());

        // nastavime checkbox a adekvatnu reakciu na neho
        check = new ConsysCheckBox();
        check.setEnabled(enabled);
        check.addChangeValueHandler(new Handler<Boolean>() {
            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                showQuantityPanel(event.getValue());
                // aktualizujeme zobrazenie zlavoveho kuponu
                showDiscountPanel(event.getValue());
                // nastavime ci je cena aktivana alebo nie. 
                setPriceLabel(event.getValue());
            }
        });

        FlowPanel checkBoxPanel = new FlowPanel();
        checkBoxPanel.setStyleName(ERResourceUtils.bundle().css().registrationFormSubItemAction());
        checkBoxPanel.add(check);
        insertIntoContent(checkBoxPanel, 0);
    }

    /**
     * aktivuje/deaktivuje zaskrtavatko
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if (check != null) {
            check.setEnabled(enabled);
        }
    }

    /**
     * vraci true pokud je polozka zaskrtla
     */
    public boolean isSelected() {
        if (check != null) {
            return check.getValue();
        } else {
            return false;
        }
    }
}
