package consys.event.registration.gwt.client.module.register.outer;

import com.google.gwt.resources.client.CssResource;

/**
 *
 * @author pepa
 */
public interface OuterCss extends CssResource {

    String top();

    String topCenter();

    String topSeparator();

    String topTakeplace();

    String topLanguage();

    String bottomSeparator();

    String bottom1();

    String bottom2();

    String bottomCenter();

    String bottomTakeplace();

    String bottomMenu();

    String bottomTextSeparator();

    String contentPanel();

    String contentPanelNoFooter();

    String middlePartTop();

    String middlePartCenterWrapper();

    String middlePartCenter();

    String middlePartSeparator();

    String middlePartBottom();

    String htmlStyle();

    String formTitleBig();

    String completeYourProfile();

    String passwordPanel();

    String showPasswordPanel();

    String dontHaveAny();

    String centerPanel();

    String title();

    String providedByTakeplace();

    String partBottom();

    String outerBundle();

    String registerButton();

    String fail();

    String eventLogo();

    String descriptionPanel();

    String portraitPanel();

    String agreeing();

    String middleBottomSeparator();

    String eventDescriptionOverflow();
}
