package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author pepa
 */
public class ClientCreateQuestion implements IsSerializable {

    // data
    private String name;
    private boolean required;

    public ClientCreateQuestion() {
    }

    public ClientCreateQuestion(String name, boolean required) {
        this.name = name;
        this.required = required;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
