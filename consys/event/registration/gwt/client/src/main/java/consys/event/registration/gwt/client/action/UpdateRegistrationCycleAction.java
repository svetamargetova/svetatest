package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;

/**
 * Aktualizuje registracni cyklus
 * @author pepa
 */
public class UpdateRegistrationCycleAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 6032907252008537444L;
    // data
    private ClientRegistrationCycle cycle;

    public UpdateRegistrationCycleAction() {
    }

    public UpdateRegistrationCycleAction(ClientRegistrationCycle cycle) {
        this.cycle = cycle;
    }

    public ClientRegistrationCycle getCycle() {
        return cycle;
    }
}
