package consys.event.registration.gwt.client.module.register.panel;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.ui.layout.panel.PanelItem;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class OuterRegistrationFormPanel extends PanelItem {

    // konstanty
    public static final String PANEL_NAME = "OuterRegisterPanel";
    public static final String OUTER_REGISTER_EVENT_NAME = "OuterRegisterEventName";

    public OuterRegistrationFormPanel() {
        super(false);
    }

    @Override
    public String getName() {
        return PANEL_NAME;
    }

    @Override
    public Widget getTitle() {
        return null;
    }

    @Override
    public Widget getContent() {
        FlowPanel panel = new FlowPanel();
        panel.setStyleName(StyleUtils.PADDING_VER_5);

        Label titleLabel = new Label(ERMessageUtils.c.outerRegisterPanel_title());
        titleLabel.setStyleName(StyleUtils.FONT_19PX);
        titleLabel.addStyleName(StyleUtils.FONT_BOLD);
        titleLabel.addStyleName(StyleUtils.TEXT_BLUE);
        titleLabel.addStyleName(StyleUtils.MARGIN_HOR_10);
        titleLabel.addStyleName(StyleUtils.MARGIN_BOT_10);
        panel.add(titleLabel);

        try {
            HTML p = new HTML(ERMessageUtils.m.outerRegisterPanel_text1(
                    (String) Cache.get().getSafe(OUTER_REGISTER_EVENT_NAME)));
            p.addStyleName(StyleUtils.MARGIN_HOR_10);
            panel.add(p);
        } catch (NotInCacheException ex) {
            // neni v cache, nemam co zobrazit!
        }

        panel.add(StyleUtils.getStyledLabel(ERMessageUtils.c.outerRegisterPanel_text_beta(),
                StyleUtils.MARGIN_HOR_10, StyleUtils.MARGIN_TOP_10));

        return panel;
    }
}
