package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConfirmDialog;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.CancelOrderAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.event.OrderCanceledEvent;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Panel zobrazujici jednu objednavku registrace
 * <p/>
 * @author pepa
 */
public class RegistrationOrderPanel extends ConsysFlowPanel {

    // data    
    private ClientRegistrationOrder order;
    private EventUser owner;
    private boolean isAdminView;

    public RegistrationOrderPanel(ClientRegistrationOrder order, EventUser owner) {
        this(order, owner, false);
    }

    public RegistrationOrderPanel(ClientRegistrationOrder order, EventUser owner, boolean isAdminView) {
        this.order = order;
        this.owner = owner;
        this.isAdminView = isAdminView;

        addStyleName(StyleUtils.MARGIN_VER_20);
    }

    public String getOrderUuid() {
        return order.getUuid();
    }

    @Override
    protected void onLoad() {
        clear();
        generate();
    }

    /** vytvori obsah objednavky */
    private void generate() {
        clear();
        add(Separator.addedStyle(StyleUtils.MARGIN_BOT_20));
        add(StyleUtils.clearDiv("20px"));
        add(rightCol());
        add(leftCol());
        add(StyleUtils.clearDiv());
    }

    /** vytvori levy panel */
    private SimplePanel leftCol() {
        OrderControlPanel orderControlPanel = new OrderControlPanel(order, isAdminView);
        orderControlPanel.setCancelClickHandler(cancelOrderClickHandler());

        SimplePanel panel = new SimplePanel(orderControlPanel);
        panel.setStyleName(StyleUtils.FLOAT_LEFT);
        panel.setWidth("330px");
        return panel;
    }

    /** vytvori pravy panel */
    private SimplePanel rightCol() {
        SimplePanel panel = new SimplePanel(new OrderDetailPanel(order));
        panel.setStyleName(StyleUtils.FLOAT_RIGHT);
        panel.setWidth("350px");
        return panel;
    }

    private ClickHandler cancelOrderClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final ConfirmDialog dialog = new ConfirmDialog(ERMessageUtils.c.registrationFormRegistered_cancel_order_question_title());
                ConsysAction action = new ConsysAction() {
                    @Override
                    public void run() {
                        EventDispatchEvent cancelEvent = new EventDispatchEvent(
                                new CancelOrderAction(order.getUuid(), true),
                                new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne vyjimky zpracovava EventActionExecuter
                                if (caught instanceof NoRecordsForAction) {
                                    //getFailMessage().setText(ERMessageUtils.c.const_exceptionCancelRegistrationNoRecords());
                                }
                                dialog.hide();
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                EventBus.fire(new OrderCanceledEvent(order));
                                dialog.hide();
                            }
                        }, RegistrationOrderPanel.this);
                        EventBus.fire(cancelEvent);
                    }
                };
                dialog.setAction(action);
                dialog.showCentered();
            }
        };
    }
}
