package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;

/**
 * Akce pro nacteni detailu zadaneho balicku (podbalicky, jestli lze vlozit slevovy tiket, ...)
 * @author pepa
 */
public class LoadUserRegistrationDetailsAction extends EventAction<ArrayListResult<ClientRegistrationPack>> {

    private static final long serialVersionUID = 5008606828681900091L;
    // data
    private String bundleUuid;

    public LoadUserRegistrationDetailsAction() {
    }

    public LoadUserRegistrationDetailsAction(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }
}
