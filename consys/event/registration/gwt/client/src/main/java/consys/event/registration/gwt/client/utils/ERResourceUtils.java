package consys.event.registration.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.event.registration.gwt.client.utils.css.ERBundle;
import consys.event.registration.gwt.client.utils.css.ERCssResources;

/**
 *
 * @author pepa
 */
public class ERResourceUtils {

    private static ERBundle bundle;

    static {
        bundle = (ERBundle) GWT.create(ERBundle.class);        
        bundle.css().ensureInjected();                
    }

    public static ERBundle bundle() {
        return bundle;
    }
    
    public static ERCssResources css() {
        return bundle.css();
    }
}
