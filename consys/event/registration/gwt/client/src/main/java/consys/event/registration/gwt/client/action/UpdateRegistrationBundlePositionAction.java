package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.action.UpdateOrderPositionAction;

/**
 * Akce pro aktualizaci pozice balicku
 * @author pepa
 */
public class UpdateRegistrationBundlePositionAction extends UpdateOrderPositionAction {

    private static final long serialVersionUID = -8531959120363905161L;
    // data
    private String uuid;
    private boolean subbundle;

    public UpdateRegistrationBundlePositionAction() {
    }

    public UpdateRegistrationBundlePositionAction(String uuid, int oldPosition, int newPosition, boolean subbundle) {
        super(oldPosition, newPosition);
        this.uuid = uuid;
        this.subbundle = subbundle;
    }

    public boolean isSubbundle() {
        return subbundle;
    }

    public String getUuid() {
        return uuid;
    }
}
