package consys.event.registration.gwt.client.bo;

import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.UserUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ClientListParticipantItem extends DataListUuidItem {

    private static final long serialVersionUID = 8504375817511170978L;
    private String userUuid;
    private String userName;
    private String userPosition;
    private String userOrganization;
    private String uuidPortraitImagePrefix;

    /**
     * @return the userUuid
     */
    public String getUserUuid() {
        return userUuid;
    }

    /**
     * @param userUuid the userUuid to set
     */
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAffiliation() {
        return UserUtils.affilation(userPosition, userOrganization, UIMessageUtils.c.const_at());
    }

    public String getUserOrganization() {
        return userOrganization;
    }

    public void setUserOrganization(String userOrganization) {
        this.userOrganization = userOrganization;
    }

    public String getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(String userPosition) {
        this.userPosition = userPosition;
    }

    public String getUuidPortraitImagePrefix() {
        return uuidPortraitImagePrefix;
    }

    public void setUuidPortraitImagePrefix(String uuidPortraitImagePrefix) {
        this.uuidPortraitImagePrefix = uuidPortraitImagePrefix;
    }
}
