package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Result pro zobrazeni vyberu pri registraci
 * @author pepa
 */
public class ClientRegistrationResult implements Result {

    // konstanty
    private static final long serialVersionUID = 1211341025368571395L;
    // data
    private ArrayList<ClientRegistrationPack> bundles;
    private ClientRegistration registration;

    public ClientRegistrationResult() {
        bundles = new ArrayList<ClientRegistrationPack>();
    }

    public ArrayList<ClientRegistrationPack> getBundles() {
        return bundles;
    }

    public void setBundles(ArrayList<ClientRegistrationPack> bundles) {
        this.bundles = bundles;
    }

    public ClientRegistration getRegistration() {
        return registration;
    }

    public void setRegistration(ClientRegistration registration) {
        this.registration = registration;
    }
}
