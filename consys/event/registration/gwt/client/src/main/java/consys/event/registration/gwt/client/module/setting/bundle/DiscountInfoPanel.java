package consys.event.registration.gwt.client.module.setting.bundle;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.InlineLabel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.event.registration.gwt.client.action.ListRegistrationSettingsAction;
import consys.event.registration.gwt.client.bo.ClientBundle;
import consys.event.registration.gwt.client.module.coupon.CouponList;
import consys.event.registration.gwt.client.module.coupon.CreateDiscountCouponDialog;
import consys.event.registration.gwt.client.module.coupon.CreateDiscountCouponDialog.RefreshAfterCreateDiscount;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class DiscountInfoPanel extends FlowPanel implements CssStyles {

    // data
    private ActionExecutionDelegate delegate;
    private RefreshAfterCreateDiscount refresh;
    private String bundleUuid;
    private int totalCodes;
    private int usedCodes;

    public DiscountInfoPanel(ActionExecutionDelegate delegate, RefreshAfterCreateDiscount refresh) {
        this.delegate = delegate;
        this.refresh = refresh;
    }

    /** necha pregenerovat obsah */
    public void refresh(ClientBundle cb) {
        bundleUuid = cb.getUuid();
        totalCodes = cb.getDiscountCodes();
        usedCodes = cb.getDiscountUsed();
        generate();
    }

    /** vygeneruje obsah panelu se slevovymi kody */
    private void generate() {
        clear();

        InlineLabel couponsText = new InlineLabel(ERMessageUtils.c.bundleItem_text_coupons() + ": ");
        ActionLabel coupons = new ActionLabel(String.valueOf(totalCodes));
        coupons.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showDiscountCoupons(null);
            }
        });

        InlineLabel usedText = new InlineLabel(" " + ERMessageUtils.c.bundleItem_text_used() + ": ");
        ActionLabel used = new ActionLabel(String.valueOf(usedCodes));
        used.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showDiscountCoupons(true);
            }
        });

        InlineLabel notUsedText = new InlineLabel(" " + ERMessageUtils.c.bundleItem_text_notUsed() + ": ");
        ActionLabel notUsed = new ActionLabel(String.valueOf(totalCodes - usedCodes));
        notUsed.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                showDiscountCoupons(false);
            }
        });


        ActionLabel individual = new ActionLabel("+ " + ERMessageUtils.c.discountInfoPanel_action_individualCoupon(),
                FLOAT_LEFT + " " + MARGIN_RIGHT_10);
        individual.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                loadListAndShowDialog(true);
            }
        });

        ActionLabel group = new ActionLabel(" + " + ERMessageUtils.c.discountInfoPanel_action_groupCoupon(), FLOAT_LEFT);
        group.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                loadListAndShowDialog(false);
            }
        });

        FlowPanel actionPanel = new FlowPanel();
        actionPanel.addStyleName(MARGIN_TOP_10);
        actionPanel.add(individual);
        actionPanel.add(group);
        actionPanel.add(StyleUtils.clearDiv());

        add(StyleUtils.getStyledLabel(ERMessageUtils.c.bundleItem_text_discountTitle(), FONT_BOLD, FONT_16PX, MARGIN_BOT_10));
        add(couponsText);
        add(coupons);
        add(usedText);
        add(used);
        add(notUsedText);
        add(notUsed);
        add(actionPanel);
    }

    /** necha zobrazit seznam se slevovymi kody */
    private void showDiscountCoupons(Boolean used) {
        CouponList w = new CouponList(bundleUuid);
        w.setUsedFilter(used);
        FormUtils.fireNextBreadcrumb(ERMessageUtils.c.discountList_title(), w);
    }

    /** nacte aktualni balicky/subbalicky a zobrazi dialog */
    private void loadListAndShowDialog(final boolean individual) {
        EventBus.get().fireEvent(new EventDispatchEvent(new ListRegistrationSettingsAction(),
                new AsyncCallback<ArrayListResult<ClientTabItem>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientTabItem> result) {
                        ArrayList<SelectBoxItem<String>> bundleList = new ArrayList<SelectBoxItem<String>>();
                        for (ClientTabItem i : result.getArrayListResult()) {
                            bundleList.add(new SelectBoxItem<String>(i.getUuid(), i.getTitle()));
                        }
                        new CreateDiscountCouponDialog(individual, bundleList, bundleUuid, refresh).showCentered();
                    }
                }, delegate));
    }
}
