package consys.event.registration.gwt.client.module.setting;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.module.AsyncModule;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.OrderDialog;
import consys.common.gwt.client.ui.comp.panel.OrderPanel.OrderPanelListener;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabOrderPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItemUI;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.CreateRegistrationBundleAction;
import consys.event.registration.gwt.client.action.ListRegistrationSettingsAction;
import consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction;
import consys.event.registration.gwt.client.bo.ClientCreateBundle;
import consys.event.registration.gwt.client.module.exception.BundleUniquityException;
import consys.event.registration.gwt.client.module.setting.bundle.BundleItem;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ModuleNavigationUtils;
import java.util.ArrayList;

/**
 * Formular pro nastavovani bundle
 * @author pepa
 */
public class RegistrationSettingsBundle extends RootPanel implements OrderPanelListener<ConsysTabOrderPanelItem<String>> {

    // instance
    private static RegistrationSettingsBundle instance;
    // komponenty
    private ConsysTabPanel<String> tabs;
    private ConsysMessage helpMessage;
    private OrderDialog orderDialog;
    private boolean subbundle;

    public RegistrationSettingsBundle(boolean subbundle) {
        super(subbundle
                ? ERMessageUtils.c.registrationSettingsSubbundle_title() : ERMessageUtils.c.registrationSettingsBundle_title());
        this.subbundle = subbundle;

        if (subbundle) {
            addLeftControll(ActionImage.breadcrumbBackBigBack());

            ActionImage add = ActionImage.getPlusButton(ERMessageUtils.c.registrationSettingsSubbundle_action_addTicketAccessory(), true);
            add.addClickHandler(addTabClickHandler());
            addLeftControll(add);

            enableDownMenu(ModuleNavigationUtils.downMenuOptionBundlesSettings(),
                    ModuleNavigationUtils.downMenuOptionCouponList(),
                    ModuleNavigationUtils.downMenuOptionSystemProperty(),
                    ModuleNavigationUtils.downMenuCustomFields());
        } else {
            ActionImage add = ActionImage.getPlusButton(ERMessageUtils.c.registrationSettingsBundle_action_addTicket(), true);
            add.addClickHandler(addTabClickHandler());
            addLeftControll(add);

            enableDownMenu(ModuleNavigationUtils.downMenuOptionSubbundlesSettings(),
                    ModuleNavigationUtils.downMenuOptionCouponList(),
                    ModuleNavigationUtils.downMenuOptionSystemProperty(),
                    ModuleNavigationUtils.downMenuCustomFields());
        }
    }

    @Override
    protected void onLoad() {
        init();
        EventDispatchEvent loadSettingsEvent = new EventDispatchEvent(new ListRegistrationSettingsAction(subbundle),
                new AsyncCallback<ArrayListResult<ClientTabItem>>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava EventActionExcecutor
                        // ToDo: posle sa NoRecors.. ukazat spravu aby si to nastavili. Inac ukazovat len kolecko ze sa nacitva
                        if (caught instanceof NoRecordsForAction) {
                            //getFailMessage().setText("NoRecordsForAction");
                        }
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientTabItem> result) {
                        setRegistrationSettingsBundle(result.getArrayListResult());
                    }
                }, getSelf());
        EventBus.get().fireEvent(loadSettingsEvent);
    }

    /** zinicializuje formular */
    private void init() {
        clear();

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(subbundle
                ? ERMessageUtils.c.registrationSettingsSubbundle_text_pleaseAddTicketAccessory()
                : ERMessageUtils.c.registrationSettingsBundle_text_pleaseAddTicket());
        helpMessage.addStyleName(StyleUtils.MARGIN_TOP_10);
        helpMessage.hideClose(true);

        String panelName = RegistrationSettingsBundle.class.getName() + (subbundle ? "-subbundle" : "-bundle");

        tabs = new ConsysTabPanel<String>(panelName);
        tabs.setStyleName(StyleUtils.MARGIN_TOP_10);
        tabs.setVisible(false);
        tabs.showOrderIcon(new ConsysAction() {
            @Override
            public void run() {
                ArrayList<ConsysTabOrderPanelItem<String>> itemList = new ArrayList<ConsysTabOrderPanelItem<String>>();
                ArrayList<ConsysTabPanelItemUI<String>> ts = tabs.getTabs();
                for (ConsysTabPanelItemUI<String> ui : ts) {
                    itemList.add(new ConsysTabOrderPanelItem<String>(ui, ui.getTabName()));
                }

                orderDialog = new OrderDialog<ConsysTabOrderPanelItem<String>>(itemList, RegistrationSettingsBundle.this);
                orderDialog.showCentered();
            }
        });

        FlowPanel panel = new FlowPanel();
        panel.add(helpMessage);
        panel.add(tabs);
        addWidget(panel);
    }

    /** vytvori asynchronne instanci */
    public static void getAsync(final AsyncModule<RegistrationSettingsBundle> async) {
        GWT.runAsync(new RunAsyncCallback() {
            @Override
            public void onFailure(Throwable reason) {
                async.onFail();
            }

            @Override
            public void onSuccess() {
                if (instance == null) {
                    instance = new RegistrationSettingsBundle(true);
                }
                async.onSuccess(instance);
            }
        });
    }

    /** vytvori ClickHandler pro pridani zalozky */
    private ClickHandler addTabClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                NewItemDialog<NewItemUI> cd = new NewItemDialog<NewItemUI>(
                        subbundle
                        ? ERMessageUtils.c.registrationSettingsSubbundle_text_newTicketAccessory()
                        : ERMessageUtils.c.registrationSettingsBundle_text_newTicket(), helpMessage) {
                    @Override
                    public void doCreateAction(ArrayList<NewItemUI> uis, ConsysMessage fail) {
                        if (uis.size() <= 0) {
                            hide();
                            return;
                        }

                        for (NewItemUI ui : uis) {
                            if (ui.getName().trim().isEmpty()) {
                                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(UIMessageUtils.c.const_name()));
                                return;
                            }
                        }

                        final ArrayList<ClientCreateBundle> bundles = new ArrayList<ClientCreateBundle>();
                        for (NewItemUI ui : uis) {
                            ClientCreateBundle ccb = new ClientCreateBundle();
                            ccb.setTitle(ui.getName().trim());
                            ccb.setDescription(ui.getDescription().trim());
                            bundles.add(ccb);
                        }

                        EventDispatchEvent createEvent = new EventDispatchEvent(
                                new CreateRegistrationBundleAction(bundles, subbundle),
                                new AsyncCallback<ArrayListResult<String>>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava EventActionExcecutor
                                        if (caught instanceof BundleUniquityException) {
                                            RegistrationSettingsBundle.this.getFailMessage().setText(ERMessageUtils.c.const_exceptionTicketUniquity());
                                        }
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<String> result) {
                                        ArrayList<String> uuids = result.getArrayListResult();
                                        for (int i = 0; i < uuids.size(); i++) {
                                            final ClientTabItem cti = new ClientTabItem();
                                            ClientCreateBundle cbt = bundles.get(i);
                                            cti.setTitle(cbt.getTitle());
                                            cti.setUuid(uuids.get(i));
                                            BundleItem.addToTab(tabs, cti, helpMessage, true);
                                            tabs.selectTabById(cti.getUuid());
                                        }

                                        tabs.setVisible(true);
                                        successCreateAction();
                                    }
                                }, this);
                        EventBus.get().fireEvent(createEvent);
                    }

                    @Override
                    public NewItemUI newItem() {
                        return new NewItemUI(255, 1000);
                    }
                };
                cd.showCentered();
            }
        };
    }

    /** vlozi nastaveni ze serveru */
    private void setRegistrationSettingsBundle(ArrayList<ClientTabItem> items) {
        for (ClientTabItem item : items) {
            BundleItem.addToTab(tabs, item, helpMessage, subbundle);
        }
        if (items.size() > 0) {
            helpMessage.setVisible(false);
            tabs.setVisible(true);
        }
    }

    @Override
    public void onOrderChange(final ConsysTabOrderPanelItem<String> object, final int oldPosition, final int newPosition) {
        String tabUuid = object.getUi().getId();
        EventBus.get().fireEvent(new EventDispatchEvent(
                new UpdateRegistrationBundlePositionAction(tabUuid, oldPosition, newPosition, subbundle),
                new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                        orderDialog.getOrderPanel().back();
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        int offset = orderDialog.getOrderPanel().getFinalPositionOffset();
                        tabs.changeTabPosition(object.getUi(), oldPosition - offset, newPosition - offset);
                    }
                }, orderDialog));
    }
}
