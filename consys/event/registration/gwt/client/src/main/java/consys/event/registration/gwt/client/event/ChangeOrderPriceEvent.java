package consys.event.registration.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.shared.bo.Monetary;

/**
 * Event vystreleny, ked si uzivatel vyberie nejaky subbalik alebo aplikuje
 * zlavovy kupon nebo zmeni kvantitu
 *
 * @author palo
 */
public class ChangeOrderPriceEvent extends GwtEvent<ChangeOrderPriceEvent.Handler> {

    /**
     * typ eventu
     */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data    
    private Monetary change;
    private String groupId;
    private boolean substract;

    public ChangeOrderPriceEvent(String groupId, Monetary change, boolean substract) {
        this.change = change;
        this.substract = substract;
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public Monetary getChange() {
        return change;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onPriceChange(this);
    }

    /**
     * @return the substract
     */
    public boolean isSubstract() {
        return substract;
    }

    /**
     * interface eventu
     */
    public interface Handler extends EventHandler {

        public void onPriceChange(ChangeOrderPriceEvent event);
    }
}
