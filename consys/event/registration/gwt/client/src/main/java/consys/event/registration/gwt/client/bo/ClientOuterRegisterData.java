package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Data pro vnejsi registraci noveho uzivatele
 * @author pepa
 */
public class ClientOuterRegisterData implements Result {

    private static final long serialVersionUID = -4059396339615799877L;
    
    // mena
    private String currency;
    // jednotlive baliky
    private ArrayList<ClientRegistrationPack> items;
    

    public ClientOuterRegisterData() {        
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    /** prvni subbalicek je hlavni registrace */
    public ArrayList<ClientRegistrationPack> getItems() {
        return items;
    }

    /** prvni subbalicek je hlavni registrace */
    public void setItems(ArrayList<ClientRegistrationPack> items) {
        this.items = items;
    }
}
