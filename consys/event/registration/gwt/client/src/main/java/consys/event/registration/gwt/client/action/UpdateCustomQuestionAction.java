package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro aktualizaci otazky administratorem
 * @author pepa
 */
public class UpdateCustomQuestionAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 6091013025115556030L;
    // data
    private String questionUuid;
    private boolean required;
    private String text;

    public UpdateCustomQuestionAction() {
    }

    public UpdateCustomQuestionAction(String questionUuid, boolean required, String text) {
        this.questionUuid = questionUuid;
        this.required = required;
        this.text = text;
    }

    public boolean isRequired() {
        return required;
    }

    public String getText() {
        return text;
    }

    public String getQuestionUuid() {
        return questionUuid;
    }
}
