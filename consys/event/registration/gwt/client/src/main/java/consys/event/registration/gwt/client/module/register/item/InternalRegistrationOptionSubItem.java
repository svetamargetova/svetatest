package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 *
 * @author pepa
 */
public class InternalRegistrationOptionSubItem extends FlowPanel {

    // komponenty
    private FlowPanel contentWrapper;
    private FlowPanel titlePanel;
    private Label priceLabel;
    private QuantityPanel quantityPanel;
    private ConsysCheckBox checkBox;
    // data
    private ClientRegistrationPack subItem;
    private String currency;

    public InternalRegistrationOptionSubItem(ClientRegistrationPack subItem, String currency) {
        this.subItem = subItem;
        this.currency = currency;
    }

    @Override
    protected void onLoad() {
        clear();
        setStyleName(ERResourceUtils.bundle().css().registrationFormSubItem());

        checkBox = new ConsysCheckBox();
        checkBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {
            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                if (event.getValue()) {
                    select();
                } else {
                    unselect();
                }
            }
        });

        quantityPanel = new QuantityPanel(subItem.getUuidBundle(), subItem.getMaxQuantity(), false, subItem.getPrice());

        contentWrapper = new FlowPanel();
        contentWrapper.setStyleName(ERResourceUtils.bundle().css().registrationItemContent());
        contentWrapper.add(getTitlePanel());
        contentWrapper.add(quantityPanel);
        add(contentWrapper);
    }

    private FlowPanel getTitlePanel() {
        SimplePanel titleActionPanel = new SimplePanel(checkBox);
        titleActionPanel.setStyleName(ERResourceUtils.bundle().css().registrationItemTitleAction());

        Label titleLabel = StyleUtils.getStyledLabel(subItem.getBundleTitle(), ERResourceUtils.bundle().css().registrationItemTitleLabel());
        titleLabel.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                checkBox.setValue(!checkBox.getValue());
                if (checkBox.getValue()) {
                    select();
                } else {
                    unselect();
                }
            }
        });

        priceLabel = StyleUtils.getStyledLabel("", ERResourceUtils.bundle().css().registrationItemTitlePrice());
        priceLabel.addStyleName(ResourceUtils.common().css().notActive());

        titlePanel = new FlowPanel();
        titlePanel.add(titleActionPanel);
        titlePanel.add(titleLabel);
        titlePanel.add(StyleUtils.clearDiv());

        FlowPanel panel = RegistrationFormPanelItem.getBasePanel(titlePanel, priceLabel);
        panel.addStyleName(ERResourceUtils.bundle().css().registrationItemTitle());
        if (RegistrationFormPanelItem.isActiveBundle(subItem)) {
            priceLabel.setText(UIMessageUtils.assignCurrency(currency, subItem.getPrice()));
        } else {
            panel.addStyleName(ResourceUtils.common().css().notActive());
            priceLabel.setText(FormUtils.NDASH);
        }
        return panel;
    }

    private void select() {
        priceLabel.removeStyleName(ResourceUtils.common().css().notActive());
    }

    private void unselect() {
        priceLabel.addStyleName(ResourceUtils.common().css().notActive());
    }

    public boolean isSelected() {
        return checkBox != null ? checkBox.getValue() : false;
    }

    public ProductThumb getProductThumb() {
        int quantity = quantityPanel != null ? quantityPanel.getQuantity() : 1;
        return new ProductThumb(subItem.getUuidBundle(), null, quantity);
    }
}
