/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.registration.gwt.client.utils;

import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import java.util.Date;
import java.util.List;

/**
 *
 * @author palo
 */
public class ValidationUtils {

    public static boolean areRegistrationCycleDatesValid(ConsysDateBox fromBox, ConsysDateBox toBox, ConsysMessage fail) {
        Date from = fromBox.getValue();
        Date to = toBox.getValue();
        if (from == null) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ECoMessageUtils.c.const_date_from()));
            return false;
        }

        if (to == null) {
            fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ECoMessageUtils.c.const_date_to()));
            return false;
        }
        if (!from.before(to)) {
            fail.addOrSetText(ERMessageUtils.m.registrationCycleList_cycle_exception_endBeforeStart(
                    fromBox.getAsText(), toBox.getAsText()));
            return false;
        }
        if (from.compareTo(to) == 1) {
            fail.addOrSetText(ERMessageUtils.m.registrationCycleList_cycle_exception_endSameAsStart(
                    fromBox.getAsText(), toBox.getAsText()));
            return false;
        }
        return true;
    }

    public static boolean isRegistrationCycleOverlapping(Date testCycleFrom, Date testCycleTo, List<ClientRegistrationCycle> cycles) {
        
        for (ClientRegistrationCycle existing : cycles) {
            // novy zacatek musi byt po prechadzajucem konci alebo sa mu rovnat 
            LoggerFactory.log(ValidationUtils.class, "Validating: "+testCycleFrom+" : "+testCycleTo + " <=> "+existing.getFrom()+" : "+existing.getTo());
            if ((testCycleFrom.after(existing.getTo())  || testCycleFrom.compareTo(existing.getTo()) == 0)) {
                continue;
            } else if ((existing.getFrom().after(testCycleTo) || existing.getFrom().compareTo(testCycleTo) == 0)) {
                continue;
            } else {
                return true;
            }
        }
        return false;
    }
}
