package consys.event.registration.gwt.client.module.register;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.event.registration.gwt.client.action.managed.ListManagedRegistrationsAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations;
import consys.event.registration.gwt.client.module.register.item.AddManagedUserDialog;
import consys.event.registration.gwt.client.module.register.item.RegisterAnotherItem;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class RegisterAnother extends RootPanel {

    // komponenty
    private ConsysMessage helpMessage;

    public RegisterAnother() {
        super(ERMessageUtils.c.registerAnother_title());

        ActionImage add = ActionImage.getPlusButton(ERMessageUtils.c.registerAnother_action_addManagedParticipant(), true);
        add.addClickHandler(addClickHandler());
        addLeftControll(add);

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ERMessageUtils.c.registerAnother_text_pleaseAddParticipant());
        helpMessage.addStyleName(StyleUtils.MARGIN_TOP_10);
        helpMessage.hideClose(true);
    }

    private ClickHandler addClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                AddManagedUserDialog d = new AddManagedUserDialog(RegisterAnother.this, helpMessage);
                d.showCentered();
            }
        };
    }

    @Override
    protected void onLoad() {
        reload();
    }

    /** nacte nova data a prekresli stranku */
    public void reload() {
        clear();
        EventBus.fire(new EventDispatchEvent(new ListManagedRegistrationsAction(),
                new AsyncCallback<ClientManagedRegistrations>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientManagedRegistrations result) {
                        if (result.getParticipants().isEmpty() && result.getRegistrations().isEmpty()) {
                            noRecords();
                        } else {
                            drawRecords(result.getParticipants(), result.getRegistrations());
                        }
                    }
                }, this));
    }

    private void noRecords() {
        addWidget(helpMessage);
    }

    private void drawRecords(ArrayList<EventUser> participants, ArrayList<ClientRegistration> registrations) {
        Map<String, ClientRegistration> reg = new HashMap<String, ClientRegistration>(registrations.size());
        for (ClientRegistration cr : registrations) {
            reg.put(cr.getEventUser().getUuid(), cr);
        }

        boolean notFirst = false;
        for (EventUser eu : participants) {
            if (notFirst) {
                addWidget(Separator.addedStyle(StyleUtils.MARGIN_VER_10));
            }
            addWidget(new RegisterAnotherItem(eu, reg.get(eu.getUuid())));
            notFirst = true;
        }
    }
}
