package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.Monetary;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
public class ClientUserRegistrationDetails implements Result {

    private static final long serialVersionUID = 3645986218384155376L;
    // data
    private ArrayList<ClientRegistrationPack> packs;
    private HashMap<Date, Monetary> pricing;

    public ClientUserRegistrationDetails() {
        packs = new ArrayList<ClientRegistrationPack>();
        pricing = new HashMap<Date, Monetary>();
    }

    public ArrayList<ClientRegistrationPack> getPacks() {
        return packs;
    }

    public void setPacks(ArrayList<ClientRegistrationPack> packs) {
        this.packs = packs;
    }

    public HashMap<Date, Monetary> getPricing() {
        return pricing;
    }

    public void setPricing(HashMap<Date, Monetary> pricing) {
        this.pricing = pricing;
    }
}
