package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author pepa
 */
public class ClientWaitingBundleInfo implements IsSerializable {

    // data
    private String bundleUuid;
    private String bundleName;
    private int filledCapacity;
    private int fullCapacity;

    public String getBundleName() {
        return bundleName;
    }

    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public void setBundleUuid(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public int getFilledCapacity() {
        return filledCapacity;
    }

    public void setFilledCapacity(int filledCapacity) {
        this.filledCapacity = filledCapacity;
    }

    public int getFullCapacity() {
        return fullCapacity;
    }

    public void setFullCapacity(int fullCapacity) {
        this.fullCapacity = fullCapacity;
    }
}
