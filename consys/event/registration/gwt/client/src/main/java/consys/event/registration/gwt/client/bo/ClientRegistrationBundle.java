package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Jedna polozka zobrazovana pro vyber pri registraci
 * @author pepa
 */
public class ClientRegistrationBundle implements IsSerializable {

    private String uuid;
    private String title;
    private String description;
    private boolean codeEnter;
    private int capacity;
    private int value;
    private boolean enabled;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isCodeEnter() {
        return codeEnter;
    }

    public void setCodeEnter(boolean codeEnter) {
        this.codeEnter = codeEnter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
