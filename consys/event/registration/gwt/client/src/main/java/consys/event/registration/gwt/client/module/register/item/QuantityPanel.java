package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.input.ConsysSelectBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.Monetary;
import consys.event.registration.gwt.client.event.ChangeOrderPriceEvent;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class QuantityPanel extends ConsysFlowPanel {

    // komponenty
    private Composite selectBox;
    private Label multipleLabel;
    // data
    private String groupId;
    private final int maxQuantity;
    private boolean newComponents;
    private Monetary basePrice;
    private int quantity = 1;

    public QuantityPanel(String groupId, int maxQuantity, boolean newComponents, Monetary basePrice) {
        this.groupId = groupId;
        this.maxQuantity = maxQuantity;
        this.newComponents = newComponents;
        this.basePrice = basePrice;

        setStyleName(ERResourceUtils.css().quantityPanel());
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    protected void onLoad() {
        generateContent();
    }

    private void generateContent() {
        clear();

        ArrayList<SelectBoxItem<Integer>> list = new ArrayList<SelectBoxItem<Integer>>();
        for (int i = 1; i <= maxQuantity; i++) {
            list.add(new SelectBoxItem<Integer>(i));
        }

        if (newComponents) {
            ConsysSelectBox<Integer> sb = new ConsysSelectBox<Integer>(60);
            sb.setItems(list);
            sb.setSelectedItem(list.get(0));
            sb.addChangeValueHandler(quantityChange());
            selectBox = sb;
        } else {
            SelectBox<Integer> sb = new SelectBox<Integer>();
            sb.setWidth(50);
            sb.setItems(list);
            sb.setSelectedItem(list.get(0));
            sb.addChangeValueHandler(quantityChange());
            selectBox = sb;
        }
        selectBox.addStyleName(ERResourceUtils.css().quantityPanelBox());

        multipleLabel = StyleUtils.getStyledLabel("x 1", ERResourceUtils.css().quantityPanelMultipleLabel());

        Label label = StyleUtils.getStyledLabel(ERMessageUtils.c.editFormBundle_form_quantity(),
                ERResourceUtils.css().quantityPanelLabel());

        add(multipleLabel);
        add(label);
        add(selectBox);
        add(StyleUtils.clearDiv());
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 1) {
            this.quantity = 1;
        } else if (quantity > maxQuantity) {
            this.quantity = maxQuantity;
        } else {
            this.quantity = quantity;
        }

        int originalValue;
        if (newComponents) {
            ConsysSelectBox<Integer> sb = (ConsysSelectBox<Integer>) selectBox;
            originalValue = sb.getSelectedItem().getItem();
            sb.selectItem(this.quantity);
        } else {
            SelectBox<Integer> sb = (SelectBox<Integer>) selectBox;
            originalValue = sb.getSelectedItem().getItem();
            sb.selectItem(this.quantity);
        }

        changePrice(originalValue, this.quantity);
    }

    /** pri zmene v selectboxu provola zmenu na cenu */
    private ChangeValueEvent.Handler<SelectBoxItem<Integer>> quantityChange() {
        return new ChangeValueEvent.Handler<SelectBoxItem<Integer>>() {
            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<Integer>> event) {
                SelectBoxItem<Integer> oldValue = event.getOldValue();
                SelectBoxItem<Integer> newValue = event.getValue();
                changePrice(oldValue.getItem(), newValue.getItem());
            }
        };
    }

    /** necha zmenit cenu podle predeslo a noveho multiplikatoru */
    private void changePrice(final int oldValue, final int newValue) {
        quantity = newValue;
        multipleLabel.setText("x " + quantity);

        Monetary oldMonetary = basePrice.multiple(oldValue);
        Monetary newMonetary = basePrice.multiple(newValue);

        EventBus.fire(new ChangeOrderPriceEvent(groupId, newMonetary, false));
        EventBus.fire(new ChangeOrderPriceEvent(groupId, oldMonetary, true));
    }
}
