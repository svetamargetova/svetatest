package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro zjisteni jestli je uzivatel jiz zaregistrovany
 * @author pepa
 */
public class LoadIsUserRegisteredAction extends EventAction<BooleanResult> {

    private static final long serialVersionUID = -3886152832547529539L;
}
