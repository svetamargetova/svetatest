package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.Monetary;
import java.util.Date;

/**
 * Klientsky nahled registracniho cyklu
 * @author pepa
 */
public class ClientRegistrationCycle implements IsSerializable, Result {

    private static final long serialVersionUID = -6821025810200014815L;
    // data
    private String uuid;
    private Date from;
    private Date to;
    private Monetary value;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public Monetary getValue() {
        return value;
    }

    public void setValue(Monetary value) {
        this.value = value;
    }
}
