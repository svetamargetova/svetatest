package consys.event.registration.gwt.client.module.register.outer.comp;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.registration.gwt.client.module.register.outer.OuterResources;

/**
 * Seda stredni cast do OuterLayout
 * @author pepa
 */
public class OuterMiddlePart extends Composite {

    // komponenty
    private FlowPanel mainPanel;
    private SimplePanel head;
    private FlowPanel content;

    public OuterMiddlePart() {
        mainPanel = new FlowPanel();
        initWidget(mainPanel);

        generate();
    }

    private void generate() {
        mainPanel.add(topPart());
        mainPanel.add(middlePart());
        mainPanel.add(bottomPart());
    }

    private Widget topPart() {
        head = new SimplePanel();
        head.setStyleName(OuterResources.INSTANCE.css().middlePartCenter());

        SimplePanel wrapper = FormUtils.createPanel(OuterResources.INSTANCE.css().middlePartTop());
        wrapper.setWidget(head);
        return wrapper;
    }

    private Widget middlePart() {
        content = new FlowPanel();
        content.setStyleName(OuterResources.INSTANCE.css().middlePartCenter());

        SimplePanel wrapper = FormUtils.createPanel(OuterResources.INSTANCE.css().middlePartCenterWrapper());
        wrapper.setWidget(content);
        return wrapper;
    }

    private Widget bottomPart() {
        return FormUtils.createPanel(OuterResources.INSTANCE.css().middlePartBottom());
    }

    public void setHead(Widget widget) {
        head.setWidget(widget);
    }

    public void setContent(Widget widget) {
        content.clear();
        content.add(widget);
    }

    public FlowPanel getContent() {
        return content;
    }
}
