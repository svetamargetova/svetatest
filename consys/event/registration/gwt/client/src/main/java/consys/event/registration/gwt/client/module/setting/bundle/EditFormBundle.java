package consys.event.registration.gwt.client.module.setting.bundle;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysIntTextBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysDoubleTextBox;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction;
import consys.event.registration.gwt.client.bo.ClientBundle;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.event.registration.gwt.client.module.exception.BundleUniquityException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Editacni formular pro upravu jedne zalozky
 * @author pepa
 */
public class EditFormBundle extends RootPanel {

    // konstanty
    private static final int SYSTEM_MAX_QUANTITY = 20;
    // komponenty
    private SimpleFormWithHelpPanel mainPanel;
    private BaseForm form;
    private ConsysStringTextBox nameBox;
    private ConsysTextArea descBox;
    private ConsysCheckBox capacityCheckBox;
    //private ConsysCheckBox b2bCheckBox;
    private ConsysIntTextBox capacityBox;
    private ConsysCheckBox quantityCheckBox;
    private ConsysIntTextBox quantityBox;
    private ConsysDoubleTextBox vatBox;
    // data
    private ClientBundle cbi;
    private BundleItem source;

    public EditFormBundle(ClientBundle cbi, BundleItem source) {
        super(!cbi.isSubbundle() ? ERMessageUtils.c.editFormBundle_title() : ERMessageUtils.c.editFormSubbundle_title());

        mainPanel = new SimpleFormWithHelpPanel();
        addWidget(mainPanel);

        this.cbi = cbi;
        this.source = source;
        init();
    }

    /** vygeneruje editační formulář */
    private void init() {
        form = new BaseForm();
        form.setStyleName(StyleUtils.MARGIN_TOP_10);

        nameBox = new ConsysStringTextBox(1, 255, UIMessageUtils.c.const_name());
        nameBox.setText(cbi.getTitle());
        descBox = new ConsysTextArea(0, 1000, UIMessageUtils.c.const_description());
        descBox.setText(cbi.getDescription());

        boolean withCapacity = cbi.getCapacity() > 0;
        capacityCheckBox = new ConsysCheckBox();
        capacityCheckBox.setValue(withCapacity);
        capacityCheckBox.addStyleName(StyleUtils.FLOAT_LEFT);
        capacityCheckBox.addStyleName(StyleUtils.MARGIN_TOP_3);
        capacityBox = new ConsysIntTextBox(0, 200000, ERMessageUtils.c.editFormBundle_form_capacity());
        capacityBox.addStyleName(StyleUtils.FLOAT_LEFT);
        capacityBox.addStyleName(StyleUtils.MARGIN_LEFT_10);
        capacityBox.setEnabled(withCapacity);
        capacityBox.setWidth("245px");
        if (withCapacity) {
            capacityBox.setText(cbi.getCapacity());
        }
        capacityCheckBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {
            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                capacityBox.setEnabled(event.getValue());
                if (!event.getValue()) {
                    capacityBox.setText("");
                } else {
                    capacityBox.setFocus(true);
                }
            }
        });

        ConsysFlowPanel capacityPanel = new ConsysFlowPanel();
        capacityPanel.add(capacityCheckBox);
        capacityPanel.add(capacityBox);

        boolean withQuantity = cbi.getQuantity() > 1;
        quantityCheckBox = new ConsysCheckBox();
        quantityCheckBox.setValue(withQuantity);
        quantityCheckBox.addStyleName(StyleUtils.FLOAT_LEFT);
        quantityCheckBox.addStyleName(StyleUtils.MARGIN_TOP_3);
        quantityBox = new ConsysIntTextBox(1, SYSTEM_MAX_QUANTITY, ERMessageUtils.c.editFormBundle_form_maxQuantity());
        quantityBox.addStyleName(StyleUtils.FLOAT_LEFT);
        quantityBox.addStyleName(StyleUtils.MARGIN_LEFT_10);
        quantityBox.setEnabled(withQuantity);
        quantityBox.setWidth("245px");
        quantityBox.setText(cbi.getQuantity());
        quantityCheckBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {
            @Override
            public void onChangeValue(ChangeValueEvent<Boolean> event) {
                quantityBox.setEnabled(event.getValue());
                if (!event.getValue()) {
                    quantityBox.setText("1");
                } else {
                    quantityBox.setFocus(true);
                }
            }
        });

        ConsysFlowPanel quantityPanel = new ConsysFlowPanel();
        quantityPanel.add(quantityCheckBox);
        quantityPanel.add(quantityBox);

        vatBox = new ConsysDoubleTextBox(0, 100, ERMessageUtils.c.editFormSubbundle_form_vatPercent());
        vatBox.setText(cbi.getVat());

        //b2bCheckBox = new ConsysCheckBox();
        //b2bCheckBox.setValue(cbi.isB2BTicket());

        ActionImage button = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        button.addClickHandler(buttonClickHandler());

        form.addRequired(0, UIMessageUtils.c.const_name(), nameBox);
        form.addOptional(1, UIMessageUtils.c.const_description(), descBox);
        form.addOptional(2, ERMessageUtils.c.editFormSubbundle_form_vatPercent(), vatBox);
        form.addOptional(3, ERMessageUtils.c.editFormBundle_form_capacity(), capacityPanel);
        form.addOptional(4, ERMessageUtils.c.editFormBundle_form_maxQuantity(), quantityPanel);
        /*form.addOptional(4, cbi.isSubbundle() ? ERMessageUtils.c.editFormBundle_form_b2bTicketAccessory()
         : ERMessageUtils.c.editFormBundle_form_b2bTicket(), b2bCheckBox);*/
        form.addActionMembers(5, button, ActionLabel.breadcrumbBackCancel());
        mainPanel.addWidget(form);

        //mainPanel.addHelpTitle("Help title");
        //mainPanel.addHelpText("help text ...");
    }

    /** ClickHandler pro potvrzovaci tlacitko */
    private ClickHandler buttonClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                // validace
                if (!form.validate(getFailMessage())) {
                    return;
                }

                final ClientBundle data = new ClientBundle();
                data.setUuid(cbi.getUuid());
                data.setTitle(nameBox.getText().trim());
                data.setSubbundle(cbi.isSubbundle());
                data.setDescription(descBox.getText().trim());
                data.setCapacity(capacityCheckBox.getValue() == Boolean.TRUE ? capacityBox.getTextInt() : 0);
                data.setQuantity(quantityCheckBox.getValue() == Boolean.TRUE ? quantityBox.getTextInt() : 1);
                data.setVat(vatBox.getTextDouble());
                //data.setB2BTicket(b2bCheckBox.getValue());
                data.setB2BTicket(false);

                EventDispatchEvent updateEvent = new EventDispatchEvent(
                        new UpdateRegistrationBundleAction(data),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava EventActionExcecutor
                                if (caught instanceof BundleUniquityException) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionTicketUniquity());
                                }
                                if (caught instanceof BundleCapacityException) {
                                    getFailMessage().setText(ERMessageUtils.c.const_exceptionTicketCapacity());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                source.setData(data);
                                FormUtils.fireBreadcrumbBack();
                            }
                        }, mainPanel);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }
}
