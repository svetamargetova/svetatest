package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.DateResult;
import consys.common.gwt.shared.action.EventAction;

/**
 *
 * @author pepa
 */
public class UpdateCheckInAction extends EventAction<DateResult> {

    private static final long serialVersionUID = 4616509704743895251L;
    // data
    private boolean checked;
    private String registrationUuid;
    private int count;

    public UpdateCheckInAction() {
    }

    public UpdateCheckInAction(boolean checked, String registrationUuid, int count) {
        this.checked = checked;
        this.registrationUuid = registrationUuid;
        this.count = count;
    }

    public boolean isChecked() {
        return checked;
    }

    public int getCount() {
        return count;
    }

    public String getRegistrationUuid() {
        return registrationUuid;
    }
}
