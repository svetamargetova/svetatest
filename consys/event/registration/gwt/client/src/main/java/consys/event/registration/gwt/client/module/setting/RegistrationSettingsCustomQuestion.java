package consys.event.registration.gwt.client.module.setting;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.OrderDialog;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.panel.ConsysTabPanel;
import consys.common.gwt.client.ui.comp.panel.OrderPanel;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabOrderPanelItem;
import consys.common.gwt.client.ui.comp.panel.element.ConsysTabPanelItemUI;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.registration.gwt.client.action.CreateCustomQuestionAction;
import consys.event.registration.gwt.client.action.ListCustomQuestionsAction;
import consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction;
import consys.event.registration.gwt.client.bo.ClientCreateQuestion;
import consys.event.registration.gwt.client.module.setting.question.CustomQuestionItem;
import consys.event.registration.gwt.client.module.setting.question.NewQuestionUI;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ModuleNavigationUtils;
import java.util.ArrayList;

/**
 * Prehled a vytvareni volitelnych poli do registrace
 * @author pepa
 */
public class RegistrationSettingsCustomQuestion extends RootPanel implements OrderPanel.OrderPanelListener<ConsysTabOrderPanelItem<String>> {

    // konstanty
    private static final String PANEL_NAME = "CUSTOM-FIELDS";
    // komponenty
    private ConsysTabPanel<String> tabs;
    private ConsysMessage helpMessage;
    private OrderDialog orderDialog;

    public RegistrationSettingsCustomQuestion() {
        super(ERMessageUtils.c.registrationSettingsCustomQuestion_title());

        addLeftControll(ActionImage.breadcrumbBackBigBack());

        ActionImage add = ActionImage.getPlusButton(ERMessageUtils.c.registrationSettingsCustomQuestion_action_addField(), true);
        add.addClickHandler(addTabClickHandler());
        addLeftControll(add);

        enableDownMenu(ModuleNavigationUtils.downMenuOptionBundlesSettings(),
                ModuleNavigationUtils.downMenuOptionCouponList(),
                ModuleNavigationUtils.downMenuOptionSystemProperty(),
                ModuleNavigationUtils.downMenuCustomFields());
    }

    @Override
    protected void onLoad() {
        init();
        EventBus.fire(new EventDispatchEvent(new ListCustomQuestionsAction(), new AsyncCallback<ArrayListResult<CommonThumb>>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava event action executor
            }

            @Override
            public void onSuccess(ArrayListResult<CommonThumb> result) {
                setTabs(result.getArrayListResult());
            }
        }, this));
    }

    private void init() {
        clear();

        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ERMessageUtils.c.registrationSettingsCustomQuestion_text_pleaseAddField());
        helpMessage.addStyleName(StyleUtils.MARGIN_TOP_10);
        helpMessage.hideClose(true);

        tabs = new ConsysTabPanel<String>(PANEL_NAME);
        tabs.setStyleName(StyleUtils.MARGIN_TOP_10);
        tabs.setVisible(false);

        tabs.showOrderIcon(new ConsysAction() {
            @Override
            public void run() {
                ArrayList<ConsysTabOrderPanelItem<String>> itemList = new ArrayList<ConsysTabOrderPanelItem<String>>();
                ArrayList<ConsysTabPanelItemUI<String>> ts = tabs.getTabs();
                for (ConsysTabPanelItemUI<String> ui : ts) {
                    itemList.add(new ConsysTabOrderPanelItem<String>(ui, ui.getTabName()));
                }

                orderDialog = new OrderDialog<ConsysTabOrderPanelItem<String>>(itemList, RegistrationSettingsCustomQuestion.this);
                orderDialog.showCentered();
            }
        });

        FlowPanel panel = new FlowPanel();
        panel.add(helpMessage);
        panel.add(tabs);
        addWidget(panel);
    }

    private void setTabs(ArrayList<CommonThumb> list) {
        for (CommonThumb thumb : list) {
            CustomQuestionItem.addToTab(tabs, thumb, helpMessage);
        }
        if (list.size() > 0) {
            helpMessage.setVisible(false);
            tabs.setVisible(true);
        }
    }

    private ClickHandler addTabClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                NewItemDialog<NewQuestionUI> cd = new NewItemDialog<NewQuestionUI>(
                        ERMessageUtils.c.registrationSettingsCustomQuestion_action_addField(), helpMessage) {
                    @Override
                    public NewQuestionUI newItem() {
                        return new NewQuestionUI();
                    }

                    @Override
                    public void doCreateAction(ArrayList<NewQuestionUI> uis, final ConsysMessage fail) {
                        if (uis.size() <= 0) {
                            hide();
                            return;
                        }

                        for (NewQuestionUI ui : uis) {
                            if (ui.getNameBox().getText().isEmpty()) {
                                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ERMessageUtils.c.customQuestionItem_text_question()));
                                return;
                            }
                        }

                        int questionsCounter = tabs.getTabCount() + uis.size();
                        if (questionsCounter > 5) {
                            fail.addOrSetText(ERMessageUtils.c.registrationSettingsCustomQuestion_text_permittedMax5Questions());
                            return;
                        }

                        final CreateCustomQuestionAction action = new CreateCustomQuestionAction();
                        for (NewQuestionUI ui : uis) {
                            action.getQuestions().add(new ClientCreateQuestion(ui.getNameBox().getText(), ui.getRequiredBox().getValue()));
                        }

                        EventBus.fire(new EventDispatchEvent(action, new AsyncCallback<ArrayListResult<String>>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                                if (caught instanceof BadInputException) {
                                    fail.addOrSetText("BadInputException");
                                }
                            }

                            @Override
                            public void onSuccess(ArrayListResult<String> result) {
                                ArrayList<String> uuids = result.getArrayListResult();
                                for (int i = 0; i < uuids.size(); i++) {
                                    final CommonThumb thumb = new CommonThumb();
                                    ClientCreateQuestion question = action.getQuestions().get(i);
                                    thumb.setName(question.getName());
                                    thumb.setUuid(uuids.get(i));
                                    CustomQuestionItem.addToTab(tabs, thumb, helpMessage);
                                    tabs.selectTabById(thumb.getUuid());
                                }

                                tabs.setVisible(true);
                                successCreateAction();
                            }
                        }, this));
                    }
                };
                cd.showCentered();
            }
        };
    }

    @Override
    public void onOrderChange(final ConsysTabOrderPanelItem<String> object, final int oldPosition, final int newPosition) {
        String tabUuid = object.getUi().getId();
        EventBus.get().fireEvent(new EventDispatchEvent(
                new UpdateCustomQuestionOrderAction(tabUuid, oldPosition, newPosition),
                new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                        orderDialog.getOrderPanel().back();
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        int offset = orderDialog.getOrderPanel().getFinalPositionOffset();
                        tabs.changeTabPosition(object.getUi(), oldPosition - offset, newPosition - offset);
                    }
                }, orderDialog));
    }
}
