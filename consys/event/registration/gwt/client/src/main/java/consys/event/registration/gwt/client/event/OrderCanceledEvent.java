package consys.event.registration.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;

/**
 * Event vystreleny, kdyz se chce uzivatel zaregistrovat do eventu
 * @author pepa
 */
public class OrderCanceledEvent extends GwtEvent<OrderCanceledEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data    
    private ClientRegistrationOrder order;

    public OrderCanceledEvent(ClientRegistrationOrder order) {
        this.order = order;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onOrderCanceled(this);
    }

    /**
     * @return the order
     */
    public ClientRegistrationOrder getOrder() {
        return order;
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onOrderCanceled(OrderCanceledEvent event);
    }
}
