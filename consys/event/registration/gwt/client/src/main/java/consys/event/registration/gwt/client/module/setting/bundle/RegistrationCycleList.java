package consys.event.registration.gwt.client.module.setting.bundle;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.shared.bo.Monetary;
import consys.event.registration.gwt.client.action.CreateRegistrationCycleAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import consys.event.registration.gwt.client.module.exception.CyclesIntersectionException;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ValidationUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class RegistrationCycleList extends SimpleFormPanel {

    // komponenty
    private ConsysMessage helpMessage;
    private FlowPanel content;
    // data
    private String parentUuid;

    public RegistrationCycleList(String parentUuid, ArrayList<ClientRegistrationCycle> crcts) {
        super();
        this.parentUuid = parentUuid;
        setStyleName(StyleUtils.MARGIN_TOP_10);

        addWidget(controlPanel());
        addWidget(helpMessage());
        addWidget(content());

        sortAndShowCycles(crcts);
    }

    private FlowPanel controlPanel() {
        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(titleLabel());
        controlPanel.add(addCycleActionLabel());
        controlPanel.add(StyleUtils.clearDiv());
        return controlPanel;
    }

    private Label titleLabel() {
        Label titleLabel = StyleUtils.getStyledLabel(ERMessageUtils.c.registrationCycleList_title(), StyleUtils.FONT_16PX);
        titleLabel.addStyleName(StyleUtils.FONT_BOLD);
        titleLabel.addStyleName(StyleUtils.TEXT_GRAY);
        titleLabel.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        titleLabel.addStyleName(StyleUtils.FLOAT_LEFT);
        return titleLabel;
    }

    private ActionLabel addCycleActionLabel() {
        ActionLabel addCycle = new ActionLabel(ERMessageUtils.c.registrationCycleList_action_addRC(),
                StyleUtils.FLOAT_LEFT + " " + MARGIN_TOP_3);
        addCycle.setClickHandler(addCycleClickHandler());
        return addCycle;
    }

    private ConsysMessage helpMessage() {
        helpMessage = ConsysMessage.getSuccess();
        helpMessage.setText(ERMessageUtils.c.registrationCycleList_text_pleaseAddRC());
        helpMessage.addStyleName(StyleUtils.MARGIN_TOP_10);
        helpMessage.addStyleName(StyleUtils.CLEAR_BOTH);
        helpMessage.hideClose(true);
        return helpMessage;
    }

    private FlowPanel content() {
        content = new FlowPanel() {

            @Override
            public boolean remove(Widget w) {
                boolean result = super.remove(w);
                if (result && getWidgetCount() == 0) {
                    helpMessage.setVisible(true);
                }
                return result;
            }
        };
        content.setStyleName(StyleUtils.CLEAR_BOTH);
        return content;
    }

    private void sortAndShowCycles(ArrayList<ClientRegistrationCycle> cycles) {
        if (cycles != null) {
            Collections.sort(cycles, new Comparator<ClientRegistrationCycle>() {

                @Override
                public int compare(ClientRegistrationCycle o1, ClientRegistrationCycle o2) {
                    return o1.getFrom().compareTo(o2.getFrom());
                }
            });
            for (ClientRegistrationCycle t : cycles) {
                content.add(new RegistrationCycleItem(t, this));
            }
            if (!cycles.isEmpty()) {
                helpMessage.setVisible(false);
            }
        }
    }

    /** vytvori ClickHandler pro pridani registracniho cyklu */
    private ClickHandler addCycleClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                final NewItemDialog<NewRegistrationCycleUI> cd = new NewItemDialog<NewRegistrationCycleUI>(
                        ERMessageUtils.c.registrationCycleList_text_newRC(), helpMessage) {

                    @Override
                    public void doCreateAction(ArrayList<NewRegistrationCycleUI> uis, final ConsysMessage fail) {
                        if (uis.size() <= 0) {
                            hide();
                            return;
                        }

                        if (!validateDateRange(uis, fail)) {
                            return;
                        }

                        final ArrayList<ClientRegistrationCycle> cycles = new ArrayList<ClientRegistrationCycle>();
                        for (NewRegistrationCycleUI ui : uis) {
                            Date newFrom = ui.getFromBox().getValue();
                            Date newTo = ui.getToBox().getValue();

                            if (ValidationUtils.isRegistrationCycleOverlapping(newFrom, newTo, getCycles())) {
                                fail.setText(ERMessageUtils.c.const_registrationCycleNotOverlap());
                                return;
                            }

                            ClientRegistrationCycle crc = new ClientRegistrationCycle();
                            crc.setFrom(newFrom);
                            crc.setTo(newTo);
                            crc.setValue(ui.getValue());
                            cycles.add(crc);
                        }

                        EventDispatchEvent createEvent = new EventDispatchEvent(
                                new CreateRegistrationCycleAction(parentUuid, cycles),
                                new AsyncCallback<ArrayListResult<String>>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava EventActionExcecutor
                                        if (caught instanceof CyclesIntersectionException) {
                                            fail.setText(ERMessageUtils.c.const_registrationCycleNotOverlap());
                                        }
                                    }

                                    @Override
                                    public void onSuccess(ArrayListResult<String> result) {
                                        ArrayList<String> uuids = result.getArrayListResult();
                                        for (int i = 0; i < uuids.size(); i++) {
                                            ClientRegistrationCycle cycle = cycles.get(i);
                                            cycle.setUuid(uuids.get(i));
                                            content.add(new RegistrationCycleItem(cycle, RegistrationCycleList.this));
                                        }
                                        sort();
                                        successCreateAction();
                                    }
                                }, this);
                        EventBus.get().fireEvent(createEvent);
                    }

                    @Override
                    public NewRegistrationCycleUI newItem() {
                        if (content.getWidgetCount() == 0) {
                            return new NewRegistrationCycleUI();
                        } else {
                            Widget w = content.getWidget(content.getWidgetCount() - 1);
                            if (w instanceof RegistrationCycleItem) {
                                ClientRegistrationCycle thumb = ((RegistrationCycleItem) w).getCycle();
                                return new NewRegistrationCycleUI(thumb.getTo(), new Monetary());
                            } else {
                                return new NewRegistrationCycleUI();
                            }
                        }
                    }

                    private boolean validateDateRange(ArrayList<NewRegistrationCycleUI> uis, ConsysMessage fail) {
                        // validace samotneho datumu - datum musi byt vlozen a OD nesmi predchazed DO ani se nesmi rovnat
                        for (NewRegistrationCycleUI ui : uis) {
                            // validacia datumu
                            if (!ValidationUtils.areRegistrationCycleDatesValid(ui.getFromBox(), ui.getToBox(), fail)) {
                                return false;
                            }
                            // validaxia zbytku formulara
                            if (!ui.form().validate(fail)) {
                                return false;
                            }
                        }
                        return true;
                    }
                };
                cd.showCentered();
            }
        };
    }

    /** vraci thumby cyklu */
    public ArrayList<ClientRegistrationCycle> getCycles() {
        ArrayList<ClientRegistrationCycle> cycles = new ArrayList<ClientRegistrationCycle>();
        for (int i = 0; i < content.getWidgetCount(); i++) {
            Widget w = content.getWidget(i);
            if (w instanceof RegistrationCycleItem) {
                RegistrationCycleItem rci = (RegistrationCycleItem) w;
                cycles.add(rci.getCycle());
            }
        }
        return cycles;
    }

    /** vraci thumby cyklu, bez cyklu zadaneho itemu */
    ArrayList<ClientRegistrationCycle> getCycles(RegistrationCycleItem item) {
        ArrayList<ClientRegistrationCycle> cycles = new ArrayList<ClientRegistrationCycle>();
        for (int i = 0; i < content.getWidgetCount(); i++) {
            Widget w = content.getWidget(i);
            if (w instanceof RegistrationCycleItem) {
                RegistrationCycleItem rci = (RegistrationCycleItem) w;
                if (!item.equals(rci)) {
                    cycles.add(rci.getCycle());
                }
            }
        }
        return cycles;
    }

    /** nastavi seznam registracnich cyklu */
    public void setCycles(ArrayList<ClientRegistrationCycle> crcts) {
        content.clear();
        // seradime
        Collections.sort(crcts, new Comparator<ClientRegistrationCycle>() {

            @Override
            public int compare(ClientRegistrationCycle o1, ClientRegistrationCycle o2) {
                return o1.getFrom().compareTo(o2.getFrom());
            }
        });

        if (crcts != null && !crcts.isEmpty()) {
            for (ClientRegistrationCycle crct : crcts) {
                content.add(new RegistrationCycleItem(crct, this));
            }
            helpMessage.setVisible(false);
        } else {
            helpMessage.setVisible(true);
        }
    }

    /** seradi polozky podle pocatecniho data */
    public void sort() {
        ArrayList<RegistrationCycleItem> items = new ArrayList<RegistrationCycleItem>();
        for (int i = 0; i < content.getWidgetCount(); i++) {
            Widget w = content.getWidget(i);
            if (w instanceof RegistrationCycleItem) {
                items.add((RegistrationCycleItem) w);
            }
        }

        content.clear();
        Collections.sort(items, new Comparator<RegistrationCycleItem>() {

            @Override
            public int compare(RegistrationCycleItem o1, RegistrationCycleItem o2) {
                return o1.getCycle().getFrom().compareTo(o2.getCycle().getFrom());
            }
        });
        for (RegistrationCycleItem item : items) {
            content.add(item);
        }
    }
}
