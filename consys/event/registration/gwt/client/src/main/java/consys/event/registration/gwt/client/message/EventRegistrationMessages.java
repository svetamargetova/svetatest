package consys.event.registration.gwt.client.message;

import com.google.gwt.i18n.client.Messages;

/**
 * abecedne serazene zpravy
 * @author pepa
 */
public interface EventRegistrationMessages extends Messages {

    String discountListItem_text_showAll(int count);

    String outerRegistrationForm_text_alreadyTakeplaceMember(String href);

    String outerRegisterPanel_text1(String eventName);

    String registrationCycleList_cycle_exception_endBeforeStart(String start, String end);

    String registrationCycleList_cycle_exception_endSameAsStart(String start, String end);

    String registrationDate(String date);

    String registrationForm_text_before(String date, String price);

    String registrationFormItem_exception_noRecordsForAction(String discountCode);

    String registrationFormItem_text_lastFreeCapacity(@PluralCount int freeCapacity);

    String registrationFormItem_text_limitedCapacity(int fullCapacity);

    String registrationFormItem_text_registrationExpiresDays(@PluralCount int days);

    String registrationFormItem_text_registrationExpiresHours(@PluralCount int hours);

    String registrationFormItem_text_registrationExpiresMinutes(@PluralCount int minutes);

    String registrationFormRegistered_text_successfulyRegisteredInfo(String redStyle, String untilDate);

    String registrationOrderPanel_text_discountCode(String discountCode);

    String registrationOrderPanel_text_registrationDate(String registrationDate);

    String registrationOrderPanel_text_statusApprove(String style);

    String registrationOrderPanel_text_statusPaid(String style);
}
