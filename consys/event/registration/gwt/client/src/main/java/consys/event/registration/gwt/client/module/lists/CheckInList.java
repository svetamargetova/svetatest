package consys.event.registration.gwt.client.module.lists;

import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.DataListCommonPanel;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import consys.event.registration.gwt.client.bo.ClientCheckInListItem;
import consys.event.registration.gwt.client.module.lists.item.CheckInItem;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Check in seznam lidi
 * @author pepa
 */
public class CheckInList extends DataListPanel implements consys.event.registration.api.list.CheckInList {

    public CheckInList() {
        super(ERMessageUtils.c.checkInList_title(), LIST_TAG);

        setListDelegate(new CheckInDelegate());

        enableExport(ListExportTypeEnum.EXCEL);

        setDefaultFilter(new ListFilter(Filter_ALL) {
            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
        DataListCommonPanel cp = new DataListCommonPanel();
        cp.addOrderer(ERMessageUtils.c.checkInList_action_name(), Order_LAST_NAME, true);
        cp.addFilter(ERMessageUtils.c.checkInList_action_filterAll(), Filter_ALL, true);
        cp.addFilter(ERMessageUtils.c.checkInList_action_filterChecked(), Filter_CHECKED);
        cp.addFilter(ERMessageUtils.c.checkInList_action_filterNotChecked(), Filter_NOT_CHECKED);
        addHeadPanel(cp);
    }

    @Override
    protected boolean asyncPreprocesing() {
        return false;
    }

    @Override
    protected void doAsyncPreprocesing(ListDataSourceResult result, final ConsysAction action) {
    }

    /** delegat seznamu */
    private class CheckInDelegate implements ListDelegate<ClientCheckInListItem, CheckInItem> {

        @Override
        public CheckInItem createCell(ClientCheckInListItem item) {
            return new CheckInItem(item);
        }
    }
}
