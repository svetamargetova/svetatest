package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.gwt.shared.bo.ProductThumb;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Akce pro vytvoreni vnitrni registrace
 * @author pepa
 */
public class CreateInternalRegistrationAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -4541525856998120650L;
    // data
    private String firstName;
    private String lastName;
    private String organization;
    private String userEmail;
    private String cutomerUuid;
    private String customerProfileUuid;
    private HashMap<String, String> answers;
    private ArrayList<ProductThumb> products;
    private ClientUserPaymentProfile profile;

    public CreateInternalRegistrationAction() {
        answers = new HashMap<String, String>();
        products = new ArrayList<ProductThumb>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getCutomerUuid() {
        return cutomerUuid;
    }

    public void setCutomerUuid(String cutomerUuid) {
        this.cutomerUuid = cutomerUuid;
    }

    public String getCustomerProfileUuid() {
        return customerProfileUuid;
    }

    public void setCustomerProfileUuid(String customerProfileUuid) {
        this.customerProfileUuid = customerProfileUuid;
    }

    public HashMap<String, String> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<String, String> answers) {
        this.answers = answers;
    }

    public ArrayList<ProductThumb> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<ProductThumb> products) {
        this.products = products;
    }

    public ClientUserPaymentProfile getProfile() {
        return profile;
    }

    public void setProfile(ClientUserPaymentProfile profile) {
        this.profile = profile;
    }
}
