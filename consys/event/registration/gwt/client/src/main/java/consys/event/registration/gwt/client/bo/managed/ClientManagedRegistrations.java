package consys.event.registration.gwt.client.bo.managed;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.EventUser;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientManagedRegistrations implements Result {

    private static final long serialVersionUID = -8181837106245206425L;
    // data
    private ArrayList<EventUser> participants;
    private ArrayList<ClientRegistration> registrations;

    public ClientManagedRegistrations() {
        participants = new ArrayList<EventUser>();
        registrations = new ArrayList<ClientRegistration>();
    }

    public ArrayList<EventUser> getParticipants() {
        return participants;
    }

    public ArrayList<ClientRegistration> getRegistrations() {
        return registrations;
    }
}
