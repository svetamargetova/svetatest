package consys.event.registration.gwt.client.module.register.outer.comp;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.AddressForm;
import consys.common.gwt.client.ui.comp.form.FormV2;
import consys.common.gwt.client.ui.comp.input.ValidableItem;
import consys.common.gwt.client.ui.comp.input.ConsysSelectBox;
import consys.common.gwt.client.ui.comp.input.ConsysTextBox;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.event.registration.gwt.client.module.register.outer.OuterLayout;
import consys.event.registration.gwt.client.module.register.outer.OuterResources;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class OuterInvoiceInfo extends Composite {

    // komponenty
    private FlowPanel mainPanel;
    private FormV2 form;
    private ConsysTextBox invoiceTo;
    private ConsysTextBox street;
    private ConsysTextBox city;
    private ConsysTextBox zip;
    private ConsysTextBox vatNo;
    private ConsysTextBox regNo;
    private ConsysSelectBox<Integer> locationBox;
    // data
    private boolean needLocation;

    public OuterInvoiceInfo() {
        needLocation = true;
        
        OuterResources.INSTANCE.css().ensureInjected();

        mainPanel = new FlowPanel();
        initWidget(mainPanel);

        generate();
    }

    private void generate() {
        Widget title = FormV2.getTitleWidget(ERMessageUtils.c.outerRegistrationForm_text_invoiceAddress(), ERMessageUtils.c.outerRegistrationForm_text_invoiceAddressHelp(), true, "");
        title.addStyleName(OuterResources.INSTANCE.css().formTitleBig());

        invoiceTo = new ConsysTextBox(1, 255);
        street = new ConsysTextBox(1, 255);
        city = new ConsysTextBox(1, 255);
        zip = new ConsysTextBox(1, 255);
        regNo = new ConsysTextBox(0, 128);
        vatNo = new ConsysTextBox(0, 128);
        
        locationBox = new ConsysSelectBox<Integer>();
        listLocations();

        form = new FormV2();
        form.add(title, StyleUtils.clearDiv("20px"), true);
        form.add(ERMessageUtils.c.outerRegistrationForm_text_invoiceTo(), new ValidableItem(invoiceTo), true);
        form.add(UIMessageUtils.c.paymentProfilePanel_form_registrationNumber(), regNo, false);
        form.add(UIMessageUtils.c.paymentProfilePanel_form_vatNumber(), vatNo, false);
        form.add(UIMessageUtils.c.const_address_street(), new ValidableItem(street), true);
        form.add(UIMessageUtils.c.const_address_city(), new ValidableItem(city), true);
        form.add(UIMessageUtils.c.const_address_zip(), new ValidableItem(zip), true);
        form.add(UIMessageUtils.c.const_address_location(), new ValidableItem(locationBox), true);

        mainPanel.add(FormUtils.createPanel(OuterResources.INSTANCE.css().middlePartSeparator()));
        mainPanel.add(form);
    }

    private void listLocations() {
        EventBus.fire(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {

            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                ArrayList<SelectBoxItem<Integer>> boxItems = new ArrayList<SelectBoxItem<Integer>>(result.getList().size());
                for (BoxItem item : result.getList()) {
                    boxItems.add(new SelectBoxItem<Integer>(item.getId(), item.getName()));
                }
                locationBox.setItems(boxItems);
            }
        }, OuterLayout.get()));
    }

    public ClientUserPaymentProfile getPaymentProfile() {
        ClientUserPaymentProfile profile = new ClientUserPaymentProfile();
        profile.setName(invoiceTo.getText());
        profile.setStreet(street.getText());
        profile.setCity(city.getText());
        profile.setZip(zip.getText());
        profile.setRegNo(regNo.getText());
        profile.setVatNo(vatNo.getText());
        profile.setCountry(locationBox.getSelectedItem() != null ? locationBox.getSelectedItem().getItem() : 0);
        return profile;
    }

    /** zvaliduje komponentku */
    public boolean validate() {
        return form.validate();
    }
    
    @Override
    public void setVisible(boolean visible) {
        if(visible && needLocation) {
            needLocation = false;
            AddressForm.initCountryBox(locationBox);
        }
        super.setVisible(visible);
    }
}
