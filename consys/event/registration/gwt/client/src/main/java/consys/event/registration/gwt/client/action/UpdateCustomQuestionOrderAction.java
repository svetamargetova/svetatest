package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 *
 * @author pepa
 */
public class UpdateCustomQuestionOrderAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -6032247606618239622L;
    // data
    private String questionUuid;
    private int oldPosition;
    private int newPosition;

    public UpdateCustomQuestionOrderAction() {
    }

    public UpdateCustomQuestionOrderAction(String questionUuid, int oldPosition, int newPosition) {
        this.questionUuid = questionUuid;
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
    }

    public String getQuestionUuid() {
        return questionUuid;
    }

    public int getOldPosition() {
        return oldPosition;
    }

    public int getNewPosition() {
        return newPosition;
    }
}
