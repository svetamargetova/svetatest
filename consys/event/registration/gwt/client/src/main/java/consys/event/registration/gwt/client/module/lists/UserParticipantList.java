package consys.event.registration.gwt.client.module.lists;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.ui.comp.list.*;
import consys.common.gwt.client.ui.comp.user.NameWithAffiliation;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.constants.img.UserProfileImageEnum;
import consys.event.registration.gwt.client.bo.ClientListParticipantItem;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class UserParticipantList extends DataListPanel implements consys.event.registration.api.list.ParticipantList {

    public UserParticipantList() {
        super(ERMessageUtils.c.userParticipantList_title(), LIST_TAG);

        setListDelegate(new ParticipantListDelegate());
        setDefaultFilter(new ListFilter(Filter_ALL) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });
        DataListCommonOrderer orderer = new DataListCommonOrderer();
        orderer.addOrderer(ERMessageUtils.c.userParticipantList_action_name(), Order_LAST_NAME, true);
        addHeadPanel(orderer);
    }

    @Override
    protected boolean asyncPreprocesing() {
        return false;
    }

    @Override
    protected void doAsyncPreprocesing(ListDataSourceResult result, final ConsysAction action) {
    }

    /** delegat seznamu */
    private class ParticipantListDelegate implements ListDelegate<ClientListParticipantItem, ParticipantItem> {

        @Override
        public ParticipantItem createCell(ClientListParticipantItem item) {
            return new ParticipantItem(item);
        }
    }

    private class ParticipantItem extends ListItem<ClientListParticipantItem> {

        public ParticipantItem(ClientListParticipantItem value) {
            super(value);
        }

        @Override
        protected void createCell(ClientListParticipantItem eventUser, FlowPanel panel) {
            panel.clear();

            Image portrait = FormUtils.userPortrait(eventUser.getUuidPortraitImagePrefix(), UserProfileImageEnum.LIST);
            FlowPanel portraitWrapper = FormUtils.portraitWrapper(UserProfileImageEnum.LIST);
            portraitWrapper.add(portrait);
            panel.add(portraitWrapper);

            FlowPanel textPanel = new FlowPanel();
            textPanel.setStyleName(StyleUtils.FLOAT_LEFT);
            textPanel.add(new NameWithAffiliation(eventUser.getUserName(), eventUser.getUserAffiliation()));
            panel.add(textPanel);
        }
    }
}
