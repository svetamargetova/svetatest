package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Zasle registracni slevovy kod
 * @author pepa
 */
public class SendDiscountCodeAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -7701003820940623964L;
    // data
    private String code;

    public SendDiscountCodeAction() {
    }

    public SendDiscountCodeAction(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
