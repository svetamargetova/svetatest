package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;

/**
 * Akce pro nacteni editace registrace
 * @author pepa
 */
public class LoadEditRegistrationAction extends EventAction<ClientRegistration> {

    private static final long serialVersionUID = -5935737574786270554L;
    // data
    private String registrationUuid;

    public LoadEditRegistrationAction() {
    }

    public LoadEditRegistrationAction(String registrationUuid) {
        this.registrationUuid = registrationUuid;
    }

    public String getRegistrationUuid() {
        return registrationUuid;
    }
}
