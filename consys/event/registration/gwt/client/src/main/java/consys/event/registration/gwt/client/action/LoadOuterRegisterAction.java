package consys.event.registration.gwt.client.action;


import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientOuterRegisterData;

/**
 * Akce pro nacteni dat registrace do eventu pro nezaregistrovaneho uzivatele
 * @author pepa
 */
public class LoadOuterRegisterAction extends EventAction<ClientOuterRegisterData> {

    private static final long serialVersionUID = 762591190197426080L;
    // data            
    private String ticketUuid;

    public LoadOuterRegisterAction() {
    }

    public LoadOuterRegisterAction(String packageUuid) {                
        this.ticketUuid = packageUuid;
    }    

    public String getTicketUuid() {
        return ticketUuid;
    }
}
