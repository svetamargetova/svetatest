package consys.event.registration.gwt.client.module.lists.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.comp.user.NameWithAffiliation;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.registration.gwt.client.bo.ClientCheckInListItem;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 * Jedna polozka v checkin seznamu
 * @author pepa
 */
public class CheckInItem extends ListItem<ClientCheckInListItem> implements CssStyles {

    // komponenty
    private FlowPanel mainPanel;
    // data
    private ClientCheckInListItem data;

    public CheckInItem(ClientCheckInListItem value) {
        super(value);
    }

    @Override
    protected void createCell(ClientCheckInListItem user, FlowPanel panel) {
        data = user;
        mainPanel = panel;

        redrawItem();
    }

    public void redrawItem() {
        mainPanel.clear();
        drawUser();
        drawActions();
    }

    private void drawUser() {
        // obrazek
        Image portrait = FormUtils.userPortrait(data.getUuidPortraitImagePrefix(), UserProfileImageEnum.LIST);
        FlowPanel portraitWrapper = FormUtils.portraitWrapper(UserProfileImageEnum.LIST);
        portraitWrapper.add(portrait);

        // jmeno (kvantita), afilace
        String check = "";
        if (data.getMaxQuantity() > 1) {
            StringBuilder sb = new StringBuilder(" (");
            sb.append(ERMessageUtils.c.editFormBundle_form_quantity());
            sb.append(" ");
            sb.append(data.getQuantityChecked());
            sb.append("/");
            sb.append(data.getMaxQuantity());
            sb.append(")");
            check = sb.toString();
        }

        FlowPanel textPanel = new FlowPanel();
        textPanel.setWidth("400px");
        textPanel.setStyleName(FLOAT_LEFT);
        textPanel.add(new NameWithAffiliation(data.getUserName() + check, data.getUserAffiliation()));

        mainPanel.add(portraitWrapper);
        mainPanel.add(textPanel);
    }

    private void drawActions() {
        ActionImage checkIn;
        if (data.getQuantityChecked() < data.getMaxQuantity()) {
            checkIn = ActionImage.getConfirmButton(ERMessageUtils.c.checkInList_action_buttonCheckIn());
            checkIn.addClickHandler(checkInClickHandler());
        } else {
            checkIn = ActionImage.getCheckButton(ERMessageUtils.c.checkInList_action_buttonChecked());
            checkIn.setEnabled(false);
        }
        checkIn.addStyleName(MARGIN_VER_10);
        checkIn.addStyleName(FLOAT_RIGHT);

        SimplePanel buttonPanel = new SimplePanel();
        buttonPanel.setWidget(checkIn);

        ActionLabel cancelCheckIn = new ActionLabel(ERMessageUtils.c.checkInList_action_buttonUncheck(), FLOAT_RIGHT);
        cancelCheckIn.setVisible(data.getQuantityChecked() > 0);
        cancelCheckIn.setClickHandler(cancelClickHander());

        FlowPanel actionPanel = new FlowPanel();
        actionPanel.setWidth("200px");
        actionPanel.addStyleName(FLOAT_LEFT);
        actionPanel.add(buttonPanel);
        actionPanel.add(StyleUtils.clearDiv());
        actionPanel.add(cancelCheckIn);
        actionPanel.add(StyleUtils.clearDiv());

        mainPanel.add(actionPanel);
        mainPanel.add(StyleUtils.clearDiv());
    }

    public ClientCheckInListItem getData() {
        return data;
    }

    /** button clickHandler */
    private ClickHandler checkInClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CheckDialog dialog = new CheckDialog(data.getUuidRegistration(), true, CheckInItem.this);
                if (data.getMaxQuantity() == 1) {
                    dialog.checkInAutomatic();
                } else {
                    dialog.showCentered();
                }
            }
        };
    }

    /** cancel clickHandler */
    private ClickHandler cancelClickHander() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CheckDialog dialog = new CheckDialog(data.getUuidRegistration(), false, CheckInItem.this);
                if (data.getMaxQuantity() == 1) {
                    dialog.checkOutAutomatic();
                } else {
                    dialog.showCentered();
                }
            }
        };
    }
}
