package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.CommonThumb;

/**
 * Nacte serazeny seznam volitelnych otazek
 * @author pepa
 */
public class ListCustomQuestionsAction extends EventAction<ArrayListResult<CommonThumb>> {

    private static final long serialVersionUID = 1947863906708862938L;
}
