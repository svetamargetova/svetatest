package consys.event.registration.gwt.client.module.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class CyclesIntersectionException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public CyclesIntersectionException() {
        super("");
    }

    public CyclesIntersectionException(String message) {
        super(message);
    }

    public CyclesIntersectionException(Throwable cause) {
        super(cause);
    }

    public CyclesIntersectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
