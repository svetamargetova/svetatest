package consys.event.registration.gwt.client.module.register.outer;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.*;
import consys.event.common.gwt.client.rpc.EventActionExecutor;
import java.util.ArrayList;

/**
 * Template pro praci z venku
 * @author pepa
 */
public class OuterLayout extends Composite implements DispatchEvent.Handler, EventDispatchEvent.Handler, ActionExecutionDelegate {

    // konstanty
    private static Logger logger = LoggerFactory.getLogger(OuterLayout.class);
    // instance
    private static OuterLayout layout;
    // komponenty
    private FlowPanel mainPanel;
    private SimplePanel contentPanel;
    private SimplePanel failPanel;
    private Waiting waiting;
    // data
    private HandlerRegistration dispatchEventHR;
    private HandlerRegistration eventDispatchEventHR;
    private boolean withFooter;

    private OuterLayout() {
        this(true);
    }

    private OuterLayout(boolean withFooter) {
        this.withFooter = withFooter;

        OuterResources.INSTANCE.css().ensureInjected();

        failPanel = new SimplePanel();
        failPanel.setStyleName(OuterResources.INSTANCE.css().centerPanel());

        mainPanel = new FlowPanel();
        mainPanel.getElement().setId(withFooter ? "wrapper" : "wrapperNoFooter");

        waiting = new Waiting(mainPanel);

        initWidget(mainPanel);
        generate();

        // potreba zeregistrovat uz v konstruktoru,
        // protoze akce z onLoad potomka, se vystreli driv nez se zaregistruje handler v onLoad layoutu
        dispatchEventHR = EventBus.get().addHandler(DispatchEvent.TYPE, OuterLayout.this);
        eventDispatchEventHR = EventBus.get().addHandler(EventDispatchEvent.TYPE, OuterLayout.this);
    }

    public static OuterLayout get() {
        if (layout == null) {
            layout = new OuterLayout();
        }
        return layout;
    }

    public static OuterLayout get(boolean withFooter) {
        if (layout == null) {
            layout = new OuterLayout(withFooter);
        }
        return layout;
    }

    @Override
    protected void onLoad() {
        if (dispatchEventHR == null) {
            dispatchEventHR = EventBus.get().addHandler(DispatchEvent.TYPE, this);
        }
        if (eventDispatchEventHR == null) {
            eventDispatchEventHR = EventBus.get().addHandler(EventDispatchEvent.TYPE, this);
        }

        EventActionExecutor.init();
    }

    @Override
    protected void onUnload() {
        if (dispatchEventHR != null) {
            dispatchEventHR.removeHandler();
            dispatchEventHR = null;
        }
        if (eventDispatchEventHR != null) {
            eventDispatchEventHR.removeHandler();
            eventDispatchEventHR = null;
        }
    }

    private void generate() {
        if (withFooter) {
            top(mainPanel);
        }
        mainPanel.add(failPanel);
        center(mainPanel);
        mainPanel.add(waiting);
    }

    private void top(FlowPanel panel) {
        SimplePanel top = FormUtils.createPanel(OuterResources.INSTANCE.css().top());
        top.setWidget(topPanel());

        SimplePanel separator = FormUtils.createPanel(OuterResources.INSTANCE.css().topSeparator());

        panel.add(top);
        panel.add(separator);
    }

    private FlowPanel topPanel() {
        SimplePanel logo = FormUtils.createPanel(OuterResources.INSTANCE.css().topTakeplace());

        Label localeLabel = new Label();
        SelectBox<String> localeBox = prepareLocaleSelect(localeLabel);

        FlowPanel language = new FlowPanel();
        language.setStyleName(OuterResources.INSTANCE.css().topLanguage());
        language.add(localeBox);
        language.add(localeLabel);
        language.add(StyleUtils.clearDiv());

        FlowPanel panel = new FlowPanel();
        panel.setStyleName(OuterResources.INSTANCE.css().topCenter());
        panel.add(language);
        panel.add(logo);
        panel.add(StyleUtils.clearDiv());
        return panel;
    }

    private SelectBox<String> prepareLocaleSelect(Label localeLabel) {
        String cookie = LocaleUtils.getLocale();
        if (ParamUtils.isSet("locale")) {
            // je nastaven parametr lokalizace v url, tak ho pouzijeme, protoze ma prednost
            cookie = ParamUtils.getValue("locale");
        }

        if (cookie.equals(LocaleUtils.LOCALE_ENGLISH)) {
            localeLabel.setText("English");
        } else if (cookie.equals(LocaleUtils.LOCALE_CZECH)) {
            localeLabel.setText("Čeština");
        } else {
            // nepodporovana lokalizace, nastavime vychozi
            localeLabel.setText("English");
            cookie = LocaleUtils.LOCALE_ENGLISH;
        }

        Image flagGreatBritain = new Image(ResourceUtils.system().flagUSGreatBritain());
        Image flagCzech = new Image(ResourceUtils.system().flagCzech());

        ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
        items.add(new SelectBoxItem<String>(LocaleUtils.LOCALE_ENGLISH, "", flagGreatBritain.toString()));
        items.add(new SelectBoxItem<String>(LocaleUtils.LOCALE_CZECH, "", flagCzech.toString()));

        SelectBox<String> localeSelect = new SelectBox<String>();
        localeSelect.setWidth(50);
        localeSelect.addChangeValueHandler(LocaleUtils.changeLocaleHandler());
        localeSelect.setItems(items);
        localeSelect.selectItem(cookie);

        // ulozeni cookie -> prodlouzeni o dalsi casovou periodu
        LocaleUtils.setLocale(cookie);

        return localeSelect;
    }

    private void center(FlowPanel panel) {
        contentPanel = FormUtils.createPanel(withFooter
                ? OuterResources.INSTANCE.css().contentPanel()
                : OuterResources.INSTANCE.css().contentPanelNoFooter());
        panel.add(contentPanel);
    }

    public void setContent(Widget w) {
        contentPanel.setWidget(w);
    }

    @Override
    public void onDispatch(DispatchEvent event) {
        logger.debug("DispatchEvent: " + event.getAction().getClass().getName());
        ActionExecutor.execute(event.getAction(), event.getCallback(), event.getActionDelegate());
    }

    @Override
    public void onDispatch(EventDispatchEvent event) {
        logger.debug("EventDispatchEvent: " + event.getAction().getClass().getName());
        String name = event.getAction().getClass().getName();
        if (name.lastIndexOf('.') > 0) {
            name = name.substring(name.lastIndexOf('.') + 1);
        }
        EventActionExecutor.execute(event.getAction(), event.getCallback(), event.getActionDelegate());
    }

    @Override
    public void actionStarted() {
        waiting.show();
    }

    @Override
    public void actionEnds() {
        waiting.hide();
    }

    @Override
    public void setFailMessage(CommonExceptionEnum value) {
        getFailMessage().setText(UIMessageUtils.getFailMessage(value));
    }

    public ConsysMessage getFailMessage() {
        return FormUtils.failMessage(failPanel, StyleUtils.MARGIN_TOP_10);
    }
}
