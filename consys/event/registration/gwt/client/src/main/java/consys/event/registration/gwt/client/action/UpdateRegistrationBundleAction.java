package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientBundle;

/**
 * Aktualizuje registracni bundle
 * @author pepa
 */
public class UpdateRegistrationBundleAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -5965357939581413970L;
    // data
    private ClientBundle bundle;

    public UpdateRegistrationBundleAction() {
    }

    public UpdateRegistrationBundleAction(ClientBundle bundle) {
        this.bundle = bundle;
    }

    public ClientBundle getBundle() {
        return bundle;
    }
}
