package consys.event.registration.gwt.client.utils.css;

import consys.common.gwt.client.ui.css.CommonCss;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource.Import;
import consys.common.gwt.client.ui.comp.action.ActionCss;
import consys.common.gwt.client.ui.comp.input.InputCss;
import consys.event.common.gwt.client.utils.css.ECoCssResources;

/**
 *
 * @author pepa
 */
public interface ERBundle extends ClientBundle {

    @Import(value = {CommonCss.class, InputCss.class, ECoCssResources.class, ActionCss.class})
    @Source("er.css")
    public ERCssResources css();
}
