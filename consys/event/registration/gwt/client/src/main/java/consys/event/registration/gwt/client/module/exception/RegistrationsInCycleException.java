package consys.event.registration.gwt.client.module.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author palo
 */
public class RegistrationsInCycleException extends ActionException {

    private static final long serialVersionUID = 7172943314968166915L;

    public RegistrationsInCycleException() {
        super("");
    }

    public RegistrationsInCycleException(String message) {
        super(message);
    }

    public RegistrationsInCycleException(Throwable cause) {
        super(cause);
    }

    public RegistrationsInCycleException(String message, Throwable cause) {
        super(message, cause);
    }
}
