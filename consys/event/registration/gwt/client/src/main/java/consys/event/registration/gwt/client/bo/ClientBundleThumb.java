package consys.event.registration.gwt.client.bo;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Klientsky nahled bundle
 * @author pepa
 */
public class ClientBundleThumb implements IsSerializable {

    private String uuid;
    private String title;
    private boolean subbundle;

    public boolean isSubbundle() {
        return subbundle;
    }

    public void setSubbundle(boolean subbundle) {
        this.subbundle = subbundle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
