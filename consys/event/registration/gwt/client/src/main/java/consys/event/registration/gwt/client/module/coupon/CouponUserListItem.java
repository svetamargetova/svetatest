package consys.event.registration.gwt.client.module.coupon;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.panel.ListPanelItem;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.ui.comp.user.UserResources;

/**
 *
 * @author pepa
 */
public class CouponUserListItem extends ListPanelItem<EventUser> {

    // data
    private EventUser eventUser;
    private ConsysActionWithValue<String> action;

    public CouponUserListItem(EventUser eventUser, ConsysActionWithValue<String> action) {
        this.eventUser = eventUser;
        this.action = action;
    }

    @Override
    public void create(FlowPanel panel) {
        UserResources.INSTANCE.userCss().ensureInjected();

        Image portrait = FormUtils.userPortrait(eventUser.getImageUuidPrefix(), UserProfileImageEnum.LIST);
        // TODO: refactorovat tak aby 
        portrait.setHeight("36px");
        portrait.setWidth("36px");

        SimplePanel imgWrapper = new SimplePanel();
        imgWrapper.setStyleName(UserResources.INSTANCE.userCss().userListItemPortrait());
        imgWrapper.setWidget(portrait);

        ActionLabel name = new ActionLabel(eventUser.getFullName());
        name.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                action.run(eventUser.getUuid());
            }
        });

        Label affilation = new Label(eventUser.getAffiliation(UIMessageUtils.c.const_at()));

        FlowPanel infoPanel = new FlowPanel();
        infoPanel.add(name);
        infoPanel.add(affilation);
        infoPanel.addStyleName(UserResources.INSTANCE.userCss().userListItemName());

        panel.add(imgWrapper);
        panel.add(infoPanel);
        panel.add(StyleUtils.clearDiv());
    }

    @Override
    public EventUser getObject() {
        return eventUser;
    }
}
