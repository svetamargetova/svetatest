package consys.event.registration.gwt.client.module.setting.bundle;

import com.google.gwt.i18n.client.TimeZone;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysDoubleTextBox;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.Monetary;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.Date;

/**
 * Trida nove polozky pridavane ve vytvarecim dialogu
 * @author pepa
 */
public class NewRegistrationCycleUI extends NewItemUI {

    // komponenty
    private ConsysDateBox fromBox;
    private ConsysDateBox toBox;
    private ConsysDoubleTextBox valueBox;
    // data
    private Date presetFrom;
    private Monetary presetPrice;

    public NewRegistrationCycleUI() {
        super();
    }

    public NewRegistrationCycleUI(Date from, Monetary price) {
        TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
        this.presetFrom = DateTimeUtils.computeClientEventDate(from, tz);
        this.presetPrice = price;
    }

    @Override
    public void createForm(BaseForm form) {
        fromBox = ConsysDateBox.getEventHalfBox(true);
        toBox = ConsysDateBox.getEventHalfBox(true);

        fromBox.addValueChangeHandler(DateTimeUtils.dateValueChangeHandler(fromBox, toBox));

        valueBox = new ConsysDoubleTextBox(ConsysDateBox.HALF_WIDTH, 0, 999999.99, ERMessageUtils.c.const_price());
        valueBox.addStyleName(StyleUtils.FLOAT_LEFT);

        ConsysFlowPanel valuePanel = new ConsysFlowPanel();
        valuePanel.add(valueBox);
        valuePanel.add(StyleUtils.getStyledLabel(ECoMessageUtils.getCurrency(), StyleUtils.MARGIN_LEFT_10, StyleUtils.FLOAT_LEFT));

        presetValues();

        form.addRequired(0, ECoMessageUtils.c.const_date_from(), fromBox);
        form.addRequired(1, ECoMessageUtils.c.const_date_to(), toBox);
        form.addRequired(2, ERMessageUtils.c.const_price(), valuePanel);
    }

    private void presetValues() {
        if (presetFrom != null) {
            fromBox.setValue(presetFrom);
        } else {
            fromBox.setValue(new Date());
        }
        if (presetPrice != null) {
            valueBox.setText(presetPrice.toString());
        } else {
            valueBox.setText("0");
        }
    }

    @Override
    public void focus() {
        fromBox.setFocus(true);
    }

    public ConsysDateBox getFromBox() {
        return fromBox;
    }

    public ConsysDateBox getToBox() {
        return toBox;
    }

    public Monetary getValue() {
        return Monetary.toMonetary(valueBox.getText());
    }
}
