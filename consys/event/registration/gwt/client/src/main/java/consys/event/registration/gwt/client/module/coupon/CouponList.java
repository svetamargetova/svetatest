package consys.event.registration.gwt.client.module.coupon;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import consys.event.registration.gwt.client.action.ListRegistrationSettingsAction;
import consys.event.registration.gwt.client.bo.ClientCouponListItem;
import consys.event.registration.gwt.client.module.coupon.CreateDiscountCouponDialog.RefreshAfterCreateDiscount;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ModuleNavigationUtils;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
public class CouponList extends DataListPanel
        implements consys.event.registration.api.list.RegistrationCouponList, RefreshAfterCreateDiscount {

    // data
    private HashMap<String, String> bundles;
    private ArrayList<SelectBoxItem<String>> bundleList;
    private CouponListFilter filters;
    private String defaultTicketUuid;

    public CouponList(String defaultTicketUuid) {
        super(ERMessageUtils.c.discountList_title(), LIST_TAG);
        this.defaultTicketUuid = defaultTicketUuid;

        bundles = new HashMap<String, String>();
        bundleList = new ArrayList<SelectBoxItem<String>>();

        ActionImage individual = ActionImage.getPlusButton(ERMessageUtils.c.discountListItem_text_individual(), true);
        individual.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new CreateDiscountCouponDialog(true, bundleList, CouponList.this.defaultTicketUuid, CouponList.this).showCentered();
            }
        });

        ActionImage group = ActionImage.getPlusButton(ERMessageUtils.c.discountListItem_text_group(), true);
        group.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                new CreateDiscountCouponDialog(false, bundleList, CouponList.this.defaultTicketUuid, CouponList.this).showCentered();
            }
        });

        addLeftControll(ActionImage.breadcrumbBackBigBack());
        addLeftControll(individual);
        addLeftControll(group);

        setListDelegate(new DiscountListDelegate());

        enableDownMenu(ModuleNavigationUtils.downMenuOptionBundlesSettings(), ModuleNavigationUtils.downMenuOptionSubbundlesSettings(), ModuleNavigationUtils.downMenuOptionSystemProperty());
        
        enableExport(ListExportTypeEnum.EXCEL);
                

        setDefaultFilter(new ListFilter(Filter_ALL) {

            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });

        filters = new CouponListFilter(this);
        addHeadPanel(filters);
    }

    /** pomoci metody nastavuje filtr aktualni vybrany bundle pro vychozi bundle v dialogu */
    public void changeDefaultBundle(String newDefaultBundleUuid) {
        defaultTicketUuid = newDefaultBundleUuid;
    }

    @Override
    public void refreshDiscount() {
        super.onLoad();
    }

    public void reload() {
        onLoad();
    }

    /** nastavuje filtr */
    public void setUsedFilter(Boolean usedFilter) {
        filters.selectUsed(usedFilter);
    }

    @Override
    protected void onLoad() {
        EventBus.get().fireEvent(new EventDispatchEvent(new ListRegistrationSettingsAction(),
                new AsyncCallback<ArrayListResult<ClientTabItem>>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ArrayListResult<ClientTabItem> result) {
                        bundleList.clear();
                        for (ClientTabItem i : result.getArrayListResult()) {
                            bundles.put(i.getUuid(), i.getTitle());
                            bundleList.add(new SelectBoxItem<String>(i.getUuid(), i.getTitle()));
                        }
                        filters.refreshBundles(bundleList);
                        if (defaultTicketUuid != null) {
                            filters.selectBundle(defaultTicketUuid);
                        } else {
                            CouponList.super.onLoad();
                        }
                    }
                }, this));
    }

    /** delegat seznamu */
    private class DiscountListDelegate implements ListDelegate<ClientCouponListItem, CouponListItem> {

        @Override
        public CouponListItem createCell(ClientCouponListItem item) {
            return new CouponListItem(item, bundles, CouponList.this);
        }
    }
}
