package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.*;
import consys.common.gwt.client.ui.comp.panel.element.Waiting;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.event.common.gwt.client.action.UpdateUserEventAction;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 *
 * @author pepa
 */
public abstract class ParticipantInfoPanel extends FlowPanel implements CssStyles {

    // komponenty
    private FlowPanel personPanel;
    private ConsysStringTextBox nameBox;
    private HelpTextBox positionBox;
    private HelpTextBox organizationBox;
    // data
    private EventUser user;
    private ConsysAction removeErrorAction;

    public ParticipantInfoPanel(EventUser user) {
        this.user = user;
    }

    @Override
    protected void onLoad() {
        generateContent();
    }

    public void setRemoveErrorAction(ConsysAction removeErrorAction) {
        this.removeErrorAction = removeErrorAction;
    }

    private void generateContent() {
        // Obrazok paticipanta
        SimplePanel portraitPanel = new SimplePanel();
        portraitPanel.setStyleName(ERResourceUtils.bundle().css().participantInfoPanelPortrait());
        portraitPanel.setWidget(FormUtils.userPortrait(user.getImageUuidPrefix(), UserProfileImageEnum.LIST));

        // Personal info - moznost editacie mena a afiliacie                
        personPanel = new FlowPanel();
        personPanel.setStyleName(ERResourceUtils.bundle().css().participantInfoPanelPersonal());
        personPanelReadMode();

        add(portraitPanel);
        add(personPanel);
    }

    private void personPanelReadMode() {
        personPanel.clear();
        if (removeErrorAction != null) {
            removeErrorAction.run();
        }

        ActionLabel editName = new ActionLabel(UIMessageUtils.c.const_edit(),
                StyleUtils.Assemble(MARGIN_TOP_3, FONT_10PX));
        editName.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                personPanelEditMode();
            }
        });

        boolean isEmptyPosition = StringUtils.isEmpty(user.getPosition());
        boolean isEmptyOrganization = StringUtils.isEmpty(user.getOrganization());

        Label nameLabel = StyleUtils.getStyledLabel(user.getFullName());
        nameLabel.getElement().setId("pName");
        Label positionLabel = StyleUtils.getStyledLabel(isEmptyPosition
                ? UIMessageUtils.c.const_position()
                : user.getPosition());
        positionLabel.getElement().setId("pPosition");
        Label atLabel = new Label(UIMessageUtils.c.const_at());
        atLabel.getElement().setId("pAt");
        Label organizationLabel = StyleUtils.getStyledLabel(isEmptyOrganization
                ? UIMessageUtils.c.const_organization()
                : user.getOrganization());
        organizationLabel.getElement().setId("pOrganization");

        if (isEmptyOrganization) {
            organizationLabel.addStyleName(ERResourceUtils.css().incomplete());
        }
        if (isEmptyPosition) {
            positionLabel.addStyleName(ERResourceUtils.css().incomplete());
        }

        FlowPanel name = new FlowPanel();
        name.add(nameLabel);
        name.add(editName);
        name.add(StyleUtils.clearDiv());
        name.addStyleName(MARGIN_BOT_10);

        nameLabel.getElement().getStyle().setDisplay(Style.Display.INLINE);

        personPanel.add(name);
        personPanel.add(positionLabel);
        personPanel.add(atLabel);
        personPanel.add(organizationLabel);
        personPanel.add(StyleUtils.clearDiv());
    }

    private void personPanelEditMode() {
        personPanel.clear();

        nameBox = new ConsysStringTextBox(1, 512, UIMessageUtils.c.const_name());
        nameBox.getElement().setId("pName");
        nameBox.setText(user.getFullName());

        positionBox = HelpTextBox.stringHelpTextBox(UIMessageUtils.c.const_position(), 0, 255);
        positionBox.getElement().setId("pPosition");
        positionBox.setText(user.getPosition());

        organizationBox = HelpTextBox.stringHelpTextBox(UIMessageUtils.c.const_organization(), 0, 512);
        organizationBox.getElement().setId("pOrganization");
        organizationBox.setText(user.getOrganization());

        ActionImage updateButton = ActionImage.getCheckButton(UIMessageUtils.c.const_update());
        updateButton.addStyleName(FLOAT_LEFT);
        ActionLabel cancelLabel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancelLabel.addStyleName(FLOAT_RIGHT);

        FlowPanel actionPanel = new FlowPanel();
        actionPanel.setWidth("100%");
        actionPanel.add(updateButton);
        actionPanel.add(cancelLabel);
        actionPanel.add(StyleUtils.clearDiv());

        personPanel.add(nameBox);
        personPanel.add(positionBox);
        personPanel.add(organizationBox);
        personPanel.add(actionPanel);

        updateButton.addClickHandler(updateClickHandler());

        cancelLabel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                personPanelReadMode();
            }
        });

        nameBox.setFocus(true);
    }

    private ClickHandler updateClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (StringUtils.isBlank(nameBox.getText())) {
                    nameBox.doValidate(getErrorMessage());
                    return;
                }

                final Waiting waiting = new Waiting(personPanel);
                SimplePanel waitingPart = new SimplePanel();
                waitingPart.setWidget(waiting);
                personPanel.add(waitingPart);

                UpdateUserEventAction action = new UpdateUserEventAction();
                action.setUserUuid(user.getUuid());
                action.setName(nameBox.getText());
                action.setPosition(positionBox.getText());
                action.setOrganization(organizationBox.getText());

                EventBus.get().fireEvent(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // nic
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        waiting.removeFromParent();
                        // nastvime nove hodnoty
                        user.setPosition(positionBox.getText());
                        user.setOrganization(organizationBox.getText());
                        user.setFullName(nameBox.getText());
                        // zobrazime read mode
                        personPanelReadMode();
                    }
                }, new ActionExecutionDelegate() {

                    @Override
                    public void actionStarted() {
                        waiting.show();
                    }

                    @Override
                    public void actionEnds() {
                        waiting.hide();
                    }

                    @Override
                    public void setFailMessage(CommonExceptionEnum value) {
                        getErrorMessage().setText(UIMessageUtils.getFailMessage(value));
                    }
                }));
            }
        };
    }

    public abstract ConsysMessage getErrorMessage();
}
