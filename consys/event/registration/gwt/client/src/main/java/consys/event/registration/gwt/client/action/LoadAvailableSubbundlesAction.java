package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;

/**
 * Akce pro nacteni subbalicku, ktere lze priobjednat
 * @author pepa
 */
public class LoadAvailableSubbundlesAction extends EventAction<ArrayListResult<ClientRegistrationPack>> {

    private static final long serialVersionUID = -1618599316709524062L;
    // data
    private String bundleUuid;
    private String managedUserUuid;

    public LoadAvailableSubbundlesAction() {
    }

    public LoadAvailableSubbundlesAction(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public String getManagedUserUuid() {
        return managedUserUuid;
    }

    public void setManagedUserUuid(String managedUserUuid) {
        this.managedUserUuid = managedUserUuid;
    }
}
