package consys.event.registration.gwt.client.module.setting.question;

import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.add.NewItemUI;
import consys.event.registration.gwt.client.utils.ERMessageUtils;

/**
 *
 * @author pepa
 */
public class NewQuestionUI extends NewItemUI {

    // komponenty
    private ConsysStringTextBox nameBox;
    private ConsysCheckBox requiredBox;

    @Override
    public void createForm(BaseForm form) {
        nameBox = new ConsysStringTextBox(1, 1024, ERMessageUtils.c.customQuestionItem_text_question());

        requiredBox = new ConsysCheckBox();
        requiredBox.setValue(Boolean.TRUE);

        form.addRequired(0, ERMessageUtils.c.customQuestionItem_text_question(), nameBox);
        form.addRequired(1, ERMessageUtils.c.customQuestionItem_text_required(), requiredBox);
    }

    @Override
    public void focus() {
        nameBox.setFocus(true);
    }

    public ConsysStringTextBox getNameBox() {
        return nameBox;
    }

    public ConsysCheckBox getRequiredBox() {
        return requiredBox;
    }
}
