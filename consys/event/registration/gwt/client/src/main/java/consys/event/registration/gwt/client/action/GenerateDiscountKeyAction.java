package consys.event.registration.gwt.client.action;

import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro vygenerovani/overeni slevoveho klice
 * @author pepa
 */
public class GenerateDiscountKeyAction extends EventAction<StringResult> {

    private static final long serialVersionUID = -6409831782800362714L;
    // data
    private String bundleUuid;
    private String key;

    public GenerateDiscountKeyAction() {
    }

    public GenerateDiscountKeyAction(String bundleUuid, String key) {
        this.bundleUuid = bundleUuid;
        this.key = key;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    public String getKey() {
        return key;
    }
}
