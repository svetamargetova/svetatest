package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.*;
import consys.common.gwt.client.ui.utils.*;
import static consys.common.gwt.client.ui.utils.CssStyles.MARGIN_BOT_10;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.event.registration.gwt.client.action.UpdateRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 * Panel obsahuje informacie o ucastnikovy ktory sa zaregistroval. Tento panel je
 * pouzity ako hlavicka celej stranky informujucej o registracii. Obsahuje
 * <ul>
 *  <li> Obrazok
 *  <li> Meno - editovatelne
 *  <li> Afiliaciu - editovatelne
 *  <li> Celiac,Vegetarian info - ak ma niektore z toho true
 *  <li> Special wishes - editovatelne ucastnikom
 *  <li> Poznamka - editovatelne len administratorom a neviditelne pre uzivatela
 * </ul>
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationParticipantInfoPanel extends FlowPanel implements CssStyles {

    // data
    private boolean administratorMode;
    private EventUser user;
    private String specialWish;
    private String adminNote;
    private String registrationUuid;
    private ClientRegistration registration;
    // komponenty
    private FlowPanel leftCol;
    private FlowPanel rightCol;
    private ConsysMessage err;

    public RegistrationParticipantInfoPanel(ClientRegistration registration) {
        this.registration = registration;
        this.user = registration.getEventUser();
        this.specialWish = registration.getSpecialWish();
        this.adminNote = registration.getNote();
        this.registrationUuid = registration.getRegistrationUuid();
    }

    public void setAdministratorMode(boolean administratorMode) {
        this.administratorMode = administratorMode;
    }

    public boolean isAdministratorMode() {
        return administratorMode;
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        generateContent();
    }

    private void generateContent() {
        setStyleName(ERResourceUtils.bundle().css().participantInfoPanel());

        ParticipantInfoPanel infoPanel = new ParticipantInfoPanel(user) {
            @Override
            public ConsysMessage getErrorMessage() {
                if (err == null) {
                    err = ConsysMessage.getFail();
                    err.addStyleName(MARGIN_BOT_10);
                    err.dontStartCloseTimer();
                    err.setWidth(250);
                    leftCol.insert(err, 0);
                }
                return err;
            }
        };
        infoPanel.setRemoveErrorAction(new ConsysAction() {
            @Override
            public void run() {
                // ak nastal nahodou error tak odstranime error panel
                if (err != null) {
                    err.removeFromParent();
                }
            }
        });

        ClientRegistrationOrder order = registration.getRegistrationOrders().get(0);

        // screen mame rozdeleny na dve casti
        leftCol = new FlowPanel();
        leftCol.setStyleName(ResourceUtils.common().css().leftColumn());
        leftCol.add(infoPanel);
        leftCol.add(new OrderControlPanel(order, registration, administratorMode));

        rightCol = new FlowPanel();
        rightCol.setStyleName(ResourceUtils.common().css().rightColumn());
        rightCol.add(new OrderDetailPanel(order, true));

        if (administratorMode) {
            // Vlozime text area na poznamku
            TextAreaUpdateBox updateAdminNote = new TextAreaUpdateBox(ERMessageUtils.c.editRegistrationForm_text_note(),
                    ERMessageUtils.c.editRegistrationForm_text_note_placeholder(), adminNote);
            updateAdminNote.setHandler(updateAdminNoteClickHandler(updateAdminNote));
            rightCol.add(updateAdminNote);

            // Ak ma user nejaku specialnu poziadavku dame o tom vediet administratorovy            
            if (StringUtils.isNotEmpty(specialWish)) {
                ConsysMessage userWishMessage = ConsysMessage.getInfo();
                userWishMessage.setText("<i>" + ERMessageUtils.c.editRegistrationForm_text_special_wish() + "</i> " + specialWish, true);
                userWishMessage.addStyleName(MARGIN_BOT_10);
                OrderDetailPanel.messageShowFactory(userWishMessage);
                rightCol.add(userWishMessage);
            }
        }

        leftCol.add(StyleUtils.clearDiv());
        rightCol.add(StyleUtils.clearDiv());

        add(rightCol);
        add(leftCol);
        add(StyleUtils.clearDiv());

    }

    private ClickHandler updateAdminNoteClickHandler(final TextAreaUpdateBox updateAdminNote) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new EventDispatchEvent(
                        new UpdateRegistrationAction(registrationUuid, updateAdminNote.getText().trim(), true),
                        new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        updateAdminNote.onFailure();
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        updateAdminNote.onSuccess();
                    }
                }, null));
            }
        };
    }
}
