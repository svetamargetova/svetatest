package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.bo.ProductThumb;
import consys.event.common.gwt.client.action.BuyProductAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Akce pro zaregistrovani uzivatele do eventu
 * @author pepa
 */
public class RegisterUserAction extends BuyProductAction<ClientRegistration> {

    private static final long serialVersionUID = -4575097093754161481L;
    // data
    private boolean additionalProducts;
    private HashMap<String, String> answers;
    private String managedUserUuid;

    public RegisterUserAction() {
    }

    public RegisterUserAction(String userUuid, String profileUuid, ArrayList<ProductThumb> products) {
        super(userUuid, profileUuid, products);
    }

    public boolean isAdditionalProducts() {
        return additionalProducts;
    }

    public void setAdditionalProducts(boolean additionalProducts) {
        this.additionalProducts = additionalProducts;
    }

    public HashMap<String, String> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<String, String> answers) {
        this.answers = answers;
    }

    public String getManagedUserUuid() {
        return managedUserUuid;
    }

    public void setManagedUserUuid(String managedUserUuid) {
        this.managedUserUuid = managedUserUuid;
    }
}
