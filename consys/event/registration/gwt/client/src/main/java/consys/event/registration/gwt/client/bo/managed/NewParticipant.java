package consys.event.registration.gwt.client.bo.managed;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author pepa
 */
public class NewParticipant implements IsSerializable {

    private String firstName;
    private String lastName;
    private String organization;

    public NewParticipant() {
    }

    public NewParticipant(String firstName, String lastName, String organization) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.organization = organization;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }
}
