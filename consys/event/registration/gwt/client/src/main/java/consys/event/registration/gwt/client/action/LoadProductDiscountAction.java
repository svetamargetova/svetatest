package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.Monetary;

/**
 * Akce pro nacteni velikosti slevy podle kodu a balicku
 * @author pepa
 */
public class LoadProductDiscountAction extends EventAction<Monetary> {

    private static final long serialVersionUID = 7728671272037295760L;
    // data
    private String productUuid;
    private String discountCode;

    public LoadProductDiscountAction() {
    }

    public LoadProductDiscountAction(String productUuid, String discountCode) {
        this.productUuid = productUuid;
        this.discountCode = discountCode;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getProductUuid() {
        return productUuid;
    }

    public void setProductUuid(String productUuid) {
        this.productUuid = productUuid;
    }
}
