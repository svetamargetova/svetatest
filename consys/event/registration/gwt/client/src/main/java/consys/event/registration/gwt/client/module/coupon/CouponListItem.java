package consys.event.registration.gwt.client.module.coupon;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.InlineLabel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.user.NameWithAffiliation;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.EventUser;
import consys.common.constants.img.UserProfileImageEnum;
import consys.common.gwt.shared.exception.AlreadyUsedObjectException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.registration.gwt.client.action.DeleteDiscountCouponAction;
import consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction;
import consys.event.registration.gwt.client.bo.ClientCouponListItem;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.module.register.AdminRegistrationOverview;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author pepa
 */
public class CouponListItem extends ListItem<ClientCouponListItem> {

    // konstanty
    private static final int LETTER_COUNTER_LIMIT = 160;
    // data
    private HashMap<String, String> bundles;
    private CouponList parent;
    private ClientCouponListItem item;

    public CouponListItem(ClientCouponListItem value, HashMap<String, String> bundles, CouponList parent) {
        super(value);
        this.bundles = bundles;
        this.parent = parent;
    }

    @Override
    protected void createCell(ClientCouponListItem item, FlowPanel panel) {
        this.item = item;
        FlowPanel userPart = new FlowPanel();
        userPart.setWidth("280px");
        userPart.addStyleName(StyleUtils.FLOAT_LEFT);
        generateUserContent(userPart);

        FlowPanel keyPart = new FlowPanel();
        keyPart.setWidth("300px");
        keyPart.addStyleName(StyleUtils.FLOAT_RIGHT);
        generateKeyContent(keyPart);

        FlowPanel portraitPanel = getPortrait();

        panel.clear();
        if (portraitPanel != null) {
            panel.add(portraitPanel);
        } else {
            userPart.setWidth("365px");
        }
        panel.add(userPart);
        panel.add(keyPart);
        panel.add(StyleUtils.clearDiv());
    }

    /** vraci obrazek v zavislosti na typu slevy */
    private FlowPanel getPortrait() {
        if (item.getCapacity() != 1) {
            return null;
        }

        boolean used = item.isUsed();
        Image portrait = null;

        if (used) {
            String imgString = item.getUsers().get(0).getImageUuidPrefix();
            portrait = FormUtils.userPortrait(imgString, UserProfileImageEnum.LIST);
        } else {
            return null;
        }

        FlowPanel portraitWrapper = FormUtils.portraitWrapper(UserProfileImageEnum.LIST);
        portraitWrapper.add(portrait);
        return portraitWrapper;
    }

    /** vygeneruje obsah panelu se seznamem uzivatelu */
    private void generateUserContent(FlowPanel panel) {
        if (item.getUsers().isEmpty()) {
            noneUser(panel);
        } else if (item.getUsers().size() == 1) {
            oneUser(panel);
        } else {
            groupOfUsers(panel);
        }
    }

    private void noneUser(FlowPanel panel) {
        Label notUsed = StyleUtils.getStyledLabel(ERMessageUtils.c.discountListItem_text_notUsed(),
                StyleUtils.FONT_17PX, StyleUtils.MARGIN_TOP_20);
        DOM.setStyleAttribute(notUsed.getElement(), "marginLeft", "85px");
        panel.add(notUsed);
    }

    private void oneUser(FlowPanel panel) {
        for (EventUser u : item.getUsers()) {
            String affilation = u.getAffiliation(UIMessageUtils.c.const_at());
            panel.add(new NameWithAffiliation(u.getFullName(), affilation, showRegistration(u.getUuid())));
        }
    }

    private void groupOfUsers(FlowPanel panel) {
        String firstFullName = item.getUsers().get(0).getFullName();
        int letterCounter = firstFullName.length();
        int userCounter = 1;

        ActionLabel firstName = new ActionLabel(firstFullName, StyleUtils.FONT_14PX);
        firstName.setClickHandler(showRegistrationClickHandler(item.getUsers().get(0).getUuid()));
        panel.add(firstName);

        for (EventUser u : item.getUsers().subList(1, item.getUsers().size())) {
            panel.add(new InlineLabel(", "));
            letterCounter += 2;

            String fullName = u.getFullName();
            letterCounter += fullName.length();

            ActionLabel name = new ActionLabel(fullName, StyleUtils.FONT_14PX);
            name.setClickHandler(showRegistrationClickHandler(u.getUuid()));
            panel.add(name);

            userCounter++;

            if (letterCounter > LETTER_COUNTER_LIMIT) {
                break;
            }
        }
        if (userCounter < item.getUsers().size()) {
            groupOfUsersMoreThanSize(panel);
        }
    }

    private void groupOfUsersMoreThanSize(FlowPanel panel) {
        panel.add(new InlineLabel(", ..."));

        ActionLabel showAll = new ActionLabel(ERMessageUtils.m.discountListItem_text_showAll(item.getUsers().size()),
                StyleUtils.MARGIN_TOP_10);
        showAll.removeStyleName(StyleUtils.INLINE);
        showAll.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                new KeyUserDialog(item.getUsers()).showCentered();
            }
        });
        panel.add(showAll);
    }

    /** clickhandler pro zobrazeni registrace */
    private ClickHandler showRegistrationClickHandler(final String userUuid) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                showRegistration(userUuid).run();
            }
        };
    }

    /** akce pro zobrazeni registrace */
    private ConsysAction showRegistration(final String userUuid) {
        return new ConsysAction() {
            @Override
            public void run() {
                EventBus.get().fireEvent(new EventDispatchEvent(new LoadEditRegistrationByUserUuidAction(userUuid),
                        new AsyncCallback<ClientRegistration>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(ClientRegistration result) {
                                fireNextBreadcrumb(result);
                            }
                        }, parent));
            }
        };
    }

    private void fireNextBreadcrumb(ClientRegistration data) {
        AdminRegistrationOverview w = new AdminRegistrationOverview(data);
        String title = RegistrationForm.anotherRegistrationBreadcrumb(data.getEventUser().getFullName());
        FormUtils.fireNextBreadcrumb(title, w);
    }

    /** vygeneruje obsah panelu s informacemi o slevovem klici */
    private void generateKeyContent(FlowPanel panel) {
        EditWithRemover ewr = new EditWithRemover(editAction(), removeAction());
        ewr.atRight();
        panel.add(ewr);
        panel.add(StyleUtils.clearDiv("5px"));

        TimeZone tz = (TimeZone) Cache.get().get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);

        panel.add(FormUtils.titleValue(ERMessageUtils.c.discountListItem_text_ticket(), bundles.get(item.getBundleUuid())));
        panel.add(FormUtils.titleValue(ERMessageUtils.c.discountListItem_text_code(), item.getKey()));
        panel.add(FormUtils.titleValue(ERMessageUtils.c.discountListItem_text_discount(), item.getDiscount() + " %"));
        panel.add(FormUtils.titleValue(ERMessageUtils.c.discountListItem_text_capacity(),
                item.getCapacity() == 0 ? ERMessageUtils.c.discountListItem_text_unlimited() : String.valueOf(item.getCapacity())));
        if (item.getEndDate() != null) {
            panel.add(FormUtils.titleValue(ERMessageUtils.c.discountListItem_text_validThrough(),
                    DateTimeUtils.getDateAndTime(item.getEndDate(), tz)));
        }
    }

    private ConsysAction editAction() {
        return new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextBreadcrumb(ERMessageUtils.c.editFormDiscountCoupon_title(),
                        new EditFormDiscountCoupon(item, bundles));
            }
        };
    }

    private ConsysAction removeAction() {
        return new ConsysAction() {
            @Override
            public void run() {
                EventBus.get().fireEvent(new EventDispatchEvent(
                        new DeleteDiscountCouponAction(item.getUuid()),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava eventactionexecutor
                                if (caught instanceof AlreadyUsedObjectException) {
                                    parent.getFailMessage().setText(ERMessageUtils.c.couponListItem_error_alreadyUsed());
                                }
                                if (caught instanceof NoRecordsForAction) {
                                    parent.getFailMessage().setText(ERMessageUtils.c.couponListItem_error_noRecords());
                                }
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                parent.reload();
                            }
                        }, parent));
            }
        };
    }

    /** dialog pro zobrazeni uzivatelu co pouzili dany slevovy klic */
    public class KeyUserDialog extends SmartDialog {

        // data
        private ArrayList<EventUser> eUsers;

        public KeyUserDialog(ArrayList<EventUser> eUsers) {
            super(400);
            this.eUsers = eUsers;
        }

        @Override
        protected void generateContent(SimpleFormPanel panel) {
            panel.addWidget(addUsers());
            panel.addWidget(new Separator("100%"));
            panel.addWidget(StyleUtils.getPadder());
            panel.addWidget(addCancel());
            panel.addWidget(StyleUtils.getPadder());
        }

        private CouponUserListPanel addUsers() {
            CouponUserListPanel users = new CouponUserListPanel(KeyUserDialog.this);
            users.setWidth("100%");
            users.setStaticHeight();
            users.setUsers(eUsers);
            return users;
        }

        private FlowPanel addCancel() {
            ActionLabel close = new ActionLabel(UIMessageUtils.c.const_close(),
                    StyleUtils.FLOAT_RIGHT + " " + StyleUtils.MARGIN_RIGHT_10);
            close.setClickHandler(hideClickHandler());

            FlowPanel cancelPanel = new FlowPanel();
            cancelPanel.add(close);
            cancelPanel.add(StyleUtils.clearDiv());
            return cancelPanel;
        }
    }
}
