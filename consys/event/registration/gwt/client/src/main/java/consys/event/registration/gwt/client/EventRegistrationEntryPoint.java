package consys.event.registration.gwt.client;

import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.rpc.action.CreateOuterRegisterAction;
import consys.common.gwt.client.rpc.action.IsUserLoggedAction;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.action.ListUserAffiliationThumbsAction;
import consys.common.gwt.client.rpc.action.ListUserThumbsAction;
import consys.common.gwt.client.rpc.action.LoadUserInfoAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.DateResult;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.list.ListDataSourceRequest;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.utils.TimeZoneProvider;
import consys.common.gwt.shared.bo.*;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.bo.ClientPaymentProfileDialog;
import consys.event.common.gwt.client.module.OverseerModule;
import consys.event.registration.gwt.client.action.CancelRegistrationAction;
import consys.event.registration.gwt.client.action.CreateRegistrationBundleAction;
import consys.event.registration.gwt.client.action.CreateRegistrationCycleAction;
import consys.event.registration.gwt.client.action.DeleteRegistrationBundleAction;
import consys.event.registration.gwt.client.action.DeleteRegistrationCycleAction;
import consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction;
import consys.event.registration.gwt.client.action.ListBundleFilterAction;
import consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction;
import consys.event.registration.gwt.client.action.LoadRegistrationCycleAction;
import consys.event.registration.gwt.client.action.ListRegistrationSettingsAction;
import consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction;
import consys.event.registration.gwt.client.action.LoadProductDiscountAction;
import consys.event.registration.gwt.client.action.LoadEditRegistrationAction;
import consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction;
import consys.event.registration.gwt.client.action.LoadRegistrationBundleAction;
import consys.event.registration.gwt.client.action.LoadUserRegistrationAction;
import consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction;
import consys.event.registration.gwt.client.action.RegisterUserAction;
import consys.event.registration.gwt.client.action.SendDiscountCodeAction;
import consys.event.registration.gwt.client.action.UpdateRegistrationAction;
import consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction;
import consys.event.registration.gwt.client.action.UpdateRegistrationCycleAction;
import consys.event.registration.gwt.client.bo.ClientBundle;
import consys.event.registration.gwt.client.bo.ClientBundlePriceSummary;
import consys.event.registration.gwt.client.bo.ClientListRegistrationItem;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.bo.ClientRegistrationResult;
import consys.event.common.gwt.client.action.*;
import consys.event.registration.gwt.client.action.CancelOrderAction;
import consys.event.registration.gwt.client.action.ConfirmOrderAction;
import consys.event.registration.gwt.client.action.CreateDiscountCouponAction;
import consys.event.registration.gwt.client.action.GenerateDiscountKeyAction;
import consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction;
import consys.event.registration.gwt.client.action.LoadOuterRegisterAction;
import consys.event.registration.gwt.client.action.UpdateCheckInAction;
import consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction;
import consys.event.registration.gwt.client.bo.ClientCheckInListItem;
import consys.event.registration.gwt.client.bo.ClientCouponListItem;
import consys.event.registration.gwt.client.bo.ClientOuterRegisterData;
import consys.event.registration.gwt.client.module.RegistrationModule;
import consys.event.registration.gwt.client.module.coupon.CouponList;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.event.common.gwt.client.module.EventCoreModule;
import consys.event.registration.gwt.client.action.*;
import consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction;
import consys.event.registration.gwt.client.action.managed.ListManagedRegistrationsAction;
import consys.event.registration.gwt.client.bo.*;
import consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations;
import consys.event.registration.gwt.client.module.lists.CheckInList;
import consys.event.registration.gwt.client.module.lists.RegistrationList;
import consys.event.registration.gwt.client.module.lists.UserParticipantList;
import consys.event.registration.gwt.client.module.register.AdminRegistrationOverview;
import consys.event.registration.gwt.client.module.register.InternalRegistration;
import consys.event.registration.gwt.client.module.register.RegistrationForm;
import consys.event.registration.gwt.client.module.register.UserRegistrationOverview;
import consys.event.registration.gwt.client.module.register.OuterRegistrationForm;
import consys.event.registration.gwt.client.module.register.RegisterAnother;
import consys.event.registration.gwt.client.module.register.panel.OuterRegistrationFormPanel;
import consys.event.registration.gwt.client.module.setting.RegistrationSettingsBundle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class EventRegistrationEntryPoint extends DebugModuleEntryPoint {

    ClientRegistration registration;

    @Override
    public void initMocks() {
        Cache.get().register(CommonProperties.EVENT_CURRENCY, "USD");
        Cache.get().register(OuterRegistrationFormPanel.OUTER_REGISTER_EVENT_NAME, "MŮJ event");

        ClientUser cu = new ClientUser();
        cu.setUuid("1244");
        cu.setFirstName("Janos");
        cu.setLastName("Slota");
        cu.setLoginEmail("my@email.cz");
        Cache.get().register(UserCacheAction.USER, cu);

        ClientEvent event = new ClientEvent();
        event.setUuid("32");
        event.setFullName("Debug event");
        event.setAcronym("Debug");
        ClientCurrentEvent cce = new ClientCurrentEvent();
        cce.setOverseerUrl("234");
        cce.setEvent(event);
        Cache.get().register(CurrentEventCacheAction.CURRENT_EVENT, cce);
        //-------------------------------------------------
        // CREATE
        //-------------------------------------------------
        ArrayListResult<String> createBundleArrayResult = new ArrayListResult<String>();
        createBundleArrayResult.getArrayListResult().add("1234");
        MockFactory.addActionMock(CreateRegistrationBundleAction.class, new ActionMock<ArrayListResult<String>>(createBundleArrayResult));
        //-------------------------------------------------
        ArrayListResult<String> createCycleArrayResult = new ArrayListResult<String>();
        createCycleArrayResult.getArrayListResult().add("1234");
        MockFactory.addActionMock(CreateRegistrationCycleAction.class, new ActionMock<ArrayListResult<String>>(createCycleArrayResult));
        //-------------------------------------------------
        // DELETE
        //-------------------------------------------------
        MockFactory.addActionMock(DeleteRegistrationBundleAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //-------------------------------------------------
        MockFactory.addActionMock(DeleteRegistrationCycleAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //-------------------------------------------------
        // LOAD
        //-------------------------------------------------

        ClientBundle bundle = new ClientBundle();
        bundle.setUuid("21342433223");
        bundle.setTitle("bundle 1");
        bundle.setDescription("description for bundle");
        bundle.setCodeEnter(true);
        bundle.setVat(23.6d);
        bundle.setQuantity(2);

        ClientRegistrationCycle crc = new ClientRegistrationCycle();
        Date from = new Date(1317600000000l); // 3.10.2011 00:00 gmt --> event v europe/london (leto +1)
        Date to = new Date(1317942000000l); // 6.10.2011 23:00 gmt --> event v europe/london (leto +1)
        String title = DateTimeUtils.getDate(from) + " - ";
        title += DateTimeUtils.getDate(to);
        crc.setUuid("33252");
        crc.setFrom(from);
        crc.setTo(to);
        bundle.getCycles().add(crc);

        crc = new ClientRegistrationCycle();
        from = new Date(1317942000000l); // 3.10.2011 00:00 gmt --> event v europe/london (leto +1)
        to = new Date(1318152000000l); // 6.10.2011 23:00 gmt --> event v europe/london (leto +1)
        title = DateTimeUtils.getDate(from) + " - ";
        title += DateTimeUtils.getDate(to);
        crc.setUuid("33251");
        crc.setFrom(from);
        crc.setTo(to);
        crc.setValue(new Monetary(100));
        bundle.getCycles().add(crc);

        ArrayListResult<ClientTabItem> settings = new ArrayListResult<ClientTabItem>();
        ClientTabItem item1 = new ClientTabItem();
        item1.setUuid("213424332");
        item1.setTitle("nazev 1");
        ClientTabItem item2 = new ClientTabItem();
        item2.setUuid("2134243322");
        item2.setTitle("nazev 2");
        ClientTabItem item3 = new ClientTabItem();
        item3.setUuid("21342433223");
        item3.setTitle("nazev 3");
        ClientTabItem item4 = new ClientTabItem();
        item4.setUuid("21342433224");
        item4.setTitle("nazev 4");
        settings.getArrayListResult().add(item1);
        settings.getArrayListResult().add(item2);
        settings.getArrayListResult().add(item3);
        settings.getArrayListResult().add(item4);

        MockFactory.addActionMock(ListRegistrationSettingsAction.class, new ActionMock<ArrayListResult<ClientTabItem>>(settings));
        MockFactory.addActionMock(LoadRegistrationBundleAction.class, new ActionMock<ClientBundle>(bundle));
        //-------------------------------------------------
        MockFactory.addActionMock(GenerateDiscountKeyAction.class, new ActionMock<StringResult>(new StringResult("slevovy-kod-1231#")));
        //-------------------------------------------------
        ClientRegistrationCycle crc1 = new ClientRegistrationCycle();
        crc1.setFrom(bundle.getCycles().get(0).getFrom());
        crc1.setTo(bundle.getCycles().get(0).getTo());
        title = DateTimeUtils.getDate(crc1.getFrom()) + " - ";
        title += DateTimeUtils.getDate(crc1.getTo());
        //crc.setFrom(new Date(2000, 12, 1));
        //crc.setTo(new Date(2000, 12, 5));
        crc1.setValue(new Monetary(500));


        ClientRegistrationCycle crc2 = new ClientRegistrationCycle();
        crc2.setFrom(bundle.getCycles().get(1).getFrom());
        crc2.setTo(bundle.getCycles().get(1).getTo());
        title = DateTimeUtils.getDate(crc2.getFrom()) + " - ";
        title += DateTimeUtils.getDate(crc2.getTo());
        //crc.setFrom(new Date(2000, 12, 1));
        //crc.setTo(new Date(2000, 12, 5));
        crc2.setValue(new Monetary(500));

        MockFactory.addActionMock(LoadRegistrationCycleAction.class, new ActionMock<ClientRegistrationCycle>(crc2));
        //-------------------------------------------------
        // UPDATE
        //-------------------------------------------------
        MockFactory.addActionMock(UpdateRegistrationBundleAction.class, new ActionMock<VoidResult>(new VoidResult()));
        MockFactory.addActionMock(UpdateRegistrationBundlePositionAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //-------------------------------------------------
        MockFactory.addActionMock(UpdateRegistrationCycleAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //-------------------------------------------------
        // LOAD USER REGISTRATION
        //-------------------------------------------------
        ClientRegistration cr = new ClientRegistration();
        cr.setNote("none");


        ClientRegistrationOrder cro1 = new ClientRegistrationOrder();
        cro1.setUuid("cro1");
        cro1.setTotalPrice(new Monetary(500));
        cro1.setDueDate(new Date());
        cro1.setOrderDate(new Date());
        cro1.setPaidDate(new Date());
        //cro1.setState(ClientRegistration.State.ORDERED);
        cro1.setState(ClientRegistration.State.CONFIRMED);
        cro1.getOrderItems().add(new ClientRegistrationOrderItem("Název balíčku",
                "Donec at convallis velit. Curabitur quis erat nisi, vitae congue odio. Pellentesque viverra dolor id enim.",
                "#### ### ###", "123"));
        cro1.getOrderItems().add(new ClientRegistrationOrderItem("název"));

        ClientRegistrationOrder cro2 = new ClientRegistrationOrder();
        cro2.setUuid("12321414abcd");
        cro2.setTotalPrice(new Monetary(150));
        cro2.setDueDate(new Date());
        cro2.setOrderDate(new Date());
        cro2.setPaidDate(new Date());
        cro2.setState(ClientRegistration.State.ORDERED);
        cro2.getOrderItems().add(new ClientRegistrationOrderItem("Workshop - Dave Morgan", "slev code xxx", "xxxxx", "12321414abcd"));
        cro2.getOrderItems().add(new ClientRegistrationOrderItem("Book: UML", "slev code xxx", null, "12321414abcd"));

        ClientRegistrationOrder cro3 = new ClientRegistrationOrder();
        cro3.setUuid("cro3");
        cro3.setTotalPrice(Monetary.ZERO);
        cro3.setDueDate(new Date());
        cro3.setOrderDate(new Date());
        cro3.setPaidDate(new Date());
        cro3.setState(ClientRegistration.State.CONFIRMED);
        cro3.getOrderItems().add(new ClientRegistrationOrderItem("Neco zadarmo", "VDIHFDPNIPDS"));

        cr.getRegistrationOrders().add(cro1);
        cr.getRegistrationOrders().add(cro2);
        cr.getRegistrationOrders().add(cro3);

        Date myDate1 = new Date(new Date().getTime() + 1100000000l);
        Date myDate2 = new Date(new Date().getTime() + 130000000l);
        Date myDate3 = new Date(new Date().getTime() + 140000000l);
        Date myDate4 = new Date(new Date().getTime() + 150000000l);
        Date myDate5 = new Date(new Date().getTime() + 160000000l);

        ClientRegistrationPack registrationOption1 = new ClientRegistrationPack();
        registrationOption1.setCapacity(11);
        registrationOption1.setWithCode(false);
        registrationOption1.setMaxQuantity(2);
        registrationOption1.setBundleDescription("Proin pellentesque placerat viverra. ");
        registrationOption1.setBundleTitle("Mobera Ticket");
        registrationOption1.setUuidBundle("12321414a");
        registrationOption1.setPrice(new Monetary(1000, 0));
        registrationOption1.setRegistred(10);
        registrationOption1.setTo(myDate1);
        registrationOption1.setWithCode(true);
        registrationOption1.getPricing().put(myDate1, new Monetary(1000));
        registrationOption1.getPricing().put(myDate2, new Monetary(1200));
        registrationOption1.getPricing().put(myDate3, new Monetary(1200));
        registrationOption1.getPricing().put(myDate4, new Monetary(1200));
        registrationOption1.getPricing().put(myDate5, new Monetary(1200));

        ClientRegistrationPack registrationOption2 = new ClientRegistrationPack();
        registrationOption2.setCapacity(50);
        registrationOption2.setWithCode(false);
        registrationOption2.setBundleDescription("Donec at convallis velit. Curabitur quis erat nisi, vitae congue odio. Pellentesque viverra dolor id enim egestas molestie. Proin semper massa ut sem elementum sodales. Cras eget metus vel ligula laoreet congue. Nunc eget lacus ut mauris sagittis sollicitudin ac ut odio. Maecenas dapibus ligula ligula. Morbi id arcu vitae lacus iaculis semper in non ipsum. Donec placerat velit varius enim lacinia faucibus. Aliquam iaculis, mi quis rhoncus imperdiet, justo magna faucibus dolor, quis mollis erat ligula in nisl. Quisque eget enim sed lacus aliquam euismod eget a enim. Nulla sollicitudin, erat et varius blandit, sapien sem porttitor ipsum, eget commodo quam nulla eu est. Aenean vel lorem consequat mi pellentesque semper eu ut urna. Phasellus ac nunc felis. Donec libero enim, sagittis suscipit volutpat faucibus, consectetur sit amet leo. Fusce sit amet nunc eget elit rutrum pretium in at est. ");
        registrationOption2.setBundleTitle("Mobera Ticket: fastest ones");
        registrationOption2.setUuidBundle("1231414b");
        //crb1.setPrice(new Monetary(2000, 0));
        registrationOption2.setRegistred(0);
        registrationOption2.setTo(new Date(new Date().getTime() + 100000000l));
        registrationOption2.setWithCode(true);

        ClientRegistrationPack registrationOption3 = new ClientRegistrationPack();
        registrationOption3.setCapacity(30);
        registrationOption3.setWithCode(false);
        registrationOption3.setBundleDescription("At convallis velit. Curabitur quis erat nisi, vitae congue odio. Pellentesque viverra dolor id enim egestas molestie. Proin semper massa ut sem elementum sodales. Cras eget metus vel ligula laoreet congue. Nunc eget lacus ut mauris sagittis sollicitudin ac ut odio. Maecenas dapibus ligula ligula. Morbi id arcu vitae lacus iaculis semper in non ipsum. Donec placerat velit varius enim lacinia faucibus. Aliquam iaculis, mi quis rhoncus imperdiet, justo magna faucibus dolor, quis mollis erat ligula in nisl. Quisque eget enim sed lacus aliquam euismod eget a enim. Nulla sollicitudin, erat et varius blandit, sapien sem porttitor ipsum, eget commodo quam nulla eu est. Aenean vel lorem consequat mi pellentesque semper eu ut urna. Phasellus ac nunc felis. Donec libero enim, sagittis suscipit volutpat faucibus, consectetur sit amet leo. Fusce sit amet nunc eget elit rutrum pretium in at est. ");
        registrationOption3.setBundleTitle("Mobera Student Ticket");
        registrationOption3.setUuidBundle("1231414ba");
        registrationOption3.setPrice(new Monetary(2000, 0));
        registrationOption3.setRegistred(30);
        registrationOption3.setTo(new Date(System.currentTimeMillis() + 120000000l));
        registrationOption3.setWithCode(true);

        ClientRegistrationResult crr = new ClientRegistrationResult();
        //crr.setRegistration(cr);
        crr.getBundles().add(registrationOption1);
        //crr.getBundles().add(registrationOption2);
        crr.getBundles().add(registrationOption3);
        MockFactory.addActionMock(LoadUserRegistrationAction.class, new ActionMock<ClientRegistrationResult>(crr));
        //-------------------------------------------------
        ArrayListResult<ClientBundlePriceSummary> summary = new ArrayListResult<ClientBundlePriceSummary>();
        summary.getArrayListResult().add(new ClientBundlePriceSummary(new Date(), new Monetary(1000)));
        summary.getArrayListResult().add(new ClientBundlePriceSummary(new Date(), new Monetary(1500)));
        summary.getArrayListResult().add(new ClientBundlePriceSummary(new Date(), new Monetary(2500)));
        MockFactory.addActionMock(ListBundlePriceSummaryAction.class, new ActionMock<ArrayListResult<ClientBundlePriceSummary>>(summary));
        //-------------------------------------------------
        // REGISTER USER
        //-------------------------------------------------

        EventUser eventUser = new EventUser();
        eventUser.setOrganization("Head ");
        eventUser.setFullName("Martin Věšeč");
        eventUser.setUuid("martinvesec");

        EventUser eventUser1 = new EventUser();
        eventUser1.setOrganization("Myšák");
        eventUser1.setFullName("Mickey Mouse");
        eventUser1.setUuid("123");

        ClientAnswer myAnswer1 = new ClientAnswer();
        myAnswer1.setAnswer("Moje odpověď na divnou otázku od organizátora této akce, zkusím ještě trochu delší text ať je vidět jak se zalamuje");
        myAnswer1.setOrder(1);
        myAnswer1.setQuestion("Text otázky pro účastníka");

        ClientAnswer myAnswer2 = new ClientAnswer();
        myAnswer2.setAnswer("Mám na to odpovídat?");
        myAnswer2.setOrder(2);
        myAnswer2.setQuestion("Všetečná otázka");

        registration = new ClientRegistration();
        registration.setNote("Organizator note");
        registration.setSpecialWish("I have a very special wish. I want to find a cow in dark night on desert with green eyes.");
        registration.setEventUser(eventUser);
        registration.getRegistrationOrders().add(cro1);
        registration.getRegistrationOrders().add(cro2);
        registration.getRegistrationOrders().add(cro3);
        registration.getAnswers().add(myAnswer1);
        registration.getAnswers().add(myAnswer2);
        MockFactory.addActionMock(RegisterUserAction.class, new ActionMock<ClientRegistration>(registration));
        //MockFactory.addActionMock(RegisterUserAction.class, new ActionMock<ClientRegistration>(new IbanException()));
        //-------------------------------------------------
        MockFactory.addActionMock(SendDiscountCodeAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //-------------------------------------------------

        MockFactory.addActionMock(LoadEditRegistrationAction.class, new ActionMock<ClientRegistration>(registration));
        //-------------------------------------------------
        MockFactory.addActionMock(UpdateRegistrationAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //-------------------------------------------------
        // DUPLICATE BUNDLE
        //-------------------------------------------------
        ClientTabItem cb = new ClientTabItem();
        cb.setTitle("bla bla - duplikat");
        cb.setUuid("1244dup-dup");
        MockFactory.addActionMock(DuplicateRegistrationBundleAction.class, new ActionMock<ClientTabItem>(cb));
        //------------------------------------------------
        ClientUserPaymentProfile profile = new ClientUserPaymentProfile();
        profile.setAccountNo("123");
        profile.setBankNo("bankno");
        profile.setCity("Brno");
        profile.setCountry(53);
        profile.setName("Nazev");
        profile.setRegNo("reg 123");
        profile.setStreet("Veveří 123");
        profile.setVatNo("324 vat");
        profile.setZip("612 00");

        ArrayList<ClientPaymentProfileThumb> payThumbs = new ArrayList<ClientPaymentProfileThumb>();
        payThumbs.add(new ClientPaymentProfileThumb("uu1", "Profil 1"));
        payThumbs.add(new ClientPaymentProfileThumb("uu2", "Profil 2"));
        ClientPaymentProfileDialog cpd = new ClientPaymentProfileDialog();
        cpd.setProfileThumbs(payThumbs);
        cpd.setFirstProfile(profile);
        MockFactory.addActionMock(ListUserPaymentProfileDialogAction.class, new ActionMock<ClientPaymentProfileDialog>(cpd));
        // --------------
        ClientPaymentOrderData cpdd = new ClientPaymentOrderData();
        cpdd.getProducts().add(new ClientPaymentProduct("Registrace 123", new Monetary(0), 1));
        cpdd.setTotalPrice(new Monetary(10));
        MockFactory.addActionMock(ListPaymentDataDialogAction.class, new ActionMock<ClientPaymentOrderData>(cpdd));
        // --------------
        MockFactory.addActionMock(LoadRegisterUserPaymentDialogAction.class, new ActionMock<ClientPaymentOrderData>(cpdd));
        //MockFactory.addActionMock(LoadRegisterUserPaymentDialogAction.class, new ActionMock<ClientPaymentOrderData>(new NoRecordsForAction()));
        // --------------
        MockFactory.addActionMock(LoadUserPaymentProfileAction.class, new ActionMock<ClientUserPaymentProfile>(profile));
        // --------------
        SelectBoxData locationList = new SelectBoxData();
        locationList.getList().add(new BoxItem("Chile", 34));
        locationList.getList().add(new BoxItem("Czech republic", 53));
        locationList.getList().add(new BoxItem("USA", 231));
        MockFactory.addActionMock(ListLocationAction.class, new ActionMock<SelectBoxData>(locationList));
        // --------------
        /*
         * ArrayList<ClientListParticipantItem> items = new
         * ArrayList<ClientListParticipantItem>(); ClientListParticipantItem a1
         * = new ClientListParticipantItem(); a1.setUserAffiliation("Afilace");
         * a1.setUserName("Martin Novák"); a1.setUserUuid("user123");
         * a1.setUuid("item123"); ClientListParticipantItem a2 = new
         * ClientListParticipantItem(); a2.setUserAffiliation("Afilace 1");
         * a2.setUserName("Lůďa Nováček"); a2.setUserUuid("user345");
         * a2.setUuid("item654"); items.add(a1);
         items.add(a2);
         */
        ArrayList<ClientListRegistrationItem> items = new ArrayList<ClientListRegistrationItem>();
        ClientListRegistrationItem a1 = new ClientListRegistrationItem();
        a1.setQuantity(2);
        a1.setBundleName("Bundle name");
        a1.setPaid(new Monetary(500));
        a1.setRegistrationDate(new Date());
        a1.setUserOrganization("Frogwares Game Development Studio");
        a1.setUserPosition("Game Developer and Sound Master");
        a1.setUserName("Martin Novák");
        a1.setUserUuid("user123");
        a1.setUuid("item123");
        a1.setState(ClientRegistrationState.PREREGISTERED);
        ClientListRegistrationItem a2 = new ClientListRegistrationItem();
        a2.setBundleName("Bundle name two");
        a2.setPaid(new Monetary(600));
        a2.setRegistrationDate(new Date());
        a2.setUserOrganization("Voda-topení-plyn Nováček");
        a2.setUserPosition("Instalatér");
        a2.setUserName("Lůďa Nováček");
        a2.setUserUuid("user345");
        a2.setUuid("item654");
        a2.setState(ClientRegistrationState.REGISTERED);
        ClientListRegistrationItem a3 = new ClientListRegistrationItem();
        a3.setBundleName("Bundle name two");
        a3.setPaid(new Monetary(600));
        a3.setRegistrationDate(new Date());
        a3.setUserOrganization("NWO");
        a3.setUserPosition("Šéf");
        a3.setUserName("Julius Hurvajs");
        a3.setUserUuid("hurvajs345");
        a3.setUuid("hurvajs654");
        a3.setState(ClientRegistrationState.WAITING);
        items.add(a1);
        items.add(a2);
        items.add(a3);

        ArrayList<ClientCheckInListItem> items2 = new ArrayList<ClientCheckInListItem>();
        ClientCheckInListItem ccil = new ClientCheckInListItem();
        ccil.setCheckDate(new Date());
        ccil.setUserOrganization("Afilace");
        ccil.setUserName("Neznámý vojín");
        ccil.setUserUuid("3423424");
        ccil.setUuidRegistration("2342");
        ccil.setQuantityChecked(1);
        items2.add(ccil);
        ClientCheckInListItem ccil2 = new ClientCheckInListItem();
        ccil2.setUserOrganization("Afilace dalšího účastníka");
        ccil2.setUserName("Známý vojín");
        ccil2.setUserUuid("32423424");
        ccil2.setUuidRegistration("23423");
        ccil2.setMaxQuantity(2);
        items2.add(ccil2);
        ClientCheckInListItem ccil3 = new ClientCheckInListItem();
        ccil3.setUserOrganization("Afilace dalšího účastníka");
        ccil3.setUserName("Známý vojín");
        ccil3.setUserUuid("32423424");
        ccil3.setUuidRegistration("23423");
        ccil3.setQuantityChecked(3);
        ccil3.setMaxQuantity(20);
        items2.add(ccil3);

        ClientUserThumb cut1 = new ClientUserThumb();
        cut1.setName("Pavol Grešša");

        ClientUserThumb cut2 = new ClientUserThumb();
        cut2.setName("Jaroslav Škrabálek");

        ClientUserThumb cut3 = new ClientUserThumb();
        cut3.setName("Josef Hubr");

        ArrayList<ClientCouponListItem> items3 = new ArrayList<ClientCouponListItem>();
        ClientCouponListItem di1 = new ClientCouponListItem();
        di1.setBundleUuid("21342433223");
        di1.setKey("123");
        di1.setDiscount(20);
        di1.setCapacity(0);
        di1.setEndDate(new Date());
        items3.add(di1);
        ClientCouponListItem di2 = new ClientCouponListItem();
        di2.setBundleUuid("21342433223");
        di2.setKey("1235");
        di2.setDiscount(10);
        di2.setCapacity(1);
        di2.getUsers().add(eventUser);
        items3.add(di2);
        ClientCouponListItem di3 = new ClientCouponListItem();
        di3.setBundleUuid("21342433224");
        di3.setKey("12353");
        di3.setDiscount(15);
        di3.setCapacity(5);
        di3.getUsers().add(eventUser);
        di3.getUsers().add(eventUser1);
        di3.getUsers().add(eventUser);
        di3.getUsers().add(eventUser1);
        di3.getUsers().add(eventUser);
        di3.getUsers().add(eventUser1);
        di3.getUsers().add(eventUser);
        di3.getUsers().add(eventUser1);
        di3.getUsers().add(eventUser);
        di3.getUsers().add(eventUser1);
        di3.getUsers().add(eventUser);
        di3.getUsers().add(eventUser1);
        di3.getUsers().add(eventUser);
        items3.add(di3);

        /*ListDataSourceResult ldsr = new ListDataSourceResult(items2, 2);
         MockFactory.addActionMock(ListDataSourceRequest.class, new ActionMock<ListDataSourceResult>(ldsr));*/
        //---------------
        ArrayListResult<ClientUserThumb> listCUT = new ArrayListResult<ClientUserThumb>();
        MockFactory.addActionMock(ListUserThumbsAction.class, new ActionMock<ArrayListResult<ClientUserThumb>>(listCUT));
        //---------------
        ArrayListResult<CommonThumb> bundleFilter = new ArrayListResult<CommonThumb>();
        bundleFilter.getArrayListResult().add(new CommonThumb("1", "První bundle"));
        bundleFilter.getArrayListResult().add(new CommonThumb("2", "Druhý bundle"));
        MockFactory.addActionMock(ListBundleFilterAction.class, new ActionMock<ArrayListResult<CommonThumb>>(bundleFilter));
        //---------------
        MockFactory.addActionMock(CancelRegistrationAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //---------------
        MockFactory.addActionMock(LoadProductDiscountAction.class, new ActionMock<Monetary>(new Monetary(1300)));
        //---------------

        //---------------
        MockFactory.addActionMock(CancelOrderAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        MockFactory.addActionMock(UpdateCheckInAction.class, new ActionMock<DateResult>(new DateResult()));
        //---------------
        MockFactory.addActionMock(CreateDiscountCouponAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //---------------
        ArrayListResult<ClientUserWithAffiliationThumb> uafs = new ArrayListResult<ClientUserWithAffiliationThumb>();
        ClientUserWithAffiliationThumb cuwat = new ClientUserWithAffiliationThumb();
        cuwat.setName("Marcel Chroustal");
        cuwat.setPosition("Chroustání chroustů");
        cuwat.setUuid("2342342");
        uafs.getArrayListResult().add(cuwat);
        MockFactory.addActionMock(ListUserAffiliationThumbsAction.class, new ActionMock<ArrayListResult<ClientUserWithAffiliationThumb>>(uafs));
        //---------------
        MockFactory.addActionMock(ConfirmOrderAction.class, new ActionMock<VoidResult>(new VoidResult()));
        //---------------
        ArrayListResult<EventUser> arleu = new ArrayListResult<EventUser>();
        arleu.getArrayListResult().add(eventUser);
        arleu.getArrayListResult().add(eventUser1);
        arleu.getArrayListResult().add(eventUser);
        arleu.getArrayListResult().add(eventUser1);
        arleu.getArrayListResult().add(eventUser);
        arleu.getArrayListResult().add(eventUser1);
        arleu.getArrayListResult().add(eventUser);
        //---------------
        MockFactory.addActionMock(LoadEditRegistrationByUserUuidAction.class, new ActionMock<ClientRegistration>(registration));
        //---------------
        ClientUserCommonInfo cui = new ClientUserCommonInfo();
        cui.setFullName("Lojzin Statečný");
        cui.setPosition("Popelář");
        cui.setOrganization(".A.S.A.");
        cui.setImageUuid("32424");
        cui.setUuid("user123123");
        cui.setEmail("mail@email.djdj.co");
        MockFactory.addActionMock(LoadUserInfoAction.class, new ActionMock<ClientUserCommonInfo>(cui));

        //-------------
        MockFactory.addActionMock(UpdateUserEventAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));




        // -----
        ArrayList<ClientPaymentProduct> productList = new ArrayList<ClientPaymentProduct>();
        ClientPaymentProduct p = new ClientPaymentProduct("Řádný účastník", new Monetary(100), 1);
        productList.add(p);

        ClientPaymentOrderData paymentData = new ClientPaymentOrderData();
        paymentData.setProducts(productList);
        paymentData.setTotalPrice(new Monetary(100));



        ArrayList<ClientRegistrationPack> datas = new ArrayList<ClientRegistrationPack>();

        ClientRegistrationPack main = new ClientRegistrationPack();
        main.setWithCode(false);
        main.setBundleDescription("Holders of MOBERA Conferenence badge have full access to the conference programme (excluding workshops) including supporting events. Registration covers conference lunch & coffee breaks and the gala dinner. Mobera social events for the first 333 registrations!");
        main.setBundleTitle("MOBERA Conference");
        main.setUuidBundle("12321414a");
        main.setPrice(new Monetary(1300, 0));
        main.setRegistred(10);
        main.setCapacity(0);
        main.setTo(new Date(new Date().getTime() + 1100000000l));
        main.setWithCode(true);

        ClientRegistrationPack sub1 = new ClientRegistrationPack();
        sub1.setWithCode(false);
        sub1.setBundleDescription("Holders of MOBERA Conferenence badge have full access to the conference programme (excluding workshops) including supporting events. Registration covers conference lunch & coffee breaks and the gala dinner. Mobera social events for the first 333 registrations!");
        sub1.setBundleTitle("Workshop - Dave Johnson");
        sub1.setUuidBundle("12321414a");
        sub1.setPrice(new Monetary(900, 0));
        sub1.setRegistred(10);
        sub1.setCapacity(12);
        sub1.setTo(new Date(new Date().getTime() + 1100000000l));

        ClientRegistrationPack sub2 = new ClientRegistrationPack();
        sub2.setBundleDescription("Holders of MOBERA Conferenence badge have full access to the conference programme (excluding workshops) including supporting events. Registration covers conference lunch & coffee breaks and the gala dinner. Mobera social events for the first 333 registrations!");
        sub2.setBundleTitle("Workshop - Dave Johnson");
        sub2.setUuidBundle("12321414b");
        sub2.setPrice(new Monetary(1000, 0));
        sub2.setRegistred(10);
        sub2.setCapacity(0);
        sub2.setTo(new Date(new Date().getTime() + 1100000000l));
        sub2.setWithCode(true);

        ClientRegistrationPack sub3 = new ClientRegistrationPack();
        sub3.setMaxQuantity(3);
        sub3.setBundleDescription("Hoja hoj");
        sub3.setBundleTitle("Workshop - Dave Johnson");
        sub3.setUuidBundle("12321414abcde");
        sub3.setPrice(new Monetary(1000, 0));
        sub3.setRegistred(8);
        sub3.setCapacity(10);
        sub3.setTo(new Date(new Date().getTime() + 1100000000l));
        sub3.setWithCode(true);

        datas.add(main);
        datas.add(sub1);
        datas.add(sub2);
        datas.add(sub3);
        ClientOuterRegisterData outerRegister = new ClientOuterRegisterData();
        outerRegister.setCurrency("EUR");
        outerRegister.setItems(datas);
        //outerRegister.setAffiliation("Acemcee developer");
        //outerRegister.setFirstName("Pavol");
        //outerRegister.setLastName("Grešša");
        //outerRegister.setEmail("gressa@acemcee.com");

        MockFactory.addActionMock(LoadOuterRegisterAction.class, new ActionMock<ClientOuterRegisterData>(outerRegister));

        MockFactory.addActionMock(CreateOuterRegisterAction.class, new ActionMock(new CouponInvalidException()));

        ClientEventThumb et = new ClientEventThumb();
        et.setFullName("Mobera 2011");
        et.setOverseer("sss");
        et.setUuid("uuid");

        MockFactory.addActionMock(LoadOuterEventInfoAction.class, new ActionMock<ClientEventThumb>(et));
        // -----
        MockFactory.addActionMock(IsUserLoggedAction.class, new ActionMock<BooleanResult>(new BooleanResult(false)));

        // -----
        ArrayListResult<ClientRegistrationPack> alrcs = new ArrayListResult<ClientRegistrationPack>();
        //alrcs.getArrayListResult().add(sub1);
        //alrcs.getArrayListResult().add(sub2);
        alrcs.getArrayListResult().add(sub3);

        MockFactory.addActionMock(LoadUserRegistrationDetailsAction.class, new ActionMock<ArrayListResult<ClientRegistrationPack>>(alrcs));
        //---------------
        MockFactory.addActionMock(LoadAvailableSubbundlesAction.class, new ActionMock<ArrayListResult<ClientRegistrationPack>>(alrcs));
        //---------------
        MockFactory.addActionMock(UpdateDiscountCouponAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        MockFactory.addActionMock(DeleteDiscountCouponAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        MockFactory.addActionMock(LoadWaitingRegistrationAction.class, new ActionMock<ClientRegistration>(registration));
        //---------------
        MockFactory.addActionMock(CancelWaitingAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        ClientWaitingBundleInfo cbi1 = new ClientWaitingBundleInfo();
        cbi1.setBundleName("Trochu delší název standardního vstupného");
        cbi1.setBundleUuid("1");
        cbi1.setFilledCapacity(5);
        cbi1.setFullCapacity(10);
        ClientWaitingBundleInfo cbi2 = new ClientWaitingBundleInfo();
        cbi2.setBundleName("Trochu delší název standardního vstupného dva");
        cbi2.setBundleUuid("2");
        cbi2.setFilledCapacity(3);
        cbi2.setFullCapacity(0);
        ClientWaitingBundlesInfo infoResult = new ClientWaitingBundlesInfo();
        infoResult.getInfos().add(cbi1);
        infoResult.getInfos().add(cbi2);
        MockFactory.addActionMock(ListOpenMainRegistrationForWaitingAction.class, new ActionMock<ClientWaitingBundlesInfo>(infoResult));
        //---------------
        ArrayListResult<CommonThumb> questions = new ArrayListResult<CommonThumb>();
        questions.getArrayListResult().add(new CommonThumb("q2", "Tři slova proč chcete jít na naši akci?"));
        questions.getArrayListResult().add(new CommonThumb("q1", "Jak se máte?"));
        MockFactory.addActionMock(ListCustomQuestionsAction.class, new ActionMock<ArrayListResult<CommonThumb>>(questions));
        //---------------
        MockFactory.addActionMock(DeleteCustomQuestionAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        ClientCustomQuestion question = new ClientCustomQuestion();
        question.setRequired(true);
        question.setText("Tři slova proč chcete jít na naši akci?");
        question.setUuid("ccqUuid1");
        MockFactory.addActionMock(LoadCustomQuestionAction.class, new ActionMock<ClientCustomQuestion>(question));
        //---------------
        MockFactory.addActionMock(UpdateCustomQuestionOrderAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        MockFactory.addActionMock(UpdateCustomQuestionAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        ArrayListResult<String> questionCreateR = new ArrayListResult<String>();
        questionCreateR.getArrayListResult().add("q3");
        MockFactory.addActionMock(CreateCustomQuestionAction.class, new ActionMock<ArrayListResult<String>>(questionCreateR));
        //---------------
        ClientCustomQuestion question1 = new ClientCustomQuestion();
        question1.setRequired(true);
        question1.setText("Oblíbená barva");
        question1.setUuid("ccqUuid2");

        ClientQuestions cQuestions = new ClientQuestions();
        cQuestions.getQuestions().add(question);
        cQuestions.getQuestions().add(question1);
        MockFactory.addActionMock(ListQuestionsForUserAction.class, new ActionMock<ClientQuestions>(cQuestions));
        //---------------
        MockFactory.addActionMock(CreateWaitingRecordAction.class, new ActionMock<ClientRegistration>(registration));
        //---------------
        MockFactory.addActionMock(AddNewParticipantsAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        //---------------
        ClientManagedRegistrations mr = new ClientManagedRegistrations();
        mr.getParticipants().add(eventUser);
        mr.getParticipants().add(eventUser1);

        MockFactory.addActionMock(ListManagedRegistrationsAction.class, new ActionMock<ClientManagedRegistrations>(mr));
        //---------------
        ClientBundlesAndQuestions baq = new ClientBundlesAndQuestions();
        baq.getBundles().add(main);
        baq.getBundles().add(sub1);
        //baq.getBundles().add(sub2);
        //baq.getBundles().add(sub3);
        baq.getQuestions().add(question);
        baq.getQuestions().add(question1);
        MockFactory.addActionMock(ListBundlesAndQuestionsForUserAction.class, new ActionMock<ClientBundlesAndQuestions>(baq));
        //---------------
        ArrayList<ClientInternalRegistrationListItem> internals = new ArrayList<ClientInternalRegistrationListItem>();
        ClientInternalRegistrationListItem int1 = new ClientInternalRegistrationListItem();
        int1.setBundleName("Balíček 1");
        int1.setRegistrationUuid("23424");
        int1.setQuantity(1);
        int1.setUserName("Testovací Uživatel 1");
        int1.setUserOrganization("Test s.r.o.");
        int1.setUserPosition("Tester - šéf");
        internals.add(int1);
        ClientInternalRegistrationListItem int2 = new ClientInternalRegistrationListItem();
        int2.setBundleName("Balíček 2");
        int2.setRegistrationUuid("234242");
        int2.setQuantity(1);
        int2.setUserName("Testovací Uživatel 2");
        int2.setUserOrganization("Test s.r.o.");
        int2.setUserPosition("Tester - poskok");
        internals.add(int2);
        ListDataSourceResult intrnls = new ListDataSourceResult(internals, 2);
        MockFactory.addActionMock(ListDataSourceRequest.class, new ActionMock<ListDataSourceResult>(intrnls));
        //----------------------------------------
        ClientUserPaymentProfile up = new ClientUserPaymentProfile();
        up.setCity("Město");
        up.setCountry2ch("CZ");
        up.setStreet("Ulice");
        up.setZip("23589");
        MockFactory.addActionMock(LoadOrderPaymentProfileAction.class, new ActionMock<ClientUserPaymentProfile>(up));
    }

    @Override
    public void registerModules(List<Module> modules) {
        modules.add(new OverseerModule());
        modules.add(new EventCoreModule());
        modules.add(new RegistrationModule());
    }

    @Override
    public void registerForms(Map<String, Widget> formMap) {
        Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, TimeZoneProvider.getTimeZoneByID("Europe/London"));

        formMap.put("User Registration Overview", new UserRegistrationOverview(registration, false));
        formMap.put("Internal registration", new InternalRegistration());
        formMap.put("Register another", new RegisterAnother());
        formMap.put("Registration", new RegistrationForm("1231414ba"));
        formMap.put("DiscountList", new CouponList("213424332"));
        formMap.put("Outer Registration", new OuterRegistrationForm());
        formMap.put("Admin Registration Overview", new AdminRegistrationOverview("213"));
        formMap.put("Registration Registration list", new RegistrationList());
        formMap.put("Registration Settings - bundle", new RegistrationSettingsBundle(false));
        formMap.put("Registration Settings - subbundle", new RegistrationSettingsBundle(true));
        formMap.put("CheckIn List", new CheckInList());
        formMap.put("Registration Participant list", new UserParticipantList());
    }
}
