package consys.event.registration.gwt.client.module.register.item;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.add.NewItemDialog;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction;
import consys.event.registration.gwt.client.bo.managed.NewParticipant;
import consys.event.registration.gwt.client.module.register.RegisterAnother;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import java.util.ArrayList;

/**
 * Dialog pro pridani spravovaneho uzivatele
 * @author pepa
 */
public class AddManagedUserDialog extends NewItemDialog<AddManagedUserUI> {

    // data
    private RegisterAnother parent;

    public AddManagedUserDialog(RegisterAnother parent, ConsysMessage helpMessage) {
        super(ERMessageUtils.c.registerAnother_action_addManagedParticipant(), helpMessage);
        this.parent = parent;
    }

    @Override
    public AddManagedUserUI newItem() {
        return new AddManagedUserUI();
    }

    @Override
    public void doCreateAction(ArrayList<AddManagedUserUI> uis, ConsysMessage fail) {
        AddNewParticipantsAction action = new AddNewParticipantsAction();

        for (AddManagedUserUI u : uis) {
            if (StringUtils.isBlank(u.getFirstName())) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ERMessageUtils.c.addManagedUserUI_text_firstName()));
                return;
            } else if (StringUtils.isBlank(u.getLastName())) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ERMessageUtils.c.addManagedUserUI_text_lastName()));
                return;
            } else if (StringUtils.isBlank(u.getOrganization())) {
                fail.addOrSetText(UIMessageUtils.m.const_fieldMustEntered(ERMessageUtils.c.addManagedUserUI_text_organization()));
                return;
            }
            action.getParticipants().add(new NewParticipant(u.getFirstName(), u.getLastName(), u.getOrganization()));
        }

        EventBus.fire(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava event action executor
            }

            @Override
            public void onSuccess(VoidResult result) {
                hide();
                parent.reload();
            }
        }, AddManagedUserDialog.this));
    }
}
