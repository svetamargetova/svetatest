package consys.event.registration.gwt.client.action;

import consys.event.common.gwt.client.action.ListPaymentDataDialogAction;

/**
 * Akce pro nacteni platebnich informaci
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadRegisterUserPaymentDialogAction extends ListPaymentDataDialogAction {

    private static final long serialVersionUID = -1269283699406017561L;
    // data
    private String managedUserUuid;

    public LoadRegisterUserPaymentDialogAction() {
    }

    public LoadRegisterUserPaymentDialogAction(String managedUserUuid) {
        this.managedUserUuid = managedUserUuid;
    }

    public String getManagedUserUuid() {
        return managedUserUuid;
    }
}
