package consys.event.registration.gwt.client;

import consys.common.gwt.client.ModuleEntryPoint;
import consys.common.gwt.client.rpc.action.CreateOuterRegisterAction;
import consys.common.gwt.client.rpc.action.InitializeSsoAction;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.action.LoadSsoUserDetailsAction;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.bo.ClientSsoUserDetails;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.event.common.gwt.client.action.LoadOuterEventInfoAction;
import consys.event.common.gwt.client.bo.ClientOuterEventData;
import consys.event.common.gwt.client.rpc.EventActionExecutor;
import consys.event.registration.gwt.client.action.ListQuestionsForUserAction;
import consys.event.registration.gwt.client.action.LoadOuterRegisterAction;
import consys.event.registration.gwt.client.action.LoadProductDiscountAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.bo.ClientOuterRegisterData;
import consys.event.registration.gwt.client.bo.ClientQuestions;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.module.register.OuterRegistrationFormV2;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class EventRegistrationEntryPointOuter extends ModuleEntryPoint {

    @Override
    public void initMocks() {
        // ------------------------------------------------
        MockFactory.addActionMock(InitializeSsoAction.class, new ActionMock<StringResult>(new StringResult("deset dvacet třicet")));

        // ------------------------------------------------
        ClientOuterEventData clientEventThumb = new ClientOuterEventData();
        clientEventThumb.setFullName("Full event name");
        clientEventThumb.setDescription("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ante purus, pretium vel varius eu, accumsan a tortor. Mauris et laoreet erat. Nulla ultrices, felis eget consequat ultrices, diam eros sollicitudin risus, sit amet blandit nunc odio vitae ipsum. Ut feugiat est in metus pellentesque sed laoreet risus ullamcorper. Quisque tempor, ligula ut euismod venenatis, mauris elit aliquet sapien, sit amet hendrerit urna urna ac metus. Proin mattis convallis euismod. Pellentesque urna est, auctor in pellentesque non, dignissim rhoncus neque. Vivamus ultricies purus quis sem blandit aliquam sit amet sit amet purus. Etiam pellentesque posuere urna, quis luctus sapien tempor non. Suspendisse potenti. Pellentesque vestibulum sagittis tortor, eget ornare odio consectetur at. Proin pellentesque, magna quis ultrices iaculis, urna risus sollicitudin quam, quis tincidunt sapien lorem nec quam. Etiam consequat lorem vel sapien vulputate suscipit. Proin scelerisque orci eu massa venenatis pharetra. Suspendisse vestibulum sagittis nibh sit amet porta.</p>"
                + "<p>Vivamus dictum viverra nunc, sit amet viverra tellus faucibus nec. Etiam metus urna, volutpat at consequat vel, porta quis ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi pretium est in eros lobortis congue. Curabitur neque dolor, aliquet in sollicitudin in, porta nec sapien. Etiam quis nisl nisl. Praesent sapien est, ornare at ornare vel, congue quis est. Suspendisse bibendum diam id metus laoreet lacinia. Fusce sed turpis eros. In hac habitasse platea dictumst. Donec vulputate arcu mauris. Aliquam et iaculis mauris. Fusce venenatis posuere magna, nec tristique mi luctus malesuada. Nulla posuere, justo nec mollis ullamcorper, diam urna dictum nulla, ut condimentum erat libero vel risus. </p>"
                + "<p>Aenean convallis ante a orci vulputate lacinia. Duis vitae turpis et metus euismod dictum nec nec massa. In adipiscing nisi id nulla ultrices pretium. Nullam iaculis scelerisque sapien, et ultricies sem fringilla ut. Integer vel nisi vitae mi tempor imperdiet sed ac orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean ultrices, purus vitae tristique varius, nunc justo malesuada purus, ut ultricies tellus tellus id nunc. Quisque leo purus, egestas eu lobortis ac, dictum non nulla. Integer accumsan eleifend libero, ut ultricies augue imperdiet in.</p>");
        clientEventThumb.setActive(true);
        clientEventThumb.setUuid("full_event_uuid");
        clientEventThumb.setProfileLogoUuidPrefix("profileImageUuid");
        MockFactory.addActionMock(LoadOuterEventInfoAction.class, new ActionMock<ClientOuterEventData>(clientEventThumb));

        // ------------------------------------------------
        SelectBoxData locationList = new SelectBoxData();
        locationList.getList().add(new BoxItem("Chile", 34));
        locationList.getList().add(new BoxItem("Czech republic", 53));
        locationList.getList().add(new BoxItem("USA", 231));
        MockFactory.addActionMock(ListLocationAction.class, new ActionMock<SelectBoxData>(locationList));

        // ------------------------------------------------
        ArrayList<ClientRegistrationPack> datas = new ArrayList<ClientRegistrationPack>();

        ClientRegistrationPack main = new ClientRegistrationPack();
        main.setWithCode(false);
        main.setBundleDescription("Holders of MOBERA Conferenence badge have full access to the conference programme (excluding workshops) including supporting events. Registration covers conference lunch & coffee breaks and the gala dinner. Mobera social events for the first 333 registrations!");
        main.setBundleTitle("MOBERA Conference");
        main.setUuidBundle("12321414a");
        main.setPrice(new Monetary(0, 0));
        main.setRegistred(10);
        main.setCapacity(0);
        main.setTo(new Date(new Date().getTime() + 1100000000l));
        main.setWithCode(true);

        ClientRegistrationPack sub1 = new ClientRegistrationPack();
        sub1.setWithCode(false);
        sub1.setBundleDescription("Holders of MOBERA Conferenence badge have full access to the conference programme (excluding workshops) including supporting events. Registration covers conference lunch & coffee breaks and the gala dinner. Mobera social events for the first 333 registrations!");
        sub1.setBundleTitle("Workshop - Dave Johnson");
        sub1.setMaxQuantity(10);
        sub1.setUuidBundle("12321414a");
        sub1.setPrice(new Monetary(900, 0));
        sub1.setRegistred(10);
        sub1.setCapacity(12);
        sub1.setTo(new Date(new Date().getTime() + 1100000000l));

        ClientRegistrationPack sub2 = new ClientRegistrationPack();
        sub2.setBundleDescription("Holders of MOBERA Conferenence badge have full access to the conference programme (excluding workshops) including supporting events. Registration covers conference lunch & coffee breaks and the gala dinner. Mobera social events for the first 333 registrations!");
        sub2.setBundleTitle("Workshop - Dave Johnson");
        sub2.setUuidBundle("12321414b");
        sub2.setPrice(new Monetary(1000, 0));
        sub2.setRegistred(10);
        sub2.setCapacity(0);
        sub2.setTo(new Date(new Date().getTime() + 1100000000l));
        sub2.setWithCode(true);

        ClientRegistrationPack sub3 = new ClientRegistrationPack();
        sub3.setBundleDescription("Hoja hoj");
        sub3.setBundleTitle("Workshop - Dave Johnson");
        sub3.setUuidBundle("12321414abcde");
        sub3.setPrice(new Monetary(1000, 0));
        sub3.setRegistred(8);
        sub3.setCapacity(10);
        sub3.setTo(new Date(new Date().getTime() + 1100000000l));
        sub3.setWithCode(true);

        datas.add(main);
        datas.add(sub1);
        datas.add(sub2);
        datas.add(sub3);
        ClientOuterRegisterData cord = new ClientOuterRegisterData();
        cord.setCurrency("EUR");
        cord.setItems(datas);
        MockFactory.addActionMock(LoadOuterRegisterAction.class, new ActionMock<ClientOuterRegisterData>(cord));

        //--------------------------------------------
        ClientSsoUserDetails userDetails = new ClientSsoUserDetails();
        userDetails.setFirstName("Marcel");
        userDetails.setLastName("Chroustal");
        userDetails.setSsoStringId("ssoStringId");
        userDetails.setProfilePictureUrl("http://hekuba.fi.muni.cz:17777/takeplace/user/profile/load?id=d80332ee676840fb9ac515474a68f5ac-100");
        MockFactory.addActionMock(LoadSsoUserDetailsAction.class, new ActionMock<ClientSsoUserDetails>(userDetails));

        //--------------------------------------------
        MockFactory.addActionMock(LoadProductDiscountAction.class, new ActionMock<Monetary>(new Monetary(100)));
        //--------------------------------------------
        MockFactory.addActionMock(CreateOuterRegisterAction.class, new ActionMock<VoidResult>(new BundleCapacityException()));
        //--------------------------------------------
        ClientCustomQuestion question1 = new ClientCustomQuestion();
        question1.setRequired(true);
        question1.setText("Oblíbená barva");
        
        ClientCustomQuestion question2 = new ClientCustomQuestion();
        question2.setRequired(false);
        question2.setText("Věk");
        
        ClientQuestions cQuestions = new ClientQuestions();
        cQuestions.getQuestions().add(question1);
        cQuestions.getQuestions().add(question2);
        MockFactory.addActionMock(ListQuestionsForUserAction.class, new ActionMock<ClientQuestions>(cQuestions));
    }

    @Override
    public void initLayout() {
        EventActionExecutor.init();

        OuterRegistrationFormV2 form = new OuterRegistrationFormV2();
        form.prepare();
        form.start();
    }
}
