package consys.event.registration.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;

/**
 * Akce pro nacteni regisracniho cyklu podle uuid
 * @author pepa
 */
public class LoadRegistrationCycleAction extends EventAction<ClientRegistrationCycle> {

    private static final long serialVersionUID = -701845423435236646L;
    // data
    private String uuid;

    public LoadRegistrationCycleAction() {
    }

    public LoadRegistrationCycleAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
