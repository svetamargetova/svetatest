package consys.event.registration.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class ClientWaitingBundlesInfo implements Result {

    private static final long serialVersionUID = 4705197652317570354L;
    // data
    private ArrayList<ClientWaitingBundleInfo> infos;

    public ClientWaitingBundlesInfo() {
        infos = new ArrayList<ClientWaitingBundleInfo>();
    }

    public ArrayList<ClientWaitingBundleInfo> getInfos() {
        return infos;
    }

    public void setInfos(ArrayList<ClientWaitingBundleInfo> infos) {
        this.infos = infos;
    }
}
