package consys.event.registration.gwt.client.module.lists.item;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.registration.gwt.client.module.lists.RegistrationList;
import consys.event.registration.gwt.client.utils.ERMessageUtils;
import consys.event.registration.gwt.client.utils.ERResourceUtils;

/**
 *
 * @author pepa
 */
public class BadgeSizeDialog extends SmartDialog {

    public BadgeSizeDialog() {
        super(320);
    }
    
    @Override
    protected void generateContent(SimpleFormPanel panel) {
        FlowPanel fp = new FlowPanel();
        fp.addStyleName(ERResourceUtils.css().badgesSizeDialogSizeButtonWrapper());

        ActionImage size1 = ActionImage.getDownArrowButton("90 mm x 50 mm", false);
        size1.addClickHandler(clickHandler(BadgeSize.SIZE_90_X_50));
        size1.addStyleName(ERResourceUtils.css().badgesSizeDialogSizeButton());

        ActionImage size2 = ActionImage.getDownArrowButton("101 mm x 76 mm", false);
        size2.addClickHandler(clickHandler(BadgeSize.SIZE_101_X_76));
        size2.addStyleName(ERResourceUtils.css().badgesSizeDialogSizeButton());

        fp.add(size1);
        fp.add(size2);
        fp.add(StyleUtils.clearDiv());

        panel.addWidget(StyleUtils.clearDiv("10px"));
        panel.addWidget(StyleUtils.getStyledLabel(ERMessageUtils.c.badgesSizeDialog_title(),
                ERResourceUtils.css().badgesSizeDialogTitle()));
        panel.addWidget(StyleUtils.clearDiv("10px"));
        panel.addWidget(fp);
        panel.addWidget(StyleUtils.clearDiv("10px"));
    }

    private ClickHandler clickHandler(final BadgeSize size) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                try {
                    Window.open(EventURLFactory.downloadRegistrationBadges(size.toString()), "_blank", "");
                    BadgeSizeDialog.this.hide();
                } catch (NotInCacheException ex) {
                    LoggerFactory.log(RegistrationList.class, "Something is missing in local cache");
                }
            }
        };
    }

    private enum BadgeSize {

        SIZE_90_X_50, SIZE_101_X_76;
    }
}
