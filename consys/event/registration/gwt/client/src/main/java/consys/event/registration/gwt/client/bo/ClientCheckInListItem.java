package consys.event.registration.gwt.client.bo;

import java.util.Date;

/**
 *
 * @author pepa
 */
public class ClientCheckInListItem extends ClientListParticipantItem {

    private static final long serialVersionUID = -7567443600292803774L;
    // data
    private String uuidRegistration;
    private Date checkDate;
    private int maxQuantity = 1;
    private int quantityChecked;

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getUuidRegistration() {
        return uuidRegistration;
    }

    public void setUuidRegistration(String uuidRegistration) {
        this.uuidRegistration = uuidRegistration;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(int maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public int getQuantityChecked() {
        return quantityChecked;
    }

    public void setQuantityChecked(int quantityChecked) {
        this.quantityChecked = quantityChecked;
    }
}
