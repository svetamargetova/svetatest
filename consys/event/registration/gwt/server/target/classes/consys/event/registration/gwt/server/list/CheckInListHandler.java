package consys.event.registration.gwt.server.list;

import consys.event.common.gwt.server.GwtListHandler;
import consys.event.registration.api.list.CheckInList;
import consys.event.registration.core.bo.thumb.CheckInListItem;
import consys.event.registration.gwt.client.bo.ClientCheckInListItem;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CheckInListHandler implements GwtListHandler<ClientCheckInListItem, CheckInListItem>, CheckInList {

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public void execute(List<CheckInListItem> in, ArrayList<ClientCheckInListItem> out) {
        for (CheckInListItem item : in) {
            ClientCheckInListItem o = new ClientCheckInListItem();
            o.setCheckDate(item.getCheckInDate());
            o.setUserPosition(item.getUserPosition());
            o.setUserOrganization(item.getUserOrganization());
            o.setUserName(item.getUserName());
            o.setUserUuid(item.getUserUuid());
            o.setUuidRegistration(item.getRegistrationUuid());
            o.setUuidPortraitImagePrefix(item.getProfileImagePrefix());
            o.setMaxQuantity(item.getMaxQuantity());
            o.setQuantityChecked(item.getQuantityChecked());
            out.add(o);
        }
    }
}
