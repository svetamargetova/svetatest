package consys.event.registration.gwt.server.list;

import consys.event.common.gwt.server.GwtListHandler;
import consys.event.registration.api.list.ParticipantList;
import consys.event.registration.core.bo.thumb.ParticipantListItem;
import consys.event.registration.gwt.client.bo.ClientListParticipantItem;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ParticipantListHandler implements GwtListHandler<ClientListParticipantItem, ParticipantListItem>, ParticipantList {

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public void execute(List<ParticipantListItem> in, ArrayList<ClientListParticipantItem> out) {
        for (ParticipantListItem item : in) {
            ClientListParticipantItem p = new ClientListParticipantItem();
            p.setUserPosition(item.getUserPosition());
            p.setUserOrganization(item.getUserOrganization());
            p.setUserName(item.getUserName());
            p.setUserUuid(item.getUserUuid());
            p.setUuid(item.getRegistrationUuid());
            p.setUuidPortraitImagePrefix(item.getProfileImagePrefix());
            out.add(p);
        }
    }
}
