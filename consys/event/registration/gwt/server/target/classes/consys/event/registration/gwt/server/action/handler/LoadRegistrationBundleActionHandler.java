package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.bo.thumb.RegistrationCouponStats;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.LoadRegistrationBundleAction;
import consys.event.registration.gwt.client.bo.ClientBundle;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadRegistrationBundleActionHandler implements EventActionHandler<LoadRegistrationBundleAction, ClientBundle> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<LoadRegistrationBundleAction> getActionType() {
        return LoadRegistrationBundleAction.class;
    }

    @Override
    public ClientBundle execute(LoadRegistrationBundleAction action) throws ActionException {
        try {
            RegistrationBundle bundle = bundleService.loadRegistrationBundleWithCycles(action.getUuid());
            ClientBundle cb = new ClientBundle();
            cb.setCapacity(bundle.getCapacity());
            cb.setQuantity(bundle.getQuantity());
            cb.setCodeEnter(bundle.isWithCode());
            cb.setDescription(bundle.getDescription());
            cb.setTitle(bundle.getTitle());
            cb.setUuid(bundle.getUuid());
            cb.setVat(bundle.getVat() != null ? bundle.getVat().doubleValue() : 0);
            cb.setB2BTicket(bundle.isB2b());
            cb.setSubbundle(bundle.isSubBundle());
            for (RegistrationCycle cycle : bundle.getCycles()) {
                ClientRegistrationCycle c = new ClientRegistrationCycle();
                c.setFrom(cycle.getFrom());
                c.setTo(cycle.getTo());
                c.setUuid(cycle.getUuid());
                c.setValue(Monetary.toMonetary(cycle.getPrice().toString()));
                cb.getCycles().add(c);
            }
            RegistrationCouponStats stats = bundleService.loadRegistrationCouponsStats(bundle.getUuid());
            cb.setDiscountCodes(stats.getCouponsAll());
            cb.setDiscountUsed(stats.getCouponsUsed());
            return cb;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
