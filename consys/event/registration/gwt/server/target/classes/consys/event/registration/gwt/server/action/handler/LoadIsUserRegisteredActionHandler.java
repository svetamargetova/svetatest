package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.LoadIsUserRegisteredAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadIsUserRegisteredActionHandler implements EventActionHandler<LoadIsUserRegisteredAction, BooleanResult> {

    @Autowired
    private UserRegistrationService registrationService;

    @Override
    public Class<LoadIsUserRegisteredAction> getActionType() {
        return LoadIsUserRegisteredAction.class;
    }

    @Override
    public BooleanResult execute(LoadIsUserRegisteredAction action) throws ActionException {
        try {
            // pokusime se nacist registraci, kdyz projde, uzivatel je registrovany
            registrationService.loadUserActiveRegistrationDetailedByUserUuid(action.getUserEventUuid());
            return new BooleanResult(true);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            // nenasla se registrace, takze uzivatel neni registrovany
            return new BooleanResult(false);
        }
    }
}
