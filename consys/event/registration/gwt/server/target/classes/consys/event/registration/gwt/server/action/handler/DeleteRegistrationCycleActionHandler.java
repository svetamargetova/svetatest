package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.exception.CycleNotEmptyException;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.DeleteRegistrationCycleAction;
import consys.event.registration.gwt.client.module.exception.RegistrationsInCycleException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DeleteRegistrationCycleActionHandler implements EventActionHandler<DeleteRegistrationCycleAction, VoidResult> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<DeleteRegistrationCycleAction> getActionType() {
        return DeleteRegistrationCycleAction.class;
    }

    @Override
    public VoidResult execute(DeleteRegistrationCycleAction action) throws ActionException {
        try {
            bundleService.deleteCycle(action.getUuid());
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (CycleNotEmptyException ex) {
            throw new RegistrationsInCycleException();
        } catch (NoRecordException ex) {
            throw new ServiceFailedException();
        }
        return VoidResult.RESULT();
    }    
}
