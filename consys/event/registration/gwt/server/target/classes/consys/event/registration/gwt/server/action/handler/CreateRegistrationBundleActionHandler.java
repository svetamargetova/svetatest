package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.CreateRegistrationBundleAction;
import consys.event.registration.gwt.client.bo.ClientCreateBundle;
import consys.event.registration.gwt.client.module.exception.BundleUniquityException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CreateRegistrationBundleActionHandler implements EventActionHandler<CreateRegistrationBundleAction, ArrayListResult<String>> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<CreateRegistrationBundleAction> getActionType() {
        return CreateRegistrationBundleAction.class;
    }

    @Override
    public ArrayListResult<String> execute(CreateRegistrationBundleAction action) throws ActionException {
        List<RegistrationBundle> bundles = Lists.newArrayListWithCapacity(action.getBundles().size());
        for (ClientCreateBundle bundle : action.getBundles()) {
            RegistrationBundle newBundle = new RegistrationBundle();
            newBundle.setTitle(bundle.getTitle());
            newBundle.setDescription(bundle.getDescription());
            bundles.add(newBundle);
        }

        try {
            if (action.isSubbundle()) {
                bundleService.createBundles(bundles, true);
            } else {
                bundleService.createBundles(bundles);
            }
        } catch (BundleNotUniqueException ex) {
            throw new BundleUniquityException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }


        ArrayList<String> out = Lists.newArrayList();
        for (RegistrationBundle bundle : bundles) {
            out.add(bundle.getUuid());
        }
        return new ArrayListResult<String>(out);
    }
}
