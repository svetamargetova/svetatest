package consys.event.registration.gwt.server.list;

import consys.common.gwt.server.utils.MonetaryUtils;
import consys.event.common.gwt.server.GwtListHandler;
import consys.event.registration.api.list.RegistrationsList;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.thumb.RegistrationListItem;
import consys.event.registration.gwt.client.bo.ClientListRegistrationItem;
import consys.event.registration.gwt.client.bo.ClientListRegistrationItem.ClientRegistrationState;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationsListHandler implements GwtListHandler<ClientListRegistrationItem, RegistrationListItem>, RegistrationsList {

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public void execute(List<RegistrationListItem> in, ArrayList<ClientListRegistrationItem> out) {
        for (RegistrationListItem item : in) {
            ClientListRegistrationItem r = new ClientListRegistrationItem();
            r.setBundleName(item.getBundleName());
            r.setPaid(MonetaryUtils.fromBigDecimal(item.getPaid()));
            r.setRegistrationDate(item.getRegistrationDate());
            r.setUserPosition(item.getUserPosition());
            r.setUserOrganization(item.getUserOrganization());
            r.setUserName(item.getUserName());
            r.setUserUuid(item.getUserUuid());
            r.setUuid(item.getRegistrationUuid());
            r.setQuantity(item.getQuantity());
            if (RegistrationState.WAITING.equals(item.getState())) {
                r.setState(ClientRegistrationState.WAITING);
            } else {
                r.setState(RegistrationState.ORDERED.equals(item.getState())
                        ? ClientRegistrationState.PREREGISTERED
                        : ClientRegistrationState.REGISTERED);
            }
            r.setUuidPortraitImagePrefix(item.getProfileImagePrefix());
            out.add(r);
        }
    }
}
