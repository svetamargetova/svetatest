package consys.event.registration.gwt.server.action.handler;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.action.ListBundlesAndQuestionsForUserAction;
import consys.event.registration.gwt.client.bo.ClientBundlesAndQuestions;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListBundlesAndQuestionsForUserActionHandler implements EventActionHandler<ListBundlesAndQuestionsForUserAction, ClientBundlesAndQuestions> {

    @Autowired
    private RegistrationBundleService bundleService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<ListBundlesAndQuestionsForUserAction> getActionType() {
        return ListBundlesAndQuestionsForUserAction.class;
    }

    @Override
    public ClientBundlesAndQuestions execute(ListBundlesAndQuestionsForUserAction action) throws ActionException {
        ClientBundlesAndQuestions result = new ClientBundlesAndQuestions();
        fillBundles(result);
        fillQuestions(result);
        return result;
    }

    private void fillBundles(ClientBundlesAndQuestions result) {
        try {
            List<RegistrationOption> options = bundleService.listMainRegistrationBundlesOptions();
            for (RegistrationOption option : options) {
                ClientRegistrationPack pack = new ClientRegistrationPack();
                pack.setBundleDescription(option.getDescription());
                pack.setBundleTitle(option.getTitle());
                pack.setCapacity(option.getCapacity());
                pack.setMaxQuantity(option.getMaxQuantity());
                pack.setUuidBundle(option.getBundleUuid());
                pack.setWithCode(option.isWithCode());
                pack.setRegistred(option.getRegistred());
                // nastaveni pro bundle ktere je proste uz mimo
                if (StringUtils.isNotBlank(option.getCycleUuid())) {
                    pack.setUuidCycle(option.getCycleUuid());
                    pack.setTo(option.getToDate());
                    pack.setPrice(MonetaryUtils.fromBigDecimal(option.getPrice()));
                    for (Map.Entry<Date, BigDecimal> entry : option.getPricing().entrySet()) {
                        pack.getPricing().put(entry.getKey(), MonetaryUtils.fromBigDecimal(entry.getValue()));
                    }
                }
                result.getBundles().add(pack);
            }
        } catch (NoRecordException ex) {
        }
    }

    private void fillQuestions(ClientBundlesAndQuestions result) {
        List<RegistrationCustomQuestionText> texts = qaService.listDetailedQuestions(SystemLocale.DEFAULT);
        for (RegistrationCustomQuestionText text : texts) {
            ClientCustomQuestion question = new ClientCustomQuestion();
            question.setRequired(text.getQuestionInfo().isRequired());
            question.setText(text.getQuestion());
            question.setUuid(text.getQuestionInfo().getUuid());
            result.getQuestions().add(question);
        }
    }
}
