package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.so.RegistrationOtherInfo;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.gwt.client.action.exception.EventNotActivatedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.CreateWaitingRecordAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import consys.payment.service.exception.EventNotActiveException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateWaitingRecordActionHandler implements EventActionHandler<CreateWaitingRecordAction, ClientRegistration> {

    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;
    @Autowired
    private ParticipantManagerService managerService;

    @Override
    public Class<CreateWaitingRecordAction> getActionType() {
        return CreateWaitingRecordAction.class;
    }

    @Override
    public ClientRegistration execute(CreateWaitingRecordAction action) throws ActionException {
        final boolean managedRegister = action.getManagedUserUuid() != null;
        if (managedRegister) {
            // zjistime jestli je uzivatel spravce uzivatele
            if (!managerService.checkManagedUser(action.getUserEventUuid(), action.getManagedUserUuid())) {
                throw new PermissionException();
            }
        }

        try {
            RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();

            String customerUuid = managedRegister ? action.getManagedUserUuid() : action.getUserEventUuid();
            UserRegistration registration = registrationService.createUserRegistrationToWaiting(customerUuid,
                    managedRegister, action.getProductUuid(), action.getQuantity(), otherInfo);
            ClientRegistration cr = UserRegistrationUtils.fromUserRegistration(registration, false);
            List<DetailedUserAnswer> answers = qaService.listUserAnswers(cr.getEventUser().getUuid());
            UserRegistrationUtils.addAnswersToClientRegistration(cr, answers);
            return cr;
        } catch (AlreadyRegistredException e) {
            throw new UserAlreadyRegistredException();
        } catch (EventNotActiveException e) {
            throw new EventNotActivatedException();
        } catch (NoRecordException e) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException e) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed e) {
            throw new ServiceFailedException();
        }
    }
}
