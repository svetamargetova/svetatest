package consys.event.registration.gwt.server.servlet;

import com.amazonaws.services.s3.model.S3Object;
import consys.common.constants.img.EventLogoImageEnum;
import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.bo.SystemProperty;
import consys.common.core.service.SystemPropertyService;
import consys.common.utils.collection.Lists;
import consys.event.badges.core.export.BadgeParticipantInfo;
import consys.event.badges.core.export.BadgeSizeType;
import consys.event.badges.core.export.PdfBadgesGenerator;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.impl.SystemPropertyServiceImpl;
import consys.event.registration.core.service.UserRegistrationService;
import java.awt.Image;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author palo
 */
public class BadgeExportServlet extends HttpServlet {

    private static final long serialVersionUID = -8985516539488705530L;
    private static final Logger logger = LoggerFactory.getLogger(BadgeExportServlet.class);
    // konstanty    
    private static final String CONTENT_TYPE = "application/pdf";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
    private static final String END_NAME = "\"";
    private static final String CACHE_CONTROL = "Cache-control";
    private static final String MAXAGE_1S = "maxage=1";
    private static final String FILENAME_PREFIX = "Participant Badges";
    private static final String PDF_SUFFIX = ".pdf";
    // servicy
    @Autowired
    private UserRegistrationService userRegistrationService;
    @Autowired
    private SystemPropertyService systemPropertyService;
    @Autowired
    private AwsFileStorageService fileStorageService;
    // data
    private String imageS3Bucket;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);

        imageS3Bucket = config.getInitParameter("Image.S3.Bucket");
        if (StringUtils.isBlank(imageS3Bucket)) {
            throw new IllegalArgumentException("Missing bucket AWS S3 image bucket name");
        }
        logger.info("{} initialized to: {}", this.getClass().getName(), imageS3Bucket);
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            List<UserEvent> users = userRegistrationService.listAllActiveParticiapntsForBadges();
            List<BadgeParticipantInfo> badges = Lists.newArrayListWithCapacity(users.size());
            //boolean position, organization;
            //String badgeInfo;
            for (UserEvent userEvent : users) {
                /*position = StringUtils.isNotBlank(userEvent.getPosition());
                 organization = StringUtils.isNotBlank(userEvent.getOrganization());
                 if (position && organization) {
                 badgeInfo = String.format("%s, %s", userEvent.getPosition(), userEvent.getOrganization());
                 } else if (position) {
                 badgeInfo = userEvent.getPosition();
                 } else if (organization) {
                 badgeInfo = userEvent.getOrganization();
                 } else {
                 badgeInfo = "";
                 }*/
                badges.add(new BadgeParticipantInfo(userEvent.getFullName(), userEvent.getOrganization()));
            }
            setResponseHeaders(response);

            List<String> keys = new ArrayList<String>();
            keys.add(CommonProperties.EVENT_ACRONYM);
            keys.add(CommonProperties.EVENT_IMAGE_UUID);
            keys.add(CommonProperties.EVENT_FROM);
            keys.add(CommonProperties.EVENT_TO);

            List<SystemProperty> properties = systemPropertyService.listProperties(keys);

            // nacteni systemovych property
            String acronym = "";
            Image logoImage = null;
            String from = null;
            String to = null;

            for (SystemProperty sp : properties) {
                if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_ACRONYM) == 0) {
                    acronym = sp.getValue() == null ? "" : sp.getValue();
                } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_IMAGE_UUID) == 0) {
                    if (StringUtils.isNotBlank(sp.getValue())) {
                        S3Object logo = fileStorageService.getObject(imageS3Bucket,
                                EventLogoImageEnum.ORIGINAL.getFullUuid(sp.getValue()));
                        if (logo != null) {
                            logoImage = ImageIO.read(logo.getObjectContent());
                            logo.getObjectContent().close();
                        }
                    }
                } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_FROM) == 0) {
                    from = sp.getValue() == null ? "" : sp.getValue();
                } else if (sp.getKey().compareToIgnoreCase(CommonProperties.EVENT_TO) == 0) {
                    to = sp.getValue() == null ? "" : sp.getValue();
                }
            }

            String date = parseDate(from, to);

            for (BadgeParticipantInfo b : badges) {
                // nastaveni acronymu a loga do badge
                b.setEventAcronym(acronym);
                b.setEventLogo(logoImage);
                b.setDate(date);
            }

            BadgeSizeType size;
            String paramSize = request.getParameter("size");

            if (StringUtils.isBlank(paramSize)) {
                size = BadgeSizeType.SIZE_90_X_50;
            } else {
                try {
                    size = BadgeSizeType.valueOf(paramSize.toUpperCase());
                } catch (IllegalArgumentException ex) {
                    // neznamy typ, budeme generovat ve vychozim rozmeru
                    size = BadgeSizeType.SIZE_90_X_50;
                }
            }

            PdfBadgesGenerator generator = new PdfBadgesGenerator();
            generator.createPdfBadges(response.getOutputStream(), badges, size, true);
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (Exception ex) {
            logger.error("Exception while generating badges: ", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getLocalizedMessage());
        }
    }

    /** vysklada datum v pozadovanem poradi */
    private static String parseDate(String from, String to) throws Exception {
        if (StringUtils.isBlank(from)) {
            return "";
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(SystemPropertyServiceImpl.ISO_DATE_FORMAT);

            if (StringUtils.isBlank(to)) {
                Date fromDate = sdf.parse(from);
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(fromDate);

                StringBuilder sb = new StringBuilder();
                sb.append(c.getDisplayName(GregorianCalendar.MONTH, GregorianCalendar.LONG, Locale.ENGLISH));
                sb.append(" ");
                sb.append(c.get(GregorianCalendar.DAY_OF_MONTH));
                sb.append(", ");
                sb.append(c.get(GregorianCalendar.YEAR));
                return sb.toString();
            } else {
                Date fromDate = sdf.parse(from);
                Date toDate = sdf.parse(to);

                GregorianCalendar cFrom = new GregorianCalendar();
                cFrom.setTime(fromDate);
                GregorianCalendar cTo = new GregorianCalendar();
                cTo.setTime(toDate);

                boolean sameYear = cFrom.get(GregorianCalendar.YEAR) == cTo.get(GregorianCalendar.YEAR);
                boolean sameMonth = cFrom.get(GregorianCalendar.MONTH) == cTo.get(GregorianCalendar.MONTH);
                boolean sameDay = cFrom.get(GregorianCalendar.DAY_OF_MONTH) == cTo.get(GregorianCalendar.DAY_OF_MONTH);

                StringBuilder sb = new StringBuilder();
                if (sameMonth) {
                    sb.append(cFrom.getDisplayName(GregorianCalendar.MONTH, GregorianCalendar.LONG, Locale.ENGLISH));
                    sb.append(" ");
                    if (sameDay) {
                        sb.append(cFrom.get(GregorianCalendar.DAY_OF_MONTH));
                    } else {
                        sb.append(cFrom.get(GregorianCalendar.DAY_OF_MONTH));
                        sb.append("\u2013");
                        sb.append(cTo.get(GregorianCalendar.DAY_OF_MONTH));
                    }
                } else {
                    sb.append(cFrom.getDisplayName(GregorianCalendar.MONTH, GregorianCalendar.LONG, Locale.ENGLISH));
                    sb.append(" ");
                    sb.append(cFrom.get(GregorianCalendar.DAY_OF_MONTH));
                    sb.append(" ");
                    sb.append("\u2013");
                    sb.append(" ");
                    sb.append(cTo.getDisplayName(GregorianCalendar.MONTH, GregorianCalendar.LONG, Locale.ENGLISH));
                    sb.append(" ");
                    sb.append(cTo.get(GregorianCalendar.DAY_OF_MONTH));
                }
                sb.append(", ");
                if (sameYear) {
                    sb.append(cFrom.get(GregorianCalendar.YEAR));
                } else {
                    sb.append(cFrom.get(GregorianCalendar.YEAR));
                    sb.append("\u2013");
                    sb.append(cTo.get(GregorianCalendar.YEAR));
                }
                return sb.toString();
            }
        } catch (ParseException ex) {
            throw new Exception("Can't parse date!", ex);
        }
    }

    private void setResponseHeaders(HttpServletResponse response) {
        StringBuilder attachment = new StringBuilder(ATTACHMENT_FILENAME);
        attachment.append(FILENAME_PREFIX);
        attachment.append(PDF_SUFFIX);
        attachment.append(END_NAME);

        response.setContentType(CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION, attachment.toString());
        response.setHeader(CACHE_CONTROL, MAXAGE_1S);
    }
}
