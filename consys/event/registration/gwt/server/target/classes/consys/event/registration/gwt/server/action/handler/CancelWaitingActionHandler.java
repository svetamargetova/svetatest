package consys.event.registration.gwt.server.action.handler;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.CancelWaitingAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CancelWaitingActionHandler implements EventActionHandler<CancelWaitingAction, VoidResult> {

    @Autowired
    private UserRegistrationService registrationService;

    @Override
    public Class<CancelWaitingAction> getActionType() {
        return CancelWaitingAction.class;
    }

    @Override
    public VoidResult execute(CancelWaitingAction action) throws ActionException {
        try {
            registrationService.updateCancelWaiting(action.getRegistrationUuid());
            return VoidResult.RESULT();
        } catch (Exception ex) {
            throw new ServiceFailedException();
        }
    }
}
