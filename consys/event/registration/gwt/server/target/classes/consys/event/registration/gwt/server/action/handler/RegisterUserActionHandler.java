package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.client.action.exception.EventNotActivatedException;
import consys.common.gwt.shared.exception.IbanException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.core.so.RegistrationOtherInfo;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.common.utils.collection.Lists;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.gwt.client.action.RegisterUserAction;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.common.gwt.shared.exception.CouponExpiredException;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.common.gwt.shared.exception.CouponUsedException;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.registration.core.bo.RegistrationCustomQuestion;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.ws.bo.Profile;
import java.util.List;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegisterUserActionHandler implements EventActionHandler<RegisterUserAction, ClientRegistration> {

    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;
    @Autowired
    private ParticipantManagerService managerService;

    @Override
    public Class<RegisterUserAction> getActionType() {
        return RegisterUserAction.class;
    }

    @Override
    public ClientRegistration execute(RegisterUserAction action) throws ActionException {
        final boolean managedRegister = action.getManagedUserUuid() != null;
        if (managedRegister) {
            // zjistime jestli je uzivatel spravce uzivatele
            if (!managerService.checkManagedUser(action.getUserEventUuid(), action.getManagedUserUuid())) {
                throw new PermissionException();
            }
        }

        final String customerUuid = managedRegister? action.getManagedUserUuid(): action.getUserEventUuid();
        
        try {
            // Volanie registracie
            UserRegistration registration;
            Profile profile = null;
            if (action.isProfileEdited()) {
                profile = ProfileUtils.toPortalProfile(action.getEditedProfile());
                profile.setUuid(action.getCustomerProfileUuid());
            }

            if (action.isAdditionalProducts()) {
                // Priprava dat
                List<BundleOrderRequest> subbundlesRequest = Lists.newArrayListWithCapacity(action.getProducts().size());
                for (ProductThumb product : action.getProducts()) {
                    subbundlesRequest.add(new BundleOrderRequest(product.getProductUuid(), product.getDiscountCode(), product.getQuantity()));
                }
                if (profile == null) {
                    registration = registrationService.addOrderToRegistration(customerUuid, subbundlesRequest,
                            action.getCustomerProfileUuid()).getUserRegistration();
                } else {
                    registration = registrationService.addOrderToRegistration(customerUuid, subbundlesRequest,
                            profile).getUserRegistration();
                }
            } else {
                // Priprava dat
                ProductThumb mainBundle = action.getProducts().remove(0);
                BundleOrderRequest mainBundleRequest = new BundleOrderRequest(mainBundle.getProductUuid(),
                        mainBundle.getDiscountCode(), mainBundle.getQuantity());
                List<BundleOrderRequest> subbundlesRequest = Lists.newArrayListWithCapacity(action.getProducts().size());
                for (ProductThumb product : action.getProducts()) {
                    subbundlesRequest.add(new BundleOrderRequest(product.getProductUuid(), product.getDiscountCode(),
                            product.getQuantity()));
                }

                // kontrola vstupnich dat odpovedi
                List<RegistrationCustomQuestion> questions = qaService.listQuestions();
                if (questions.size() > 0 && action.getAnswers() == null) {
                    throw new BadInputException();
                }
                for (RegistrationCustomQuestion question : questions) {
                    if (question.isRequired() && StringUtils.isBlank(action.getAnswers().get(question.getUuid()))) {
                        throw new BadInputException();
                    }
                }

                // vlozime jen naplnene odpovedi
                RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
                for (Entry<String, String> entry : action.getAnswers().entrySet()) {
                    if (StringUtils.isNotBlank(entry.getValue())) {
                        otherInfo.getAnswers().put(entry.getKey(), entry.getValue());
                    }
                }

                if (profile == null) {
                    registration = registrationService.createUserRegistration(
                            customerUuid,
                            managedRegister,
                            mainBundleRequest,
                            subbundlesRequest,
                            action.getCustomerProfileUuid(),
                            otherInfo);
                } else {
                    registration = registrationService.createUserRegistration(
                            customerUuid,
                            managedRegister,
                            mainBundleRequest,
                            subbundlesRequest,
                            profile,
                            otherInfo);
                }
            }
            ClientRegistration cr = UserRegistrationUtils.fromUserRegistration(registration, false);
            List<DetailedUserAnswer> answers = qaService.listUserAnswers(cr.getEventUser().getUuid());
            UserRegistrationUtils.addAnswersToClientRegistration(cr, answers);
            return cr;
        } catch (CouponCodeException ex) {
            // kupon neexistuje
            throw new CouponInvalidException();
        } catch (IbanFormatException ex) {
            throw new IbanException();
        } catch (EventNotActiveException ex) {
            throw new EventNotActivatedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (CouponOutOfDateException ex) {
            // vyprsel cas pouziti
            throw new CouponExpiredException();
        } catch (CouponCapacityException ex) {
            // kupon je uz pouzity a je nastaveny na one-time
            throw new CouponUsedException();
        } catch (NoRecordException ex) {
            // neexistuje bundle alebo podobne
            throw new NoRecordsForAction();
        } catch (AlreadyRegistredException ex) {
            throw new UserAlreadyRegistredException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (BundleCapacityFullException ex) {
            throw new BundleCapacityException();
        }
    }
}
