package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.LoadEditRegistrationByUserUuidAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadEditRegistrationByUserUuidActionHandler implements EventActionHandler<LoadEditRegistrationByUserUuidAction, ClientRegistration> {

    @Autowired
    public UserRegistrationService registrationService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<LoadEditRegistrationByUserUuidAction> getActionType() {
        return LoadEditRegistrationByUserUuidAction.class;
    }

    @Override
    public ClientRegistration execute(LoadEditRegistrationByUserUuidAction action) throws ActionException {
        try {
            UserRegistration userReg = registrationService.loadUserActiveRegistrationDetailedByUserUuid(action.getUserUuid());
            ClientRegistration cr = UserRegistrationUtils.fromUserRegistration(userReg, true);
            List<DetailedUserAnswer> answers = qaService.listUserAnswers(cr.getEventUser().getUuid());
            UserRegistrationUtils.addAnswersToClientRegistration(cr, answers);
            return cr;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
