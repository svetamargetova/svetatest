package consys.event.registration.gwt.server.action.utils;

import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.utils.collection.Lists;
import consys.common.utils.date.DateProvider;
import consys.common.utils.date.DateUtils;
import consys.event.common.gwt.server.action.handler.utils.EventUserUtils;
import consys.event.registration.core.bo.RegistrationOrder;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.UserBundleRegistration;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.gwt.client.bo.ClientAnswer;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrder;
import consys.event.registration.gwt.client.bo.ClientRegistrationOrderItem;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserRegistrationUtils {

    public static ClientRegistration fromUserRegistration(UserRegistration ur, boolean toAdministrator) {
        ClientRegistration reg = new ClientRegistration();
        reg.setEventUser(EventUserUtils.toEventUser(ur.getUser()));
        reg.setCeliac(ur.getUser().isCeliac());
        reg.setVegetarian(ur.getUser().isVegetarian());
        reg.setState(fromRegistrationState(ur.getState()));
        reg.setRegistrationUuid(ur.getUuid());
        if (toAdministrator) {
            reg.setNote(ur.getNote());
        }
        reg.setSpecialWish(ur.getSpecialWish());
        for (RegistrationOrder order : ur.getActiveOrders()) {
            ClientRegistrationOrder cOrder = new ClientRegistrationOrder();
            cOrder.setState(fromRegistrationState(order.getState()));
            cOrder.setDueDate(DateUtils.addDays(order.getOrderDate(), 14));
            cOrder.setOrderDate(order.getOrderDate());
            cOrder.setPaidDate(order.getPaidDate());
            cOrder.setTotalPrice(MonetaryUtils.fromBigDecimal(order.getOverallPrice()));
            cOrder.setUuid(order.getUuid());
            for (UserBundleRegistration ubr : order.getRegistredBundles()) {
                ClientRegistrationOrderItem item = new ClientRegistrationOrderItem();
                item.setBundleUuid(ubr.getRegistrationCycle().getBundle().getUuid());
                item.setDescription(ubr.getRegistrationCycle().getBundle().getDescription());
                item.setTitle(ubr.getRegistrationCycle().getBundle().getTitle());
                item.setDiscountCode(ubr.getRegistrationCoupon() != null ? ubr.getRegistrationCoupon().getKey() : null);
                item.setQuantity(ubr.getQuantity());
                cOrder.getOrderItems().add(item);
            }
            reg.getRegistrationOrders().add(cOrder);
        }
        return reg;
    }

    public static ClientRegistration.State fromRegistrationState(RegistrationState state) {
        if (state == null) {
            return null;
        }
        switch (state) {
            case APPROVE:
                return ClientRegistration.State.APPROVE;
            case ORDERED:
                return ClientRegistration.State.ORDERED;
            case TIMEOUTED:
                return ClientRegistration.State.TIMEOUTED;
            case CONFIRMED:
                return ClientRegistration.State.CONFIRMED;
            case WAITING:
                return ClientRegistration.State.WAITING;
            case CANCELED:
            default:
                return ClientRegistration.State.CANCELED;
        }
    }

    public static ArrayList<ClientRegistrationPack> fromRegistrationOption(List<RegistrationOption> options, boolean firstAsMain) {
        ArrayList<ClientRegistrationPack> out = Lists.newArrayListWithCapacity(options.size());
        Date now = DateProvider.getCurrentDate();
        for (int i = 0; i < options.size(); i++) {
            RegistrationOption registrationOption = options.get(i);
            if (registrationOption.getToDate() == null || now.after(registrationOption.getToDate()) || registrationOption.getPrice() == null) {
                // preskocime subbalicky, kterym vyprsel cyklus nebo ty co nemaji nastaveny cas konce
                // preskocime len ak su to subbalicky - ak je to hlavny cyklus tak to tam ide
                if (i != 0 || !firstAsMain) {
                    continue;
                }
            }
            ClientRegistrationPack pack = new ClientRegistrationPack();
            pack.setUuidBundle(registrationOption.getBundleUuid());
            pack.setUuidCycle(registrationOption.getCycleUuid());
            pack.setBundleTitle(registrationOption.getTitle());
            pack.setBundleDescription(registrationOption.getDescription());
            pack.setCapacity(registrationOption.getCapacity());
            pack.setPrice(MonetaryUtils.fromBigDecimal(registrationOption.getPrice()));
            pack.setRegistred(registrationOption.getRegistred());
            pack.setTo(registrationOption.getToDate());
            pack.setWithCode(registrationOption.isWithCode());
            pack.setMaxQuantity(registrationOption.getMaxQuantity());
            out.add(pack);
        }
        return out;
    }

    public static ArrayList<ClientRegistrationPack> fromRegistrationOption(List<RegistrationOption> options) {
        return fromRegistrationOption(options, false);
    }

    /** prida odo objektu registrace informace o uzivatelovych odpovedich */
    public static void addAnswersToClientRegistration(ClientRegistration cr, List<DetailedUserAnswer> answers) {
        for (DetailedUserAnswer answer : answers) {
            ClientAnswer ca = new ClientAnswer();
            ca.setAnswer(answer.getAnswer());
            ca.setOrder(answer.getOrder());
            ca.setQuestion(answer.getQuestion());
            cr.getAnswers().add(ca);
        }
    }
}
