package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.LoadRegistrationCycleAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadRegistrationCycleActionHandler implements EventActionHandler<LoadRegistrationCycleAction, ClientRegistrationCycle> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<LoadRegistrationCycleAction> getActionType() {
        return LoadRegistrationCycleAction.class;
    }

    @Override
    public ClientRegistrationCycle execute(LoadRegistrationCycleAction action) throws ActionException {
        try {
            RegistrationCycle cycle = bundleService.loadRegistrationCycle(action.getUuid());
            ClientRegistrationCycle crc = new ClientRegistrationCycle();
            crc.setFrom(cycle.getFrom());
            crc.setTo(cycle.getTo());
            crc.setUuid(cycle.getUuid());
            crc.setValue(Monetary.toMonetary(cycle.getPrice().toString()));

            return crc;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (NumberFormatException ex) {
            throw new ServiceFailedException();
        } catch (IllegalArgumentException ex) {
            throw new ServiceFailedException();
        }
    }
}
