package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCustomQuestion;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.action.CreateCustomQuestionAction;
import consys.event.registration.gwt.client.bo.ClientCreateQuestion;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateCustomQuestionActionHandler implements EventActionHandler<CreateCustomQuestionAction, ArrayListResult<String>> {

    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<CreateCustomQuestionAction> getActionType() {
        return CreateCustomQuestionAction.class;
    }

    @Override
    public ArrayListResult<String> execute(CreateCustomQuestionAction action) throws ActionException {
        int questionsCount = qaService.loadQuestionsCount().intValue();
        int nextOrder = questionsCount + 1;

        // kontrola jestli se nechce pridavat vic polozek nez je povoleno (mame omezeno na max 5)
        if (questionsCount + action.getQuestions().size() > 5) {
            throw new ServiceFailedException("Over 5 question items");
        }

        try {
            ArrayListResult<String> result = new ArrayListResult<String>();
            for (ClientCreateQuestion q : action.getQuestions()) {
                RegistrationCustomQuestion question = qaService.createQuestion(q.isRequired(), nextOrder, q.getName());
                result.getArrayListResult().add(question.getUuid());
                nextOrder++;
            }
            return result;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
