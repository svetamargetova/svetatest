package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.ListBundlePriceSummaryAction;
import consys.event.registration.gwt.client.bo.ClientBundlePriceSummary;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListBundlePriceSummaryActionHandler implements EventActionHandler<ListBundlePriceSummaryAction, ArrayListResult<ClientBundlePriceSummary>> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<ListBundlePriceSummaryAction> getActionType() {
        return ListBundlePriceSummaryAction.class;
    }

    @Override
    public ArrayListResult<ClientBundlePriceSummary> execute(ListBundlePriceSummaryAction action) throws ActionException {
        try {
            List<RegistrationCycle> cycles = bundleService.listNextCyclesAfter(action.getBundleUuid());
            ArrayList<ClientBundlePriceSummary> out = Lists.newArrayList();
            for(RegistrationCycle cycle : cycles){
                ClientBundlePriceSummary s = new ClientBundlePriceSummary(cycle.getTo(), MonetaryUtils.fromBigDecimal(cycle.getPrice()));
                out.add(s);
            }
            return new ArrayListResult<ClientBundlePriceSummary>(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }    
}
