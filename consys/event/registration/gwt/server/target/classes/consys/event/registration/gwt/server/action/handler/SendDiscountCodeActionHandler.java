package consys.event.registration.gwt.server.action.handler;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.SendDiscountCodeAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class SendDiscountCodeActionHandler implements EventActionHandler<SendDiscountCodeAction, VoidResult>{
    

    @Autowired
    private UserRegistrationService registrationService;

    @Override
    public Class<SendDiscountCodeAction> getActionType() {
        return SendDiscountCodeAction.class;
    }

    @Override
    public VoidResult execute(SendDiscountCodeAction action) throws ActionException {
        throw new UnsupportedOperationException("Operacie nie je implementovana");
    }    
}
