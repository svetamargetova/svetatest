package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.DuplicateRegistrationBundleAction;
import consys.event.registration.gwt.client.module.exception.BundleUniquityException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DuplicateRegistrationBundleActionHandler implements EventActionHandler<DuplicateRegistrationBundleAction, ClientTabItem> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<DuplicateRegistrationBundleAction> getActionType() {
        return DuplicateRegistrationBundleAction.class;
    }

    @Override
    public ClientTabItem execute(DuplicateRegistrationBundleAction action) throws ActionException {
        try {
            RegistrationBundle bundle = bundleService.createDuplicate(action.getUuid(), action.getTitle());
            return new ClientTabItem(bundle.getUuid(), bundle.getTitle());
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (BundleNotUniqueException ex) {
            throw new BundleUniquityException();
        } catch (NoRecordException ex) {
            throw new ServiceFailedException();
        }
    }
}
