package consys.event.registration.gwt.server.list.export;

import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.common.gwt.server.list.export.xls.AbstractXlsExportHandler;
import consys.event.registration.api.list.CheckInList;
import consys.event.registration.core.bo.thumb.CheckInListItem;
import consys.event.registration.core.service.CheckInListService;
import java.util.Date;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CheckInListExportHandler extends AbstractXlsExportHandler {

    private static final String[] allTitles = {"Name", "Checked", "Checkin Date", "Position", "Organization", "Email"};
    private static final String[] checkedTitles = {"Name", "Checked", "Checkin Date", "Position", "Organization", "Email"};
    private static final String[] notCheckedTitles = {"Name", "Position", "Organization", "Email"};
    @Autowired
    private CheckInListService listService;

    public CheckInListExportHandler() {
        super(CheckInList.LIST_TAG);
    }

    @Override
    public void buildExport(HSSFWorkbook workbook, Constraints c) {
        Paging p = getDefaultPaging();

        // all header row
        HSSFSheet all = workbook.createSheet("All");
        createHead(all, allTitles);
        int allRows = 1;

        // checked
        HSSFSheet checked = workbook.createSheet("Checked");
        createHead(checked, checkedTitles);
        int checkedRows = 1;

        // not checked
        HSSFSheet notChecked = workbook.createSheet("Not checked");
        createHead(notChecked, notCheckedTitles);
        int notCheckedRows = 1;

        List<CheckInListItem> items = null;

        do {
            if (items != null) {
                nextPaging(p);
            }
            items = listService.listItems(c, p);
            for (CheckInListItem item : items) {

                // all
                Row row = all.createRow(allRows++);
                addRow(row, item, true);

                if (item.getCheckInDate() == null) {
                    // not checked
                    row = notChecked.createRow(notCheckedRows++);
                    addRow(row, item, false);
                } else {
                    // checked
                    row = checked.createRow(checkedRows++);
                    addRow(row, item, false);
                }
            }
        } while (hasNext(p, items));

        // autosize sloupcu
        autosize(all, allTitles.length);
        autosize(checked, checkedTitles.length);
        autosize(notChecked, notCheckedTitles.length);
    }

    /** vytvori hlavicku v sheetu */
    private void createHead(HSSFSheet sheet, String[] titles) {
        Row headerRow = sheet.createRow(0);
        headerRow.setHeightInPoints(12f);
        for (int i = 0; i < titles.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(titles[i]);
            cell.setCellStyle(style.getHeaderStyle());
        }
    }

    /** pokud je zadano all nezohlednuje se hodnota v checked */
    private void addRow(Row row, CheckInListItem item, boolean all) {
        int counter = 0;

        setTextCell(row, counter++, string(item.getUserName()));
        if (all) {
            setTextCell(row, counter++, string(item.getQuantityChecked() + " / " + item.getMaxQuantity()));
        }
        if (!all && item.getCheckInDate() != null) {
            setTextCell(row, counter++, string(item.getQuantityChecked() + " / " + item.getMaxQuantity()));
        }
        if (all || item.getCheckInDate() != null) {
            setDate(row, counter++, item.getCheckInDate());
        }
        setTextCell(row, counter++, string(item.getUserPosition()));
        setTextCell(row, counter++, string(item.getUserOrganization()));
        setTextCell(row, counter++, string(item.getEmail()));
    }

    /** prida radek s datumem nebo prazdny pokud je datum null */
    private void setDate(Row row, int rowNumber, Date date) {
        if (date != null) {
            setDateCell(row, rowNumber, date);
        } else {
            setTextCell(row, rowNumber, "");
        }
    }

    private void autosize(HSSFSheet sheet, int columns) {
        for (int i = 0; i < columns; i++) {
            sheet.autoSizeColumn(i);
        }
    }
}
