package consys.event.registration.gwt.server.list;

import consys.event.common.gwt.server.GwtListHandler;
import consys.event.registration.api.list.InternalRegistrationListApi;
import consys.event.registration.core.bo.thumb.InternalRegistrationListItem;
import consys.event.registration.gwt.client.bo.ClientInternalRegistrationListItem;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class InternalRegistrationListHandler implements GwtListHandler<ClientInternalRegistrationListItem, InternalRegistrationListItem>, InternalRegistrationListApi {

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public void execute(List<InternalRegistrationListItem> in, ArrayList<ClientInternalRegistrationListItem> out) {
        for (InternalRegistrationListItem i : in) {
            ClientInternalRegistrationListItem item = new ClientInternalRegistrationListItem();
            item.setBundleName(i.getBundleName());
            item.setQuantity(i.getQuantity());
            item.setRegistrationUuid(i.getRegistrationUuid());
            item.setUserName(i.getUserName());
            item.setUserOrganization(i.getUserOrganization());
            item.setUserPosition(i.getUserPosition());
            item.setUuid(i.getUserUuid());
            out.add(item);
        }
    }
}
