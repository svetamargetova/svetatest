package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.exception.CyclesOverlapsException;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.UpdateRegistrationCycleAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import consys.event.registration.gwt.client.module.exception.CyclesIntersectionException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateRegistrationCycleActionHandler implements EventActionHandler<UpdateRegistrationCycleAction, VoidResult> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<UpdateRegistrationCycleAction> getActionType() {
        return UpdateRegistrationCycleAction.class;
    }

    @Override
    public VoidResult execute(UpdateRegistrationCycleAction action) throws ActionException {
        ClientRegistrationCycle crc = action.getCycle();
        try {
            RegistrationCycle cycle = new RegistrationCycle();
            cycle.setUuid(crc.getUuid());
            cycle.setFrom(crc.getFrom());
            cycle.setTo(crc.getTo());
            cycle.setPrice(MonetaryUtils.fromMonetary(crc.getValue()));
            bundleService.updateRegistrationCycle(cycle);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (CyclesOverlapsException ex) {
            throw new CyclesIntersectionException();
        } catch (NoRecordException ex) {
            throw new ServiceFailedException();
        }
        return VoidResult.RESULT();
    }
}
