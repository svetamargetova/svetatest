package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.ConfirmOrderAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ConfirmOrderActionHandler implements EventActionHandler<ConfirmOrderAction, VoidResult> {

    @Autowired
    private UserRegistrationService registrationService;
    
    @Override
    public Class<ConfirmOrderAction> getActionType() {
        return ConfirmOrderAction.class;
    }

    @Override
    public VoidResult execute(ConfirmOrderAction action) throws ActionException {
        try {
            registrationService.updateConfirmRegistrationOrderFromOrganizator(action.getOrderUuid(), action.getUserEventUuid(), action.getNote(), action.isNotifyUser());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        }
    }
}
