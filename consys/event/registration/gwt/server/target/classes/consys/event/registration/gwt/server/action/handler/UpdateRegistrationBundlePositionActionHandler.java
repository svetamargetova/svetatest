package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.UpdateRegistrationBundlePositionAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateRegistrationBundlePositionActionHandler implements EventActionHandler<UpdateRegistrationBundlePositionAction, VoidResult> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<UpdateRegistrationBundlePositionAction> getActionType() {
        return UpdateRegistrationBundlePositionAction.class;
    }

    @Override
    public VoidResult execute(UpdateRegistrationBundlePositionAction action) throws ActionException {
        try {
            bundleService.updateRegistrationBundleOrder(action.getUuid(), action.getOldPosition(), action.getNewPosition(), action.isSubbundle());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
