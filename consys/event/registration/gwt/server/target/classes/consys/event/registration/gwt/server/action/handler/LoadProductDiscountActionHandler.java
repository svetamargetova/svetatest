package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.event.registration.gwt.client.action.LoadProductDiscountAction;
import consys.common.gwt.shared.bo.Monetary;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.common.gwt.shared.exception.CouponUsedException;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadProductDiscountActionHandler implements EventActionHandler<LoadProductDiscountAction,Monetary>{

    
    @Autowired
    private UserRegistrationService userRegistrationService;

    @Override
    public Class<LoadProductDiscountAction> getActionType() {
        return LoadProductDiscountAction.class;
    }

    @Override
    public Monetary execute(LoadProductDiscountAction action) throws ActionException {
        try {
            BigDecimal discount = userRegistrationService.loadDiscount(action.getProductUuid(), action.getDiscountCode());
            return MonetaryUtils.fromBigDecimal(discount);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (CouponCapacityException ex) {
            throw new CouponUsedException();
        } catch (CouponCodeException ex) {
            throw new CouponInvalidException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
           throw new BadInputException();
        }

    }

}
