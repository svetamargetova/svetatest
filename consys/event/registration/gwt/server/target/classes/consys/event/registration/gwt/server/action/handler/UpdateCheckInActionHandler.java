package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.DateResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.exception.RegistrationNotActiveException;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.UpdateCheckInAction;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateCheckInActionHandler implements EventActionHandler<UpdateCheckInAction, DateResult> {

    private static final Logger logger = LoggerFactory.getLogger(UpdateCheckInActionHandler.class);
    @Autowired
    private UserRegistrationService registrationService;

    @Override
    public Class<UpdateCheckInAction> getActionType() {
        return UpdateCheckInAction.class;
    }

    @Override
    public DateResult execute(UpdateCheckInAction action) throws ActionException {
        try {
            Date out = registrationService.updateRegistrationCheckIn(action.getRegistrationUuid(),
                    action.isChecked(), action.getCount());
            return new DateResult(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (RegistrationNotActiveException ex) {
            throw new ServiceFailedException();
        }
    }
}
