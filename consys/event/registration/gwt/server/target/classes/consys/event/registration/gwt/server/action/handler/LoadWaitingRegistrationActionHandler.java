package consys.event.registration.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.LoadWaitingRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadWaitingRegistrationActionHandler implements EventActionHandler<LoadWaitingRegistrationAction, ClientRegistration> {

    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<LoadWaitingRegistrationAction> getActionType() {
        return LoadWaitingRegistrationAction.class;
    }

    @Override
    public ClientRegistration execute(LoadWaitingRegistrationAction action) throws ActionException {
        try {
            UserRegistration registration = registrationService.loadWaitingRegistration(action.getRegistrationUuid());
            ClientRegistration cr = UserRegistrationUtils.fromUserRegistration(registration, false);
            List<DetailedUserAnswer> answers = qaService.listUserAnswers(cr.getEventUser().getUuid());
            UserRegistrationUtils.addAnswersToClientRegistration(cr, answers);
            return cr;
        } catch (Exception ex) {
            throw new ServiceFailedException();
        }
    }
}
