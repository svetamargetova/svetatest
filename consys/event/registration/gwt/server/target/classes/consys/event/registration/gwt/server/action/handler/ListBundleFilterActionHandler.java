package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.utils.collection.Lists;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.ListBundleFilterAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListBundleFilterActionHandler implements EventActionHandler<ListBundleFilterAction,ArrayListResult<CommonThumb> > {

    @Autowired
    private RegistrationBundleService registrationBundleService;

    @Override
    public Class<ListBundleFilterAction> getActionType() {
        return ListBundleFilterAction.class;
    }

    @Override
    public ArrayListResult<CommonThumb> execute(ListBundleFilterAction action) throws ActionException {
        try {

            List<RegistrationBundle> bundles = registrationBundleService.listAllBundles();
            ArrayList<CommonThumb> out = Lists.newArrayList();
            for(RegistrationBundle bundle : bundles){
                out.add(new CommonThumb(bundle.getUuid(), bundle.getTitle()));
            }
            return new ArrayListResult<CommonThumb>(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
   
}
