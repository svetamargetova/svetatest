package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.GenerateDiscountKeyAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class GenerateDiscountKeyActionHandler implements EventActionHandler<GenerateDiscountKeyAction, StringResult> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<GenerateDiscountKeyAction> getActionType() {
        return GenerateDiscountKeyAction.class;
    }

    @Override
    public StringResult execute(GenerateDiscountKeyAction action) throws ActionException {
        try {
            StringResult r;
            if (StringUtils.isNotBlank(action.getKey()) && bundleService.isValidRegistrationCouponKey(action.getBundleUuid(), action.getKey())) {
                r = new StringResult(action.getKey());
            } else {
                r = new StringResult(bundleService.generateRegistrationCouponKey(action.getBundleUuid()));
            }

            return r;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
