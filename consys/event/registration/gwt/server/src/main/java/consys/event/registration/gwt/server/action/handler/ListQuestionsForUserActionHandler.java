package consys.event.registration.gwt.server.action.handler;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.action.ListQuestionsForUserAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import consys.event.registration.gwt.client.bo.ClientQuestions;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListQuestionsForUserActionHandler implements EventActionHandler<ListQuestionsForUserAction, ClientQuestions> {

    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<ListQuestionsForUserAction> getActionType() {
        return ListQuestionsForUserAction.class;
    }

    @Override
    public ClientQuestions execute(ListQuestionsForUserAction action) throws ActionException {
        List<RegistrationCustomQuestionText> texts = qaService.listDetailedQuestions(SystemLocale.DEFAULT);
        ClientQuestions questions = new ClientQuestions();

        for (RegistrationCustomQuestionText text : texts) {
            ClientCustomQuestion question = new ClientCustomQuestion();
            question.setRequired(text.getQuestionInfo().isRequired());
            question.setText(text.getQuestion());
            question.setUuid(text.getQuestionInfo().getUuid());
            questions.getQuestions().add(question);
        }

        return questions;
    }
}
