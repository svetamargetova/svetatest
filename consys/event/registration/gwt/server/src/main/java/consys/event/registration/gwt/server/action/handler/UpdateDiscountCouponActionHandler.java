package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.UpdateDiscountCouponAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateDiscountCouponActionHandler implements EventActionHandler<UpdateDiscountCouponAction, VoidResult> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<UpdateDiscountCouponAction> getActionType() {
        return UpdateDiscountCouponAction.class;
    }

    @Override
    public VoidResult execute(UpdateDiscountCouponAction action) throws ActionException {
        try {
            bundleService.updateRegistrationCoupon(action.getUuid(), action.getCapacity(), action.getEndDate());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
