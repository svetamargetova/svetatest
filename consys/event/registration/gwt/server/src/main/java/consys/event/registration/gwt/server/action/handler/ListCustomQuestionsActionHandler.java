package consys.event.registration.gwt.server.action.handler;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.CommonThumb;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.action.ListCustomQuestionsAction;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListCustomQuestionsActionHandler implements EventActionHandler<ListCustomQuestionsAction, ArrayListResult<CommonThumb>> {

    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<ListCustomQuestionsAction> getActionType() {
        return ListCustomQuestionsAction.class;
    }

    @Override
    public ArrayListResult<CommonThumb> execute(ListCustomQuestionsAction action) throws ActionException {
        List<RegistrationCustomQuestionText> texts = qaService.listDetailedQuestions(SystemLocale.DEFAULT);
        ArrayListResult<CommonThumb> thumbs = new ArrayListResult<CommonThumb>();

        for (RegistrationCustomQuestionText text : texts) {
            thumbs.getArrayListResult().add(new CommonThumb(text.getQuestionInfo().getUuid(), text.getQuestion()));
        }

        return thumbs;
    }
}
