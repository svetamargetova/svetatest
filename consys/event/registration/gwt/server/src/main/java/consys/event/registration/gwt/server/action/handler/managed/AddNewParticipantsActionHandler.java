package consys.event.registration.gwt.server.action.handler.managed;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.gwt.client.action.managed.AddNewParticipantsAction;
import consys.event.registration.gwt.client.bo.managed.NewParticipant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class AddNewParticipantsActionHandler implements EventActionHandler<AddNewParticipantsAction, VoidResult> {

    @Autowired
    private ParticipantManagerService pmService;

    @Override
    public Class<AddNewParticipantsAction> getActionType() {
        return AddNewParticipantsAction.class;
    }

    @Override
    public VoidResult execute(AddNewParticipantsAction action) throws ActionException {
        String managerUuid = action.getUserEventUuid();

        try {
            for (NewParticipant np : action.getParticipants()) {
                if (StringUtils.isBlank(np.getFirstName()) || StringUtils.isBlank(np.getLastName())
                        || StringUtils.isBlank(np.getOrganization())) {
                    throw new BadInputException();
                }

                UserEvent ue = new UserEvent();
                ue.setFullName(np.getFirstName() + " " + np.getLastName());
                ue.setLastName(np.getLastName());
                ue.setOrganization(np.getOrganization());

                pmService.createNewManagedUser(managerUuid, ue);
            }
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        }

        return VoidResult.RESULT();
    }
}
