package consys.event.registration.gwt.server.list;

import consys.common.gwt.shared.bo.EventUser;
import consys.event.common.core.bo.thumb.UserEventThumb;
import consys.event.common.gwt.server.GwtListHandler;
import consys.event.registration.api.list.RegistrationCouponList;
import consys.event.registration.core.bo.thumb.RegistrationCouponListItem;
import consys.event.registration.gwt.client.bo.ClientCouponListItem;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationCouponListHandler implements GwtListHandler<ClientCouponListItem, RegistrationCouponListItem>, RegistrationCouponList {

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public void execute(List<RegistrationCouponListItem> in, ArrayList<ClientCouponListItem> out) {
        for (RegistrationCouponListItem item : in) {
            ClientCouponListItem p = new ClientCouponListItem();
            p.setBundleUuid(item.getBundleUuid());
            p.setCapacity(item.getCapacity());
            p.setUuid(item.getCouponUuid());
            p.setDiscount(item.getDiscount());
            p.setEndDate(item.getEndDate());
            p.setKey(item.getKey());
            p.setUsedCount(item.getUsedCount());
            for (UserEventThumb i : item.getUsers()) {
                EventUser user = new EventUser();
                user.setFullName(i.getName());
                user.setPosition(i.getPosition());
                user.setOrganization(i.getOrganization());
                user.setUuid(i.getUuid());
                user.setImageUuidPrefix(i.getImageProfileUuidPrefix());
                p.getUsers().add(user);
            }
            out.add(p);
        }
    }
}
