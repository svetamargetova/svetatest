package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientTabItem;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.ListRegistrationSettingsAction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListRegistrationSettingsActionHandler implements EventActionHandler<ListRegistrationSettingsAction, ArrayListResult<ClientTabItem>> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<ListRegistrationSettingsAction> getActionType() {
        return ListRegistrationSettingsAction.class;
    }

    @Override
    public ArrayListResult<ClientTabItem> execute(ListRegistrationSettingsAction action) throws ActionException {
        try {
            List<RegistrationBundle> list;
            if (action.isAll()) {
                // Todo: Optimalizovat
                list = bundleService.listAllBundles();
            } else {
                if (action.isSubbundle()) {
                    list = bundleService.listRegistrationSubBundles();
                } else {
                    list = bundleService.listRegistrationMainBundles();
                }
            }
            ArrayList<ClientTabItem> out = Lists.newArrayListWithCapacity(list.size());
            for (RegistrationBundle b : list) {
                out.add(new ClientTabItem(b.getUuid(), b.getTitle()));
            }

            return new ArrayListResult<ClientTabItem>(out);
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
