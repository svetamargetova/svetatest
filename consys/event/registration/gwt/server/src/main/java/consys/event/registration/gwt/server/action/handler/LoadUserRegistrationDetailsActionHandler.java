package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.LoadUserRegistrationDetailsAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadUserRegistrationDetailsActionHandler implements EventActionHandler<LoadUserRegistrationDetailsAction, ArrayListResult<ClientRegistrationPack>> {

    @Autowired
    private RegistrationBundleService registrationBundleService;

    @Override
    public Class<LoadUserRegistrationDetailsAction> getActionType() {
        return LoadUserRegistrationDetailsAction.class;
    }

    @Override
    public ArrayListResult<ClientRegistrationPack> execute(LoadUserRegistrationDetailsAction action) throws ActionException {
        try {
            List<RegistrationOption> subbundles = registrationBundleService.listRegistrationSubBundlesOptions(action.getBundleUuid());            
            return new ArrayListResult<ClientRegistrationPack>(UserRegistrationUtils.fromRegistrationOption(subbundles));
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

}
