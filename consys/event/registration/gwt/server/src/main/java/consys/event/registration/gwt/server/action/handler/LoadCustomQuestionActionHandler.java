package consys.event.registration.gwt.server.action.handler;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.action.LoadCustomQuestionAction;
import consys.event.registration.gwt.client.bo.ClientCustomQuestion;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadCustomQuestionActionHandler implements EventActionHandler<LoadCustomQuestionAction, ClientCustomQuestion> {

    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<LoadCustomQuestionAction> getActionType() {
        return LoadCustomQuestionAction.class;
    }

    @Override
    public ClientCustomQuestion execute(LoadCustomQuestionAction action) throws ActionException {
        try {
            RegistrationCustomQuestionText questionText = qaService.loadDetaileQuestion(action.getQuestionUuid(), SystemLocale.DEFAULT);

            ClientCustomQuestion question = new ClientCustomQuestion();
            question.setRequired(questionText.getQuestionInfo().isRequired());
            question.setText(questionText.getQuestion());
            question.setUuid(questionText.getQuestionInfo().getUuid());
            return question;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
