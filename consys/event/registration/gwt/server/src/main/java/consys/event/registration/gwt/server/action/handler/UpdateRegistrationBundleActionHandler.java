package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.common.core.exception.BundleCapacityFullException;
import consys.event.registration.core.exception.BundleNotUniqueException;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.UpdateRegistrationBundleAction;
import consys.event.registration.gwt.client.bo.ClientBundle;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.event.registration.gwt.client.module.exception.BundleUniquityException;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
 
/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateRegistrationBundleActionHandler implements EventActionHandler<UpdateRegistrationBundleAction, VoidResult> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<UpdateRegistrationBundleAction> getActionType() {
        return UpdateRegistrationBundleAction.class;
    }

    @Override
    public VoidResult execute(UpdateRegistrationBundleAction action) throws ActionException {

        ClientBundle cb = action.getBundle();
        try {
            RegistrationBundle bundle = bundleService.loadRegistrationBundle(cb.getUuid());
            bundle.setCapacity(cb.getCapacity());
            bundle.setQuantity(cb.getQuantity());
            bundle.setDescription(cb.getDescription());
            bundle.setTitle(cb.getTitle());            
            bundle.setVat(BigDecimal.valueOf(cb.getVat()));
            bundle.setB2b(cb.isB2BTicket());
            // cykle
            bundleService.updateRegistrationBundle(bundle);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (BundleNotUniqueException ex) {
            throw new BundleUniquityException();
        } catch (BundleCapacityFullException ex) {
            throw new BundleCapacityException();
        } catch (NoRecordException ex) {
            throw new ServiceFailedException();
        }



        return VoidResult.RESULT();
    }    
}
