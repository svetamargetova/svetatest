package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCycle;
import consys.event.registration.core.exception.CyclesOverlapsException;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.CreateRegistrationCycleAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationCycle;
import consys.event.registration.gwt.client.module.exception.CyclesIntersectionException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CreateRegistrationCycleActionHandler implements EventActionHandler<CreateRegistrationCycleAction, ArrayListResult<String>> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<CreateRegistrationCycleAction> getActionType() {
        return CreateRegistrationCycleAction.class;
    }

    @Override
    public ArrayListResult<String> execute(CreateRegistrationCycleAction action) throws ActionException {

        List<RegistrationCycle> newCycles = Lists.newArrayList();
        String bundle = action.getBundleUuid();
        for (ClientRegistrationCycle c : action.getCycles()) {
            RegistrationCycle cycle = new RegistrationCycle();
            cycle.setFrom(c.getFrom());
            cycle.setTo(c.getTo());
            cycle.setPrice(new BigDecimal(c.getValue().toString()));
            newCycles.add(cycle);
        }
        try {
            bundleService.createRegistrationCycles(bundle, newCycles);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (CyclesOverlapsException ex) {
            throw new CyclesIntersectionException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        }
        ArrayList<String> out = Lists.newArrayList();
        for (RegistrationCycle cycle : newCycles) {
            out.add(cycle.getUuid());
        }
        return new ArrayListResult<String>(out);
    }   
}
