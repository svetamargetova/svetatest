package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.UpdateRegistrationAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateRegistrationActionHandler implements EventActionHandler<UpdateRegistrationAction, VoidResult> {

    @Autowired
    private UserRegistrationService userRegistrationService;


    @Override
    public Class<UpdateRegistrationAction> getActionType() {
        return UpdateRegistrationAction.class;
    }

    @Override
    public VoidResult execute(UpdateRegistrationAction action) throws ActionException {
        try {
            userRegistrationService.updateRegistrationNote(action.getRegistrationUuid(), action.getNote(),action.isAdmin());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }   
}
