package consys.event.registration.gwt.server.action.handler;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.LoadOuterRegisterAction;
import consys.event.registration.gwt.client.bo.ClientOuterRegisterData;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadOuterRegisterActionHandler implements EventActionHandler<LoadOuterRegisterAction, ClientOuterRegisterData> {

    
    @Autowired
    private RegistrationBundleService bundleService;
    @Autowired
    private SystemPropertyService propertyService;
    
    
    @Override
    public Class<LoadOuterRegisterAction> getActionType() {
        return LoadOuterRegisterAction.class;
    }

    @Override
    public ClientOuterRegisterData execute(LoadOuterRegisterAction action) throws ActionException {

        try {
            ClientOuterRegisterData out = new ClientOuterRegisterData();            
            List<RegistrationOption> options = bundleService.listRegistrationSubBundlesWithMainBundle(action.getTicketUuid());
            out.setItems(UserRegistrationUtils.fromRegistrationOption(options,true));            
            SystemProperty sp =propertyService.loadSystemProperty(CommonProperties.EVENT_CURRENCY);            
            out.setCurrency(sp.getValue());                       
            return out;           
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }   
}
