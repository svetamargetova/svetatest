package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.RegistrationCoupon;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.CreateDiscountCouponAction;
import consys.common.gwt.shared.exception.CouponInvalidException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CreateDiscountCouponActionHandler implements EventActionHandler<CreateDiscountCouponAction, VoidResult>{

    @Autowired
    private RegistrationBundleService bundleService;
    
    @Override
    public Class<CreateDiscountCouponAction> getActionType() {
        return CreateDiscountCouponAction.class;
    }

    @Override
    public VoidResult execute(CreateDiscountCouponAction action) throws ActionException {
        try {
            RegistrationCoupon coupon = new RegistrationCoupon();
            coupon.setCapacity(action.getCapacity());
            coupon.setDiscount(action.getDiscount());
            coupon.setEndDate(action.getEndDate());        
            coupon.setKey(action.getKey());
            bundleService.createRegistrationCoupon(action.getBundleUuid(), coupon);
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (CouponCodeException ex) {
            throw new CouponInvalidException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }                
    }

    
}
