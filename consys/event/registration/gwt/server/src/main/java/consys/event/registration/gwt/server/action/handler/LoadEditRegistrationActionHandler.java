package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.LoadEditRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientAnswer;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadEditRegistrationActionHandler implements EventActionHandler<LoadEditRegistrationAction, ClientRegistration> {

    @Autowired
    private UserRegistrationService userRegistrationService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<LoadEditRegistrationAction> getActionType() {
        return LoadEditRegistrationAction.class;
    }

    @Override
    public ClientRegistration execute(LoadEditRegistrationAction action) throws ActionException {
        try {
            UserRegistration reg = userRegistrationService.loadRegistrationDetailed(action.getRegistrationUuid());
            ClientRegistration cr = UserRegistrationUtils.fromUserRegistration(reg, true);
            List<DetailedUserAnswer> answers = qaService.listUserAnswers(cr.getEventUser().getUuid());
            UserRegistrationUtils.addAnswersToClientRegistration(cr, answers);
            return cr;
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
