package consys.event.registration.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.utils.enums.CountryEnum;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.event.registration.gwt.client.action.LoadOrderPaymentProfileAction;
import consys.payment.webservice.ProfileWebService;
import consys.payment.ws.profile.LoadOrderProfileRequest;
import consys.payment.ws.profile.LoadOrderProfileResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadOrderPaymentProfileActionHandler implements EventActionHandler<LoadOrderPaymentProfileAction, ClientUserPaymentProfile> {

    @Autowired
    private ProfileWebService profileWebService;

    @Override
    public Class<LoadOrderPaymentProfileAction> getActionType() {
        return LoadOrderPaymentProfileAction.class;
    }

    @Override
    public ClientUserPaymentProfile execute(LoadOrderPaymentProfileAction action) throws ActionException {
        LoadOrderProfileRequest request = ProfileWebService.OBJECT_FACTORY.createLoadOrderProfileRequest();
        request.setOrderUuid(action.getOrderUuid());
        LoadOrderProfileResponse response = profileWebService.loadOrderProfile(request);
        ClientUserPaymentProfile profile = ProfileUtils.toClientUserProfile(response.getProfile());
        profile.setCountry2ch(CountryEnum.fromId(profile.getCountry()).getCode());
        return profile;
    }
}
