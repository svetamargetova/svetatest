package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.CancelRegistrationAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CancelRegistrationActionHandler implements EventActionHandler<CancelRegistrationAction, VoidResult>{

    @Autowired
    private UserRegistrationService registrationService;

    @Override
    public Class<CancelRegistrationAction> getActionType() {
        return CancelRegistrationAction.class;
    }

    @Override
    public VoidResult execute(CancelRegistrationAction action) throws ActionException {
        try {
             if(action.isSourceOwner()){
                registrationService.updateCancelRegistrationOrderFromOwner(action.getRegistrationOrderUuid());
            }else{
                registrationService.updateCancelRegistrationOrderFromOrganizator(action.getRegistrationOrderUuid());
            }       
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        }
    }
   
}
