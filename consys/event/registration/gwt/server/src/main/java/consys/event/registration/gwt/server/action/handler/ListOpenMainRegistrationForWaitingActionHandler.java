package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.ListOpenMainRegistrationForWaitingAction;
import consys.event.registration.gwt.client.bo.ClientWaitingBundleInfo;
import consys.event.registration.gwt.client.bo.ClientWaitingBundlesInfo;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListOpenMainRegistrationForWaitingActionHandler implements EventActionHandler<ListOpenMainRegistrationForWaitingAction, ClientWaitingBundlesInfo> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<ListOpenMainRegistrationForWaitingAction> getActionType() {
        return ListOpenMainRegistrationForWaitingAction.class;
    }

    @Override
    public ClientWaitingBundlesInfo execute(ListOpenMainRegistrationForWaitingAction action) throws ActionException {
        try {
            List<RegistrationOption> options = bundleService.listMainRegistrationBundlesOptions();
            ClientWaitingBundlesInfo result = new ClientWaitingBundlesInfo();

            Date now = new Date();

            for (RegistrationOption o : options) {
                if (o.getToDate() != null && now.before(o.getToDate())) {
                    ClientWaitingBundleInfo info = new ClientWaitingBundleInfo();
                    info.setBundleUuid(o.getBundleUuid());
                    info.setBundleName(o.getTitle());
                    info.setFilledCapacity(o.getRegistred());
                    info.setFullCapacity(o.getCapacity());
                    result.getInfos().add(info);
                }
            }

            return result;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
