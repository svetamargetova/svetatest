package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.so.RegistrationOtherInfo;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.common.gwt.shared.exception.CouponExpiredException;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.common.gwt.shared.exception.CouponUsedException;
import consys.common.gwt.shared.exception.IbanException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.gwt.client.action.exception.EventNotActivatedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.event.registration.core.bo.RegistrationCustomQuestion;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.CreateInternalRegistrationAction;
import consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.ws.bo.Profile;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateInternalRegistrationActionHandler implements EventActionHandler<CreateInternalRegistrationAction, VoidResult> {

    @Autowired
    private UserRegistrationService userRegistrationService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<CreateInternalRegistrationAction> getActionType() {
        return CreateInternalRegistrationAction.class;
    }

    @Override
    public VoidResult execute(CreateInternalRegistrationAction action) throws ActionException {
        try {
            UserEvent userEvent = preprareNewUser(action);

            ProductThumb mainBundle = action.getProducts().remove(0);
            BundleOrderRequest mainBundleRequest = new BundleOrderRequest(mainBundle.getProductUuid(), null, mainBundle.getQuantity());

            List<BundleOrderRequest> subbundlesRequest = Lists.newArrayListWithCapacity(action.getProducts().size());
            for (ProductThumb product : action.getProducts()) {
                subbundlesRequest.add(new BundleOrderRequest(product.getProductUuid(), null, product.getQuantity()));
            }

            // kontrola vstupnich dat odpovedi
            List<RegistrationCustomQuestion> questions = qaService.listQuestions();
            if (questions.size() > 0 && (action.getAnswers() == null || action.getAnswers().isEmpty())) {
                throw new BadInputException();
            }
            for (RegistrationCustomQuestion question : questions) {
                if (question.isRequired() && StringUtils.isBlank(action.getAnswers().get(question.getUuid()))) {
                    throw new BadInputException();
                }
            }

            Profile profile = ProfileUtils.toPortalProfile(action.getProfile());

            // vlozime jen naplnene odpovedi
            RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
            for (Map.Entry<String, String> entry : action.getAnswers().entrySet()) {
                if (StringUtils.isNotBlank(entry.getValue())) {
                    otherInfo.getAnswers().put(entry.getKey(), entry.getValue());
                }
            }

            userRegistrationService.createInternalRegistration(action.getUserEventUuid(), userEvent,
                    mainBundleRequest, subbundlesRequest, profile, otherInfo);

            return VoidResult.RESULT();
        } catch (CouponCodeException ex) {
            // kupon neexistuje
            throw new CouponInvalidException();
        } catch (IbanFormatException ex) {
            throw new IbanException();
        } catch (EventNotActiveException ex) {
            throw new EventNotActivatedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (CouponOutOfDateException ex) {
            // vyprsel cas pouziti
            throw new CouponExpiredException();
        } catch (CouponCapacityException ex) {
            // kupon je uz pouzity a je nastaveny na one-time
            throw new CouponUsedException();
        } catch (NoRecordException ex) {
            // neexistuje bundle alebo podobne
            throw new NoRecordsForAction();
        } catch (AlreadyRegistredException ex) {
            throw new UserAlreadyRegistredException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (BundleCapacityFullException ex) {
            throw new BundleCapacityException();
        }
    }

    private UserEvent preprareNewUser(CreateInternalRegistrationAction action) {
        UserEvent ue = new UserEvent();
        ue.setCeliac(false);
        ue.setEmail(action.getUserEmail());
        ue.setFullName(action.getFirstName() + " " + action.getLastName());
        ue.setLastName(action.getLastName());
        ue.setOrganization(action.getOrganization());
        ue.setInternalUser(true);
        return ue;
    }
}
