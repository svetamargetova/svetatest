package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.bo.thumb.DetailedUserAnswer;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.LoadUserRegistrationAction;
import consys.event.registration.gwt.client.bo.ClientRegistration;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.client.bo.ClientRegistrationResult;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com> 
 */
public class LoadUserRegistrationActionHandler implements EventActionHandler<LoadUserRegistrationAction, ClientRegistrationResult> {

    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private RegistrationBundleService bundleService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;
    @Autowired
    private ParticipantManagerService managerService;

    @Override
    public Class<LoadUserRegistrationAction> getActionType() {
        return LoadUserRegistrationAction.class;
    }

    @Override
    public ClientRegistrationResult execute(LoadUserRegistrationAction action) throws ActionException {
        final boolean managedRegistration = action.getManagedUserUuid() != null;
        if (managedRegistration) {
            // zjistime jestli je uzivatel spravce uzivatele
            if (!managerService.checkManagedUser(action.getUserEventUuid(), action.getManagedUserUuid())) {
                throw new PermissionException();
            }
        }

        ClientRegistrationResult cr = new ClientRegistrationResult();
        try {

            String customerUuid = managedRegistration ? action.getManagedUserUuid() : action.getUserEventUuid();
            // Nacitani registracie
            UserRegistration userReg = registrationService.loadUserActiveRegistrationDetailedByUserUuid(customerUuid);
            ClientRegistration clr = UserRegistrationUtils.fromUserRegistration(userReg, false);
            List<DetailedUserAnswer> answers = qaService.listUserAnswers(clr.getEventUser().getUuid());
            UserRegistrationUtils.addAnswersToClientRegistration(clr, answers);
            cr.setRegistration(clr);
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            try {
                // nie je zaregistrovany.. nacitame bundle
                List<RegistrationOption> options = bundleService.listMainRegistrationBundlesOptions();
                for (RegistrationOption option : options) {
                    ClientRegistrationPack pack = new ClientRegistrationPack();
                    pack.setBundleDescription(option.getDescription());
                    pack.setBundleTitle(option.getTitle());
                    pack.setCapacity(option.getCapacity());
                    pack.setMaxQuantity(option.getMaxQuantity());
                    pack.setUuidBundle(option.getBundleUuid());
                    pack.setWithCode(option.isWithCode());
                    pack.setRegistred(option.getRegistred());
                    // nastaveni pro bundle ktere je proste uz mimo
                    if (StringUtils.isNotBlank(option.getCycleUuid())) {
                        pack.setUuidCycle(option.getCycleUuid());
                        pack.setTo(option.getToDate());
                        pack.setPrice(MonetaryUtils.fromBigDecimal(option.getPrice()));
                        for (Entry<Date, BigDecimal> entry : option.getPricing().entrySet()) {
                            pack.getPricing().put(entry.getKey(), MonetaryUtils.fromBigDecimal(entry.getValue()));
                        }
                    }
                    cr.getBundles().add(pack);
                }
            } catch (NoRecordException ex1) {
                throw new NoRecordsForAction();
            }
        }
        return cr;
    }
}
