package consys.event.registration.gwt.server.servlet;

import consys.common.core.bo.SystemProperty;
import consys.common.core.service.SystemPropertyService;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.TicketData;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.export.PdfTicketGenerator;
import consys.event.registration.core.export.utils.TicketDataConverter;
import consys.event.registration.core.service.UserRegistrationService;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author pepa
 */
public class DownloadTicketServlet extends HttpServlet {

    private static final long serialVersionUID = -8985516539488705530L;
    private static final Logger logger = LoggerFactory.getLogger(DownloadTicketServlet.class);
    // konstanty
    private static final String REGISTRATION_PARAM = "registration";
    private static final String CONTENT_TYPE = "application/pdf";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
    private static final String END_NAME = "\"";
    private static final String CACHE_CONTROL = "Cache-control";
    private static final String MAXAGE_1S = "maxage=1";
    private static final String FILENAME_PREFIX = "Event ticket - ";
    private static final String PDF_SUFFIX = ".pdf";
    // servicy
    @Autowired
    private UserRegistrationService userRegistrationService;
    @Autowired
    private SystemPropertyService systemPropertyService;    
    @Autowired
    private TicketDataConverter converter;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);               
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String registrationUuid = request.getParameter(REGISTRATION_PARAM);
            if (StringUtils.isBlank(registrationUuid)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing registration identificator");
                return;
            }

            UserRegistration ur = userRegistrationService.loadRegistrationDetailed(registrationUuid);
            if (!ur.getState().equals(RegistrationState.CONFIRMED)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Registration is not confirmed");
                return;
            }
            List<SystemProperty> properties = systemPropertyService.listAllProperties();

            TicketData data = converter.createTicketData(ur, properties);
            if (data == null) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing important properties!");
                return;
            }

            setResponseHeaders(data.getEventName(), response);

            PdfTicketGenerator generator = new PdfTicketGenerator();
            generator.createPdfTicket(response.getOutputStream(), data, true);

            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (Exception ex) {
            logger.error("Exception while generating ticket: ",ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getLocalizedMessage());
        }
    }
   
    private void setResponseHeaders(String eventName, HttpServletResponse response) {
        StringBuilder attachment = new StringBuilder(ATTACHMENT_FILENAME);
        attachment.append(FILENAME_PREFIX);
        attachment.append(eventName);
        attachment.append(PDF_SUFFIX);
        attachment.append(END_NAME);

        response.setContentType(CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION, attachment.toString());
        response.setHeader(CACHE_CONTROL, MAXAGE_1S);
    }
}
