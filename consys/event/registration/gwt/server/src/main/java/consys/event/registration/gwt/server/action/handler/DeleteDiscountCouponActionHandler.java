package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.AlreadyUsedException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.AlreadyUsedObjectException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.DeleteDiscountCouponAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class DeleteDiscountCouponActionHandler implements EventActionHandler<DeleteDiscountCouponAction, VoidResult> {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public Class<DeleteDiscountCouponAction> getActionType() {
        return DeleteDiscountCouponAction.class;
    }

    @Override
    public VoidResult execute(DeleteDiscountCouponAction action) throws ActionException {
        try {
            bundleService.deleteRegistrationCoupon(action.getUuid());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (AlreadyUsedException ex) {
            throw new AlreadyUsedObjectException();
        }
    }
}
