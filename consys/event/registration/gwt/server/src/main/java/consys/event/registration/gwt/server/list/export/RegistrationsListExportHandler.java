package consys.event.registration.gwt.server.list.export;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.ListService;
import consys.event.common.gwt.server.list.export.xls.AbstractTabsXlsExportHandler;
import consys.event.common.gwt.server.list.export.xls.SheetInfo;
import consys.event.registration.api.list.RegistrationsList;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.RegistrationCustomQuestionText;
import consys.event.registration.core.bo.RegistrationState;
import consys.event.registration.core.bo.thumb.RegistrationListExportItem;
import consys.event.registration.core.bo.thumb.RegistrationListItem;
import consys.event.registration.core.bo.thumb.UserAnswer;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.core.service.RegistrationsListService;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class RegistrationsListExportHandler extends AbstractTabsXlsExportHandler<RegistrationListItem, RegistrationListExportItem> {

    // konstanty
    private static final String[] TITLES = {"Registration date", "State", "Price", "User name", "Position", "Organization", "Is celiac", "Is vegetarian", "Special wish"};
    private static final String[] ALL_TITLES = {"Registration date", "State", "Price", "Ticket name", "Is ticket accessory", "User name", "Position", "Organization", "Is celiac", "Is vegetarian", "Special wish", "Answer 1", "Answer 2", "Answer 3", "Answer 4", "Answer 5"};
    private static final String[] CUSTOM_TITLES = {"Registration date", "State", "Price", "Ticket name", "User name", "Position", "Organization", "Email"};
    //private static final String[] CUSTOM_CANCELED_TITLES = {"Registration date", "Price", "Ticket name", "User name", "Position", "Organization", "Email"};
    // servicy
    @Autowired
    private RegistrationsListService listService;
    @Autowired
    private RegistrationBundleService bundleService;
    @Autowired
    private RegistrationQuestionAnswerService qaService;
    // data
    private DecimalFormat decimalFormat;
    private HSSFSheet customSheet;
    //private HSSFSheet customCanceledSheet;
    private int customSheetRowCounter;
    //private int customCanceledSheetRowCounter;
    private List<RegistrationCustomQuestionText> questions;
    private Map<String, List<UserAnswer>> userAnswers;

    public RegistrationsListExportHandler() {
        super(RegistrationsList.LIST_TAG, true);
        decimalFormat = new DecimalFormat("###,###,##0.00");
    }

    @Override
    public void buildExport(HSSFWorkbook workbook, Constraints c) {
        questions = qaService.listDetailedQuestions(SystemLocale.DEFAULT);
        userAnswers = qaService.listExportUsersAnswers();
        super.buildExport(workbook, c);
    }

    @Override
    protected void createTabsWithHeaders(HSSFWorkbook workbook, Map<String, SheetInfo> sheets) {
        List<RegistrationBundle> bundles = loadBundles();
        if (bundles == null) {
            return;
        }

        for (RegistrationBundle bundle : bundles) {
            HSSFSheet sheet = workbook.createSheet(sheetName(bundle.getTitle()));
            createHeader(TITLES, sheet);
            sheets.put(bundle.getTitle(), new SheetInfo(sheet));
        }
    }

    @Override
    protected void createDataRow(RegistrationListExportItem item, Row row, boolean allSheet) {
        int counter = 0;

        List<UserAnswer> answers = userAnswers.get(item.getUserUuid());

        setDateCell(row, counter++, item.getRegistrationDate());
        setTextCell(row, counter++, string(item.getState().name()));
        setTextCell(row, counter++, string(decimalFormat.format(item.getPaid())));
        if (allSheet) {
            setTextCell(row, counter++, string(item.getBundleName()));
            setTextCell(row, counter++, string(item.isSubbundle() ? "yes" : "no"));
        }
        setTextCell(row, counter++, string(item.getUserName()));
        setTextCell(row, counter++, string(item.getUserPosition()));
        setTextCell(row, counter++, string(item.getUserOrganization()));
        setTextCell(row, counter++, string(item.isCeliac() ? "yes" : "no"));
        setTextCell(row, counter++, string(item.isVegetarian() ? "yes" : "no"));
        setTextCell(row, counter++, string(item.getSpecialWish()));

        if (allSheet) {
            if (answers != null) {
                Collections.sort(answers);
                int nextOrder = 2;
                for (UserAnswer answer : answers) {
                    if (nextOrder != answer.getOrder() + 1) {
                        for (int i = nextOrder; i < answer.getOrder(); i++) {
                            setTextCell(row, counter++, string("-"));
                        }
                    }
                    setTextCell(row, counter++, string(answer.getAnswer()));
                    nextOrder = answer.getOrder() + 1;
                }
                for (int i = nextOrder; i < 6; i++) {
                    setTextCell(row, counter++, string("-"));
                }
            } else {
                for (int i = 0; i < 5; i++) {
                    setTextCell(row, counter++, string("-"));

                }
            }
        }
    }

    @Override
    protected ListService<RegistrationListItem, RegistrationListExportItem> listService() {
        return listService;
    }

    @Override
    protected String getSheetKey(RegistrationListExportItem item) {
        return item.getBundleName();
    }

    @Override
    protected String[] allSheetTitles() {
        String[] allTitlesWithQuestions = new String[16];
        System.arraycopy(ALL_TITLES, 0, allTitlesWithQuestions, 0, 11);
        int column = 11;
        for (RegistrationCustomQuestionText q : questions) {
            allTitlesWithQuestions[column] = ALL_TITLES[column] + ": " + q.getQuestion();
            column++;
        }
        System.arraycopy(ALL_TITLES, column, allTitlesWithQuestions, column, 16 - column);
        return allTitlesWithQuestions;
    }

    @Override
    protected int[] autosizeColumnIndexes() {
        return new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8};
    }

    @Override
    protected int[] allTabAutosizeColumnIndexes() {
        return new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    }

    private List<RegistrationBundle> loadBundles() {
        try {
            return bundleService.listAllBundles();
        } catch (NoRecordException ex) {
            logger.info("None bundles for discount coupon list export");
            return null;
        }
    }

    @Override
    protected void createCustomSheetsHeaders(HSSFWorkbook workbook) {
        customSheet = workbook.createSheet(" Participant registration ");
        createHeader(CUSTOM_TITLES, customSheet);
        customSheetRowCounter = 1;

        /*customCanceledSheet = workbook.createSheet(" Canceled participants ");
        createHeader(CUSTOM_CANCELED_TITLES, customCanceledSheet);
        customCanceledSheetRowCounter = 1;*/
    }

    @Override
    protected void createCustomSheetsDataRows(RegistrationListExportItem item) {
        /*if (RegistrationState.CANCELED.equals(item.getState())) {
            int counter = 0;

            Row row = customCanceledSheet.createRow(customCanceledSheetRowCounter++);
            setDateCell(row, counter++, item.getRegistrationDate());
            setTextCell(row, counter++, string(decimalFormat.format(item.getPaid())));
            setTextCell(row, counter++, string(item.getBundleName()));
            setTextCell(row, counter++, string(item.getUserName()));
            setTextCell(row, counter++, string(item.getUserPosition()));
            setTextCell(row, counter++, string(item.getUserOrganization()));
            setTextCell(row, counter++, string(item.getEmail()));
        }*/

        boolean process;
        switch (item.getState()) {
            case APPROVE:
            case CONFIRMED:
            case ORDERED:
            case WAITING:
                process = true;
                break;
            default:
                process = false;
                break;
        }

        if (process && !item.isSubbundle()) {
            int counter = 0;

            Row row = customSheet.createRow(customSheetRowCounter++);
            setDateCell(row, counter++, item.getRegistrationDate());
            setTextCell(row, counter++, string(item.getState().name()));
            setTextCell(row, counter++, string(decimalFormat.format(item.getPaid())));
            setTextCell(row, counter++, string(item.getBundleName()));
            setTextCell(row, counter++, string(item.getUserName()));
            setTextCell(row, counter++, string(item.getUserPosition()));
            setTextCell(row, counter++, string(item.getUserOrganization()));
            setTextCell(row, counter++, string(item.getEmail()));
        }
    }

    @Override
    protected void autosizeCustomSheets() {
        autosizeColumns(customSheet, new int[]{0, 1, 2, 3, 4, 5, 6, 7});
        //autosizeColumns(customCanceledSheet, new int[]{0, 1, 2, 3, 4, 5, 6});
    }
}
