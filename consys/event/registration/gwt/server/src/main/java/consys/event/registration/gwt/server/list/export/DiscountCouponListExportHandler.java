package consys.event.registration.gwt.server.list.export;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Maps;
import consys.event.common.core.bo.thumb.UserEventThumb;
import consys.event.common.core.service.list.ListService;
import consys.event.common.gwt.server.list.export.xls.AbstractTabsXlsExportHandler;
import consys.event.common.gwt.server.list.export.xls.SheetInfo;
import consys.event.registration.api.list.RegistrationCouponList;
import consys.event.registration.core.bo.RegistrationBundle;
import consys.event.registration.core.bo.thumb.RegistrationCouponListItem;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.core.service.RegistrationCouponListService;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class DiscountCouponListExportHandler extends AbstractTabsXlsExportHandler<RegistrationCouponListItem, RegistrationCouponListItem> {

    // konstanty
    private static final String[] TITLES = {"Used", "Discount", "Key", "End date", "Used", "Capacity", "Used by"};
    private static final String[] ALL_TITLES = {"Used", "Discount", "Key", "Ticket name", "End date", "Used", "Capacity", "Used by"};
    // servicy
    @Autowired
    private RegistrationCouponListService listService;
    @Autowired
    private RegistrationBundleService bundleService;
    // data
    private Map<String, String> bundlesMap;

    public DiscountCouponListExportHandler() {
        super(RegistrationCouponList.LIST_TAG);
    }

    @Override
    protected void createTabsWithHeaders(HSSFWorkbook workbook, Map<String, SheetInfo> sheets) {
        List<RegistrationBundle> bundles = loadBundles();
        if (bundles == null) {
            return;
        }

        bundlesMap = Maps.newHashMap();

        for (RegistrationBundle bundle : bundles) {
            HSSFSheet sheet = workbook.createSheet(sheetName(bundle.getTitle()));
            createHeader(TITLES, sheet);
            sheets.put(bundle.getUuid(), new SheetInfo(sheet));
            bundlesMap.put(bundle.getUuid(), bundle.getTitle());
        }
    }

    @Override
    protected void createDataRow(RegistrationCouponListItem item, Row row, boolean allSheet) {
        List<UserEventThumb> users = item.getUsers();
        StringBuilder sb = new StringBuilder();

        if (!users.isEmpty()) {
            sb.append(users.get(0).getName());
            for (int i = 1; i < users.size(); i++) {
                sb.append(", ");
                sb.append(users.get(i).getName());
            }
        }

        int counter = 0;

        setTextCell(row, counter++, item.getUsedCount() == 0 ? "no" : "yes");
        setTextCell(row, counter++, string(item.getDiscount() + " %"));
        setTextCell(row, counter++, string(item.getKey()));
        if (allSheet) {
            setTextCell(row, counter++, string(bundlesMap.get(item.getBundleUuid())));
        }
        if (item.getEndDate() != null) {
            setDateCell(row, counter++, item.getEndDate());
        } else {
            setTextCell(row, counter++, "unlimited");
        }
        setTextCell(row, counter++, String.valueOf(item.getUsedCount()));
        setTextCell(row, counter++, item.getCapacity() == 0 ? "unlimited" : String.valueOf(item.getCapacity()));
        setTextCell(row, counter++, sb.toString());
    }

    @Override
    protected ListService<RegistrationCouponListItem, RegistrationCouponListItem> listService() {
        return listService;
    }

    @Override
    protected String getSheetKey(RegistrationCouponListItem item) {
        return item.getBundleUuid();
    }

    @Override
    protected String[] allSheetTitles() {
        return ALL_TITLES;
    }

    @Override
    protected int[] autosizeColumnIndexes() {
        return new int[]{0, 1, 2, 3, 6};
    }

    @Override
    protected int[] allTabAutosizeColumnIndexes() {
        return new int[]{0, 1, 2, 3, 4, 7};
    }

    private List<RegistrationBundle> loadBundles() {
        try {
            return bundleService.listAllBundles();
        } catch (NoRecordException ex) {
            logger.info("None bundles for discount coupon list export");
            return null;
        }
    }
}
