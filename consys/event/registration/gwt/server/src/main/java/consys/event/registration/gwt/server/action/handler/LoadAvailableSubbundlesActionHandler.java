package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.gwt.client.action.LoadAvailableSubbundlesAction;
import consys.event.registration.gwt.client.bo.ClientRegistrationPack;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadAvailableSubbundlesActionHandler implements EventActionHandler<LoadAvailableSubbundlesAction, ArrayListResult<ClientRegistrationPack>> {

    @Autowired
    private RegistrationBundleService registrationBundleService;
    @Autowired
    private ParticipantManagerService managerService;

    @Override
    public Class<LoadAvailableSubbundlesAction> getActionType() {
        return LoadAvailableSubbundlesAction.class;
    }

    @Override
    public ArrayListResult<ClientRegistrationPack> execute(LoadAvailableSubbundlesAction action) throws ActionException {
        final boolean managedRegister = action.getManagedUserUuid() != null;
        if (managedRegister) {
            // zjistime jestli je uzivatel spravce uzivatele
            if (!managerService.checkManagedUser(action.getUserEventUuid(), action.getManagedUserUuid())) {
                throw new PermissionException();
            }
        }

        try {
            String customerUuid = action.getUserEventUuid();
            List<RegistrationOption> options = registrationBundleService.listRegistrationSubBundlesOptionsForUser(customerUuid);
            return new ArrayListResult<ClientRegistrationPack>(UserRegistrationUtils.fromRegistrationOption(options));
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
