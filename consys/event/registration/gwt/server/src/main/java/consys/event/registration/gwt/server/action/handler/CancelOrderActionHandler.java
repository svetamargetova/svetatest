package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.CancelOrderAction;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CancelOrderActionHandler implements EventActionHandler<CancelOrderAction, VoidResult>{
    private static final Logger logger = LoggerFactory.getLogger(CancelOrderActionHandler.class);

    @Autowired
    private UserRegistrationService registrationService;


    @Override
    public Class<CancelOrderAction> getActionType() {
        return CancelOrderAction.class;
    }

    @Override
    public VoidResult execute(CancelOrderAction action) throws ActionException {
        try {
            if(action.isSourceOwner()){
                registrationService.updateCancelRegistrationOrderFromOwner(action.getOrderUuid());
            }else{
                registrationService.updateCancelRegistrationOrderFromOrganizator(action.getOrderUuid());
            }            
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        }
    }




}
