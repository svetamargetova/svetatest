package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.gwt.shared.bo.ClientPaymentOrderData;
import consys.common.gwt.shared.bo.ClientPaymentProduct;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.bo.thumb.ReservedRegistrationOrder;
import consys.event.registration.core.exception.AlreadyRegistredException;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.bo.ProductThumb;
import consys.common.utils.collection.Lists;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.core.bo.thumb.ReservedUserBundleRegistration;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.LoadRegisterUserPaymentDialogAction;
import consys.common.gwt.shared.exception.BundleCapacityException;
import consys.common.gwt.shared.exception.CouponExpiredException;
import consys.common.gwt.shared.exception.CouponInvalidException;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.registration.gwt.client.module.exception.UserAlreadyRegistredException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadRegisterUserPaymentDialogActionHandler implements EventActionHandler<LoadRegisterUserPaymentDialogAction, ClientPaymentOrderData> {

    // logger
    private Logger log = LoggerFactory.getLogger(LoadRegisterUserPaymentDialogActionHandler.class);
    // sluzby
    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private ParticipantManagerService managerService;

    @Override
    public Class<LoadRegisterUserPaymentDialogAction> getActionType() {
        return LoadRegisterUserPaymentDialogAction.class;
    }

    @Override
    public ClientPaymentOrderData execute(LoadRegisterUserPaymentDialogAction action) throws ActionException {
        final boolean managedRegister = action.getManagedUserUuid() != null;
        if (managedRegister) {
            // zjistime jestli je uzivatel spravce uzivatele
            if (!managerService.checkManagedUser(action.getUserEventUuid(), action.getManagedUserUuid())) {
                throw new PermissionException();
            }
        }

        try {
            List<BundleOrderRequest> orders = Lists.newArrayListWithCapacity(action.getProducts().size());
            for (ProductThumb commonThumb : action.getProducts()) {
                BundleOrderRequest r = new BundleOrderRequest(commonThumb.getProductUuid(),
                        commonThumb.getDiscountCode(), commonThumb.getQuantity());
                orders.add(r);
                log.debug(r.toString());
            }

            String customerUuid = managedRegister ? action.getManagedUserUuid() : action.getUserEventUuid();
            ReservedRegistrationOrder reg = registrationService.createRegistrationOrderReservation(customerUuid, orders);
            ClientPaymentOrderData out = new ClientPaymentOrderData();
            out.setTotalPrice(MonetaryUtils.fromBigDecimal(reg.getTotal()));
            for (ReservedUserBundleRegistration r : reg.getRegistrations()) {
                ClientPaymentProduct p = new ClientPaymentProduct();
                p.setName(r.getBundle().getTitle());
                p.setPrice(MonetaryUtils.fromBigDecimal(r.getTotal()));
                p.setQuantity(r.getQuantity());
                out.getProducts().add(p);
                log.debug(p.toString());
            }
            return out;
        } catch (CouponOutOfDateException ex) {
            throw new CouponExpiredException();
        } catch (CouponCodeException ex) {
            throw new CouponInvalidException();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (NoRecordException ex) {
            // kupon neexistuje
            throw new NoRecordsForAction();
        } catch (AlreadyRegistredException ex) {
            throw new UserAlreadyRegistredException();
        } catch (BundleCapacityFullException ex) {
            throw new BundleCapacityException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
