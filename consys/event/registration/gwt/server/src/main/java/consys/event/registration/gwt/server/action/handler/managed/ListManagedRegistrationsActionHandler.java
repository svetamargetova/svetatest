package consys.event.registration.gwt.server.action.handler.managed;

import consys.common.gwt.shared.action.ActionException;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.EventUserUtils;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.service.UserRegistrationService;
import consys.event.registration.gwt.client.action.managed.ListManagedRegistrationsAction;
import consys.event.registration.gwt.client.bo.managed.ClientManagedRegistrations;
import consys.event.registration.gwt.server.action.utils.UserRegistrationUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ListManagedRegistrationsActionHandler implements EventActionHandler<ListManagedRegistrationsAction, ClientManagedRegistrations> {

    @Autowired
    private UserRegistrationService registrationService;
    @Autowired
    private ParticipantManagerService pmService;

    @Override
    public Class<ListManagedRegistrationsAction> getActionType() {
        return ListManagedRegistrationsAction.class;
    }

    @Override
    public ClientManagedRegistrations execute(ListManagedRegistrationsAction action) throws ActionException {
        ClientManagedRegistrations result = new ClientManagedRegistrations();

        List<UserEvent> participants = pmService.listManagedParticipants(action.getUserEventUuid());
        for (UserEvent ue : participants) {
            result.getParticipants().add(EventUserUtils.toEventUser(ue));
        }

        List<UserRegistration> registrations = registrationService.listManagedRegistrations(action.getUserEventUuid());
        for (UserRegistration ur : registrations) {
            result.getRegistrations().add(UserRegistrationUtils.fromUserRegistration(ur, true));
        }

        return result;
    }
}
