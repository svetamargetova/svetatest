package consys.event.registration.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.action.UpdateCustomQuestionOrderAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateCustomQuestionOrderActionHandler implements EventActionHandler<UpdateCustomQuestionOrderAction, VoidResult> {

    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<UpdateCustomQuestionOrderAction> getActionType() {
        return UpdateCustomQuestionOrderAction.class;
    }

    @Override
    public VoidResult execute(UpdateCustomQuestionOrderAction action) throws ActionException {
        try {
            qaService.updateQuestionOrder(action.getQuestionUuid(), action.getOldPosition(), action.getNewPosition());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
