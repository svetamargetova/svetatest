package consys.event.registration.gwt.server.action.handler;

import consys.common.core.bo.enums.SystemLocale;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.registration.core.service.RegistrationQuestionAnswerService;
import consys.event.registration.gwt.client.action.UpdateCustomQuestionAction;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateCustomQuestionActionHandler implements EventActionHandler<UpdateCustomQuestionAction, VoidResult> {

    @Autowired
    private RegistrationQuestionAnswerService qaService;

    @Override
    public Class<UpdateCustomQuestionAction> getActionType() {
        return UpdateCustomQuestionAction.class;
    }

    @Override
    public VoidResult execute(UpdateCustomQuestionAction action) throws ActionException {
        try {
            qaService.updateQuestion(action.getQuestionUuid(), action.isRequired());
            qaService.updateQuestionText(action.getQuestionUuid(), action.getText(), SystemLocale.DEFAULT);
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }
}
