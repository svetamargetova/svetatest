package consys.event.registration.json.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.service.ModuleRightService;
import consys.event.registration.core.bo.RegistrationOrder;
import consys.event.registration.core.bo.UserBundleRegistration;
import consys.event.registration.core.bo.UserRegistration;
import consys.event.registration.core.service.UserRegistrationService;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserDetailActionHandler implements ActionHandler {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailActionHandler.class);
    @Autowired
    private ModuleRightService moduleRightService;
    @Autowired
    private UserRegistrationService userRegistrationService;

    @Override
    public String getActionName() {
        return "user-detail";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        List<ModuleRight> rights;
        UserRegistration registration = null;
        try {
            rights = moduleRightService.listUserRightsByUuid(action.getUserUuid());
        } catch (NoRecordException ex) {
            rights = Collections.EMPTY_LIST;
        }
        try {
            registration = userRegistrationService.loadUserActiveRegistrationDetailedByUserUuid(action.getUserUuid());
        } catch (NoRecordException ex) {
        } catch (RequiredPropertyNullException ex) {
        }

        result.writeObjectFieldStart("user-event");

        // ROLE V JAKYCH JE
        result.writeArrayFieldStart("roles");
        if (!rights.isEmpty()) {
            for (ModuleRight right : rights) {
                result.writeString(right.getName());
            }
        }
        result.writeEndArray();

        // REGISTRACE
        if (registration != null) {
            result.writeStringField("registration-uuid", registration.getUuid());
            result.writeStringField("ticket", registration.getUuid());

            String mainBundleName = null;
            result.writeArrayFieldStart("subbundles");
            for (RegistrationOrder order : registration.getActiveOrders()) {
                for (UserBundleRegistration reg : order.getRegistredBundles()) {
                    if (!reg.getRegistrationCycle().getBundle().isSubBundle()) {
                        mainBundleName = reg.getRegistrationCycle().getBundle().getTitle();
                    } else {
                        result.writeString(reg.getRegistrationCycle().getBundle().getTitle());
                    }
                }
            }
            result.writeEndArray();
            result.writeStringField("ticket-type", mainBundleName);
        }
        result.writeEndObject();
    }
}
