package consys.event.registration.json.result;

import consys.common.json.Result;
import consys.event.registration.core.bo.thumb.CheckInListItem;
import consys.event.registration.core.bo.thumb.ParticipantListItem;
import java.io.IOException;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ParticipantResult {

    public static final String RESULT_NAME = "participants";

    public static void writePublicParticipants(Result result, List<ParticipantListItem> items) throws IOException {
        result.writeArrayFieldStart(RESULT_NAME);
        for (ParticipantListItem item : items) {
            result.writeStartObject();
            result.writeStringField("uuid", item.getUserUuid());
            result.writeStringField("name", item.getUserName());
            result.writeStringField("portrait", item.getProfileImagePrefix());
            result.writeStringField("organization", item.getUserOrganization());
            result.writeEndObject();
        }
        result.writeEndArray();
    }

    public static void writeAdminParticipants(Result result, List<CheckInListItem> items) throws IOException {
        result.writeArrayFieldStart(RESULT_NAME);
        for (CheckInListItem item : items) {
            result.writeStartObject();
            result.writeStringField("uuid", item.getUserUuid());
            result.writeStringField("name", item.getUserName());
            result.writeStringField("portrait", item.getProfileImagePrefix());
            result.writeStringField("organization", item.getUserOrganization());
            result.writeStringField("uuid-registration", item.getRegistrationUuid());
            result.writeBooleanField("checked", item.isChecked());
            result.writeDateTimeField("checkin-date", item.getCheckInDate());
            result.writeNumberField("max-quantity", item.getMaxQuantity());
            result.writeNumberField("quantity-checked", item.getQuantityChecked());
            result.writeEndObject();
        }
        result.writeEndArray();
    }
}
