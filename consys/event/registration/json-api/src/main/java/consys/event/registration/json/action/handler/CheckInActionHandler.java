package consys.event.registration.json.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.common.json.error.ErrorCode;
import consys.event.registration.core.exception.RegistrationNotActiveException;
import consys.event.registration.core.service.UserRegistrationService;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CheckInActionHandler implements ActionHandler {

    private static final Logger logger = LoggerFactory.getLogger(CheckInActionHandler.class);
    @Autowired
    private UserRegistrationService userRegistrationService;

    @Override
    public String getActionName() {
        return "checkin";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        try {
            Boolean b = action.getBooleanParam("action");
            if (b == null) {
                result.writeError(3, "Param action with type boolean is missing or has incorect representation. Valid are true,false. ");
                return;
            }
            Integer count = action.getIntParam("count");
            if (count == null) {
                result.writeError(3, "Missing parameter count");
                return;
            }
            if (count < -2 || count == 0) {
                result.writeError(3, "Bad value of parameter count");
                return;
            }
            userRegistrationService.updateRegistrationCheckIn(action.getStringParam("registration-uuid"), b, count);
            result.writeStringField("result", "done");
        } catch (NoRecordException ex) {
            ErrorCode.error(result, ex);
        } catch (RequiredPropertyNullException ex) {
            ErrorCode.error(result, ex, "action", "registration-uuid ");
        } catch (RegistrationNotActiveException ex) {
            ErrorCode.error(result, ex, "Registration is not active!");
        }
    }
}
