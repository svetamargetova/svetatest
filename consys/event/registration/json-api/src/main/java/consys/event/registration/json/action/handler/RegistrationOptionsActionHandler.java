package consys.event.registration.json.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.event.registration.core.bo.thumb.RegistrationOption;
import consys.event.registration.core.service.RegistrationBundleService;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RegistrationOptionsActionHandler implements ActionHandler {

    @Autowired
    private RegistrationBundleService bundleService;

    @Override
    public String getActionName() {
        return "registration-options";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        result.writeArrayFieldStart("options");
        try {
            List<RegistrationOption> options = bundleService.listMainRegistrationBundlesOptions();
            for (RegistrationOption item : options) {
                result.writeStartObject();
                result.writeStringField("uuid", item.getBundleUuid());
                result.writeStringField("title", item.getTitle());
                result.writeStringField("description", item.getDescription());
                result.writeNumberField("capacity", item.getCapacity());
                result.writeNumberField("registred", item.getRegistred());
                result.writeNumberField("price", item.getPrice() == null ? -1 : item.getPrice().intValue());
                result.writeEndObject();
            }
        } catch (NoRecordException ex) {
        }
        result.writeEndArray();
    }
}
