package consys.event.registration.json.action.handler;


import consys.common.json.Action;
import consys.common.json.ActionHandler;
import consys.common.json.Result;
import consys.event.common.core.service.list.Constraints;
import consys.event.registration.api.list.CheckInList;
import consys.event.registration.core.bo.thumb.CheckInListItem;
import consys.event.registration.core.service.CheckInListService;
import consys.event.registration.json.result.ParticipantResult;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ParticipantsManageActionHandler implements ActionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ParticipantsManageActionHandler.class);
    @Autowired
    private CheckInListService checkInListService;

    @Override
    public String getActionName() {
        return "participants-manage";
    }

    @Override
    public void execute(Action action, Result result) throws IOException {
        Constraints c = new Constraints();
        c.setFilterTag(CheckInList.Filter_ALL);
        List<CheckInListItem> items = checkInListService.listItems(c,null);
        ParticipantResult.writeAdminParticipants(result, items);
    }
}
