package consys.event.ws.admin.service;

import consys.admin.event.ws.AdminEventWebService;

import consys.admin.event.ws.EventUserRequest;
import consys.admin.event.ws.LoadEventOverseerRequest;
import consys.admin.event.ws.LoadEventOverseerResponse;
import consys.admin.event.ws.LoadUsersToEventRequest;
import consys.admin.event.ws.LoadUsersToEventResponse;
import consys.admin.event.ws.UpdateUserRelationToEventRequest;
import consys.admin.event.ws.UpdateUserRelationToEventResponse;
import consys.admin.user.ws.LoadUserEmailRequest;
import consys.admin.user.ws.LoadUserEmailResponse;
import consys.event.overseer.connector.EventRequestContextProvider;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class AdminEventWebServiceImpl extends WebServiceGatewaySupport implements AdminEventWebService {

    private static final Logger log = LoggerFactory.getLogger(AdminEventWebServiceImpl.class);

    private EventRequestContextProvider eventRequestContextProvider;

    @Override
    public UpdateUserRelationToEventResponse updateUserRelationToEvent(UpdateUserRelationToEventRequest request) {        
        request.setEventUuid(eventRequestContextProvider.getContext().getEventUuid().getId());
        log.debug("Updating user relation for event {} to relation {}" ,request.getEventUuid(),request.getNewRelation());            
        return (UpdateUserRelationToEventResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    /**
     * Nacita uzivatelov z administracie.
     * EventUuid a FromUserUuid sa nastavuju automaticky.
     */
    @Override
    public LoadUsersToEventResponse loadUserToEvent(LoadUsersToEventRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Load users to event: {}invite: {} to relation: {}", new Object[]{request.getEventUuid(),request.isInvite(),request.getRelation()});
            for(EventUserRequest r : request.getEventUserRequest()){
                if(StringUtils.isBlank(r.getUserUuid())){
                    log.debug("[new user]      name: {} email: {}", r.getUserName(),r.getUserEmail());
                }else{
                    log.debug("[existing user] uuid: {} ",r.getUserUuid());
                }
            }
        }
        request.setEventUuid(eventRequestContextProvider.getContext().getEventUuid().getId());
        request.setFromUserUuid(eventRequestContextProvider.getContext().getUserEventUuid());
        return (LoadUsersToEventResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadEventOverseerResponse loadEventOverseer(LoadEventOverseerRequest request) {
        request.setEventUuid(eventRequestContextProvider.getContext().getEventUuid().getId());
        return (LoadEventOverseerResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

   


    /**
     * @return the eventRequestContextProvider
     */
    public EventRequestContextProvider getEventRequestContextProvider() {
        return eventRequestContextProvider;
    }

    /**
     * @param eventRequestContextProvider the eventRequestContextProvider to set
     */
    public void setEventRequestContextProvider(EventRequestContextProvider eventRequestContextProvider) {
        this.eventRequestContextProvider = eventRequestContextProvider;
    }

    
    
   
}
