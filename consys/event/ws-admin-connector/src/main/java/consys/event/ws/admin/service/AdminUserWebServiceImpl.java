package consys.event.ws.admin.service;

import consys.admin.user.ws.AdminUserWebService;
import consys.admin.user.ws.LoadUserEmailRequest;
import consys.admin.user.ws.LoadUserEmailResponse;
import consys.event.overseer.connector.EventRequestContextProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class AdminUserWebServiceImpl extends WebServiceGatewaySupport implements AdminUserWebService {

    private static final Logger log = LoggerFactory.getLogger(AdminUserWebServiceImpl.class);

    private EventRequestContextProvider eventRequestContextProvider;

    @Override
    public LoadUserEmailResponse loadUserEmail(LoadUserEmailRequest request) {
        return (LoadUserEmailResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }


    /**
     * @return the eventRequestContextProvider
     */
    public EventRequestContextProvider getEventRequestContextProvider() {
        return eventRequestContextProvider;
    }

    /**
     * @param eventRequestContextProvider the eventRequestContextProvider to set
     */
    public void setEventRequestContextProvider(EventRequestContextProvider eventRequestContextProvider) {
        this.eventRequestContextProvider = eventRequestContextProvider;
    }

    
    
   
}
