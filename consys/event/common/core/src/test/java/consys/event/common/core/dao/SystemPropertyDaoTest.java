package consys.event.common.core.dao;

import consys.common.core.dao.SystemPropertyDao;
import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.AbstractEventCommonTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.testng.annotations.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Palo
 */

public class SystemPropertyDaoTest extends AbstractEventCommonTest{

    private SystemPropertyDao dao;

    /**
     * @param dao the dao to set
     */
    @Autowired
    @Required
    public void setDao(SystemPropertyDao dao) {
        this.dao = dao;
    }

    @Test(groups = {"dao"})
    public void createSystemProperty(){
        SystemProperty sp = new SystemProperty();
        sp.setKey("key");
        sp.setValue("value");
        dao.create(sp);
    }

    @Test(groups = {"dao"})
    public void loadByKey() throws NoRecordException{
        createSystemProperty();
        SystemProperty spo = new SystemProperty();
        spo.setKey("key");
        spo.setValue("value");
        SystemProperty sp = dao.loadByKey("key");
        assertEquals(sp, spo);
    }

}
