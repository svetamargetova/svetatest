package consys.event.common.core.service;

import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.thumb.Invitation;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.utils.UuidProvider;
import consys.event.common.api.right.EventCommonModuleRoleModel;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.bo.thumb.NewUserInvitation;
import consys.event.common.core.dao.CommitteeDao;
import consys.event.common.core.dao.GroupDao;
import consys.event.common.core.dao.ModuleDao;
import consys.event.common.core.dao.ModuleRightDao;
import consys.event.common.core.dao.UserEventDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */

public class CommitteeServiceTest  extends AbstractEventCommonTest{

    private ModuleDao moduleDao;
    private ModuleRightDao moduleRightDao;
    private GroupDao groupDao;
    private UserEventDao eventDao;
    private CommitteeDao committeeDao;
    private CommitteeService committeeService;
    private ModuleRightService moduleRightService;

    @Autowired(required = true)
    public void setModuleRightService(ModuleRightService moduleRightService) {
        this.moduleRightService = moduleRightService;
    }

    @Autowired(required = true)
    public void setCommitteeService(CommitteeService committeeService) {
        this.committeeService = committeeService;
    }

    @Autowired(required = true)
    public void setCommitteeDao(CommitteeDao committeeDao) {
        this.committeeDao = committeeDao;
    }

    @Autowired(required = true)
    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Autowired(required = true)
    public void setEventDao(UserEventDao eventDao) {
        this.eventDao = eventDao;
    }

    @Autowired(required = true)
    public void setModuleDao(ModuleDao moduleDao) {
        this.moduleDao = moduleDao;
    }

    @Autowired(required = true)
    public void setModuleRightDao(ModuleRightDao moduleRightDao) {
        this.moduleRightDao = moduleRightDao;
    }

    @Test(groups={"service"})
    public void testLoadCommittee() throws NoRecordException{
        String rightName = "Test module name";
        Module m = new Module();
        m.setName("Module Name");
        ModuleRight mr = new ModuleRight();
        mr.setName(rightName);
        m.getRights().add(mr);
        moduleDao.create(m);

       

        // Vytvorime user
        UserEvent ue = new UserEvent();
        ue.setPrivateGroup(new Group());
        ue.setUuid(UuidProvider.getUuid());
        ue.setFullName("1");
        ue.setLastName("1");
        ue.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        eventDao.create(ue);

        // GROUP 1
        Group g1 = new Group();
        g1.addRight(moduleRightDao.loadModuleRightByName(rightName));

        Group chairGroup = new Group();
        chairGroup.addRight(moduleRightDao.loadModuleRightByName(rightName));

        Committee c1 = new Committee();
        c1.setName("C1");
        c1.setGroup(g1);
        c1.setChairGroup(chairGroup);
        committeeDao.create(c1);

        c1.getGroup().addMember(ue);
        committeeDao.update(c1);

        Committee c2 = new Committee();
        c2.setName("C2");
        c2.setGroup(new Group());
        c2.setChairGroup(new Group());
        committeeDao.create(c2);

        committeeService.loadCommitteeWithRightsAndMembers(c1.getId());

        List<ListLongItem> committees = committeeService.listCommittees();
        assertTrue(committees.size()==4); // 2+ za automaticke Organization a Programme
    }

    @Test(groups={"service"})
    public void testCreateAndUpdateCommittee() throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed{
        // Vytvorime user
        UserEvent ue1 = new UserEvent();
        ue1.setPrivateGroup(new Group());
        ue1.setUuid(UuidProvider.getUuid());
        ue1.setFullName("A");
        ue1.setLastName("B");
        ue1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        eventDao.create(ue1);
        UserEvent ue2 = new UserEvent();
        ue2.setPrivateGroup(new Group());
        ue2.setUuid(UuidProvider.getUuid());
        ue2.setFullName("D");
        ue2.setLastName("C");
        ue2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        eventDao.create(ue2);


        Invitation chair1Invitation = new ExistingUserInvitation(ue1.getUuid());
        Invitation chair2Invitation = new NewUserInvitation("chair@domain.cz", "Palko");
        Invitation member1 = new NewUserInvitation("chair@domain.cz", "Palko");
        Invitation member2 = new ExistingUserInvitation(ue2.getUuid());
        Invitation member3 = new NewUserInvitation("member@domain.eu", "Jozef Jozef");

        logger.info("TEST 1 ---------------------------------------------------");
        // Vytvorime Vybor
        Committee c = committeeService.createCommittee("Committee", chair1Invitation,
                Lists.newArrayList(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_REMOVE),
                Lists.newArrayList(member1,member2), // prihodime aj chaira kvoli duplikovaniu
                Lists.newArrayList(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_REMOVE,EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT));

        Long committeeId = c.getId();
        List listedCommittesForUser = committeeService.listCommitteesForUser(ue1.getUuid());
        assertEquals(listedCommittesForUser.size(), 1);

        c = null;
        // Test ci ma user ue2 naozaj prava priradene
        List<ModuleRight> rights =  moduleRightService.listUserRightsByUuid(ue2.getUuid());
        assertEquals(2, rights.size());

        logger.info("TEST 2 ---------------------------------------------------");
        // Upravime vybor pridame prava a zmenime chaira
        committeeService.updateCommittee(committeeId, "Name", chair2Invitation,
               null, // neodstranime ziadne prava
               null, // nepridame mu ziadne prava
               Lists.newArrayList(member3), // novy clenovia
               Lists.newArrayList(ue2.getUuid()), // odstraneni clenovia
               null, // nove prava vyboru
               Lists.newArrayList(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT)); // odstranene prava vyboru

        listedCommittesForUser = committeeService.listCommitteesForUser(ue1.getUuid());
        assertEquals(listedCommittesForUser.size(), 0);

        // Test ci ma user ue2 naozaj prava priradene - uz nie je clenom vyboru takze prava nema
        rights =  moduleRightService.listUserRightsByUuid(ue2.getUuid());
        assertEquals(rights.size(),0);
        
    }




}
