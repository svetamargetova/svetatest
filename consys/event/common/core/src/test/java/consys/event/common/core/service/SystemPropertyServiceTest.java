package consys.event.common.core.service;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.api.right.EventCommonModuleRoleModel;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.module.CommonModule;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */
public class SystemPropertyServiceTest extends AbstractEventCommonTest {

    private SystemPropertyService service;

    /**
     * @param service the service to set
     */
    @Autowired(required = true)
    public void setService(SystemPropertyService service) {
        this.service = service;
    }

    @Test(groups = {"service"},dependsOnMethods={"testSystemPropertyCache"})
    public void testCreateUpdate() {
        assertNotNull(service);
        service.createBooleanProperty("bool", Boolean.TRUE);
        assertTrue(service.getBoolean("bool"));
    }

    @Test(groups = {"service"})
    public void testSystemPropertyCache() {
        CommonModule cm = new CommonModule();

        List<SystemProperty> sp = service.listAllProperties();
        
        
        
        assertTrue(sp.size() >= cm.getModuleProperties().size());

        service.getString(CommonProperties.EVENT_EMAIL);
        service.getString(CommonProperties.EVENT_ADDRESS);
        service.getString(CommonProperties.EVENT_CURRENCY);
        service.getString(CommonProperties.EVENT_FROM);
        service.getString(CommonProperties.EVENT_SHORT_NAME);
        
        
        
    }

    @Test(groups = {"service"}, expectedExceptions = {IllegalPropertyValue.class})
    public void testUpdateCurrencyWithBadValue() throws IllegalPropertyValue, NoRecordException {
        service.updateProperty(CommonProperties.EVENT_CURRENCY, "SKK");
    }
}
