package consys.event.common.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.UuidProvider;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.UserEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */

public class UserEventDaoTest extends AbstractEventCommonTest {

    private UserEventDao userDao;
    private GroupDao groupDao;

    /**
     * @param userDao the userDao to set
     */
    @Autowired(required = true)
    public void setUserDao(UserEventDao userDao) {
        this.userDao = userDao;
    }

    /**
     * @param groupDao the groupDao to set
     */
    @Autowired(required = true)
    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Test(groups = {"dao"})
    public void testLoadByUuid() throws NoRecordException {

        Group g = new Group();
        groupDao.create(g);

        UserEvent ue = new UserEvent();
        ue.setUuid(UuidProvider.getUuid());
        ue.setFullName("a");
        ue.setLastName("a");
        ue.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        ue.setPrivateGroup(g);
        userDao.create(ue);

        UserEvent ul = userDao.loadUserEvent(ue.getUuid());

        assertEquals(ul, ue);
    }
}
