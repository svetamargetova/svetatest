package consys.event.common.core.properties;

import consys.common.core.exception.IllegalPropertyValue;
import org.testng.annotations.Test;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventPropertiesTests {


    @Test(groups={"utils"})
    public void testTimeZoneValidation() throws IllegalPropertyValue{
        String value1 = "Atlantic/South_Georgia";
        String value2 = "Canada/Atlantic";
        String value3 = "Europe/Prague";

        EventTimeZonePropertyHandler handler = new EventTimeZonePropertyHandler();
        handler.handle(null, value1);
        handler.handle(null, value2);
        handler.handle(null, value3);

    }




}
