package consys.event.common.core.service;

import consys.common.core.exception.RequiredPropertyNullException;
import consys.event.common.core.bo.thumb.Invitation;
import consys.common.utils.collection.Lists;
import java.util.List;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.UuidProvider;
import consys.event.common.api.right.EventCommonModuleRoleModel;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.NewUserInvitation;
import consys.event.common.core.dao.ModuleDao;
import consys.event.common.core.dao.UserEventDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserEventServiceTest extends AbstractEventCommonTest {

    private UserEventService userEventService;
    private ModuleDao moduleDao;
    private UserEventDao userEventDao;

    @Test(groups = "service")
    public void createUserWithRights() throws NoRecordException, RequiredPropertyNullException {
        Module m = new Module();
        m.setName("Module Name");
        ModuleRight mr = new ModuleRight();
        mr.setName("Right");
        m.getRights().add(mr);
        moduleDao.create(m);

        UserEvent ue1 = new UserEvent();
        ue1.setFullName("a");
        ue1.setLastName("a");
        ue1.setUuid("123");
        ue1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        userEventService.createUserEventWithRights(ue1, new String[]{mr.getName(), EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS});

        UserEvent ue2 = userEventService.loadUserEvent("123");

        assertEquals(ue1, ue2);
        assertEquals(ue1.getPrivateGroup().getRights(), ue2.getPrivateGroup().getRights());
        assertEquals(ue1.getPrivateGroup().getRights().size(), ue2.getPrivateGroup().getRights().size());
        assertTrue(userEventService.hasUserRight(ue1.getUuid(), EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS));
        
        forceSessionFlush();
        
        userEventService.addUserPersonalRight(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR, ue2);
        
        forceSessionFlush();
        
        ue2 = userEventService.loadUserEvent(ue2.getUuid());
        
        assertTrue(userEventService.hasUserRight(ue2.getUuid(), EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR));
        
        forceSessionFlush();
        
        assertTrue(userEventService.hasUserRight(ue2.getUuid(), EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR));
        
        ue2 = userEventService.loadUserEvent(ue2.getUuid());
        userEventService.addUserPersonalRight(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR, ue2);
        userEventService.removeUserPersonalRight(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR, ue2);
        
        forceSessionFlush();
        
        ue2 = userEventService.loadUserEvent(ue2.getUuid());
        
        assertFalse(userEventService.hasUserRight(ue2.getUuid(), EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR));
        
        
    }

    @Test(groups = "service")
    public void createUserWithRequestsToAdministration() throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException {
        UserEvent ue1 = new UserEvent();
        ue1.setFullName("a");
        ue1.setLastName("a");
        ue1.setPrivateGroup(new Group());
        ue1.setUuid(UuidProvider.getUuid());
        ue1.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        userEventDao.create(ue1);
        
        UserEvent ue2 = new UserEvent();
        ue2.setFullName("a");
        ue2.setLastName("a");
        ue2.setPrivateGroup(new Group());
        ue2.setUuid(UuidProvider.getUuid());
        ue2.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        userEventDao.create(ue2);
        
        // Testujeme
        logger.info("TEST 1: vytvoreni uzivatela s requestem nad existujucim uzivatelem");
        Invitation eur1 = new ExistingUserInvitation(ue1.getUuid());
        UserEvent ue11 = userEventService.createUserOrLoad(eur1);
        assertEquals(ue1, ue11);

        logger.info("TEST 2: vytvoreni uzivatela s requestem nad novym uzivatelem");
        Invitation eur2 = new NewUserInvitation("email@domain.cz", "meno meno");
        UserEvent ue21 = userEventService.createUserOrLoad(eur2);
        assertNotNull(ue21.getId());
        assertNotNull(ue2.getUuid());

        logger.info("TEST 3: vytvoreni uzivatelu s requestem nad novym uzivatelem aj existujucim");
        Invitation eur31 = new ExistingUserInvitation(ue2.getUuid());
        Invitation eur32 = new NewUserInvitation("email@domain.cz", "name 1");
        Invitation eur33 = new NewUserInvitation("email@domain.cz", "name 2");
        
        
        List<UserEvent> ues = userEventService.createUserOrLoad(Lists.newArrayList(eur31,eur32,eur33));
        assertEquals(ues.size(), 3);
        for (UserEvent userEvent : ues) {
            UserEvent loadedUser = userEventDao.loadUserEvent(userEvent.getUuid());
            assertEquals(loadedUser, userEvent);
        }
    }

    /**
     * @param userEventService the userEventService to set
     */
    @Autowired(required = true)
    public void setUserEventService(UserEventService userEventService) {
        this.userEventService = userEventService;
    }

    /**
     * @param moduleDao the moduleDao to set
     */
    @Autowired(required = true)
    public void setModuleDao(ModuleDao moduleDao) {
        this.moduleDao = moduleDao;
    }

    @Autowired(required = true)
    public void setUserEventDao(UserEventDao userEventDao) {
        this.userEventDao = userEventDao;
    }


}
