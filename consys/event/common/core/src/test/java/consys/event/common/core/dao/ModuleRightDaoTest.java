package consys.event.common.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.UuidProvider;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.bo.UserEvent;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */

public class ModuleRightDaoTest extends AbstractEventCommonTest {

    private ModuleDao moduleDao;
    private ModuleRightDao moduleRightDao;
    private GroupDao groupDao;
    private UserEventDao eventDao;
    private CommitteeDao committeeDao;

    @Autowired(required = true)
    public void setCommitteeDao(CommitteeDao committeeDao) {
        this.committeeDao = committeeDao;
    }

    @Autowired(required = true)
    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Autowired(required = true)
    public void setEventDao(UserEventDao eventDao) {
        this.eventDao = eventDao;
    }

    @Autowired(required = true)
    public void setModuleDao(ModuleDao moduleDao) {
        this.moduleDao = moduleDao;
    }

    @Autowired(required = true)
    public void setModuleRightDao(ModuleRightDao moduleRightDao) {
        this.moduleRightDao = moduleRightDao;
    }

    @Test(groups = {"dao"})
    public void testLoadByName() throws NoRecordException {
       
        Module m = new Module();
        m.setName("Module Name");
        ModuleRight mr = new ModuleRight();
        mr.setName("Test module name");
        m.getRights().add(mr);
        moduleDao.create(m);

        ModuleRight lmr = moduleRightDao.loadModuleRightByName(mr.getName());
        assertEquals(mr.getName(), mr.getName());
    }

    @Test(groups = {"dao"})
    public void testLoadUserRights() throws NoRecordException {
        String rightName1 = "Test_module_name";
        String rightName2 = "Test_module_name2";
        Module m = new Module();
        m.setName("Module Name");
        ModuleRight mr = new ModuleRight();
        mr.setName(rightName1);
        ModuleRight mrr = new ModuleRight();
        mrr.setName(rightName2);
        m.getRights().add(mr);
        m.getRights().add(mrr);
        moduleDao.create(m);

        Group userGroup = new Group();
        userGroup.addRight(moduleRightDao.loadModuleRightByName(rightName1));
        groupDao.create(userGroup);


        UserEvent ue = new UserEvent();
        ue.setFullName("1");
        ue.setLastName("1");
        ue.setEmail("m" + System.currentTimeMillis() + "@mail.cz");
        ue.setPrivateGroup(userGroup);
        ue.setUuid(UuidProvider.getUuid());
        eventDao.create(ue);

        // GROUP 1
        Group g1 = new Group();
        g1.addRight(moduleRightDao.loadModuleRightByName(rightName1));
       
        Group chairGroup = new Group();
        chairGroup.addRight(moduleRightDao.loadModuleRightByName(rightName1));

        Committee c1 = new Committee();
        c1.setName("C1");
        c1.setGroup(g1);
        c1.setChairGroup(chairGroup);
        committeeDao.create(c1);

        c1.getGroup().addMember(ue);
        committeeDao.update(c1);

        // GROUP 2
        Group g2 = new Group();
        g2.addRight(moduleRightDao.loadModuleRightByName(rightName1));
        Committee c2 = new Committee();
        c2.setName("C1");
        c2.setGroup(g2);
        c2.setChairGroup(new Group());
        c2.getGroup().addMember(ue);
        committeeDao.create(c2);

        List<ModuleRight> l = moduleRightDao.listUserEventRights(ue.getId());
        assertTrue(l.size() == 1);
        l = moduleRightDao.listUserEventRights(ue.getUuid());
        assertTrue(l.size() == 1);
        assertTrue(moduleRightDao.hasUserRight(ue.getUuid(), rightName1));
        assertFalse(moduleRightDao.hasUserRight(ue.getUuid(), rightName2));

        List<UserEvent> users = groupDao.listAllUserEventsWithRightName(rightName1);
        assertEquals(users.size(),1);
    }
}
