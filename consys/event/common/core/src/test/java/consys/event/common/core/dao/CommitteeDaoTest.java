package consys.event.common.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.Group;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Palo
 */

public class CommitteeDaoTest  extends AbstractEventCommonTest{

    private CommitteeDao committeeDao;
    private GroupDao groupDao;

    @Test(groups = {"dao"})
    public void testCreate() throws NoRecordException {
        Group g = new Group();
        groupDao.create(g);

        Committee c = new Committee();
        c.setName("Name");
        c.setGroup(g);
        c.setChairGroup(new Group());
        committeeDao.create(c);
        
        Committee cl = committeeDao.load(c.getId());
        assertEquals(c, cl);
    }

    @Test(groups = {"dao"})
    public void testListCommittees() throws NoRecordException {
        Group g = new Group();
        groupDao.create(g);

        Committee c = new Committee();
        c.setName("BA");
        c.setGroup(g);
        c.setChairGroup(new Group());
        committeeDao.create(c);

        g = new Group();
        groupDao.create(g);

        c = new Committee();
        c.setName("AB");
        c.setGroup(g);
        c.setChairGroup(new Group());
        committeeDao.create(c);

        List<Committee> list =  committeeDao.listCommittees();
        assertTrue(list.size()== 4, "Nerovnaju sa!"); // 2+ za automaticke Organization a Programme
        assertTrue(list.get(0).getName().equalsIgnoreCase("AB"), "Nerovan sa prvy");
        assertTrue(list.get(1).getName().equalsIgnoreCase("BA"), "Nerovan sa druhy");
    }

    /**
     * @return the committeeDao
     */
    public CommitteeDao getCommitteeDao() {
        return committeeDao;
    }

    /**
     * @param committeeDao the committeeDao to set
     */
    @Autowired(required = true)
    public void setCommitteeDao(CommitteeDao committeeDao) {
        this.committeeDao = committeeDao;
    }

    /**
     * @return the groupDao
     */
    public GroupDao getGroupDao() {
        return groupDao;
    }

    /**
     * @param groupDao the groupDao to set
     */
    @Autowired(required = true)
    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }
}
