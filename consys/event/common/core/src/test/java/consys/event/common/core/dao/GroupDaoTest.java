package consys.event.common.core.dao;

import consys.event.common.core.AbstractEventCommonTest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */

public class GroupDaoTest extends AbstractEventCommonTest {

    private GroupDao groupDao;

    /**
     * @return the groupDao
     */
    public GroupDao getGroupDao() {
        return groupDao;
    }

    /**
     * @param groupDao the groupDao to set
     */
    @Autowired(required = true)
    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }
}
