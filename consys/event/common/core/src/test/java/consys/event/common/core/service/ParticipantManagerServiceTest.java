package consys.event.common.core.service;

import consys.common.utils.UuidProvider;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.UserEventDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class ParticipantManagerServiceTest extends AbstractEventCommonTest {

    // konstanty
    private static final String TEST_MAIL = "mail@mail.ma";
    // sluzby
    @Autowired
    private ParticipantManagerService service;
    // dao
    @Autowired
    private UserEventDao userEventDao;

    private UserEvent createManager() {
        UserEvent manager = new UserEvent();
        manager.setEmail(TEST_MAIL);
        manager.setFullName("Jožin");
        manager.setLastName("Kdovi");
        manager.setOrganization("Takeplace");
        manager.setPosition("Dev");
        manager.setPrivateGroup(new Group());
        manager.setUuid(UuidProvider.getUuid());

        try {
            userEventDao.create(manager);
        } catch (Exception ex) {
            fail("Fail", ex);
        }

        return manager;
    }

    private UserEvent createParticipant() {
        UserEvent p = new UserEvent();
        p.setFullName(UuidProvider.getUuid());
        p.setLastName(UuidProvider.getUuid());
        p.setOrganization(UuidProvider.getUuid());
        return p;
    }

    private void assignParticipant(String uuidManager, UserEvent participant) {
        try {
            service.createNewManagedUser(uuidManager, participant);
        } catch (Exception ex) {
            fail("Fail", ex);
        }
    }

    @Test(groups = "service")
    @Rollback
    public void createNewManagedUserTest() {
        UserEvent manager = createManager();
        assertTrue(manager.isSystemUser());
        
        UserEvent participant = createParticipant();

        assignParticipant(manager.getUuid(), participant);

        assertNotNull(participant.getId());
        assertNotNull(participant.getEmail());
        assertNotNull(participant.getPrivateGroup());
        assertNotNull(participant.getUuid());
        assertFalse(participant.isSystemUser());

        assertEquals(TEST_MAIL, manager.getEmail());
        assertEquals(manager.getEmail(), participant.getEmail());
    }

    @Test(groups = "service")
    @Rollback
    public void checkManagedUser() {
        UserEvent manager = createManager();

        assertFalse(service.checkManagedUser(manager.getUuid(), UuidProvider.getUuid()));

        UserEvent participant = createParticipant();
        assignParticipant(manager.getUuid(), participant);

        assertTrue(service.checkManagedUser(manager.getUuid(), participant.getUuid()));
        assertFalse(service.checkManagedUser(manager.getUuid(), UuidProvider.getUuid()));
    }

    @Test(groups = "service")
    @Rollback
    public void listManagedParticipants() {
        UserEvent manager = createManager();

        List<UserEvent> list = service.listManagedParticipants(manager.getUuid());
        assertEquals(0, list.size());

        UserEvent participant1 = createParticipant();
        assignParticipant(manager.getUuid(), participant1);

        list = service.listManagedParticipants(manager.getUuid());
        assertEquals(1, list.size());

        UserEvent participant2 = createParticipant();
        assignParticipant(manager.getUuid(), participant2);

        list = service.listManagedParticipants(manager.getUuid());
        assertEquals(2, list.size());
    }
}
