package consys.event.common.core.dao;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.UuidProvider;
import consys.event.common.core.AbstractEventCommonTest;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.bo.enums.EventVideoState;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author pepa
 */
public class EventVideoDaoTest extends AbstractEventCommonTest {

    private EventVideoDao videoDao;

    @Autowired(required = true)
    public void setVideoDao(EventVideoDao videoDao) {
        this.videoDao = videoDao;
    }

    @Test(groups = {"dao"})
    public void testLoadBySystemName() {
        final String SYSTEM_NAME = "1245566";
        final String SYSTEM_NAME_NOEXISTING = "1245566342";

        EventVideo ev = new EventVideo();
        ev.setCustomName("bla bla název");
        ev.setDescription("bla bla popisek");
        ev.setState(EventVideoState.UPLOADED);
        ev.setSystemName(SYSTEM_NAME);

        videoDao.create(ev);

        try {
            EventVideo evLoaded = videoDao.loadVideo(SYSTEM_NAME);
            assertEquals(evLoaded, ev);
        } catch (NoRecordException ex) {
            fail("Not found existing video");
        }

        try {
            videoDao.loadVideo(SYSTEM_NAME_NOEXISTING);
            fail("Load nonexisting video");
        } catch (NoRecordException ex) {
            // ok, neexistujici video
        }
    }

    @Test(groups = {"dao"})
    public void testUpdateStateAndHash() {
        final String SYSTEM_NAME = "12455662";
        final String HASH = "hash";

        EventVideo ev = new EventVideo();
        ev.setCustomName("bla bla název");
        ev.setDescription("bla bla popisek");
        ev.setState(EventVideoState.UPLOADED);
        ev.setSystemName(SYSTEM_NAME);

        videoDao.create(ev);

        try {
            EventVideo loaded = videoDao.load(ev.getId());
            assertEquals(loaded.getState(), EventVideoState.UPLOADED);

            loaded.setState(EventVideoState.TAKEPLACE_PROCESSING);
            loaded.setSuperlecturesHash(HASH);

            videoDao.update(loaded);

            EventVideo loaded2 = videoDao.loadVideo(SYSTEM_NAME);

            assertEquals(loaded2.getState(), EventVideoState.TAKEPLACE_PROCESSING);
            assertEquals(loaded2.getSuperlecturesHash(), HASH);
        } catch (NoRecordException ex) {
            fail("Existing video not loaded");
        }
    }

    @Test(groups = {"dao"})
    public void testListVideoByState() {
        final String SYSTEM_NAME = "12455662";

        EventVideo ev = new EventVideo();
        ev.setCustomName("bla bla název");
        ev.setDescription("bla bla popisek");
        ev.setState(EventVideoState.UPLOADED);
        ev.setSystemName(SYSTEM_NAME);
        ev.setUuid("1231241242141");
        ev.setToProcessing(true);

        videoDao.create(ev);

        List<EventVideo> list1 = videoDao.listVideosByState(EventVideoState.UPLOADED);
        assertEquals(list1.size(), 1);
        List<EventVideo> list2 = videoDao.listVideosByState(EventVideoState.TAKEPLACE_PROCESSING);
        assertEquals(list2.size(), 0);

    }

    @Test(groups = {"dao"})
    public void testListVideo() {
        Constraints constraints = new Constraints();
        constraints.setFilterTag(1);

        Paging paging = new Paging();
        paging.setFirstResult(0);
        paging.setMaxResults(10);

        assertEquals(videoDao.listVideos(constraints, paging).size(), 0);
        for (int i = 0; i < 5; i++) {
            newEventVideo();
        }

        paging.setFirstResult(2);
        paging.setMaxResults(10);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 3);

        paging.setFirstResult(2);
        paging.setMaxResults(3);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 3);

        paging.setFirstResult(0);
        paging.setMaxResults(3);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 3);

        paging.setFirstResult(1);
        paging.setMaxResults(4);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 4);

        paging.setFirstResult(0);
        paging.setMaxResults(4);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 4);

        assertEquals(videoDao.loadVideoCount(constraints).intValue(), 5);

        newEventVideo(EventVideoState.SUPERLECTURES_PROCESSED);

        paging.setFirstResult(0);
        paging.setMaxResults(10);
        constraints.setFilterTag(1);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 6);
        constraints.setFilterTag(2);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 0);
        constraints.setFilterTag(3);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 5);
        constraints.setFilterTag(4);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 1);
        constraints.setFilterTag(5);
        assertEquals(videoDao.listVideos(constraints, paging).size(), 0);
    }

    private EventVideo newEventVideo() {
        return newEventVideo(EventVideoState.UPLOADED);
    }

    private EventVideo newEventVideo(EventVideoState state) {
        EventVideo ev = new EventVideo();
        ev.setCustomName("bla bla název");
        ev.setDescription("bla bla popisek");
        ev.setState(state);
        ev.setSystemName(UuidProvider.getUuid());

        videoDao.create(ev);

        return ev;
    }
}
