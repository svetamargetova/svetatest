package consys.event.common.core;

import consys.event.common.test.AbstractEventDaoTestCase;
import org.springframework.test.context.ContextConfiguration;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@ContextConfiguration(locations = {"event-common-spring.xml", "classpath:consys/event/common/test/core/service-mocks.xml"})
public class AbstractEventCommonTest extends AbstractEventDaoTestCase {
}
