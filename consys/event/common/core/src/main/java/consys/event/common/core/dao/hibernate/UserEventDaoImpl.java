package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.UserEventDao;
import java.util.List;

/**
 *
 * @author Palo
 */
public class UserEventDaoImpl extends GenericDaoImpl<UserEvent> implements UserEventDao {

    @Override
    public List<UserEvent> listByUuid(List<String> uuids) {
        return session().createQuery("from UserEvent u where u.uuid in (:UUIDS)").setParameterList("UUIDS", uuids).list();
    }

    @Override
    public UserEvent loadUserEvent(String uuid) throws NoRecordException {
        UserEvent e = (UserEvent) session().createQuery("select u from UserEvent u where u.uuid=:UUID").setString("UUID", uuid).uniqueResult();
        if(e == null){
            throw new NoRecordException("No user with UUID: "+uuid);
        }
        return e;
    }

    @Override
    public UserEvent load(Long id) throws NoRecordException {
        return load(id, UserEvent.class);
    }
}
