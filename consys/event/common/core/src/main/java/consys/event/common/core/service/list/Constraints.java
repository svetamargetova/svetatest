package consys.event.common.core.service.list;

/**
 * Omedzenia listu. Popusuju filter a orderovace ktore obmdezuju vysledky zoznamu.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public final class Constraints {

    private int[] orderers;
    private int filterTag;
    private String stringFilterValue;
    private int intFilterValue;
    private String userUuid;

    
    /**
     * @return the orderers
     */
    public int[] getOrderers() {
        return orderers;
    }

    /**
     * @return the filterTag
     */
    public int getFilterTag() {
        return filterTag;
    }

    /**
     * @return the filterValue
     */
    public String getStringFilterValue() {
        return stringFilterValue;
    }

    /**
     * @param orderers the orderers to set
     */
    public void setOrderers(int[] orderers) {
        this.orderers = orderers;
    }

    /**
     * @param filterTag the filterTag to set
     */
    public void setFilterTag(int filterTag) {
        this.filterTag = filterTag;
    }

    /**
     * @param filterValue the filterValue to set
     */
    public void setStringFilterValue(String filterValue) {
        this.stringFilterValue = filterValue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(String.format("Constraints[ftag=%d fval=%s ival=%d orderers=",filterTag,stringFilterValue,intFilterValue));
        for(int i = 0 ; i < orderers.length; i++){
            sb.append(orderers[i]).append(" , ");
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * @return the intFilterValue
     */
    public int getIntFilterValue() {
        return intFilterValue;
    }

    /**
     * @param intFilterValue the intFilterValue to set
     */
    public void setIntFilterValue(int intFilterValue) {
        this.intFilterValue = intFilterValue;
    }

    /**
     * @return the userUuid
     */
    public String getUserUuid() {
        return userUuid;
    }

    /**
     * @param userUuid the userUuid to set
     */
    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }


}
