package consys.event.common.core.youtube;

import com.amazonaws.services.s3.model.S3Object;
import com.google.gdata.client.authn.oauth.GoogleOAuthHelper;
import com.google.gdata.client.authn.oauth.GoogleOAuthParameters;
import com.google.gdata.client.authn.oauth.OAuthRsaSha1Signer;
import com.google.gdata.client.authn.oauth.OAuthSigner;
import com.google.gdata.client.youtube.YouTubeService;
import com.google.gdata.data.media.MediaStreamSource;
import com.google.gdata.data.media.mediarss.MediaCategory;
import com.google.gdata.data.media.mediarss.MediaDescription;
import com.google.gdata.data.media.mediarss.MediaTitle;
import com.google.gdata.data.youtube.VideoEntry;
import com.google.gdata.data.youtube.YouTubeMediaGroup;
import com.google.gdata.data.youtube.YouTubeNamespace;
import com.google.gdata.data.youtube.YtPublicationState;
import com.google.gdata.util.XmlBlob;
import consys.common.core.aws.AwsFileStorageService;
import consys.event.common.core.bo.EventVideo;
import java.io.InputStream;
import java.net.URL;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Enumeration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class YouTubeManager {

    // logger
    private static final Logger logger = LoggerFactory.getLogger(YouTubeManager.class);
    // pristupove udaje
    private static final String GOOGLE_ID = "641876194093-7ap59nuubkqs61oqlko9ik3s6rl6bo90.apps.googleusercontent.com";
    private static final String GOOGLE_ID_EMAIL = "641876194093-7ap59nuubkqs61oqlko9ik3s6rl6bo90@developer.gserviceaccount.com";
    private static final String DEVELOPER_KEY = "AI39si7Xg3Yc6DN7a0p7CZyH6GsqknfP9X6puSXQaxVs15XPdLPt3yb1wwJ7HOturEJbGcXn43dRQffwgA_a4i3QRalnr5ah9Q";
    // konstanty
    private static final String SCHEME_NAME = "People";
    private static final String SCOPE_URL = "https://uploads.gdata.youtube.com/feeds";
    private static final String FEED_URL = "https://uploads.gdata.youtube.com/feeds/api/users/default/uploads";
    private static final String YOUTUBE_URL = "https://gdata.youtube.com/feeds/api/users/default/uploads";
    private static final String YOUTUBE_TAKEPLACE_URL = "https://gdata.youtube.com/feeds/api/users/takeplaceVideo/uploads/";
    private static final String VIDEO_ENTRY_URL_BEGIN = "http://gdata.youtube.com/feeds/api/videos/";
    private static final String P12_PATH = "/p12/edb8f15f99f3074ef8b8d8bff1b3bb30619e0995-privatekey.p12";
    // servicy
    @Autowired
    private AwsFileStorageService storageService;
    // data
    private final YouTubeService service;
    private OAuthSigner signer;
    private GoogleOAuthParameters parameters;
    private GoogleOAuthHelper oauthHelper;
    private String videoBucket;
    private String youtubeUserName;
    private String youtubeUserPass;

    public YouTubeManager() throws Exception {
        service = new YouTubeService("Lectures upload", DEVELOPER_KEY);

        PrivateKey privKey = getPrivateKey();
        if (privKey == null) {
            throw new Exception("Missing private key");
        }

        signer = new OAuthRsaSha1Signer(privKey);

        oauthHelper = new GoogleOAuthHelper(signer);
    }

    /** pokusi se uploadovat video na youtube */
    public synchronized String uploadVideo(EventVideo ev) throws Exception {
        /*if (parameters == null) {
         throw new Exception("Youtube credientials parameters are NOT initialized");
         }*/

        String id = null;
        S3Object video = null;

        try {
            video = storageService.getObject(videoBucket, ev.getSystemName());
            VideoEntry newEntry = newEntry(video.getObjectContent(), ev);
            VideoEntry createdEntry;
            synchronized (service) {
                //service.setOAuthCredentials(parameters, signer);
                service.setUserCredentials(youtubeUserName, youtubeUserPass);
                service.getRequestFactory().setHeader("Slug", ev.getSystemName());

                logger.debug("File uploading start: " + ev.getSystemName());
                createdEntry = service.insert(new URL(FEED_URL), newEntry);
                createdEntry.update();
                logger.debug("File uploading end: " + ev.getSystemName());
            }
            id = createdEntry.getId();
        } finally {
            if (video != null) {
                video.getObjectContent().close();
            }
        }

        return id;
    }

    /** smaze video z youtube */
    public void removeVideo(String youTubeId) throws Exception {
        String id = idFromYouTubeId(youTubeId);

        synchronized (service) {
            service.setUserCredentials(youtubeUserName, youtubeUserPass);
            VideoEntry videoEntry = service.getEntry(new URL(YOUTUBE_TAKEPLACE_URL + id), VideoEntry.class);
            videoEntry.delete();
        }
    }

    /** vygeneruje novy vstup videa pro youtube */
    private static VideoEntry newEntry(InputStream is, EventVideo ev) {
        VideoEntry newEntry = new VideoEntry();

        YouTubeMediaGroup mg = newEntry.getOrCreateMediaGroup();
        mg.setTitle(new MediaTitle());
        mg.getTitle().setPlainTextContent(ev.getCustomName());
        mg.addCategory(new MediaCategory(YouTubeNamespace.CATEGORY_SCHEME, SCHEME_NAME));
        mg.setDescription(new MediaDescription());
        mg.getDescription().setPlainTextContent(ev.getDescription());
        mg.setPrivate(false);

        // video bude neverejne
        XmlBlob nonlisted = new XmlBlob();
        nonlisted.setBlob("<yt:accessControl action='list' permission='denied'/>");
        newEntry.setXmlBlob(nonlisted);

        MediaStreamSource ms = new MediaStreamSource(is, ev.getMediaType());
        newEntry.setMediaSource(ms);

        return newEntry;
    }

    /** zkontroluje na youtube jestli je video zpracovane */
    public synchronized YouTubeProcessingState checkVideoProcessed(EventVideo ev) throws Exception {
        if (StringUtils.isBlank(ev.getYouTubeId())) {
            throw new IllegalArgumentException("Empty youtube id");
        }

        String id = idFromYouTubeId(ev.getYouTubeId());
        VideoEntry ve;

        synchronized (service) {
            service.setUserCredentials(youtubeUserName, youtubeUserPass);
            ve = service.getEntry(new URL(VIDEO_ENTRY_URL_BEGIN + id), VideoEntry.class);
        }

        if (ve.isDraft()) {
            YtPublicationState pubState = ve.getPublicationState();
            if (YtPublicationState.State.PROCESSING.equals(pubState.getState())) {
                logger.info("Video " + ev + " is processing in youtube");
                return YouTubeProcessingState.PROCESSING;
            } else if (YtPublicationState.State.REJECTED.equals(pubState.getState())) {
                logger.error("Video " + ev + " was rejected in youtube. " + pubState.getDescription());
                return YouTubeProcessingState.FAILED;
            } else if (YtPublicationState.State.FAILED.equals(pubState.getState())) {
                logger.error("Video " + ev + " failed in youtube. " + pubState.getDescription());
                return YouTubeProcessingState.FAILED;
            }
            return YouTubeProcessingState.PROCESSING;
        }

        logger.info("Video " + ev + " processed in youtube");
        return YouTubeProcessingState.PROCESSED;
    }

    /** parametry pro pristup - nepouziva se, ale nechavam kdyby se to jednou predelavalo*/
    private GoogleOAuthParameters googleOAuthParameters() throws Exception {
        GoogleOAuthParameters oauthParameters = new GoogleOAuthParameters();
        oauthParameters.setOAuthConsumerKey(GOOGLE_ID);
        oauthParameters.setScope(SCOPE_URL);
        //oauthParameters.setOAuthCallback(callbackUrl);

        oauthHelper.getUnauthorizedRequestToken(oauthParameters);

        return oauthParameters;
    }

    /** vraci privatni klic pro podepsani - nepouziva se, ale nechavam kdyby se to jednou predelavalo */
    private PrivateKey getPrivateKey() throws Exception {
        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(this.getClass().getResourceAsStream(P12_PATH), "notasecret".toCharArray());

        Enumeration<String> aliases = ks.aliases();
        Key key = null;

        while (aliases.hasMoreElements()) {
            String alias = aliases.nextElement();
            key = ks.getKey(alias, "notasecret".toCharArray());
            // bereme jen prvni (a taky jediny)
            break;
        }
        return (PrivateKey) key;
    }

    /** vraci id do url z youTubeId */
    public static String idFromYouTubeId(String youTubeId) {
        String[] split = youTubeId.split(":");
        return split[split.length - 1];
    }

    public void setVideoBucket(String videoBucket) {
        this.videoBucket = videoBucket;
    }

    public void setYoutubeUserName(String youtubeUserName) {
        this.youtubeUserName = youtubeUserName;
    }

    public void setYoutubeUserPass(String youtubeUserPass) {
        this.youtubeUserPass = youtubeUserPass;
    }
}
