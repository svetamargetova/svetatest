package consys.event.common.core;

import java.util.Date;

/**
 * Rozhranie ktore implementuje nejake spracovanie faktury
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface EventOrderStateChangeListener {

    public enum OrderState{
        CONFIRMED,CANCELED,APPROVE;
    }
        
    public boolean orderStateChanged(String orderUuid, OrderState state, boolean notify, Date paidDate);

}
