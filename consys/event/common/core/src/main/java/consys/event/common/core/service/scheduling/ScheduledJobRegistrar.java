package consys.event.common.core.service.scheduling;

import com.google.common.collect.Maps;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 *
 * @author palo
 */
public class ScheduledJobRegistrar extends ScheduledTaskRegistrar implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledJobRegistrar.class);
    // data
    private boolean initialized = false;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (!initialized) {
            Map<String, ScheduledJob> jobs = event.getApplicationContext().getBeansOfType(ScheduledJob.class);
            Map<Runnable, String> cronTasks = Maps.newHashMapWithExpectedSize(jobs.size());

            for (Map.Entry<String, ScheduledJob> entry : jobs.entrySet()) {
                ScheduledJob scheduledJob = entry.getValue();
                logger.info("Scheduler '{}' registred for '{}'", scheduledJob.getName(), scheduledJob.getCron());
                cronTasks.put(scheduledJob, scheduledJob.getCron());
            }

            // nastavime
            setCronTasks(cronTasks);

            // pustime namapovanie
            afterPropertiesSet();
        }
    }
}
