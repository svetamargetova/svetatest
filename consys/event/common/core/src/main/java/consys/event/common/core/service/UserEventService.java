package consys.event.common.core.service;

import consys.admin.event.ws.EventUserResponse;
import consys.admin.event.ws.Relation;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.Invitation;
import java.util.List;

/**
 *
 * @author palo
 */
public interface UserEventService {

    /* ------------------------------------------------------------------------ */
    /* --- C R E A T E --- */
    /* ------------------------------------------------------------------------ */
    /**
     * Vytvori alebo nacita uzivatela z databaze na zaklade vysledku pozvanky
     * ktora bola vratena z administracie. Je to vysledok volania webovej sluzby
     * na nacitane detailu uzivatela ak sa pracuje s uzivatelom o ktorom sa nevie
     * ci je uz v evente prihlaseny.
     * Najskor sa metoda pozre podla UUID do databaze a ak tam uzivatel nie je
     * tak ho vytvori.
     *
     * @param response vysledok dotazu do administracie
     */
    public UserEvent createUserOrLoad(EventUserResponse response)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Vytvori alebo nacita uzivatela po dotaze na administraciu. Na zaklade
     * existencie resp. neexistencie uzivatla v db eventu len prevola resp.
     * vytvori uzivatela.
     * Vysledny dotaz do administracie ma nastaveny priznak
     * <code>invite</code>
     * false a sluzi len na nacitanie detailu uzivatela priradne jeho vytvorenie
     * a pozvanky do takeplace. Tj nevytvori priamo pozvanku do eventu.
     * Metoda najskor zisti ci uuid uzivatela uz nie je v db eventu, ak ano tak
     * neprevolava dotaz do administracie ale natahuje data z eventu.
     *
     *
     * @throws ServiceExecutionFailed ak neobsahuje odpoved
     */
    public UserEvent createUserOrLoad(Invitation invitation)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Vytvori alebo nacita uzivatelov po dotaze na administraciu. Na zaklade
     * existencie resp. neexistencie uzivatela v db eventu len prevola resp.
     * vytvori uzivatela.
     * Vysledny dotaz do administracie ma nastaveny priznak
     * <code>invite</code>
     * false a sluzi len na nacitanie detailu uzivatela priradne jeho vytvorenie
     * a pozvanky do takeplace. Tj nevytvori priamo pozvanku do eventu.
     * Metoda najskor zisti ci uuid uzivatela uz nie je v db eventu, ak ano tak
     * neprevolava dotaz do administracie ale natahuje data z eventu.
     *
     *
     * @throws ServiceExecutionFailed ak je pocet pozvanok odlisny ako vratenych
     */
    public List<UserEvent> createUserOrLoad(List<Invitation> invitations)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Vytvori uzivatela bez akehokolvek testovania existencie v administracii.
     *
     * <strong> Pouzivat toto volanie len vynimocne z inych sluzieb </strong>
     *
     * @param userEvent uzivatel ktory sa ma vytvorit
     * @throws ServiceExecutionFailed ak uzivatel so zadanym UUID uz existuje
     * @throws RequiredPropertyNullException ak chybaju niektore nalezitosti
     */
    public void createUser(UserEvent userEvent)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * V pripade ze uzivatel existuje tak nacita uzivatela podla uuid alebo 
     * ho vytvori.
     * 
     * @param userEvent
     * @throws ServiceExecutionFailed
     * @throws RequiredPropertyNullException 
     */
    public UserEvent createUserOrLoad(UserEvent userEvent)
            throws ServiceExecutionFailed, RequiredPropertyNullException;

    /**
     * Vytvori uzivatela eventu s pravami.
     */
    public UserEvent createUserEventWithRights(UserEvent user, String... rights)
            throws NoRecordException;

    /* ------------------------------------------------------------------------ */
    /* --- L O A D --- */
    /* ------------------------------------------------------------------------ */
    /**
     * Metoda nacita uzivatela z databaze
     */
    public UserEvent loadUserEvent(String uuid) throws NoRecordException;

    /**
     * Zisti ci ma uzivate s
     * <code>uuid</code> priradene pravo s nazvom
     * <code>rightName</code>.
     */
    public boolean hasUserRight(String uuid, String rightName);

    /* ------------------------------------------------------------------------ */
    /* --- L I S T --- */
    /* ------------------------------------------------------------------------ */
    /**
     * Nacita vsetkych uzivatelov ktory maju pravo
     * <code>rightName</code>.
     */
    public List<UserEvent> listUserEventsWithRight(String rightName) throws NoRecordException, RequiredPropertyNullException;

    /** Nacte vsechny uzivatele se zadanymi uuid */
    public List<UserEvent> listUserEventsByUuids(List<String> uuids) throws RequiredPropertyNullException;

    /* ------------------------------------------------------------------------ */
    /* --- U P D A T E --- */
    /* ------------------------------------------------------------------------ */
    /**
     * Update
     */
    public void updateUserEvent(UserEvent ue) throws NoRecordException;

    /**
     * Upravi nastvitelne vlastnosti uzivatelskej entity.
     *
     * @param userUuid uuid uzivatela
     * @param name meno uzivatela
     * @param position nazev pozice uzivatelele v organizaci
     * @param organization nazev organizace za kterou se uzivatel ucastni
     * @throws NoRecordException
     */
    public void updateUserEvent(String userUuid, String name, String position, String organization) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Prida uzivatelovi do jeho privatnej skupiny pravo ak ho tam este nema
     */
    public void addUserPersonalRight(String rightName, UserEvent ue) throws NoRecordException, RequiredPropertyNullException;

    public void removeUserPersonalRight(String rightName, UserEvent ue) throws NoRecordException, RequiredPropertyNullException;

    /**
     * Metoda hromadne upravi relacie uzivatelov na event v administracii
     *
     * @param nova relacia ktora sa ma nastavit uzivatelom
     * @param message ak nie je null tak sa sprava posle uzivatelom na email
     * @param users uzivatelia ktorym sa ma relacia zmenit
     */
    public void doUpdateRelationToEvent(Relation newRelation, String message, List<String> users)
            throws ServiceExecutionFailed;
}
