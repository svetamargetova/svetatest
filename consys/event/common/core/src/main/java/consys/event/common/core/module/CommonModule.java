package consys.event.common.core.module;

import consys.common.core.bo.SystemProperty;
import consys.common.utils.collection.Lists;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.api.right.EventCommonModuleRoleModel;
import consys.event.common.api.right.Role;
import consys.event.common.core.AbstractEventModuleRegistrator;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Palo
 */
public class CommonModule extends AbstractEventModuleRegistrator {

    public static final String MODULE_NAME = "COMMON";
    public static final String DEFAULT_ORGANIZATION_COMMITTEE_NAME = "Organization";
    public static final String DEFAULT_PROGRAMME_COMMITTEE_NAME = "Programme";

    // Pouziva nativny public
    @Override
    public String getSchemaName() {
        return null;
    }

    private void createCommitteeIfNotExists(Session session, String committeeName) throws HibernateException {
        if (!existsCommittee(session, committeeName)) {
            Committee c = new Committee();
            c.setName(committeeName);
            c.setGroup(new Group());
            c.setChairGroup(new Group());
            session.save(c);
        }
    }

    private boolean existsCommittee(Session s, String name) {
        Long id = (Long) s.createQuery("select c.id from Committee c where c.name = :NAME").setString("NAME", name).uniqueResult();
        return id == null ? false : true;
    }

    @Override
    public void doAfterSessionFactoryCreate(Session session) {
        // Create Default committees
        createCommitteeIfNotExists(session, DEFAULT_ORGANIZATION_COMMITTEE_NAME);
        createCommitteeIfNotExists(session, DEFAULT_PROGRAMME_COMMITTEE_NAME);
    }

    @Override
    public List<SystemProperty> getModuleProperties() {
        List<SystemProperty> propertyList = Lists.newArrayList();
        registerProperty(CommonProperties.EVENT_CURRENCY, true, propertyList);
        registerProperty(CommonProperties.EVENT_VAT_RATE, true, propertyList).setValue("0");
        registerProperty(CommonProperties.EVENT_NAME, false, propertyList);
        registerProperty(CommonProperties.EVENT_FROM, false, propertyList);
        registerProperty(CommonProperties.EVENT_TO, false, propertyList);
        registerProperty(CommonProperties.EVENT_IMAGE_UUID, false, propertyList);
        registerProperty(CommonProperties.EVENT_ADDRESS, false, propertyList);
        registerProperty(CommonProperties.EVENT_PARTNER_FOOTER_IMAGE, true, propertyList);
        registerProperty(CommonProperties.EVENT_PARTNER_PANEL_IMAGE, true, propertyList);
        registerProperty(CommonProperties.EVENT_EMAIL, true, propertyList);
        registerProperty(CommonProperties.EVENT_TIMEZONE, true, propertyList).setValue("Europe/London");
        registerProperty(CommonProperties.EVENT_UUID, false, propertyList);
        registerProperty(CommonProperties.EVENT_PROFILE_IMAGE_UUID, false, propertyList);
        registerProperty(CommonProperties.EVENT_ACRONYM, false, propertyList);
        registerProperty(CommonProperties.EVENT_SHORT_NAME, false, propertyList);
        registerProperty(CommonProperties.EVENT_UNIVERSAL_NAME, false, propertyList);
        registerProperty(CommonProperties.EVENT_TAGS, false, propertyList);
        registerProperty(CommonProperties.EVENT_YEAR, false, propertyList);
        registerProperty(CommonProperties.EVENT_SERIES, false, propertyList);
        registerProperty(CommonProperties.EVENT_DESCRIPTION, false, propertyList);
        registerProperty(CommonProperties.EVENT_ADDRESS_COUNTRY, false, propertyList);
        registerProperty(CommonProperties.EVENT_ADDRESS_STREET, false, propertyList);
        registerProperty(CommonProperties.EVENT_ADDRESS_PLACE, false, propertyList);
        registerProperty(CommonProperties.EVENT_ADDRESS_CITY, false, propertyList);
        registerProperty(CommonProperties.EVENT_ADDRESS_ZIP, false, propertyList);
        registerProperty(CommonProperties.EVENT_WEB, false, propertyList);
        registerProperty(CommonProperties.EVENT_WEB_PHONE, true, propertyList);
        registerProperty(CommonProperties.EVENT_WEB_EMAIL, true, propertyList);
        registerProperty(CommonProperties.EVENT_COMMERCIAL, true, propertyList).setValueFromBoolean(true);
        registerProperty(CommonProperties.EVENT_INVOICING, true, propertyList).setValueFromBoolean(true);
        registerProperty(CommonProperties.EVENT_CORPORATION, false, propertyList).setValue("");
        registerProperty(CommonProperties.EVENT_VIDEO_UPLOAD, true, propertyList).setValue("false");
        return propertyList;
    }

    private SystemProperty registerProperty(String propertyName, boolean toClient, List<SystemProperty> register) {
        SystemProperty property = new SystemProperty();
        property.setKey(propertyName);
        property.setValue("");
        property.setToClient(toClient);
        register.add(property);
        return property;
    }

    /**
     * Defaultne nastavenie:
     * ROLE_ADMINISTRATOR - visible true, forall false
     * ROLE_OWNER - visible true, forall false
     */
    @Override
    public Module getModule() {
        Module m = new Module();
        m.setName(MODULE_NAME);

        EventCommonModuleRoleModel rights = new EventCommonModuleRoleModel();

        for (Role r : rights.getRoles()) {
            ModuleRight right = new ModuleRight();
            right.setName(r.getName());
            right.setUniqueValue(r.getIdentifier());
            m.getRights().add(right);
        }
        return m;
    }
}
