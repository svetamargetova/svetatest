package consys.event.common.core.bo.thumb;

/**
 * Genericky thumb pre list item
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUuidItem {

    private String name;
    private String uuid;

    public ListUuidItem(String name, String uuid) {
        this.name = name;
        this.uuid = uuid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }
}
