package consys.event.common.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.UuidProvider;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.ParticipantManager;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.ParticipantManagerDao;
import consys.event.common.core.dao.UserEventDao;
import consys.event.common.core.service.ParticipantManagerService;
import java.util.ArrayList;
import java.util.List;
import net.notx.ws.user.Contact;
import net.notx.ws.user.CreateOrUpdateUserRequest;
import net.notx.ws.user.User;
import net.notx.ws.user.UserWebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class ParticipantManagerServiceImpl implements ParticipantManagerService {

    private Logger log = LoggerFactory.getLogger(ParticipantManagerServiceImpl.class);
    // dao
    @Autowired
    private ParticipantManagerDao participantManagerDao;
    @Autowired
    private UserEventDao userEventDao;
    @Autowired
    private UserWebService notxUserWebService;

    @Override
    public boolean checkManagedUser(String uuidManager, String uuidParticipant) {
        return participantManagerDao.checkManagedUser(uuidManager, uuidParticipant);
    }

    @Override
    public ParticipantManager createNewManagedUser(String uuidManager, UserEvent userEvent)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {

        UserEvent manager = userEventDao.loadUserEvent(uuidManager);

        // nastavime mail na managera
        if (userEvent.getEmail() == null || userEvent.getEmail().isEmpty()) {
            userEvent.setEmail(manager.getEmail());
        }
        // nastavime zbyle potrebne promenne
        userEvent.setPrivateGroup(new Group());
        userEvent.setUuid(UuidProvider.getUuid());
        userEvent.setSystemUser(false);

        ParticipantManager pm = new ParticipantManager();
        pm.setManager(manager);
        pm.setParticipant(userEvent);

        try {
            userEventDao.create(userEvent);
            participantManagerDao.create(pm);
        } catch (Exception ex) {
            log.error("Error creating UserEvent or ParticipantManager", ex);
            throw new ServiceExecutionFailed();
        }

        // zaregistrujeme do notx
        notxUserWebService.createOrUpdateUser(prepareNotxCreateUserRequest(userEvent));

        return pm;
    }

    /** pripravi request pro pridani spravovaneho uzivatele do notx */
    private CreateOrUpdateUserRequest prepareNotxCreateUserRequest(UserEvent user) {
        User notxUser = UserWebService.OBJECT_FACTORY.createUser();
        notxUser.setLang("en");
        notxUser.setUserId(user.getUuid());
        notxUser.setName(user.getFullName());
        Contact email = UserWebService.OBJECT_FACTORY.createContact();
        email.setKey("email");
        email.setValue(user.getEmail());
        notxUser.getContacts().add(email);

        CreateOrUpdateUserRequest request = UserWebService.OBJECT_FACTORY.createCreateOrUpdateUserRequest();
        request.setUser(notxUser);
        return request;
    }

    @Override
    public List<UserEvent> listManagedParticipants(String uuidManager) {
        List<ParticipantManager> list = participantManagerDao.listManagedParticipants(uuidManager);
        List<UserEvent> result = new ArrayList<UserEvent>();
        for (ParticipantManager pm : list) {
            result.add(pm.getParticipant());
        }
        return result;
    }
}
