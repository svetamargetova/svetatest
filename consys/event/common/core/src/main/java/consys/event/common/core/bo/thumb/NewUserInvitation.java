/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.common.core.bo.thumb;

import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.EventUserRequest;

/**
 *
 * @author palo
 */
public class NewUserInvitation extends Invitation{

    private String email;
    private String name;

    public NewUserInvitation(String email, String name) {
        this.email = email;
        this.name = name;
    }


    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    @Override
    public EventUserRequest toRequest() {
        EventUserRequest req = AdminEventWebService.OBJECT_FACTORY.createEventUserRequest();
        req.setMessage(getMessage());
        req.setUserName(getName());
        req.setUserEmail(getEmail());
        return req;
    }
}
