package consys.event.common.core.servlet;

import consys.common.core.service.SystemPropertyService;
import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadProgressMonitor;
import consys.common.core.so.UploadStatus;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.bo.enums.EventVideoState;
import consys.event.common.core.service.VideoService;
import consys.event.common.core.service.VideoUploadService;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author pepa
 */
public class UploadVideoServlet extends HttpServlet {

    private static final long serialVersionUID = -3402630427795267548L;
    // konstanty
    private static final String MY_ID = "myid";
    private static final String UID = "uid";
    private static final String EID = "eid";
    private static final String LENGTH = "l";
    // logger
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    // sluzby
    @Autowired
    private VideoUploadService videoUploadService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private SystemPropertyService systemPropertyService;
    // data
    private String bucketName;
    private UploadProgressMonitor monitor;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);

        bucketName = config.getInitParameter("Video.S3.Bucket");
        if (StringUtils.isBlank(bucketName)) {
            throw new IllegalArgumentException("Missing bucket AWS S3 video bucket name");
        }
        logger.info("{} initialized to: {}", this.getClass().getName(), bucketName);

        monitor = (UploadProgressMonitor) ctx.getBean("uploadProgressMonitor");
    }

    /** Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response) {
        String lengthParam = request.getParameter(LENGTH);
        String attributeName = null;

        try {
            if (StringUtils.isBlank(lengthParam)) {
                logger.error("Missing parameter 'l' with length of video file");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print("HTTP ERROR: 400");
                return;
            }

            long length;
            try {
                length = Long.parseLong(lengthParam);
            } catch (NumberFormatException ex) {
                logger.error("Parameter with length of video file is not number");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.getWriter().print("HTTP ERROR: 403");
                return;
            }

            Boolean uploadEnabled;
            try {
                String upload = systemPropertyService.loadSystemProperty(CommonProperties.EVENT_VIDEO_UPLOAD).getValue();
                uploadEnabled = Boolean.parseBoolean(upload);
            } catch (Exception ex) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                logger.error("Missing property EVENT_VIDEO_UPLOAD");
                response.getWriter().print("HTTP ERROR: 403");
                return;
            }

            if (!uploadEnabled) {
                logger.info("Upload video file is not enabled");
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                response.getWriter().print("HTTP ERROR: 403");
                return;
            }

            final String user = request.getParameter(UID);
            final String event = request.getParameter(EID);
            final String uuid = request.getParameter(MY_ID);
            final String name = event + "-" + uuid;

            attributeName = user + event + uuid + UploadProgressServlet.VIDEO_UPLOAD_SESSION_KEY_PART;

            while (monitor.isUploadingUser(user)) {
                try {
                    // čekáme než skončí předchozí upload
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    logger.error("sleep error", ex);
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    response.getWriter().print("HTTP ERROR: 500");
                    return;
                }
            }

            ServletFileUpload upload = new ServletFileUpload();
            try {
                FileItemIterator iter = upload.getItemIterator(request);
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();

                    logger.info(name + " " + item.getContentType() + " from " + user);

                    // vytvorime status pro zjistovani stavu uploadu
                    UploadProgress status = new UploadProgress();
                    status.setTotalBytes(length);
                    monitor.addUploadProgress(attributeName, status);

                    // poslat na amazony - zacneme uploadovat
                    UploadStatus result = videoUploadService.uploadVideo(bucketName, name, item.openStream(),
                            item.getContentType(), status);

                    if (UploadStatus.UPLOADED.equals(result)) {
                        // ulozit vysledek do db
                        EventVideo ev = videoService.loadVideoByUuid(uuid);
                        ev.setState(EventVideoState.UPLOADED);
                        ev.setMediaType(item.getContentType());
                        videoService.updateVideo(ev);
                        response.setStatus(HttpServletResponse.SC_OK);
                        logger.info("Upload successfull");
                    } else if (UploadStatus.CANCELED.equals(result)) {
                        logger.info("Upload canceled");
                        response.setStatus(HttpServletResponse.SC_OK);
                    } else {
                        logger.error("Uploaded not successfull");
                        EventVideo ev = videoService.loadVideoByUuid(uuid);
                        ev.setState(EventVideoState.PROCESS_ERROR);
                        videoService.updateVideo(ev);
                        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        response.getWriter().print("HTTP ERROR: 500");
                    }

                    break; // pouze jeden soubor
                }
            } catch (Exception e) {
                logger.error("Upload File error", e);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                response.getWriter().print("HTTP ERROR: 500");
            }
        } catch (IOException ex) {
            logger.error("error", ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            try {
                response.getWriter().print("HTTP ERROR: 500");
            } catch (IOException e) {
                // s timto uz nic nenadelame
            }
        } finally {
            if (attributeName != null) {
                monitor.removeUploadProgress(attributeName);
            }
        }
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }
}
