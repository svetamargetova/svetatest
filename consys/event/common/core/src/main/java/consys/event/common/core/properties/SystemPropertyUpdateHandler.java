package consys.event.common.core.properties;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.bo.SystemProperty;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface SystemPropertyUpdateHandler { 
    /**
     * Identifikuje obsluhu pre systemovu property
     */
    public String getKey();

    /**
     */
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue;
}
