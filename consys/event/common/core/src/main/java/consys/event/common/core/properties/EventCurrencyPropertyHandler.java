package consys.event.common.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.common.utils.enums.Currency;
import consys.event.common.api.properties.CommonProperties;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventCurrencyPropertyHandler implements SystemPropertyUpdateHandler {

    @Override
    public String getKey() {
        return CommonProperties.EVENT_CURRENCY;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
        Currency[] currencies = Currency.values();
        for (int i = 0; i < currencies.length; i++) {
            Currency c = currencies[i];
            if (newValue.equalsIgnoreCase(c.name())) {                
                return;
            }
        }
        throw new IllegalPropertyValue("Not valid Currency " + newValue);
    }
}
