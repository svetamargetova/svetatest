package consys.event.common.core.service.impl;

import consys.common.core.service.impl.AbstractService;
import consys.event.common.api.list.VideoFilesUserList;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.dao.EventVideoDao;
import consys.event.common.core.service.VideoUserListService;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class VideoUserListServiceImpl extends AbstractService implements VideoUserListService {

    @Autowired
    private EventVideoDao videoDao;

    @Override
    public String getTag() {
        return VideoFilesUserList.LIST_TAG;
    }

    @Override
    public List<EventVideo> listItems(Constraints constraints, Paging paging) {
        return videoDao.listUserVideos(constraints, paging);
    }

    @Override
    public int listAllCount(Constraints constraints) {
        return videoDao.loadUserVideoCount(constraints).intValue();
    }

    @Override
    public List<EventVideo> listItemsExport(Constraints constraints, Paging paging) {
        return videoDao.listUserVideos(constraints, paging);
    }
}
