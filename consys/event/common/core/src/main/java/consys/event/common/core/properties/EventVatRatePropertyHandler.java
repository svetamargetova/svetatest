package consys.event.common.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.api.properties.CommonProperties;
import consys.payment.webservice.EventWebService;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventVatRatePropertyHandler implements SystemPropertyUpdateHandler {

    private EventWebService eventWebService;

    @Override
    public String getKey() {
        return CommonProperties.EVENT_VAT_RATE;
    }

    // hodnota musi byt od 0 - 100
    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {

        try {
            Integer vat = Integer.valueOf(newValue);
            if (vat >= 0 && vat <= 100) {                                
                return;
            }
        } catch (NumberFormatException e) {
        }
        throw new IllegalPropertyValue("Not valid value 0 - 100 instead of " + newValue);
    }

    /**
     * @return the eventWebService
     */
    public EventWebService getEventWebService() {
        return eventWebService;
    }

    /**
     * @param eventWebService the eventWebService to set
     */
    public void setEventWebService(EventWebService eventWebService) {
        this.eventWebService = eventWebService;
    }
}
