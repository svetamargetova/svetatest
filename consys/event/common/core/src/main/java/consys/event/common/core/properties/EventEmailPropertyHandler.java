package consys.event.common.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.api.properties.CommonProperties;
import java.util.regex.Pattern;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventEmailPropertyHandler implements SystemPropertyUpdateHandler {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private Pattern pattern;

    public EventEmailPropertyHandler() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }



    @Override
    public String getKey() {
        return CommonProperties.EVENT_EMAIL;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
         if(pattern.matcher(newValue).matches()){
            return;
         }
         throw new IllegalPropertyValue("Validate email address failed");
    }
}
