package consys.event.common.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.bo.enums.EventVideoState;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface EventVideoDao extends GenericDao<EventVideo> {

    /** nacte seznam vsech uploadovanych videii */
    List<EventVideo> listVideos(Constraints constraints, Paging paging);

    /** nacte seznam vsech uploadovanych videii */
    List<EventVideo> listUserVideos(Constraints constraints, Paging paging);

    /** nacte videa ktera nebyla predana ke zpracovani */
    List<EventVideo> listVideosByState(EventVideoState state);

    /** nacte video podle systemoveho nazvu */
    EventVideo loadVideo(String systemName) throws NoRecordException;

    /** nacte video podle uuid */
    EventVideo loadVideoByUuid(String uuid) throws NoRecordException;

    /** vraci pocet videi */
    Long loadVideoCount(Constraints constraints);

    /** vraci pocet videi */
    Long loadUserVideoCount(Constraints constraints);

    /** smaze vsechny vazby spravovanych uzivatelu (urceno pro pouziti pri klonovani) */
    int deleteAllVideoRecords();
}
