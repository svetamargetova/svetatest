package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Palo
 */
public class Module implements ConsysObject {

    private static final long serialVersionUID = 2278924917598348862L;
    private Long id;
    private String name;
    private Set<ModuleRight> rights;

    public Module() {
        rights = new HashSet<ModuleRight>(); 
    }



    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Module[id=" + id + " name=" + name + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        Module e = (Module) obj;
        return e.name.equalsIgnoreCase(name);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    /**
     * @return the rights
     */
    public Set<ModuleRight> getRights() {
        return rights;
    }

    /**
     * @param rights the rights to set
     */
    public void setRights(Set<ModuleRight> rights) {
        this.rights = rights;
    }
}
