package consys.event.common.core.service.list;

import java.util.List;

/**
 * Rozhranie pre sluzbu realizujucu jeden zoznam. Obsahuje Tag ktory zoznam
 * identifikuje a sluzbu ktora na zaklade obmedzeni vracia vysledky.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ListService<T, E> {

    public String getTag();

    public List<T> listItems(Constraints constraints, Paging paging);

    public int listAllCount(Constraints constraints);

    public List<E> listItemsExport(Constraints constraints, Paging paging);
}
