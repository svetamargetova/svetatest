package consys.event.common.core.superlectures.so;

import java.util.HashMap;
import java.util.Map;

/**
 * Jedna osoba
 * @author pepa
 */
public class SuperlecturesPerson {

    // konstanty
    public static final String ITEM_TWITTER = "twitter";
    public static final String ITEM_FACEBOOK = "facebook";
    public static final String ITEM_BLOG = "blog";
    public static final String ITEM_WWW = "www";
    public static final String ITEM_INSTITUTION = "institution";
    public static final String ITEM_GOOGLEP = "googlep";
    public static final String ITEM_LINKEDIN = "linkedin";
    public static final String ITEM_VIMEO = "vimeo";
    public static final String ITEM_PHONE = "phone";
    public static final String ITEM_EMAIL = "email";
    // data
    private String name;
    private Map<String, SuperlecturesPersonItem> personItems;

    public SuperlecturesPerson() {
        personItems = new HashMap<String, SuperlecturesPersonItem>();
    }

    public SuperlecturesPerson(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, SuperlecturesPersonItem> getPersonItems() {
        return personItems;
    }

    @Override
    public String toString() {
        return name;
    }
}
