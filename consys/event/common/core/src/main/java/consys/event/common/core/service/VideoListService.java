package consys.event.common.core.service;

import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.service.list.ListService;

/**
 *
 * @author pepa
 */
public interface VideoListService extends ListService<EventVideo, EventVideo> {
}
