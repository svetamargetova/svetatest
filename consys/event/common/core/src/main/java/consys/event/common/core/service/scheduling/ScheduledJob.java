package consys.event.common.core.service.scheduling;

import consys.common.core.exception.ConsysException;
import consys.event.overseer.connector.*;
import consys.event.overseer.connector.providers.AppContextEventProvider;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstraktna trieda ktora implementuje naplanovanu pracu nad vsetkymi databazami
 * ktore overseer spravuje.
 * <p/>
 * @author palo
 */
public abstract class ScheduledJob implements Runnable {

    protected Logger logger = LoggerFactory.getLogger(ScheduledJob.class);
    @Autowired
    private OverseerTransactionCallbackExecutor callbackExecutor;
    @Autowired
    private OverseerSessionFactoryPool factoryPool;
    @Autowired
    private AppContextEventProvider contextEventProvider;
    /**
     * Vyraz v CRON notacii vymedzujuci kedy sa ma pustat job. Ak sa bude rozsirovat
     * a dalsie vlasntosti ako fixedDelay a pod. zvazit pouzitie priamo Trigger.
     * <p/>
     */
    private String cron;

    @Override
    public void run() {
        Set<EventId> events = factoryPool.getOverseerManagedEvents();

        OverseerTransactionCallbackWithoutResult callback = new OverseerTransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(EventId event) throws ConsysException {
                doJobForEvent(event);
            }
        };

        logger.debug("Executing scheduled job '{}'", getName());

        for (EventId eventId : events) {
            logger.trace("...executing for '{}'", eventId.getId());
            try {
                // injectujeme kontext
                EventRequestContext ctx = new EventRequestContext();
                ctx.initContext(null, eventId.getId());
                contextEventProvider.injectContext(ctx);
                callback.setEventId(eventId);
                callbackExecutor.execute(callback);
            } catch (Exception e) {
                logger.error("Exception while executing scheduled job '" + getName() + "'", e);
            } finally {
                contextEventProvider.injectContext(null);
            }
        }
    }

    protected abstract void doJobForEvent(EventId event) throws ConsysException;

    /**
     * Vrati nazov jobu
     * <p/>
     * @return
     */
    protected abstract String getName();

    /**
     * Vyraz v CRON notacii vymedzujuci kedy sa ma pustat job. Ak sa bude rozsirovat
     * a dalsie vlasntosti ako fixedDelay a pod. zvazit pouzitie priamo Trigger.
     * <p/>
     * @return the cron
     */
    public String getCron() {
        return cron;
    }

    /**
     * Vyraz v CRON notacii vymedzujuci kedy sa ma pustat job. Ak sa bude rozsirovat
     * a dalsie vlasntosti ako fixedDelay a pod. zvazit pouzitie priamo Trigger.
     * <p/>
     * @param cron the cron to set
     */
    public void setCron(String cron) {
        this.cron = cron;
    }
}
