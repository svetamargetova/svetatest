package consys.event.common.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import java.util.List;

/**
 *
 * @author Palo
 */
public interface UserEventDao extends GenericDao<UserEvent> {

    /** nacte uzivatele podle zadaneho uuid */
    public UserEvent loadUserEvent(String uuid) throws NoRecordException;

    /** nacte uzivatele podle zadanych uuid */
    public List<UserEvent> listByUuid(List<String> uuids);
}
