package consys.event.common.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.bo.enums.EventVideoState;
import consys.event.common.core.dao.EventVideoDao;
import consys.event.common.core.service.VideoService;
import consys.event.common.core.service.VideoUploadService;
import consys.event.common.core.superlectures.SuperlecturesApiConnector;
import consys.event.common.core.superlectures.so.SuperlecturesAddRequest;
import consys.event.common.core.superlectures.so.SuperlecturesAddResponse;
import consys.event.common.core.superlectures.so.SuperlecturesGetResponse;
import consys.event.common.core.superlectures.so.SuperlecturesResponseStatus;
import consys.event.common.core.youtube.YouTubeManager;
import consys.event.common.core.youtube.YouTubeProcessingState;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class VideoServiceImpl implements VideoService {

    // logger
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    // dao
    @Autowired
    private EventVideoDao videoDao;
    // servicy
    @Autowired
    private SystemPropertyService systemPropertyService;
    @Autowired
    private VideoUploadService videoUploadService;
    @Autowired
    private YouTubeManager youTubeManager;
    // data
    private String videoServerUrl;
    private String videoBucket;

    @Override
    public void createVideo(EventVideo video) {
        videoDao.create(video);
    }

    @Override
    public EventVideo loadVideo(String systemName) throws NoRecordException {
        return videoDao.loadVideo(systemName);
    }

    @Override
    public EventVideo loadVideoByUuid(String uuid) throws NoRecordException {
        return videoDao.loadVideoByUuid(uuid);
    }

    @Override
    public void updateVideo(EventVideo video) {
        videoDao.update(video);
    }

    @Override
    public void updateSuperlecturesProcessing() {
        // kontrola stavu zpracovani
        List<EventVideo> processing = videoDao.listVideosByState(EventVideoState.SUPERLECTURES_PROCESSING);
        for (EventVideo ev : processing) {
            try {
                SuperlecturesGetResponse response = SuperlecturesApiConnector.getVideo(ev.getSuperlecturesHash());
                if (SuperlecturesResponseStatus.VIDEO_IS_READY.equals(response.getStatus())) {
                    ev.setState(EventVideoState.SUPERLECTURES_PROCESSED);
                    ev.setVideoUrl(response.getSuperlectureVideoUrl());
                    updateVideo(ev);
                } else if (isSuperlecturesError(response.getStatus())) {
                    logger.error("Error superlectures GET response: " + response.getStatus() + " " + ev);
                    ev.setState(EventVideoState.PROCESS_ERROR);
                    updateVideo(ev);
                }
            } catch (Exception ex) {
                logger.error("Error superlectures GET", ex);
            }
        }

        // nahrani videi pro zpracovani
        List<EventVideo> uploaded = videoDao.listVideosByState(EventVideoState.TAKEPLACE_PROCESSED);
        if (uploaded.size() > 0) {
            try {
                final String eventName = systemPropertyService.loadSystemProperty(CommonProperties.EVENT_NAME).getValue();
                for (EventVideo ev : uploaded) {
                    if (StringUtils.isBlank(ev.getCustomName())) {
                        // neni vyplnen uzivatelsky nazev
                        continue;
                    }
                    //String videoUrl = videoServerUrl + "/" + ev.getSystemName();
                    String id = YouTubeManager.idFromYouTubeId(ev.getYouTubeId());
                    String videoUrl = "http://www.youtube.com/watch?v=" + id;
                    if (!tryAddVideoToSuperlectures(videoUrl, ev, eventName)) {
                        logger.error("Unable add video to superlectures: " + ev.getSystemName());
                        // TODO: po nekolikanasobnem opakovani chyby pokusu nahrati, zmenit stav na ERROR
                    }
                }
            } catch (NoRecordException ex) {
                logger.error("Unable load EVENT_NAME");
            }
        }

        // smazani zdroju zpracovanych videi na s3
        List<EventVideo> processed = videoDao.listVideosByState(EventVideoState.SUPERLECTURES_PROCESSED);
        for (EventVideo ev : processed) {
            videoUploadService.deleteVideo(videoBucket, ev.getSystemName());
        }
    }

    /** pokud se jedna o chybu */
    private boolean isSuperlecturesError(SuperlecturesResponseStatus status) {
        if (status == null) {
            return true;
        }
        switch (status) {
            case VIDEO_IN_ERROR:
            case VIDEO_NOT_EXISTS:
            case ERROR_ALREADY_ADDED:
            case ERROR_CORRUPTED_XML:
            case ERROR_NO_NAME_INFORMATION:
            case ERROR_NO_XML_DATA:
            case ERROR_UNKNOWN_STATE:
                return true;
            default:
                return false;
        }
    }

    /** zkusi pridat video do superlectures */
    private boolean tryAddVideoToSuperlectures(String videoUrl, EventVideo ev, String eventName) {
        try {
            SuperlecturesAddRequest sr = new SuperlecturesAddRequest();
            sr.setVideoUrl(videoUrl);
            sr.setVideoName(ev.getCustomName());
            sr.setDescription(ev.getDescription());
            sr.setEventName(eventName);
            sr.setDatetimeRecorded(new Date());
            sr.parseAuthors(ev.getAuthors());
            sr.parseSpeakers(ev.getSpeakers());

            // zavolame na superlectures, at se zacne se zpracovanim
            SuperlecturesAddResponse sres = SuperlecturesApiConnector.addVideo(sr);
            if (sres == null) {
                logger.error("Unknown response from superlectures");
                return false;
            } else {
                if (sres.isOkResponse()) {
                    logger.info("Video added to superlectures: " + ev);
                    ev.setSuperlecturesHash(sres.getHash());
                    ev.setState(EventVideoState.SUPERLECTURES_PROCESSING);
                    updateVideo(ev);
                    return true;
                } else {
                    // neco neni v poradku
                    logger.error("Error state from superlectures: " + sres.getStatus());
                    return false;
                }
            }
        } catch (Exception ex) {
            logger.error("Error while communicating with superlectures", ex);
            return false;
        }
    }

    @Override
    public void updateYouTubeProcessing() {
        // kontrola zpracovani a aktualizace stavu
        List<EventVideo> processing = videoDao.listVideosByState(EventVideoState.TAKEPLACE_PROCESSING);
        for (EventVideo ev : processing) {
            try {
                YouTubeProcessingState state = youTubeManager.checkVideoProcessed(ev);
                if (YouTubeProcessingState.PROCESSED.equals(state)) {
                    ev.setState(EventVideoState.TAKEPLACE_PROCESSED);
                    updateVideo(ev);
                } else if (YouTubeProcessingState.FAILED.equals(state)) {
                    ev.setState(EventVideoState.PROCESS_ERROR);
                    updateVideo(ev);
                }
            } catch (Exception ex) {
                logger.error("YouTube check processing", ex);
            }
        }

        // nahrati uploadovanych videi na youtube
        List<EventVideo> toProcess = videoDao.listVideosByState(EventVideoState.UPLOADED);
        for (EventVideo ev : toProcess) {
            if (StringUtils.isBlank(ev.getCustomName())) {
                // neni vyplnen nazev videa
                continue;
            }
            try {
                ev.setYouTubeId(youTubeManager.uploadVideo(ev));
                ev.setState(EventVideoState.TAKEPLACE_PROCESSING);
                updateVideo(ev);
            } catch (Exception ex) {
                logger.error("YouTube processing", ex);
            }
        }
    }

    @Override
    public void deleteVideo(String systemName) throws NoRecordException {
        EventVideo eventVideo = videoDao.loadVideo(systemName);

        if (StringUtils.isNotBlank(eventVideo.getSuperlecturesHash())) {
            try {
                // smazeme ze superlectures
                SuperlecturesApiConnector.removeVideo(eventVideo.getSuperlecturesHash());
            } catch (Exception ex) {
                // zatim nebudeme resit, hlavne at je to pryc od nas
                logger.error("Superlectures remove video error", ex);
            }
        }

        if (StringUtils.isNotBlank(eventVideo.getYouTubeId())) {
            try {
                // smazeme z youtube
                youTubeManager.removeVideo(eventVideo.getYouTubeId());
            } catch (Exception ex) {
                // zatim nebudeme resit, hlavne at je to pryc od nas
                logger.error("Youtube remove video error", ex);
            }
        }

        if (!eventVideo.getState().equals(EventVideoState.SUPERLECTURES_PROCESSED)) {
            // jeste nebylo zpracovano superlectures a tak zustava na s3
            videoUploadService.deleteVideo(videoBucket, systemName);
        }

        // smazeme z databaze
        videoDao.delete(eventVideo);
    }

    public void setVideoServerUrl(String videoServerUrl) {
        this.videoServerUrl = videoServerUrl;
    }

    public void setVideoBucket(String videoBucket) {
        this.videoBucket = videoBucket;
    }
}
