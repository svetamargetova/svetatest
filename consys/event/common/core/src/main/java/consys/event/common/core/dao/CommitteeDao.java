/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core.dao;


import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.thumb.ListLongItem;
import java.util.List;

/**
 *
 * @author palo 
 */
public interface CommitteeDao extends GenericDao<Committee> {

    /**
     * Vrati seznam skupin pro event
     */
    public List<Committee> listCommittees();

    public List<ListLongItem> listCommitteesListThumbs();

    public Committee loadCommitteeWithAll(Long id) throws NoRecordException;

    public List<ListLongItem> listCommitteesForUser(String userUuid) throws NoRecordException;

    public int loadManagingCommittesCount(String userEvent);
}
