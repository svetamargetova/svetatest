package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.dao.DataFileDao;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Palo
 */
public class DataFileDaoImpl extends GenericDaoImpl<DataFile> implements DataFileDao {

    @Override
    public DataFile load(Long id) throws NoRecordException {
        return load(id, DataFile.class);
    }

    @Override
    public void deleteByUuid(String uuid) throws NoRecordException {
        Session session = session();
        String HQL = "delete from DataFile d where d.uuid=:uuid";
        Query q = session.createQuery(HQL).setString("uuid", uuid);
        if (q.executeUpdate() == 0) {
            throw new NoRecordException("DataFile with uuid " + uuid + " not exists!");
        }
    }

    @Override
    public DataFile loadByUuid(String uuid) throws NoRecordException {
        Session session = session();
        String HQL = "from DataFile d where d.uuid=:uuid";
        DataFile df = (DataFile) session.createQuery(HQL).setString("uuid", uuid).uniqueResult();
        if(df == null){
            throw new NoRecordException();
        }
        return df;

    }
}
