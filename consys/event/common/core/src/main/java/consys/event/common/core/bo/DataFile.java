package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;
import java.sql.Blob;
import java.util.Date;

/**
 *
 * @author Palo
 */
public class DataFile implements ConsysObject{
    private static final long serialVersionUID = -283568548391697477L;

    private Long id;
    private Long size;
    private String uuid;
    private Blob data;
    private String fileName;
    private String contentType;
    private Date dateInserted;
    private byte[] byteData;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the data
     */
    public Blob getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Blob data) {
        this.data = data;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @param contentType the contentType to set
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * @return the dateInserted
     */
    public Date getDateInserted() {
        return dateInserted;
    }

    /**
     * @param dateInserted the dateInserted to set
     */
    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    /**
     * @return the byteData
     */
    public byte[] getByteData() {
        return byteData;
    }

    /**
     * @param byteData the byteData to set
     */
    public void setByteData(byte[] byteData) {
        this.byteData = byteData;
    }

    @Override
    public String toString() {
        return String.format("File[name=%s type=%s uuid=%s]", fileName,contentType,uuid);
    }

    @Override
    public boolean equals(Object obj) {
          if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        DataFile e = (DataFile) obj;
        return uuid.equalsIgnoreCase(e.uuid);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    
    /**
     * @return the size
     */
    public Long getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(Long size) {
        this.size = size;
    }

    

}
