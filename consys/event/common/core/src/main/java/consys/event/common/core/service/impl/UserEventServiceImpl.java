package consys.event.common.core.service.impl;

import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.EventUserRequest;
import consys.admin.event.ws.EventUserResponse;
import consys.admin.event.ws.LoadUsersToEventRequest;
import consys.admin.event.ws.Relation;
import consys.admin.event.ws.UpdateUserRelationToEventRequest;
import consys.admin.event.ws.UpdateUserRelationToEventResponse;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.NotX;
import consys.common.core.service.NotxService;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.collection.Lists;
import consys.event.common.api.notx.CommonTags;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.dao.GroupDao;
import consys.event.common.core.dao.ModuleRightDao;
import consys.event.common.core.dao.UserEventDao;
import consys.event.common.core.service.UserEventService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserEventServiceImpl extends AbstractService implements UserEventService {

    private UserEventDao userEventDao;
    private ModuleRightDao moduleRightDao;
    private AdminEventWebService adminEventWebService;
    private GroupDao groupDao;

    /* ------------------------------------------------------------------------ */
    /* --- C R E A T E --- */
    /* ------------------------------------------------------------------------ */
    /**
     * {@inheritDoc }
     */
    @Override
    public UserEvent createUserEventWithRights(UserEvent userEvent, String... rights) throws NoRecordException {
        try {
            createUser(userEvent);
            // Nastavime uzivatelovi skupinu
            for (int i = 0; i < rights.length; i++) {
                ModuleRight right = moduleRightDao.loadModuleRightByName(rights[i]);
                if (log().isDebugEnabled()) {
                    log().debug(" +" + right.getName());
                }
                userEvent.getPrivateGroup().addRight(right);
            }
            updateUserEvent(userEvent);
            return userEvent;
        } catch (ServiceExecutionFailed ex) {
            throw new NoRecordException("ServiceFailed!");
        } catch (RequiredPropertyNullException ex) {
            throw new NoRecordException("ServiceFailed!");
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public UserEvent createUserOrLoad(EventUserResponse response)
            throws ServiceExecutionFailed, RequiredPropertyNullException {
        if (response == null) {
            throw new NullPointerException("UserUuid is not valid NULL");
        }
        // najskor zistime ci uz nie je uzivatel v db
        UserEvent userEvent;
        try {
            userEvent = userEventDao.loadUserEvent(response.getUserUuid());
        } catch (NoRecordException ex) {
            userEvent = createUserFromResponse(response);
        }
        return userEvent;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public UserEvent createUserOrLoad(Invitation invitation)
            throws ServiceExecutionFailed, RequiredPropertyNullException {
        EventUserRequest request = invitation.toRequest();
        // skusime natahnut z DB ci tu nahodov uz nie je
        if (StringUtils.isNotBlank(request.getUserUuid())) {
            try {
                UserEvent userEvent = userEventDao.loadUserEvent(request.getUserUuid());
                // ak je tak vracame bez dotazu do administracie
                return userEvent;
            } catch (NoRecordException ex) {
            }
            // musime skontrolovat ci tu je vsetko
        } else if (StringUtils.isBlank(request.getUserEmail()) || StringUtils.isBlank(request.getUserName())) {
            throw new RequiredPropertyNullException("Can't invite user! Email and Name are required!");

        }

        // jedna sa bud o vytvorenie noveho uzivatela alebo uzivatel nie je v db
        LoadUsersToEventRequest luter = AdminEventWebService.OBJECT_FACTORY.createLoadUsersToEventRequest();
        luter.setInvite(false);
        luter.getEventUserRequest().add(request);
        List<EventUserResponse> resps = adminEventWebService.loadUserToEvent(luter).getEventUserResponse();
        if (resps.isEmpty()) {
            throw new ServiceExecutionFailed("Reponse size is ZERO should be 1. Possible problem on administration.");
        }
        // vytvorime uzivatela z vysledku dotazu
        EventUserResponse resp = resps.get(0);
        return createUserFromResponse(resp);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public List<UserEvent> createUserOrLoad(List<Invitation> invitations)
            throws ServiceExecutionFailed, RequiredPropertyNullException {

        List<UserEvent> ues = Lists.newArrayList();
        LoadUsersToEventRequest luter = AdminEventWebService.OBJECT_FACTORY.createLoadUsersToEventRequest();
        luter.setInvite(false);

        for (Invitation invitation : invitations) {
            EventUserRequest request = invitation.toRequest();
            // ak ma nastavene UUID tak skusime nacitat z db eventu
            if (StringUtils.isNotBlank(request.getUserUuid())) {
                try {
                    UserEvent userEvent = userEventDao.loadUserEvent(request.getUserUuid());
                    // ak je tak vracame bez dotazu do administracie
                    ues.add(userEvent);
                    continue;
                } catch (NoRecordException ex) {
                }
            }
            luter.getEventUserRequest().add(request);
        }
        List<EventUserResponse> resps = adminEventWebService.loadUserToEvent(luter).getEventUserResponse();
        if (resps.size() != luter.getEventUserRequest().size()) {
            throw new ServiceExecutionFailed("Reponse size is ZERO should be 1. Possible problem on administration.");
        }

        for (EventUserResponse eventUserResponse : resps) {
            ues.add(createUserFromResponse(eventUserResponse));
        }

        return ues;

    }

    private UserEvent createUserFromResponse(EventUserResponse response) throws ServiceExecutionFailed, RequiredPropertyNullException {
        UserEvent userEvent = new UserEvent();
        userEvent.setUuid(response.getUserUuid());
        userEvent.setFullName(response.getFullName());
        userEvent.setLastName(response.getLastName());
        userEvent.setPosition(response.getPosition());
        userEvent.setOrganization(response.getOrganization());
        userEvent.setEmail(response.getEmail());
        userEvent.setProfileImagePrefix(response.getProfileImagePrefix());
        createUser(userEvent);
        return userEvent;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public UserEvent createUserOrLoad(UserEvent userEvent)
            throws ServiceExecutionFailed, RequiredPropertyNullException {
        if (userEvent == null || StringUtils.isBlank(userEvent.getUuid())
                || StringUtils.isBlank(userEvent.getFullName()) || StringUtils.isBlank(userEvent.getLastName())
                || StringUtils.isBlank(userEvent.getEmail())) {
            throw new RequiredPropertyNullException();
        }
        try {
            // uzivatel sa uz vytvoril nacitame ho len
            return userEventDao.loadUserEvent(userEvent.getUuid());
        } catch (NoRecordException ex) {
            userEvent.setPrivateGroup(new Group());
            userEvent.getPrivateGroup().addMember(userEvent);
            log().debug("Creating {}", userEvent);
            userEventDao.create(userEvent);
            // otagujeme
            NotxService.TaggingBuilder b = NotX.getService().createTaggingBuilder().addTag(CommonTags.ATTENDEE).addUser(userEvent.getUuid());
            NotX.getService().tag(b);
            return userEvent;
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void createUser(UserEvent userEvent)
            throws ServiceExecutionFailed, RequiredPropertyNullException {
        if (userEvent == null || StringUtils.isBlank(userEvent.getUuid())
                || StringUtils.isBlank(userEvent.getFullName()) || StringUtils.isBlank(userEvent.getLastName())
                || StringUtils.isBlank(userEvent.getEmail())) {
            throw new RequiredPropertyNullException();
        }
        try {
            userEvent.setPrivateGroup(new Group());
            userEvent.getPrivateGroup().addMember(userEvent);
            log().debug("Creating {}", userEvent);
            userEventDao.create(userEvent);
            // otagujeme
            NotxService.TaggingBuilder b = NotX.getService().createTaggingBuilder().addTag(CommonTags.ATTENDEE).addUser(userEvent.getUuid());
            NotX.getService().tag(b);
        } catch (ConstraintViolationException e) {
            try {
                // uzivatel sa uz vytvoril nacitame ho len
                userEvent = userEventDao.loadUserEvent(userEvent.getUuid());
            } catch (NoRecordException ex) {
                // vcil je to error a poradny
                log().error("User with " + userEvent.getFullName() + " can't be created.", e);
                // Co sa stane vcil je magic
                throw new ServiceExecutionFailed("User with " + userEvent.getFullName() + " can't be created.");
            }
        }
    }


    /* ------------------------------------------------------------------------ */
    /* --- L O A D --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public UserEvent loadUserEvent(String uuid) throws NoRecordException {
        if (StringUtils.isBlank(uuid)) {
            throw new NullPointerException("UserUuid is not valid NULL");
        }
        return userEventDao.loadUserEvent(uuid);
    }

    @Override
    public boolean hasUserRight(String uuid, String roleName) {
        return moduleRightDao.hasUserRight(uuid, roleName);
    }

    /* ------------------------------------------------------------------------ */
    /* --- U P D A T E --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public void updateUserEvent(String userUuid, String name, String position, String organization)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid) || StringUtils.isBlank(name)) {
            throw new RequiredPropertyNullException("Missing name or user uuid");
        }

        UserEvent ue = userEventDao.loadUserEvent(userUuid);
        ue.setFullName(name);
        ue.setPosition(position);
        ue.setOrganization(organization);
        userEventDao.update(ue);
    }

    @Override
    public void updateUserEvent(UserEvent ue) throws NoRecordException {
        userEventDao.update(ue);
    }

    @Override
    public void removeUserPersonalRight(String rightName, UserEvent ue)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(rightName) || ue == null || StringUtils.isBlank(ue.getUuid())) {
            throw new RequiredPropertyNullException();
        }

        ModuleRight right = null;

        for (ModuleRight mr : ue.getPrivateGroup().getRights()) {
            if (mr.getName().equals(rightName)) {
                right = mr;
                break;
            }
        }

        if (right != null) {
            log().debug("{} -{}", ue, rightName);
            ue.getPrivateGroup().removeRight(right);
            userEventDao.update(ue);
        } else {
            log().debug("{} don't have right {}", ue, rightName);
        }
    }

    @Override
    public void addUserPersonalRight(String rightName, UserEvent ue)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(rightName) || ue == null || StringUtils.isBlank(ue.getUuid())) {
            throw new RequiredPropertyNullException();
        }
        log().debug("{} +{}", ue, rightName);
        ModuleRight t = moduleRightDao.loadModuleRightByName(rightName);
        if (ue.getPrivateGroup().getRights().contains(t)) {
            log().debug("{} already has right {}", ue, rightName);
        } else {
            ue.getPrivateGroup().addRight(t);
            userEventDao.update(ue);
        }
    }

    @Override
    public void doUpdateRelationToEvent(Relation newRelation, String message, List<String> users) throws ServiceExecutionFailed {
        UpdateUserRelationToEventRequest request = AdminEventWebService.OBJECT_FACTORY.createUpdateUserRelationToEventRequest();
        request.setMessage(message);
        request.setNewRelation(newRelation);
        request.getUserUuids().addAll(users);
        UpdateUserRelationToEventResponse resp = adminEventWebService.updateUserRelationToEvent(request);
        if (!resp.isResult()) {
            log().error("Update Relation to Event:");
            log().error("event: {}", request.getEventUuid());
            log().error("relation: {}", request.getNewRelation());
            log().error("users:");
            for (String string : users) {
                log().error("   {}", string);
            }
            throw new ServiceExecutionFailed("Update relation to event failed!");
        }
    }

    /* ------------------------------------------------------------------------ */
    /* --- L I S T --- */
    /* ------------------------------------------------------------------------ */
    @Override
    public List<UserEvent> listUserEventsWithRight(String rightName)
            throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(rightName)) {
            throw new RequiredPropertyNullException();
        }
        return groupDao.listAllUserEventsWithRightName(rightName);

    }

    @Override
    public List<UserEvent> listUserEventsByUuids(List<String> uuids) throws RequiredPropertyNullException {
        if (uuids == null) {
            throw new RequiredPropertyNullException();
        }
        if (uuids.isEmpty()) {
            return new ArrayList<UserEvent>();
        }
        return userEventDao.listByUuid(uuids);
    }

    /* ======================================================================== */
    /**
     * @return the administrationWebService
     */
    public AdminEventWebService getAdminEventWebService() {
        return adminEventWebService;
    }

    /**
     * @param administrationWebService the administrationWebService to set
     */
    public void setAdminEventWebService(AdminEventWebService administrationWebService) {
        this.adminEventWebService = administrationWebService;
    }

    public GroupDao getGroupDao() {
        return groupDao;
    }

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    /**
     * @return the userEventDao
     */
    public UserEventDao getUserEventDao() {
        return userEventDao;
    }

    /**
     * @param userEventDao the userEventDao to set
     */
    public void setUserEventDao(UserEventDao userEventDao) {
        this.userEventDao = userEventDao;
    }

    /**
     * @return the moduleRightDao
     */
    public ModuleRightDao getModuleRightDao() {
        return moduleRightDao;
    }

    /**
     * @param moduleRightDao the moduleRightDao to set
     */
    public void setModuleRightDao(ModuleRightDao moduleRightDao) {
        this.moduleRightDao = moduleRightDao;
    }
}
