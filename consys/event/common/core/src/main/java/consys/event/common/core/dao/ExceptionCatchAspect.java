package consys.event.common.core.dao;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateAccessor;

/**
 * Aspect ktory automaticky spracovava vynimku na celej DAO vrstve
 * @author Palo
 */
@Aspect
public class ExceptionCatchAspect extends HibernateAccessor {

    private static final Logger log = LoggerFactory.getLogger(ExceptionCatchAspect.class);

    public ExceptionCatchAspect() {
        log.info("Aspect Initialized..");
    }

    @Override
    public void afterPropertiesSet() {
    }

    @AfterThrowing(pointcut = "execution(* consys.event.*.core.dao.*Dao.*(..))",
    throwing = "ex")
    public void doRecoveryActions(HibernateException ex) {
        log.error("----- Error!!!");
        DataAccessException dae = convertHibernateAccessException(ex);
        logger.error(dae);
        throw dae;
    }
}
