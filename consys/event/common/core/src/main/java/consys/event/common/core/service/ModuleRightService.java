package consys.event.common.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.event.common.core.bo.ModuleRight;

import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ModuleRightService {

    /**
     * Nacitanie uzivatelskych roli
     */
    public List<ModuleRight> listUserRightsByUuid(String uuid) throws NoRecordException;

    /**
     * Nacitanie vstkych roli ktore su v liste
     */
    public List<ModuleRight> listRightsByName(List<String> names) throws NoRecordException;
    
    /**
     * Nacita vsetky prava v ramci eventu.
     * 
     */
    public List<ModuleRight> listAllRights();
    

}
