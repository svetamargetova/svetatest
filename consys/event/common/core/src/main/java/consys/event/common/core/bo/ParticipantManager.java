package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 * Spravce uzivatele
 * @author pepa
 */
public class ParticipantManager implements ConsysObject {

    private static final long serialVersionUID = -594997689046986130L;
    // data
    private UserEvent manager;
    private UserEvent participant;

    public UserEvent getManager() {
        return manager;
    }

    public void setManager(UserEvent manager) {
        this.manager = manager;
    }

    public UserEvent getParticipant() {
        return participant;
    }

    public void setParticipant(UserEvent participant) {
        this.participant = participant;
    }
}
