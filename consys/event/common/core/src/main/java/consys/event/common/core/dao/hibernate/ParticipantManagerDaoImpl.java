package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.ParticipantManager;
import consys.event.common.core.dao.ParticipantManagerDao;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author pepa
 */
public class ParticipantManagerDaoImpl extends GenericDaoImpl<ParticipantManager> implements ParticipantManagerDao {

    @Override
    public ParticipantManager load(Long id) throws NoRecordException {
        return load(id, ParticipantManager.class);
    }

    @Override
    public boolean checkManagedUser(String uuidManager, String uuidParticipant) {
        Query query = session().createQuery("FROM ParticipantManager pm WHERE pm.manager.uuid=:MANAGER AND pm.participant.uuid=:PARTICIPANT");
        ParticipantManager manager = (ParticipantManager) query
                .setString("MANAGER", uuidManager)
                .setString("PARTICIPANT", uuidParticipant)
                .uniqueResult();
        return manager != null;
    }

    @Override
    public List<ParticipantManager> listManagedParticipants(String uuidManager) {
        Query query = session().createQuery("FROM ParticipantManager pm LEFT JOIN FETCH pm.participant WHERE pm.manager.uuid=:MANAGER order by pm.participant.lastName");
        return query.setString("MANAGER", uuidManager).list();
    }

    @Override
    public int deleteAllManager() {
        return session().createQuery("delete from ParticipantManager").executeUpdate();
    }
}
