package consys.event.common.core.superlectures.so;

import java.text.SimpleDateFormat;
import org.w3c.dom.Document;

/**
 *
 * @author pepa
 */
public class SuperlecturesGetResponse extends SuperlecturesBaseObject implements SuperlecturesResponse {

    // data
    private SuperlecturesResponseStatus status;
    private String superlectureVideoUrl;

    public SuperlecturesResponseStatus getStatus() {
        return status;
    }

    public void setStatus(SuperlecturesResponseStatus status) {
        this.status = status;
    }

    public String getSuperlectureVideoUrl() {
        return superlectureVideoUrl;
    }

    public void setSuperlectureVideoUrl(String superlectureVideoUrl) {
        this.superlectureVideoUrl = superlectureVideoUrl;
    }

    @Override
    public String toString() {
        return getStatus() + " - " + getVideoName();
    }

    @Override
    public void loadFromXML(Document doc) throws Exception {
        logger.debug("loading object data from " + convertDocumentToString(doc));

        SimpleDateFormat timeFormat = new SimpleDateFormat(DATETIME_FORMAT);

        setAuthors(listPersonsFromDocument(doc, TAG_AUTHORS));
        setCategory(firstTagTextValue(doc, TAG_CATEGORY));
        setDatetimeRecorded(timeFormat.parse(firstTagTextValue(doc, TAG_DATETIME_RECORDED)));
        setDescription(firstTagTextValue(doc, TAG_DESCRIPTION));
        setEventName(firstTagTextValue(doc, TAG_EVENT));
        setKeywords(firstTagTextValue(doc, TAG_KEYWORDS));
        setLinkUrl(attributeOfElement(doc, TAG_LINK, ATT_SRC));
        setPdfUrl(attributeOfElement(doc, TAG_PDF, ATT_SRC));
        setRecorded(firstTagTextValue(doc, TAG_RECORDED));
        setSpeekers(listPersonsFromDocument(doc, TAG_SPEAKERS));
        setStatus(SuperlecturesResponseStatus.valueOf(firstTagTextValue(doc, TAG_CODE)));
        setVideoName(firstTagTextValue(doc, TAG_NAME));
        setVideoUrl(attributeOfElement(doc, TAG_VIDEO, ATT_SRC));
        setSuperlectureVideoUrl(attributeOfElement(doc, TAG_LECTURE, ATT_EMBEDDED_PAGE_SRC));
    }
}
