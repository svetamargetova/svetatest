/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.common.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.DataFile;

/**
 *
 * @author palo
 */
public interface DataFileDao extends GenericDao<DataFile> {

    public void deleteByUuid(String uuid) throws NoRecordException;

    public DataFile loadByUuid(String uuid) throws NoRecordException;

}
