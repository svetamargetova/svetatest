package consys.event.common.core.superlectures.so;

/**
 * Vsechny stavy superlectures
 * @author pepa
 */
public enum SuperlecturesResponseStatus {

    /** video uspesne pridano */
    VIDEO_ADDED,
    /** video se zpracovava */
    VIDEO_IN_PROCESSING,
    /** video bylo zpracovano */
    VIDEO_IS_READY,
    /** zpracovani videa skoncilo chybou */
    VIDEO_IN_ERROR,
    /** video uz bylo odstraneno */
    VIDEO_REMOVED,
    /** video neexistuje (spatny hash)*/
    VIDEO_NOT_EXISTS,
    /** video nebylo odstraneno (spatny kod) */
    VIDEO_NOT_REMOVED,
    /** video uz bylo jednou pridano */
    ERROR_ALREADY_ADDED,
    /** spatne xml, superlectures nerozumi */
    ERROR_CORRUPTED_XML,
    /** nejsou odeslane xml data */
    ERROR_NO_XML_DATA,
    /** chybi nazev akce v xml */
    ERROR_NO_NAME_INFORMATION,
    /** chybi url videa */
    ERROR_NO_VIDEO_URL,
    /** neznamy stav */
    ERROR_UNKNOWN_STATE;
}
