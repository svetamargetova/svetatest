package consys.event.common.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.api.properties.CommonProperties;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventNamePropertyHandler implements SystemPropertyUpdateHandler {

       

    @Override
    public String getKey() {
        return CommonProperties.EVENT_NAME;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
        // nothing - can't be empty
        if(StringUtils.isBlank(newValue)){
            throw new IllegalPropertyValue("Event name can't be empty!");
        }
       
    }
}
