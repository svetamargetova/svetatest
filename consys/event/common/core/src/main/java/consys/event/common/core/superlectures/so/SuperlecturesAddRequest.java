package consys.event.common.core.superlectures.so;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author pepa
 */
public class SuperlecturesAddRequest extends SuperlecturesBaseObject implements SuperlecturesRequest {

    /** naplni seznam autoru ze vstupniho stringu */
    public void parseAuthors(String authors) {
        List<String> names = parseNames(authors);
        getAuthors().clear();
        for (String name : names) {
            getAuthors().add(new SuperlecturesPerson(name));
        }
    }

    /** naplni seznam prednasejicich ze vstupniho stringu */
    public void parseSpeakers(String speakers) {
        List<String> names = parseNames(speakers);
        getSpeekers().clear();
        for (String name : names) {
            getSpeekers().add(new SuperlecturesPerson(name));
        }
    }

    /** rozparsuje jmena oddelena carkou */
    private List<String> parseNames(String input) {
        List<String> result = new ArrayList<String>();
        if (StringUtils.isBlank(input)) {
            return result;
        }
        String[] s = input.split(",");
        for (int i = 0; i < s.length; i++) {
            String name = s[i].trim();
            if (StringUtils.isNotEmpty(name)) {
                result.add(name);
            }
        }
        return result;
    }

    @Override
    public Document createDocument() throws Exception {
        DOMImplementation dom = DocumentBuilderFactory.newInstance().newDocumentBuilder().getDOMImplementation();
        Document doc = dom.createDocument(null, DOCUMENT_ROOT, null);
        doc.setXmlStandalone(true);

        SimpleDateFormat timeFormat = new SimpleDateFormat(DATETIME_FORMAT);
        String datetimeCreated = timeFormat.format(getDatetimeRecorded());

        Element dataE = doc.getDocumentElement();
        Element lectureE = appendNewChild(doc, dataE, TAG_LECTURE);

        Element nameE = appendNewChild(doc, lectureE, TAG_NAME);
        appendNewTextChild(doc, nameE, getVideoName());
        Element eventE = appendNewChild(doc, lectureE, TAG_EVENT);
        appendNewTextChild(doc, eventE, getEventName());
        Element categoryE = appendNewChild(doc, lectureE, TAG_CATEGORY);
        appendNewTextChild(doc, categoryE, getCategory());
        Element authorsE = appendNewChild(doc, lectureE, TAG_AUTHORS);
        appendPersons(doc, authorsE, getAuthors());
        Element speakersE = appendNewChild(doc, lectureE, TAG_SPEAKERS);
        appendPersons(doc, speakersE, getSpeekers());
        Element keywordsE = appendNewChild(doc, lectureE, TAG_KEYWORDS);
        appendNewTextChild(doc, keywordsE, getKeywords());
        Element descriptionE = appendNewChild(doc, lectureE, TAG_DESCRIPTION);
        appendNewTextChild(doc, descriptionE, getDescription());
        Element recordedE = appendNewChild(doc, lectureE, TAG_RECORDED);
        appendNewTextChild(doc, recordedE, getRecorded());
        Element datetimeRecordedE = appendNewChild(doc, lectureE, TAG_DATETIME_RECORDED);
        appendNewTextChild(doc, datetimeRecordedE, datetimeCreated);
        Element videoE = appendNewChild(doc, lectureE, TAG_VIDEO);
        videoE.setAttribute(ATT_SRC, getVideoUrl());
        Element pdfE = appendNewChild(doc, lectureE, TAG_PDF);
        pdfE.setAttribute(ATT_SRC, getPdfUrl());
        Element linkE = appendNewChild(doc, lectureE, TAG_LINK);
        linkE.setAttribute(ATT_SRC, getLinkUrl());

        logger.debug("created xml: " + convertDocumentToString(doc));

        return doc;
    }

    /** pripoji osoby k pozadovanemu elementu */
    private void appendPersons(Document doc, Element parent, List<SuperlecturesPerson> persons) {
        for (SuperlecturesPerson person : persons) {
            Element e = doc.createElement(TAG_PERSON);
            e.setAttribute(ATT_NAME, person.getName());
            parent.appendChild(e);
        }
    }
}
