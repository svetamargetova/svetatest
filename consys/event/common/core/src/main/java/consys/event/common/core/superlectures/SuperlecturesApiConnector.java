package consys.event.common.core.superlectures;

import consys.event.common.core.superlectures.so.SuperlecturesAddResponse;
import consys.event.common.core.superlectures.so.SuperlecturesAddRequest;
import consys.event.common.core.superlectures.so.SuperlecturesGetResponse;
import consys.event.common.core.superlectures.so.SuperlecturesXMLObject;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

/**
 *
 * @author pepa
 */
public class SuperlecturesApiConnector {

    // konstanty - adresy
    private static final String SUPERLECTURES_ADD_VIDEO = "http://147.229.8.65/api/addVideo.php?";
    private static final String SUPERLECTURES_GET_VIDEO = "http://147.229.8.65/api/getVideo.php?";
    private static final String SUPERLECTURES_REMOVE_VIDEO = "http://147.229.8.65/api/removeVideo.php?";
    private static final String HASH = "hash=";
    private static final String USER = "user=takeplace";
    private static final String KEY = "key=";
    // konstanty
    private static final String XML_DATA = "xmlData";

    public static SuperlecturesAddResponse addVideo(SuperlecturesAddRequest sr) throws Exception {
        Document docRequest = sr.createDocument();

        String key = "";

        // napr: .../addVideo.php?user=takeplace&key=faa6bd04cbfe87879b62cced8b021de5bb84601f
        StringBuilder urlBuilder = new StringBuilder(SUPERLECTURES_ADD_VIDEO);
        urlBuilder.append(USER);
        urlBuilder.append("&");
        urlBuilder.append(KEY);
        urlBuilder.append(key);

        // vytvorime spojeni
        URL url = new URL(SUPERLECTURES_ADD_VIDEO);
        URLConnection c = url.openConnection();
        c.setDoInput(true);
        c.setDoOutput(true);
        c.setUseCaches(false);

        // posleme data
        StringBuilder requestData = new StringBuilder(URLEncoder.encode(XML_DATA, "UTF-8"));
        requestData.append("=");
        requestData.append(SuperlecturesXMLObject.convertDocumentToString(docRequest));

        OutputStreamWriter osw = new OutputStreamWriter(c.getOutputStream());
        osw.write(requestData.toString());
        osw.flush();

        // precteme data
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document docResponse = builder.parse(c.getInputStream());

        osw.close();
        c.getInputStream().close();

        // vytvorime odpoved
        SuperlecturesAddResponse response = new SuperlecturesAddResponse();
        response.loadFromXML(docResponse);
        return response;
    }

    /** nacte informace o videu podle zadaneho hashe */
    public static SuperlecturesGetResponse getVideo(String hash) throws Exception {
        if (StringUtils.isBlank(hash)) {
            throw new IllegalArgumentException();
        }

        String key = "";

        // napr: .../getVideo.php?hash=6bc7b223c746c050d8a914ac5854c3a946d6ad20&user=takeplace&key=153bf8413fc524c6440a15e2445b406124aac994
        StringBuilder urlBuilder = new StringBuilder(SUPERLECTURES_GET_VIDEO);
        urlBuilder.append(HASH);
        urlBuilder.append(hash);
        urlBuilder.append("&");
        urlBuilder.append(USER);
        urlBuilder.append("&");
        urlBuilder.append(KEY);
        urlBuilder.append(key);

        // vytvorime spojeni
        URL url = new URL(urlBuilder.toString());
        URLConnection c = url.openConnection();
        c.setDoInput(true);
        c.setUseCaches(false);

        // precteme data
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(c.getInputStream());
        c.getInputStream().close();

        // vytvorime odpoved
        SuperlecturesGetResponse response = new SuperlecturesGetResponse();
        response.loadFromXML(doc);
        return response;
    }

    public static void removeVideo(String hash) throws Exception {
        if (StringUtils.isBlank(hash)) {
            throw new IllegalArgumentException();
        }

        String key = "";

        // napr: http://147.229.8.65/api/removeVideo.php?hash=6bc7b223c746c050d8a914ac5854c3a946d6ad20&code=97785c19325cf8311dd214151aa2e54000df717b&user=takeplace&key=153bf8413fc524c6440a15e2445b406124aac994
        StringBuilder sb = new StringBuilder(SUPERLECTURES_REMOVE_VIDEO);
        sb.append(HASH);
        sb.append(hash);
        sb.append("&code");
        sb.append(sha1(hash));
        sb.append(USER);
        sb.append("&");
        sb.append(KEY);
        sb.append(key);

        // vytvorime spojeni
        URL url = new URL(sb.toString());
        URLConnection c = url.openConnection();
        c.setDoInput(true);
        c.setUseCaches(false);
    }

    private static String sha1(String hash) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String code = hash + "SuPerLeCtUrEs";
        byte[] input = code.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        return byteArrayToHexString(md.digest(input));
    }

    private static String byteArrayToHexString(byte[] b) {
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    /** pro testovaci ucely */
    public static void main(String[] args) {
        try {
            SuperlecturesAddRequest srq = new SuperlecturesAddRequest();
            srq.setVideoName("test upload 1");
            srq.setEventName("98a68d6038334ddc84185ada19af7cd8");
            srq.setCategory("Takeplace");
            srq.setDatetimeRecorded(new Date());
            srq.setVideoUrl("http://www.youtube.com/watch?v=EnTntQaPH3I");
            srq.parseSpeakers("Jindřich Sličný, , Markéta Kulhavá");
            //Document doc = srq.createDocument();
            //System.out.println(SuperlecturesXMLObject.convertDocumentToString(doc));
            //SuperlecturesAddResponse sr = addVideo(srq);
            //System.out.println("Status: " + sr.getStatus());
            //System.out.println("Hash: " + sr.getHash());
            //SuperlecturesAddResponse r = SuperlecturesApiConnector.addVideo(srq);
            //System.out.println("Response status: " + r.getStatus() + " , Hash = "+r.getHash());
            SuperlecturesGetResponse r = SuperlecturesApiConnector.getVideo("6bc7b223c746c050d8a914ac5854c3a946d6ad20");
            System.out.println("Response status: " + r.getStatus() + ", video url: " + r.getSuperlectureVideoUrl());
        } catch (Exception ex) {
            System.err.println("Je to rozbity " + ex.getLocalizedMessage());
        }
    }
}
