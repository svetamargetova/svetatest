package consys.event.common.core.service.list;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ListServiceRegistry {
    
    public ListService getHandlerByTag(String tag) throws ListHandlerMissingException;





}
