/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core.service.cache;

import consys.common.core.bo.SystemProperty;
import consys.common.core.dao.SystemPropertyDao;
import consys.common.core.service.SystemPropertyService;
import consys.event.overseer.cache.AbstractCacheService;
import java.util.List;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Searchable;
import net.sf.ehcache.constructs.blocking.CacheEntryFactory;
import net.sf.ehcache.constructs.blocking.SelfPopulatingCache;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Cache bude pracovat stale v ramci Session a transkacie pretoze bude volana
 * vnutorne zo
 * {@link SystemPropertyService}. Tato cache pouziva {@link SelfPopulatingCache}
 * ktora automaticky dotahuje.
 *
 * @author palo
 */
public class SystemPropertyCache extends AbstractCacheService<String, SystemProperty> {

    @Autowired
    private SystemPropertyDao systemPropertyDao;

    public SystemPropertyCache() {
        super("system-property");
    }

    @Override
    protected Ehcache decorate(Ehcache cache) {
        return new SelfPopulatingCache(cache, new SystemPropertyCacheEntryFactory());
    }

    @Override
    protected void initializeCache(Ehcache cache) {
        List<SystemProperty> properties = systemPropertyDao.listAll(SystemProperty.class);
        for (SystemProperty systemProperty : properties) {
            cache.putQuiet(new Element(systemProperty.getKey(), systemProperty));
        }
    }

    @Override
    protected CacheConfiguration getConfiguration(String name) {
        CacheConfiguration cacheConfig = new CacheConfiguration(name, 100).overflowToDisk(false).
                eternal(true).
                searchable(new Searchable()).
                timeToLiveSeconds(0).
                diskPersistent(false).
                timeToIdleSeconds(0);
        cacheConfig.getSearchable().keys();
        cacheConfig.getSearchable().values();
        return cacheConfig;
    }

    private class SystemPropertyCacheEntryFactory implements CacheEntryFactory {

        @Override
        public Object createEntry(Object key) throws Exception {
            return systemPropertyDao.loadByKey((String) key);
        }
    }
}
