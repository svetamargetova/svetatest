/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.DataFile;

/**
 *
 * @author palo
 */
public interface DataFileService {

    public void create(DataFile dataFile) throws ServiceExecutionFailed;

    /** nacte obrazek podle jeho uuid */
    public DataFile loadByUuid(String uuid) throws ServiceExecutionFailed, NoRecordException;

    public void deleteByUuid(String uuid) throws ServiceExecutionFailed, NoRecordException;

    
}
