package consys.event.common.core.service.list;

import consys.common.core.exception.ConsysException;

/**
 *
 * @author Palo
 */
public class ListHandlerMissingException extends ConsysException{
    private static final long serialVersionUID = 8934944940313787852L;

    public ListHandlerMissingException() {
        super();
    }

     public ListHandlerMissingException(String message) {
	super(message);
    }

}
