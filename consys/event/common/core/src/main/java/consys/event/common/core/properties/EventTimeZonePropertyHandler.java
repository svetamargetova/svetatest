package consys.event.common.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.utils.TimeZoneUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventTimeZonePropertyHandler implements SystemPropertyUpdateHandler {

    @Override
    public String getKey() {
        return CommonProperties.EVENT_TIMEZONE;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
        if (TimeZoneUtils.isTimeZoneValid(newValue)) {
            return;
        }
        throw new IllegalPropertyValue("New TimeZone '" + newValue + "'for event is not recognized!");
    }
}
