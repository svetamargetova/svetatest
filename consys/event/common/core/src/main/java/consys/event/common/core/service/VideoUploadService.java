package consys.event.common.core.service;

import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadStatus;
import java.io.InputStream;

/**
 *
 * @author pepa
 */
public interface VideoUploadService {

    /** smaze video z bucketu */
    void deleteVideo(String bucketName, String fileName);
    
    /** uploaduje soubor z disku */
    UploadStatus uploadVideo(String bucketName, String fileName, String path, UploadProgress uploadStatus);

    /** uploaduje ze streamu, uploadStatus musi mit nastavenu celkovou velikost souboru */
    UploadStatus uploadVideo(String bucketName, String fileName, InputStream is, String contentType, UploadProgress uploadStatus);
}
