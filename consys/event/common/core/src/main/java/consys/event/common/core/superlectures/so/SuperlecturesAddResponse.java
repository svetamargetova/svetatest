package consys.event.common.core.superlectures.so;

import consys.event.common.core.superlectures.SuperlecturesTags;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 *
 * @author pepa
 */
public class SuperlecturesAddResponse extends SuperlecturesXMLObject implements SuperlecturesResponse, SuperlecturesTags {

    private String hash;
    private SuperlecturesResponseStatus status;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public SuperlecturesResponseStatus getStatus() {
        return status;
    }

    public void setStatus(SuperlecturesResponseStatus status) {
        this.status = status;
    }

    /** @return true pokud je kladna odpoved ze serveru */
    public boolean isOkResponse() {
        if (status == null) {
            return false;
        }
        switch (status) {
            case VIDEO_IN_PROCESSING:
                return true;
            case ERROR_ALREADY_ADDED:
            case ERROR_CORRUPTED_XML:
            case ERROR_NO_NAME_INFORMATION:
            case ERROR_NO_XML_DATA:
            case ERROR_UNKNOWN_STATE:
                return false;
            default:
                return false;
        }
    }

    @Override
    public void loadFromXML(Document doc) throws Exception {
        logger.debug("loading object data from " + convertDocumentToString(doc));

        String statusValue = doc.getElementsByTagName(TAG_CODE).item(0).getTextContent();
        if (StringUtils.isBlank(statusValue)) {
            status = SuperlecturesResponseStatus.ERROR_UNKNOWN_STATE;
        } else {
            status = SuperlecturesResponseStatus.valueOf(statusValue);
        }

        if (!isOkResponse()) {
            // neni potreba pokracovat dal, chybovy stav
            return;
        }

        Node lecture = doc.getElementsByTagName(TAG_LECTURE).item(0);
        NamedNodeMap attributes = lecture.getAttributes();

        setHash(getAttribute(ATT_HASH, attributes));
    }
}
