package consys.event.common.core.utils;

import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.ImmutableSortedSet.Builder;
import java.util.Set;
import java.util.TimeZone;


/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TimeZoneUtils {

    static ImmutableSortedSet<String> TIME_ZONES;

    static {
        Builder<String> b = ImmutableSortedSet.orderedBy(String.CASE_INSENSITIVE_ORDER);
        String[] ids = TimeZone.getAvailableIDs();
        for (int i = 0; i < ids.length; i++) {
            b.add(ids[i]);
        }
        TIME_ZONES = b.build();
    }

    public static boolean isTimeZoneValid(String timezone){
        return TIME_ZONES.contains(timezone);
    }

    public static Set<String> getTimeZones(){
        return TIME_ZONES;
    }
}
