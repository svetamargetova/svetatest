package consys.event.common.core.utils;

import consys.common.core.exception.IllegalPropertyValue;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public final class SystemPropertyUtils {

    public static final String BOOLEAN_FALSE = "false";
    public static final String BOOLEAN_TRUE = "true";

    public static final void isBooleanProperty(String value) throws IllegalPropertyValue {
        if (value != null && (value.equalsIgnoreCase(BOOLEAN_FALSE) || value.equalsIgnoreCase(BOOLEAN_TRUE))) {
            return;
        } else {
            throw new IllegalPropertyValue("Set boolean system property failed! Regular values are: 'true' or 'false'");
        }
    }

    public static final void isUrlValid(String value) throws IllegalPropertyValue{
        if (value != null && (value.startsWith("http://") || value.startsWith("https://"))) {
            return;
        } else {
            throw new IllegalPropertyValue("Set URL system property failed!");
        }
    }
}
