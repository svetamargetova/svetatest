package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.common.utils.collection.Sets;
import java.util.Set;

/**
 *
 * @author Palo
 */
public class Group implements ConsysObject {

    private static final long serialVersionUID = 6625851576889187288L;
    private Long id;
    private int membersCount = 0;
    private Set<UserEvent> members;
    private Set<ModuleRight> rights;

    public Group() {
        members = Sets.newHashSet();
        rights = Sets.newHashSet();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the membersCount
     */
    public int getMembersCount() {
        return membersCount;
    }

    /**
     * @param membersCount the membersCount to set
     */
    public void setMembersCount(int membersCount) {
        this.membersCount = membersCount;
    }

    /**
     * @return the members
     */
    public Set<UserEvent> getMembers() {
        return members;
    }

    /**
     * @param members the members to set
     */
    public void setMembers(Set<UserEvent> members) {
        this.members = members;
    }

    /**
     * @return the rights
     */
    public Set<ModuleRight> getRights() {
        return rights;
    }

    /**
     * @param rights the rights to set
     */
    public void setRights(Set<ModuleRight> rights) {
        this.rights = rights;
    }

    // UTILS
    public boolean addMember(UserEvent uo) {
        return getMembers().add(uo);
    }

    public boolean addRight(ModuleRight emr) {
        return getRights().add(emr);
    }
    
    public boolean removeRight(ModuleRight mr){
        return getRights().remove(mr);
    }
}
