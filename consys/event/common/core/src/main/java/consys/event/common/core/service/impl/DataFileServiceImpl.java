package consys.event.common.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.date.DateProvider;
import consys.common.utils.UuidProvider;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.dao.DataFileDao;
import consys.event.common.core.service.DataFileService;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Palo
 */
public class DataFileServiceImpl extends AbstractService implements DataFileService {

    private DataFileDao dataFileDao;

    @Override
    public void create(DataFile dataFile) throws ServiceExecutionFailed {
        dataFile.setUuid(UuidProvider.getUuid());
        dataFile.setDateInserted(DateProvider.getCurrentDate());
        dataFileDao.create(dataFile);
    }

    @Override
    public DataFile loadByUuid(String uuid) throws ServiceExecutionFailed, NoRecordException {
        try {
            if (StringUtils.isBlank(uuid)) {
                throw new NoRecordException();
            }
            DataFile df = dataFileDao.loadByUuid(uuid);
            df.setByteData(df.getData().getBytes(1, (int) df.getData().length()));
            return df;
        } catch (SQLException ex) {
            log().error("Error load datafile ",ex);
            throw new ServiceExecutionFailed();
        }
    }

    @Override
    public void deleteByUuid(String uuid) throws ServiceExecutionFailed, NoRecordException {
        if (StringUtils.isBlank(uuid)) {
            throw new NoRecordException();
        }
        dataFileDao.deleteByUuid(uuid);
    }

    /**
     * @return the dataFileDao
     */
    public DataFileDao getDataFileDao() {
        return dataFileDao;
    }

    /**
     * @param dataFileDao the dataFileDao to set
     */
    public void setDataFileDao(DataFileDao dataFileDao) {
        this.dataFileDao = dataFileDao;
    }
}
