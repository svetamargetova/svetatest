package consys.event.common.core.superlectures.so;

import org.w3c.dom.Document;

/**
 *
 * @author pepa
 */
public interface SuperlecturesRequest {

    /** provede vytvori xml dokumentu */
    Document createDocument() throws Exception;
}
