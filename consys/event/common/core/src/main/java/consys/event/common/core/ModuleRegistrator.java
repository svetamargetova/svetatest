package consys.event.common.core;

import consys.common.core.bo.SystemProperty;
import consys.event.common.core.bo.Module;
import consys.event.common.core.bo.ModuleRight;
import consys.event.overseer.connector.OverseerSessionFactoryPool;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author Palo
 */
public class ModuleRegistrator implements OverseerSessionFactoryPool.Delegate, ApplicationContextAware, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(ModuleRegistrator.class);
    private ApplicationContext applicationContext;
    private OverseerSessionFactoryPool factoryPool;

    protected void registerModulesFor(SessionFactory sessionFactory) {
        Iterator<EventModuleRegistrator> iterator = getAfterSessionCreationDelegates().iterator();
        Session session = sessionFactory.openSession();
        while (iterator.hasNext()) {
            Transaction tx = session.beginTransaction();
            tx.begin();
            // doAfterSessionCreate
            try {

                EventModuleRegistrator emr = iterator.next();
                logger.info("Module registrator " + emr.getClass().getSimpleName());
                // specificke zalezitosti
                emr.doAfterSessionFactoryCreate(session);
                logger.info("   Do after session create ... [OK]");

                checkSystemProperties(emr, session);
                checkModuleRights(emr, session);
                installSqlScripts(emr.getSQLScripts(), session);

                tx.commit();
            } catch (Exception e) {
                tx.rollback();
                logger.error("AfterSessionCreateDelegate occures an exception!", e);
            }
        }
        session.close();

    }

    private void installSqlScripts(Map<String, String> scripts, Session session) throws IOException {
        if (scripts == null) {
            return;
        }
        logger.info("   Installing SQL Scripts ... ");
        for (Map.Entry<String, String> entry : scripts.entrySet()) {
            


            InputStream is = null;
            try {
                is = ModuleRegistrator.class.getResourceAsStream(entry.getValue());
                StringWriter writer = new StringWriter();
                IOUtils.copy(is, writer, "UTF-8");
                String sqlScript = writer.toString();                
                logger.info("      install " + entry.getKey());
                logger.debug(sqlScript);
                session.createSQLQuery(sqlScript).executeUpdate();
            } catch (SQLGrammarException e) {
                logger.warn("SQL Script Grammer Exception", e);
            } catch (Exception e) {
                logger.error("      ERROR!!! SQL script on file path " + entry.getKey() + " not found!", e);
            } finally {
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }
        }
        logger.info("   Installing SQL Scripts ... [OK]");

    }

    private void checkSystemProperties(EventModuleRegistrator emr, Session session) throws HibernateException {
        // systemove property
        logger.info("   Checking SystemProperties ... ");
        List<SystemProperty> properties = emr.getModuleProperties();
        if (properties != null) {
            for (SystemProperty s : properties) {
                if (!existsProperty(session, s.getKey())) {
                    logger.info("      +" + s);
                    session.save(s);
                }
            }
        }
        logger.info("   Checking SystemProperties ... [OK]");
    }

    private void checkModuleRights(EventModuleRegistrator emr, Session session) throws HibernateException {
        // systemove property
        logger.info("   Checking ModuleRights ... ");
        Module module = emr.getModule();
        Module loadedModule = (Module) session.createQuery("from Module m  LEFT JOIN FETCH m.rights where m.name=:MODULE_NAME").setString("MODULE_NAME", module.getName()).uniqueResult();
        if (loadedModule == null) {
            logger.info("      +" + module);
            session.save(module);
        } else {
            boolean hasNewRights = false;
            for (ModuleRight moduleRight : module.getRights()) {
                boolean newRight = true;
                for (ModuleRight persistedRight : loadedModule.getRights()) {
                    if (persistedRight.equals(moduleRight)) {
                        newRight = false;
                    }
                }
                if (newRight) {
                    hasNewRights = true;
                    logger.info("      + " + moduleRight);
                    loadedModule.getRights().add(moduleRight);
                }
            }
            if (hasNewRights) {
                session.update(loadedModule);
            }
        }
        logger.info("   Checking ModuleRights ... [OK]");
    }

    private boolean existsProperty(Session s, String key) {
        Object o = s.createCriteria(SystemProperty.class).add(Restrictions.eq("key", key)).uniqueResult();
        return o == null ? false : true;
    }

    private boolean existsRight(Session s, String name) {
        Object o = s.createCriteria(ModuleRight.class).add(Restrictions.eq("name", name)).uniqueResult();
        return o == null ? false : true;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * @return the afterSessionCreationDelegates
     */
    public Collection<EventModuleRegistrator> getAfterSessionCreationDelegates() {
        return applicationContext.getBeansOfType(EventModuleRegistrator.class).values();
    }

    @Override
    public void onSessionFactoryCreated(SessionFactory sessionFactory) {
        logger.info("Module Registrator ");
        registerModulesFor(sessionFactory);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Register as FactoryPool delegate");
        getFactoryPool().addDelegate(this);
    }

    /**
     * @return the factoryPool
     */
    public OverseerSessionFactoryPool getFactoryPool() {
        return factoryPool;
    }

    /**
     * @param factoryPool the factoryPool to set
     */
    @Autowired(required = true)
    public void setFactoryPool(OverseerSessionFactoryPool factoryPool) {
        this.factoryPool = factoryPool;
    }
}
