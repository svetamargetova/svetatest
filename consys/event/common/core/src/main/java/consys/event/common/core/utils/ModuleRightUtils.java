package consys.event.common.core.utils;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import consys.common.utils.collection.Lists;
import consys.event.common.core.EventModuleRegistrator;
import consys.event.common.core.bo.ModuleRight;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ModuleRightUtils implements ApplicationContextAware, ApplicationListener {

    private static final Logger logger = LoggerFactory.getLogger(ModuleRightUtils.class);
    private ApplicationContext applicationContext;
    // ROLE -> UID
    private Map<String, String> toClientTranslationMap;
    // UID -> ROLE
    private Map<String, String> fromClientTranslationMap;

    /**
     * Prelozenie prav z klienta podla UID ktore ma kazde pravo priradene
     */
    public List<String> translateFromClient(Collection<String> rights) {
        List<String> out = Lists.newArrayList();
        if (rights != null && !rights.isEmpty()) {
            for (String right : rights) {
                String roleName = translateFromClient(right);
                if (roleName != null) {
                    out.add(roleName);
                }
            }
        }
        return out;
    }

    /**
     * Prelozenie prav z klienta podla UID ktore ma kazde pravo priradene
     */
    private String translateFromClient(String right) {
        String roleName = getFromClientTranslationMap().get(right);
        if (roleName == null) {
            throw new NullPointerException("Projection from client right " + right + " not found!");
        }
        return roleName;
    }

    /**
     * Prelozenie prav klientovy podla UID.
     */
    public List<String> translateToClient(Collection<ModuleRight> rights) {
        List<String> out = Lists.newArrayList();
        if (rights != null && !rights.isEmpty()) {
            for (ModuleRight right : rights) {
                String uuid = translateToClient(right);
                if (uuid != null) {
                    out.add(uuid);
                    if (logger.isDebugEnabled()) {
                        logger.debug("    - {} ---> {}", right.getName(), uuid);
                    }
                }
            }
        }
        return out;
    }

    /**
     * Prelozenie prav klientovy podla UID.
     */
    private String translateToClient(ModuleRight right) {
        String uuid = getToClientTranslationMap().get(right.getName());
        if (uuid == null) {
            logger.warn("Projection from database right " + right.getName() + " not found! Maybe right doesn't exists anymore?");
        }
        return uuid;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    private Map<String, String> getToClientTranslationMap() {
        return toClientTranslationMap;
    }

    private Map<String, String> getFromClientTranslationMap() {
        return fromClientTranslationMap;
    }

    @Override
    public synchronized void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextRefreshedEvent && (toClientTranslationMap == null || toClientTranslationMap.isEmpty())) {
            Map<String, EventModuleRegistrator> modules = applicationContext.getBeansOfType(EventModuleRegistrator.class);

            logger.debug("Initializing ModuleRight - > ClientUID ");

            Builder<String, String> toMap = ImmutableMap.builder();
            Builder<String, String> fromMap = ImmutableMap.builder();

            // Nacitame vsetky zaregistrovane moduly a inicializujeme mapy
            for (EventModuleRegistrator emr : modules.values()) {
                Set<ModuleRight> rights = emr.getModule().getRights();
                for (Iterator<ModuleRight> it = rights.iterator(); it.hasNext();) {
                    ModuleRight moduleRight = it.next();
                    logger.debug("   {}  <-> {} ", moduleRight.getName(), moduleRight.getUniqueValue());
                    toMap.put(moduleRight.getName(), moduleRight.getUniqueValue());
                    fromMap.put(moduleRight.getUniqueValue(), moduleRight.getName());
                }
            }

            // Synchronize them
            toClientTranslationMap = Collections.synchronizedMap(toMap.build());
            fromClientTranslationMap = Collections.synchronizedMap(fromMap.build());
        }
    }
}
