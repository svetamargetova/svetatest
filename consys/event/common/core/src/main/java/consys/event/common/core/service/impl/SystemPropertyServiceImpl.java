/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core.service.impl;

import consys.common.core.dao.SystemPropertyDao;
import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.collection.Maps;
import consys.event.common.core.properties.SystemPropertyUpdateHandler;
import consys.event.common.core.service.cache.SystemPropertyCache;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.attribute.AttributeExtractor;
import net.sf.ehcache.search.expression.BaseCriteria;
import net.sf.ehcache.search.expression.ComparableValue;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Servica si drzi v pamati vsetky systemove properties a na vyziadanie ich
 * znova nacita. Treba respektovat defaultne hodnoty danych properties
 * <p/>
 * @author Palo
 */
public class SystemPropertyServiceImpl extends AbstractService implements SystemPropertyService, ApplicationContextAware {

    private SystemPropertyDao dao;
    private ApplicationContext context;
    public static final String DEFAULT_EMPTY_STRING = "";
    public static final Integer DEFAULT_EMPTY_INTEGER = 0;
    public static final Boolean DEFAULT_EMPTY_BOOLEAN = Boolean.FALSE;
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ss";
    private Map<String, SystemPropertyUpdateHandler> updateHandlerMap;
    final Object createMapLock = new Object();
    // cache
    
    private SystemPropertyCache systemPropertyCache;
    
    

    @Override
    public String getString(String key) {
        String value = getValue(key);
        return value == null ? DEFAULT_EMPTY_STRING : value;
    }

    @Override
    public Integer getInteger(String key) {
        String value = getValue(key);
        return value == null ? DEFAULT_EMPTY_INTEGER : Integer.valueOf(value);
    }

    @Override
    public Boolean getBoolean(String key) {
        String value = getValue(key);
        return value == null ? DEFAULT_EMPTY_BOOLEAN : Boolean.valueOf(value);
    }

    @Override
    public Date getDate(String key) {
        String value = getValue(key);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT);
            return value == null ? null : sdf.parse(value);
        } catch (ParseException ex) {
            throw new RuntimeException("Can't parse string!", ex);
        }
    }

    @Override
    public void createStringProperty(String key, String value) {
        setValue(key, value);
    }

    @Override
    public void createIntegerProperty(String key, Integer value) {
        setValue(key, Integer.toString(value));
    }

    @Override
    public void createBooleanProperty(String key, Boolean value) {
        setValue(key, Boolean.toString(value));
    }

    @Override
    public void createDateProperty(String key, Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT);
        setValue(key, sdf.format(date));
    }

    private void setValue(String key, String value) {
        SystemProperty s = new SystemProperty();
        s.setKey(key);
        s.setValue(value);
        dao.create(s);
        systemPropertyCache.put(key, s);
    }

    private String getValue(String key) {
        try {
            SystemProperty p = systemPropertyCache.get(key);
            return p.getValue();
        } catch (CacheException ex) {
            log().info("Property key {} not in database!",key);
            return null;
        }
    }
    
    @Override
    public List<SystemProperty> listProperiesToClient() {
        return systemPropertyCache.listByCriteria(new BaseCriteria() {
            @Override
            public boolean execute(Element element, Map<String, AttributeExtractor> attributeExtractors) {
                return ((SystemProperty)element.getValue()).isToClient();
            }
        });        
    }

    @Override
    public List<SystemProperty> listProperties(List<String> keys) {
        return systemPropertyCache.listAll(keys);
    }
    
    @Override
    public List<SystemProperty> listAllProperties() {
        return systemPropertyCache.listAll();
    }

    @Override
    public void updateStringProperty(String key, String value) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        s.setValue(value);
        dao.update(s);
        systemPropertyCache.put(key, s);
    }

    @Override
    public void updateIntegerProperty(String key, Integer value) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        s.setValue(Integer.toString(value));
        dao.update(s);
        systemPropertyCache.put(key, s);
    }

    @Override
    public void updateBooleanProperty(String key, Boolean value) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        s.setValueFromBoolean(value);
        dao.update(s);
        systemPropertyCache.put(key, s);
    }

    @Override
    public void updateDateProperty(String key, Date date) throws NoRecordException {
        SystemProperty s = loadSystemProperty(key);
        SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_FORMAT);
        s.setValue(sdf.format(date));
        dao.update(s);
        systemPropertyCache.put(key, s);
    }

    @Override
    public void updateProperty(String key, String newValue) throws IllegalPropertyValue, NoRecordException {

        SystemProperty s = loadSystemProperty(key);

        log().debug("Update '{}' <- '{}' ", s, newValue);

        SystemPropertyUpdateHandler handler = getUpdateHandlerMap().get(key);
        if (handler != null) {
            handler.handle(s, newValue);
        }
        if (newValue == null) {
            s.setValue("");
        } else {
            s.setValue(newValue);
        }
        dao.update(s);
        systemPropertyCache.put(key, s);
    }

    @Override
    public void updateProperties(Map<String, Object> properties) throws NoRecordException, IllegalPropertyValue {
        for (Map.Entry<String, Object> entry : properties.entrySet()) {
            if (entry.getValue() instanceof Date) {
                updateDateProperty(entry.getKey(), (Date) entry.getValue());
            } else if (entry.getValue() instanceof Integer) {
                updateIntegerProperty(entry.getKey(), (Integer) entry.getValue());
            } else if (entry.getValue() instanceof Boolean) {
                updateBooleanProperty(entry.getKey(), (Boolean) entry.getValue());
            } else if (entry.getValue() == null) {
                updateProperty(entry.getKey(), "");
            } else {
                updateProperty(entry.getKey(), (String) entry.getValue());
            }
        }
    }

    @Override
    public SystemProperty loadSystemProperty(String key) throws NoRecordException {
        return systemPropertyCache.get(key);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    private Map<String, SystemPropertyUpdateHandler> getUpdateHandlerMap() {
        synchronized (createMapLock) {
            if (updateHandlerMap == null) {                
                log().debug("Initializing SystemPropertyUpdateHandler Map");                
                Map<String, SystemPropertyUpdateHandler> map = Maps.newHashMap();
                updateHandlerMap = Collections.synchronizedMap(map);
                Map<String, SystemPropertyUpdateHandler> handlers = context.getBeansOfType(SystemPropertyUpdateHandler.class);
                for (SystemPropertyUpdateHandler handler : handlers.values()) {
                    updateHandlerMap.put(handler.getKey(), handler);                    
                    log().debug("   +{}",handler.getKey());                    
                }
            }
        }
        return updateHandlerMap;
    }

    /**
     * @return the systemPropertyCache
     */
    public SystemPropertyCache getSystemPropertyCache() {
        return systemPropertyCache;
    }

    /**
     * @param systemPropertyCache the systemPropertyCache to set
     */
    public void setSystemPropertyCache(SystemPropertyCache systemPropertyCache) {
        this.systemPropertyCache = systemPropertyCache;
    }
    
    /**
     * @return the dao
     */
    public SystemPropertyDao getDao() {
        return dao;
    }

    /**
     * @param dao the dao to set
     */
    public void setDao(SystemPropertyDao dao) {
        this.dao = dao;
    }

    
}
