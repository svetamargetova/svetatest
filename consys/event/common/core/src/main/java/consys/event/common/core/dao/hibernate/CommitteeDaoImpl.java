package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.dao.CommitteeDao;
import java.util.List;

/**
 *
 * @author Palo
 */
public class CommitteeDaoImpl extends GenericDaoImpl<Committee> implements CommitteeDao {

    @Override
    public void create(Committee c, boolean flush) {
        c.getGroup().setMembersCount(c.getGroup().getMembers().size());
        session().save(c);
        if (flush) {
            session().flush();
        }
    }

    @Override
    public void update(Committee c, boolean flush) {
        c.getGroup().setMembersCount(c.getGroup().getMembers().size());
        session().update(c);
        if (flush) {
            session().flush();
        }
    }

    @Override
    public Committee load(Long id) throws NoRecordException {
        return load(id, Committee.class);
    }

    @Override
    public List<Committee> listCommittees() {
        return session().createQuery("from Committee c order by c.name").list();
    }

    @Override
    public Committee loadCommitteeWithAll(Long id) throws NoRecordException {
        Committee c = (Committee) session().
                createQuery("from Committee c LEFT JOIN FETCH c.group as g LEFT JOIN FETCH c.chairGroup as cg LEFT JOIN FETCH g.members LEFT JOIN FETCH g.rights  LEFT JOIN FETCH cg.members LEFT JOIN FETCH cg.rights where c.id=:ID").
                setLong("ID", id).uniqueResult();
        if (c == null) {
            throw new NoRecordException();
        }
        return c;
    }

    @Override
    public List<ListLongItem> listCommitteesListThumbs() {
        List<Object[]> list = session().
                createQuery("select c.name,c.id from Committee c order by c.name").list();
        List<ListLongItem> items = Lists.newArrayList();
        for (Object[] item : list) {
            items.add(new ListLongItem((String) item[0], (Long) item[1]));
        }
        return items;
    }

    @Override
    public int loadManagingCommittesCount(String userEvent) {
        Long count = (Long) session().
                createQuery("select distinct count(c.id) from Committee c left join c.chairGroup as cg left join cg.members as m where m.uuid=:CUUID").
                setString("CUUID", userEvent).
                uniqueResult();
        return count == null ? 0 : count.intValue();
    }

    @Override
    public List<ListLongItem> listCommitteesForUser(String userUuid) throws NoRecordException {
        List<Object[]> list = session().
                createQuery("select distinct c.name,c.id from Committee c left join c.chairGroup as cg left join cg.members as m where m.uuid=:CUUID order by c.name").
                setString("CUUID", userUuid).
                list();
        List<ListLongItem> items = Lists.newArrayList();
        for (Object[] item : list) {
            items.add(new ListLongItem((String) item[0], (Long) item[1]));
        }
        return items;
    }
}
