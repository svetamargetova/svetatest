package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;
import consys.event.common.core.bo.enums.EventVideoState;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class EventVideo implements ConsysObject {

    private static final long serialVersionUID = -6832098798939565802L;
    // data
    private Long id;
    private String uuid;
    private String systemName;
    private String customName;
    private String description;
    private EventVideoState state;
    private String superlecturesHash;
    private String speakers;
    private String authors;
    private String mediaType;
    private String youTubeId;
    private Boolean toProcessing;
    private Date uploadStarted;
    private String videoUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EventVideoState getState() {
        return state;
    }

    public void setState(EventVideoState state) {
        this.state = state;
    }

    public String getSuperlecturesHash() {
        return superlecturesHash;
    }

    public void setSuperlecturesHash(String superlecturesHash) {
        this.superlecturesHash = superlecturesHash;
    }

    public String getSpeakers() {
        return speakers;
    }

    public void setSpeakers(String speakers) {
        this.speakers = speakers;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getYouTubeId() {
        return youTubeId;
    }

    public void setYouTubeId(String youTubeId) {
        this.youTubeId = youTubeId;
    }

    public Boolean isToProcessing() {
        if (toProcessing == null) {
            toProcessing = false;
        }
        return toProcessing;
    }

    public void setToProcessing(Boolean toProcessing) {
        if (toProcessing == null) {
            this.toProcessing = false;
        } else {
            this.toProcessing = toProcessing;
        }
    }

    public Date getUploadStarted() {
        return uploadStarted;
    }

    public void setUploadStarted(Date uploadStarted) {
        this.uploadStarted = uploadStarted;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.systemName != null ? this.systemName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EventVideo other = (EventVideo) obj;
        if ((this.systemName == null) ? (other.systemName != null) : !this.systemName.equals(other.systemName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EventVideo: " + systemName + " yt: " + youTubeId;
    }
}
