package consys.event.common.core.service.list;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListUtils {

    public static final boolean hasOrderer(int[] orderers, int orderer) {
        for (int i = 0; i < orderers.length; i++) {
            if (orderers[i] == orderer) {
                return true;
            }
        }
        return false;
    }

    public static final boolean isFilter(int filterConstant, Constraints c){
        if(c == null) return false;
        return c.getFilterTag() == filterConstant;
    }
}
