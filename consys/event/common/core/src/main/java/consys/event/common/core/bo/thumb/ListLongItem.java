package consys.event.common.core.bo.thumb;

/**
 * Genericky thumb pre list item s identifikatorom LONG
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListLongItem {

    private String name;
    private Long id;

    public ListLongItem(String name, Long id) {
        this.name = name;
        this.id = id;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

   
}
