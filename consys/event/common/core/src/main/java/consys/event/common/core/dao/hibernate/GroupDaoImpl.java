package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.dao.GroupDao;
import java.util.List;

/**
 *
 * @author Palo
 */
public class GroupDaoImpl extends GenericDaoImpl<Group> implements GroupDao{

    @Override
    public Group load(Long id) throws NoRecordException {
       return load(id, Group.class);
    }

    @Override
    public List<UserEvent> listAllUserEventsWithRightName(String right) throws NoRecordException {
        List<UserEvent> users = session().
                createQuery("select distinct mem from Group g join g.members as mem join g.rights as righ where righ.name=:RIGHT").
                setString("RIGHT", right).
                list();
        if(users == null || users.isEmpty()){
            throw new NoRecordException();
        }
        return users;
    }

}
