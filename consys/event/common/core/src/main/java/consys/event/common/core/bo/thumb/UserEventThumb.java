package consys.event.common.core.bo.thumb;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UserEventThumb {

    private String uuid;
    private String imageProfileUuidPrefix;
    private String name;
    private String position;
    private String organization;

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the imageProfileUuidPrefix
     */
    public String getImageProfileUuidPrefix() {
        return imageProfileUuidPrefix;
    }

    /**
     * @param imageProfileUuidPrefix the imageProfileUuidPrefix to set
     */
    public void setImageProfileUuidPrefix(String imageProfileUuidPrefix) {
        this.imageProfileUuidPrefix = imageProfileUuidPrefix;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
