package consys.event.common.core.service;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.EventVideo;

/**
 *
 * @author pepa
 */
public interface VideoService {

    /** vytvori zaznam o videu */
    void createVideo(EventVideo video);

    /** nacte video podle systemoveho nazvu */
    EventVideo loadVideo(String systemName) throws NoRecordException;

    /** nacte video podle uuid */
    EventVideo loadVideoByUuid(String uuid) throws NoRecordException;

    /** aktualizuje informace o videu */
    void updateVideo(EventVideo video);

    /** spusti kontrolu stavu a zpracovani videi na superlectures */
    void updateSuperlecturesProcessing();

    /** spusti kontrolu stavu a zpracovani videi na youtube */
    void updateYouTubeProcessing();

    /** smaze video */
    void deleteVideo(String systemName) throws NoRecordException;
}
