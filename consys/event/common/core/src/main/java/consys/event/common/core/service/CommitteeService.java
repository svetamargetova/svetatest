package consys.event.common.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.ListLongItem;
import java.util.List;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface CommitteeService {

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    public Committee loadCommitteeWithRightsAndMembers(Long id) throws NoRecordException;

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E--- */
    /*------------------------------------------------------------------------*/
    public Committee createCommittee(String name, Invitation chair, List<String> chairRights, List<Invitation> members, List<String> rights)
            throws NoRecordException,RequiredPropertyNullException,ServiceExecutionFailed;

    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    public void updateCommittee(Long id, String name, Invitation chair,
            List<String> newChairRights, List<String> removedChairRights,
            List<Invitation> newMembers, List<String> removedMembers,
            List<String> newMemberRights, List<String> removedMemberRights) 
            throws NoRecordException, RequiredPropertyNullException,ServiceExecutionFailed;

    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    public List<ListLongItem> listCommittees();

    public List<ListLongItem> listCommitteesForUser(String userUuid) throws NoRecordException,RequiredPropertyNullException;
}
