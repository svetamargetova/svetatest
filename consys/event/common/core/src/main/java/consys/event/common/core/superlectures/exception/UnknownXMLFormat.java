package consys.event.common.core.superlectures.exception;

/**
 * Vyjimka pro neznamy format xml (chybi tag, ...)
 * @author pepa
 */
public class UnknownXMLFormat extends Exception {

    private static final long serialVersionUID = -4985347266633885672L;
    
    public UnknownXMLFormat(String errorText) {
        super(errorText);
    }
}
