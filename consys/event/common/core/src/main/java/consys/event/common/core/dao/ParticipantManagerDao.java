package consys.event.common.core.dao;

import consys.common.core.dao.GenericDao;
import consys.event.common.core.bo.ParticipantManager;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface ParticipantManagerDao extends GenericDao<ParticipantManager> {

    /** kontrola jestli manager spravuje ucastnika */
    boolean checkManagedUser(String uuidManager, String uuidParticipant);

    /** nacte spravovane ucastniky */
    List<ParticipantManager> listManagedParticipants(String uuidManager);

    /** smaze vsechny vazby spravovanych uzivatelu (urceno pro pouziti pri klonovani) */
    int deleteAllManager();
}
