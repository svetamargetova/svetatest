package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author Palo
 */
public class UserEvent implements ConsysObject {

    private static final long serialVersionUID = -8032255486764372265L;
    private Long id;
    private String uuid;
    private Group privateGroup;
    private String lastName;
    private String fullName;
    private String position;
    private String organization;
    private String profileImagePrefix;
    private String email;
    private boolean celiac;
    private boolean vegetarian;
    private boolean systemUser = true;
    private boolean internalUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "UserEvent[name=" + fullName + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        UserEvent e = (UserEvent) obj;
        return e.getUuid().equalsIgnoreCase(uuid);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        return hash;
    }

    public Group getPrivateGroup() {
        return privateGroup;
    }

    public void setPrivateGroup(Group privateGroup) {
        this.privateGroup = privateGroup;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getProfileImagePrefix() {
        return profileImagePrefix;
    }

    public void setProfileImagePrefix(String profileImagePrefix) {
        this.profileImagePrefix = profileImagePrefix;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isCeliac() {
        return celiac;
    }

    public void setCeliac(boolean celiac) {
        this.celiac = celiac;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public boolean isTakeplaceUser() {
        return email.contains("@");
    }

    /** jedna se o uzivatele registrovaneho v systemu (true) nebo jen nekoho kdo je registrovany nekym jinym (false) */
    public boolean isSystemUser() {
        return systemUser;
    }

    public void setSystemUser(boolean systemUser) {
        this.systemUser = systemUser;
    }

    /** uzivatel vytvoreny organizatorem */
    public boolean isInternalUser() {
        return internalUser;
    }

    public void setInternalUser(boolean internalUser) {
        this.internalUser = internalUser;
    }
}
