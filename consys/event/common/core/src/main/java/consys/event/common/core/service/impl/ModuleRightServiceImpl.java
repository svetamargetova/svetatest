package consys.event.common.core.service.impl;

import consys.common.core.exception.NoRecordException;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.collection.Lists;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.dao.ModuleRightDao;
import consys.event.common.core.service.ModuleRightService;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ModuleRightServiceImpl extends AbstractService implements ModuleRightService {
   
    private ModuleRightDao moduleRightDao;

    public void setModuleRightDao(ModuleRightDao moduleRightDao) {
        this.moduleRightDao = moduleRightDao;
    }

    public ModuleRightDao getModuleRightDao() {
        return moduleRightDao;
    }

            

    @Override
    public List<ModuleRight> listUserRightsByUuid(String uuid) throws NoRecordException {
        return moduleRightDao.listUserEventRights(uuid);
    }

    @Override
    public List<ModuleRight> listRightsByName(List<String> names) throws NoRecordException {
        if(names == null || names.isEmpty()){
            return Lists.newArrayList();
        }
        return moduleRightDao.listRightsByName(names);
    }

    @Override
    public List<ModuleRight> listAllRights() {
        return moduleRightDao.listAllRights();
    }
}
