package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author Palo
 */
public class Committee implements ConsysObject {

    private static final long serialVersionUID = -4459368828861696869L;
    private Long id;
    private String name;
    private Group group;
    private Group chairGroup;
 
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

   
    /**
     * @return the group
     */
    public Group getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        Committee e = (Committee) obj;
        return name.equalsIgnoreCase(e.name);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("Committee[name=%s]", name);
    }

    /**
     * @return the chairGroup
     */
    public Group getChairGroup() {
        return chairGroup;
    }

    /**
     * @param chairGroup the chairGroup to set
     */
    public void setChairGroup(Group chairGroup) {
        this.chairGroup = chairGroup;
    }
}
