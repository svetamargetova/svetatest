package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.bo.enums.EventVideoState;
import consys.event.common.core.dao.EventVideoDao;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author pepa
 */
public class EventVideoDaoImpl extends GenericDaoImpl<EventVideo> implements EventVideoDao {

    @Override
    public List<EventVideo> listVideos(Constraints constraints, Paging paging) {
        StringBuilder sb = new StringBuilder("from EventVideo ev");
        sb.append(adminVideoFilter(constraints.getFilterTag()));
        sb.append(" order by ev.customName");

        Query query = session().createQuery(sb.toString());
        query.setFirstResult(paging.getFirstResult());
        query.setMaxResults(paging.getMaxResults());
        return query.list();
    }

    @Override
    public List<EventVideo> listUserVideos(Constraints constraints, Paging paging) {
        Query query = session().createQuery("from EventVideo ev where ev.state=3 order by ev.customName");
        query.setFirstResult(paging.getFirstResult());
        query.setMaxResults(paging.getMaxResults());
        return query.list();
    }

    @Override
    public List<EventVideo> listVideosByState(EventVideoState state) {
        Query query = session().createQuery("from EventVideo ev where ev.state=:STATE and ev.toProcessing=true");
        query.setParameter("STATE", state);
        return query.list();
    }

    @Override
    public EventVideo load(Long id) throws NoRecordException {
        return load(id, EventVideo.class);
    }

    @Override
    public EventVideo loadVideo(String systemName) throws NoRecordException {
        Query q = session().createQuery("from EventVideo ev where ev.systemName=:NAME").setString("NAME", systemName);
        EventVideo video = (EventVideo) q.uniqueResult();
        if (video == null) {
            throw new NoRecordException();
        }
        return video;
    }

    @Override
    public EventVideo loadVideoByUuid(String uuid) throws NoRecordException {
        Query q = session().createQuery("from EventVideo ev where ev.uuid=:UUID").setString("UUID", uuid);
        EventVideo video = (EventVideo) q.uniqueResult();
        if (video == null) {
            throw new NoRecordException();
        }
        return video;
    }

    @Override
    public Long loadVideoCount(Constraints constraints) {
        StringBuilder sb = new StringBuilder("select count(*) from EventVideo ev");
        sb.append(adminVideoFilter(constraints.getFilterTag()));
        return (Long) session().createQuery(sb.toString()).uniqueResult();
    }

    @Override
    public Long loadUserVideoCount(Constraints constraints) {
        return (Long) session().createQuery("select count(*) from EventVideo ev where ev.state=3").uniqueResult();
    }

    @Override
    public int deleteAllVideoRecords() {
        return session().createQuery("delete from EventVideo").executeUpdate();
    }

    private String adminVideoFilter(int filterTag) {
        switch (filterTag) {
            case 2:
                // uploading
                return " where ev.state=0";
            case 3:
                // processing
                return " where ev.state in (1,2,5,6)";
            case 4:
                // processed
                return " where ev.state=3";
            case 5:
                // error
                return " where ev.state=4";
            default:
                // nic nebudeme delat
                return "";
        }
    }
}
