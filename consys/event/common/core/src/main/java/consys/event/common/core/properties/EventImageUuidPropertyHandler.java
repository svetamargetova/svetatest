package consys.event.common.core.properties;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.IllegalPropertyValue;
import consys.event.common.api.properties.CommonProperties;

/**
 *
 * @author pepa
 */
public class EventImageUuidPropertyHandler implements SystemPropertyUpdateHandler {

    @Override
    public String getKey() {
        return CommonProperties.EVENT_IMAGE_UUID;
    }

    @Override
    public void handle(SystemProperty property, String newValue) throws IllegalPropertyValue {
    }
}
