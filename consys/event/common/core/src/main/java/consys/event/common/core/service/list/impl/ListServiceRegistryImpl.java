package consys.event.common.core.service.list.impl;

import consys.event.common.core.service.list.ListService;
import consys.event.common.core.service.list.ListHandlerMissingException;
import consys.event.common.core.service.list.ListServiceRegistry;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListServiceRegistryImpl implements ListServiceRegistry, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(ListServiceRegistry.class);
    
    private ApplicationContext applicationContext;
    private Map<String, ListService> handlers;

    @Override
    public ListService getHandlerByTag(String tag) throws ListHandlerMissingException {
        ListService h = getHandlers().get(tag);
        if (h == null) {
            logger.error("List handler not found for tag: "+tag);
            throw new ListHandlerMissingException();
        }
        return h;
    }

    private Map<String, ListService> getHandlers() {
        if (handlers == null) {
            logger.info("List Service Registry initialization:");
            handlers = new HashMap<String, ListService>();
            Map<String,ListService> services = applicationContext.getBeansOfType(ListService.class);
            for(Map.Entry<String,ListService> s : services.entrySet()){
                logger.info(" -> " + s.getKey() + " tag: "+s.getValue().getTag());
                handlers.put(s.getValue().getTag(), s.getValue());
            }
        }
        return handlers;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
