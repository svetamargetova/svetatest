package consys.event.common.core.servlet;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.servlet.AbstractUploadImageServlet;
import consys.common.utils.UuidProvider;
import consys.event.common.api.servlet.UploadPartnersImageServletProperties;
import consys.event.common.core.context.PropertiesChangedEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.MissingResourceException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

/**
 *
 * @author palo
 */
public class UploadPartnersImageServlet extends AbstractUploadImageServlet implements UploadPartnersImageServletProperties {

    @Autowired
    private SystemPropertyService systemPropertyService;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    protected String saveImage(HttpServletRequest request, String name, BufferedImage bi, String contentType, String subContentType) throws NoRecordException, ServiceExecutionFailed {
        String param = (String) request.getParameter(PARAM_TYPE);
        if (StringUtils.isBlank(param)) {
            throw new MissingResourceException("Missing", this.getClass().getName(), PARAM_TYPE);
        }

        TYPE type = null;
        try {
            type = TYPE.valueOf(param.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new MissingResourceException("Missing", this.getClass().getName(), PARAM_TYPE);
        }

        String imageUuid = UuidProvider.getUuid();
        logger.debug("Uploading Partner image {}", type);

        try {
            // Wall
            int width = type.getWidth();
            createImage(width, bi.getHeight(), bi, name, contentType, subContentType, imageUuid);

        } catch (IOException ex) {
            throw new ServiceExecutionFailed();
        }

        String oldImageUuid = systemPropertyService.getString(type.getPropertyKey());
        try {
            systemPropertyService.updateProperty(type.getPropertyKey(), imageUuid);
        } catch (IllegalPropertyValue ex) {
        }
        if (type.equals(TYPE.FOOTER)) {
            applicationEventPublisher.publishEvent(new PropertiesChangedEvent(this));
        }

        if (StringUtils.isNotBlank(oldImageUuid)) {
            logger.debug("Deleting old image: {}", oldImageUuid);
            deleteImage(oldImageUuid);
        }

        return String.format("<partnerImage>%s</partnerImage>", imageUuid);
    }
}
