package consys.event.common.core.superlectures.so;

import consys.event.common.core.superlectures.SuperlecturesTags;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Zakladni objekt se sdilenymi informacemi o videu
 * @author pepa
 */
public class SuperlecturesBaseObject extends SuperlecturesXMLObject implements SuperlecturesTags {

    // data
    private String videoName;
    private String eventName;
    private String category;
    private List<SuperlecturesPerson> authors;
    private List<SuperlecturesPerson> speekers;
    private String keywords;
    private String description;
    private String recorded;
    private Date datetimeRecorded;
    private String videoUrl;
    private String pdfUrl;
    private String linkUrl;

    public SuperlecturesBaseObject() {
        authors = new ArrayList<SuperlecturesPerson>();
        speekers = new ArrayList<SuperlecturesPerson>();
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<SuperlecturesPerson> getAuthors() {
        return authors;
    }

    public void setAuthors(List<SuperlecturesPerson> authors) {
        this.authors = authors;
    }

    public List<SuperlecturesPerson> getSpeekers() {
        return speekers;
    }

    public void setSpeekers(List<SuperlecturesPerson> speekers) {
        this.speekers = speekers;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRecorded() {
        return recorded;
    }

    public void setRecorded(String recorded) {
        this.recorded = recorded;
    }

    public Date getDatetimeRecorded() {
        return datetimeRecorded;
    }

    public void setDatetimeRecorded(Date datetimeRecorded) {
        this.datetimeRecorded = datetimeRecorded;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    /** nacte uzivatele ze zadaneho tagu */
    protected List<SuperlecturesPerson> listPersonsFromDocument(Document doc, String tagName) throws Exception {
        List<SuperlecturesPerson> persons = new ArrayList<SuperlecturesPerson>();

        Node item = doc.getElementsByTagName(tagName).item(0);
        NodeList list = item.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node n = list.item(i);
            if (n.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            NamedNodeMap attributes = n.getAttributes();

            SuperlecturesPerson person = new SuperlecturesPerson();
            person.setName(getAttribute(ATT_NAME, attributes));

            setPersonItems(person, n.getChildNodes());

            persons.add(person);
        }

        return persons;
    }

    /** nacte polozky uzivatelu: email, facebook, ... */
    private void setPersonItems(SuperlecturesPerson person, NodeList list) {
        for (int i = 0; i < list.getLength(); i++) {
            Node n = list.item(i);
            if (n.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            String tagName = n.getNodeName();
            NamedNodeMap attributes = n.getAttributes();

            SuperlecturesPersonItem item = new SuperlecturesPersonItem();
            item.setSrc(getAttribute(ATT_SRC, attributes));
            item.setTitle(getAttribute(ATT_TITLE, attributes));

            person.getPersonItems().put(tagName, item);
        }
    }
}
