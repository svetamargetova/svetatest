/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core;

import consys.common.core.bo.SystemProperty;
import consys.event.common.core.bo.Module;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;

/**
 * Interface ktory defiunuje sadu volani urcenych k pocitaocnej inicializacii modulu po vytvoreni
 * @author palo
 */
public interface EventModuleRegistrator {

    /**
     * Nazov schematu v databaze. Defaultne public;
     * 
     * @return name or NULL 
     */
    public String getSchemaName();

    /**
     * Inicializacia modulovo zavislych veci
     */
    public void doAfterSessionFactoryCreate(Session session);

    /**
     * Inizializacia systemovych properties
     */
    public List<SystemProperty> getModuleProperties();
    
    /**
     * Inizializacia Modulu a prav
     */
    public Module getModule();

    /**
     * Vrati null alebo mapu kde kluc je nazov fukcie a hodnota je cesta k suboru
     * kde sa tento skript nachadza
     */
    public Map<String,String> getSQLScripts(); 
}
