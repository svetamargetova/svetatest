/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.common.core.bo.thumb;

import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.EventUserRequest;

/**
 * Pozvanka existujuceho uzivatela
 * @author palo
 */
public class ExistingUserInvitation extends Invitation{

    private String uuid;

    public ExistingUserInvitation(String uuid) {
        this.uuid = uuid;
    }


    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    @Override
    public EventUserRequest toRequest() {
        EventUserRequest req = AdminEventWebService.OBJECT_FACTORY.createEventUserRequest();
        req.setMessage(getMessage());
        req.setUserUuid(uuid);
        return req;
    }



}
