package consys.event.common.core.dao.hibernate;

import consys.common.core.dao.GenericDao;
import consys.common.core.bo.ConsysObject;
import consys.common.core.exception.NoRecordException;
import consys.event.overseer.connector.dao.OverseerDaoSupport;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo
 */
public abstract class GenericDaoImpl<T extends ConsysObject> extends OverseerDaoSupport implements GenericDao<T> {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    protected Session session() {
        Session session = getSession(false);
        if (session == null) {
            throw new SessionException("Session is NULL!");
        } else if (!session.getTransaction().isActive()) {
            throw new SessionException("Transaction is not Active!");
        }
        return session;
    }

    protected T throwIfNull(T co, String message) throws NoRecordException {
        if (co == null) {
            throw new NoRecordException(message);
        }
        return co;
    }

    @Override
    public void create(T o) {
        create(o, true);
    }

    @Override
    public void create(T o, boolean flush) {
        session().save(o);
        if (flush) {
            session().flush();
        }
    }

    @Override
    public void update(T o) {
        update(o, true);
    }

    @Override
    public void update(T o, boolean flush) {
        session().update(o);
        if (flush) {
            session().flush();
        }
    }

    @Override
    public void delete(T o) {
        session().delete(o);
        session().flush();
    }

    @Override
    public T load(Long id, Class<T> clazz) throws NoRecordException {
        T l = (T) session().get(clazz, id);
        if (l == null) {
            throw new NoRecordException("No record for " + clazz.getSimpleName() + " with id " + id);
        }
        return l;
    }

    @Override
    public List<T> listAll(Class<T> c) {
        return session().createQuery("from " + c.getName()).list();
    }
}
