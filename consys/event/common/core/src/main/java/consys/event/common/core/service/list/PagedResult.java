package consys.event.common.core.service.list;

import java.util.List;

/**
 * Vysledok zoznamu. V pripade ze bol zadany aj paging tak sa vklada informacia
 * o tom kolko obsahuje zoznam celkovo prvkov.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public final class PagedResult<I> {

    private Long allItemsCount;
    private List<I> items;

    public PagedResult(Long count, List<I> items) {
        this.allItemsCount = count;
        this.items = items;
    }

    /**
     * @return the count
     */
    public Long getAllItemsCount() {
        return allItemsCount;
    }

    public boolean hasAllItemCount(){
        return allItemsCount != null;
    }

    public List<I> getItems() {
        return items;
    }
}
