package consys.event.common.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.UserEvent;
import java.util.List;

/**
 *
 * @author Palo
 */
public interface GroupDao extends GenericDao<Group>{
    
    public List<UserEvent> listAllUserEventsWithRightName(String right) throws NoRecordException;

}
