package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;

/**
 *
 * @author Palo
 */
public class ModuleRight implements ConsysObject {

    private static final long serialVersionUID = 2215786441175651077L;
    private Long id;
    /* NOT - PERSISTENT */
    private String uniqueValue;
    
    /* Nazev prava */
    private String name;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

  

    @Override
    public String toString() {
        return "ModuleRight[name=" + name +"]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ModuleRight e = (ModuleRight) obj;
        return e.name.equalsIgnoreCase(name);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    /**
     * @return the uniqueValue
     */
    public String getUniqueValue() {
        return uniqueValue;
    }

    /**
     * @param uniqueValue the uniqueValue to set
     */
    public void setUniqueValue(String uniqueValue) {
        this.uniqueValue = uniqueValue;
    }

   
}
