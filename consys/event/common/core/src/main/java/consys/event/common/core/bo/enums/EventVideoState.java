package consys.event.common.core.bo.enums;

/**
 * Workflow stavy videa
 * @author pepa
 */
public enum EventVideoState {

    /** zrovna se nahrava */
    UPLOADING(0),
    /** nahrano na s3 */
    UPLOADED(1),
    /** ceka na zpracovani v superlectures */
    SUPERLECTURES_PROCESSING(2),
    /** zpracovano v superlectures */
    SUPERLECTURES_PROCESSED(3),
    /** chyba pri zpracovani */
    PROCESS_ERROR(4),
    /** zpracovavano ze strany takeplace */
    TAKEPLACE_PROCESSING(5),
    /** zpracovano ze strany takeplace */
    TAKEPLACE_PROCESSED(6);
    // data
    private Integer id;

    private EventVideoState(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public static EventVideoState getStateById(Integer id) {
        switch (id) {
            case 0:
                return UPLOADING;
            case 1:
                return UPLOADED;
            case 2:
                return SUPERLECTURES_PROCESSING;
            case 3:
                return SUPERLECTURES_PROCESSED;
            case 4:
                return PROCESS_ERROR;
            case 5:
                return TAKEPLACE_PROCESSING;
            case 6:
                return TAKEPLACE_PROCESSED;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static Integer getIdByState(EventVideoState state) {
        switch (state) {
            case UPLOADING:
                return 0;
            case UPLOADED:
                return 1;
            case SUPERLECTURES_PROCESSING:
                return 2;
            case SUPERLECTURES_PROCESSED:
                return 3;
            case PROCESS_ERROR:
                return 4;
            case TAKEPLACE_PROCESSING:
                return 5;
            case TAKEPLACE_PROCESSED:
                return 6;
            default:
                throw new IllegalArgumentException();
        }
    }
}
