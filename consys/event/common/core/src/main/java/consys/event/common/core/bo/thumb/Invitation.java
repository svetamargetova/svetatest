/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.common.core.bo.thumb;

import consys.admin.event.ws.EventUserRequest;

/**
 * Pozvanka uzivatela do eventu. 
 * @author palo
 */
public abstract class Invitation {
    
    private String message;

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public abstract EventUserRequest toRequest();

}
