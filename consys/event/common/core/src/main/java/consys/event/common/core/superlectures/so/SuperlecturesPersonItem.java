package consys.event.common.core.superlectures.so;

/**
 * Hodnota jedne polozky osoby
 * @author pepa
 */
public class SuperlecturesPersonItem {

    // data
    private String src;
    private String title;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
