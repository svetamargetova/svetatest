package consys.event.common.core.service.list;

/**
 * Objekt popisujuci strankovanie
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public final class Paging {

    private int firstResult;
    private int maxResults;

    /**
     * @return the firstResult
     */
    public int getFirstResult() {
        return firstResult;
    }

    /**
     * @param firstResult the firstResult to set
     */
    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }

    /**
     * @return the maxResults
     */
    public int getMaxResults() {
        return maxResults;
    }

    /**
     * @param maxResults the maxResults to set
     */
    public void setMaxResults(int maxResults) {
        this.maxResults = maxResults;
    }

    @Override
    public String toString() {
        return String.format("Pagign[first=%d max=%d]", firstResult,maxResults);
    }





}
