package consys.event.common.core.service.scheduling;

import consys.common.core.exception.ConsysException;
import consys.event.common.core.service.VideoService;
import consys.event.overseer.connector.EventId;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Zpousti zpracování a kontrolu videii na youtube
 * @author pepa
 */
public class CheckYoutubeProcessing extends ScheduledJob {

    // sluzby
    @Autowired
    private VideoService videoService;
    
    @Override
    protected void doJobForEvent(EventId event) throws ConsysException {
        videoService.updateYouTubeProcessing();
    }

    @Override
    protected String getName() {
        return "YouTube process checking";
    }
}
