/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core.service.impl;

import com.amazonaws.auth.AWSCredentials;
import consys.common.core.notx.MessageType;
import consys.common.core.notx.SystemMessage;
import consys.common.core.service.SystemPropertyService;
import consys.common.core.service.impl.NotxServiceImpl;
import consys.event.common.api.properties.CommonProperties;
import consys.event.overseer.connector.EventRequestContextProvider;
import net.notx.client.PlaceHolders;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Upravuje zakladne chovanie NotxServiceImpl kedy automaticky vklada pred tagy
 * event uuid ako zakladnu domenu
 * @author palo
 */
public class EventNotxServiceImpl extends NotxServiceImpl {

    @Autowired
    private EventRequestContextProvider contextProvider;
    
    @Autowired
    private SystemPropertyService systemPropertyService;

    public EventNotxServiceImpl(AWSCredentials credentials, String endpoint, String queueName) {
        super(credentials, endpoint, queueName);
    }
    
     @Override
    public void sytemNotification(SystemMessage msg, String target, PlaceHolders placeholder) {        
        super.sytemNotification(msg, target, enrichPlaceholders(placeholder));
    }

    @Override
    public void notify(MessageType type, String templateName, String target, PlaceHolders placeholder) {
        super.notify(type, templateName, target, enrichPlaceholders(placeholder));
    }
    
    /**
     * Obohatime placeholdere o nativne prvky
     * @param p
     * @return 
     */
    private PlaceHolders enrichPlaceholders(PlaceHolders p){
        p.addPlaceHolder("event_name", systemPropertyService.getString(CommonProperties.EVENT_NAME));
        return p;
    }

    @Override
    public TaggingBuilder createTaggingBuilder() {
        return new TaggingBuilderImpl(getEventDomain());
    }
    
       
    private String getEventDomain() {
        if (contextProvider.getContext() == null) {
            throw new NullPointerException("Missing request context! ");
        }
        return contextProvider.getContext().getEventUuid().getId();
    }
}
