package consys.event.common.core.service.impl;

import consys.event.common.core.dao.EventVideoDao;
import consys.event.common.core.dao.ParticipantManagerDao;
import consys.event.common.core.service.CommonCloneClearService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CommonCloneClearServiceImpl implements CommonCloneClearService {

    @Autowired
    private EventVideoDao eventVideoDao;
    @Autowired
    private ParticipantManagerDao participantManagerDao;

    @Override
    public void clearClone() {
        eventVideoDao.deleteAllVideoRecords();
        participantManagerDao.deleteAllManager();
    }
}
