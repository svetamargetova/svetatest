package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.dao.ModuleRightDao;

import java.util.List;
import org.hibernate.Query;

/**
 *
 * @author Palo
 */
public class ModuleRightDaoImpl extends GenericDaoImpl<ModuleRight> implements ModuleRightDao {

    @Override
    public ModuleRight load(Long id) throws NoRecordException {
        return load(id, ModuleRight.class);
    }

    @Override
    public ModuleRight loadModuleRightByName(String right) throws NoRecordException {
        ModuleRight mr = (ModuleRight) session().
                createQuery("from ModuleRight mr where mr.name=:RIGHT_NAME").
                setString("RIGHT_NAME", right).
                uniqueResult();
        if(mr == null){
            throw new NoRecordException("No module right with name " + right);
        }
        return mr;
        
    }

    @Override
    public List<ModuleRight> listUserEventRights(Long userId) throws NoRecordException {
        Query q = session().createQuery("select distinct r from ModuleRight r, Group g join g.members as mbr join g.rights as rgh where mbr.id=:USER_ID and r.id=rgh.id");
        q.setLong("USER_ID", userId);
        return q.list();
    }

    @Override
    public List<ModuleRight> listUserEventRights(String userEventUuid) throws NoRecordException {
        Query q = session().createQuery("select distinct r from ModuleRight r, Group g join g.members as mbr join g.rights as rgh where mbr.uuid=:USER_UUID and r.id=rgh.id");
        q.setString("USER_UUID", userEventUuid);
        return q.list();
    }

    @Override
    public boolean hasUserRight(String user, String right) {        
        Query q = session().createQuery("select distinct g.id from ModuleRight r, Group g join g.members as mbr join g.rights as rgh where mbr.uuid=:USER_UUID and r.name=:RIGHT and r.id=rgh.id");
        q.setString("USER_UUID", user).setString("RIGHT", right);
        List<Long> groupIds = q.list();
        return groupIds.isEmpty() ? false : true;
    }

    @Override
    public List<ModuleRight> listRightsByName(List<String> names) throws NoRecordException {
        List<ModuleRight> mr = session().
                createQuery("from ModuleRight mr where mr.name IN (:RIGHT_NAMES)").
                setParameterList("RIGHT_NAMES", names).list();
        if (mr == null || mr.size() != names.size()) {
            throw new NoRecordException();
        }
        return mr;
    }

    @Override
    public List<ModuleRight> listAllRights() {
         List<ModuleRight> mr = session().
                createQuery("from ModuleRight mr").
                list();
         return mr;
    }
}
