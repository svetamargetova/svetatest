package consys.event.common.core.servlet;

import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadProgressMonitor;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author pepa
 */
public class UploadProgressServlet extends HttpServlet {

    private static final long serialVersionUID = -4280439044136438233L;
    // logger
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    // konstanty
    public static final String VIDEO_UPLOAD_SESSION_KEY_PART = "videoUpload";
    // data
    private UploadProgressMonitor monitor;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        monitor = (UploadProgressMonitor) ctx.getBean("uploadProgressMonitor");
    }

    /** Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response) {
        final String user = request.getParameter("uid");
        final String event = request.getParameter("eid");
        final String video = request.getParameter("vid");

        final String attributeName = user + event + video + VIDEO_UPLOAD_SESSION_KEY_PART;

        try {
            response.getOutputStream().print(getUploadProgress(attributeName));
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (IOException ex) {
            logger.error("Sending status error", ex);
        }
        return;
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

    private String getUploadProgress(String attributeName) {
        UploadProgress progress = monitor.getProgress(attributeName);
        if (progress == null) {
            return "100";
        } else {
            DecimalFormat df = new DecimalFormat("#.00");
            return df.format(progress.getUploadPercent());
        }
    }
}
