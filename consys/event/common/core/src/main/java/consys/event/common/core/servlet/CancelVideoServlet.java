package consys.event.common.core.servlet;

import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadProgressMonitor;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author pepa
 */
public class CancelVideoServlet extends HttpServlet {

    private static final long serialVersionUID = -3073340095127119735L;
    // logger
    private Logger log = LoggerFactory.getLogger(CancelVideoServlet.class);
    // konstanty
    private static final String EID = "eid";
    private static final String UID = "uid";
    private static final String VID = "vid";
    // data
    private UploadProgressMonitor monitor;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        monitor = (UploadProgressMonitor) ctx.getBean("uploadProgressMonitor");
    }

    /** Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     */
    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
            final String user = request.getParameter(UID);
            final String event = request.getParameter(EID);
            final String videoUuid = request.getParameter(VID);

            String attributeName = user + event + videoUuid + UploadProgressServlet.VIDEO_UPLOAD_SESSION_KEY_PART;

            UploadProgress progress = monitor.getProgress(attributeName);
            if (progress == null) {
                // uz zruseno nebo skoncilo
                log.debug("Video upload already canceled");
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                // jeste se neco deje
                log.debug("Video uploading in progress");
                monitor.removeUploadProgress(attributeName);
                progress.cancel();
                response.setStatus(HttpServletResponse.SC_OK);
            }
        } catch (Exception ex) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
}
