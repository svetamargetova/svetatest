package consys.event.common.core.youtube;

/**
 * Stavy zpracovani youtube videa
 * @author pepa
 */
public enum YouTubeProcessingState {

    /** video se jeste zpracovava na youtube */
    PROCESSING,
    /** video bylo zpracovano na youtube */
    PROCESSED,
    /** zpracovani videa se na youtube nezdarilo */
    FAILED;
}
