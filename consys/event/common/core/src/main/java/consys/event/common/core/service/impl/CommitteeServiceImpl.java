package consys.event.common.core.service.impl;

import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.EventUserResponse;
import consys.admin.event.ws.LoadUsersToEventRequest;
import consys.admin.event.ws.LoadUsersToEventResponse;
import consys.admin.event.ws.Relation;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.NotX;
import consys.common.core.service.NotxService;
import consys.common.core.service.impl.AbstractService;
import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.api.notx.CommonTags;
import consys.event.common.api.right.EventCommonModuleRoleModel;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.Group;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.dao.CommitteeDao;
import consys.event.common.core.service.CommitteeService;
import consys.event.common.core.service.ModuleRightService;
import consys.event.common.core.service.UserEventService;
import consys.event.common.core.utils.ModuleRightUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.hibernate.dialect.function.NoArgSQLFunction;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CommitteeServiceImpl extends AbstractService implements CommitteeService {

    private CommitteeDao committeeDao;
    private UserEventService userEventService;
    private ModuleRightService moduleRightService;
    private ModuleRightUtils moduleRightUtils;    
    private AdminEventWebService administrationWebService;

    /*------------------------------------------------------------------------*/
    /* ---  L O A D --- */
    /*------------------------------------------------------------------------*/
    @Override
    public Committee loadCommitteeWithRightsAndMembers(Long id) throws NoRecordException {        
        log().debug("Load Committee detail for id={}",id);        
        return committeeDao.loadCommitteeWithAll(id);
    }


    /*------------------------------------------------------------------------*/
    /* ---  L I S T --- */
    /*------------------------------------------------------------------------*/
    @Override
    public List<ListLongItem> listCommitteesForUser(String userUuid) throws NoRecordException, RequiredPropertyNullException {
        if (StringUtils.isBlank(userUuid)) {
            throw new RequiredPropertyNullException();
        }
        // ak ma pravo COMMON_SETTINGS tak vidi vsetky vybory, inak len tie kde je chair
        if (userEventService.hasUserRight(userUuid, EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS)) {
            return committeeDao.listCommitteesListThumbs();
        } else {
            return committeeDao.listCommitteesForUser(userUuid);
        }

    }

    @Override
    public List<ListLongItem> listCommittees() {
        return committeeDao.listCommitteesListThumbs();
    }

    /*------------------------------------------------------------------------*/
    /* ---  C R E A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public Committee createCommittee(String name, Invitation chair, List<String> chairRights, List<Invitation> members, List<String> memberRights)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {
        if (StringUtils.isBlank(name)) {
            throw new RequiredPropertyNullException();
        }
        Map<String, ModuleRight> allRights = getLocalRightCache();
        // Vytvorime vybor a nastavime prava
        Committee c = new Committee();
        c.setChairGroup(new Group());
        c.setGroup(new Group());
        c.setName(name);


        log().debug("Create {}", c);


        // Ak ma nastavene prava chair
        if (chairRights != null && !chairRights.isEmpty()) {
            List<ModuleRight> rights = listRightsFromLocalCache(allRights, chairRights);
            for (ModuleRight right : rights) {
                log().debug("Committee <{}> + CHAIR: {} ", c.getName(), right);
                c.getChairGroup().addRight(right);
            }
        }
        // Ak su nastavene prava clenov
        if (memberRights != null && !memberRights.isEmpty()) {
            List<ModuleRight> rights = listRightsFromLocalCache(allRights, memberRights);
            for (ModuleRight right : rights) {
                log().debug("Committee <{}> + MEMBER: {} ", c.getName(), right);
                c.getGroup().addRight(right);
            }
        }


        // Nacitame info z administracie - operacia zachovava poradie vlozenych 
        LoadUsersToEventRequest req = AdminEventWebService.OBJECT_FACTORY.createLoadUsersToEventRequest();
        req.setInvite(true);
        req.setRelation(Relation.ADMINISTRATOR);

        // Pozvanka pre chaira
        if (chair != null) {
            req.getEventUserRequest().add(chair.toRequest());
        }

        // Pozvanka pre clenov uzivatelov
        if (members != null && !members.isEmpty()) {
            for (Invitation i : members) {
                req.getEventUserRequest().add(i.toRequest());
            }
        }
        
        // userove su pozvany
        List<EventUserResponse> users = null;

        if (!req.getEventUserRequest().isEmpty()) {
            LoadUsersToEventResponse resp = administrationWebService.loadUserToEvent(req);
            users = resp.getEventUserResponse();
        }

        // Nastvime chaira ale ak mal pozvanku
        if (chair != null && users != null && !users.isEmpty()) {
            processCommitteeChair(c, users.remove(0), allRights.get(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR));
        }

        // Nastavime clenov vyboru
        if (users != null && !users.isEmpty()) {
            processCommitteeMembers(c, users);
        }

        // Vytvorenie noveho vyboru
        committeeDao.create(c);
        return c;
    }


    /*------------------------------------------------------------------------*/
    /* ---  U P D A T E --- */
    /*------------------------------------------------------------------------*/
    @Override
    public void updateCommittee(Long id, String name, Invitation chair,
            List<String> newChairRights, List<String> removedChairRights,
            List<Invitation> newMembers, List<String> removedMembers,
            List<String> newMemberRights, List<String> removedMemberRights)
            throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed {

        
        
        
        
        
        
        if (StringUtils.isBlank(name)) {
            throw new RequiredPropertyNullException();
        }

        Committee c = loadCommitteeWithRightsAndMembers(id);
        c.setName(name);
        Map<String, ModuleRight> allRights = getLocalRightCache();

        
        log().debug("Update {}", c);
        

        // Nacitame info z administracie - operacia zachovava poradie vlozenych
        LoadUsersToEventRequest req = AdminEventWebService.OBJECT_FACTORY.createLoadUsersToEventRequest();
        req.setInvite(true);
        req.setRelation(Relation.ADMINISTRATOR);


        // Chair processing - ak tam nie je nikdo nastaveny alebo ten co je nastaveny tak sa nerovna novemu
        boolean processChair = chair != null && processChairUpdate(c, chair);
        if (processChair) {                                       
            removeFormerChairRights(c, allRights);
            c.getChairGroup().getMembers().clear();
            req.getEventUserRequest().add(chair.toRequest());
        }

        // Odstranenie clenov vyboru
        processRemovedMembers(removedMembers, c);
        
        

        // Spracovanie prav pre CHAIRA - NOVE PRAVA
        List<ModuleRight> rights = listRightsFromLocalCache(allRights, newChairRights);
        for (ModuleRight newRight : rights) {
            log().debug("Committee <{}> + CHAIR: {} ", c.getName(), newRight);            
            c.getChairGroup().addRight(newRight);
        }

        // Spracovanie prav pre CHAIRA - ODSTRANENE PRAVA
        rights = listRightsFromLocalCache(allRights, removedChairRights);
        for (ModuleRight delRight : rights) {
            log().debug("Committee <{}> - CHAIR: {} ", c.getName(), delRight);            
            c.getChairGroup().getRights().remove(delRight);
        }

        // Spracovanie prav pre MEMBROV - NOVE PRAVA
        rights = listRightsFromLocalCache(allRights, newMemberRights);
        for (ModuleRight newRight : rights) {
            log().debug("Committee <{}> + MEMBER: {} ", c.getName(), newRight);            
            c.getGroup().addRight(newRight);
        }

        // Spracovanie prav pre MEMBROV - ODSTRANENE PRAVA
        rights = listRightsFromLocalCache(allRights, removedMemberRights);
        for (ModuleRight delRight : rights) {
            log().debug("Committee <{}> - MEMBER: {} ", c.getName(), delRight);
            c.getGroup().getRights().remove(delRight);
        }


        // Pozvanka pre novych clenov uzivatelov
        for (Invitation i : newMembers) {
            req.getEventUserRequest().add(i.toRequest());
        }


        // userove su pozvany
        LoadUsersToEventResponse resp = administrationWebService.loadUserToEvent(req);
        List<EventUserResponse> users = resp.getEventUserResponse();
        // Nastvime chaira ale ak mal pozvanku
        if (processChair) {
            processCommitteeChair(c, users.remove(0), allRights.get(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR));
        }
        // Nastavime clenov vyboru
        processCommitteeMembers(c, users);

        committeeDao.update(c);
        
        
        // tagujeme
        NotxService.TaggingBuilder tagBuilder = NotX.getService().createTaggingBuilder();
        NotxService.TaggingBuilder untagBuilder = NotX.getService().createTaggingBuilder();
                
        // Tag neobsahuje chair uuid
        for(EventUserResponse user : users){
            tagBuilder.addUser(user.getUserUuid());
        }        
                
        untagBuilder.addUsers(removedMembers);
        
        untagBuilder.addTag(CommonTags.COMMITTEE,c.getId().toString());
        tagBuilder.addTag(CommonTags.COMMITTEE,c.getId().toString());
        
        NotX.getService().tag(tagBuilder);
        NotX.getService().unTag(untagBuilder);                        
    }

    /*------------------------------------------------------------------------*/
    /* ---  H E L P   M E T H O D S --- */
    /*------------------------------------------------------------------------*/
    /**
     * Na zaklade pozvanky chaira metoda vrati priznak ci sa ma chair spracovat,
     * teda:
     * - chair este nie je nastaveny
     * - pozvanka je pre noveho uzivatela
     * - uuid chaira a noveho chaira je odlisna
     */
    private boolean processChairUpdate(Committee c, Invitation i) {
        boolean process = false;
        // ak je chair zatial prazdny tak spracovat
        if (c.getChairGroup().getMembers().isEmpty()) {
            process = true;
        } else if (i instanceof ExistingUserInvitation) {
            if (c.getChairGroup().getMembers().iterator().next().getUuid().equalsIgnoreCase(((ExistingUserInvitation) i).getUuid())) {
                process = false;
            } else {
                process = true;
            }
        } else {
            // ak sa jedna o neexistujuceho uzivatela tak true
            process = true;
        }
        return process;
    }

    /**
     * Metoda odstrani stavajucemu chairovy pravo chaira ak uz nie je chairom ziadneho
     * vybory
     * 
     * @return vrati odstraneneho chaira
     */
    private UserEvent removeFormerChairRights(Committee c, Map<String, ModuleRight> allRights) throws NoRecordException {
        // Odstranime staremu chairovy pravo na spracovanie vyborov
        if (!c.getChairGroup().getMembers().isEmpty()) {
            UserEvent oldChair = c.getChairGroup().getMembers().iterator().next();
            // ak je pocet spravovanych vyborov == 1 potom odstranime pravo
            int count = committeeDao.loadManagingCommittesCount(oldChair.getUuid());
            if (count <= 1) {                
                log().debug("Removing Old Chair ROLE_COMMITTEE_CHAIR right.");                
                oldChair.getPrivateGroup().getRights().remove(allRights.get(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR));
                userEventService.updateUserEvent(oldChair);
            } else {                
                 log().debug("Old Committee Chair {} is still chair of next {} committees.",oldChair,(count - 1));                
            }
            return oldChair;
        }
        return null;
    }

    private void processRemovedMembers(List<String> removedMembers, Committee c) {
        if (removedMembers != null || !removedMembers.isEmpty()) {
            
            
            
            for (String memberUuid : removedMembers) {
                UserEvent removeMember = null;
                // Najdi clena vyboru vo vybore podla uuid
                for (Iterator<UserEvent> it = c.getGroup().getMembers().iterator(); it.hasNext();) {
                    UserEvent member = it.next();
                    if (member.getUuid().equalsIgnoreCase(memberUuid)) {
                        removeMember = member;
                    }
                }
                // Ak sa nenasiel tak chyba
                if (removeMember == null) {
                    throw new NullPointerException("Member with uuid " + memberUuid + " not found in committee " + c);
                }
                // log a ostranienie clena z                
                log().debug("Committee<{}> - MEMBER {}", c.getName(),removeMember);                
                c.getGroup().getMembers().remove(removeMember);
            }
        }
    }

    private void processCommitteeChair(Committee c, EventUserResponse chairResponse, ModuleRight committeeChair)
            throws NoRecordException, ServiceExecutionFailed, RequiredPropertyNullException {
        if (chairResponse != null) {
            UserEvent chair = userEventService.createUserOrLoad(chairResponse);
            // nastavimeho ho do vyboru
            c.getChairGroup().addMember(chair);
            c.getChairGroup().setMembersCount(1);
            
            log().debug("Committee <{}> CHAIR {}",c.getName(), chair);
            // nastavime mu privatne pravo ze je chair ak pravo este nam
            if (!userEventService.hasUserRight(chair.getUuid(), committeeChair.getName())) {
                if (log().isDebugEnabled()) {
                    log().debug(chair + " + " + committeeChair.getName());
                }
                chair.getPrivateGroup().addRight(committeeChair);
                userEventService.updateUserEvent(chair);
            }
        }
    }

    /**
     * Spracuje pozvanych clenov vyboru
     */
    private void processCommitteeMembers(Committee c, List<EventUserResponse> responses)
            throws ServiceExecutionFailed, RequiredPropertyNullException {
        if (responses != null && !responses.isEmpty()) {
            // ulozime si chaira v pripade duplikacie aby sa neopkaovalo zbytecny select
            UserEvent chair = c.getChairGroup().getMembers().isEmpty() ? null : c.getChairGroup().getMembers().iterator().next();
            UserEvent newMember = null;
            for (EventUserResponse response : responses) {
                if (chair != null && chair.getUuid().equalsIgnoreCase(response.getUserUuid())) {
                    newMember = chair;
                } else {
                    newMember = userEventService.createUserOrLoad(response);
                }                
                log().debug("Committee<{}> + MEMBER {}", c.getName(),newMember);                
                c.getGroup().addMember(newMember);
            }
        }
    }

    private Map<String, ModuleRight> getLocalRightCache() {
        // lokalna cache s pravami v tejto session z dovodu duplikovania prav
        Map<String, ModuleRight> localRightCache = Maps.newHashMap();
        List<ModuleRight> rights = moduleRightService.listAllRights();


        for (ModuleRight right : rights) {
            localRightCache.put(right.getName(), right);
        }
        return localRightCache;


    }

    private List<ModuleRight> listRightsFromLocalCache(Map<String, ModuleRight> cache, List<String> names) {
        List<ModuleRight> rights = Lists.newArrayList();
        if (names != null && !names.isEmpty()) {
            for (String name : names) {
                ModuleRight right = cache.get(name);
                rights.add(right);
            }
            if (rights.size() != names.size()) {
                log().info("Matching rights failed");
            }
        }
        return rights;


    }

    /*========================================================================*/
    /**
     * @return the committeeDao
     */
    public CommitteeDao getCommitteeDao() {
        return committeeDao;
    }

    /**
     * @param committeeDao the committeeDao to set
     */
    public void setCommitteeDao(CommitteeDao committeeDao) {
        this.committeeDao = committeeDao;
    }

    /**
     * @return the userEventService
     */
    public UserEventService getUserEventService() {
        return userEventService;
    }

    /**
     * @param userEventService the userEventService to set
     */
    public void setUserEventService(UserEventService userEventService) {
        this.userEventService = userEventService;
    }

    /**
     * @return the moduleRightService
     */
    public ModuleRightService getModuleRightService() {
        return moduleRightService;
    }

    /**
     * @param moduleRightService the moduleRightService to set
     */
    public void setModuleRightService(ModuleRightService moduleRightService) {
        this.moduleRightService = moduleRightService;
    }

    /**
     * @return the moduleRightUtils
     */
    public ModuleRightUtils getModuleRightUtils() {
        return moduleRightUtils;
    }

    /**
     * @param moduleRightUtils the moduleRightUtils to set
     */
    public void setModuleRightUtils(ModuleRightUtils moduleRightUtils) {
        this.moduleRightUtils = moduleRightUtils;
    }
 

    /**
     * @return the administrationWebService
     */
    public AdminEventWebService getAdminEventWebService() {
        return administrationWebService;
    }

    /**
     * @param administrationWebService the administrationWebService to set
     */
    public void setAdminEventWebService(AdminEventWebService administrationWebService) {
        this.administrationWebService = administrationWebService;
    }
}
