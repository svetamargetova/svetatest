/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.Module;

/**
 * DAO pro pracu s modulama
 * @author Palo
 */
public interface ModuleDao extends GenericDao<Module> {    

    public Module loadModuleByName(String name) throws NoRecordException;
    
    public Module loadModuleWithRightsByName(String name) throws NoRecordException;

}
