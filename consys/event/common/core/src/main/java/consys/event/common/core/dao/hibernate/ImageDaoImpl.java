package consys.event.common.core.dao.hibernate;

import consys.common.core.dao.ImageDao;
import consys.common.core.bo.Image;
import consys.common.core.exception.NoRecordException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Palci
 */
public class ImageDaoImpl extends GenericDaoImpl<Image> implements ImageDao {

    @Override
    public int deleteByUuid(String uid) {
        Session session = session();
        String HQL = "delete from Image i where i.uuid=:uuid";
        Query q = session.createQuery(HQL).setString("uuid", uid);
        return q.executeUpdate();
    }

    @Override
    public int deleteByUuidPrefix(String uuid) {
        Session session = session();
        String HQL = "delete from Image i where i.uuid like :uuid";
        Query q = session.createQuery(HQL).setString("uuid", uuid+"%");
        return q.executeUpdate();
    }

    @Override
    public Image load(Long id) throws NoRecordException {
        return load(id, Image.class);
    }

    @Override
    public Image loadByUid(String uid) throws NoRecordException {
        String HQL = "from Image i where i.uuid=:uuid";
        Query q = session().createQuery(HQL);
        q.setString("uuid", uid);
        Image image = (Image) q.uniqueResult();
        if (image == null) {
            throw new NoRecordException("Image not exists: " + uid);
        }
        return image;
    }
}
