package consys.event.common.core.superlectures.so;

import org.w3c.dom.Document;

/**
 *
 * @author pepa
 */
public interface SuperlecturesResponse {

    /** provede nacteni dat z xml dokumentu */
    void loadFromXML(Document doc) throws Exception;
}
