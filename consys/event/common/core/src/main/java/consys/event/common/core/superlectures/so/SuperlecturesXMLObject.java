package consys.event.common.core.superlectures.so;

import java.io.StringWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

/**
 * Zaklad pro xml objekty dotazu a odpovedi
 * @author pepa
 */
public abstract class SuperlecturesXMLObject {

    // logger
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    // format zapisu datumu a casu
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /** vytvori novy tag a pripoji ho k elementu */
    public static Element appendNewChild(Document doc, Element to, String newTagName) {
        Element element = doc.createElement(newTagName);
        to.appendChild(element);
        return element;
    }

    /** vytvori text a pripoji ho k elementu */
    public static Text appendNewTextChild(Document doc, Element to, String childText) {
        Text text = doc.createTextNode(childText == null ? "" : childText);
        to.appendChild(text);
        return text;
    }

    /** prevede xml dokument na xml string */
    public static String convertDocumentToString(Document doc) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        writer.close();
        return writer.toString();
    }

    /** nacte textovou hodnotu tagu, tag vyhleda podle nazvu a vraci prvni nalezeny */
    public static String firstTagTextValue(Document doc, String tagName) {
        try {
            return doc.getElementsByTagName(tagName).item(0).getTextContent();
        } catch (Exception ex) {
            // vyskytl se problem
            return null;
        }
    }

    /** nacte atribut elementu, pokud takovy neexistuje vraci null */
    public static String getAttribute(String name, NamedNodeMap attributes) {
        Node node = attributes.getNamedItem(name);
        return node == null ? null : node.getNodeValue();
    }

    /** najde prvni odpovidajici element a pozadovany atribut */
    public static String attributeOfElement(Document doc, String tagName, String name) {
        try {
            Node n = doc.getElementsByTagName(tagName).item(0);
            NamedNodeMap attributes = n.getAttributes();
            return getAttribute(name, attributes);
        } catch (Exception ex) {
            // vyskytl se problem
            return null;
        }
    }
}
