package consys.event.common.core.bo;

import consys.common.core.bo.ConsysObject;
import java.util.Date;


/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ImportantDate implements ConsysObject{
    private static final long serialVersionUID = -4084262403882400100L;

    private Long id;
    /** key ktorym sa prelozi dolokalizacie */
    private String key;
    /** important datum - povinne*/
    private Date date;
    /** ak sa jedna o interval tak ukonceni intervalu */
    private Date endDate;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * ak sa jedna o interval tak ukonceni intervalu
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * ak sa jedna o interval tak ukonceni intervalu
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }


        ImportantDate e = (ImportantDate) obj;
        return key.equalsIgnoreCase(e.key);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.key != null ? this.key.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("ImportantDate[key=%s from=%s to=%s]", key,date,endDate);
    }


}
