/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.common.core.dao;

import consys.common.core.dao.GenericDao;
import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.ModuleRight;
import java.util.List;

/**
 *
 * @author palo
 */
public interface ModuleRightDao extends GenericDao<ModuleRight>{

    public ModuleRight loadModuleRightByName(String right) throws NoRecordException;

     /**
     * Nacita vsetky prava k uzivatelovi v organizacii pre jeden event podla ID
     */
    public List<ModuleRight> listUserEventRights(Long userId) throws NoRecordException;

    /**
     * Nacita vsetky prava k uzivatelovi v organizacii pre jeden event podla ID
     */
    public List<ModuleRight> listUserEventRights(String userEventUuid) throws  NoRecordException;

    public List<ModuleRight> listRightsByName(List<String> names) throws NoRecordException;

    public List<ModuleRight> listAllRights();

    public boolean hasUserRight(String user, String right);


}
