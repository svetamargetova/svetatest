/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.core;

import java.util.Map;

/**
 * Interface ktory defiunuje sadu volani urcenych k pocitaocnej inicializacii modulu po vytvoreni
 * @author palo
 */
public abstract class AbstractEventModuleRegistrator implements EventModuleRegistrator {
  
    /**
     * Vrati null alebo mapu kde kluc je nazov fukcie a hodnota je cesta k suboru
     * kde sa tento skript nachadza
     */
    @Override
    public Map<String,String> getSQLScripts(){
        return null;
    }
}
