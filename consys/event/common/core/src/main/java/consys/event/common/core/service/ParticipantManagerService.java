package consys.event.common.core.service;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.event.common.core.bo.ParticipantManager;
import consys.event.common.core.bo.UserEvent;
import java.util.List;

/**
 *
 * @author pepa
 */
public interface ParticipantManagerService {

    /** kontrola jestli je manager spravcem ucastnika */
    boolean checkManagedUser(String uuidManager, String uuidParticipant);

    /** vytvori nove spravovaneho uzivatele */
    ParticipantManager createNewManagedUser(String uuidManager, UserEvent userEvent) throws NoRecordException, RequiredPropertyNullException, ServiceExecutionFailed;

    /** Nacte vsechny spravovane uzivatele pro managera se zadanym uuid */
    List<UserEvent> listManagedParticipants(String uuidManager);
}
