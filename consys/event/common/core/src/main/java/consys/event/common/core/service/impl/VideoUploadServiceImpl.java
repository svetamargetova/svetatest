package consys.event.common.core.service.impl;

import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadStatus;
import consys.event.common.core.service.VideoUploadService;
import java.io.File;
import java.io.InputStream;

/**
 *
 * @author pepa
 */
public class VideoUploadServiceImpl implements VideoUploadService {

    // servicy
    private AwsFileStorageService storageService;

    @Override
    public UploadStatus uploadVideo(String bundle, String fileName, String path, UploadProgress uploadStatus) {
        return storageService.uploadFile(bundle, fileName, new File(path), uploadStatus);
    }

    @Override
    public UploadStatus uploadVideo(String bucketName, String fileName, InputStream is, String contentType, UploadProgress uploadStatus) {
        return storageService.uploadFile(bucketName, fileName, is, contentType, uploadStatus);
    }

    @Override
    public void deleteVideo(String bucketName, String fileName) {
        storageService.deleteObject(bucketName, fileName);
    }

    public void setStorageService(AwsFileStorageService storageService) {
        this.storageService = storageService;
    }
}
