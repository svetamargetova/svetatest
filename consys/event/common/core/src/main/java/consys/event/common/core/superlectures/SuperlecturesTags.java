package consys.event.common.core.superlectures;

/**
 *
 * @author pepa
 */
public interface SuperlecturesTags {

    // konstanty - tagy
    public static final String ATT_HASH = "hash";
    public static final String ATT_NAME = "name";
    public static final String ATT_SRC = "src";
    public static final String ATT_TITLE = "title";
    public static final String ATT_EMBEDDED_PAGE_SRC = "embedded_page_src";
    public static final String DOCUMENT_ROOT = "data";
    public static final String TAG_AUTHORS = "authors";
    public static final String TAG_CATEGORY = "category";
    public static final String TAG_CODE = "code";
    public static final String TAG_DATETIME_RECORDED = "datetime_recorded";
    public static final String TAG_DESCRIPTION = "description";
    public static final String TAG_EVENT = "event";
    public static final String TAG_KEYWORDS = "keywords";
    public static final String TAG_LECTURE = "lecture";
    public static final String TAG_LINK = "link";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_NAME = "name";
    public static final String TAG_PDF = "pdf";
    public static final String TAG_PERSON = "person";
    public static final String TAG_RECORDED = "recorded";
    public static final String TAG_SPEAKERS = "speakers";
    public static final String TAG_STATUS = "status";
    public static final String TAG_VIDEO = "video";
}
