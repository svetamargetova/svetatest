package consys.event.common.core.dao.hibernate;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.Module;
import consys.event.common.core.dao.ModuleDao;
import java.util.List;

/**
 *
 * @author Palo
 */
public class ModuleDaoImpl extends GenericDaoImpl<Module> implements ModuleDao {

    @Override
    public Module load(Long id) throws NoRecordException {
        return load(id, Module.class);
    }

    @Override
    public Module loadModuleByName(String name) throws NoRecordException {
        Module m = (Module) session().
                createQuery("from Module m where m.name=:MODULE_NAME").
                setString("MODULE_NAME", name).
                uniqueResult();
        if (m == null) {
            throw  new NoRecordException("Module with name "+name+" not exists!");
        }
        return m;
    }

    @Override
    public Module loadModuleWithRightsByName(String name) throws NoRecordException {
        Module m = (Module) session().
                createQuery("from Module m LEFT JOIN FETCH m.rights where m.name=:MODULE_NAME").
                setString("MODULE_NAME", name).
                uniqueResult();
        if (m == null) {
            throw  new NoRecordException("Module with name "+name+" not exists!");
        }
        return m;
    }
}
