package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Mock akcia ktora sa nepreposiela do eventu a kontroluje ci ma user priradene
 * pravo podla right.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CurrentEventUserRightAction  extends EventAction<BooleanResult>{
    private static final long serialVersionUID = 6906129002599591732L;

    private String right;

    public CurrentEventUserRightAction() {
    }

    public CurrentEventUserRightAction(String right) {
        this.right = right;
    }

    /**
     * @return the right
     */
    public String getRight() {
        return right;
    }
    
}
