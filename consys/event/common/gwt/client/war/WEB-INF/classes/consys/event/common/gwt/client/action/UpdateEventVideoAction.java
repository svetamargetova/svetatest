package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Aktualizuje informace o videu
 * @author pepa
 */
public class UpdateEventVideoAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -5046574227306294978L;
    // data
    private String systemName;
    private String name;
    private String description;
    private String authors;
    private String speakers;

    public UpdateEventVideoAction() {
    }

    public UpdateEventVideoAction(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemName() {
        return systemName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getSpeakers() {
        return speakers;
    }

    public void setSpeakers(String speakers) {
        this.speakers = speakers;
    }
}
