package consys.event.common.gwt.client.module.event.committees;

import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.ui.comp.panel.ListPanel;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RightList extends ListPanel<RightListItem> {

    public RightList() {
        this(5);
    }

    public RightList(int visible) {
        super(RightListItem.HEIGHT, visible);
    }

    public void setRights(List<ModuleRole> rightsIdentifiers) {
        clear();
        for (ModuleRole role : rightsIdentifiers) {
            addItem(new RightListItem(role, false));
        }
    }
}
