package consys.event.common.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.event.common.gwt.client.utils.css.ECoBundle;


/**
 *
 * @author pepa
 */
public class ECoResourceUtils {

    private static ECoBundle bundle;

    static{
        bundle = (ECoBundle) GWT.create(ECoBundle.class);
        bundle.css().ensureInjected();
    }

     

    public static ECoBundle bundle() {
        return bundle;
    }
}
