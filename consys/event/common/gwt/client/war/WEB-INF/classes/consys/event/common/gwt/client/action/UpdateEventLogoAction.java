package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

public class UpdateEventLogoAction implements Action<VoidResult>{
    private static final long serialVersionUID = -4025340046216761221L;

    private String logoPregix;
    private boolean profile;

    public UpdateEventLogoAction() {
    }

    public UpdateEventLogoAction(String logoPregix,boolean profile) {
        this.logoPregix = logoPregix;
        this.profile = profile;
    }

    public String getLogoPregix() {
        return logoPregix;
    }

    /**
     * @return the profile
     */
    public boolean isProfile() {
        return profile;
    }

    
    

}
