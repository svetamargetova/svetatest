package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;

/**
 * Akce pro nacteni platebniho profilu
 * @author pepa
 */
public class LoadUserPaymentProfileAction implements Action<ClientUserPaymentProfile> {

    private static final long serialVersionUID = 864172475360168777L;
    // data
    private String uuid;

    public LoadUserPaymentProfileAction() {
    }

    public LoadUserPaymentProfileAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
