package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 * Informace o licenci eventu
 * 
 * licensePurchased == true
 *  => show button download [ licensePaid ? Invoice : Proforma Invoice  ]
 * 
 * @author pepa
 */
public class ClientEventLicense implements Result {

    private static final long serialVersionUID = -3736744606585450130L;
    // data
    private Date activatedFrom;
    private Date activatedTo;
    private Date licensePurchased;
    private Date licenseDue;
    private Date licenseConfirmed;

    public ClientEventLicense() {
    }

    public boolean isLicensePurchased() {
        return getLicensePurchased() != null;
    }

    public boolean isLicensePaid() {
        return getLicenseConfirmed() != null;
    }

    public Date getActivatedFrom() {
        return activatedFrom;
    }

    public void setActivatedFrom(Date activatedFrom) {
        this.activatedFrom = activatedFrom;
    }

    public Date getActivatedTo() {
        return activatedTo;
    }

    public void setActivatedTo(Date activatedTo) {
        this.activatedTo = activatedTo;
    }

    public Date getLicenseConfirmed() {
        return licenseConfirmed;
    }

    public void setLicenseConfirmed(Date licenseConfirmed) {
        this.licenseConfirmed = licenseConfirmed;
    }

    public Date getLicenseDue() {
        return licenseDue;
    }

    public void setLicenseDue(Date licenseDue) {
        this.licenseDue = licenseDue;
    }

    public Date getLicensePurchased() {
        return licensePurchased;
    }

    public void setLicensePurchased(Date licensePurchased) {
        this.licensePurchased = licensePurchased;
    }
}
