package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 * Odpoved na aktivaci eventu
 * @author pepa
 */
public class ClientPaymentActivation implements Result {

    private static final long serialVersionUID = 687396832355345914L;
    // data
    private Date from;
    private Date to;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
