package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro vytvoreni zaznamu o uploadovanem videu
 * @author pepa
 */
public class CreateVideoRecordAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 8806247927920455402L;
    // data
    private String uuid;
    private String fileName;

    public CreateVideoRecordAction() {
    }

    public CreateVideoRecordAction(String uuid, String fileName) {
        this.uuid = uuid;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getUuid() {
        return uuid;
    }
}
