package consys.event.common.gwt.client.module.payment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.payment.PaymentPriceList;
import consys.common.gwt.client.ui.payment.PaymentProfilePanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.bo.ClientEventPaymentProfile;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.gwt.shared.bo.ClientPaymentProfileThumb;
import consys.event.common.gwt.client.action.ListPaymentDataDialogAction;
import consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction;
import consys.event.common.gwt.client.action.LoadUserPaymentProfileAction;
import consys.common.gwt.shared.bo.ClientPaymentOrderData;
import consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction;
import consys.event.common.gwt.client.action.LoadEventPaymentProfileAction;
import consys.event.common.gwt.client.bo.ClientPaymentProfileDialog;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import java.util.ArrayList;

/**
 * Dialog pro platby
 * @author pepa
 */
public abstract class PaymentDialog extends SmartDialog {

    // komponenty
    protected SelectBox<String> profileBox;
    protected PaymentProfilePanel profilePanel;
    protected SimplePanel overviewPanel;
    protected Widget customWidget;
    // data
    // priznak ze zakaznikom tohto platobneho dialogu je event. Z toho dovodu sa budu nacitavat platobne informacie eventovymi profilom
    private boolean eventCustomer;
    // priznak ci sa maju prenacitat data pri zmene profilu
    private boolean reloadProductOnProfileChange;
    private String customerUuid;
    private String currency;
    private boolean zeroInvoice;
    private boolean wasZeroInvoice;
    private ConsysActionWithValue<Boolean> zeroAction;
    private String managedUserUuid;

    public PaymentDialog(String customerUuid, boolean zeroInvoice, boolean showBankInfo, boolean requiredBankInfo) {
        super();
        this.customerUuid = customerUuid;
        this.zeroInvoice = zeroInvoice;

        profilePanel = new PaymentProfilePanel(this, showBankInfo);
        profilePanel.addStyleName(StyleUtils.MARGIN_LEFT_10);
        profilePanel.setVisible(false);
        profilePanel.setRequiredBankInfo(requiredBankInfo);

        profileBox = new SelectBox<String>();
        profileBox.selectFirst(true);
        profileBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {
            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                if (eventCustomer) {
                    EventBus.fire(new DispatchEvent(
                            new LoadEventPaymentProfileAction(event.getValue().getItem()),
                            new AsyncCallback<ClientEventPaymentProfile>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    // obecne chyby zpracovava action executor
                                }

                                @Override
                                public void onSuccess(ClientEventPaymentProfile result) {
                                    profilePanel.setProfile(result);
                                }
                            }, PaymentDialog.this));
                } else {
                    EventBus.fire(new DispatchEvent(
                            new LoadUserPaymentProfileAction(event.getValue().getItem()),
                            new AsyncCallback<ClientUserPaymentProfile>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    // obecne chyby zpracovava action executor
                                }

                                @Override
                                public void onSuccess(ClientUserPaymentProfile result) {
                                    profilePanel.setProfile(result);
                                }
                            }, PaymentDialog.this));
                }
            }
        });

        overviewPanel = new SimplePanel();
        DOM.setStyleAttribute(overviewPanel.getElement(), "minHeight", "10px");
    }

    public void setManagedUserUuid(String managedUserUuid) {
        this.managedUserUuid = managedUserUuid;
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        if (customWidget == null) {
            callActions();
        }
    }

    private void callActions() {
        EventAction<ClientPaymentProfileDialog> action;
        if (eventCustomer) {
            action = new ListEventPaymentProfileDialogAction();
        } else {
            if (managedUserUuid == null) {
                action = new ListUserPaymentProfileDialogAction();
            } else {
                action = new ListUserPaymentProfileDialogAction(managedUserUuid);
            }
        }

        EventBus.fire(new EventDispatchEvent(action,
                new AsyncCallback<ClientPaymentProfileDialog>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                        if (caught instanceof PermissionException) {
                            getFailMessage().setText(UIMessageUtils.c.const_error_noPermissionForAction());
                        }
                    }

                    @Override
                    public void onSuccess(ClientPaymentProfileDialog result) {
                        ArrayList<SelectBoxItem<String>> list = new ArrayList<SelectBoxItem<String>>();
                        if (result.getProfileThumbs() == null || result.getProfileThumbs().isEmpty()) {
                            list.add(new SelectBoxItem<String>("", ""));
                        } else {
                            for (ClientPaymentProfileThumb t : result.getProfileThumbs()) {
                                list.add(new SelectBoxItem<String>(t.getUuid(), t.getName()));
                            }
                        }
                        profileBox.setItems(list);
                        profilePanel.setProfile(result.getFirstProfile());
                        runCustomDataAction(getDataAction());
                    }
                }, this));
    }

    /** akce pro nacitani dat dialogu */
    public abstract ListPaymentDataDialogAction getDataAction();

    /** urceno k pretizeni, pokud je potreba zpracovat jine nez obecne vyjimky */
    public void processDataActionFailure(Throwable caught, ConsysMessage fail) {
    }

    /** spusti akci pro nacitani dat dialogu, mela by byt stejna jako je v getDataAction (rozdil jen v parametrech)  */
    public void runCustomDataAction(ListPaymentDataDialogAction action) {
        action.setCustomerProfileUuid(profilePanel.getClientPaymentProfile().getUuid());
        EventBus.fire(new EventDispatchEvent(action,
                new AsyncCallback<ClientPaymentOrderData>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                        processDataActionFailure(caught, getFailMessage());
                    }

                    @Override
                    public void onSuccess(ClientPaymentOrderData result) {
                        wasZeroInvoice = result.getTotalPrice().isZero();
                        zeroAction.run(zeroInvoice ? wasZeroInvoice : false);
                        overviewPanel.setWidget(generateListData(result, null));
                        if (wasZeroInvoice) {
                            doAction(profilePanel, customerUuid, getFailMessage());
                        }
                    }
                }, PaymentDialog.this));
    }

    public boolean wasZeroInvoice() {
        return wasZeroInvoice;
    }

    public Widget getCustomWidget() {
        return customWidget;
    }

    public void setCustomWidget(Widget customWidget) {
        this.customWidget = customWidget;
    }

    @Override
    protected void generateContent(SimpleFormPanel panel) {
        if (customWidget != null) {
            drawCustomWidgetContent();
        } else {
            drawContent(true);
        }
    }

    protected void drawCustomWidgetContent() {
        if (customWidget != null) {
            panel.clear();
            panel.addWidget(customWidget);
        }
    }

    public void drawContent(boolean showCancel) {
        // vycpavka, aby se spravne umistil obsah do ramecku dialogu
        SimplePanel padder = new SimplePanel();
        padder.setSize("10px", "10px");

        final FlowPanel profileSelectPanel = new FlowPanel();
        profileSelectPanel.addStyleName(StyleUtils.MARGIN_LEFT_10);
        profileSelectPanel.addStyleName(StyleUtils.MARGIN_BOT_10);
        profileSelectPanel.add(padder);
        //profileSelectPanel.add(profileBox);

        ActionImage button = ActionImage.getConfirmButton(ECoMessageUtils.c.paymentDialog_button_ok());
        button.addStyleName(ECoResourceUtils.bundle().css().paymentDialogOk());
        button.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                doAction(profilePanel, customerUuid, getFailMessage());
            }
        });

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), StyleUtils.FLOAT_LEFT + " " + StyleUtils.MARGIN_TOP_3);
        cancel.setClickHandler(hideClickHandler());

        ActionLabel back = new ActionLabel(UIMessageUtils.c.const_back(), StyleUtils.FLOAT_LEFT + " " + StyleUtils.MARGIN_TOP_3);
        back.setClickHandler(backClickHandler());

        FlowPanel controlPanel = new FlowPanel();
        controlPanel.add(button);
        if (customWidget != null && !showCancel) {
            controlPanel.add(back);
        } else {
            controlPanel.add(cancel);
        }
        controlPanel.add(StyleUtils.clearDiv("10px"));

        final Separator separator1 = new Separator("580px", StyleUtils.MARGIN_VER_10);
        separator1.addStyleName(StyleUtils.MARGIN_HOR_10);
        separator1.setVisible(false);

        Separator separator2 = new Separator("580px", StyleUtils.MARGIN_VER_10);
        separator2.addStyleName(StyleUtils.MARGIN_HOR_10);

        zeroAction = new ConsysActionWithValue<Boolean>() {
            @Override
            public void run(Boolean data) {
                profileSelectPanel.setVisible(!data);
                profilePanel.setVisible(!data);
                separator1.setVisible(!data);
            }
        };

        panel.clear();
        panel.addWidget(profileSelectPanel);
        panel.addWidget(profilePanel);
        panel.addWidget(separator1);
        panel.addWidget(overviewPanel);
        panel.addWidget(separator2);
        panel.addWidget(controlPanel);

        if (customWidget != null) {
            callActions();
        }
    }

    /** validace a odeslani dat na server */
    public abstract void doAction(PaymentProfilePanel profilePanel, String customerUuid, ConsysMessage fail);

    /** zobrazi data k platbe */
    protected PaymentPriceList generateListData(ClientPaymentOrderData data, String forceCurrency) {
        currency = forceCurrency == null ? ECoMessageUtils.getCurrency() : forceCurrency;
        return new PaymentPriceList(data, currency);
    }

    /** vraci panel pro volani akce z abstraktni tridy (delegat) */
    protected SimpleFormPanel getContentPanel() {
        return panel;
    }

    /**
     * @return the eventCustomer
     */
    public boolean isEventCustomer() {
        return eventCustomer;
    }

    /**
     * @param eventCustomer the eventCustomer to set
     */
    public void setEventCustomer(boolean eventCustomer) {
        this.eventCustomer = eventCustomer;
    }

    /**
     * @return the reloadProductOnProfileChange
     */
    public boolean isReloadProductOnProfileChange() {
        return reloadProductOnProfileChange;
    }

    /**
     * @param reloadProductOnProfileChange the reloadProductOnProfileChange to set
     */
    public void setReloadProductOnProfileChange(boolean reloadProductOnProfileChange) {
        this.reloadProductOnProfileChange = reloadProductOnProfileChange;
    }

    protected ClickHandler backClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                drawCustomWidgetContent();
            }
        };
    }
}
