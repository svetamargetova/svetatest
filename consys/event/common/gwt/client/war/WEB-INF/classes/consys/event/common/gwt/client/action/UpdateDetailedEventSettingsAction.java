package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientDateTime;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateDetailedEventSettingsAction implements Action<VoidResult> {

    private static final long serialVersionUID = -4746108888401527655L;
    private String eventUuid;
    private ClientDateTime from;
    private ClientDateTime to;
    private String web;
    private String city;
    private String street;
    private String place;
    private Integer location;
    private String timeZone;

    public UpdateDetailedEventSettingsAction() {
    }

    /**
     * @return the from
     */
    public ClientDateTime getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(ClientDateTime from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public ClientDateTime getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(ClientDateTime to) {
        this.to = to;
    }

    /**
     * @return the web
     */
    public String getWeb() {
        return web;
    }

    /**
     * @param web the web to set
     */
    public void setWeb(String web) {
        this.web = web;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * @return the location
     */
    public Integer getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Integer location) {
        this.location = location;
    }

    /**
     * @return the eventUuid
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * @param eventUuid the eventUuid to set
     */
    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

    /**
     * @return the timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone the timeZone to set
     */
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
