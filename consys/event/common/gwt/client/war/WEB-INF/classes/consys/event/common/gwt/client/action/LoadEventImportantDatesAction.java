package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.common.gwt.client.bo.ClientEventImportantDates;

/**
 * Akce pro nacteni dulezitych datumu eventu
 * @author pepa
 */
public class LoadEventImportantDatesAction extends EventAction<ClientEventImportantDates> {

    private static final long serialVersionUID = -3769042427603866336L;
}
