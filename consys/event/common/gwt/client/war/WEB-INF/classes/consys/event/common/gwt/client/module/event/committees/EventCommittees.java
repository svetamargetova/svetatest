package consys.event.common.gwt.client.module.event.committees;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.panel.MenuFormPanel;
import consys.event.common.gwt.client.action.LoadCommitteesAction;
import consys.event.common.gwt.client.bo.ClientCommitteeThumb;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventCommittees extends MenuFormPanel {

    public EventCommittees() {
        super(ECoMessageUtils.c.eventCommittees_title());
        ActionImage addCommittee = ActionImage.getPlusButton(ECoMessageUtils.c.committee_button_addCommittee(), true);
        addCommittee.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                CreateCommitteeDialog ccd = new CreateCommitteeDialog(EventCommittees.this);
                ccd.showCentered();
            }
        });
        addLeftControll(addCommittee);
        initialize();
    }

    private void initialize() {
        EventBus.get().fireEvent(new EventDispatchEvent(new LoadCommitteesAction(), new AsyncCallback<ArrayListResult<ClientCommitteeThumb>>() {

            @Override
            public void onFailure(Throwable caught) {
                //
            }

            @Override
            public void onSuccess(ArrayListResult<ClientCommitteeThumb> result) {
                for (ClientCommitteeThumb cct : result.getArrayListResult()) {
                    addItem(new EventCommitteePanel(cct));
                }
                EventCommittees.this.initWidget();
            }
        }, this));

    }
}
