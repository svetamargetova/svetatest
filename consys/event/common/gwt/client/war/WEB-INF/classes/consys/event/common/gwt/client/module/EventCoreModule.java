/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.gwt.client.module;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.event.EventSettingsLoadedEvent;
import consys.common.gwt.client.event.EventSettingsReceivedEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.RoleManager;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.module.event.EventNavigationModel;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.EventSettings;
import consys.event.common.gwt.client.action.LoadUserEventRolesAction;
import consys.event.common.gwt.client.event.ReloadEventMenuEvent;
import consys.event.common.gwt.client.rpc.EventActionExecutor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/**
 * Modul ktory implementuje nastroje dispatch a dalsie ktore su zviazane s
 * eventom a ktore odbremenuju {@link OverseerModule}. Tento modul je na rozdiel
 * od {@link OverseerModule} automaticky nacitany uz pri spusteny GWT.
 *
 * @author palo
 */
public class EventCoreModule implements EventModule,
        EventDispatchEvent.Handler,
        EventSettingsLoadedEvent.Handler,
        ReloadEventMenuEvent.Handler {

    Logger logger = LoggerFactory.getLogger(EventCoreModule.class);

    @Override
    public String getLocalizedModuleName() {
        return "DEFAULT_DISPATCH_MODULE";
    }

    @Override
    public void registerModuleRoles(List<ModuleRole> moduleRoles) {
    }

    @Override
    public void registerEventNavigation(EventNavigationModel model) {
    }

    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
    }

    @Override
    public void registerModule() {
        EventBus.get().addHandler(EventDispatchEvent.TYPE, this);
        EventBus.get().addHandler(EventSettingsLoadedEvent.TYPE, this);
        EventBus.get().addHandler(ReloadEventMenuEvent.TYPE, this);

    }

    @Override
    public void unregisterModule() {
        EventBus.get().removeHandler(EventDispatchEvent.TYPE, this);
        EventBus.get().removeHandler(EventSettingsLoadedEvent.TYPE, this);
        EventBus.get().removeHandler(ReloadEventMenuEvent.TYPE, this);

    }

    /**
     * EventDispatchEvent HANDLER
     */
    @Override
    public void onDispatch(EventDispatchEvent event) {
        // event action executor
        String name = event.getAction().getClass().getName();
        if (name.lastIndexOf('.') > 0) {
            name = name.substring(name.lastIndexOf('.') + 1);
        }
        logger.debug("Dispatching action - " + name);
        if (CacheRegistrator.hasCacheHandler(event.getAction())) {
            CacheRegistrator.getCacheHandler(event.getAction()).doAction(event.getAction(), event.getCallback(), event.getActionDelegate());
        } else {
            EventActionExecutor.execute(event.getAction(), event.getCallback(), event.getActionDelegate());
        }
    }

    @Override
    public void onLoadedSettings(EventSettings settings) {
        try{
        // Nacitame a prelozime prava        
        List<String> clientRoles = translateRoles(settings.getRoles());

        // registrace properties
        for (Entry<String, String> e : settings.getSystemProperties().entrySet()) {
            Cache.get().register(e.getKey(), e.getValue().trim(), true);
        }
        // Na zaklade prav ktere ma user sa vytvori kopia modelu poplatna
        EventBus.get().fireEvent(new EventSettingsReceivedEvent(clientRoles, settings.getSystemProperties(),true));
        }catch(Exception e){
            LoggerFactory.log(EventCoreModule.class,"Exception!");            
        }
    }

    @Override
    public void onMenuReloadRequest(ReloadEventMenuEvent event) {
        logger.info("Reaload event menu");

        EventBus.get().fireEvent(new EventDispatchEvent(new LoadUserEventRolesAction(), new AsyncCallback<ArrayListResult<String>>() {

            @Override
            public void onFailure(Throwable caught) {
                logger.error("Load rights failed!");
            }

            @Override
            public void onSuccess(ArrayListResult<String> result) {
                // Nacitame a prelozime prava        
                List<String> clientRoles = translateRoles(result.getArrayListResult());

                // Pouzijeme rovnake systemove properties                
                EventBus.get().fireEvent(new EventSettingsReceivedEvent(clientRoles, Cache.get().getSystemPropertiesMap(),false));
            }
        }, null));



    }

    private List<String> translateRoles(List<String> serverRoles) {
        // Role z klienta su shortucty
        List<String> clientRoles = new ArrayList<String>();
        for (String identifier : serverRoles) {
            String translatedRole = RoleManager.translateToShortcut(identifier);
            if(StringUtils.isBlank(translatedRole)){
                logger.debug("Event Role - NOT TRANSABLE " + identifier);
            }else{
                logger.debug("Event Role - " + translatedRole);
                clientRoles.add(translatedRole);
            }            
        }
        return clientRoles;
    }
}
