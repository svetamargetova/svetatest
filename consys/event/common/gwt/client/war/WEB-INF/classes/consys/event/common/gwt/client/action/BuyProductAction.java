package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.gwt.shared.bo.ProductThumb;
import java.util.ArrayList;

/**
 * Predek vsech platebnich akci
 * @author pepa
 */
public class BuyProductAction<T extends Result> extends EventAction<T> {

    private static final long serialVersionUID = -8142701379124261156L;
    // data
    private String cutomerUuid;
    private String customerProfileUuid;
    private ArrayList<ProductThumb> products;
    private boolean profileEdited;
    private ClientUserPaymentProfile editedProfile;

    public BuyProductAction() {
        products = new ArrayList<ProductThumb>();
    }

    public BuyProductAction(String customerUuid, String customerProfileUuid, ArrayList<ProductThumb> products) {
        this.cutomerUuid = customerUuid;
        this.customerProfileUuid = customerProfileUuid;
        this.products = products == null ? new ArrayList<ProductThumb>() : products;
    }

    public ArrayList<ProductThumb> getProducts() {
        return products;
    }

    public String getCustomerProfileUuid() {
        return customerProfileUuid;
    }

    public String getCustomerUuid() {
        return cutomerUuid;
    }

    public ClientUserPaymentProfile getEditedProfile() {
        return editedProfile;
    }

    public void setEditedProfile(ClientUserPaymentProfile editedProfile) {
        this.editedProfile = editedProfile;
    }

    public boolean isProfileEdited() {
        return profileEdited;
    }

    public void setProfileEdited(boolean profileEdited) {
        this.profileEdited = profileEdited;
    }
}
