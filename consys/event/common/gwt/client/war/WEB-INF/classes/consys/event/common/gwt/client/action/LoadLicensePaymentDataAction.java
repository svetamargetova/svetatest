package consys.event.common.gwt.client.action;

/**
 *  Akcia na nacitanie dat payment dialogu pri kupe licencie. Zatial prazdna
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadLicensePaymentDataAction extends ListPaymentDataDialogAction {

    private static final long serialVersionUID = -618346803261778347L;
        
    public LoadLicensePaymentDataAction() {
    }           
}
