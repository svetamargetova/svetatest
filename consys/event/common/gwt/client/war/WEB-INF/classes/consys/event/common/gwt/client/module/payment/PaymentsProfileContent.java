package consys.event.common.gwt.client.module.payment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientEventPaymentProfile;
import consys.common.gwt.shared.bo.ClientPaymentProfileThumb;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.constants.currency.CurrencyEnum;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.utils.*;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.action.LoadEventPaymentProfileAction;
import consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction;
import consys.event.common.gwt.client.bo.ClientPaymentSettings;
import consys.event.common.gwt.client.event.UpdateEventPaymentProfileEvent;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import java.util.ArrayList;

/**
 * Profil pro platby
 * @author pepa
 */
public class PaymentsProfileContent extends RUSimpleHelpFormPanel implements UpdateEventPaymentProfileEvent.Handler {

    // konstanty
    private static final CurrencyEnum DEFAULT_CURRENCY = CurrencyEnum.EUR;
    // komponenty
    private SelectBox<String> profileBox;
    private BaseForm form;
    private ConsysStringTextBox nameBox;
    private ConsysStringTextBox cityBox;
    private ConsysStringTextBox streetBox;
    private ConsysStringTextBox zipBox;
    private SelectBox<Integer> countryBox;
    private SelectBox<String> currencyBox;
    private ConsysStringTextBox regNoBox;
    private ConsysStringTextBox vatNoBox;
    private ConsysStringTextBox accountNoBox;
    private ConsysStringTextBox bankNoBox;
    private ConsysTextArea invoiceNoteBox;
    private ConsysCheckBox noVatPayerBox;
    // data
    private ClientEventPaymentProfile profile;

    public PaymentsProfileContent(ClientPaymentSettings data) {
        addHelpText(ECoMessageUtils.c.paymentsProfileContent_helpText1());
        addHelpText(ECoMessageUtils.c.paymentsProfileContent_helpText2());
        //addHelpText(ECoMessageUtils.c.paymentsProfileContent_helpText3());

        profileBox = new SelectBox<String>();
        profileBox.selectFirst(true);
        profileBox.addStyleName(MARGIN_BOT_10);
        ArrayList<SelectBoxItem<String>> list = new ArrayList<SelectBoxItem<String>>();
        for (ClientPaymentProfileThumb t : data.getProfileThumbs()) {
            list.add(new SelectBoxItem<String>(t.getUuid(), t.getName()));
        }
        profileBox.setItems(list);
        profileBox.addChangeValueHandler(new ChangeValueEvent.Handler<SelectBoxItem<String>>() {
            @Override
            public void onChangeValue(ChangeValueEvent<SelectBoxItem<String>> event) {
                EventBus.get().fireEvent(new DispatchEvent(
                        new LoadEventPaymentProfileAction(event.getValue().getItem()),
                        new AsyncCallback<ClientEventPaymentProfile>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava action executor
                            }

                            @Override
                            public void onSuccess(ClientEventPaymentProfile result) {
                                profile = result;
                                setWidget(readForm());
                            }
                        }, PaymentsProfileContent.this));
            }
        });

        profile = data.getFirstProfile();
    }

    @Override
    protected void onLoad() {
        EventBus.get().addHandler(UpdateEventPaymentProfileEvent.TYPE, this);
        super.onLoad();
    }

    @Override
    protected void onUnload() {
        EventBus.get().removeHandler(UpdateEventPaymentProfileEvent.TYPE, this);
        super.onUnload();
    }

    @Override
    public Widget readForm() {
        final Label countryLabel = new Label();

        EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                boolean found = false;
                if (profile != null && profile.getCountry() != null) {
                    for (BoxItem e : result.getList()) {
                        if (profile.getCountry().equals(e.getId())) {
                            countryLabel.setText(e.getName());
                            found = true;
                            break;
                        }
                    }
                }
                if (!found) {
                    countryLabel.setText(FormUtils.NDASH);
                }
            }
        }, this));

        SimplePanel sep = new SimplePanel();
        sep.setSize("30px", "20px");

        SimplePanel sep1 = new SimplePanel();
        sep1.setSize("30px", "20px");

        int row = 0;

        form = new BaseForm("170px");
        form.addOptional(row++, ECoMessageUtils.c.paymentsProfileContent_form_organizationName(), new Label(FormUtils.valueOrDash(profile != null ? profile.getName() : "")));
        form.addOptional(row++, UIMessageUtils.c.const_address_street(), new Label(FormUtils.valueOrDash(profile != null ? profile.getStreet() : "")));
        form.addOptional(row++, UIMessageUtils.c.const_address_city(), new Label(FormUtils.valueOrDash(profile != null ? profile.getCity() : "")));
        form.addOptional(row++, UIMessageUtils.c.const_address_zip(), new Label(FormUtils.valueOrDash(profile != null ? profile.getZip() : "")));
        form.addOptional(row++, UIMessageUtils.c.const_address_location(), countryLabel);
        form.addContent(row++, sep1);
        form.addOptional(row++, UIMessageUtils.c.const_currency(), new Label(FormUtils.valueOrDash(profile != null ? profile.getCurrency() : "")));
        form.addContent(row++, sep);
        form.addOptional(row++, UIMessageUtils.c.paymentProfilePanel_form_accountNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getAccountNo() : "")));
        form.addOptional(row++, UIMessageUtils.c.paymentProfilePanel_form_bankNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getBankNo() : "")));
        form.addOptional(row++, UIMessageUtils.c.paymentProfilePanel_form_registrationNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getRegNo() : "")));
        if (!profile.isNoVatPayer()) {
            form.addOptional(row++, UIMessageUtils.c.paymentProfilePanel_form_vatNumber(), new Label(FormUtils.valueOrDash(profile != null ? profile.getVatNo() : "")));
        }
        form.addOptional(row++, ECoMessageUtils.c.paymentProfileContent_form_noVatPayer(), new Label(UIMessageUtils.yesNo(profile.isNoVatPayer())));
        form.addOptional(row++, ECoMessageUtils.c.paymentsProfileContent_form_invoiceNote(), new Label(FormUtils.valueOrDash(profile != null ? profile.getInvoiceNote() : "")));

        FlowPanel panel = new FlowPanel();
        //panel.add(profileBox);
        panel.add(FormPanelUtils.getOneLinedReadMode(this,
                ECoMessageUtils.c.paymentsProfileContent_title(), UIMessageUtils.c.const_edit()));
        panel.add(form);
        return panel;
    }

    @Override
    public Widget updateForm(final String id) {
        final FlowPanel panel = new FlowPanel();
        //panel.add(profileBox);
        panel.add(StyleUtils.getStyledLabel(ECoMessageUtils.c.paymentsProfileContent_title(), FONT_16PX, FONT_BOLD, MARGIN_BOT_20));

        EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                ArrayList<SelectBoxItem<Integer>> out = new ArrayList<SelectBoxItem<Integer>>();
                for (BoxItem e : result.getList()) {
                    out.add(new SelectBoxItem<Integer>(e.getId(), e.getName()));
                }

                nameBox = new ConsysStringTextBox(1, 128, ECoMessageUtils.c.paymentsProfileContent_form_organizationName());
                nameBox.setText(profile != null ? profile.getName() : "");

                cityBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.const_address_city());
                cityBox.setText(profile != null ? profile.getCity() : "");

                streetBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.const_address_street());
                streetBox.setText(profile != null ? profile.getStreet() : "");

                zipBox = new ConsysStringTextBox(1, 64, UIMessageUtils.c.const_address_zip());
                zipBox.setText(profile != null ? profile.getZip() : "");

                countryBox = new SelectBox<Integer>();
                countryBox.selectFirst(true);
                countryBox.setItems(out);
                if (profile != null && profile.getCountry() != null) {
                    countryBox.selectItem(profile.getCountry());
                }

                SimplePanel sep1 = new SimplePanel();
                sep1.setSize("30px", "20px");

                currencyBox = new SelectBox<String>();
                currencyBox.setItems(UIMessageUtils.currencyItems());
                if (profile != null && profile.getCurrency() != null) {
                    currencyBox.selectItem(profile.getCurrency().toString());
                } else {
                    currencyBox.selectItem(DEFAULT_CURRENCY.toString());
                }

                SimplePanel sep = new SimplePanel();
                sep.setSize("30px", "20px");

                regNoBox = new ConsysStringTextBox(0, 128, UIMessageUtils.c.paymentProfilePanel_form_registrationNumber());
                regNoBox.setText(profile != null ? profile.getRegNo() : "");

                vatNoBox = new ConsysStringTextBox(0, 128, UIMessageUtils.c.paymentProfilePanel_form_vatNumber());
                vatNoBox.setText(profile != null ? profile.getVatNo() : "");

                accountNoBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.paymentProfilePanel_form_accountNumber()) {
                    @Override
                    public boolean doValidate(ConsysMessage fail) {
                        boolean result = super.doValidate(fail);
                        if (result) {
                            String value = getText().trim();
                            if (!value.isEmpty()) {
                                value = value.replaceAll("\\s+", "");
                                value = value.toLowerCase();
                                if (!value.matches("[a-z]{2}[0-9]{2}[a-z0-9]{1,30}")) {
                                    fail.addOrSetText(UIMessageUtils.c.paymentProfilePanel_error_badIban());
                                    return false;
                                }
                            }
                        }
                        return result;
                    }
                };
                accountNoBox.setText(profile != null ? profile.getAccountNo() : "");

                bankNoBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.paymentProfilePanel_form_bankNumber());
                bankNoBox.setText(profile != null ? profile.getBankNo() : "");

                noVatPayerBox = new ConsysCheckBox();
                noVatPayerBox.setValue(profile.isNoVatPayer());

                invoiceNoteBox = new ConsysTextArea(0, 512, ECoMessageUtils.c.paymentsProfileContent_form_invoiceNote());
                invoiceNoteBox.setText(profile.getInvoiceNote());

                ActionImage button = ActionImage.getConfirmButton(UIMessageUtils.c.const_save());
                button.addClickHandler(confirmUpdateClickHandler(id));

                ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
                cancel.setClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        setWidget(readForm());
                    }
                });

                HTML ibanInfo = new HTML(UIMessageUtils.c.paymentProfilePanel_text_supportedText());
                ibanInfo.addStyleName(TEXT_GRAY);
                ibanInfo.addStyleName(ResourceUtils.system().css().htmlAgreeing());
                ibanInfo.addStyleName(MARGIN_TOP_10);
                ibanInfo.addStyleName(MARGIN_BOT_20);
                ibanInfo.addStyleName(FONT_11PX);
                ibanInfo.setWidth("270px");

                int row = 0;
                final int vatNoRow = 12;

                form = new BaseForm("170px");
                form.addRequired(row++, ECoMessageUtils.c.paymentsProfileContent_form_organizationName(), nameBox);
                form.addRequired(row++, UIMessageUtils.c.const_address_street(), streetBox);
                form.addRequired(row++, UIMessageUtils.c.const_address_city(), cityBox);
                form.addRequired(row++, UIMessageUtils.c.const_address_zip(), zipBox);
                form.addRequired(row++, UIMessageUtils.c.const_address_location(), countryBox);
                form.addContent(row++, sep1);
                form.addRequired(row++, UIMessageUtils.c.const_currency(), currencyBox);
                form.addContent(row++, sep);
                form.addRequired(row++, UIMessageUtils.c.paymentProfilePanel_form_accountNumber(), createAccountNumberPanel());
                form.addRequired(row++, UIMessageUtils.c.paymentProfilePanel_form_bankNumber(), createBankNoPanel());
                form.addContent(row++, ibanInfo);
                form.addOptional(row++, UIMessageUtils.c.paymentProfilePanel_form_registrationNumber(), regNoBox);
                form.addOptional(row++, UIMessageUtils.c.paymentProfilePanel_form_vatNumber(), vatNoBox);
                form.addOptional(row++, ECoMessageUtils.c.paymentProfileContent_form_noVatPayer(), noVatPayerBox);
                form.addOptional(row++, ECoMessageUtils.c.paymentsProfileContent_form_invoiceNote(), invoiceNoteBox);
                form.addActionMembers(row++, button, cancel);
                panel.add(form);

                if (profile.isNoVatPayer()) {
                    form.showRow(vatNoRow, false);
                }

                noVatPayerBox.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {
                    @Override
                    public void onChangeValue(ChangeValueEvent<Boolean> event) {
                        form.showRow(vatNoRow, !event.getValue());
                    }
                });
            }
        }, this));

        return panel;
    }

    private ConsysFlowPanel createBankNoPanel() {
        ConsysFlowPanel p = new ConsysFlowPanel();
        p.add(bankNoBox);
        p.add(StyleUtils.getStyledLabel(UIMessageUtils.c.paymentProfilePanel_text_supportedSwift(),
                FONT_11PX, TEXT_GRAY, WIDTH_270));
        return p;
    }

    private ConsysFlowPanel createAccountNumberPanel() {
        ConsysFlowPanel p = new ConsysFlowPanel();
        p.add(accountNoBox);
        p.add(StyleUtils.getStyledLabel(UIMessageUtils.c.paymentProfilePanel_text_supportedIban(),
                FONT_11PX, TEXT_GRAY, WIDTH_270));
        return p;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (!form.validate(getFailMessage())) {
                    return;
                }

                final ClientEventPaymentProfile cpp = new ClientEventPaymentProfile();
                cpp.setUuid(profile.getUuid());
                cpp.setAccountNo(accountNoBox.getText().trim());
                cpp.setBankNo(bankNoBox.getText().trim());
                cpp.setCity(cityBox.getText().trim());
                cpp.setCountry(countryBox.getSelectedItem().getItem());
                cpp.setCurrency(CurrencyEnum.valueOf(currencyBox.getSelectedItem().getItem()));
                cpp.setName(nameBox.getText().trim());
                cpp.setRegNo(regNoBox.getText().trim());
                cpp.setStreet(streetBox.getText().trim());
                cpp.setVatNo(vatNoBox.getText().trim());
                cpp.setZip(zipBox.getText().trim());
                cpp.setInvoiceNote(invoiceNoteBox.getText().trim());
                cpp.setNoVatPayer(noVatPayerBox.getValue());

                EventBus.get().fireEvent(new EventDispatchEvent(
                        new UpdateEventPaymentProfileAction(cpp),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                profile = cpp;
                                setWidget(readForm());
                                Cache.get().register(CommonProperties.EVENT_CURRENCY, cpp.getCurrency().toString());
                            }
                        },
                        PaymentsProfileContent.this));
            }
        };
    }

    @Override
    public void updatePaymentProfile(UpdateEventPaymentProfileEvent event) {
        ClientUserPaymentProfile p = event.getProfile();

        profile.setAccountNo(p.getAccountNo());
        profile.setBankNo(p.getBankNo());
        profile.setCity(p.getCity());
        profile.setCountry(p.getCountry());
        profile.setName(p.getName());
        profile.setRegNo(p.getRegNo());
        profile.setStreet(p.getStreet());
        profile.setVatNo(p.getVatNo());
        profile.setZip(p.getZip());
        profile.setNoVatPayer(p.isNoVatPayer());

        setWidget(readForm());
    }
}
