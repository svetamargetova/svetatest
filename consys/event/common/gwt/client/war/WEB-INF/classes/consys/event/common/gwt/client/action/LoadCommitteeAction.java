package consys.event.common.gwt.client.action;

import consys.event.common.gwt.client.bo.ClientCommittee;
import consys.common.gwt.shared.action.EventAction;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadCommitteeAction extends EventAction<ClientCommittee>{
    private static final long serialVersionUID = -5277076477314974773L;
    
    private Long id;

    public LoadCommitteeAction() {
    }

    public LoadCommitteeAction(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
