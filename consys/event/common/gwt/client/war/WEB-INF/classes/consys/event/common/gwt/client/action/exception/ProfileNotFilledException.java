package consys.event.common.gwt.client.action.exception;

import consys.common.gwt.shared.action.ActionException;

/**
 *
 * @author Palo
 */
public class ProfileNotFilledException extends ActionException {

    private static final long serialVersionUID = -6331817380354178617L;

    public ProfileNotFilledException() {
        super();
    }
}
