package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Smaze video
 * @author pepa
 */
public class DeleteEventVideoAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = -5721792915500797437L;
    // data
    private String systemName;

    public DeleteEventVideoAction() {
    }

    public DeleteEventVideoAction(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemName() {
        return systemName;
    }
}
