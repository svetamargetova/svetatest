package consys.event.common.gwt.client.message;

import com.google.gwt.i18n.client.Messages;

/**
 * abecedne serazene zpravy
 * @author pepa
 */
public interface EventCoreMessages extends Messages {

    String baseSettingsContent_text_infoCloned(String url);
    
    String paymentsLicencesContent_text_confirmed(String eventName, String from, String to);

    String partnerBannerSettings_TypeNote(int width);
}
