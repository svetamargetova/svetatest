package consys.event.common.gwt.client.utils.css;

import com.google.gwt.resources.client.ClientBundle;

/**
 *
 * @author pepa
 */
public interface ECoBundle extends ClientBundle {

    @Source("eco.css")
    public ECoCssResources css();
}
