package consys.event.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Event vystreleny, kdyz se chce uzivatel zaregistrovat do eventu
 * @author pepa
 */
public class EventRegistrationRequestEvent extends GwtEvent<EventRegistrationRequestEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private String bundleUuid;

    public EventRegistrationRequestEvent() {
    }

    public EventRegistrationRequestEvent(String bundleUuid) {
        this.bundleUuid = bundleUuid;
    }

    public String getBundleUuid() {
        return bundleUuid;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onEventRegister(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onEventRegister(EventRegistrationRequestEvent event);
    }
}
