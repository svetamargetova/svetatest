package consys.event.common.gwt.client.module.payment;

/**
 *
 * @author pepa
 */
public enum PaymentPayPalButtonType {

    CARD, PAYPAL;
}
