package consys.event.common.gwt.client.utils.css;

import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.ImportedWithPrefix;

/**
 *
 * @author pepa
 */
@ImportedWithPrefix("eco")
public interface ECoCssResources extends CssResource {

    /** registracni tlacitko na wall */
    String eventWallRegister();

    /** payment */
    String paymentDialogOk();

    /** Right List Panel */
    String rightListItem();

    /** Rights Dialog */
    String rightsDialogBody();

    String rightsDialogList();

    String rightsDialogHelp();

    String rightsDialogHelpTitle();

    String rightsDialogHelpDesc();

    /** styl uzivatelem tvoreneho popisku a vzhledu (HTML) */
    String eventDescription();

    String eventProfile();

    String seriesLabel();

    String redClone();

    String videoItem();

    String videoItemPeople();

    String videoLabel();

    String videoLink();

    String videoState();

    String videoDisplay();

    String videoDisplayL();

    String videoDisplayR();

    String videoDisplayUpload();

    String videoPercentWaiting();

    String videoPercentLabel();

    String videoEdit();
}
