package consys.event.common.gwt.client.bo;

import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import consys.common.gwt.shared.bo.Monetary;
import java.util.Date;

/**
 * Polozka prehledu plateb. Faktura sa generuje na zaklade linku podla UUID
 * transferu
 * @author pepa
 */
public class ClientPaymentTransferHead extends DataListUuidItem {

    private static final long serialVersionUID = 5748187887177849542L;
    // data    
    private Date transferDate;
    private String info;
    /** Sucet vseckych faktur */
    private Monetary total;
    /** Poplatky nad ramec provizie - pay_pal fees a pod */
    private Monetary thirdPartyFees;
    /** Provizia ktoru si bereme */
    private Monetary fees;
    /** Stav transferu **/
    private ClientPaymentTransferState state;

    public String getInfo() {
        return info;
    }

    public void setInfo(String accountNumber) {
        this.info = accountNumber;
    }
    
    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date date) {
        this.transferDate = date;
    }
      
    /**
     * Sucet vseckych faktur
     * @return the total
     */
    public Monetary getTotal() {
        return total;
    }

    /**
     * Sucet vseckych faktur
     * @param total the total to set
     */
    public void setTotal(Monetary total) {
        this.total = total;
    }

    /**
     * Poplatky nad ramec provizie - pay_pal fees a pod
     * @return the fees
     */
    public Monetary getThirdPartyFees() {
        return thirdPartyFees;
    }

    /**
     * Poplatky nad ramec provizie - pay_pal fees a pod
     * @param fees the fees to set
     */
    public void setThirdPartyFees(Monetary fees) {
        this.thirdPartyFees = fees;
    }

    /**
     * Provizia ktoru si bereme
     * @return the commission
     */
    public Monetary getFees() {
        return fees;
    }

    /**
     * Provizia ktoru si bereme
     * @param commission the commission to set
     */
    public void setFees(Monetary commission) {
        this.fees = commission;
    }

    /**
     * Stav transferu
     * @return the state
     */
    public ClientPaymentTransferState getState() {
        return state;
    }

    /**
     * Stav transferu
     * @param state the state to set
     */
    public void setState(ClientPaymentTransferState state) {
        this.state = state;
    }

   

}
