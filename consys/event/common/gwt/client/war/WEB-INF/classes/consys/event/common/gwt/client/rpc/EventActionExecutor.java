package consys.event.common.gwt.client.rpc;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.StatusCodeException;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.SecurityExceptionHandler;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.exception.ActionNotPermittedException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.gwt.shared.exception.UncaughtException;

/**
 * Spousteni akci na serveru
 * <p/>
 * @author pepa
 */
public class EventActionExecutor {

    private static final Logger logger = LoggerFactory.getLogger(EventActionExecutor.class);
    private static AbstractEventDispatchAsync dispatchAsync;

    public static void init() {
        if (dispatchAsync == null) {
            dispatchAsync = GWT.create(AbstractEventDispatchAsync.class);
            dispatchAsync.addExceptionHandler(new SecurityExceptionHandler());
        }
    }

    public static AbstractEventDispatchAsync getDispatchAsync() {
        return dispatchAsync;
    }

    /**
     * necha provest akci na serveru a vrátí výsledek v callbacku
     * <p/>
     * @param action akce, která se má provést na serveru
     * @param callback co se provede s vysledkem nebo pri chybe
     * @param delegate slouzi ke spusteni akci pri spusteni a ukonceni prikazu (napr. zobrazeni/skryti waiting)
     */
    public static void execute(final EventAction action, final AsyncCallback callback, final ActionExecutionDelegate delegate) {
        if (delegate != null) {
            delegate.actionStarted();
        }

        logger.info("Dispatch -> " + action.getClass().toString());

        dispatchAsync.execute(action, new AsyncCallback() {

            @Override
            public void onFailure(Throwable caught) {
                logger.info("Failed (" + action.getClass().toString() + ") - " + caught.toString());
                if (caught instanceof StatusCodeException) {
                    StatusCodeException e = (StatusCodeException) caught;
                    if (e.getStatusCode() == Response.SC_SERVICE_UNAVAILABLE) {
                        Window.Location.reload();
                        return;
                    }
                }
                commonException(delegate, caught);
                callback.onFailure(caught);
                if (delegate != null) {
                    delegate.actionEnds();
                }
            }

            @Override
            public void onSuccess(Object result) {
                if (delegate != null) {
                    delegate.actionEnds();
                }
                callback.onSuccess(result);
            }
        });
    }

    /** zkontroluje jestli se nejedna o obecnou vyjimku service failed nebo bad input */
    private static void commonException(ActionExecutionDelegate source, Throwable caught) {
        if (source == null) {
            // pokud je delegat null, musi si vsechny chyby chytat sam
            return;
        }
        if (caught instanceof ServiceFailedException) {
            source.setFailMessage(CommonExceptionEnum.SERVICE_FAILED);
        } else if (caught instanceof BadInputException) {
            source.setFailMessage(CommonExceptionEnum.BAD_INPUT);
        } else if (caught instanceof StatusCodeException) {
        } else if (caught instanceof UncaughtException) {
            source.setFailMessage(CommonExceptionEnum.SERVICE_FAILED);
        } else if (caught instanceof ActionNotPermittedException) {
            source.setFailMessage(CommonExceptionEnum.NOT_PERMITTED);
        }
    }
}
