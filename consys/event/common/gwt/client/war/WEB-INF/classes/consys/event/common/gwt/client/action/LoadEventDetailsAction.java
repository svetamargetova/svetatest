package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientEvent;

/**
 * Nacita detailne informacie o eventu. Tj. vsetky vlastnosti ktore event ma. 
 * Ak je potreba nacitat len strucne informacie, pouzti  {@link LoadEventInfoAction}.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadEventDetailsAction implements Action<ClientEvent>{
    private static final long serialVersionUID = -4047457288111094922L;
    

    private String eventUuid;

    public LoadEventDetailsAction() {
    }

    public LoadEventDetailsAction(String uuid) {
        this.eventUuid = uuid;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    




}
