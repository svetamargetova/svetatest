package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.UpdateEventPropertyAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.input.ConsysRichTextArea;
import consys.common.gwt.client.ui.comp.input.ConsysRichTextToolbar;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.SwapPanel;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.CorporateUtils;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.EventUtils;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.action.LoadEventDetailsAction;
import consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CommunitySettings extends RUSimpleHelpFormPanel {

    // konstanty
    private static final String READ_FORM_WIDTH_NOPX = "330";
    private static final String READ_FORM_WIDTH = "330px";
    // komponenty
    private ConsysRichTextArea description;
    private ConsysTextArea descriptionAreaHTML;
    private ConsysStringTextBox tags;
    private ConsysStringTextBox webBox;
    private ConsysStringTextBox emailBox;
    private ConsysStringTextBox phoneNumberBox;
    // data
    private ClientEvent clientEvent;
    private boolean inEditor;

    public CommunitySettings() {
        super(true);
        addHelpTitle(ECoMessageUtils.c.communitySettings_helpTitle());
        addHelpText(ECoMessageUtils.c.communitySettings_helpText1());
        addHelpText(ECoMessageUtils.c.communitySettings_helpText2());
        addStyleName(MARGIN_BOT_10);
    }

    @Override
    public Widget readForm() {
        final HTML descriptionHTML = new HTML();
        descriptionHTML.setWidth(READ_FORM_WIDTH);
        descriptionHTML.setStyleName(ECoResourceUtils.bundle().css().eventDescription());

        final Label tagsLabel = readLabel();
        final Label webLabel = readLabel();
        final Label phoneNumberLabel = readLabel();
        final Label emailLabel = readLabel();

        final HTML profileWebPageHTML = new HTML();
        profileWebPageHTML.setWidth(READ_FORM_WIDTH);
        profileWebPageHTML.setStyleName(ECoResourceUtils.bundle().css().eventDescription());

        EventBus.get().fireEvent(new DispatchEvent(new LoadEventDetailsAction(), new AsyncCallback<ClientEvent>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(ClientEvent result) {
                clientEvent = result;
                descriptionHTML.setHTML(FormUtils.valueOrDash(result.getDescription()));
                tagsLabel.setText(FormUtils.valueOrDash(result.getTags()));
                webLabel.setText(FormUtils.valueOrDash(result.getWeb()));

                String webPageUrl = "http://" + EventUtils.profileName(result.getAcronym(), String.valueOf(result.getYear()), result.getSeries()) + ".takeplace.eu/en";
                profileWebPageHTML.setHTML("<a href=\"" + webPageUrl + "\" target=\"blank\">" + webPageUrl + "</a>");
            }
        }, this));

        phoneNumberLabel.setText(FormUtils.valueOrDash((String) Cache.get().get(CommonProperties.EVENT_WEB_PHONE)));
        emailLabel.setText(FormUtils.valueOrDash((String) Cache.get().get(CommonProperties.EVENT_WEB_EMAIL)));

        Label separator = new Label("");
        separator.setSize("100px", "10px");

        ActionLabel showLinkSource = new ActionLabel(ECoMessageUtils.c.communitySettings_action_showLinkSource());
        ActionLabel showWidgetSource = new ActionLabel(ECoMessageUtils.c.communitySettings_action_showWidgetSource());

        final TextArea widgetSource = new TextArea();
        widgetSource.setStyleName(StyleUtils.BORDER);
        widgetSource.addStyleName(ResourceUtils.system().css().inputAreaPadd());
        widgetSource.setReadOnly(true);
        widgetSource.setWidth(READ_FORM_WIDTH);
        widgetSource.setHeight("90px");

        final SwapPanel swapRegistrationWidget = new SwapPanel(showWidgetSource, widgetSource);
        showWidgetSource.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                widgetSource.setText(widgetSource());
                swapRegistrationWidget.swap();
            }
        });

        final TextArea linkSource = new TextArea();
        linkSource.setStyleName(StyleUtils.BORDER);
        linkSource.addStyleName(ResourceUtils.system().css().inputAreaPadd());
        linkSource.setReadOnly(true);
        linkSource.setWidth(READ_FORM_WIDTH);
        linkSource.setHeight("90px");

        final SwapPanel swapRegistrationLink = new SwapPanel(showLinkSource, linkSource);
        showLinkSource.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                linkSource.setText(linkSource());
                swapRegistrationLink.swap();
            }
        });

        Form form = new Form();
        form.addOptional(UIMessageUtils.c.const_description(), descriptionHTML);
        form.addContent(separator);
        form.addOptional(ECoMessageUtils.c.communitySettings_form_tags(), tagsLabel);
        form.addOptional(UIMessageUtils.c.const_website(), webLabel);
        form.addOptional(UIMessageUtils.c.const_phoneNumber(), phoneNumberLabel);
        form.addOptional(UIMessageUtils.c.const_email(), emailLabel);
        form.addOptional(ECoMessageUtils.c.communitySettings_form_profileWebPage(), profileWebPageHTML);
        form.addOptional(ECoMessageUtils.c.communitySettings_form_registerWidget(), swapRegistrationWidget);
        //form.addOptional(ECoMessageUtils.c.communitySettings_form_registerLink(), swapRegistrationLink);

        FlowPanel panel = new FlowPanel();
        panel.add(FormPanelUtils.getOneLinedReadMode(this, ECoMessageUtils.c.communitySettings_title(), UIMessageUtils.c.const_edit()));
        panel.add(form);
        return panel;
    }

    private Label readLabel() {
        Label label = new Label();
        label.setWidth(READ_FORM_WIDTH);
        return label;
    }

    private String widgetSource() {
        try {
            ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
            String prefix = CorporateUtils.corporationPrefix();
            StringBuilder url = new StringBuilder(EventURLFactory.overseerServlet("/support/registration/bundles.do"));
            url.append("&api_key=");
            url.append(cce.getEvent().getUuid());
            url.append("&prefix=");
            url.append(prefix);
            url.append("&locale=");
            url.append(UIMessageUtils.c.locale());
            return "<iframe src=\"" + url.toString() + "\"></iframe>";
        } catch (NotInCacheException ex) {
            return UIMessageUtils.m.const_notInCache(CurrentEventCacheAction.CURRENT_EVENT);
        }
    }

    private String linkSource() {
        try {
            ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);

            StringBuilder sb = new StringBuilder("<a href=\"");
            sb.append(URLFactory.eventUrl(cce.getEvent().getUuid()));
            sb.append("&page=registration");
            sb.append("\">");
            sb.append("<img src=\"http://static.takeplace.eu/takeplace-register.jpg\" alt=\"Takeplace register\" title=\"Takeplace register\" style=\"border:none\"/>");
            sb.append("</a>");
            return sb.toString();
        } catch (NotInCacheException ex) {
            return UIMessageUtils.m.const_notInCache(CurrentEventCacheAction.CURRENT_EVENT);
        }
    }

    @Override
    public Widget updateForm(String id) {
        Label titleLabel = StyleUtils.getStyledLabel(ECoMessageUtils.c.communitySettings_title(), FONT_16PX);
        titleLabel.addStyleName(FONT_BOLD);
        titleLabel.addStyleName(MARGIN_BOT_20);

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), FLOAT_RIGHT + " " + MARGIN_TOP_3);
        cancel.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                clearMessageBox();
                setWidget(readForm());
            }
        });

        ActionImage save = ActionImage.getCheckButton(UIMessageUtils.c.const_saveUpper());
        save.addStyleName(FLOAT_LEFT);
        save.addClickHandler(confirmUpdateClickHandler(null));

        descriptionAreaHTML = new ConsysTextArea(0, 0, READ_FORM_WIDTH, "150px", false, "");
        descriptionAreaHTML.setText(clientEvent.getDescription());

        description = new ConsysRichTextArea(READ_FORM_WIDTH_NOPX, "150");
        description.setHTML(clientEvent.getDescription());

        ConsysRichTextToolbar toolbar = new ConsysRichTextToolbar(description);

        final SimplePanel descriptionWrapper = new SimplePanel();

        inEditor = true;

        ActionLabel toHTML = new ActionLabel(ECoMessageUtils.c.communitySettings_action_toHTML());
        ActionLabel toEditor = new ActionLabel(ECoMessageUtils.c.communitySettings_action_toEditor());

        final ConsysFlowPanel descriptionPanel = new ConsysFlowPanel();
        descriptionPanel.add(toHTML);
        descriptionPanel.add(toolbar);
        descriptionPanel.add(description);

        final ConsysFlowPanel descriptionPanelHTML = new ConsysFlowPanel();
        descriptionPanelHTML.add(toEditor);
        descriptionPanelHTML.add(descriptionAreaHTML);

        toEditor.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                inEditor = true;
                description.setHTML(descriptionAreaHTML.getText());
                descriptionWrapper.setWidget(descriptionPanel);
            }
        });

        toHTML.setClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                inEditor = false;
                descriptionAreaHTML.setText(description.getHTML());
                descriptionWrapper.setWidget(descriptionPanelHTML);
            }
        });

        descriptionWrapper.setWidget(descriptionPanel);

        tags = new ConsysStringTextBox(READ_FORM_WIDTH, 0, 255, ECoMessageUtils.c.communitySettings_form_tags());
        tags.setText(clientEvent.getTags());

        webBox = new ConsysStringTextBox(READ_FORM_WIDTH, 0, 255, UIMessageUtils.c.const_website());
        webBox.setText(clientEvent.getWeb());

        phoneNumberBox = new ConsysStringTextBox(READ_FORM_WIDTH, 0, 255, UIMessageUtils.c.const_phoneNumber());
        phoneNumberBox.setText((String) Cache.get().get(CommonProperties.EVENT_WEB_PHONE));

        emailBox = new ConsysStringTextBox(READ_FORM_WIDTH, 0, 255, UIMessageUtils.c.const_email());
        emailBox.setText((String) Cache.get().get(CommonProperties.EVENT_WEB_EMAIL));

        Label separator = new Label("");
        separator.setSize("100px", "10px");

        Form form = new Form();
        form.addOptional(UIMessageUtils.c.const_description(), descriptionWrapper);
        form.addContent(separator);
        form.addOptional(ECoMessageUtils.c.communitySettings_form_tags(), tags);
        form.addOptional(UIMessageUtils.c.const_website(), webBox);
        form.addOptional(UIMessageUtils.c.const_phoneNumber(), phoneNumberBox);
        form.addOptional(UIMessageUtils.c.const_email(), emailBox);
        form.addActionMembers(save, cancel, READ_FORM_WIDTH);

        FlowPanel panel = new FlowPanel();
        panel.add(titleLabel);
        panel.add(form);
        return panel;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                UpdateEventPropertyAction action = new UpdateEventPropertyAction();
                action.getProperties().put(CommonProperties.EVENT_WEB_PHONE, phoneNumberBox.getText());
                action.getProperties().put(CommonProperties.EVENT_WEB_EMAIL, emailBox.getText());
                EventBus.get().fireEvent(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovany EventActionExecutorem
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        Cache.get().register(CommonProperties.EVENT_WEB_PHONE, phoneNumberBox.getText());
                        Cache.get().register(CommonProperties.EVENT_WEB_EMAIL, emailBox.getText());
                        updateLeftAtributes().run();
                    }
                }, CommunitySettings.this));
            }
        };
    }

    private ConsysAction updateLeftAtributes() {
        return new ConsysAction() {
            @Override
            public void run() {
                if (!inEditor) {
                    description.setHTML(descriptionAreaHTML.getText());
                }

                EventBus.get().fireEvent(new DispatchEvent(
                        new UpdateEventCommunitySettingsAction(tags.getText(), description.getHTML(), webBox.getText()),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovany ActionExecutorem
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                onSuccessUpdate(ECoMessageUtils.c.communitySettings_text_updateSuccess());
                            }
                        }, CommunitySettings.this));
            }
        };
    }
}
