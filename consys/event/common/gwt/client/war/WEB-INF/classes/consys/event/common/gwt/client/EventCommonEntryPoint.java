package consys.event.common.gwt.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.module.Module;
import consys.common.gwt.client.rpc.action.*;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.client.rpc.result.*;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.list.ListDataSourceRequest;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.client.ui.debug.DebugModuleEntryPoint;
import consys.common.gwt.client.ui.payment.PaymentProfilePanel;
import consys.common.gwt.client.utils.TimeZoneProvider;
import consys.common.gwt.shared.bo.*;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.action.*;
import consys.event.common.gwt.client.bo.*;
import consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem;
import consys.event.common.gwt.client.bo.list.VideoRecordState;
import consys.event.common.gwt.client.comp.Preparing;
import consys.event.common.gwt.client.module.EventCoreModule;
import consys.event.common.gwt.client.module.OverseerModule;
import consys.event.common.gwt.client.module.event.EventWall;
import consys.event.common.gwt.client.module.event.committees.EventCommittees;
import consys.event.common.gwt.client.module.event.settings.EventSettings;
import consys.event.common.gwt.client.module.event.video.VideoFilesSettings;
import consys.event.common.gwt.client.module.event.video.Videos;
import consys.event.common.gwt.client.module.payment.PaymentDialog;
import consys.event.common.gwt.client.module.payment.PaymentPayPalButton;
import consys.event.common.gwt.client.module.payment.PaymentPayPalButtonType;
import consys.event.common.gwt.client.module.payment.PaymentTransfers;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pepa
 */
public class EventCommonEntryPoint extends DebugModuleEntryPoint {

    @Override
    public void initMocks() {
        Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, TimeZoneProvider.getTimeZoneByID("Europe/London"));
        Cache.get().register(CommonProperties.EVENT_VIDEO_UPLOAD, "true");

        DateTimeFormat format = DateTimeFormat.getFormat("d.M.y");
        ClientUser u = new ClientUser();
        u.setFirstName("Karlík");
        u.setMiddleName("s");
        u.setLastName("Čokoládou");
        Cache.get().register(UserCacheAction.USER, u);

        SelectBoxData locationList = new SelectBoxData();
        locationList.getList().add(new BoxItem("Chile", 34));
        locationList.getList().add(new BoxItem("Czech republic", 53));
        locationList.getList().add(new BoxItem("USA", 231));
        MockFactory.addActionMock(ListLocationAction.class, new ActionMock<SelectBoxData>(locationList));

        // ------
        SelectBoxData stateList = new SelectBoxData();
        stateList.getList().add(new BoxItem("DA", 34));
        stateList.getList().add(new BoxItem("CA", 44));
        stateList.getList().add(new BoxItem("LA", 54));
        MockFactory.addActionMock(ListUsStateAction.class, new ActionMock<SelectBoxData>(stateList));

        // -------
        ClientCurrentEvent cu = new ClientCurrentEvent();
        cu.setJustVisitor(true);
        ClientEvent ce = new ClientEvent();
        ce.setAcronym("MEMICS");
        ce.setFullName("Annual Doctoral Workshop on Mathematical and Engineering Methods in Computer Science");
        ce.setSeries("X.");
        ce.setShortName("MEMICS 2011");
        ce.setType(ClientEventType.CONFERENCE);
        ce.setUniversalName("Annual Doctoral Workshop on Mathematical and Engineering Methods in Computer Science");
        ce.setUuid("XX");
        ce.setWeb("http://www.memics.cz/2010/");
        ce.setYear(2011);
        ClientDateTime cdt1 = new ClientDateTime();
        cdt1.setDate(new ClientDate(26, 9, 2011));
        cdt1.setTime(new ClientTime(1, 3));
        ce.setFrom(cdt1); // 26.9.2011 01:03 gmt
        ClientDateTime cdt2 = new ClientDateTime();
        cdt2.setDate(new ClientDate(3, 10, 2011));
        cdt2.setTime(new ClientTime(15, 49));
        ce.setTo(cdt2); // 3.10.2011 15:49 gmt
        ce.setCity("Brno");
        ce.setLocation(53);
        ce.setVisible(true);
        ce.setTags("a, b c f, e t t");
        ce.setDescription("<a href=\"#\">kuk</a>ahoj<p>odstavec</p><ul><li>odrážka</li></ul>");
        cu.setEvent(ce);
        cu.setOverseerUrl("overseer1");
        Cache.get().register(CurrentEventCacheAction.CURRENT_EVENT, cu);

        // ------
        MockFactory.addActionMock(UpdateBaseEventSettingsAction.class, new ActionMock<VoidResult>(new VoidResult()));
        MockFactory.addActionMock(UpdateDetailedEventSettingsAction.class, new ActionMock<VoidResult>(new VoidResult()));

        // ------
        MockFactory.addActionMock(UpdateEventCommunitySettingsAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // ------
        ArrayListResult<ClientCommitteeThumb> cthumbs = new ArrayListResult<ClientCommitteeThumb>();
        ClientCommitteeThumb cct = new ClientCommitteeThumb();
        cct.setId(1L);
        cct.setName("Organization Committee");
        cthumbs.getArrayListResult().add(cct);
        cct = new ClientCommitteeThumb();
        cct.setId(1L);
        cct.setName("Programme Committee");
        cthumbs.getArrayListResult().add(cct);
        MockFactory.addActionMock(LoadCommitteesAction.class, new ActionMock<ArrayListResult<ClientCommitteeThumb>>(cthumbs));

        // -------
        ClientCommittee cc = new ClientCommittee();
        cc.setName("Organization Committee");
        ArrayList<String> rights = new ArrayList<String>();
        rights.add("10");
        rights.add("11");
        ArrayList<String> memebrs = new ArrayList<String>();
        memebrs.add("1");
        memebrs.add("1");

        EventUser eu1 = new EventUser();
        eu1.setFullName("Pavol Grešša");
        eu1.setLastName("Grešša");
        eu1.setUuid("213");
        EventUser eu2 = new EventUser();
        eu2.setFullName("Jaroslav Škrabálek");
        eu2.setLastName("Škrabálek");
        eu2.setUuid("2132");
        ArrayList<EventUser> ememebrs = new ArrayList<EventUser>();
        ememebrs.add(eu1);
        ememebrs.add(eu2);

        cc.setRights(rights);
        cc.setMemebers(ememebrs);

        cc.setChair(eu2);

        MockFactory.addActionMock(LoadCommitteeAction.class, new ActionMock<ClientCommittee>(cc));

        // -------
        ArrayListResult<ClientUserWithAffiliationThumb> listThumbs = new ArrayListResult<ClientUserWithAffiliationThumb>();
        for (int i = 0; i < 20; i++) {
            ClientUserWithAffiliationThumb cult = new ClientUserWithAffiliationThumb();
            cult.setName("Ivan Krasko");
            cult.setPosition("Dictator of Democratic republic of Kenya");
            listThumbs.getArrayListResult().add(cult);
        }
        MockFactory.addActionMock(ListUserAffiliationThumbsAction.class, new ActionMock<ArrayListResult<ClientUserWithAffiliationThumb>>(listThumbs));
        // -------
        ArrayListResult<ClientUserThumb> userThumbsList = new ArrayListResult<ClientUserThumb>();
        for (int i = 0; i < 7; i++) {
            ClientUserThumb cult = new ClientUserThumb();
            cult.setName("Ivan Krasko");
            cult.setUuid("1");
            userThumbsList.getArrayListResult().add(cult);
        }
        MockFactory.addActionMock(ListUserThumbsAction.class, new ActionMock<ArrayListResult<ClientUserThumb>>(userThumbsList));
        // -------
        ClientUserThumb ut = new ClientUserThumb();
        ut.setUuid("uuid");
        ut.setName("Kofoloun Smečný");
        ut.setUuidPortraitImagePrefix("iuuid");
        MockFactory.addActionMock(LoadUserThumbAction.class, new ActionMock<ClientUserThumb>(ut));
        // -------
        MockFactory.addActionMock(UpdateCommitteeAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // -------
        ClientEventLicense license = new ClientEventLicense();
        license.setActivatedFrom(new Date());
        license.setActivatedTo(new Date());
        //license.setLicensePurchased(new Date());
        MockFactory.addActionMock(LoadLicenseInfoAction.class, new ActionMock<ClientEventLicense>(license));
        // -------

        // -------
        MockFactory.addActionMock(CreateCommitteeAction.class, new ActionMock<LongResult>(new LongResult(5)));
        ClientPaymentSettings settings = new ClientPaymentSettings();

        ArrayList<ClientPaymentProfileThumb> payThumbs = new ArrayList<ClientPaymentProfileThumb>();
        payThumbs.add(new ClientPaymentProfileThumb("uu1", "Profil 1"));
        payThumbs.add(new ClientPaymentProfileThumb("uu2", "Profil 2"));
        //settings.setProfileThumbs(payThumbs);

        ClientEventPaymentProfile profile = new ClientEventPaymentProfile();
        profile.setAccountNo("HR12 1001 0051 8630 0016 0");
        profile.setBankNo("bankno");
        profile.setCity("Brno");
        //profile.setCountry(53);
        profile.setName("Nazev");
        profile.setRegNo("reg 123");
        profile.setStreet("Veveří 123");
        profile.setVatNo("324 vat");
        profile.setZip("612 00");
        profile.setInvoiceNote("Poznámka na fakturu, která obsahuje trochu delší text ať je vidět jestli se zobrazuje správně zalomeně.");
        //settings.setFirstProfile(new ClientPaymentProfile());
        settings.setFirstProfile(profile);

        MockFactory.addActionMock(LoadEventPaymentSettingsAction.class, new ActionMock<ClientPaymentSettings>(settings));
        // --------------
        MockFactory.addActionMock(UpdateEventPaymentProfileAction.class, new ActionMock<VoidResult>(new VoidResult()));
        // --------------
        ClientPaymentProfileDialog cpd = new ClientPaymentProfileDialog();
        cpd.setProfileThumbs(payThumbs);
        cpd.setFirstProfile(profile);
        MockFactory.addActionMock(ListUserPaymentProfileDialogAction.class, new ActionMock<ClientPaymentProfileDialog>(cpd));
        // --------------
        ClientPaymentOrderData cpdd = new ClientPaymentOrderData();
        cpdd.getProducts().add(new ClientPaymentProduct("Registrace 123", new Monetary(500), 1));
        cpdd.setTotalPrice(new Monetary(500));
        MockFactory.addActionMock(ListPaymentDataDialogAction.class, new ActionMock<ClientPaymentOrderData>(cpdd));
        // --------------
        MockFactory.addActionMock(LoadUserPaymentProfileAction.class, new ActionMock<ClientUserPaymentProfile>(profile));
        // --------------
        ClientEventConfirmed confirmed = new ClientEventConfirmed();
        confirmed.setFrom(new Date());
        confirmed.setTo(new Date());
        MockFactory.addActionMock(BuyLicencesAction.class, new ActionMock<ClientEventConfirmed>(confirmed));
        // --------------
        ArrayList<ClientPaymentTransferHead> items = new ArrayList<ClientPaymentTransferHead>();
        ClientPaymentTransferHead a1 = new ClientPaymentTransferHead();
        a1.setUuid("item123");
        a1.setTransferDate(new Date());
        a1.setTotal(new Monetary(1000));
        a1.setInfo("5678277346433/0500");
        a1.setThirdPartyFees(new Monetary(30));
        a1.setFees(new Monetary(45));
        a1.setState(ClientPaymentTransferState.PREPARED);
        ClientPaymentTransferHead a2 = new ClientPaymentTransferHead();
        a2.setUuid("item654");
        a2.setTransferDate(new Date());
        a2.setTotal(new Monetary(500));
        a2.setInfo("2342525235352/3333");
        a2.setThirdPartyFees(new Monetary(20));
        a2.setFees(new Monetary(25));
        a2.setState(ClientPaymentTransferState.ACTIVE);
        items.add(a1);
        items.add(a2);
        ListDataSourceResult ldsr = new ListDataSourceResult(items, 2);
        //MockFactory.addActionMock(ListDataSourceRequest.class, new ActionMock<ListDataSourceResult>(ldsr));
        // --------------
        ClientPaymentTransferDetail detail = new ClientPaymentTransferDetail();
        detail.setHeader(new ClientPaymentTransferHead());
        detail.getHeader().setTotal(new Monetary(500));
        detail.getHeader().setThirdPartyFees(new Monetary(5));
        detail.getHeader().setFees(new Monetary(495));
        detail.getHeader().setInfo("2342523523535");
        detail.getHeader().setTransferDate(new Date());
        detail.getHeader().setUuid("12");
        detail.getHeader().setState(ClientPaymentTransferState.ACTIVE);
        ClientPaymentTransferItem cp1 = new ClientPaymentTransferItem();
        cp1.setFees(new Monetary(10000));
        cp1.setThirdPartyFees(new Monetary(5000));
        cp1.setTotal(new Monetary(130000));
        cp1.setInfo("Bla bla bla bla bla bla bla bla bla");
        cp1.setInvoicePaidDate(new Date());
        cp1.setUuid("1244");
        detail.getItems().add(cp1);
        ClientPaymentTransferItem cp2 = new ClientPaymentTransferItem();
        cp2.setFees(new Monetary(10000));
        cp2.setThirdPartyFees(new Monetary(5000));
        cp2.setTotal(new Monetary(130000));
        cp2.setInfo("Bla bla bla bla bla bla bla bla bla");
        cp2.setInvoicePaidDate(new Date());
        cp2.setUuid("1244");
        detail.getItems().add(cp2);
        MockFactory.addActionMock(LoadEventPaymentTransferDetailAction.class, new ActionMock<ClientPaymentTransferDetail>(detail));
        // --------------
        ListDataSourceResult ldsr1 = new ListDataSourceResult(items, 2);
        MockFactory.addActionMock(ListTransfersAction.class, new ActionMock<ListDataSourceResult>(ldsr1));
        // --------------
        MockFactory.addActionMock(LoadLicensePaymentDataAction.class, new ActionMock<ClientPaymentOrderData>(cpdd));
        // --------------
        ClientPaymentProfileDialog cppd = new ClientPaymentProfileDialog();
        cppd.setFirstProfile(profile);
        MockFactory.addActionMock(ListEventPaymentProfileDialogAction.class, new ActionMock<ClientPaymentProfileDialog>(cppd));
        // --------------
        MockFactory.addActionMock(UpdateEventPropertyAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        // --------------
        MockFactory.addActionMock(LoadEventDetailsAction.class, new ActionMock<ClientEvent>(ce));
        // --------------
        ClientEventImportantDates importantDates = new ClientEventImportantDates();
        //importantDates.setRegistrationFrom(new Date());
        MockFactory.addActionMock(LoadEventImportantDatesAction.class, new ActionMock<ClientEventImportantDates>(importantDates));
        // --------------
        MockFactory.addActionMock(CloneEventAction.class, new ActionMock<StringResult>(new StringResult("21313123")));
        // --------------
        ClientVideoFilesListItem vr1 = new ClientVideoFilesListItem();
        //vr1.setCustomName("Uživatelský název");
        vr1.setDescription("Popisek videa");
        vr1.setState(VideoRecordState.UPLOADING);
        vr1.setSystemName("41414AF123123-14124ADAFEAEFE1");
        ClientVideoFilesListItem vr2 = new ClientVideoFilesListItem();
        vr2.setCustomName("Petr Mach: Jak vyrovnat státní rozpočet bez zvyšování daní aneb co se s tím dá dělat");
        vr2.setDescription("Popisek videa");
        vr2.setState(VideoRecordState.PROCESSING);
        vr2.setSystemName("41414AF123123-14124ADAFEAEFE2");
        ClientVideoFilesListItem vr3 = new ClientVideoFilesListItem();
        vr3.setCustomName("Uživatelský název zpracováno");
        vr3.setDescription("Popisek videa");
        vr3.setAuthors("Marcel Chroustal");
        vr3.setSpeakers("Marcel Chroustal, Lojzin Hujo");
        vr3.setState(VideoRecordState.PROCESSED);
        vr3.setSystemName("41414AF123123-14124ADAFEAEFE3");
        ClientVideoFilesListItem vr4 = new ClientVideoFilesListItem();
        vr4.setCustomName("Uživatelský název s chybou");
        vr4.setDescription("Popisek videa");
        vr4.setState(VideoRecordState.PROCESS_ERROR);
        vr4.setSystemName("41414AF123123-14124ADAFEAEFE4");

        ArrayList<ClientVideoFilesListItem> items2 = new ArrayList<ClientVideoFilesListItem>();
        items2.add(vr1);
        items2.add(vr2);
        items2.add(vr3);
        items2.add(vr4);

        ListDataSourceResult ldsr2 = new ListDataSourceResult(items2, 4);

        MockFactory.addActionMock(ListDataSourceRequest.class, new ActionMock<ListDataSourceResult>(ldsr2));
        // --------------
        MockFactory.addActionMock(UpdateEventVideoAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        // --------------
        MockFactory.addActionMock(DeleteEventVideoAction.class, new ActionMock<VoidResult>(VoidResult.RESULT()));
        // --------------
        MockFactory.addActionMock(LoadUuidAction.class, new ActionMock<StringResult>(new StringResult("1231231")));
    }

    @Override
    public void registerForms(Map<String, Widget> formMap) {
        FlowPanel fp = new FlowPanel() {
            @Override
            protected void onLoad() {
                PaymentDialog pd = new PaymentDialog("123", true, false, false) {
                    @Override
                    public void doAction(PaymentProfilePanel profilePanel, String userUuid, ConsysMessage fail) {
                        GWT.log("validation check: " + profilePanel.doValidate(fail));
                    }

                    @Override
                    public ListPaymentDataDialogAction getDataAction() {
                        ListPaymentDataDialogAction d = new ListPaymentDataDialogAction() {
                        };
                        d.addProduct("produkt1", null, 1);
                        return d;
                    }
                };
                pd.showCentered();
            }
        };

        PaymentPayPalButton paypal = new PaymentPayPalButton("4312W", "123", "234", PaymentPayPalButtonType.PAYPAL);
        paypal.setCurrencyCode("CZK");
        paypal.setItemName("žluťoučký koníček řediteloval jen píď");
        paypal.setItemNumber("2342342313512GGA");
        paypal.setAmount("500.05");

        Cache.get().register(CommonProperties.EVENT_CURRENCY, "CZK");
        Cache.get().register(CommonProperties.EVENT_VAT_RATE, "20");
        Cache.get().register(CommonProperties.EVENT_PARTNER_FOOTER_IMAGE, "");
        Cache.get().register(CommonProperties.EVENT_PARTNER_PANEL_IMAGE, "");
        Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, TimeZoneProvider.getTimeZoneByID("Europe/London"));

        formMap.put("Button", paypal);
        formMap.put("Video settings", new VideoFilesSettings());
        formMap.put("Videos", new Videos());
        formMap.put("Preparing", new Preparing("Preparing event data"));
        formMap.put("Event Settings", new EventSettings());
        formMap.put("Event Wall", new EventWall());
        formMap.put("Committees", new EventCommittees());
        formMap.put("Payment transfers", new PaymentTransfers());
        formMap.put("Dialog plateb", fp);
    }

    @Override
    public void registerModules(List<Module> modules) {
        modules.add(new OverseerModule());
        modules.add(new EventCoreModule());
    }
}
