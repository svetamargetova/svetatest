package consys.event.common.gwt.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.action.EventAction;



/**
 * This is an asynchronous equivalent of the {@link Dispatch} interface on the
 * server side. The reason it exists is because GWT currently can't correctly
 * handle having generic method templates in method signatures (eg.
 * <code>&lt;A&gt; A
 * create( Class<A> type )</code>)
 * 
 * @author David Peterson
 */
public interface EventDispatchAsync {
    <A extends EventAction<R>, R extends Result> void execute( A action, AsyncCallback<R> callback );
}
