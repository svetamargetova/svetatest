package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.event.common.gwt.client.bo.ClientCommitteeThumb;
import consys.common.gwt.shared.action.EventAction;

/**
 * Nacita thumby pre vybory
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadCommitteesAction extends EventAction<ArrayListResult<ClientCommitteeThumb>> {

    private static final long serialVersionUID = -4047457288111094922L;
}
