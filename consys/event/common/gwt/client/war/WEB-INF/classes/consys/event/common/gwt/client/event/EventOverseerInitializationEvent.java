package consys.event.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 * Inicializuje DispatchServiceAsynv pro napojeni na Overseera
 * @author Palo
 */
public class EventOverseerInitializationEvent extends GwtEvent<EventOverseerInitializationEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    
    private String eventUuid;
    private String userUuid;
    private String overseerUrl;

    public EventOverseerInitializationEvent(String eventUuid, String userUuid, String overseerUrl) {
        this.eventUuid = eventUuid;
        this.userUuid = userUuid;
        this.overseerUrl = overseerUrl;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public String getOverseerUrl() {
        return overseerUrl;
    }

    public String getUserUuid() {
        return userUuid;
    }                
    

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.initializeOverseer(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void initializeOverseer(EventOverseerInitializationEvent event);
    }
}
