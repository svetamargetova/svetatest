package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.common.gwt.client.bo.ClientPaymentSettings;

/**
 * Nacteni nastaveni placeni
 * @author pepa
 */
public class LoadEventPaymentSettingsAction extends EventAction<ClientPaymentSettings> {

    private static final long serialVersionUID = 6822914191959669035L;
}
