package consys.event.common.gwt.client.module.event;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ServletConstants;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.messaging.EventWallUI;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.HistoryUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.constants.img.EventLogoImageEnum;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.event.UriFragmentTokenEvent;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.utils.EventUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.right.EventCommonModuleRoleModel;
import consys.event.common.gwt.client.action.CurrentEventUserRightAction;
import consys.event.common.gwt.client.action.LoadEventDetailsAction;
import consys.event.common.gwt.client.action.LoadEventImportantDatesAction;
import consys.event.common.gwt.client.bo.ClientEventImportantDates;
import consys.event.common.gwt.client.event.CheckUserIsRegistered;
import consys.event.common.gwt.client.event.EventRegistrationRequestEvent;
import consys.event.common.gwt.client.event.EventSendSubmissionRequest;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Wall eventu
 * @author pepa
 */
public class EventWall extends SimpleFormPanel {

    // logger
    private Logger log = LoggerFactory.getLogger(EventWall.class);
    // konstanty
    public static final String PARAM_NEW_USER = "newUser";
    public static final String PARAM_BUNDLE_UUID = "bundleUuid";
    public static final String PARAM_EVENT_PAGE = "page";
    // komponenty
    private SimplePanel logoWrapper;
    private SimplePanel messagesContent;
    private ActionImage register;
    private ActionImage sendSubmission;
    private HTML googleMapHTML;
    // data
    private boolean firstCallHideButtonsOnLoad = true;

    public EventWall() {
        logoWrapper = new SimplePanel();
        logoWrapper.addStyleName(FLOAT_LEFT);
        logoWrapper.addStyleName(MARGIN_RIGHT_20);

        register = ActionImage.getPersonButton(ECoMessageUtils.c.eventWall_button_register(), true);
        register.addStyleName(ECoResourceUtils.bundle().css().eventWallRegister());
        register.setVisible(false);
        register.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new EventRegistrationRequestEvent());
            }
        });

        sendSubmission = ActionImage.getConfirmButton(ECoMessageUtils.c.eventWall_button_sendContribution(), true);
        sendSubmission.addStyleName(ECoResourceUtils.bundle().css().eventWallRegister());
        sendSubmission.addStyleName(MARGIN_LEFT_20);
        sendSubmission.setVisible(false);
        sendSubmission.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new EventSendSubmissionRequest());
            }
        });

        FlowPanel titlePanel = new FlowPanel();
        titlePanel.add(logoWrapper);
        titlePanel.add(register);
        titlePanel.add(sendSubmission);
        titlePanel.add(StyleUtils.clearDiv());

        googleMapHTML = new HTML();
        googleMapHTML.addStyleName(MARGIN_TOP_10);

        addWidget(titlePanel);
        addWidget(Separator.addedStyle(MARGIN_VER_10));
        addWidget(eventProfile());
        addWidget(googleMapHTML);

        messagesContent = new SimplePanel();
        addWidget(messagesContent);
    }

    @Override
    protected void onLoad() {
        if (processParameters()) {
            return;
        }
        try {
            ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
            String logoUuid = cce.getEvent().getLogoUuidPrefix();
            if (logoUuid != null) {
                String url = GWT.getModuleBaseURL() + ServletConstants.EVENT_LOGO_IMG_LOAD + EventLogoImageEnum.WALL.getFullUuid(logoUuid);
                logoWrapper.setWidget(new Image(url));
            } else {
                logoWrapper.setWidget(new Image(ResourceUtils.system().confLogoBig()));
            }
        } catch (NotInCacheException ex) {
            // TODO: co delat?
        }

        if (!firstCallHideButtonsOnLoad) {
            // kazde dalsi zobrazeni event wall, nacteme aktualni stav, aby se spravne zobrazovala tlacitka
            EventBus.fire(new EventDispatchEvent(new LoadEventImportantDatesAction(),
                    new AsyncCallback<ClientEventImportantDates>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                        }

                        @Override
                        public void onSuccess(ClientEventImportantDates result) {
                            showHideButtons(result);
                        }
                    },
                    EventWall.this));
        } else {
            // pri prvnim nacteni nepotrebujeme data pro skryti tlacitek protoze uz je mame z event profilu
            firstCallHideButtonsOnLoad = false;
        }

        final boolean[] rights = new boolean[3];
        EventBus.fire(insertMessageEvent(rights));
    }

    /** akce na zjisteni jestli ma uzivatel pravo vkladat zpravy na wall */
    private EventDispatchEvent insertMessageEvent(final boolean[] rights) {
        return new EventDispatchEvent(
                new CurrentEventUserRightAction(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT),
                new AsyncCallback<BooleanResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        insertWall(rights, "RIGHT_COMMON_WALL_MESSAGE_INSERT");
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        rights[0] = result.isBool();
                        EventBus.fire(insertMessageAsEventEvent(rights));
                    }
                }, null);
    }

    /** akce na zjisteni jestli ma uzivatel pravo vkladat zpravy na wall jako event */
    private EventDispatchEvent insertMessageAsEventEvent(final boolean[] rights) {
        return new EventDispatchEvent(
                new CurrentEventUserRightAction(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT),
                new AsyncCallback<BooleanResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        insertWall(rights, "RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT");
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        rights[1] = result.isBool();
                        EventBus.fire(removeMessage(rights));
                    }
                }, null);
    }

    /** akce na zjisteni jestli ma uzivatel pravo mazat z wallu */
    private EventDispatchEvent removeMessage(final boolean[] rights) {
        return new EventDispatchEvent(
                new CurrentEventUserRightAction(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_REMOVE),
                new AsyncCallback<BooleanResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        insertWall(rights, "RIGHT_COMMON_WALL_MESSAGE_REMOVE");
                    }

                    @Override
                    public void onSuccess(BooleanResult result) {
                        rights[2] = result.isBool();
                        insertWall(rights, null);
                    }
                }, null);
    }

    /** vlozi wall */
    private void insertWall(boolean[] wallRights, String errorMessage) {
        messagesContent.setWidget(new EventWallUI(wallRights[0], wallRights[1], wallRights[2], errorMessage));
    }

    /** zkontroluje parametry jestli se nejedna o vstup uzivatele s registraci z venku, pokud ano vraci true */
    private boolean processParameters() {
        HashMap<String, String> parameters = HistoryUtils.getParameters();
        String newUserValue = parameters.get(PARAM_NEW_USER);
        if (newUserValue != null) {
            EventRegistrationRequestEvent requestEvent;
            if (newUserValue.equals("yes")) {
                requestEvent = new EventRegistrationRequestEvent();
            } else {
                String bundleUuid = parameters.get(PARAM_BUNDLE_UUID);
                if (bundleUuid != null) {
                    requestEvent = new EventRegistrationRequestEvent(bundleUuid);
                } else {
                    getFailMessage().setText(UIMessageUtils.m.const_missingParameter(PARAM_BUNDLE_UUID));
                    return true;
                }
            }
            EventBus.get().fireEvent(requestEvent);
            return true;
        }


        String page = parameters.get(PARAM_EVENT_PAGE);
        if (page != null) {
            LoggerFactory.log(EventWall.class, "Processing event subpage: " + page);
            EventBus.get().fireEvent(new UriFragmentTokenEvent(page));
        }
        return false;
    }

    /** zobrazi nebo skryje tlacitka v zahlavi */
    private void showHideButtons(ClientEventImportantDates dates) {
        Date now = new Date();

        // zobrazime tlacitko registrace?
        boolean registerNotNull = dates.getRegistrationFrom() != null && dates.getRegistrationTo() != null;
        boolean inRange = false;
        if (registerNotNull) {
            inRange = now.after(dates.getRegistrationFrom()) && now.before(dates.getRegistrationTo());
        }
        register.setVisible(false);
        EventBus.fire(new CheckUserIsRegistered(inRange, register));

        // zobrazime tlacitko pro zaslani prispevku?
        boolean sendVisible = false;
        boolean sendNotNull = dates.getContributionFrom() != null && dates.getContributionTo() != null;
        if (sendNotNull) {
            sendVisible = now.after(dates.getContributionFrom()) && now.before(dates.getContributionTo());
        }
        sendSubmission.setVisible(sendVisible);
    }

    private HTML eventProfile() {
        final HTML html = new HTML();
        html.setStyleName(ECoResourceUtils.bundle().css().eventProfile());

        EventBus.fire(new DispatchEvent(new LoadEventDetailsAction(),
                new AsyncCallback<ClientEvent>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                    }

                    @Override
                    public void onSuccess(final ClientEvent clientEvent) {
                        EventBus.fire(new EventDispatchEvent(new LoadEventImportantDatesAction(),
                                new AsyncCallback<ClientEventImportantDates>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        // obecne chyby zpracovava action executor
                                    }

                                    @Override
                                    public void onSuccess(ClientEventImportantDates result) {
                                        showHideButtons(result);
                                        generateEventProfile(html, clientEvent, result);
                                        googleMap(clientEvent);
                                    }
                                },
                                EventWall.this));
                    }
                }, EventWall.this));

        return html;
    }

    private void generateEventProfile(HTML html, ClientEvent data, ClientEventImportantDates dates) {
        StringBuilder source = new StringBuilder();
        source.append("<div class=\"");
        source.append(ECoResourceUtils.bundle().css().eventDescription());
        source.append("\">");
        source.append(data.getDescription() == null ? "" : data.getDescription());
        source.append("</div>");

        importantDates(source, data, dates);

        html.setHTML(source.toString());
    }

    private void importantDates(StringBuilder source, ClientEvent data, ClientEventImportantDates dates) {
        boolean showImportantDates = false;
        ArrayList<DateRecord> list = new ArrayList<DateRecord>();

        if (data.getFrom() != null) {
            list.add(new DateRecord(data.getFrom(), data.getTo(), data.getFullName()));
            showImportantDates = true;
        }
        if (dates.getRegistrationFrom() != null) {
            list.add(new DateRecord(dates.getRegistrationFrom(), dates.getRegistrationTo(),
                    ECoMessageUtils.c.eventWall_text_participantsRegistration()));
            showImportantDates = true;
            log.debug("Registration date: " + dates.getRegistrationFrom());
        }
        if (dates.getContributionFrom() != null) {
            list.add(new DateRecord(dates.getContributionFrom(), dates.getContributionTo(),
                    ECoMessageUtils.c.eventWall_text_collectingContributions()));
            showImportantDates = true;
            log.debug("Contribution date: " + dates.getContributionFrom());
        }
        if (!showImportantDates) {
            // neni nastaveny zadny z datumu, nemame co zobrazovat
            return;
        }

        if (!list.isEmpty()) {
            source.append("<h2>");
            source.append(ECoMessageUtils.c.eventWall_text_importantDates());
            source.append("</h2>");
            source.append("<div>");

            for (DateRecord dr : list) {
                divStart(source, "date1");
                source.append(dr.getFrom() == null ? "n/a" : DateTimeUtils.getDateWithoutYearWithTime(dr.getFrom()));
                if (!dr.isInSameDay() && dr.getTo() != null) {
                    source.append(" - ");
                    source.append(DateTimeUtils.getDateWithoutYearWithTime(dr.getTo()));
                } else if (dr.isInSameDay() && dr.getTo() != null) {
                    source.append(" - ");
                    source.append(DateTimeUtils.getHourAndMinutes(dr.getTo()));
                }
                source.append("</div>");
                divStart(source, "date2");
                source.append(dr.getText());
                source.append("</div>");
                source.append(StyleUtils.clearDiv("3px"));
            }

            source.append("</div>");
        }
    }

    private void googleMap(final ClientEvent event) {
        if (event.getLocation() == 0 || StringUtils.isBlank(event.getCity())) {
            return;
        }
        EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(SelectBoxData result) {
                StringBuilder address = new StringBuilder();
                if (!StringUtils.isEmpty(event.getStreet())) {
                    address.append(event.getStreet());
                    address.append(", ");
                }
                if (!StringUtils.isEmpty(event.getCity())) {
                    address.append(event.getCity());
                    address.append(", ");
                }

                for (BoxItem item : result.getList()) {
                    if (item.getId().equals(event.getLocation())) {
                        address.append(item.getName());
                    }
                }
                String a = address.toString();
                if (a.endsWith(", ")) {
                    a = a.substring(0, a.length() - 2);
                }

                googleMapHTML.setHTML(EventUtils.googleMap(URL.encode(a), "en"));
            }
        }, null));
    }

    private void divStart(StringBuilder sb, String style) {
        sb.append("<div class=\"");
        sb.append(style);
        sb.append("\">");
    }
}
