package consys.event.common.gwt.client.module;

import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.event.EventNavigationModelUpdate;
import consys.common.gwt.client.event.EventSettingsLoadedEvent;
import consys.common.gwt.client.event.EventSettingsReceivedEvent;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.module.ModuleMenuRegistrator;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.event.EventModule;
import consys.common.gwt.client.module.event.EventNavigationItem;
import consys.common.gwt.client.module.event.EventNavigationItemVisiblityResolver;
import consys.common.gwt.client.module.event.EventNavigationModel;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventUserRightsCacheAction;
import consys.common.gwt.client.ui.event.BreadcrumbContentEvent;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.common.gwt.client.ui.event.EventPartnerImageChanged;
import consys.common.gwt.client.ui.event.PanelItemEvent;
import consys.common.gwt.client.ui.layout.LayoutBottomMenu;
import consys.common.gwt.client.ui.layout.LayoutPanel;
import consys.common.gwt.client.ui.layout.panel.PanelItemAction;
import consys.common.gwt.client.ui.layout.panel.PanelItemType;
import consys.common.gwt.client.ui.layout.panel.banner.BannerImagePanelItem;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.ArrayUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.client.utils.TimeZoneProvider;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.EventSettings;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.api.right.EventCommonModuleRoleModel;
import consys.event.common.api.right.Role;
import consys.event.common.gwt.client.action.*;
import consys.event.common.gwt.client.cache.handler.*;
import consys.event.common.gwt.client.event.OverseerInitializedEvent;
import consys.event.common.gwt.client.module.event.EventWall;
import consys.event.common.gwt.client.module.event.committees.EventCommittees;
import consys.event.common.gwt.client.module.event.video.VideoFilesSettings;
import consys.event.common.gwt.client.module.event.video.Videos;
import consys.event.common.gwt.client.module.payment.PaymentTransfers;
import consys.event.common.gwt.client.rpc.EventActionExecutor;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import java.util.List;
import java.util.Map;

/**
 * Modul pro prokliknuti do eventu na strane overseera
 *
 * @author pepa
 */
public class OverseerModule implements EventModule,
        OverseerInitializedEvent.Handler,
        EventNavigationModelUpdate.Handler,
        EventSettingsReceivedEvent.Handler,
        EventPartnerImageChanged.Handler {

    private static final Logger logger = LoggerFactory.getLogger(OverseerModule.class);
    // konstanty
    public static final String NAVIGATION_PARENT_ADMINISTRATION = ECoMessageUtils.c.overseerModule_text_administration();
    // data
    private Timer newEventUuidRemoverTimer;
    private BannerImagePanelItem bannerImagePanelItem;

    public OverseerModule() {
        newEventUuidRemoverTimer = new Timer() {
            @Override
            public void run() {
            }
        };
    }

    // -------
    /**
     * EVENT MODULE
     */
    @Override
    public void onInitialize(OverseerInitializedEvent event) {
        logger.info("Event initialization");
        LoadEventSettingsAction action = new LoadEventSettingsAction();
        EventDispatchEvent settings = new EventDispatchEvent(action, new AsyncCallback<EventSettings>() {
            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava EventActionExecutor
                // TODO: waiting false
            }

            @Override
            public void onSuccess(EventSettings result) {
                // TODO: waiting false
                // TODO: provedeni akci
                EventBus.get().fireEvent(new EventSettingsLoadedEvent(result));
            }
        }, null);
        // TODO: waiting true
        EventBus.get().fireEvent(settings);
    }

    /**
     * Pri incializacii modulu sa : - inicializuje ActionExecutor - ktory
     * zachytava vstup do eventu a tym nastavi spravnu adresu na RPC -
     * CacheHandler - LoadCurrentEventSettingsAction ktory autoamticky uklada do
     * cache - CacheHandler - CurrentEventUserRightAction - ktory autoamticky
     * vracia nacachovane prava uzivatela
     */
    @Override
    public void initializeModule(ModuleMenuRegistrator menuRegistrator) {
        // Inicializacia action executora
        EventActionExecutor.init();
        // Registracia CacheHandleru
        CacheRegistrator.register(LoadEventDetailsAction.class, new LoadCurrentEventCacheHandler());
        CacheRegistrator.register(CurrentEventUserRightAction.class, new CurrentEventUserRightCacheHandler());
        CacheRegistrator.register(UpdateBaseEventSettingsAction.class, new UpdateBaseSettingsCacheHandler());
        CacheRegistrator.register(UpdateDetailedEventSettingsAction.class, new UpdateDetailedSettingsCacheHandler());
        CacheRegistrator.register(UpdateEventCommunitySettingsAction.class, new UpdateEventCommunitySettingsCacheHandler());
        CacheRegistrator.register(UpdateEventLogoAction.class, new UpdateEventLogoCacheHandler());
    }

    @Override
    public void registerModule() {
        EventBus.get().addHandler(EventNavigationModelUpdate.TYPE, this);
        EventBus.get().addHandler(OverseerInitializedEvent.TYPE, this);
        EventBus.get().addHandler(EventSettingsReceivedEvent.TYPE, this);
        EventBus.get().addHandler(EventPartnerImageChanged.TYPE, this);
    }

    @Override
    public void unregisterModule() {
        EventBus.get().removeHandler(EventNavigationModelUpdate.TYPE, this);
        EventBus.get().removeHandler(OverseerInitializedEvent.TYPE, this);
        EventBus.get().removeHandler(EventSettingsReceivedEvent.TYPE, this);
        EventBus.get().removeHandler(EventPartnerImageChanged.TYPE, this);
    }

    @Override
    public void registerEventNavigation(EventNavigationModel model) {
        // Mal by byt zrejme co najnizsie
        EventNavigationItem administration = new EventNavigationItem(NAVIGATION_PARENT_ADMINISTRATION, 1, new ConsysAction() {
            @Override
            public void run() {
                String title = ECoMessageUtils.c.eventSettings_title();
                FormUtils.fireNextRootBreadcrumb(title, new consys.event.common.gwt.client.module.event.settings.EventSettings());
            }
        }, true, EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS);

        model.insertFirstLevelAddon(administration, UIMessageUtils.c.const_settings(), 10, new ConsysAction() {
            @Override
            public void run() {
                String title = ECoMessageUtils.c.eventSettings_title();
                FormUtils.fireNextRootBreadcrumb(title, new consys.event.common.gwt.client.module.event.settings.EventSettings());
            }
        }, EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS);
        // Rozsah 10 aby sme mali rezervuy pridania dalsich nastaveni
        // USER SETTINGS [40]
        /*
         * model.insertFirstLevelAddon(administration,
         * ECoMessageUtils.c.userSettings(), 40, new ConsysAction() {
         *
         * @Override public void run() { BreadcrumbContentEvent e = new
         * BreadcrumbContentEvent(ECoMessageUtils.c.userSettings(),
         * BreadcrumbContentEvent.LEVEL.NEXT_ROOT, new ChangeContentEvent(new
         * UnknownTokenContent())); EventBus.get().fireEvent(e); } },
         * ArrayUtils.EMPTY_STRING_ARRAY);
         */
        // EVENT COMMITTEES [20]
        model.insertFirstLevelAddon(administration, ECoMessageUtils.c.eventCommittees_title(), 20, new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextRootBreadcrumb(ECoMessageUtils.c.eventCommittees_title(), new EventCommittees());
            }
        }, EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR, EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS);
        // MODULES [30]
        /*
         * model.insertFirstLevelAddon(administration,
         * ECoMessageUtils.c.moduleActivation(), 30, new ConsysAction() {
         *
         * @Override public void run() { BreadcrumbContentEvent e = new
         * BreadcrumbContentEvent(ECoMessageUtils.c.moduleActivation(),
         * BreadcrumbContentEvent.LEVEL.NEXT_ROOT, new ChangeContentEvent(new
         * UnknownTokenContent())); EventBus.get().fireEvent(e); } },
         * EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS);
         */
        model.insertFirstLevelAddon(administration, ECoMessageUtils.c.overseerModule_text_payments(), 40, new ConsysAction() {
            @Override
            public void run() {
                FormUtils.fireNextRootBreadcrumb(ECoMessageUtils.c.overseerModule_text_payments(), new PaymentTransfers());
            }
        }, EventCommonModuleRoleModel.RIGHT_COMMON_TRANSFERS);

        EventNavigationItem video = new EventNavigationItem(ECoMessageUtils.c.overseerModule_text_video(), 100, null, true, new EventNavigationItemVisiblityResolver() {
            @Override
            public boolean isItemVisibile(List<String> rights, Map<String, String> systemProperties) {
                String value = systemProperties.get(CommonProperties.EVENT_VIDEO_UPLOAD);
                return value.equalsIgnoreCase("true");
            }
        });

        model.insertFirstLevelAddon(video, UIMessageUtils.c.const_settings(), 10, new ConsysAction() {
            @Override
            public void run() {
                String title = ECoMessageUtils.c.eventSettings_title();
                FormUtils.fireNextRootBreadcrumb(title, new VideoFilesSettings());
            }
        }, EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS, EventCommonModuleRoleModel.RIGHT_VIDEO_SETTINGS);

        model.insertFirstLevelAddon(video, ECoMessageUtils.c.videos_title(), 10, new ConsysAction() {
            @Override
            public void run() {
                String title = ECoMessageUtils.c.videos_title();
                FormUtils.fireNextRootBreadcrumb(title, new Videos());
            }
        }, ArrayUtils.EMPTY_STRING_ARRAY);
    }

    /**
     * EventNavigationModelUpdate HANDLER
     */
    @Override
    public void onModelUpdate(final EventNavigationModelUpdate event) {
        if (event.isResetNavigationModel()) {
            Window.setTitle(event.getTitle());
            final BreadcrumbContentEvent breadcrumbContentEvent = new BreadcrumbContentEvent(event.getTitle(),
                    BreadcrumbContentEvent.LEVEL.ROOT,
                    new ChangeContentEvent(new EventWall()));
            EventBus.get().fireEvent(breadcrumbContentEvent);

            // prepnuti obsahu do detailniho nastaveni pokud se jedna o prave vytvoreny event
            if (Cache.get().isRegistered(Cache.NEW_EVENT_CREATED)) {
                try {
                    String uuid = (String) Cache.get().getSafe(Cache.NEW_EVENT_CREATED);
                    ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);

                    newEventUuidRemoverTimer.cancel();
                    newEventUuidRemoverTimer = new Timer() {
                        @Override
                        public void run() {
                            Cache.get().unregister(Cache.NEW_EVENT_CREATED);
                        }
                    };
                    newEventUuidRemoverTimer.schedule(2000);

                    if (uuid.equals(cce.getEvent().getUuid())) {
                        String title = ECoMessageUtils.c.eventSettings_title();
                        FormUtils.fireNextRootBreadcrumb(title, new consys.event.common.gwt.client.module.event.settings.EventSettings(true));
                    }
                } catch (NotInCacheException ex) {
                    // neni v cache, coz je divne
                    logger.error("uuid (created event) or current event not in cache");
                }
            }

            LayoutPanel.get().setAddonPanelTitleAction(new ConsysAction() {
                @Override
                public void run() {
                    EventBus.get().fireEvent(breadcrumbContentEvent);
                }
            });
        }
    }

    /**
     * EventUserRightsEvent HANDLER
     */
    @Override
    public void onReceivedSettingsLoaded(List<String> rights, Map<String, String> properties, boolean resetNavigationModel) {
        Cache.get().register(CurrentEventUserRightsCacheAction.CURRENT_USER_EVENT_RIGHTS, rights);

        if (resetNavigationModel) {
            // ukazeme bannere
            LayoutBottomMenu.get().showEventFooterImage(properties.get(CommonProperties.EVENT_PARTNER_FOOTER_IMAGE));

            String panelBannerUuid = properties.get(CommonProperties.EVENT_PARTNER_PANEL_IMAGE);
            if (StringUtils.isNotBlank(panelBannerUuid)) {
                showBanner(panelBannerUuid);
            }

            // prenastavime timezonu
            String eventTimeZone = properties.get(CommonProperties.EVENT_TIMEZONE);
            TimeZone tz = TimeZoneProvider.getTimeZoneByID(eventTimeZone);
            Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, tz);
        } else {
            logger.debug("Just current navigation model reset - keeping same Timezone and Banners");
        }
    }

    @Override
    public void onChange(EventPartnerImageChanged event) {
        if (event.getType().equals("PANEL")) {
            if (bannerImagePanelItem == null) {
                showBanner(event.getUuid());
            } else {
                bannerImagePanelItem.setUuid(event.getUuid());
            }
        }
    }

    private void showBanner(String imgUuid) {
        bannerImagePanelItem = new BannerImagePanelItem(imgUuid);
        EventBus.get().fireEvent(new PanelItemEvent(bannerImagePanelItem, PanelItemAction.ADD_BANNER, PanelItemType.BANNER));
    }

    @Override
    public String getLocalizedModuleName() {
        return ECoMessageUtils.c.overseerModule_text_overseerModuleName();
    }

    @Override
    public void registerModuleRoles(List<ModuleRole> moduleRoles) {
        EventCommonModuleRoleModel roles = new EventCommonModuleRoleModel();
        Map<String, Role> rightMap = roles.getModelInMap();

        // EventCommonModuleRoleModel.ROLE_ADMINISTRATOR
        ModuleRole administrator = new ModuleRole();
        administrator.setId(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS).getIdentifier());
        administrator.setSystem(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS).isSystem());
        administrator.setShortcut(EventCommonModuleRoleModel.RIGHT_COMMON_SETTINGS);
        administrator.setLocalizedName(ECoMessageUtils.c.overseerModule_text_roleAdministratorName());
        administrator.setLocalizedDescription(ECoMessageUtils.c.overseerModule_text_roleAdministratorDescription());
        moduleRoles.add(administrator);

        // RIGHT_COMMON_WALL_MESSAGE_INSERT
        ModuleRole insertMessage = new ModuleRole();
        insertMessage.setId(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT).getIdentifier());
        insertMessage.setSystem(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT).isSystem());
        insertMessage.setShortcut(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT);
        insertMessage.setLocalizedName(ECoMessageUtils.c.overseerModule_text_moduleRightInsertWallMessage());
        insertMessage.setLocalizedDescription(ECoMessageUtils.c.overseerModule_text_moduleRightInsertWallMessageDescription());
        moduleRoles.add(insertMessage);

        // RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT
        ModuleRole insertMessageAE = new ModuleRole();
        insertMessageAE.setId(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT).getIdentifier());
        insertMessageAE.setSystem(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT).isSystem());
        insertMessageAE.setShortcut(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT);
        insertMessageAE.setLocalizedName(ECoMessageUtils.c.overseerModule_text_moduleRightInsertWallMessageAsEvent());
        insertMessageAE.setLocalizedDescription(ECoMessageUtils.c.overseerModule_text_moduleRightInsertWallMessageAsEventDescription());
        moduleRoles.add(insertMessageAE);

        // RIGHT_COMMON_WALL_MESSAGE_REMOVE
        ModuleRole removeMessage = new ModuleRole();
        removeMessage.setId(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_REMOVE).getIdentifier());
        removeMessage.setSystem(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_REMOVE).isSystem());
        removeMessage.setShortcut(EventCommonModuleRoleModel.RIGHT_COMMON_WALL_MESSAGE_REMOVE);
        removeMessage.setLocalizedName(ECoMessageUtils.c.overseerModule_text_moduleRightRemoveWallMessage());
        removeMessage.setLocalizedDescription(ECoMessageUtils.c.overseerModule_text_moduleRightRemoveWallMessageDescription());
        moduleRoles.add(removeMessage);

        // ROLE_COMMITTEE_CHAIR
        ModuleRole committeeChair = new ModuleRole();
        committeeChair.setId(rightMap.get(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR).getIdentifier());
        committeeChair.setSystem(rightMap.get(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR).isSystem());
        committeeChair.setShortcut(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR);
        committeeChair.setLocalizedName(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR);
        committeeChair.setLocalizedDescription(EventCommonModuleRoleModel.ROLE_COMMON_COMMITTEE_CHAIR);
        moduleRoles.add(committeeChair);

        // ROLE_COMMITTEE_TRANSFERS
        ModuleRole transfers = new ModuleRole();
        transfers.setId(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_TRANSFERS).getIdentifier());
        transfers.setSystem(rightMap.get(EventCommonModuleRoleModel.RIGHT_COMMON_TRANSFERS).isSystem());
        transfers.setShortcut(EventCommonModuleRoleModel.RIGHT_COMMON_TRANSFERS);
        transfers.setLocalizedName(ECoMessageUtils.c.overseerModule_text_moduleRightTransfers());
        transfers.setLocalizedDescription(ECoMessageUtils.c.overseerModule_text_moduleRightTransfersDescription());
        moduleRoles.add(transfers);
    }
}
