package consys.event.common.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.action.ActionExecutor;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.cache.CacheTimeoutException;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.event.common.gwt.client.action.UpdateEventCommunitySettingsAction;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateEventCommunitySettingsCacheHandler implements CacheHandler<UpdateEventCommunitySettingsAction> {

    @Override
    public void doAction(final UpdateEventCommunitySettingsAction action, final AsyncCallback callback, final ActionExecutionDelegate delegate) {

        if (Cache.get().isRegistered(CurrentEventCacheAction.CURRENT_EVENT)) {
            Cache.get().doCacheAction(new CurrentEventCacheAction() {

                @Override
                public void doAction(final ClientCurrentEvent cce) {
                    action.setEventUuid(cce.getEvent().getUuid());
                    ActionExecutor.execute(action, new AsyncCallback<VoidResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            callback.onFailure(caught);
                        }

                        @Override
                        public void onSuccess(VoidResult result) {
                            ClientEvent ce = cce.getEvent();
                            ce.setTags(action.getTags());
                            ce.setDescription(action.getDescription());
                            ce.setWeb(action.getWeb());
                            callback.onSuccess(result);
                        }
                    }, delegate);
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(CURRENT_EVENT));
                }
            });
        } else {            
            callback.onFailure(new CacheTimeoutException(CurrentEventCacheAction.CURRENT_EVENT));
        }
    }
}
