package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.ClientEventPaymentProfile;
import consys.common.gwt.shared.bo.ClientPaymentProfileThumb;
import java.util.ArrayList;

/**
 * Nastaveni placeni
 *
 * POZOR: UUID faktury za event == event_uuid. Takze ak
 *
 * @author pepa
 */
public class ClientPaymentSettings implements Result {

    private static final long serialVersionUID = 7584033725736221657L;
    // data
    private ArrayList<ClientPaymentProfileThumb> profileThumbs;
    private ClientEventPaymentProfile firstProfile;

    public ClientPaymentSettings() {
        profileThumbs = new ArrayList<ClientPaymentProfileThumb>();
    }

    public ClientEventPaymentProfile getFirstProfile() {
        return firstProfile;
    }

    public void setFirstProfile(ClientEventPaymentProfile firstProfile) {
        this.firstProfile = firstProfile;
    }

    public ArrayList<ClientPaymentProfileThumb> getProfileThumbs() {
        return profileThumbs;
    }

    public void setProfileThumbs(ArrayList<ClientPaymentProfileThumb> profileThumbs) {
        this.profileThumbs = profileThumbs;
    }
}
