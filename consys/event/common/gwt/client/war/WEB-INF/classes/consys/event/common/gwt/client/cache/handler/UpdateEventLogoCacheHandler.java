package consys.event.common.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.cache.CacheTimeoutException;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.event.common.gwt.client.action.UpdateEventLogoAction;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateEventLogoCacheHandler implements CacheHandler<UpdateEventLogoAction> {

    @Override
    public void doAction(final UpdateEventLogoAction action, final AsyncCallback callback, final ActionExecutionDelegate delegate) {

        if (Cache.get().isRegistered(CurrentEventCacheAction.CURRENT_EVENT)) {
            Cache.get().doCacheAction(new CurrentEventCacheAction() {

                @Override
                public void doAction(final ClientCurrentEvent cce) {
                    ClientEvent ce = cce.getEvent();
                    if(action.isProfile()){
                        ce.setProfileLogoUuidPrefix(action.getLogoPregix());
                    }else{
                        ce.setLogoUuidPrefix(action.getLogoPregix());
                    }
                    callback.onSuccess(new VoidResult());
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(CURRENT_EVENT));
                }
            });
        } else {            
            callback.onFailure(new CacheTimeoutException(CurrentEventCacheAction.CURRENT_EVENT));
        }
    }
}
