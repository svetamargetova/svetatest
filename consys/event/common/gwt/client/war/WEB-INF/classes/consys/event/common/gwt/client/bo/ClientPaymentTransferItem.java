package consys.event.common.gwt.client.bo;

import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;
import consys.common.gwt.shared.bo.Monetary;
import java.util.Date;

/**
 * Polozka detailu transferu. Jedna sa o konkrentu jednu fakturu
 * @author pepa
 */
public class ClientPaymentTransferItem extends DataListUuidItem {
    private static final long serialVersionUID = -7815677566025870778L;
            
    private Date invoicePaidDate;
    /** Info k fakture */
    private String info;
    /** Sucet vseckych faktur */
    private Monetary total;
    /** Poplatky nad ramec provizie - pay_pal fees a pod */
    private Monetary thirdPartyFees;
    /** Provizia ktoru si bereme */
    private Monetary fees;

    /**
     * @return the invoicePaidDate
     */
    public Date getInvoicePaidDate() {
        return invoicePaidDate;
    }

    /**
     * @param invoicePaidDate the invoicePaidDate to set
     */
    public void setInvoicePaidDate(Date invoicePaidDate) {
        this.invoicePaidDate = invoicePaidDate;
    }

    /**
     * Info k fakture
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * Info k fakture
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * Sucet vseckych faktur
     * @return the total
     */
    public Monetary getTotal() {
        return total;
    }

    /**
     * Sucet vseckych faktur
     * @param total the total to set
     */
    public void setTotal(Monetary total) {
        this.total = total;
    }

    /**
     * Poplatky nad ramec provizie - pay_pal fees a pod
     * @return the fees
     */
    public Monetary getThirdPartyFees() {
        return thirdPartyFees;
    }

    /**
     * Poplatky nad ramec provizie - pay_pal fees a pod
     * @param fees the fees to set
     */
    public void setThirdPartyFees(Monetary fees) {
        this.thirdPartyFees = fees;
    }

    /**
     * Provizia ktoru si bereme
     * @return the commission
     */
    public Monetary getFees() {
        return fees;
    }

    /**
     * Provizia ktoru si bereme
     * @param commission the commission to set
     */
    public void setFees(Monetary commission) {
        this.fees = commission;
    }
    

}
