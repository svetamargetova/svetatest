package consys.event.common.gwt.client.action;


import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akcia ktora stiahne z eventu role uzivatela
 * @author Palo
 */
public class LoadUserEventRolesAction extends EventAction<ArrayListResult<String>>{
    private static final long serialVersionUID = -4025340046216761221L;

    public LoadUserEventRolesAction() {
    }
       
}
