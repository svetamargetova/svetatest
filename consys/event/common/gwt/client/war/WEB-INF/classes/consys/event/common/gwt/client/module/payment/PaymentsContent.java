package consys.event.common.gwt.client.module.payment;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.event.common.gwt.client.action.LoadEventPaymentSettingsAction;
import consys.event.common.gwt.client.bo.ClientPaymentSettings;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * Polozka menu pro nastaveni
 * @author pepa
 */
public class PaymentsContent extends SimpleFormPanel implements MenuFormItem {

    @Override
    protected void onLoad() {
        EventBus.get().fireEvent(new EventDispatchEvent(new LoadEventPaymentSettingsAction(),
                new AsyncCallback<ClientPaymentSettings>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executor
                    }

                    @Override
                    public void onSuccess(ClientPaymentSettings result) {
                        showContent(result);
                    }
                }, this));
    }

    /** vykresli obsah */
    private void showContent(ClientPaymentSettings data) {
        clear();
        addWidget(new PaymentsProfileContent(data));
        addWidget(Separator.addedStyle(MARGIN_VER_10));
        addWidget(new PaymentsPropertyPanel());
    }

    @Override
    public String getName() {
        return ECoMessageUtils.c.paymentsContent_title();
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.setWidget(this);
    }
}
