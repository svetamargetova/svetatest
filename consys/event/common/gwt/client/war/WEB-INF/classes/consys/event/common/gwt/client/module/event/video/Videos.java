package consys.event.common.gwt.client.module.event.video;

import com.google.gwt.user.client.ui.FlowPanel;
import consys.common.gwt.client.ui.comp.list.DataListCommonPanel;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.event.common.api.list.VideoFilesUserList;
import consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * Prehled videi pro uzivatele
 * @author pepa
 */
public class Videos extends DataListPanel implements VideoFilesUserList {

    public Videos() {
        super(ECoMessageUtils.c.videos_title(), LIST_TAG);

        setListDelegate(new UserVideoFilesListDelegate());
        setDefaultFilter(new ListFilter(Filter_ALL) {
            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });

        DataListCommonPanel cp = new DataListCommonPanel();
        cp.addOrderer(ECoMessageUtils.c.videoFilesSettings_text_videoName(), Order_NAME);
        cp.addFilter(ECoMessageUtils.c.videoFilesSettings_text_all(), Filter_ALL, true);
        addHeadPanel(cp);
    }

    private class UserVideoFilesListDelegate implements ListDelegate<ClientVideoFilesListItem, UserVideoFilesListItem> {

        @Override
        public UserVideoFilesListItem createCell(ClientVideoFilesListItem item) {
            return new UserVideoFilesListItem(item);
        }
    }

    private class UserVideoFilesListItem extends ListItem<ClientVideoFilesListItem> {

        public UserVideoFilesListItem(ClientVideoFilesListItem item) {
            super(item);
        }

        @Override
        protected void createCell(ClientVideoFilesListItem item, FlowPanel panel) {
            VideoFilesListItem.createShowItem(panel, item);
        }
    }
}
