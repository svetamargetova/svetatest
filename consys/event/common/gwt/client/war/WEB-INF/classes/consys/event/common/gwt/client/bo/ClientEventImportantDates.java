package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class ClientEventImportantDates implements Result {

    private static final long serialVersionUID = -9058490078761381587L;
    // data
    private Date registrationFrom;
    private Date registrationTo;
    private Date contributionFrom;
    private Date contributionTo;

    public Date getContributionFrom() {
        return contributionFrom;
    }

    public void setContributionFrom(Date contributionFrom) {
        this.contributionFrom = contributionFrom;
    }

    public Date getContributionTo() {
        return contributionTo;
    }

    public void setContributionTo(Date contributionTo) {
        this.contributionTo = contributionTo;
    }

    public Date getRegistrationFrom() {
        return registrationFrom;
    }

    public void setRegistrationFrom(Date registrationFrom) {
        this.registrationFrom = registrationFrom;
    }

    public Date getRegistrationTo() {
        return registrationTo;
    }

    public void setRegistrationTo(Date registrationTo) {
        this.registrationTo = registrationTo;
    }
}
