package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.ArrayList;

/**
 * Detail transferu obsahuje hlavicku a zoznam faktur ktore su v tomto transfere
 * zahrnute
 * @author palo
 */
public class ClientPaymentTransferDetail implements Result {

    private static final long serialVersionUID = 2251570577481280109L;
    // data
    private ClientPaymentTransferHead header;
    private ArrayList<ClientPaymentTransferItem> items;

    public ClientPaymentTransferDetail() {
    }



    public ArrayList<ClientPaymentTransferItem> getItems() {
        if(items == null){
            items = new ArrayList<ClientPaymentTransferItem>();
        }
        return items;
    }

    /**
     * @return the header
     */
    public ClientPaymentTransferHead getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(ClientPaymentTransferHead header) {
        this.header = header;
    }
   
}
