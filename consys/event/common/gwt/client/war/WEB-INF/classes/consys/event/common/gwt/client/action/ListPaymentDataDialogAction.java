package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientPaymentOrderData;
import consys.common.gwt.shared.bo.ProductThumb;
import java.util.ArrayList;

/**
 * Akce pro nacteni dat do platebniho dialogu.
 * @author pepa
 *
 * Implementacia zalezi na konkretnom charaktere objednavanych produktov. 
 * 
 */
public abstract class ListPaymentDataDialogAction extends EventAction<ClientPaymentOrderData> {

    private static final long serialVersionUID = -291808169669907L;
    // data
    private String customerProfileUuid;
    private ArrayList<ProductThumb> products;

    public ListPaymentDataDialogAction() {
        products = new ArrayList<ProductThumb>();
    }

    /** prida jedne produkt */
    public void addProduct(String productUuid, String productDiscountCode, int quantity) {
        products.add(new ProductThumb(productUuid,
                productDiscountCode == null ? "" : productDiscountCode,
                quantity < 1 ? 1 : quantity));
    }

    /** prida seznam produktu */
    public void addProducts(ArrayList<ProductThumb> bundles) {
        products.addAll(bundles);
    }

    /** vraci seznam s pridanymi produkty */
    public ArrayList<ProductThumb> getProducts() {
        return products;
    }

    /**
     * @return the customerProfileUuid
     */
    public String getCustomerProfileUuid() {
        return customerProfileUuid;
    }

    /**
     * @param customerProfileUuid the customerProfileUuid to set
     */
    public void setCustomerProfileUuid(String customerProfileUuid) {
        this.customerProfileUuid = customerProfileUuid;
    }
}
