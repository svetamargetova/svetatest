package consys.event.common.gwt.client.action;

import consys.event.common.gwt.client.bo.ClientEventConfirmed;

/**
 * Akce pro zakoupeni licenci, vraci datum splatnosti
 * @author pepa
 */
public class BuyLicencesAction extends BuyProductAction<ClientEventConfirmed> {

    private static final long serialVersionUID = 3493160628308823665L;

    public BuyLicencesAction() {
    }

    public BuyLicencesAction(String userUuid, String profileUuid) {
        super(userUuid, profileUuid, null);
    }
}
