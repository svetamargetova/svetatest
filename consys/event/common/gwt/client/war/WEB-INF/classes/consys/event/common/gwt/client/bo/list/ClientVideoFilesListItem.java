package consys.event.common.gwt.client.bo.list;

import consys.common.gwt.client.ui.comp.list.item.DataListUuidItem;

/**
 *
 * @author pepa
 */
public class ClientVideoFilesListItem extends DataListUuidItem {

    private static final long serialVersionUID = 1280117764828249480L;
    // data
    private String systemName;
    private String customName;
    private String description;
    private VideoRecordState state;
    private String authors;
    private String speakers;
    private boolean processing;
    private String url;

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public VideoRecordState getState() {
        return state;
    }

    public void setState(VideoRecordState state) {
        this.state = state;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getSpeakers() {
        return speakers;
    }

    public void setSpeakers(String speakers) {
        this.speakers = speakers;
    }

    public boolean isProcessing() {
        return processing;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
