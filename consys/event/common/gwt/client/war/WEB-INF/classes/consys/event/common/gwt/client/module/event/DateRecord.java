package consys.event.common.gwt.client.module.event;

import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.shared.bo.ClientDateTime;
import java.util.Date;

/**
 *
 * @author pepa
 */
public class DateRecord {

    private ClientDateTime from;
    private ClientDateTime to;
    private String text;

    public DateRecord(Date from, Date to, String text) {
        this.to = DateTimeUtils.toClientDateTime(to);
        this.from = DateTimeUtils.toClientDateTime(from);
        this.text = text;
    }

    public DateRecord(ClientDateTime from, ClientDateTime to, String text) {
        this.from = from;
        this.to = to;
        this.text = text;
    }

    public ClientDateTime getFrom() {
        return from;
    }

    public void setFrom(ClientDateTime from) {
        this.from = from;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ClientDateTime getTo() {
        return to;
    }

    public void setTo(ClientDateTime to) {
        this.to = to;
    }

    /** pokud se jedna o stejny den, vraci true */
    public boolean isInSameDay() {
        if (getFrom() == null || getTo() == null) {
            return false;
        }
        ClientDateTime f = getFrom();
        ClientDateTime t = getTo();
        return (f.getDate().getDay() == t.getDate().getDay() && f.getDate().getMonth() == t.getDate().getMonth()
                && f.getDate().getYear() == t.getDate().getYear());
    }
}
