package consys.event.common.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.cache.CacheTimeoutException;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.event.common.gwt.client.action.LoadEventDetailsAction;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadCurrentEventCacheHandler implements CacheHandler<LoadEventDetailsAction> {

    @Override
    public void doAction(LoadEventDetailsAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
        if (Cache.get().isRegistered(CurrentEventCacheAction.CURRENT_EVENT)) {
            Cache.get().doCacheAction(new CurrentEventCacheAction() {

                @Override
                public void doAction(ClientCurrentEvent value) {
                    callback.onSuccess(value.getEvent());
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(CURRENT_EVENT));
                }
            });
        } else {            
            callback.onFailure(new CacheTimeoutException(CurrentEventCacheAction.CURRENT_EVENT));
        }
    }
}
