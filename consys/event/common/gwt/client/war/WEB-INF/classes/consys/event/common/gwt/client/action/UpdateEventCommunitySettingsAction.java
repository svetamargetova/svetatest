package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.Action;

public class UpdateEventCommunitySettingsAction implements Action<VoidResult>{
    private static final long serialVersionUID = -4025340046216761221L;

    private String eventUuid;
    private String tags;
    private String description;
    private String web;

    public UpdateEventCommunitySettingsAction() {
    }

    public UpdateEventCommunitySettingsAction(String tags, String description, String web) {
        this.tags = tags;
        this.description = description;
        this.web = web;
    }

    public String getWeb() {
        return web;
    }

    public String getDescription() {
        return description;
    }

    public String getTags() {
        return tags;
    }

    /**
     * @return the eventUuid
     */
    public String getEventUuid() {
        return eventUuid;
    }

    /**
     * @param eventUuid the eventUuid to set
     */
    public void setEventUuid(String eventUuid) {
        this.eventUuid = eventUuid;
    }

   
    
    

}
