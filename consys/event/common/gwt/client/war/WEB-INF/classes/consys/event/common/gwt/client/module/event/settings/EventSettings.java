package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.panel.MenuFormPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.common.gwt.client.module.payment.PaymentsContent;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * Nastavnie eventu obsahuje dve zalozky.
 * General Settings - nastavnenie eventu do adminstracie
 * Event Properties - nastavenie systemovych properties
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventSettings extends MenuFormPanel {

    public EventSettings() {
        this(false);
    }

    public EventSettings(boolean newEvent) {
        super(ECoMessageUtils.c.eventSettings_title());

        /*ActionImage propertyButton = ActionImage.getPersonButton(UIMessageUtils.c.const_property(), true);
        propertyButton.addClickHandler(propertyHandler());
        addLeftControll(propertyButton);*/

        if (newEvent) {
            getSuccessMessage().setText(ECoMessageUtils.c.eventSettings_text_eventSuccessfullyCreated(), true);
        }

        addItem(new GeneralSettings());
        addItem(new EventCommunitySettings());
        addItem(new PartnerAndBannerSettings());
        addItem(new PaymentsContent());
        initWidget();
    }

    private ClickHandler propertyHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                CommonPropertyPanel w = new CommonPropertyPanel();
                FormUtils.fireNextBreadcrumb(UIMessageUtils.c.const_property(), w);
            }
        };
    }
}
