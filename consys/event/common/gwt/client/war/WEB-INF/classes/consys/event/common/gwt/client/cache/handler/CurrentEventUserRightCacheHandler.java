package consys.event.common.gwt.client.cache.handler;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.action.ActionExecutionDelegate;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.cache.CacheRegistrator.CacheHandler;
import consys.common.gwt.client.cache.CacheTimeoutException;
import consys.common.gwt.client.rpc.action.cache.CurrentEventUserRightsCacheAction;
import consys.common.gwt.client.rpc.result.BooleanResult;
import consys.event.common.gwt.client.action.CurrentEventUserRightAction;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CurrentEventUserRightCacheHandler implements CacheHandler<CurrentEventUserRightAction> {

    @Override
    public void doAction(final CurrentEventUserRightAction action, final AsyncCallback callback, ActionExecutionDelegate delegate) {
        if (Cache.get().isRegistered(CurrentEventUserRightsCacheAction.CURRENT_USER_EVENT_RIGHTS)) {
            Cache.get().doCacheAction(new CurrentEventUserRightsCacheAction() {

                @Override
                public void doAction(List<String> value) {
                    callback.onSuccess(new BooleanResult(value.contains(action.getRight())));
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(CURRENT_USER_EVENT_RIGHTS));
                }
            });
        } else {
            Cache.get().doCacheAction(new CurrentEventUserRightsCacheAction() {

                @Override
                public void doAction(List<String> value) {
                    callback.onSuccess(new BooleanResult(value.contains(action.getRight())));
                }

                @Override
                public void doTimeoutAction() {
                    callback.onFailure(new CacheTimeoutException(CURRENT_USER_EVENT_RIGHTS));
                }
            }, 15);
        }
    }
}
