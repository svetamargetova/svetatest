package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.InvitedUser;
import java.util.ArrayList;

/**
 * Akce pro vytvoreni committee
 * @author pepa
 */
public class CreateCommitteeAction extends EventAction<LongResult> {

    private static final long serialVersionUID = 104786361316167497L;
    // data
    private String name;
    /** chair */
    private InvitedUser chair;
    /** seznam id prav */
    private ArrayList<String> chairRights;
    /** seznam uuid clenu */
    private ArrayList<InvitedUser> members;
    /** seznam id prav */
    private ArrayList<String> rights;

    public CreateCommitteeAction() {
    }

    public InvitedUser getChair() {
        return chair;
    }

    public void setChair(InvitedUser chair) {
        this.chair = chair;
    }

    public ArrayList<String> getChairRights() {
        return chairRights;
    }

    public void setChairRights(ArrayList<String> chairRights) {
        this.chairRights = chairRights;
    }

    public ArrayList<InvitedUser> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<InvitedUser> members) {
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getRights() {
        return rights;
    }

    public void setRights(ArrayList<String> rights) {
        this.rights = rights;
    }
}
