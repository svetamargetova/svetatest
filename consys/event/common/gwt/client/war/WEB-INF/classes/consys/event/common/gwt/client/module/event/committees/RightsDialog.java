package consys.event.common.gwt.client.module.event.committees;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.RoleManager;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.Dialog;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.ListPanel.ListPanelDelegate;
import consys.common.gwt.client.ui.comp.search.SearchListDelegate;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *  Dialog pre prava
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RightsDialog extends Dialog implements ListPanelDelegate<RightListItem> {

    private static final int WIDTH = 500;
    // Komponenty
    private ActionImage select;
    private RightList rights;
    private Label rightTitleLabel;
    private HTML rightDescLabel;
    // Data
    private SearchListDelegate<RightListItem> delegate;
    private ArrayList<RightListItem> selectedItems;
    private ArrayList<ModuleRole> disableRoles;

    public RightsDialog() {
        super(WIDTH);

        selectedItems = new ArrayList<RightListItem>();
        disableRoles = new ArrayList<ModuleRole>();

        rights = new RightList(15);
        rights.setStaticHeight();
        rights.setWidth("290px");
        rights.addStyleName(ECoResourceUtils.bundle().css().rightsDialogList());
        rights.setSelectable(true);
        rights.setDelegate(this);
    }

    public void setListDelegate(SearchListDelegate<RightListItem> delegate) {
        this.delegate = delegate;
    }

    private void addItem(RightListItem searchItem) {
        rights.addItem(searchItem);
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        List<RoleManager.Module> modules = RoleManager.registredModules();
        for (RoleManager.Module module : modules) {
            for (ModuleRole role : module.getRoles()) {
                if (role.isSystem()) {
                    if (!disableRoles.contains(role)) {
                        addItem(new RightListItem(role, true));
                    } else {
                        RightListItem li = new RightListItem(role, true);
                        li.showDisabled();
                        addItem(li);
                    }
                }
            }
        }
    }

    @Override
    protected Widget createContent() {
        FlowPanel body = new FlowPanel();
        body.setWidth("100%");

        // Right detail
        rightTitleLabel = new Label(ECoMessageUtils.c.rightsDialog_helpTitle());
        rightTitleLabel.setStyleName(ECoResourceUtils.bundle().css().rightsDialogHelpTitle());
        rightDescLabel = new HTML(ECoMessageUtils.c.rightsDialog_helpText());
        rightDescLabel.setStyleName(ECoResourceUtils.bundle().css().rightsDialogHelpDesc());

        FlowPanel helpPanel = new FlowPanel();
        helpPanel.addStyleName(ECoResourceUtils.bundle().css().rightsDialogHelp());
        helpPanel.add(rightTitleLabel);
        helpPanel.add(rightDescLabel);

        // Dialog BODY
        FlowPanel rightsBody = new FlowPanel();
        rightsBody.setStyleName(ECoResourceUtils.bundle().css().rightsDialogBody());
        rightsBody.add(rights);
        rightsBody.add(helpPanel);

        SimplePanel panel = new SimplePanel();
        panel.setSize("100%", "20px");
        body.add(panel);
        body.add(Separator.separator());
        body.add(rightsBody);
        body.add(Separator.separator());

        // Select panel        
        select = ActionImage.getConfirmButton(ECoMessageUtils.c.rightsDialog_button_select());
        select.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (delegate != null) {
                    delegate.didSelect(selectedItems);
                }
                hide();
            }
        });
        select.setVisible(false);
        ActionLabel close = new ActionLabel(UIMessageUtils.c.const_close());

        HTMLPanel actionPanel = new HTMLPanel("<table style='width:100%;' cellspacing='10' cellpadding='0'><tr><td style='height:20px;'><div id='select'></div></td><td><div id='close' style='text-align: right;'></div></td></tr></table>");
        actionPanel.setWidth("100%");
        actionPanel.add(select, "select");
        actionPanel.add(close, "close");
        body.add(actionPanel);

        close.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
            }
        });
        return body;
    }

    @Override
    public void didSelect(RightListItem item) {
        select.setVisible(true);
        if (selectedItems.contains(item)) {
            selectedItems.remove(item);
        } else {
            selectedItems.add(item);
        }
        rightTitleLabel.setText(item.getObject().getLocalizedName());
        rightDescLabel.setHTML(item.getObject().getLocalizedDescription());
    }

    @Override
    public void hide() {
        super.hide();
        selectedItems.clear();
    }

    /** nastavi do dialogu jiz vybrana prava */
    public void setAddedRights(ArrayList<RightListEditItem> editItems) {
        disableRoles.clear();
        for (RightListEditItem i : editItems) {
            disableRoles.add(i.getObject());
        }
    }
}
