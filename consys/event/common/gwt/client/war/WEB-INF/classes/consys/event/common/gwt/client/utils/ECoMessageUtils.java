package consys.event.common.gwt.client.utils;

import com.google.gwt.core.client.GWT;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.message.EventCoreConstants;
import consys.event.common.gwt.client.message.EventCoreMessages;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ECoMessageUtils {

    public static final EventCoreConstants c = (EventCoreConstants) GWT.create(EventCoreConstants.class);
    public static final EventCoreMessages m = (EventCoreMessages) GWT.create(EventCoreMessages.class);

    /** vraci menu pro event ulozenou v cache */
    public static String getCurrency() {
        String result;
        try {
            result = (String) Cache.get().getSafe(CommonProperties.EVENT_CURRENCY);
        } catch (NotInCacheException ex) {
            result = ECoMessageUtils.c.const_error_currencyNotFoundInCache();
        }
        return result;
    }
}
