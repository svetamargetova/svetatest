/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Znovunacita prava a pregenerujeme menu
 * 
 * @author palo
 */
public class ReloadEventMenuEvent extends GwtEvent<ReloadEventMenuEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<ReloadEventMenuEvent.Handler> TYPE = new GwtEvent.Type<ReloadEventMenuEvent.Handler>();

    @Override
    public GwtEvent.Type<ReloadEventMenuEvent.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onMenuReloadRequest(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onMenuReloadRequest(ReloadEventMenuEvent event);
    }
    
}
