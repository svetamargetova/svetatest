package consys.event.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;

/**
 * Event je vystrelen pokud je akce zaplacena a je pri tom upraven jeji platebni profil
 * @author pepa
 */
public class UpdateEventPaymentProfileEvent extends GwtEvent<UpdateEventPaymentProfileEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();
    // data
    private ClientUserPaymentProfile profile;

    public UpdateEventPaymentProfileEvent(ClientUserPaymentProfile profile) {
        this.profile = profile;
    }

    public ClientUserPaymentProfile getProfile() {
        return profile;
    }

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.updatePaymentProfile(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void updatePaymentProfile(UpdateEventPaymentProfileEvent event);
    }
}
