package consys.event.common.gwt.client.module.event.committees;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.ui.comp.panel.ListPanelItem;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.search.SearchItem;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RightListItem extends ListPanelItem<ModuleRole> implements SearchItem<ModuleRole> {

    // konstanty
    public static final int HEIGHT = 20;
    // komponenty
    private ConsysCheckBox check;
    // data
    private ModuleRole role;
    private boolean disabled;
    private boolean selected;
    private boolean showCheck;

    public RightListItem(ModuleRole role, boolean showCheck) {
        this.role = role;
        this.showCheck = showCheck;
        selected = false;
        disabled = false;

        check = new ConsysCheckBox();
        check.addStyleName(StyleUtils.FLOAT_LEFT);
        check.addStyleName(StyleUtils.MARGIN_HOR_5);
        check.setVisible(showCheck);
    }

    @Override
    public void create(FlowPanel panel) {
        Label label = new Label(role.getLocalizedModuleName() + " — " + role.getLocalizedName());
        label.setStyleName(ECoResourceUtils.bundle().css().rightListItem());
        label.addStyleName(StyleUtils.FLOAT_LEFT);
        label.setWidth(showCheck ? "235px" : "320px");

        panel.add(check);
        panel.add(label);
        panel.add(StyleUtils.clearDiv());
    }

    @Override
    public void hover() {
    }

    @Override
    public void unhover() {
    }

    @Override
    public ModuleRole getObject() {
        return role;
    }

    @Override
    public ModuleRole getItem() {
        return role;
    }

    @Override
    public void selected() {
        if (!disabled) {
            if (selected) {
                super.unselected();
                selected = false;
            } else {
                super.selected();
                selected = true;
            }
            check.setValue(selected);
        }
    }

    @Override
    public void unselected() {
        showOddEven();
    }

    public void showDisabled() {
        disabled = true;
        check.setValue(true);
        check.setEnabled(false);
    }
}
