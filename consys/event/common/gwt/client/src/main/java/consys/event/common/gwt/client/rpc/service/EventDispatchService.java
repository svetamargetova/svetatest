package consys.event.common.gwt.client.rpc.service;

import com.google.gwt.user.client.rpc.RemoteService;
import consys.common.gwt.client.rpc.DispatchAsync;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.action.EventAction;

 

/**
 * There currently seem to be GWT compilation problems with services that use
 * generic templates in method parameters. As such, they are stripped here, but
 * added back into the {@link Dispatch} and {@link DispatchAsync}, which are
 * the interfaces that should be accessed by regular code.
 * <p/>
 * Once GWT can compile templatized methods correctly, this should be merged
 * into a single pair of interfaces.
 *
 * @author David Peterson
 */
public interface EventDispatchService extends RemoteService {
    Result execute( EventAction<?> action ) throws ActionException;
}
