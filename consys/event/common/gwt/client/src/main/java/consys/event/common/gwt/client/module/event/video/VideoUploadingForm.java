package consys.event.common.gwt.client.module.event.video;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.action.ConsysActionWithValue;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysFileUpload;
import consys.common.gwt.client.ui.comp.upload.UploadingForm;
import consys.common.gwt.client.ui.utils.ServletUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.gwt.client.action.CreateVideoRecordAction;
import consys.event.common.gwt.client.module.event.video.VideoFilesSettings.AddVideoDialog;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 *
 * @author pepa
 */
public class VideoUploadingForm extends FlowPanel implements UploadingForm {

    // konstanty
    public static final String FULL_PERCENT = "100 %";
    // komponenty
    private FormPanel formPanel;
    private ConsysFileUpload videoUpload;
    // data
    private AddVideoDialog dialog;
    private VideoFilesSettings parent;
    private String statusUrl;
    private Timer timer;
    private String percent;
    private String uuid;
    private String idName;

    public VideoUploadingForm(AddVideoDialog dialog, VideoFilesSettings parent, String uuid) {
        this.dialog = dialog;
        this.parent = parent;
        this.uuid = uuid;

        timer = new Timer() {
            @Override
            public void run() {
                checkUploadStatus();
            }
        };
    }

    @Override
    protected void onLoad() {
        if (formPanel == null) {
            generateContent();
        }
    }

    @Override
    protected void onUnload() {
        timer.cancel();
    }

    @Override
    public void runTimer() {
        timer.scheduleRepeating(4000);
    }

    private void generateContent() {
        formPanel = new FormPanel();
        formPanel.setMethod(FormPanel.METHOD_POST);
        formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);
        formPanel.addStyleName(StyleUtils.MARGIN_LEFT_10);
        formPanel.addSubmitHandler(new FormPanel.SubmitHandler() {
            @Override
            public void onSubmit(FormPanel.SubmitEvent event) {
                dialog.hide();
                Timer t = new Timer() {
                    @Override
                    public void run() {
                        parent.refresh();
                    }
                };
                t.schedule(1500);
            }
        });
        formPanel.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            @Override
            public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) {
                timer.cancel();

                String result = event.getResults();
                // odstraneni pripadnych tagu
                result = result.replaceAll(ServletUtils.TAG_MATCH, "").replaceAll(ServletUtils.TAG_MATCH_CODE, "").trim();
                if (!ServletUtils.containHttpFail(result, parent.getFailMessage())) {
                    percent = FULL_PERCENT;
                }
            }
        });

        formPanel.setWidget(generateFormContent());

        add(formPanel);
    }

    private FlowPanel generateFormContent() {
        idName = "videoUpload" + uuid;

        videoUpload = new ConsysFileUpload(270);
        videoUpload.setName("videoFile" + uuid);
        videoUpload.setId(idName);

        final ActionImage submit = ActionImage.getConfirmButton(UIMessageUtils.c.const_sendUpper());
        submit.addClickHandler(sendHandler());
        submit.setEnabled(false);

        videoUpload.setEmptyHandler(new ConsysActionWithValue<Boolean>() {
            @Override
            public void run(Boolean data) {
                submit.setEnabled(!data);
            }
        });

        SimplePanel submitWrapper = new SimplePanel(submit);
        submitWrapper.addStyleName(StyleUtils.FLOAT_LEFT);
        submitWrapper.setWidth((videoUpload.getInputWidth() + 10) + "px");

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel(), StyleUtils.FLOAT_RIGHT + " " + StyleUtils.MARGIN_TOP_3);
        cancel.setClickHandler(cancelHandler());

        FlowPanel fp = new FlowPanel();
        fp.setWidth(270 + "px");
        fp.setStyleName(StyleUtils.MARGIN_TOP_10);
        fp.add(submitWrapper);
        fp.add(cancel);
        fp.add(StyleUtils.clearDiv());

        FlowPanel formContent = new FlowPanel();
        formContent.addStyleName(StyleUtils.OVERFLOW_HIDDEN);
        formContent.add(videoUpload);
        formContent.add(fp);
        return formContent;
    }

    private ClickHandler sendHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                EventBus.fire(new EventDispatchEvent(new CreateVideoRecordAction(uuid, videoUpload.getFilename()),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                sendForm();
                            }
                        }, dialog));
            }
        };
    }

    /** spusti odeslani formulare */
    private void sendForm() {
        try {
            int fileLength = fileSize(idName);
            if (fileLength > 0) {
                final ClientUser user = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
                final String uid = user.getUuid();
                final String overseer = EventURLFactory.overseerServlet("/upload/video") + "&uid=" + uid;

                statusUrl = EventURLFactory.overseerServlet("/upload/progress") + "&uid=" + uid + "&vid=" + uuid;

                formPanel.setAction(overseer + "&l=" + fileLength + "&myid=" + uuid);

                LayoutManager.get().getUploadingForms().put(uuid, VideoUploadingForm.this);
                LayoutManager.get().getUploadingFormsPlace().add(VideoUploadingForm.this);
                formPanel.submit();
            } else {
                dialog.getFailMessage().addOrSetText(ECoMessageUtils.c.videoFilesSettings_error_unsupportedBrowser());
            }
        } catch (NotInCacheException ex) {
            dialog.getFailMessage().addOrSetText("Not in cache: overseer");
        }
    }

    private ClickHandler cancelHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                dialog.hide();
            }
        };
    }

    /** 
     * pro ie je potřeba nastavit v Tools -> Internet Options -> Security -> Custom Level
     * (ActiveX controls and Plugins group enable the Initialize and script Activex controls not marked as safe for script option)
     */
    private native int fileSize(String idName)/*-{
     if (typeof FileReader !== "undefined") { // ff+chrome+safari
     return $doc.getElementById(idName).files[0].size;
     } else if(navigator.appName == 'Microsoft Internet Explorer') { // ie
     try {
     var myFSO = new ActiveXObject('Scripting.FileSystemObject');
     var filepath = $doc.getElementById(idName).value;
     var thefile = myFSO.getFile(filepath);
     return thefile.size;
     } catch(error) {
     return -1;
     }
     }
     return -1;
     }-*/;

    private void checkUploadStatus() {
        RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, statusUrl);
        rb.setCallback(new RequestCallback() {
            @Override
            public void onResponseReceived(Request request, Response response) {
                if (response.getStatusCode() == Response.SC_OK) {
                    String text = response.getText();
                    percent = StringUtils.isBlank(text) ? text : text + " %";
                }
            }

            @Override
            public void onError(Request request, Throwable exception) {
            }
        });
        try {
            rb.send();
        } catch (RequestException ex) {
        }
    }

    public String getPercent() {
        return percent;
    }

    public String getUuid() {
        return uuid;
    }
}
