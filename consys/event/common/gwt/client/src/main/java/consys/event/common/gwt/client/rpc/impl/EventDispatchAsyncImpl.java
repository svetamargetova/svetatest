package consys.event.common.gwt.client.rpc.impl;

import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.gwt.client.rpc.AbstractEventDispatchAsync;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventEntered;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.action.Result;
import consys.event.common.gwt.client.event.OverseerInitializedEvent;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.ui.event.ChangeContentEvent;
import consys.event.common.gwt.client.comp.Preparing;
import consys.event.common.gwt.client.event.EventOverseerInitializationEvent;
import consys.event.common.gwt.client.rpc.service.EventDispatchService;
import consys.event.common.gwt.client.rpc.service.EventDispatchServiceAsync;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * This class is the default implementation of {@link EventDispatchAsyncImpl}, which is
 * essentially the client-side access to the {@link Dispatch} class on the
 * server-side.
 *
 *
 * @author David Peterson
 */
public class EventDispatchAsyncImpl extends AbstractEventDispatchAsync implements EventEntered.Handler, EventOverseerInitializationEvent.Handler {

    private static final Logger logger = LoggerFactory.getLogger(EventDispatchAsyncImpl.class);
    // data
    private static EventDispatchServiceAsync realService;
    private String eventUuid;
    private String userUuid;

    public EventDispatchAsyncImpl() {
        super();
        EventBus.get().addHandler(EventEntered.TYPE, this);
        EventBus.get().addHandler(EventOverseerInitializationEvent.TYPE, this);
    }

    @Override
    public <A extends EventAction<R>, R extends Result> void execute(final A action, final AsyncCallback<R> callback) {
        // Nastavime context
        action.setEventUuid(eventUuid);
        action.setUserEventUuid(userUuid);


        // Posleme
        realService.execute(action, new AsyncCallback<Result>() {
            @Override
            public void onFailure(Throwable caught) {
                logger.debug(caught.getMessage() + " " + caught.toString());
                EventDispatchAsyncImpl.this.onFailure(action, caught, callback);
            }

            @Override
            public void onSuccess(Result result) {
                EventDispatchAsyncImpl.this.onSuccess(action, (R) result, callback);
            }
        });
    }

    /**
     * Inicializuje dispatch servicu na konkretneho overseera. EventUuid a OverseerUrl
     * musia byt vlozene. 
     * @param eventUuid
     * @param userUuid
     * @param overseerUrl 
     */
    private void initDispatchService(String eventUuid, String userUuid, String overseerUrl) {
        if (StringUtils.isBlank(eventUuid) || StringUtils.isBlank(overseerUrl)) {
            throw new NullPointerException("EventUuid(" + eventUuid + ") or OverseerUrl(" + overseerUrl + ") is empty!");
        }

        String url = EventURLFactory.constructOverseerDispatchURL(overseerUrl);
        logger.info("Initializing EventDispatchService to " + url);
        // Nainicialoizujeme novu servicu
        realService = (EventDispatchServiceAsync) GWT.create(EventDispatchService.class);
        ((ServiceDefTarget) realService).setServiceEntryPoint(url + "/dispatch");


        this.eventUuid = eventUuid;
        this.userUuid = userUuid;
    }

    // TODO: Nahradit to kompeletne EventOverseerInitializationEvent
    @Override
    public void onEventEnter(EventEntered event) {
        EventBus.fire(new ChangeContentEvent(new Preparing(ECoMessageUtils.c.const_preparingEventData())));
        try {
            // inivializacia s prave prihlasenym uzivatelom
            ClientUser user = Cache.get().getSafe(UserCacheAction.USER);
            initDispatchService(event.getEnteredEvent().getEvent().getUuid(), user.getUuid(), event.getEnteredEvent().getOverseerUrl());
        } catch (NotInCacheException ex) {
            // inicializacia bez uzivatela
            initDispatchService(event.getEnteredEvent().getEvent().getUuid(), null, event.getEnteredEvent().getOverseerUrl());
        }

        // Vyhodime event ze je vsetko nainicializovane
        EventBus.get().fireEvent(new OverseerInitializedEvent());
    }

    @Override
    public void initializeOverseer(EventOverseerInitializationEvent event) {
        initDispatchService(event.getEventUuid(), event.getUserUuid(), event.getOverseerUrl());
    }
}
