package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.Action;
import consys.event.common.gwt.client.bo.ClientOuterEventData;

/**
 * Nacita strucne informacie o eventu. Ak je potreba nacitat detailne informacie
 * pouzi  {@link LoadEventDetailsAction}.
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadOuterEventInfoAction implements Action<ClientOuterEventData> {

    private static final long serialVersionUID = -4047457288111094922L;
    // data
    private String eventUuid;
    private boolean fillDescription;

    public LoadOuterEventInfoAction() {
    }

    public LoadOuterEventInfoAction(String uuid) {
        this.eventUuid = uuid;
    }

    public LoadOuterEventInfoAction(String uuid, boolean fillDescription) {
        this(uuid);
        this.fillDescription = fillDescription;
    }

    public String getEventUuid() {
        return eventUuid;
    }

    public boolean isFillDescription() {
        return fillDescription;
    }
}
