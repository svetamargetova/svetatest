package consys.event.common.gwt.client.module.payment;

import com.google.gwt.user.client.ui.TextBox;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.ui.comp.panel.PropertyPanel;
import consys.common.gwt.client.ui.comp.panel.PropertyPanelItem;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 *
 * @author pepa
 */
public class PaymentsPropertyPanel extends SimpleFormPanel {

    public PaymentsPropertyPanel() {
        addWidget(new PaymentPropertyPanel());
    }

    private class PaymentPropertyPanel extends PropertyPanel {

        @Override
        protected void onLoad() {
            clear();
            try {
                String actualPercent = (String) Cache.get().getSafe(CommonProperties.EVENT_VAT_RATE);
                TextBox vatBox = StyleUtils.getFormInput();
                addProperty(new PropertyPanelItem(this, CommonProperties.EVENT_VAT_RATE, ECoMessageUtils.c.eventCommonPropertyPanel_property_defaultVatRate(), vatBox,
                        actualPercent, "", "170px", new PropertyPanelItem.PropertyPanelItemValidator() {

                    @Override
                    public void doValidate(TextBox box) throws BadInputException {
                        try {
                            double d = Double.parseDouble(box.getText().trim());
                            if (d < 0 || d > 100) {
                                throw new Exception();
                            }
                        } catch (Exception ex) {
                            throw new BadInputException(ECoMessageUtils.c.paymentsPropertyPanel_error_invalidVat());
                        }
                    }
                }), false);
            } catch (NotInCacheException ex) {
                getFailMessage().setText(UIMessageUtils.m.const_notInCache(CommonProperties.EVENT_CURRENCY));
                getFailMessage().addOrSetText(UIMessageUtils.m.const_notInCache(CommonProperties.EVENT_VAT_RATE));
            }
        }
    }
}
