package consys.event.common.gwt.client.module.payment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.PopupHelp;
import consys.common.gwt.client.ui.comp.panel.RootPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.constants.currency.CurrencyEnum;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.common.gwt.client.EventURLFactory;
import consys.event.common.gwt.client.action.LoadEventPaymentTransferDetailAction;
import consys.event.common.gwt.client.bo.ClientPaymentTransferDetail;
import consys.event.common.gwt.client.bo.ClientPaymentTransferHead;
import consys.event.common.gwt.client.bo.ClientPaymentTransferItem;
import consys.event.common.gwt.client.bo.ClientPaymentTransferState;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * Detail transferu
 * @author pepa
 */
public class PaymentTransfersDetail extends RootPanel {

    // data
    private String uuid;
    private DateTimeFormat dateFormat;
    private final CurrencyEnum ce;

    public PaymentTransfersDetail(String title, String uuid) {
        super(title);
        this.uuid = uuid;
        dateFormat = DateTimeFormat.getFormat("dd.MM.yy");
        ce = CurrencyEnum.toEnum(ECoMessageUtils.getCurrency());

        addLeftControll(ActionImage.breadcrumbBackBigBack());
    }

    @Override
    protected void onLoad() {
        EventBus.get().fireEvent(new EventDispatchEvent(new LoadEventPaymentTransferDetailAction(uuid),
                new AsyncCallback<ClientPaymentTransferDetail>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action excecutor
                    }

                    @Override
                    public void onSuccess(ClientPaymentTransferDetail result) {
                        init(result);
                    }
                }, this));
    }

    private void init(ClientPaymentTransferDetail data) {
        clear();

        SimplePanel padd = new SimplePanel();
        padd.setSize("25px", "10px");
        Label information = StyleUtils.getStyledLabel(ECoMessageUtils.c.paymentTransfers_text_information(), FONT_14PX, FONT_BOLD, TEXT_BLUE);
        Label total = StyleUtils.getStyledLabel(ECoMessageUtils.c.paymentTransfers_text_total(), FONT_14PX, FONT_BOLD, TEXT_BLUE);
        Label date = StyleUtils.getStyledLabel(ECoMessageUtils.c.paymentTransfers_text_date(), FONT_14PX, FONT_BOLD, TEXT_BLUE);
        Label fees = StyleUtils.getStyledLabel(ECoMessageUtils.c.paymentTransfers_text_fees(), FONT_14PX, FONT_BOLD, TEXT_BLUE);
        Label fittage = StyleUtils.getStyledLabel(ECoMessageUtils.c.paymentTransfers_text_fittage(), FONT_14PX, FONT_BOLD, TEXT_BLUE);
        Label download = StyleUtils.getStyledLabel(UIMessageUtils.c.const_download(), MARGIN_LEFT_10, FONT_14PX, FONT_BOLD, TEXT_BLUE);

        HorizontalPanel headPanel = new HorizontalPanel();
        headPanel.setHorizontalAlignment(HorizontalPanel.ALIGN_CENTER);
        headPanel.add(padd);
        headPanel.add(information);
        headPanel.add(total);
        headPanel.add(date);
        headPanel.add(fees);
        headPanel.add(fittage);
        headPanel.add(download);
        headPanel.setCellWidth(padd, "25px");
        headPanel.setCellWidth(information, "210px");
        headPanel.setCellWidth(total, "80px");
        headPanel.setCellWidth(date, "80px");
        headPanel.setCellWidth(fees, "80px");
        headPanel.setCellWidth(fittage, "80px");
        headPanel.setCellWidth(download, "115px");
        headPanel.setCellHorizontalAlignment(information, HorizontalPanel.ALIGN_LEFT);
        headPanel.setCellHorizontalAlignment(download, HorizontalPanel.ALIGN_LEFT);
        addWidget(headPanel);

        addWidget(getTopPanel(data.getHeader()));

        for (ClientPaymentTransferItem item : data.getItems()) {
            addWidget(getItem(item));
        }
    }

    private SimplePanel getTopPanel(final ClientPaymentTransferHead payItem) {
        SimplePanel wrapper = new SimplePanel();
        wrapper.addStyleName(MARGIN_TOP_10);
        DOM.setStyleAttribute(wrapper.getElement(), "borderTop", "1px solid #c4c4c4");
        DOM.setStyleAttribute(wrapper.getElement(), "borderBottom", "1px solid #c4c4c4");
        DOM.setStyleAttribute(wrapper.getElement(), "backgroundColor", "#f8f8f8");

        FlowPanel panel = new FlowPanel();
        panel.addStyleName(MARGIN_HOR_5);
        panel.addStyleName(MARGIN_VER_10);
        panel.add(StyleUtils.clearDiv());

        Image image = null;
        String helpText = "";
        switch (payItem.getState()) {
            case ACTIVE:
                image = new Image(ResourceUtils.system().transferGreen());
                helpText = ECoMessageUtils.c.paymentTransfers_help_activeTransfer();
                break;
            case ERR:
                image = new Image(ResourceUtils.system().transferRed());
                helpText = ECoMessageUtils.c.paymentTransfers_help_errorTransfer();
                break;
            case PREPARED:
                image = new Image(ResourceUtils.system().transferOrange());
                helpText = ECoMessageUtils.c.paymentTransfers_help_preparedTransfer();
                break;
            case TRANSFERED:
                image = new Image(ResourceUtils.system().transferBlue());
                helpText = ECoMessageUtils.c.paymentTransfers_help_transferedTransfer();
                break;
        }
        image.addStyleName(HAND);
        image.addStyleName(MARGIN_TOP_3);
        final PopupHelp pupImage = new PopupHelp(helpText, image);
        image.addMouseOverHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                pupImage.show();
            }
        });
        image.addMouseOutHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                pupImage.hide();
            }
        });

        SimplePanel status = new SimplePanel();
        status.setWidget(image);
        status.setSize("15px", "15px");
        status.addStyleName(FLOAT_LEFT);
        status.addStyleName(MARGIN_RIGHT_5);
        panel.add(status);

        Label infoLabel = StyleUtils.getStyledLabel(payItem.getInfo() == null ? FormUtils.NDASH : payItem.getInfo(), FLOAT_LEFT, FONT_BOLD);
        infoLabel.setWidth("210px");
        panel.add(infoLabel);

        String totalValue = payItem.getTotal() == null ? FormUtils.NDASH : UIMessageUtils.assignCurrency(ce, payItem.getTotal());
        Label totalLabel = StyleUtils.getStyledLabel(totalValue, FLOAT_LEFT, FONT_BOLD, ALIGN_RIGHT, MARGIN_RIGHT_3);
        totalLabel.setWidth("77px");
        panel.add(totalLabel);

        String dateValue = payItem.getTransferDate() == null ? FormUtils.NDASH : dateFormat.format(payItem.getTransferDate());
        Label dateLabel = StyleUtils.getStyledLabel(dateValue, FLOAT_LEFT, ALIGN_CENTER, MARGIN_HOR_3, FONT_BOLD);
        dateLabel.setWidth("74px");
        panel.add(dateLabel);

        String feesValue = payItem.getThirdPartyFees() == null ? FormUtils.NDASH : UIMessageUtils.assignCurrency(ce, payItem.getThirdPartyFees());
        Label feesLabel = StyleUtils.getStyledLabel(feesValue, FLOAT_LEFT, ALIGN_RIGHT, MARGIN_RIGHT_3, FONT_BOLD);
        feesLabel.setWidth("77px");
        panel.add(feesLabel);

        String fittingValue = payItem.getFees() == null ? FormUtils.NDASH : UIMessageUtils.assignCurrency(ce, payItem.getFees());
        Label fittingLabel = StyleUtils.getStyledLabel(fittingValue, FLOAT_LEFT, ALIGN_RIGHT, MARGIN_RIGHT_3, FONT_BOLD);
        fittingLabel.setWidth("77px");
        panel.add(fittingLabel);

        // ukladani / detail
        Image pdf = new Image(ResourceUtils.system().commonPdf());
        pdf.addStyleName(HAND);
        pdf.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                try {
                    Window.open(EventURLFactory.downloadInvoice(payItem.getUuid()), "_blank", "");
                } catch (NotInCacheException ex) {
                    getFailMessage().setText(UIMessageUtils.m.const_notInCache("EVENT_URL"));
                }
            }
        });
        Image zip = new Image(ResourceUtils.system().commonZip());
        zip.addStyleName(HAND);
        zip.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                try {
                    Window.open(EventURLFactory.downloadInvoiceTransferPack(payItem.getUuid()), "_blank", "");
                } catch (NotInCacheException ex) {
                    getFailMessage().setText(UIMessageUtils.m.const_notInCache("EVENT_URL"));
                }
            }
        });
        Image xls = new Image(ResourceUtils.system().commonXls());
        xls.addStyleName(HAND);
        xls.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                try {
                    Window.open(EventURLFactory.downloadExportTransferOverview(payItem.getUuid()), "_blank", "");
                } catch (NotInCacheException ex) {
                    getFailMessage().setText(UIMessageUtils.m.const_notInCache("EVENT_URL"));
                }
            }
        });

        SimplePanel pdfPanel = new SimplePanel();
        pdfPanel.setSize("21px", "16px");
        pdfPanel.addStyleName(FLOAT_LEFT);
        pdfPanel.addStyleName(MARGIN_LEFT_20);
        pdfPanel.addStyleName(MARGIN_RIGHT_5);
        pdfPanel.addStyleName(OVERFLOW_HIDDEN);

        if (!payItem.getState().equals(ClientPaymentTransferState.ACTIVE)) {
            pdfPanel.setWidget(pdf);
        }

        SimplePanel zipPanel = new SimplePanel();
        zipPanel.setWidth("21px");
        zipPanel.addStyleName(FLOAT_LEFT);
        zipPanel.addStyleName(MARGIN_HOR_5);
        zipPanel.setWidget(zip);

        SimplePanel xlsPanel = new SimplePanel();
        xlsPanel.setSize("21px", "16px");
        xlsPanel.addStyleName(FLOAT_LEFT);
        xlsPanel.addStyleName(MARGIN_HOR_5);

        if (payItem.getState().equals(ClientPaymentTransferState.PREPARED)
                || payItem.getState().equals(ClientPaymentTransferState.TRANSFERED)) {
            xlsPanel.setWidget(xls);
        }

        FlowPanel downloads = new FlowPanel();
        downloads.addStyleName(FLOAT_LEFT);
        downloads.addStyleName(MARGIN_LEFT_5);
        downloads.add(pdfPanel);
        downloads.add(zipPanel);
        downloads.add(xlsPanel);
        panel.add(downloads);

        panel.add(StyleUtils.clearDiv());

        wrapper.setWidget(panel);
        return wrapper;
    }

    private SimplePanel getItem(final ClientPaymentTransferItem item) {
        SimplePanel wrapper = new SimplePanel();
        wrapper.addStyleName(MARGIN_TOP_20);

        FlowPanel panel = new FlowPanel();
        panel.addStyleName(MARGIN_HOR_5);
        panel.addStyleName(MARGIN_VER_10);
        panel.add(StyleUtils.clearDiv());

        SimplePanel status = new SimplePanel();
        status.setSize("15px", "15px");
        status.addStyleName(FLOAT_LEFT);
        status.addStyleName(MARGIN_RIGHT_5);
        panel.add(status);

        Label infoLabel = StyleUtils.getStyledLabel(item.getInfo() == null ? FormUtils.NDASH : item.getInfo(), FLOAT_LEFT);
        infoLabel.setWidth("210px");
        panel.add(infoLabel);

        String totalValue = item.getTotal() == null ? FormUtils.NDASH : UIMessageUtils.assignCurrency(ce, item.getTotal());
        Label totalLabel = StyleUtils.getStyledLabel(totalValue, FLOAT_LEFT, FONT_BOLD, ALIGN_RIGHT, MARGIN_RIGHT_3);
        totalLabel.setWidth("77px");
        panel.add(totalLabel);

        String dateValue = item.getInvoicePaidDate() == null ? FormUtils.NDASH : dateFormat.format(item.getInvoicePaidDate());
        Label dateLabel = StyleUtils.getStyledLabel(dateValue, FLOAT_LEFT, ALIGN_CENTER, MARGIN_HOR_3);
        dateLabel.setWidth("74px");
        panel.add(dateLabel);

        String feesValue = item.getThirdPartyFees() == null ? FormUtils.NDASH : UIMessageUtils.assignCurrency(ce, item.getThirdPartyFees());
        Label feesLabel = StyleUtils.getStyledLabel(feesValue, FLOAT_LEFT, ALIGN_RIGHT, MARGIN_RIGHT_3);
        feesLabel.setWidth("77px");
        panel.add(feesLabel);

        String fittingValue = item.getFees() == null ? FormUtils.NDASH : UIMessageUtils.assignCurrency(ce, item.getFees());
        Label fittingLabel = StyleUtils.getStyledLabel(fittingValue, FLOAT_LEFT, ALIGN_RIGHT, MARGIN_RIGHT_3);
        fittingLabel.setWidth("77px");
        panel.add(fittingLabel);

        // ukladani / detail
        Image pdf = new Image(ResourceUtils.system().commonPdf());
        pdf.addStyleName(HAND);
        pdf.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                try {
                    Window.open(EventURLFactory.downloadInvoice(item.getUuid()), "_blank", "");
                } catch (NotInCacheException ex) {
                    getFailMessage().setText(UIMessageUtils.m.const_notInCache("EVENT_URL"));
                }
            }
        });

        SimplePanel pdfPanel = new SimplePanel();
        pdfPanel.setWidth("21px");
        pdfPanel.addStyleName(FLOAT_LEFT);
        pdfPanel.addStyleName(MARGIN_LEFT_20);
        pdfPanel.addStyleName(MARGIN_RIGHT_10);
        pdfPanel.setWidget(pdf);

        FlowPanel downloads = new FlowPanel();
        downloads.addStyleName(FLOAT_LEFT);
        downloads.addStyleName(MARGIN_LEFT_5);
        downloads.add(pdfPanel);
        panel.add(downloads);

        panel.add(StyleUtils.clearDiv());

        wrapper.setWidget(panel);
        return wrapper;
    }
}
