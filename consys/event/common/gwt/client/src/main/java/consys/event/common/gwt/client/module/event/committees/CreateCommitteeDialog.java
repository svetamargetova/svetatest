package consys.event.common.gwt.client.module.event.committees;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.CreateDialog;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.panel.ListEditPanel;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.search.SearchListDelegate;
import consys.common.gwt.client.ui.comp.user.UserListThumbLabel;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.comp.user.list.UserLabelListEditPanel;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.gwt.shared.bo.InvitedUser;
import consys.event.common.gwt.client.action.CreateCommitteeAction;
import consys.event.common.gwt.client.bo.ClientCommitteeThumb;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import java.util.ArrayList;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CreateCommitteeDialog extends CreateDialog {

    // komponenty
    private ConsysStringTextBox committeeNameBox;
    private ActionLabel selectChairActionLabel;
    private ListEditPanel<RightListEditItem> chairRightsEditList;
    private ListEditPanel<RightListEditItem> membersRightsEditList;
    private UserLabelListEditPanel membersUserEditList;
    private ClientInvitedUser committeeChair;
    private FlowPanel failPanel;
    // data
    private EventCommittees parent;

    public CreateCommitteeDialog(EventCommittees parent) {
        super("New Committee");
        this.parent = parent;

    }

    @Override
    protected Widget createForm() {
        Form form = new Form();

        // NAME
        committeeNameBox = new ConsysStringTextBox(1, 50);
        form.addRequired(ECoMessageUtils.c.committee_text_committeeName(), committeeNameBox);

        // CHAIR - name , za menom je odkaz na zmenu chaira
        selectChairActionLabel = new ActionLabel(ECoMessageUtils.c.committee_action_chairSelect());

        selectChairActionLabel.addStyleName(CssStyles.FONT_11PX);
        selectChairActionLabel.addStyleName(CssStyles.MARGIN_LEFT_10);

        final FlowPanel chairNamePanel = new FlowPanel();
        chairNamePanel.add(selectChairActionLabel);

        selectChairActionLabel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog searchDialog = new UserSearchDialog();
                searchDialog.setDelegate(new SearchDelegate<ClientInvitedUser>() {

                    @Override
                    public void didSelect(ClientInvitedUser item) {
                        committeeChair = item;
                        chairNamePanel.clear();
                        UserListThumbLabel chairLabel = new UserListThumbLabel(committeeChair.getUserWithAffiliationThumb());
                        chairNamePanel.add(chairLabel);

                        selectChairActionLabel.setText(ECoMessageUtils.c.committee_action_chairChange());

                        chairNamePanel.add(selectChairActionLabel);
                    }
                });
                searchDialog.showCentered();
            }
        });

        // CHAIR - Rights
        ArrayList<RightListEditItem> chairEditListItems = new ArrayList<RightListEditItem>();
        chairRightsEditList = new ListEditPanel<RightListEditItem>(chairEditListItems, RightListItem.HEIGHT, 5);
        chairRightsEditList.addListStyleName(ResourceUtils.system().css().borderGray1());
        chairRightsEditList.setActionName(ECoMessageUtils.c.committee_action_addRight());
        chairRightsEditList.setActionClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                RightsDialog dialog = new RightsDialog();
                dialog.setListDelegate(new SearchListDelegate<RightListItem>() {

                    @Override
                    public void didSelect(ArrayList<RightListItem> items) {
                        for (RightListItem item : items) {
                            chairRightsEditList.addItem(new RightListEditItem(item.getItem()));
                        }
                    }
                });
                dialog.setAddedRights(chairRightsEditList.getAllItems());
                dialog.showCentered();
            }
        });
        chairRightsEditList.setStaticHeight();

        FlowPanel chairPanel = new FlowPanel();
        chairPanel.add(chairNamePanel);
        chairNamePanel.addStyleName(CssStyles.MARGIN_BOT_5);
        chairPanel.add(chairRightsEditList);
        form.addRequired(ECoMessageUtils.c.committee_form_committeeChair(), chairPanel);

        // MEMBERS - users
        membersUserEditList = new UserLabelListEditPanel(new ArrayList<ClientUserThumb>());
        membersUserEditList.setStaticHeight();
        membersUserEditList.addListStyleName(ResourceUtils.system().css().borderGray1());
        membersUserEditList.setAddLabelName(ECoMessageUtils.c.committee_action_addMember());
        form.addSeparator();
        form.addOptional(ECoMessageUtils.c.committee_form_committeeMembers(), membersUserEditList);

        // MEMBERS - rights
        ArrayList<RightListEditItem> membersEditListItems = new ArrayList<RightListEditItem>();
        membersRightsEditList = new ListEditPanel<RightListEditItem>(membersEditListItems, RightListItem.HEIGHT, 5);
        membersRightsEditList.addListStyleName(ResourceUtils.system().css().borderGray1());
        membersRightsEditList.setActionName(ECoMessageUtils.c.committee_action_addRight());
        membersRightsEditList.setActionClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                RightsDialog dialog = new RightsDialog();
                dialog.setListDelegate(new SearchListDelegate<RightListItem>() {

                    @Override
                    public void didSelect(ArrayList<RightListItem> items) {
                        for (RightListItem item : items) {
                            membersRightsEditList.addItem(new RightListEditItem(item.getItem()));
                        }
                    }
                });
                dialog.setAddedRights(membersRightsEditList.getAllItems());
                dialog.showCentered();

            }
        });
        membersRightsEditList.setStaticHeight();

        // Vlozeni separatoru a kontroleru
        form.addSeparator();
        form.addOptional(ECoMessageUtils.c.committee_form_committeeRights(), membersRightsEditList);

        failPanel = new FlowPanel();
        failPanel.addStyleName(StyleUtils.MARGIN_LEFT_10);

        FlowPanel panel = new FlowPanel();
        panel.add(failPanel);
        panel.add(form);
        return panel;
    }

    @Override
    protected void onSave() {
        if (!committeeNameBox.doValidate(getFailMessage())) {
            return;
        }

        final String name = committeeNameBox.getText().trim();
        InvitedUser chair = committeeChair == null ? null : committeeChair.parentObject();
        ArrayList<String> chairRights = new ArrayList<String>();
        ArrayList<InvitedUser> members = new ArrayList<InvitedUser>();
        ArrayList<String> rights = new ArrayList<String>();

        for (RightListEditItem r : chairRightsEditList.getNewMembers()) {
            chairRights.add(r.getObject().getId());
        }

        for (ClientInvitedUser u : membersUserEditList.getNewMembers()) {
            members.add(u.parentObject());
        }

        for (RightListEditItem r : membersRightsEditList.getNewMembers()) {
            rights.add(r.getObject().getId());
        }

        CreateCommitteeAction createCommitteeAction = new CreateCommitteeAction();
        createCommitteeAction.setName(name);
        createCommitteeAction.setChair(chair);
        createCommitteeAction.setChairRights(chairRights);
        createCommitteeAction.setMembers(members);
        createCommitteeAction.setRights(rights);

        EventBus.get().fireEvent(new EventDispatchEvent(
                createCommitteeAction,
                new AsyncCallback<LongResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava event action executer
                    }

                    @Override
                    public void onSuccess(LongResult result) {
                        ClientCommitteeThumb cct = new ClientCommitteeThumb();
                        cct.setId(result.getValue());
                        cct.setName(name);
                        parent.addItem(new EventCommitteePanel(cct));
                        hide();
                    }
                }, this));
    }

    @Override
    protected void onCancel() {
    }

    @Override
    public ConsysMessage getFailMessage() {
        ConsysMessage failMessage = ConsysMessage.getFail();
        failMessage.addStyleName(StyleUtils.MARGIN_BOT_10);
        failPanel.add(failMessage);
        return failMessage;
    }
}
