package consys.event.common.gwt.client.module.payment;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysCheckBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.panel.ConsysFlowPanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormWithHelpPanel;
import consys.common.gwt.client.ui.event.ChangeValueEvent;
import consys.common.gwt.client.ui.payment.PaymentProfilePanel;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.exception.IbanException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.gwt.client.action.BuyLicencesAction;
import consys.event.common.gwt.client.action.ListPaymentDataDialogAction;
import consys.event.common.gwt.client.action.LoadLicenseInfoAction;
import consys.event.common.gwt.client.action.LoadLicensePaymentDataAction;
import consys.event.common.gwt.client.action.exception.LicensePurchasedException;
import consys.event.common.gwt.client.action.exception.ProfileNotFilledException;
import consys.event.common.gwt.client.bo.ClientEventConfirmed;
import consys.event.common.gwt.client.bo.ClientEventLicense;
import consys.event.common.gwt.client.event.UpdateEventPaymentProfileEvent;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * Licence k platbam
 * @author pepa
 */
public class PaymentsLicencesContent extends SimpleFormWithHelpPanel {

    // komponenty
    private ActionImage button;
    private SimplePanel infoPanel;
    private ConsysCheckBox agree1;
    private ConsysCheckBox agree2;
    private ConsysCheckBox agree3;
    // data
    private ClientCurrentEvent ce;

    public PaymentsLicencesContent() {
        addWidget(StyleUtils.getStyledLabel(ECoMessageUtils.c.paymentsLicencesContent_title(), FONT_16PX, FONT_BOLD, MARGIN_BOT_20));
        addHelpText(ECoMessageUtils.c.paymentsLicencesContent_helpText());
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        EventBus.fire(new EventDispatchEvent(new LoadLicenseInfoAction(),
                new AsyncCallback<ClientEventLicense>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }

                    @Override
                    public void onSuccess(ClientEventLicense result) {
                        init(result);
                    }
                }, this));
    }

    private void init(ClientEventLicense data) {
        infoPanel = new SimplePanel();

        final BaseForm form = new BaseForm();
        try {
            ce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache("CLIENT_EVENT"));
            return;
        }

        agree1 = agreeCheckBox();
        agree2 = agreeCheckBox();
        agree3 = agreeCheckBox();

        ConsysFlowPanel licencesPanel = new ConsysFlowPanel();
        licencesPanel.add(agreePanel(agree1, agreeHTML(ECoMessageUtils.c.paymentsLicencesContent_text_agree1())));
        licencesPanel.add(agreePanel(agree2, agreeHTML(ECoMessageUtils.c.paymentsLicencesContent_text_agree2())));
        licencesPanel.add(agreePanel(agree3, agreeHTML(ECoMessageUtils.c.paymentsLicencesContent_text_agree3())));

        button = ActionImage.getCheckButton(ECoMessageUtils.c.paymentsLicencesContent_button_activateEvent());
        button.setEnabled(false);
        button.addClickHandler(buttonClickHandler(ce.getEvent().getUuid()));

        if (data.isLicensePurchased()) {
            enableAgrees(false);
            agree1.setValue(Boolean.TRUE);
            agree2.setValue(Boolean.TRUE);
            agree3.setValue(Boolean.TRUE);
            button.setEnabled(false);
            button.setVisible(false);

            ConsysMessage infoMessage = ConsysMessage.getInfo();
            infoMessage.hideClose(true);
            infoMessage.setText(ECoMessageUtils.m.paymentsLicencesContent_text_confirmed(
                    ce.getEvent().getAcronym(),
                    DateTimeUtils.getDate(data.getActivatedFrom()),
                    DateTimeUtils.getDate(data.getActivatedTo())), true);
            infoPanel.setWidget(infoMessage);
        } else {
            agree1.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

                @Override
                public void onChangeValue(ChangeValueEvent<Boolean> event) {
                    if (event.getValue() && agree2.getValue() && agree3.getValue()) {
                        button.setEnabled(true);
                    } else {
                        button.setEnabled(false);
                    }
                }
            });
            agree2.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

                @Override
                public void onChangeValue(ChangeValueEvent<Boolean> event) {
                    if (event.getValue() && agree1.getValue() && agree3.getValue()) {
                        button.setEnabled(true);
                    } else {
                        button.setEnabled(false);
                    }
                }
            });
            agree3.addChangeValueHandler(new ChangeValueEvent.Handler<Boolean>() {

                @Override
                public void onChangeValue(ChangeValueEvent<Boolean> event) {
                    if (event.getValue() && agree1.getValue() && agree2.getValue()) {
                        button.setEnabled(true);
                    } else {
                        button.setEnabled(false);
                    }
                }
            });
        }

        form.addOptional(0, ECoMessageUtils.c.paymentsLicencesContent_form_agreements(), licencesPanel);
        form.addActionMembers(1, button, null);

        FlowPanel panel = new FlowPanel();
        panel.add(FormPanelUtils.getOneLineEditMode(ECoMessageUtils.c.paymentsLicencesContent_title()));
        panel.add(form);
        panel.add(infoPanel);
        setWidget(panel);
    }

    private ConsysCheckBox agreeCheckBox() {
        ConsysCheckBox agree = new ConsysCheckBox();
        agree.addStyleName(FLOAT_LEFT);
        agree.addStyleName(MARGIN_RIGHT_10);
        return agree;
    }

    private HTML agreeHTML(String text) {
        HTML agreeHTML = new HTML(text);
        agreeHTML.addStyleName(ResourceUtils.system().css().htmlAgreeing());
        agreeHTML.addStyleName(FLOAT_LEFT);
        return agreeHTML;
    }

    private FlowPanel agreePanel(ConsysCheckBox agreeBox, HTML agreeHTML) {
        FlowPanel panel = new FlowPanel();
        panel.setWidth("320px");
        panel.add(agreeBox);
        panel.add(agreeHTML);
        panel.add(StyleUtils.clearDiv());
        return panel;
    }

    private void enableAgrees(boolean enable) {
        agree1.setEnabled(enable);
        agree2.setEnabled(enable);
        agree3.setEnabled(enable);
    }

    private ClickHandler buttonClickHandler(final String eventUuid) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {

                PaymentDialog payDialog = new PaymentDialog(eventUuid, false, true, true) {

                    @Override
                    public void doAction(final PaymentProfilePanel profilePanel, String userUuid, ConsysMessage fail) {
                        final PaymentDialog self = this;
                        if (profilePanel.isEdited() && !profilePanel.doValidate(fail)) {
                            return;
                        }
                        BuyLicencesAction buyLicenceAction = new BuyLicencesAction(userUuid,
                                profilePanel.getClientPaymentProfile().getUuid());
                        buyLicenceAction.setProfileEdited(profilePanel.isEdited());
                        if (profilePanel.isEdited()) {
                            buyLicenceAction.setEditedProfile(profilePanel.getClientPaymentProfile());
                        }
                        EventBus.get().fireEvent(new EventDispatchEvent(
                                buyLicenceAction,
                                new AsyncCallback<ClientEventConfirmed>() {

                                    @Override
                                    public void onFailure(Throwable caught) {
                                        if (caught instanceof LicensePurchasedException) {
                                            self.getFailMessage().setText("LicensePurchasedException");
                                        } else if (caught instanceof ProfileNotFilledException) {
                                            self.getFailMessage().setText("ProfileNotFilledException");
                                        } else if (caught instanceof IbanException) {
                                            self.getFailMessage().setText("IbanException");
                                        } else if (caught instanceof NoRecordsForAction) {
                                            self.getFailMessage().setText("NoRecordsForAction");
                                        }
                                    }

                                    @Override
                                    public void onSuccess(ClientEventConfirmed result) {
                                        enableAgrees(false);
                                        button.setEnabled(false);
                                        button.setVisible(false);

                                        ConsysMessage infoMessage = ConsysMessage.getInfo();
                                        infoMessage.hideClose(true);
                                        infoMessage.setText(ECoMessageUtils.m.paymentsLicencesContent_text_confirmed(
                                                ce.getEvent().getAcronym(),
                                                DateTimeUtils.getDate(result.getFrom()),
                                                DateTimeUtils.getDate(result.getTo())), true);
                                        infoPanel.setWidget(infoMessage);

                                        fireUpdateEventProfileIfChanged(profilePanel);

                                        hide();
                                    }
                                }, self));
                    }

                    @Override
                    public ListPaymentDataDialogAction getDataAction() {
                        return new LoadLicensePaymentDataAction();
                    }
                };
                payDialog.setEventCustomer(true);
                payDialog.showCentered();
            }
        };
    }

    private void fireUpdateEventProfileIfChanged(PaymentProfilePanel profilePanel) {
        if (profilePanel.isEdited()) {
            EventBus.get().fireEvent(new UpdateEventPaymentProfileEvent(profilePanel.getClientPaymentProfile()));
        }
    }
}
