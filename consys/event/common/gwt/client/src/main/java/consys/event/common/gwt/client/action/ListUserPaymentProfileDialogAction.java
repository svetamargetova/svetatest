package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.common.gwt.client.bo.ClientPaymentProfileDialog;

/**
 * Akce pro nacteni profilu pro platebni dialog
 * @author pepa
 */
public class ListUserPaymentProfileDialogAction extends EventAction<ClientPaymentProfileDialog> {

    private static final long serialVersionUID = 8943368709549235982L;
    // data
    private String managedUserUuid;

    public ListUserPaymentProfileDialogAction() {
    }

    public ListUserPaymentProfileDialogAction(String managedUserUuid) {
        this.managedUserUuid = managedUserUuid;
    }

    public String getManagedUserUuid() {
        return managedUserUuid;
    }
}
