package consys.event.common.gwt.client.module.event.video;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.SmartDialog;
import consys.common.gwt.client.ui.comp.list.DataListCommonPanel;
import consys.common.gwt.client.ui.comp.list.DataListPanel;
import consys.common.gwt.client.ui.comp.list.ListDelegate;
import consys.common.gwt.client.ui.comp.list.ListFilter;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.common.api.list.VideoFilesList;
import consys.event.common.gwt.client.action.LoadUuidAction;
import consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 *
 * @author pepa
 */
public class VideoFilesSettings extends DataListPanel implements VideoFilesList {

    public VideoFilesSettings() {
        super(ECoMessageUtils.c.videoFilesSettings_title(), LIST_TAG);

        setListDelegate(new VideoFilesListDelegate());
        setDefaultFilter(new ListFilter(Filter_ALL) {
            @Override
            public void deselect() {
            }

            @Override
            public void select() {
            }
        });

        ActionImage add = ActionImage.getPlusButton(ECoMessageUtils.c.videoFilesSettings_action_addVideo(), true);
        add.addClickHandler(addVideoClickHandler());
        addLeftControll(add);

        DataListCommonPanel cp = new DataListCommonPanel();
        cp.addOrderer(ECoMessageUtils.c.videoFilesSettings_text_videoName(), Order_NAME);
        cp.addFilter(ECoMessageUtils.c.videoFilesSettings_text_all(), Filter_ALL, true);
        cp.addFilter(ECoMessageUtils.c.videoFilesSettings_state_uploading(), Filter_UPLOADING);
        cp.addFilter(ECoMessageUtils.c.videoFilesSettings_state_processing(), Filter_PROCESSING);
        cp.addFilter(ECoMessageUtils.c.videoFilesSettings_state_processed(), Filter_PROCESSED);
        cp.addFilter(ECoMessageUtils.c.videoFilesSettings_state_error(), Filter_ERROR);
        addHeadPanel(cp);
    }

    private ClickHandler addVideoClickHandler() {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                AddVideoDialog dialog = new AddVideoDialog();
                dialog.showCentered();
            }
        };
    }

    public void refresh() {
        onLoad();
    }

    private class VideoFilesListDelegate implements ListDelegate<ClientVideoFilesListItem, VideoFilesListItem> {

        @Override
        public VideoFilesListItem createCell(ClientVideoFilesListItem item) {
            return new VideoFilesListItem(item, VideoFilesSettings.this);
        }
    }

    public class AddVideoDialog extends SmartDialog {

        @Override
        protected void generateContent(SimpleFormPanel panel) {
            panel.clear();
            EventBus.fire(dispatchEvent());
        }

        private EventDispatchEvent dispatchEvent() {
            return new EventDispatchEvent(new LoadUuidAction(), new AsyncCallback<StringResult>() {
                @Override
                public void onFailure(Throwable caught) {
                    // obecne chyby zpracovava event action executor
                }

                @Override
                public void onSuccess(StringResult result) {
                    panel.addWidget(StyleUtils.getPadder());
                    panel.addWidget(StyleUtils.getStyledLabel(ECoMessageUtils.c.videoFilesSettings_action_addVideo(),
                            ResourceUtils.system().css().createDialogHeaderTitle()));
                    panel.addWidget(StyleUtils.getPadder());
                    panel.addWidget(new VideoUploadingForm(AddVideoDialog.this, VideoFilesSettings.this, result.getStringResult()));
                    panel.addWidget(StyleUtils.getPadder());
                }
            }, this);
        }
    }
}
