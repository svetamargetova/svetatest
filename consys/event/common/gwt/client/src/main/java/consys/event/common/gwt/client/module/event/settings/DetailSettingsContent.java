package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.action.ListLocationAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventCacheAction;
import consys.common.gwt.client.rpc.action.cache.CurrentEventTimeZoneCacheAction;
import consys.common.gwt.client.rpc.result.BoxItem;
import consys.common.gwt.client.rpc.result.SelectBoxData;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ConsysDateBox;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.SelectBox;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.TimeZoneProvider;
import consys.common.gwt.shared.bo.ClientCurrentEvent;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.common.gwt.shared.exception.ReadOnlyException;
import consys.event.common.gwt.client.action.LoadEventDetailsAction;
import consys.event.common.gwt.client.action.UpdateDetailedEventSettingsAction;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import java.util.ArrayList;

/**
 * Time and Location
 * Formular pro nastaveni mista a data konani eventu
 * @author pepa
 */
public class DetailSettingsContent extends RUSimpleHelpFormPanel {

    // logger
    private Logger log = LoggerFactory.getLogger(DetailSettingsContent.class);
    // komponenty
    private Form editForm;
    private SelectBox<Integer> locationBox;
    private SelectBox<String> timeZoneBox;
    private ConsysStringTextBox cityBox;
    private ConsysStringTextBox streetBox;
    private ConsysStringTextBox placeBox;
    private ConsysDateBox fromDate;
    private ConsysDateBox toDate;

    public DetailSettingsContent() {
        super(true);
        //addHelpTitle("HELP TITLE");
        //addHelpText("Text by mal obsahovat informacie o tom ze tieto su o adrese a casse konania eventu");
    }

    @Override
    public Widget readForm() {
        final Label fromDateLabel = new Label("");
        fromDateLabel.addStyleName(StyleUtils.MARGIN_RIGHT_10);
        final Label toDateLabel = new Label("");
        toDateLabel.addStyleName(StyleUtils.MARGIN_LEFT_10);
        final Label locationLabel = new Label();
        final Label cityLabel = new Label();
        final Label streetLabel = new Label();
        final Label placeLabel = new Label();
        final Label timeZoneLabel = new Label();

        HorizontalPanel hp = new HorizontalPanel();
        hp.add(fromDateLabel);
        hp.add(new Label(UIMessageUtils.c.const_toLower()));
        hp.add(toDateLabel);

        Form viewForm = new Form();
        viewForm.addOptional(ECoMessageUtils.c.const_date(), hp);
        viewForm.addRequired(UIMessageUtils.c.const_address_location(), locationLabel);
        viewForm.addOptional(UIMessageUtils.c.const_address_city(), cityLabel);
        viewForm.addOptional(UIMessageUtils.c.const_address_street(), streetLabel);
        viewForm.addOptional(UIMessageUtils.c.const_address_place(), placeLabel);
        viewForm.addRequired(UIMessageUtils.c.const_timeZone(), timeZoneLabel);

        EventBus.get().fireEvent(new DispatchEvent(new LoadEventDetailsAction(), new AsyncCallback<ClientEvent>() {

            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(final ClientEvent e) {
                log.debug("Incoming dates (readForm): from<" + e.getFrom() + "> to <" + e.getTo() + ">");
                fromDateLabel.setText(DateTimeUtils.getDateAndTime(e.getFrom()));
                toDateLabel.setText(DateTimeUtils.getDateAndTime(e.getTo()));
                cityLabel.setText(e.getCity());
                streetLabel.setText(e.getStreet());
                placeLabel.setText(e.getPlace());
                timeZoneLabel.setText(getEventTimeZone().getID());
                if (e.getLocation() > 0) {
                    EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // obecne chyby zpracovava action executor
                        }

                        @Override
                        public void onSuccess(SelectBoxData result) {
                            for (BoxItem item : result.getList()) {
                                if (e.getLocation().equals(item.getId())) {
                                    locationLabel.setText(item.getName());
                                    break;
                                }
                            }
                        }
                    }, DetailSettingsContent.this));
                }
            }
        }, this));

        FlowPanel panel = new FlowPanel();
        panel.add(FormPanelUtils.getOneLinedReadMode(this, ECoMessageUtils.c.detailSettingsContent_title(), UIMessageUtils.c.const_edit()));
        panel.add(viewForm);
        return panel;
    }

    @Override
    public Widget updateForm(String id) {
        fromDate = createDateBox();
        toDate = createDateBox();
        locationBox = new SelectBox<Integer>();
        cityBox = new ConsysStringTextBox(0, 255, UIMessageUtils.c.const_address_city());
        streetBox = new ConsysStringTextBox(0, 255, UIMessageUtils.c.const_address_street());
        placeBox = new ConsysStringTextBox(0, 255, UIMessageUtils.c.const_address_place());
        timeZoneBox = new SelectBox<String>();

        fromDate.addValueChangeHandler(DateTimeUtils.dateValueChangeHandler(fromDate, toDate));

        FlowPanel fp = new FlowPanel();
        fp.add(fromDate);
        fp.add(StyleUtils.getStyledLabel(UIMessageUtils.c.const_toLower(), MARGIN_HOR_20, FLOAT_LEFT, MARGIN_TOP_3));
        fp.add(toDate);
        fp.add(StyleUtils.clearDiv());

        editForm = new Form();
        editForm.addOptional(ECoMessageUtils.c.const_date(), fp);
        editForm.addRequired(UIMessageUtils.c.const_address_location(), locationBox);
        editForm.addOptional(UIMessageUtils.c.const_address_city(), cityBox);
        editForm.addOptional(UIMessageUtils.c.const_address_street(), streetBox);
        editForm.addOptional(UIMessageUtils.c.const_address_place(), placeBox);
        editForm.addOptional(UIMessageUtils.c.const_timeZone(), timeZoneBox);

        editForm.addActionMembers(FormPanelUtils.getUpdateButton(this, ""), FormPanelUtils.getCancelButton(this));

        EventBus.get().fireEvent(new DispatchEvent(new LoadEventDetailsAction(), new AsyncCallback<ClientEvent>() {

            @Override
            public void onFailure(Throwable caught) {
                // obecne chyby zpracovava action executor
            }

            @Override
            public void onSuccess(final ClientEvent e) {
                log.debug("Incoming dates (updateForm): from<" + e.getFrom() + "> to <" + e.getTo() + ">");
                fromDate.setValue(DateTimeUtils.toDate(e.getFrom()));
                toDate.setValue(DateTimeUtils.toDate(e.getTo()));
                cityBox.setText(e.getCity());
                streetBox.setText(e.getStreet());
                placeBox.setText(e.getPlace());

                EventBus.get().fireEvent(new DispatchEvent(new ListLocationAction(), new AsyncCallback<SelectBoxData>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                        if (caught instanceof ReadOnlyException) {
                            // ToDo: Nasetovat Text o tem ze country je mozne menit LEN a LEN pred Aktivaciu eventu.
                            // Ked je EVENT zaktivovany tak uz nemoze
                        }
                    }

                    @Override
                    public void onSuccess(SelectBoxData result) {
                        ArrayList<SelectBoxItem<Integer>> boxItems = new ArrayList<SelectBoxItem<Integer>>(result.getList().size());
                        for (BoxItem item : result.getList()) {
                            boxItems.add(new SelectBoxItem<Integer>(item.getId(), item.getName()));
                        }
                        locationBox.setItems(boxItems);
                        if (e.getLocation() > 0) {
                            locationBox.selectItem(e.getLocation());
                        }
                    }
                }, DetailSettingsContent.this));

                // initialize timezone box
                ArrayList<SelectBoxItem<String>> boxItems = new ArrayList<SelectBoxItem<String>>();
                for (String item : TimeZoneProvider.getTimeZonesNames()) {
                    boxItems.add(new SelectBoxItem<String>(item));
                }
                timeZoneBox.setItems(boxItems);
                timeZoneBox.selectItem(getEventTimeZone().getID());
            }
        }, this));

        FlowPanel out = new FlowPanel();
        out.add(FormPanelUtils.getOneLineEditMode(ECoMessageUtils.c.detailSettingsContent_title()));
        out.add(editForm);
        return out;
    }

    private ConsysDateBox createDateBox() {
        ConsysDateBox db = ConsysDateBox.getHalfBox(true);
        db.addStyleName(FLOAT_LEFT);
        return db;
    }

    private TimeZone getEventTimeZone() {
        return Cache.get().<TimeZone>get(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE);
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // Validacia
                if (locationBox.getSelectedItem() == null) {
                    getFailMessage().setText(UIMessageUtils.m.const_mustChoose(UIMessageUtils.c.const_address_location()));
                    return;
                }
                int year;
                try {
                    ClientCurrentEvent cce = (ClientCurrentEvent) Cache.get().getSafe(CurrentEventCacheAction.CURRENT_EVENT);
                    year = cce.getEvent().getYear();
                } catch (NotInCacheException ex) {
                    getFailMessage().setText(UIMessageUtils.m.const_notInCache(ECoMessageUtils.c.detailSettingsContent_text_currentEvent()));
                    return;
                }
                if (fromDate.getValue() != null && DateTimeUtils.getYear(fromDate.getValue()) != year) {
                    getFailMessage().setText(UIMessageUtils.m.const_notInRange(ECoMessageUtils.c.detailSettingsContent_error_eventMustStartInSameYear()));
                    return;
                }
                if (editForm.validate(getFailMessage())) {
                    UpdateDetailedEventSettingsAction udesa = new UpdateDetailedEventSettingsAction();
                    udesa.setCity(cityBox.getText());
                    udesa.setStreet(streetBox.getText());
                    udesa.setPlace(placeBox.getText());
                    udesa.setLocation(locationBox.getSelectedItem().getItem().intValue());
                    udesa.setFrom(DateTimeUtils.toClientDateTime(fromDate.getValue()));
                    udesa.setTo(DateTimeUtils.toClientDateTime(toDate.getValue()));
                    udesa.setTimeZone(timeZoneBox.getSelectedItem().getItem());
                    EventBus.get().fireEvent(new DispatchEvent(udesa, new AsyncCallback<VoidResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            if (caught instanceof ReadOnlyException) {
                                getFailMessage().addOrSetText(ECoMessageUtils.c.const_error_readOnlyProperty());
                            }
                        }

                        @Override
                        public void onSuccess(VoidResult result) {
                            Cache.get().register(CurrentEventTimeZoneCacheAction.CURRENT_EVENT_TIME_ZONE, TimeZoneProvider.getTimeZoneByID(timeZoneBox.getSelectedItem().getItem()));
                            DetailSettingsContent.this.onSuccessUpdate(ECoMessageUtils.c.eventSettings_text_updateSuccess());
                        }
                    }, DetailSettingsContent.this));
                }
            }
        };
    }
}
