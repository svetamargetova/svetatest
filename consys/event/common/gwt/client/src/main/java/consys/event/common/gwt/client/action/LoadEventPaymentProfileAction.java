package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.Action;
import consys.common.gwt.shared.bo.ClientEventPaymentProfile;

/**
 * Akce pro nacteni platebniho profilu
 * @author pepa
 */
public class LoadEventPaymentProfileAction implements Action<ClientEventPaymentProfile> {

    private static final long serialVersionUID = 864172475360168777L;
    // data
    private String uuid;

    public LoadEventPaymentProfileAction() {
    }

    public LoadEventPaymentProfileAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
