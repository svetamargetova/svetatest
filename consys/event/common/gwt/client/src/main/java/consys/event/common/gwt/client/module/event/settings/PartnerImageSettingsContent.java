package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleFormPanel;
import consys.common.gwt.client.ui.utils.*;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.servlet.UploadPartnersImageServletProperties;
import consys.common.gwt.client.ui.event.EventPartnerImageChanged;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 *
 * @author pepa
 */
public class PartnerImageSettingsContent extends RUSimpleFormPanel implements UploadPartnersImageServletProperties {

    private FormPanel formPanel;
    private TYPE type;

    public PartnerImageSettingsContent(TYPE type) {
        super(true);
        this.type = type;
    }

    private static String getLabel(TYPE type) {
        switch (type) {
            case FOOTER:
                return ECoMessageUtils.c.partnerBannerSettings_footerTypeTitle();
            case PANEL:
                return ECoMessageUtils.c.partnerBannerSettings_panelTypeTitle();
        }
        return "";
    }

    @Override
    public Widget readForm() {
        Form baseForm = new Form();

        ActionLabel edit = FormPanelUtils.getEditModeButton(ECoMessageUtils.c.logoSettingsContent_action_changeLogo(), this, "");
        baseForm.addOptional(getLabel(type), edit);
        baseForm.addContent(1, Separator.changedWidth("100px"));
        Image image = new Image(ResourceUtils.system().confLogoBig());

        baseForm.addContent(image);
        Label note = StyleUtils.getStyledLabel(ECoMessageUtils.m.partnerBannerSettings_TypeNote(type.getWidth()), StyleUtils.TEXT_GRAY, StyleUtils.FONT_11PX);
        baseForm.addContent(note);

        try {
            String footerImageUuid = Cache.get().getSafe(type.getPropertyKey());
            if (StringUtils.isNotBlank(footerImageUuid)) {
                image.setUrl(URLFactory.loadImageUrl(footerImageUuid));
                // zmensime lebo je to o hodne sirsie
                if (type.equals(TYPE.FOOTER)) {
                    image.setWidth("500px");
                }
            }
        } catch (NotInCacheException ex) {
        }
        edit.setClickHandler(PartnerImageSettingsContent.this.toEditModeClickHandler(null));

        return baseForm;
    }

    @Override
    public Widget updateForm(String id) {
        clearMessageBox();
        BaseForm baseForm = new BaseForm();

        formPanel = new FormPanel();

        String url = null;
        try {
            url = EventURLFactory.overseerServlet(PATH);
        } catch (NotInCacheException ex) {
        }

        formPanel.setAction(EventURLFactory.joinQueryUrl(url, PARAM_TYPE, type.name()));
        formPanel.setMethod(FormPanel.METHOD_POST);
        formPanel.setStyleName(StyleUtils.MARGIN_BOT_10);
        formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);
        formPanel.addSubmitHandler(new FormPanel.SubmitHandler() {

            @Override
            public void onSubmit(FormPanel.SubmitEvent event) {
                showWaiting(true);
            }
        });
        formPanel.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {

            @Override
            public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) {
                String result = event.getResults();
                // odstraneni pripadnych tagu
                result = result.replaceAll(ServletUtils.TAG_MATCH, "").replaceAll(ServletUtils.TAG_MATCH_CODE, "").trim();
                if (!ServletUtils.containHttpFail(result, getFailMessage())) {
                    Cache.get().register(type.getPropertyKey(), result);
                    EventBus.get().fireEvent(new EventPartnerImageChanged(result, type.name()));
                    PartnerImageSettingsContent.this.onSuccessUpdate(ECoMessageUtils.c.partnerBannerSettings_changeSuccess());
                } else {
                    PartnerImageSettingsContent.this.onFailedUpdate(ECoMessageUtils.c.partnerBannerSettings_changeFailed());
                }
                showWaiting(false);
            }
        });

        FlowPanel panel = FormUtils.sendFileWrapper("imageFile", 270, ECoMessageUtils.m.partnerBannerSettings_TypeNote(type.getWidth()),
                confirmUpdateClickHandler(null), toReadModeClickHandler(null));

        formPanel.setWidget(panel);
        baseForm.addOptional(0, getLabel(type), formPanel);
        return baseForm;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                formPanel.submit();
            }
        };
    }
}
