package consys.event.common.gwt.client.module.payment;

import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import consys.common.gwt.client.ui.comp.HiddenTextBox;
import consys.common.gwt.client.ui.utils.UIMessageUtils;

/**
 * Platebni tlacitko paypal
 * @author pepa
 */
public class PaymentPayPalButton extends Composite {

    // komponenty
    private HiddenTextBox currencyBox;
    private HiddenTextBox currencyCodeBox;
    private HiddenTextBox itemNameBox;
    private HiddenTextBox itemNumberBox;
    private HiddenTextBox amountBox;
    private HiddenTextBox taxRateBox;
    // komponenty - predvyplneni udaju pro platce kartou
    private HiddenTextBox firstNameBox;
    private HiddenTextBox lastNameBox;
    private HiddenTextBox address1Box;
    private HiddenTextBox address2Box;
    private HiddenTextBox cityBox;
    private HiddenTextBox zipBox;
    private HiddenTextBox countryBox;
    private HiddenTextBox emailBox;

    public PaymentPayPalButton(String eventUuid, String userUuid, String invoiceUuid, PaymentPayPalButtonType buttonType) {
        currencyBox = new HiddenTextBox("US", "lc");
        currencyCodeBox = new HiddenTextBox("", "currency_code");
        itemNameBox = new HiddenTextBox("", "item_name");
        itemNumberBox = new HiddenTextBox("", "item_number");
        amountBox = new HiddenTextBox("", "amount");
        taxRateBox = new HiddenTextBox("0.000", "tax_rate");

        firstNameBox = new HiddenTextBox("", "first_name");
        lastNameBox = new HiddenTextBox("", "last_name");
        address1Box = new HiddenTextBox("", "address1");
        address2Box = new HiddenTextBox("", "address2");
        cityBox = new HiddenTextBox("", "city");
        zipBox = new HiddenTextBox("", "zip");
        countryBox = new HiddenTextBox("", "country");
        emailBox = new HiddenTextBox("", "email");

        FlowPanel panel = new FlowPanel();
        panel.add(new HiddenTextBox("_xclick", "cmd"));
        panel.add(new HiddenTextBox("payment@takeplace.eu", "business"));
        panel.add(new HiddenTextBox("services", "button_subtype"));
        panel.add(new HiddenTextBox("0", "no_note"));
        panel.add(new HiddenTextBox("PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest", "bn"));
        panel.add(new HiddenTextBox("0.00", "shipping"));
        panel.add(new HiddenTextBox("1", "no_shipping"));
        panel.add(new HiddenTextBox("Return to Takeplace", "cbt"));
        panel.add(new HiddenTextBox("0", "rm"));
        panel.add(new HiddenTextBox(eventUuid + "#" + userUuid, "custom"));
        panel.add(new HiddenTextBox(invoiceUuid, "invoice"));
        //panel.add(new HiddenTextBox(EventURLFactory.overseerURL("/payment/paypal/ipn"), "notify_url"));
        panel.add(new HiddenTextBox(Window.Location.getHref(), "return"));
        panel.add(new HiddenTextBox("utf-8", "charset"));
        //panel.add(new HiddenTextBox(Window.Location.getHref(), "cancel_ return"));
        panel.add(currencyBox);
        panel.add(currencyCodeBox);
        panel.add(itemNameBox);
        panel.add(itemNumberBox);
        panel.add(amountBox);
        panel.add(taxRateBox);

        panel.add(firstNameBox);
        panel.add(lastNameBox);
        panel.add(address1Box);
        panel.add(address2Box);
        panel.add(cityBox);
        panel.add(zipBox);
        panel.add(countryBox);
        panel.add(emailBox);

        StringBuilder sb = new StringBuilder("img/pay_by_");
        switch (buttonType) {
            case CARD:
                sb.append("card_");
                break;
            case PAYPAL:
            default:
                sb.append("paypal_");
                break;
        }
        sb.append(UIMessageUtils.c.locale());
        sb.append(".png");
        //HTML html = new HTML("<input type=\"image\" src=\"https://www.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif\" border=\"0\" name=\"submit\" alt=\"PayPal - The safer, easier way to pay online!\">");
        HTML html = new HTML("<input type=\"image\" src=\"" + sb.toString() + "\" border=\"0\" name=\"submit\" alt=\"PayPal - The safer, easier way to pay online!\">");
        panel.add(html);

        Image img = new Image("https://www.paypal.com/en_US/i/scr/pixel.gif");
        Element imgElement = img.getElement();
        imgElement.setAttribute("width", "1");
        imgElement.setAttribute("height", "1");
        imgElement.setAttribute("border", "0");
        panel.add(img);

        FormPanel form = new FormPanel("_self");
        form.setAction("https://www.paypal.com/cgi-bin/webscr");
        //form.setAction("https://www.sandbox.paypal.com/cgi-bin/webscr");
        form.setEncoding(FormPanel.ENCODING_URLENCODED);
        form.setMethod(FormPanel.METHOD_POST);

        form.setWidget(panel);
        initWidget(form);
    }

    /** nastavi menu */
    public void setCurrency(String currency) {
        currencyBox.setValue(currency);
    }

    /** nastavi kod meny */
    public void setCurrencyCode(String currencyCode) {
        currencyCodeBox.setValue(currencyCode);
    }

    /** nazev placene polozky */
    public void setItemName(String name) {
        itemNameBox.setValue(name);
    }

    /** id placene polozky */
    public void setItemNumber(String id) {
        itemNumberBox.setValue(id);
    }

    /** suma */
    public void setAmount(String amount) {
        amountBox.setValue(amount);
    }

    /** dan */
    public void setTaxRate(String taxRate) {
        taxRateBox.setValue(taxRate);
    }

    public void setFirstName(String firstName) {
        firstNameBox.setValue(firstName);
    }

    public void setLastName(String lastName) {
        lastNameBox.setValue(lastName);
    }

    public void setAddress1(String address1) {
        address1Box.setValue(address1);
    }

    public void setAddress2(String address2) {
        address2Box.setValue(address2);
    }

    public void setCity(String city) {
        cityBox.setValue(city);
    }

    public void setZip(String zip) {
        zipBox.setValue(zip);
    }

    public void setCountry(String country) {
        countryBox.setValue(country);
    }

    public void setEmail(String email) {
        emailBox.setValue(email);
    }
}
