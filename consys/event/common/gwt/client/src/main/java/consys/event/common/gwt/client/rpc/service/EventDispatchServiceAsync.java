/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package consys.event.common.gwt.client.rpc.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.action.EventAction;

public interface EventDispatchServiceAsync {

    /**
     * Executes the specified action.
     *
     * @param action The action to execute.
     * @param callback The callback to execute once the action completes.
     *
     * @see net.customware.gwt.dispatch.server.Dispatch
     */
    void execute( EventAction<?> action, AsyncCallback<Result> callback );
}
