package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.bo.EventSettings;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akcia ktora stiahne z eventu nastavenia eventu a role uzivatela.
 * @author Palo
 */
public class LoadEventSettingsAction extends EventAction<EventSettings> {

    private static final long serialVersionUID = -4025340046216761221L;
}
