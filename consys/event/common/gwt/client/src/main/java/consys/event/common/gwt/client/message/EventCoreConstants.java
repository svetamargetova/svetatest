package consys.event.common.gwt.client.message;

import com.google.gwt.i18n.client.Constants;

/**
 * abecedne serazene konstanty
 * @author pepa
 */
public interface EventCoreConstants extends Constants {

    String rightsDialog_button_select();

    String baseSettingsContent_action_createSeries();

    String baseSettingsContent_form_eventType();

    String baseSettingsContent_form_fullName();

    String baseSettingsContent_form_internationalName();

    String baseSettingsContent_form_shortName();

    String baseSettingsContent_form_series();

    String baseSettingsContent_title();

    String committee_action_addMember();

    String committee_action_addRight();

    String committee_action_chairChange();

    String committee_action_chairSelect();

    String committee_button_addCommittee();

    String committee_form_committeeChair();

    String committee_form_committeeMembers();

    String committee_form_committeeRights();

    String committee_text_chairMissing();

    String committee_text_committeeName();

    String commonPropertyPanel_property_footerImgSrc();

    String commonPropertyPanel_property_visibleVideos();

    String commonPropertyPanel_property_wallImgSrc();

    String communitySettings_action_showLinkSource();

    String communitySettings_action_showWidgetSource();

    String communitySettings_action_toEditor();

    String communitySettings_action_toHTML();

    String communitySettings_form_profileWebPage();

    String communitySettings_form_registerLink();

    String communitySettings_form_registerWidget();

    String communitySettings_form_tags();

    String communitySettings_helpText1();

    String communitySettings_helpText2();

    String communitySettings_helpTitle();

    String communitySettings_title();

    String communitySettings_text_insertDescription();

    String communitySettings_text_updateSuccess();

    String const_currency();

    String const_date();

    String const_date_from();

    String const_date_to();

    String const_downloadInvoice();

    String const_downloadProFormaInvoice();

    String const_error_currencyNotFoundInCache();

    String const_error_readOnlyProperty();

    String const_invoice();

    String const_preparingEventData();

    String detailSettingsContent_error_eventMustStartInSameYear();

    String detailSettingsContent_text_currentEvent();

    String detailSettingsContent_title();

    String eventCommittees_title();

    String eventCommonPropertyPanel_property_defaultVatRate();

    String eventSettings_text_eventSuccessfullyCreated();

    String eventSettings_text_updateSuccess();

    String eventSettings_title();

    String eventWall_button_register();

    String eventWall_button_sendContribution();

    String eventWall_text_collectingContributions();

    String eventWall_text_importantDates();

    String eventWall_text_participantsRegistration();

    String generalSettings_title();

    String logoSettingsContent_action_changeLogo();

    String logoSettingsContent_error_updateLogoFailed();

    String logoSettingsContent_form_logoPicture();

    String logoSettingsContent_form_logoProfilePicture();

    String logoSettingsContent_text_logoMissing();

    String logoSettingsContent_text_sizeInfo();

    String logoSettingsContent_text_updateLogoSuccess();

    String overseerModule_text_administration();

    String overseerModule_text_moduleRightInsertWallMessage();

    String overseerModule_text_moduleRightInsertWallMessageDescription();

    String overseerModule_text_moduleRightInsertWallMessageAsEvent();

    String overseerModule_text_moduleRightInsertWallMessageAsEventDescription();

    String overseerModule_text_moduleRightRemoveWallMessage();

    String overseerModule_text_moduleRightRemoveWallMessageDescription();

    String overseerModule_text_moduleRightTransfers();

    String overseerModule_text_moduleRightTransfersDescription();

    String overseerModule_text_payments();

    String overseerModule_text_overseerModuleName();

    String overseerModule_text_roleAdministratorName();

    String overseerModule_text_roleAdministratorDescription();

    String overseerModule_text_video();

    String partnerBannerSettings_title();

    String partnerBannerSettings_changeImage();

    String partnerBannerSettings_changeSuccess();

    String partnerBannerSettings_changeFailed();

    String partnerBannerSettings_footerTypeTitle();

    String partnerBannerSettings_panelTypeTitle();

    String paymentDialog_button_ok();

    String paymentDialog_text_name();

    String paymentDialog_text_quantity();

    String paymentDialog_text_total();

    String paymentDialog_text_unitPrice();

    String paymentsContent_title();

    String paymentsLicencesContent_button_activateEvent();

    String paymentsLicencesContent_form_agreements();

    String paymentsLicencesContent_helpText();

    String paymentsLicencesContent_text_agree1();

    String paymentsLicencesContent_text_agree2();

    String paymentsLicencesContent_text_agree3();

    String paymentsLicencesContent_title();

    String paymentsProfileContent_form_invoiceNote();

    String paymentProfileContent_form_noVatPayer();

    String paymentsProfileContent_form_organizationName();

    String paymentsProfileContent_helpText1();

    String paymentsProfileContent_helpText2();

    String paymentsProfileContent_helpText3();

    String paymentsProfileContent_title();

    String paymentsPropertyPanel_error_invalidVat();

    String paymentTransfers_action_showDetail();

    String paymentTransfers_help_activeTransfer();

    String paymentTransfers_help_errorTransfer();

    String paymentTransfers_help_preparedTransfer();

    String paymentTransfers_help_showDetails();

    String paymentTransfers_help_transferedTransfer();

    String paymentTransfers_text_date();

    String paymentTransfers_text_fees();

    String paymentTransfers_text_fittage();

    String paymentTransfers_text_information();

    String paymentTransfers_text_total();

    String paymentTransfers_text_transfer();

    String paymentTransfers_title();

    String paymentTransfersDetail_form_account();

    String paymentTransfersDetail_form_date();

    String paymentTransfersDetail_form_sum();

    String paymentTransfersDetail_form_sumOfFees();

    String paymentTransfersDetail_form_total();

    String profileLogoSettingsContent_form_logoPicture();

    String profileLogoSettingsContent_text_sizeInfo();

    String rightsDialog_helpText();

    String rightsDialog_helpTitle();

    String videoFilesListItem_text_error();

    String videoFilesListItem_text_processing();

    String videoFilesListItem_text_readyToProcessing();

    String videoFilesSettings_action_addVideo();

    String videoFilesSettings_action_startProcessing();

    String videoFilesSettings_error_unsupportedBrowser();

    String videoFilesSettings_state_error();

    String videoFilesSettings_state_processed();

    String videoFilesSettings_state_processing();

    String videoFilesSettings_state_uploading();

    String videoFilesSettings_text_all();

    String videoFilesSettings_text_authors();

    String videoFilesSettings_text_maxSize();

    String videoFilesSettings_text_separateNamesByCommas();

    String videoFilesSettings_text_speakers();

    String videoFilesSettings_text_startProcessing();

    String videoFilesSettings_text_videoName();

    String videoFilesSettings_text_uploading();

    String videoFilesSettings_title();

    String videos_title();
}
