package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.EventUser;
import java.util.ArrayList;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ClientCommittee implements Result {

    private static final long serialVersionUID = -6706380429593580001L;
    private Long id;
    private String name;
    private EventUser chair;
    /** Prava ktore ma cely vybor. Identifikovane podla identifieru */
    private ArrayList<String> chairRights;
    /** Clenovia vyboru identifikovbany podla UUID */
    private ArrayList<EventUser> memebers;
    /** Prava ktore ma cely vybor. Identifikovane podla identifieru */
    private ArrayList<String> rights;

    public ClientCommittee() {
        chairRights = new ArrayList<String>();
        memebers = new ArrayList<EventUser>();
        rights = new ArrayList<String>();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the chairUuid
     */
    public EventUser getChair() {
        return chair;
    }

    /**
     * @param chair the chairUuid to set
     */
    public void setChair(EventUser chair) {
        this.chair = chair;
    }

    /**
     * @return the memebers
     */
    public ArrayList<EventUser> getMemebers() {
        return memebers;
    }

    /**
     * @param memebers the memebers to set
     */
    public void setMemebers(ArrayList<EventUser> memebers) {
        this.memebers = memebers;
    }

    /**
     * @return the rights
     */
    public ArrayList<String> getRights() {
        return rights;
    }

    /**
     * @param rights the rights to set
     */
    public void setRights(ArrayList<String> rights) {
        this.rights = rights;
    }

    /**
     * @return the chairRights
     */
    public ArrayList<String> getChairRights() {
        return chairRights;
    }

    /**
     * @param chairRights the chairRights to set
     */
    public void setChairRights(ArrayList<String> chairRights) {
        this.chairRights = chairRights;
    }
}
