package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.common.api.servlet.UploadPartnersImageServletProperties;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 *
 * @author palo
 */
public class PartnerAndBannerSettings extends SimpleFormPanel implements MenuFormItem {

    @Override
    public String getName() {
        return ECoMessageUtils.c.partnerBannerSettings_title();
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        clear();
        this.addWidget(new LogoSettingsContent());
        this.addWidget(Separator.addedStyle(StyleUtils.MARGIN_BOT_10));
        this.addWidget(new ProfileLogoSettingsContent());
        this.addWidget(Separator.addedStyle(StyleUtils.MARGIN_BOT_10));
        this.addWidget(new PartnerImageSettingsContent(UploadPartnersImageServletProperties.TYPE.FOOTER));
        this.addWidget(Separator.addedStyle(StyleUtils.MARGIN_BOT_10));
        this.addWidget(new PartnerImageSettingsContent(UploadPartnersImageServletProperties.TYPE.PANEL));
        panel.setWidget(this);
    }
}
