package consys.event.common.gwt.client.comp;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;

/**
 *
 * @author pepa
 */
public class Preparing extends Label {

    // data
    private String text;
    private Timer timer;
    private int counter;

    public Preparing(String text) {
        super(text);
        this.text = text;
    }

    @Override
    protected void onLoad() {
        if (timer == null) {
            timer = new Timer() {
                @Override
                public void run() {
                    switch (counter) {
                        case 0:
                            setText(text + " .");
                            counter = 1;
                            break;
                        case 1:
                            setText(text + " ..");
                            counter = 2;
                            break;
                        case 2:
                            setText(text + " ...");
                            counter = 3;
                            break;
                        default:
                            setText(text);
                            counter = 0;
                            break;
                    }
                }
            };
        }
        timer.scheduleRepeating(1000);
    }

    @Override
    protected void onUnload() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
}
