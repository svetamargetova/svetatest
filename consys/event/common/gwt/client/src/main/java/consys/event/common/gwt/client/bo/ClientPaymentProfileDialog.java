package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.gwt.shared.bo.ClientPaymentProfileThumb;
import java.util.ArrayList;

/**
 * Data vysledku akce ListPaymentProfileDialogAction
 * @author pepa
 */
public class ClientPaymentProfileDialog implements Result {

    private static final long serialVersionUID = 7951926710666153168L;
    // data
    private ArrayList<ClientPaymentProfileThumb> profileThumbs;
    private ClientUserPaymentProfile firstProfile;

    public ClientPaymentProfileDialog() {
        profileThumbs = new ArrayList<ClientPaymentProfileThumb>();
    }

    public ClientUserPaymentProfile getFirstProfile() {
        return firstProfile;
    }

    public void setFirstProfile(ClientUserPaymentProfile firstProfile) {
        this.firstProfile = firstProfile;
    }

    public ArrayList<ClientPaymentProfileThumb> getProfileThumbs() {
        return profileThumbs;
    }

    public void setProfileThumbs(ArrayList<ClientPaymentProfileThumb> profileThumbs) {
        this.profileThumbs = profileThumbs;
    }
}
