package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.URLFactory;
import consys.common.gwt.client.event.EventNameChangedEvent;
import consys.common.gwt.client.rpc.action.CloneEventAction;
import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.ConsysIntTextBox;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.DateTimeUtils;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.event.common.gwt.client.action.LoadEventDetailsAction;
import consys.event.common.gwt.client.action.UpdateBaseEventSettingsAction;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;
import java.util.Date;

/**
 * Event Naming
 * Formular pro zakladni editaci nazvu eventu, rocniku a serie
 * @author pepa
 */
public class BaseSettingsContent extends RUSimpleHelpFormPanel {

    // Komponenty editacneho formulara
    ConsysStringTextBox acronymBox;
    ConsysIntTextBox yearBox;
    ConsysStringTextBox fullNameBox;
    ConsysStringTextBox universalNameBox;
    ConsysStringTextBox shortNameBox;
    BaseForm editForm;
    // data
    private String clonedEventUuid;
    private String series;

    public BaseSettingsContent() {
        super(true);
        //addHelpTitle("HELP TITLE");
        //addHelpText("Text by mal obsahovat informacie o tom ze ide o identifikaciu eventu podla nazvu. Short Name je defaultne generovane na zaklade \"ACRONYM ROK\". Medzinarodne meno ma vyssiu prioritu zobrazenia sa ako full name");
    }

    @Override
    public Widget readForm() {
        final Label acronymLabel = new Label();
        final Label typeLabel = new Label();
        final Label yearLabel = new Label();
        final Label seriesLabel = new Label();
        final Label fullNameLabel = new Label();
        final Label universalNameLabel = new Label();
        final Label shortName = new Label();
        Label separator = new Label("");
        separator.setSize("100px", "10px");

        BaseForm viewForm = new BaseForm();
        viewForm.setStyleName(StyleUtils.MARGIN_BOT_10);
        viewForm.addRequired(0, UIMessageUtils.c.const_acronym(), acronymLabel);
        viewForm.addRequired(1, ECoMessageUtils.c.baseSettingsContent_form_fullName(), fullNameLabel);
        viewForm.addRequired(2, ECoMessageUtils.c.baseSettingsContent_form_eventType(), typeLabel);
        viewForm.addRequired(3, UIMessageUtils.c.const_date_year(), yearLabel);
        viewForm.addContent(4, separator);
        viewForm.addOptional(5, ECoMessageUtils.c.baseSettingsContent_form_internationalName(), universalNameLabel);
        viewForm.addOptional(6, ECoMessageUtils.c.baseSettingsContent_form_shortName(), shortName);
        viewForm.addOptional(7, ECoMessageUtils.c.baseSettingsContent_form_series(), seriesLabel);

        EventBus.get().fireEvent(new DispatchEvent(new LoadEventDetailsAction(), new AsyncCallback<ClientEvent>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ClientEvent e) {
                series = seriesValue(e.getSeries());
                
                typeLabel.setText("Conference");
                acronymLabel.setText(e.getAcronym());
                seriesLabel.setText(series);
                fullNameLabel.setText(e.getFullName());
                universalNameLabel.setText(e.getUniversalName());
                yearLabel.setText(e.getYear() + "");
                shortName.setText(e.getShortName());
            }
        }, this));


        FlowPanel panel = new FlowPanel();
        if (!StringUtils.isBlank(clonedEventUuid)) {
            String url = URLFactory.eventUrl(clonedEventUuid);
            ConsysMessage cloneMessage = ConsysMessage.getInfo();
            cloneMessage.setText(ECoMessageUtils.m.baseSettingsContent_text_infoCloned(url), true);
            cloneMessage.addStyleName(StyleUtils.MARGIN_BOT_10);

            panel.add(cloneMessage);
            clonedEventUuid = null;
        }
        panel.add(FormPanelUtils.getOneLinedReadMode(this, ECoMessageUtils.c.baseSettingsContent_title(), UIMessageUtils.c.const_edit()));
        panel.add(viewForm);
        return panel;
    }

    @Override
    public Widget updateForm(String id) {
        editForm = new BaseForm();

        Date now = new Date();
        int year = DateTimeUtils.getYear(now);

        acronymBox = new ConsysStringTextBox(1, 128, UIMessageUtils.c.const_acronym());
        yearBox = new ConsysIntTextBox("60px", year, year + 50, UIMessageUtils.c.const_date_year());
        fullNameBox = new ConsysStringTextBox(1, 256, ECoMessageUtils.c.baseSettingsContent_form_fullName());
        universalNameBox = new ConsysStringTextBox(0, 256, ECoMessageUtils.c.baseSettingsContent_form_internationalName());
        shortNameBox = new ConsysStringTextBox(1, 256, ECoMessageUtils.c.baseSettingsContent_form_shortName());
        final Label typeLabel = new Label();
        Label separator = new Label("");
        separator.setSize("100px", "10px");

        editForm.addRequired(0, UIMessageUtils.c.const_acronym(), acronymBox);
        editForm.addRequired(1, ECoMessageUtils.c.baseSettingsContent_form_fullName(), fullNameBox);
        editForm.addRequired(2, ECoMessageUtils.c.baseSettingsContent_form_eventType(), typeLabel);
        editForm.addRequired(3, UIMessageUtils.c.const_date_year(), yearBox);
        editForm.addContent(4, separator);
        editForm.addOptional(5, ECoMessageUtils.c.baseSettingsContent_form_internationalName(), universalNameBox);
        editForm.addOptional(6, ECoMessageUtils.c.baseSettingsContent_form_shortName(), shortNameBox);
        editForm.addActionMembers(8, FormPanelUtils.getUpdateButton(this, ""), FormPanelUtils.getCancelButton(this));

        EventBus.get().fireEvent(new DispatchEvent(new LoadEventDetailsAction(), new AsyncCallback<ClientEvent>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ClientEvent e) {
                series = seriesValue(e.getSeries());
                
                acronymBox.setText(e.getAcronym());
                yearBox.setText(String.valueOf(e.getYear()));
                fullNameBox.setText(e.getFullName());
                universalNameBox.setText(e.getUniversalName());
                shortNameBox.setText(e.getShortName());
                typeLabel.setText("Conference");

                // serie + klonovaci tlacitko
                final Label seriesLabel = e.isVisible()
                        ? StyleUtils.getStyledLabel("", ECoResourceUtils.bundle().css().seriesLabel())
                        : new Label();

                consys.common.gwt.client.ui.comp.action.ActionImage redClone =
                        consys.common.gwt.client.ui.comp.action.ActionImage.delete(ECoMessageUtils.c.baseSettingsContent_action_createSeries());
                redClone.addStyleName(ECoResourceUtils.bundle().css().redClone());
                redClone.setClickHandler(cloneClickHandler());

                FlowPanel sPanel = new FlowPanel();
                sPanel.add(seriesLabel);
                if (e.isVisible()) {
                    sPanel.add(redClone);
                }
                sPanel.add(StyleUtils.clearDiv());
                seriesLabel.setText(series);
                editForm.addOptional(7, ECoMessageUtils.c.baseSettingsContent_form_series(), sPanel);
            }
        }, this));

        FlowPanel out = new FlowPanel();
        out.add(FormPanelUtils.getOneLineEditMode(ECoMessageUtils.c.baseSettingsContent_title()));
        out.add(editForm);
        return out;
    }

    private String seriesValue(String s) {
        return StringUtils.isNotBlank(s) ? s : "1";
    }
    
    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                // Validacia
                if (editForm.validate(getFailMessage())) {
                    final UpdateBaseEventSettingsAction ubesa = new UpdateBaseEventSettingsAction();
                    ubesa.setAcronym(acronymBox.getText());
                    ubesa.setFullName(fullNameBox.getText());
                    ubesa.setSeries(series); // serie je nastavovana automaticky systemem
                    ubesa.setShortName(shortNameBox.getText());
                    ubesa.setUniversalName(universalNameBox.getText());
                    ubesa.setYear(yearBox.getTextInt());
                    EventBus.get().fireEvent(new DispatchEvent(ubesa, new AsyncCallback<VoidResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            // nemeli by nastat zadne ine veci
                        }

                        @Override
                        public void onSuccess(VoidResult result) {
                            BaseSettingsContent.this.onSuccessUpdate(ECoMessageUtils.c.eventSettings_text_updateSuccess());
                            EventBus.get().fireEvent(new EventNameChangedEvent(ubesa.getAcronym(), ubesa.getYear()));
                        }
                    }, BaseSettingsContent.this));
                }
            }
        };
    }

    private ClickHandler cloneClickHandler() {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                EventBus.get().fireEvent(new DispatchEvent(new LoadEventDetailsAction(), new AsyncCallback<ClientEvent>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                    }

                    @Override
                    public void onSuccess(ClientEvent e) {
                        fireClone(e.getUuid());
                    }
                }, BaseSettingsContent.this));
            }
        };
    }

    /** zavola vytvoreni klonu eventu */
    private void fireClone(String eventUuid) {
        EventBus.fire(new DispatchEvent(new CloneEventAction(eventUuid),
                new AsyncCallback<StringResult>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        // obecne chyby zpracovava action executor
                    }

                    @Override
                    public void onSuccess(StringResult result) {
                        clonedEventUuid = result.getStringResult();
                        setWidget(readForm());
                    }
                }, BaseSettingsContent.this));
    }
}
