package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.common.gwt.client.bo.ClientPaymentTransferDetail;

/**
 * Akce pro nacteni detailu transferu
 * @author pepa
 */
public class LoadEventPaymentTransferDetailAction extends EventAction<ClientPaymentTransferDetail> {

    private static final long serialVersionUID = 5803937739414090056L;
    // data
    private String uuid;

    public LoadEventPaymentTransferDetailAction() {
    }

    public LoadEventPaymentTransferDetailAction(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

   
}
