package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.ClientEventPaymentProfile;

/**
 *
 * @author pepa
 */
public class UpdateEventPaymentProfileAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 7691928364277210992L;
    // data
    private ClientEventPaymentProfile profile;    

    public UpdateEventPaymentProfileAction() {
    }

    public UpdateEventPaymentProfileAction(ClientEventPaymentProfile profile) {
        this.profile = profile;
    }

    public ClientEventPaymentProfile getProfile() {
        return profile;
    }
}
