package consys.event.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import consys.common.gwt.client.ui.comp.ActionImage;

/**
 * Akce pro zjisteni jestli je uzivatel zaregistrovan
 * @author pepa
 */
public class CheckUserIsRegistered extends GwtEvent<CheckUserIsRegistered.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<CheckUserIsRegistered.Handler> TYPE = new GwtEvent.Type<CheckUserIsRegistered.Handler>();
    // data
    private boolean inRange;
    private ActionImage button;

    public CheckUserIsRegistered(boolean inRange, ActionImage button) {
        this.inRange = inRange;
        this.button = button;
    }

    public ActionImage getButton() {
        return button;
    }

    public boolean isInRange() {
        return inRange;
    }

    @Override
    public GwtEvent.Type<CheckUserIsRegistered.Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CheckUserIsRegistered.Handler handler) {
        handler.isUserRegistered(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void isUserRegistered(CheckUserIsRegistered event);
    }
}
