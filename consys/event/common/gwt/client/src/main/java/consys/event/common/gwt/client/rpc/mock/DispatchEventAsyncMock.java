package consys.event.common.gwt.client.rpc.mock;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import consys.common.gwt.client.logger.Logger;
import consys.common.gwt.client.logger.LoggerFactory;
import consys.common.gwt.client.rpc.mock.ActionMock;
import consys.common.gwt.client.rpc.mock.MockFactory;
import consys.common.gwt.shared.action.Result;
import consys.event.common.gwt.client.rpc.AbstractEventDispatchAsync;
import consys.common.gwt.shared.action.EventAction;


/**
 *
 * @author Palo
 */
public class DispatchEventAsyncMock extends AbstractEventDispatchAsync {

    Logger logger = LoggerFactory.getLogger(DispatchEventAsyncMock.class);
    public DispatchEventAsyncMock() {
        super(); 
        
    }


    @Override
    public <A extends EventAction<R>, R extends Result> void execute(final A action, final AsyncCallback<R> callback) {
        final ActionMock mock = MockFactory.get(action);
        if (mock != null) {

            Timer t = new Timer() {

                @Override
                public void run() {
                    mock.doAction(new AsyncCallback<R>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            logger.info(action.getClass()+": Failure");
                            DispatchEventAsyncMock.this.onFailure(action, caught, callback);
                        }

                        @Override
                        public void onSuccess(R result) {
                            logger.info(action.getClass()+": Success");
                            DispatchEventAsyncMock.this.onSuccess(action, (R) result, callback);
                        }
                    });
                }
            };
            t.schedule(500);

        } else {
            GWT.log("No Mock for Action " + action.getClass().getName());
            callback.onFailure(new NullPointerException("No Mock"));
        }
    }
}

