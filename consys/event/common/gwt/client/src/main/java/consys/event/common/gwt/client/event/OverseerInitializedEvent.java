package consys.event.common.gwt.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 *
 * @author Palo
 */
public class OverseerInitializedEvent extends GwtEvent<OverseerInitializedEvent.Handler> {

    /** typ eventu */
    public static final GwtEvent.Type<Handler> TYPE = new GwtEvent.Type<Handler>();

    @Override
    public Type<Handler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(Handler handler) {
        handler.onInitialize(this);
    }

    /** interface eventu */
    public interface Handler extends EventHandler {

        public void onInitialize(OverseerInitializedEvent event);
    }
}
