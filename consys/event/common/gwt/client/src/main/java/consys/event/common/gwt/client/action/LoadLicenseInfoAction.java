package consys.event.common.gwt.client.action;

import consys.common.gwt.shared.action.EventAction;
import consys.event.common.gwt.client.bo.ClientEventLicense;

/**
 * Akce pro nacteni informaci o licenci eventu
 * @author pepa
 */
public class LoadLicenseInfoAction extends EventAction<ClientEventLicense> {

    private static final long serialVersionUID = -4283396865182819501L;
}
