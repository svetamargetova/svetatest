package consys.event.common.gwt.client.bo.list;

/**
 *
 * @author pepa
 */
public enum VideoRecordState {

    UPLOADING, UPLOADED, PROCESSED, PROCESS_ERROR, PROCESSING;

    public static VideoRecordState getStateById(int id) {
        switch (id) {
            case 0:
                return UPLOADING;
            case 1:
                return UPLOADED;
            case 3:
                return PROCESSED;
            case 4:
                return PROCESS_ERROR;
            case 2:
            case 5:
            case 6:
                return PROCESSING;
            default:
                throw new IllegalArgumentException();
        }
    }
}
