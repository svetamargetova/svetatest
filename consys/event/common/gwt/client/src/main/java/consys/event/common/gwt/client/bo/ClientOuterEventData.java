/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.bo.ClientEventThumb;

/**
 *
 * @author palo
 */
public class ClientOuterEventData extends ClientEventThumb{
    
    private boolean active;

    public ClientOuterEventData() {
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    
    
    
    
}
