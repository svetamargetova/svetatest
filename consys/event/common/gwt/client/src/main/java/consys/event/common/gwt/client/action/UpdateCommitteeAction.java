package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.EventAction;
import consys.common.gwt.shared.bo.InvitedUser;
import java.util.ArrayList;

/**
 * Akce pro aktualizaci vyboru
 *
 * 
 *
 * @author pepa
 */
public class UpdateCommitteeAction extends EventAction<VoidResult> {

    private static final long serialVersionUID = 7546103655812584789L;
    // data
    private Long id;
    private InvitedUser chair;
    private String committeeName;
    private ArrayList<String> newChairRights;
    private ArrayList<String> removeChairRights;
    private ArrayList<InvitedUser> newMembers;
    private ArrayList<String> removeMembers;
    private ArrayList<String> newMembersRights;
    private ArrayList<String> removeMembersRights;

    public InvitedUser getChair() {
        return chair;
    }

    public void setChair(InvitedUser chair) {
        this.chair = chair;
    }

    public String getCommitteeName() {
        return committeeName;
    }

    public void setCommitteeName(String committeeName) {
        this.committeeName = committeeName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ArrayList<String> getNewChairRights() {
        return newChairRights;
    }

    public void setNewChairRights(ArrayList<String> newChairRights) {
        this.newChairRights = newChairRights;
    }

    public ArrayList<InvitedUser> getNewMembers() {
        return newMembers;
    }

    public void setNewMembers(ArrayList<InvitedUser> newMembers) {
        this.newMembers = newMembers;
    }

    public ArrayList<String> getNewMembersRights() {
        return newMembersRights;
    }

    public void setNewMembersRights(ArrayList<String> newMembersRights) {
        this.newMembersRights = newMembersRights;
    }

    public ArrayList<String> getRemoveChairRights() {
        return removeChairRights;
    }

    public void setRemoveChairRights(ArrayList<String> removeChairRights) {
        this.removeChairRights = removeChairRights;
    }

    public ArrayList<String> getRemoveMembers() {
        return removeMembers;
    }

    public void setRemoveMembers(ArrayList<String> removeMembers) {
        this.removeMembers = removeMembers;
    }

    public ArrayList<String> getRemoveMembersRights() {
        return removeMembersRights;
    }

    public void setRemoveMembersRights(ArrayList<String> removeMembersRights) {
        this.removeMembersRights = removeMembersRights;
    }
}
