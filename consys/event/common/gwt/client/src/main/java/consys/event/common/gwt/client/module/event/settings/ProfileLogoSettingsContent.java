package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.ServletConstants;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.BaseForm;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.event.DispatchEvent;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.client.ui.utils.ServletUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientEvent;
import consys.common.constants.img.EventProfileImageEnum;
import consys.event.common.gwt.client.action.LoadEventDetailsAction;
import consys.event.common.gwt.client.action.UpdateEventLogoAction;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProfileLogoSettingsContent extends RUSimpleHelpFormPanel {

    FormPanel formPanel;
    String eventUuid;

    public ProfileLogoSettingsContent() {
        super(true);
        //addHelpTitle("HELP TITLE");
        //addHelpText("Horizontalne loga su urcene na prezentaciu eventu v zoznamoch. Logo sa automaticky rozgenerovava na potrebne orabzky. Casom musime pridat podporu na stvorcove obrazky. Mozeme uvazovat ze sa budu moct menit aj jednotlive loga.");
    }

    @Override
    public Widget readForm() {
        final ActionLabel edit = FormPanelUtils.getEditModeButton(ECoMessageUtils.c.logoSettingsContent_action_changeLogo(), this, "");

        Label separator = new Label("");
        separator.setSize("100px", "10px");

        final Image bigLogo = new Image(ResourceUtils.system().confProfileLogoBig());

        // Ak sa zisti ze user event nema nastavene logo tak ukazat I message ze by si ho mel nastavit 
        EventBus.get().fireEvent(new DispatchEvent(new LoadEventDetailsAction(), new AsyncCallback<ClientEvent>() {

            @Override
            public void onFailure(Throwable caught) {
                throw new UnsupportedOperationException("Not supported yet.");
            }

            @Override
            public void onSuccess(ClientEvent result) {
                edit.setClickHandler(ProfileLogoSettingsContent.this.toEditModeClickHandler(result.getUuid()));
                String uuid = result.getProfileLogoUuidPrefix();
                eventUuid = result.getUuid();
                if (StringUtils.isNotBlank(uuid)) {
                    String url = GWT.getModuleBaseURL() + ServletConstants.EVENT_PROFILE_IMG_LOAD + EventProfileImageEnum.WEB.getFullUuid(uuid);
                    bigLogo.setUrl(url);
                } else {
                    ProfileLogoSettingsContent.this.clearMessageBox();
                    ProfileLogoSettingsContent.this.getSuccessMessage().setText(ECoMessageUtils.c.logoSettingsContent_text_logoMissing());
                }
            }
        }, this));

        Label note = StyleUtils.getStyledLabel(ECoMessageUtils.c.profileLogoSettingsContent_text_sizeInfo(), StyleUtils.TEXT_GRAY, StyleUtils.FONT_11PX);

        Form baseForm = new Form();
        baseForm.addOptional(ECoMessageUtils.c.logoSettingsContent_form_logoProfilePicture(), edit);
        baseForm.addContent(1, separator);
        baseForm.addContent(bigLogo);
        baseForm.addContent(note);
        return baseForm;
    }

    @Override
    public Widget updateForm(String id) {
        ProfileLogoSettingsContent.this.clearMessageBox();
        BaseForm baseForm = new BaseForm();

        formPanel = new FormPanel();
        formPanel.setAction(ServletConstants.BASE_URL + ServletConstants.EVENT_PROFILE_IMG_SAVE + eventUuid);
        formPanel.setMethod(FormPanel.METHOD_POST);
        formPanel.setStyleName(StyleUtils.MARGIN_BOT_10);
        formPanel.setEncoding(FormPanel.ENCODING_MULTIPART);
        formPanel.addSubmitHandler(new SubmitHandler() {

            @Override
            public void onSubmit(SubmitEvent event) {
                showWaiting(true);
            }
        });
        formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {

            @Override
            public void onSubmitComplete(SubmitCompleteEvent event) {
                String result = event.getResults();
                // odstraneni pripadnych tagu
                result = result.replaceAll(ServletUtils.TAG_MATCH, "").replaceAll(ServletUtils.TAG_MATCH_CODE, "").trim();
                if (!ServletUtils.containHttpFail(result, getFailMessage())) {
                    EventBus.get().fireEvent(new DispatchEvent(new UpdateEventLogoAction(result, true), new AsyncCallback<VoidResult>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            throw new UnsupportedOperationException("Not supported yet.");
                        }

                        @Override
                        public void onSuccess(VoidResult result) {
                            ProfileLogoSettingsContent.this.onSuccessUpdate(ECoMessageUtils.c.logoSettingsContent_text_updateLogoSuccess());
                        }
                    }, ProfileLogoSettingsContent.this));
                } else {
                    ProfileLogoSettingsContent.this.onFailedUpdate(ECoMessageUtils.c.logoSettingsContent_error_updateLogoFailed());
                }
                showWaiting(false);
            }
        });

        FlowPanel panel = FormUtils.sendFileWrapper("imageFile", 270, ECoMessageUtils.c.profileLogoSettingsContent_text_sizeInfo(),
                confirmUpdateClickHandler(null), toReadModeClickHandler(null));

        formPanel.setWidget(panel);
        baseForm.addOptional(0, ECoMessageUtils.c.profileLogoSettingsContent_form_logoPicture(), formPanel);
        return baseForm;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                formPanel.submit();
            }
        };
    }
}
