package consys.event.common.gwt.client.module.event.video;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.EventURLFactory;
import consys.common.gwt.client.action.CommonExceptionEnum;
import consys.common.gwt.client.action.ConsysAction;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.action.cache.UserCacheAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.LayoutManager;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysMessage;
import consys.common.gwt.client.ui.comp.ConsysTextArea;
import consys.common.gwt.client.ui.comp.EditWithRemover;
import consys.common.gwt.client.ui.comp.HelpTextBox;
import consys.common.gwt.client.ui.comp.list.ListItem;
import consys.common.gwt.client.ui.comp.upload.UploadingForm;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.client.utils.StringUtils;
import consys.common.gwt.shared.bo.ClientUser;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.gwt.client.action.DeleteEventVideoAction;
import consys.event.common.gwt.client.action.UpdateEventVideoAction;
import consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem;
import consys.event.common.gwt.client.bo.list.VideoRecordState;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import consys.event.common.gwt.client.utils.ECoResourceUtils;

/**
 *
 * @author pepa
 */
public class VideoFilesListItem extends ListItem<ClientVideoFilesListItem> {

    // komponenty
    private FlowPanel panel;
    private Timer timer;
    private Image image;
    private Label infoLabel;
    // data
    private VideoFilesSettings parent;
    private ClientVideoFilesListItem item;

    public VideoFilesListItem(ClientVideoFilesListItem value, VideoFilesSettings parent) {
        super(value);
        this.parent = parent;
    }

    @Override
    protected void onUnload() {
        super.onUnload();
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    protected void createCell(ClientVideoFilesListItem item, FlowPanel panel) {
        this.panel = panel;
        this.item = item;

        switch (item.getState()) {
            case UPLOADING:
                showUploading();
            case UPLOADED:
                if (item.isProcessing()) {
                    showItem();
                } else {
                    showUploading();
                }
                break;
            default:
                showItem();
                break;
        }
    }

    /** zobrazi informace o videu */
    private void showItem() {
        panel.clear();

        EditWithRemover ewr = new EditWithRemover(new ConsysAction() {
            @Override
            public void run() {
                showEdit();
            }
        }, removeAction());
        ewr.addStyleName(ECoResourceUtils.bundle().css().videoEdit());

        panel.add(ewr);
        createShowItem(panel, item);
    }

    /** vygeneruje prohlizeci polozku seznamu */
    public static void createShowItem(FlowPanel fp, ClientVideoFilesListItem item) {
        FlowPanel p1 = leftPanel();
        p1.add(StyleUtils.getStyledLabel(item.getCustomName(), ECoResourceUtils.bundle().css().videoLabel()));
        p1.add(StyleUtils.getStyledLabel(item.getDescription(), StyleUtils.MARGIN_TOP_10));

        fp.add(StyleUtils.clearDiv());
        fp.add(p1);
        fp.add(rightPanel(generateInfoLabel(item), item.getState().equals(VideoRecordState.PROCESSED) ? item.getUrl() : null));
        fp.add(StyleUtils.clearDiv());
    }

    private ConsysAction removeAction() {
        return new ConsysAction() {
            @Override
            public void run() {
                EventBus.fire(new EventDispatchEvent(new DeleteEventVideoAction(item.getSystemName()),
                        new AsyncCallback<VoidResult>() {
                            @Override
                            public void onFailure(Throwable caught) {
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                parent.refresh();
                            }
                        }, null));
            }
        };
    }

    /** zobrazi editacni formular videa */
    private void showEdit() {
        panel.clear();

        final HelpTextBox nameBox = nameBox(item.getCustomName());
        final ConsysTextArea descriptionBox = descriptionBox(item.getDescription());
        final HelpTextBox speakerBox = speakerBox(item.getSpeakers());
        final HelpTextBox authorBox = authorBox(item.getAuthors());

        FlowPanel p1 = leftPanel();
        p1.add(nameBox);
        p1.add(descriptionBox);
        p1.add(wrapPersons(speakerBox));
        p1.add(wrapPersons(authorBox));
        p1.add(control(nameBox, descriptionBox, speakerBox, authorBox, true));

        panel.add(p1);
        panel.add(rightPanel(generateInfoLabel(item), item.getState().equals(VideoRecordState.PROCESSED) ? item.getUrl() : null));
        panel.add(StyleUtils.clearDiv());
    }

    /** vytvori infoLabel v zavislosti na stavu */
    private static Label generateInfoLabel(ClientVideoFilesListItem item) {
        Label label = new Label();
        label.setStyleName(ECoResourceUtils.bundle().css().videoPercentLabel());
        switch (item.getState()) {
            case PROCESSED:
                return null;
            case PROCESSING:
                label.setText(ECoMessageUtils.c.videoFilesListItem_text_processing());
                label.addStyleName(StyleUtils.TEXT_GRAY);
                break;
            case PROCESS_ERROR:
                label.setText(ECoMessageUtils.c.videoFilesListItem_text_error());
                label.addStyleName(StyleUtils.TEXT_RED);
                break;
            case UPLOADED:
                label.setText(item.isProcessing()
                        ? ECoMessageUtils.c.videoFilesListItem_text_processing()
                        : ECoMessageUtils.c.videoFilesListItem_text_readyToProcessing());
                label.addStyleName(StyleUtils.TEXT_GRAY);
                break;
            case UPLOADING:
                break;
        }
        return label;
    }

    /** zobrazi uploadovaci formular */
    private void showUploading() {
        panel.clear();

        HelpTextBox nameBox = nameBox(item.getCustomName());
        ConsysTextArea descriptionBox = descriptionBox("");
        HelpTextBox speakerBox = speakerBox("");
        HelpTextBox authorBox = authorBox("");

        FlowPanel p1 = leftPanel();
        p1.add(nameBox);
        p1.add(descriptionBox);
        p1.add(wrapPersons(speakerBox));
        p1.add(wrapPersons(authorBox));
        p1.add(control(nameBox, descriptionBox, speakerBox, authorBox, false));

        boolean uploading = VideoRecordState.UPLOADING.equals(item.getState());

        infoLabel = generateInfoLabel(item);

        image = new Image("img/loader/loading.gif");
        image.setStyleName(ECoResourceUtils.bundle().css().videoPercentWaiting());

        panel.add(p1);
        panel.add(rightPanel(infoLabel, image, uploading, null));
        panel.add(StyleUtils.clearDiv());
    }

    private HelpTextBox nameBox(String text) {
        HelpTextBox nameBox = HelpTextBox.stringHelpTextBox(ECoMessageUtils.c.videoFilesSettings_text_videoName(), 1, 256);
        nameBox.addStyleName(ECoResourceUtils.bundle().css().videoItem());
        nameBox.setText(text);
        return nameBox;
    }

    private ConsysTextArea descriptionBox(String text) {
        ConsysTextArea descriptionBox = new ConsysTextArea(0, 0, ConsysTextArea.WIDTH, ConsysTextArea.HEIGHT, false, UIMessageUtils.c.const_description());
        descriptionBox.setEmptyText(UIMessageUtils.c.const_description());
        descriptionBox.addStyleName(ECoResourceUtils.bundle().css().videoItem());
        descriptionBox.setText(text);
        return descriptionBox;
    }

    private HelpTextBox speakerBox(String text) {
        HelpTextBox speakerBox = HelpTextBox.stringHelpTextBox(ECoMessageUtils.c.videoFilesSettings_text_speakers(), 0, 2048);
        speakerBox.addStyleName(ECoResourceUtils.bundle().css().videoItemPeople());
        speakerBox.setText(text);
        return speakerBox;
    }

    private HelpTextBox authorBox(String text) {
        HelpTextBox authorBox = HelpTextBox.stringHelpTextBox(ECoMessageUtils.c.videoFilesSettings_text_authors(), 0, 2048);
        authorBox.addStyleName(ECoResourceUtils.bundle().css().videoItemPeople());
        authorBox.setText(text);
        return authorBox;
    }

    /** vytvori levou cast polozky */
    private static FlowPanel leftPanel() {
        FlowPanel p = new FlowPanel();
        p.setStyleName(ECoResourceUtils.bundle().css().videoDisplayL());
        return p;
    }

    /** vytvori pravou cast polozky */
    private static FlowPanel rightPanel(Label label, String videoUrl) {
        return rightPanel(label, null, false, videoUrl);
    }

    /** vytvori pravou cast polozky */
    private static FlowPanel rightPanel(Label label, Image image, boolean uploading, String videoUrl) {
        FlowPanel display = new FlowPanel();
        display.setStyleName(ECoResourceUtils.bundle().css().videoDisplay());

        if (videoUrl == null) {
            if (label != null) {
                FlowPanel fp = new FlowPanel();
                fp.setStyleName(ECoResourceUtils.bundle().css().videoDisplayUpload());
                if (uploading && image != null) {
                    fp.add(image);
                }
                fp.add(label);

                display.add(fp);
            }
        } else {
            //<iframe width="364" height="364" src="http://147.229.8.65/uploaded/embed?id=25" frameborder="0" allowfullscreen></iframe>
            Frame frame = new Frame(videoUrl);
            frame.getElement().setAttribute("width", "364");
            frame.getElement().setAttribute("height", "364");
            frame.getElement().setAttribute("frameborder", "0");
            frame.getElement().setAttribute("allowfullscreen", "");
            display.add(frame);
        }

        FlowPanel p = new FlowPanel();
        p.setStyleName(ECoResourceUtils.bundle().css().videoDisplayR());
        p.add(display);

        return p;
    }

    /** prida popisek k policku */
    private FlowPanel wrapPersons(HelpTextBox box) {
        FlowPanel p = new FlowPanel();
        p.add(box);
        p.add(StyleUtils.getStyledLabel(ECoMessageUtils.c.videoFilesSettings_text_separateNamesByCommas(),
                StyleUtils.FONT_11PX, StyleUtils.TEXT_GRAY, StyleUtils.MARGIN_BOT_10));
        return p;
    }

    private void hideWaitingWithPercent(boolean hideAll) {
        if (timer != null) {
            timer.cancel();
        }
        if (infoLabel != null) {
            infoLabel.setText(hideAll ? "" : ECoMessageUtils.c.videoFilesListItem_text_readyToProcessing());
            infoLabel.addStyleName(StyleUtils.TEXT_GRAY);
        }
        if (image != null) {
            image.setVisible(false);
        }
    }

    /** vygeneruje ovladaci prvky formulare */
    private FlowPanel control(HelpTextBox nameBox, ConsysTextArea descriptionBox, HelpTextBox speakersBox, HelpTextBox authorsBox, boolean fromEdit) {
        FlowPanel failPanel = new FlowPanel();

        final ActionImage button;
        if (fromEdit) {
            button = ActionImage.getConfirmButton(UIMessageUtils.c.const_update());
            button.addClickHandler(updateClickHandler(nameBox, descriptionBox, speakersBox, authorsBox, failPanel));
        } else {
            if (VideoRecordState.UPLOADING.equals(item.getState())) {
                button = ActionImage.getConfirmButton(ECoMessageUtils.c.videoFilesSettings_text_uploading());
                button.setEnabled(false);
                if (timer != null) {
                    timer.cancel();
                }
                timer = new Timer() {
                    @Override
                    public void run() {
                        UploadingForm uf = LayoutManager.get().getUploadingForms().get(item.getUuid());
                        VideoUploadingForm form = (VideoUploadingForm) uf;
                        if (form == null) {
                            hideWaitingWithPercent(true);
                            // TODO: oznacit za chybne uploadovany?
                            return;
                        }

                        if (form.getPercent().equals(VideoUploadingForm.FULL_PERCENT)) {
                            // hotovo
                            hideWaitingWithPercent(false);
                            button.setText(ECoMessageUtils.c.videoFilesSettings_action_startProcessing());
                            button.setEnabled(true);
                            LayoutManager.get().getUploadingForms().remove(item.getUuid());
                            form.removeFromParent();
                        } else {
                            if (infoLabel != null) {
                                infoLabel.setText(form.getPercent());
                            }
                        }
                    }
                };
                timer.scheduleRepeating(2000);
            } else {
                button = ActionImage.getConfirmButton(ECoMessageUtils.c.videoFilesSettings_action_startProcessing());
                button.setEnabled(true);
            }
            button.addClickHandler(startProcessingClickHandler(nameBox, descriptionBox, speakersBox, authorsBox, failPanel));
        }
        button.addStyleName(StyleUtils.FLOAT_LEFT);

        ActionLabel cancel = new ActionLabel(UIMessageUtils.c.const_cancel());
        cancel.addStyleName(StyleUtils.FLOAT_RIGHT);
        if (fromEdit) {
            cancel.setClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    showItem();
                }
            });
        } else {
            cancel.setClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    cancelUpload();
                }
            });
        }

        FlowPanel p = new FlowPanel();
        p.add(failPanel);
        p.add(cancel);
        p.add(button);
        p.add(StyleUtils.clearDiv());
        return p;
    }

    private ClickHandler updateClickHandler(final HelpTextBox nameBox, final ConsysTextArea descriptionBox,
            final HelpTextBox speakersBox, final HelpTextBox authorsBox, final FlowPanel failPanel) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final UpdateEventVideoAction action = new UpdateEventVideoAction(item.getSystemName());
                action.setAuthors(authorsBox.getText());
                action.setDescription(descriptionBox.getText());
                action.setName(nameBox.getText());
                action.setSpeakers(speakersBox.getText());

                if (StringUtils.isBlank(action.getName())) {
                    failPanel.add(getFailMessage(UIMessageUtils.m.const_fieldMustEntered(ECoMessageUtils.c.videoFilesSettings_text_videoName())));
                    return;
                }

                EventBus.fire(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        if (caught instanceof NoRecordsForAction) {
                            // vypsat nekam chybu
                        }
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        item.setAuthors(action.getAuthors());
                        item.setCustomName(action.getName());
                        item.setDescription(action.getDescription());
                        item.setSpeakers(action.getSpeakers());
                        showItem();
                    }
                }, null));
            }
        };
    }

    private ClickHandler startProcessingClickHandler(final HelpTextBox nameBox, final ConsysTextArea descriptionBox,
            final HelpTextBox speakersBox, final HelpTextBox authorsBox, final FlowPanel failPanel) {
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                final UpdateEventVideoAction action = new UpdateEventVideoAction(item.getSystemName());
                action.setAuthors(authorsBox.getText());
                action.setDescription(descriptionBox.getText());
                action.setName(nameBox.getText());
                action.setSpeakers(speakersBox.getText());

                if (StringUtils.isBlank(action.getName())) {
                    failPanel.add(getFailMessage(UIMessageUtils.m.const_fieldMustEntered(ECoMessageUtils.c.videoFilesSettings_text_videoName())));
                    return;
                }

                EventBus.fire(new EventDispatchEvent(action, new AsyncCallback<VoidResult>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        if (caught instanceof NoRecordsForAction) {
                            // vypsat nekam chybu
                        }
                    }

                    @Override
                    public void onSuccess(VoidResult result) {
                        item.setAuthors(action.getAuthors());
                        item.setCustomName(action.getName());
                        item.setDescription(action.getDescription());
                        item.setSpeakers(action.getSpeakers());
                        item.setState(VideoRecordState.UPLOADED);
                        item.setProcessing(true);
                        showItem();
                    }
                }, null));
            }
        };
    }

    private ConsysMessage getFailMessage(String fail) {
        ConsysMessage message = ConsysMessage.getFail();
        message.setWidth(190);
        message.addStyleName(StyleUtils.MARGIN_BOT_10);
        message.setText(fail);
        return message;
    }

    private void cancelUpload() {
        // nejdriv zrusime uploadovani
        String cancelUrl;

        try {
            final ClientUser user = (ClientUser) Cache.get().getSafe(UserCacheAction.USER);
            final String uid = user.getUuid();

            cancelUrl = EventURLFactory.overseerServlet("/upload/cancelVideo") + "&uid=" + uid + "&vid=" + item.getUuid();
        } catch (NotInCacheException ex) {
            parent.getFailMessage().addOrSetText("Missing in cache: " + UserCacheAction.USER);
            return;
        }

        RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, cancelUrl);
        rb.setCallback(new RequestCallback() {
            @Override
            public void onResponseReceived(Request request, Response response) {
                if (response.getStatusCode() == Response.SC_OK) {
                    // podarilo se zastavit uploadovani smazeme zaznam o videu
                    EventBus.fire(new EventDispatchEvent(new DeleteEventVideoAction(item.getSystemName()),
                            new AsyncCallback<VoidResult>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    // obecne chyby zpracovava event action executor
                                }

                                @Override
                                public void onSuccess(VoidResult result) {
                                    parent.refresh();
                                }
                            }, parent));
                }
            }

            @Override
            public void onError(Request request, Throwable exception) {
                parent.getFailMessage().addOrSetText(UIMessageUtils.getFailMessage(CommonExceptionEnum.SERVICE_FAILED));
            }
        });
        try {
            rb.send();
        } catch (RequestException ex) {
            // neco se pokazilo
        }
    }
}
