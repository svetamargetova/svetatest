package consys.event.common.gwt.client.module.event.committees;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.ui.comp.panel.ListEditPanelItem;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class RightListEditItem extends ListEditPanelItem<ModuleRole> {

    private ModuleRole role;

    public RightListEditItem(ModuleRole role) {
        this.role = role;
    }

    @Override
    public SimplePanel createContent() {
        SimplePanel wrapper = new SimplePanel();
        wrapper.setWidget(new Label(role.getLocalizedModuleName() + " — " + role.getLocalizedName()));
        return wrapper;
    }

    @Override
    public ModuleRole getObject() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof RightListEditItem) && role != null && ((RightListEditItem) o).role != null && ((RightListEditItem) o).role.getId().equals(role.getId());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.role != null ? this.role.hashCode() : 0);
        return hash;
    }
}
