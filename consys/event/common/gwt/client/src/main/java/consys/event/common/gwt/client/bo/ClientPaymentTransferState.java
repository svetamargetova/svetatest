package consys.event.common.gwt.client.bo;

/**
 * Stav transferu
 * @author Palo Gressa <gressa@acemcee.com>
 */
public enum ClientPaymentTransferState {
    /** Len sa do neho pridavaju dalsi a dalsi zaplatene faktury.  */
    ACTIVE,
    /** Transfer je uzatvoreny a ceka na potvrdeni ze sme prevedli penize */
    PREPARED,
    /** Prevedli sme penize */
    TRANSFERED,
    /** Nastavala nejaka chyba */
    ERR;
}
