package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.event.common.gwt.client.module.payment.PaymentsLicencesContent;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class GeneralSettings extends SimpleFormPanel implements MenuFormItem {

    @Override
    public String getName() {
        return ECoMessageUtils.c.generalSettings_title();
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        clear();
        addWidget(new BaseSettingsContent());
        addWidget(Separator.addedStyle(StyleUtils.MARGIN_BOT_10));
        addWidget(new DetailSettingsContent());
        addWidget(Separator.addedStyle(MARGIN_VER_10));
        addWidget(new PaymentsLicencesContent());
        panel.setWidget(this);
    }
}
