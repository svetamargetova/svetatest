package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import consys.common.gwt.client.cache.Cache;
import consys.common.gwt.client.rpc.result.SelectBoxItem;
import consys.common.gwt.client.ui.base.TitlePanel;
import consys.common.gwt.client.ui.comp.ActionImage;
import consys.common.gwt.client.ui.comp.Separator;
import consys.common.gwt.client.ui.comp.panel.PropertyPanel;
import consys.common.gwt.client.ui.utils.FormUtils;
import consys.common.gwt.client.ui.utils.StyleUtils;
import consys.common.gwt.client.ui.utils.UIMessageUtils;
import consys.common.gwt.shared.exception.NotInCacheException;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import java.util.ArrayList;

/**
 *
 * @author pepa
 */
public class CommonPropertyPanel extends PropertyPanel implements TitlePanel {

    // komponenty
    private HorizontalPanel controllPart;
    private Label titleLabel;

    public CommonPropertyPanel() {
        titleLabel = StyleUtils.getStyledLabel(UIMessageUtils.c.const_property(), FONT_19PX, FONT_BOLD, MARGIN_RIGHT_10);

        ActionImage backButton = ActionImage.getBackButton(ECoMessageUtils.c.eventSettings_title(), true);
        backButton.addStyleName(MARGIN_LEFT_20);
        backButton.addClickHandler(FormUtils.breadcrumbBackClickHandler());

        controllPart = new HorizontalPanel();
        controllPart.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
        controllPart.add(titleLabel);
        controllPart.add(backButton);
    }

    @Override
    protected void onLoad() {
        clear();
        addWidget(controllPart);
        addWidget(Separator.addedStyle(MARGIN_VER_10));

        try {
            String value = (String) Cache.get().getSafe(CommonProperties.EVENT_VIDEO_UPLOAD);

            ArrayList<SelectBoxItem<String>> items = new ArrayList<SelectBoxItem<String>>();
            items.add(new SelectBoxItem<String>("true", UIMessageUtils.c.const_yes()));
            items.add(new SelectBoxItem<String>("false", UIMessageUtils.c.const_no()));

            addProperty(CommonProperties.EVENT_VIDEO_UPLOAD,
                    ECoMessageUtils.c.commonPropertyPanel_property_visibleVideos(),
                    items, value, "", "280px");
        } catch (NotInCacheException ex) {
            getFailMessage().setText(UIMessageUtils.m.const_notInCache(CommonProperties.EVENT_VIDEO_UPLOAD));
        }
    }

    @Override
    public String getPanelTitle() {
        return UIMessageUtils.c.const_property();
    }
}
