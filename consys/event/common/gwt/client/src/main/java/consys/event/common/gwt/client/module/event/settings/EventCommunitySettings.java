package consys.event.common.gwt.client.module.event.settings;

import com.google.gwt.user.client.ui.SimplePanel;
import consys.common.gwt.client.ui.comp.panel.SimpleFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.event.common.gwt.client.utils.ECoMessageUtils;

/**
 *
 * @author pepa
 */
public class EventCommunitySettings extends SimpleFormPanel implements MenuFormItem {

    @Override
    public String getName() {
        return ECoMessageUtils.c.communitySettings_title();
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        clear();
        addWidget(new CommunitySettings());
        panel.setWidget(this);
    }
}
