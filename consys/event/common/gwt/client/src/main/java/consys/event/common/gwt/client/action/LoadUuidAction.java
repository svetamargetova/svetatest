package consys.event.common.gwt.client.action;

import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.shared.action.EventAction;

/**
 * Akce pro nacteni noveho uuid
 * @author pepa
 */
public class LoadUuidAction extends EventAction<StringResult> {

    private static final long serialVersionUID = 6356377878279029701L;
}
