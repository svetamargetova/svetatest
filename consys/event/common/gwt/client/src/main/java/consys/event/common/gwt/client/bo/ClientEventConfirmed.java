package consys.event.common.gwt.client.bo;

import consys.common.gwt.shared.action.Result;
import java.util.Date;

/**
 *
 * @author Hubr
 */
public class ClientEventConfirmed implements Result {

    private Date from;
    private Date to;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
