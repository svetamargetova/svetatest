package consys.event.common.gwt.client.module.event.committees;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import consys.common.gwt.client.EventBus;
import consys.common.gwt.client.module.ModuleRole;
import consys.common.gwt.client.module.RoleManager;
import consys.common.gwt.client.ui.comp.ActionLabel;
import consys.common.gwt.client.ui.comp.ConsysStringTextBox;
import consys.common.gwt.client.ui.comp.Form;
import consys.common.gwt.client.ui.comp.panel.ListEditPanel;
import consys.common.gwt.client.ui.comp.panel.SwapPanel;
import consys.common.gwt.client.ui.comp.panel.abst.RUSimpleHelpFormPanel;
import consys.common.gwt.client.ui.comp.panel.element.MenuFormItem;
import consys.common.gwt.client.ui.comp.search.SearchDelegate;
import consys.common.gwt.client.ui.comp.user.UserLabel;
import consys.common.gwt.client.ui.comp.user.UserListThumbLabel;
import consys.common.gwt.client.ui.comp.user.UserSearchDialog;
import consys.common.gwt.client.ui.comp.user.UserUuidLabel;
import consys.common.gwt.client.ui.comp.user.list.UserLabelListEditPanel;
import consys.common.gwt.client.ui.comp.user.list.UserLabelListPanel;
import consys.common.gwt.client.ui.dom.DOMX;
import consys.common.gwt.client.ui.utils.CssStyles;
import consys.common.gwt.client.ui.utils.FormPanelUtils;
import consys.common.gwt.client.ui.utils.LabelColorEnum;
import consys.common.gwt.shared.bo.ClientUserWithAffiliationThumb;
import consys.event.common.gwt.client.action.LoadCommitteeAction;
import consys.event.common.gwt.client.bo.ClientCommittee;
import consys.event.common.gwt.client.bo.ClientCommitteeThumb;
import consys.common.gwt.client.event.EventDispatchEvent;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.client.ui.comp.search.SearchListDelegate;
import consys.common.gwt.client.ui.utils.ResourceUtils;
import consys.common.gwt.shared.bo.ClientInvitedUser;
import consys.common.gwt.shared.bo.ClientUserThumb;
import consys.common.gwt.shared.bo.InvitedUser;
import consys.event.common.gwt.client.action.UpdateCommitteeAction;
import consys.event.common.gwt.client.utils.ECoMessageUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventCommitteePanel extends RUSimpleHelpFormPanel implements MenuFormItem {

    public static final String CREATE_COMMITTEE_TOKEN = "CreateCommittee";
    // Data
    private ClientCommitteeThumb committeeThumb;
    private ClientCommittee loadedCommittee;
    //
    // ReadMode
    //
    private UserLabel chairLabel;
    private RightList chairRightList;
    private UserLabelListPanel membersUserList;
    private RightList membersRightsList;
    //
    // EditMode
    //
    private ConsysStringTextBox committeeNameBox;
    private ActionLabel selectChairActionLabel;
    private ListEditPanel<RightListEditItem> chairRightsEditList;
    private ListEditPanel<RightListEditItem> membersRightsEditList;
    private UserLabelListEditPanel membersUserEditList;
    private InvitedUser newCommitteeChair;

    public EventCommitteePanel(ClientCommitteeThumb committeeThumb) {
        super(true);
        this.committeeThumb = committeeThumb;
        //addHelpTitle("Committee Settings");
        //addHelpText("Text Text Text Text Text Text Text Text Text Text Text Text Text Text ");
    }

    private RightList chairRightsList() {
        if (chairRightList == null) {
            chairRightList = new RightList();
            chairRightList.setStaticHeight();
            chairRightList.addStyleName(ResourceUtils.system().css().borderGray1());
        }
        return chairRightList;
    }

    private UserLabelListPanel membersList() {
        if (membersUserList == null) {
            membersUserList = new UserLabelListPanel();
            membersUserList.setStaticHeight();
            membersUserList.addStyleName(ResourceUtils.system().css().borderGray1());
        }
        return membersUserList;
    }

    private RightList membersRightsList() {
        if (membersRightsList == null) {
            membersRightsList = new RightList();
            membersRightsList.setStaticHeight();
            membersRightsList.addStyleName(ResourceUtils.system().css().borderGray1());
        }
        return membersRightsList;
    }

    @Override
    public String getName() {
        return committeeThumb.getName();
    }

    @Override
    public String getNote() {
        return null;
    }

    @Override
    public void onShow(SimplePanel panel) {
        panel.clear();
        panel.setWidget(this);
    }

    // Swap panel pro prava membru a pod? ze este nenanstavenby
    @Override
    public Widget readForm() {
        Form form = new Form();
        final Label name = new Label();

        final SwapPanel swapPanel = new SwapPanel();
        chairLabel = new UserUuidLabel();
        swapPanel.setDefaultWidget(chairLabel);

        HorizontalPanel hp = new HorizontalPanel();
        hp.add(name);
        hp.add(FormPanelUtils.getEditModeButton(this, null));

        form.addRequired(ECoMessageUtils.c.committee_text_committeeName(), hp);
        form.addSeparator();

        FlowPanel fp = new FlowPanel();
        swapPanel.addStyleName(CssStyles.MARGIN_BOT_5);
        fp.add(swapPanel);
        fp.add(chairRightsList());

        form.addRequired(ECoMessageUtils.c.committee_form_committeeChair(), fp);
        form.addSeparator();
        form.addOptional(ECoMessageUtils.c.committee_form_committeeMembers(), membersList());
        form.addSeparator();
        form.addOptional(ECoMessageUtils.c.committee_form_committeeRights(), membersRightsList());

        EventBus.get().fireEvent(new EventDispatchEvent(new LoadCommitteeAction(committeeThumb.getId()), new AsyncCallback<ClientCommittee>() {

            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ClientCommittee result) {
                // nastaveni nazvu vyboru
                name.setText(result.getName());

                // CHAIR - name
                if (result.getChair() == null) {
                    Label missingChair = new Label(ECoMessageUtils.c.committee_text_chairMissing());
                    DOMX.setColor(missingChair.getElement(), LabelColorEnum.getHexColor(LabelColorEnum.FAIL_COLOR));
                    swapPanel.setSwapWidget(missingChair);
                    swapPanel.swap();
                } else {
                    chairLabel.init(result.getChair().getFullName(),result.getChair().getUuid());
                }
                // CHAIR - rights
                List<ModuleRole> rights = RoleManager.translateToModuleRoles(result.getChairRights());
                Collections.sort(rights);
                chairRightsList().setRights(rights);

                // MEMBERS - users
                Collections.sort(result.getMemebers());
                membersList().setEventUsers(result.getMemebers());


                // MEMBERS - rights
                rights = RoleManager.translateToModuleRoles(result.getRights());
                Collections.sort(rights);
                membersRightsList().setRights(rights);

                loadedCommittee = result;
            }
        }, this));

        return form;
    }

    @Override
    public Widget updateForm(String id) {
        Form form = new Form();

        // NAME
        committeeNameBox = new ConsysStringTextBox(1, 50);
        committeeNameBox.setText(loadedCommittee.getName());
        form.addRequired(ECoMessageUtils.c.committee_text_committeeName(), committeeNameBox);

        // CHAIR - name , za menom je odkaz na zmenu chaira
        if (loadedCommittee.getChair() == null) {
            selectChairActionLabel = new ActionLabel(ECoMessageUtils.c.committee_action_chairChange());
        } else {
            selectChairActionLabel = new ActionLabel(ECoMessageUtils.c.committee_action_chairSelect());
        }
        selectChairActionLabel.addStyleName(CssStyles.FONT_11PX);
        selectChairActionLabel.addStyleName(CssStyles.MARGIN_LEFT_10);

        final FlowPanel chairNamePanel = new FlowPanel();
        chairNamePanel.add(chairLabel);
        chairNamePanel.add(selectChairActionLabel);

        selectChairActionLabel.setClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserSearchDialog searchDialog = new UserSearchDialog();
                searchDialog.setDelegate(new SearchDelegate<ClientInvitedUser>() {

                    @Override
                    public void didSelect(ClientInvitedUser item) {
                        ClientUserWithAffiliationThumb chair = item.getUserWithAffiliationThumb();
                        chairNamePanel.clear();
                        chairLabel = new UserListThumbLabel(chair);
                        chairNamePanel.add(chairLabel);

                        selectChairActionLabel.setText(ECoMessageUtils.c.committee_action_chairChange());

                        chairNamePanel.add(selectChairActionLabel);
                        newCommitteeChair = item.parentObject();
                    }
                });
                searchDialog.showCentered();
            }
        });



        // CHAIR - Rights
        ArrayList<RightListEditItem> chairEditListItems = new ArrayList<RightListEditItem>();

        ArrayList<ModuleRole> chairRights = RoleManager.translateToModuleRoles(loadedCommittee.getChairRights());
        for (ModuleRole m : chairRights) {
            chairEditListItems.add(new RightListEditItem(m));
        }


        chairRightsEditList = new ListEditPanel<RightListEditItem>(chairEditListItems, RightListItem.HEIGHT, 5);
        chairRightsEditList.addListStyleName(ResourceUtils.system().css().borderGray1());
        chairRightsEditList.setActionName(ECoMessageUtils.c.committee_action_addRight());
        chairRightsEditList.setActionClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                RightsDialog dialog = new RightsDialog();
                dialog.setListDelegate(new SearchListDelegate<RightListItem>() {

                    @Override
                    public void didSelect(ArrayList<RightListItem> items) {
                        for (RightListItem item : items) {
                            chairRightsEditList.addItem(new RightListEditItem(item.getItem()));
                        }
                    }
                });
                dialog.setAddedRights(chairRightsEditList.getAllItems());
                dialog.showCentered();
            }
        });
        chairRightsEditList.setStaticHeight();

        FlowPanel chairPanel = new FlowPanel();
        chairPanel.add(chairNamePanel);
        chairNamePanel.addStyleName(CssStyles.MARGIN_BOT_5);
        chairPanel.add(chairRightsEditList);
        form.addRequired(ECoMessageUtils.c.committee_form_committeeChair(), chairPanel);

        // MEMBERS - users
        membersUserEditList = new UserLabelListEditPanel(membersUserList.getUsers());
        membersUserEditList.addListStyleName(ResourceUtils.system().css().borderGray1());
        membersUserEditList.setStaticHeight();
        membersUserEditList.setAddLabelName(ECoMessageUtils.c.committee_action_addMember());
        form.addSeparator();
        form.addOptional(ECoMessageUtils.c.committee_form_committeeMembers(), membersUserEditList);

        // MEMBERS - rights
        ArrayList<RightListEditItem> membersEditListItems = new ArrayList<RightListEditItem>();


        List<ModuleRole> membersRights = RoleManager.translateToModuleRoles(loadedCommittee.getRights());
        for (ModuleRole m : membersRights) {
            membersEditListItems.add(new RightListEditItem(m));
        }


        membersRightsEditList = new ListEditPanel<RightListEditItem>(membersEditListItems, RightListItem.HEIGHT, 5);
        membersRightsEditList.addListStyleName(ResourceUtils.system().css().borderGray1());
        membersRightsEditList.setActionName(ECoMessageUtils.c.committee_action_addRight());
        membersRightsEditList.setActionClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                RightsDialog dialog = new RightsDialog();
                dialog.setListDelegate(new SearchListDelegate<RightListItem>() {

                    @Override
                    public void didSelect(ArrayList<RightListItem> items) {
                        for (RightListItem item : items) {
                            membersRightsEditList.addItem(new RightListEditItem(item.getItem()));
                        }
                    }
                });
                dialog.setAddedRights(membersRightsEditList.getAllItems());
                dialog.showCentered();

            }
        });
        membersRightsEditList.setStaticHeight();

        // Vlyoeni separatoru a kontroleru
        form.addSeparator();
        form.addOptional(ECoMessageUtils.c.committee_form_committeeRights(), membersRightsEditList);
        form.addActionMembers(FormPanelUtils.getUpdateButton(this, ""), FormPanelUtils.getCancelButton(this), "350px");

        return form;
    }

    @Override
    public ClickHandler confirmUpdateClickHandler(String id) {
        return new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ArrayList<String> newChairRights = new ArrayList<String>();
                for (RightListEditItem i : chairRightsEditList.getNewMembers()) {
                    newChairRights.add(i.getObject().getId());
                }
                ArrayList<String> removeChairRights = new ArrayList<String>();
                for (RightListEditItem i : chairRightsEditList.getRemovedMembers()) {
                    removeChairRights.add(i.getObject().getId());
                }

                ArrayList<InvitedUser> newMembers = new ArrayList<InvitedUser>();
                for (ClientInvitedUser u : membersUserEditList.getNewMembers()) {
                    newMembers.add(u.parentObject());
                }
                ArrayList<String> removeMembers = new ArrayList<String>();
                for (ClientUserThumb t : membersUserEditList.getRemovedMembers()) {
                    removeMembers.add(t.getUuid());
                }

                ArrayList<String> newMembersRights = new ArrayList<String>();
                for (RightListEditItem i : membersRightsEditList.getNewMembers()) {
                    newMembersRights.add(i.getObject().getId());
                }
                ArrayList<String> removeMembersRights = new ArrayList<String>();
                for (RightListEditItem i : membersRightsEditList.getRemovedMembers()) {
                    removeMembersRights.add(i.getObject().getId());
                }

                UpdateCommitteeAction update = new UpdateCommitteeAction();
                update.setId(loadedCommittee.getId());
                update.setCommitteeName(committeeNameBox.getText().trim());

                InvitedUser chair = null;
                if (newCommitteeChair != null) {
                    chair = newCommitteeChair;
                } else if (loadedCommittee.getChair() != null) {
                    chair = new InvitedUser(loadedCommittee.getChair().getUuid());
                }
                update.setChair(chair);

                update.setNewChairRights(newChairRights);
                update.setRemoveChairRights(removeChairRights);

                update.setNewMembers(newMembers);
                update.setRemoveMembers(removeMembers);

                update.setNewMembersRights(newMembersRights);
                update.setRemoveMembersRights(removeMembersRights);

                EventDispatchEvent updateEvent = new EventDispatchEvent(update,
                        new AsyncCallback<VoidResult>() {

                            @Override
                            public void onFailure(Throwable caught) {
                                // obecne chyby zpracovava event action executor
                            }

                            @Override
                            public void onSuccess(VoidResult result) {
                                clearMessageBox();
                                setWidget(readForm());
                            }
                        }, EventCommitteePanel.this);
                EventBus.get().fireEvent(updateEvent);
            }
        };
    }
}
