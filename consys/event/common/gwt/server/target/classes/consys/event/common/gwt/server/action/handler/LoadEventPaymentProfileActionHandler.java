package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.server.action.server.ActionHandler;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientEventPaymentProfile;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.event.common.gwt.client.action.LoadEventPaymentProfileAction;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.payment.webservice.EventWebService;
import consys.payment.ws.event.LoadEventPaymentProfileRequest;
import consys.payment.ws.event.LoadEventPaymentProfileResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadEventPaymentProfileActionHandler implements ActionHandler<LoadEventPaymentProfileAction, ClientUserPaymentProfile> {

    @Autowired
    private EventWebService eventWebService;
    
    @Override
    public Class<LoadEventPaymentProfileAction> getActionType() {
        return LoadEventPaymentProfileAction.class;
    }

    @Override
    public ClientEventPaymentProfile execute(LoadEventPaymentProfileAction action) throws ActionException {        
        LoadEventPaymentProfileRequest request = EventWebService.OBJECT_FACTORY.createLoadEventPaymentProfileRequest();
        request.setEventProfileUuid(action.getUuid());
        LoadEventPaymentProfileResponse response = eventWebService.loadEventPaymentProfile(request);      
        return ProfileUtils.toClientEventProfile(response.getProfile());        
    }
}
