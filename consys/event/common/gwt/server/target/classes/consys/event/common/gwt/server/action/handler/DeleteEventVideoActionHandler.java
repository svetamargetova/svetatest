package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.service.VideoService;
import consys.event.common.gwt.client.action.DeleteEventVideoAction;
import consys.event.common.gwt.server.EventActionHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class DeleteEventVideoActionHandler implements EventActionHandler<DeleteEventVideoAction, VoidResult> {

    @Autowired
    private VideoService videoService;

    @Override
    public Class<DeleteEventVideoAction> getActionType() {
        return DeleteEventVideoAction.class;
    }

    @Override
    public VoidResult execute(DeleteEventVideoAction action) throws ActionException {
        try {
            videoService.deleteVideo(action.getSystemName());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
