/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.gwt.server.action.handler.utils;

import consys.common.gwt.shared.bo.EventUser;
import consys.event.common.core.bo.UserEvent;

/**
 *
 * @author palo
 */
public class EventUserUtils {

    public static EventUser toEventUser(UserEvent ue) {
        EventUser eu = new EventUser();
        eu.setUuid(ue.getUuid());
        eu.setFullName(ue.getFullName());
        eu.setPosition(ue.getPosition());
        eu.setOrganization(ue.getOrganization());
        eu.setLastName(ue.getLastName());
        eu.setImageUuidPrefix(ue.getProfileImagePrefix());
        return eu;
    }
}
