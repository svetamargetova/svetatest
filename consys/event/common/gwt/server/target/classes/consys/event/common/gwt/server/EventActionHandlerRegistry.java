package consys.event.common.gwt.server;

import consys.common.gwt.server.action.server.ActionHandlerRegistry;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.action.EventAction;

public abstract class EventActionHandlerRegistry extends ActionHandlerRegistry{

    /**
     * Searches the registry and returns the first handler which supports the
     * specied action, or <code>null</code> if none is available.
     * 
     * @param action
     *            The action.
     * @return The handler.
     */
    public abstract <A extends EventAction<R>, R extends Result> EventActionHandler<A, R> findHandler( A action );
    
}
