package consys.event.common.gwt.server.action.handler;

import consys.common.utils.collection.Lists;
import consys.common.gwt.client.ui.comp.list.ListDataSourceRequest;
import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.ListService;
import consys.event.common.core.service.list.ListHandlerMissingException;
import consys.event.common.core.service.list.ListServiceRegistry;
import consys.event.common.core.service.list.Paging;
import consys.event.common.gwt.client.action.exception.ListHandlerException;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.GwtListHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
 
/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListDataSourceRequestHandler implements EventActionHandler<ListDataSourceRequest, ListDataSourceResult>, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(ListDataSourceRequestHandler.class);

    private ListServiceRegistry handlerRegistry;
    private ApplicationContext applicationContext;
    private Map<String,GwtListHandler> listHandlers;


    @Override
    public Class<ListDataSourceRequest> getActionType() {
        return ListDataSourceRequest.class;
    }

    @Override
    public ListDataSourceResult execute(ListDataSourceRequest action) throws ActionException {
        try {            
            // Obmedzenie zoznamu
            Constraints constraints = new Constraints();
            constraints.setFilterTag(action.getFilter().tag());
            constraints.setStringFilterValue(action.getFilter().getStringValue());
            constraints.setIntFilterValue(action.getFilter().getIntValue());
            constraints.setOrderers(action.getOrderers());
            constraints.setUserUuid(action.getUserEventUuid());

            // Strankovanie
            Paging paging = new Paging();
            paging.setFirstResult(action.getFirstResult());
            paging.setMaxResults(action.getItemsPerPage());

            if(logger.isDebugEnabled()){
                logger.debug("ListRequest: "+action.getDataListTag());
                logger.debug("    "+constraints);
                logger.debug("    "+paging);
            }
            
            // Vykoname vylistovanie sluzby
            ListService handler = handlerRegistry.getHandlerByTag(action.getDataListTag());
            GwtListHandler listHandler = getHandlers().get(action.getDataListTag());
            if(handler == null ){
                throw new ServiceFailedException("No handler found fot list with tag: "+action.getDataListTag());
            }

            if(listHandler == null){
                throw new ServiceFailedException("No GWT handler found fot list with tag: "+action.getDataListTag());
            }
            
            ArrayList gwtArray = Lists.newArrayList();
            listHandler.execute(handler.listItems(constraints,paging),gwtArray);
            return new ListDataSourceResult(gwtArray,handler.listAllCount(constraints));
        } catch (ListHandlerMissingException ex) {
            throw new ListHandlerException();
        }
    }

    /**
     * @param handlerRegistry the handlerRegistry to set
     */
    @Autowired(required = true)
    public void setHandlerRegistry(ListServiceRegistry handlerRegistry) {
        this.handlerRegistry = handlerRegistry;
    }

     private Map<String, GwtListHandler> getHandlers() {
        if (listHandlers == null) {            
            listHandlers = new HashMap<String, GwtListHandler>();
            Map<String,GwtListHandler> handlers = applicationContext.getBeansOfType(GwtListHandler.class);
            for(GwtListHandler h : handlers.values()) {
                listHandlers.put(h.getTag(), h);
            }
        }
        return listHandlers;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
