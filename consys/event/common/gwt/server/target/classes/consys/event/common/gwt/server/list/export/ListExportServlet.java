package consys.event.common.gwt.server.list.export;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Lists;
import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import consys.event.common.api.servlet.CommonServletProperties;
import consys.event.common.api.servlet.ListExportServletProperties;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.gwt.server.servlet.AbstractEventServlet;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListExportServlet extends AbstractEventServlet implements CommonServletProperties, ListExportServletProperties {

    private static final long serialVersionUID = -9066498179559507064L;
    private Map<String, ExportHandler> handlers;

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        ListExportTypeEnum outtype = null;
        try {
            String outputtype = request.getParameter(PARAM_OUTPUT_TYPE);
            if (StringUtils.isBlank(outputtype)) {
                sendError(response);
            }
            outtype = ListExportTypeEnum.valueOf(outputtype);
        } catch (IllegalArgumentException e) {
            logger.info("Illegal output export type: " + request.getParameter(PARAM_OUTPUT_TYPE));
            sendError(response);
            return;
        }

        // list tag
        String listTag = request.getParameter(PARAM_TAG);
        if (StringUtils.isBlank(listTag)) {
            logger.debug("Missing list tag.");
            sendError(response);
            return;
        }

        // filter tag
        int filterTag = 0;
        try {
            String sfilterTag = request.getParameter(PARAM_FILTER);
            if (StringUtils.isBlank(sfilterTag)) {
                logger.debug("Missing filter tag.");
                sendError(response);
                return;
            } else {
                filterTag = Integer.parseInt(sfilterTag);
            }

        } catch (NumberFormatException e) {
        }

        // filter int value
        int filterIntValue = 0;
        try {
            String value = request.getParameter(PARAM_FILTER_INT_VALUE);
            if (StringUtils.isNotBlank(value)) {
                filterIntValue = Integer.parseInt(value);
            }
        } catch (NumberFormatException e) {
        }

        // filter string value
        String filterStringValue = request.getParameter(PARAM_FILTER_STRING_VALUE);

        // orderers
        Map<String, String[]> params = request.getParameterMap();
        List<Integer> orderers = Lists.newArrayListWithExpectedSize(1);

        for (Map.Entry<String, String[]> e : params.entrySet()) {
            if (e.getKey().startsWith(PARAM_ORDER_PREFIX)) {
                try {
                    orderers.add(Integer.valueOf(e.getValue()[0]));
                } catch (Exception nfe) {
                    logger.error("Parsing orderer failed: " + e.getValue(), nfe);
                }
            }
        }

        Constraints constraints = new Constraints();
        constraints.setFilterTag(filterTag);
        constraints.setIntFilterValue(filterIntValue);
        constraints.setStringFilterValue(filterStringValue);
        constraints.setUserUuid(getActualContext().getUserEventUuid());
        constraints.setOrderers(toIntArray(orderers));

        String key = composeHandlerKey(listTag, outtype);

        if (getHandlers().containsKey(key)) {
            ExportHandler handler = handlers.get(key);
            try {
                addResponseHeaders(response, outtype);
                handler.doExport(response.getOutputStream(), constraints);
            } catch (IOException ex) {
                logger.error("Error", ex);
                sendError(response);
            }
        } else {
            logger.error("There is no export handler for list tag: " + listTag);
            sendError(response);
            return;
        }
    }

    private void sendError(HttpServletResponse response) {
    }

    private static int[] toIntArray(List<Integer> integerList) {
        int[] intArray = new int[integerList.size()];
        for (int i = 0; i < integerList.size(); i++) {
            intArray[i] = integerList.get(i);
        }
        return intArray;
    }

    public Map<String, ExportHandler> getHandlers() {
        if (handlers == null) {
            Map<String, ExportHandler> map = getApplicationContext().getBeansOfType(ExportHandler.class);
            Builder<String, ExportHandler> builder = ImmutableMap.builder();
            for (ExportHandler handler : map.values()) {
                builder.put(composeHandlerKey(handler), handler);
            }
            handlers = builder.build();
        }
        return handlers;
    }

    /** hlavicka s informacemi o souboru */
    private void addResponseHeaders(HttpServletResponse response, ListExportTypeEnum outtype) {
        String mimeType;
        String suffix;

        switch (outtype) {
            case EXCEL:
                mimeType = "application/excel";
                suffix = "xls";
                break;
            case PDF:
                mimeType = "application/pdf";
                suffix = "pdf";
                break;
            default:
                mimeType = "application/octet-stream";
                suffix = "data";
                break;
        }
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", "attachment; filename=\"export." + suffix + "\"");
    }

    private static String composeHandlerKey(ExportHandler handler) {
        return handler.getListTag() + handler.getType().name();
    }

    private static String composeHandlerKey(String tag, ListExportTypeEnum type) {
        return tag + type.name();
    }
}
