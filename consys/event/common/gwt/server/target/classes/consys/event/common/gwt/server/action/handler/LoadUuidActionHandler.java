package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.client.rpc.result.StringResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.utils.UuidProvider;
import consys.event.common.gwt.client.action.LoadUuidAction;
import consys.event.common.gwt.server.EventActionHandler;

/**
 *
 * @author pepa
 */
public class LoadUuidActionHandler implements EventActionHandler<LoadUuidAction, StringResult> {

    @Override
    public Class<LoadUuidAction> getActionType() {
        return LoadUuidAction.class;
    }

    @Override
    public StringResult execute(LoadUuidAction action) throws ActionException {
        return new StringResult(UuidProvider.getUuid());
    }
}
