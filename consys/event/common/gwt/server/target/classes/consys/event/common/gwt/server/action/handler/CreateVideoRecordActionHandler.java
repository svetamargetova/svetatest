package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.bo.enums.EventVideoState;
import consys.event.common.core.service.VideoService;
import consys.event.common.gwt.client.action.CreateVideoRecordAction;
import consys.event.common.gwt.server.EventActionHandler;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class CreateVideoRecordActionHandler implements EventActionHandler<CreateVideoRecordAction, VoidResult> {

    @Autowired
    private VideoService videoService;

    @Override
    public Class<CreateVideoRecordAction> getActionType() {
        return CreateVideoRecordAction.class;
    }

    @Override
    public VoidResult execute(CreateVideoRecordAction action) throws ActionException {
        String name = action.getEventUuid() + "-" + action.getUuid();

        EventVideo ev = new EventVideo();
        ev.setUuid(action.getUuid());
        ev.setSystemName(name);
        ev.setCustomName(action.getFileName());
        ev.setDescription("");
        ev.setState(EventVideoState.UPLOADING);
        ev.setUploadStarted(new Date());
        videoService.createVideo(ev);

        return VoidResult.RESULT();
    }
}
