package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.gwt.client.action.UpdateEventPaymentProfileAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.payment.webservice.EventWebService;
import consys.payment.ws.bo.Profile;
import consys.payment.ws.event.UpdateEventProfileRequest;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateEventPaymentProfileActionHandler implements EventActionHandler<UpdateEventPaymentProfileAction, VoidResult> {

    @Autowired
    private EventWebService eventWebService;
    @Autowired
    private SystemPropertyService propertyService;

    @Override
    public Class<UpdateEventPaymentProfileAction> getActionType() {
        return UpdateEventPaymentProfileAction.class;
    }

    @Override
    public VoidResult execute(UpdateEventPaymentProfileAction action) throws ActionException {
        try {
            // default currency
            propertyService.updateStringProperty(CommonProperties.EVENT_CURRENCY, action.getProfile().getCurrency().name());

            Profile profile = ProfileUtils.toPortalProfile(action.getProfile());

            UpdateEventProfileRequest request = EventWebService.OBJECT_FACTORY.createUpdateEventProfileRequest();
            request.setBankInvoicedToProfile(action.getProfile().isBankOrderAsProfile());
            request.setCurrency(ProfileUtils.toPortalCurrency(action.getProfile().getCurrency()));
            request.setInvoiceNote(action.getProfile().getInvoiceNote());
            request.setProfile(profile);

            request.setProfileUuid(action.getProfile().getUuid());
            eventWebService.updateEventProfile(request);

            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction("Problem with update system property default currency");
        }
    }
}
