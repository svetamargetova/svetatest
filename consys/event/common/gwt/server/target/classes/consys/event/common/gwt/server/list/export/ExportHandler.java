package consys.event.common.gwt.server.list.export;

import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import consys.event.common.core.service.list.Constraints;
import java.io.OutputStream;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface ExportHandler {

    public String getListTag();
    
    public ListExportTypeEnum getType();

    public void doExport(OutputStream os, Constraints c);    
    
}
