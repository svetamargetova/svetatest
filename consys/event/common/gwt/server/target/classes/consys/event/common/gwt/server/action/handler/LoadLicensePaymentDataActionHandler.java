package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientPaymentOrderData;
import consys.common.gwt.shared.bo.ClientPaymentProduct;
import consys.event.common.gwt.client.action.LoadLicensePaymentDataAction;
import consys.event.common.gwt.server.EventActionHandler;
import consys.payment.webservice.ProductWebService;
import consys.payment.ws.product.LoadLicenseProductRequest;
import consys.payment.ws.product.LoadLicenseProductResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadLicensePaymentDataActionHandler implements EventActionHandler<LoadLicensePaymentDataAction, ClientPaymentOrderData> {

    @Autowired
    private ProductWebService productWebService;

    @Override
    public Class<LoadLicensePaymentDataAction> getActionType() {
        return LoadLicensePaymentDataAction.class;
    }

    @Override
    public ClientPaymentOrderData execute(LoadLicensePaymentDataAction action) throws ActionException {
        LoadLicenseProductRequest req = ProductWebService.OBJECT_FACTORY.createLoadLicenseProductRequest();
        req.setEventProfileUuid(action.getCustomerProfileUuid());
        req.setEventUuid(action.getEventUuid());
        LoadLicenseProductResponse response = productWebService.loadLicenseProduct(req);

        ClientPaymentOrderData out = new ClientPaymentOrderData();
        ClientPaymentProduct license = new ClientPaymentProduct();
        license.setName(response.getLicense().getName());
        license.setPrice(MonetaryUtils.fromBigDecimal(response.getLicense().getUnitPrice()));
        license.setQuantity(1);
        out.getProducts().add(license);
        out.setTotalPrice(license.getPrice());
        return out;
    }
}
