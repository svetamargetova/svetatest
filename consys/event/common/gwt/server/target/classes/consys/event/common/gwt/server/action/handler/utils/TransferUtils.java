package consys.event.common.gwt.server.action.handler.utils;

import consys.common.gwt.server.utils.MonetaryUtils;
import consys.event.common.gwt.client.bo.ClientPaymentTransferHead;
import consys.event.common.gwt.client.bo.ClientPaymentTransferState;
import consys.event.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.transfer.TransferHead;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TransferUtils {

    public static ClientPaymentTransferHead toHead(TransferHead head) {
        ClientPaymentTransferHead out = new ClientPaymentTransferHead();
        out.setFees(MonetaryUtils.fromBigDecimal(head.getFees()));
        out.setThirdPartyFees(MonetaryUtils.fromBigDecimal(head.getThirdPartyFees()));
        out.setTotal(MonetaryUtils.fromBigDecimal(head.getTotal()));
        out.setInfo(head.getInfo());
        switch (head.getState()) {
            case ACTIVE:
                out.setState(ClientPaymentTransferState.ACTIVE);
                break;
            case ERROR:
                out.setState(ClientPaymentTransferState.ERR);
                break;
            case PREPARED:
                out.setState(ClientPaymentTransferState.PREPARED);
                break;
            case TRANSFERED:
                out.setState(ClientPaymentTransferState.TRANSFERED);
                break;
        }
        out.setTransferDate(ConvertUtils.toDate(head.getDate()));
        out.setUuid(head.getUuid());
        return out;
    }
}
