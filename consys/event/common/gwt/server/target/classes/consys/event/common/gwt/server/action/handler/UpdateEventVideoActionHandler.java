package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.service.VideoService;
import consys.event.common.gwt.client.action.UpdateEventVideoAction;
import consys.event.common.gwt.server.EventActionHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class UpdateEventVideoActionHandler implements EventActionHandler<UpdateEventVideoAction, VoidResult> {

    @Autowired
    private VideoService videoService;

    @Override
    public Class<UpdateEventVideoAction> getActionType() {
        return UpdateEventVideoAction.class;
    }

    @Override
    public VoidResult execute(UpdateEventVideoAction action) throws ActionException {
        try {
            EventVideo ev = videoService.loadVideo(action.getSystemName());

            ev.setAuthors(action.getAuthors());
            ev.setCustomName(action.getName());
            ev.setDescription(action.getDescription());
            ev.setSpeakers(action.getSpeakers());
            ev.setToProcessing(true);

            videoService.updateVideo(ev);
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
