package consys.event.common.gwt.server.servlet;

import consys.event.common.api.servlet.CommonServletProperties;
import consys.event.overseer.connector.EventRequestContext;
import consys.event.overseer.connector.EventRequestContextProvider;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractEventServlet extends HttpServlet implements CommonServletProperties{

    private static final long serialVersionUID = 536971834601469272L;
    protected Logger logger = LoggerFactory.getLogger(getClass());   
    private ApplicationContext applicationContext;

    @Autowired
    private EventRequestContextProvider requestContextProvider;

    protected ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    protected void initActualContext(String user, String event){
        requestContextProvider.initContext(user, event);
    }

    protected EventRequestContext getActualContext() {
        return requestContextProvider.getContext();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);                               
        applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
        logger.info("Autowiring servlet as spring bean - {}",this.getClass());

    }

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected abstract void processRequest(HttpServletRequest request, HttpServletResponse response);

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            processRequest(req, resp);
        } catch (Throwable t) {
            logger.warn("Error Occured", t);
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            processRequest(req, resp);
        } catch (Throwable t) {
            logger.warn("Error Occured", t);
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }    
}
