package consys.event.common.gwt.server.list.export.xls;

import consys.common.utils.collection.Maps;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.ListService;
import consys.event.common.core.service.list.Paging;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author pepa
 */
public abstract class AbstractTabsXlsExportHandler<T, E> extends AbstractXlsExportHandler {

    // konstanty
    public static final String ALL_SHEET = " All ";
    private static final int[] EMPTY_INDEXES = new int[0];
    // servicy
    private ListService<T, E> listService;
    // data
    private boolean customSheets;

    public AbstractTabsXlsExportHandler(String listTag) {
        this(listTag, false);
    }

    public AbstractTabsXlsExportHandler(String listTag, boolean customSheets) {
        super(listTag);
        this.customSheets = customSheets;
    }

    @Override
    public void buildExport(HSSFWorkbook workbook, Constraints c) {
        listService = listService();

        Paging p = getDefaultPaging();
        Map<String, SheetInfo> sheets = Maps.newHashMap();

        createHeaders(workbook, sheets);

        if (sheets.isEmpty()) {
            // nevytvorily se zadne zalozky, takze nemame do ceho naplnovat
            return;
        }

        List<E> items = null;
        do {
            if (items != null) {
                nextPaging(p);
            }
            items = listService.listItemsExport(c, p);

            for (E item : items) {
                SheetInfo sheetInfo = sheets.get(getSheetKey(item));
                if (sheetInfo == null) {
                    // problem, protoze mame data, ke kterym nemame vytvorenou zalozku
                    logger.warn("data to sheet that is not created");
                    continue;
                }

                createDataRow(sheetInfo, item, sheets);
            }
        } while (hasNext(p, items));

        removeEmptySheetsAndAutosizeNonempty(workbook, sheets);
    }

    private void createHeaders(HSSFWorkbook workbook, Map<String, SheetInfo> sheets) {
        if (customSheets) {
            createCustomSheetsHeaders(workbook);
        }
        createAllTabWithHeader(workbook, sheets);
        createTabsWithHeaders(workbook, sheets);
    }

    private void createDataRow(SheetInfo sheetInfo, E item, Map<String, SheetInfo> sheets) {
        Row row = sheetInfo.getSheet().createRow(sheetInfo.increseAndGetRows());
        createDataRow(item, row, false);

        SheetInfo allSheetInfo = sheets.get(ALL_SHEET);
        Row allRow = allSheetInfo.getSheet().createRow(allSheetInfo.increseAndGetRows());
        createDataRow(item, allRow, true);

        if (customSheets) {
            createCustomSheetsDataRows(item);
        }
    }

    private void removeEmptySheetsAndAutosizeNonempty(HSSFWorkbook workbook, Map<String, SheetInfo> sheets) {
        for (SheetInfo si : sheets.values()) {
            if (si.increseAndGetRows() == SheetInfo.START_ROW_COUNT) {
                int index = workbook.getSheetIndex(si.getSheet());
                workbook.removeSheetAt(index);
            } else {
                boolean all = si.getSheet().getSheetName().equals(ALL_SHEET);
                autosizeColumns(si.getSheet(), all ? allTabAutosizeColumnIndexes() : autosizeColumnIndexes());
            }
        }

        if (customSheets) {
            autosizeCustomSheets();
        }
    }

    /** vytvori potrebne zalozky a vlozi hlavicky tabulek */
    protected abstract void createTabsWithHeaders(HSSFWorkbook workbook, Map<String, SheetInfo> sheets);

    /** vylozi jeden radek dat do zadane zalozky */
    protected abstract void createDataRow(E item, Row row, boolean allSheet);

    /** vraci servicu pro tahani seznamu */
    protected abstract ListService<T, E> listService();

    /** vraci klic zalozky podle zadane polozky */
    protected abstract String getSheetKey(E item);

    /** vraci pole hlavicky (pro prvni zalozku se vsemi polozkami) */
    protected abstract String[] allSheetTitles();

    protected void createHeader(String[] titles, HSSFSheet sheet) {
        Row headerRow = sheet.createRow(0);
        headerRow.setHeightInPoints(12f);
        for (int i = 0; i < titles.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(titles[i]);
            cell.setCellStyle(style.getHeaderStyle());
        }
    }

    protected int[] autosizeColumnIndexes() {
        return EMPTY_INDEXES;
    }

    protected int[] allTabAutosizeColumnIndexes() {
        return EMPTY_INDEXES;
    }

    /** urceno k pretizeni pokud jsou potreba vytvaret dalsi zalozky, ktere nejsou automatizovane */
    protected void createCustomSheetsHeaders(HSSFWorkbook workbook) {
        // implementuji potomci, kteri chteji pridavat vlastni listy (je potreba pouzit i spravny konstruktor)
    }

    /** urceno k pretizeni pokud jsou potreba vytvaret dalsi zalozky, ktere nejsou automatizovane */
    protected void createCustomSheetsDataRows(E item) {
        // implementuji potomci, kteri chteji pridavat vlastni listy (je potreba pouzit i spravny konstruktor)
    }

    /** urceno k pretizeni pokud jsou potreba vytvaret dalsi zalozky, ktere nejsou automatizovane */
    protected void autosizeCustomSheets() {
        // implementuji potomci, kteri chteji pridavat vlastni listy (je potreba pouzit i spravny konstruktor)
    }

    private void createAllTabWithHeader(HSSFWorkbook workbook, Map<String, SheetInfo> sheets) {
        HSSFSheet sheet = workbook.createSheet(ALL_SHEET);
        createHeader(allSheetTitles(), sheet);
        sheets.put(ALL_SHEET, new SheetInfo(sheet));
    }

    protected void autosizeColumns(HSSFSheet sheet, int[] indexes) {
        for (int i = 0; i < indexes.length; i++) {
            sheet.autoSizeColumn(indexes[i]);
        }
    }
}
