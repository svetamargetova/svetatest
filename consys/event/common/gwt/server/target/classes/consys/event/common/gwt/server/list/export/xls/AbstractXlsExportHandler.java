package consys.event.common.gwt.server.list.export.xls;

import consys.common.gwt.shared.bo.list.ListExportTypeEnum;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.core.service.list.Paging;
import consys.event.common.gwt.server.list.export.ExportHandler;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.ClassUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Component
public abstract class AbstractXlsExportHandler implements ExportHandler {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private String tag;
    protected XlsCellStyle style;

    public AbstractXlsExportHandler(String listTag) {
        this.tag = listTag;
        logger.info("Export handler {} - {}", getType().name(), ClassUtils.getShortClassName(this.getClass()));
    }

    @Override
    public void doExport(OutputStream os, Constraints c) {
        try {
            // vytvorime work book
            HSSFWorkbook workbook = new HSSFWorkbook();
            // vytvorime defautlne styly
            style = new DefaultCellStyle(workbook);
            // naplnime
            buildExport(workbook, c);
            // za[piseme na vystup
            workbook.write(os);
        } catch (IOException ex) {
            logger.error("Error generating excel export", ex);
        }
    }

    @Override
    public ListExportTypeEnum getType() {
        return ListExportTypeEnum.EXCEL;
    }

    @Override
    public String getListTag() {
        return tag;
    }

    public abstract void buildExport(HSSFWorkbook workbook, Constraints c);

    /**
     * Convenient method to set a String as text content in a cell.
     * @param cell the cell in which the text must be put
     * @param text the text to put in the cell
     */
    protected void setText(HSSFCell cell, String text) {
        cell.setCellType(HSSFCell.CELL_TYPE_STRING);
        cell.setCellValue(text);
    }

    /**
     * Convenient method to set a Number as text content in a cell.
     * @param cell the cell in which the text must be put
     * @param text the text to put in the cell
     */
    protected void setNumber(HSSFCell cell, Number text) {
        cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
        cell.setCellValue(text.doubleValue());
    }

    protected void setTextCell(Row row, int cellIdx, String text) {
        setTextCell(row, cellIdx, text, style.getCellStyle());
    }

    protected void setDateCell(Row row, int cellIdx, Date date) {
        setDateCell(row, cellIdx, date, style.getCellDateStyle());
    }

    protected void setTextCell(Row row, int cellIdx, String text, CellStyle style) {
        Cell cell = row.createCell(cellIdx);
        cell.setCellStyle(style);
        cell.setCellValue(text);
    }

    protected void setDateCell(Row row, int cellIdx, Date text, CellStyle style) {
        Cell cell = row.createCell(cellIdx);
        cell.setCellStyle(style);
        cell.setCellValue(text);
    }

    protected Paging getDefaultPaging() {
        Paging p = new Paging();
        p.setFirstResult(0);
        p.setMaxResults(100);
        return p;
    }

    protected boolean hasNext(Paging p, List l) {
        return p.getMaxResults() == l.size();
    }

    protected void nextPaging(Paging p) {
        p.setFirstResult(p.getFirstResult() + p.getMaxResults());
    }

    /** vraci String i kdyz je hodnota null (vraci prazdny String) */
    protected String string(String value) {
        return value == null ? "" : value;
    }
    
    /** vyhodi nepovolene znaky pro nazev sheetu */
    public static String sheetName(String text) {
        final String replaceCharacter = "-";
        // nepovolene znaky : \ / ? * [ ]
        String result = text.replaceAll(":", replaceCharacter);
        result = result.replaceAll("\\\\", replaceCharacter);
        result = result.replaceAll("/", replaceCharacter);
        result = result.replaceAll("\\?", replaceCharacter);
        result = result.replaceAll("\\*", replaceCharacter);
        result = result.replaceAll("\\[", replaceCharacter);
        result = result.replaceAll("\\]", replaceCharacter);
        // maximalni delka 31 znaku
        result = result.substring(0,Math.min(31, result.length()));
        return result;
    }
}
