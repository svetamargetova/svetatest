package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.InvitedUser;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.NewUserInvitation;
import consys.event.common.core.service.CommitteeService;
import consys.event.common.core.utils.ModuleRightUtils;
import consys.event.common.gwt.client.action.UpdateCommitteeAction;
import consys.event.common.gwt.server.EventActionHandler;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateCommitteeActionHandler implements EventActionHandler<UpdateCommitteeAction, VoidResult> {

    private CommitteeService committeeService;
    private ModuleRightUtils moduleRightUtils;

    @Override
    public Class<UpdateCommitteeAction> getActionType() {
        return UpdateCommitteeAction.class;
    }

    @Override
    public VoidResult execute(UpdateCommitteeAction action) throws ActionException {
        try {

            // Prelozime UID prav do nazovu ROLI
            List<String> removedChairRights = moduleRightUtils.translateFromClient(action.getRemoveChairRights());
            List<String> newChairRights = moduleRightUtils.translateFromClient(action.getNewChairRights());
            List<String> newMemberRights = moduleRightUtils.translateFromClient(action.getNewMembersRights());
            List<String> removedMemberRights = moduleRightUtils.translateFromClient(action.getRemoveMembersRights());

            // spracujeme pozvanky
            Invitation chair = null;
            if (action.getChair() != null) {
                if (action.getChair().isUserNew()) {
                    chair = new NewUserInvitation(action.getChair().getEmail(), action.getChair().getName());
                } else {
                    chair = new ExistingUserInvitation(action.getChair().getUuid());
                }
            }

            List<Invitation> members = Lists.newArrayList();
            Invitation newMember = null;
            for (InvitedUser member : action.getNewMembers()) {
                if (member.isUserNew()) {
                    newMember = new NewUserInvitation(member.getEmail(), member.getName());
                } else {
                    newMember = new ExistingUserInvitation(member.getUuid());
                }
                members.add(newMember);
            }


            // Posleme na spracovanie           
            committeeService.updateCommittee(action.getId(), action.getCommitteeName(), chair,
                    newChairRights, removedChairRights,
                    members, action.getRemoveMembers(),
                    newMemberRights, removedMemberRights);
            return VoidResult.RESULT();
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }

    }

    /**
     * @param committeeService the committeeService to set
     */
    @Autowired(required = true)
    public void setCommitteeService(CommitteeService committeeService) {
        this.committeeService = committeeService;
    }

    /**
     * @param moduleRightUtils the moduleRightUtils to set
     */
    @Autowired(required = true)
    public void setModuleRightUtils(ModuleRightUtils moduleRightUtils) {
        this.moduleRightUtils = moduleRightUtils;
    }
}
