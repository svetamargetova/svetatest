package consys.event.common.gwt.server.servlet;

import consys.common.core.exception.NoRecordException;
import consys.event.common.api.servlet.CommonServletProperties;
import consys.event.common.core.bo.DataFile;
import consys.event.common.core.service.DataFileService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Na zaklade UUID data file nacita subor
 * @author pepa
 */
public class DownloadDataFileServlet extends AbstractEventServlet{

    private static final long serialVersionUID = 4321229156572750226L;
    

    @Autowired
    private DataFileService dataFileService;

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        try {
            String uuid = request.getParameter(DOWNLOAD_FILE_SERVLET_FILE_ID);
            if (StringUtils.isBlank(uuid)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("File UUID is missing");
                }
                throw new NullPointerException();
            }

            try {
                DataFile file = getDataFileService().loadByUuid(uuid);

                if (file == null) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND);
                    return;
                }

                StringBuilder contentDisposition = new StringBuilder();
                contentDisposition.append(ATTACHMENT_FILENAME);
                contentDisposition.append(file.getFileName());
                contentDisposition.append(END_NAME);
                response.setContentType(file.getContentType());

                response.setHeader(CONTENT_DISPOSITION, contentDisposition.toString());
                response.setHeader(CACHE_CONTROL, NO_CACHE);
                response.getOutputStream().write(file.getByteData());
            } catch (NoRecordException e) {
                if (logger.isDebugEnabled()) {
                    logger.debug("File with uuid " + uuid + " not exists!");
                }
                response.sendError(response.SC_BAD_REQUEST);
            }
        } catch (Exception ex) {
            logger.error("Error!", ex);
        }
    }

    public DataFileService getDataFileService() {        
        return dataFileService;
    }
}
