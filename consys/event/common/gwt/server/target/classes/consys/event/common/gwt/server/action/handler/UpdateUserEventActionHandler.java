package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.service.UserEventService;
import consys.event.common.gwt.client.action.UpdateUserEventAction;
import consys.event.common.gwt.server.EventActionHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateUserEventActionHandler implements EventActionHandler<UpdateUserEventAction, VoidResult> {

    @Autowired
    private UserEventService userEventService;

    @Override
    public Class<UpdateUserEventAction> getActionType() {
        return UpdateUserEventAction.class;
    }

    @Override
    public VoidResult execute(UpdateUserEventAction action) throws ActionException {
        try {
            userEventService.updateUserEvent(action.getUserUuid(), action.getName(), action.getPosition(), action.getOrganization());
            return VoidResult.RESULT();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }        
    }
}
