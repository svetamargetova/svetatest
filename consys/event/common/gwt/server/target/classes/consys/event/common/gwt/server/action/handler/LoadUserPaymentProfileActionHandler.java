package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.server.action.server.ActionHandler;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.event.common.gwt.client.action.LoadUserPaymentProfileAction;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.payment.webservice.ProfileWebService;
import consys.payment.ws.profile.LoadProfileRequest;
import consys.payment.ws.profile.LoadProfileResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadUserPaymentProfileActionHandler implements ActionHandler<LoadUserPaymentProfileAction, ClientUserPaymentProfile> {

    @Autowired
    private ProfileWebService profileWebService;
    
    @Override
    public Class<LoadUserPaymentProfileAction> getActionType() {
        return LoadUserPaymentProfileAction.class;
    }

    @Override
    public ClientUserPaymentProfile execute(LoadUserPaymentProfileAction action) throws ActionException {        
        LoadProfileRequest request = ProfileWebService.OBJECT_FACTORY.createLoadProfileRequest();
        request.setProfileUuid(action.getUuid());
        LoadProfileResponse response = profileWebService.loadProfile(request);        
        return ProfileUtils.toClientUserProfile(response.getProfile());        
    }
}
