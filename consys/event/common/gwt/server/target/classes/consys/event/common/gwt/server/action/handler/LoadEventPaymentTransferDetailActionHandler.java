package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.server.utils.MonetaryUtils;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.gwt.client.action.LoadEventPaymentTransferDetailAction;
import consys.event.common.gwt.client.bo.ClientPaymentTransferDetail;
import consys.event.common.gwt.client.bo.ClientPaymentTransferItem;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.TransferUtils;
import consys.payment.webservice.TransferWebService;
import consys.event.payment.webservice.utils.ConvertUtils;
import consys.payment.ws.transfer.LoadTransferRequest;
import consys.payment.ws.transfer.LoadTransferResponse;
import consys.payment.ws.transfer.TransferItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author pepa
 */
public class LoadEventPaymentTransferDetailActionHandler 
        implements EventActionHandler<LoadEventPaymentTransferDetailAction, ClientPaymentTransferDetail> {

    private TransferWebService transferWebService;

    @Override
    public Class<LoadEventPaymentTransferDetailAction> getActionType() {
        return LoadEventPaymentTransferDetailAction.class;
    }

    @Override
    public ClientPaymentTransferDetail execute(LoadEventPaymentTransferDetailAction action) throws ActionException {
        LoadTransferRequest request  = TransferWebService.OBJECT_FACTORY.createLoadTransferRequest();
        request.setTransferUuid(action.getUuid());
        LoadTransferResponse response = transferWebService.loadEventTransfer(request);
        ClientPaymentTransferDetail out = new ClientPaymentTransferDetail();
        out.setHeader(TransferUtils.toHead(response.getTransfer()));
        for(TransferItem item : response.getItems()){
            ClientPaymentTransferItem i = new ClientPaymentTransferItem();
            i.setFees(MonetaryUtils.fromBigDecimal(item.getFees()));
            i.setThirdPartyFees(MonetaryUtils.fromBigDecimal(item.getThirPartyFees()));
            i.setTotal(MonetaryUtils.fromBigDecimal(item.getTotal()));
            i.setInfo(item.getInfo());
            i.setInvoicePaidDate(ConvertUtils.toDate(item.getDate()));
            i.setUuid(item.getUuid());
            out.getItems().add(i);
        }
        return out;
    }

    /**
     * @param transferWebService the transferWebService to set
     */
    @Autowired
    @Required
    public void setTransferWebService(TransferWebService transferWebService) {
        this.transferWebService = transferWebService;
    }
}
