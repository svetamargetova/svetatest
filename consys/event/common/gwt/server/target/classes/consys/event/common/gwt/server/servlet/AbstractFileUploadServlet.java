package consys.event.common.gwt.server.servlet;

import consys.common.utils.collection.Lists;
import consys.common.utils.collection.Maps;
import consys.event.common.core.bo.DataFile;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractFileUploadServlet extends AbstractEventServlet {

    private static final long serialVersionUID = 536971834601469272L;

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) {
        try {

            // Cely vstup rozprasujeme
            Map<String, String> formInput = Maps.newHashMap();
            List<DataFile> files = Lists.newArrayList();
            
            try {
                // Spracujeme cely vstup
                ServletFileUpload upload = new ServletFileUpload();                
                FileItemIterator iter = upload.getItemIterator(request);                
                while (iter.hasNext()) {
                    FileItemStream item = iter.next();
                    // Spracovanie fromularovych poli
                    if (item.isFormField()) {
                        String fname = item.getFieldName();
                        String fval = getContentFromItem(item);                        
                        logger.debug("Form<{}>: {}", fname ,fval);                        
                        formInput.put(fname, fval);
                    } else {
                        // Spracovanie suborov
                        String name = item.getName();
                        String type = item.getContentType();                                            
                        logger.debug("File<{}>: {}",name,type);   
                        byte[] bytes = IOUtils.toByteArray(item.openStream());                        
                        DataFile file = new DataFile();
                        file.setContentType(type);
                        file.setFileName(name);
                        file.setByteData(bytes);                           
                        files.add(file);                        
                    }
                }
            } catch (FileUploadException ex) {
                logger.warn("Missing event or user parameter. Illegal configuration.", ex);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
            saveFile(files,formInput, request, response);
        } catch (Exception e) {
            logger.error("Upload File error", e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    protected String getContentFromItem(FileItemStream item) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(item.openStream(), writer);
        return writer.toString();
    }

    protected abstract void saveFile(List<DataFile> files, Map<String,String> formFields, HttpServletRequest request, HttpServletResponse response) throws IOException;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "POST REQUEST REQUIRED");
    }   
}
