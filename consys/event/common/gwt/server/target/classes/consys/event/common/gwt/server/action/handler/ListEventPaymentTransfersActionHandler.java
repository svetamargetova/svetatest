package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.client.ui.comp.list.ListDataSourceResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.utils.collection.Lists;
import consys.event.common.gwt.client.action.ListTransfersAction;
import consys.event.common.gwt.client.bo.ClientPaymentTransferHead;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.TransferUtils;
import consys.payment.webservice.TransferWebService;
import consys.payment.ws.transfer.ListTransfersRequest;
import consys.payment.ws.transfer.ListTransfersResponse;
import consys.payment.ws.transfer.TransferHead;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ListEventPaymentTransfersActionHandler implements EventActionHandler<ListTransfersAction, ListDataSourceResult>{

    @Autowired
    private TransferWebService transferWebService;

    @Override
    public Class<ListTransfersAction> getActionType() {
        return ListTransfersAction.class;
    }

    @Override
    public ListDataSourceResult execute(ListTransfersAction action) throws ActionException {
        ListTransfersRequest request = TransferWebService.OBJECT_FACTORY.createListTransfersRequest();
        request.setItemsPerPage(action.getItemsPerPage());
        request.setStartIndex(action.getFirstResult());
        ListTransfersResponse response = transferWebService.listEventTransfers(request);
        ArrayList<ClientPaymentTransferHead> transfers = Lists.newArrayList();
        for(TransferHead h : response.getTransfers()){
            transfers.add(TransferUtils.toHead(h));
        }
        return new ListDataSourceResult(transfers, response.getAllCount());
    }    
}
