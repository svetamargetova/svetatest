package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.exceptions.PermissionException;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.event.common.core.service.ParticipantManagerService;
import consys.event.common.gwt.client.action.ListUserPaymentProfileDialogAction;
import consys.event.common.gwt.client.bo.ClientPaymentProfileDialog;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.payment.webservice.ProfileWebService;
import consys.payment.ws.bo.Profile;
import consys.payment.ws.profile.ListUserProfilesRequest;
import consys.payment.ws.profile.ListUserProfilesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author pepa
 */
public class ListUserPaymentProfileDialogActionHandler implements EventActionHandler<ListUserPaymentProfileDialogAction, ClientPaymentProfileDialog> {

    // logger
    private static final Logger log = LoggerFactory.getLogger(ListUserPaymentProfileDialogActionHandler.class);
    // sluzby
    @Autowired
    private ProfileWebService profileWebService;
    @Autowired
    private ParticipantManagerService managerService;

    @Override
    public Class<ListUserPaymentProfileDialogAction> getActionType() {
        return ListUserPaymentProfileDialogAction.class;
    }

    @Override
    public ClientPaymentProfileDialog execute(ListUserPaymentProfileDialogAction action) throws ActionException {
        final boolean managedListPayment = action.getManagedUserUuid() != null;
        if (managedListPayment) {
            // zjistime jestli je uzivatel spravce uzivatele
            if (!managerService.checkManagedUser(action.getUserEventUuid(), action.getManagedUserUuid())) {
                throw new PermissionException();
            }
        }

        ListUserProfilesRequest profileReq = ProfileWebService.OBJECT_FACTORY.createListUserProfilesRequest();
        profileReq.setUserUuid(action.getUserEventUuid());
        profileReq.setManagedUserUuid(action.getManagedUserUuid());
        ListUserProfilesResponse profilesResponse = profileWebService.listUserProfiles(profileReq);

        ClientPaymentProfileDialog profileDialog = new ClientPaymentProfileDialog();

        ClientUserPaymentProfile profile = new ClientUserPaymentProfile();
        if (!CollectionUtils.isEmpty(profilesResponse.getProfiles())) {
            Profile p = profilesResponse.getProfiles().get(0);
            profile = ProfileUtils.toClientUserProfile(p);
        }
        profileDialog.setFirstProfile(profile);
        profileDialog.setProfileThumbs(ProfileUtils.toClientUserProfileThumbs(profilesResponse.getProfiles()));
        return profileDialog;
    }
}
