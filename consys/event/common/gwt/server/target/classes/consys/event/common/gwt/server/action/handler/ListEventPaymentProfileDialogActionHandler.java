package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.event.common.gwt.client.action.ListEventPaymentProfileDialogAction;
import consys.event.common.gwt.client.bo.ClientPaymentProfileDialog;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.payment.webservice.EventWebService;
import consys.payment.ws.bo.EventProfile;
import consys.payment.ws.event.ListEventProfilesRequest;
import consys.payment.ws.event.ListEventProfilesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author pepa
 */
public class ListEventPaymentProfileDialogActionHandler implements EventActionHandler<ListEventPaymentProfileDialogAction, ClientPaymentProfileDialog> {

    @Autowired
    private EventWebService eventWebService;    

    @Override
    public Class<ListEventPaymentProfileDialogAction> getActionType() {
        return ListEventPaymentProfileDialogAction.class;
    }

    @Override
    public ClientPaymentProfileDialog execute(ListEventPaymentProfileDialogAction action) throws ActionException {

        ListEventProfilesRequest profileReq = EventWebService.OBJECT_FACTORY.createListEventProfilesRequest();
        profileReq.setEventUuid(action.getEventUuid());
        ListEventProfilesResponse profilesResponse = eventWebService.listEventProfiles(profileReq);


        ClientPaymentProfileDialog profileDialog = new ClientPaymentProfileDialog();

        ClientUserPaymentProfile profile = new ClientUserPaymentProfile();
        if (!CollectionUtils.isEmpty(profilesResponse.getProfiles())) {
            EventProfile p = profilesResponse.getProfiles().get(0);
            profile = ProfileUtils.toClientEventProfile(p);
        }
        profileDialog.setFirstProfile(profile);
        profileDialog.setProfileThumbs(ProfileUtils.toClientEventProfileThumbs(profilesResponse.getProfiles()));
        return profileDialog;
    }   
}
