/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.gwt.server;

import com.google.gwt.user.server.rpc.RPC;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.RPCRequest;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.google.gwt.user.server.rpc.SerializationPolicy;
import com.google.gwt.user.server.rpc.SerializationPolicyLoader;
import consys.common.gwt.server.action.server.ActionHandler;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.common.gwt.shared.exception.UncaughtException;
import consys.common.gwt.shared.action.EventAction;
import consys.event.common.gwt.client.rpc.service.EventDispatchService;
import consys.event.overseer.connector.EventRequestContextProvider;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * DispatchServle prevola akciu GWT.
 *
 *
 * @author palo
 */
public class DispatchServiceServlet extends RemoteServiceServlet implements EventDispatchService {

    private static final long serialVersionUID = 2281668758186963114L;
    private static final Logger logger = LoggerFactory.getLogger(DispatchServiceServlet.class);
    private SpringBeanContextEventHandlerRegistry handlerRegistry;
    private EventRequestContextProvider contextProvider;

    /**
     * PO inicializacii vlozi do contetextu
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
        logger.info("Dispatch servlet initialized... ");
    }

    @Override
    protected SerializationPolicy doGetSerializationPolicy(HttpServletRequest request, String moduleBaseURL, String strongName) {
        logger.debug("Load serialization policy " + moduleBaseURL + " " + strongName);

        String serializationPolicyFilePath = SerializationPolicyLoader.getSerializationPolicyFileName(moduleBaseURL + strongName);

        SerializationPolicy serializationPolicy = null;
        // Open the RPC resource file and read its contents.
        logger.debug("Downloading: "+serializationPolicyFilePath);
        InputStream is = null;
        try {
            is = new URL(serializationPolicyFilePath).openStream();
        } catch (IOException ex) {
            logger.error("Error: open stream for serialization policy at " + serializationPolicyFilePath, ex);
        }


        try {
            if (is != null) {
                try {
                    serializationPolicy = SerializationPolicyLoader.loadFromStream(is,
                            null);
                } catch (ParseException e) {
                    log("ERROR: Failed to parse the policy file '"
                            + serializationPolicyFilePath + "'", e);
                } catch (IOException e) {
                    log("ERROR: Could not read the policy file '"
                            + serializationPolicyFilePath + "'", e);
                }
            } else {
                String message = "ERROR: The serialization policy file '"
                        + serializationPolicyFilePath
                        + "' was not found; did you forget to include it in this deployment?";
                log(message);
            }
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // Ignore this error
                }
            }
        }
        return serializationPolicy;
    }

    /*
     * TODO: SpringSecurity exceptions
     */
    @Override
    public Result execute(EventAction<?> action) throws ActionException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Dispatching -> " + action.getClass().getSimpleName());
            }
            contextProvider.initContext(action.getUserEventUuid(), action.getEventUuid());
            ActionHandler handler = handlerRegistry.findHandler(action);
            return handler.execute(action);
        } catch (IllegalArgumentException e) {
            logger.debug("Initializing context failed! ", e);
            throw new ServiceFailedException();
        } catch (NullPointerException e) {
            String error = "NullPointerException: " + action.getClass().getSimpleName() + ": " + e.getMessage();
            if (logger.isErrorEnabled()) {
                logger.error(error, e);
            }
            throw new UncaughtException(error);
        } catch (RuntimeException e) {
            String error = "RuntimeException: " + action.getClass().getSimpleName() + ": " + e.getMessage();
            if (logger.isErrorEnabled()) {
                logger.error(error, e);
            }
            throw new UncaughtException(error);
        } catch (ActionException a) {
            logger.debug("Action exception" + a);
            throw a;
        }
    }



    @Override
    public String processCall(String payload) throws SerializationException {
        try {
            RPCRequest rpcRequest = RPC.decodeRequest(payload, this.getClass(), this);
            // delegate work to the spring injected service
            return RPC.invokeAndEncodeResponse(this, rpcRequest.getMethod(), rpcRequest.getParameters(), rpcRequest.getSerializationPolicy());
        } catch (IncompatibleRemoteServiceException e) {
            logger.debug("Error", e);
            return RPC.encodeResponseForFailure(null, e);
        }
    }

    /**
     * @param handlerRegistry the handlerRegistry to set
     */
    @Autowired
    @Required
    public void setHandlerRegistry(SpringBeanContextEventHandlerRegistry handlerRegistry) {
        this.handlerRegistry = handlerRegistry;
    }

    /**
     * @param contextProvider the contextProvider to set
     */
    @Autowired
    @Required
    public void setContextProvider(EventRequestContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }
}
