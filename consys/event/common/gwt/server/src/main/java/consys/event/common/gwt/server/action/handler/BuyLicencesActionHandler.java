package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.gwt.client.action.BuyLicencesAction;
import consys.common.gwt.shared.exception.IbanException;
import consys.event.common.gwt.client.action.exception.LicensePurchasedException;
import consys.event.common.gwt.client.action.exception.ProfileNotFilledException;
import consys.event.common.gwt.client.bo.ClientEventConfirmed;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.event.payment.webservice.utils.ConvertUtils;
import consys.payment.webservice.EventWebService;
import consys.payment.ws.bo.Profile;
import consys.payment.ws.event.BuyLicenseExceptionEnum;
import consys.payment.ws.event.BuyLicenseRequest;
import consys.payment.ws.event.BuyLicenseResponse;
import consys.payment.ws.event.BuyLicenseWithProfileRequest;
import consys.payment.ws.event.BuyLicenseWithProfileResponse;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author pepa
 */
public class BuyLicencesActionHandler implements EventActionHandler<BuyLicencesAction, ClientEventConfirmed> {

    private EventWebService eventWebService;

    @Override
    public Class<BuyLicencesAction> getActionType() {
        return BuyLicencesAction.class;
    }

    @Override
    public ClientEventConfirmed execute(BuyLicencesAction action) throws ActionException {
        BuyLicenseExceptionEnum exception;
        Date licenseIssuedDate;

        if (action.isProfileEdited()) {
            Profile profile = ProfileUtils.toPortalProfile(action.getEditedProfile());
            profile.setUuid(action.getCustomerProfileUuid());

            BuyLicenseWithProfileRequest request = EventWebService.OBJECT_FACTORY.createBuyLicenseWithProfileRequest();
            request.setProfile(profile);
            BuyLicenseWithProfileResponse response = eventWebService.buyLicenseWithProfile(request);
            exception = response.getException();
            licenseIssuedDate = ConvertUtils.toDate(response.getInvoiceIssuedDate());
        } else {
            BuyLicenseRequest request = EventWebService.OBJECT_FACTORY.createBuyLicenseRequest();
            request.setProfileUuid(action.getCustomerProfileUuid());
            BuyLicenseResponse response = eventWebService.buyLicense(request);
            exception = response.getException();
            licenseIssuedDate = ConvertUtils.toDate(response.getInvoiceIssuedDate());
        }

        if (exception == null) {
            ClientEventConfirmed result = new ClientEventConfirmed();
            // TADY JE POTREBA SETOVAT DATA Z PAYMENTU
            if (licenseIssuedDate == null) {
                licenseIssuedDate = new Date();
            }
            result.setFrom(licenseIssuedDate);
            result.setTo(DateUtils.addMonths(licenseIssuedDate, 15));
            return result;
        } else {
            if (BuyLicenseExceptionEnum.EVENT_LICENSE_ALREADY_PURCHASED.equals(exception)) {
                throw new LicensePurchasedException();
            } else if (BuyLicenseExceptionEnum.EVENT_PROFILE_NOT_FILLED_EXCEPTION.equals(exception)) {
                throw new ProfileNotFilledException();
            } else if (BuyLicenseExceptionEnum.IBAN_FORMAT_EXCEPTION.equals(exception)) {
                throw new IbanException();
            } else if (BuyLicenseExceptionEnum.NO_RECORD_EXCEPTION.equals(exception)) {
                throw new NoRecordsForAction();
            } else if (BuyLicenseExceptionEnum.REQUIRED_PROPERTY_EXCEPTION.equals(exception)) {
                throw new BadInputException();
            } else {
                throw new ServiceFailedException();
            }
        }
    }

    /**
     * @param eventWebService the eventWebService to set
     */
    @Autowired
    @Required
    public void setEventWebService(EventWebService eventWebService) {
        this.eventWebService = eventWebService;
    }
}
