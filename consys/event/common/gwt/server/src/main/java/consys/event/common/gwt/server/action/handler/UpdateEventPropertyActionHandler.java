package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.common.gwt.client.rpc.action.UpdateEventPropertyAction;
import consys.common.gwt.client.rpc.result.VoidResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.gwt.server.EventActionHandler;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class UpdateEventPropertyActionHandler implements EventActionHandler<UpdateEventPropertyAction, VoidResult> {

    private SystemPropertyService propertyService;

    @Override
    public Class<UpdateEventPropertyAction> getActionType() {
        return UpdateEventPropertyAction.class;
    }

    @Override
    public VoidResult execute(UpdateEventPropertyAction action) throws ActionException {
        try {
            if (action.getPropertyKey() != null) {
                propertyService.updateProperty(action.getPropertyKey(), action.getPropertyValue());
            } else {
                for (Entry<String, String> e : action.getProperties().entrySet()) {
                    propertyService.updateProperty(e.getKey(), e.getValue());
                }
            }
            return VoidResult.RESULT();
        } catch (IllegalPropertyValue ex) {
            throw new BadInputException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }

    /**
     * @param propertyService the propertyService to set
     */
    @Autowired
    @Required
    public void setPropertyService(SystemPropertyService propertyService) {
        this.propertyService = propertyService;
    }
}
