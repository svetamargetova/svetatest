package consys.event.common.gwt.server.list;

import consys.event.common.api.list.VideoFilesUserList;
import consys.event.common.core.bo.EventVideo;
import consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem;
import consys.event.common.gwt.server.GwtListHandler;
import consys.event.common.gwt.server.utils.VideoUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class VideosListHandler implements GwtListHandler<ClientVideoFilesListItem, EventVideo>, VideoFilesUserList {

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public void execute(List<EventVideo> in, ArrayList<ClientVideoFilesListItem> out) {
        for (EventVideo item : in) {
            out.add(VideoUtils.toClient(item));
        }
    }
}
