package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.bo.thumb.ListLongItem;
import consys.event.common.core.service.CommitteeService;
import consys.event.common.gwt.client.action.LoadCommitteesAction;
import consys.event.common.gwt.client.bo.ClientCommitteeThumb;
import consys.event.common.gwt.server.EventActionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadCommitteesActionHandler implements EventActionHandler<LoadCommitteesAction, ArrayListResult<ClientCommitteeThumb>> {

    private CommitteeService committeeService;

    @Override
    public Class<LoadCommitteesAction> getActionType() {
        return LoadCommitteesAction.class;
    }

    @Override
    public ArrayListResult<ClientCommitteeThumb> execute(LoadCommitteesAction action) throws ActionException {
        try {
            List<ListLongItem> items = committeeService.listCommitteesForUser(action.getUserEventUuid());
            ArrayList<ClientCommitteeThumb> out = Lists.newArrayList();
            for (ListLongItem listLongItem : items) {
                ClientCommitteeThumb cc = new ClientCommitteeThumb();
                cc.setId(listLongItem.getId());
                cc.setName(listLongItem.getName());
                out.add(cc);
            }
            return new ArrayListResult<ClientCommitteeThumb>(out);
        } catch (NoRecordException ex) {
            // vubec by nemelo nastat
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            // chyba uuid
            throw new BadInputException();
        }
    }

    /**
     * @param committeeService the committeeService to set
     */
    @Autowired(required = true)
    public void setCommitteeService(CommitteeService committeeService) {
        this.committeeService = committeeService;
    }
}
