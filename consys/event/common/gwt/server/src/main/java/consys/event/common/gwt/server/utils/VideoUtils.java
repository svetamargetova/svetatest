package consys.event.common.gwt.server.utils;

import consys.event.common.core.bo.EventVideo;
import consys.event.common.core.bo.enums.EventVideoState;
import consys.event.common.gwt.client.bo.list.ClientVideoFilesListItem;
import consys.event.common.gwt.client.bo.list.VideoRecordState;

/**
 *
 * @author pepa
 */
public class VideoUtils {

    /** prevede serverovy stav na klientsky */
    public static VideoRecordState state(EventVideoState state) {
        if (state == null) {
            return null;
        }
        switch (state) {
            case UPLOADING:
                return VideoRecordState.UPLOADING;
            case SUPERLECTURES_PROCESSED:
                return VideoRecordState.PROCESSED;
            case PROCESS_ERROR:
                return VideoRecordState.PROCESS_ERROR;
            case UPLOADED:
                return VideoRecordState.UPLOADED;
            case TAKEPLACE_PROCESSING:
            case TAKEPLACE_PROCESSED:
            case SUPERLECTURES_PROCESSING:
                return VideoRecordState.PROCESSING;
            default:
                return null;
        }
    }

    /** vytvori klientsky objekt reprezentujici video */
    public static ClientVideoFilesListItem toClient(EventVideo ev) {
        ClientVideoFilesListItem video = new ClientVideoFilesListItem();
        video.setAuthors(ev.getAuthors());
        video.setCustomName(ev.getCustomName());
        video.setDescription(ev.getDescription());
        video.setSpeakers(ev.getSpeakers());
        video.setState(VideoRecordState.getStateById(ev.getState().getId()));
        video.setSystemName(ev.getSystemName());
        video.setProcessing(ev.isToProcessing() == null ? false : ev.isToProcessing());
        video.setUrl(ev.getVideoUrl());
        return video;
    }

    /** vytvori klientsky objekt reprezentujici video, s vyplnenym uuid */
    public static ClientVideoFilesListItem toClientWithUuid(EventVideo ev) {
        ClientVideoFilesListItem video = toClient(ev);
        video.setUuid(ev.getUuid());
        return video;
    }
}
