package consys.event.common.gwt.server.action.handler.utils;

import consys.common.constants.currency.CurrencyEnum;
import consys.common.gwt.shared.bo.ClientEventPaymentProfile;
import consys.common.gwt.shared.bo.ClientPaymentProfileThumb;
import consys.common.gwt.shared.bo.ClientUserPaymentProfile;
import consys.common.utils.collection.Lists;
import consys.payment.ws.bo.Currency;
import consys.payment.ws.bo.EventProfile;
import consys.payment.ws.bo.Profile;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProfileUtils {

    public static Profile toPortalProfile(ClientUserPaymentProfile p) {
        Profile profile = new Profile();
        profile.setCity(p.getCity());
        profile.setCountry(p.getCountry());
        profile.setIBAN(p.getAccountNo());
        profile.setName(p.getName());
        profile.setRegistrationNo(p.getRegNo());
        profile.setSWIFT(p.getBankNo());
        profile.setStreet(p.getStreet());
        profile.setUuid(p.getUuid());
        profile.setVatNo(p.getVatNo());
        profile.setZip(p.getZip());
        profile.setNoVatPayer(p.isNoVatPayer());
        return profile;
    }

    public static EventProfile toPortalEventProfile(ClientEventPaymentProfile p) {
        Profile profile = new Profile();
        profile.setCity(p.getCity());
        profile.setCountry(p.getCountry());
        profile.setIBAN(p.getAccountNo());
        profile.setName(p.getName());
        profile.setRegistrationNo(p.getRegNo());
        profile.setSWIFT(p.getBankNo());
        profile.setStreet(p.getStreet());
        profile.setUuid(p.getUuid());
        profile.setVatNo(p.getVatNo());
        profile.setZip(p.getZip());
        profile.setNoVatPayer(p.isNoVatPayer());

        EventProfile eventProfile = new EventProfile();
        eventProfile.setProfile(profile);
        eventProfile.setBankInvoicedToProfile(p.isBankOrderAsProfile());
        switch (p.getCurrency()) {
            case CZK:
                eventProfile.setCurrency(Currency.CZK);
                break;
            case EUR:
                eventProfile.setCurrency(Currency.EUR);
                break;
            case USD:
                eventProfile.setCurrency(Currency.USD);
                break;
        }
        eventProfile.setInvoiceNote(p.getInvoiceNote());
        return eventProfile;
    }

    public static ClientUserPaymentProfile toClientUserProfile(Profile p) {
        ClientUserPaymentProfile profile = new ClientUserPaymentProfile();
        profile.setAccountNo(p.getIBAN());
        profile.setBankNo(p.getSWIFT());
        profile.setCity(p.getCity());
        profile.setCountry(p.getCountry());
        profile.setName(p.getName());
        profile.setRegNo(p.getRegistrationNo());
        profile.setStreet(p.getStreet());
        profile.setUuid(p.getUuid());
        profile.setVatNo(p.getVatNo());
        profile.setZip(p.getZip());
        profile.setNoVatPayer(p.isNoVatPayer() == null ? false : p.isNoVatPayer());
        return profile;

    }

    public static ClientEventPaymentProfile toClientEventProfile(EventProfile ep) {
        ClientEventPaymentProfile profile = new ClientEventPaymentProfile();
        Profile p = ep.getProfile();
        profile.setAccountNo(p.getIBAN());
        profile.setBankNo(p.getSWIFT());
        profile.setCity(p.getCity());
        profile.setCountry(p.getCountry());
        profile.setName(p.getName());
        profile.setRegNo(p.getRegistrationNo());
        profile.setStreet(p.getStreet());
        profile.setUuid(p.getUuid());
        profile.setVatNo(p.getVatNo());
        profile.setZip(p.getZip());
        profile.setNoVatPayer(p.isNoVatPayer() == null ? false : p.isNoVatPayer());
        // event        
        switch (ep.getCurrency()) {
            case CZK:
                profile.setCurrency(CurrencyEnum.CZK);
                break;
            case EUR:
                profile.setCurrency(CurrencyEnum.EUR);
                break;
            case USD:
                profile.setCurrency(CurrencyEnum.USD);
                break;
        }
        profile.setInvoiceNote(ep.getInvoiceNote());
        profile.setBankOrderAsProfile(ep.isBankInvoicedToProfile());
        return profile;
    }

    public static ClientPaymentProfileThumb toClientProfileThumb(Profile profile) {
        ClientPaymentProfileThumb thumb = new ClientUserPaymentProfile();
        StringBuilder sb;
        if (StringUtils.isBlank(profile.getName())) {
            sb = new StringBuilder('-');
        } else {
            sb = new StringBuilder(profile.getName());
        }

        if (StringUtils.isBlank(profile.getStreet())) {
            sb.append(", ");
            sb.append(profile.getStreet());
        }
        if (StringUtils.isBlank(profile.getCity())) {
            sb.append(", ");
            sb.append(profile.getCity());
        }
        thumb.setUuid(profile.getUuid());
        thumb.setName(sb.toString());

        return thumb;
    }

    public static ArrayList<ClientPaymentProfileThumb> toClientUserProfileThumbs(List<Profile> profiles) {
        ArrayList<ClientPaymentProfileThumb> profilesThumbs = Lists.newArrayList();
        for (Profile profile : profiles) {
            profilesThumbs.add(toClientProfileThumb(profile));
        }
        return profilesThumbs;
    }

    public static ArrayList<ClientPaymentProfileThumb> toClientEventProfileThumbs(List<EventProfile> profiles) {
        ArrayList<ClientPaymentProfileThumb> profilesThumbs = Lists.newArrayList();
        for (EventProfile profile : profiles) {
            profilesThumbs.add(toClientProfileThumb(profile.getProfile()));
        }
        return profilesThumbs;
    }

    public static Currency toPortalCurrency(CurrencyEnum currency) {
        switch (currency) {
            case CZK:
                return Currency.CZK;
            case EUR:
                return Currency.EUR;
            case USD:
                return Currency.USD;
        }
        return null;
    }
}
