package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.EventUser;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.CommitteeService;
import consys.event.common.core.utils.ModuleRightUtils;
import consys.event.common.gwt.client.action.LoadCommitteeAction;
import consys.event.common.gwt.client.bo.ClientCommittee;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.EventUserUtils;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class LoadCommitteeActionHandler implements EventActionHandler<LoadCommitteeAction, ClientCommittee> {

    private CommitteeService committeeService;
    private ModuleRightUtils moduleRightUtils;

    @Override
    public Class<LoadCommitteeAction> getActionType() {
        return LoadCommitteeAction.class;
    }

    @Override
    public ClientCommittee execute(LoadCommitteeAction action) throws ActionException {
        try {
            Committee committee = committeeService.loadCommitteeWithRightsAndMembers(action.getId());
            ClientCommittee co = new ClientCommittee();
            co.setId(action.getId());
            co.setName(committee.getName());
            // Nastavime chaira
            if (!committee.getChairGroup().getMembers().isEmpty()) {
                UserEvent chair = committee.getChairGroup().getMembers().iterator().next();                
                co.setChair(EventUserUtils.toEventUser(chair));
            }
            // Nastavime chairove prava
            co.setChairRights((ArrayList) moduleRightUtils.translateToClient(committee.getChairGroup().getRights()));

            // Nastavime uuid clenov
            for (UserEvent ue : committee.getGroup().getMembers()) {                
                co.getMemebers().add(EventUserUtils.toEventUser(ue));
            }                        
                        
            // Nastavime prava clenov
            co.setRights((ArrayList) moduleRightUtils.translateToClient(committee.getGroup().getRights()));

            return co;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }

    }

    /**
     * @param committeeService the committeeService to set
     */
    @Autowired(required = true)
    public void setCommitteeService(CommitteeService committeeService) {
        this.committeeService = committeeService;
    }

    /**
     * @param moduleRightUtils the moduleRightUtils to set
     */
    @Autowired(required = true)
    public void setModuleRightUtils(ModuleRightUtils moduleRightUtils) {
        this.moduleRightUtils = moduleRightUtils;
    }
}
