package consys.event.common.gwt.server.list.export.xls;

import org.apache.poi.hssf.usermodel.HSSFSheet;

/**
 *
 * @author pepa
 */
public class SheetInfo {

    // konstanty
    public static final int START_ROW_COUNT = 1;
    // data
    private HSSFSheet sheet;
    private int rowCount;

    public SheetInfo(HSSFSheet sheet) {
        this.sheet = sheet;
        this.rowCount = START_ROW_COUNT;
    }

    public int increseAndGetRows() {
        return rowCount++;
    }

    public HSSFSheet getSheet() {
        return sheet;
    }
}
