package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.action.LoadUserInfoAction;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientUserCommonInfo;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.common.gwt.server.EventActionHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadUserInfoActionHandler implements EventActionHandler<LoadUserInfoAction, ClientUserCommonInfo> {

    @Autowired
    private UserEventService userEventService;

    @Override
    public Class<LoadUserInfoAction> getActionType() {
        return LoadUserInfoAction.class;
    }

    @Override
    public ClientUserCommonInfo execute(LoadUserInfoAction action) throws ActionException {
        try {
            UserEvent user = userEventService.loadUserEvent(action.getUserUuid());

            ClientUserCommonInfo info = new ClientUserCommonInfo();
            info.setEmail(user.getEmail());
            info.setFullName(user.getFullName());
            info.setImageUuid(user.getProfileImagePrefix());
            info.setOrganization(user.getOrganization());
            info.setPosition(user.getPosition());
            info.setUuid(user.getUuid());
            return info;
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        }
    }
}
