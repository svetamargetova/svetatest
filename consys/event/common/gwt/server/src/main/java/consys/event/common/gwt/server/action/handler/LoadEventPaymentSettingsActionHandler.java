package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.ClientPaymentProfileThumb;
import consys.event.common.gwt.client.action.LoadEventPaymentSettingsAction;
import consys.event.common.gwt.client.bo.ClientPaymentSettings;
import consys.event.common.gwt.server.EventActionHandler;
import consys.event.common.gwt.server.action.handler.utils.ProfileUtils;
import consys.payment.webservice.EventWebService;
import consys.payment.ws.bo.EventProfile;
import consys.payment.ws.event.LoadEventPaymentDetailsRequest;
import consys.payment.ws.event.LoadEventPaymentDetailsResponse;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author pepa
 */
public class LoadEventPaymentSettingsActionHandler implements EventActionHandler<LoadEventPaymentSettingsAction, ClientPaymentSettings> {

    @Autowired
    private EventWebService eventWebService;

    @Override
    public Class<LoadEventPaymentSettingsAction> getActionType() {
        return LoadEventPaymentSettingsAction.class;
    }

    @Override
    public ClientPaymentSettings execute(LoadEventPaymentSettingsAction action) throws ActionException {
        ClientPaymentSettings settings = new ClientPaymentSettings();

        LoadEventPaymentDetailsRequest req = EventWebService.OBJECT_FACTORY.createLoadEventPaymentDetailsRequest();
        req.setEventUuid(action.getEventUuid());
        LoadEventPaymentDetailsResponse resp = eventWebService.loadEventPaymentDetails(req);

        // Prvy profile
        if (CollectionUtils.isEmpty(resp.getProfiles())) {
            settings.setProfileThumbs(new ArrayList<ClientPaymentProfileThumb>());
            settings.getProfileThumbs().add(new ClientPaymentProfileThumb("", ""));
        } else {
            settings.setProfileThumbs(ProfileUtils.toClientEventProfileThumbs(resp.getProfiles()));
            EventProfile p = resp.getProfiles().get(0);
            settings.setFirstProfile(ProfileUtils.toClientEventProfile(p));
        }
        return settings;
    }
}
