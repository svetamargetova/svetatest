/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.gwt.server;

import consys.common.gwt.shared.action.Result;
import consys.common.gwt.shared.action.EventAction;
import org.springframework.stereotype.Component;

/**
 * HandlerRegister zalozeny na tom ze vsetky EventActionHandler instancie su automaticky
 * umiestnene v ApplicationContexte. Takze po jeho nabehnutimozeme ich mozeme
 * autoamticky zaregistrovat vsetky naraz.
 * 
 * @author palo
 */

@Component
public class SpringBeanContextEventHandlerRegistry extends EventActionHandlerRegistry {
    
    @Override
    public <A extends EventAction<R>, R extends Result> EventActionHandler<A, R> findHandler(A action) {
        return (EventActionHandler<A, R>) getHandlers().get(action.getClass());
    }

    @Override
    public void clearHandlers() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    
}
