package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.utils.collection.Lists;
import consys.common.gwt.client.rpc.result.LongResult;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.InvitedUser;
import consys.common.gwt.shared.exception.BadInputException;
import consys.common.gwt.shared.exception.NoRecordsForAction;
import consys.common.gwt.shared.exception.ServiceFailedException;
import consys.event.common.core.bo.Committee;
import consys.event.common.core.bo.thumb.ExistingUserInvitation;
import consys.event.common.core.bo.thumb.Invitation;
import consys.event.common.core.bo.thumb.NewUserInvitation;
import consys.event.common.core.service.CommitteeService;
import consys.event.common.core.utils.ModuleRightUtils;
import consys.event.common.gwt.client.action.CreateCommitteeAction;
import consys.event.common.gwt.server.EventActionHandler;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class CreateCommitteeActionHandler implements EventActionHandler<CreateCommitteeAction, LongResult> {

    private CommitteeService committeeService;
    private ModuleRightUtils moduleRightUtils;

    @Override
    public Class<CreateCommitteeAction> getActionType() {
        return CreateCommitteeAction.class;
    }

    @Override
    public LongResult execute(CreateCommitteeAction action) throws ActionException {
        try {
            // Prelozime UID roli na nazvy roli
            List<String> chairRights = moduleRightUtils.translateFromClient(action.getChairRights());
            List<String> memberRights = moduleRightUtils.translateFromClient(action.getRights());
            // Vytvorime vybor
            Invitation chair = null;
            if(action.getChair() != null){
                if(action.getChair().isUserNew()){
                    chair = new NewUserInvitation(action.getChair().getEmail(), action.getChair().getName());
                }else{
                    chair = new ExistingUserInvitation(action.getChair().getUuid());
                }
            }

            List<Invitation> members = Lists.newArrayList();
            Invitation newMember = null;
            for(InvitedUser member : action.getMembers()){
                if(member.isUserNew()){
                    newMember = new NewUserInvitation(member.getEmail(), member.getName());
                }else{
                    newMember = new ExistingUserInvitation(member.getUuid());
                }
                members.add(newMember);
            }

            Committee c = committeeService.createCommittee(action.getName(), chair, chairRights, members,memberRights );

            return new LongResult(c.getId());
        } catch (ServiceExecutionFailed ex) {
            throw new ServiceFailedException();
        } catch (NoRecordException ex) {
            throw new NoRecordsForAction();
        } catch (RequiredPropertyNullException ex) {
            throw new BadInputException();
        }
    }

    /**
     * @param committeeService the committeeService to set
     */
    @Autowired
    @Required
    public void setCommitteeService(CommitteeService committeeService) {
        this.committeeService = committeeService;
    }

    /**
     * @param moduleRightUtils the moduleRightUtils to set
     */
    @Autowired
    @Required
    public void setModuleRightUtils(ModuleRightUtils moduleRightUtils) {
        this.moduleRightUtils = moduleRightUtils;
    }
    




}
