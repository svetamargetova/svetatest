/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.gwt.server.action.handler;

import consys.common.core.exception.NoRecordException;
import consys.common.gwt.client.rpc.result.ArrayListResult;
import consys.common.gwt.shared.action.ActionException;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.service.ModuleRightService;
import consys.event.common.core.utils.ModuleRightUtils;
import consys.event.common.gwt.client.action.LoadUserEventRolesAction;
import consys.event.common.gwt.server.EventActionHandler;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author palo
 */
public class LoadUserEventRolesActionHandler implements EventActionHandler<LoadUserEventRolesAction, ArrayListResult<String>> {

    private static final Logger logger = LoggerFactory.getLogger(LoadEventSettingsActionHandler.class);
    @Autowired
    private ModuleRightService moduleRightService;
    @Autowired
    private ModuleRightUtils moduleRightUtils;

    @Override
    public Class<LoadUserEventRolesAction> getActionType() {
        return LoadUserEventRolesAction.class;
    }

    @Override
    public ArrayListResult<String>  execute(LoadUserEventRolesAction action) throws ActionException {        
        String userUuid = action.getUserEventUuid();
        if (StringUtils.isNotBlank(userUuid)) {
            try {
                List<ModuleRight> rights = moduleRightService.listUserRightsByUuid(userUuid);
                try {                                        
                    return new ArrayListResult((ArrayList<String>) moduleRightUtils.translateToClient(rights));
                } catch (NullPointerException e) {
                    logger.error("Error: Translate rights", e);
                }
            } catch (NoRecordException ex) {
                logger.debug("User has no rights");
            }
        } else {
            logger.debug("Context UserUuid not set");
        }        
        return new ArrayListResult();
    }  
}
