package consys.event.common.gwt.server.action.handler;

import consys.common.core.bo.SystemProperty;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.common.gwt.shared.action.ActionException;
import consys.common.gwt.shared.bo.EventSettings;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.service.ModuleRightService;
import consys.event.common.core.utils.ModuleRightUtils;
import consys.event.common.gwt.client.action.LoadEventSettingsAction;
import consys.event.common.gwt.server.EventActionHandler;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 *
 * @author Palo
 */
public class LoadEventSettingsActionHandler implements EventActionHandler<LoadEventSettingsAction, EventSettings> {

    private static final Logger logger = LoggerFactory.getLogger(LoadEventSettingsActionHandler.class);
    private ModuleRightService moduleRightService;
    private SystemPropertyService systemPropertyService;
    private ModuleRightUtils moduleRightUtils;

    @Override
    public Class<LoadEventSettingsAction> getActionType() {
        return LoadEventSettingsAction.class;
    }

    @Override
    public EventSettings execute(LoadEventSettingsAction action) throws ActionException {
        EventSettings settings = new EventSettings();
        String userUuid = action.getUserEventUuid();
        if (StringUtils.isNotBlank(userUuid)) {
            try {
                List<ModuleRight> rights = moduleRightService.listUserRightsByUuid(userUuid);
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("{} user has rights:", action.getUserEventUuid());
                    }
                    settings.setRoles((ArrayList<String>) moduleRightUtils.translateToClient(rights));
                } catch (NullPointerException e) {
                    logger.error("Error: Translate rights", e);
                }
            } catch (NoRecordException ex) {
                logger.debug("User has no rights");
            }
        } else {
            logger.debug("Context UserUuid not set");
        }

        logger.debug("Loading properties...");

        List<SystemProperty> propesties = systemPropertyService.listProperiesToClient();
        for (SystemProperty systemProperty : propesties) {
            settings.getSystemProperties().put(systemProperty.getKey(), systemProperty.getValue());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("System properties:");
            for (int i = 0; i < propesties.size(); i++) {
                SystemProperty r = propesties.get(i);
                logger.debug("  {}", r);
            }
        }
        return settings;
    }

    /**
     * @return the moduleRightService
     */
    public ModuleRightService getModuleRightService() {
        return moduleRightService;
    }

    /**
     * @param moduleRightService the moduleRightService to set
     */
    @Autowired
    @Required
    public void setModuleRightService(ModuleRightService moduleRightService) {
        this.moduleRightService = moduleRightService;
    }

    /**
     * @return the systemPropertyService
     */
    public SystemPropertyService getSystemPropertyService() {
        return systemPropertyService;
    }

    /**
     * @param systemPropertyService the systemPropertyService to set
     */
    @Autowired
    @Required
    public void setSystemPropertyService(SystemPropertyService systemPropertyService) {
        this.systemPropertyService = systemPropertyService;

    }

    @Autowired
    @Required
    public void setModuleRightUtils(ModuleRightUtils moduleRightUtils) {
        this.moduleRightUtils = moduleRightUtils;
    }
}
