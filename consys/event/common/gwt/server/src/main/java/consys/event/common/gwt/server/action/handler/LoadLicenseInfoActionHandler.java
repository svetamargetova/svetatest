package consys.event.common.gwt.server.action.handler;

import consys.common.gwt.shared.action.ActionException;
import consys.common.utils.ws.Convert;
import consys.event.common.gwt.client.action.LoadLicenseInfoAction;
import consys.event.common.gwt.client.bo.ClientEventLicense;
import consys.event.common.gwt.server.EventActionHandler;
import consys.payment.webservice.EventWebService;
import consys.payment.ws.event.LoadEventPaymentDetailsRequest;
import consys.payment.ws.event.LoadEventPaymentDetailsResponse;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author pepa
 */
public class LoadLicenseInfoActionHandler implements EventActionHandler<LoadLicenseInfoAction, ClientEventLicense> {

    @Autowired
    private EventWebService eventWebService;

    @Override
    public Class<LoadLicenseInfoAction> getActionType() {
        return LoadLicenseInfoAction.class;
    }

    @Override
    public ClientEventLicense execute(LoadLicenseInfoAction action) throws ActionException {
        LoadEventPaymentDetailsRequest req = EventWebService.OBJECT_FACTORY.createLoadEventPaymentDetailsRequest();
        req.setEventUuid(action.getEventUuid());
        LoadEventPaymentDetailsResponse resp = eventWebService.loadEventPaymentDetails(req);
        
        ClientEventLicense license = new ClientEventLicense();
        license.setActivatedFrom(Convert.toDate(resp.getEventActiveFrom()));
        license.setActivatedTo(Convert.toDate(resp.getEventActiveTill()));
        license.setLicenseConfirmed(Convert.toDate(resp.getOrderConfirmedDate()));
        license.setLicenseDue(Convert.toDate(resp.getOrderDueDate()));
        license.setLicensePurchased(Convert.toDate(resp.getOrderPurchasedDate()));
        
        return license;
    }
}
