package consys.event.common.gwt.server;

import consys.common.gwt.client.ui.comp.list.item.DataListItem;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * List Handler pre ListRequesty. Prvym generikom je vystup z metody list,
 * druhym generikom je obsah PagedResult.
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Component
public interface GwtListHandler<R extends DataListItem,T> {

    public String getTag();

    /**
     * Vracia data pre gwt. Vstup je vystup list serivici
     */
    public void execute(List<T> in,ArrayList<R> out);

}
