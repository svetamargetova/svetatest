package consys.event.common.gwt.server.list.export.xls;

import java.util.HashMap;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Defaultna implementacia stylu. Je mozne ju upravovat podla takeplace schemy
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DefaultCellStyle implements XlsCellStyle {

    Map<String, CellStyle> styles = new HashMap<String, CellStyle>();

    public DefaultCellStyle(Workbook wb) {
        createStyles(wb);
    }

    private Map<String, CellStyle> createStyles(Workbook wb) {
        DataFormat df = wb.createDataFormat();
        CellStyle style;
        Font headerFont = wb.createFont();
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFont(headerFont);
        styles.put("header", style);

        Font font1 = wb.createFont();
        font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(font1);
        styles.put("cell_bold", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(font1);
        styles.put("cell_bold_centered", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(font1);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_bold_date", style);


        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setWrapText(true);
        styles.put("cell_normal", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setWrapText(true);
        styles.put("cell_normal_centered", style);

        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setWrapText(true);
        style.setDataFormat(df.getFormat("d-mmm"));
        styles.put("cell_normal_date", style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    @Override
    public CellStyle getHeaderStyle() {
        return styles.get("header");
    }

    @Override
    public CellStyle getCellStyle() {
        return styles.get("cell_normal");
    }

    @Override
    public CellStyle getCellCenterStyle() {
        return styles.get("cell_normal_centered");
    }

    @Override
    public CellStyle getCellDateStyle() {
        return styles.get("cell_normal_date");
    }

    @Override
    public CellStyle getCellBoldStyle() {
        return styles.get("cell_bold");
    }

    @Override
    public CellStyle getCellBoldCenterStyle() {
        return styles.get("cell_bold_centered");
    }

    @Override
    public CellStyle getCellBoldDateStyle() {
        return styles.get("cell_bold_date");
    }
}
