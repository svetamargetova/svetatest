package consys.event.common.gwt.server.list.export.xls;

import org.apache.poi.ss.usermodel.CellStyle;

/**
 * Interface ktory definuje jednotny styl pre vsetky exporty ak nie je uvedene inak
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface XlsCellStyle {

    public CellStyle getHeaderStyle();

    public CellStyle getCellStyle();

    public CellStyle getCellCenterStyle();

    public CellStyle getCellDateStyle();

    public CellStyle getCellBoldStyle();

    public CellStyle getCellBoldCenterStyle();

    public CellStyle getCellBoldDateStyle();




}
