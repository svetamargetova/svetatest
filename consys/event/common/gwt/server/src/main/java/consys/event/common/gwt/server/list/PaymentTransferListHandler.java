package consys.event.common.gwt.server.list;

import consys.event.common.api.list.PaymentTransferList;
import consys.event.common.core.bo.thumb.PaymentTransferListItem;
import consys.event.common.gwt.client.bo.ClientPaymentTransferHead;
import consys.event.common.gwt.server.GwtListHandler;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pepa
 */
public class PaymentTransferListHandler
        implements GwtListHandler<ClientPaymentTransferHead, PaymentTransferListItem>, PaymentTransferList {

    @Override
    public String getTag() {
        return LIST_TAG;
    }

    @Override
    public void execute(List<PaymentTransferListItem> in, ArrayList<ClientPaymentTransferHead> out) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
