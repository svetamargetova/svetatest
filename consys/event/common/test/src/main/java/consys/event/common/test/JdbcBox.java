/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.test;

/**
 *
 * @author Palo
 */
public class JdbcBox {

    private String jdbc;   
    private String user;
    private String pass;

   
    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the jdbc
     */
    public String getJdbc() {
        return jdbc;
    }

    /**
     * @param jdbc the jdbc to set
     */
    public void setJdbc(String jdbc) {
        this.jdbc = jdbc;
    }
}
