package consys.event.common.test.provider;


import consys.event.common.test.listener.InitEventTestListener;
import consys.event.overseer.connector.EventRequestContext;
import consys.event.overseer.connector.EventRequestContextProvider;



/**
 * Provider event contextu
 * @author Palo
 */
public class TestEventRequestContextProvider implements EventRequestContextProvider{
    
    @Override
    public EventRequestContext getContext(){
        EventRequestContext e = new EventRequestContext();
        e.initContext(InitEventTestListener.TEST_EVENT_ID, InitEventTestListener.TEST_EVENT_ID);
        return e;
    }

    @Override
    public void initContext(String user, String event) {

    }



    
}
