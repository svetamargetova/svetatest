package consys.event.common.test;

import consys.common.core.service.NotX;
import consys.common.core.service.impl.NotxServiceImpl;
import consys.event.common.test.listener.InitEventTestListener;
import consys.event.common.test.listener.LoggingListener;
import consys.event.common.test.listener.OverseerTransactionalTestExecutionListener;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeSuite;

/**
 *
 * @author Palo
 */
@TestExecutionListeners({OverseerTransactionalTestExecutionListener.class,InitEventTestListener.class,LoggingListener.class})
@Transactional
@ContextConfiguration(locations = {"hibernate.conf.xml"})
public abstract class AbstractEventDaoTestCase extends AbstractTestNGSpringContextTests  {

    public void forceSessionFlush(){
        InitEventTestListener.getTestSessionFactory().getCurrentSession().flush();
        InitEventTestListener.getTestSessionFactory().getCurrentSession().clear();        
        InitEventTestListener.getTestSessionFactory().getCache().evictEntityRegions();
    }
    
    
    @BeforeSuite(alwaysRun=true)
    public void mockNotification(){
        NotxServiceImpl mock = mock(NotxServiceImpl.class);        
        when(mock.createTaggingBuilder()).thenReturn(mock.new TaggingBuilderImpl(null));        
        NotX.setNotificationService(mock);
    }
    
}
