package consys.event.common.test.listener;

import consys.event.common.test.JdbcBox;
import consys.event.overseer.connector.EventId;
import consys.event.overseer.connector.OverseerEvent;
import consys.event.overseer.connector.OverseerSessionFactoryPool;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

/**
 *
 * @author Palo
 */
public class InitEventTestListener extends AbstractTestExecutionListener {

    private static final Logger logger = LoggerFactory.getLogger(InitEventTestListener.class);    
    private static OverseerSessionFactoryPool factoryPool;
    public static final String TEST_EVENT_ID = "1";

    @Override
    public void prepareTestInstance(TestContext testContext) throws Exception {
        if (factoryPool == null) {
            logger.info("Initializing ONE session factory for testing purpouse");
            factoryPool = (OverseerSessionFactoryPool) testContext.getApplicationContext().getBean("sessionFactoryPool");
            JdbcBox jdbcBox = (JdbcBox) testContext.getApplicationContext().getBean("jdbcBox");
            OverseerEvent event = new OverseerEvent(jdbcBox.getJdbc(), jdbcBox.getPass(), jdbcBox.getUser(), TEST_EVENT_ID);
            factoryPool.addNewSessionFactory(event);        
            return;
        }        
    }
    
    public static SessionFactory getTestSessionFactory(){
        return factoryPool.getSessionFactory(new EventId(TEST_EVENT_ID));
    }
}
