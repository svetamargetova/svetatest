package consys.event.common.test.core.mock;

import com.amazonaws.services.s3.model.S3Object;
import consys.common.core.aws.AwsFileStorageService;
import consys.common.core.so.UploadProgress;
import consys.common.core.so.UploadStatus;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 *
 * @author palo
 */
public class AwsRelatedServiceMocks implements AwsFileStorageService {

    @Override
    public void deleteObject(String bucket, String uniqueKey) {
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, String mimeType, String cache, boolean attachment, InputStream is, long size) {
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, String mimeType, String cache, boolean attachment, InputStream is) {
    }

    @Override
    public S3Object getObject(String bucket, String uniqueKey) {
        return null;
    }

    @Override
    public void deleteObject(String bucket, List<String> uniqueKeys) {
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, Cache cache, boolean attachment, InputStream is, long size) {
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, Cache cache, boolean attachment, InputStream is) {
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, boolean attachment, InputStream is, long size) {
    }

    @Override
    public void createObject(String bucket, String uniqueKey, String fileName, FileType fileType, boolean attachment, InputStream is) {
    }

    @Override
    public UploadStatus uploadFile(String bucket, String fileName, File file, UploadProgress status) {
        return UploadStatus.UPLOADED;
    }

    @Override
    public UploadStatus uploadFile(String bucketName, String fileName, InputStream is, String contentType, UploadProgress status) {
        return UploadStatus.UPLOADED;
    }
}
