package consys.event.common.test.core.mock;

import consys.admin.event.ws.AdminEventWebService;
import consys.admin.event.ws.EventUserRequest;
import consys.admin.event.ws.EventUserResponse;
import consys.admin.event.ws.LoadEventOverseerRequest;
import consys.admin.event.ws.LoadEventOverseerResponse;
import consys.admin.event.ws.LoadUsersToEventRequest;
import consys.admin.event.ws.LoadUsersToEventResponse;
import consys.admin.event.ws.UpdateUserRelationToEventRequest;
import consys.admin.event.ws.UpdateUserRelationToEventResponse;
import consys.admin.user.ws.AdminUserWebService;
import consys.admin.user.ws.LoadUserEmailRequest;
import consys.admin.user.ws.LoadUserEmailResponse;
import consys.common.utils.UuidProvider;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author palo
 */
public class AdminWebServicesMock implements AdminUserWebService, AdminEventWebService {

    @Override
    public UpdateUserRelationToEventResponse updateUserRelationToEvent(UpdateUserRelationToEventRequest request) {
        UpdateUserRelationToEventResponse response = new UpdateUserRelationToEventResponse();
        response.setResult(true);
        return response;
    }

    @Override
    public LoadUsersToEventResponse loadUserToEvent(LoadUsersToEventRequest request) {
        LoadUsersToEventResponse resp = new LoadUsersToEventResponse();

        for (EventUserRequest r : request.getEventUserRequest()) {
            EventUserResponse user = new EventUserResponse();
            if (StringUtils.isBlank(r.getUserUuid())) {
                user.setFullName(r.getUserName());
                user.setLastName(r.getUserName());
                user.setUserUuid(UuidProvider.getUuid());
                user.setEmail("my" + System.currentTimeMillis() + "@mail.cz");
            } else {
                user.setFullName("AAAA AAAA");
                user.setLastName("AAAA");
                user.setUserUuid(r.getUserUuid());
                user.setEmail("my" + System.currentTimeMillis() + "@mail.cz");
            }
            resp.getEventUserResponse().add(user);
        }
        return resp;
    }

    @Override
    public LoadEventOverseerResponse loadEventOverseer(LoadEventOverseerRequest request) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public LoadUserEmailResponse loadUserEmail(LoadUserEmailRequest request) {
        LoadUserEmailResponse response = new LoadUserEmailResponse();
        response.setEmail("email");
        return response;
    }
}
