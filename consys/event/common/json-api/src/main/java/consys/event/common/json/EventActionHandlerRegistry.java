package consys.event.common.json;

import consys.common.json.AbstractActionHandlerRegistry;
import consys.common.json.Action;
import consys.common.json.Result;
import consys.event.overseer.connector.EventRequestContextProvider;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Handler pre json API akcie
 * @author Palo Gressa <gressa@acemcee.com>
 */ 
public class EventActionHandlerRegistry extends AbstractActionHandlerRegistry{

    @Autowired
    private EventRequestContextProvider requestContextProvider;

    @Override
    public void preAction(Action action, Result result) {
        requestContextProvider.initContext(action.getUserUuid(), action.getEventUuid());
    }


}
