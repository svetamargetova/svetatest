package consys.event.common.json.servlet;

import consys.event.common.json.EventActionHandlerRegistry;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class DispatchServiceServlet extends HttpServlet {

    private static final long serialVersionUID = 6910790572992942560L;
    // logger
    private static final Logger logger = LoggerFactory.getLogger(DispatchServiceServlet.class);
    // sluzby
    @Autowired
    private EventActionHandlerRegistry handlerRegistry;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
        logger.info("JSON Dispatch servlet initialized... ");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            handlerRegistry.handleAction(req.getInputStream(), resp.getOutputStream());
        } catch (Exception ex) {
            logger.warn("Overseer json request problem", ex);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}