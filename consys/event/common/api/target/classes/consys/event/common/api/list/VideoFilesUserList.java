package consys.event.common.api.list;

/**
 *
 * @author pepa
 */
public interface VideoFilesUserList {

    public static final String LIST_TAG = "98a68d6038334ddc84185ada19af7cd8";
    public static final int Order_NAME = 11;
    public static final int Filter_ALL = 1;
}
