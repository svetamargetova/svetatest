package consys.event.common.api.list;

/**
 *
 * @author pepa
 */
public interface VideoFilesList {

    public static final String LIST_TAG = "a09fa124729e4d8b97969c80fd6f3a2b";
    public static final int Order_NAME = 11;
    public static final int Filter_ALL = 1;
    public static final int Filter_UPLOADING = 2;
    public static final int Filter_PROCESSING = 3;
    public static final int Filter_PROCESSED = 4;
    public static final int Filter_ERROR = 5;
}
