package consys.event.common.api.servlet;

/**
 * Vsetky servletove adresy sa mapuju v URL Factory alebo EventUrlFactory
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface CommonServletProperties {

    public static final String FILTER_EVENT_TAG = "eid";
    public static final String FILTER_USER_TAG = "uid";
    
    
    // overseer_url/download?fid=UUID_FILE
    public static final String DOWNLOAD_FILE_SERVLET_FILE_ID = "fid";
    public static final String UPLOAD_PATTERN = "/upload/pattern";
    public static final String NO_CACHE = "no-cache";
    public static final String CACHE_CONTROL = "Cache-control";
    public static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
    public static final String CONTENT_DISPOSITION = "Content-Disposition";
    public static final String END_NAME = "\"";
}
