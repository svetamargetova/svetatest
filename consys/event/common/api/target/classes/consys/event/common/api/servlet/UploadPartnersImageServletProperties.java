/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.api.servlet;

import consys.event.common.api.properties.CommonProperties;

/**
 * Servlet ktory uploaduje obrazok do paticky alebo pod prave menu. Obrazok je
 * smerovany na OVERSEERA!!. Obsahuje standardne EID + UID + type=FOOTER | type=PANEL
 * @author palo
 */
public interface UploadPartnersImageServletProperties {
    
    public static String PATH = "/upload/image/partner";
    public static String PARAM_TYPE = "type";
    
    public static enum TYPE{
        FOOTER(700,CommonProperties.EVENT_PARTNER_FOOTER_IMAGE),
        PANEL(220,CommonProperties.EVENT_PARTNER_PANEL_IMAGE);
        
        private int width;
        private String propertyKey;

        private TYPE(int width, String propertyKey) {
            this.width = width;
            this.propertyKey = propertyKey;
        }

        public String getPropertyKey() {
            return propertyKey;
        }
                                       

        public int getWidth() {
            return width;
        }                        
        
    }
    
    
    
}
