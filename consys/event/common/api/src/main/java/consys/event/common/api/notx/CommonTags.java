/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.api.notx;

/**
 *
 * @author palo
 */
public interface CommonTags {

    /**
     * Vsetci v nejakom vybore. Sklada sa z COMMITEE.[uuid_commitee]
     */
    public static final String COMMITTEE = "committee";
    
    /**
     * Ucatnik bez ohladu na to ci je vo vybore, ma listok alebo podobne. Jedna 
     * sa o vsetkych ktory su v priestore eventu v databazi.
     */
    public static final String ATTENDEE = "attendee";
}
