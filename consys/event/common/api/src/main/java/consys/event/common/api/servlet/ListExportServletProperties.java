/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.api.servlet;

/**
 * <strong>POZOR</strong> tieto vlastnosti su natvrdo naimplementovane v ListExportButton, 
 * z toho dovodu ze v Commons UI nie je modul eventu ani modul API. 
 * TODO: previest vsetky spolocne API a dalsie rozhrania do obecneho COMMON modulu
 * @author palo
 */
public interface ListExportServletProperties {
    static final String PARAM_TAG = "t";
    static final String PARAM_FILTER = "f";
    static final String PARAM_FILTER_INT_VALUE = "fi";
    static final String PARAM_FILTER_STRING_VALUE = "fs";
    static final String PARAM_ORDER_PREFIX = "fo";
    static final String PARAM_OUTPUT_TYPE = "out";
}
