package consys.event.common.api.right;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventCommonModuleRoleModel extends ModuleRoleModel {

    /* Rola ze uzivatel je administrator */
    public static final String RIGHT_COMMON_SETTINGS = "RIGHT_COMMON_SETTINGS";
    /* Rola ze uzivatel je chair vyboru */
    public static final String ROLE_COMMON_COMMITTEE_CHAIR = "ROLE_COMMON_COMMITTEE_CHAIR";
    /* Pravo ze uzivatel muze vkladat zpravu na zed */
    public static final String RIGHT_COMMON_WALL_MESSAGE_INSERT = "RIGHT_COMMON_WALL_MESSAGE_INSERT";
    /* Pravo ze uzivatel muze vkladat zpravu na zed jako event */
    public static final String RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT = "RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT";
    /* Pravo ze uzivatel muze smazat zpravu ze zdi */
    public static final String RIGHT_COMMON_WALL_MESSAGE_REMOVE = "RIGHT_COMMON_WALL_MESSAGE_REMOVE";
    /* Pravo ze uzivatel muze pracovat s transfery */
    public static final String RIGHT_COMMON_TRANSFERS = "RIGHT_COMMON_TRANSFERS";
    /** Pravo pro nastaveni videa */
    public static final String RIGHT_VIDEO_SETTINGS = "RIGHT_VIDEO_SETTINGS";

    public EventCommonModuleRoleModel() {
        super(1);
    }

    @Override
    public void registerRights() {
        addRole(RIGHT_COMMON_SETTINGS, true);
        addRole(ROLE_COMMON_COMMITTEE_CHAIR, false);
        addRole(RIGHT_COMMON_WALL_MESSAGE_INSERT, true);
        addRole(RIGHT_COMMON_WALL_MESSAGE_INSERT_AS_EVENT, true);
        addRole(RIGHT_COMMON_WALL_MESSAGE_REMOVE, true);
        addRole(RIGHT_COMMON_TRANSFERS, true);
        addRole(RIGHT_VIDEO_SETTINGS, true);
    }
}
