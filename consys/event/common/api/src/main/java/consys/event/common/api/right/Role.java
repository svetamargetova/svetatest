package consys.event.common.api.right;

import java.io.Serializable;

/**
 * Objekt predsvujuci pravo modulu. V ramci vsetkych prav musi mat unikatny
 * identifikator. Identifikator je tvoreny <moduleID><valueId>
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class Role implements Serializable{
    private static final long serialVersionUID = -794609538744972440L;
    /** Unikatny nazov prava v popisnej podobne */
    private String name;
    /** Unikatny identifikator prava  */
    private String identifier;
    /** Systemove pravo. Priznak ze pravo je autoamticky generovane systemom a nemoze byt priradene */
    private boolean system;

    Role(String name, int valueId, int moduleId, boolean system) {
        this.name = name;
        this.identifier = createIdentifier(valueId, moduleId);
        this.system = system;
    }

   
    private static final String createIdentifier(int value, int module){
        return module+""+value;
    }

    /**
     * Systemove pravo. Priznak ze pravo je autoamticky generovane systemom a nemoze byt priradene
     * @return the system
     */
    public boolean isSystem() {
        return system;
    }

    /**
     * Unikatny nazov prava v popisnej podobne
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Unikatny identifikator prava
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    

    
}
