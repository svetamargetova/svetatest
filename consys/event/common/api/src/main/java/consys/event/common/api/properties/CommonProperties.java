package consys.event.common.api.properties;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public interface CommonProperties {

    public static final String EVENT_CURRENCY = "common_currency";
    public static final String EVENT_VAT_RATE = "vat_rate";
    public static final String EVENT_PARTNER_FOOTER_IMAGE = "footer_img_src";
    public static final String EVENT_PARTNER_PANEL_IMAGE = "wall_img_src";
    public static final String EVENT_EMAIL = "event_email";
    public static final String EVENT_IMAGE_UUID = "event_image_uuid";
    public static final String EVENT_PROFILE_IMAGE_UUID = "event_profile_image_uuid";
    public static final String EVENT_ADDRESS = "event_address";
    public static final String EVENT_UUID = "event_uuid";
    public static final String EVENT_ACRONYM = "event_acronym";
    public static final String EVENT_NAME = "event_name";
    public static final String EVENT_SHORT_NAME = "event_short_name";
    public static final String EVENT_UNIVERSAL_NAME = "event_universal_name";
    public static final String EVENT_TAGS = "event_tags";
    public static final String EVENT_DESCRIPTION = "event_description";
    public static final String EVENT_FROM = "event_start";
    public static final String EVENT_TIMEZONE = "event_time_zone";
    public static final String EVENT_TO = "event_end";
    public static final String EVENT_YEAR = "event_year";
    public static final String EVENT_SERIES = "event_series";
    public static final String EVENT_ADDRESS_COUNTRY = "event_address_country";
    public static final String EVENT_ADDRESS_STREET = "event_address_street";
    public static final String EVENT_ADDRESS_PLACE = "event_address_place";
    public static final String EVENT_ADDRESS_CITY = "event_address_city";
    public static final String EVENT_ADDRESS_ZIP = "event_address_zip";
    public static final String EVENT_WEB = "event_web";
    public static final String EVENT_WEB_PHONE = "event_web_phone";
    public static final String EVENT_WEB_EMAIL = "event_web_email";
    // Properties ktore sa vztahuju na chovanie eventu
    public static final String EVENT_COMMERCIAL = "event_commercial";
    public static final String EVENT_INVOICING = "event_invoicing_enabled";
    public static final String EVENT_CORPORATION = "event_corporation";
    public static final String EVENT_VIDEO_UPLOAD = "event_video_upload";
}
