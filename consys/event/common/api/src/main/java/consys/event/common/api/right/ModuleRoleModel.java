package consys.event.common.api.right;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Objekt obalujuci registrovane prava modulu
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class ModuleRoleModel {
    
    private List<Role> rights;
    private int moduleId;
    private int counter;

    public ModuleRoleModel(int moduleId) {
        this.moduleId = moduleId;
        rights = new ArrayList<Role>();
        registerRights();
    }

    public List<Role> getRoles() {
        return rights;
    }    

    /**
     * Metoda vrati model roli v mape vo formate shortcut : Role
     */
    public Map<String,Role> getModelInMap(){
        Map<String,Role> out = new HashMap<String, Role>();
        for (Role role : rights) {
            out.put(role.getName(), role);
        }
        return out;
    }

    /**
     * Zaregistruje rolu do ModeluRoli.
     * @param shortcut skratka prava
     * @param system priznak ci je pravo priraditelne uzivatelovi alebo je
     * systemove. Systemove pravo generuje sam system na zaklade workflow
     */
    protected void addRole(String shortcut,boolean system){
        Role r = new Role(shortcut, counter++, moduleId,system);
        rights.add(r);
    }

    /**
     * Metoda v ktorej sa registruiju prava do modulu pomocou metody
     * <code>addRole(String roleName)</code>
     */
    public abstract void registerRights();

}
