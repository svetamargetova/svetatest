/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.common.ws;

/**
 * Endpoint pre overseera
 * @author palo
 */
public interface EventCommonWebService {

    public final static String NAMESPACE = "http://takeplace.eu/event/common";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
}
