/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.user.ws;




/**
 * Endpoint pre overseera
 * @author palo
 */
public interface EventUserWebService {

    public final static String NAMESPACE = "http://takeplace.eu/event/user";

    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
   

    public UpdateUserDetailResponse updateUserInEvents(UpdateUserDetailRequest request);

    public UpdateUserOrganizationResponse updateOrganizationInEvents(UpdateUserOrganizationRequest request);   
}
