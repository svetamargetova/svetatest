/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.registration.ws;




/**
 * Endpoint pre overseera
 * @author palo
 */
public interface EventRegistrationWebService {

    public final static String NAMESPACE = "http://takeplace.eu/event/registration";

    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();
    

    public RegisterUserIntoEventResponse registerUserIntoEvent(RegisterUserIntoEventRequest request);

}
