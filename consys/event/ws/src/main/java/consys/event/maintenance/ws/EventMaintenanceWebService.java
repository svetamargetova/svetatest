package consys.event.maintenance.ws;

/**
 * Endpoint pre overseera
 * @author palo
 */
public interface EventMaintenanceWebService {

    public final static String NAMESPACE = "http://takeplace.eu/event/maintenance";
    public static final ObjectFactory OBJECT_FACTORY = new ObjectFactory();

    public CreateEventResponse createEvent(CreateEventRequest request);
    
    public CloneEventResponse cloneEvent(CloneEventRequest request);

    public UpdateEventResponse updateEvent(UpdateEventRequest request);

    public DeleteEventResponse deleteEvent(DeleteEventRequest request);

    public SetEventStateResponse setEventState(SetEventStateRequest request);

    public MigrateEventDatabasesResponse migrateEventDatabases(MigrateEventDatabasesRequest request);

    public RecreateEventProfilePageResponse recreateEventProfilePage(RecreateEventProfilePageRequest request);
}
