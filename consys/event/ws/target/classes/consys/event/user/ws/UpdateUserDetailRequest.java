//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.03.01 at 04:54:00 PM CET 
//


package consys.event.user.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Uuid" type="{http://takeplace.eu/event/common}Uuid"/>
 *         &lt;element name="events" type="{http://takeplace.eu/event/user}stringList"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fullName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="profileImagePrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="celiac" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="vegetarian" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uuid",
    "events",
    "lastName",
    "fullName",
    "email",
    "profileImagePrefix",
    "celiac",
    "vegetarian"
})
@XmlRootElement(name = "UpdateUserDetailRequest")
public class UpdateUserDetailRequest
    implements Equals, HashCode, ToString
{

    @XmlElement(name = "Uuid", required = true)
    protected String uuid;
    @XmlList
    @XmlElement(required = true)
    protected List<String> events;
    @XmlElement(required = true)
    protected String lastName;
    @XmlElement(required = true)
    protected String fullName;
    @XmlElement(required = true)
    protected String email;
    @XmlElement(required = true, nillable = true)
    protected String profileImagePrefix;
    protected boolean celiac;
    protected boolean vegetarian;

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUuid(String value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the events property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the events property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEvents() {
        if (events == null) {
            events = new ArrayList<String>();
        }
        return this.events;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the profileImagePrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileImagePrefix() {
        return profileImagePrefix;
    }

    /**
     * Sets the value of the profileImagePrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileImagePrefix(String value) {
        this.profileImagePrefix = value;
    }

    /**
     * Gets the value of the celiac property.
     * 
     */
    public boolean isCeliac() {
        return celiac;
    }

    /**
     * Sets the value of the celiac property.
     * 
     */
    public void setCeliac(boolean value) {
        this.celiac = value;
    }

    /**
     * Gets the value of the vegetarian property.
     * 
     */
    public boolean isVegetarian() {
        return vegetarian;
    }

    /**
     * Sets the value of the vegetarian property.
     * 
     */
    public void setVegetarian(boolean value) {
        this.vegetarian = value;
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theUuid;
            theUuid = this.getUuid();
            strategy.appendField(locator, this, "uuid", buffer, theUuid);
        }
        {
            List<String> theEvents;
            theEvents = this.getEvents();
            strategy.appendField(locator, this, "events", buffer, theEvents);
        }
        {
            String theLastName;
            theLastName = this.getLastName();
            strategy.appendField(locator, this, "lastName", buffer, theLastName);
        }
        {
            String theFullName;
            theFullName = this.getFullName();
            strategy.appendField(locator, this, "fullName", buffer, theFullName);
        }
        {
            String theEmail;
            theEmail = this.getEmail();
            strategy.appendField(locator, this, "email", buffer, theEmail);
        }
        {
            String theProfileImagePrefix;
            theProfileImagePrefix = this.getProfileImagePrefix();
            strategy.appendField(locator, this, "profileImagePrefix", buffer, theProfileImagePrefix);
        }
        {
            boolean theCeliac;
            theCeliac = this.isCeliac();
            strategy.appendField(locator, this, "celiac", buffer, theCeliac);
        }
        {
            boolean theVegetarian;
            theVegetarian = this.isVegetarian();
            strategy.appendField(locator, this, "vegetarian", buffer, theVegetarian);
        }
        return buffer;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof UpdateUserDetailRequest)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final UpdateUserDetailRequest that = ((UpdateUserDetailRequest) object);
        {
            String lhsUuid;
            lhsUuid = this.getUuid();
            String rhsUuid;
            rhsUuid = that.getUuid();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "uuid", lhsUuid), LocatorUtils.property(thatLocator, "uuid", rhsUuid), lhsUuid, rhsUuid)) {
                return false;
            }
        }
        {
            List<String> lhsEvents;
            lhsEvents = this.getEvents();
            List<String> rhsEvents;
            rhsEvents = that.getEvents();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "events", lhsEvents), LocatorUtils.property(thatLocator, "events", rhsEvents), lhsEvents, rhsEvents)) {
                return false;
            }
        }
        {
            String lhsLastName;
            lhsLastName = this.getLastName();
            String rhsLastName;
            rhsLastName = that.getLastName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "lastName", lhsLastName), LocatorUtils.property(thatLocator, "lastName", rhsLastName), lhsLastName, rhsLastName)) {
                return false;
            }
        }
        {
            String lhsFullName;
            lhsFullName = this.getFullName();
            String rhsFullName;
            rhsFullName = that.getFullName();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "fullName", lhsFullName), LocatorUtils.property(thatLocator, "fullName", rhsFullName), lhsFullName, rhsFullName)) {
                return false;
            }
        }
        {
            String lhsEmail;
            lhsEmail = this.getEmail();
            String rhsEmail;
            rhsEmail = that.getEmail();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "email", lhsEmail), LocatorUtils.property(thatLocator, "email", rhsEmail), lhsEmail, rhsEmail)) {
                return false;
            }
        }
        {
            String lhsProfileImagePrefix;
            lhsProfileImagePrefix = this.getProfileImagePrefix();
            String rhsProfileImagePrefix;
            rhsProfileImagePrefix = that.getProfileImagePrefix();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "profileImagePrefix", lhsProfileImagePrefix), LocatorUtils.property(thatLocator, "profileImagePrefix", rhsProfileImagePrefix), lhsProfileImagePrefix, rhsProfileImagePrefix)) {
                return false;
            }
        }
        {
            boolean lhsCeliac;
            lhsCeliac = this.isCeliac();
            boolean rhsCeliac;
            rhsCeliac = that.isCeliac();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "celiac", lhsCeliac), LocatorUtils.property(thatLocator, "celiac", rhsCeliac), lhsCeliac, rhsCeliac)) {
                return false;
            }
        }
        {
            boolean lhsVegetarian;
            lhsVegetarian = this.isVegetarian();
            boolean rhsVegetarian;
            rhsVegetarian = that.isVegetarian();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "vegetarian", lhsVegetarian), LocatorUtils.property(thatLocator, "vegetarian", rhsVegetarian), lhsVegetarian, rhsVegetarian)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theUuid;
            theUuid = this.getUuid();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "uuid", theUuid), currentHashCode, theUuid);
        }
        {
            List<String> theEvents;
            theEvents = this.getEvents();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "events", theEvents), currentHashCode, theEvents);
        }
        {
            String theLastName;
            theLastName = this.getLastName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "lastName", theLastName), currentHashCode, theLastName);
        }
        {
            String theFullName;
            theFullName = this.getFullName();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "fullName", theFullName), currentHashCode, theFullName);
        }
        {
            String theEmail;
            theEmail = this.getEmail();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "email", theEmail), currentHashCode, theEmail);
        }
        {
            String theProfileImagePrefix;
            theProfileImagePrefix = this.getProfileImagePrefix();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "profileImagePrefix", theProfileImagePrefix), currentHashCode, theProfileImagePrefix);
        }
        {
            boolean theCeliac;
            theCeliac = this.isCeliac();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "celiac", theCeliac), currentHashCode, theCeliac);
        }
        {
            boolean theVegetarian;
            theVegetarian = this.isVegetarian();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "vegetarian", theVegetarian), currentHashCode, theVegetarian);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

}
