package consys.event.payment.webservice;

import consys.event.overseer.connector.EventRequestContextProvider;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public abstract class AbstractWebServiceImpl extends WebServiceGatewaySupport{

    private EventRequestContextProvider eventRequestContextProvider;

    /**
     * @return the eventRequestContextProvider
     */
    public EventRequestContextProvider getEventRequestContextProvider() {
        return eventRequestContextProvider;
    }

    /**
     * @param eventRequestContextProvider the eventRequestContextProvider to set
     */
    public void setEventRequestContextProvider(EventRequestContextProvider eventRequestContextProvider) {
        this.eventRequestContextProvider = eventRequestContextProvider;
    }

    public String getEventUuid(){
        return getEventRequestContextProvider().getContext().getEventUuid().getId();
    }
    public String getUserUuid(){
        return getEventRequestContextProvider().getContext().getUserEventUuid();
    }

}
