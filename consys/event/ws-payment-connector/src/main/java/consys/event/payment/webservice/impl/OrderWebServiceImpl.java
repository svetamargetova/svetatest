package consys.event.payment.webservice.impl;

import consys.event.payment.webservice.AbstractWebServiceImpl;
import consys.payment.webservice.OrderWebService;
import consys.payment.ws.order.*;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class OrderWebServiceImpl extends AbstractWebServiceImpl implements OrderWebService {

    @Override
    public ConfirmOrderResponse confirmOrder(ConfirmOrderRequest request) {
        request.setEventUuid(getEventUuid());
        request.setUserUuid(getUserUuid());
        return (ConfirmOrderResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public CreateOrderResponse createOrder(CreateOrderRequest request) {
        request.setEventUuid(getEventUuid());
        request.setUserUuid(getUserUuid());
        return (CreateOrderResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public CreateOrderWithProfileResponse createOrderWithProfile(CreateOrderWithProfileRequest request) {
        request.setEventUuid(getEventUuid());
        request.setUserUuid(getUserUuid());
        return (CreateOrderWithProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public CancelOrderResponse cancelOrder(CancelOrderRequest request) {
        request.setEventUuid(getEventUuid());
        request.setUserUuid(getUserUuid());
        return (CancelOrderResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public CancelOrdersResponse cancelOrders(CancelOrdersRequest request) {
        request.setEventUuid(getEventUuid());
        request.setUserUuid(getUserUuid());
        return (CancelOrdersResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadOrderResponse loadOrder(LoadOrderRequest request) {
        return (LoadOrderResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
