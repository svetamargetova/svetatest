package consys.event.payment.webservice.impl;

import consys.event.payment.webservice.AbstractWebServiceImpl;
import consys.payment.webservice.ProductWebService;
import consys.payment.ws.product.CreateEventProductRequest;
import consys.payment.ws.product.CreateEventProductResponse;
import consys.payment.ws.product.CreateEventProductsRequest;
import consys.payment.ws.product.CreateEventProductsResponse;
import consys.payment.ws.product.DeleteEventProductsRequest;
import consys.payment.ws.product.DeleteEventProductsResponse;
import consys.payment.ws.product.LoadLicenseProductRequest;
import consys.payment.ws.product.LoadLicenseProductResponse;
import consys.payment.ws.product.LoadProductRequest;
import consys.payment.ws.product.LoadProductResponse;
import consys.payment.ws.product.UpdateEventProductRequest;
import consys.payment.ws.product.UpdateEventProductResponse;
import consys.payment.ws.product.UpdateEventProductsRequest;
import consys.payment.ws.product.UpdateEventProductsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProductWebServiceImpl extends AbstractWebServiceImpl implements ProductWebService {

    private static final Logger log = LoggerFactory.getLogger(ProductWebServiceImpl.class);

    @Override
    public CreateEventProductResponse createProduct(CreateEventProductRequest request) {
        request.setEventUuid(getEventUuid());
        return (CreateEventProductResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public UpdateEventProductResponse updateProduct(UpdateEventProductRequest request) {
        request.setEventUuid(getEventUuid());
        return (UpdateEventProductResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public UpdateEventProductsResponse updateProducts(UpdateEventProductsRequest request) {
        request.setEventUuid(getEventUuid());
        return (UpdateEventProductsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public CreateEventProductsResponse createProducts(CreateEventProductsRequest request) {
        request.setEventUuid(getEventUuid());
        return (CreateEventProductsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadProductResponse loadProduct(LoadProductRequest request) {
        return (LoadProductResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    

    @Override
    public DeleteEventProductsResponse deleteEventProducts(DeleteEventProductsRequest request) {
        request.setEventUuid(getEventUuid());        
        return (DeleteEventProductsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadLicenseProductResponse loadLicenseProduct(LoadLicenseProductRequest request) {
        request.setEventUuid(getEventUuid());
        return (LoadLicenseProductResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
