package consys.event.payment.webservice.impl;

import consys.event.payment.webservice.AbstractWebServiceImpl;
import consys.payment.webservice.TransferWebService;
import consys.payment.ws.transfer.ListTransfersRequest;
import consys.payment.ws.transfer.ListTransfersResponse;
import consys.payment.ws.transfer.LoadTransferRequest;
import consys.payment.ws.transfer.LoadTransferResponse;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class TransferWebServiceImpl extends AbstractWebServiceImpl implements TransferWebService{

    @Override
    public ListTransfersResponse listEventTransfers(ListTransfersRequest request) {
        request.setEventUuid(getEventUuid());
        return (ListTransfersResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadTransferResponse loadEventTransfer(LoadTransferRequest request) {
        request.setEventUuid(getEventUuid());
        return (LoadTransferResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

}
