package consys.event.payment.webservice.impl;

import consys.event.payment.webservice.AbstractWebServiceImpl;
import consys.payment.webservice.ProfileWebService;
import consys.payment.ws.profile.ListUserProfilesRequest;
import consys.payment.ws.profile.ListUserProfilesResponse;
import consys.payment.ws.profile.LoadOrderProfileRequest;
import consys.payment.ws.profile.LoadOrderProfileResponse;
import consys.payment.ws.profile.LoadProfileRequest;
import consys.payment.ws.profile.LoadProfileResponse;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ProfileWebServiceImpl extends AbstractWebServiceImpl implements ProfileWebService {

    @Override
    public LoadOrderProfileResponse loadOrderProfile(LoadOrderProfileRequest request) {
        return (LoadOrderProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadProfileResponse loadProfile(LoadProfileRequest request) {
        return (LoadProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public ListUserProfilesResponse listUserProfiles(ListUserProfilesRequest request) {
        request.setUserUuid(getUserUuid());
        return (ListUserProfilesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
