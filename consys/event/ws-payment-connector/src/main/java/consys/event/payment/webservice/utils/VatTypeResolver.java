package consys.event.payment.webservice.utils;

import consys.common.utils.enums.CountryEnum;
import consys.payment.ws.bo.InvoiceVat;

/**
 * Na zaklade county sa vrati typ VATu
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class VatTypeResolver {

    public static InvoiceVat getVatTypeFromCountry(CountryEnum country) {
        switch (country) {
            case CZECH_REPUBLIC:
                return InvoiceVat.VAT_CZ;
            case AUSTRIA:
            case BELGIUM:
            case BULGARIA:
            case CYPRUS:
            case DENMARK:
            case ESTONIA:
            case FINLAND:
            case FRANCE:
            case GERMANY:
            case GREECE:
            case HUNGARY:
            case IRELAND:
            case ITALY:
            case LATVIA:
            case LITHUANIA:
            case LUXEMBOURG:
            case MALTA:
            case NETHERLANDS:
            case POLAND:
            case PORTUGAL:
            case ROMANIA:
            case SLOVAKIA:
            case SLOVENIA:
            case SPAIN:
            case SWEDEN:
            case UNITED_KINGDOM:
                return InvoiceVat.VAT_EU;
            default:
                return InvoiceVat.VAT_OT;
        }
    }
}
