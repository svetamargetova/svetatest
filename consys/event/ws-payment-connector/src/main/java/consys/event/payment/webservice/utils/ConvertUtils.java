package consys.event.payment.webservice.utils;

import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class ConvertUtils {

    public static XMLGregorianCalendar toXmlCalendar(Date date) {
        if (date == null) {
            return null;
        }
        try {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(date);
            XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
            return date2;
        } catch (DatatypeConfigurationException ex) {
            throw new IllegalArgumentException("Can't convert Date => XMLGregorianCalendar", ex);
        }
    }

    public static Date toDate(XMLGregorianCalendar cal) {
        if (cal == null) {
            return null;
        } else {
            return cal.toGregorianCalendar().getTime();
        }
    }
}
