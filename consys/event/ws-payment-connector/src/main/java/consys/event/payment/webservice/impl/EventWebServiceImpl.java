package consys.event.payment.webservice.impl;

import consys.event.payment.webservice.AbstractWebServiceImpl;
import consys.payment.webservice.EventWebService;
import consys.payment.ws.event.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Palo Gressa <gressa@acemcee.com>
 */
public class EventWebServiceImpl extends AbstractWebServiceImpl implements EventWebService {

    private static final Logger log = LoggerFactory.getLogger(EventWebServiceImpl.class);

    @Override
    public InitializeEventResponse initializeEvent(InitializeEventRequest request){
        request.setEventUuid(getEventUuid());
        return (InitializeEventResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
    
    @Override
    public InitializeCloneEventResponse initializeCloneEvent(InitializeCloneEventRequest request) {
        return (InitializeCloneEventResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public BuyLicenseResponse buyLicense(BuyLicenseRequest request){
        request.setEventUuid(getEventUuid());
        return (BuyLicenseResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public BuyLicenseWithProfileResponse buyLicenseWithProfile(BuyLicenseWithProfileRequest request){
        request.setEventUuid(getEventUuid());
        return (BuyLicenseWithProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadEventPaymentDetailsResponse loadEventPaymentDetails(LoadEventPaymentDetailsRequest request){
        request.setEventUuid(getEventUuid());
        return (LoadEventPaymentDetailsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public UpdateEventProfileResponse updateEventProfile(UpdateEventProfileRequest request){        
        return (UpdateEventProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public ListEventProfilesResponse listEventProfiles(ListEventProfilesRequest request){
        request.setEventUuid(getEventUuid());
        return (ListEventProfilesResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    @Override
    public LoadEventPaymentProfileResponse loadEventPaymentProfile(LoadEventPaymentProfileRequest request) {
        return (LoadEventPaymentProfileResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }

    
}
