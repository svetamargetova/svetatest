package consys.event.user.ws;

import consys.common.core.exception.NoRecordException;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.service.UserEventService;
import consys.event.overseer.connector.EventRequestContextProvider;
import consys.event.overseer.management.core.OverseerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/** 
 *
 * @author pepa
 */
@Endpoint
public class EventUserWebServiceEndpoint implements EventUserWebService, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(EventUserWebServiceEndpoint.class);
    // -- Administration services
    private OverseerManager overseerManager;
    private EventRequestContextProvider requestContextProvider;
    private ApplicationContext applicationContext;
    // -- Services
    private UserEventService userEventService;

    @Override
    @PayloadRoot(localPart = "UpdateUserDetailRequest", namespace = NAMESPACE)
    public UpdateUserDetailResponse updateUserInEvents(UpdateUserDetailRequest request) {
        // vytvorime si mapu akci podle jejich uuid, at se lip pracuje
        /*List<Event> overseerEvents = overseerManager.listAll();
        Map<String, Event> eventsMap = new HashMap<String, Event>();
        for (Event e : overseerEvents) {
            eventsMap.put(e.getEventUuid(), e);
        }*/

        UpdateUserDetailResponse out = new UpdateUserDetailResponse();
        for (String event : request.getEvents()) {
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Updating user details at " + event);
                }

                // TODO: kontrolovat jestli se jedna o budouci nebo aktualne probihajici akci,
                //       pokud jde o neco starsiho, tak preskocit
                /*Event e = eventsMap.get(event);
                if(?) {
                    continue;
                }*/

                getRequestContextProvider().initContext(request.getUuid(), event);
                UserEvent ue = getUserEventService().loadUserEvent(request.getUuid());
                ue.setFullName(request.getFullName());
                ue.setLastName(request.getLastName());
                ue.setProfileImagePrefix(request.getProfileImagePrefix());
                ue.setCeliac(request.isCeliac());
                ue.setVegetarian(request.isVegetarian());
                ue.setEmail(request.getEmail());
                getUserEventService().updateUserEvent(ue);
                out.setResult(true);
            } catch (NoRecordException ex) {
                logger.error("User " + request.getUuid() + " is not present in  " + event + ".");
                out.setResult(false);
            } catch (Exception e) {
                logger.error("User " + request.getUuid() + "can't be updated in event with UUID: " + event);
            }
        }

        return out;
    }

    @Override
    @PayloadRoot(localPart = "UpdateUserOrganizationRequest", namespace = NAMESPACE)
    public UpdateUserOrganizationResponse updateOrganizationInEvents(UpdateUserOrganizationRequest request) {
        if (logger.isDebugEnabled()) {
            logger.debug("Updating user affiliation");
        }
        UpdateUserOrganizationResponse out = new UpdateUserOrganizationResponse();
        for (String event : request.getEvents()) {
            try {
                getRequestContextProvider().initContext(request.getUuid(), event);
                UserEvent ue = getUserEventService().loadUserEvent(request.getUuid());
                ue.setOrganization(request.getAffiliation());
                getUserEventService().updateUserEvent(ue);
                out.setResult(true);
            } catch (NoRecordException ex) {
                logger.error("Load User " + request.getUuid() + " from event " + event + " failed!");
                out.setResult(false);
            }
        }
        return out;
    }

    /**
     * @return the overseerManager
     */
    public OverseerManager getOverseerManager() {
        if (overseerManager == null) {
            overseerManager = (OverseerManager) applicationContext.getParent().getBean("overseerManager", OverseerManager.class);
        }
        return overseerManager;
    }

    /**
     * @param overseerManager the overseerManager to set
     */
    public void setOverseerManager(OverseerManager overseerManager) {
        this.overseerManager = overseerManager;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * @return the userEventService
     */
    public UserEventService getUserEventService() {
        if (userEventService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(UserEventService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserService not found!");
                }
                userEventService = (UserEventService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return userEventService;
    }

    /**
     * @return the requestContextProvider
     */
    public EventRequestContextProvider getRequestContextProvider() {
        if (requestContextProvider == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventRequestContextProvider.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean EventRequestContextProvider not found!");
                }
                requestContextProvider = (EventRequestContextProvider) applicationContext.getParent().getBean(names[0]);
            }
        }
        return requestContextProvider;
    }
}
