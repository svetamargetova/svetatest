package consys.event.maintenance.ws;

import consys.common.core.exception.IllegalPropertyValue;
import consys.common.core.exception.NoRecordException;
import consys.common.core.service.SystemPropertyService;
import consys.common.utils.collection.Maps;
import consys.common.utils.date.DateProvider;
import consys.common.utils.enums.CountryEnum;
import consys.common.utils.enums.Currency;
import consys.common.ws.utils.WSConvertUtils;
import consys.event.common.api.properties.CommonProperties;
import consys.event.common.core.bo.ModuleRight;
import consys.event.common.core.bo.UserEvent;
import consys.event.common.core.context.PropertiesChangedEvent;
import consys.event.common.core.service.CommonCloneClearService;
import consys.event.common.core.service.ModuleRightService;
import consys.event.common.core.service.UserEventService;
import consys.event.common.ws.EventDescriptor;
import consys.event.conference.core.service.ConferenceCloneClearService;
import consys.event.overseer.connector.EventRequestContextProvider;
import consys.event.overseer.management.core.OverseerManager;
import consys.event.overseer.management.core.bo.Event;
import consys.event.overseer.management.core.dao.EventDao;
import consys.event.overseer.management.core.exception.DatabaseUserCreateException;
import consys.event.profile.core.service.EventProfileService;
import consys.event.registration.core.service.RegistrationCloneClearService;
import consys.event.scheduling.core.service.SchedulingCloneClearService;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 *
 * @author pepa
 */
@Endpoint
public class EventMaintenanceWebServiceEndpoint implements EventMaintenanceWebService, ApplicationContextAware, ApplicationEventPublisherAware {

    private static final Logger logger = LoggerFactory.getLogger(EventMaintenanceWebServiceEndpoint.class);
    // -- Administration services
    private OverseerManager overseerManager;
    private EventRequestContextProvider requestContextProvider;
    private ApplicationContext applicationContext;
    private ApplicationEventPublisher publisher;
    // -- Services
    private UserEventService userEventService;
    private ModuleRightService moduleRightService;
    private SystemPropertyService systemPropertyService;
    private EventDao eventDao;
    private EventProfileService eventProfileService;

    @Override
    @PayloadRoot(localPart = "CreateEventRequest", namespace = NAMESPACE)
    public CreateEventResponse createEvent(CreateEventRequest request) {
        boolean out = false;
        try {
            logger.info("Create New Event!" + request.getEvent().getAcronym());
            Event e = new Event();
            e.setAcronym(request.getEvent().getAcronym());
            e.setEventUuid(request.getEvent().getUuid());
            getOverseerManager().createEvent(e);
            // TODO: v pripade problemu poslat email 
            try {
                logger.info("Create owner ........ ");
                // vytvorime vlastnika
                getRequestContextProvider().initContext(request.getCreater().getUuid(), request.getEvent().getUuid());
                UserEvent ue = new UserEvent();
                ue.setFullName(request.getCreater().getFullName());
                ue.setLastName(request.getCreater().getLastName());
                ue.setProfileImagePrefix(request.getCreater().getPotraitImgPrefixUuid());
                ue.setEmail(request.getCreater().getEmail());
                ue.setPosition(request.getCreater().getPosition());
                ue.setOrganization(request.getCreater().getOrganization());
                ue.setUuid(request.getCreater().getUuid());
                getUserEventService().createUserEventWithRights(ue, ArrayUtils.EMPTY_STRING_ARRAY);
                // priradime vlastnikovy vsetky prava
                logger.info("Add all module rights ......");
                List<ModuleRight> allRights = getModuleRightService().listAllRights();
                ue.getPrivateGroup().getRights().addAll(allRights);
                getUserEventService().updateUserEvent(ue);
                out = true;

                // vytvorime systemove premenne
                updateEventProperties(request.getEvent());

                getSystemPropertyService().updateProperty(CommonProperties.EVENT_CURRENCY, Currency.fromId(request.currencyId).name());


            } catch (Exception exception) {
                logger.error("Error create user owner! ");
                logger.error("Event:  " + e.getAcronym());
                logger.error("      " + e.getEventUuid());
                logger.error("Owner:  " + request.getCreater().getFullName());
                logger.error("      " + request.getCreater().getUuid());
                logger.error("Error:", exception);
                out = false;
            }
        } catch (DatabaseUserCreateException ex) {
            logger.error("Error!", ex);
        } catch (InstantiationException ex) {
            logger.error("Error!", ex);
        }

        return OBJECT_FACTORY.createCreateEventResponse();
    }

    @Override
    @PayloadRoot(localPart = "CloneEventRequest", namespace = NAMESPACE)
    public CloneEventResponse cloneEvent(CloneEventRequest request) {
        CloneEventResponse response = new CloneEventResponse();
        try {
            Event originalEvent = getOverseerManager().loadEvent(request.getOriginalEventUuid());

            Event cloneEvent = new Event();
            cloneEvent.setAcronym(originalEvent.getAcronym());
            cloneEvent.setEventUuid(request.getCloneEventUuid());

            getOverseerManager().cloneEvent(originalEvent, cloneEvent);

            getRequestContextProvider().initContext(null, cloneEvent.getEventUuid());

            // uprava uuid eventu v properties
            Map<String, Object> b = Maps.newHashMap();
            b.put(CommonProperties.EVENT_UUID, cloneEvent.getEventUuid());
            b.put(CommonProperties.EVENT_FROM, null);
            b.put(CommonProperties.EVENT_TO, null);
            b.put(CommonProperties.EVENT_SERIES, request.getSeries());
            getSystemPropertyService().updateProperties(b);

            // provedeni promazani nad jednotlivymi moduly
            logger.debug("Clear clone database");
            getCommonCloneClearService().clearClone();
            getRegistrationCloneClearService().clearClone();
            getSchedulingCloneClearService().clearClone();
            getConferenceCloneClearService().clearClone();

            // vygenerovani profilove stranky
            getEventProfileService().generateEventProfileWebPage();

            response.setSuccess(true);
        } catch (Exception ex) {
            logger.error("Error!", ex);
            response.setSuccess(false);
        }
        return response;
    }

    @Override
    @PayloadRoot(localPart = "DeleteEventRequest", namespace = NAMESPACE)
    public DeleteEventResponse deleteEvent(DeleteEventRequest request) {
        getOverseerManager().deleteEvent(request.getEventUuid());
        return OBJECT_FACTORY.createDeleteEventResponse();
    }

    @Override
    @PayloadRoot(localPart = "UpdateEventRequest", namespace = NAMESPACE)
    public UpdateEventResponse updateEvent(UpdateEventRequest request) {
        updateEventProperties(request.getEventDescriptor());
        return OBJECT_FACTORY.createUpdateEventResponse();
    }

    private void updateEventProperties(EventDescriptor descriptor) {
        try {
            getRequestContextProvider().initContext(null, descriptor.getUuid());

            Map<String, Object> b = Maps.newHashMap();

            b.put(CommonProperties.EVENT_NAME, descriptor.getName());
            b.put(CommonProperties.EVENT_ACRONYM, descriptor.getAcronym());
            b.put(CommonProperties.EVENT_UNIVERSAL_NAME, descriptor.getUniversalName());
            b.put(CommonProperties.EVENT_TAGS, descriptor.getTags());
            b.put(CommonProperties.EVENT_DESCRIPTION, descriptor.getDescription());
            b.put(CommonProperties.EVENT_UUID, descriptor.getUuid());
            b.put(CommonProperties.EVENT_YEAR, descriptor.getYear());
            b.put(CommonProperties.EVENT_SERIES, descriptor.getSeries());
            b.put(CommonProperties.EVENT_WEB, descriptor.getWeb());
            b.put(CommonProperties.EVENT_IMAGE_UUID, descriptor.getImageLogoPrefix());
            b.put(CommonProperties.EVENT_PROFILE_IMAGE_UUID, descriptor.getImageProfileLogoPrefix());
            b.put(CommonProperties.EVENT_CORPORATION, descriptor.getCorporationPrefix());

            if (descriptor.getFromDate() != null) {
                b.put(CommonProperties.EVENT_FROM, WSConvertUtils.toDate(descriptor.getFromDate()));
            }
            if (descriptor.getToDate() != null) {
                b.put(CommonProperties.EVENT_TO, WSConvertUtils.toDate(descriptor.getToDate()));
            }
            if (descriptor.getTimeZone() != null) {
                b.put(CommonProperties.EVENT_TIMEZONE, descriptor.getTimeZone());
            }


            StringBuilder address = new StringBuilder();
            if (!StringUtils.isEmpty(descriptor.getStreet())) {
                address.append(descriptor.getStreet());
                address.append(", ");
            }
            if (!StringUtils.isEmpty(descriptor.getCity())) {
                address.append(descriptor.getCity());
                address.append(", ");
            }
            if (descriptor.getCountry() != 0) {
                CountryEnum c = CountryEnum.fromId(descriptor.getCountry());
                address.append(c.getName().replace("_", " "));
                b.put(CommonProperties.EVENT_ADDRESS_COUNTRY, c.getName());
            }
            String a = address.toString();
            if (a.endsWith(", ")) {
                a = a.substring(0, a.length() - 2);
            }
            b.put(CommonProperties.EVENT_ADDRESS, a);
            b.put(CommonProperties.EVENT_ADDRESS_CITY, descriptor.getCity());
            b.put(CommonProperties.EVENT_ADDRESS_STREET, descriptor.getStreet());
            b.put(CommonProperties.EVENT_ADDRESS_PLACE, descriptor.getPlace());
            b.put(CommonProperties.EVENT_ADDRESS_ZIP, descriptor.getZip());
            getSystemPropertyService().updateProperties(b);
            notifyAboutChangedEventProperties();
        } catch (IllegalPropertyValue ex) {
            logger.error("Error update property!");
        } catch (NoRecordException ex) {
            logger.error("System property is missing!");
        } catch (Exception e) {
            logger.error("System property error!", e);
        }
    }

    @Override
    @PayloadRoot(localPart = "RecreateEventProfilePageRequest", namespace = NAMESPACE)
    public RecreateEventProfilePageResponse recreateEventProfilePage(RecreateEventProfilePageRequest request) {
        List<Event> events = getEventDao().listAllEvents();
        for (Event event : events) {
            try {
                getRequestContextProvider().initContext(null, event.getEventUuid());
                getEventProfileService().generateEventProfileWebPage();
            } catch (Exception e) {
                logger.error("Failed to recreate event profile page: {}", event);
            }
        }
        return OBJECT_FACTORY.createRecreateEventProfilePageResponse();
    }

    @Override
    public SetEventStateResponse setEventState(SetEventStateRequest request) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    @PayloadRoot(localPart = "MigrateEventDatabasesRequest", namespace = NAMESPACE)
    public MigrateEventDatabasesResponse migrateEventDatabases(MigrateEventDatabasesRequest request) {
        List<Event> events = getEventDao().listAllEvents();
        MigrateEventDatabasesResponse response = OBJECT_FACTORY.createMigrateEventDatabasesResponse();
        for (Event event : events) {
            MigrationResponseItem item = OBJECT_FACTORY.createMigrationResponseItem();
            try {
                item.setEventName(event.getAcronym());
                item.setEventDatabase(event.getDbUrl());
                getEventDao().executeSqlOnEvent(event, request.getSqlScript());
                GregorianCalendar c = new GregorianCalendar();
                c.setTime(DateProvider.getCurrentDate());
                XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
                item.setFinished(date2);
            } catch (DatatypeConfigurationException ex) {
                // nemelo by nastat
                item.setError("Error when creating date time of migrated result");
                logger.error(item.getError(), ex);
            } catch (SQLException ex) {
                item.setError("State:" + ex.getSQLState() + " code:" + ex.getErrorCode() + " " + ex.getMessage());
                logger.error(item.getError(), ex);
            }
            response.getMigratedEventsList().add(item);
        }
        return response;
    }

    /**
     * @return the overseerManager
     */
    public OverseerManager getOverseerManager() {
        if (overseerManager == null) {
            overseerManager = (OverseerManager) applicationContext.getParent().getBean("overseerManager", OverseerManager.class);
        }
        return overseerManager;
    }

    /**
     * @param overseerManager the overseerManager to set
     */
    public void setOverseerManager(OverseerManager overseerManager) {
        this.overseerManager = overseerManager;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * @return the userEventService
     */
    public UserEventService getUserEventService() {
        if (userEventService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(UserEventService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserService not found!");
                }
                userEventService = (UserEventService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return userEventService;
    }

    /**
     * @return the userEventService
     */
    public synchronized ModuleRightService getModuleRightService() {
        if (moduleRightService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(ModuleRightService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean ModuleRightService not found!");
                }
                moduleRightService = (ModuleRightService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return moduleRightService;
    }

    public SystemPropertyService getSystemPropertyService() {
        if (systemPropertyService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(SystemPropertyService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean SystemPropertyService not found!");
                }
                systemPropertyService = (SystemPropertyService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return systemPropertyService;
    }

    public EventProfileService getEventProfileService() {
        if (eventProfileService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventProfileService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean EventProfileService not found!");
                }
                eventProfileService = (EventProfileService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return eventProfileService;
    }

    /**
     * @return the requestContextProvider
     */
    public EventRequestContextProvider getRequestContextProvider() {
        if (requestContextProvider == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventRequestContextProvider.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean EventRequestContextProvider not found!");
                }
                requestContextProvider = (EventRequestContextProvider) applicationContext.getParent().getBean(names[0]);
            }
        }
        return requestContextProvider;
    }

    /**
     * @return the eventDao
     */
    public EventDao getEventDao() {
        if (eventDao == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventDao.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean EventDao not found!");
                }
                eventDao = (EventDao) applicationContext.getParent().getBean(names[0]);
            }
        }
        return eventDao;

    }

    private void notifyAboutChangedEventProperties() {
        publisher.publishEvent(new PropertiesChangedEvent(this));
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.publisher = applicationEventPublisher;
    }

    public RegistrationCloneClearService getRegistrationCloneClearService() {
        RegistrationCloneClearService service;
        synchronized (this) {
            String[] names = applicationContext.getParent().getBeanNamesForType(RegistrationCloneClearService.class);
            if (names.length == 0) {
                throw new NullPointerException("Bean RegistrationCloneClearService not found!");
            }
            service = (RegistrationCloneClearService) applicationContext.getParent().getBean(names[0]);
        }
        return service;
    }

    public CommonCloneClearService getCommonCloneClearService() {
        CommonCloneClearService service;
        synchronized (this) {
            String[] names = applicationContext.getParent().getBeanNamesForType(CommonCloneClearService.class);
            if (names.length == 0) {
                throw new NullPointerException("Bean CommonCloneClearService not found!");
            }
            service = (CommonCloneClearService) applicationContext.getParent().getBean(names[0]);
        }
        return service;
    }

    public ConferenceCloneClearService getConferenceCloneClearService() {
        ConferenceCloneClearService service;
        synchronized (this) {
            String[] names = applicationContext.getParent().getBeanNamesForType(ConferenceCloneClearService.class);
            if (names.length == 0) {
                throw new NullPointerException("Bean ConferenceCloneClearService not found!");
            }
            service = (ConferenceCloneClearService) applicationContext.getParent().getBean(names[0]);
        }
        return service;
    }

    public SchedulingCloneClearService getSchedulingCloneClearService() {
        SchedulingCloneClearService service;
        synchronized (this) {
            String[] names = applicationContext.getParent().getBeanNamesForType(SchedulingCloneClearService.class);
            if (names.length == 0) {
                throw new NullPointerException("Bean SchedulingCloneClearService not found!");
            }
            service = (SchedulingCloneClearService) applicationContext.getParent().getBean(names[0]);
        }
        return service;
    }
}
