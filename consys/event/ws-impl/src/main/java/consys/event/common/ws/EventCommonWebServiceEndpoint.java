package consys.event.common.ws;


import consys.event.overseer.connector.EventRequestContextProvider;
import consys.event.overseer.management.core.OverseerManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.server.endpoint.annotation.Endpoint;

/** 
 *
 * @author pepa
 */
@Endpoint
public class EventCommonWebServiceEndpoint implements EventCommonWebService, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(EventCommonWebServiceEndpoint.class);
    // -- Administration services
    private OverseerManager overseerManager;
    private EventRequestContextProvider requestContextProvider;
    private ApplicationContext applicationContext;
  

    /**
     * @return the overseerManager
     */
    public OverseerManager getOverseerManager() {
        if (overseerManager == null) {
            overseerManager = (OverseerManager) applicationContext.getParent().getBean("overseerManager", OverseerManager.class);
        }
        return overseerManager;
    }

    /**
     * @param overseerManager the overseerManager to set
     */
    public void setOverseerManager(OverseerManager overseerManager) {
        this.overseerManager = overseerManager;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    
    /**
     * @return the requestContextProvider
     */
    public EventRequestContextProvider getRequestContextProvider() {
        if (requestContextProvider == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventRequestContextProvider.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean EventRequestContextProvider not found!");
                }
                requestContextProvider = (EventRequestContextProvider) applicationContext.getParent().getBean(names[0]);
            }
        }
        return requestContextProvider;
    }
}
