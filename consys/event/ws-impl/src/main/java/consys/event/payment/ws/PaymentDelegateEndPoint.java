package consys.event.payment.ws;

import consys.common.utils.date.DateProvider;
import consys.event.common.core.EventOrderStateChangeListener;
import consys.event.overseer.connector.EventRequestContextProvider;
import consys.payment.webservice.DelegateWebService;
import consys.payment.ws.delegate.DelegateOrderStateChangedRequest;
import consys.payment.ws.delegate.DelegateOrderStateChangedResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/**
 *
 * @author Palo Gressa <gressa@acemcee.com>
 */
@Endpoint
public class PaymentDelegateEndPoint implements DelegateWebService, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(PaymentDelegateEndPoint.class);
    private ApplicationContext applicationContext;
    private List<EventOrderStateChangeListener> eventInvoiceProcessors;
    private EventRequestContextProvider requestContextProvider;

    public PaymentDelegateEndPoint() {
        eventInvoiceProcessors = (List<EventOrderStateChangeListener>) Collections.synchronizedList(new ArrayList<EventOrderStateChangeListener>());
    }

    @Override
    @PayloadRoot(localPart = "DelegateOrderStateChangedRequest", namespace = NAMESPACE)
    public DelegateOrderStateChangedResponse delegateOrderStateChanged(DelegateOrderStateChangedRequest request) {
        // init context
        getRequestContextProvider().initContext(request.getEventUuid(), request.getEventUuid());
        // process providers
        boolean result = false;
        Date date = null;
        if (request.getChangeDate() != null) {
            date = request.getChangeDate().toGregorianCalendar().getTime();
        } else {
            date = DateProvider.getCurrentDate();
        }
        List<EventOrderStateChangeListener> processors = getProcessors();
        logger.debug("Event({}) order({}) changed state to: {}", new String[]{request.getEventUuid(), request.getOrderUuid(), request.getToState().name()});
        EventOrderStateChangeListener.OrderState toState = null;
        switch (request.getToState()) {
            case CANCELED:
                toState = EventOrderStateChangeListener.OrderState.CANCELED;
                break;
            case CONFIRMED:
                toState = EventOrderStateChangeListener.OrderState.CONFIRMED;
                break;
            case NEED_TO_APPROVE:
                toState = EventOrderStateChangeListener.OrderState.APPROVE;
                break;
        }

        for (EventOrderStateChangeListener eventInvoiceProcessor : processors) {
            result = eventInvoiceProcessor.orderStateChanged(request.getOrderUuid(), toState, request.isNotify(), date);
            if (result) {
                // nasli sme a spracovali sme
                logger.debug("Processed invoice by {}", eventInvoiceProcessor.getClass());
                break;
            }
        }
        DelegateOrderStateChangedResponse response = OBJECT_FACTORY.createDelegateOrderStateChangedResponse();
        response.setProcessed(result);
        return response;
    }

    private synchronized List<EventOrderStateChangeListener> getProcessors() {
        if (eventInvoiceProcessors.isEmpty()) { // simple test
            // inicializujeme po prvy krat
            Map<String, EventOrderStateChangeListener> beans = applicationContext.getParent().getBeansOfType(EventOrderStateChangeListener.class);
            logger.info("Loading processors {}",beans.size());
            eventInvoiceProcessors.addAll(beans.values());
        }
        return eventInvoiceProcessors;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * @return the requestContextProvider
     */
    public synchronized EventRequestContextProvider getRequestContextProvider() {
        if (requestContextProvider == null) {
            String[] names = applicationContext.getParent().getBeanNamesForType(EventRequestContextProvider.class);
            if (names.length == 0) {
                throw new NullPointerException("Bean EventRequestContextProvider not found!");
            }
            requestContextProvider = (EventRequestContextProvider) applicationContext.getParent().getBean(names[0]);
        }
        return requestContextProvider;
    }
}
