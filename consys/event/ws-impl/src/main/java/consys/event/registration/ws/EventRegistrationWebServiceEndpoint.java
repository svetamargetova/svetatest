package consys.event.registration.ws;

import consys.common.core.exception.CouponCapacityException;
import consys.common.core.exception.CouponCodeException;
import consys.common.core.exception.CouponOutOfDateException;
import consys.common.core.exception.NoRecordException;
import consys.common.core.exception.RequiredPropertyNullException;
import consys.common.core.exception.ServiceExecutionFailed;
import consys.common.core.service.SystemPropertyService;
import consys.event.common.core.bo.UserEvent;
import consys.event.overseer.connector.EventRequestContextProvider;
import consys.event.overseer.management.core.OverseerManager;
import consys.common.core.exception.BundleCapacityFullException;
import consys.common.core.so.RegistrationOtherInfo;
import consys.common.utils.collection.Lists;
import consys.event.registration.core.bo.thumb.BundleOrderRequest;
import consys.event.registration.core.service.RegistrationBundleService;
import consys.event.registration.core.service.UserRegistrationService;
import consys.payment.service.exception.EventNotActiveException;
import consys.payment.service.exception.IbanFormatException;
import consys.payment.ws.bo.Profile;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;

/** 
 *
 * @author pepa
 */
@Endpoint
public class EventRegistrationWebServiceEndpoint implements EventRegistrationWebService, ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(EventRegistrationWebServiceEndpoint.class);
    // -- Administration services
    private OverseerManager overseerManager;
    private EventRequestContextProvider requestContextProvider;
    private ApplicationContext applicationContext;
    // -- Services
    private UserRegistrationService userRegistrationService;
    private RegistrationBundleService registrationBundleService;
    private SystemPropertyService systemPropertyService;

    @Override
    @PayloadRoot(localPart = "RegisterUserIntoEventRequest", namespace = NAMESPACE)
    public RegisterUserIntoEventResponse registerUserIntoEvent(RegisterUserIntoEventRequest request) {
        RegisterUserIntoEventResponse response = OBJECT_FACTORY.createRegisterUserIntoEventResponse();
        response.setResult(false);
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Registering user " + request.getEventUserOrganization().getFullName() + " into event " + request.getEventUuid() + "for budnle ");
            }
            // initializacia kontextu
            getRequestContextProvider().initContext(request.getEventUserOrganization().getUuid(), request.getEventUuid());

            // vytvorime noveho uzivatela
            UserEvent newUserEvent = new UserEvent();
            newUserEvent.setFullName(request.getEventUserOrganization().getFullName());
            newUserEvent.setLastName(request.getEventUserOrganization().getLastName());
            newUserEvent.setUuid(request.getEventUserOrganization().getUuid());
            newUserEvent.setOrganization(request.getEventUserOrganization().getOrganization());
            newUserEvent.setProfileImagePrefix(request.getEventUserOrganization().getPotraitImgPrefixUuid());
            newUserEvent.setEmail(request.getUserEmail());

            // vytvorime platobny profil
            Profile profile = new Profile();
            profile.setCity(request.getPaymentProfile().getCity());
            profile.setCountry(request.getPaymentProfile().getCountry());
            profile.setIBAN(request.getPaymentProfile().getIBAN());
            profile.setName(request.getPaymentProfile().getName());
            profile.setRegistrationNo(request.getPaymentProfile().getRegistrationNo());
            profile.setSWIFT(request.getPaymentProfile().getSWIFT());
            profile.setStreet(request.getPaymentProfile().getStreet());
            profile.setVatNo(request.getPaymentProfile().getVatNo());
            profile.setZip(request.getPaymentProfile().getZip());
            logger.info("Register new user: {}, {}", newUserEvent.getFullName(), request.getUserEmail());

            ProductOrder main = request.getMainBundle();
            BundleOrderRequest mainBundle = new BundleOrderRequest(main.getUuid(), main.getDiscountCode(), main.getQuantity());
            List<BundleOrderRequest> subbundles = Lists.newArrayListWithCapacity(request.getSubBundleOrders().size());
            for (ProductOrder order : request.getSubBundleOrders()) {
                subbundles.add(new BundleOrderRequest(order.getUuid(), order.getDiscountCode(), order.getQuantity()));
            }

            RegistrationOtherInfo otherInfo = new RegistrationOtherInfo();
            for (ParticipantAnswer pa : request.getAnswers()) {
                otherInfo.getAnswers().put(pa.getUuid(), pa.getAnswer());
            }

            getUserRegistrationService().createUserRegistrationForNewUser(newUserEvent, request.getUserEmail(),
                    mainBundle, subbundles, profile, otherInfo);
            response.setResult(true);
        } catch (CouponOutOfDateException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.COUPON_EXPIRED_EXPIRED);
        } catch (CouponCapacityException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.COUPON_CODE_EXCEPTION);
        } catch (CouponCodeException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.COUPON_CODE_EXCEPTION);
        } catch (RequiredPropertyNullException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.REQUIRED_PROPERTY_EXCEPTION);
        } catch (ServiceExecutionFailed ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.SERVICE_EXECUTION_FAILED);
        } catch (NoRecordException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.NO_RECORD_EXCEPTION);
        } catch (BundleCapacityFullException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.BUNDLE_CAPACITY_FULL_EXCEPTION);
        } catch (EventNotActiveException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.EVENT_NOT_ACTIVE_EXCEPTION);
        } catch (IbanFormatException ex) {
            response.setException(RegisterUserIntoEventExceptionEnum.IBAN_FORMAT_EXCEPTION);
        }
        return response;
    }

    /**
     * @return the overseerManager
     */
    public OverseerManager getOverseerManager() {
        if (overseerManager == null) {
            overseerManager = (OverseerManager) applicationContext.getParent().getBean("overseerManager", OverseerManager.class);
        }
        return overseerManager;
    }

    /**
     * @param overseerManager the overseerManager to set
     */
    public void setOverseerManager(OverseerManager overseerManager) {
        this.overseerManager = overseerManager;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    /**
     * @return the userEventService
     */
    public UserRegistrationService getUserRegistrationService() {
        if (userRegistrationService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(UserRegistrationService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean UserRegistrationService not found!");
                }
                userRegistrationService = (UserRegistrationService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return userRegistrationService;
    }

    public RegistrationBundleService getRegistrationBundleService() {
        if (registrationBundleService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(RegistrationBundleService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean RegistrationBundleService not found!");
                }
                registrationBundleService = (RegistrationBundleService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return registrationBundleService;
    }

    public SystemPropertyService getSystemPropertyService() {
        if (systemPropertyService == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(SystemPropertyService.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean SystemPropertyService not found!");
                }
                systemPropertyService = (SystemPropertyService) applicationContext.getParent().getBean(names[0]);
            }
        }
        return systemPropertyService;
    }

    /**
     * @return the requestContextProvider
     */
    public EventRequestContextProvider getRequestContextProvider() {
        if (requestContextProvider == null) {
            synchronized (this) {
                String[] names = applicationContext.getParent().getBeanNamesForType(EventRequestContextProvider.class);
                if (names.length == 0) {
                    throw new NullPointerException("Bean EventRequestContextProvider not found!");
                }
                requestContextProvider = (EventRequestContextProvider) applicationContext.getParent().getBean(names[0]);
            }
        }
        return requestContextProvider;
    }
}
