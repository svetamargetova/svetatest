package consys.event.b2b.api.properties;

public interface B2BProperties {

    public static final String B2B_MODULE_ENABLED = "b2b_module_enabled";
        
    public static final String DATES_FORMAT = "dd-MM-yyyy hh:mm:ss";
    
    public static final String B2B_REGISTRATION_START = "event_registration_from";
    public static final String B2B_REGISTRATION_END = "event_registration_to";
 
    public static final String B2B_BOOKINGS_START = "event_booking_from";
    public static final String B2B_BOOKINGS_END = "event_booking_to";
    
    public static final String B2B_MEETINGS_START = "event_meetings_from";
    public static final String B2B_MEETINGS_END = "event_meetings_to";    
}
