package consys.event.b2b.api.right;

import consys.event.common.api.right.ModuleRoleModel;

public class EventB2BModuleRoleModel extends ModuleRoleModel {

    public static final String RIGHT_B2B_AGENT = "RIGHT_B2B_AGENT";
    public static final String RIGHT_B2B_ADMINISTRATOR = "RIGHT_B2B_ADMINISTRATOR";

    public EventB2BModuleRoleModel() {
	super(5);
    }

    @Override
    public void registerRights() {
	this.addRole(RIGHT_B2B_AGENT, false);
	this.addRole(RIGHT_B2B_ADMINISTRATOR, true);
    }
}
