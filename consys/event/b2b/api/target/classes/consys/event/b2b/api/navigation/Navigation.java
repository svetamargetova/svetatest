/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package consys.event.b2b.api.navigation;

/**
 *
 * @author palo
 */
public interface Navigation {
 
    
    public static final String AGENT_DASHBOARD = "b2b-dashboard";
    public static final String AGENT_ORGANIZATION_PROFILE = "b2b-organization-profile";
    public static final String AGENT_PROFILE = "b2b-agent-profile";
    public static final String AGENT_COOPERATION_PROFILE = "b2b-cooperation-profile";
    public static final String AGENT_PARTICIPANTS = "b2b-participants";
    public static final String AGENT_MEETINGS = "b2b-meetings";
    
    
}
