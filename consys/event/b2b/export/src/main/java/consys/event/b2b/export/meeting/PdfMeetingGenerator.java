package consys.event.b2b.export.meeting;

import com.itextpdf.text.*;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.html.simpleparser.StyleSheet;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfWriter;
import consys.event.b2b.core.bo.thumb.MeetingExportData;
import consys.event.export.pdf.DefaultFontProvider;
import consys.event.export.pdf.FontProvider;
import consys.event.export.pdf.FontProvider.FontStyle;
import consys.event.export.pdf.NewPage;
import consys.event.export.pdf.PdfA4Utils;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author pepa
 */
public class PdfMeetingGenerator implements NewPage {

    // konstanty
    private static final float CONTENT_WIDTH = 170;
    private static final float LEFT_BORDER = 20;
    private static final long HOUR = 3600000l;
    // data
    private Document document;
    private PdfWriter writer;
    private StyleSheet styles;
    private HashMap<String, Object> providers;
    private SimpleDateFormat format = new SimpleDateFormat("HH:mm     d. MMMM, yyyy", Locale.ENGLISH);

    public PdfMeetingGenerator() {
        HashMap s = new HashMap();
        s.put("size", "11pt");

        styles = new StyleSheet();
        styles.loadTagStyle("body", s);

        providers = new HashMap<String, Object>();
    }

    private void addMetadata() {
        document.addTitle("Meeting");
        document.addAuthor("Takeplace");
        document.addCreator("Takeplace");
        document.setMargins(0, 0, 0, 0);
    }

    public void createPdfMeeting(OutputStream os, MeetingExportData meetingInfo, boolean closeOutputStream) throws Exception {
        List<MeetingExportData> meetingInfos = new ArrayList<MeetingExportData>();
        meetingInfos.add(meetingInfo);
        generate(os, meetingInfos, closeOutputStream);
    }

    public void createPdfMeeting(OutputStream os, List<MeetingExportData> meetingInfos, boolean closeOutputStream) throws Exception {
        generate(os, meetingInfos, closeOutputStream);
    }

    private void generate(OutputStream os, List<MeetingExportData> meetingInfos, boolean closeOutputStream) throws Exception {
        providers.put(HTMLWorker.FONT_PROVIDER, new DefaultFontProvider());

        startWork(os);

        for (MeetingExportData meetingInfo : meetingInfos) {
            float yLineMM = addHead(meetingInfo);

            yLineMM = addOrganizationName(yLineMM + 3, meetingInfo);

            // yLineMM = addOrganizationNotes(yLineMM);
            yLineMM = addProfileInfo(yLineMM, meetingInfo);

            if (meetingInfo.getNotes() != null && !meetingInfo.getNotes().isEmpty()) {
                yLineMM = addPreparedNotes(yLineMM, meetingInfo);
            }

            addHandNotes(yLineMM);

            newPage();
        }

        stopWork(closeOutputStream);
    }

    private void startWork(OutputStream os) throws Exception {
        document = new Document(PageSize.A4);
        writer = PdfWriter.getInstance(document, os);

        addMetadata();

        document.open();
    }

    private void stopWork(boolean closeOutputStream) {
        if (closeOutputStream) {
            writer.flush();
            document.close();
        }
    }

    /** vlozi hlavicku: datum, misto a oddelovac */
    private float addHead(MeetingExportData meetingInfo) throws Exception {
        StringBuilder sb;
        if (meetingInfo.getMeetingTime() == null) {
            sb = new StringBuilder("Not scheduled meeting yet");
        } else {
            // TODO: pozor, tady se nejdede pres casovou zonu, ale pricita se primo jedna hodina
            sb = new StringBuilder(format.format(new Date(meetingInfo.getMeetingTime().getTime() + HOUR)));
            sb.append("   /   ").append(meetingInfo.getTableName());
            //sb.append(meetingInfo.getTableIdx());
        }


        Paragraph p = new Paragraph(sb.toString(), FontProvider.font(FontStyle.ARIAL_14_BOLD));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, 10, CONTENT_WIDTH, 10);
        ct.addElement(p);
        ct.go();

        float yLineMM = PdfA4Utils.yPositionInMM(ct.getYLine()) + 2;

        Rectangle r = PdfA4Utils.rectangle(LEFT_BORDER, yLineMM, CONTENT_WIDTH, 0.5f);
        r.setBackgroundColor(BaseColor.BLACK);
        document.add(r);

        return yLineMM + 0.5f;
    }

    /** vlozi nazev organizace */
    private float addOrganizationName(float yLineMM, MeetingExportData meetingInfo) throws Exception {
        Paragraph name = new Paragraph(meetingInfo.getOrganizationTitle(), FontProvider.font(FontStyle.ARIAL_13_BOLD));
        Paragraph user = new Paragraph(meetingInfo.getAgentName(), FontProvider.font(FontStyle.ARIAL_11_ITALICS));
        user.setSpacingBefore(PdfA4Utils.yps(2));

        List<Element> list = new ArrayList<Element>();
        list.add(name);
        list.add(user);

        ColumnText ctName = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 8);
        float yLine = ctName.getYLine();
        ctName.addElement(name);
        ctName.addElement(user);

        PdfA4Utils.fitContentToColumnText(ctName, list, 8, yLine, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 5, this);

        return PdfA4Utils.yPositionInMM(ctName.getYLine()) + 5;
    }

    /** vlozi info o organizaci */
    private float addOrganizationInfo(float yLineMM, MeetingExportData meetingInfo) throws Exception {
        Paragraph name = new Paragraph(meetingInfo.getOrganizationTitle(), FontProvider.font(FontStyle.ARIAL_11_BOLD));

        ColumnText ctName = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 8);
        ctName.addElement(name);
        ctName.go();

        final float y = PdfA4Utils.yPositionInMM(ctName.getYLine()) + 2;

        ColumnText ctDescription = PdfA4Utils.columnText(writer, LEFT_BORDER, y, CONTENT_WIDTH, 8);
        final float yLine = ctDescription.getYLine();

        List<Element> elements = HTMLWorker.parseToList(new StringReader(meetingInfo.getOrganizationDescription()), styles, providers);
        for (Element element : elements) {
            ctDescription.addElement(element);
        }

        PdfA4Utils.fitContentToColumnText(ctDescription, elements, 5, yLine, LEFT_BORDER, y, CONTENT_WIDTH, 50, this);

        return PdfA4Utils.yPositionInMM(ctDescription.getYLine()) + 5;
    }

    /** vlozi info o profilu */
    private float addProfileInfo(float yLineMM, MeetingExportData meetingInfo) throws Exception {
        float y;
        if (meetingInfo.getProfileTitle() != null) {
            Paragraph profile = new Paragraph("Profile:", FontProvider.font(FontStyle.ARIAL_11_BOLD));

            ColumnText ctProfile = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, 15, 8);
            ctProfile.addElement(profile);
            ctProfile.go();

            Paragraph name = new Paragraph(meetingInfo.getProfileTitle(), FontProvider.font(FontStyle.ARIAL_11_BOLD));

            ColumnText ctName = PdfA4Utils.columnText(writer, LEFT_BORDER + 15, yLineMM, CONTENT_WIDTH - 15, 8);
            float yLineName = ctName.getYLine();
            ctName.addElement(name);
            ctName.go();

            PdfA4Utils.fitContentToColumnText(ctName, name, 8, yLineName, LEFT_BORDER + 15, yLineMM, CONTENT_WIDTH - 15, 5, this);

            y = PdfA4Utils.yPositionInMM(ctName.getYLine()) + 2;
        } else {
            y = yLineMM;
        }

        // Vlozit info o request offer 

        ColumnText ctDescription = PdfA4Utils.columnText(writer, LEFT_BORDER, y, CONTENT_WIDTH, 8);
        final float yLine = ctDescription.getYLine();

        List<Element> elements = HTMLWorker.parseToList(new StringReader(meetingInfo.getProfileTitle() == null
                ? meetingInfo.getOrganizationDescription()
                : meetingInfo.getProfileDescription()), styles, providers);
        for (Element element : elements) {
            ctDescription.addElement(element);
        }

        PdfA4Utils.fitContentToColumnText(ctDescription, elements, 5, yLine, LEFT_BORDER, y, CONTENT_WIDTH, 50, this);

        return PdfA4Utils.yPositionInMM(ctDescription.getYLine()) + 5;
    }

    /** vlozi pripravene poznamky */
    private float addPreparedNotes(float yLineMM, MeetingExportData meetingInfo) throws Exception {
        Paragraph p = new Paragraph("Prepared notes:", FontProvider.font(FontStyle.ARIAL_11_BOLD));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 8);
        ct.addElement(p);
        ct.go();

        final float y = PdfA4Utils.yPositionInMM(ct.getYLine()) + 2;
        ColumnText ctHTML = PdfA4Utils.columnText(writer, LEFT_BORDER, y, CONTENT_WIDTH, 5);
        final float yLine = ctHTML.getYLine();

        List<Element> elements = HTMLWorker.parseToList(new StringReader(meetingInfo.getNotes()), styles, providers);
        for (Element element : elements) {
            ctHTML.addElement(element);
        }

        PdfA4Utils.fitContentToColumnText(ctHTML, elements, 5, yLine, LEFT_BORDER, y, CONTENT_WIDTH, 50, this);

        return PdfA4Utils.yPositionInMM(ctHTML.getYLine()) + 5;
    }

    /** vlozi misto pro rucne psane poznamky */
    private void addHandNotes(float yLineMM) throws Exception {
        // TODO: dodelat odstrankovani kdyby se nemely rucni poznamky vlezt do aktuallni stranky

        float startFrom = yLineMM + 8;
        // vyska stranky - spodni mezera - misto zacatku ramecku
        float height = 297 - 10 - startFrom;

        Paragraph p = new Paragraph("Hand notes:", FontProvider.font(FontStyle.ARIAL_11_BOLD));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 8);
        ct.addElement(p);
        ct.go();

        Rectangle r1 = PdfA4Utils.rectangle(LEFT_BORDER, startFrom, CONTENT_WIDTH, height);
        r1.setBackgroundColor(BaseColor.BLACK);
        document.add(r1);

        Rectangle r2 = PdfA4Utils.rectangle(LEFT_BORDER + 0.5f, startFrom + 0.5f, CONTENT_WIDTH - 1, height - 1);
        r2.setBackgroundColor(BaseColor.WHITE);
        document.add(r2);
    }

    @Override
    public float newPage(float yLineMM) throws Exception {
        // dodelat v pripade potreby
        return yLineMM;
    }

    @Override
    public float newPage() throws Exception {
        document.newPage();
        // navratova hodnota se v teto tride nevyuziva
        return 0f;
    }
}
