package consys.event.b2b.export.meeting;

import com.itextpdf.text.*;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.html.simpleparser.StyleSheet;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import consys.event.b2b.core.bo.AreaOfActivity;
import consys.event.b2b.core.bo.CooperationProfile;
import consys.event.b2b.core.bo.CooperationProfileType;
import consys.event.b2b.core.bo.thumb.OrganizationExportData;
import consys.event.export.pdf.*;
import consys.event.export.pdf.FontProvider;
import consys.event.export.pdf.FontProvider.FontStyle;
import consys.event.export.pdf.NewPage;
import consys.event.export.pdf.PdfA4Utils;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author pepa
 */
public class PdfOrganizationsGenerator implements NewPage {

    // konstanty
    private static final float CONTENT_WIDTH = 170;
    private static final float LEFT_BORDER = 20;
    private static final float PAGE_THRESHOLD = 270;
    private static final String REGEXP_REMOVE_STYLE = "style=\"[^\"]*\"";
    private static final String REGEXP_REMOVE_CLASS = "class=\"[^\"]*\"";
    // data
    private Document document;
    private PdfWriter writer;
    private StyleSheet styles;
    private HashMap<String, Object> providers;
    private float contentStart;

    public PdfOrganizationsGenerator() {
        HashMap s = new HashMap();
        s.put("size", "10pt");

        styles = new StyleSheet();
        styles.loadTagStyle("body", s);

        providers = new HashMap<String, Object>();
    }

    private void addMetadata() {
        document.addTitle("Organizations");
        document.addAuthor("Takeplace");
        document.addCreator("Takeplace");
        document.setMargins(0, 0, 0, 0);
    }

    public void createPdfOrganization(OutputStream os, List<OrganizationExportData> items, boolean closeOutputStream) throws Exception {
        generate(os, items, closeOutputStream);
    }

    private void generate(OutputStream os, List<OrganizationExportData> items, boolean closeOutputStream) throws Exception {
        providers.put(HTMLWorker.FONT_PROVIDER, new DefaultFontProvider());

        startWork(os);

        float yLineMM = addHead();
        for (int i = 0; i < items.size(); i++) {
            OrganizationExportData data = items.get(i);
            yLineMM = addSquareTitle(yLineMM, "Company Profile " + data.getTitle(), new BaseColor(0, 0, 255));
            yLineMM = addSquareTitle(yLineMM + 5, data.getTitle() + " profile", new BaseColor(0, 128, 0));
            yLineMM = addProfile(yLineMM + 2, data);
            yLineMM = addSquareTitle(yLineMM + 5, "Cooperation profile(s)", new BaseColor(0, 128, 0));
            addCooperationProfiles(yLineMM + 5, data);
            if (i < items.size() - 1) {
                yLineMM = newPage();
            }
        }

        stopWork(closeOutputStream);
    }

    private void startWork(OutputStream os) throws Exception {
        document = new Document(PageSize.A4);

        writer = PdfWriter.getInstance(document, os);
        writer.setPageEvent(new PageHelper());

        addMetadata();

        document.open();
    }

    private void stopWork(boolean closeOutputStream) {
        if (closeOutputStream) {
            writer.flush();
            document.close();
        }
    }

    /** prida text zahlavi */
    private float addHead() throws Exception {
        Paragraph p = new Paragraph("B2B All Participants",
                FontProvider.font(FontStyle.ARIAL_9_BOLD));
        p.setAlignment(Paragraph.ALIGN_CENTER);

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, 7, CONTENT_WIDTH, 7);
        ct.addElement(p);
        ct.go();

        contentStart = PdfA4Utils.yPositionInMM(ct.getYLine()) + 3;
        return contentStart;
    }

    /** prida titulek s barevnym ctvereckem */
    private float addSquareTitle(float yLineMM, String title, BaseColor color) throws Exception {
        final float from = yLineMM + 5;

        Rectangle r = PdfA4Utils.rectangle(LEFT_BORDER, from + 3, 4, 4);
        r.setBackgroundColor(color);
        document.add(r);

        Paragraph p = new Paragraph(title, FontProvider.font(FontStyle.ARIAL_13_BOLD));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER + 7, from, CONTENT_WIDTH, 10);
        ct.addElement(p);
        ct.go();

        return PdfA4Utils.yPositionInMM(ct.getYLine());
    }

    /** vlozi profil organizace */
    private float addProfile(float yLineMM, OrganizationExportData data) throws Exception {
        // TODO: upravit vytvoreni pokud neco chybi
        StringBuilder address = new StringBuilder();
        address.append(data.getStreet());
        address.append(", ");
        address.append(data.getZip());
        address.append(" ");
        address.append(data.getCity());
        address.append(", ");
        address.append(data.getCountry());

        float line = addOverviewRow(yLineMM, "Name", data.getTitle());
        line = addOverviewRow(line, "Address", address.toString());
        line = addOverviewRow(line, "Phone number", data.getPhone());
        line = addOverviewRow(line, "Website", data.getWeb());
        if (data.getAgent() != null && data.getAgent().getFullName() != null && !data.getAgent().getFullName().isEmpty()) {
            line = addOverviewRow(line + 5, "Agent", data.getAgent().getFullName());
        }

        line = addAboutOrganisation(line + 5, data);

        line = addAreasOfActivity(line + 5, data);

        return line;
    }

    /** vlozi jeden radek zakladnich informaci o organizaci */
    private float addOverviewRow(float yLineMM, String title, String value) throws Exception {
        StringBuilder sb = new StringBuilder(title);
        sb.append(": ");
        sb.append(value);

        Paragraph p = new Paragraph(sb.toString(), FontProvider.font(FontStyle.ARIAL_10_REGULAR));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 10);
        ct.addElement(p);
        ct.go();

        return PdfA4Utils.yPositionInMM(ct.getYLine());
    }

    private float addAboutOrganisation(float yLineMM, OrganizationExportData data) throws Exception {
        Paragraph p = new Paragraph("About organisation", FontProvider.font(FontStyle.ARIAL_10_REGULAR));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 8);
        ct.addElement(p);
        ct.go();

        final float y = PdfA4Utils.yPositionInMM(ct.getYLine()) + 2;
        ColumnText ctHTML = PdfA4Utils.columnText(writer, LEFT_BORDER, y, CONTENT_WIDTH, 5);
        final float yLine = ctHTML.getYLine();

        String html = data.getDescription() == null ? "" : data.getDescription();
        html = html.replaceAll(REGEXP_REMOVE_CLASS, "");
        html = html.replaceAll(REGEXP_REMOVE_STYLE, "");

        List<Element> elements = HTMLWorker.parseToList(new StringReader(html), styles, providers);
        for (Element element : elements) {
            ctHTML.addElement(element);
        }

        ColumnTextHelper helper = new ColumnTextHelper(ctHTML, elements, this);
        helper.setColumnTextPostion(LEFT_BORDER, y);
        helper.setColumnTextSize(CONTENT_WIDTH, 5);
        helper.setPageContentPosition(contentStart, PAGE_THRESHOLD);
        helper.run(yLine);

        return PdfA4Utils.yPositionInMM(ctHTML.getYLine());
    }

    /** vlozi aktivity */
    private float addAreasOfActivity(float yLineMM, OrganizationExportData data) throws Exception {
        // TODO: je potreba kontrolovat jestli nejsou oblasti prazdne?

        Paragraph p = new Paragraph("AREAS OF ACTIVITY", FontProvider.font(FontStyle.ARIAL_11_BOLD));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 8);
        ct.addElement(p);
        ct.go();

        float line = PdfA4Utils.yPositionInMM(ct.getYLine());
        line = newPage(line);
        for (AreaOfActivity aoa : data.getAreas()) {
            line = addActivity(line + 4, aoa);
            line = newPage(line);
        }

        return line;
    }

    /** vlozi jednu aktivitu */
    private float addActivity(float yLineMM, AreaOfActivity aoa) throws Exception {
        float line = newPage(yLineMM);

        Paragraph p = new Paragraph(aoa.getTitle(), FontProvider.font(FontStyle.ARIAL_10_REGULAR));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, line, CONTENT_WIDTH, 8);
        ct.addElement(p);
        ct.go();

        line = PdfA4Utils.yPositionInMM(ct.getYLine()) + 1;
        for (AreaOfActivity a : aoa.getChildrens()) {
            line = addListItem(line, a.getTitle());
            line = newPage(line);
        }

        return line;
    }

    /** vlozi jednu polozku seznamu */
    private float addListItem(float yLineMM, String title) throws Exception {
        Rectangle r = PdfA4Utils.rectangle(LEFT_BORDER, yLineMM + 3, 2, 2);
        r.setBackgroundColor(BaseColor.BLACK);
        document.add(r);

        Paragraph p = new Paragraph(title, FontProvider.font(FontStyle.ARIAL_10_REGULAR));

        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER + 4, yLineMM, CONTENT_WIDTH - 4, 7);
        ct.addElement(p);
        ct.go();

        return PdfA4Utils.yPositionInMM(ct.getYLine());
    }

    /** prida kooperacni profily */
    private void addCooperationProfiles(float yLineMM, OrganizationExportData data) throws Exception {
        float line = yLineMM;
        for (CooperationProfile cp : data.getCooperationProfiles()) {
            if (onNewPage(line + 14)) {
                line = newPage(line + 14);
            }

            Paragraph title = new Paragraph(cp.getTitle(), FontProvider.font(FontStyle.ARIAL_10_BOLD));
            ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, line, CONTENT_WIDTH, 7);
            float yLine = ct.getYLine();
            ct.addElement(title);

            PdfA4Utils.fitContentToColumnText(ct, title, 7, yLine, LEFT_BORDER, line, CONTENT_WIDTH, 4, this);

            line = PdfA4Utils.yPositionInMM(ct.getYLine()) + 5;
            line = newPage(line);

            // offered
            boolean isOffered = false;
            boolean isRequested = false;
            for (CooperationProfileType cpt : cp.getCooperationProfileTypes()) {
                // kontrola jestli jsou vubec nejake polozky v offered nebo requested
                if (cpt.isOffer()) {
                    isOffered = true;
                }
                if (cpt.isRequest()) {
                    isRequested = true;
                }
            }
            line = addOffered(line, cp, isOffered);
            line = newPage(line);

            // requested
            line = addRequested(line, cp, isRequested, isOffered);

            // description
            if (isOffered || isRequested) {
                line += 5;
            }
            line = newPage(line);
            line = addCooperationProfileDescription(line, cp);
            line += 5;
        }
    }

    private float addOffered(float line, CooperationProfile cp, boolean isOffered) throws Exception {
        if (isOffered) {
            if (onNewPage(line + 7)) {
                // odstrankuje pokud by se nemel vlezt nadpis offered
                line = newPage(line + 7);
            }
            line = addOfferedOrRequested(line, cp, true);
            line = newPage(line);

            for (CooperationProfileType cpt : cp.getCooperationProfileTypes()) {
                if (cpt.isOffer()) {
                    line = addListItem(line, cpt.getTypeName());
                    line = newPage(line);
                }
            }
        }
        return line;
    }

    private float addRequested(float yLineMM, CooperationProfile cp, boolean isRequested, boolean isOffered) throws Exception {
        float line = yLineMM;
        if (isRequested) {
            if (isOffered) {
                line += 4;
            }
            if (onNewPage(line + 7)) {
                // odstrankuje pokud by se nemel vlezt nadpis requested
                line = newPage(line + 7);
            }
            line = addOfferedOrRequested(line, cp, false);
            line = newPage(line);

            for (CooperationProfileType cpt : cp.getCooperationProfileTypes()) {
                if (cpt.isRequest()) {
                    line = addListItem(line, cpt.getTypeName());
                    line = newPage(line);
                }
            }
        }
        return line;
    }

    /** prida Offered: nebo Requested: */
    private float addOfferedOrRequested(float yLineMM, CooperationProfile cp, boolean offered) throws Exception {
        Paragraph title = new Paragraph(offered ? "Offered:" : "Requested:", FontProvider.font(FontStyle.ARIAL_10_BOLD));
        ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, yLineMM, CONTENT_WIDTH, 7);
        ct.addElement(title);
        ct.go();

        return PdfA4Utils.yPositionInMM(ct.getYLine());
    }

    /** vlozi popisek kooperacniho profilu */
    private float addCooperationProfileDescription(float yLineMM, CooperationProfile cp) throws Exception {
        final float y = yLineMM;
        ColumnText ctHTML = PdfA4Utils.columnText(writer, LEFT_BORDER, y, CONTENT_WIDTH, 5);
        final float yLine = ctHTML.getYLine();

        String html = cp.getDescription() == null ? "" : cp.getDescription();
        html = html.replaceAll(REGEXP_REMOVE_CLASS, "");
        html = html.replaceAll(REGEXP_REMOVE_STYLE, "");

        List<Element> elements = HTMLWorker.parseToList(new StringReader(html), styles, providers);
        for (Element element : elements) {
            ctHTML.addElement(element);
        }

        ColumnTextHelper helper = new ColumnTextHelper(ctHTML, elements, this);
        helper.setColumnTextPostion(LEFT_BORDER, y);
        helper.setColumnTextSize(CONTENT_WIDTH, 5);
        helper.setPageContentPosition(contentStart, PAGE_THRESHOLD);
        helper.run(yLine);

        return PdfA4Utils.yPositionInMM(ctHTML.getYLine());
    }

    @Override
    public float newPage(float yLineMM) throws Exception {
        if (onNewPage(yLineMM)) {
            return newPage();
        }
        return yLineMM;
    }

    /** test na to jestli se bude odstrankovavat */
    private boolean onNewPage(float yLineMM) {
        return yLineMM > PAGE_THRESHOLD;
    }

    /** nova stranka */
    @Override
    public float newPage() throws Exception {
        document.newPage();
        return addHead();
    }

    private class PageHelper extends PdfPageEventHelper {

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            super.onEndPage(writer, document);
            try {
                Paragraph page = new Paragraph(String.valueOf(writer.getPageNumber()), FontProvider.font(FontStyle.ARIAL_8_REGULAR));
                page.setAlignment(Paragraph.ALIGN_RIGHT);
                ColumnText ct = PdfA4Utils.columnText(writer, LEFT_BORDER, 285, CONTENT_WIDTH, 10);
                ct.addElement(page);
                ct.go();
            } catch (Exception ex) {
                throw new IllegalStateException("Problem input page number");
            }
        }
    }
}
