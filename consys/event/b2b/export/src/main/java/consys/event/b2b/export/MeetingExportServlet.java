package consys.event.b2b.export;

import consys.event.b2b.core.bo.thumb.MeetingExportData;
import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import consys.event.b2b.core.service.MeetingService;
import consys.event.b2b.core.service.OrganizationService;
import consys.event.b2b.export.meeting.PdfMeetingGenerator;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author palo
 */
public class MeetingExportServlet extends HttpServlet {

    private static final long serialVersionUID = -8985516539488705530L;
    private static final Logger logger = LoggerFactory.getLogger(MeetingExportServlet.class);
    // konstanty    
    private static final String CONTENT_TYPE = "application/pdf";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
    private static final String END_NAME = "\"";
    private static final String CACHE_CONTROL = "Cache-control";
    private static final String MAXAGE_1S = "maxage=1";
    private static final String FILENAME_PREFIX = "Meeting: ";
    private static final String PDF_SUFFIX = ".pdf";
    // servicy
    @Autowired
    private MeetingService meetingService;
    @Autowired
    private OrganizationService organizationService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String idp = request.getParameter("id");
            String uid = request.getParameter("uid");
            String date = request.getParameter("date");

            PdfMeetingGenerator generator = new PdfMeetingGenerator();

            if (StringUtils.isNotBlank(idp)) {
                List<MeetingExportData> meetings = meetingService.listMeetingExportDataForOrganization(Integer.parseInt(idp));
                Collections.sort(meetings);
                setResponseHeaders("Meetings", response);
                generator.createPdfMeeting(response.getOutputStream(), meetings, true);
            } else if (StringUtils.isNotBlank(uid) && StringUtils.isNotBlank(date)) {
                List<MeetingExportData> meetings = meetingService.listMeetingExportData(uid, date);
                Collections.sort(meetings);
                String dateS = FastDateFormat.getInstance("dd.mm.yyyy").format(new Date(Long.parseLong(date)));
                setResponseHeaders(dateS, response);
                generator.createPdfMeeting(response.getOutputStream(), meetings, true);

            } else {
                // DO ZIPU       
                response.setCharacterEncoding("UTF-8");

                ZipArchiveOutputStream zipOutputStream = new ZipArchiveOutputStream(response.getOutputStream());
                zipOutputStream.setEncoding("UTF-8");
                zipOutputStream.setFallbackToUTF8(true);
                zipOutputStream.setUseLanguageEncodingFlag(true);


                setZipResponseHeaders("All meetings", response);


                List<OrganizationThumb> organizations = organizationService.listAllOrganizations();
                for (OrganizationThumb organizationThumb : organizations) {
                    logger.debug("Export organization {}", organizationThumb.getOrganizationName());


                    List<MeetingExportData> meetings = meetingService.listMeetingExportDataForOrganization(organizationThumb.getOrganizationId());
                    if (!meetings.isEmpty()) {
                        logger.debug("Meetings {}", meetings.size());

                        Collections.sort(meetings);

                        String orgname = organizationThumb.getOrganizationName().replaceAll("/", "-");

                        // vytvorime archiv
                        String fileName = decompose(String.format("%s-%s.pdf", organizationThumb.getAgentFullname(), orgname));



                        ZipArchiveEntry entry = new ZipArchiveEntry(fileName);
                        zipOutputStream.putArchiveEntry(entry);
                        ByteArrayOutputStream tmpOutputStream = new ByteArrayOutputStream();
                        generator = new PdfMeetingGenerator();
                        generator.createPdfMeeting(tmpOutputStream, meetings, true);

                        zipOutputStream.write(tmpOutputStream.toByteArray());
                        zipOutputStream.closeArchiveEntry();
                    }
                }
                zipOutputStream.close();
            }

            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (Exception ex) {
            logger.error("Exception while generating ticket: ", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getLocalizedMessage());
        }
    }

    public static String decompose(String s) {
        return java.text.Normalizer.normalize(s, java.text.Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    private void setResponseHeaders(String eventName, HttpServletResponse response) {
        StringBuilder attachment = new StringBuilder(ATTACHMENT_FILENAME);
        attachment.append(FILENAME_PREFIX);
        attachment.append(eventName);
        attachment.append(PDF_SUFFIX);
        attachment.append(END_NAME);

        response.setContentType(CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION, attachment.toString());
        response.setHeader(CACHE_CONTROL, MAXAGE_1S);
    }

    private void setZipResponseHeaders(String eventName, HttpServletResponse response) {
        StringBuilder attachment = new StringBuilder(ATTACHMENT_FILENAME);
        attachment.append(eventName);
        attachment.append(".zip");
        attachment.append(END_NAME);

        response.setContentType("application/zip");
        response.setHeader(CONTENT_DISPOSITION, attachment.toString());
        response.setHeader(CACHE_CONTROL, MAXAGE_1S);
    }
}
