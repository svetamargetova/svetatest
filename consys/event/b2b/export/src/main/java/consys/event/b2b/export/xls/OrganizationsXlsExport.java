package consys.event.b2b.export.xls;

import consys.event.b2b.core.bo.thumb.OrganizationThumb;
import consys.event.common.core.service.list.Constraints;
import consys.event.common.gwt.server.list.export.xls.AbstractXlsExportHandler;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author pepa
 */
public class OrganizationsXlsExport extends AbstractXlsExportHandler {

    private static final String[] allTitles = {"Organization", "Name", "Position", "Phone", "Website", "Country", "City", "Street", "Zip"};
    private List<OrganizationThumb> organizations;

    public OrganizationsXlsExport(List<OrganizationThumb> organizations) {
        super("");
        this.organizations = organizations;
    }

    @Override
    public void buildExport(HSSFWorkbook workbook, Constraints c) {
        // all header row
        HSSFSheet all = workbook.createSheet("All");
        createHead(all, allTitles);
        int allRows = 1;

        for (OrganizationThumb org : organizations) {
            Row row = all.createRow(allRows++);
            addRow(row, org);
        }

        // autosize sloupcu
        autosize(all, allTitles.length);
    }

    /** vytvori hlavicku v sheetu */
    private void createHead(HSSFSheet sheet, String[] titles) {
        Row headerRow = sheet.createRow(0);
        headerRow.setHeightInPoints(12f);
        for (int i = 0; i < titles.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(titles[i]);
            cell.setCellStyle(style.getHeaderStyle());
        }
    }

    /** pokud je zadano all nezohlednuje se hodnota v checked */
    private void addRow(Row row, OrganizationThumb org) {
        int counter = 0;

        setTextCell(row, counter++, string(org.getOrganizationName()));
        setTextCell(row, counter++, string(org.getAgentFullname()));
        setTextCell(row, counter++, string(org.getAgentPosition()));
        setTextCell(row, counter++, string(org.getOrganizationPhone()));
        setTextCell(row, counter++, string(org.getOrganizationWebsite()));
        setTextCell(row, counter++, string(org.getOrganizationCountry()));
        setTextCell(row, counter++, string(org.getOrganizationCity()));
        setTextCell(row, counter++, string(org.getOrganizationStreet()));
        setTextCell(row, counter++, string(org.getOrganizationZip()));
    }

    private void autosize(HSSFSheet sheet, int columns) {
        for (int i = 0; i < columns; i++) {
            sheet.autoSizeColumn(i);
        }
    }
}
