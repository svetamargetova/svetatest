package consys.event.b2b.export;

import consys.common.core.bo.SystemProperty;
import consys.common.core.service.SystemPropertyService;
import consys.event.b2b.core.service.OrganizationService;
import consys.event.badges.core.export.BadgeParticipantInfo;
import consys.event.badges.core.export.BadgeSizeType;
import consys.event.badges.core.export.PdfBadgesGenerator;
import consys.event.common.api.properties.CommonProperties;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author palo
 */
public class BadgeExportServlet extends HttpServlet {

    private static final long serialVersionUID = -8985516539488705530L;
    private static final Logger logger = LoggerFactory.getLogger(BadgeExportServlet.class);
    // konstanty    
    private static final String CONTENT_TYPE = "application/pdf";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
    private static final String END_NAME = "\"";
    private static final String CACHE_CONTROL = "Cache-control";
    private static final String MAXAGE_1S = "maxage=1";
    private static final String FILENAME_PREFIX = "B2B Badges";
    private static final String PDF_SUFFIX = ".pdf";
    // servicy
    @Autowired
    private OrganizationService organizationService;
    @Autowired
    private SystemPropertyService systemPropertyService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(config.getServletContext());
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
    }

    /** Zpracovani GET pozadavku */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    /** Zpracovani POST pozadavku */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            // nacteni acronymu eventu
            SystemProperty acronymProperty = systemPropertyService.loadSystemProperty(CommonProperties.EVENT_ACRONYM);
            String acronym = acronymProperty.getValue() == null ? "" : acronymProperty.getValue();

            // nacteni agentu
            List<BadgeParticipantInfo> agents = organizationService.listAllOrganizationAgents();
            for (BadgeParticipantInfo a : agents) {
                // nastaveni acronymu do badge
                a.setEventAcronym(acronym);
            }

            setResponseHeaders(response);
            PdfBadgesGenerator generator = new PdfBadgesGenerator();
            generator.createPdfBadges(response.getOutputStream(), agents, BadgeSizeType.SIZE_90_X_50, true);
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (Exception ex) {
            logger.error("Exception while generating badges: ", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getLocalizedMessage());
        }
    }

    private void setResponseHeaders(HttpServletResponse response) {
        StringBuilder attachment = new StringBuilder(ATTACHMENT_FILENAME);
        attachment.append(FILENAME_PREFIX);
        attachment.append(PDF_SUFFIX);
        attachment.append(END_NAME);

        response.setContentType(CONTENT_TYPE);
        response.setHeader(CONTENT_DISPOSITION, attachment.toString());
        response.setHeader(CACHE_CONTROL, MAXAGE_1S);
    }
}
