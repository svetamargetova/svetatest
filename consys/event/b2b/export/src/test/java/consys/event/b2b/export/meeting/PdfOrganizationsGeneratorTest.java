package consys.event.b2b.export.meeting;

import consys.event.b2b.core.bo.AreaOfActivity;
import consys.event.b2b.core.bo.CooperationProfile;
import consys.event.b2b.core.bo.CooperationProfileType;
import consys.event.b2b.core.bo.thumb.OrganizationExportData;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.testng.annotations.Test;

/**
 *
 * @author pepa
 */
public class PdfOrganizationsGeneratorTest {

    @Test(groups = {"export"})
    public void createPdfOrganization() throws Exception {
        AreaOfActivity a11 = new AreaOfActivity();
        a11.setTitle("Modeling for design and clustering of nanotechnology sectors");
        AreaOfActivity a12 = new AreaOfActivity();
        a12.setTitle("Nanotechnology for sustainable development, energy and health");
        AreaOfActivity a13 = new AreaOfActivity();
        a13.setTitle("Safety in nanotechnology (assessment and handling)");
        AreaOfActivity a14 = new AreaOfActivity();
        a14.setTitle("Sustainable materials and ''Life Cycle Assessment''");

        AreaOfActivity a1 = new AreaOfActivity();
        a1.setTitle("Call NMP Framework Programme");
        a1.getChildrens().add(a11);
        a1.getChildrens().add(a14);
        a1.getChildrens().add(a14);

        AreaOfActivity a21 = new AreaOfActivity();
        a21.setTitle("Energy efficient buildings (EeB)");
        AreaOfActivity a22 = new AreaOfActivity();
        a22.setTitle("Green Cars (GC)");

        AreaOfActivity a2 = new AreaOfActivity();
        a2.setTitle("Public Private Partnership ");
        a2.getChildrens().add(a21);
        a2.getChildrens().add(a22);

        CooperationProfileType cpt1 = new CooperationProfileType();
        cpt1.setTypeName("Technology/Expertise offered or requested");
        cpt1.setOffer(true);
        cpt1.setRequest(true);

        CooperationProfileType cpt2 = new CooperationProfileType();
        cpt2.setTypeName("Partner(s)/consortium needed for FP7 R&D cooperation");
        cpt2.setRequest(true);

        CooperationProfile cp1 = new CooperationProfile();
        cp1.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam adipiscing dui in massa tincidunt convallis elementum elit congue. Donec tempus laoreet erat lobortis dignissim. Proin ornare molestie molestie. Nulla pretium, erat vel ultrices eleifend, nisi nibh pretium odio, sit amet sollicitudin ante nisl ut dolor. Proin ipsum nibh, scelerisque in rhoncus non, pretium pretium eros. Mauris pretium tempus urna ut posuere. Donec varius elementum vulputate. Aenean velit nibh, feugiat id adipiscing sed, convallis id nibh. Nulla id porta mauris."
                + "Nunc vel massa a elit venenatis aliquam. Integer rhoncus lectus sit amet mi faucibus a faucibus mi lacinia. Pellentesque elementum iaculis ante, ac tristique diam sagittis a. Cras vehicula eleifend odio, et pulvinar metus gravida nec. Curabitur sagittis, ante sed lobortis pellentesque, ante nisi semper elit, eget sollicitudin purus justo ut dolor. Integer pulvinar justo nec neque imperdiet tempor. In ac elit magna, at tristique nibh. Nulla pharetra luctus risus, porttitor scelerisque dui facilisis a. Cras lacinia cursus orci vel porttitor. Nulla sagittis varius fringilla. Phasellus orci nunc, rhoncus in fringilla eu, faucibus quis erat."
                + "Vivamus eleifend ante sit amet sem semper varius non at lectus. Nam auctor mi nec magna molestie quis euismod nisi gravida. Nulla nisl lacus, accumsan in facilisis non, volutpat ut felis. Etiam sapien ante, venenatis et luctus ut, ultrices non sem. Praesent ornare elementum lorem sit amet dignissim. Ut eu odio vel dolor fermentum dictum. Curabitur tempor nibh vel purus suscipit mattis. Mauris in gravida dui. Sed aliquet sapien nec diam molestie tristique. Ut luctus, mauris vitae aliquam dictum, justo nunc sagittis libero, sed gravida mi purus in sapien."
                + "Quisque eros sapien, semper quis sollicitudin sit amet, fermentum quis tortor. Maecenas ultricies justo et dui aliquet vel aliquet massa pretium. Mauris placerat nisl a metus mollis nec congue est varius. Duis gravida feugiat eros, a gravida felis viverra vel. Ut hendrerit tellus in mi sagittis feugiat eleifend quis neque. Mauris auctor cursus leo a sagittis. Vestibulum sodales turpis nec erat venenatis eget aliquet dui pharetra. Nam quis nisl non orci dictum venenatis ac vel nunc."
                + "Fusce eros tellus, vestibulum vel feugiat a, porttitor at nisl. Praesent dictum orci sollicitudin quam condimentum sit amet scelerisque metus posuere. Pellentesque sodales feugiat neque, sed consectetur sapien rutrum vitae. Aenean commodo, odio sed sagittis ultrices, augue lorem dignissim metus, at facilisis nibh ipsum eget arcu. Fusce pellentesque suscipit odio. Sed varius massa vel nibh porttitor at feugiat lectus condimentum. Maecenas tincidunt faucibus augue, quis viverra diam cursus quis. Donec eget turpis vel lectus sodales gravida. Donec suscipit enim in purus rutrum interdum. Nam risus odio, malesuada a tristique tincidunt, luctus vulputate tortor. Curabitur aliquet elementum magna, feugiat pellentesque arcu posuere eu. Sed purus nulla, dictum non tempus ac, porta sit amet nibh. Proin ultrices eleifend dolor, non ullamcorper nunc egestas ac. Sed feugiat, ipsum commodo tempor mollis, enim mauris lacinia justo, a condimentum dolor elit vitae nisl. Sed risus urna, adipiscing vel placerat vitae, imperdiet vitae magna. ");
        cp1.setTitle("Kooperační profil jedna");
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt1);
        cp1.getCooperationProfileTypes().add(cpt2);

        CooperationProfile cp2 = new CooperationProfile();
        cp2.setDescription("<div class=\"MsoNormal\" style=\"line-height: normal; margin: 0cm 0cm 0pt;\"><span><span style=\"font-size: small;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">AMIRES is a consulting company for research, development and innovation projects. Our main strength is the creation of new international sustainable partnerships within innovation focused value chains. Our developed projects remain the main enablers for our industrial clients and their growth and provide the important opportunities for R&amp;D community.</span></span></span></div>"
                + "<div class=\"MsoNormal\" style=\"line-height: normal; margin: 0cm 0cm 0pt;\">&nbsp;</div>"
                + "<div class=\"MsoNormal\" style=\"line-height: normal; margin: 0cm 0cm 0pt;\"><span><span style=\"font-size: small;\"><span style=\"font-family: tahoma, arial, helvetica, sans-serif;\">We are specialized in creation of consortia of partners with common aim for product or service innovation with significant potential for know-how commercialization. Unique and profound knowledge of the European public funding environment (e.g. 7th Framework Programme EU for research and technological development) as well as broad network of market key-players contribute to the high quality of entrusted projects. We follow projects from their initiation and planning, through negotiation, execution and management to the final stage, where exploitation of a new technologies, products or services is facilitated.</span></span></span></div>");
        cp2.setTitle("Kooperační profil dva");
        cp2.getCooperationProfileTypes().add(cpt1);

        OrganizationExportData data1 = new OrganizationExportData();
        data1.setTitle("AMIRES");
        data1.setCity("City");
        data1.setCountry("Country");
        data1.setDescription("Popisek něčeho nevím čeho");
        data1.setPhone("+000 1234 566 789");
        data1.setStreet("Street");
        data1.setWeb("www.name.web");
        data1.setZip("zip");
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a1);
        data1.getAreas().add(a2);
        data1.getAreas().add(a2);
        data1.getAreas().add(a2);
        data1.getAreas().add(a2);
        data1.getAreas().add(a2);
        data1.getCooperationProfiles().add(cp1);
        //data1.getCooperationProfiles().add(cp2);

        List<OrganizationExportData> list = new ArrayList<OrganizationExportData>();
        list.add(data1);
        list.add(data1);
        list.add(data1);

        PdfOrganizationsGenerator gen = new PdfOrganizationsGenerator();
        gen.createPdfOrganization(new FileOutputStream(new File("c:\\data\\organizationMeeting.pdf")), list, true);
    }
}
