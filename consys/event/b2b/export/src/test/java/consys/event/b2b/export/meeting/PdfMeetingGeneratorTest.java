package consys.event.b2b.export.meeting;

import consys.event.b2b.core.bo.thumb.MeetingExportData;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.SystemUtils;
import org.testng.annotations.Test;

/**
 *
 * @author pepa
 */
public class PdfMeetingGeneratorTest {

    @Test(groups = {"export"})
    public void createPdfMeeting() throws Exception {
        MeetingExportData info = new MeetingExportData();
        info.setMeetingTime(new Date());
        info.setTableIdx(new BigInteger("5"));
        info.setAgentName("Moje meno");       
        info.setOrganizationTitle("Association of Chemical Industry of the Czech Republic profile Association of Chemical Industry of the Czech Republic profile");
        info.setOrganizationDescription("popisek organizace");
        //info.setProfileTitle("Association of Chemical Industry of the Czech Republic profile Association of Chemical Industry of the Czech Republic profile");
        info.setProfileDescription("poznámka<ul><li>odrážka jedna</li><li>druhá odrážka</li></ul> popisek super hyper mega truper organizace <p>--Šíleně žluťoučký koníček úpěl ďábelské ódy--&#269;--</p>");
        info.setNotes(":-)<ul><li>odrážka jedna</li><li>druhá odrážka</li></ul>nějaký teěxt <p>odstavcový text</p><ul><li>odrážka jedna</li><li>druhá odrážka</li></ul>");

        List<MeetingExportData> list = new ArrayList<MeetingExportData>();
        list.add(info);
        list.add(info);
        
        PdfMeetingGenerator gen = new PdfMeetingGenerator();

        if (SystemUtils.IS_OS_LINUX) {
            gen.createPdfMeeting(new FileOutputStream(new File("/tmp/meeting.pdf")), info, true);
        } else if (SystemUtils.IS_OS_WINDOWS) {
            gen.createPdfMeeting(new FileOutputStream(new File("c:\\data\\meeting.pdf")), info, true);
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            gen.createPdfMeeting(new FileOutputStream(new File("/Users/palo/meeting.pdf")), info, true);
        }
        
        
    }
}
