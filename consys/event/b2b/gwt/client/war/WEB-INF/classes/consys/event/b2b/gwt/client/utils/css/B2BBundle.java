package consys.event.b2b.gwt.client.utils.css;

import com.google.gwt.resources.client.ClientBundle;

/**
 * 
 * @author pepa
 */
public interface B2BBundle extends ClientBundle {

    @Source("b2b.css")
    public B2BCssResources css();
}
