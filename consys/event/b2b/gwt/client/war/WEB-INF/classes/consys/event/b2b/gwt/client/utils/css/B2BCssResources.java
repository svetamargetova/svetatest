package consys.event.b2b.gwt.client.utils.css;

import com.google.gwt.resources.client.CssResource;

/**
 * 
 * @author pepa
 */
public interface B2BCssResources extends CssResource {
}
