package consys.event.b2b.gwt.client.bo;

public class ClientMeetingDetails extends AbstractClientBO {

    private static final long serialVersionUID = 5455772720743779482L;

    private String description;

    public ClientMeetingDetails() {
    }

    public ClientMeetingDetails(String name, String description) {
	this.setName(name);
	this.description = description;
    }

    public String getDescription() {
	return this.description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

}
